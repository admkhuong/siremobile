﻿<cfparam name="datatable_columnModel" default="#Arraynew(1)#">
﻿<cfparam name="datatable_filterId" default="">
﻿<cfparam name="datatable_jsCallback" default="">
﻿<cfparam name="datatable_showFilterDashboard" default="false">
<style type="text/css">
	#DataTableFilterContainer .filter-btn{
		border: 1px solid #0888D1;
	    /*font-family: verdana;*/
		height:22px;
	    color: #FFFFFF !important;
		margin-left:10px;
	 	background-color: #006DCC;
	    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
	    background-repeat: repeat-x;
	    color: #FFFFFF;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
		border-radius: 4px;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 12px;
	    margin-bottom: 0;
	    padding: 4px 12px;
	    text-align: center;
	    vertical-align: middle;
	}
	#DataTableFilterContainer .filter-btn:hover{   
		background: -moz-linear-gradient(center top , #0095CC, #00678E) repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	#DataTableFilterContainer .btn{
		padding:0px;
	}
	#DataTableFilterContainer .datatable_filter_row{
		margin-top:8px;
	}
	#DataTableFilterContainer .btn-primary{
		height: 22px;
	}
	#DataTableFilterContainer input.filter_val {
	    border: 1px solid #CCCCCC;
	    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
		background-color: #F5F5F5;
	    border-radius: 4px;
	    color: #555555;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 10px;
	    padding: 4px 6px;
	    vertical-align: middle;
	}
	#DataTableFilterContainer input.filter_val:hover {
		border-color:rgba(82, 168, 263, 0.8);
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(82, 168, 236, 0.6);
	    outline: 0 none;
	}
	
	
	#DataTableFilterContainer .ui-datepicker .ui-widget-content{
		width:200px;
	}
		
	#DataTableFilterContainer .ui-custom-css{
		margin-left:-2px!important;
	}
		
		
	.datatable_filter_dropdownlist
	{
		background-color: #f5f5f5;
		border: 1px solid #cccccc;
		border-radius: 4px;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
		color: #555555;
		display: inline-block;
		font-size: 1em;
		line-height: 1.5em;
		margin-bottom: 10px;
		padding: 4px 6px;
		transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
		vertical-align: middle;			
	}
	
	
</style>
<cfoutput>
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/cf_table.css" />
</cfoutput>

<!---validate messages --->
<cfset ISNOTNUMBER = "Is not a number">
<cfset ISNOTDATE = "Is not a date">
<cfset CANNOTBEEMPTY = "Cannot be empty">
<cfset FIELDREQUIRED = "This field is required">

<!---list of data type --->
<cfset LIST = "LIST">
<cfset TEXT = "TEXT">
<cfset DATE = "DATE">
<cfset INTEGER = "INTEGER">

<!---list of operator --->
<!--- {VALUE='NOT LIKE', DISPLAY='NOT Similar To', INLIST='#DATE#'}, --->
<cfset
	operatorKeys = [
		{VALUE='=', DISPLAY='Is', INLIST='#TEXT#,#INTEGER#,#LIST#'},
		{VALUE='LIKE', DISPLAY='Is', INLIST='#DATE#'},
		{VALUE='<>', DISPLAY='Is Not', INLIST='#TEXT#,#INTEGER#,#LIST#'},
		{VALUE='>', DISPLAY='Is Greater Than', INLIST='#INTEGER#'},
		{VALUE='<', DISPLAY='Is Less Than', INLIST='#INTEGER#'},
		{VALUE='<', DISPLAY='Is Before', INLIST='#DATE#'},
		{VALUE='>', DISPLAY='Is After', INLIST='#DATE#'},
		{VALUE='LIKE', DISPLAY='Similar To', INLIST='#TEXT#'},
		{VALUE='NOT LIKE', DISPLAY='NOT Similar To', INLIST='#TEXT#'}
	]>
	
<!---show list of data filter--->
 
<!---list of cf_sql_type --->
<cfset validFieldType = [
			'CF_SQL_BIGINT',
			'CF_SQL_BIT',
			'CF_SQL_CHAR',
			'CF_SQL_BLOB',
			'CF_SQL_CLOB',
			'CF_SQL_DATE',
			'CF_SQL_DECIMAL',
			'CF_SQL_DOUBLE',
			'CF_SQL_FLOAT',
			'CF_SQL_IDSTAMP',
			'CF_SQL_INTEGER',
			'CF_SQL_LONGVARCHAR',
			'CF_SQL_MONEY',
			'CF_SQL_MONEY4',
			'CF_SQL_NUMERIC',
			'CF_SQL_REAL',
			'CF_SQL_REFCURSOR',
			'CF_SQL_SMALLINT',
			'CF_SQL_TIME',
			'CF_SQL_TIMESTAMP',
			'CF_SQL_TINYINT',
			'CF_SQL_VARCHAR'
		]>
<!---build template for filter rows --->
<cfoutput>
	<script id="filter_row_template_#datatable_filterId#" type="text/x-jquery-tmpl"> 
		<div class="datatable_filter_row row filter_row">
			<select class="datatable_filter_dropdownlist" rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
				<cfloop array="#datatable_ColumnModel#" index="columnItem">
					<option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
						#columnItem.DISPLAY_NAME#
					</option>
				</cfloop>
			</select>
			<select class="datatable_filter_dropdownlist" rel="operator">
				<cfloop array="#operatorKeys#" index="operatorItem">
					<option value="#operatorItem.VALUE#" >
						#operatorItem.DISPLAY#
					</option>
				</cfloop>
			</select>
			
			<input type="text"  rel="datatable_filter_text" class="filter_val" value="">
			<span style="color:red;display:none;" rel="datatable_filter_validate"></span>
			<div class="action">
				<!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" href="javascript:void(0);">--->
				<a class="delete_filter left" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
					<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
				</a>
				<a class="add_filter left"  href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
					<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
				</a>
			</div>
		</div>
	</script>	
</cfoutput>


<!---

<!---this is filter rows --->
        <div id="filter_rows">
            <div class="datatable_filter_row row filter_row">
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <select class="datatable_filter_dropdownlist"  rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
                        <cfloop array="#datatable_ColumnModel#" index="columnItem">
                            <option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
                                #columnItem.DISPLAY_NAME#
                            </option>
                        </cfloop>
                    </select>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <select class="datatable_filter_dropdownlist" rel="operator">
                        <cfloop array="#operatorKeys#" index="operatorItem">
                            <option value="#operatorItem.VALUE#" >
                                #operatorItem.DISPLAY#
                            </option>
                        </cfloop>
                    </select>
                </div>
    
    			<div class="col-md-3 col-sm-6 col-xs-12">
                	<input type="text"  rel="datatable_filter_text" class="filter_val" value="">
                </div>
                
                <span style="color:red;display:none;" rel="datatable_filter_validate"></span>
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="action">
                        <!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" style="display:none;" href="javascript:void(0);">--->
						<a class="delete_filter left" style="display:none;" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
							<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
						</a>
						<a class="add_filter left" href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
							<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
						</a>
						</div>
                </div>
            </div>
        </div>

--->


<!---

<!---this is filter rows --->
        <div id="filter_rows">
            <div class="datatable_filter_row row filter_row">
                <select class="datatable_filter_dropdownlist"  rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
                    <cfloop array="#datatable_ColumnModel#" index="columnItem">
                        <option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
                            #columnItem.DISPLAY_NAME#
                        </option>
                    </cfloop>
                </select>
                <select class="datatable_filter_dropdownlist" rel="operator">
                    <cfloop array="#operatorKeys#" index="operatorItem">
                        <option value="#operatorItem.VALUE#" >
                            #operatorItem.DISPLAY#
                        </option>
                    </cfloop>
                </select>
    
                <input type="text"  rel="datatable_filter_text" class="filter_val" value="">
                <span style="color:red;display:none;" rel="datatable_filter_validate"></span>
                <div class="action">
                    <!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" style="display:none;" href="javascript:void(0);">--->
                    <a class="delete_filter left" style="display:none;" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
                    </a>
                    <a class="add_filter left" href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
                    </a>
                </div>
            </div>
        </div>


--->


<cfoutput>

    <div id="DataTableFilterContainer">
    <div class="filter_box" id="filter_box_#datatable_filterId#">
        <!---this div contain caption for filter --->
        <div style="background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');" class="actions_filter_box">
            <span class="filter_title">Filters</span>
            <span style="display:none;" id="show_action_#datatable_filterId#" class="filter_action">
                <a onclick="ShowFilterDatatable_#datatable_filterId#(); return false;" href="javascript:void(0);">
                    <img width='25' height='25' alt='Show Filter Fields' src="#rootUrl#/#PublicPath#/images/up_btn.jpg">
                </a>
            </span>
            <span style="display:block;" id="hide_action_#datatable_filterId#" class="filter_action">
                <a onclick="HideFilterDatatable_#datatable_filterId#(); return false;" href="javascript:void(0);">
                    <img width="25" height="25" alt="Hide Filter Fields" src="#rootUrl#/#PublicPath#/images/down_btn.jpg">
                </a>
            </span>	
        </div>
        
        <!---this is filter rows --->
        <div id="filter_rows">
            <div class="datatable_filter_row row filter_row">
                <select class="datatable_filter_dropdownlist"  rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
                    <cfloop array="#datatable_ColumnModel#" index="columnItem">
                        <option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
                            #columnItem.DISPLAY_NAME#
                        </option>
                    </cfloop>
                </select>
                <select class="datatable_filter_dropdownlist" rel="operator">
                    <cfloop array="#operatorKeys#" index="operatorItem">
                        <option value="#operatorItem.VALUE#" >
                            #operatorItem.DISPLAY#
                        </option>
                    </cfloop>
                </select>
    
                <input type="text"  rel="datatable_filter_text" class="filter_val" value="">
                <span style="color:red;display:none;" rel="datatable_filter_validate"></span>
                <div class="action">
                    <!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" style="display:none;" href="javascript:void(0);">--->
                    <a class="delete_filter left" style="display:none;" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
                    </a>
                    <a class="add_filter left" href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
                    </a>
                </div>
            </div>
        </div>

        <!---button for submit filter --->
        <div style="background-image:url('#rootUrl#/#PublicPath#/images/backgroud_bottom_table.jpg');" id="filter_submits">
            <div style="float:right; margin-right: 1em; margin-top: 0.7em;" class="row" value="3" id="filter_row_3">
                <cfif datatable_showFilterDashboard eq 'true'>
                    <a class="filter-btn btn-primary dropdown-toggle" onclick="ComboBatchListReporting_#datatable_filterId#(); return false;" href="javascript:void(0);"><img title="Reports" height="24px" width="24px" src="#rootUrl#/#publicPath#/images/pie-chart-icon24x24.png"><span style="float:right;">Filtered Dashboard</span></a>
                </cfif>
                <a class="filter-btn btn-primary dropdown-toggle" onclick="ClearFilter_#datatable_filterId#(); return false;" href="javascript:void(0);">Clear</a>
                <a class="filter-btn btn-primary dropdown-toggle" onclick="ApplyFilter_#datatable_filterId#(); return false;" href="javascript:void(0);">Apply Filter</a>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div style="margin-top:10px">
    </div>

</div>

</cfoutput>
<cfoutput>
<script type="text/javascript">
	var addressFormatting_#datatable_filterId# = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
	//this func will init controls in filter box like dropdown, etc
	function InitDataTableFilterControl_#datatable_filterId#(){
		
		UpdateOperator_#datatable_filterId#($("##filter_box_#datatable_filterId# .datatable_filter_dropdownlist:first"));
	}
	
	//this function will remove a filter row
	//callerObject is button triggering this function
	$( document ).ready(function(){		
		
	});
	
	//add new filter row 
	//callerObject is button triggering this function
	function AddFilterRow_#datatable_filterId#(callerObject){
		//use jquery template to add html right after container div of callerObject
		$(callerObject).parent().parent().after($("##filter_row_template_").tmpl());
		
		//update operator for recent added filter row
		UpdateOperator_#datatable_filterId#($(this).parent().parent().next().find(".datatable_filter_dropdownlist:first"));
		
		
		//display all delete button
		that_parent = $(this).parent().parent().parent().parent();
		$(that_parent).find(".delete_filter").show();
		
		//UpdateOperator(that_parent);
		return false;
		
		
	}		
	
	//this function will remove a filter row
	//callerObject is button triggering this function
	function RemoveFilterRow_#datatable_filterId#(callerObject){
		
		var numberOfFilterRow = $(".datatable_filter_row").size();
		//if there is only one filter row left, just hide its delete button
		if(numberOfFilterRow <=2){
			$(".delete_filter").hide();
		}
		//remove html of container div of this button
		$(callerObject).parent().parent().remove();
			
	}
	
	//show filter box when triggered
	function ShowFilterDatatable_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##hide_action_#datatable_filterId#").show();
		$("##filter_box_#datatable_filterId# ##filter_rows").toggle("slow");
		$("##filter_box_#datatable_filterId# ##show_action_#datatable_filterId#").hide();
	}
	
	//hide filter box when triggered
	function HideFilterDatatable_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##show_action_#datatable_filterId#").show();
		$("##filter_box_#datatable_filterId# ##filter_rows").toggle("slow");
		$("##filter_box_#datatable_filterId# ##hide_action_#datatable_filterId#").hide();
	}
	
	//clear filter
	function ClearFilter_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##filter_rows").html("");
		$("##filter_box_#datatable_filterId# ##filter_rows").append($("##filter_row_template_#datatable_filterId#").tmpl());
		//use select menu for dropdowns
		InitDataTableFilterControl_#datatable_filterId#();
		
		var numberOfFilterRow = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row").size();
		//if there is only one filter row left, just hide its delete button
		if(numberOfFilterRow <=1){
			$("##filter_box_#datatable_filterId# .delete_filter").hide();
		}
		
		if('#datatable_jsCallback#' != ''){
			#datatable_jsCallback#();
		}
	}
	
	//apply filter 	
	function ApplyFilter_#datatable_filterId#(){
		//validate first, if false, return
		if(!Validate_#datatable_filterId#()) return false;
		//if true, call js call back function
		var filterData = new Array();
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		for(var i=0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var fieldName = $(filterRowItem).find("select[rel=column]").find(":selected").val();
			var operator = $(filterRowItem).find("select[rel=operator]").find(":selected").val();
			var fieldType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('sqlType');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val()!=""?$(filterRowItem).find("input[rel=datatable_filter_text]").val():$(filterRowItem).find("select[rel=listDataFilter]").val();
			
			//we need to custom data for date field
			if(fieldType == "CF_SQL_TIMESTAMP"){
				var tempDateTime = inputValue.split("/")[2] +"-"+inputValue.split("/")[0]+"-"+inputValue.split("/")[1];
				inputValue = tempDateTime;
				if(operator ==">"){
					inputValue += " 00:00:00";
				}
				if(operator =="<"){
					inputValue += " 23:59:59";
				}
			}
			filterData.push({
				NAME: fieldName,
				OPERATOR: operator,
				TYPE: fieldType,
				VALUE:inputValue
			});
		}
		if('#datatable_jsCallback#' != ''){
			#datatable_jsCallback#(filterData);
		}
	}
	
	//validate data of filter rows
	function Validate_#datatable_filterId#(){
		var result = true;
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		//loop over filter rows to validate
		for(var i = 0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var errorSpan = $(filterRowItem).find("span[rel=datatable_filter_validate]");
			var filterType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('filterType');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val();
			
			//validate empty
			if (filterType == '#INTEGER#' || filterType == '#TEXT#'||filterType == '#DATE#') {
				if (inputValue == "") {
					$(errorSpan).html("#FIELDREQUIRED#");
					$(errorSpan).show();
					result = false;
				}else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
			//validate number
			if (filterType =='#INTEGER#' && inputValue != '') {
				if (!isNumber(inputValue)) {
					$(errorSpan).html("#ISNOTNUMBER#");
					$(errorSpan).show();
					result = false;
				}
				else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
			//validate date
			if (filterType == '#DATE#') {
				if(!IsValidDate_#datatable_filterId#(inputValue)){
					$(errorSpan).html("#ISNOTDATE#");
					$(errorSpan).show();
					result = false;
				}else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
		}	
		return result;
	}
	
	//we need to regenerate operator dropdown options whenever user reselect column filter name 
	function UpdateOperator_#datatable_filterId#(callerObject){
		//get field type of column
		var filterType = $(callerObject).find(":selected").attr('filterType');

		// get object select value		
		var objectSelectValue = $(callerObject).find(":selected").val();

		// check filterType is List or not		
		if(filterType == 'LIST'){
			// if true to hide filter_val textbox
			$(callerObject).parent().find('.filter_val').hide();
			var dataFilterList = '';
			// remove a listDataFilter because onchange event of selectmenu class is triggered 2 time 
			$(callerObject).parent().find('.listDataFilter').remove();
			// fill data to selectmenu
			<cfoutput>
				dataFilterList += '<select class="datatable_filter_dropdownlist listDataFilter" rel="listDataFilter">';
					<cfloop array="#datatable_ColumnModel#" index="columnItem">
						<cfif #columnItem.TYPE# eq "LIST">
							if("#columnItem.SQL_FIELD_NAME#" == objectSelectValue){
								<cfloop array="#columnItem.LISTOFVALUES#" index="listItem">
									dataFilterList += '<option value="#listItem.VALUE#" >';
										dataFilterList += '#listItem.DISPLAY#';
									dataFilterList += '</option>';
								</cfloop>
							}
						</cfif>
					</cfloop>
				dataFilterList += '</select>';
			</cfoutput>
			// append to page
			$(callerObject).parent().append(dataFilterList);
			
		}else{
			// show filter_val textbox
			$(callerObject).parent().find('.filter_val').show();
			// remove listData
			$(callerObject).parent().find('.listDataFilter').remove();
		}
		
		var operators = new Array();//this is operator to be used to generate dropdown options below
		var operatorKeys = <cfoutput>#serializeJSON(operatorKeys)#</cfoutput>;
		for (var i = 0; i < operatorKeys.length; i++) {
			if (operatorKeys[i].INLIST.indexOf(filterType) > -1){
				//if operatorKeys item has this datatype, add this operator
				operators.push(operatorKeys[i])
			}
		}
		//now we have list of operator
		var operatorDropDown = $(callerObject).parent().find("select[rel=operator]");
		operatorDropDown.find('option').remove();
			
		for (var i = 0; i < operators.length; i++) {
			operatorDropDown.append(new Option(operators[i].DISPLAY, operators[i].VALUE));
		}
			
		//reset text of input field 
		var inputField = $(callerObject).nextAll(".filter_val");
		$(inputField).val('');
		//if filterType is DATE, init datepicker in input field
		if(filterType == '<cfoutput>#DATE#</cfoutput>'){
			$(inputField).datepicker({
				inline: true,
				calendars: 4,
				extraWidth: 100,
				dateFormat: 'mm/dd/yy' 
			}).attr('readonly','true');
		}else{
			$(inputField).datepicker("destroy").removeAttr('readonly');
		}
		
		// check content of filter name 
		// if filter name too long then cut filter name 20 characters
		var allContent = $('span .ui-selectmenu-item-content');
		allContent.each(function(index) {
				$(this).text($.trim($(this).text()));
				if($(this).text().length > 16){
					$(this).text($(this).text().substr(0,16) + '...');
				}
				
				$(this).parent().parent().css('min-width','188px');
				<!---$(this).parent().parent().css('float','left');--->
				
			});
		}
	
	
	function IsValidDate_#datatable_filterId#(input){
		var monthfield=input.split("/")[0];
		var dayfield=input.split("/")[1];
		var yearfield=input.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield)
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield)){
			return false;
		}
		return true;
	}
	
	
	function ComboBatchListReporting_#datatable_filterId#()
	{	
		var filterData = new Array();
		
		//if true, call js call back function
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		for(var i=0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var fieldName = $(filterRowItem).find("select[rel=column]").find(":selected").val();
			var operator = $(filterRowItem).find("select[rel=operator]").find(":selected").val();
			var fieldType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('sqlType');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val()!=""?$(filterRowItem).find("input[rel=datatable_filter_text]").val():$(filterRowItem).find("select[rel=listDataFilter]").val();
			
			//we need to custom data for date field
			if(fieldType == "CF_SQL_TIMESTAMP"){
				var tempDateTime = inputValue.split("/")[2] +"-"+inputValue.split("/")[0]+"-"+inputValue.split("/")[1];
				inputValue = tempDateTime;
				if(operator ==">"){
					inputValue += " 00:00:00";
				}
				if(operator =="<"){
					inputValue += " 23:59:59";
				}
			}
			
			if(typeof(inputValue) != "undefined")
			{
				if(inputValue.trim != "")
				{
				
					filterData.push({
						NAME: fieldName,
						OPERATOR: operator,
						TYPE: fieldType,
						VALUE:inputValue
					});
	
				}
			}
		}
		
		
		if(filterData.length > 0)
		{			
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetBatchListFromFilters&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async:false,
				data:  { 
					customFilter : JSON.stringify(filterData)
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {					
				},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{								
						
						if(d.RXRESULTCODE > 0)
						{
							<!--- Validate count not too many? 100--->
							if(d.BATCHLIST == "" && d.RECORDCOUNT == 0)
							{
								window.location = "<cfoutput>#rootUrl#/#sessionPath#/reporting/reportbatch</cfoutput>";							
							}
							else if(d.RECORDCOUNT < 100)
							{
								window.location = "<cfoutput>#rootUrl#/#sessionPath#/reporting/reportbatch?inpBatchIdList=</cfoutput>" + d.BATCHLIST;							
							}						
						}
					} 		
			});
		
		}
		else
		{
			window.location = "../reporting/reportbatch";				
		}
	
	}
	
	
	InitDataTableFilterControl_#datatable_filterId#();
	
	
	
</script>
</cfoutput>