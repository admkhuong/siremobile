﻿
<cfparam name="mode" default="" />
<cfparam name="INPBATCHID" default="" />

<style>
	<cfoutput>
		@import url('#rootUrl#/#PublicPath#/css/campaign/mycampaign/schedule.css');
	</cfoutput>
</style> 

<cfif mode EQ "edit">	
	<input type="hidden" value='<cfoutput>#serializeJSON(SCHEDULE)#</cfoutput>' id="SCHEDULEDATA">
</cfif>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h4 class="modal-title">Schedule Options</h4>

</div>
           
<input type="hidden" value='<cfoutput>#INPBATCHID#</cfoutput>' id="INPBATCHID" name="INPBATCHID">
           
<div class="modal-body">
	<div id="AdvanceScheduleContent">
        <div id='SetScheduleSBDiv' class="RXFormXXX">
            <div id="RightStage">
                    <div class="myScheduleType">	                
                        <div style="clear: both;">
                            <div class="row">
                                <input 
                                    id="rdoSchedultType_1" style="width: 15px;"
                                    name="SCHEDULETYPE" 
                                    type="radio" 
                                    value="1"
                                    checked="checked">
                                <label for="rdoSchedultType_1">Deliver starting on this date</label>
                            </div>
                            <div class="row_label">
                                You can always change the scheduled time before the campaign is started and while it is paused
                            </div>
                            <div class="row_label">
                                <span class="following_time_label">Date (mm/dd/yy)</span>
                                <select id="FollowingTimeDate_Month" onchange="setValForDatePicker('FollowingTimeDate');" class="month_list month" style="width:70px;"></select>
                                <select id="FollowingTimeDate_Day" onchange="setValForDatePicker('FollowingTimeDate');" class="day_list margin_left5 day"></select>
                                <select id="FollowingTimeDate_Year" onchange="setValForDatePicker('FollowingTimeDate');" class="year_list margin_left5 year"></select>
                                <input type="button" onchange="SelectDate(this)" class="hidden_date_picker"  value="" id="FollowingTimeDate">
                            </div>
                        </div>
                        <div style="clear: both;">
                            <div class="row">
                                <input 
                                    id="rdoSchedultType_2" style="width: 15px;"
                                    name="SCHEDULETYPE" 
                                    type="radio" 
                                    value="2">
                                <label for="rdoSchedultType_2">Deliver on a custom schedule</label>
                            </div>
                            <div class="row_label">
                                You can always change the scheduled time before the campaign is started and while it is paused
                            </div>
                            <div class="row_label">
                                <span class="across_multiple_days_label">Start Date (mm/dd/yy)</span>
                                <select id="AcrossMultipleDaysStartDate_Month" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="month_list month multiple_days"></select>
                                <select id="AcrossMultipleDaysStartDate_Day" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="day_list day multiple_days"></select>
                                <select id="AcrossMultipleDaysStartDate_Year" onchange="setValForDatePicker('AcrossMultipleDaysStartDate');" class="year_list year multiple_days"></select>
                                <input type="button" onchange="SelectDate(this); DoCheckAll();" class="hidden_date_picker"  value="" id="AcrossMultipleDaysStartDate">
                            </div>
                            <div class="row_label">
                                <span class="across_multiple_days_label">End Date (mm/dd/yy)</span>
                                <select id="AcrossMultipleDaysEndDate_Month" onchange="setValForDatePicker('AcrossMultipleDaysEndDate');" class="month_list month multiple_days"></select>
                                <select id="AcrossMultipleDaysEndDate_Day" onchange="setValForDatePicker('AcrossMultipleDaysEndDate');" class="day_list day multiple_days"></select>
                                <select id="AcrossMultipleDaysEndDate_Year" class="year_list year multiple_days"  onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"></select>
                                <input type="button" onchange="SelectDate(this); DoCheckAll();" class="hidden_date_picker"  value="" id="AcrossMultipleDaysEndDate">
                            </div>
                            <div id="divDaySelector">
                                <ul id="MyscheduleTabs" class="MyscheduleTabs nav nav-tabs" style="background: none;">
                                </ul>
                                <div class="row_label">
                                    <label>
                                        * The campaign will  run on <img alt="" width="13" height="13" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/check_box.png"/> days of the week
                                    </label>
                                </div>
                            </div>
                            <!---<div style="float:right;"> 
                                <a class="button filterButton small btn-auu-admin" onclick="updateSchedule();" href="javascript:void(0);">Use this Schedule</a>
                            </div>--->
                        </div>
                        <div style="clear:both;">
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="updateSchedule();">Save changes</button>
                        </div>
                    
                    </div>
                    <div style="clear:both;">
                    </div>
                </div>
            </div>       
    
        </div>
	
    </div>
    
</div>


<script id="tmplScheduleDay" type="text/x-jquery-tmpl">
	<li class="DSSunday">
	    <input type="checkbox" id="chkAcrossMultipleDays_${i}" value="${i}"  {{if i == 0}} checked {{/if}} class="schedule_day_checkbox {{if i == 0}} check_all {{else}} check {{/if}} onclick="SelectScheduleTab(${i})">
	    <a href="#tabs-${i}" class="tab_label">${dayName}</a>
    </li>
</script>

<script id="tmplScheduleDayTime" type="text/x-jquery-tmpl">
    <li id="tabs-${i}">
	    <div class="hourRow" >		
		    <div class="row">
			    <label title="Calls will not start until the time LOCALOUTPUT to the phone number is the start time or later." style="display:block;">Start Time</label>         
			    <div class="padding_left5">
				    <select id="cboStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="start"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboStartTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_StartTime_${i}" class="noon_list" customfield="start">
				    </select>
			    </div>
		    </div>
									                    
		    <div class="row"> 
			    <label title="Calls still in queue after this time will carry over until tommorow's start time." style="display:block;">End Time</label>            
			    <div class="padding_left5">
				    <select id="cboEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="end"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboEndTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_EndTime_${i}" class="noon_list" customfield="end">
				    </select>
			    </div>
		    </div>
	    </div>                         
	    
		<div class="hourRow">
			<div class="row last_row">
				<input type="checkbox" id="chkEnableBlachout_${i}" name="chkEnableBlachout" onclick="VisibleBlackout(${i})">
				<label for="chkEnableBlachout">Enable Blackout Start and End Time</label>
			</div>
			<div id="divBlackoutTime_${i}" style="display: none; clear: both;">
				<div class="row">
					<label style="display:block;">Blackout Start Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout start"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutStartTime_${i}" class="noon_list" customfield="start">
						</select>
					</div>
				</div>
				<div class="row">
					<label style="display:block;">Blackout End Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout end"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutEndTime_${i}" class="noon_list" customfield="end">
						</select>
					</div>
				</div>                          
			</div>
		</div>
    </li>	
</script>
<style>
	#SetScheduleSBDiv .ui-dialog-content{
		background-color:#fff;
	}
	#SetScheduleSBDiv .RXFormXXX{
		padding:10px;
	}
	#SetScheduleSBDiv .ui-tabs .ui-tabs-nav li
	{
		width:96px !important;
		border: 1px solid #CCCCCC;
	}
	#SetScheduleSBDiv .ui-tabs .ui-tabs-nav li a{
		width:69px !important;
		border: 0px;
		font-size: 10px !important;
		text-align: left;
	}
</style>
<cfinclude template="advance_schedule_js.cfm">