﻿<cfparam name="batchId" default="-1">
<cfset  retValGetEmergency = 0>
<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="GetEmergencyForClone" returnvariable="retValGetEmergency">
	<cfinvokeargument name="batchId" value="#batchId#" >
</cfinvoke>
<style type="text/css">
	.clone_error{
		color:red;
		text-indent:10px;
	}
	.clone-footer{
		border-color: #46A6DD #CCCCCC #CCCCCC;
	    border-style: solid;
	    border-width: 3px 1px 1px;
	    height: 45px;
	    margin-top: 49px;
	    padding: 10px;
	    text-align: right;
	}
	.clone-message-block{
		width:100%;
		margin-top:10px;
	}
</style>
<cfif retValGetEmergency.RXRESULTCODE NEQ 1>
	<div class="clone_error">
	<cfoutput>
		#retValGetEmergency.MESSAGE#
		<br/>
		#retValGetEmergency.ERRMESSAGE#
	</cfoutput>
	</div>
	<cfexit>
</cfif>
<div>
	<div class="message-block clone-message-block ">
	    <div class="left-input">
	    	<span class="em-lbl">Campaign</span>
			<div class="hide-info showToolTip">&nbsp;</div>
			<div class="tooltiptext">
				Coming Soon
			</div>		
			<div class="info-block" style="display:none">content info block</div>
		</div>
	    <div class="right-input"><input type="text" value="" id="campaign-name-txt"></div>
	</div>
	<div style="clear:both;">
	</div>
	<div class="clone-footer">
		<a href="javascript:void(0);" onclick="CloneItem('<cfoutput>#batchId#</cfoutput>');" class="button filterButton small btn-auu-admin">Clone item</a>
		<a href="javascript:void(0);" onclick="closeDialog();" class="button filterButton small btn-auu-admin">Cancel</a>
	</div>
</div>