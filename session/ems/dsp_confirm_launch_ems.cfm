﻿<cfparam name="INPGROUPID" default="0">
<cfparam name="INPSHORTCODE" default="0">
<cfparam name="CONTACTTYPES" default="">
<!---params below are used in campaigncontrolconsole page when we launching ems --->
<cfparam name="ISINLISTFORM" default="0">
<cfparam name="INPSCRIPTID" default="0">
<cfparam name="INPLIBID" default="0">
<cfparam name="INPELEID" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="CDFDATA" default="">
<cfparam name="CUSTOMSTRING" default="">
<cfparam name="NOTE" default="">
<cfparam name="ISAPPLYFILTER" default="1">
<cfoutput>
	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/bootstrap/dist/css/customize.css">--->
</cfoutput>
<style type="text/css">
	
	#divConfirm .left-input{
	    border-radius: 4px 0 0 4px;
	    border-style: solid none solid solid;
	    border-width: 1px medium 1px 1px;
	    color: #666666;
	    float: left;
	    height: 28px;
	    line-height: 25px;
	    padding-left: 10px;
	    width: 170px;
	}
	#divConfirm .right-input {
       width: 226px;
	}
	#divConfirm input {
       width: 226px;
	}
	#divConfirm .left-input > span.em-lbl {
       width: auto;
	}
	.m_top_10 {
	    margin-top: 10px;
	}
	
	.right-input > input {
    background-color: #fbfbfb;
    border: medium none !important;
    color: #000 !important;
    font-size: 12px !important;
    height: auto !important;
    line-height: 26px !important;
    width: 371px;
}

<!--- Override style5.css--->
.ui-dialog {
    padding-bottom: 0px;
}

#overlayheaderLaunch {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: #f3f3f3;
		filter:alpha(opacity=85);
		-moz-opacity:0.85;
		-khtml-opacity: 0.85;
		opacity: 0.85;
		z-index: 1000;
		display:none;
	}


#overlayheaderLaunchComplete {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: #f3f3f3;
		z-index: 1001;
		display:none;
	}


</style>

<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="GetConfirmationData" returnvariable="retValGETEMSConfirmationData">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
	<cfinvokeargument name="INPSHORTCODE" value="#INPSHORTCODE#">
	<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
	<cfinvokeargument name="CDFDATA" value="#CDFDATA#">
	<cfinvokeargument name="CUSTOMSTRING" value="#CUSTOMSTRING#">
	
</cfinvoke>
<cfif retValGETEMSConfirmationData.RXRESULTCODE NEQ 1>
	<div ><h4>Error getting preview data - Please try again. If problem persists, please contact your system administrator.</h4></div>
	<cfexit><!---do not generate anything if error --->
</cfif>
<cfoutput>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirm Launch EMS</h4>     
    </div>


	<div id="overlayheaderLaunch">
    
    	<div id="PreLoadIcon">
            <p>Please wait while loading ...</p>
        </div>         
    
    </div>
    
    <div id="overlayheaderLaunchComplete">
    	
        <div class="message-block  m_top_10">
            <div class="">
                <span id="LauchCompleteMessage" class="em-lbl">Launch Complete!</span>
            </div>
        </div>
            
        <div class="clear"></div>
            
        <div style="width: 210px; text-align:center; margin-top:25px;"> 
			<!---<a class="btn btn-primary dropdown-toggle" href="javascript:void(0)" onclick="location.href='campaigncontrolconsole';">OK</a>	--->	
             <button type="button" class="btn btn-primary" onclick ="location.href='campaigncontrolconsole';" data-dismiss="modal">OK</button>	
		</div>
    
    </div>

	<div id="divConfirm" align="center" style="padding: 2em;">
		<div ><h4>Are you sure you want to launch this ems?</h4></div>
		<!---do not contact --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Eligible contact</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.ELIGIBLECOUNT#" name="ELIGIBLECOUNT" id="ELIGIBLECOUNT">
			</div>				
	    </div>
		<div class="clear"></div>
        <!---do not contact --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Total Credits Needed</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.UNITCOUNT#" name="UNITCOUNT" id="UNITCOUNT">
			</div>				
	    </div>
		<div class="clear"></div>
		<!---do not contact --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Do not contact</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.DNCCOUNT#" name="DNCCOUNT" id="DNCCOUNT">
			</div>				
	    </div>
		<div class="clear"></div>
		<!---duplicate --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Duplicate</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.DUPLICATECOUNT#" name="DUPLICATECOUNT" id="DUPLICATECOUNT">
			</div>				
	    </div>
		<div class="clear"></div>
		<!---invalid timezone --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Invalid Timezone</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.INVALIDTIMEZONECOUNT#" name="INVALIDTIMEZONECOUNT" id="INVALIDTIMEZONECOUNT">
			</div>				
	    </div>
		<div class="clear"></div>
        
        <!--- replaced by Total Credits needed --->
		<!---<!---cost per unit--->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Cost per unit</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.ESTIMATEDCOSTPERUNIT#" name="ESTIMATEDCOSTPERUNIT" id="ESTIMATEDCOSTPERUNIT">
			</div>				
	    </div>
		<div class="clear"></div>--->
		<!---current in queue cost--->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Current in queue cost</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.COSTCURRENTQUEUE#" name="COSTCURRENTQUEUE" id="COSTCURRENTQUEUE">
			</div>				
	    </div>
		<div class="clear"></div>
		<!---current balance --->
		<div class="message-block  m_top_10">
	        <div class="left-input">
	        	<span class="em-lbl">Current balance</span>
			</div>
	        <div class="right-input">		        		
				<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.CURRENTBALANCE#" name="CURRENTBALANCE" id="CURRENTBALANCE">
			</div>				
	    </div>
		<div class="clear"></div>
		
	</div>
    
    <!---buttons --->
		<cfoutput>	
		<div class="modal-footer">
			<cfif INPBATCHID EQ 0>
				                
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        		<button type="button" class="btn btn-primary" onclick ="AddNewEmergency(true);" data-dismiss="modal">Save For Review</button>
                <button type="button" class="btn btn-primary" onclick ="AddNewEmergency(false);" <!---data-dismiss="modal"--->>Launch Now</button>
        
			<cfelse>
				<!---INPBATCHID NEQ 0 means this popup is invoked from emergency list when user click button launch --->
				<!---LaunchEMS is js function defined in campaigncontrolconsole --->
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        		<button type="button" class="btn btn-primary" onclick ="LauchEmergency(<cfoutput>#inpscriptid#,#inplibid#,#inpeleid#,#inpbatchid#,#inpgroupid#,'#contacttypes#','#note#',#isapplyfilter#</cfoutput>);" <!---data-dismiss="modal"--->>Launch Now</button>
                
			</cfif>
		</div>
		</cfoutput>	
        
</cfoutput>

<cfif inpbatchid GT 0>
	<script type="text/javascript">
		//Lauch emergency
		function LauchEmergency(inpscriptid, inplibid, inpeleid, inpbatchid, inpgroupid, contacttypes, note, contactfilter, isapplyfilter){
				
			var currPage = $(this).attr('pageRedirect');
			var data = {
				INPSCRIPTID: '<cfoutput>#retValGETEMSConfirmationData.INPSCRIPTID#</cfoutput>',
				INPLIBID: '<cfoutput>#retValGETEMSConfirmationData.INPLIBID#</cfoutput>',
				INPELEID: '<cfoutput>#retValGETEMSConfirmationData.INPELEID#</cfoutput>',
				INPBATCHID: '<cfoutput>#retValGETEMSConfirmationData.INPBATCHID#</cfoutput>',
				INPBATCHDESC: '<cfoutput>#retValGETEMSConfirmationData.INPBATCHID#</cfoutput>',
				INPGROUPID: '<cfoutput>#retValGETEMSConfirmationData.INPGROUPID#</cfoutput>',
				CONTACTTYPES: '<cfoutput>#retValGETEMSConfirmationData.CONTACTTYPES#</cfoutput>',
				CONTACTFILTER: '<cfoutput>#retValGETEMSConfirmationData.CONTACTFILTER#</cfoutput>',
				NOTE: '<cfoutput>#retValGETEMSConfirmationData.NOTE#</cfoutput>',
				ISAPPLYFILTER:'<cfoutput>#retValGETEMSConfirmationData.ISAPPLYFILTER#</cfoutput>' 
			};
				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddFiltersToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async:false,
				data:  data,					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.alerts.okButton = '&nbsp;OK&nbsp;';					
					bootbox.alert("No Response from the remote server. Check your connection and try again.");
				},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						<!--- Check if variable is part of JSON result string --->								
						CurrRXResultCode = d.DATA.RXRESULTCODE;	
						if(CurrRXResultCode > 0)
						{
							bootbox.alert('You have successfully launched the campaign.', function(result) { 
								location.reload(); 
							});
							return false;
						}
						else
						{							
							bootbox.alert("Campaign has NOT been launch.\n" + d.DATA.MESSAGE + "\n" + d.DATA.ERRMESSAGE, function(result) { } );
						}
					} 		
			});
			return false;																	
		}
	</script>
</cfif>
