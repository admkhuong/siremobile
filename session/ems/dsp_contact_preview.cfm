﻿<cfparam name="isGroupDetail" default="0">

<style type="text/css">

	#divCDFBox .filter-btn{
		border: 1px solid #0888D1;
	    /*font-family: verdana;*/
		height:22px;
	    color: #FFFFFF !important;
		margin-left:10px;
	 	background-color: #006DCC;
	    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
	    background-repeat: repeat-x;
	    color: #FFFFFF;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
		border-radius: 4px;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 12px;
	    margin-bottom: 0;
	    padding: 4px 12px;
	    text-align: center;
	    vertical-align: middle;
	}
	#divCDFBox .filter-btn:hover{   
		background: -moz-linear-gradient(center top , #0095CC, #00678E) repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	
	#divShowHideGroupContactFilter{
		<!---background: none repeat scroll 0 0 #DDDDDD;--->
		<!---height:40px;--->
		cursor:pointer;
		color: #666666;
		
		font-size: 13px;
		font-style: normal;
	}
	#divGroupContactFilterContent{
	     border: 1px solid #CCCCCC;	
		border-radius: 4px 4px 4px 4px;	
	}
	#divShowHideGroupContactFilter:hover{
		<!---background: none repeat scroll 0 0 #F0F0F0;--->
		<!---height:40px;--->
		cursor:pointer;
	}
	#divGroupContactFilter{
		width:100%;
	    border-radius: 4px 4px 4px 4px;
	   <!--- border: 1px solid #CCCCCC;		--->
	}
	#divShowHideGroupContactFilter span{
	    color: #0888D1 !important;
	    
	    font-size: 16px;
	    font-weight: bold;
	    line-height: 35px;
	    margin-left: 20px;
	    padding-bottom: 20px;
	    padding-top: 30px;
	}
	
	.ui-selectmenu-open ul{
		width: 168px !important;
	}
	
	#divCDFBox input.filter_val {
	    border: 1px solid #CCCCCC;
	    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
		background-color: #F5F5F5;
	    border-radius: 4px;
	    color: #555555;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 22px;
	    padding: 4px 6px;
	    vertical-align: middle;
	}
	#divCDFBox input.filter_val:hover {
		border-color:rgba(82, 168, 263, 0.8);
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(82, 168, 236, 0.6);
	    outline: 0 none;
	}
	
	#CDFFilterRows .filter_row {
		border-radius: 5px;
		height: auto;
		padding-top: 5px;
		padding-left: 7px;
		padding-bottom: 5px;
		width: 100%;
		background: none repeat scroll 0 0 #E8F4FC;
		display: inline-block;
		margin: 5px 0 0px !important;
		vertical-align: middle;
	}
	#CDFFilterRows .btn{
		padding:0;
	}
  	#CDFFilterRows .filter_row .action {
	    float: right;
	    margin-right: 5px;
	    margin-top: 6px;
	}
	
	#divCDFBox{
		margin-left: 2.5%;
    	width: 95%;		
	}
	
	#divCDFBox .actions_filter_box {
	    background: url("images/filter_header.png") repeat scroll 50% 50% #FFFFFF;
	    color: #FFFFFF;
	    font-size: 13px;
	    height: 31px;
	    line-height: 27px;
	    padding-left: 10px;
	    text-transform: uppercase;
	}
		
	#divCDFBox .filter_submit{
		padding-right: 10px;	
	}
	.preview-link{
		margin-left:10px;
		float:left;
		padding-top:5px;
	}
	
	#CDFFilterRows .ui-selectmenu-button
	{
		float:left;
		margin-right:1em;	
		min-width:188px;
	}
	
	#divCDFBox input.filter_val {
	float: left;
	margin-right: 27px;
	min-width: 188px;
}

	
</style>
<cfoutput>
	<cfset FIELDREQUIRED = "This field is required!">
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/cf_table.css" />
	<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="GetCustomDataFields" returnvariable="retValCustomDataField">
	</cfinvoke>
</cfoutput>



<cfset numberOfCdfRow = 0><!--- Moved up or will crash page if skipped this variable is used to define whether button "remove filter row" is display or not--->


<cfif mode eq 'edit'>
	<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="GetContactCountByCDFAndCustomString" returnvariable="retValGetContactCountByCDF">
		<cfinvokeargument name="CDFData" value="#serializeJson(EmergencyById.CdfFilter)#">
		<cfinvokeargument name="CustomString" value="#serializeJson(EmergencyById.ContactSringFilter)#">
		<cfinvokeargument name="INPGROUPID" value="#EmergencyById.GROUPID#">
	</cfinvoke>
</cfif>		
<div id="divGroupContactFilter" class="" style="margin-bottom:10px; margin-top:10px">
	<!---<div id="divShowHideGroupContactFilter">
		<span >
			Contact Filter
		</span>
	</div>--->
    
    <cfif isGroupDetail eq 0>
	    <div id="divShowHideGroupContactFilter">
        <a id="FilterLinkText" href='#'>Click here if you would like to review or filter contacts.</a>
	        <!---<div style="float:left; line-height: 24px;" class="TextLinkHoverStyle" id="FilterLinkText">Click here if you would like to review or filter contacts.</div>--->
	    </div>
	</cfif>
        
	<div id="divGroupContactFilterContent" style="display:none; padding: 20px 10px 10px 10px;">
	    <div class="message-block m_top_20">
	        <div class="left-input">
	        	<span class="em-lbl">Contact String</span>
				<div class="hide-info showToolTip">&nbsp;</div>
				<div class="tooltiptext">Input text to get contacts you want</div>
			</div>
	        <div class="right-input">
	        	<input type="text" value="<cfif mode eq 'edit'><cfoutput>#EmergencyById.ContactSringFilter#</cfoutput></cfif>" id="txtContactFilter"/>
			</div>
	    </div>
		<div class="clear"></div>
		<div id="divCDFBox" class="m_top_20">
			<!---this div contain caption for filter --->
			<cfif arraylen(retValCustomDataField.CDFArray) GT 0>
			<cfoutput>
				<div style="background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');" class="actions_filter_box">
					<span class="filter_title">Custom Data Field Filters</span>
					<span style="display:none;" class="filter_action">
						<a onclick="ShowFilterCDF(); return false;" href="javascript:void(0);">
							<img width='25' height='25' alt='Show Filter Fields' src="#rootUrl#/#PublicPath#/images/up_btn.jpg">
						</a>
					</span>
					<span style="display:block;" class="filter_action">
						<a onclick="HideFilterCDF(); return false;" href="javascript:void(0);">
							<img width="25" height="25" alt="Hide Filter Fields" src="#rootUrl#/#PublicPath#/images/down_btn.jpg">
						</a>
					</span>	
				</div>
			</cfoutput>

			<div id="CDFFilterRows">
				<cfif mode eq "edit" and isArray(EmergencyById.CdfFilter) and arraylen(EmergencyById.CdfFilter) GT 0>
					<cfoutput>
						<!---loop through array EmergencyById.CdfFilter to generate cdf filter rows --->
						<!---CDF is an array of structs which looks like [
													{
														"CDFID":"12",
														"ROWS":[{"OPERATOR":"=","VALUE":"abc"},{"OPERATOR":"<>","VALUE":"abc"}]
													},
													{
														"CDFID":"13",
														"ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
													}
												] --->
						<cfset numberOfCdfRow = 0><!---this variable is used to define whether button "remove filter row" is display or not--->
						<cfloop array="#EmergencyById.CdfFilter#" index = "cdfItem">
							<cfloop array="#cdfItem.ROWS#" index="cdfRowItem">
								<cfset numberOfCdfRow++>
								<div class="contact_filter_row filter_row">
									<cfif retValCustomDataField.RXRESULTCODE EQ 1 AND  arraylen(retValCustomDataField.CDFArray) GT 0 >
										<select class="contact_filter_dropdownlist" rel="column">
											<cfloop array="#retValCustomDataField.CDFArray#" index="columnItem">
												<option value="#columnItem.CdfId#" <cfif columnItem.CdfId EQ cdfItem.CDFID> selected='selected'</cfif>>
													#HTMLEditFormat(HTMLEditFormat(columnItem.VariableName))#
												</option>
											</cfloop>
										</select>
										<select class="contact_filter_dropdownlist" rel="operator">
											<option value="=" <cfif cdfRowItem.OPERATOR EQ '='> selected='selected'</cfif>>
												Is
											</option>
											<option value="<>" <cfif cdfRowItem.OPERATOR EQ '<>'> selected='selected'</cfif> >
												Is Not
											</option>
											<option value="Like" <cfif lcase(cdfRowItem.OPERATOR) EQ 'like'> selected='selected'</cfif> >
												Similar To
											</option>
			                                <option value="<" <cfif cdfRowItem.OPERATOR EQ '<'> selected='selected'</cfif> >
												Less Than
											</option>
			                                <option value=">" <cfif cdfRowItem.OPERATOR EQ '>'> selected='selected'</cfif> >
												Greater Than
											</option>
										</select>
									</cfif>
									<input type="text"  rel="CDF_filter_text" class="filter_val" value="#cdfRowItem.VALUE#">
									<span style="color:red;display:none;" rel="CDF_filter_validate"></span>
									<div class="action">
										<a class="contact_delete_filter left" onclick="RemoveFilterRow_CDF(this);return false;" href="javascript:void(0);" style="">
											<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
										</a>
										<a class="contact_add_filter left" onclick="AddFilterRow_CDF(this);return false;" href="javascript:void(0);">
											<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
										</a>
									</div>
								</div>
							</cfloop>
						</cfloop>
						<!---end loop cdf array --->
					</cfoutput>
				<cfelse>
					<cfoutput>
						<div class="contact_filter_row filter_row">
							<!---if there is no error and return value has more than one record, generate html dropdown --->
							<cfif retValCustomDataField.RXRESULTCODE EQ 1 AND  arraylen(retValCustomDataField.CDFArray) GT 0 >
								<select class="contact_filter_dropdownlist" rel="column">
                                                                            
									<cfloop array="#retValCustomDataField.CDFArray#" index="columnItem">
										<option value="#columnItem.CdfId#">
											#HTMLEditFormat(HTMLEditFormat(columnItem.VariableName))#
										</option>
									</cfloop>
								</select>
								<select class="contact_filter_dropdownlist" rel="operator">
									<option value="=" >
										Is
									</option>
									<option value="<>" >
										Is Not
									</option>
									<option value="Like" >
										Similar To
									</option>
	                                <option value="<" >
										Less Than
									</option>
	                                <option value=">" >
										Greater Than
									</option>
								</select>
							</cfif>
							<input type="text"  rel="CDF_filter_text" class="filter_val" value="">
							<span style="color:red;display:none;" rel="CDF_filter_validate"></span>
							<div class="action">
								<a class="contact_delete_filter left" onclick="RemoveFilterRow_CDF(this);return false;" href="javascript:void(0);" style="display:none;">
									<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
								</a>
								<a class="contact_add_filter left" onclick="AddFilterRow_CDF(this);return false;" href="javascript:void(0);">
									<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
								</a>
							</div>
						</div>
					</cfoutput>
				</cfif>
				
			</div>

			</cfif>
			<div style="background-image:url('#rootUrl#/#PublicPath#/images/backgroud_bottom_table.jpg');" class="filter_submit m_top_10">
				<div style="float:right;">
					<a class="filter-btn btn-primary dropdown-toggle" onclick="ClearFilter_CDF(); return false;" href="javascript:void(0);">Clear</a>
					<a class="filter-btn btn-primary dropdown-toggle" onclick="ApplyFilter_CDF(true); return false;" href="javascript:void(0);">Apply Current Filters</a>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	    <div class="message-block m_top_20">
	        <div class="left-input">
	        	<span class="em-lbl">Voice</span>
				<div class="hide-info showToolTip">&nbsp;</div>
				<div class="tooltiptext">Number of Voice contact matched your condition!</div>
			</div>
	        <div class="right-input">
	        	<cfif mode eq 'edit' and retValGetContactCountByCDF.RXRESULTCODE eq 1>
	        		<input type="text" value="<cfoutput>#retValGetContactCountByCDF.PHONENUMBER#</cfoutput>" id="txtContactVoice" readonly="readonly"/>
				<cfelse>
	        		<input type="text" value="" id="txtContactVoice" readonly="readonly"/>
				</cfif>
			</div>
			<div class="preview-link hide">
				<a href="javascript:void(0);" onclick="OpenListContactPopUp(1,'hddListVoiceContact');return false;" >View contacts</a>
				<input type="hidden" id="hddListVoiceContact" value=""/>
		    </div>
	    </div>
		<div class="clear"></div>
	    <div class="message-block m_top_20">
	        <div class="left-input">
	        	<span class="em-lbl">SMS</span>
				<div class="hide-info showToolTip">&nbsp;</div>
				<div class="tooltiptext">Number of SMS contact matched your condition!</div>
			</div>
	        <div class="right-input">
	        	<cfif mode eq 'edit' and retValGetContactCountByCDF.RXRESULTCODE eq 1>
	        		<input type="text" value="<cfoutput>#retValGetContactCountByCDF.SMSNUMBER#</cfoutput>" id="txtContactSMS" readonly="readonly"/>
				<cfelse>
	        		<input type="text" value="" id="txtContactSMS" readonly="readonly"/>
				</cfif>
			</div>
			<div class="preview-link hide">
				<a href="javascript:void(0);" onclick="OpenListContactPopUp(3,'hddListSMSContact');return false;" >View contacts</a>
				<input type="hidden" id="hddListSMSContact" value=""/>
		    </div>
	    </div>
		<div class="clear"></div>
	    <div class="message-block m_top_20">
	        <div class="left-input">
	        	<span class="em-lbl">Email</span>
				<div class="hide-info showToolTip">&nbsp;</div>
				<div class="tooltiptext">Number of Email contact matched your condition!</div>
			</div>
	        <div class="right-input">
	        	<cfif mode eq 'edit' and retValGetContactCountByCDF.RXRESULTCODE eq 1>
	        		<input type="text" value="<cfoutput>#retValGetContactCountByCDF.MAILNUMBER#</cfoutput>" id="txtContactEmail" readonly="readonly"/>
				<cfelse>
	        		<input type="text" value="" id="txtContactEmail" readonly="readonly"/>
				</cfif>
			</div>
			<div class="preview-link hide">
				<a href="javascript:void(0);" onclick="OpenListContactPopUp(2,'hddListEmailContact');return false;" >View contacts</a>
				<input type="hidden" id="hddListEmailContact" value=""/>
		    </div>
	    </div>
        
        <div class="clear"></div>
         
	    <div class="message-block m_top_20">
            <!---<a data-toggle='modal' href='dsp_addcontacttogroup?inpGroupId6=XXX' data-target='#AddContactModal'>Click here if you would like to add a contact to the current group.</a>--->
            <a href="javascript:void(0);" onclick="AddContact(); return false;">Click here if you would like to add a contact to the current group.</a>
	  	</div>
        
        
		<div class="clear m_top_10">&nbsp;</div>
        
       
        
	</div>
</div>
<!---<div class="clear m_top_10">&nbsp;</div>--->
<cfoutput>
	<script id="contact_filter_row_template" type="text/x-jquery-tmpl"> 
		<div class="contact_filter_row filter_row">
			<!---if there is no error and return value has more than one record, generate html dropdown --->
			<cfif retValCustomDataField.RXRESULTCODE EQ 1 AND  arraylen(retValCustomDataField.CDFArray) GT 0 >
				<select class="contact_filter_dropdownlist" rel="column">
					<cfloop array="#retValCustomDataField.CDFArray#" index="columnItem">
						<option value="#columnItem.CdfId#">
							#HTMLEditFormat(HTMLEditFormat(columnItem.VariableName))#
						</option>
					</cfloop>
				</select>
				<select class="contact_filter_dropdownlist" rel="operator">
					<option value="=" >
						Is
					</option>
					<option value="<>" >
						Is Not
					</option>
					<option value="Like" >
						Similar To
					</option>
					<option value="<" >
						Less Than
					</option>
                    <option value=">" >
						Greater Than
					</option>
				</select>
			</cfif>
			<input type="text"  rel="CDF_filter_text" class="filter_val" value="">
			<span style="color:red;display:none;" rel="CDF_filter_validate"></span>
			<div class="action">
				<a class="contact_delete_filter left" onclick="RemoveFilterRow_CDF(this);return false;" href="javascript:void(0);">
					<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
				</a>
				<a class="contact_add_filter left" onclick="AddFilterRow_CDF(this);return false;" href="javascript:void(0);">
					<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="#rootUrl#/#PublicPath#/images/dock/blank.gif">
				</a>
			</div>
		</div>
	</script>	
</cfoutput>

<script>
	var addressFormatting_CDF = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
	$("#divShowHideGroupContactFilter").click(function(){
		$("#divGroupContactFilterContent").toggle("slow",
		function() {
    		<!---// Animation complete.--->
			if($("#divGroupContactFilterContent").is(':visible'))
			{
				$("#FilterLinkText").html('Hide Filters');
			}
			else
			{
				$("#FilterLinkText").html('Click here if you would like to review or filter contacts.');
			}
  		});
		
		
		
	});
	$(document).ready(function(){
		$(".contact_filter_dropdownlist").selectmenu({
			style:'popup',
			width:170,
			format: addressFormatting_CDF
		});
	});
	
	var editContactPopup;
	//contactControl is hidden field that contains list of contact id
	function OpenListContactPopUp(contactType, contactControl){
		<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
		if($dialogGroupPicker != null){							
			editContactPopup.remove();
			editContactPopup = null;
		}
		if(editContactPopup  == null){
			var GroupId = $("#inpGroupPickerId").val();
			var ContactType = contactType;
			var ContactId = $("#"+contactControl).val() != ''?$("#"+contactControl).val() : "-1";
			<!--- Load content into a picker dialog--->			
			editContactPopup = $('<div id="EditContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
				width: 850,
				height: 'auto',
				modal: true,
				dialogClass:'GreyBG',
				autoOpen: false,
				title: 'Contact List',
				draggable: true,
				resizable: true,
				close: function( event, ui ) {
			      	//now before close dialog, we should remove all styles which have been loaded into this page from external css file 
					$("#EditContactPopUp").html('');
					if(editContactPopup != null)
					{							
						editContactPopup.remove();
						editContactPopup = null;
					}
			     }
			}).load('dsp_list_and_edit_contact',{//do NOT add here or URL rewrite engine will kill all post data
				"GroupId":GroupId,
				"ContactType":ContactType,
				"ContactId":ContactId
			});
		}
		editContactPopup.dialog('open');
	}
	
	//add new filter row 
	//callerObject is button triggering this function
	function AddFilterRow_CDF(callerObject){
		//use jquery template to add html right after container div of callerObject
		$(callerObject).parent().parent().after($("#contact_filter_row_template").tmpl());
		//custom dropdown
		$(".contact_filter_dropdownlist").selectmenu({
			style:'popup',
			width:170,
			format: addressFormatting_CDF
		});
		
		//display all delete button
		$(".contact_delete_filter").show();
	}		
	
	//this function will remove a filter row
	//callerObject is button triggering this function
	function RemoveFilterRow_CDF(callerObject){
		var numberOfFilterRow = $(".filter_row.contact_filter_row").size();
		//if there is only one filter row left, just hide its delete button
		if(numberOfFilterRow <=2){
			$(".contact_delete_filter").hide();
		}
		//remove html of container div of this button
		$(callerObject).parent().parent().remove();
	}
	
	//show filter box when triggered
	function ShowFilterCDF(){
		$("#hide_action_CDF").show();
		$("#CDFFilterRows").toggle("slow");
	}
	
	//hide filter box when triggered
	function HideFilterCDF(){
		$("#show_action_CDF").show();
		$("#CDFFilterRows").toggle("slow");
	}
	
	//clear filter
	function ClearFilter_CDF(){
		//reset text
		$("#txtContactVoice").val("");
		$("#txtContactSMS").val("");
		$("#txtContactEmail").val("");
		$("#txtContactFilter").val("");
		
		//remove all row
		$(".contact_filter_row.filter_row").remove();
		$("#CDFFilterRows").prepend($("#contact_filter_row_template").tmpl());
		$("#CDFFilterRows .contact_delete_filter").hide();//remove delete row button
		//use select menu for dropdowns
		$(".contact_filter_dropdownlist").selectmenu({
			style:'popup',
			width:170,
			format: addressFormatting_CDF
		});
		$(".preview-link").addClass("hide");
		//apply filter with cleared data
//		ApplyFilter_CDF(false);

	}
	
	function Validate_CDF(){
		var result = true;
		//validate filter row
		var filterRows = $(".filter_row.contact_filter_row");
		//loop over filter rows to validate
		for (var i = 0; i < filterRows.size(); i++) {
			var filterRowItem = filterRows[i];
			var errorSpan = $(filterRowItem).find("span[rel=CDF_filter_validate]");
			var inputValue = $(filterRowItem).find("input[rel=CDF_filter_text]").val();
			if(inputValue == ""){
				$(errorSpan).html("<cfoutput>#FIELDREQUIRED#</cfoutput>");
				$(errorSpan).show();
				result = false;
			}else{
				$(errorSpan).hide();
			}
		}
		return result;
	}


	function BuildCDFFilterArray(){
//		build cdf data as format 
//		[
//			{
//				"CDFID":"8",
//				"ROWS":[{"OPERATOR":"Like","VALUE":"123"},{"OPERATOR":"<>","VALUE":"abc"}]
//			},
//			{
//				"CDFID":"7",
//				"ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
//			}
//		]

		var filterRows = $(".filter_row.contact_filter_row");
		var distinctFilterIdArr = new Array();//this is the array containing id of cdf, each id appears only once
		for (var i = 0; i < filterRows.size(); i++) {
			var filterRowItem = filterRows[i];
			var cdfId = $(filterRowItem).find("select[rel=column]").find(":selected").val();
			var inputValue =  $(filterRowItem).find("input[rel=CDF_filter_text]").val();
			if(inputValue != '' && distinctFilterIdArr.indexOf(cdfId) == -1){
				//if this id did not exist in distinctFilterIdArr and its textbox value is not empty, append to this array
				distinctFilterIdArr.push(cdfId);
			}
		}
		
		var cdfResultData = new Array();
		for (var i = 0; i < distinctFilterIdArr.length; i++) {
			//var filterRowItem = filterRows[i];
			var resultArray = $.grep(filterRows, function(e){
				var cdfId =  $(e).find("select[rel=column]").find(":selected").val();
				return cdfId == distinctFilterIdArr[i]; 
			});
			
			var rowArray = new Array();
			//now loop through resultArray to push data in to cdfResultData
			for(var j = 0 ; j < resultArray.length;j++){
				var filterRowItem = resultArray[j];
				temp1 = filterRowItem;
				var cdfId = $(filterRowItem).find("select[rel=column]").find(":selected").val();
				var operator = $(filterRowItem).find("select[rel=operator]").find(":selected").val();
				var inputValue =  $(filterRowItem).find("input[rel=CDF_filter_text]").val();
				var rowItem = {
					OPERATOR: operator,
					VALUE:encodeURIComponent(inputValue)
				};
				rowArray.push(rowItem);
			}
			var cdfResultItem = {
				CDFID: distinctFilterIdArr[i],
				ROWS:rowArray
			};
			cdfResultData.push(cdfResultItem);
		}
//		finish build cdf data
		return cdfResultData;
	}
	
	//apply filter 	
	function ApplyFilter_CDF(isApplyCDF){
		//group should be selected before apply filter
		var GroupPicker = $("#inpGroupPickerId").val();
		if(typeof(GroupPicker) == "undefined" || GroupPicker == ""){
			jAlert("Please select at least one group.", "Failure!",function(){
				$("#inpGroupPickerDesc").click();
			});
			return false;
		}
		

		var cdfData;
		//validate CDF filter once isApplyCDF is true
		if(isApplyCDF){
			//if false, return
			//if(!Validate_CDF()) return false;
			cdfData = BuildCDFFilterArray();
		}

		var dataValue = {
			CDFData: JSON.stringify(cdfData),
			CustomString: $("#txtContactFilter").val(),
			INPGROUPID:GroupPicker
		};
		
		$.ajax({
		    type: "POST",
		    url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetContactCountByCDFAndCustomString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		    data: dataValue,
		    async: false,
			dataType:"json",
		    success: function(dStruct ) {
				// Check if variable is part of JSON result string	
				if(dStruct.RXRESULTCODE > 0)
				{
					//set preview data
					$("#txtContactVoice").val(dStruct.PHONENUMBER);
					$("#txtContactSMS").val(dStruct.SMSNUMBER);
					$("#txtContactEmail").val(dStruct.MAILNUMBER);
					$("#hddListSMSContact").val(dStruct.SMSLIST);
					$("#hddListEmailContact").val(dStruct.MAILLIST);
					$("#hddListVoiceContact").val(dStruct.PHONELIST);
					$(".preview-link").removeClass("hide");
					return false;
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE, "Failure!", function(result) { } );							
				}
		    },
			error: function(jq,status,message) {
		        alert('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
		    }
		});
	}
</script>

<!---init data for edit mode --->
<cfif mode eq 'edit'>
	<script type="text/javascript">
		$(document).ready(function(){
			if(<cfoutput>#numberOfCdfRow#</cfoutput> < 2){
				$("#CDFFilterRows .contact_delete_filter").hide();//remove delete row button
			}
		});
	</script>
</cfif>