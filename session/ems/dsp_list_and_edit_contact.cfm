﻿<cfparam name="ContactType" default="0">
<cfparam name="ContactId" default="0">
<cfparam name="GroupId" default="0">
<style>
	#tblListContact_info{
		margin-left:0px;
		width:100%;
		height:auto;
	}
	#EditContactPopUp {
		margin: 15px;
	}
	#tblListContact_info {
		width: 98%;
		padding-left: 2%;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.no-border-right{
		border-right:none !important;
	}
	#submitContact .filter-btn {
	    background-color: #006DCC;
	    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
	    background-repeat: repeat-x;
	    border: 1px solid #0888D1;
	    border-radius: 4px;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	    color: #FFFFFF !important;
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    height: 30px;
	    line-height: 20px;
	    margin-bottom: 0;
	    margin-left: 10px;
	    padding: 4px 12px;
	    text-align: center;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	    vertical-align: middle;
	}
</style>
<div>
	<table id="tblListContact" class="table">
	</table>
</div>
<script id="cdf_row_detail" type="text/x-jquery-tmpl"> 
	 <div class=""> 
        <div style="width:100%;margin:5px 0px 5px 5px;"> 
        	<span style="display:inline-block;min-width:30%;max-width:30%">${NAME}</span>
        	<input type="text" id="${CDFID}" rel="${CDFID}" class="cdf-detail-value" value="${VALUE}"/>
		</div> 
    </div>
</script>
<script>
	var _tblListContact;
	//init datatable for active agent
	function InitListContact(){
		_tblListContact = $('#tblListContact').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
		    "aoColumns": [
				{"sName": 'contact Id', "bVisible":false},
				{"sName": 'Name', "sTitle": 'Contact Name', "sWidth": '97%',"bSortable": true,"sClass":"no-border-right"},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '3%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetListContactById&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "ContactType", "value": <cfoutput>'#ContactType#'</cfoutput>},
		            { "name": "ContactId", "value": <cfoutput>'#ContactId#'</cfoutput>},
		            { "name": "INPGROUPID", "value": <cfoutput>'#GroupId#'</cfoutput>}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
//				$('#tblListGroupContact').attr('style', '');
//				$('#tblListGroupContact').css('min-width', '100%');
			}
	    });
	}
	InitListContact();
	
	//this function will init html for contact detail form
	function InitRowDetail(caller,dStruct,contactId){
		var htmlResult = '<div style="border:#333 1px solid; margin:5px;">';
		for (var i = 0; i < dStruct.CDFARRAY.length; i++) {
			htmlResult += $('#cdf_row_detail').tmpl(dStruct.CDFARRAY[i]).html();
		}
		htmlResult +="</div>";
		return htmlResult ;
	}
	
	function InitContactDetail(dStruct){
		var result = '<div style="border:#333 1px solid; margin:5px;">';
		result += 		'<table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist">'+   
                        '<tr>'+
							'<td class="cx_rxmultilist_rtxt">Company Name</td>'+							
                            '<td><input type="text" id="Company_vch" name="Company_vch" class="validate[maxSize[500]] ui-corner-all" size="30" value="'+dStruct.COMPANY_VCH+'"/></td>'+
							'<td class="cx_rxmultilist_rtxt">Account Identifier</td>'+							
                            '<td><input type="text" id="UserDefinedKey_vch" name="UserDefinedKey_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" value="'+dStruct.USERDEFINEDKEY_VCH+'"/></td>'+							
						'</tr>'+                        
                        '<tr>'+
							'<td class="cx_rxmultilist_rtxt">First Name</td>'+
							'<td><input type="text" id="FirstName_vch" name="FirstName_vch" class="validate[maxSize[90]] ui-corner-all" size="30" value="'+dStruct.FIRSTNAME_VCH+'"/></td>'+
							'<td class="cx_rxmultilist_rtxt">Last Name</td>'+
							'<td><input type="text" id="LastName_vch" name="LastName_vch" class="validate[maxSize[90]] ui-corner-all" size="30" value="'+dStruct.LASTNAME_VCH+'"/></td>'+
						'</tr>'+
                        '<tr>'+
							'<td class="cx_rxmultilist_rtxt">Address Line One</td>'+
							'<td><input type="text" id="Address_vch" name="Address_vch" class="validate[maxSize[250]] ui-corner-all" size="30" value="'+dStruct.ADDRESS_VCH+'"/></td>'+
							'<td class="cx_rxmultilist_rtxt">Address Line Two</td>'+
							'<td><input type="text" id="Address1_vch" name="Address1_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="'+dStruct.ADDRESS1_VCH+'"/></td>'+
						'</tr>'+
                        '<tr>'+
							'<td class="cx_rxmultilist_rtxt">City</td>'+
							'<td><input type="text" id="City_vch" name="City_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="'+dStruct.CITY_VCH+'"/></td>'+
							'<td class="cx_rxmultilist_rtxt">State</td>'+
							'<td><input type="text" id="State_vch" name="State_vch" class="validate[maxSize[50]] ui-corner-all" size="30" value="'+dStruct.STATE_VCH+'"/></td>'+
						'</tr>'+
                        '<tr>'+
							'<td class="cx_rxmultilist_rtxt">Postal Code</td>'+
							'<td><input type="text" id="ZipCode_vch" name="ZipCode_vch" class="validate[maxSize[20]] ui-corner-all" size="30" value="'+dStruct.ZIPCODE_VCH+'"/></td>'+
							'<td class="cx_rxmultilist_rtxt">Country</td>'+
							'<td><input type="text" id="Country_vch" name="Country_vch" class="validate[maxSize[100]] ui-corner-all" size="30" value="'+dStruct.COUNTRY_VCH+'"/></td>'+
						'</tr>'+  
                   '</table>'+
                   '</div>';
		return result;   
	}
	
	function RemoveDetail(caller){
		var tr = $(caller).parents("tr");
		var contactId = $(tr).attr("rel");
		$("#img"+contactId).show();
		tr.html("").remove();
	}
	
	function UpdateContact(contactId,caller){
		var dataValue = {};
		dataValue.Company_vch = $("#Company_vch").val();
		dataValue.FirstName_vch =  $("#FirstName_vch").val();
		dataValue.LastName_vch =  $("#LastName_vch").val();
		dataValue.Address_vch =  $("#Address_vch").val();
		dataValue.Address1_vch =  $("#Address1_vch").val();
		dataValue.City_vch =  $("#City_vch").val();
		dataValue.State_vch =  $("#State_vch").val();
		dataValue.ZipCode_vch =  $("#ZipCode_vch").val();
		dataValue.Country_vch =  $("#Country_vch").val();
		dataValue.UserDefinedKey_vch =  $("#UserDefinedKey_vch").val();
		dataValue.ContactId_bi =  contactId;
		
		var listCdf = $("#tblListContact .cdf-detail-value");
		for(var i = 0 ; i< listCdf.size();i++){
			var cdfId = $(listCdf[i]).attr('rel');
			var cdfValue = $(listCdf[i]).val();
			dataValue[cdfId] = cdfValue;
		}
		
		var data ={
			contactData : JSON.stringify(dataValue)
		}
		$.ajax({
		    type: "POST",
		    url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=UpdateContactData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		    data: data,
		    async: false,
			dataType:"json",
		    success: function(dStruct ) {
				// Check if variable is part of JSON result string	
				if(dStruct.RXRESULTCODE > 0)
				{
					jAlert("Edit contact successfully!", "Success!", function(result) { 
						RemoveDetail(caller);
					} );
					return false;
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE, "Failure!", function(result) { } );							
				}
		    },
			error: function(jq,status,message) {
		        alert('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
		    }
		});
		
	}
	
    function ShowContactDetail(caller,contactId){
		//now call ajax to get data for contact 
		var dataValue = {
			"ContactId":contactId
		};
		$(caller).hide();
		$.ajax({
		    type: "POST",
		    url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=GetContactDataById&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		    data: dataValue,
		    async: false,
			dataType:"json",
		    success: function(dStruct ) {
				// Check if variable is part of JSON result string	
				if(dStruct.RXRESULTCODE > 0)
				{
					var rowContactDetail = InitContactDetail(dStruct);
					var rowCDFDetail = InitRowDetail(caller,dStruct,contactId);
					var tr = $(caller).parents('tr');
					$(tr).after('<tr rel="'+contactId+'"><td colspan="2">'+rowContactDetail+rowCDFDetail+'<div id="submitContact">'+'<input type="button" class="filter-btn btn-primary dropdown-toggle" onclick = "UpdateContact('+contactId+',this);return false;" value="Update"/>'+'<input type="button" onclick="RemoveDetail(this);return false;" class="filter-btn btn-primary dropdown-toggle" value="Exit"/>'+'</div>'+'</td></tr>');
					return false;
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE, "Failure!", function(result) {
						$(caller).show();	
					 } );							
				}
		    },
			error: function(jq,status,message) {
		        alert('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
		    }
		});
	}
</script>