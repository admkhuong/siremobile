﻿
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="getAccountInfo" returnvariable="getAccount" />

<script src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/swfobject.js" type="text/javascript" charset="utf-8"></script>
<cfset ISEDIT = 0>
<cfset ISEMAILDATA = 0>
<cfset ISSMSDATA = 0>
<cfset ISVOICEDATA = 0>
<cfif mode EQ "edit">
	<cfset ISEDIT = 1>
	<cfif StructKeyExists(EmergencyById.EMAILDATA, 'ExistData')>
		<cfset ISEMAILDATA = 1>
	</cfif>
	<cfif StructKeyExists(EmergencyById.SMSDATA, 'ExistData')>
		<cfset ISSMSDATA = 1>
	</cfif>
	<cfif StructKeyExists(EmergencyById.VOICEDATA, 'ExistData')>
		<cfset ISVOICEDATA = 1>
	</cfif>	
</cfif>
<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
<style type="text/css">
	.record-new{
		width:60%;		
	}

	.ui-dialog 
	{
    	padding-bottom: 5px;
		background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/images/main-bg.jpg") repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	
	#IVRReviewBtn.active
	{
		/* background: url(images/redo20n_inactive.png); */
		background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
	}
	
	#IVRReviewBtn.inactive
	{
		/* background: url(images/redo20n_inactive.png); */
		background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
	}
		
	#IVRNewBtn.active
	{
		/* background: url(images/redo20n_inactive.png); */
		background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
	}
	
	#IVRNewBtn.inactive
	{
		/* background: url(images/redo20n_inactive.png); */
		background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
	}
				
				
				
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
		-moz-border-bottom-colors: none;
		-moz-border-left-colors: none;
		-moz-border-right-colors: none;
		-moz-border-top-colors: none;
		background-color: #ffffff;
		border-color: #dddddd #dddddd transparent;
		border-image: none;
		border-style: solid;
		border-width: 1px;
		color: #555555;
		cursor: default;
	}
	.nav-tabs > li > a {
		border: 1px solid transparent;
		border-radius: 4px 4px 0 0;
		line-height: 1.42857;
		margin-right: 2px;
	}
	.nav > li > a {
		display: block;
		padding: 10px 15px;
		position: relative;
	}

	
	.tab-content > .tab-pane {
		padding:20px 10px 10px 10px;
		background-color: #ffffff;
		border-image: none;
		border-style: none;
		color: #555555;
		cursor: default;
	}

	<!--- Cludge to prevent tab from going display:nonbe which kills the flash. Yes Flash sux but there is still no good voice recording tools for HTML yet--->
	.tab-content
	{
		position:relative;
		min-height:420px;
		border-color: #dddddd ;
		border-image: none;
		border-style: solid;
		border-width: 1px;
		
	}
	
	/* bootstrap hack: fix content width inside hidden tabs */
	.tab-content > .tab-pane,
	.pill-content > .pill-pane {
		display: block;     /* undo display:none          */
		height: 0;          /* height:0 is also invisible */ 
		overflow-y: hidden; /* no-overflow                */
		visibility:hidden;
		position:absolute;
		top:0;
		left:0;
		width:100%;
		
	}
	.tab-content > .active,
	.pill-content > .active {
	   <!--- height: auto;--->       /* let the content decide it  */
	} /* bootstrap hack end */
	


</style>


<!--- User defined eMail Templates--->
<cfset TemplateList = "" />

<cfinvoke method="ReadTemplateList" component="#Session.SessionCFCPath#.emailtemplates" returnvariable="RetVarReadTemplateList"></cfinvoke> 

<!---<cfdump var="#RetVarReadTemplateList#">--->

<cfloop array="#RetVarReadTemplateList.TEMPLATELIST#" index="CurrIndex">		
		
        <!---<cfdump var="#CurrIndex#">--->
    
    	<!--- comma seperated if this is not the first one --->
        <cfif TemplateList NEQ "">
	        <cfset TemplateList = TemplateList & "," />
        </cfif>
        
        <cfset extenededDesc = "">
        
        <cfif Session.UserId EQ CurrIndex[3]>
        	<cfset extenededDesc = "User Defined">
        </cfif>
        
        <cfif 0 EQ CurrIndex[3]>
        	<cfset extenededDesc = "System Defined">
        </cfif>
        
        <cfif Session.UserId NEQ CurrIndex[3] AND 0 NEQ CurrIndex[3]>
        	<cfset extenededDesc = "Company Defined">
        </cfif>
	        
        <cfset TemplateList = TemplateList & "{inpUID: '#CurrIndex[3]#' ,TemplateId: '#CurrIndex[1]#', title: '#CurrIndex[2]#', description: '#CurrIndex[2]# - #extenededDesc#',url: '#rootUrl#/#SessionPath#/email/act_loademailtemplate?inpID=#CurrIndex[1]#'}" /> 
		
</cfloop>
<!--- <cfdump var="#TemplateList#"> ---> 


<!--- Script library management methods --->
<cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />



<ul class="nav nav-tabs">
  	<cfif VoicePermissionStr.havePermission>
        <li class="active" id="tab_a_li"><a href="#tab_a" data-toggle="tab">VOICE</a></li>
    </cfif>
    <cfif SMSPermissionStr.havePermission>
        <li id="tab_b_li"><a href="#tab_b" data-toggle="tab">SMS</a></li>
    </cfif>
    <cfif EmailPermissionStr.havePermission>
        <li id="tab_c_li"><a href="#tab_c" data-toggle="tab">EMAIL</a></li>
    </cfif>
            
</ul>
<div class="tab-content">

	<cfif VoicePermissionStr.havePermission>
        <div class="tab-pane active" id="tab_a"> <!--- tab_a --->
            
            <div class="message-block">
		        <div class="left-input">
		        	<span class="em-lbl">Caller ID</span>
					<div class="hide-info showToolTip">&nbsp;</div>
					<div class="tooltiptext">
						All phone calls must display a valid caller ID.
					</div>		
					<div class="info-block" style="display:none">content info block</div>
				</div>
		        <div class="right-input">	
                
                
                	        		
				<cfif ISEDIT EQ 1>
					<cfif ISVOICEDATA EQ 1>
						<input type="hidden" id="fileMp3Id" value="<cfoutput>#EmergencyById.VOICEDATA.FILEID#</cfoutput>">
					</cfif>
        			<input id="caller-id-txt" type="text" <cfoutput>#ISVOICEDATA EQ 1 ? "value='#EmergencyById.VOICEDATA.CALLERID#'" : "#getaccount.INFO.DefaultCID_vch#"#</cfoutput> >
				<cfelse>
					<input id="caller-id-txt" type="text" value="<cfoutput>#getaccount.INFO.DefaultCID_vch#</cfoutput>">
				</cfif>
				</div>				
		    </div>
			
            <br />
			
            <div class="clear"></div>
            
            <div id="record-new" class="record-new">
				<div id='flashRecorder' style='outline: 0;'>
				</div>
			</div>	
            
            <div id="IVRBatchInfo" style="display:none; height:240px; padding-top:30px;">
                        
                <div class="message-block">
                    <div class="left-input">
                        <span class="em-lbl">Content Desc</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Choose an IVR content container. This will be copied into the new campaign and used for the voice portion of the e-messaging campaign.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                    <div class="right-input">	
                    
                    	<!---<div id="IVRBatchDesc" style="display:none;"></div>
                    	<input id="inpIVRBatchDesc" type="text" value="<cfoutput></cfoutput>" >--->
                                                
                        <div id="inpIVRBatchDescPicker" class="wrapper-picker-container">        	    
                            <div class="wrapper-dropdown-picker BatchPicker"> 
                                <span id="inpIVRBatchDesc">Click here to select an IVR content container</span> 
                                <a id="sbToggle_inpIVRBatchDesc" class="sbToggle" href="javascript:void(0);"></a>   
                            </div>                                            
                        </div>
            
                    
                    </div>				
                </div>     
                
                <div class="clear"></div>
                
                <!--- IVR Edit Option--->
                <div id="select-IVRReview" class="select-record popupMenu" style="margin-top:10px;">
                    <div id="IVRReviewBtn" class="active" title="IVR Review" style="float:left; margin-right:10px;"></div>
                    <div style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Click here if you would like to review/edit this IVR in a new browser window.</div>
                </div>  
                
                <div class="clear"></div>
                
               	<!--- IVR Edit Option--->
                <div id="select-IVRNew" class="select-record popupMenu" style="margin-top:10px;">
                    <div id="IVRNewBtn" class="active" title="IVR New" style="float:left; margin-right:10px;"></div>
                    <div style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Click here if you would like to create a new IVR content container in a new browser window.</div>
                </div>   
            	           	
            	<!---<div id="IVRBatchDesc" style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Select a Batch</div>--->
       			<input type="hidden" id="IVRBatchId" value="<cfoutput><!---#EmergencyById.VOICEDATA.FILEID#---></cfoutput>">
            
            	<div class="clear"></div>
            
            </div>
			            
            <div class="clear"></div>
           
            
            <!--- Script Record Option--->
            <div id="select-BackToRecording" class="select-record popupMenu" style="display:none; margin-top:10px;">
                <div id="BackToRecordingBtn" class="active" title="IVR Selection" style="float:left; margin-right:10px;"></div>
                <div style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Click here if you would rather record a new script now.</div>
            </div>
                
            
           <!--- <div id="select-record" class="select-record popupMenu">
                <div id="ScriptPickerBtn" class="active" title="Script Selection" style="float:left; margin-right:10px;"></div>
                <div style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Click here if you would like to choose a previously recorded audio file from your library.</div>
            </div>--->
            
            <a data-toggle='modal' href='dsp_script_picker' data-target='#SelectScriptModal'>Click here to choose a file from your library.</a>
                        
            <div class="clear"></div>
            
            <a data-toggle='modal' href='../campaign/act_campaignpicker?inpContainerType=1&inpSkipDT=1' data-target='#SelectBatchModal'>Click here to use a previously defined IVR.</a>
            
			<!---<!--- IVR Option--->
            <div id="select-IVR" class="select-record popupMenu">
                <div id="IVRPickerBtn" class="active" title="IVR Selection" style="float:left; margin-right:10px;"></div>
                <div style="float:left; line-height: 24px;" class="TextLinkHoverStyle">Click here if you would like to use a previously defined IVR.</div>
            </div>--->
            
            <div class="clear"></div>
            
			<div style="display:block;margin-right:20px; width: 90%; text-align:right;">	
				<div class="showToolTip" style="margin-top:5px;float:right;">
					<!---<span style="color:#666666;font-size:10px;padding-top:7px;width:200px;">--->
						<a class="tool-tip1" style="float:right;">&nbsp;</a>
                        <a style="float:right;color:#666666;font-size:10px;padding-top:2px;">Is your microphone available...&nbsp;&nbsp;&nbsp;</a>
					<!---</span>--->
				</div>
	            <div class="tooltiptext">
	                Accept allow microphone if your browser asks.<BR/>OR...<BR/>Go to flash Settings, select Privacy tab. Choose Allow microphone, then close and refresh. <BR />On PC Please try clicking right mouse on recorder.<BR/>On Mac try control-option-click on recorder.
	            </div>
            </div>
            
            <BR />
            <div class="clear"></div>
        		                       
<!---        	<div id="note-record" style="color:#666666; font-size:10px; max-width:650px; bottom:15px;"><b>Note: </b>If you have trouble recording, go to flash Settings, select Privacy tab. Choose Allow microphone, then close and refresh. <BR />On PC Please try clicking right mouse on recorder. On Mac try control-option-click on recorder.</div>
--->        
			<!---input hinden--->
			<input type="hidden" value="" name="fileSelectTxt" id="fileSelectTxt"/>
			<input type="hidden" value="0" name="timeRecordTxt" id="timeRecordTxt"/>
	
    
        </div>	<!--- tab_a --->
      
   </cfif>
   
   <cfif SMSPermissionStr.havePermission>     
        <div class="tab-pane" id="tab_b"> <!--- tab_b --->
           
            <cfset disableSMSText = "">
			
			<!---if there is no available short code--->
			<cfif SCMBbyUser.RecordCount EQ 0>
			
            	<cfset disableSMSText = "disabled='true'">
				<span style="color:red;">There is no available short code. Please request one or contact an administrator to get new one.</span>
                <a href="#tab2" id="anchor-tab2"></a>
				<div class="clear" style="margin-bottom:10px"></div>
			
			<!---if only one short code available no need to show drop downlist--->	
			<cfelseif SCMBbyUser.RecordCount EQ 1>
				
				<div class="message-block">
                    <div class="left-input">
                        <span class="em-lbl">Short Code</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Your account has been setup to use only this short code.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                    <div class="right-input">
                        <cfoutput>#SCMBbyUser.ShortCode_vch#</cfoutput>
                    </div>
                </div>
				
				<cfoutput><input type="hidden" id="inpAvailableShortCode" value="#SCMBbyUser.ShortCode_vch#" /></cfoutput>
			
			<!---if there are many short code available show drop downlist--->	
			<cfelse>		
                <div class="message-block">
                    
                    <div class="left-input">
                        <span class="em-lbl">Short Code</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Your must choose which short code to use from this accounts multiple short codes.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                    
                    <div class="right-input">
                        <select id="inpAvailableShortCode">
                            <option value="-1">Select an available short code</option>
                            <cfif ISEDIT EQ 1>
                                <cfif ISSMSDATA EQ 1>
                                    <cfoutput>
                                        <cfloop query="SCMBbyUser">
                                            <cfif ShortCode_vch EQ EmergencyById.SMSDATA.SHORTCODE>
                                                <option selected="selected" value="#ShortCode_vch#">#ShortCode_vch#</option>
                                            <cfelse>
                                                <option value="#ShortCode_vch#">#ShortCode_vch#</option>	
                                            </cfif>
                                        </cfloop>
                                    </cfoutput>
                                <cfelse>
                                    <cfoutput>
                                        <cfloop query="SCMBbyUser">
                                            <option value="#ShortCode_vch#">#ShortCode_vch#</option>
                                        </cfloop>
                                    </cfoutput>	
                                </cfif>	
                            <cfelse>
                                <cfoutput>
                                    <cfloop query="SCMBbyUser">
                                        <option value="#ShortCode_vch#">#ShortCode_vch#</option>
                                    </cfloop>
                                </cfoutput>
                            </cfif>
                        </select>
                    </div>
                </div>
				
				<div class="clear" style="margin-bottom:10px"></div>
			</cfif>
            
            <div>
                <div>
                    <cfif ISEDIT EQ 1>
                        <textarea maxlength="160" <cfoutput>#disableSMSText#</cfoutput> id="txtSMS"><cfoutput>#ISSMSDATA EQ 1 ? "#EmergencyById.SMSDATA.MainMessage#" : ""#</cfoutput></textarea>
                        <input type="hidden" id="CallRemainChars" value="1">
                    <cfelse>
                        <textarea id="txtSMS" style="" maxlength="160" <cfoutput>#disableSMSText#</cfoutput>></textarea>
                        <input type="hidden" id="CallRemainChars" value="0">
                    </cfif>
                </div>
                <div class="remainChar">
                    <label id="lblRemainChar">0/160</label>
                </div>
            </div>
        
        </div> <!--- tab_b --->
                
   </cfif>    
   
   <cfif EmailPermissionStr.havePermission> 
        <div class="tab-pane" id="tab_c"> <!--- tab_c --->
			
			<!---from --->
            <div class="message-block m_left_0">
                <div class="left-input">
                    <span class="em-lbl">From</span>
                    <div class="hide-info showToolTip">&nbsp;</div>
                    <div class="tooltiptext">
                        Required - Please enter a valid email address from whom this message will be from
                    </div>		
                    <div class="info-block" style="display:none">content info block</div>
                </div>
                <div class="right-input">
                    <cfif ISEDIT EQ 1>
                        <input id="txtEmail" type="text" <cfoutput>#ISEMAILDATA EQ 1 ? "value='#EmergencyById.EMAILDATA.FROM#'" : ""#</cfoutput> >
                    <cfelse>
                        <input id="txtEmail" type="text" value="">
                    </cfif>
                </div>
            </div>
            
            <div class="clear"></div>
            
            <!--- Just use copy of To address for now --->
            <input id="txtReplyTo" type="hidden" value="">
            
            <!---<!---reply to --->
            <div class="message-block m_top_10 m_left_0">
                <div class="left-input">
                    <span class="em-lbl">Reply to</span>
                    <div class="hide-info showToolTip">&nbsp;</div>
                    <div class="tooltiptext">
                        Required - Please enter a valid email address from whom this message will accept replies from
                    </div>	
                    <div class="info-block" style="display:none">content info block</div>
                </div>
                <div class="right-input">
                    <cfif ISEDIT EQ 1>
                            <input id="txtReplyTo" type="text" <cfoutput>#ISEMAILDATA EQ 1 ? "value='#EmergencyById.EMAILDATA.REPLYTO#'" : ""#</cfoutput> >
                    <cfelse>
                        <input id="txtReplyTo" type="text" value="">
                    </cfif>
                </div>
            </div>
            <div class="clear"></div>--->
            
            <!---subject --->
            <div class="message-block m_top_10 m_left_0">
            
                <div class="left-input">
                    <span class="em-lbl">Subject</span>
                    <div class="hide-info showToolTip">&nbsp;</div>	
                    <div class="tooltiptext">
                        Required - Please enter a valid subject line for this email message
                    </div>
                    <div class="info-block" style="display:none">content info block</div>
                </div>
            
                <div class="right-input">
                    <cfif ISEDIT EQ 1>
                            <input id="txtSubject" type="text" <cfoutput>#ISEMAILDATA EQ 1 ? "value='#EmergencyById.EMAILDATA.SUBJECT#'" : ""#</cfoutput> >
                    <cfelse>
                        <input id="txtSubject" type="text" value="">
                    </cfif>
                </div>
            </div>
            
            <div class="clear"></div>
            
            <div class="editor">
                <cfif ISEDIT EQ 1>
                    <textarea name="txtContent" id="txtContent"><cfoutput>#ISEMAILDATA EQ 1 ? "#EmergencyById.EMAILDATA.EmailContent#" : ""#</cfoutput></textarea>
                <cfelse>
                    <textarea name="txtContent" id="txtContent"></textarea>
                </cfif>
            </div>
        
        </div> <!--- tab_c --->
        
   </cfif>
        
</div><!-- tab content -->




<!---
	<div id="tabs" class="m_left_20" style="opacity:0;">
		<ul>
			<cfif VoicePermissionStr.havePermission>
				<li id="tab1">VOICE</li>
			</cfif>
			<cfif SMSPermissionStr.havePermission>
				<li id="tab2">SMS</li>
			</cfif>
			<cfif EmailPermissionStr.havePermission>
				<li id="tab3">EMAIL</li>
			</cfif>
		</ul>
        
		<cfif VoicePermissionStr.havePermission>
		<div id="tabs-1" style="visibility:hidden;">		
		</div>
		</cfif>
		
		
		<cfif SMSPermissionStr.havePermission>
		<div id="tabs-2" style="visibility:hidden;">
			
	</div>
	</cfif>
	<cfif EmailPermissionStr.havePermission>
	<div id="tabs-3" style="visibility:hidden;">
		
	</div>
	</cfif>
</div>
--->

<div id="popupTree" style="display:none">
	<div id="recordTreeData">
	</div>
</div>


<div id="IVRBuilder" style="display:none">
	
</div>

<script type="text/javascript">

	<!--- Global var for the Campaign picker dialog--->
	var $dialogCampaignPicker = null;
		
	<!--- allow multiple batch pickers on one page--->
	var $TargetBatchId = null;
	var $TargetBatchDesc = null;
	
	
	$(function(){	
		
		<!---$(".select-record").hide();--->
			
		$('textarea[maxlength]').keyup(function(){  
	        //get the limit from maxlength attribute  
	        var limit = parseInt($(this).attr('maxlength'));  
	        //get the current text inside the textarea  
	        var text = $(this).val();  
	        //count the number of characters in the text  
	        var chars = text.length;  
	  <!--- no more limits - just warn increments charged per 153 character anthing over 160 characters to begin with  --->
	  <!---
	        //check if there are more characters then allowed  
	        if(chars > limit){  
	            //and if there are use substr to get the text before the limit  
	            var new_text = text.substr(0, limit);  
	  
	            //and change the current text with the new text  
	            $(this).val(new_text);  
	        }
	  --->
	    }); 
		$('#txtSMS').bind('cut copy paste', function (e) {
			ViewRemainChars();
		});
	    $('#txtSMS').keyup(function(){  
	        ViewRemainChars();
	    });  
	});
	
	$(document).ready(function(){
		
		$TargetBatchDesc = $("#inpIVRBatchDesc"); 
		$TargetBatchId =  $("#IVRBatchId"); 
			
		 //FORMAT PHONE NUMBER OF USA
		 var oStringMask = new Mask("(###) ###-####");
		 if(document.getElementById("caller-id-txt")){
			 oStringMask.attach(document.getElementById("caller-id-txt"));
		 }
		 var isedit = "<cfoutput>#ISEDIT#</cfoutput>";
		 var isvoicedata = "<cfoutput>#ISVOICEDATA#</cfoutput>";
		 		 
		 <!--- Blur format default CID --->
		 $("#caller-id-txt").blur();
				 
		 if(isedit == 1 && isvoicedata == 1)
		 {
		 	$("#caller-id-txt").focus();
			$("#caller-id-txt").blur();
		 	var fileId = $("#fileMp3Id").val();
			$("#fileSelectTxt").attr("value", fileId);
		 	playerflash(fileId,'flashRecorder');
		 }
		 else{
		 	playerflash('','flashRecorder');
		 }
		//Call RemainChars when edit
		if($("#CallRemainChars").val() == 1)
		{
			ViewRemainChars();
		}
		$('select#inpOptionRecord').selectmenu({
			style:'popup',
			width: 385,
			format: addressFormatting
		});
		
	<!---
		$('select#inpOptionRecord').change(function(){
			if($('select#inpOptionRecord').val() == 1){
				// select create new record
				$('.record-new').show();
				$('.select-record').hide();				
			}else{
				// select find a exist record
				$('.record-new').hide();
				$('.select-record').show(); 
			}
		});	
		--->
		
		// change style tab css active
		$("#tabs>ul>li").css("background-color","#F0F0F0");
		$("#tabs>ul>li:first-child").css("background-color","#FFFFFF");
		$("#tabs>ul>li:first-child").css("color","#0888d1");
		$("#tabs>ul>li:first-child").css("padding-bottom","1px");
		
		// not display:none. it will reload flash content
		$("#tab_a").css("visibility","inherit");
		$("#tab_b").css("visibility","hidden");
		$("#tab_c").css("visibility","hidden");
		
		// add click event to tab
		$("#tab_a_li").click(function (){
			
			// not display:none. it will reload flash content
			$("#tab_a").css("visibility","inherit");
			$("#tab_b").css("visibility","hidden");
			$("#tab_c").css("visibility","hidden");
		
			// hide tinymce object
			$(".editor").hide();
			
			// change style tab css active
			$(".nav-tabs>ul>li").css("background-color","#F0F0F0");
			$(".nav-tabs>ul>li").css("color","#666666");
			$(".nav-tabs>ul>li").css("padding-bottom","0px");
			$(this).css("background-color","#FFFFFF");
			$(this).css("color","#0888d1");
			$(this).css("padding-bottom","1px");
			$("#tab_a").css("min-height","410px");
		});
		
		$("#tab_b_li").click(function (){
			
			// not display:none. it will reload flash content
			$("#tab_a").css("visibility","hidden");
			$("#tab_b").css("visibility","inherit");
			$("#tab_c").css("visibility","hidden");
		
			// hide tinymce object
			$(".editor").hide();
			
			// change style tab css active
			$(".nav-tabs>ul>li").css("background-color","#F0F0F0");
			$(".nav-tabs>ul>li").css("color","#666666");
			$(".nav-tabs>ul>li").css("padding-bottom","0px");
			$(this).css("background-color","#FFFFFF");
			$(this).css("color","#0888d1");
			$(this).css("padding-bottom","1px");
			$("#tab_b").css("min-height","410px");			
		});
		
		$("#tab_c_li").click(function (){
				
			// not display:none. it will reload flash content
			$("#tab_a").css("visibility","hidden");
			$("#tab_b").css("visibility","hidden");
			$("#tab_c").css("visibility","inherit");
				
			// show tinymce object
			$(".editor").show();
			
			// change style tab css active
			$(".nav-tabs>ul>li").css("background-color","#F0F0F0");
			$(".nav-tabs>ul>li").css("color","#666666");
			$(".nav-tabs>ul>li").css("padding-bottom","0px");
			$(this).css("background-color","#FFFFFF");
			$(this).css("color","#0888d1");
			$(this).css("padding-bottom","1px");
			$("#tab_c").css("min-height","410px");			
		});
		
		
		tinymce.init({
			mode : "textareas",	
			selector: "#txtContent",
			width: "100%",
			convert_urls: false,
			theme: "modern",
			plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor"
			],
			toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			toolbar2: "print preview media | forecolor backcolor emoticons",
			image_advtab: true,
			templates: [
			// eMail templates courtesy of https://github.com/mailchimp/Email-Blueprints
			// slight mods to remove direct links back to mailchimp servers
			
			<cfoutput>#TemplateList#</cfoutput>
						
		]	
		});
		
		// hide tinymce object
		$(".editor").hide();
		
		$('select#inpAvailableShortCode').selectmenu({
			style:'popup',
			width: '100%',
			height:'20px',
			format: addressFormatting
		});
		
		<cfoutput>
			<cfif VoicePermissionStr.havePermission>
				$("##tab_a_li").click();
			<cfelseif SMSPermissionStr.havePermission>
				$("##tab_b_li").click();
			<cfelseif EmailPermissionStr.havePermission>
				$("##tab_c_li").click();
			</cfif>
		</cfoutput>
		
		//remove loading image when document ready
		$(".loading-data").removeClass("loading-data");
		$("#tabs").css("opacity",1);
					
		$("#select-BackToRecording").click( function() 
		{	
			<!--- Clear selected IVR Batch Id info--->
			$("#inpIVRBatchDesc").val("");
			$("#IVRBatchDesc").html("");
			$("#IVRBatchId").val(0);
			
			$("#select-BackToRecording").hide();
			$("#IVRBatchInfo").hide();						
			$("#record-new").show();
			$("#select-record").show();
			$("#select-IVR").show();
			
		}); 
			
		<!--- ***JLP Todo: Change to popup of stage - currently just opens new window/tab depending on browser setup --->
		$("#select-IVRReview").click( function() 
		{	
			if(parseInt($("#IVRBatchId").val()) > 0)
			{
				var IVRReviewTargetURL = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/stage/src/?inpBatchId=' + $("#IVRBatchId").val();
				var win = window.open(IVRReviewTargetURL, '_blank');
				win.focus();	
			}
			
		}); 
		
		<!--- ***JLP Todo: Change to popup of stage - currently just opens new window/tab depending on browser setup --->
		$("#select-IVRNew").click( function() 
		{	
			if(parseInt($("#IVRBatchId").val()) > 0)
			{
				var IVRNewTargetURL = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/batch/savenewbatch';
				var win = window.open(IVRNewTargetURL, '_blank');
				win.focus();	
			}
			
		}); 	
		
	});

	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
	var urlDownload="";
	var checkMicro= true;
	var recording = false;
	var alertMicro = false;
	function flash_callback(info){
		// put id error when crash
		if(info.code=="EVENT_ERROR"){
			console.log(info.description);
		}
		
		// Save is not successful because saving url service error		
		if (info.code=="RESPONSE_SAVE_RECORD_ERROR"){
			$('.save-process').hide();
			jAlert("Save record failure. Please check saving url service.", "Failure!", function(result) { } );
		}
		
		// Save is not successful because saving url service error		
		if (info.code=="CMDRECORDERROR"){
			jAlert("Record process error. Please check microphone.", "Failure!", function(result) { } );
		}
		
		// Load is not successful because url file error		
		if (info.code=="CMDLOADAUDIOERROR"){
			jAlert("Load audio failure. Please select other file.", "Failure!", function(result) { 
				$("#fileSelectTxt").attr('value','');
				playerflash('','flashRecorder');
			 });
		}
		
		// Load is not successful because url file error		
		if (info.code=="CMDLOADFILEERROR"){
			jAlert("Load audio failure. Please select other file.", "Failure!", function(result) { 
				$("#fileSelectTxt").attr('value','');
				playerflash('','flashRecorder');
			 });
		}
		
		// alert! Limit time duration record
		if (info.code=="LIMIT_RECORD"){
			sendToFlash('CMD_STOP');
			jAlert(info.Description, "Failure!", function(result) { } );
		}
		
		// Save successful
		if (info.code=="RESPONSE_SAVE_RECORD_SUCCESSFUL"){
			$('.save-process').hide();
			urlDownload=info.url;
			if(typeof(urlDownload) == "undefined" || typeof(urlDownload) == ""){
				jAlert("Save record failure. Please check saving url service.", "Failure!", function(result) { } );
			}
			var isedit = "<cfoutput>#ISEDIT#</cfoutput>";
		 	var isvoicedata = "<cfoutput>#ISVOICEDATA#</cfoutput>";
			if (isedit == 1 && isvoicedata == 1) {
				UpdateEmergencyMesage(update_saveOnly, urlDownload, info.libId, info.eleId, info.scrId);	
			}
			else{
				CreateNewEMS(urlDownload, info.libId, info.eleId, info.scrId);	
			}
		}
		
		// Not find a new record
		if (info.code=="CMDNOTRECORDFILESAVE_NEWRECORD"){
			$("#tab1").click();
			jAlert("Please create a new record!", "Failure!", function(result) { } );
		}
		
		// Request plug in microphone when recording
		if (info.code=="SHOW_MSSNOMIC"){
			if(recording && !alertMicro){
				jAlert("Please connect microphone to record!", "Failure!", function(result){
					alertMicro = true;
				});
			}
			checkMicro = false;
		}
		
		// Request plug in microphone when start record
		if (info.code=="START_RECORD"){
			recording = true;
			if (!checkMicro && !alertMicro) {
				jAlert("Please connect microphone to record!", "Failure!", function(result){
					alertMicro = true;
				});
			}
		}
		
		// Stop record and save duration to textField timeRecordTxt
		if (info.code=="STOP_RECORD"){
			$("#timeRecordTxt").attr('value',info.duration + '');
		}
		
		// Delete file record
		if (info.code=="CMDDELETEFILE_NEWRECORD"){
			jConfirm( "Are you sure you want to delete this Record?", "Delete Record", function(result) { 
				if(result)
				{	
					$("#fileSelectTxt").attr('value','');
					playerflash('','flashRecorder');
				}
				return false;																	
			});
		}
	}
	
	<!---// function setup flash movie to get ID--->
	function getFlashMovie(movieName)
	{
		var isIE = navigator.appName.indexOf("Microsoft") != -1;
		return (isIE) ? window[movieName] : document[movieName];
	}
	
	<!---// function send to flash from Javascript--->
	<!---
		This method is called everytime a EMS is saved with recorded on the fly voice content
		Processing is directed through Flash to save any recorded audio
		Flash then calls back to javascript via function flash_callback(info)
		
		This should NOT go through flash unless audio is recorded	
	--->
	function sendToFlash(val, lib, ele, scriptID)
	{
		if(typeof(lib) == "undefined" || typeof(ele) == "undefined" || typeof(scriptID) == "undefined")
		{
			<!--- Just use currently recorded audio --->
			document.getElementById("emsRecordPlayer").js_callback(val);				
		}
		else
		{
			<!--- Use of flash to copy over existing script ?!? --->	
			document.getElementById("emsRecordPlayer").js_callback(val, lib, ele, scriptID);				
		}
	}
	
	
	
	function playerflash(playId, container) {
		
		// remove content record-new object
		$('.record-new').html("<div id='flashRecorder' style='outline: 0;'></div>");
		
		if(typeof(container) == 'undefined'){
			container = "flashplayer";
			width = 400;
		}
		
		var flashvars; 
		// embed flash voice
		if(playId == ''){
			flashvars = {
				callbackFunction: "flash_callback",
				postService: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/act_save",
				backgroundShadow : "0xCCCCCC",
				maxRecordDuration: "<cfoutput>#emsMaxRecordDuration#</cfoutput>",
				urlStartFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/audio/start.mp3",
				urlStopFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/audio/stop.mp3"
			};
		}else{
			flashvars = {
				callbackFunction: "flash_callback",
				postService: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/act_save",
				backgroundShadow : "0xCCCCCC",
				<!--- ***JLP - BUG: RXDS dir and flash requires .cfm still for some reason--->
				urlFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/get-script.cfm?id="+playId,
				urlStartFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/audio/start.mp3",
				urlStopFile : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/audio/stop.mp3",
				maxRecordDuration: "<cfoutput>#emsMaxRecordDuration#</cfoutput>"
			};
		}
		
		
		var params = {
		 	quality:      'high',
			bgcolor:	'#FFFFFF#',
		 	play:		'true',
		 	loop:		'true',
		 	wmode:		'transparent',
		 	scale:		'showall',
		 	devicefont:	'false',
		 	salign:		'',
		 	allowscriptaccess:    'always'
		 	
		};
		var attributes = {
			 id:                   'emsRecordPlayer', 
			 name:                 'emsRecorder'
		};
		
		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/flash/emsrecorder.swf', container, '375px;', '270px;', '9.0.124', false, flashvars, params, attributes);
	
	}
	
	function checkValidateParseXML(data){
		// replace &amp; to &
		data = data.replace(/\&amp;/g,'&');
			
		// replace & to &amp;
		data = data.replace(/\&/g,'&amp;');
		
		return data;
	}
</script>


<cfinclude template="../email/dsp_edittemplates.cfm">


