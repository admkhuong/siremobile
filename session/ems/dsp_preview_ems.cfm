﻿<cfparam name="batchid" type="numeric" default="0" >
<cfparam name="methodType" type="numeric" default="1" >

<cfinvoke 
	component="#LocalSessionDotPath#.cfc.emergencymessages" 
	method="GetPreviewData" 
	returnvariable="GetPreviewData">
	<cfinvokeargument name="batchid" value="#batchid#" >
	<cfinvokeargument name="methodType" value="#methodType#" >
</cfinvoke>
<cfoutput>
	<!---display for Email Method = 3--->
	<cfif methodType EQ 3>
		<cfif StructkeyExists(GetPreviewData, "FROM") AND StructkeyExists(GetPreviewData, "SUBJECT") AND StructkeyExists(GetPreviewData, "EMAILCONTENT")>

			<!---from --->
			<div class="message-block m_top_10 m_left_0">
		        <div class="left-input">
		        	<span class="em-lbl">From</span>
					
					<div class="info-block" style="display:none">content info block</div>
				</div>
	        	<div class="right-input"><input id="txtEmail" value="#GetPreviewData.FROM#" disabled="true"></div>
	    	</div>
			<div class="clear"></div>
			
			<!---subject --->
			<div class="message-block m_top_10 m_left_0">
		        <div class="left-input">
		        	<span class="em-lbl">Subject</span>
					
					<div class="info-block" style="display:none">content info block</div>
				</div>
	        	<div class="right-input"><input id="txtSubject" value="#GetPreviewData.SUBJECT#" disabled="true"></div>
	    	</div>
			<div class="clear"></div>
			<div class="message-block m_top_10 m_left_0">
		        <div class="left-input-larger">
		        	<span class="em-lbl">Email Content</span>
				</div>
	        	<div class="right-input-larger mainmessage p_left_10">
	        		#GetPreviewData.EMAILCONTENT#
				</div>
	    	</div>
		<cfelse>
			There was an error when processing data, please contact your administrator.	
		</cfif>			
	<!---display for SMS Method = 4--->	
	<cfelseif methodType EQ 4>
		<cfif StructkeyExists(GetPreviewData, "SHORTCODE") AND StructkeyExists(GetPreviewData, "MAINMESSAGE")>		
			<!---ShortCode --->
			<div class="message-block m_top_10 m_left_0">
		        <div class="left-input">
		        	<span class="em-lbl">ShortCode</span>
				</div>
	        	<div class="right-input"><input id="txtEmail" value="#GetPreviewData.SHORTCODE#" disabled="true"></div>
	    	</div>
			<div class="clear"></div>
			<!---Main Message --->
			<div class="message-block m_top_10 m_left_0">
		        <div class="left-input-larger">
		        	<span class="em-lbl">Main Message</span>
				</div>
	        	<div class="right-input-larger mainmessage">
	        		#GetPreviewData.MAINMESSAGE#
				</div>
	    	</div>
			<div class="clear"></div>
		<cfelse>
			There was an error when processing data, please contact your administrator.	
		</cfif>		
	</cfif>
</cfoutput>
<style>
	.m_top_10{
		margin-top:10px;
	}
	
	.p_left_10{
		padding-left:10px;
	}
	
	.mainmessage ol{
		margin-left:20px;
		margin-top:5px;
	}
	
	.emailcontent
	{
		margin:10px 10px 10px 20px;
		border:1px solid #CCC;
		border-radius: 4px;
		background-color:#fff;
		height:auto;
		padding:5px;
		word-wrap:break-word;
	}
	.mainmessage{		
		border:1px solid #CCC;
		border-radius: 4px;
		background-color:#fff;
		height:auto;
		padding:5px;
		width: 518px;
		word-wrap:break-word;
	}
	.left-input-larger
	{
		border-radius: 4px 4px 0 0;
	    border-style: solid solid none solid;
	    border-width: 1px 1px medium 1px;
		border-color:#ccc;
	    color: #666666;
	    float: left;
	    line-height: 25px;
	    padding-left: 10px;
	    width: 518px;
		background-color:#fff;
	}
	.right-input-larger
	{
		border: 1px solid #CCCCCC;
	    border-radius: 0 0 4px 4px;
	    color: #666666;
	    float: left;
	    height: auto;
		color: #666666;
	}
</style>