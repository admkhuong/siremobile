﻿<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jstree/jstree.min.js">
	</script>
	<link type="text/css" rel="stylesheet" 
	      href="#rootUrl#/#PublicPath#/js/jstree/themes/default/style.min.css"/>
	<link type="text/css" rel="stylesheet" 
	      href="#rootUrl#/#publicPath#/css/datepicker/base.css"/>
	<link type="text/css" rel="stylesheet" 
	      href="#rootUrl#/#publicPath#/css/datepicker/clean.css"/>
</cfoutput>
<style type="text/css">
	#jstree_filter .left-input{
	    border-radius: 4px 0 0 4px;
	    border-style: solid none solid solid;
	    border-width: 1px medium 1px 1px;
	    color: #666666;
	    float: left;
	    height: 28px;
	    line-height: 25px;
	    padding-left: 10px;
	    width: 120px;
	}
	
	#jstree_filter .right-input > input {
	    background-color: #FBFBFB;
	    border: medium none !important;
	    color: #000000 !important;
	    font-family: "Verdana" !important;
	    font-size: 12px !important;
	    height: 28px;
	    line-height: 30px;
	    width: 195px;
	}
	#jstree_filter .right-input{
		width:211px;
	}
	#jstree_filter .message-block{
		margin-top:10px;
	}
	.ui-datepicker .ui-widget-content{
		width:200px;
	}
	#jstree_filter .ui-tabs .ui-tabs-panel{
		padding:0px;
	}
	#jstree_filter .ui-tabs-panel {
		min-height:140px;
	}
	#jstree_filter .ui-tabs .ui-tabs-nav {
	    background: none repeat scroll 0 0 #DDDDDD;
	}
	#jstree_filter .ui-tabs .ui-tabs-nav li a {
	    color: #666666;
	    font-family: Verdana;
	    font-size: 14px;
	    font-weight: bold;
	    line-height: 30px;
	    padding: 0;
	    text-align: center;
	    width: 119px;
	}
	#jstree_filter .ui-tabs-nav a, ui-tabs-nav a:link {
	    border-color: #AAAAAA;
	    border-radius: 5px 5px 0 0;
	    border-style: solid;
	    border-width: 1px;
	    color: #666666;
	    height: 28px;
	}
	#jstree_filter .ui-tabs-active a {
	    color: #0888D1 !important;
	}
	#jstree_filter .ui-tabs .ui-tabs-nav li {
	    background: none repeat scroll 0 0 #F0F0F0;
	    bottom: 0;
	    height: 30px;
	    margin-bottom: 0;
	    margin-left: 5px;
	    margin-top: 9px;
	    top: 0;
	    width: 120px;
	}
	#jstree_filter .ui-tabs .ui-tabs-nav li.ui-state-default {
	    border-bottom: 0 none;
	}
	#divShowHideTreeFilter{
		background: none repeat scroll 0 0 #F0F0F0;
		height:40px;
		cursor:pointer;
	}
	#divShowHideTreeFilter:hover{
		background: none repeat scroll 0 0 #DDDDDD;
		height:40px;
		cursor:pointer;
	}
	#ShowHideSpan{
		margin-left:10px;
	}
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Select the script you wish to use</h4>     
</div>
            
<div id="jstree_filter">
	<div id="divShowHideTreeFilter" >
		<span id = "ShowHideSpan" rel="0">Click here to collapse/expand filter</span>
	</div>
	<script type="text/javascript">
		$("#divShowHideTreeFilter").click(function(){
			$("#jsTreeFilterContainer").toggle("slow");
		});
	</script>
	<div id="jsTreeFilterContainer" style="display:none;">
		<div id="jsTreeFilterTab" class="m_top_10">
			<ul>
				<li><a href="#divLib">Library</a></li>
				<li><a href="#divElement">Element</a></li>
				<li><a href="#divScript">Script</a></li>
			</ul>
			<div id="divLib">
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">Library Name</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="name_jsTree_Lib" class="clearable" value=""></div>
			    </div>
				<div class="clear m_top_20">&nbsp;</div>
			</div>
			<div id="divElement">
				<!---<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">From</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="from_jsTree_Ele" class="jsTree-datepicker clearable" value=""></div>
			    </div>
				<div class="clear"></div>
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">To</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="to_jsTree_Ele" class="jsTree-datepicker clearable" value=""></div>
			    </div>
				<div class="clear"></div>--->
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">Element Name</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="name_jsTree_Ele" class="clearable" value=""></div>
			    </div>
				<div class="clear m_top_20">&nbsp;</div>
			</div>
			<div id="divScript">
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">From</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="from_jsTree_Script" class="jsTree-datepicker clearable" value=""></div>
			    </div>
				<div class="clear"></div>
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">To</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="to_jsTree_Script" class="jsTree-datepicker clearable" value=""></div>
			    </div>
				<div class="clear"></div>
				<div class="message-block">
			        <div class="left-input">
			        	<span class="em-lbl">Script Name</span>
						<div style="display:none" class="info-block">content info block</div>
					</div>
			        <div class="right-input"><input type="text" id="name_jsTree_Script" class="clearable" value=""></div>
			    </div>
				<div class="clear m_top_20">&nbsp;</div>
			</div>
		</div>
		<div style="margin-top:10px;padding-right:19px;">
			<input type="button" class="button filterButton small btn-auu-admin" style="float:right;" id="btnApplyJstreeFilter" value="Filter" onclick="ApplyTreeFilter();return false;">
			<input type="button" class="button filterButton small btn-auu-admin" style="float:right;" id="btnClearJstreeFilter" value="Clear" onclick="ClearTreeFilter();return false;">
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div id="jstree_demo_div" class="m_top_20">
</div>

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    
    </div>	
</div>   
    
    
<script>
	$("#jsTreeFilterTab").tabs();
	$(".jsTree-datepicker").datepicker({
		inline: true,
		calendars: 4,
		extraWidth: 100,
		dateFormat: 'yy-mm-dd' 
	}).attr('readonly','true');
	
	function ApplyTreeFilter(){
		GetTreeData();
	}
	
	function ClearTreeFilter(){
		$(".clearable").val("");
		GetTreeData();
	}
	
	function ValidateTreeFilter(){
		var validateResult = false;
		//validate script tab
		var fromDate = $("#from_jsTree_Script").val();
		var toDate = $("#to_jsTree_Script").val();
		if(fromDate != "" && toDate != ""){
			if(fromDate > toDate){
				bootbox.alert("From field must be less than To field. Please input again!- Script Filter Error", function(result) {
					$("#jsTreeFilterTab").tabs( "select", "divScript" );
					$("#to_jsTree_Script").focus();
				});
				return validateResult;
			}
		}
		
		validateResult = true;
		return validateResult;
	}
	
	function GetTreeData(){
		if(!ValidateTreeFilter())
			return false;
		var libQueryString = "libName="+$("#name_jsTree_Lib").val();
		var elementQueryString = "&elementName="+$("#name_jsTree_Ele").val();
		var scriptQueryString = "&scriptFrom="+$("#from_jsTree_Script").val()+"&scriptTo="+$("#to_jsTree_Script").val()+"&scriptName="+$("#name_jsTree_Script").val();
		$('#jstree_demo_div').jstree("destroy");
	    $('#jstree_demo_div').jstree({
			'core': {
				'data': {
					'url': function(node){
						var nodeId = node.id;
						var url = "";
						if (nodeId === '#') {
							url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getlibrary?'+libQueryString+elementQueryString+scriptQueryString;
						}
						else 
							if (nodeId.indexOf('ds') != -1) {
								url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getelement?'+elementQueryString+scriptQueryString;
							}
							else {
								url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getscript?'+scriptQueryString;
							}
						return url;
					},
					'data': function(node){
						return {
							'id': node.id
						};
					}
				}
			}
		});
		// add listen for event
		$('#jstree_demo_div').on("changed.jstree", function (e, data) {			
			$(document).on( "click", ".select_script", function() {
				 // check data out
				  var str = data.node.id.toString(),
				  _ids = str.split("_");
				if(_ids.length == 4){
					//close tree
					$("#fileSelectTxt").attr('value',str);
			  		playerflash(str,'flashRecorder');	
					<!--- Manually close modal--->
					$('#SelectScriptModal').modal('hide');					
		  		}
			});
		});
	}
	
	$(function(){
	    $('#jstree_demo_div').jstree({
	        'core': {
	            'data': {
	                'url': function(node){
	                    var nodeId = node.id;
	                    var url = "";
	                    if (nodeId === '#') {
							url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getlibrary';
	                    }
	                    else 
	                        if (nodeId.indexOf('ds') != -1) {
	                            url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getelement';
	                        }
	                        else {
	                            url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getscript';
	                        }
	                    return url;
	                },
	                'data': function(node){
	                    return {
	                        'id': node.id
	                    };
	                }
	            }
	        }
	    });
		
		// add listen for event
		$('#jstree_demo_div').on("changed.jstree", function (e, data) {			
			$(document).on( "click", ".select_script", function() {
				 // check data out
				  var str = data.node.id.toString(),
				  _ids = str.split("_");
				if(_ids.length == 4){
					//close tree
					$("#fileSelectTxt").attr('value',str);
			  		playerflash(str,'flashRecorder');			  		
					<!--- Manually close modal--->
					$('#SelectScriptModal').modal('hide');
					
		  		}
			});
		});
	    
	});
</script>