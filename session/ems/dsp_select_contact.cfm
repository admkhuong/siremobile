﻿<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset currentAccount = permissionObject.getCurentUser()>

<cfset Session.USERROLE  = currentAccount.USERROLE> 

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Add_Contact_Group_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfset session.permissionError = permissionStr.message>
<cfelse>

<style type="text/css">
	#ListGroupsContainer #tblListGroupContact_info{
		margin-left:0px;
		width:100%;
		height:auto;
	}
	#ListGroupsContainer #SelectContactPopUp {
		margin: 15px;
	}
	#ListGroupsContainer #tblListGroupContact_info {
		width: 100%;
		padding-left: 1em;
	}
	#ListGroupsContainer .ui-dialog{
		padding-bottom:10px;
	}
	
	#ListGroupsContainer .table 
	{
    	margin-bottom: 0;
  	}

</style>


<!-- Modal -->
			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select the list of contacts you wish to message</h4>     
			</div>


			<div class="modal-body">
            
             	<!--- Surround in own styled div so styles dont apply to parent objects when this is used as a picker --->
    			<div id="ListGroupsContainer">
    

					<!---this is custom filter for datatable--->
                    <div id="filter">
                        <cfscript>
                            strTotalFilter = "( SELECT COUNT(*) FROM simplelists.rxmultilist r WHERE r.grouplist_vch LIKE CONCAT('%,'" ;
                            strTotalFilter =  strTotalFilter & " , g.GROUPID_BI, ',%')"; 
                            if(session.userRole NEQ 'SuperUser'){
                                strTotalFilter = strTotalFilter & " AND r.UserId_int = #Session.USERID#";
                            }
                            strTotalFilterPhone = strTotalFilter & " AND r.ContactTypeId_int=1 )";
                            strTotalFilterEmail = strTotalFilter & " AND r.ContactTypeId_int=2 )";				
                            strTotalFilterSMS = strTotalFilter & " AND r.ContactTypeId_int=3 )";
                        </cfscript>
                        <cfoutput>
                        <!---set up column --->
                        <cfset datatable_ColumnModel = arrayNew(1)>
                        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List id', CF_SQL_TYPE = 'CF_SQL_BIGINT', TYPE='INTEGER', SQL_FIELD_NAME = 'g.GroupId_bi'})>
                        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'List name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'g.GroupName_vch'})>
                        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total phone', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.phoneNumber'})>
                        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total email', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.mailNumber'})>
                        <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total SMS', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'contactCount.smsNumber'})>
                        <!---we must define javascript function name to be called from filter later--->
                        <cfset datatable_jsCallback = "InitGroupContact">
                        <cfinclude template="datatable_filter.cfm" >
                        </cfoutput>
                    </div>
                    <div class="border_top_none">
                        <table id="tblListGroupContact" class="table">
                        </table>
                    </div>
                    
                    <div style="clear:both"></div>
                    
	            </div>
             
            </div>
             
            <div class="modal-footer">   
                <div class="sms_popup_action">
                
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                
                </div>	
            </div>   
    
 

    
<script>
	var _tblListGroupContact;
	//init datatable for active agent
	function InitGroupContact(customFilterObj){
		var customFilterDataGroupContact =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListGroupContact = $('#tblListGroupContact').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false},
				<!---{"sName": 'List Id', "sTitle": 'List Id', "sWidth": '8%',"bSortable": false},--->
				{"sName": 'List Name', "sTitle": 'List Name', "sWidth": '36%',"bSortable": true},
				{"sName": 'Voice', "sTitle": 'Voice', "sWidth": '12%',"bSortable": true},
				{"sName": 'SMS', "sTitle": 'SMS', "sWidth": '12%',"bSortable": true},
				{"sName": 'Email', "sTitle": 'Email', "sWidth": '12%',"bSortable": true}				
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetGroupListForDatatableForPicker&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterDataGroupContact}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				<!---//in order to prevent this talbe from collapsing, we must fix its min width --->
				<!---$('#tblListGroupContact').attr('style', '');
				$('#tblListGroupContact').css('min-width', '100%');--->
			}
	    });
	}
	
	$(document).ready(function(){
		InitGroupContact();
	
	});
	
</script>
</cfif>