﻿<cfparam name="batchid" type="numeric" default="0" >
<cfset mode = "edit">
<cfinclude template="createnewemergency_js.cfm">
<!---check permission for edit emergency --->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#EMS_Edit_Title#">
</cfinvoke>
<cfif NOT Session.USERID GT 0>
	<cfexit><!---exit if session expired --->
</cfif>
<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
    </cfinvoke>
<cfelse>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#session.userId#">
    </cfinvoke>
</cfif>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="VoicePermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Voice_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="SMSPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_SMS_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="EmailPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Email_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
	OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
	<div style="color:red;">
		You do not have permission to edit new EMS!
	</div>
	<cfexit>
</cfif>
<!---end check permission --->
<cfinvoke 
	component="#LocalSessionDotPath#.cfc.emergencymessages" 
	method="GetEmergencyById" 
	returnvariable="EmergencyById">
	<cfinvokeargument name="batchid" value="#batchid#" >
</cfinvoke>
<cfif EmergencyById.RXRESULTCODE LT 0>
<div>
	Could not locate to this EMS. Please contact your administrator.
</div>
<cfelse>
	<cfset SCHEDULE = Arraynew(1)>
	<cfif Arraylen(EmergencyById.SCHEDULEBYBATCHID) GT 0>
		<cfset SCHEDULE = EmergencyById.SCHEDULEBYBATCHID>	
	</cfif>
	<cfoutput>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/custominput.jquery.js"></script>
		<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>	
		<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
		<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
		<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
		<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/wddx.js"></script>
		<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/DataTables-1.9.4/media/js/jquery.datatables.min.js"></script>	
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/DataTables-1.9.4/media/css/jquery.datatables.css">
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/DataTables-1.9.4/media/css/jquery.datatables_themeroller.css">
		<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
		
		<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/style5.css" />
		
		<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>
		
		<script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
		<script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
		<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
		<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>	
	   
	</cfoutput>
	
	<div id="content-AAU-Admin">
		<div id="head-AAU-Admin">e-Messaging - Messages</div>
		
		<!---block 1--->
	
		<div class="messages-lbl">Campaign Name</div>
	    <div class="message-block">
	        <div class="left-input">
	        	<span class="em-lbl">Campaign</span>
				<div class="hide-info showToolTip">&nbsp;</div>
				<div class="tooltiptext">
					Give your campaign a name so you may more easily identify it later.
				</div>		
				<div class="info-block" style="display:none">content info block</div>
			</div>
	        <div class="right-input"><input type="text" value="<cfoutput>#EmergencyById.CAMPAIGNNAME#</cfoutput>" id="campaign-name-txt"></div>
	    </div>
		<div class="clear"></div>
		<!---block 2--->
		<div class="messages-lbl">Add Contacts</div>		
		<div class="inputbox-container">
	        <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value='<cfoutput>#EmergencyById.GROUPID#</cfoutput>'>
	         <div class="wrapper-picker-container">        	    
	            <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
					<cfif EmergencyById.GROUPNAME neq "">
						<span id="inpGroupPickerDesc"><cfoutput>#EmergencyById.GROUPNAME#</cfoutput></span>
					<cfelse>
						<span id="inpGroupPickerDesc">Click here to select a contact list</span>
					</cfif> 
					<a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   
				</div>                                            
	        </div>
	    </div>
		
		<div>&nbsp;</div>
		
		<div id="add-group" class="EM-btn add-btn hide" onclick = "AddGroup();">Add Group</div>
		<!---<div id="add-contact" class="EM-btn add-btn" onclick="AddContact();return false;">Add Contact to selected group </div>--->
		<div class="inputbox-container">
	         <div class="wrapper-picker-container">        	    
	            <div class="wrapper-dropdown-picker " tabindex="1" onclick="AddContact();return false;"> 
					<span id="inpGroupPickerDesc">Click here to add a contact to selected group</span> 
					<a class="sbToggle" href="javascript:void(0);"></a>   
				</div>                                            
	        </div>
	    </div>
		
		<div id="delete-checked" class="EM-btn delete-btn hide">Delete Checked</div>
		<div class="clear"></div>
		
		<!---
		for version 2
		<div id="divAgent" class="border_top_none">
			<div id="activeAgents">
				<table id="tblActiveAgent" class="table">
				</table>
			</div>
		</div>
		<div class="clear-fix"></div>--->
		
		<!---block 2-2 contact preview --->
		<div>
			<cfinclude template="dsp_contact_preview.cfm">
		</div>
		
		<!---block 3--->
		<cfset disableSMSText = "">		
		<div class="messages-lbl">Message</div>
			<div class="loading-data">  
				<cfinclude template="dsp_message.cfm">
			</div>
		<div class="clear"></div>
		
		<!---block 4--->
		<div class="messages-lbl">Delivery Methods</div>
		<div class="clear"></div>
		<div class="deliverymethods" align="center">
			<div style="width:auto; height:auto;">
			<cfif VoicePermissionStr.havePermission>
				<div class="method-btn voice-checkbox" style="float:left; margin-left:23px;">
					<input name="inpContactMethodVoice" id="inpContactMethodVoice" type="checkbox" value="1" <cfoutput>#ArrayContains(EmergencyById.ContactTypesArray, 1) ? 'checked="checked"': ""#</cfoutput>/>					 
					<label for="inpContactMethodVoice"> Voice Call </label>
				</div>
			</cfif>
			<cfif SMSPermissionStr.havePermission>
				<div class="method-btn voice-checkbox" style="float:left; margin-left:23px;">
					<input name="inpContactMethodSMS" id="inpContactMethodSMS" <cfoutput>#disableSMSText#</cfoutput> type="checkbox" value="1" <cfoutput>#ArrayContains(EmergencyById.ContactTypesArray, 3) ? 'checked="checked"': ""#</cfoutput>/> 
					<label for="inpContactMethodSMS"> SMS </label>
				</div>
			</cfif>
			<cfif EmailPermissionStr.havePermission>
				<div class="method-btn voice-checkbox" style="float:left; margin-left:23px;">					
					<input name="inpContactMethodEmail" id="inpContactMethodEmail" type="checkbox" value="1" <cfoutput>#ArrayContains(EmergencyById.ContactTypesArray, 2) ? 'checked="checked"': ""#</cfoutput>/>						  
					<label for="inpContactMethodEmail"> E-Mail </label>
				</div>
			</cfif>
			</div>
		</div>
		<br />
		<div class="clear"></div>
		
		<div class="warning-block">
			<div id="warning-content"><span class="warning-text">Notice:</span> By default, messages are scheduled between 9am - 7pm.</div>
			
	        
	        <div onclick="OpenDialogAdvancedSchedule();">
	        	<div id="ScheduleBtn" class="active" title="Schecule" style="float:left; margin-right:10px;"></div>
	        	<div style="float:left;" class="TextLinkHoverStyle">Click here if you would like to enable alternate delivery times.</div>
	        </div>
			<br />
			<div class="clear"></div>
		</div>
		<br /><br />
		<div class="clear"></div>	
		<div id="footer-AAU-Admin">
			<a href="javascript:void(0);" onclick="UpdateEmergency(false);return false;" class="button filterButton small btn-auu-admin">Update and launch</a>			
	        <a href="javascript:void(0);" onclick="UpdateEmergency(true);return false;" class="button filterButton small btn-auu-admin">Update</a>
		</div>
	</div>
	
	<script type="text/javascript">
			
	function Play(selector, index, curObj){
				//check rel2 is mp3 path file
				var mp3file = $("#jquery_jplayer_bs_" + index).attr("rel2");
				//if does not exist, call ajax to load file
				if(mp3file == "")
				{
					//load file to play
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=LoadMp3File&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async:false,
						data:  { 
							idFile : $(curObj).data("id")
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) {					
						},					  
						success:		
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d) 
							{								
								CurrRXResultCode = d.RXRESULTCODE;	
								if(CurrRXResultCode > 0)
								{
									$("#jquery_jplayer_bs_" + index).attr("rel2", d.PLAYMYMP3);							
									$("#jquery_jplayer_bs_" + index).jPlayer({
										ready: function () {
											$(this).jPlayer("setmedia", {
												mp3: d.PLAYMYMP3
											}).jPlayer("play");
										},
										cssSelectorAncestor: '#jquery_jplayer_bs_' + index,				 	
										swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
										supplied: "mp3",
										wmode: "window"
									});							
									
									$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
										$("#cp-play-"+index).show();
										$("#cp-pause-"+index).hide();
								  	});
								}
							} 		
					});
				}
				else{
					$(selector).jPlayer("play");	
				}
				$("#cp-play-"+index).hide();
				$("#cp-pause-"+index).show();
			}
			
			
			function Pause(selector, index){
				$(selector).jPlayer("pause");
				$("#cp-play-"+index).show();
				$("#cp-pause-"+index).hide();
			}
	
	 $(document).ready(function()
	 {
	     // MAKE SURE YOUR SELECTOR MATCHES SOMETHING IN YOUR HTML!!!
	     $('.showToolTip').each(function() {
	         $(this).qtip({
	             content: {
	                 text: $(this).next('.tooltiptext')
	             }
	         });
	     }); 
		 //custom Input
		 $('input#inpContactMethodVoice').custominput();
		 $('input#inpContactMethodSMS').custominput();
		 $('input#inpContactMethodEmail').custominput();
		 $('input#inpEnableImmediateDelivery').custominput();
		 $('input#inpUseAdvancedSchedule').custominput();
		 $('input#inpDeliveryNow').custominput();
		 ApplyFilter_CDF(true);
	 });
	 
	 
	 
	 
	</script>
	<cfinclude template="createnewemergency_js.cfm">
	<div id="AdvanceScheduleContent" style="display:none;">
		<cfinclude template="dsp_advance_schedule.cfm">
	</div>
</cfif>	
<style type="text/css">
		.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			width: 700px;
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#IVRPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#IVRPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#BackToRecordingBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#BackToRecordingBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 280px;
		}
		
		.ui-dialog-content 
		{
		    overflow: scroll !important;
		}
		
	</style>