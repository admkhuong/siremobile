<cftry>
	<cfinclude template="lib/jsonencode.cfm">
	<cfscript>
		retVal = StructNew();
		retVal.success = true;
		retVal.message = "Save file success";
		retVal.url = "Save file success";
		retVal.libId = "libId file";
		retVal.eleId = "eleId file";
		retVal.scrId = "srcID file";
	</cfscript>
	
	<cfif isDefined("scrId")>
		<cfset ids = ListToArray(scrId, "_") />
		<cfset userId = Session.UserID /> 
		<cfset libId = ids[1] /> 
		<cfset eleId = ids[2] /> 
		<cfset scrId = ids[3] />
	
		<cfquery name="getScript" datasource="#application.datasource#">
			SELECT 
				UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
			FROM 
				#application.database#.scriptdata
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libId#">
				AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
				AND DataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scrId#">
				AND Active_int = 1
			LIMIT 1
		</cfquery>
		
		
		<cfif Not DirectoryExists("#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/")>
			<cfdirectory action = "create" directory = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/" recurse="yes" mode="0775" />
		</cfif>
		<cfif getScript.RecordCount GT 0>
			<cffile action="write" file = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/RXDS_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3" output = "#getHTTPRequestData().content#">
			<cfscript>
				retVal.success = true;
				retVal.message = "Save file success";
				retVal.url = "RXDS_#userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3";
				retVal.libId = "#libId#";
				retVal.eleId = "#eleId#";
				retVal.scrId = "#scrId#";
			</cfscript>
		<cfelse>
	 		<cfscript>
				retVal.success = false;
				retVal.message = "ScriptId is incorect";
				retVal.url = "";
				retVal.libId = "";
				retVal.eleId = "";
				retVal.scrId = "";
			</cfscript>
	 	</cfif>
	<cfelse>
		<cfscript>
			retVal.success = false;
			retVal.message = "ScriptId is incorect";
			retVal.url = "";
			retVal.libId = "";
			retVal.eleId = "";
			retVal.scrId = "";
		</cfscript>
	</cfif>
	
<cfcatch type="Any" >
</cfcatch>
</cftry>

<cfoutput>#jsonencode(retVal)#</cfoutput>