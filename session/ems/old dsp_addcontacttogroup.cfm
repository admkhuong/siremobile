

<cfparam name="inpGroupId6" default="0">
<cfparam name="inpContactTypeId" default="1">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="VoicePermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Voice_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="SMSPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_SMS_Title#">
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="EmailPermissionStr">
	<cfinvokeargument name="operator" value="#EMS_Email_Title#">
</cfinvoke>


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>
			
	.modal-body select {
		font-size: 1em;
	}
	
	.inputbox-container input[type='text'], .inputbox-container select 
	{
		width:100%;	
	}
	
	.small
	{
		font-size:.8em;	
	}

</style>


<script type="text/javascript">


	$(function() 
	{		
				
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();			
		});	
										
	});



	<!---// Save contacts to existing group--->
	function AddNewContact() {
		// Call ajax
		$("#err_group_id").hide();
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=AddContactStringToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			INPCONTACTSTRING: $("#ADDContactStringToGroupContainer #inpContactString").val(), 
			INPCONTACTTYPEID: $("#ADDContactStringToGroupContainer #inpContactTypeId").val(), 
			INPUSERSPECIFIEDDATA: $("#ADDContactStringToGroupContainer #inpUserSpecifiedData").val(), 
			INPGROUPID: $("#ADDContactStringToGroupContainer #inpGroupId6").val(), 
			INPALLOWDUPLICATES: $('input[name=ISDuplicateAllowed]:checked', '#ADDContactStringToGroupContainer').val(),
			INPAPPENDFLAG: $('input[name=IsAppend]:checked', '#ADDContactStringToGroupContainer').val()
		},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			function(d) 
			{
				if(parseInt(d.RESULT)  < 1){
					if(d.MESSAGE != "TIMEOUT"){
						bootbox.alert(d.MESSAGE, 'Warning');
						return false;
					}
				}else{					
					if(d.DATA.RXRESULTCODE[0] != 1){
						bootbox.alert('Add contact failed!<br/>'+d.DATA.MESSAGE[0] + ' - Failure', function(result) {
						});
					}else{
						CloseDialog('newContact');
						bootbox.alert('Add contact successful', function(result) {
							$('#AddContactModal').modal('hide');						
						} );
					}
										
				}
			} 		
			
		});		
	}
		

</script>	


<cfoutput>
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      
        <h4 class="modal-title">Add a new contact to list</h4>        
      
    </div>


	<div class="modal-body">
                                      
           
        <input type="hidden" name="inpGroupId6" id="inpGroupId6" value="#inpGroupId6#" /> 
                                                
        <div class="inputbox-container">
        
            <label for="INPDESC">Contact Type <span class="small">Required</span></label>
            <select id="inpContactTypeId" name="inpContactTypeId">
                <cfif VoicePermissionStr.havePermission>
                    <option value="1" >Voice</option>
                </cfif>
                
                <cfif EmailPermissionStr.havePermission>
                    <option value="2" >e-mail</option>
                </cfif>
                
                <cfif SMSPermissionStr.havePermission>
                    <option value="3" >SMS</option>
                </cfif>
                
            </select>  
        </div>
       
        <div style="clear:both"></div>

        <div class="inputbox-container">
            <label for="INPDESC">Contact String <span class="small">Required</span></label>
            <BR />
            <input type="text" id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" autofocus="autofocus" />
        </div>
                                                               
        <div style="clear:both"></div>

        <div class="inputbox-container">
            <label for="INPDESC">Notes <span class="small">Optional</span></label>
            <BR />
            <input type="text" id="inpUserSpecifiedData" name="inpUserSpecifiedData" placeholder="Enter Notes Here" size="20" />
        </div>
        
        <div style="clear:both"></div>
        
        <div class="inputbox-container">
            <label class="left bold_label">Allow Duplicates</label>
            <BR />
            <div style="text-align:left; width:100%; float:left;">
                <input type="radio" name="ISDuplicateAllowed" value="1" id="ISDuplicateAllowed" class="apply_yesno" />                               
                <span class="apply_yesno_label">Yes</span>
                <input type="radio" name="ISDuplicateAllowed" id="ISDuplicateAllowed" value="0" class="apply_yesno" checked/>                                
                <span class="apply_yesno_label">No</span>
	        </div>
        </div>
        
        <div style="clear:both"></div>
        
        <div class="inputbox-container">
            <label class="left bold_label">Append to if Found</label>
            <BR />
            <div style="text-align:left; width:100%; float:left;">
                <input type="radio" name="IsAppend" value="1" id="IsAppend" class="apply_yesno" checked/>                               
                <span class="apply_yesno_label">Yes</span>
                <input type="radio" name="IsAppend" id="IsAppend" value="0" class="apply_yesno" />                                
                <span class="apply_yesno_label">No</span>
            </div>
        </div>
          
        <div style="clear:both"></div>
        <div style="clear:both"></div>
                
         
    
	</div>
    
     <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick ="AddNewContact(); return false" data-dismiss="modal">Add Contact</button>

    </div>	
            
</cfoutput>


    