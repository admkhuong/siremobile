<cfparam name="id" default="##">
<cfparam name="libName" default="">
<cfparam name="elementName" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptTo" default="">
<cfparam name="scriptName" default="">

<cfset getLibrary = 0>
<cfset libraryArr = ArrayNew(1)>
		
<cftry>
	<cfquery name="getLibrary" datasource="#Session.DBSourceEBM#">
			SELECT 
				count(Ele.DSEID_int) as TotalScript,
				lib.UserId_int, 
				lib.DSId_int as DSId_int, 
				lib.Desc_vch as Desc_vch
			FROM 
				rxds.dynamicscript as lib
			INNER JOIN
				rxds.dselement as ele
			ON
				Ele.DSId_int = lib.DSId_int
			AND
				lib.UserId_int = Ele.UserId_int
			INNER JOIN
				rxds.scriptData as script
			ON 
				script.DSEID_int = Ele.DSEID_int
			AND
				script.UserId_int = Ele.UserId_int
			AND
				script.DSId_int = Ele.DSId_int <!---DSId_int need to be here --->
			WHERE 
				lib.UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
			<cfif libName NEQ "">
				AND
				lib.Desc_vch Like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#URLDecode(libName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
			<cfif elementName NEQ "">
				AND
					ele.Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#URLDecode(elementName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
			<cfif scriptFrom NEQ "">
				AND
					script.Created_dt >= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptFrom# 00:00:00">
			</cfif>
			<cfif scriptTo NEQ "">
				AND
					script.Created_dt <= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptTo# 23:59:59">
			</cfif>
			<cfif scriptName NEQ "">
				AND
					script.Desc_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#URLDecode(scriptName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
			group by lib.DSId_int
			order by lib.DSId_int asc
	</cfquery>
	<cfset libraryArr = ArrayNew(1)>
	<cfloop query="getLibrary">
		<cfset hasChildren = false>
		<cfif getLibrary.TotalScript GT 0 >
			<cfset hasChildren = true>	
		</cfif>
		
		<cfset libItem = {
			"id" = 'ds_' & getLibrary.DSId_int,
			"text" = getLibrary.Desc_vch,
			"children" = hasChildren
		}>
		<cfset ArrayAppend(libraryArr, libItem)>
	</cfloop>
	
<cfcatch type="Any" >
</cfcatch>
</cftry>

<cfheader name="Content-Type" value="application/json;charset=UTF-8" />

<cfoutput>#serializeJSON(libraryArr)#</cfoutput>


