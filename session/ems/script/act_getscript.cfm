﻿<cfparam name="id" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptFrom" default="">
<cfparam name="scriptTo" default="">
<cfparam name="scriptName" default="">
<cfset audioArr = ArrayNew(1)>
<cfset getAudio = 0>
<cftry>
	<!---Get element id --->
	<cfset id = ReplaceNoCase(id, "ele_", "")>
	<cfset data = listToArray(id, '_')>
	<cfset dsId = data[1]>
	<cfset eleId = data[2]>
	
	<cfquery name="getAudio" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, 
				DSId_int,
				DSEID_int, 
				Desc_vch,
				DataId_int
			FROM 
				rxds.ScriptData
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
			AND
				DSId_int = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dsId#">
			AND
				DSEID_int = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
			<cfif scriptFrom NEQ "">
				AND
				Created_dt >= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptFrom# 00:00:00">
			</cfif>
			<cfif scriptTo NEQ "">
				AND
				Created_dt <= <CFQUERYPARAM CFSQLTYPE="cf_sql_timestamp" VALUE="#scriptTo# 23:59:59">
			</cfif>
			<cfif scriptName NEQ "">
				AND
				Desc_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#URLDecode(scriptName)#%"><!---need to use URLDecode here due to param has been passed as urlencode format by jstree lib--->
			</cfif>
	</cfquery>
	<cfset audioArr = ArrayNew(1)>
	
	<cfset DisplayAvatarClass = 'cp-icon_00'>
	<cfset iFlashCount = 1>
	<cfloop query="getAudio">
		<cfset iFlashCount = getAudio.UserId_int & '_' & getAudio.DSEID_int & '_' & getAudio.DSId_int & '_' & getAudio.DataId_int>
		<!---Init mp3 player --->
		<cfset PlayMyMP3_BD = "">
		<cfset PlayVoiceFlashHTML5 = '<div class="jquery_jplayer babbleItem#iFlashCount#" style="width:20px; float:left;margin-right: 10px;">'>
                        
        <!--- The jPlayer div must not be hidden. Keep it at the root of the body element to avoid any such problems. --->
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="jquery_jplayer_bs_#iFlashCount#" rel1="#iFlashCount#" rel2="#PlayMyMP3_BD#" rel3="#getAudio.UserId_int#" rel4="#getAudio.DSEID_int#" class="cp-jplayer"></div>' >  
		
        <!--- The container for the interface can go where you want to display it. Show and hide it as you need. --->
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '<div id="cp_container_bs_#iFlashCount#" class="cp-container #DisplayAvatarClass#">' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-buffer-holder"> ' >  <!--- .cp-gt50 only needed when buffer is > than 50% --->
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-1"></div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-buffer-2"></div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-progress-holder"> ' >  <!--- .cp-gt50 only needed when progress is > than 50% --->
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-1"></div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <div class="cp-progress-2"></div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <div class="cp-circle-control"></div>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    <ul class="cp-controls">' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-play-#iFlashCount#" href="javascript:void(0);" onclick="Play(''jquery_jplayer_bs_#iFlashCount#'', ''#iFlashCount#'', this);" data-id="#getAudio.UserId_int#_#getAudio.DSId_int#_#getAudio.DSEID_int#_#getAudio.DataId_int#" class="cp-play" tabindex="1">play</a></li>' >  
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '        <li><a id="cp-pause-#iFlashCount#" href="javascript:void(0);" onclick="Pause(''jquery_jplayer_bs_#iFlashCount#'', ''#iFlashCount#'');" class="cp-pause" style="display:none;" tabindex="1">pause</a></li> ' >  <!--- Needs the inline style here, or jQuery.show() uses display:inline instead of display:block --->
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '    </ul>' >
        <cfset PlayVoiceFlashHTML5 = PlayVoiceFlashHTML5 & '</div></div>' >
		 
		
		<cfset audioItem = {
		    "id" = '#session.userId#_' & getAudio.DSId_int & "_" & getAudio.DSEID_int & "_" & getAudio.DataId_int,
			"text"= PlayVoiceFlashHTML5 & '<a class="select_script">' & getAudio.Desc_vch & '</a>',
			"icon" = "jstree_no_icon"
		}>	
		<cfset ArrayAppend(audioArr, audioItem)>
	</cfloop>
<cfcatch type="Any" >
</cfcatch>
</cftry>

<cfheader name="Content-Type" value="application/json;charset=UTF-8" />

<cfoutput>#serializeJSON(audioArr)#</cfoutput>