<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jstree/jstree.min.js">

	</script>
	
	<link type="text/css" rel="stylesheet" 
	      href="#rootUrl#/#PublicPath#/js/jstree/themes/default/style.min.css"/>
	
</cfoutput>

<div id="jstree_demo_div">
<div>

<script>
	$(function(){
	    $('#jstree_demo_div').jstree({
	        'core': {
	            'data': {
	                'url': function(node){
	                    var nodeId = node.id;
	                    var url = "";
	                    if (nodeId === '#') {
							url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getlibrary';
	                    }
	                    else 
	                        if (nodeId.indexOf('ds') != -1) {
	                            url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getelement';
	                        }
	                        else {
	                            url = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/script/act_getscript';
	                        }
	                    return url;
	                },
	                'data': function(node){
	                    return {
	                        'id': node.id
	                    };
	                }
	            }
	        }
	    });
	    
	});
</script>