<cfparam name="isDialog" default="0">
<cfparam name="errid">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
    <cfinvokeargument name="operator" value="#EMS_List_Title#">
</cfinvoke>

<cfset errorLogData = {} >
<cfinvoke component="public.sire.models.cfc.error_logs" method="getErrorLogDetailById" returnvariable="errorLogData">
     <cfinvokeargument name="errid" value="#errid#">
</cfinvoke>

<cfif Session.USERROLE NEQ 'SuperUser'>
    <cflocation url = "/session/account/home" addtoken="false" >
</cfif>

<cfif errorLogData.RXRESULTCODE NEQ 1>
    <cfcookie name="ERROR_LOG_MESSAGE" value="#errorLogData.MESSAGE#">
    <cflocation url = "/session/errorreports/listerror?existflag=0" addtoken="false" >
</cfif>

<cfif NOT Session.USERID GT 0>
    <cfexit><!---exit if session expired --->
</cfif>
<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
    </cfinvoke>
<cfelse>
    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
        <cfinvokeargument name="userId" value="#session.userId#">
    </cfinvoke>
</cfif>

<cfif NOT permissionStr.havePermission  OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
    OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
    <div style="color:red;">
        You do not have permission to view list
    </div>
    <cfexit>
</cfif>
<!---end check permission --->

<cfoutput>
    <script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
<!---   <script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>--->

    <script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>

    <script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>
</cfoutput>

<!---wrap the page content do not style this--->
<div id="page-content">
  <div class="container" >
        <h2 class="no-margin-top">Error log detail</h2>

<cfset errorDetail = errorLogData.DATA>

        <div id="content-AAU-Admin">
            <div class="clear"></div>

            <!---block 1--->
            <!---<div class="messages-lbl">Campaign Name</div>--->
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">Error message</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.ErrorMessage_txt#</cfoutput>" disabled="disabled"></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">Request URL</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.RequestUrl_txt#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">User Id</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.UserId_int#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">User IP</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.UserIp_vch#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">Error detail</span>
                </div>
                <div class="right-input-area"><textarea disabled="disabled" rows="4" cols="63"><cfoutput>#errorDetail.ErrorDetail_txt#</cfoutput></textarea></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">HTTP User Agent</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.HttpUserAgent_txt#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">HTTP Referer</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#errorDetail.HttpReferer_txt#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">Status</span>
                </div>
                <div class="right-input">
                    <select id="input-status" name="status">
                        <option value="0" <cfif errorDetail.Status_int EQ 0>selected="selected"</cfif>>Open</option>
                        <option value="1" <cfif errorDetail.Status_int EQ 1>selected="selected"</cfif>>In Progess</option>
                        <option value="2" <cfif errorDetail.Status_int EQ 2>selected="selected"</cfif>>Tech Review</option>
                        <option value="3" <cfif errorDetail.Status_int EQ 3>selected="selected"</cfif>>QA Review</option>
                        <option value="4" <cfif errorDetail.Status_int EQ 4>selected="selected"</cfif>>Close</option>
                    </select>
                </div>
            </div>
            <div class="clear"></div>

            <br />
            <div class="message-block">
                <div class="left-input hidden-xs">
                    <span class="em-lbl">Created date</span>
                </div>
                <div class="right-input"><input type="text" value="<cfoutput>#LSDateFormat(errorDetail.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(errorDetail.Created_dt, 'HH:mm:ss')#</cfoutput>" disabled="disabled" /></div>
            </div>
            <div class="clear"></div>

            <br /><br />
            <div class="clear"></div>
            <div id="footer-AAU-Admin" style="margin-bottom: 5em;">
                <button type="button" class="btn btn-primary" onclick="updateErrorStatus()">Update</button>
            </div>
            <div class="clear-fix"></div>
        </div>

        <p class="lead"></p>

        <row>
            <div class="">&nbsp;</div>
        </row>
   </div>
  <!--- /.container --->
</div>
<!--- /#page-content --->

<script type="text/javascript">
$(document).ready(function(){
});
function updateErrorStatus() {
    $.ajax({
        type: "POST",
        url: '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/sire/models/cfc/error_logs.cfc?method=updateErrorStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        dataType: 'json',
        data: {
            status : $('#input-status').val(),
            errid: <cfoutput>#errid#</cfoutput>
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        },
        success: function(data){
            if (data.SUCCESS) {
                window.location.href="/session/errorreports/listerror?isupdatesuccess=1";
            } else {
                location.reload();
            }
        }
    });

    return false;
}
</script>