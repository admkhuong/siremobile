<!--- 
This page contains functions which generate the XML data for the chart.
Effectively, we've separated this part from each CFM page to simulate a
3-tier architecture.
--->

<!--- getSalesByYear function returns the XML for yearly sales figures (including quantity) --->
<cffunction name="getSalesByYear">
	<!--- Get the yearly sales query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT Year(O.OrderDate) As SalesYear, round(SUM(d.quantity*p.UnitPrice),0) As Total, SUM(d.quantity) as Quantity FROM FC_OrderDetails d,FC_Orders o,FC_Products p WHERE o.orderid= d.orderid and d.productid=p.productid GROUP BY Year(O.OrderDate) ORDER BY Year(O.OrderDate)
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Revenue'>">
	<cfset strQtyDS = "<dataset seriesName='Units Sold' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strCat = "#strCat#<category label='#qry.SalesYear#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#' link='" & UrlEncodedFormat("javascript:updateCharts(#qry.SalesYear#)") & "'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getCumulativeSalesByCatXML returns the cumulative sales for each category in a given year --->
<cffunction name="getCumulativeSalesByCatXML">
	<cfargument name="intYear">
	<cfargument name="forDataURL">

	<!--- Get the categories query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT categoryId,categoryName from FC_Categories GROUP BY categoryId,categoryName
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strDataXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strDataXML = "#strDataXML#<dataset seriesName='#XmlFormat(qry.categoryName)#'>">

		<!--- Get monthly sales for the products in the category --->
		<cfquery name="qryMonthly" datasource="#request.dsn#">
			SELECT  Month(o.orderdate) as MonthNum, g.CategoryID, g.CategoryName, round(sum(d.quantity),0) as quantity, SUM(d.quantity*p.Unitprice) As Total FROM FC_categories g,  FC_products p, FC_orders o, FC_OrderDetails d  WHERE year(o.OrderDate)=#intYear# and g.categoryId=#qry.CategoryId# and d.productid= p.productid and g.categoryid= p.categoryid and o.orderid= d.orderid GROUP BY g.CategoryID,g.categoryname,Month(o.orderdate)
		</cfquery>
		
		<!--- Loop over products data and generate xml tags --->
		<cfloop query="qryMonthly">
			<cfif Not IsDefined("catXMLDone")><cfset strCat = "#strCat#<category label='#MonthAsString(qryMonthly.MonthNum)#' />"></cfif>
			<cfset strDataXML = "#strDataXML#<set value='#qryMonthly.Total#' link='" & URLEncodedFormat("javaScript:updateProductChart(#intYear#,#qryMonthly.MonthNum#,#qryMonthly.CategoryId#);") & "'/>">
		</cfloop>

		<cfset strDataXML = "#strDataXML#</dataset>">
		
		<cfset catXMLDone = 1>
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strDataXML#">
</cffunction>

<!--- getSalesByProdXML returns the sales for the products within a category for a given year and month  --->
<cffunction name="getSalesByProdXML">
	<cfargument name="intYear">
	<cfargument name="intMonth">
	<cfargument name="intCatId">
	<cfargument name="forDataURL">

	<!--- Get the sales by product query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT  g.categoryname,p.productname,round(sum(d.quantity),0) as quantity, round(SUM(d.quantity*p.UnitPrice),0) As Total FROM FC_Categories g,  FC_Products p, FC_Orders o, FC_OrderDetails d WHERE year(o.OrderDate)=#intYear# and month(o.OrderDate)=#intMonth# and g.CategoryID=#intCatId# and d.productid= p.productid and g.categoryid= p.categoryid and o.orderid= d.orderid GROUP BY g.categoryname,p.ProductName
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Revenue'>">
	<cfset strQtyDS = "<dataset seriesName='Units Sold' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.ProductName)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getAvgShipTimeXML function returns the delay in average shipping time required to ship an item.  --->
<cffunction name="getAvgShipTimeXML">
	<cfargument name="intYear">
	<cfargument name="numCountries">
	<cfargument name="addJSLinks">

	<!--- Get the avg ship time  sales query --->
	<cfif numCountries Is -1><cfset numCountries = 9999></cfif>
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #numCountries# c.country as Country, Round(AVG(DAY(O.ShippedDate)-DAY(o.requiredDate)),0) As Average FROM FC_Customers c, FC_Orders o WHERE YEAR(o.OrderDate)=#intYear#  and c.customerid=o.customerid GROUP BY c.country ORDER BY AVG(DAY(O.ShippedDate)-DAY(o.requiredDate)) DESC 
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfif addJSLinks>
			<!---
				generate the link
				TRICKY: We're having to escape the " character using chr(34) character.
				In HTML, the data is provided as chart.setXMLData(" - so " is already used and un-terminated
				For each XML attribute, we use '. So ' is used in <set link='
				Now, we've to pass Country Name to JavaScript function, so we've to use chr(34)		
			--->
			<cfset strLink = URLEncodedFormat("javaScript:updateChart(#intYear#,#chr(34)##qry.Country##chr(34)#);")>
			<cfset strXML = "#strXml#<set label='#XMLFormat(qry.Country)#' value='#qry.Average#' link='#strLink#'/>">
		<cfelse>
			<cfset strXML = strXML & "<set label='#XMLFormat(qry.Country)#' value='#qry.Average#'/>">
		</cfif>
	</cfloop>
	
	<!--- Return the full xml --->
	<cfreturn strXml>	
</cffunction>

<!--- getAvgShipTimeCityXML function returns the average shipping time required to ship an item for the cities within the given country  --->
<cffunction name="getAvgShipTimeCityXML">
	<cfargument name="intYear">
	<cfargument name="country">

	<!--- Get the data query --->
	<cfquery name="qry" datasource="#request.dsn#">
		Select ShipCity, Round(AVG(DAY(ShippedDate)-DAY(requiredDate)),0) As Average from FC_Orders WHERE YEAR(OrderDate)=#intYear# and ShipCountry='#country#' GROUP BY ShipCity ORDER BY AVG(DAY(ShippedDate)-DAY(requiredDate)) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strXML = strXML & "<set label='#XMLFormat(qry.ShipCity)#' value='#qry.Average#'/>">
	</cfloop>
	
	<!--- Return the full xml --->
	<cfreturn strXml>	
</cffunction>

<!--- getTopCustomersXML returns the XML data for top customers for the given year. --->
<cffunction name="getTopCustomersXML">
	<cfargument name="intYear">
	<cfargument name="howMany">
		
	<!--- Get the yearly sales query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #howMany# c.companyName as customername, SUM(d.quantity*p.UnitPrice) As Total, SUM(d.Quantity) As Quantity FROM FC_Customers c, FC_OrderDetails d, FC_Orders o, FC_products p WHERE YEAR(OrderDate)=#intYear# and c.customerid=o.customerid and o.orderid=d.orderid and d.productid=p.productid GROUP BY c.CompanyName ORDER BY SUM(d.quantity*p.UnitPrice) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.CustomerName)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getCustByCountry function returns number of customers present in each country in the database.  --->
<cffunction name="getCustByCountry">

	<!--- Get the customer query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT count(CustomerId) AS Num, Country FROM FC_Customers GROUP BY Country order by count(CustomerId) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfset counter = 1>
	<cfloop query="qry">
		<cfif counter Is 1>
			<cfset strXML = strXML & "<set label='#XMLFormat(qry.Country)#' value='#qry.Num#' isSliced='1'/>">
		<cfelse>
			<cfset strXML = strXML & "<set label='#XMLFormat(qry.Country)#' value='#qry.Num#'/>">
		</cfif>	
	</cfloop>
	
	<!--- Return the full xml --->
	<cfreturn strXml>	
</cffunction>

<!--- getSalePerEmp function returns the XML data for sales generated by each employee for the given year  --->
<cffunction name="getSalePerEmpXML">
	<cfargument name="intYear">
	<cfargument name="howMany">
	<cfargument name="slicePies">
	<cfargument name="addJSLinks">
	
	<!--- Get the sales pe employee sales query --->
	<cfif howMany Is -1><cfset howMany = 9999></cfif>
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #howMany# e.lastname, e.EmployeeID, SUM(d.quantity*p.UnitPrice) As Total FROM FC_Employees e,FC_Orders o, FC_OrderDetails d, FC_Products p WHERE YEAR(OrderDate)=#intYear# and e.employeeid= o.employeeid and o.orderid= d.orderid and d.productid=p.productid GROUP BY e.lastname,e.EmployeeID ORDER BY SUM(d.quantity*p.UnitPrice*(1- d.discount)) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfset count = 1>
	<cfloop query="qry">
		<cfif slicePies AND count LT 3><cfset slicedOut = 1><cfelse><cfset slicedOut = 0></cfif>
		<cfif addJSLinks>
			<cfset strLink = URLEncodedFormat("javaScript:updateChart(#qry.employeeID#);")>
			<cfset strXML = strXML & "<set label='#XMLFormat(qry.LastName)#' value='#Int(qry.Total)#' isSliced='#slicedOut#' link='#strLink#'/>">
		<cfelse>
			<cfset strXML = strXML & "<set label='#XMLFormat(qry.LastName)#' value='#Int(qry.Total)#' isSliced='#slicedOut#'/>">
		</cfif>
	</cfloop>
	
	<!--- Return the full xml --->
	<cfreturn strXml>	
</cffunction>

<!--- getSalesByCountryXML function returns the XML Data for sales for a given country in a given year.  --->
<cffunction name="getSalesByCountryXML">
	<cfargument name="intYear">
	<cfargument name="howMany">
	<cfargument name="addJSLinks">

	<!--- Get the sales by country  sales query --->
	<cfif howMany Is -1><cfset howMany = 9999></cfif>
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #howMany# c.country, round(SUM(d.quantity*p.UnitPrice*(1-d.discount)),0) As Total, SUM(d.quantity) as Quantity FROM FC_Customers c, FC_Products p, FC_Orders o, FC_OrderDetails d WHERE YEAR(OrderDate)=#intYear# and d.productid= p.productid and c.customerid= o.customerid and o.orderid= d.orderid GROUP BY c.country ORDER BY SUM(d.quantity*p.UnitPrice*(1- d.discount)) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">

		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.Country)#'/>">	
		<cfif addJSLinks>
			<!---
				generate the link
				TRICKY: We're having to escape the " character using chr(34) character.
				In HTML, the data is provided as chart.setXMLData(" - so " is already used and un-terminated
				For each XML attribute, we use '. So ' is used in <set link='
				Now, we've to pass Country Name to JavaScript function, so we've to use chr(34)		
			--->
			<cfset strLink = URLEncodedFormat("javaScript:updateChart(#intYear#,#chr(34)##qry.Country##chr(34)#);")>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#' link='#strLink#'/>">
		<cfelse>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		</cfif>
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">

	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">
</cffunction>

<!--- getSalesByProdXML returns the sales for the products within a category for a given year and month  --->
<cffunction name="getSalesByCountryCityXML">
	<cfargument name="intYear">
	<cfargument name="country">

	<!--- Get the sales by country city query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT c.city, round(SUM(d.quantity*p.UnitPrice*(1-d.discount)),0) As Total, SUM(d.quantity) as Quantity  FROM FC_Customers c, FC_Products p, FC_Orders o, FC_OrderDetails d WHERE YEAR(OrderDate)=#intYear# and d.productid= p.productid and c.customerid= o.customerid and o.orderid= d.orderid and c.country='#country#' GROUP BY c.city ORDER BY SUM(d.quantity*p.UnitPrice*(1- d.discount)) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.City)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- etSalesByCountryCustomerXML function generates the XML data for sales by customers within the given country, for the given year.  --->
<cffunction name="getSalesByCountryCustomerXML">
	<cfargument name="intYear">
	<cfargument name="country">

	<!--- Get the sales by country city query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT c.companyName as CustomerName, SUM(d.quantity*p.UnitPrice) As Total, SUM(d.Quantity) As Quantity FROM FC_Customers c, FC_OrderDetails d, FC_Orders o, FC_products p WHERE YEAR(OrderDate)=#intYear# and c.customerid=o.customerid and o.orderid=d.orderid and d.productid=p.productid and c.country='#country#' GROUP BY c.CompanyName ORDER BY SUM(d.quantity*p.UnitPrice) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.CustomerName)#' tooltext='#XMLFormat(qry.CustomerName)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getExpensiveProdXML method returns the 10 most expensive products in the database along with the sales quantity of those products for the given year  --->
<cffunction name="getExpensiveProdXML">
	<cfargument name="intYear">
	<cfargument name="howMany">
	<cfargument name="forHTML">

	<!--- Get the sales by country city query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #howMany# p.ProductName, p.UnitPrice, SUM(d.quantity) as Quantity FROM FC_Products p, FC_Orders o, FC_OrderDetails d where YEAR(OrderDate)=#intYear# and d.productid= p.productid and o.orderid= d.orderid group by p.productname,p.UnitPrice  order by p.UnitPrice desc
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfif forHTML>
		<cfset strCat = "#strCat#<category label='#Replace(XMLFormat(qry.ProductName),"'","&apos;","All")#'/>">	
		<cfelse>
		<cfset strCat = "#strCat#<category label='#Replace(HTMLEditFormat(qry.ProductName),"'","%26apos;","All")#'/>">	
		</cfif>
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.UnitPrice#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getInventoryByCatXML function returns the inventory of all items and their respective quantity  --->
<cffunction name="getInventoryByCatXML">
	<cfargument name="addJSLinks">

	<!--- Get the sales by country  sales query --->
	<cfquery name="qry" datasource="#request.dsn#">
		select  c.categoryname,round((sum(p.UnitsInStock)),0) as Quantity, round((sum(p.UnitsInStock*p.UnitPrice)),0) as Total from FC_categories c , FC_products p where c.categoryid=p.categoryid group by c.categoryname order by (sum(p.UnitsInStock*p.UnitPrice)) Desc
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">

		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.CategoryName)#'/>">	
		<cfif addJSLinks>
			<!---
				generate the link
				TRICKY: We're having to escape the " character using chr(34) character.
				In HTML, the data is provided as chart.setXMLData(" - so " is already used and un-terminated
				For each XML attribute, we use '. So ' is used in <set link='
				Now, we've to pass Country Name to JavaScript function, so we've to use chr(34)		
			--->
			<cfset strLink = URLEncodedFormat("javaScript:updateChart(#chr(34)##qry.CategoryName##chr(34)#);")>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#' link='#strLink#'/>">
		<cfelse>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		</cfif>
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">

	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">
</cffunction>

<!--- getInventoryByProdXML function returns the inventory of all items within a given category and their respective quantity  --->
<cffunction name="getInventoryByProdXML">
	<cfargument name="catName">

	<!--- Get the inverntory query --->
	<cfquery name="qry" datasource="#request.dsn#">
		select p.productname,round((sum(p.UnitsInStock)),0) as Quantity , round((sum(p.UnitsInStock*p.UnitPrice)),0) as Total from FC_Categories c , FC_products p where c.categoryid=p.categoryid and c.categoryname='#catName#' group by p.productname having sum(p.UnitsInStock)>0
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Amount'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfif Len(qry.ProductName) GT 8><cfset shortName = Left(qry.ProductName, 8) & "..."><cfelse><cfset shortName = qry.ProductName></cfif>
		<cfset strCat = "#strCat#<category label='#XMLFormat(shortName)#' toolText='#XMLFormat(qry.productName)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getSalesByCityXML function returns the XML Data for sales for all cities in a given year.  --->
<cffunction name="getSalesByCityXML">
	<cfargument name="intYear">
	<cfargument name="howMany">
	
	<!--- Get the sales per city sales query --->
	<cfif howMany Is -1><cfset howMany = 9999></cfif>
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT TOP #howMany# c.city, SUM(d.quantity*p.UnitPrice) As Total FROM FC_Customers c, FC_Products p, FC_Orders o, FC_OrderDetails d WHERE YEAR(OrderDate)=#intYear# and d.productid= p.productid and c.customerid = o.customerid and o.orderid= d.orderid GROUP BY c.city order by SUM(d.quantity*p.UnitPrice) desc
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strXML = "">

	<!--- Loop over the data and generate xml tags --->	
	<cfset count = 1>
	<cfloop query="qry">
		<cfset strXML = strXML & "<set label='#XMLFormat(qry.City)#' value='#Int(qry.Total)#'/>">
	</cfloop>
	
	<!--- Return the full xml --->
	<cfreturn strXml>	
</cffunction>

<!--- getYrlySalesByCatXML function returns the XML Data for sales for a given country in a given year.  --->
<cffunction name="getYrlySalesByCatXML">
	<cfargument name="intYear">
	<cfargument name="addJSLinks">
	
	<!--- Get the yearly sales query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT g.categoryId,g.categoryname,SUM(d.quantity*p.UnitPrice) as Total, SUM(d.quantity) As Quantity FROM FC_categories g, FC_products p, FC_orders o, FC_orderdetails d  WHERE YEAR(OrderDate)=#intYear# and d.productid=p.productid and g.categoryid=p.categoryid and o.orderid=d.orderid GROUP BY g.categoryId,g.categoryname ORDER BY SUM(d.quantity*p.UnitPrice) DESC
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Revenue'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">

		<cfset strCat = "#strCat#<category label='#XMLFormat(qry.CategoryName)#'/>">	
		<cfif addJSLinks>
			<cfset strLink = URLEncodedFormat("javaScript:updateChart(#intYear#,#qry.CategoryID#);")>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#' link='#strLink#'/>">
		<cfelse>
			<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		</cfif>
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">

	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">
</cffunction>

<!--- getSalesByProdCatXML function returns the sales of all items within a given category in a year and their respective quantity  --->
<cffunction name="getSalesByProdCatXML">
	<cfargument name="intYear">
	<cfargument name="catID">
	
	<!--- Get the sales by product category query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT g.categoryname,p.productname,round(sum(d.quantity),0) as quantity, round(SUM(d.quantity*p.UnitPrice),0) As Total FROM FC_Categories g,  FC_Products p, FC_Orders o, FC_OrderDetails d WHERE year(o.OrderDate)=#intYear# and g.CategoryID=#catId# and d.productid= p.productid and g.categoryid= p.categoryid and o.orderid=d.orderid GROUP BY g.categoryname,p.ProductName
	</cfquery>
	
	<!--- Initialize the strings --->
	<cfset strCat = "<categories>">
	<cfset strAmtDS = "<dataset seriesname='Revenue'>">
	<cfset strQtyDS = "<dataset seriesName='Quantity' parentYAxis='S'>">

	<!--- Loop over the data and generate xml tags --->	
	<cfloop query="qry">
		<cfif Len(qry.ProductName) GT 8><cfset shortName = Left(qry.ProductName, 8) & "..."><cfelse><cfset shortName = qry.ProductName></cfif>
		<cfset strCat = "#strCat#<category label='#XMLFormat(shortName)#' toolText='#XMLFormat(qry.ProductName)#'/>">	
		<cfset strAmtDS = "#strAmtDS#<set value='#qry.Total#'/>">
		<cfset strQtyDS = "#strQtyDS#<set value='#qry.Quantity#'/>">
	</cfloop>

	<!--- Close the strings --->
	<cfset strCat = "#strCat#</categories>">	
	<cfset strAmtDS = "#strAmtDS#</dataset>">
	<cfset strQtyDS = "#strQtyDS#</dataset>">
	
	<!--- Return the full xml --->
	<cfreturn "#strCat##strAmtDS##strQtyDS#">	
</cffunction>

<!--- getEmployeeName function returns the name of an employee based on his id.  --->
<cffunction name="getEmployeeName">
	<cfargument name="empID">
	
	<!--- Get the sales by product category query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT FirstName, lastname FROM FC_Employees where EmployeeID=#empId#
	</cfquery>
	
	<cfif qry.RecordCount>
		<cfreturn "#qry.FirstName# #qry.LastName#">
	<cfelse>
		<cfreturn " ">
	</cfif>	
</cffunction>

<!--- getCategoryName function returns the name of a category based on its id.  --->
<cffunction name="getCategoryName">
	<cfargument name="catID">
	
	<!--- Get the sales by product category query --->
	<cfquery name="qry" datasource="#request.dsn#">
		SELECT CategoryName FROM FC_Categories where CategoryId=#catID#
	</cfquery>
	
	<cfif qry.RecordCount>
		<cfreturn "#qry.CategoryName#">
	<cfelse>
		<cfreturn " ">
	</cfif>	
</cffunction>