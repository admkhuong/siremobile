<cflock scope="SESSION" type="EXCLUSIVE" timeout="10">
	<!--- 
	Pallete is a value between 1-5 depending on which
	paletter the user wants to plot the chart with. 
	Here, we just read from Session variable and show it
	In your application, you could read this configuration from your 
	User Configuration Manager, database, or global application settings
	--->
	<cfparam name="Session.palette" default="2">
	<cfset palette = Session.palette>

	<!---
	Whether we've to animate chart. Here, we just read from Session variable and show it
	In your application, you could read this configuration from your 
	User Configuration Manager, database, or global application settings
	--->
	<cfparam name="Session.animation" default="1">
	<cfset animateCharts = Session.animation>
</cflock>

<!---
A color code for caption. Basic idea to use this is to demonstrate how to centralize your cosmetic 
attributes for the chart
--->
<cfset captionFontColor = "666666">