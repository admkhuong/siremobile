


   <!--- If this is a survey - get the first question --->
                <cfif GetResponse.Survey_int EQ 1 AND ISNUMERIC(GetResponse.BatchId_bi) AND IsSurveyInProgressFlag EQ 0 >
                
                	<cfset DebugStr = DebugStr & "Start survey now... #GetResponse.BatchId_bi#">
                
                	<!--- Rare case but... Check if another survey is in progress on this shared short code ... ?warn user another survey is in progress --->
					<!--- Terminate any other outstanding surveys in case there is more than one  --->    
                    <cfif GetSurveyState.RecordCount GT 1>
                    
                        <cfquery name="TerminateSurveyState" datasource="#Session.DBSourceEBM#" >                            
                            UPDATE
                                simplexresults.contactresults
                            SET
                                SMSSurveyState_int = 7                                  
                            WHERE
                                SMSCSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                            AND 
                                SMSSurveyState_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                            AND
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSourceAddress)#"> 
                            AND
                                MasterRXCallDetailId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSurveyState.MasterRXCallDetailId_int#">                                      	            	    	
                        </cfquery>                  
                    
                    </cfif>	                        
                        
                    <!--- Start the survey now! --->
                    
                    <cfset DebugStr = DebugStr & " A.AddContactResult #GetResponse.BatchId_bi# #inpShortCode# #TRIM(inpSourceAddress)# " > 
                    
        	        <!--- Insert the ContactResult Record --->   
                    <cfinvoke method="AddContactResult" component="#Session.SessionCFCPath#.csc.CSC" returnvariable="RetVarStartSMSSurvey">
                        <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#">
                        <cfinvokeargument name="inpContactString" value="#TRIM(inpSourceAddress)#">
                        <cfinvokeargument name="inpResultString" value="<LASTQ QID='1' />">
                        <cfinvokeargument name="inpSMSResult" value=100>
                        <cfinvokeargument name="inpSMSSurveyState" value=1>
                        <cfinvokeargument name="inpDBSourceEBM" value="#inpDBSourceEBM#"> 
                    </cfinvoke>
                    
                    <cfset DebugStr = DebugStr & " RetVarStartSMSSurvey.RXRESULTCODE = #RetVarStartSMSSurvey.RXRESULTCODE# " > 
                 
                 
                    <cfif RetVarStartSMSSurvey.RXRESULTCODE LT 0>                    
                        <cfthrow MESSAGE="#RetVarStartSMSSurvey.MESSAGE#" TYPE="Any" detail="#RetVarStartSMSSurvey.ERRMESSAGE#" errorcode="-2">                   
                    </cfif> 
                                    
                	<!--- Response always starts at question 1 for SMS surveys --->
               				
                    <cfset DebugStr = DebugStr & " A.ReadQuestionDataById">        
                            			                    
                    <cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataById">
                        <cfinvokeargument name="INPBATCHID" value="#GetResponse.BatchId_bi#">
                        <cfinvokeargument name="inpQID" value="1">
                        <cfinvokeargument name="inpIDKey" value="RQ">
                        <cfinvokeargument name="inpDBSourceEBM" value="#inpDBSourceEBM#"> 
                    </cfinvoke>
                    
                   	<cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION) GT 0>
                    	
						<cfif arrayLen(RetVarReadQuestionDataById.ARRAYQUESTION[1]) GT 0>
                                            
                            <cfset GetResponse.Response_vch = "#RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TEXT#">
                           
                            <cfset ResponseType = RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TYPE> 
                           
                            <!--- If next question is a branch - calculate where to go next ---> 
							<cfif RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TYPE EQ "ONESELECTION"
                                  OR 
                                  RetVarReadQuestionDataById.ARRAYQUESTION[1][1].TYPE EQ "SHORTANSWER"	
                            >                                  
                                <cfloop array="#RetVarReadQuestionDataById.ARRAYQUESTION[1][1].ANSWERS#" index="CurrAnswer">
                                                                                 
                                    <cfswitch expression="#RetVarReadQuestionDataById.ARRAYQUESTION[1][1].AnswerFormat#"> 
                                        
                                        <cfcase value="NOFORMAT">
                                            <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                        </cfcase>
                                        
                                        <cfcase value="NUMERIC">
                                            <cfif !IsNumeric(CurrAnswer.TEXT) AND CurrAnswer.TEXT DOES NOT CONTAIN('other') >
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.ID# for #CurrAnswer.TEXT#"> 
                                            <cfelse>                                            
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                            </cfif>
                                        </cfcase> 
                                        
                                        <cfcase value="NUMERICPAR">
                                            <cfif !IsNumeric(CurrAnswer.TEXT) AND CurrAnswer.TEXT DOES NOT CONTAIN('other') >
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.ID#) #CurrAnswer.TEXT#"> 
                                            <cfelse>                                            
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                            </cfif>
                                        </cfcase>                                                
                                        
                                        <cfcase value="ALPHA">
                                            <cfif !IsNumeric(CurrAnswer.TEXT) AND CurrAnswer.TEXT DOES NOT CONTAIN('other') >
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CHR(CurrAnswer.ID + 64)# for #CurrAnswer.TEXT#"> 
                                            <cfelse>                                            
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                            </cfif>
                                        </cfcase>   
                                        
                                        <cfcase value="ALPHAPAR">
                                            <cfif !IsNumeric(CurrAnswer.TEXT) AND CurrAnswer.TEXT DOES NOT CONTAIN('other') >
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CHR(CurrAnswer.ID + 64)#) #CurrAnswer.TEXT#"> 
                                            <cfelse>                                            
                                                <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                            </cfif>
                                        </cfcase> 
                                        
                                        <cfdefaultcase>
                                            <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.TEXT#"> 
                                        </cfdefaultcase>                                               
                                
                                    </cfswitch>
                                                                                                    
                                </cfloop>
                            
                            <cfelse>            
                                <cfloop array="#RetVarReadQuestionDataById.ARRAYQUESTION[1][1].ANSWERS#" index="CurrAnswer">
                                    <cfset GetResponse.Response_vch = GetResponse.Response_vch & "#inpNewLine##CurrAnswer.ID# #CurrAnswer.TEXT#"> 
                                </cfloop>
                			</cfif>
                                        
                		<cfelse>
                        	
                            <cfset DebugStr = DebugStr & " No Start question found"> 
                        	
							<!--- Default to no response on errors --->
                        	<cfset GetResponse.Response_vch = "">
                        
                        </cfif>
                        
                    <cfelse>
                        
                         <cfset DebugStr = DebugStr & " No Start question found"> 
                        	
							<!--- Default to no response on errors --->
                        	<cfset GetResponse.Response_vch = "">
                
                    </cfif>
            
                </cfif> <!--- If this is a survey - get the first question --->
                
                

















<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h1>Notes:</h1>
<p>A unique &quot;Authenticated&quot; user will have one entry in the ContactList table.  Each &quot;unauthenticated&quot; user is treated as a new ContactList entry. Each Phone, SMS, or email will be stored in the ContactString Table linked to a ContactList entry. Each group is a unique entry in the grouplist table. Each group is linked to one user. Each ContactString is linked to a group through the GroupContactList table.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>ContactAddressId_bi in simplelists.contactstring is the main unique key for every contact. use this to group off of.</p>
<p>	simplelists.rxmultilist <br />
  is mostly replaced by<br />
  simplelists.groupcontactlist <br />
  INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi<br />
  INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi<br />
  <br />
</p>
<p>&nbsp;</p>
<p>r.UserSpecifiedData_vch is replaced by simplelists.contactstring.contactstring_vch</p>
<p>r.TimeZone_int is replaced by simplelists.contactstring.timezone_int</p>
<p>r.ContactTypeId_int is replaced by simplelists.contactstring.contacttypeid_int</p>
<p>r.OptInFlag_int is replaced by simplelists.contactstring.optin_int</p>
<p>r.ContactTypeId_int is replaced by simplelists.contactstring.contacttype_int</p>
<p>r.UserId_int is replaced by simplelists.contactlist.userid_int</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Replace for groups</p>
<p>simplelists.simplephonelistgroups with simplelists.grouplist<br />
s.DESC_VCH with s.GroupName_vch<br />
s.GroupId_int with s.GroupId_bi</p>
<p>Get rid of and replace <br />
AND GroupId_bi &gt; 4</p>
<p>&nbsp;</p>
<p>Remove old references to simplelists.cfc - everything should be MultiLists2 now </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>The concept of an &quot;ALL&quot; list is dead - no contact can exist not in a group - at least not accesible</p>
<p>&nbsp;</p>
<p>To get a users custom variables</p>
<p>&nbsp;</p>
<p> &lt;cfquery name=&quot;getudvars&quot; datasource=&quot;#Session.DBSourceEBM#&quot;&gt;<br />
select <br />
Distinct(VariableName_vch)<br />
from <br />
simplelists.contactvariable<br />
INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi <br />
where<br />
simplelists.contactlist.userid_int = &lt;cfqueryparam cfsqltype=&quot;cf_sql_integer&quot; value=&quot;#Session.UserId#&quot;&gt;<br />
&lt;/cfquery&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h1>Tasks:</h1>
<p>Need new structure(s) for DNC lists</p>
<p>Need to be able to reference possible custom data fields in MCID objects</p>
<p>Need editor for custom fields</p>
<p>Need list of possible custom data field choices</p>
<p>Add UserSpecifiedData_vch as standard field for RXDialer compatibility</p>
<p>Add FileSeqNumber_int as standard field for RXDialer compatibility</p>
<p>Tables like Con tact List return javascript error when no data is found based on filters</p>
<p>&nbsp;</p>
<p> &lt;!--- Exclude LOCALOUTPUT DNC ---&gt; &lt;!--- LOCALOUTPUT DNC - Use AND instead of OR to exclude all cases ---&gt;<br />
AND<br />
( grouplist_vch NOT LIKE '%,1,%' AND grouplist_vch NOT LIKE '1,%' ) <br />
</p>
<p>Allow EBM user to add their own custom fields without uploading new data.</p>
<p>&nbsp;</p>
<p>In filtered list of contacts - allow add all to group - not the goofy multiselct in html but filter based group add</p>
<p>Remove old references to simplelists.cfc - everything should be MultiLists2 now</p>
<p>&nbsp;</p>
<p>When adding contacts to a group under a centralized company account make sure the list is the company user id not the current logged in user. </p>
<h1>CPP Tasks:</h1>
<p>Authentication fields<br />
</p>
<p>add string</p>
<p>&nbsp; </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>