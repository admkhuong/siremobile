Manh,

I still think you or the team is not understanding some basic SMS stuff.
It is impossible to work on until you do understand these basics. Each
Note below contains a critical concept you need to fully understand in
order to work on Agent stuff. Please add below to any working documents
you have on this project.


Note:
Ebmresponse/sms/resources is a black box as far as the Seta team is
concerned. I do not want anyone messing with this code. OK to read it to
understand if you want, but no changes. Different carriers respond with
different code structures but they all end up triggering the same csc.cfc
logic. There is no need for you to waste time understanding all the
different carrier structures. The architecture of the EBM handles this for
you.


Note:
The SMS QA tool will only insert to simplequeue.moinboundqueue if an
Interval (Interval is a special Control Point (CP) type) is triggered.
Carrier MOs are different and will be logged in
simplequeue.moinboundqueue, but there is nothing you need to directly do
here.

Note:
url:
'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMS
Response
Wether it is live or the QA tool an MO will always trigger a call to the
csc.cfc.  This trigger is called via QA tool or inbound MO and basically
posts the MO content to a method that will process what needs to be done
next. This method also logs all events as needed.

Note:
url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc
includes several SMS MO/MT methods in other organizational files.

<cfinclude template="inc_smsdelivery.cfm">
	<cfinclude template="inc_ext_csc.cfm">

Note:
Any code needing to be modified in csc.cfc leave to me. There is a stub
already for agents at line 4046 in inc_ext_csc.cfm. I am just waiting for
Seta team to tell me what other CFC to call and what I will need to send.
I assume I will send
	Device Address (Phone Number)
	Message Sent
	Time Stamp
	EBM - Session Id






Note:
Session states are very complicated - There are a variety of pre-built
methods for updating states. Again Seta does not need to modify these - I
do not whan misunderstandings breaking stuff.
The state of a device address (SMS Phone number) is stored in
simplexresults.contactresults as a record that is updated until the state
is complete.
Only one session per device address per short code at any one time.
Each running session is tied to a BatchId - The BatchId has the
XMLControlString that is defining how the session flows over various
Control Points (CP)


Note:
Every MT will also have an entry in the simplexresults.contactresults
table. CallResult_int will always be 76 for SMS MT results.

Note:
Every Session will have an entry in the simplexresults.contactresults
table. Special entry for Session States - SMSResult_int = 100 and
SMSSurveyState_int is variable
MasterRXCallDetailid_int from the simplexresults.contactresults table is
the unique Id of any running session.


Possible Survey States -
<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/constants.cfm

<!--- Contact Results SMS Result Status--->
<cfset SMSSURVEYSTATE_NOTASURVEY = 0>
<cfset SMSSURVEYSTATE_RUNNING = 1>
<cfset SMSSURVEYSTATE_INTERVAL_HOLD = 2>
<cfset SMSSURVEYSTATE_RESPONSEINTERVAL = 3>
<cfset SMSSURVEYSTATE_COMPLETE = 4>
<cfset SMSSURVEYSTATE_CANCELLED = 5>
<cfset SMSSURVEYSTATE_EXPIRED = 6>
<cfset SMSSURVEYSTATE_TERMINATED = 7>
<cfset SMSSURVEYSTATE_STOP = 8>





Note:
While a session is running, every MO will be in response to a specific
Control Point (CP). The CP can be a question, branch, interval, etc. One
of the options is Agent Assist. The GetSMSResponse method is already
programmed to handle all the possible responses.


Note:
When an MO comes in:
What I know from the request -
	Device Address (Phone Number)
	Message Sent
	Time Stamp

What I can store and look up - EBM is the one storing session states in
the DB
	Is there an existing session running - if not start one - What
Interactive Campaign Structure (BatchId) am I currently running
	What control point Device Address is on
	What control point type Device Address is on
	

Note:
I assume there is a working doc of features required somewhere else
already but as a refresher of basicsŠ

For Agent Assist SMS - A variety of things can happen.
An end user will start a agent session based on keyword. The keyword can
be anything as defined in EBM interactive SMS.
The EBM will auto respond - <cfset LocalResponse = "Agent Assisted Request
is pendingŠ">
When a keyword is sent that triggers an Agent session, the Control Point
is on AGENT, then I need to forward the message sent to the Agent Assist
area

The agent will respond or decline or transfer. IF responding the Agent
will send an MT to the user.
The user gets the MT. The user may or may not respond further at this
point. IF the user responds further than another MO is sent.
When an MO is sent and the Control Point is still on AGENT, then I need to
forward the message sent to the Agent Assist area
The agent assist area will capture and log this information for
presentation to the agents
I will then loop in the agent assist session state until conversation is
either terminated by User or terminated by the Agent

A new session can always be started based on keyword.

The SMS MO with with a keyword setup to trigger Agent system will start a
new session.
The SMS MO will forward requests to Agent system
The SMS MO will maintain agent session until terminated
The user can terminate session - STOP request sent via MO

All responses are logged. New Agent tool log. Agent log should link to
existing EBM SMS Session Id - MasterRXCallDetailid_int from the
simplexresults.contactresults table is the unique Id of any running
session.


The Agent system will respond with MTs
The Agent system can terminate sessions - there are methods in the
existing csc.cfc to handle this.
The Agent can respond with typed entry or canned entries - MT - there are
methods in the existing csc.cfc to handle this. For the QA tool you need a
socket method to push to.
Note:
For a real device you just send an MT. NO real MTs until I see it working
in QA tool first.







If you want to test MO¹s not using the QA tool then you need to get a
north american Google Voice phone number and then use with SMS.
https://support.google.com/voice/answer/168516?hl=en


Live EBM short codes can broadcast to Google Voice SMS numbers.






Jeffery Lee Peterson


Intelligent, Adaptable, Customizable Messaging and Notification
MessageBroadcast
4685 MacArthur Ct., Ste 250, Newport Beach CA  92660
www.messagebroadcast.com <http://www.messagebroadcast.com/>
Office: 949-428-4899
Mobile: 949-400-0553
email: jpeterson@messagebroadcast.com






On 10/24/14, 2:57 AM, "manhhd" <manh.hoang@setacinq.vn> wrote:

Hi Jeff,

I tested follow the steps that you shown me via Webex (I copied your xml
controltring and applied to my local campaign). Then I selected data in
simplequeue.moinboundqueue, no data was there.

Does EBM public a webservice that Mblox call to save MO message to
database?
Does webservice put in folder "ebmresponse/sms/resources"? and service
for each carrier in sub categories in it (for example: response255835
for ALLTEL)?
Can we have an environment for testing similar with your system? Or at
least we want to know more about exactly db structure for MO, MT message.
and what is different between a MO message comes from QA tool and from
Real network?

We need a skype meeting for this discussion soon. I want whole
Vietnamese team can join it. It's fine for us if you are able to join at
9h am Monday morning (7h pm pst).


Best,
Manh

