<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Batch</h1>
<p>&nbsp;</p>
<p>See Session/cfc/ Batch.cfc</p>
<p>and</p>
<p>Session/cfc/Distribution.cfc</p>
<p>&nbsp;</p>
<p>The Batch is a container for campaign content decsiptors (XMLControlString_vch)</p>
<p>The BatchId_bi field is aunique accross the entire system.</p>
<p>Access to the Batch is limited to authenticated users.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>CREATE TABLE `batch` (<br />
`BatchId_bi` int(11) unsigned NOT NULL AUTO_INCREMENT,<br />
`UserId_int` int(11) DEFAULT NULL,<br />
`rxdsLibrary_int` int(11) NOT NULL DEFAULT '0',<br />
`rxdsElement_int` int(11) NOT NULL DEFAULT '0',<br />
`rxdsScript_int` int(11) NOT NULL DEFAULT '0',<br />
`UserBatchNumber_int` int(11) DEFAULT NULL,<br />
`Created_dt` datetime NOT NULL,<br />
`Desc_vch` varchar(255) DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
`GroupId_int` int(11) DEFAULT NULL,<br />
`AltSysId_vch` varchar(255) DEFAULT NULL,<br />
`XMLControlString_vch` text,<br />
`Active_int` int(11) DEFAULT NULL,<br />
`ServicePassword_vch` varchar(255) DEFAULT NULL,<br />
`AllowDuplicates_ti` int(4) NOT NULL DEFAULT '0',<br />
PRIMARY KEY (`BatchId_bi`),<br />
UNIQUE KEY `UC_BatchId_bi` (`BatchId_bi`),<br />
KEY `IDX_UserId_int` (`UserId_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=latin1$$<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>