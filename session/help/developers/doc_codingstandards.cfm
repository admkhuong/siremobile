<h1>Coding Standards:
  
  
  </h1>
<h1>&nbsp;</h1>
<p>&nbsp;</p>
<p>JLP is the arbitrator of what is acceptable and what is not. I'm not perfect so if you see violations of my own standards feel free to either a) fix or b) point out as you go along.</p>
<p>If I can read it OK then its good. When in doubt ask.</p>
<p>&nbsp;</p>
<p>All major code sections should be commented so that other developers know what you are thinking - or in the case of maintaining older code what you were thinking in the first place.</p>
<p>I prefer that code is indented to match each section – liberal use of open and close brackets – even on conditional logic with one line of code.
  <br />
  Code brackets should match character positions on a line. Open and close brackets should be on a line by themselves – NOT open bracket on same line as starting command like some people use.<br />
</p>
<p>Minor sections such as javascript methods and if blocks need to be separated by one blank line.<br />
    Major Sections such as functions within a CFC shall be seperated by two blank lines. Comments describing function as well as hints for each input shall be defined.</p>
<p><br />
</p>
<h3>File Names:</h3>
<p>In the intertest of linux compatiablity we all need to start nameing files in all lower case.</p>
<p>&nbsp;</p>
<h3>URL Parameters:</h3>
<p>In all new calls that need to pass URL parameters please use use post_to_url method I added to header.</p>
<p>Also as time permits replace old calls with this new logic.</p>
<p>OLD STYLE<br />
  window.location = &quot;&lt;cfoutput&gt;#rootUrl#/#SessionPath#&lt;/cfoutput&gt;/campaign/launchCampaign.cfm?inpbatchid=&quot; + INPBATCHID;<br />
</p>
<p>NEW STYLE<br />
  &lt;cfoutput&gt;<br />
  var params = {'INPBATCHID': INPBATCHID}; <br />
  post_to_url('#rootUrl#/#SessionPath#/reporting/recordedresponses/listrecordings.cfm', params, 'POST'); <br />
  &lt;/cfoutput&gt;<br />
</p>
<p>&nbsp;</p>
<h3> SQL    </h3>
<p>  I like my SQL to be consistant and easy to read – see sample
  All Queries will reference both the DB and the table name in dot(.) notation and not rely on default DB in data source definition.  </p>
<p>All SQL will be indented like sample
  All SQL commands will be in all CAPS
  All user input to DB needs to be wrapped in  
  &lt;cfqueryparams&gt; - no SQL injection holes.
<p>I used to manually replace single quotes with double quotes but now rely on
&lt;cfqueryparams&gt;  mixing old and new code might break XML strings with too many quotes.
<p> All SQL queries should load specific fields - no SELECT * queries please.
<p> Sample:<br />
&lt;cfquery name=&quot;getDistinctBatchIdsDupeProcessingFlag&quot; datasource=&quot;#Session.DBSourceEBM#&quot;&gt; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AllowDuplicates_ti<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;simpleobjects.batch<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BatchId_bi = &lt;CFQUERYPARAM CFSQLTYPE=&quot;CF_SQL_INTEGER&quot; VALUE=&quot;#INPBATCHID#&quot;&gt;<br />
&lt;/cfquery&gt; <br />
</p>


  
  As many queries (All of them) as possible should be in the CFC objects and acceseed through AJAX OR CFINVOKE.
<p> If we need to convert DB's (say go to Microsoft SQL vs mySQL) down the road, all the data calls are centralized.
<p>
<p> Note CF8 vs CF9 – CF9 auto converts all JSON return values to upper case. Sites written in CF8 expecting mix of upper and lower case will break.
<p> All CFC methods will need to add Session UID checks and error out accordingly.
<p> Remove all console.log calls in release versions – good for debugging but still breaks IE.
<p> I used to use .json JQuery calls but now prefer .ajax for better error handling and better clarity of parameters.
  
  All .ajax should use 'POST' option as opposed to 'GET'.
<p> Compress / Compact production JS when complete.
<p> Pages that begin with dsp_ use the site wide display templates as defined in the application.cfc.<br />
<p> Main DBSource is DBSourceEBM default="Bishop"</p>
<p></p>
<p><strong>Line Spacing</strong> - <br />
  Simgle space if commands between indented elelments are only one
<pre>&lt;cfif ArrayLen(genEleArr) GT 0 &gt;<br />	&lt;cfset XmlAppend( genEleArr[1] [1],generatedElementEmail) /&gt;<br />&lt;cfelse&gt;<br />    &lt;cfthrow MESSAGE=&quot;Invalid Control Point Specified&quot; TYPE=&quot;Any&quot; detail=&quot;&quot; errorcode=&quot;-2&quot;&gt;
&lt;/cfif&gt;
</pre>
</p>
Blank Space if there are more than one commands between elements
<pre>	&lt;cfif ArrayLen(xmlRxssSMSElement) GT 0 &gt;<br />							<br />		&lt;!--- Just add node ---&gt;<br />      	&lt;cfset inpCPGXML = Replace(inpCPGXML, &quot;&amp;&quot;,&quot;&amp;amp;&quot;,&quot;ALL&quot;) /&gt;<br />       &lt;cfset genEleArr = XmlSearch(inpCPGXML, &quot;/*&quot;) /&gt;<br />                           <br />   &lt;/cfif&gt;</pre>
</p>
<BR />
<h3>Exception Handleing</h3>
<p> All code blocks will be within either local exception handelers or within global exception handelers.
  
  In ColdFusion this is done within the CFTRY and CFCATCH Tags
  
  Liberal chaecking for bad parameters should use CFTHROW to exit gracefully.
  
  The end user should NEVER see generic ColdFusion server errors.
<p>
<p>
<p>When using a JavaScript switch statement on  a variable returned from an ajax call to a cfc - always surround variable with  parseInt()<br />
example: switch(d.DATA.CURRRXT[0]) becomes  switch(parseInt(d.DATA.CURRRXT[0]))
<BR />
<BR />
<h3>Bloatware  </h3>
<p>Team members should ensure that they do not bring in  any bloat that is not used.
<p>an enitre js library just for one function is not efficient. If there is intended future use - great, but if not just copt the one function out of it.<br />
<BR />
<BR />
<h3>System Maintenance Check</h3>
<p>On all systems login pages (or other pages of critical nature such as payment systems), please add the following check to your logic.</p>
<p>If the value of systemup_int is 0 or unavailable then redirect user from login page to page displaying down for maintenance.</p>
<p>SELECT
  <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;systemup_int<br />
  FROM<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;simpleobjects.maintenance<br />
  WHERE<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mid_int = 1</p>
<p></p>
<h3></h3>
<h3>Form inputs:</h3>
All form inputs that are passed to DB or File system need to be processed for special charaters so user can pretty much type anything without causing crashes and ugly error messages
<p></p>
<p></p>
