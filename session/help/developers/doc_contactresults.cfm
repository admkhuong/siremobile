<style type="text/css">
.warning {
	color: #F00;
}
</style>
<p class="warning">&nbsp;</p>
<p>New Call Result Numbers<br />
BR Range</p>
<p>Call Results and codes -Most codes get grouped into OTHER catagory - otherwise it might be TMI - too much information<br />
  -3	Problem with line routing<br />
  -2	Result Not Set Never got a result<br />
  -1	Severe Error<br />
  1	Cadence Break<br />
  2	Loop Current - Analog only<br />
  3	Positive Voice Detection<br />
  4	Positive Answering Machine Detected<br />
  5	Unknown But Assume Live<br />
  6	CO Intercept<br />
  7	Busy<br />
  8	Call Error<br />
  9	Fax Machine<br />
  10	No Answer<br />
  11	No Dialtone<br />
  12	No Ringback<br />
  13	Channel Stopped<br />
  14	Ring time + AnsMachine Message exceeded 90 seconds<br />
  15	Unable to allocate resource<br />
  20	MakeCall Failed<br />
  21	Failed media Detect<br />
  22	Failed Set Event Mask<br />
  23	Failed GCEV_ERROR While waiting for connection<br />
  24	Disconnected for unknown reson<br />
  25	Call disconnected by remote network<br />
  26	GCEV_ERROR<br />
  27	GCEV_TASKFAIL<br />
  28	Old style default<br />
  40	Answered but real quick hangup<br />
  42	CO Intercept <br />
  45	Invalid GCRN - makecall must have failed<br />
  46	Legacy Other<br />
  47	CO Intercept Type II<br />
  48	No Answer Type II<br />
  49	Should Not Get Here - Other<br />
  50	ISDN Connected but failure to get more information <br />
  -50	Invalid XML<br />
  --- Legacy 75	Invalid AE Combo - unable to get Time Zone information<br />
  76	SMS <br />
  75  email
</p>
<p>500 - <br />
  501 - Duplicate detected during distribution - already distributed for this batch<br />
  502 - Duplicate based on UUID<br />
  503 - Bad XML Prior to Distribution<br />
</p>
<p>2000 - CPP contact Insert<br />
  2001 - CPP Contact Inert - Already Existed
</p>
<p>&nbsp;</p>
<p><br />
delimiter $$</p>
<p>    CREATE TABLE simplexresults.`contactresults` (<br />
`MasterRXCallDetailId_int` bigint(11) NOT NULL AUTO_INCREMENT,<br />
`RXCallDetailId_int` bigint(11) NOT NULL,<br />
`CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',<br />
`DTSID_int` int(10) unsigned DEFAULT NULL,<br />
`BatchId_bi` bigint(20) DEFAULT NULL,<br />
`PhoneId_int` int(11) DEFAULT NULL,<br />
`TotalObjectTime_int` int(11) DEFAULT NULL,<br />
`TotalCallTimeLiveTransfer_int` int(11) DEFAULT NULL,<br />
`TotalCallTime_int` int(11) DEFAULT NULL,<br />
`TotalConnectTime_int` int(11) DEFAULT '0',<br />
`ReplayTotalCallTime_int` int(11) DEFAULT NULL,<br />
`SixSecondBilling_int` int(11) DEFAULT NULL,<br />
`SystemBilling_int` int(11) DEFAULT NULL,<br />
`TotalAnswerTime_int` int(11) DEFAULT NULL,<br />
`CallResult_int` int(11) DEFAULT NULL,<br />
`RedialNumber_int` int(11) DEFAULT NULL,<br />
`MessageDelivered_si` smallint(6) DEFAULT NULL,<br />
`SingleResponseSurvey_si` smallint(6) DEFAULT NULL,<br />
`UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,<br />
`NumberOfHoursToRescheduleRedial_si` smallint(6) DEFAULT NULL,<br />
`TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,<br />
`TransferStatusId_ti` tinyint(4) DEFAULT NULL,<br />
`IsHangUpDetected_ti` tinyint(4) DEFAULT NULL,<br />
`IsOptOut_ti` tinyint(4) DEFAULT NULL,<br />
`IsOptIn_ti` tinyint(4) DEFAULT '0',<br />
`IsMaxRedialsReached_ti` tinyint(4) DEFAULT NULL,<br />
`IsRescheduled_ti` tinyint(4) DEFAULT NULL,<br />
`RXCDLStartTime_dt` datetime DEFAULT NULL,<br />
`CallStartTime_dt` datetime DEFAULT NULL,<br />
`CallEndTime_dt` datetime DEFAULT NULL,<br />
`CallStartTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
`CallEndTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
`CallResultTS_dt` datetime DEFAULT NULL,<br />
`PlayFileStartTime_dt` datetime DEFAULT NULL,<br />
`PlayFileEndTime_dt` datetime DEFAULT NULL,<br />
`HangUpDetectedTS_dt` datetime DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
`DialerName_vch` varchar(255) DEFAULT NULL,<br />
`DialerIP_vch` varchar(50) DEFAULT NULL,<br />
`CurrTS_vch` varchar(255) DEFAULT NULL,<br />
`CurrVoice_vch` varchar(255) DEFAULT NULL,<br />
`CurrTSLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
`CurrVoiceLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
`CurrCDP_vch` varchar(255) DEFAULT NULL,<br />
`CurrCDPLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
`MsgReference_vch` varchar(1000) DEFAULT NULL,<br />
`Reason_vch` varchar(1000) DEFAULT NULL,<br />
`SMSType_ti` tinyint(4) DEFAULT NULL,<br />
`ContactTypeId_int` int(11) DEFAULT NULL,<br />
`XMLResultStr_vch` text,<br />
`XMLControlString_vch` longtext,<br />
`FileSeqNumber_int` int(11) DEFAULT NULL,<br />
`UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,<br />
`ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',<br />
`dial6_vch` varchar(10) DEFAULT NULL,<br />
`MainMessageLengthSeconds_int` int(11) DEFAULT '0',<br />
`RecordedResultPulled_int` int(1) NOT NULL DEFAULT '0',<br />
`RecordedResponseProcessingStart_dt` datetime DEFAULT NULL,<br />
`RecordedResponseProcessingComplete_dt` datetime DEFAULT NULL,<br />
`Status_ti` tinyint(4) DEFAULT NULL,<br />
`Time_dt` datetime DEFAULT NULL,<br />
`SMSResult_int` int(4) DEFAULT '0',<br />
`SMSSurveyState_int` int(4) DEFAULT '0',<br />
`SMSSequence_ti` tinyint(4) DEFAULT '0',<br />
`SMSCSC_vch` varchar(255) DEFAULT NULL,<br />
`SMSTrackingOne_bi` bigint(20) DEFAULT '0',<br />
`SMSTrackingTwo_bi` bigint(20) DEFAULT '0',<br />
`SMSMTPostResultCode_vch` varchar(255) DEFAULT '0',<br />
`APIRequestJSON_vch` varchar(2048) DEFAULT NULL,<br />
`ControlKey_int` int(11) DEFAULT '0',<br />
PRIMARY KEY (`MasterRXCallDetailId_int`),<br />
KEY `XPKRXCallDetails` (`MasterRXCallDetailId_int`),<br />
KEY `IDX_ContactString_vch` (`ContactString_vch`),<br />
KEY `IDX_DTSID_int` (`DTSID_int`),<br />
KEY `IDX_CallResult_int` (`CallResult_int`),<br />
KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),<br />
KEY `IDX_BatchId_bi` (`BatchId_bi`),<br />
KEY `IDX_Dial6` (`dial6_vch`),<br />
KEY `IDX_RecordedResultPulled_int` (`RecordedResultPulled_int`),<br />
KEY `IDX_CDLDate` (`RXCDLStartTime_dt`),<br />
KEY `IDX_Combo_Date_CSC_SMSResult` (`RXCDLStartTime_dt`,`SMSResult_int`,`SMSCSC_vch`),<br />
KEY `IDX_COMBO_dt_cs_batchId` (`BatchId_bi`,`RXCDLStartTime_dt`,`ContactString_vch`),<br />
KEY `IDX_COMBO_contactstring_smssurveystate` (`ContactString_vch`,`SMSSurveyState_int`),<br />
KEY `IDX_COMBO_contactstring_smssurveystate_shortcode_batch` (`ContactString_vch`,`SMSSurveyState_int`,`SMSCSC_vch`,`BatchId_bi`),<br />
KEY `IDX_Combo_CSC_SurveyState` (`SMSCSC_vch`,`SMSSurveyState_int`),<br />
KEY `IDX_batchid_CS_ST_RS` (`BatchId_bi`,`RXCDLStartTime_dt`),<br />
KEY `IDX_BATCH_CDLDT` (`RXCDLStartTime_dt`,`BatchId_bi`),<br />
KEY `IDX_CDLDT_BATCH_CALLRESULT` (`RXCDLStartTime_dt`,`BatchId_bi`,`CallResult_int`),<br />
KEY `IDX_COMBO_CS_CDLDT_BATCH_CALLRESULT` (`RXCDLStartTime_dt`,`BatchId_bi`,`CallResult_int`,`ContactString_vch`),<br />
KEY `IDX_Combo_CDID_CDLDT_SState_Status_Batch` (`MasterRXCallDetailId_int`,`RXCDLStartTime_dt`,`SMSSurveyState_int`,`Status_ti`,`BatchId_bi`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=587382 DEFAULT CHARSET=latin1;</p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
    simplexresults.contactresults` (<br />
`MasterRXCallDetailId_int` bigint(11) NOT NULL AUTO_INCREMENT, - PK for table<br />
`RXCallDetailId_int` bigint(11) NOT NULL, - PK for remote table if this was imported from a fulfillment device<br />
`CallDetailsStatusId_ti` tinyint(4) DEFAULT '0', - Flag for further processing - used for things like audio response review<br />
`DTSID_int` int(10) unsigned DEFAULT NULL,Fulfillments DTS id<br />
`BatchId_bi` bigint(20) DEFAULT NULL, - the batch<br />
`PhoneId_int` int(11) DEFAULT NULL, - ignored - legacy<br />
`TotalObjectTime_int` int(11) DEFAULT NULL, total time in milliseconds it took to fulfill this message - includes time it takes to build audio concatenation prior to call<br />
`TotalCallTimeLiveTransfer_int` int(11) DEFAULT NULL, - Live Agent Tracking time<br />
`TotalCallTime_int` int(11) DEFAULT NULL, Total time the voice call took to make<br />
`TotalConnectTime_int` int(11) DEFAULT '0', total time it took for call after connection is made - does not include preconnect stuff like ring time<br />
`ReplayTotalCallTime_int` int(11) DEFAULT NULL, - if there is a option for a user to repeat a single messge, this tracks how long the repeats were - legacy for the most part<br />
`SixSecondBilling_int` int(11) DEFAULT NULL, Rounds total connect time to nearest 6 second increment - standard telecomm practice<br />
`SystemBilling_int` int(11) DEFAULT NULL, Total time billable - <br />
`TotalAnswerTime_int` int(11) DEFAULT NULL,How long from call initiialization to the actual connect time.<br />
`CallResult_int` int(11) DEFAULT NULL, see table above of actual results <br />
`RedialNumber_int` int(11) DEFAULT NULL, - how many attempts to deliver message<br />
`MessageDelivered_si` smallint(6) DEFAULT NULL, - was message delivered. No answres, CO0Intercepts, and Busy among other things would cause this to be 0<br />
`SingleResponseSurvey_si` smallint(6) DEFAULT NULL, - Legacy<br />
`UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL, Telecom equipment tracking field<br />
`NumberOfHoursToRescheduleRedial_si` smallint(6) DEFAULT NULL, - legacy<br />
`TimeZone_ti` tinyint(3) unsigned DEFAULT NULL, Standard RX time zone - 31=PST, 30 MST, 29 CST, 28 EST....<br />
`TransferStatusId_ti` tinyint(4) DEFAULT NULL, Flag if transfer is succesful<br />
`IsHangUpDetected_ti` tinyint(4) DEFAULT NULL, - flag for if Caller hung up before message completed<br />
`IsOptOut_ti` tinyint(4) DEFAULT NULL,  Legacy - tracks opt outs<br />
`IsOptIn_ti` tinyint(4) DEFAULT '0',  Legacy - tracks opt ins<br />
`IsMaxRedialsReached_ti` tinyint(4) DEFAULT NULL, No more redials even if message is not left<br />
`IsRescheduled_ti` tinyint(4) DEFAULT NULL, - legacy - flag 1 if rescheduled<br />
`RXCDLStartTime_dt` datetime DEFAULT NULL, - Main field for tracking message object attempt start time - main date field<br />
`CallStartTime_dt` datetime DEFAULT NULL,<br />
`CallEndTime_dt` datetime DEFAULT NULL,<br />
`CallStartTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
`CallEndTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
`CallResultTS_dt` datetime DEFAULT NULL, - time stamp when call result was calculated<br />
`PlayFileStartTime_dt` datetime DEFAULT NULL,<br />
`PlayFileEndTime_dt` datetime DEFAULT NULL,<br />
`HangUpDetectedTS_dt` datetime DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
`DialerName_vch` varchar(255) DEFAULT NULL, - Fullfillment device DNS name<br />
`DialerIP_vch` varchar(50) DEFAULT NULL, fulfillment device IP address<br />
`CurrTS_vch` varchar(255) DEFAULT NULL,  telecom tracking - devices time slot<br />
`CurrVoice_vch` varchar(255) DEFAULT NULL, telecom tracking devices voice channel CSP slot<br />
`CurrTSLiveTransfer_vch` varchar(255) DEFAULT NULL, telecom tracking - devices time slot - for LAT<br />
`CurrVoiceLiveTransfer_vch` varchar(255) DEFAULT NULL, telecom tracking - devices voice channel CSP slot for LAT<br />
`CurrCDP_vch` varchar(255) DEFAULT NULL, Fulfillment device tracking number - legacy<br />
`CurrCDPLiveTransfer_vch` varchar(255) DEFAULT NULL, - Fulfillment device tracking number - legacy<br />
`ContactString_vch` varchar(255) DEFAULT NULL, Phone Number, eMail Address, or SMS number message was delivered too.<br />
`MsgReference_vch` varchar(1000) DEFAULT NULL, - Legacy<br />
`Reason_vch` varchar(1000) DEFAULT NULL,<br />
`SMSType_ti` tinyint(4) DEFAULT NULL, - MO or MT flag<br />
`ContactTypeId_int` int(11) DEFAULT NULL, 1=voice, 2=email, 3=SMS<br />
`XMLResultStr_vch` text, Captures all responses to all control points <br />
`XMLControlString_vch` longtext, - actual final control string for message - includes any user data customized elements <br />
`FileSeqNumber_int` int(11) DEFAULT NULL, - Legacy - if file process to generate this message had a sequence number. Useful for timed feeds.<br />
`UserSpecifiedData_vch` varchar(1000) DEFAULT NULL, - any thing related to the contact the user wants to store here -usually oringal datqa record is stored here.<br />
`ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000', - Calculate cost of the message - done at time of extraction from fulfillment device<br />
`dial6_vch` varchar(10) DEFAULT NULL, Pre filled field for get first six digits of contact string for voice number information matching<br />
`MainMessageLengthSeconds_int` int(11) DEFAULT '0', -  length of message sent<br />
`RecordedResultPulled_int` int(1) NOT NULL DEFAULT '0', - use to track recorded audio results <br />
`RecordedResponseProcessingStart_dt` datetime DEFAULT NULL,  use to track recorded audio results <br />
`RecordedResponseProcessingComplete_dt` datetime DEFAULT NULL,  use to track recorded audio results <br />
`Status_ti` tinyint(4) DEFAULT NULL, - Legacy for now<br />
`Time_dt` datetime DEFAULT NULL,- Legacy for now<br />
`SMSResult_int` int(4) DEFAULT '0', SMS result from sending MT<br />
`SMSSurveyState_int` int(4) DEFAULT '0', Tracks session state - each session will have its own line<br />
`SMSSequence_ti` tinyint(4) DEFAULT '0' IF multipart SMS this is used to track how many 160 character it was needed to send the concatentated SMS message<br />
`SMSCSC_vch` varchar(255) DEFAULT NULL Short Code related to SMS transaction,<br />
`SMSTrackingOne_bi` bigint(20) DEFAULT '0', - used for tracking MTs sent to aggreagators<br />
`SMSTrackingTwo_bi` bigint(20) DEFAULT '0', - used for tracking MTs sent to aggreagators<br />
`SMSMTPostResultCode_vch` varchar(255) DEFAULT '0', - Post result code from MT attempt<br />
`APIRequestJSON_vch` varchar(2048) DEFAULT NULL, JSON returned from Web Service Call<br />
`ControlKey_int` int(11) DEFAULT '0' - this is the question number of the current control point - Voice, SMS, and eMail<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
