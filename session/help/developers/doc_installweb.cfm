<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Steps to install on web site</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Configure Coldfusion CFIDE virtual directory</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Users permissions</h3>
<p>Add users permission to read &amp; execute. Folders not created with the IIS admin tool will not have the proper permissions.</p>
<p><img src="images/webpermissions.png" width="371" height="453" /></p>
<p>&nbsp;</p>
<h3>Install code from GIT</h3>
<p>Do not install GIT on web server directly in case it gets hacked.</p>
<p>From a develpment box</p>
<p>Install git<br />
  Setup Git - per Github instructions<br />
  Test connection<br />
  <br />
  Clone Repository<br />
      git clone <a rel="external nofollow" href="mailto:git@github.com" title="mailto:git@github.com" target="_blank">git@github.com</a>:MESSAGEBROADCAST/Event-Based-Messaging<br />
  <br />
  Clone will pull entire project down<br />
  Switch to project directory<br />
      cd Event-Based-Messaging<br />
      <br />
  checkout desired branch<br />
      git checkout WebDevelopment<br />
      <br />
  Verify your are in correct branch - * next to current branch<br />
      git branch        <br />
  <br />
  Use the set-upstream arg:<br />
      git branch --set-upstream WebDevelopment origin/WebDevelopment<br />
  Running the above command updates your .git/config file correctly and even verifies with this output:<br />
  <br />
  <br />
  To add/track any new files<br />
      git add .<br />
  <br />
  to commit changes to current branch<br />
      git commit -m 'your message and notes here' -a<br />
      <br />
  to push to current branch<br />
      git push<br />
    </p>
<p>&nbsp;</p>
<h3>Install mod re-write tools</h3>
<p><a href="http://www.iis.net/downloads/microsoft/url-rewrite" rel="nofollow">http://www.iis.net/downloads/microsoft/url-rewrite</a><br />
</p>
<p><img src="images/modrewrite.png" width="883" height="650" /></p>
<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;<br />
  &lt;configuration&gt;<br />
  &lt;system.webServer&gt;<br />
  &lt;rewrite&gt;<br />
  &lt;rules&gt;<br />
  &lt;rule name=&quot;CppUrlRewrite&quot; enabled=&quot;true&quot;&gt;<br />
  &lt;match url=&quot;^[\w]*$&quot; /&gt;<br />
  &lt;action type=&quot;Rewrite&quot; url=&quot;/SignIn.cfm?inpCPPUUID=</p>
{R:0}
<p>&quot; /&gt;<br />
  &lt;/rule&gt;<br />
  &lt;/rules&gt;<br />
  &lt;/rewrite&gt;<br />
  &lt;/system.webServer&gt;<br />
  &lt;/configuration&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Install Font mime types - else you will get 404 errors<br />
http://stackoverflow.com/questions/4015816/why-is-font-face-throwing-a-404-error-on-woff-files</p>
<p><strong>application/x-font-woff</strong> </p>
<p><img src="fontmime.png" width="949" height="739" /></p>
<p>&nbsp;</p>
<h3>Install TTS</h3>
<p>Install Virtual or real sound card in system.</p>
<p>Install TexTAloud <br />
  Your TextAloud Registration Code is:<br />
PPJSYP-326722-VJMJEW-237274-5934</p>
<p>Register Text aloud before installing voice font<br />
  Install SAPI Voice Font -  in text alloud directory<br />
  Samanthaa1.exe,<br />
  paulinaa1.exe<br />
  tomo1.exe</p>
<p>Use control panel to set speech voice to default to Samantha -</p>
<ul type="square">
  <li>Use normal text speed setting</li>
  <li>or crappy voice from microsoft will play by default<br />
    OR if speech is not in the control panel by default -</li>
  <li>look for &quot;C:\Program Files\Common Files\Microsoft Shared\Speech\sapi.cpl&quot;</li>
  <li>just execute to open speech control panel</li>
</ul>
<p>&nbsp;</p>
<p>Install the proper version of Microsoft Speech SDK<br />
http://www.microsoft.com/en-us/download/details.aspx?id=27226</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Install Sphinx Files</h3>
<p>Install JAR files<br />
Install Config and resource files</p>
<p>&nbsp;</p>
<p>Build the project </p>
<p>The &quot;Gold&quot; is in the configuration of the</p>
<p>The config file path is specified in the initialization of the java class object.<br />
  Files and directories needed to run<br />
  cmudict.hub4.06d.dict - file<br />
  language_model.arpaformat.DMP - file<br />
  hub4opensrc.cd_continuous_8gau - dir<br />
</p>
<p>&nbsp;</p>
<p>Creating the JAR file<br />
  Export the running class into a jar file - only need class data</p>
<p>Installing the JAR file on Coldfusion</p>
<p>Copy the .jar file to the lib diectory in your coldfusion install. c$\ColdFusion8\wwwroot\WEB-INF\lib</p>
<p>Additional jars needed to run</p>
<p>transcriber9.jar<br />
  jl1.0.1.jar<br />
  js.jar <br />
  jsapi.jar <br />
  jsapi-1.0-base.jar <br />
  sphinx4.jar <br />
  tags.jar <br />
  WSJ_8gau_13dCep_16k_40mel_130Hz_6800Hz.jar<br />
</p>
<p>&nbsp;</p>
<p>Possible Error:<br />
  java.lang.UnsupportedClassVersionError: TranscriberII : Unsupported major.minor version 51.0 </p>
<p>Make sure the class is compiled with Eclipse project settings where the java is comapatible with the Coldfusion's java version. In this case the code was compiled using 1.7 but Coldfusion 8 the Jar file was installed on only supported 1.6. </p>
<p>Under Project Settings<br />
  Under Java Compiler - change Compiler compliance level back to 1.6. You can still use 1.7 JRE for testing so you dont need to install older JRE</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Setup extra memeory for transcriptions</p>
<p>&nbsp;</p>
<p>To test:</p>
<p>First instantiate the object<br />
  &lt;cfset jarvar = createObject(&quot;java&quot;,&quot;TranscriberII&quot;)&gt;<br />
</p>
<p>Then Set the config path<br />
  &lt;cfset inpConfigPath = &quot;C:/cds/webplayground/config.xml&quot;&gt;<br />
  <br />
  Then call the TranscribeMe method passing in config and file paths<br />
  &lt;cfset outVal2 = jarvar.TranscribeMe(inpConfigPath, &quot;C:/cds/webplayground/BabbleSphere_705_1_1_8.mp3&quot;)&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Setup auto run cron tasks</h3>
<p>Install a separte management web site that is not visible to public. Make sure it is NOT visible to the public.</p>
<p>Run through port 8503</p>
<p>http://EBMDevIIManagement.mb:8503/management/processing/act_Processcontactqueue.cfm?VerboseDebug=0
http://EBMDevIIManagement.mb:8503/management/processing/act_ProcessExtractionRXDialers.cfm?VerboseDebug=0</p>
<p>127.0.0.1 ebmdeviimanagement.mb</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Setup Script Stuff</h3>
<p>Install application.basedir</p>
<p>need read/write #application.baseDir#/data/blank.mp3</p>
<p>&nbsp;</p>
<p>Configure Database setup in Coldfusion to support CLOB - long text data<br />
See - 
http://coldfusion9.blogspot.com/2010/05/enable-long-text-retrieval-in.html<br />
Big Interactive Campaigns/Surveys will exceed 64000 characters</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> <br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>