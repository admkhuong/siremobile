<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<p>&nbsp;</p>
<p>In each CompanyAccount there is an optional field for a &quot;Shared&quot; User Id</p>
<p>&nbsp;</p>
<p>This means any user that logs in as a member of this company will impersonate the UserId of the &quot;Shared&quot; userId specified in the `simpleobjects`.`companyaccount` table.</p>
<p>&nbsp;</p>
<p>Default is 0 and this option will be ignored.</p>
<p>&nbsp;</p>
<p>All logging, permissions, and authentication actions still need to refer to the actual logged in user but all list, reporting and campaigns are one big &quot;Shared&quot; account.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Authentication checks</p>
<p>IF CompanyId is specified in Users Account<br />
and<br />
IF SharedAccountUserId_int is greater than 0 in that company account<br />
</p>
<p>Then<br />
Session.UserID = SharedAccountUserId_int<br />
    and<br />
Session.COMPANYID = getUser.CompanyAccountId_int<br />
and<br />
Session.CompanyUserId = getUser.UserId</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>For Permissions<br />
</p>
<p>IF Session.CompanyUserId greater than 0<br />
    THEN Check against this account<br />
    ELSE<br />
    Check against 
Session.UserID</p>
<p>&lt;!--- Check permission against acutal logged in user not &quot;Shared&quot; user---&gt;<br />
&lt;cfif Session.CompanyUserId GT 0&gt;<br />
&lt;cfinvoke component=&quot;#LocalSessionDotPath#.cfc.administrator.permission&quot; method=&quot;getUserByUserId&quot; returnvariable=&quot;getCurrentUser&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;userId&quot; value=&quot;#Session.CompanyUserId#&quot;&gt;<br />
&lt;/cfinvoke&gt;<br />
&lt;cfelse&gt;<br />
&lt;cfinvoke component=&quot;#LocalSessionDotPath#.cfc.administrator.permission&quot; method=&quot;getUserByUserId&quot; returnvariable=&quot;getCurrentUser&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;userId&quot; value=&quot;#session.userId#&quot;&gt;<br />
&lt;/cfinvoke&gt;<br />
&lt;/cfif&gt;</p>
<p>&nbsp; </p>
<p>For User Logging</p>
<p>&lt;!--- Check permission against acutal logged in user not &quot;Shared&quot; user---&gt;<br />
&lt;cfif Session.CompanyUserId GT 0&gt;<br />
&lt;cfinvoke method=&quot;createUserLog&quot; component=&quot;#Session.SessionCFCPath#.administrator.usersLogs&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;userId&quot; value=&quot;#Session.CompanyUserId#&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;moduleName&quot; value=&quot;Administration&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;operator&quot; value=&quot;Change permission for company #getPermissionByUCIDLoop.userCompanyPermissionId_int#&quot;&gt;<br />
&lt;/cfinvoke&gt;<br />
&lt;cfelse&gt;<br />
&lt;cfinvoke method=&quot;createUserLog&quot; component=&quot;#Session.SessionCFCPath#.administrator.usersLogs&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;userId&quot; value=&quot;#session.userid#&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;moduleName&quot; value=&quot;Administration&quot;&gt;<br />
&lt;cfinvokeargument name=&quot;operator&quot; value=&quot;Change permission for company #getPermissionByUCIDLoop.userCompanyPermissionId_int#&quot;&gt;<br />
&lt;/cfinvoke&gt;<br />
&lt;/cfif&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>