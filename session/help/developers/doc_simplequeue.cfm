<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Simple Queue</h1>
<h3>&nbsp;</h3>
<h3>Overview</h3>
<p>The Simple Queue is where content and contacts are combined into a request for fulfillment. If properly inserted into the simplequeue.contactqueue table then fulfillment will be taken care of by automated processes.</p>
<p>&nbsp;</p>
<h3>DTSId_int</h3>
<p>Each entry into the Dial Queue will gets its own auto incremented ID. This is it.</p>
<h3>DTS_UUID_vch</h3>
<p>Each entry in the Queue will get its own Globally Unique Identifier. This is passed all the way through to fullfillment and back into the results. Can be used as Dynamic data on distribution to fullfillemnt. If Automated processes are handling distribution to fulfillment, then UUID_vch willl replace all text in XMLControlString that is labled REPLACETHISUUID. This can be used in multiple ways.</p>
<h3>BatchId_bi</h3>
<p>Nothing can go out without a BatchID. This not only ties back to the user, but defines the initial template used to buld the custom XMLControlString used in this entry to the Dial Queue. Schedules are also tied to the Batch Id</p>
<h3>DTSStatusType_ti </h3>
These are Status Flags to tell what state each entry in the Dial Queue is in.
<p>0=Paused<br />
  1 = Queued<br />
  2 = Queued to go on RXDialer - this means processing has started<br />
  3 = Now On Dialer <br />
  4 = Extracted due BR rule (duplicates) on distibution<br />
  5 = Extracted by Process<br />
6 = Extracted due BR rule (bad XML) on distibution<br />
7 = SMS Stop request<br />
 8 = Fulfilled by Real Time request<br />
    100 - Queue hold due to distribution of audio error(s)<br />
    There will be more status types as the system is built out. </p>
<p>For Instant Load - demos, quick responders, etc<br />
  Use status 2 to load queue <br />
  then update to 3 when pushed to RXDialer<br />
  To manually force queue to distribute you will need your own  distribution object ala act_processcontactqueue for just one queue item without stage objects.</p>
<p>&lt;!--- SMS Outbound Queue states ---&gt;<br />
    &lt;cfset SMSOUT_PAUSED = 0&gt;<br />
    &lt;cfset SMSOUT_QUEUED = 1&gt;<br />
    &lt;cfset SMSOUT_QUEUEDTOGO = 2&gt;<br />
    &lt;cfset SMSOUT_INPROCESSONDIALER = 3&gt;<br />
    &lt;cfset SMSOUT_DUPLICATE = 4&gt;<br />
    &lt;cfset SMSOUT_EXTRACTED = 5&gt;<br />
    &lt;cfset SMSOUT_BADXML = 6&gt;<br />
    &lt;cfset SMSOUT_SMSSTOP = 7&gt;<br />
&lt;cfset SMSOUT_HOLDAUDIOERROR = 100&gt;</p>
<p>TypeMask_ti</p>
<p>This is used to indicate what primary type of content is being sent to the fullfillment device</p>
<p>1 = Voice - ContactString_vch will be a Phone Number<br />
  2 = eMail - 
  ContactString_vch will be an eMail Address<br />
  3 = SMS - ContactString_vch will be an SMS capable Phone Number </p>
<h3>TimeZone_ti</h3>
<p>Each request is associated with both a Batch Schedule and the actual Scheduled_dt in the Dial Queue. The time zone is used to determine if <br />
  a. The Dial Queue Scheduled_dt has been reached<br />
  b. If the Batch schedule is elligible to go out </p>
<h3>CurrentRedialCount_ti</h3>
<p>In the case where there are multiple unique attempts to the same contact for the same batch then this value will indicated which attempt this is. This is independent of any redial logic specified as part of the CCD structures. Initial attempt starts at 0 and goes up from there.</p>
<h3>UserId_int</h3>
<p>This is the EBM Users ID. Thbis feild may be redundent as it can be reverse looked up from the Batch ID but this field helps with reporting and monitoring.</p>
<h3>PushLibrary_int<br />
  PushElement_int  <br />
PushScript_int  </h3>
<p>These fields are used in the very simple case where you know all of the script library data in advance. Default behavior is to specify these as 0 and the automated distribution processes will auto claculate the audio libraries needed for each batch. These libraries need to be pushed and updated on the fulfillment devices to ensure proper messaging.<br />
</p>
<h3>PushSkip_int</h3>
<p>For Advanced users only. ) be default. IF you are absolutly sure your audio libraries are up to date on all the fulfillemtn devices, you can speed up distribution by specifiying a value greater than 0.</p>
<h3>EstimatedCost_int</h3>
<p>This is used to make a guess at how much a given message will cost a EBM User account. This value is used to determine if the EBM User has enough money left in their account billing balance to add more messages to the Queue.</p>
<h3>ActualCost_int</h3>
<p>This value is populated after fulfillment is complete and is used to make sure EstimatedCosts are in line with expectations. Billing is decremented by this actual cost.</p>
<h3>GroupId_int</h3>
<p>If the EBM user distriubtes an entie group of contacts from the RXMultiList, this ID is the group that is queued.</p>
<h3>Scheduled_dt</h3>
<p>This is the date the Queued data is elligible to be sent. This date can be in the future and will keep a request in queue until this date time is reached. Can be used to push out 15 minutes. Can be used to push out 30 days or more. This date time is just when it is elligible. The Batch Schedule will control final schedule elligibility.</p>
<h3>Queue_dt</h3>
<p>This is when the message is first inserted into the queue.</p>
<h3>Queued_DialerIP_vch</h3>
<p>This is blank until automated proecesses push to fulfillment. When in fulfillemnt this is the IP address of the fulfillment device responsible for final message delivery. Fulfillment is a distributed database system. Each fulfillment device is an Island on to itself. This can be used to track errors.</p>
<h3>ContactString_vch</h3>
<p>This is who the message is to go out to. Phone or eMail addresses go here.</p>
<h3>Sender_vch</h3>
<p>As different systems access this structure EBM, BabbleSphere, PASP, CFTE, .... each system will uniquly identify itself.</p>
<h3>XMLControlString_vch</h3>
<p>This is the control string that is customized from the main Batch template XMLControlString and is ready to go to fulfillment devices. The only thing left to do to it on distribution should be REPLACETHISUUID.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Strucure</h3>
<p>CREATE TABLE `contactqueue` (<br />
  `DTSId_int` int(10) unsigned NOT NULL AUTO_INCREMENT,<br />
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
  `BatchId_bi` bigint(20) unsigned DEFAULT NULL,<br />
  `DTSStatusType_ti` tinyint(4) DEFAULT '0',<br />
  `TypeMask_ti` tinyint(4) NOT NULL DEFAULT '1',<br />
  `TimeZone_ti` tinyint(4) DEFAULT '0',<br />
  `CurrentRedialCount_ti` tinyint(3) unsigned DEFAULT '0',<br />
  `UserId_int` int(10) unsigned DEFAULT NULL,<br />
  `PushLibrary_int` int(11) NOT NULL DEFAULT '0',<br />
  `PushElement_int` int(11) NOT NULL DEFAULT '0',<br />
  `PushScript_int` int(11) NOT NULL DEFAULT '0',<br />
  `PushSkip_int` tinyint(4) NOT NULL DEFAULT '0',<br />
  `EstimatedCost_int` float(10,3) NOT NULL DEFAULT '0.000',<br />
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',<br />
  `CampaignTypeId_int` int(11) DEFAULT NULL,<br />
  `GroupId_int` int(11) NOT NULL DEFAULT '0',<br />
  `Scheduled_dt` datetime DEFAULT NULL,<br />
  `Queue_dt` datetime DEFAULT NULL,<br />
  `Queued_DialerIP_vch` varchar(255) DEFAULT NULL,<br />
  `ContactString_vch` varchar(255) NOT NULL,<br />
  `Sender_vch` varchar(255) DEFAULT NULL,<br />
  `XMLControlString_vch` text,<br />
  PRIMARY KEY (`DTSId_int`),<br />
  UNIQUE KEY `UC_DTSId_int` (`DTSId_int`),<br />
  KEY `IDX_Batch_Status_TimeZone_Combo` (`BatchId_bi`,`DTSStatusType_ti`,`TimeZone_ti`),<br />
  KEY `IDX_Scheduled` (`Scheduled_dt`),<br />
  KEY `IDX_UserId_int` (`UserId_int`),<br />
  KEY `IDX_Queued_DialerIP` (`Queued_DialerIP_vch`),<br />
  KEY `IDX_DialString` (`ContactString_vch`),<br />
  KEY `IDX_BatchId` (`BatchId_bi`),<br />
  KEY `IDX_DtsStatusType` (`DTSStatusType_ti`),<br />
  KEY `IDX_TypeMask` (`TypeMask_ti`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=935 DEFAULT CHARSET=latin1</p>
<p>&nbsp;</p>
<p><br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p></p>
<p></p>
<p></p>
<p></p>
</body>
</html>