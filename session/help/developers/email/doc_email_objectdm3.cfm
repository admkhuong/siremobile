<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>A &quot;DM&quot; object is a special case XML construct meaning &quot;Dynamic Message&quot;</p>
<p>MT is the &quot;Message Type&quot; within a DM</p>
<p>MT = 1 is live voice<br />
    MT = 2 is Machine Voice - Deprecated - see MCID RXT=22 for new methodology<br />
    MT = 3 is email<br />
    MT = 4
isd voice</p>
<p>DESC = 'What ever you want to call it for future reference'</p>
<p>X=x position on GUI stage<br />
Y=y position on GUI stage</p>
<p>BS=Build Script - not applied to email can be 0 or ignored<br />
    DSUID=Dynamic Script User ID - does not apply to email
        <br />
        LIB=Library ID - again does not apply to email
        <br />
        LINK=is used to link to another obect on the GUI stage
    <br />
</p>
<p>&nbsp;</p>
<p>Each DM does contain a set of &lt;ELE&gt;. For email there is only one ELE</p>
<p>&nbsp;</p>
<p><br />
</p>
<p>&nbsp;</p>
<h3>Back End:</h3>
<p>All data for an email message needs to be stored in XMLControlString_vch in a Batch</p>
<p>&nbsp;</p>
<p>Files in play</p>
<p>Session/CFC/mcidtoolsii.cfc</p>
<p>ReadDM_MT3XML - reads current from DB<br />
    WriteDM_MT3XML - writes an XML string to the master string in DB</p>
<p>Session/CFC/MCID/read_DM_MT3.cfm - tool used in above method to read values from the DM</p>
<p>/MCID/cfc/mcidtools.cfc?method=genrxt13XML - generates a valid &lt;DM&gt; XML string</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
    <DM BS='0' DESC='Description Not Specified' DSUID='237' LIB='0' MT='3' PT='12' X='80' Y='80'>
        <ELE BS='0' CK1='0' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' CK2='' CK3='' CK4='<p>Hello World!</p>' CK5='NoReply@MBCampaign.com' CK6='' CK7='' CK8='Testing 123' CK9='' DESC='Description Not Specified' DSUID='237' LINK='0' QID='1' RXT='13' X='0' Y='0'></ELE>
    </DM> 
    Sample Hello World email:
</p>
<pre scrollleft="0" scrolltop="0" id="__cxXmlContent">&lt;DM BS='0' DESC='Description Not Specified' DSUID='237' LIB='0' MT='3' PT='12' X='80' Y='80'&gt;<br />     &lt;ELE BS='0' CK1='0' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' CK2='' CK3='' CK4='&lt;p&gt;Hello World!&lt;/p&gt;' CK5='NoReply@MBCampaign.com' CK6='' CK7='' CK8='Testing 123' CK9='' DESC='Description Not Specified' DSUID='237' LINK='0' QID='1' RXT='13' X='0' Y='0'&gt;0&lt;/ELE&gt;
&lt;/DM&gt;</pre>
<p>&nbsp;</p>
<p>Email specific control keys:</p>
<p>&lt;DM MT='4'</p>
<p>           <br />
                    CK1 =  Async Flag 1 = Async 0 or blank for Syncronouse - Async releases line  faster - good for inbound - not as critical for outbound<br />
                    CK2 = MessageWarning;                        <br />
                    CK3 = MessageText;        <br />
                    CK4 = MessageHTML;                            <br />
                    CK5 = MessageFromUser;                        <br />
                    CK6 = MessageToUser;    ; seperated list of email addresses;    <br />
                    CK7 = MessageCCUser;     ; seperated list of email addresses;<br />
                    CK8 = MessageSubject;    <br />
                    CK9 = MessageCreationDate;    <br />
                    CK10 = ConnFromUser;    <br />
                    CK11 = ConnFromPassword;    <br />
                    CK12 = AttachmentPath;    <br />
                    CK13 = SMTP Server IP Address - if blank use local dialers Reg settings<br />
                    CK14 = Optional UUID - if not  specified will be a combination of BatchID and Dialer specific DTS id<br />
                CK15 = Next QID</p>
<p>&nbsp;</p>
<p>Special characters if your going to use in your email, make sure you escape them properly.</p>
<table border="1" cellpadding="3" width="100%">
    <tbody>
        <tr>
            <td>Double Quote &quot;</td>
            <td>\&quot;</td>
        </tr>
        <tr>
            <td>apostrophe (')</td>
            <td>&amp;apos;</td>
        </tr>
        <tr>
            <td>ampersand (&amp;)</td>
            <td>&amp;amp;</td>
        </tr>
        <tr>
            <td>less than (&lt;)</td>
            <td>&amp;lt;</td>
        </tr>
        <tr>
            <td>greater than (&gt;)</td>
            <td>&amp;gt;</td>
        </tr>
        <tr>
            <td>slash (\)</td>
            <td>\\</td>
        </tr>
        <tr>
            <td>space</td>
            <td>No escape required</td>
        </tr>
    </tbody>
</table>
<p> </p>
<p> </p>
<p>&nbsp;</p>
<p>Type SMTP in search on tech wiki - copy documents that make sense to session/help/developer/email folder</p>
<p>&nbsp;</p>
<p>http://techwiki.messagebroadcast.com/Lines_of_Business/EMail_-_LOB/RFC_Standards/RFC_821_-_Simple_Mail_Transfer_Protocol<br />
    http://techwiki.messagebroadcast.com/Lines_of_Business/Checklist_for_new_email_campaign</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Email Testing:</p>
<div id="pageText">
    <p>The easiest way to send an e-mail  message without a client is to drop a properly formatted text file into  the Pickup directory on the server. RFC 822 defines the formatting  standards for ARPA Internet text messages. Following the guidelines in  RFC 822 you can easily create text files, that when copied into the  Pickup directory, will be delivered to the specified mailbox.</p>
    <p> </p>
    <p> </p>
    <p>Open the cmd prompt.<br />
        <br />
        Type telnet server.com 25 (If you live in Canada, put 23) (where &quot;<a id="KonaLink0" target="undefined" rel="external nofollow" href="http://www.debianhelp.co.uk/mail.htm#" title="http://www.debianhelp.co.uk/mail.htm#">server</a>.com&quot; is the name of the smtp (outgoing)<br />
        <br />
        server of your email provider, such as smtp-server.austin.rr.com).  This can be found by checking your account info in the program you  normally use for email.<br />
        <br />
        Type HELO server.com.<br />
        <br />
        Type MAIL FROM:<a rel="external nofollow" href="mailto:you@server.com" title="mailto:you@server.com" target="_blank">you@server.com</a>.<br />
        <br />
        Type RCPT TO <a rel="external nofollow" href="mailto:Friend1@anotherserver.com" title="mailto:Friend1@anotherserver.com" target="_blank">Friend1@anotherserver.com</a>, <a rel="external nofollow" href="mailto:friend_two@someotherserver.org" title="mailto:friend_two@someotherserver.org" target="_blank">friend_two@someotherserver.org</a>, <a rel="external nofollow" href="mailto:friend.3three@Someserver.com" title="mailto:friend.3three@Someserver.com" target="_blank">friend.3three@Someserver.com</a>, etc.<br />
    </p>
    <p><strong>To write the message, type DATA and press Enter</strong>.  </p>
    <ol>
        <li>On the first line type SUBJECT:yoursubject and press Enter twice.</li>
        <li>Continue typing your message.</li>
        <li>Put  a single period (.) on a line by itself and press Enter to send your  message. The server should say 'Message accepted for delivery'. (Or it  says 250 OK id=`a long id`)</li>
    </ol>
    <p><br />
        To end the message, put a period on a line by itself and press Enter.<br />
         </p>
    <p>type QUIT to exit or type mail from to start a new email</p>
    <p><br />
        <strong>Important Tips<br />
        </strong><br />
        Just a note: you may have to enter a &quot;:&quot; after the &quot;mail from:&quot; and the &quot;rcpt to:&quot;<br />
        <br />
        This can also be used to send email as other people.<br />
        <br />
        Some servers also accept 'ELHO' in place of 'HELO'<br />
        <br />
        <strong>Warnings for users<br />
        </strong><br />
        Hotmail and some other mail services do not allow telnet access to their <a id="KonaLink2" target="undefined" rel="external nofollow" href="http://www.debianhelp.co.uk/mail.htm#" title="http://www.debianhelp.co.uk/mail.htm#">mail servers</a>.<br />
        <br />
        This can be tracked by anyone with enough technical skill, access to your <a id="KonaLink3" target="undefined" rel="external nofollow" href="http://www.debianhelp.co.uk/mail.htm#" title="http://www.debianhelp.co.uk/mail.htm#">ISP's</a> records, and a bit of determination, so don't do anything you wouldn't want to possibly be associated with you.<br />
        <br />
        <strong>How to Check Email With Telnet</strong><br />
        <br />
        Telnet can be used as another way to check email. The first two steps are for <a id="KonaLink4" target="undefined" rel="external nofollow" href="http://www.debianhelp.co.uk/mail.htm#" title="http://www.debianhelp.co.uk/mail.htm#">Windows</a> only; Mac and Linux users should use their own methods for launching a terminal/console window.<br />
        <br />
        <strong>Follow these Steps</strong><br />
        <br />
        Select Start in the bottom left corner of the screen, then select Run.<br />
        <br />
        Once the Run window starts, type in cmd.<br />
        <br />
        At the <a id="KonaLink5" target="undefined" rel="external nofollow" href="http://www.debianhelp.co.uk/mail.htm#" title="http://www.debianhelp.co.uk/mail.htm#">command prompt</a>, type in telnet emailprovider.com 110 (where &quot;emailprovider&quot; is the name of the service you use for email).<br />
        <br />
        Type USER yourusername (you may see what you type or not, and  &quot;yourusername&quot; should be changed to whatever comes before the @ in your  email address).<br />
        <br />
        Then type in PASS yourpassword (if you can see what you type, you will see your password).<br />
        <br />
        Type list.<br />
        <br />
        You will see a list of items with labels like &quot;1 1024&quot; and &quot;2 123556.&quot;<br />
        <br />
        If you want to look at the message labeled 2 123556, type retr 2.  You can replace the 2 with any other number to view other messages.<br />
        <br />
        If you want to delete message 1 1024, type dele 1.<br />
        <br />
        When you are done checking your email, type quit.&lt;wbr&gt; &lt;/wbr&gt;</p>
    <p>&lt;wbr&gt;</p>
    <p> </p>
    <p>&lt;/wbr&gt;</p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>