<p>&nbsp;</p>
<p>Download<br />
http://cmusphinx.sourceforge.net/wiki/download/</p>
<p>&nbsp;</p>
<p>WIKI<br />
http://cmusphinx.sourceforge.net/wiki/</p>
<p>&nbsp;</p>
<p>Tutorial<br />
http://cmusphinx.sourceforge.net/wiki/tutorial<br />
http://cmusphinx.sourceforge.net/sphinx4/doc/ProgrammersGuide.html<br />
http://cmusphinx.sourceforge.net/sphinx4/javadoc/edu/cmu/sphinx/jsgf/JSGFGrammar.html<br />
http://www.speech.cs.cmu.edu/sphinx/tutorial.html</p>
<p>&nbsp;</p>
<p>http://java2everyone.blogspot.com/2011/01/create-java-mp3-to-wav-converter.html<br />
http://stackoverflow.com/questions/5743390/creating-arpa-language-model-file-with-50-000-words<br />
http://stackoverflow.com/questions/8727389/dictation-application-using-sphinx4<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>JAVA<br />
http://cmusphinx.sourceforge.net/sphinx4/</p>
<p>Eclipse Dev environment<br />
http://www.stanford.edu/class/cs108/JavaTools/eclipse-guide/</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Demos<br />
http://cmusphinx.sourceforge.net/sphinx4/src/apps/edu/cmu/sphinx/demo/transcriber/README.html</p>
<p>http://stackoverflow.com/questions/9841870/how-to-improve-cmusphinxs-accuracy</p>
<p>&nbsp;</p>
<h3>In Practice Transcriptions sample:</h3>
<p>https://github.com/ox-it/spindle-code/tree/master/speechToText</p>
<p>http://blogs.oucs.ox.ac.uk/openspires/2012/10/05/spindle-speech-to-text-to-keywords-to-captions-the-grand-finale/</p>
<p>http://www.speech.cs.cmu.edu/sphinx/tutorial.html</p>
<p>&nbsp;</p>
<p>Debbugging extra info<br />
http://cmusphinx.sourceforge.net/sphinx4/javadoc/edu/cmu/sphinx/instrumentation/doc-files/Instrumentation.html</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Language Models</p>
<p>http://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Models/</p>
<p>http://www.speech.cs.cmu.edu/sphinx/models/    <br />
  http://www.keithv.com/software/giga/</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>K:\CDs\speechrec\sphinx4src\sphinx4-1.0beta6\tests\performance</p>
<p>http://www.cs.columbia.edu/~julia/courses/CS4706/building-asr.pdf</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h1>Notes:</h1>
<p>To run the demos you need to make sure the jsapi.jar file has been created. Due to license restrictions it is not part of distribution.</p>
<h3>Setting Up Your JSAPI Environment</h3>
<p>The sources to the JSAPI 1.0 specification implementation (i.e., the javax.speech.* classes) are not available under a BSD-style license. Instead, we make the JSAPI binary available under a separate binary code license (BCL).</p>
<p>Obtaining the JSAPI binary is as simple as unpacking the jsapi.jar file in the lib directory:</p>
<ul>
  <li>
    <p>UNIX-like Systems (including Linux, MacOSX, etc)</p>
    <ul>
      <li>Go to the lib directory.</li>
      <li>Type chmod +x ./jsapi.sh.</li>
      <li>Type sh ./jsapi.sh and view the BCL.</li>
      <li>If the BCL is acceptable, accept it by typing &quot;y&quot;. The jsapi.jar file will be unpacked and deposited into the lib directory.</li>
      <li>A note about MacOSX: when running the jsapi.sh script, a warning message may appear saying that the option &quot;--print-text-domain-dir&quot; does not exist. Please ignore this message. The jsapi.jar will still be installed.</li>
    </ul>
  </li>
  <li>
    <p>Windows Systems</p>
    <ul>
      <li>Go to the lib directory.</li>
      <li>Type .\jsapi.exe</li>
      <li>A window will appear with the BCL. View it.</li>
      <li>If the BCL is acceptable, accept it by clicking on the &quot;I accept&quot; button. The jsapi.jar file will be unpacked and deposited into the lib directory.</li>
    </ul>
  </li>
</ul>
<p>&nbsp;</p>
<h3>Build grammer/dictionary/language files from other formats</h3>
<p>I am new to Sphinx 4. I have been reading about Sphinx 4 and have to implement it as a part of the project. I am building upon HelloNGram model. To improve accuracy I wish to include a language model I downloaded from <a href="http://www.keithv.com/software/giga/" rel="nofollow">http://www.keithv.com/software/giga/</a> I have downloaded 64k NVP 3 gram.</p>
<p>I see these following files in the zip archive lm_giga_64k_nvp.hdecode.dic lm_giga_64k_nvp.hvite.dic lm_giga_64k_nvp.sphinx.dic lm_giga_64k_nvp.sphinx.filler lm_giga_64k_nvp_3gram.arpa phonelist wlist_giga_64k_nvp wlist_giga_64k_nvp_start_end</p>
<p>I want to integrate it in HelloNGram. How do I go about doing this? Please explain all the steps. I am very new to sphinx 4! Or if there is any other model you would suggest and can instruct as to how to integrate it in my project... Thanks!</p>
<p>&nbsp;</p>
<p>Step 1. Convert lm_giga_64k_nvp_3gram.arpa to DMP format with sphinx_lm_convert from sphinxbase.</p>
<p>Step 2. In config file change the type of language model component from SimpleNGramModel to LargeTrigramModel</p>
<p>Step 3. Update component property to load converted DMP file</p>
<p>To learn more about Sphinx4 read the tutorial</p>
<p><a href="http://cmusphinx.sourceforge.net/wiki/tutorial" rel="nofollow">http://cmusphinx.sourceforge.net/wiki/tutorial</a></p>
<p>It will help you to answer the questions you have</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Big Vocab Set up</p>
<p>&nbsp;</p>
<p>language_model.arpaformat.DMP<br />
cmudict.hub4.06d.dict</p>
<p><br />
&lt;!-- ******************************************************** --&gt;<br />
&lt;!-- The Language Model configuration                         --&gt;<br />
&lt;!-- ******************************************************** --&gt;<br />
</p>
<p><br />
  &lt;component name=&quot;trigramModel&quot; <br />
  type=&quot;edu.cmu.sphinx.linguist.language.ngram.large.LargeTrigramModel&quot;&gt;<br />
  &lt;property name=&quot;location&quot; <br />
  value=&quot;resource:/edu/cmu/sphinx/demo/hellongram/hellongram.trigram.lm&quot;/&gt;<br />
  &lt;property name=&quot;logMath&quot; value=&quot;logMath&quot;/&gt;<br />
  &lt;property name=&quot;dictionary&quot; value=&quot;dictionary&quot;/&gt;<br />
  &lt;property name=&quot;maxDepth&quot; value=&quot;3&quot;/&gt;<br />
  &lt;property name=&quot;unigramWeight&quot; value=&quot;.7&quot;/&gt;<br />
  &lt;/component&gt;<br />
</p>
<p> &lt;component name=&quot;trigramModel&quot;<br />
type=&quot;edu.cmu.sphinx.linguist.language.ngram.large.LargeTrigramModel&quot;&gt;<br />
&lt;property name=&quot;unigramWeight&quot; value=&quot;.5&quot;/&gt;<br />
&lt;property name=&quot;maxDepth&quot; value=&quot;3&quot;/&gt;<br />
&lt;property name=&quot;logMath&quot; value=&quot;logMath&quot;/&gt;<br />
&lt;property name=&quot;dictionary&quot; value=&quot;dictionary&quot;/&gt;<br />
&lt;property name=&quot;location&quot;<br />
value=&quot;/lab/speech/sphinx4/data/hub4_model/language_model.arpaformat.DMP&quot;/&gt;<br />
&lt;/component&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
