<!--- Add a new Batch Form --->


<cfinclude template="../paths.cfm" >
<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>

<script TYPE="text/javascript">
	function SendValidationCode()
	{
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "Are you sure?", "About to send validation code to " + $("#SendValidationCodeDiv #inpEmail").val(), 
	function(result) { 
		if(!result){
			return;
		}else{	
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cfc/users.cfc?method=SendValidationCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpUserName : $("#SendValidationCodeDiv #inpUserName").val(),inpMailTo : $("#SendValidationCodeDiv #inpEmail").val()},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert(d.DATA.MESSAGE[0],d.DATA.MESSAGE[0], function(result) { } );
							}else{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert( d.DATA.MESSAGE[0],d.DATA.ERRMESSAGE[0], function(result) { } );
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
					
		}); 

	 }  } );		

	
		return false;

	}
	
	$(function() {
		$("#SendValidationCodeButton").click( function() { SendValidationCode(); } );});
	
	var ResetPasswordDialog = 0;
	function ResetPasswordDialogCall(){


		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		<!--- Erase any existing dialog data --->
		if(ResetPasswordDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			ResetPasswordDialog.remove();
			ResetPasswordDialog = 0;
			
		}	
		ResetPasswordDialog = $('<div></div>').append($loading.clone());	
		ResetPasswordDialog
			.load('./help/dsp_ResetPassword.cfm')
			.dialog({
				modal : true,
				title: 'Reset User\'s password',
				close: function() {
					 ResetPasswordDialog.remove(); 
					 ResetPasswordDialog = 0; 
				},
				width: 400,
				position:'top',
				height: 'auto',
				buttons:[
					{
						text:'Reset',
						id:'ResetPasswordButton',
						disabled:false,
						click:function(){
							ResetPassword();
						}
					},
					{
						text: 'Cancel',
						click:function(){
							ResetPasswordDialog.remove(); 
							ResetPasswordDialog = 0;
						}
					}
				]
			});
		ResetPasswordDialog.dialog('open');
		return false;
	}
</script>


<style>

#SendValidationCodeDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#SendValidationCodeDiv div{
display:inline;
border:none;
}

</style> 
 

<cfoutput>

<div id='SendValidationCodeDiv' class="RXForm">



<form id="SendValidationCodeForm" name="SendValidationCodeForm" action="" method="POST">
		<p>Enter username and your registered email</p>
		<p><a onclick="ResetPasswordDialogCall()">or enter validation code</a></p>
<br/>
       <p> <label>User Name
        </label>
        <input TYPE="text" name="inpUserName" id="inpUserName" size="30" class="ui-corner-all" title="User Name" style="position:absolute;right:70px"  /> </p>
		<br /> 
       <p>
        <label>Email
        </label>
        <input TYPE="text" name="inpEmail" id="inpEmail" size="30" class="ui-corner-all" style="position:absolute;right:70px" title="Registered Email to receive validation code"/> </p>
        <BR/>
</form>

</div>
</cfoutput>

























