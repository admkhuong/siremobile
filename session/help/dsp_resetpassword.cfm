<cfinclude template="../paths.cfm" >

<style>

#SendValidationCodeDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#SendValidationCodeDiv div{
display:inline;
border:none;
}

</style> 

<script>
	function ResetPassword()
	{
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "Are you sure?", "send new password to " + $("#ResetPasswordDiv #inpEmail").val(), 
	function(result) { 
		if(!result){
			return;
		}else{	
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cfc/users.cfc?method=ResetPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpUserName : $("#ResetPasswordDiv #inpUserName").val(),inpEmail : $("#ResetPasswordDiv #inpEmail").val(),inpValidationCode : $("#ResetPasswordDiv #inpValidationCode").val()},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert(d.DATA.MESSAGE[0],d.DATA.MESSAGE[0], function(result) { } );
							}else{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert( d.DATA.MESSAGE[0],d.DATA.ERRMESSAGE[0], function(result) { } );
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			}
					
		}); 

	 }  } );		

	
		return false;

	}
	
	
	$(function(){
		$("#ResetPasswordDiv #inpUserName").val($("#SendValidationCodeDiv #inpUserName").val());
		$("#ResetPasswordDiv #inpEmail").val($("#SendValidationCodeDiv #inpEmail").val());
	});
</script>

<cfoutput>

<div id='ResetPasswordDiv' class="RXForm">

<form id="ResetPasswordForm" name="ResetPasswordForm" action="" method="POST">
       <p> <label>User Name
        </label>
        <input TYPE="text" name="inpUserName" id="inpUserName" size="20" class="ui-corner-all" title="User name" style="position:absolute;right:150px" /> 
        <br /> 
		<br /> 
       
        <p><label>Email
        </label>
        <input TYPE="text" name="inpEmail" id="inpEmail" size="40" class="ui-corner-all" style="position:absolute;right:51px" title="Registered Email" /> 
        <BR/>
		<br /> 


        <label>Validation Code
        </label>
        <input TYPE="text" name="inpValidationCode" id="inpValidationCode" size="20" class="ui-corner-all" title="Validation Code from recieved email" style="position:absolute;right:150px"  /> 
        <br /> 
		<br /> 

</form>
</div>
</cfoutput>