﻿<html>
	
<head>
	<title>Brief Description</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="generator" content="HelpNDoc Personal Edition 3.9.1.648">
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/reset.css" />
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/hnd.css" />
	<!--[if lte IE 8]>
		<link type="text/css" rel="stylesheet" media="all" href="css/ielte8.css" />
	<![endif]-->
	<style type="text/css">
		#topic_header
		{
			background-color: #EFEFEF;
		}
	</style>
	<script type="text/javascript" src="file:///C|/Users/akhare/Desktop/ebm/js/jquery.min.js"></script>
	<script type="text/javascript" src="file:///C|/Users/akhare/Desktop/ebm/js/hnd.js"></script>
	<script type="text/javascript">
		$(document).ready(function()
		{
			if (top.frames.length == 0)
			{
				var sTopicUrl = top.location.href.substring(top.location.href.lastIndexOf("/") + 1, top.location.href.length);
				top.location.href = "Event Based Messaging Documentation.html?" + sTopicUrl;
			}
			else if (top && top.FrameTOC && top.FrameTOC.SelectTocItem)
			{
				top.FrameTOC.SelectTocItem("BriefDescription");
			}
		});
	</script>
</head>

<body>

	<div id="topic_header">
			<div id="topic_header_content">
				<h1>Brief Description</h1>
				
				<div id="topic_breadcrumb">
					<a href="file:///C|/Users/akhare/Desktop/ebm/EMBLogging.html">EMB Logging</a> &rsaquo;&rsaquo; </div>
				</div>
			<div id="topic_header_nav">
				<a href="file:///C|/Users/akhare/Desktop/ebm/EMBLogging.html"><img src="file:///C|/Users/akhare/Desktop/ebm/img/arrow_up.png" alt="Parent"/></a>
				
				<a href="file:///C|/Users/akhare/Desktop/ebm/EMBLogging.html"><img src="file:///C|/Users/akhare/Desktop/ebm/img/arrow_left.png" alt="Previous"/></a>
				
				<a href="file:///C|/Users/akhare/Desktop/ebm/Howitworks.html"><img src="file:///C|/Users/akhare/Desktop/ebm/img/arrow_right.png" alt="Next"/></a>
				
				</div>
			<div class="clear"></div>
		</div>
	<div id="topic_content">
		
<p></p>
<p class="rvps3"><span class="rvts8">The basic explanation from the logging perspective would be anything that is required to be logged, it can be put inside a cold fusion component, the log function can be called and will show up after typing the commands.</span></p>
<p></p>
<p class="rvps2">&nbsp;</p>
</div>
	
	<div id="topic_footer">

			<div id="topic_footer_content">
				Copyright &copy; &lt;Dates&gt; by &lt;Authors&gt;. All Rights Reserved.</div>
		</div>
	</body>
	
</html>

