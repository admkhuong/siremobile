<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset currentAccount = permissionObject.getCurentUser()>

<cfset Session.USERROLE  = currentAccount.USERROLE>

<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignCreatePermission = permissionObject.havePermission(Create_Campaign_Title)>

<cfset marketingPermission = permissionObject.havePermission(Marketing_Title)>
<cfset surveyPermission = permissionObject.havePermission(Survey_Title)>
<cfset surveyCreatePermission = permissionObject.havePermission(Create_Survey_Title)>

<cfset contactPermission = permissionObject.havePermission(Contact_Title)>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactAddPermission = permissionObject.havePermission(Add_Contact_Title)>
<cfset contactAddBulkPermission = permissionObject.havePermission(Add_Bulk_Contact_Title)>
<cfset contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddBulkUploadPermission = permissionObject.havePermission(Add_Bulk_Groups_Title)>

<cfset reportingPermission = permissionObject.havePermission(Reporting_Title)>
<cfset reportingCampaignPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset reportingSurveyPermission = permissionObject.havePermission(Reporting_Surveys_Title)>
<cfset reportingSmsMessagingPermission = permissionObject.havePermission(Reporting_SmsMessagings_Title)>

<cfset cppPermission = permissionObject.havePermission(CPP_Title)>
<cfset cppCreatePermission = permissionObject.havePermission(CPP_Create_Title)>
<cfset cppEditPermission = permissionObject.havePermission(CPP_edit_Title)>
<cfset cppDeletePermission = permissionObject.havePermission(CPP_delete_Title)>
<cfset cppAgentPermission = permissionObject.havePermission(CPP_Agent_Title)>
<cfset smsPermission = permissionObject.havePermission(SMS_Campaigns_Management_Title)>
<!--- Get user balance --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">                     
</cfinvoke>

<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.administrator.usersTool"
	 method="getRate"
	 returnvariable="getRateData">                     
</cfinvoke>
<script type="text/javascript">
	var fullWidth;
	$(window).scroll(function() {
		if (fullWidth == undefined) {
			fullWidth = $("#headerPage").width();
		}
		var headerPageWidth = fullWidth + $(window).scrollLeft();
		$("#headerPage").width(headerPageWidth);
		
		$("#lineTop").width(headerPageWidth);
		$("#mainTitle").width(headerPageWidth);
		$("#subTitleContent").width(headerPageWidth);
		$("#mainPage").width(headerPageWidth);
		$("#subTitle").width(headerPageWidth);
		
	});
</script>

<body>
	<div id="lineTop" class ="container no-print"></div>
	
	<div id="headerPage" class ="container no-print">
		<div id="headContent">
			<a id="logoMB" href="<cfoutput>#rootUrl#/#sessionPath#/account/home</cfoutput>"></a>
			<ul>
				<cfoutput>
					<li <!---title="#Session.UserId#"--->>
						Welcome #currentAccount.FIRSTNAME#  #currentAccount.LASTNAME#  
					</li>
					<li>|</li>
					<li>
						<cfif RetValBillingData.RXRESULTCODE LT 1>
						    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
						</cfif>
					 	<!--- Calculate Billing--->
						<cfswitch expression="#RetValBillingData.RateType#">
					   		<!---SimpleX -  Rate 1 at Incrment 1--->
				  	 		<cfcase value="1">
							 	Funds Available: 
							 	<cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
								 	Unlimited
							 	<cfelse>
								 	#lseuroCurrencyFormat(RetValBillingData.Balance)#
							 	</cfif>
						 	 	
						   </cfcase>
						   <!---SimpleX -  Rate 2 at 1 per unit--->
						   <cfcase value="2">
								Funds Available: 
			 					<cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
								 	Unlimited
							 	<cfelse>
								 	#lseuroCurrencyFormat(RetValBillingData.Balance)#
								 </cfif>
						   </cfcase>
						   <cfdefaultcase>    
						 	 	Funds Available:
						 	 	<cfif RetValBillingData.UNLIMITEDBALANCE EQ 1>
								 	Unlimited
							 	<cfelse>
								 	#lseuroCurrencyFormat(RetValBillingData.Balance)#
								</cfif>
						   </cfdefaultcase>
						</cfswitch>
					</li>  
				</cfoutput>
				<li>|</li>
				<cfif getRateData.RXRESULTCODE GT 0>
					<li>Rate: <cfoutput> #lsEuroCurrencyFormat(getRateData.Rate)#</cfoutput></li>
					<li>|</li>
				</cfif>
				<li><a href ="#" id="help">Help Center</a> </li>
				<li>|</li>
				<li><a href ="#" id="logout">Log Off</a> </li>
			</ul>
		</div>
	</div>
	
	<div id="mainTitle" class ="container no-print">
		<span id="mainTitleText">Home</span>
		
		<a href="#" id = "forwardHistory" onClick ="return false;"></a>
		<a id = "showHistory" onClick ="return false;">Back</a>
		<a href="#" id = "backHistory" onClick ="return false;"></a>
        <a href = "../ebm/Introduction.html " id="devhelp"> Dev	Help</a>	
<div id="historyContent" >
			<ul id="listHistory"></ul>
			<div id="closeHistory">Close</div>
		</div>
		
	</div>
	<div id="subTitle" class ="container no-print">
		<div id="subTitleLeft" class="leftBackground">
		</div>
		<div id="subTitleContent">
			<!---<a href ="#" onClick="return false;" id="show_menu_left" title = "Show Navigation"><<<BR/></a>--->
			<a href ="#" onClick="return false;" id="show_menu_left" title="Show Navigation Menu"></a>
        
        	<span id="subTitleText">Sub Title</span>
			<a href ="#" id ="information"></a>
			<a href ="#" id ="setting"></a>
		</div>
	</div>
	
	<div id="mainPage" class="leftBackground">
    
		<div id="menu_left" class="no-print">
		   	<div id="menu_left_content" >
				<!---<a href ="#" onClick="return false;" id="slide_menu_left" title = "Hide Navigation"></a>--->
			 	<BR/>
                <cfoutput>
               		 <cfinclude template="metromenu.cfm"> 
                </cfoutput>
				<!---<div class="clear_both"></div>--->                
			</div>
            
		</div>
        
     <!---   <div style="clear:both"></div>
        <div style="clear:right"></div>--->
         
		<div id="maincontent" class="containerRight">
			<div id="innertube">
