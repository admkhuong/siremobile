﻿<html>
	
<head>
	<title>How it works?</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="generator" content="HelpNDoc Personal Edition 3.9.1.648">
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/reset.css" />
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/base.css" />
	<link type="text/css" rel="stylesheet" media="all" href="file:///C|/Users/akhare/Desktop/ebm/css/hnd.css" />
	<!--[if lte IE 8]>
		<link type="text/css" rel="stylesheet" media="all" href="css/ielte8.css" />
	<![endif]-->
	<style type="text/css">
		#topic_header
		{
			background-color: #EFEFEF;
		}
	</style>
	<script type="text/javascript" src="file:///C|/Users/akhare/Desktop/ebm/js/jquery.min.js"></script>
	<script type="text/javascript" src="file:///C|/Users/akhare/Desktop/ebm/js/hnd.js"></script>
	<script type="text/javascript">
		$(document).ready(function()
		{
			if (top.frames.length == 0)
			{
				var sTopicUrl = top.location.href.substring(top.location.href.lastIndexOf("/") + 1, top.location.href.length);
				top.location.href = "Event Based Messaging Documentation.html?" + sTopicUrl;
			}
			else if (top && top.FrameTOC && top.FrameTOC.SelectTocItem)
			{
				top.FrameTOC.SelectTocItem("Howitworks");
			}
		});
	</script>
</head>

<body>

	<div id="topic_header">
			<div id="topic_header_content">
				<h1>How it works?</h1>
				
				<div id="topic_breadcrumb">
					<a href="file:///C|/Users/akhare/Desktop/ebm/EMBLogging.html">EMB Logging</a> &rsaquo;&rsaquo; </div>
				</div>
			<div id="topic_header_nav">
				<a href="file:///C|/Users/akhare/Desktop/ebm/EMBLogging.html"><img src="file:///C|/Users/akhare/Desktop/ebm/img/arrow_up.png" alt="Parent"/></a>
				
				<a href="file:///C|/Users/akhare/Desktop/ebm/BriefDescription.html"><img src="file:///C|/Users/akhare/Desktop/ebm/img/arrow_left.png" alt="Previous"/></a>
				
				</div>
			<div class="clear"></div>
		</div>
	<div id="topic_content">
		
<p></p>
<p class="rvps3"><span class="rvts8">A cold fusion component named "userLogs.cfc" is located at the following location: Session / cfc / administrator / usersLogs.cfc . It contains the following functions:</span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts9"><br/></span></p>
<p class="rvps3"><span class="rvts9">1) createUserLog </span><span class="rvts8">: Basically used to insert the data into database table viz. userlogs. </span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts9">2) getUserLogs</span><span class="rvts8"> &nbsp; &nbsp;: Basically used to pull up the data from the database table viz. userlogs.</span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><img width="899" height="894" alt="" style="padding : 1px;" src="file:///C|/Users/akhare/Desktop/ebm/lib/Diagram1.jpg"/></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p class="rvps3"><span class="rvts8"><br/></span></p>
<p></p>
<p class="rvps2"><span class="rvts6">Created with the Personal Edition of HelpNDoc: </span><a class="rvts7" href="http://www.helpndoc.com/help-authoring-tool">Create HTML Help, DOC, PDF and print manuals from 1 single source</a></p>
</div>
	
	<div id="topic_footer">

			<div id="topic_footer_content">
				Copyright &copy; &lt;Dates&gt; by &lt;Authors&gt;. All Rights Reserved.</div>
		</div>
	</body>
	
</html>

