<cfparam name="INPBATCHID" default="0">
<cfparam name="INPPOSITION" default="1">
<cfparam name="INPPAGE" default="1">
<cfparam name="INPQID" default="0">
<cfparam name="INPCPGID" default="0">
<cfparam name="INPAFTER" default="0">
<cfparam name="INPBEFORE" default="0">
<cfparam name="INPCOMTYPE" default="SMS">

<cfoutput>
</cfoutput>


<!---<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
<cfset query = surveyObj.ReadXMLQuestions1(INPBATCHID)>--->


<!--- Read question data this RESPONSE keys off of --->        
<cfinvoke method="ReadXMLQuestionsOptimized" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="query">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>
                                        
                                        
<!---<cfdump var="#query#">--->

<!--- Set defaults for edit mode --->
<cfset RQ = 0>
<cfset BOQ = 0>
<cfset BOC = "=">
<cfset BOV = "">
<cfset BOAV = "">
<cfset BOCDV = "">
<cfset BOTNQ = 0>
<cfset BOFNQ = 0>
               
               

<style>
	#branching .question_label {
	    padding-left: 20px;
	    width: 25%;		
	}
		
	.sbOptions
	{
		max-height:250px !important;		
	}
				
	.ConditionBottomSpacer
	{			
		min-height:300px;		
	}
	
	.ConditionContainer
	{
		max-height: 450px;
		overflow: auto;
		padding: 10px 5px 10px 5px;
		width: 100%;
	}
	
	.smallLable 
	{	
		font-size:12px;	
		padding: 0px;
	}
	
</style>


<!---
 <style>
  .ui-tooltip, .arrow:after {
    background: black;
    border: 2px solid white;
  }
  .ui-tooltip {
    padding: 10px 20px;
    color: white;
    border-radius: 20px;
    font: bold 14px "Helvetica Neue", Sans-Serif;
    text-transform: uppercase;
    box-shadow: 0 0 7px black;
  }
  .arrow {
    width: 70px;
    height: 16px;
    overflow: hidden;
    position: absolute;
    left: 50%;
    margin-left: -35px;
    bottom: -16px;
  }
  .arrow.top {
    top: -16px;
    bottom: auto;
  }
  .arrow.left {
    left: 20%;
  }
  .arrow:after {
    content: "";
    position: absolute;
    left: 20px;
    top: -20px;
    width: 25px;
    height: 25px;
    box-shadow: 6px 5px 9px -9px black;
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    tranform: rotate(45deg);
  }
  .arrow.top:after {
    bottom: -20px;
    top: auto;
  }
  </style>
  --->
  
  
  

<script type="text/javascript">
	function InitConditions()
	{
		ConditionCount = 0;
		
		<!--- To get to edit mode supply a INPQID --->               
		<cfif INPQID GT 0>
		
			<!--- Read question data for this BRANCH  --->        
			<cfinvoke method="ReadQuestionDataById" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarReadQuestionDataByIdEditBO">
				<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				<cfinvokeargument name="inpQID" value="#INPQID#">
				<cfinvokeargument name="inpIDKey" value="ID">
			</cfinvoke>
						
			<cfif RetVarReadQuestionDataByIdEditBO.RXRESULTCODE GT 0>                                            
				<cfif arrayLen(RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION) GT 0>                            
					<cfif arrayLen(RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1]) GT 0>
						
						<!--- Update description--->		
						$("#QuestionDesc").val(<cfoutput>'#RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].TEXT#'</cfoutput>);	
						
						<!--- BOFNQ --->
						CurrBOFNQ = $("#ddlBOFNQ");
						CurrBOFNQ.val(<cfoutput>'#RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].BOFNQ#'</cfoutput>);
						
						<!---$("#ddlBOFNQ").selectbox(
							{
									onOpen: function (inst) 
									{																					
										var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
										var CurrSelection = "a[rel='" + $("#ddlBOFNQ").val() + "']";
										
										if($("#" + CurrObjId).find(CurrSelection).length > 0)
											$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
									}
							 });	
							 --->
																	
						<cfset RQ = RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].RQ>
											 												
						var CurrBOQ =  "";
						var LocalConditionCount = 0;
															
						<!--- Put loop here --->
						<cfloop array="#RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].Conditions#" index="CondIndex">
						
							LocalConditionCount++;
							
							<cfif CondIndex.Type NEQ ""> 
								AddMoreResponseCondition(<cfoutput>'#CondIndex.Type#'</cfoutput>) ;																
							</cfif>
							
							<cfif CondIndex.Type EQ "RESPONSE" >
														
								<!--- BOQ --->
								CurrBOQ = $("#ddlBranchQuestion_"  + LocalConditionCount);
								CurrBOQ.val(<cfoutput>'#CondIndex.BOQ#'</cfoutput>);
								
								<!---$("#ddlBranchQuestion_"  + LocalConditionCount).selectbox(
								{
										onOpen: function (inst) 
										{																					
											var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
											var CurrSelection = "a[rel='" + $("#" + $(inst).attr('id')).val() + "']";
																																
											if($("#" + CurrObjId).find(CurrSelection).length > 0)
												$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
										}
								 });	--->
								 
								BindAnswerList(LocalConditionCount);	
		
								<!--- BOC --->
								CurrBOC = $("#ddlOperator_"  + LocalConditionCount);
								CurrBOC.val(<cfoutput>'#CondIndex.BOC#'</cfoutput>);
								
								<!--- BOV --->									
								var outBOV = <cfoutput>'#CondIndex.BOV#'</cfoutput>;
								var arrayArea = outBOV.split(',');
								$("#ddlAnswers_"  + LocalConditionCount).val(arrayArea);
								
								<!--- BOAV --->
								CurrBOAV = $("#INPBOAV_"  + LocalConditionCount);
								CurrBOAV.val(<cfoutput>'#CondIndex.BOAV#'</cfoutput>);
																			
								<!--- BOTNQ --->
								CurrBOTNQ = $("#ddlBOTNQ_"  + LocalConditionCount);
								CurrBOTNQ.val(<cfoutput>'#CondIndex.BOTNQ#'</cfoutput>);
								
								<!--- Be kind - preselect already seleced in drop down list ....--->
							<!---	$("#ddlBOTNQ_" + LocalConditionCount).selectbox(
													{
															onOpen: function (inst) 
															{																					
																var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
																var CurrSelection = "a[rel='" + $("#" + $(inst).attr('id')).val() + "']";
																
																if($("#" + CurrObjId).find(CurrSelection).length > 0)
																	$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
															}
													 });	
							--->
		
								
							<cfelseif CondIndex.Type EQ "CDF">
							
								<!--- BOCDV --->
								CurrBOCDV = $("#INPBOCDV_"  + LocalConditionCount);
								CurrBOCDV.val(<cfoutput>'#CondIndex.BOCDV#'</cfoutput>);
							
								<!--- BOC --->
								CurrBOC = $("#ddlOperator_"  + LocalConditionCount);
								CurrBOC.val(<cfoutput>'#CondIndex.BOC#'</cfoutput>);
																	
								<!--- BOAV --->
								CurrBOAV = $("#INPBOAV_"  + LocalConditionCount);
								CurrBOAV.val(<cfoutput>'#CondIndex.BOAV#'</cfoutput>);
																			
								<!--- BOTNQ --->
								CurrBOTNQ = $("#ddlBOTNQ_"  + LocalConditionCount);
								CurrBOTNQ.val(<cfoutput>'#CondIndex.BOTNQ#'</cfoutput>);
								
								<!---<!--- Be kind - preselect already seleced in drop down list ....--->
								$("#ddlBOTNQ_" + LocalConditionCount).selectbox(
								{
										onOpen: function (inst) 
										{																					
											var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
											var CurrSelection = "a[rel='" + $("#" + $(inst).attr('id')).val() + "']";
											
											if($("#" + CurrObjId).find(CurrSelection).length > 0)
												$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
										}
								 });	--->
		
		
							
							</cfif>
							
							<!--- Initialize values based on DB data--->
							
							<!---<cfset RQ = #RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].RQ#> 
							<cfset BOQ = #CondIndex.BOQ#>
							<cfset BOC = #CondIndex.BOC#>
							<cfset BOV = #CondIndex.BOV#>
							<cfset BOAV = #CondIndex.BOAV#>
							<cfset BOCDV = #CondIndex.BOCDV#>
							<cfset BOTNQ = #CondIndex.BOTNQ#>
							<cfset BOFNQ = #RetVarReadQuestionDataByIdEditBO.ARRAYQUESTION[1][1].BOFNQ#>--->
						
						<!---	$("#FreeFormHelp_" + LocalConditionCount).hover(function(){
								
								$("#FreeFormHelp_" + LocalConditionCount + " #info-box1").toggle();
								$("#FreeFormHelp_" + LocalConditionCount + " #tool2").toggle();
								//$("#tool2").css('margin-top', 122 + $('#vanityText').height() + 'px');
								//$("#info-box1").css('margin-top', 102 + $('#vanityText').height() + 'px');
								// $("#FreeFormHelp_" + LocalConditionCount + " .info-box").hide();
								 $("#FreeFormHelp_" + LocalConditionCount + " #info-box2").hide();
							  });--->
		  
		  
					<!---	  $("#FreeFormHelp_" + LocalConditionCount).tooltip({
					  
								content: function() 
								{
									var element = $( this );
									
									  var text = element.text();
									  return '<span id="info-txt-blue1">Information - Choose Layout</span><br /><span id="info-txt-black1">Customers may either fillout one short form or fill in information one section per page at a time.</span>';
								}
							});--->
	
						
						</cfloop>
				
										   
					</cfif>                         
				
				<cfelse>
								
				</cfif>   
				
			<cfelse>
										
			</cfif>
		<cfelse>
		
			<!---$("#ddlBOFNQ").selectbox(
			{
					onOpen: function (inst) 
					{																					
						var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
						var CurrSelection = "a[rel='" + $("#ddlBOFNQ").val() + "']";
						
						if($("#" + CurrObjId).find(CurrSelection).length > 0)
							$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
					}
			 });	--->
							 
		
		</cfif>
		
		var Container = $("#CONDITIONS");
		Container.scrollTop(0);		
		
	}   
</script> 

<!---<cfif INPQID GT 0>
	<cfdump var="#RetVarReadQuestionDataByIdEditBO#">
</cfif>--->

<script type="text/javascript">
	
	var allQuestions = <cfoutput>#SerializeJSON(query.ARRAYQUESTION)#</cfoutput>
	<!---//var <cfoutput>#ToScript(query.ARRAYQUESTION,"allQuestions")#</cfoutput>--->
	<!---var <cfoutput>#ToScript(query.ARRAYQUESTION,"allQuestions")#</cfoutput>--->
	
	var ConditionCount = 0;
	
	
	$(function() 
	{		
	
		$("#q_des_err").hide();
	
		$('#BranchEditorTitle').html('<cfoutput>Branch Logic <cfif INPQID GT 0> - Edit Mode CP#RQ#</cfif></cfoutput>');
			
		$('#inpSubmitBranchLogic').click(function(){
			
			<!--- To get to edit mode supply a INPQID --->  
			<cfif INPQID GT 0>
				AddCondition(true);
			<cfelse>
				AddCondition(false);
			</cfif>			
		});			 	
				
		InitConditions();	
		
		<!--- Initialize help tool tips for condition boxes --->
		 $('.more_info_box_cond').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).find('.tooltiptext')
				 },
				style: {
					classes: 'qtip-bootstrap qtip-shadow qtip-rounded'
				}
			 });
		 });
						 					
	});
	
	// validate CP title
	function isValidQuestionTitle()
	{
		if($.trim($("#QuestionDesc").val()) == ''){
			$("#q_des_err").show();
						
			//$("#QuestionDesc").show();
			return false;
		}else{
			//$("#QuestionDesc").show();
			$("#q_des_err").hide();
			return true;
		}
	}

	function BindAnswerList(inpConditionId) {
	
		var selectedQuestionId = $('#ddlBranchQuestion_' + inpConditionId).val();
		
		$('#ddlAnswers_' + inpConditionId).remove();				
		
		$('#AnswersContainer_' + inpConditionId).html('<select multiple size="3" class="ddlAnswers" id="ddlAnswers_' + inpConditionId + '" style="width: 100%;  max-height:55px;"></select>');
		
		var isSelectedItem = false;
		for(var i = 0; i < allQuestions.length; ++i) {
			
			var question = allQuestions[i];
			<!---var str = inpBOV;
			var strAV = inpBOAV;--->
			
			if (selectedQuestionId == question.ID)
			{
		
			//	$('#ddlAnswers_' + inpConditionId).append('<option value="0" selected="selected">Select Answer(s)</option>')
						
				for(var j = 0; j < question.LISTANSWER.length; ++j) 
				{
					
					isSelectedItem = false;
					
					<!--- Selection(s) now set from above in Init function--->
				<!---	<!--- If current option is part of input answer - and is selected - preselect it.--->
					if (jQuery.inArray(question.LISTANSWER[j].ID, str.replace(/,\s+/g, ',').split(',')) >= 0) 
					{
    					//Found it!
						if(parseInt(<cfoutput>'#INPQID#'</cfoutput>) > 0 && parseInt(<cfoutput>'#BOQ#'</cfoutput>) == question.ID)
							isSelectedItem = true;
					}
				--->

					if (isSelectedItem)
					{
						$('#ddlAnswers_' + inpConditionId).append('<option value="' + question.LISTANSWER[j].ID + '" selected="selected">' + question.LISTANSWER[j].TEXT + '</option>')
									
					}
					else
					{
						$('#ddlAnswers_' + inpConditionId).append('<option value="' + question.LISTANSWER[j].ID + '">' + question.LISTANSWER[j].TEXT + '</option>')						
					}
				}
				
				
							
			}	
		}
								
	}
	
	
	function AddCondition(inpEditMode) 
	{		
		var INPBOVCSV = "";
	
		if(!isValidQuestionTitle())
			return false;
	
		var BRANCHDesc = $("#QuestionDesc").val();
	
		if(inpEditMode)
		{<!--- inpEditMode--->
			
			
			var INPBOValues = [];
			var INPBOVID = [];
			var xmlQuestion='';	
			
			
						
			xmlQuestion += "<Q ID='" + '<cfoutput>#INPQID#</cfoutput>' + "' TYPE='" + 'BRANCH' + "' RQ='" + '<cfoutput>#INPPOSITION#</cfoutput>'
								+ "' TEXT='" + BRANCHDesc + "' GID='" + '<cfoutput>#INPPAGE#</cfoutput>' + "' " + "AF='" +  '' + "' "  
								+ "ITYPE='" +  '' + "' " + "IVALUE='" +  '' + "' "
								+ "REQANS='" +  '' + "' errMsgTxt='" + '' + "' BOFNQ='" + $('#ddlBOFNQ').val() + "'" + ">";
								
								
			var CondXML = "";
			
			$('.ConditionItem').each(function(i, selectedElement) {
			 							
				var CurrCondIndex = $(selectedElement).attr('CID');
												
				INPBOVCSV = "";
						
				$('#ddlAnswers_' + CurrCondIndex + ' :selected').each(function(i, selectedElement) {
				 
					 if(INPBOVCSV == "")
						 INPBOVCSV = INPBOVCSV + $(selectedElement).val();
					 else
						INPBOVCSV = INPBOVCSV + "," + $(selectedElement).val();	
			
					<!---hexvalues[i] = $(selectedElement).val();--->
					<!---labelvalues[i] = $(selectedElement).text();--->
				});
				
				
				CondXML = CondXML + "<COND ";
				
				CondXML = CondXML + "CID='" + $('#divBranchItem_' + CurrCondIndex).attr('CID') + "' ";
				CondXML = CondXML + "BOQ='" + $('#ddlBranchQuestion_' + CurrCondIndex).val() + "' ";		
				CondXML = CondXML + "BOC='" + $('#ddlOperator_' + CurrCondIndex).val() + "' ";	
				CondXML = CondXML + "BOV='" + INPBOVCSV + "' ";	
				CondXML = CondXML + "BOAV='" + $('#INPBOAV_' + CurrCondIndex).val() + "' ";	
				CondXML = CondXML + "BOCDV='" + $('#INPBOCDV_' + CurrCondIndex).val() + "' ";	
				CondXML = CondXML + "BOTNQ='" + $('#ddlBOTNQ_' + CurrCondIndex).val() + "' ";	
				CondXML = CondXML + "TYPE='" + $('#divBranchItem_' + CurrCondIndex).attr('ctype') + "' ";	
				
				
				CondXML = CondXML + "/>"
			
							
				<!---hexvalues[i] = $(selectedElement).val();--->
			 	<!---labelvalues[i] = $(selectedElement).text();--->
			});
			
			xmlQuestion = xmlQuestion + CondXML + "</Q>";
			<!---console.log(xmlQuestion);
			return false;	--->	
			
			<!--- Update question values --->
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/SMSSurvey.cfc?method=UpdateQuestionBranchOptions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID: '<cfoutput>#INPBATCHID#</cfoutput>', 
					INPQID: '<cfoutput>#INPQID#</cfoutput>' , 
					INPQXML : xmlQuestion,
					INPTYPE : '<cfoutput>#INPCOMTYPE#</cfoutput>'
					},							  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- --->
				function(d) 
				{																										
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								<!--- Update display --->
								$("#TableSurveyQuestions li[rq='" + '<cfoutput>#INPPOSITION#</cfoutput>' + "']").find(".SurveyQuestionText").html(BRANCHDesc);
								
								<!--- Draw conditions --->
								
								$('#CPBranchModal').modal('hide');	
								
								<!--- Todo: Finish and then Disable auto refresh for now --->
								var QLableNav = '';
								
								if(parseInt('<cfoutput>#INPQID#</cfoutput>')  > 0)
									QLableNav = '&QNav=' + (parseInt('<cfoutput>#INPQID#</cfoutput>') );
								
								// window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editquestions/index?inpbatchid=" + <cfoutput>#INPBATCHID#</cfoutput> + "&PAGE=" + <cfoutput>#INPPAGE#</cfoutput> + QLableNav;	
								return true;		
							}
							else
							{
								bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->								
						bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
					}
									
				} 		
				
			});
			
			
		}<!--- inpEditMode--->
		else
		{<!--- NOT inpEditMode--->
						
			var AnswersBuff = new Array();
			var AnswersVal = new Array();
										
			<!--- Validate form inputs --->						
			if ($("#BranchLogicInlineForm").valid()) 
			{
				var isRequiredAnswer = $("#requiredAns").is(":checked");
			
				if(isRequiredAnswer)
				{
					// Check errMsgTxt
					if($("#errMsgText").val() == '')
					{
						$("#errMsgText").focus();
						$("#errRequiredMsg").show();
						return false;
					}
					else
					{
						$("#errRequiredMsg").hide();
					}
				}				
								
				if (communicationType != null) 
				{				
				
				
					var CondXMLArray = new Array();
			
					var CondXML = "";			
					$('.ConditionItem').each(function(i, selectedElement) {
			 							
						var CurrCondIndex = $(selectedElement).attr('CID');
						INPBOVCSV = "";
									
						$('#ddlAnswers_' + CurrCondIndex + ' :selected').each(function(i, selectedElement) {
						 
							 if(INPBOVCSV == "")
								 INPBOVCSV = INPBOVCSV + $(selectedElement).val();
							 else
								INPBOVCSV = INPBOVCSV + "," + $(selectedElement).val();	
					
							<!---hexvalues[i] = $(selectedElement).val();--->
							<!---labelvalues[i] = $(selectedElement).text();--->
							
						});
												
						CondXML = CondXML + "<COND ";
						
						CondXML = CondXML + "CID='" + $('#divBranchItem_' + CurrCondIndex).attr('CID') + "' ";
						CondXML = CondXML + "BOQ='" + $('#ddlBranchQuestion_' + CurrCondIndex).val() + "' ";		
						CondXML = CondXML + "BOC='" + $('#ddlOperator_' + CurrCondIndex).val() + "' ";	
						CondXML = CondXML + "BOV='" + INPBOVCSV + "' ";	
						CondXML = CondXML + "BOAV='" + $('#INPBOAV_' + CurrCondIndex).val() + "' ";	
						CondXML = CondXML + "BOCDV='" + $('#INPBOCDV_' + CurrCondIndex).val() + "' ";	
						CondXML = CondXML + "BOTNQ='" + $('#ddlBOTNQ_' + CurrCondIndex).val() + "' ";	
						CondXML = CondXML + "TYPE='" + $('#divBranchItem_' + CurrCondIndex).attr('ctype') + "' ";	
												
						CondXML = CondXML + "/>"
						
						
						var CondObj = new Object();
			
						CondObj.ID = $('#divBranchItem_' + CurrCondIndex).attr('CID');
						CondObj.CID = $('#divBranchItem_' + CurrCondIndex).attr('CID');
						CondObj.TEXT = '';
						CondObj.BOAV = $('#INPBOAV_' + CurrCondIndex).val();
						CondObj.BOC = $('#ddlOperator_' + CurrCondIndex).val();
						CondObj.BOCDV = $('#INPBOCDV_' + CurrCondIndex).val();
						CondObj.BOQ = $('#ddlBranchQuestion_' + CurrCondIndex).val();
						CondObj.BOTNQ = $('#ddlBOTNQ_' + CurrCondIndex).val();
						CondObj.BOV = INPBOVCSV;
						CondObj.TYPE = $('#divBranchItem_' + CurrCondIndex).attr('ctype');
						 
						CondXMLArray.push(CondObj);	
				
														
						<!---hexvalues[i] = $(selectedElement).val();--->
						<!---labelvalues[i] = $(selectedElement).text();--->
					});
				
				
				<!---console.log(CondXML);
				return;--->
														
					<!--- SaveQuestionsToXML(communicationType, <cfoutput>#INPPOSITION#</cfoutput>);--->
					AddNewQuestion('<cfoutput>#INPBEFORE#</cfoutput>', '<cfoutput>#INPAFTER#</cfoutput>', '<cfoutput>#INPCPGID#</cfoutput>', BRANCHDesc, 'BRANCH', AnswersBuff, AnswersVal, '', '', communicationType, false, false, 0, <cfoutput>#INPPAGE#</cfoutput>, <cfoutput>#INPPOSITION#</cfoutput>, false, '', '', '', '', '', '', '','', '', '0', 'true', CondXML, $('#ddlBOFNQ').val(), CondXMLArray); 
					pageQuestionNumber++;
					
					$('#CPBranchModal').modal('hide');
					
					if (communicationType != SURVEY_COMMUNICATION_PHONE) 
					{
						//$("#pager2").show();
					}					
				}
												
				<!---
				// window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editquestions/index?inpbatchid=" + <cfoutput>#INPBATCHID#</cfoutput> + "&PAGE=" + <cfoutput>#INPPAGE#</cfoutput>;
				--->
				return true; 	
			}
			else
			{
				 return false;
			}	
		
		}<!--- NOT inpEditMode--->
		
	}
	
	
	function AddMoreResponseCondition(inpConditionType) 
	{
		ConditionCount++;
		var ConditionObj = CreateConditionObject(ConditionCount, '', inpConditionType);
				
		$("#tmplBranchItem").tmpl(ConditionObj).appendTo('#divBranchList');		
				
		<cfif INPQID EQ 0>
			<!---$("#ddlBOTNQ_" + ConditionCount).selectbox(
														{
																onOpen: function (inst) 
																{																					
																	var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
																	var CurrSelection = "a[rel='" + $("#" + $(inst).attr('id')).val() + "']";
																	
																	if($("#" + CurrObjId).find(CurrSelection).length > 0)
																		$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
																}
														 });	--->
		
		</cfif>
		
		var CIRBObj = $("#divBranchItem_" + ConditionCount).find($('.ConditionItemRemoveButton'));
		CIRBObj.attr('onClick', 'RemoveBranch(' + ConditionCount + ');');
				
		if(ConditionCount > 1 && inpConditionType == 'RESPONSE')
		{
			<!--- Details: Default currently selected question to the last one selected --->			
			$("#ddlBranchQuestion_"  + ConditionCount).val($("#ddlBranchQuestion_" + (ConditionCount-1)).val());
		}		
		
		if(inpConditionType == 'RESPONSE')
		{	
		
			<cfif INPQID EQ 0>
		
				<!---$("#ddlBranchQuestion_"  + ConditionCount).selectbox(
								{
										onOpen: function (inst) 
										{																					
											var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
											var CurrSelection = "a[rel='" + $("#" + $(inst).attr('id')).val() + "']";
											
																					
											if($("#" + CurrObjId).find(CurrSelection).length > 0)
												$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
										}
								 });	--->
			</cfif>					 
								 
			
	
			BindAnswerList(ConditionCount);	
		}	
				
		<!--- Bring the newly created condition into user view --->
		<!---$("#CONDITIONS").scrollTop($("#CONDITIONS")[0].scrollHeight);--->
		var Container = $("#CONDITIONS");
		Container.scrollTop($("#divBranchItem_"+ ConditionCount).offset().top - Container.offset().top + Container.scrollTop()) ;
	}
	
	
	function CreateConditionObject(i, desc, inpConditionType) {
		var ConditionObj = new Object();
		ConditionObj.i = i;
		ConditionObj.ConditionDesc = desc;
		ConditionObj.ConditionType = inpConditionType;
		
		return ConditionObj;
	}
	
	function RemoveBranch(i) {
					
		$('#divBranchItem_' + i).remove();
		ConditionCount--;
		
		<!--- Reorder on delete--->
		reIndexConditions();				
	}
	
	
	function reIndexConditions()
	{
			
		var position = 1;
		$.each($('.ConditionItem'), function(index, value) {
			$(this).attr('id', 'divBranchItem_' + position);
			$(this).attr('CID', position);
						
			<!--- These are not in all conditions so initialize these one here so id's stay in sequence --->
			var BOQObj = $(this).find($('.ddlBranchQuestion'));
			BOQObj.attr('id', 'ddlBranchQuestion_' + position);
			BOQObj.attr('onChange', 'BindAnswerList(' + position + ');');
		
			var AnswersObj = $(this).find($('.ddlAnswers'));
			AnswersObj.attr('id', 'ddlAnswers_' + position);
			
			var CDVObj = $(this).find($('.INPBOCDV'));
			CDVObj.attr('id', 'INPBOCDV_' + position);
			
			<!--- Keep this at bottom if you add more items--->
			position = position + 1;
		});
				
		position = 1;
		$.each($('.ConditionItemRemoveButton'), function(index, value) {
			$(this).attr('id', 'ConditionItemRemoveButton_' + position);
			$(this).unbind('click');		
			$(this).attr('onClick', 'RemoveBranch(' + position + ');');
			position = position + 1;
		});
						
		position = 1
		$.each($('.Conditionlabel_'), function(index, value) {
			$(this).html("C" + position);
			position = position + 1;
		});
				
		var position = 1;
		$.each($('.ddlOperator'), function(index, value) {
			$(this).attr('id', 'ddlOperator_' + position);
			position = position + 1;
		});				
		
		var position = 1;
		$.each($('.INPBOAV'), function(index, value) {
			$(this).attr('id', 'INPBOAV_' + position);
			position = position + 1;
		});
		
		var position = 1;
		$.each($('.ddlBOTNQ'), function(index, value) {
			$(this).attr('id', 'ddlBOTNQ_' + position);
			position = position + 1;
		});	
				
					
	}
	
	
</script>

<cfoutput>



<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="BranchEditorTitle">Branch Editor</h4>        
</div>

<div class="modal-body">
                  
    <form id="BranchLogicInlineForm" method="POST">              
                                     
        <div class="clear">
            
            <div class="qe-label-a">Branch Description:</div> 
            <label class='ErrorColor' id='q_des_err'>*This field's required.</label>
        
            <textarea  
                maxlength="1000"
                id="QuestionDesc" name="QuestionDesc" role="textbox" aria-autocomplete="list" 
                aria-haspopup="true"
                class="textarea" style="width:100%; height:2em;"	
            >Branch</textarea>
                
        </div>
                                       
        <div>                           
            <button style="margin-top:.8em;" type="button" class="btn btn-default" onclick="AddMoreResponseCondition('RESPONSE')">Add Response Condition</button>  
            <button style="margin-top:.8em;" type="button" class="btn btn-default" onclick="AddMoreResponseCondition('CDF')">Add Data Condition</button>  
        </div>
   
        <div style="margin-top:.8em;">
            Branch on first Match: Condition(s)
        </div>
                           
        <div id="CONDITIONS" class="ConditionContainer">
            
            <div id="divBranchList"></div>
            
        </div>
            
        <!--- BrachOptionFalseNextQuestion --->        
        <div class="clear row_padding_top">
           
            <label class="qe-label-a">Condition(s) NOT-Satisfied Jump To Question:</label>
            <select id="ddlBOFNQ" style="width: 100%; max-height:50px;" onchange="">	
                
                <option value="0" selected>-- Default to Next Question --</option>
                <cfset isSelectedItem = false>
                <cfloop array="#query.ARRAYQUESTION#" index="question">
                    <cfif question.TYPE NEQ "GARBAGE">
                        <!--- <cfif NOT isSelectedItem> <cfset isSelectedItem = true> selected </cfif> --->	
                        <option value="<cfoutput>#question.ID#</cfoutput>" <cfif INPQID GT 0 AND BOFNQ EQ question.ID>SELECTED</cfif> >
                            <cfoutput>CP #question.RQ#. #question.TEXT#</cfoutput>
                        </option>
                    </cfif>
                </cfloop>
            </select>
           
        </div>     
               
    </form>
            
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
     
	<button type="button" class="btn btn-primary" id="inpSubmitBranchLogic">Save Changes</button>  

</div>	

</cfoutput>

<!--- http://stephenwalther.com/archive/2010/11/30/an-introduction-to-jquery-templates --->
<script id="tmplBranchItem" type="text/x-jquery-tmpl">
		
		<div class="Branch ConditionItem" id="divBranchItem_${i}" ctype='${ConditionType}' CID='${i}'>
				
			<div class="" style="margin: 5px 0 0; padding-right: 5px; position:absolute; top:5px; right:10px;">
				<label style="display:inline; margin-right:1em;"><span class="Conditionlabel_">C${i}</span></label>
				<!---<input id="ConditionItemRemoveButton_${i}" type="button" value="x" class="pointer button filterButton small ConditionItemRemoveButton"/>--->
				<button id="ConditionItemRemoveButton_${i}" class="close ConditionItemRemoveButton" aria-hidden="true" type="button">X</button>
			
			</div>	
			
			<!--- If this is a question response condition --->
			{{if ConditionType == 'RESPONSE'}}
          
			   	<div class="question_label">
					<label class="qe-label-a">Response to Question:</label>
					<div class="smallLable">Required for match.</div>
				</div>
				<div class="">
					<select id="ddlBranchQuestion_${i}" class="ddlBranchQuestion" style="width: 100%; max-height:50px;" onChange="BindAnswerList(${i})">	
						
						<option value="0" selected>-- Select Question --</option>
						<cfset isSelectedItem = false>
						<cfloop array="#query.ARRAYQUESTION#" index="question">
							<cfif question.TYPE NEQ "GARBAGE">
								
								<!--- <cfif NOT isSelectedItem> <cfset isSelectedItem = true> selected </cfif> --->	
								<option value="<cfoutput>#question.ID#</cfoutput>">
									<cfoutput>CP #question.RQ#. #question.TEXT#</cfoutput>
								</option>
								 
							</cfif>
						</cfloop>
					</select>
				</div>
			
    		{{/if}}
			
			<!--- If this is a Custom Data Fieled condition --->
			{{if ConditionType == 'CDF'}}          
			   	                
				<div class="question_label">
					<label class="qe-label-a">Client Data Field(s):</label>
					<div class="smallLable">Each of the comma seperated items in the list you input here will be checked for match.</div>
				</div>				           
				
				<div class="inputbox-container">
					<input class="INPBOCDV" id="INPBOCDV_${i}" name="INPBOCDV_${i}" placeholder="Enter Client Data to Match Against Here" style="width: 100%; max-height:50px;" value="" />
				</div>
								  
			
    		{{/if}}							
			
			<div class="clear row_padding_top" style="width:100%;">
				<div class="question_label">
					<label class="qe-label-a">Comparison Operator:</label>
				</div>
				<div class="">
					<select id="ddlOperator_${i}" class="ddlOperator">	
						<option selected="selected" value="=" <cfif INPQID GT 0 AND BOC EQ "=">SELECTED</cfif> >Equal <!---- Use Answer Choice, Freeform CSV, or Freeform Regular expression. Note: NOT CASE sensitive---></option>
						<option value="LIKE" <cfif INPQID GT 0 AND BOC EQ "LIKE">SELECTED</cfif> >Similar<!--- - Specify Static value, CSV, or Regular expression---></option>
						<option value="&amp;lt;" <cfif INPQID GT 0 AND BOC EQ "<">SELECTED</cfif> >Less than</option>
						<option value="&amp;gt;" <cfif INPQID GT 0 AND BOC EQ ">">SELECTED</cfif> >More than</option>
						<option value="&amp;lt;=" <cfif INPQID GT 0 AND BOC EQ "<=">SELECTED</cfif> ><=</option>
						<option value="&amp;gt;=" <cfif INPQID GT 0 AND BOC EQ ">=">SELECTED</cfif> >>=</option>                                    
					</select>
				</div>
			</div>						
						
			<!--- Hold down the Ctrl (windows) / Command (Mac) button to select multiple options. --->			
			<!--- If this is a question response condition --->
			{{if ConditionType == 'RESPONSE'}}	
							
				<div class="clear row_padding_top">
					 <div class="question_label">
						<label class="qe-label-a">Answer Choice(s):</label>
					</div>
					<div class="" id="AnswersContainer_${i}">
						<select multiple size="3" class="ddlAnswers" id="ddlAnswers_${i}" style="width: 100%;  max-height:55px;">	
							
						</select>
					</div>
				</div>		
				
			{{/if}}
			
			
			
				<div class="question_label" style="position:static;">
				
						<div style="margin-top:14px;">
							<label class="qe-label-a">Free Form Answer(s):</label>
						</div>
		
						<div class="more_info_box more_info_box_cond" id="FreeFormHelp_${i}" style="">
							<!---<div class="tooltiptext">
									<!---Complex <b>inline</b> <i>HTML</i> in your <u>config</u>--->
									Each of the comma seperated items in the list you input here will be checked for match. <BR>
									<b>OR</b><BR/> in the case of "=" you can specify a Regular Expression to match - use the tag REGEXP followed by any Regular Expression
									<!---<span>Information - Choose Layout</span><br /><span id="">Customers may either fillout one short form or fill in information one section per page at a time.</span>--->
							</div>		--->										
						</div>
					
				<!---	<div class="smallLable">Each of the comma seperated items in the list you input here will be checked for match. <sapn title='OR in the case of "=" you can specify a Regular Expression to match - use the tag REGEXP followed by any Regular Expression ' >more...</span></div>	
				--->					
					
								                            
				
				<div class="inputbox-container">
					<input class="INPBOAV" id="INPBOAV_${i}" name="INPBOAV_${i}" placeholder="Enter Answer(s) or REGEXP Here" style="width: 100%; max-height:50px;" value="" />
				</div>
				
			</div>     
			
    			
			
			
			<!--- BrachOptionTrueNextQuestion --->        
			<div class="clear row_padding_top">
				 <div class="question_label">
					<label class="qe-label-a">Condition Satisfied Jump To Question:</label>
				</div>
				<div class="">
					<select class="ddlBOTNQ" id="ddlBOTNQ_${i}" style="width: 100%; max-height:50px;" onchange="">	
						
						<option value="0" selected>-- Default to Next Question --</option>
						<cfset isSelectedItem = false>
						<cfloop array="#query.ARRAYQUESTION#" index="question">
							<cfif question.TYPE NEQ "GARBAGE">
								<!--- <cfif NOT isSelectedItem> <cfset isSelectedItem = true> selected </cfif> --->	
								<option value="<cfoutput>#question.ID#</cfoutput>" >
									<cfoutput>CP #question.RQ#. #question.TEXT#</cfoutput>
								</option>
							</cfif>
						</cfloop>
					</select>
				</div>
			</div>
		
		</div>
		
		<div class="clear row_padding_top"></div>
			
</script>