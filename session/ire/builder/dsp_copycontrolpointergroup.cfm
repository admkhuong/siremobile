<cfparam name="INPBATCHID" default="0">
<cfparam name="inpCPGID" default="0">
<cfparam name="INPTYPE" default="">
<cfparam name="INPPAGE" default="1">

<!---
<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
<cfset query = surveyObj.ReadXMLQuestions1(INPBATCHID)>
--->

<!--- Read question data this RESPONSE keys off of --->        
<cfinvoke method="ReadXMLQuestionsOptimized" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="query">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<style>
	#branching .question_label {
	    padding-left: 20px;
	    width: 25%;		
	}
	
	.sbHolder
	{
		width:428px;	
		border-radius: 3px 3px 3px 3px;			
	}
	
	<!--- 30 less than sbHolder --->	
	.sbSelector
	{
		width:400px;		
	}
	
	.sbOptions
	{
		max-height:250px !important;		
	}
	
</style>
	
<cfoutput>

	
        
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Insert Copy of data to after Control Point selected below</h4>     
</div>

<div class="modal-body">  

    <div style="padding-bottom: 10px; padding-top: 10px;">
            
        <!--- BrachOptionFalseNextQuestion --->        
        <div class="clear row_padding_top">
            <div class="question_label">
                <label class="qe-label-a">Insert Copy of data to after Control Point selected below</label>
            </div>
            <div class="left">
                <select id="ddlCPGIA" style="width: 100%; max-height:50px;" onchange="">	
                    <option value="0" selected>-- Place at top --</option>
                    <cfloop array="#query.ARRAYQUESTION#" index="question">
                        <cfif question.TYPE NEQ "GARBAGE">
                            <!--- Get RQ for CP copy - different than ID --->	
                            <option value="<cfoutput>#question.RQ#</cfoutput>" >
                                <cfoutput>CP #question.RQ#.#question.TEXT#</cfoutput>
                            </option>
                        </cfif>
                    </cfloop>
                </select>
            </div>
        </div>    
    
    </div>
    
    <div id="loadingDlgCopyCPG" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
           
</div>		

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        <button type="button" class="btn btn-primary" onclick="SaveCPG();">Save Changes</button>
    
    </div>	
</div>   

</cfoutput>

<script TYPE="text/javascript">
	function SaveCPG(INPBATCHID) {
		
		$("#loadingDlgCopyCPG").show();		
			
		if($("#inpDesc").val() == '')
		{
			
			bootbox.alert("Control Point Group description has not been updated.\n New description can not be blank." + "\n", function(result) { } );										
			$("#loadingDlgCopyCPG").hide();	
			return;	
		}
			
		var data =  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPTYPE: '<cfoutput>#INPTYPE#</cfoutput>',
						inpCPGID: '<cfoutput>#inpCPGID#</cfoutput>', 
						inpInsertAfterQID: $("#ddlCPGIA").val()
					};		
					   
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CopyControlPointGroup', data, "Control Point Group data has not been copied!", function(d ) {
			<!--- Update Display --->
			$("#TableSurveyQuestions div[CPGID='<cfoutput>#inpCPGID#</cfoutput>']").find('.DescText').html($("#inpDesc").val());
			
			bootbox.alert("Control Point Group data has been copied", function(result) { 	
			
				var QLableNav = '';
				
				if(parseInt($("#ddlCPGIA").val() ) > 0)
					QLableNav = '&QNav=' + (parseInt($("#ddlCPGIA").val()) + 1 );
																	
				NewLocation = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editquestions/index?inpbatchid="
								+ '<cfoutput>#INPBATCHID#</cfoutput>' + "&PAGE=" + '<cfoutput>#INPPAGE#</cfoutput>' + QLableNav;	
																	
				window.location = NewLocation;	
				
				$('#CopyCPGModal').modal('hide');																						
				
			});
		});		
	
	}
	
	$(function() {	
		
		$("#loadingDlgCopyCPG").hide();	
	});
		
		
	
</script>

