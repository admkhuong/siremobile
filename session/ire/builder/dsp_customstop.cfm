<cfparam name="INPBATCHID" default="0">        

<!--- Check permission against acutal logged in user not "Shared" user--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>


<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset surveyEditQuestionPermission = permissionObject.havePermission(edit_Question_Title)>

<cfif surveyEditQuestionPermission.HAVEPERMISSION> 
	<cfset DoesHavePermissionEditQuestion = true>
<cfelse>
	<cfset DoesHavePermissionEditQuestion = false>
</cfif>

<cfif NOT surveyEditQuestionPermission.HAVEPERMISSION >
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<style>

	
</style>

<cfinvoke method="GetCustomSTOPMessage" component="#Session.SessionCFCPath#.csc.csc" returnvariable="RetVarCSTOPMSG">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
    <cfinvokeargument name="REQSESSION" value="1">
</cfinvoke>  


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Custom STOP</h4>        
</div>

<cfoutput>
<div class="modal-body">        
  	<form action="" method="POST" id="cid_form">
		
        <div class="">
            Custom STOP
        </div>
        
        <div class="">
            <textarea type="text" class="" id="txtCustomSTOP" row="5" maxlength='612' style="min-height:60px; width:100%; padding:1em;">#RetVarCSTOPMSG.CSTOPMSG#</textarea> <br>
            <label class="sms_remaining_chars_keyword" id="lblRemainCharsCH">160 of first 160 characters remaining</label>
        </div>	            
		
	</form>
</div>        
</cfoutput>
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
	
		$('textarea[maxlength]').keyup(function(){  
	        //get the limit from maxlength attribute  
	        var limit = parseInt($(this).attr('maxlength'));  
	        //get the current text inside the textarea  
	        var text = $(this).val();  
	        //count the number of characters in the text  
	        var chars = text.length;  
	  <!--- no more limits - just warn increments charged per 153 character anthing over 160 characters to begin with  --->
	  <!---
	        //check if there are more characters then allowed  
	        if(chars > limit){  
	            //and if there are use substr to get the text before the limit  
	            var new_text = text.substr(0, limit);  
	  
	            //and change the current text with the new text  
	            $(this).val(new_text);  
	        }
	  --->
			
	    });  
		
	
		$('#txtCustomSTOP').bind('cut copy paste', function (e) {
			ViewRemainChars($('#lblRemainCharsCH'),$('#txtCustomSTOP'));
		});
		
	    $('#txtCustomSTOP').keyup(function(){  
	        ViewRemainChars($('#lblRemainCharsCH'),$('#txtCustomSTOP'));
	    });  
		
	});
	
	function Save() {		
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=SetCustomSTOPMessage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpBatchId : '<cfoutput>#INPBATCHID#</cfoutput>', 
				INPDESC : $('#txtCustomSTOP').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d)
			{
				if(d.RXRESULTCODE > 0)
				{	
					$('#CustomStopModal').modal('hide');	
				}
				else
				{
					bootbox.alert(d.MESSAGE);
					$('#CustomStopModal').modal('hide');
				}
			} 		
		});
	}
	
	
	function ViewRemainChars(inpObj, inpObj2) {
		var value = (160 -  countSymbols(inpObj2.val()) <!---inpObj2.val().length--->) + '/160 characters remaining';
		inpObj.text(value)	
	}
	
	<!--- Cheap unicode counter logic --->
	function countSymbols(string) {
		var regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
	
		return string
		// replace every surrogate pair with a BMP symbol
		.replace(regexAstralSymbols, '_')
		// then get the length
		.length;
	}
		
</script>
