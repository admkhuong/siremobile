<cfparam name="INPBATCHID" default="0">

<cfquery name="GetDeliveryReceiptFlag" datasource="#Session.DBSourceEBM#">

	SELECT 
    	DeliveryReceipt_ti 
    FROM 
    	sms.keyword
	WHERE
    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">

</cfquery>

<cfif GetDeliveryReceiptFlag.RecordCount EQ 0>
	<cfset GetDeliveryReceiptFlag.DeliveryReceipt_ti = 0 />
</cfif>


<cfoutput>

       
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Turn on delivery receipts</h4>     
</div>

<div class="modal-body">  

    <div style="padding-bottom: 10px; padding-top: 10px;">
            
         <div class="inputbox-container">
            <label for="inpDesc" style="display:block;">Check Here to enable deliver receipts for this keyword/campaign</label>            
           
        	<div id="DROption" class="btn-group btn-toggle" data-toggle="buttons">
                <label class="btn btn-primary active"> 
                  <input type="radio" name="options" id="options" value="1" <cfif GetDeliveryReceiptFlag.DeliveryReceipt_ti GT 0>checked</cfif> >On
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="options" id="options" value="0" <cfif GetDeliveryReceiptFlag.DeliveryReceipt_ti EQ 0>checked</cfif>>Off
                </label>
         	</div>
                    
        </div>
    
    </div>
    
    <div id="loadingDlgRenameCPG" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
            
</div>		

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        <button type="button" class="btn btn-primary" onclick="SaveDRVal();">Save Changes</button>
    
    </div>	
</div>   

</cfoutput>

<script TYPE="text/javascript">
	function SaveDRVal(INPBATCHID) {
		
		$("#loadingDlgRenameCPG").show();		
							
		var data =  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',						
						inpDRFlag: $("#DROption input[name=options]:checked").val() 
					};	
							
		<!---bootbox.alert(" Option selected is " + $("#DROption input[name=options]:checked").val() );			  ---> 
			
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'UpdateKeywordDeliveryReceiptOption', data, "Problem updating delivery receipt option.", function(d) {					
			$('#DRModal').modal('hide');			
		});		
	
	}
	
	$(function() {	
		
		$("#loadingDlgRenameCPG").hide();	
		
		$('.btn-toggle').click(function() {
			$(this).find('.btn').toggleClass('active');  
			
			if ($(this).find('.btn-primary').size()>0) {
				$(this).find('.btn').toggleClass('btn-primary');
			}
			if ($(this).find('.btn-danger').size()>0) {
				$(this).find('.btn').toggleClass('btn-danger');
			}
			if ($(this).find('.btn-success').size()>0) {
				$(this).find('.btn').toggleClass('btn-success');
			}
			if ($(this).find('.btn-info').size()>0) {
				$(this).find('.btn').toggleClass('btn-info');
			}
			
			$(this).find('.btn').toggleClass('btn-default');
			   
		});
		 

		<cfif GetDeliveryReceiptFlag.DeliveryReceipt_ti EQ 0>
			$('.btn-toggle').click();		
		</cfif>
 
 
	});
		
		
	
</script>
