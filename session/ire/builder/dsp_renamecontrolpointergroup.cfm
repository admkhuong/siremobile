<cfparam name="inpOldDesc" default="">
<cfparam name="INPBATCHID" default="0">
<cfparam name="inpCPGID" default="0">
<cfparam name="INPTYPE" default="">

<cfoutput>

	
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">RE-Name your Control Pointer Group</h4>     
</div>

<div class="modal-body">  
        
            <div style="padding-bottom: 10px; padding-top: 10px;">
                    
                 <div class="inputbox-container">
                    <label for="inpDesc">Name your Control Pointer Group<span class="small">Required</span></label>            
                    <input id="inpDesc" name="inpDesc" placeholder="Enter Control Pointer Group Name Here" size="40" value="#inpOldDesc#"/>
                </div>
            
            </div>
            <div id="loadingDlgRenameCPG" style="display:inline;">
                <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
            	
      
</div>		

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        <button type="button" class="btn btn-primary" onclick="SaveCPG();">Save Changes</button>
    
    </div>	
</div>   

</cfoutput>

<script TYPE="text/javascript">
	function SaveCPG(INPBATCHID) {
		
		$("#loadingDlgRenameCPG").show();		
			
		if($("#inpDesc").val() == '')
		{			
			bootbox.alert("Control Point Group description has not been updated.\n"  + "New description can not be blank." + "\n", function(result) { } );										
			$("#loadingDlgRenameCPG").hide();	
			return;	
		}
			
		var data =  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPTYPE: '<cfoutput>#INPTYPE#</cfoutput>',
						inpCPGID: '<cfoutput>#inpCPGID#</cfoutput>', 
						inpMin: 0,
						inpMax: 0,
						inpDesc: $("#inpDesc").val()
					};		
					   
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'UpdateControlPointGroup', data, "Control Point Group description has not been updated!", function(d ) {
			<!--- Update Display --->
			$("#TableSurveyQuestions div[CPGID='<cfoutput>#inpCPGID#</cfoutput>']").find('.ControlPointGroupDesc').html($("#inpDesc").val());
			$('#RenameCPGModal').modal('hide');
						
		});		
	
	}
	
	$(function() {	
				
		$("#loadingDlgRenameCPG").hide();	
	});
		
		
	
</script>


<style>

#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}




</style> 

<cfoutput>
 
</cfoutput>
