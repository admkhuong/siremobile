<cfparam name="INPBATCHID" default="0">

<cfinvoke method="GetActiveMessage" component="#Session.SessionCFCPath#.csc.SMSSurvey" returnvariable="RetVarActive">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
    <cfinvokeargument name="REQSESSION" value="1">
</cfinvoke>  

<cfoutput>

	
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">program is already active warning message</h4>     
</div>

<div class="modal-body">        
    <div style="padding-bottom: 10px; padding-top: 10px;">
            
         <div class="inputbox-container">
            <label for="inpDesc" style="display:block;">Set the program is already active warning message.<span class="small" style="display:block;">Not-Required leave blank for no response</span></label>            
           
           <textarea  
                        maxlength="1000"
                        id="inpDesc" name="inpDesc" role="textbox" aria-autocomplete="list" 
                        aria-haspopup="true"
                        rows="3" cols="40"	
                        class="textarea" style="width:100%;"
                        placeholder="Enter 'Program is already active' message here"	
                    >#RetVarActive.AMSG#</textarea>
                    
        </div>
    
    </div>
    
    <div id="loadingDlgRenameCPG" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
    
</div>		

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        <button type="button" class="btn btn-primary" onclick="SaveCPG();">Save Changes</button>
    
    </div>	
</div>   

</cfoutput>

<script TYPE="text/javascript">
	function SaveCPG(INPBATCHID) {
		
		$("#loadingDlgRenameCPG").show();		
							
		var data =  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',						
						inpDesc: $("#inpDesc").val()
					};		
					   
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/SMSSurvey.cfc', 'SetActiveMessage', data, "Program already active message has not been updated!", function(d ) {
			<!--- Update Display --->			
			$('#ProgramActiveMSGBtnModal').modal('hide');
			
		});		
	
	}
	
	$(function() {	
								
		$("#loadingDlgRenameCPG").hide();	
	});
		
		
	
</script>

