
<cfinclude template="../../sire/configs/paths.cfm">

<cfparam name="INPBATCHID" default="0">
<cfparam name="getMaxOrder" default="">
<cfparam name="RXOrderNumber" default="1">
<cfparam name="orderNumber" default="1">

<!--- Presumes dialog is opened assigned to a javascript var of CreateXMLContorlStringMasterTemplateDialogVoiceTools--->


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	
	#MC_Stage_XMLControlString_Master_Template .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}
	
	
	#MC_Stage_XMLControlString_Master_Template .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;   
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 100%;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
		position:absolute;
		top: -25px;
		right: 37px;
	}
	
	#MC_Stage_XMLControlString_Master_Template .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#MC_Stage_XMLControlString_Master_Template .form-right-portal {
    z-index: 1000;
}

.dd-option-text,.dd-selected-text{
	line-height: 30px !important;
}
.dd-option-image, .dd-selected-image{
	max-height: 50px;
}

</style>
<script type="text/javascript">


	$(function() 
	{		
		ReadReviewXMLStringForMasterTemplate();
		
		$("#MC_Stage_XMLControlString_Master_Template #SaveCCD").click(function() { WriteCCDXMLString(); });								

		$("#inpSubmit").click(function() { 

			var INPNAME = $('#INPNAME').val();
			var INPDESC = tinymce.get("INPDESC").getContent();
			var CurrentReviewXMLString = $('#CurrentReviewXMLString').val();

			if(INPNAME === ""){
				bootbox.alert('Template Name can not blank');
				return false;
			}

			if(INPDESC === ""){
				bootbox.alert('Template Description can not be blank');
				return false;
			}

			if(CurrentReviewXMLString === ""){
				bootbox.alert('Template XML can not be blank');
				return false;
			}

            tinymce.get("INPDESC").save();

			try{
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/templates.cfc?method=CreateCampaignTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:$('#form-create-template').serialize(),
		            dataType: "json", 
		            success: function(d) {
		            	
         				if(d.RXRESULTCODE == 1){
							
							bootbox.alert('Added Success', function() { 
								$('#MakeXMLTemplateModal').modal('hide');
							});
						}
						else
						{
							<!--- No result returned --->							
							bootbox.alert(d.MESSAGE);
						}
		         	}
		   		});
			}catch(ex){
				bootbox.alert('Create Fail', function() {});
			}
		});

        tinymce.init({
            selector: "textarea.normal",
//            theme: "modern",
            plugins: [
                 "advlist autolink link image lists charmap preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
                 "table contextmenu directionality emoticons paste textcolor code"
            ],
            relative_urls: true,
            browser_spellcheck: true,
            codemirror: {
                indentOnInit: true, // Whether or not to indent code on init. 
                path: 'CodeMirror'
            },
            convert_urls: false,
            menubar: false,
            image_advtab: true,
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | print preview code ",
            plugin_preview_width : "208",
        	plugin_preview_height : "180",
        	/*
            external_plugins: {
                "filemanager": "/public/js/tinymce_4.0.8/plugins/filemanager/plugin.js"
            }*/
         });

         $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

         $('#template_image').ddslick({
         	height: '200px',
         	width: '400px',
		    selectText: "Select Template Images",
		    onSelected: function (data) {
		        //console.log(data.selectedData.value);
		        $("#INPIMAGE").val(data.selectedData.value)
		    }
		});

	});



<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools fiel for updating preview menu item--->
		<!--- Output an XML CCD String based on form values --->
	function ReadReviewXMLStringForMasterTemplate()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=Readdisplayxml&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    		 		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				 },
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
         				if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								<!---displayxml(d.DATA.DISPLAYXML_VCH[0]); --->
																								
								 var xml=d.DATA.RAWXML_VCH[0];
								  <!---//before call funcation format replace all ul,li,pre in XML --> ""--->
								  xml = xml.replace(/<br>/g, "");
								  xml = xml.replace(/<ul>/g, "");
								  xml = xml.replace(/<\/ul>/g, "");
								  xml = xml.replace(/<li>/g, "");
								  xml = xml.replace(/<\/li>/g, "");
								  xml = xml.replace(/<pre>/g, "");
								  xml = xml.replace(/<\/pre>/g, "");
								  xml = xml.replace(/&gt;/g, ">");
								  xml = xml.replace(/&lt;/g, "<");
								  xml = xml.replace(/>\s*</g, "><");
								  xml=format_xml(xml);
								  xml = xml.replace(/>/g, "&gt;");
								  xml = xml.replace(/</g, "&lt;");
								  			  
								$('#MC_Stage_XMLControlString_Master_Template #CurrentReviewXMLString').html(xml);
																					
							}
						}
						else
						{
							<!--- No result returned --->							
							bootbox.alert("No Response from the remote server. Check your connection and try again.");
						}
         		 }
   		  });
	}
	
	<!---
	
	
	<!--- Output an XML CCD String based on form values --->
	function WriteCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpCID : $("#MC_Stage_XMLControlString_Master_Template #inpCID").val(), 
    			 inpFileSeq : $("#MC_Stage_XMLControlString_Master_Template #inpFileSeq").val(), 
    			 inpUserSpecData : $("#MC_Stage_XMLControlString_Master_Template #inpUserSpecData").val(), 
    			 inpDRD : $("#MC_Stage_XMLControlString_Master_Template #inpDRD").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadCCDXMLString();
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.CCDXMLSTRING[0]) != "undefined")
										{									
											$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);																					
										}									
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_XMLControlString_Master_Template #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
				  }
			 });
		}--->
	
	
	function spaces(len)
	{
        var s = '';
        var indent = len*4;
        for (i= 0;i<indent;i++) {s += " ";}
        
        return s;
	}

	function format_xml(str)
	{
        var xml = '';

        // add newlines
        str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

        // add indents
        var pad = 0;
        var indent;
        var node;

        // split the string
        var strArr = str.split("\r");

        // check the various tag states
        for (var i = 0; i < strArr.length; i++) {
                indent = 0;
                node = strArr[i];

                if(node.match(/.+<\/\w[^>]*>$/)){ //open and closing in the same line
                        indent = 0;
                } else if(node.match(/^<\/\w/)){ // closing tag
                        if (pad > 0){pad -= 1;}
                } else if (node.match(/^<\w[^>]*[^\/]>.*$/)){ //opening tag
                        indent = 1;
                } else
                        indent = 0;
                //}

                xml += spaces(pad) + node + "\r";
                pad += indent;
        }

        return xml;
	}
	
	
	

	

</script>	

<!--- GET LIST IMAGE IN FOLDER --->

<cfset UserFiles 		= ExpandPath(TEMPLATE_PICKER_PATH)>

 <cfdirectory 
    directory    = "#UserFiles#"
    action        = "list"
    sort        = "type asc, name asc"
    name        = "getAllImage">
<cfoutput>

<!--- GET MAX ORDER IN ORDER --->

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add a new Template</h4>        
</div>

<cfquery name="getMaxOrder" datasource="#Session.DBSourceEBM#" result="RXOrderNumber">
	SELECT max(Order_int) as MaxOrder FROM simpleobjects.templatesxml;
</cfquery>

<cfif RXOrderNumber.RECORDCOUNT GT 0>
	<cfloop query="getMaxOrder">
		<cfset orderNumber = MaxOrder>
	</cfloop>
	
</cfif>

<cfquery name="getCategory" datasource="#Session.DBSourceEBM#">
	SELECT * FROM simpleobjects.template_category 
</cfquery>

<div class="modal-body">


    <div id="MC_Stage_XMLControlString_Master_Template" class="EBMDialog">
                          
        <form method="POST" id="form-create-template">
                                      
            <div class="inner-txt-box">
   
                <div class="inner-txt-hd">Add a new Template to the master list</div>
                <div class="inner-txt">Please give your Template a unique name.</div>
            
                <div class="form-left-portal">
            
                    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
                                                            
                   
                    <label for="INPDESC">Template Name</label>
                    <input type="text" class="input-box" name="INPNAME" id="INPNAME" placeholder="Give it a unique name here"  style="width:100%;">

                    <label for="INPDESC">Template Category</label>
                    <select name="INPCATEGORY" style="width:100%;">
                    	<cfloop query="getCategory">
                    		<option  value="<cfoutput>#getCategory.CID_int#</cfoutput>"><cfoutput>#getCategory.CategoryName_vch#</cfoutput>	</option>
                    	</cfloop>
                    </select>
                   
                    <div style="clear:both"></div>

                    <label for="INPDESC">Template Description</label>
                    <textarea id="INPDESC" name="INPDESC" placeholder="Give it a good description here" class="normal"></textarea>
                                                
                    <div style="clear:both"></div>

                    <label for="INPIMAGE">Template Image</label>
                    <input type="hidden" name="INPIMAGE" id="INPIMAGE" value=''>
                    <cfif #getAllImage.RECORDCOUNT# GT 0>
                    	<select id="template_image">
                    			<option value=''>Not use image</option>
                    		<cfloop query="getAllImage">
                    			<option value="#name#" data-imagesrc="#TEMPLATE_PICKER_PATH#/#name#">#name#</option>
                    		</cfloop>
					    </select>
					<cfelse>
					    <p> No Image Found.</p>
					</cfif>	

					<label for="INPORDER">Template Order</label>
                	<select id="template_order" name="INPORDER">
                		<cfloop from ="1" to="#Int(orderNumber+1)#" step="1" index="index">
                			<option value="#index#">#index#</option>
                		</cfloop>
				    </select>

				    <div style="clear:both"></div>

                    <label for="INPDESC">Template XML</label>
                    <textarea id="CurrentReviewXMLString" name="CurrentReviewXMLString" style="overflow:auto; font-size:12px; width:100%; min-height:150px;" readonly="readonly">
                        NA
                    </textarea>
                                            
                </div>                            
                                       
          
          
            </div>
                       
        </form>
           
    </div>     
    
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
     
	<button type="button" class="btn btn-primary" id="inpSubmit">Add Template</button>
   
</div>	

    
</cfoutput>



    