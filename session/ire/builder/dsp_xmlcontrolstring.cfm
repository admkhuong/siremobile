

<cfparam name="INPBATCHID" default="0">

<style>

#PreLoadMaskXMK {
    background-color: #f3f3f3;
    display: inline-block;
    height: 100%;
    left: 0;
    opacity: 1;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10000;
}

</style>


<!--- Presumes dialog is opened assigned to a javascript var of $CreateXMLContorlStringReviewDialogVoiceTools--->

<script type="text/javascript">

	$(function() 
	{		
		$("#PreLoadMaskXML").show();
		
		ReadReviewXMLString();
				
		$("#SaveXMLToDB").click(function() 
		{ 
			bootbox.confirm( "Are you sure you want to save your XML changes? This will overwrite all CPs in this interactive campaign?", function(result) { 
				if(result)
				{	
				
					<!--- Need to remove newlines and other formating here --->					
					var DeformattedXML = $("#MC_EditQ_XMLControlString #CurrentReviewXMLString").val();
					<!---DeformattedXML = DeformattedXML.replace(/>\r*/g, ">");--->
					DeformattedXML = DeformattedXML.replace(/>\s*</g, "><");					
					
					var data = 
					{ 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						inpXML : DeformattedXML
					};
							
					ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/SMSSurvey.cfc', 'WriteXML', data, "Error - XML Control String has not been updated!", function(d ) {
						bootbox.alert("XML Control String has been updated", function(result) { 
							location.reload();
						});
					});
					
					return;		
				}			
				else
				{
					return;					
				}
			});																										
		});
													
		$("#MC_EditQ_XMLControlString #MakeXMLATemplate").click(function() { XMLContorlStringMasterTemplateDialogVoiceTools(); });	
				
	});

	<!--- Global so popup can refernece it to close it--->
	var CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;
	
	function XMLContorlStringMasterTemplateDialogVoiceTools()
	{											
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
			
		var options = 
		{
			show: true,
			"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_TemplateAdd' + ParamStr
		}

		$('#MakeXMLTemplateModal').modal(options);	
		
		return false;				
	}
			
	<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools fiel for updating preview menu item--->
	<!--- Output an XML CCD String based on form values --->
	function ReadReviewXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=Readdisplayxml&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    		 		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				 },
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
         				if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								<!---displayxml(d.DATA.DISPLAYXML_VCH[0]); --->
																								
								 var xml=d.DATA.RAWXML_VCH[0];
								 ///var xml=d.DATA.DISPLAYXML_VCH[0];
								  <!---//before call function format replace all ul,li,pre in XML --> ""--->
								<!---  xml = xml.replace(/<br>/g, "");
								  xml = xml.replace(/<ul>/g, "");
								  xml = xml.replace(/<\/ul>/g, "");
								  xml = xml.replace(/<li>/g, "");
								  xml = xml.replace(/<\/li>/g, "");
								  xml = xml.replace(/<pre>/g, "");
								  xml = xml.replace(/<\/pre>/g, "");
								  xml = xml.replace(/&gt;/g, ">");
								  xml = xml.replace(/&lt;/g, "<");--->
								  xml = xml.replace(/>\s*</g, "><");
								  xml=format_xml(xml);
								<!---  xml = xml.replace(/>/g, "&gt;");
								  xml = xml.replace(/</g, "&lt;");--->
								  			  
								$('#MC_EditQ_XMLControlString #CurrentReviewXMLString').html(xml);
								
								$("#PreLoadMaskXML").hide();
								
								<!---$('#MC_EditQ_XMLControlString #CurrentReviewXMLStringRaw').html(d.DATA.RAWXML_VCH[0]);--->
								<!---$('#MC_EditQ_XMLControlString #CurrentReviewXMLStringRaw').html(d.DATA.DISPLAYXML_VCH[0]);--->
								<!---$('#MC_EditQ_XMLControlString #CurrentReviewXMLStringRaw').html(d.DATA.DISPLAYXML_VCH[0]);--->
								
															
							}
						}
						else
						{
							<!--- No result returned --->
							bootbox.alert("No Response from the remote server. Check your connection and try again.");
						}
         		 }
   		  });
	}
			
	function spaces(len)
	{
        var s = '';
        var indent = len*4;
        for (i= 0;i<indent;i++) {s += " ";}
        
        return s;
	}

	function format_xml(str)
	{
        var xml = '';

        // add newlines
        str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

        // add indents
        var pad = 0;
        var indent;
        var node;

        // split the string
        var strArr = str.split("\r");

        // check the various tag states
        for (var i = 0; i < strArr.length; i++) 
		{
                indent = 0;
                node = strArr[i];

                if(node.match(/.+<\/\w[^>]*>$/))
				{ //open and closing in the same line
                        indent = 0;
                }
				else if(node.match(/^<\/\w/))
				{ // closing tag
                        if (pad > 0){pad -= 1;}
                } 
				else if (node.match(/^<\w[^>]*[^\/]>.*$/))
				{ //opening tag
                        indent = 1;
                }
				else
				{
                        indent = 0;
                }

                xml += spaces(pad) + node + "\r";
                pad += indent;
        }

        return xml;
	}


</script>	


<cfoutput>



<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">XML Control String</h4>        
</div>

<div class="modal-body">

    <div id="MC_EditQ_XMLControlString" class="">
    
        <div style="position:relative;">
        
           <button type="button" class="btn btn-primary" id="MakeXMLATemplate">Make Template</button>    
                      
        </div>    
        
            <p class='pActive'></p>	
         
            <form id="CCDForm" name="CCDForm" method="post" action="index.html">
            
            <div class="spacer"></div>
            
            <div id="PreLoadMaskXML">
   			    <div id="PreLoadIcon">
                    <p>LOADING ...</p>
                </div>
   			</div>
          
            <textarea id="CurrentReviewXMLString" name="CurrentReviewXMLString" style="overflow:auto; font-size:12px; width:100%; min-height:200px;">
                              
            </textarea>
                         
          <!---  <!--- Read from DB Batch Options --->
            <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                SELECT                
                  XMLControlString_vch
                FROM
                  simpleobjects.batch
                WHERE
                  BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
            </cfquery>  
            
            <cfdump var="#GetBatchOptions#">
                
            <textarea id="CurrentReviewXMLStringRaw" name="CurrentReviewXMLStringRaw" style=" white-space:pre-wrap; word-wrap:break-word; overflow:auto; font-size:12px; width:952px; height:200px;">
                <pre>#GetBatchOptions.XMLControlString_vch#</pre>
            </textarea>--->
                           
            </form>
            
    </div>     
    
    
    
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
     
	<button type="button" class="btn btn-primary" id="SaveXMLToDB">Save Changes</button>
   
</div>	
   
</cfoutput>


    