<cfparam name="INPBATCHID" default="0">
<cfparam name="QNav" default="0">

<cfset BATCHTITLE = "">

<!--- Style load --->
<cfoutput>
    
    <script src="#rootUrl#/#PublicPath#/js/validationregex.js"></script>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery.ddslick.min.js"></script>
        
	<style>		
		@import url('#rootUrl#/#PublicPath#/css/survey.css') all;
		@import url('#rootUrl#/#PublicPath#/css/utility.css') all;
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css') all;		
	</style>
      
</cfoutput>

<style>
   .ui-selecting { background: #FECA40; }
   .ui-selected { background: #F39814; }
   
	#TableSurveyQuestions { list-style: none; margin: 0; padding: 0; }
  	.ui-selecting { background: #FECA40; }
	.ui-selecting .handle { background: #FECA40; cursor:move;}
	.ui-selected { background: #F39814; }
	.ui-selected .handle { background: #F39814; cursor:move; }
	.handle {cursor:move; }
	
	
	.header
	{
		border-bottom: 1px solid #f5f5f5;
		padding-bottom: 1em;
		position: relative;
		width: 100%;
		z-index: 998;
	}
	
	
	#SurveyContent button
	{
	  margin-top:.8em;
	}
			
	#CPBuilderSubMenu .navbar-collapse.collapse 
	{
		display: block!important;
	}
	
	#CPBuilderSubMenu .navbar-nav>li, .navbar-nav 
	{
		float: right !important;
	}
	
	#CPBuilderSubMenu .navbar-nav.navbar-right:last-child 
	{
		margin-right: -15px !important;
	}
	
	#CPBuilderSubMenu .navbar-right {
		float: right!important;
	}
	
	.dropdown-submenu
	{
		font-size: 14px;
		list-style: outside none none;
		text-align: left;
	}
	
	.dropdown-submenu > li > a 
	{
		clear: both;
		color: #333;
		display: block;
		font-weight: 400;
		line-height: 1.42857;
		padding: 3px 20px;
		white-space: nowrap;
	}

	.dropdown-submenu > li > a:focus, .dropdown-submenu > li > a:hover  
	{
		text-decoration:none;
		color: #262262;
		background-color: #f5f5f5;
	}
		
	.dropdown-toggle:focus, .dropdown-toggle:hover
	{
    	background: none !important;
    	border-color: none !important;
	}
	
</style>

<!--- 
FA7D29
ul { list-style: none; margin: 0; padding: 0; }
.q_contents { list-style-type: none; margin: 0; padding: 0; width: 450px; }
.q_contents li { margin: 3px; padding: 1px; float: left; width: 100px; height: 80px; font-size: 4em; text-align: center; } 
  
 --->  

<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
<!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
<cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
</cfinvoke> 

<cfif RetVarXML.RXRESULTCODE LT 0>
	
    <div>
    	No campaign data found.    
    </div>

	<cfabort/>

</cfif>
        
<cfset BATCHTITLE = RetVarXML.DESC>                                    

<!--- Import constant variables --->
<cfinclude template="/#sessionPath#/ire/builder/surveyConstants.cfm">

<!--- Set defaults for blank new survey --->
<cfset indexNumber = 1>
       
<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Survey_Question_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Survey_Question_Title#">
</cfinvoke>

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<!--- Can not call these each time - round trip to the db for each question - WAY too much load --->
<cfset surveyAddQuestionPermission = permissionObject.havePermission(Add_Question_Title)>
<cfset surveyEditQuestionPermission = permissionObject.havePermission(edit_Question_Title)>
<cfset surveyDeleteQuestionPermission = permissionObject.havePermission(delete_Question_Title)>

<cfif surveyAddQuestionPermission.HAVEPERMISSION> 
	<cfset DoesHavePermissionAddQuestion = true>
<cfelse>
	<cfset DoesHavePermissionAddQuestion = false>
</cfif>

<cfif surveyEditQuestionPermission.HAVEPERMISSION> 
	<cfset DoesHavePermissionEditQuestion = true>
<cfelse>
	<cfset DoesHavePermissionEditQuestion = false>
</cfif>

<cfif surveyDeleteQuestionPermission.HAVEPERMISSION> 
	<cfset DoesHavePermissionDeleteQuestion = true>
<cfelse>
	<cfset DoesHavePermissionDeleteQuestion = false>
</cfif>

<cfif not structkeyExists(URL, "PAGE")>
	<cfset page = 1>
<cfelse>
	<cfset page = StructFind(URL, "PAGE")>
	<cfif isNumeric(page) eq false>
		<cfset page = 1>
	</cfif>
</cfif>

<!--- Library of tools for javascript handling of XML --->
<cfoutput>
	<script src="#rootUrl#/#PublicPath#/js/rxxml.js" type="text/javascript" charset="utf-8"></script>
</cfoutput>

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Survey_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #edit_Survey_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput>#BATCHTITLE#</cfoutput>');	
</script>

<!---
<cfif structkeyExists(query, 'PROMPT')>
	<cfset arrayPromt = query.PROMPT>
</cfif>--->

<cfset disabledBack = "">

<cfif isdefined('arrayPromt')>
	<cfif page gt 1 and arrayisEmpty(arrayPromt) eq false and page lte arraylen(arrayPromt) and arrayIsDefined(arrayPromt, page)>
		<cfif arrayIsDefined(arrayPromt[page], 2)>
			<cfset disabledBack = arrayPromt[page][2]>
			<cfif disabledBack eq 1>
				<cfset disabledBack = "checked">
			<cfelse>
				<cfset disabledBack = "">
			</cfif>
		</cfif>
	</cfif>
</cfif>


<!--- Read question GetQAggregateData  --->        
<cfinvoke method="GetQAggregateData" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarGetQAggregateData">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">    
    <cfinvokeargument name="GROUPID" value="#PAGE#"> 
</cfinvoke>
        
<!---<cfdump var="#RetVarGetQAggregateData#">     --->    

<cfset questionsVoice = arraynew(1)>
<!---
<cfif structkeyExists(query, 'LSTQIDVOICE_QUESTION')>
	<cfset questionsVoice = query.LSTQIDVOICE_QUESTION>
<cfelse>
	<cfset questionsVoice = arraynew(1)>
</cfif>--->

<cfif RetVarGetQAggregateData.RXRESULTCODE gt 0>
	<cfif structkeyExists(RetVarGetQAggregateData, 'PAGEQUESTIONSCOUNT')>
		<cfset pageQuestion = RetVarGetQAggregateData.PAGEQUESTIONSCOUNT>
	<cfelse>
		<cfset pageQuestion = 1>
	</cfif>    
<cfelse>
	<cfset pageQuestion = 1>
</cfif>

<cfif structkeyExists(RetVarGetQAggregateData, 'COMTYPE')>
	<cfset communicationType = RetVarGetQAggregateData.COMTYPE>
<cfelse>
	<cfset communicationType = 'NAN'>
</cfif>

<cfif StructKeyExists(RetVarGetQAggregateData, 'VOICE')>
	<cfset VOICETYPE = RetVarGetQAggregateData.VOICE>
<cfelse>
	<cfset VOICETYPE = ''>
</cfif>

<cfif structkeyExists(RetVarGetQAggregateData, 'BA')>
	<cfset GLOBALDISABLEBACK = RetVarGetQAggregateData.BA>
<cfelse>
	<cfset GLOBALDISABLEBACK = '1'>
</cfif>

<cfif RetVarGetQAggregateData.RXRESULTCODE gt 0>
	<cfif structkeyExists(RetVarGetQAggregateData, 'GROUPCOUNT')>
		<cfset totalPage = RetVarGetQAggregateData.GROUPCOUNT>
	<cfelse>
		<cfset totalPage = 1>
	</cfif>
    
<cfelse>
	<cfset totalPage = 1>
</cfif>

<cfif RetVarGetQAggregateData.RXRESULTCODE gt 0>
	<cfif structkeyExists(RetVarGetQAggregateData, 'TOTALQ')>
		<cfset totalQs = RetVarGetQAggregateData.TOTALQ>
	<cfelse>
		<cfset totalQs = 0>
	</cfif>
    
<cfelse>
	<cfset totalQs = 0>
</cfif>

<cfif RetVarGetQAggregateData.RXRESULTCODE gt 0>
	<cfif structkeyExists(RetVarGetQAggregateData, 'NEXTPID')>
		<cfset NEXTPID = RetVarGetQAggregateData.NEXTPID>
	<cfelse>
		<cfset NEXTPID = 1>
	</cfif>
    
<cfelse>
	<cfset NEXTPID = 1>
</cfif>

<cfif RetVarGetQAggregateData.RXRESULTCODE gt 0>
	<cfif structkeyExists(RetVarGetQAggregateData, 'STAGEWIDTH')>
		<cfset STAGEWIDTH = RetVarGetQAggregateData.STAGEWIDTH>
	<cfelse>
		<cfset STAGEWIDTH = 950>
	</cfif>
    
<cfelse>
	<cfset STAGEWIDTH = 950>
</cfif>


<!---
<cfif page gt totalPage>
	<cflocation url="#rootUrl#/#sessionPath#/ire/builder/index?inpbatchid=#INPBATCHID#">
</cfif>
--->

<cfinvoke component="#LocalSessionDotPath#..cfc.ire.marketingSurveyTools" method="GetStartQ" returnvariable="RetValGetStartQ">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
    <cfinvokeargument name="GROUPID" value="#page#">
</cfinvoke>

<cfset startNumberQ = RetValGetStartQ.STARTNUMBERQ>

<!--- Get the last page question number --->
<cfset allowAddPage = false>

<cfif totalPage neq page and totalQs gt 0>
	<cfset allowAddPage = true>	
</cfif>

<cfif totalPage eq page or totalPage eq 0>
	<cfset allowAddPage = true>
</cfif>

<cfinclude template="new_js_question.cfm"> 

<!--- List all survey's questions--->


<cfinvoke component="#LocalSessionDotPath#.cfc.history" method="CheckHistory" returnvariable="RetValCheckHistory">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>



<cfoutput>

<!---wrap the page content do not style this--->
<div id="page-content">
   
  	<div class="container" >
      		
        
        <div id="SurveyBuilderHeader" style="padding-top: 5px; padding-bottom: 5px;">
			<cfif communicationType neq SURVEY_COMMUNICATION_BOTH>
                <label>Delivery Method <cfoutput>#communicationType#</cfoutput></label>
            <cfelse>
                <label>Delivery Method <cfoutput>#SURVEY_COMMUNICATION_BOTH_TITLE#</cfoutput></label>
            </cfif>
                        
           
                                       
                <ul id="CPBuilderSubMenu" class="no-print nav navbar-nav" id="links" style="float:right; position:relative; top:-15px;">
                   
                    <li class="dropdown"> <a href="##" class="dropdown-toggle" data-toggle="dropdown" style="text-align:right;">Advanced Options <b class="caret"></b></a>
            
                                        
                        <ul class="dropdown-menu" style="min-width:325px;">
                        
                            <li>
                                <div class="row" style="">
                                    <ul class="dropdown-submenu col-xs-6 col-sm-6 col-md-6">
                                        <li class="dropdown-header">Edit</li>
                            
                                        <li>
                                            <a id="undoBtn" href="##">Undo</a>
                                        </li>
                                        
                                        <li>
                                            <a id="redoBtn" href="##">Redo</a>
                                        </li>
                                        
                                        <li>
                                            <a id="xmlBtn" href="##" title="Edit Raw XML Direct - Can copy paste from here" >Raw XML</a>
                                        </li>
                                        
                                        <li>
                                            <a id="ToggleToggleBtnBtn" href="##" title="Toggle Edit Options">Hide Buttons</a>            
                                        </li>
                                        
                                    </ul>
                                    <ul class="dropdown-submenu col-xs-6 col-sm-6 col-md-6">
                                        <li class="dropdown-header">Campaign Options</li>
                            
                                        <li>
                                            <a id="ProgramActiveMSGBtn" href="##" title="Edit Program Already Active Message">Active Warning</a>                
                                        </li>
                                        
                                        <li>
                                            <a id="TMRMSGBtn" href="##" title="Edit Too Many Retries Message">Retry Warning</a>                
                                        </li>
                                        
                                        <li>
                                            <a id="ScheduleBtn" href="##">Schedule</a>                
                                        </li>
                                        
                                        <li>
                                           	<a data-toggle='modal' href='dsp_customhelp?inpBatchId=#INPBATCHID#' data-target='##CustomHelpModal'>Custom HELP</a>                                                         
                                        </li>
                                        
                                        <li>
                                           	<a data-toggle='modal' href='dsp_customstop?inpBatchId=#INPBATCHID#' data-target='##CustomStopModal'>Custom STOP</a>                                                         
                                        </li>
                                        
                                          
                                        <li>
                                           	<a data-toggle='modal' href='dsp_deliveryreceipts?inpBatchId=#INPBATCHID#' data-target='##DRModal'>Delivery Receipt</a>                                                         
                                        </li>
                                       
                                    </ul>
                                   
                                </div>
                            </li>
                            
                            <li class="dropdown-header"><hr /></li>
                            
                            <li>
                                <div class="row" style="">
                                    <ul class="dropdown-submenu col-xs-6 col-sm-6 col-md-6">
                                        <li class="dropdown-header">Grouping</li>
                            
                                        <li>
                                            <a id="btnGroup" href="##">Group Selected</a>
                                        </li>
                                        
                                        <li>
                                            <a id="btnMoveNewPage" href="##">Insert Page</a>
                                        </li>
                                        
                                        <li>
                                            <a id="addPage" href="##">Add Page</a>
                                        </li>
                                        
                                                                                
                                    </ul>
                                    <ul class="dropdown-submenu col-xs-6 col-sm-6 col-md-6">
                                        <li class="dropdown-header">Delete!</li>
                                                                    
                                        <li>
                                            <a id="DelAllQuestionsFromSurveyList" href="##">Delete All</a>
                                        </li>

                                        <li>
                                            <a id="DelAllCPGFromSurveyList" href="##">Delete All Groups</a>
                                        </li>

										<li>
                                            <a id="deletePage" href="##">Delete Page</a>
                                        </li>
                                                                              
                                    </ul>
                                   
                                </div>
                            </li>
                                                       
                        </ul>
                    </li>
                </ul>
              
        </div>
    
    	<h4 class="no-margin-top  no-print">#Survey_Builder_Title#</h4>
    
        <div id="SurveyBuilderContainer" class="stand-alone-content" style="width:100%; margin-bottom:400px;">
                   
                
            <div id="PageLoadingInfo" style="float:left; margin:5px 5px; color:##e1e1e1" class="no-print">Page Loading...</div>
                          
                        
			<!--- ***TODO: Show summary Stats --->
            <cfif page EQ 0>
            
                <!--- Number of control points --->
                
                <!--- Counts by question type --->
                
                <!--- Possible paths --->
                                
            </cfif>

            <div id="SurveyContent" >
            
                <div id="AddQuestionSection" style="width: 100%; padding-top: 5px;">
                                            
                 <!---   <cfif communicationType neq SURVEY_COMMUNICATION_PHONE > <!---AND communicationType neq SURVEY_COMMUNICATION_SMS--->
                        <cfoutput>
                            <button type="button" class="no-print ui-corner-all addPage survey_builder_button" style="width: auto;">Add Page</button>                
                        </cfoutput>
                    </cfif>--->
                    <cfif communicationType neq SURVEY_COMMUNICATION_ONLINE AND communicationType neq SURVEY_COMMUNICATION_SMS>
                     <!---   <cfoutput>
                            <input type="hidden" id="hidErrorPrompt" value="<cfoutput>#query.ERRORSCRIPT#</cfoutput>" desc="<cfoutput>#query.ERRORDESCRIPTION#</cfoutput>">						
                            <button class="no-print btn btn-default add_prompt" QID="5" promptValueControl="hidErrorPrompt"
                                    type="button">Add Error Prompt</button>
                            <img alt="" class="ErrorPromptRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
                            <cfif query.ERRORSCRIPT EQ "">display:none;</cfif>		
                            "scriptId="<cfoutput>#query.ERRORSCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
                            
                            <input type="hidden" id="hidTryAgainPrompt" value="<cfoutput>#query.TRYAGAINSCRIPT#</cfoutput>" desc="<cfoutput>#query.TRYAGAINDESCRIPTION#</cfoutput>">
                            <button class="no-print btn btn-default add_prompt" QID="6" promptValueControl="hidTryAgainPrompt"
                                type="button">Add Try Again Prompt</button>	
                            
                            <img alt="" class="TryAgainPromptRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
                            <cfif query.TRYAGAINSCRIPT EQ "">display:none;</cfif>		
                            "scriptId="<cfoutput>#query.TRYAGAINSCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
                            
                        </cfoutput>--->
                    </cfif>
                    
                    <cfif communicationType neq SURVEY_COMMUNICATION_PHONE AND communicationType neq SURVEY_COMMUNICATION_SMS>
                         <div style="float: right;">
                             
                                <input id="chkGloballyDisableBack" type="checkbox" class="back_checkbox" 
                                    <cfif GLOBALDISABLEBACK eq "0">
                                        <cfoutput> checked="checked" </cfoutput>
                                    </cfif>
                                />
                                <span class="back_label"><cfoutput>#SURVEY_LABEL_GLOBAL_DISABLE#</cfoutput></span>
                                <br/>
                            
                            <div id='chkDisableBackDiv'>
                                <input id="chkDisableBack" type="checkbox" class="back_checkbox" 
                                    <cfif disabledBack eq "checked">
                                        <cfoutput> checked="checked" </cfoutput>
                                    </cfif>
                                />
                                <span class="back_label">Disable Back Button for this page</span>
                            </div>
                        </div>     	
                    </cfif>
                    
                    
                    
                </div>
                
                <!---<div id="TitleTypeSurvey" style="color:##df0000;font-size: 16px;padding: 10px 0 0 15px; clear: both;"></div>--->
                                
                <!--- Top page options - Email questions only--->
                <cfif communicationType neq SURVEY_COMMUNICATION_PHONE> <!--- AND communicationType neq SURVEY_COMMUNICATION_SMS --->
                    <div class="pager no-print" id="pager1" style="margin-top:0px; margin-bottom:0px; clear: both;">
                        <span>Page 
                            <cfloop from="1" to="#totalPage#" index="index">
                                <cfif index eq page>
                                    <cfoutput><span><strong>#index#</strong></span></cfoutput>
                                <cfelse>
                                    <cfoutput>
                                        <a href="#rootUrl#/#SessionPath#/ire/builder/index?inpbatchid=#INPBATCHID#&PAGE=#index#">
                                            <span>#index#</span>
                                        </a>
                                    </cfoutput>
                                </cfif>
                            </cfloop>
                        </span>	
                       <!--- <div class="clear advanced_option_label advanced_option_padding" onclick="showBranchPage()" style="font-weight: bold; float: right;">
                            Add Page Condition
                        </div>		--->
                    </div>
                </cfif>
                
                <ul id="TableSurveyQuestions">
                    <!--- These are now loaded asyncronously to speed up apparaent page load to user --->
                    <!--- Todo: AJAX Loader  --->
                </ul>    
                
                <cfif DoesHavePermissionAddQuestion>
                    <div class='add_box handle no-print'>
                        <button class="no-print btn btn-default add_button edit_q pageLevelAddQ " type="button" rel2="0" rel3="#communicationType#" rel4="#pageQuestion + 1#" INPAFTER="1" INPBEFORE="0">Add CP</button>
                        <button class="no-print btn btn-default add_branch_button AddBranchLogicInline pageLevelAddBranch" type="button" rel1='<cfoutput>#indexNumber#</cfoutput>' INPAFTER="1"  INPBEFORE="0">Add Branch</button>
                    </div>
                </cfif>
                
                <!--- Bottom page options - Email questions only--->
                <cfif communicationType neq SURVEY_COMMUNICATION_PHONE> <!--- AND communicationType neq SURVEY_COMMUNICATION_SMS --->
                    <div class="pager no-print" id="pager2">
                        <span>Page 
                            <cfloop from="1" to="#totalPage#" index="index">
                                <cfif index eq page>
                                    <cfoutput><span><strong>#index#</strong></span></cfoutput>
                                <cfelse>
                                    <cfoutput>
                                        <a href="#rootUrl#/#SessionPath#/ire/builder/index?inpbatchid=#INPBATCHID#&PAGE=#index#">
                                            <span>#index#</span>
                                        </a>
                                    </cfoutput>
                                </cfif>
                            </cfloop>
                        </span>			
                    </div>
                </cfif>
                
                
                <!--- Voice questions --->
                <cfif ArrayLen(questionsVoice) gt 0>
                    
                </cfif>
                
                <hr>
                
                <div style="padding-top: 5px">
                   <!--- <button id="deletePage" type="button" class="no-print btn btn-default" style="width: auto;">Delete Page</button>--->
                    
                    <cfif communicationType neq SURVEY_COMMUNICATION_ONLINE AND communicationType neq SURVEY_COMMUNICATION_SMS>
                        <cfoutput>
                            <button class="no-print btn btn-default add_prompt" QID="5"  promptValueControl="hidErrorPrompt"
                                    type="button">Add Error Prompt</button>
                  <!---          <img alt="" class="ErrorPromptRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
                            <cfif query.ERRORSCRIPT EQ "">display:none;</cfif>		
                            "scriptId="<cfoutput>#query.ERRORSCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
               --->             
                            <button class="no-print btn btn-default add_prompt" QID="6"  promptValueControl="hidTryAgainPrompt"
                                type="button">Add Try Again Prompt</button>	
                            
                          <!---  <img alt="" class="TryAgainPromptRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
                            <cfif query.TRYAGAINSCRIPT EQ "">display:none;</cfif>		
                            "scriptId="<cfoutput>#query.TRYAGAINSCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
                        --->    
                        </cfoutput>
                    </cfif>
                    
                    <!---<button id="btnFinishSurvey" class="no-print btn btn-default right" type="button">Save</button>--->
                    <div class="clear"></div>
                </div>
            
            </div>
            
            
            <div class="clear" style="height: 30px; width: 100%; display: block;">
                
            </div>
      
        </div>
        
        <div style="clear:both"></div>
        <div style="clear:both"></div>
                  
           
        <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->



</cfoutput>



<!-- Modal -->
<div class="modal fade no-print" id="EditQuestionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="RawXMLModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="MakeXMLTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="CPBranchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade no-print" id="GroupPickerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="ProgramActiveMSGBtnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="TMRMSGBtnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="RenameCPGModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="CopyCPGModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="CustomHelpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title"> Custom HELP</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade no-print" id="CustomStopModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title"> Custom STOP</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade no-print" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Schedule Options</h4>

            </div>
            <div class="modal-body">
            
                <!---<div id="AdvanceScheduleContent">
                    <cfinclude template="../../ems/dsp_advance_schedule.cfm">
                </div>--->
            
			</div>
           
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade no-print" id="DRModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Delivery Receipt Option</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


		<!--- Read question data for Contol Point Groups --->        
        <cfinvoke method="ReadGroupData" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="GetCPG">
            <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">    
            <cfinvokeargument name="INPTYPE" value="#communicationType#"> 
        </cfinvoke>

		<script type="text/javascript">
            var CurrCPGCount = 0;
        </script>

        <!---<cfdump var="#GetCPG#">  --->     
       
       <script type="text/javascript"> 
	    
       function initCPG()
       {	
                                 
		<cfif ArrayLen(GetCPG.ARRAYCPG) gt 0>
			
				               
                       
                        var MINOBJPos = -1;
						var MAXOBJPos = -1;
							
						<cfset indexNumber = 1>
                        <cfloop array="#GetCPG.ARRAYCPG#" index="CPGObj">
                            
							MINOBJPos = -1;
							MAXOBJPos = -1;
						
							<!--- Validate range --->
							<cfif CPGObj.MAXOBJ GT ArrayLen(GetCPG.ARRAYCPG) >
							
							
							</cfif>
							
							<cfif CPGObj.MINOBJ GT ArrayLen(GetCPG.ARRAYCPG) >
							
							
							</cfif>
																					
                        	CurrCPGCount++;
													
							var MINOBJPos = $("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MINOBJ#</cfoutput>']").attr('rel3');
							var MAXOBJPos = $("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MAXOBJ#</cfoutput>']").attr('rel3');
							
						<!---	console.log('<cfoutput>#CPGObj.MINOBJ#</cfoutput>');
							console.log('<cfoutput>#CPGObj.MAXOBJ#</cfoutput>');
							console.log("<cfoutput>#CPGObj.DESC#</cfoutput>");
							console.log($("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MINOBJ#</cfoutput>']"));
							console.log($("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MAXOBJ#</cfoutput>']"));
							console.log(MINOBJPos);
							console.log(MAXOBJPos);--->
																				
							if(MINOBJPos > 0 && MAXOBJPos > 0)
							{
							
								$("#TableSurveyQuestions li").slice(MINOBJPos-1, MAXOBJPos).wrapAll("<div class='ObjGroup' id='fieldset" + CurrCPGCount + "' CPGID='" + <cfoutput>#CPGObj.ID#</cfoutput> + "'></div>");	
	
	
								<!--- Description text and options --->
								$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").prepend("<div class='ControlPointGroupHeader'>"
									+ "<div class='CPGButtons no-print nosel' style='min-width: 100%; min-height:24px;'>"
									+ " <button class='no-print btn btn-default add_button edit_q Topadd_box_Q no-print' type='button' rel2='0' rel3='<cfoutput>#communicationType#</cfoutput>' rel4='" +  (<cfoutput>#CPGObj.MINOBJ#</cfoutput> - startNumberQ + 1) + "' INPBEFORE='1' INPBEFORE='0'  addCPGID='" + CurrCPGCount + "'>Add CP Above CPG</button> "
									+ " <button class='no-print btn btn-default add_branch_button AddBranchLogicInline Topadd_box_B no-print' type='button' rel1='" +  (<cfoutput>#CPGObj.MINOBJ#</cfoutput> + 1) + "' INPAFTER='1' INPBEFORE='0' addCPGID='" + CurrCPGCount + "' >Add Branch Above CPG</button> "
									+ "		<img class='btnRemoveGroup' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Letter-x-icon24x24.png' style='float:right; padding-left:7px;' width='24' height='24' title='Ungroup Control Point Group'/>"
									+ "		<img class='btnCPGCopy' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Copy-v2-icon24x24.png' style='float:right; padding-left:5px;' width='24' height='24' title='Copy Control Point Group Question Layout'/>"
									+ "		<img class='btnCPGEditDesc' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Pencil-icon24x24.png' style='float:right; padding-left:5px;' width='24' height='24' title='Rename Control Point Group'/>"
									+ "</div>"
									+ "<div class='ControlPointGroupDesc'><cfoutput>#CPGObj.DESC#</cfoutput></div>"
									+ "</div>");	
														
								<!--- Re-bind new elelement --->	
								$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnRemoveGroup").click(function()
								{
									RemoveGrouping($(this).parents("div .ObjGroup"));		
								});
								
								<!--- bind new elelement --->	
								$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnCPGEditDesc").click(function()
								{
									RenameCPG($(this).parents("div .ObjGroup"));		
								});
								
								<!--- bind new elelement --->	
								$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnCPGCopy").click(function()
								{
									CopyCPG($(this).parents("div .ObjGroup"));		
								});
								
							<!---	$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").prepend("<div class='no-print add_box Topadd_box handle'> "
										+ " <button class='no-print btn btn-default add_button edit_q Topadd_box_Q' type='button' rel2='0' rel3='<cfoutput>#communicationType#</cfoutput>' rel4='" +  (<cfoutput>#CPGObj.MINOBJ#</cfoutput> - startNumberQ + 1) + "' INPBEFORE='1' INPBEFORE='0'  addCPGID='" + CurrCPGCount + "'>Add Question Above CPG</button> "
										+ " <button class='no-print btn btn-default add_branch_button AddBranchLogicInline Topadd_box_B' type='button' rel1='" +  (<cfoutput>#CPGObj.MINOBJ#</cfoutput> + 1) + "' INPAFTER='1' INPBEFORE='0' addCPGID='" + CurrCPGCount + "' >Add Branch Above CPG</button> "
									+ "</div>"  );--->
								
								$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").append("<div class='nosel no-print CPGButtons Bottomadd_box handle'> "
										+ " <button class='no-print btn btn-default add_button edit_q Bottomadd_box_Q' type='button' rel2='0' rel3='<cfoutput>#communicationType#</cfoutput>' rel4='" +  (<cfoutput>#CPGObj.MAXOBJ#</cfoutput> - startNumberQ + 2) + "' INPAFTER='1' INPBEFORE='0' addCPGID='" + CurrCPGCount + "' >Add CP to Bottom of CPG</button> "
										+ " <button class='no-print btn btn-default add_branch_button AddBranchLogicInline Bottomadd_box_B' type='button' rel1='" +  (<cfoutput>#CPGObj.MAXOBJ#</cfoutput> + 1) + "' INPAFTER='1' INPBEFORE='0'  addCPGID='" + CurrCPGCount + "' >Add Branch to Bottom of CPG</button> "
									+ "</div>"  );
								
					<!---			BindEditQuestionInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Bottomadd_box_Q'));
								BindAddBranchLogicInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Bottomadd_box_B'));
								BindEditQuestionInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Topadd_box_Q'));
								BindAddBranchLogicInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Topadd_box_B'));
			--->				}
							
                        </cfloop>
						
                   
                    
             
        </cfif>
                            
    	}
             
	    </script>
                    
                    
                                
<!--- Javascript --->
<script>
	var currPage = '<cfoutput>#page#</cfoutput>';
	var maxPage = '<cfoutput>#totalPage#</cfoutput>';
	
	var startNumberQ = '<cfoutput>#startNumberQ#</cfoutput>';
	
	var isLastPage = false;
	
	var communicationType = '<cfoutput>#communicationType#</cfoutput>';
	var VOICETYPE = '<cfoutput>#VOICETYPE#</cfoutput>';
	
	if(currPage == maxPage){
		isLastPage = true;
	}
	//alert(communicationType);
	if(communicationType != SURVEY_COMMUNICATION_PHONE){
		if(currPage == 1){
			//$("#chkDisableBack").attr('disabled', 'disabled');
			$("#chkDisableBackDiv").hide();
		}else{
			$("#chkDisableBackDiv").show();
			//$("#chkDisableBack").attr('disabled', 'enabled');
		}
	}
	
	//var totalPage = '<cfoutput>#totalPage#</cfoutput>';
	
	if(maxPage <= 1 || communicationType == SURVEY_COMMUNICATION_PHONE){
		// hiden delete page button
		$("#deletePage").hide();
	}
	
	if(communicationType == SURVEY_COMMUNICATION_PHONE){
		$("#chkDisableBackDiv").hide();
		$("#deletePage").hide();
	}
	
	var QuestionEmailID_ARRAY, QuestionVoiceID_ARRAY;
	
	var pageQuestionNumber = '<cfoutput>#pageQuestion#</cfoutput>';
	
	var communicationType = '<cfoutput>#communicationType#</cfoutput>';
	
	var allowAddPage = '<cfoutput>#allowAddPage#</cfoutput>';
	
	var BATCHIDSurvey = '<cfOutput>#INPBATCHID#</cfOutput>';	
	
	var numberContinous = 0;
	
	var addQuestionPermission = '';
	
	var StartSortIndex = -1;
	
	var NEXTPID = <cfoutput>#NEXTPID#</cfoutput>;
	var STAGEWIDTH = <cfoutput>#STAGEWIDTH#</cfoutput>;
	
	
	
				
	$(function()
	{					
		<!--- Force modal to reload with new parameters each tine or it will just re-SHOW the old data --->
		$('body').on('hidden.bs.modal', '.modal', function () {	
			$(this).removeData('bs.modal');
		});
		
		distance: 30
		
				
		AsyncLoadQuestions();
			
		<!--- Becuase these buttons can be dynamically added - set click method here --->	
		$.each($('.pageLevelAddBranch'), function (index, item) {
	       	BindAddBranchLogicInline($(this));
	    });
		
		<!--- Becuase these buttons can be dynamically added - set click method here --->	
		$.each($('.pageLevelAddQ'), function (index, item) {
	       	BindEditQuestionInline($(this));
	    });
	
	<!---	//check add question permission
		$.ajax({
			type:"POST",
			async: false,
			url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/permission.cfc?method=checkBatchPermissionByBatchId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
				OPERATOR : '<cfoutput>#Add_Question_Title#</cfoutput>'
			},
			dataType: "json", 
			success: function(data) {
				addQuestionPermission = data;
			}
       	});--->
		
		arrQuestion = new Array();
		arrPrompt = new Array();
	
	
	
		<!---//set default array of id Email Question every load form edit Survey.It needed by generate valid HTML Survey
	    QuestionEmailID_ARRAY = [];	
	    //set default array of QID Voice Question every load form edit Survey.It needed by generate valid HTML Survey and conflict QID
	    QuestionVoiceID_ARRAY = [];
	    
	    var i = 0;
	    <cfif arraylen(totalQuestionArray2) gt 0>
			<cfloop array="#totalQuestionArray2#" index="quest">
				QuestionEmailID_ARRAY[i] = '<cfoutput>#quest.ID#</cfoutput>';
				QuestionVoiceID_ARRAY[i] = '<cfoutput>#quest.ID#</cfoutput>';
				i++;
			</cfloop>
		</cfif>
	    
	    i = 0;
	    <cfif arraylen(questionsVoice) gt 0>
			<cfloop array="#questionsVoice#" index="quest">
				QuestionVoiceID_ARRAY[i] = '<cfoutput>#quest#</cfoutput>';
				QuestionEmailID_ARRAY[i] = '<cfoutput>#quest#</cfoutput>';
				i++;
			</cfloop>
		</cfif>--->
		
		$("#addPage").click(function() {
			if (pageQuestionNumber <= 0 || allowAddPage == 'false') {
				bootbox.alert("You cannot add a page when the first or last page does not contain any questions.");
				return;
			}	
			//$("#deletePage").show();
			maxPage = addPageSurvey();
			$('#chkDisableBack').attr('checked', false);
	  	});
	  	
	  	$("button.add_prompt").click(function() {
	  		var QID = $(this).attr('QID');
	  		var clientId = $(this).attr('promptValueControl');
	  		var data = {
							INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
							INPSURVEYNAME: '<cfoutput>#Replace("Batch Desc Goes Here", "'", "\'")#</cfoutput>',
							INPSECONDLEVELNAME: 'prompts'
						}
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyElementAudioData', data, "Create script error!", function(d ) {
				var url = '<cfoutput>#rootUrl#/#SessionPath#/rxds/flash/recordercontrol</cfoutput>?QID=' + QID + '&clientId=' + clientId + '&<cfoutput>eleId=#SESSION.USERID#_</cfoutput>' + d.DATA.INPLIBID[0] + "_" + d.DATA.NEXTELEID[0];
				ShowVoiceRecordDialog(url);
			});
	  	});
	  
		$("#chkDisableBack").click( function() {
	  		var promptContent = ''
		  	var disableBack = $('#chkDisableBack').is(':checked') ? "1" : "0";
		  	
		  	savePrompt(promptContent, disableBack);
		});
		
		$("#DelAllQuestionsFromSurveyList").click(function() { 
			
			bootbox.confirm( "Are you sure you want to delete all questions from this interactive campaign?",function(result) { 
				if(result)
				{	
					DeleteAllQuestionsFromSurvey(<cfoutput>#INPBATCHID#</cfoutput>);
					return;
				}
				else
				{
					return;	
				}
			});		 
		});
		
		$("#btnExit").click(function() { 
			document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/';
		});
		
		$("#btnFinishSurvey").click(function() { 
			if ($("#TableSurveyQuestions").html() != '') {
			 	document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/';	
			} 
			else {
				<!---$.jGrowl("You can't close popup when current page not contain question.", { life:1000, position:"center", header:' Message' });--->
			}
		});
		  
		$("#deletePage").click(function() {
						
			bootbox.confirm( "Are you sure you want to delete this Page?", function(result) { 
				if(result)
				{	
					deletePage();
					maxPage = maxPage - 1;
					if(maxPage <= 1){
						$("#deletePage").hide();
					}
					
					return;
				}
				else
				{
					return;					
				}
			});
		});
		
		$("#chkGloballyDisableBack").click(function(){
			SaveEnableBack();
		});
		
		<!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->
		$("#TableSurveyQuestions").sortable({
			
			handle: ".handle",
	 		start: function(event, ui)
			{	 							
				<!---console.log('Start Sort');
				console.log('ID' + ui.item.attr('id'));
				console.log('RQ ' + ui.item.attr('RQ'));
				console.log('QUID ' + ui.item.attr('QID'));
				console.log(ui.item.index());--->
				
				<!--- Track where sort object started from--->
				StartSortIndex = ui.item.index();
				
				$(this).find('.SurveyQuestionBorder').css('border-style','dotted');	
								
	 		},		    
			stop: function(event, ui) 
			{
				
				<!---console.log('Stop Sort');
				console.log('ID' + ui.item.attr('id'));
				console.log('RQ '+ ui.item.attr('RQ'));
				console.log('QUID ' + ui.item.attr('QID'));
				console.log(ui.item.index());
				console.log(ui.item);--->
				
				<!--- Only update if position actually changes--->
				if(StartSortIndex != ui.item.index())
					updateXMLQuestionPosition('<cfoutput>#INPBATCHID#</cfoutput>', ui.item.attr('QID'), ui.item.index()  );
								
				<!--- Reset so we know what the start index was --->
				StartSortIndex = -1;		
				
				$(this).find('.SurveyQuestionBorder').css('border-style','none');				    	
				
		    }
		})
		.selectable({ filter: ".q_contents", cancel: ".handle,.nosel" });
				
		<!---.find( "li" )--->
        <!---.addClass( "ui-corner-all" )--->
        <!---.prepend( "<div class='handle'><span class='ui-icon ui-icon-carat-2-n-s'></span></div>" )--->
		
		<!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->
		$("#TableSurveyQuestions .ObjGroup").sortable({
			
			handle: ".handle",
	 		start: function(event, ui)
			{	 							
				<!---console.log('Start Sort');
				console.log('ID' + ui.item.attr('id'));
				console.log('RQ ' + ui.item.attr('RQ'));
				console.log('QUID ' + ui.item.attr('QID'));
				console.log(ui.item.index());--->
				
				<!--- Track where sort object started from--->
				StartSortIndex = ui.item.index();
				
				$(this).find('.SurveyQuestionBorder').css('border-style','dotted');	
								
	 		},		    
			stop: function(event, ui) 
			{
				
				<!---console.log('Stop Sort');
				console.log('ID' + ui.item.attr('id'));
				console.log('RQ '+ ui.item.attr('RQ'));
				console.log('QUID ' + ui.item.attr('QID'));
				console.log(ui.item.index());
				console.log(ui.item);--->
				
				<!--- Only update if position actually changes--->
				if(StartSortIndex != ui.item.index())
					updateXMLQuestionPosition('<cfoutput>#INPBATCHID#</cfoutput>', ui.item.attr('QID'), ui.item.index()  );
								
				<!--- Reset so we know what the start index was --->
				StartSortIndex = -1;		
				
				$(this).find('.SurveyQuestionBorder').css('border-style','none');				    	
				
		    }
		})
		.selectable({ filter: ".q_contents", cancel: ".handle, .nosel" });
		
		<!--- Toggle the Toggle Button Button function --->
		$("#xmlBtn").click(function()
		{	
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) 
	
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_XMLControlString' + ParamStr
			}
	
			$('#RawXMLModal').modal(options);	
						
		});		
		
		<!--- Toggle the Toggle Button Button function --->
		$("#ScheduleBtn").click(function()
		{
			
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) 
	
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/dsp_advance_schedule' + ParamStr
			}
	
			$('#ScheduleOptionsModal').modal(options);	
			
		<!---	
			window.location='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/scheduleCampaign?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>&mode=edit'
		--->				
		});		
		
		<!--- Program Active Message Editor  --->
		$("#ProgramActiveMSGBtn").click(function()
		{
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) 
	
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_SetProgramActiveMessage' + ParamStr
			}
	
			$('#ProgramActiveMSGBtnModal').modal(options);
						
		});		
		
		<!--- Program Active Message Editor  --->
		$("#TMRMSGBtn").click(function()
		{
			
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) 
							
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_SetTMRMessage' + ParamStr
			}
	
			$('#TMRMSGBtnModal').modal(options);
						
		});				
		
		<!--- Toggle the Toggle Button Button function --->
		$("#ToggleToggleBtnBtn").click(function()
		{
			if($("#ToggleToggleBtnBtn").html() == "Hide Buttons")
				$("#ToggleToggleBtnBtn").html("Show Buttons");
			else
				$("#ToggleToggleBtnBtn").html("Hide Buttons");
			
			$(".add_box").toggle();
			$(".button_box").toggle();
			$(".CPGButtons").toggle();
			
		});
		
		<!--- Undo function --->
		$("#undoBtn").click(function()
		{
			if ($('#undoBtn').is('.inactive') == false)
			{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/history.cfc?method=UndoMCID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> },
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
					success: function(d2)
					{
						if(typeof(d2.DATA) != "undefined"){
							if(d2.DATA.RXRESULTCODE[0] < 0){
								bootbox.alert(d2.DATA.MESSAGE[0]);
								return false;
							}
						}else{
						
							if (d2 == 0)
							{
								$('#undoBtn').removeClass().addClass('inactive');
							}
							else if (d2 == 1)
							{
								location.reload();
								$('#undoBtn').removeClass().addClass('inactive')
								$('#redoBtn').removeClass().addClass('active');
							}
							else
							{
								location.reload();
								$('#undoBtn').removeClass().addClass('active');
								$('#redoBtn').removeClass().addClass('active');
							}
						
						}
						
					}
				});
			}
		});
	
		<!--- Redo function --->
		$("#redoBtn").click(function()
		{
			if ($('#redoBtn').is('.active'))
			{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/history.cfc?method=RedoMCID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					datatype: 'json',
					data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> },
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
					success: function(d2, textStatus, xhr)
					{
						if (d2 == 0)
						{
							$('#redoBtn').removeClass().addClass('inactive');
						}
						else if (d2 == 1)
						{
							location.reload();
							////$('#redoBtn').removeClass();
							$('#undoBtn').removeClass().addClass('active');
							$('#redoBtn').removeClass().addClass('active');
						}
						else
						{
							location.reload();
							$('#undoBtn').removeClass().addClass('active');
							$('#redoBtn').removeClass().addClass('active');
						}
					}
				});
			}
		});
		
		<cfif QNav GT 0>
			if($("#QLabel_<cfoutput>#QNav#</cfoutput>").length > 0)
				$(document).scrollTop( $("#QLabel_<cfoutput>#QNav#</cfoutput>").offset().top ); 
		</cfif>
		
				
				
		<!--- btnGroup --->
		<!--- Toggle the Toggle Button Button function --->
		$("#btnGroup").click(function()
		{	
			addgrouping();			
		});
		
		$("#btnMoveNewPage").click(function()
		{	
			AddToNewPage();			
		});
	
	<!---			
		$(".btnRemoveGroup").click(function()
		{
			RemoveGrouping($(this).parents("div .ObjGroup"));		
		});
	--->
		
		$("#DelAllCPGFromSurveyList").click(function()
		{	
			
			bootbox.confirm( "Are you sure you want to delete all Control Point Groups from this interactive campaign?", function(result) { 
				if(result)
				{	
					RemoveAllGrouping();
					return;
				}
				else
				{
					return;	
				}
			});		
						
		});		
		
	});
	
	
	<!--- Add Grouping based on Selection --->
	function addgrouping()
	{		
		<!--- Validate no over lap --->
							
		<!--- Get min and max of list of elements --->	
		function minMaxId(selector) 
		{
			var min=null, max=null;
			$(selector).each(function(index, item) {			  
				var id = parseInt($(item).attr("rel1"), 10);
				if ((min===null) || (id < min)) { min = id; }
				if ((max===null) || (id > max)) { max = id; }
			});
			return {min:min, max:max};
		}
		
		<!--- Get min max of selected elements --->	
		var Result = minMaxId('.ui-selected'); <!---// => {min:1, max:5}--->
	
		<!---var MINOBJPos = $("#TableSurveyQuestions li[rq='" + Result.min + "']").index();
		var MAXOBJPos = $("#TableSurveyQuestions li[rq='" + Result.max + "']").index();--->
		
		var MINOBJPos = Result.min;
		var MAXOBJPos = Result.max;
			
		<!---	
		console.log(Result);
		console.log(Result.min);
		console.log(Result.max);
		console.log(MINOBJPos);
		console.log(MAXOBJPos);
		--->
											
		var NewCPG = "<CPG ID='0' MINOBJ='" + MINOBJPos + "' MAXOBJ='" + MAXOBJPos + "'></CPG>"
		
		<!--- Update DB --->
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=AddNewControlPointGrouping&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
					inpCPGXML: NewCPG, 
					INPTYPE: '<cfoutput>#communicationType#</cfoutput>'
				   },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{																			
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Update Display --->
								
								
								
								var NewMINOBJPos = $("#TableSurveyQuestions li[rq='" + MINOBJPos + "']").attr('rel3');
								var NewMAXOBJPos = $("#TableSurveyQuestions li[rq='" + MAXOBJPos + "']").attr('rel3');
							
						<!---	console.log("<cfoutput>#CPGObj.DESC#</cfoutput>");
							console.log($("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MINOBJ#</cfoutput>']"));
							console.log($("#TableSurveyQuestions li[rq='<cfoutput>#CPGObj.MAXOBJ#</cfoutput>']"));
							console.log(MINOBJPos);
							console.log(MAXOBJPos);--->
																				
								if(NewMINOBJPos > 0 && NewMAXOBJPos > 0)
								{
									CurrCPGCount++;
							
									$("#TableSurveyQuestions li").slice(NewMINOBJPos-1, NewMAXOBJPos).wrapAll("<div class='ObjGroup' id='fieldset" + CurrCPGCount + "' CPGID='" + CurrCPGCount + "'></div>");	
									
									<!--- Description text and options --->
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").prepend("<div class='ControlPointGroupHeader'>"
									+ "<div class='CPGButtons no-print nosel' style='min-width: 600px;  min-height:24px;'>"
									+ " <button class='no-print btn btn-default add_button edit_q Topadd_box_Q' type='button' rel2='0' rel3='<cfoutput>#communicationType#</cfoutput>' rel4='" +  (parseInt(MINOBJPos) - parseInt(startNumberQ) + 1) + "' INPBEFORE='1' INPBEFORE='0'  addCPGID='" + CurrCPGCount + "'>Add CP Above CPG</button> "
									+ " <button class='no-print btn btn-default add_branch_button AddBranchLogicInline Topadd_box_B' type='button' rel1='" +  (parseInt(MINOBJPos) + 1) + "' INPAFTER='1' INPBEFORE='0' addCPGID='" + CurrCPGCount + "' >Add Branch Above CPG</button> "
									+ "		<img class='no-print btnRemoveGroup' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Letter-x-icon24x24.png' style='float:right; padding-left:7px;' width='24' height='24' title='Ungroup Control Point Group'/>"
									+ "		<img class='no-print btnCPGCopy' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Copy-v2-icon24x24.png' style='float:right; padding-left:5px;' width='24' height='24' title='Copy Control Point Group Question Layout'/>"
									+ "		<img class='no-printbtnCPGEditDesc' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/Pencil-icon24x24.png' style='float:right; padding-left:5px;' width='24' height='24' title='Rename Control Point Group'/>"
									+ "</div>"
									+ "<div class='ControlPointGroupDesc'>This is a new Control Point Grouping</div>"
									+ "</div>");						
										
									<!--- Re-bind new elelement --->	
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnRemoveGroup").click(function()
									{
										RemoveGrouping($(this).parents("div .ObjGroup"));		
									});
									
									<!--- bind new elelement --->	
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnCPGEditDesc").click(function()
									{
										RenameCPG($(this).parents("div .ObjGroup"));		
									});
									
									<!--- bind new elelement --->	
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find(".btnCPGCopy").click(function()
									{
										CopyCPG($(this).parents("div .ObjGroup"));		
									});
									
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").append("<div class='no-print CPGButtons Bottomadd_box handle'> "
										+ "	 <button class='no-print btn btn-default add_button edit_q Bottomadd_box_Q' type='button' rel2='0' rel3='<cfoutput>#communicationType#</cfoutput>' rel4='" +  (MAXOBJPos + 1) + "' INPAFTER='1' INPBEFORE='0'>Add CP to Bottom of CPG</button> "
										+ "	 <button class='no-print btn btn-default add_branch_button AddBranchLogicInline Bottomadd_box_B' type='button' rel1='" +  (MAXOBJPos + 1) + "' INPAFTER='1' INPBEFORE='0'>Add Branch  to Bottom of CPG</button> "
										+ "</div>"
									);
																									
									BindEditQuestionInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Bottomadd_box_Q'));
									BindAddBranchLogicInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Bottomadd_box_B'));
									BindEditQuestionInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Topadd_box_Q'));
									BindAddBranchLogicInline($("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").find('.Topadd_box_B'));
																	
									<!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->
									$("#TableSurveyQuestions div[CPGID='" + CurrCPGCount + "']").sortable({
										
										handle: ".handle",
										start: function(event, ui)
										{	 							
											<!---console.log('Start Sort');
											console.log('ID' + ui.item.attr('id'));
											console.log('RQ ' + ui.item.attr('RQ'));
											console.log('QUID ' + ui.item.attr('QID'));
											console.log(ui.item.index());--->
											
											<!--- Track where sort object started from--->
											StartSortIndex = ui.item.index();
											
											$(this).find('.SurveyQuestionBorder').css('border-style','dotted');	
															
										},		    
										stop: function(event, ui) 
										{
											
											<!---console.log('Stop Sort');
											console.log('ID' + ui.item.attr('id'));
											console.log('RQ '+ ui.item.attr('RQ'));
											console.log('QUID ' + ui.item.attr('QID'));
											console.log(ui.item.index());
											console.log(ui.item);--->
											
											<!--- Only update if position actually changes--->
											if(StartSortIndex != ui.item.index())
												updateXMLQuestionPosition('<cfoutput>#INPBATCHID#</cfoutput>', ui.item.attr('QID'), ui.item.index()  );
															
											<!--- Reset so we know what the start index was --->
											StartSortIndex = -1;		
											
											$(this).find('.SurveyQuestionBorder').css('border-style','none');				    	
											
										}
									})
									.selectable({ filter: ".q_contents", cancel: ".handle, .nosel" });									
											
								}
		
								return true;		
							}
							else
							{								
								bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->		
						bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
					}				
				} 		
				
			});
			
	  return false;			
		
	}
	
	function RenameCPG(inpObj) 
	{
		var inpOldDesc = inpObj.find('.ControlPointGroupDesc').html();
			
		var ParamStr = '?inpbatchid=' + encodeURIComponent('<cfoutput>#INPBATCHID#</cfoutput>') + '&inpCPGID=' +  parseInt(inpObj.attr("CPGID")) + '&INPTYPE=' + encodeURIComponent('<cfoutput>#communicationType#</cfoutput>')  + '&inpOldDesc=' + encodeURIComponent(inpOldDesc) ;
		
		var options = 
		{
			show: true,
			"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_renameControlPointerGroup' + ParamStr
		}

		$('#RenameCPGModal').modal(options);			
	}
	
	function CopyCPG(inpObj) 
	{					
		var ParamStr = '?inpbatchid=' + encodeURIComponent('<cfoutput>#INPBATCHID#</cfoutput>') + '&inpCPGID=' +  parseInt(inpObj.attr("CPGID")) + '&INPTYPE=' + encodeURIComponent('<cfoutput>#communicationType#</cfoutput>');
				
		var options = 
		{
			show: true,
			"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_CopyControlPointerGroup' + ParamStr
		}

		$('#CopyCPGModal').modal(options);		
	}
	
	function RemoveGrouping(inpObj)
	{
		<!--- Update DB  --->
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=RemoveControlPointGrouping&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
					inpCPGID: parseInt(inpObj.attr("CPGID")), 
					INPTYPE: '<cfoutput>#communicationType#</cfoutput>'
				   },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{																			
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Update Display --->
								inpObj.find(".ControlPointGroupHeader").remove();
								inpObj.find(".Bottomadd_box").remove();
								
								var cnt = inpObj.contents()
								inpObj.replaceWith(cnt);	
								
								CurrCPGCount--;
								
								return true;		
							}
							else
							{								
								bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->		
						bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
					}				
				} 		
				
			});
		
		return false;				
	}
	
	function RemoveAllGrouping()
	{
		<!--- Update DB  --->
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=RemoveAllControlPointGrouping&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
					INPTYPE: '<cfoutput>#communicationType#</cfoutput>'
				   },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{																			
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Update Display --->
								
								$.each($('.ObjGroup'), function (index, item) {
									
									$(item).find(".ControlPointGroupHeader").remove();									
									$(item).find(".Bottomadd_box").remove();
								
									var cnt = $(item).contents()
									$(item).replaceWith(cnt);
								});
															
								CurrCPGCount = 0;
								
								return true;		
							}
							else
							{								
								bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->		
						bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
					}				
				} 		
				
			});
		
		return false;				
	}
	
	function UpdateCPGData(inpCPGID, inpNewLocation, inpNewObjLocation, inpDirection)
	{		
		<!--- Read current Control Point Group Data --->
		
		<!--- Get min and max of list of elements based on rq attribute values--->	
		function minMaxId(selector) 
		{
			var min=null, max=null;
			$(selector).each(function(index, item) {			  
				var id = parseInt($(item).attr("rq"), 10);
				if ((min===null) || (id < min)) { min = id; }
				if ((max===null) || (id > max)) { max = id; }
			});
			return {min:min, max:max};
		}
		
		<!--- Get min max of selected elements from this  --->	
		var Result = minMaxId("#TableSurveyQuestions div[CPGID='" + inpCPGID + "'] li"); <!---// => {min:1, max:5}--->
		
		var MINOBJPos = Result.min;
		var MAXOBJPos = Result.max;	
		
		var inpDesc = "NA";	
		
		
	<!---	console.log($("#TableSurveyQuestions div[CPGID='" + inpCPGID + "'] li"));
		console.log(MINOBJPos);
		console.log(MAXOBJPos);
		console.log(Result);--->
		
	
		if(MINOBJPos == null || MAXOBJPos==null)
		{
			RemoveGrouping($("#TableSurveyQuestions div[CPGID='" + inpCPGID + "']"));
		}
		else
		{		
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=UpdateControlPointGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: false,
				data:  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPTYPE: '<cfoutput>#communicationType#</cfoutput>',
						inpCPGID: inpCPGID, 
						inpMin: MINOBJPos,
						inpMax: MAXOBJPos,
						inpDesc: inpDesc
					   },					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{																			
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{																		
									UpdateCPGDataAboveBelow(inpNewLocation, inpNewObjLocation, inpDirection)
									
									return true;		
								}
								else
								{
									
									<!---bootbox.alert("Control Point Group not updated."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]); --->		
								}
							}
							else
							{<!--- Invalid structure returned --->	
								 return false;
							}
						}
						else
						{<!--- No result returned --->		
							
							<!---bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");--->
						}				
					} 		
					
			});
		}
		
		return false;
	}
	
	function UpdateCPGDataAboveBelow(inpNewLocation, inpNewObjLocation, inpDirection)
	{	
	
		<!---console.log("inpNewLocation=" + inpNewLocation);
		console.log("inpNewObjLocation=" + inpNewObjLocation);
		console.log("inpDirection=" + inpDirection);--->
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=UpdateCPGDataAboveBelow&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
					INPTYPE: '<cfoutput>#communicationType#</cfoutput>',
					inpDirection: inpDirection, 
					inpNewObjLocation: inpNewObjLocation
				   },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{																			
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								reIndexQuestions();
								
								if(inpNewLocation != '')
									window.location = inpNewLocation;									
								
								return true;		
							}
							else
							{
								
								<!---bootbox.alert("Control Point Group not updated."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
--->							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->		
						
						<!---bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");--->
					}				
				} 		
				
		});
		
		
		return false;
	}

	function updateXMLQuestionPosition(BatchID, INPQID, INPNEWPOS)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/smssurvey.cfc?method=MoveQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID: BatchID, 
					INPQID: INPQID, 
					INPNEWPOS: INPNEWPOS,
					STARTNUMBERQ: startNumberQ 
				   },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
					if(d.DATA.RXRESULTCODE[0] == '-4'){
						window.location = "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home";
					}																
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								reIndexQuestions();
								return true;		
							}
							else
							{								
								bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
							}
						}
						else
						{<!--- Invalid structure returned --->	
							 return false;
						}
					}
					else
					{<!--- No result returned --->		
						
						bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
					}				
				} 		
				
			});
		  return false;
	}
		
	<!--- Becuase these buttons can be dynamically added - set click method here --->
	function BindAddBranchLogicInline(inpObj)
	{
		inpObj.click(function(){
			
			var ParamStr = '';
			
			var CurrCPGID = 0;
			
			<!--- Search all parents for GorupObj if it exists --->
			if(typeof($(this).parents("div .ObjGroup").attr("CPGID")) != "undefined")
				CurrCPGID = $(this).parents("div .ObjGroup").attr("CPGID");
			
			if(typeof($(inpObj).attr("addCPGID")) != "undefined" && CurrCPGID == 0)
				CurrCPGID = $(inpObj).attr("addCPGID");
						
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) + '&INPPOSITION=' + inpObj.attr("rel1") + '&INPBEFORE=' + inpObj.attr("INPBEFORE") + '&INPAFTER=' + inpObj.attr("INPAFTER") + '&INPCPGID=' + CurrCPGID + '&INPPAGE=' + '<cfoutput>#page#</cfoutput>';
				
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_BranchQuestionInline' + ParamStr
			}
	
			$('#CPBranchModal').modal(options);	
						
		});
	}
	
	<!--- Becuase these buttons can be dynamically added - set click method here --->
	function BindEditQuestionInline(inpObj)
	{
		
		<!---	
			console.log(inpObj);
			console.log(inpObj.attr("rel2"))
			console.log(inpObj);
			console.log(inpObj.parent().attr("CPGID"));
			console.log(inpObj.parent().parent().attr("CPGID"));
			console.log(inpObj.parent().parent().parent().attr("CPGID"));
		--->
		
		inpObj.click(function(){
			
			<!---console.log('BindEditQuestionInline');--->
			
			var ParamStr = '';
			
			var CurrCPGID = 0;
			
			<!--- Search all parents for GorupObj if it exists --->
			if(typeof($(this).parents("div .ObjGroup").attr("CPGID")) != "undefined")
				CurrCPGID = $(this).parents("div .ObjGroup").attr("CPGID");
				
			if(typeof($(this).attr("addCPGID")) != "undefined" && CurrCPGID == 0)
				CurrCPGID = $(this).attr("addCPGID");
				
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) +  '&INPQID=' + $(this).attr("rel2") + '&INPCOMTYPE=' + $(this).attr("rel3") + '&INPPOSITION=' + $(this).attr("rel4") + '&INPBEFORE=' + $(this).attr("INPBEFORE") + '&INPAFTER=' + $(this).attr("INPAFTER") + '&INPCPGID=' + CurrCPGID + '&INPPAGE=' + '<cfoutput>#page#</cfoutput>';
		
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_EditQuestionInline' + ParamStr
			}
	
			$('#EditQuestionModal').modal(options);	
			
		});
	}
		
		
	<!--- javascript functions --->
	function addPageSurvey() {
		var new_gid = parseInt(currPage) + 1;
		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						GID: new_gid,
						INPTYPE: '<cfoutput>#communicationType#</cfoutput>'
					};
					
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'AddPage', data, "Page has not been added", function() {
			 window.location = '<cfoutput>#rootUrl#/#sessionPath#/ire/builder/index?inpbatchid=#INPBATCHID#&PAGE=</cfoutput>' + new_gid;
		}); 
		return maxPage;
	}
	
	function deletePage() {
		var data = {
						INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
						INPPAGEID : currPage,
						INPTYPE: '<cfoutput>#communicationType#</cfoutput>'
					};
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'DeletePage', data, "Page has not deleted", function() {
			currPage = 1;
							
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid=' + '<cfoutput>#INPBATCHID#</cfoutput>';
		}); 
	}
	
	function DeleteAllQuestionsFromSurvey(INPBATCHID) {
		var data = 	{
						INPBATCHID : INPBATCHID
					};
					
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'DeleteAllSurveyQuestions', data, "Error - Survey questions have not beed deleted", function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid=' + '<cfoutput>#INPBATCHID#</cfoutput>';
		}); 
	}
		
	function savePrompt(promptContent,disableBack) {
		var data = {
						INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
						INPGROUPID : currPage,
						INPPROMPT : promptContent,
						INPBACK : disableBack
					};
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'UpdatePrompt', data, "Prompt has not saved", function() {
			
		}); 
	}
	
	function SaveEnableBack() {
		var data = { 
					INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput>,
					INPDISABLEBACK : $("#chkGloballyDisableBack").is(":checked") ? 0 : 1,
					INPISONLYUPDATEDISABLEBACK : true
				};
			
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'UpdateSurveyCustomize', data, "Disable Back has not saved", function() {
			bootbox.alert("Globally disable back button saved successfully");
		}); 
	}
	
	function SaveSystemPromptVoice(QID, scriptId, desc) {
		if (desc == '<cfoutput>#VOICEPROMPT#</cfoutput>') {
			desc = '';
		}
		switch (QID) {
			case "5": {
				if (scriptId != "") {
					$('.ErrorPromptRecord').show();
					$('.ErrorPromptRecord').attr('scriptId', scriptId)
				}
				else {
					$('.ErrorPromptRecord').hide();
				}
				break;
			}
			case "6": {
				if (scriptId != "") {
					$('.TryAgainPromptRecord').show();
					$('.TryAgainPromptRecord').attr('scriptId', scriptId)
				}
				else {
					$('.TryAgainPromptRecord').hide();
				}
				break;
			}
			default: {
				break;
			}
		}
			
		
		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPDESC: desc,
						INPPROMPTMCID: QID,
						INPSCRIPTID: scriptId
					}
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyPromptAudioData', data, "Create script error!", function(d ) {
		});
	}
	

<!--- Add Grouping based on Selection --->
	function AddToNewPage()
	{		
		<!--- Validate no over lap --->
							
		<!--- Get min and max of list of elements --->	
		function minMaxId(selector) 
		{
			var min=null, max=null;
			$(selector).each(function(index, item) {			  
				var id = parseInt($(item).attr("rel1"), 10);
				if ((min===null) || (id < min)) { min = id; }
				if ((max===null) || (id > max)) { max = id; }
			});
			return {min:min, max:max};
		}
		
		<!--- Get min max of selected elements --->	
		var Result = minMaxId('.ui-selected'); <!---// => {min:1, max:5}--->
	
		<!---var MINOBJPos = $("#TableSurveyQuestions li[rq='" + Result.min + "']").index();
		var MAXOBJPos = $("#TableSurveyQuestions li[rq='" + Result.max + "']").index();--->
		
		var MINOBJPos = Result.min;
		var MAXOBJPos = Result.max;
			
			<!---
		console.log(Result);
		console.log(Result.min);
		console.log(Result.max);
		console.log(MINOBJPos);
		console.log(MAXOBJPos);
		--->
											
		var NewCPG = "<CPG ID='0' MINOBJ='" + MINOBJPos + "' MAXOBJ='" + MAXOBJPos + "'></CPG>"
		
		<!--- Update DB --->
		
		var new_gid = parseInt(currPage) + 1;
		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						GID: new_gid,
						INPTYPE: '<cfoutput>#communicationType#</cfoutput>',
						INPSTARTQID : MAXOBJPos
					};
					
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'AddBelowToNewPage', data, "Page has not been added", function() {
			 window.location = '<cfoutput>#rootUrl#/#sessionPath#/ire/builder/index?inpbatchid=#INPBATCHID#&PAGE=</cfoutput>' + new_gid;
		}); 
		return maxPage;	
		
	}
		
	function AsyncLoadQuestions()
	{						
		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						GROUPID : '<cfoutput>#PAGE#</cfoutput>'
					};
					
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'ReadXMLQuestions1', data, "Page has not been loaded", function(d) 
		{
			<!--- User feedback - loading --->
			$("#PageLoadingInfo").html('Page Loading... <span id="PageTotalSoFar">' + ii + '</span> of <span id="PageTotalToLoad">' + d.DATA.ARRAYQUESTION[0].length + '</span>');	
			
			if(d.DATA.ARRAYQUESTION[0].length == 0)
			{
				$("#PageLoadingInfo").html('');		
			}
										
			<!--- Loop over results --->
			for (var ii=0; ii <  d.DATA.ARRAYQUESTION[0].length; ii++)
			{					
			
				$("#PageTotalSoFar").html('' + (ii+1));
						
				<!--- Call shared cfc method to get display content - use for page load as well as AJAX updates --->
				
				var CurrQuestionObj = new Object();
				CurrQuestionObj.Description = d.DATA.ARRAYQUESTION[0][ii].TEXT;
				CurrQuestionObj.TEXT = d.DATA.ARRAYQUESTION[0][ii].TEXT;
				CurrQuestionObj.CurrQuestionObjType = d.DATA.ARRAYQUESTION[0][ii].TYPE;
				CurrQuestionObj.TYPE = d.DATA.ARRAYQUESTION[0][ii].TYPE;
				CurrQuestionObj.AF = d.DATA.ARRAYQUESTION[0][ii].AF;
				CurrQuestionObj.IntervalType = d.DATA.ARRAYQUESTION[0][ii].ITYPE;
				CurrQuestionObj.ITYPE = d.DATA.ARRAYQUESTION[0][ii].ITYPE;
				CurrQuestionObj.IntervalValue = d.DATA.ARRAYQUESTION[0][ii].IVALUE;
				CurrQuestionObj.IVALUE = d.DATA.ARRAYQUESTION[0][ii].IVALUE;
				CurrQuestionObj.IntervalHour = d.DATA.ARRAYQUESTION[0][ii].IHOUR;
				CurrQuestionObj.IHOUR = d.DATA.ARRAYQUESTION[0][ii].IHOUR;
				CurrQuestionObj.IntervalMin = d.DATA.ARRAYQUESTION[0][ii].IMIN;
				CurrQuestionObj.IMIN = d.DATA.ARRAYQUESTION[0][ii].IMIN;
				CurrQuestionObj.IntervalNoon = d.DATA.ARRAYQUESTION[0][ii].INOON;
				CurrQuestionObj.INOON = d.DATA.ARRAYQUESTION[0][ii].INOON;
				CurrQuestionObj.IntervalExpiredNextRQ = d.DATA.ARRAYQUESTION[0][ii].IENQID;
				CurrQuestionObj.IENQID = d.DATA.ARRAYQUESTION[0][ii].IENQID;		
				CurrQuestionObj.ErrMsgTxt = d.DATA.ARRAYQUESTION[0][ii].ERRMSGTXT;
				CurrQuestionObj.RequiresAns = d.DATA.ARRAYQUESTION[0][ii].REQUIREDANS; 
				CurrQuestionObj.scriptId = '';
				CurrQuestionObj.LISTANSWER = JSON.stringify(d.DATA.ARRAYQUESTION[0][ii].LISTANSWER);	
				CurrQuestionObj.Conditions = JSON.stringify(d.DATA.ARRAYQUESTION[0][ii].CONDITIONS);
				CurrQuestionObj.RQ = parseInt(ii+1) + parseInt(startNumberQ) - 1;
				CurrQuestionObj.ID = d.DATA.ARRAYQUESTION[0][ii].ID;
				CurrQuestionObj.inpCondXML = '';
				CurrQuestionObj.PAGE = '<cfoutput>#PAGE#</cfoutput>';
				CurrQuestionObj.BOFNQ = d.DATA.ARRAYQUESTION[0][ii].BOFNQ;
				CurrQuestionObj.IntervalMRNR = d.DATA.ARRAYQUESTION[0][ii].IMRNR;
				CurrQuestionObj.IMRNR = d.DATA.ARRAYQUESTION[0][ii].IMRNR;
				CurrQuestionObj.IntervalNRMO = d.DATA.ARRAYQUESTION[0][ii].INRMO;
				CurrQuestionObj.INRMO = d.DATA.ARRAYQUESTION[0][ii].INRMO;
				
				var data2 =  { 
								INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
								questionJSON : JSON.stringify(CurrQuestionObj),						
								startNumberQ: '<cfoutput>#startNumberQ#</cfoutput>' ,
								indexNumber : ii + 1,
								DoesHavePermissionAddQuestion : '<cfoutput>#DoesHavePermissionAddQuestion#</cfoutput>',
								DoesHavePermissionEditQuestion : '<cfoutput>#DoesHavePermissionEditQuestion#</cfoutput>',
								DOESHAVEPERMISSIONDELETEQUESTION : '<cfoutput>#DOESHAVEPERMISSIONDELETEQUESTION#</cfoutput>',
								COMMUNICATIONTYPE : '<cfoutput>#COMMUNICATIONTYPE#</cfoutput>',
								PAGE : '<cfoutput>#PAGE#</cfoutput>',
								includeLI : 1,
								inpXMLControlString : '',
								UniqueReqCount : ii + 1						
							};	
				
							
				<!--- Run stuff on page loading complete --->	
				if( (d.DATA.ARRAYQUESTION[0].length - 1) == ii )
				{
					ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/smssurvey.cfc', 'generateQuestionBox', data2, "Problem generating Question", function(d2) 
					{												
						var el = $(d2.QOUT);
																		
						$("#TableSurveyQuestions").append(el);
						<!--- Bind click events to children of newly appended object --->
						BindAddBranchLogicInline(el.find('.AddBranchLogicInline'));
						BindEditQuestionInline(el.find('.edit_q'));
												
						<!--- Run stuff on page loading complete --->	
						initCPG();
						
						$("#PageLoadingInfo").html('');						
						
					});					
					
				}
				else
				{
					ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/smssurvey.cfc', 'generateQuestionBox', data2, "Problem generating Question", function(d2) 
					{						
						var el = $(d2.QOUT);
																		
						$("#TableSurveyQuestions").append(el);
						<!--- Bind click events to children of newly appended object --->
						BindAddBranchLogicInline(el.find('.AddBranchLogicInline'));
						BindEditQuestionInline(el.find('.edit_q'));
						
					});
				
				}
			
				
			}
			
		}); 
		
	}
		
</script>

<!---<cfsetting showdebugoutput="yes">--->