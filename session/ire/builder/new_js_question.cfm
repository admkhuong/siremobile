<script language="javascript" src="survey.js"></script>

<cfoutput>
	
	</cfoutput>
	<script type="text/javascript">  
		var SHORTANSWER = "SHORTANSWER";
		var ONETOTENANSWER = "ONETOTENANSWER"
		var ONESELECTION = "ONESELECTION";
		var STRONGAGREEORDISAGREE = "STRONGAGREEORDISAGREE";
		var NETPROMTERSCORE = "NETPROMTERSCORE";
		var RECOMMENDATION = "RECOMMENDATION";
		var SATISFACTION = "SATISFACTION";
		var STATEMENT = "STATEMENT";
		var INTERVAL = "INTERVAL";
		var API = "API";
		var OPTIN = "OPTIN";
		var CDF = "CDF";
		var RESET = "RESET";
		var AGENT = "AGENT";
		var OTTPOST = "OTTPOST";
		var TRAILER = "TRAILER";
		<!--- var STRONGDISAGREE = "Strong Disagree";
		var DISAGREE = "Disagree";
		var NEITHERDISAGREEORAGREE = "Neither Disagree or Agree";
		var AGREE = "Agree";
		var STRONGAGREE = "Strong Agree";
		var EXTREMELYLIKELY = "Extremely likely";
		var NOTATALLLIKELY = "Not at all likely";
		var EXTREMELYSATISFIED = "Extremely satisfied";
		var NOTATALLSATISFIED = "Not at all satisfied"; --->
		var VOICE = SURVEY_COMMUNICATION_PHONE;
		var EMAIL = SURVEY_COMMUNICATION_ONLINE;
		var SMS = SURVEY_COMMUNICATION_SMS;
		var BOTH = SURVEY_COMMUNICATION_BOTH;
		var VOICE_SINGLE_SELECTION_rxt = 2;
		var VOICE_SHORT_ANSWER_rxt = 3;
		var TWO_DIGIT_VOICE_SINGLE_SELECTION_rxt = 6;
		var INVALIDQUESTIONID = 2;
		var ERRORQUESTIONID = 3;
		var ERRORPROMPTMCID = 5;
		var TRYAGAINMCID = 6;
		var FIRSTQID = 7;
		
		UniqueReqCount = 1;
						
		function IsInList(QuestionID_ARRAY, questionID) {
			var result = false;
			for (var i = 0; i < QuestionID_ARRAY.length; ++i) {
				if (QuestionID_ARRAY[i] == questionID) {
					result = true;
					break;
				}
			}
			
			return result;
		}
		function is_array(input){
		  return typeof(input)=='object'&&(input instanceof Array);
		}
		function delete_array(input,value) {
			var output = new Array();
			if (is_array(input)) {
				for(var i=0; i<input.length; i++) {
					if (value != input[i]) { output.push(input[i])}
				}
			}
			return output;
		}
	   
		<!--- Shared method between Branch and regular Questions - used for update or new depending if 0 is passed in for new QuestionId--->
		function AddNewQuestion(inpBefore, inpAfter, inpCPGID, QuestionDesc, QuestionType, Answers, AnswersVal, AF, scriptId, ComType, isgenerateInValid, isgenerateError, QuestionID, GroupId, position, isRequiredAns, errMsgTxt, IntervalType, IntervalValue, IntervalHour, IntervalMin, IntervalNoon, IntervalExpiredNextRQ, IntervalMRNR, IntervalNRMO, OIG, ReloadPage, inpCondXML, BrachOptionFalseNextQuestion, CondXMLArray)
		{						
			<!--- QuestionID undefined when create a new question.It will be assigned by max QuestionID--->
			 var isNew = false;//check question is new or not.			 
						 
			 if(parseInt(QuestionID) == 0) 
			 {				
			 	<!--- get max Question ID --->
				NEXTPID++;  
				QuestionID = NEXTPID;
				isNew = true;
			 		    
			 }
		
			if(typeof(CondXMLArray) == "undefined")
			{
				CondXMLArray = new Array();				
			}
			
			if(typeof(IntervalExpiredNextRQ) == "undefined")
			{
				IntervalExpiredNextRQ = "0";				
			}
		
			<!--- Call shared cfc method to get display content - use for page load as well as AJAX updates --->
			
			var CurrQuestionObj = new Object();
			CurrQuestionObj.Description = QuestionDesc;
			CurrQuestionObj.TEXT = QuestionDesc;
			CurrQuestionObj.CurrQuestionObjType = ComType;
			CurrQuestionObj.TYPE = QuestionType;
			CurrQuestionObj.AF = AF;
			CurrQuestionObj.OIG = OIG;
			CurrQuestionObj.IntervalType = IntervalType;
			CurrQuestionObj.ITYPE = IntervalType;
			CurrQuestionObj.IntervalValue = IntervalValue;
			CurrQuestionObj.IVALUE = IntervalValue;
			CurrQuestionObj.IntervalHour = IntervalHour;
			CurrQuestionObj.IHOUR = IntervalHour;
			CurrQuestionObj.IntervalMin = IntervalMin;
			CurrQuestionObj.IMIN = IntervalMin;
			CurrQuestionObj.IntervalNoon = IntervalNoon;
			CurrQuestionObj.INOON = IntervalNoon;
			CurrQuestionObj.IntervalExpiredNextRQ = IntervalExpiredNextRQ;
			CurrQuestionObj.IENQID = IntervalExpiredNextRQ;	
			CurrQuestionObj.IMRNR = IntervalMRNR;
			CurrQuestionObj.INRMO = IntervalNRMO;		
			CurrQuestionObj.ErrMsgTxt = errMsgTxt;
			CurrQuestionObj.RequiresAns = isRequiredAns; 
			CurrQuestionObj.Answers = Answers;
			CurrQuestionObj.AnswersVal = AnswersVal;
			CurrQuestionObj.scriptId = scriptId;
			
			
			<!--- Pull from Form directly and skip passing in - easier to maintain - change so other variable work this way too  --->
			CurrQuestionObj.TemplateDesc = $("#TemplateDesc").val();
			CurrQuestionObj.MDF = $("#MDF").val();
			CurrQuestionObj.API_TYPE = $("#API_TYPE").val();
			CurrQuestionObj.API_DOM = $("#API_DOM").val();
			CurrQuestionObj.API_DIR = $("#API_DIR").val();
			CurrQuestionObj.API_PORT = $("#API_PORT").val();
			CurrQuestionObj.API_DATA = $("#API_DATA").val();	
			CurrQuestionObj.API_RA = $("#API_RA").val();	
			CurrQuestionObj.API_ACT = $("#API_ACT").val();		
			
			CurrQuestionObj.SCDF = $("#SCDF").val();	
								
			CurrQuestionObj.LISTANSWER = new Array();
									
			for (var ii=0; ii < CurrQuestionObj.Answers.length; ii++)
			{
			
				var AnswersObj = new Object();
						
				AnswersObj.ID = ii + 1;
				AnswersObj.TEXT = CurrQuestionObj.Answers[ii];	
				AnswersObj.AVAL = CurrQuestionObj.AnswersVal[ii];				
				 
				CurrQuestionObj.LISTANSWER.push(AnswersObj);	
				
			}
						
			CurrQuestionObj.Conditions = CondXMLArray;
			CurrQuestionObj.Conditions = JSON.stringify(CurrQuestionObj.Conditions);			
			CurrQuestionObj.RQ = parseInt(position) + parseInt(startNumberQ) - 1;				
			CurrQuestionObj.ID = QuestionID;
			
			CurrQuestionObj.inpCondXML = inpCondXML;
			CurrQuestionObj.PAGE = GroupId;
			CurrQuestionObj.BOFNQ = BrachOptionFalseNextQuestion;
			CurrQuestionObj.LISTANSWER = JSON.stringify(CurrQuestionObj.LISTANSWER);
			
			UniqueReqCount++;		
			var data =  { 
							INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
							questionJSON : JSON.stringify(CurrQuestionObj),						
							startNumberQ: startNumberQ,
							indexNumber : position, 
							DoesHavePermissionAddQuestion : '<cfoutput>#DoesHavePermissionAddQuestion#</cfoutput>',
							DoesHavePermissionEditQuestion : '<cfoutput>#DoesHavePermissionEditQuestion#</cfoutput>',
							DOESHAVEPERMISSIONDELETEQUESTION : '<cfoutput>#DOESHAVEPERMISSIONDELETEQUESTION#</cfoutput>',
							COMMUNICATIONTYPE : '<cfoutput>#COMMUNICATIONTYPE#</cfoutput>',
							PAGE : '<cfoutput>#PAGE#</cfoutput>',
							includeLI : 1,
							inpXMLControlString : '',
							UniqueReqCount : UniqueReqCount							
						};		
				
			<!---console.log(data);	--->
				
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/SMSSurvey.cfc', 'generateQuestionBox', data, "Problem generating Question", function(d ) 
			{
			<!--- Update Display --->			
			
				var replaceAfter = position - 1;
											
				<!---
				console.log("position=" + position);
				console.log("replaceAfter=" + replaceAfter);
				console.log("pageQuestionNumber=" + pageQuestionNumber);
				console.log("inpCPGID=" + inpCPGID);
				console.log("inpAfter=" + inpAfter);
				--->
				
				 var el = $(d.QOUT);
				 
				 if(ComType == EMAIL)
				 {
					 if(pageQuestionNumber <= 0)
					 {
						$("#TableSurveyQuestions").append(el);
					 }
					 else
					 {
						if(replaceAfter != 0 && inpCPGID <= 0)
						{
							$("#q_" + replaceAfter).after(el);
						}
						else
						{
							$("#q_" + position).before(el);
						}
					 }
				 }
				 else if(ComType == SMS)
				 {
					 if(pageQuestionNumber <= 0)
					 {					
						$("#TableSurveyQuestions").append(el);
					 }
					 else
					 {
						<!--- if position above is in Control Point Group then put in group--->
						if(parseInt(inpCPGID) > 0 && parseInt(inpAfter) > 0 && replaceAfter > 0)
						{
							$("#q_" + replaceAfter).after(el);						
						}
						else
						{
							if(parseInt(inpAfter) > 0 && replaceAfter > 0)
								$("#q_" + replaceAfter).after(el);	
							else
							{
								if(parseInt(inpBefore) > 0 && parseInt(inpCPGID) > 0)
								{
									<!---	
									console.log("parent");
									console.log($("#q_" + position).parent());
									--->
									
									$("#q_" + position).parent().before(el);
								}
								else
								{								
									$("#q_" + position).before(el);
								}
							}
						}
					 }
				 }
				 else if (ComType == VOICE)
				 {	 
					 if(pageQuestionNumber <= 0)
					 {
						$("#TableSurveyQuestions").append(el);
					 }
					 else
					 {
						if(replaceAfter != 0 && inpCPGID <= 0)
						{
							$("#q_" + replaceAfter).after(el);
						}
						else
						{
							$("#q_" + position).before(el);
						}
					 }
				 }
																
				<!--- Bind click events to children of newly appended object --->
				BindAddBranchLogicInline(el.find('.AddBranchLogicInline'));
				BindEditQuestionInline(el.find('.edit_q'));
							 
				 //if user add a new question-->generate XML question and save it to database
				 if(isNew)
				 {
					
					if(ComType == SMS)
					{
						var inpXML = generateXmlBaseQuestionType(CurrQuestionObj);
					}
					if(ComType == EMAIL)
					{
						var inpXML = generateXmlBaseQuestionType(CurrQuestionObj);
					}
					else if(ComType == VOICE)
					{
						var inpXML = generateXmlVoiceBaseQuestionType(QuestionID, QuestionDesc, QuestionType, Answers, isgenerateInValid, isgenerateError, scriptId, IntervalType, IntervalValue, IntervalHour, IntervalMin, IntervalNoon, IntervalExpiredNextRQ); 		
					}
						
					SaveQuestions(inpCPGID, inpXML, ComType, position, QuestionID, ReloadPage); 	 
				 }		 

			
			});	
	
	
		}
		
		
		<!--- Is this code ever reached? --->
		function AddNewQuestionVoiceEmail(inpBefore, inpAfter, inpCPGID, questionDesc, questionType, answers, AF, scriptId, isgenerateValid, isgenerateError, questionID, GroupId, position, isRequiredAns, errMsgTxt, IntervalType, IntervalValue, IntervalHour, IntervalMin, IntervalNoon, IntervalExpiredNextRQ, IntervalMRNR, IntervalNRMO, OIG, ReloadPage)
		{				
			 var isNew = false;//check question is new or not.
			
			 if(parseInt(QuestionID) == 0)
			 {
			 	NEXTPID++;  
				QuestionID = NEXTPID;
				isNew = true;
			}
	
			//generate HTML question and push it to form edit survey questions.
			
			var inpHTMLBoth = generateTmpHtml(questionID, questionDesc, questionType, answers ,SURVEY_COMMUNICATION_BOTH, GroupId, position, isRequiredAns, scriptId, AF, IntervalType, IntervalValue, IntervalMRNR, IntervalNRMO);
			
			var replaceAfter = position - 1;
			
			var el = $(inpHTMLBoth);
			
			if(pageQuestionNumber <= 0){
			 	 $("#TableSurveyQuestions").append(el);
			}else{
			 	if(replaceAfter != 0){
				 	$("#q_" + replaceAfter).after(el);
				}else{
				 	$("#q_" + position).before(el);
				}
			}			
									
			<!--- Bind click events to children of newly appended object --->
			BindAddBranchLogicInline(el.find('.AddBranchLogicInline'));
			BindEditQuestionInline(el.find('.edit_q'));
			 	 
			reIndexQuestions();
		  		
			//if user add a new question-->generate XML question and save it to database
			if (isNew) { 	
				var inpXMLEmail = generateXmlBaseQuestionType(questionID, questionDesc, questionType, 
			 									answers, AF, GroupId, isRequiredAns, errMsgTxt, IntervalType, IntervalValue);
			 									
			 	var inpXMLVoice = generateXmlVoiceBaseQuestionType(questionID, questionDesc, questionType, 
			 									answers, AF, isgenerateValid, isgenerateError, scriptId, IntervalType, IntervalValue); 		
			    
			    SaveQuestionVoiceEmail(inpXMLEmail, inpXMLVoice, position, questionID, ReloadPage); 	 
			 }		 	
		}
		
		function deleteQuestion(obj,BatchID,QuestionID,ComType,currPage, CurrentRQ)
		{	
			<!--- Move to attr for easier update after other object is deleted or added --->
			CurrentRQ = $(obj).attr("rq");
																		
			$.ajax({
				type:"POST",
				async: false,
				url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/permission.cfc?method=checkBatchPermissionByBatchId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				data:{
					INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
					OPERATOR : '<cfoutput>#delete_Question_Title#</cfoutput>'
				},
				dataType: "json", 
				success: function(data) {
					if(!data.HAVEPERMISSION)
					{
						bootbox.alert(data.MESSAGE);
						return false;
					}
					else
					{
					
						bootbox.confirm('Are you sure you want to delete this control point?', function(r) {
						if (r == true) 
						{
							$.ajax({
								type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
								url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=DeleteQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
								dataType: 'json',
								async: false,
								data:  { INPBATCHID : BatchID,INPQUESTIONID : QuestionID,INPTYPE:ComType},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
								success:		
									<!--- Default return function for Do CFTE Demo - Async call back --->
									function(d) 
									{
										<!--- Alert if failure --->
										if(d.DATA.RXRESULTCODE[0] == '-4'){
											window.location = "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home";
										}																	
										<!--- Get row 1 of results if exisits--->
										if (d.ROWCOUNT > 0) 
										{						
											<!--- Check if variable is part of JSON result string --->								
											if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
											{	
												var QLableNav = '';
												
												if(CurrentRQ > 1)
													QLableNav = '&QNav=' + (parseInt(CurrentRQ) - 1);
												
												<!--- check if part of group --->
												var CurrCPGID = 0;
												
												<!--- Search all parents for GorupObj if it exists --->
												if(typeof($(obj).parents("div .ObjGroup").attr("CPGID")) != "undefined")
													CurrCPGID = $(obj).parents("div .ObjGroup").attr("CPGID");
		
												var inpNewLocation = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid="
													+ BatchID + "&PAGE=" + currPage + QLableNav;
												
												<!--- Remove current question from page --->															
												$(".q_box[rq='" + CurrentRQ + "']").remove();
												
												reIndexQuestions();
													
												if(CurrCPGID > 0)
												{	
													UpdateCPGData(CurrCPGID, '', CurrentRQ, -1);
												}
												else
												{						
													UpdateCPGDataAboveBelow('', CurrentRQ, -1);							
													// window.location = inpNewLocation;															
												}
																														
											}
											else
											{<!--- Invalid structure returned --->	
												
											}
										}
										else
										{<!--- No result returned --->		
											
											bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
										}			
														
									} 		
									
								});
								
								return;
							}
							else
							{
								return;	
							}
						});		
						return false;
					}
				}
	       	});
	       	
		}
				
		<!--- Resuse add box ???? --->
		function EditBranchLogic(QuestionID, ComType, position, inpParent)
		{		
			var CurrCPGID = 0;
			
			<!--- Search all parents for GorupObj if it exists --->
			if(typeof(inpParent.parents("div .ObjGroup").attr("CPGID")) != "undefined")
				CurrCPGID = inpParent.parents("div .ObjGroup").attr("CPGID");				
				
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) + '&INPPOSITION=' + position + '&INPQID=' + QuestionID + '&INPCOMTYPE=' + ComType + '&INPCPGID=' + CurrCPGID + '&INPPAGE=' + '<cfoutput>#page#</cfoutput>';
				
			var options = 
			{
				show: true,
				"remote" : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/dsp_branchquestioninline' + ParamStr
			}
	
			$('#CPBranchModal').modal(options);	
			
		}			
		
		function updateXMLQuestion(BatchID, QuestionID, inpXMLEmail, inpXMLSMS, inpXMLVoice, ComType, currPage, ReloadPage){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=UpdateQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  { 
					INPBATCHID: BatchID, 
					INPQUESTIONID: QuestionID, 
					INPXMLEmail: inpXMLEmail, 
					INPXMLVoice: inpXMLVoice, 
					INPXMLSMS : inpXMLSMS,
					INPTYPE: ComType},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
						if(d.DATA.RXRESULTCODE[0] == '-4'){
							window.location = "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home";
						}																
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									$('#EditQuestionModal').modal('hide');	
									
									reIndexQuestions();
										
									if(ReloadPage)
										window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid=" + BatchID + "&PAGE=" + currPage;
									else
									{
										<!--- Manually close modal--->
										$('#EditQuestionModal').modal('hide');
										
									}
						
									return true;		
								}
								else
								{
						            
									bootbox.alert("Question not found."  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);	
								}
							}
							else
							{<!--- Invalid structure returned --->	
								 return false;
							}
						}
						else
						{<!--- No result returned --->		
						 	
							bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
						}
											
									
				} 		
				
			});
		  return false;
		}
		
		function generateXmlVoiceBaseQuestionType(QuestionID, QuestionDesc, QuestionType, Answers, isgenerateInValid, isgenerateError, scriptId)
		{
			var QuestionDesc = RXencodeXML(QuestionDesc);
			var tmpVoiceQ = ''; //ELE Question and answer
			var invalidReponse = ''; //ELE Response
			var CK6 = '0';
			var CK7 = '0';
			var CK9 = '0'
			var CK10 = '0'
			var XPos = 150;
			var YPos = 250;
			var CK5 = '0';
			var CK1 = '0';
			var CK11 = '0';
			var CK3 = '0';
			var CK8 = '0';
			var CK12 = '0';
			var DI = '0', DS = '0', DSE = '0', BS = '1';
			var TTSXml = '0';
			var LINK = '-1'
			var CK4 = '';
			var CK2 = "(";
			var width = parseInt(STAGEWIDTH);
			var userId = '<cfoutput>#SESSION.USERID#</cfoutput>'
			var rxt = QuestionType == SHORTANSWER ? VOICE_SHORT_ANSWER_rxt : VOICE_SINGLE_SELECTION_rxt;
			
			var columnCount = Math.ceil(width / XPos) - 2;
			var columnIndex = QuestionID % columnCount
			if (columnIndex == 0) {
				columnIndex = columnCount;		
			}
			var rowIndex = Math.ceil(QuestionID / columnCount);
			
			QuestionDesc += '?';
	
			
			<!---// generate invalid and error response if not exists
			var isInvalidResponseExists = IsInList(QuestionEmailID_ARRAY, INVALIDQUESTIONID);
			if (!isInvalidResponseExists) {
				invalidReponse += "<ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry, That is an invalid response!' DI='0' DS='0' DSE='0' DSUID='" + userId + "' LINK='-1' QID='" + INVALIDQUESTIONID + "' RXT='1' X='" + (XPos*INVALIDQUESTIONID) + "' Y='" + YPos + "'>";
				invalidReponse += "<ELE ID='TTS' RXBR='16' RXVID='" + VOICETYPE + "'>I am sorry, That is an invalid response!</ELE>";
		        invalidReponse += "</ELE>";
		        QuestionEmailID_ARRAY.push(INVALIDQUESTIONID);	 
	  			QuestionVoiceID_ARRAY.push(INVALIDQUESTIONID);	
	        }
	        var isErrorResponseExists = IsInList(QuestionEmailID_ARRAY, ERRORQUESTIONID);
			if (!isErrorResponseExists) {
				invalidReponse += "<ELE BS='1' CK1='0' CK5='-1' DESC='I did not understand. Please try again.' DI='0' DS='0' DSE='0' DSUID='" + userId + "' LINK='-1' QID='" + ERRORQUESTIONID + "' RXT='1' X='" + (XPos*ERRORQUESTIONID) + "' Y='" + YPos + "'>";
				invalidReponse += "<ELE ID='TTS' RXBR='16' RXVID='" + VOICETYPE + "'>I did not understand. Please try again.</ELE>";
	        	invalidReponse += "</ELE>"; 
	        	QuestionEmailID_ARRAY.push(ERRORQUESTIONID);	 
	  			QuestionVoiceID_ARRAY.push(ERRORQUESTIONID);	
			}--->
			
			
			if (QuestionType == ONESELECTION || QuestionType == ONETOTENANSWER || QuestionType == STRONGAGREEORDISAGREE || QuestionType == NETPROMTERSCORE) {
				var numberOfRepeats = $('#ddlNumberOfInvalidRepeats').val();
	
				if (Answers.length >= 10) {
					rxt = TWO_DIGIT_VOICE_SINGLE_SELECTION_rxt;
					CK9 = '';
					CK10 = '';
					CK11 = numberOfRepeats;
					CK3 = $('#ddlNumberOfBetweenKeyPresses').val();
					CK12 = '5';
				}
				else {
					CK5 = '-1';
					CK1 = numberOfRepeats;
					CK8 = '5';
				}
				for (i = 0; i < Answers.length; i++) {
					if (Answers[i] != '') {
						QuestionDesc += 'Press ' + (i + 1) + ' for ' + RXencodeXML(Answers[i]);
						if (i != Answers.length - 1) {
							QuestionDesc += ",";	
						}
					}	
				}
				
				if (isgenerateInValid) {
					if (Answers.length >= 10) {
						CK10 = INVALIDQUESTIONID;
					}
					else {
						CK7 = INVALIDQUESTIONID;
					}
				}
				
				if (isgenerateError) {
					if (Answers.length >= 10) {
						CK9 = ERRORQUESTIONID;
					}
					else {
						CK6 = ERRORQUESTIONID;
					}
				}
			}
			
			if (QuestionType == SHORTANSWER) {
				if (QuestionID == FIRSTQID) {
					CK8 = '4';
					LINK = '4'
				}
			}
			else {
				if (QuestionID == FIRSTQID) {
					CK5 = '4';
					LINK = '4'
					
					for (i = 0; i < Answers.length; i++) {
						if (Answers[i] != '') {
							if (i != 0) {
								CK4 += ',';
								CK2 += ',';
							}
							CK4 += "(" + (i + 1) + ",4)";
							CK2 += i + 1; 
						}	
					}
				}
			}
			CK2 += ")";
			
			if (scriptId != "") {
				var quesScriptIds = scriptId.split('_')
				DI = quesScriptIds[3];
				DS = quesScriptIds[1];
				DSE = quesScriptIds[2];
				BS = '0'
			}
			else {
				TTSXml = "<ELE ID='TTS' RXBR='16' RXVID='" + VOICETYPE + "'>" + QuestionDesc + "</ELE>";	
			}
	
			tmpVoiceQ += "<ELE BS='" + BS + "' CK1='" + CK1 + "' CK10='" + CK10 + "' CK11='" + CK11 + "' CK12='" + CK12 + "' CK2='" + CK2 + "' CK3='" + CK3 + "' CK4='" + CK4 + "' CK5='" + CK5 + "' CK6='" + CK6 + "' CK7='" + CK7 + "' CK8='" + CK8 + "' CK9='" + CK9 + "' CP='0' DESC='" + QuestionDesc + "' DI='" + DI + "' DS='" + DS + "' DSE='" + DSE + "' DSUID='" + userId + "' LINK='" + LINK + "' QID='" + QuestionID + "' RQ='0' RXT='" + rxt + "' X='" + (XPos * columnIndex) + "' Y='" + (YPos * rowIndex) + "'>";
	        tmpVoiceQ += TTSXml;
		    tmpVoiceQ += "</ELE>";	    	
		    //add root tag process validate well-form XML later
			xmlQuestion = '<RXSSVOICE>' + tmpVoiceQ + invalidReponse + '</RXSSVOICE>';
	
			return xmlQuestion;
		}
		
		function generateXmlBaseQuestionType(CurrQuestionObj)
		{
		
		// QuestionID, QuestionDesc, QuestionType, Answers, AF, GroupId, isRequiredAns, errMsgTxt, IntervalType, IntervalValue, IntervalHour, IntervalMin, IntervalNoon, IntervalExpiredNextRQ, inpCondXML, BrachOptionFalseNextQuestion) {
												
			if (typeof(CurrQuestionObj.PAGE) == 'undefined') {
				CurrQuestionObj.PAGE = 1;
			}
			
			var xmlQuestion='';	
			<!---Why was this here?!?!?   var questionType = questionType == SHORTANSWER ? SHORTANSWER : ONESELECTION;--->
			
			
			<!---console.log("QuestionType=" + QuestionType);--->
			<!---console.log("inpCondXML=" + inpCondXML);--->
			
						
			var BranchXML = '';
		
			<!--- All fields required or will fail XML validation --->	
			if(CurrQuestionObj.TYPE == 'BRANCH')			
				BranchXML += " BOFNQ='" + CurrQuestionObj.BOFNQ + "'";
				
			var APIXML = '';
		
			<!--- All fields required or will fail XML validation --->	
			if(CurrQuestionObj.TYPE == 'API')			
			{
				APIXML += " API_TYPE='" + CurrQuestionObj.API_TYPE + "'";	
				APIXML += " API_DOM='" + RXencodeXML(CurrQuestionObj.API_DOM) + "'";	
				APIXML += " API_DIR='" + RXencodeXML(CurrQuestionObj.API_DIR) + "'";	
				APIXML += " API_PORT='" + RXencodeXML(CurrQuestionObj.API_PORT) + "'";	
				APIXML += " API_DATA='" + RXencodeXML(CurrQuestionObj.API_DATA) + "'";	
				APIXML += " API_RA='" + RXencodeXML(CurrQuestionObj.API_RA) + "'";	
				APIXML += " API_ACT='" + RXencodeXML(CurrQuestionObj.API_ACT) + "'";	
				
			}
			
			var CDFXML = '';
		
			<!--- All fields required or will fail XML validation --->	
			if(CurrQuestionObj.TYPE == 'CDF')			
			{
				CDFXML += " SCDF='" +  CurrQuestionObj.SCDF + "'";								
			}
						
			xmlQuestion += "<Q ID='" + CurrQuestionObj.ID + "' TYPE='" + CurrQuestionObj.TYPE 
								+ "' TEXT='" + RXencodeXML(nl2br(CurrQuestionObj.TEXT, true)) + "' TDESC='" + RXencodeXML(nl2br(CurrQuestionObj.TemplateDesc, true)) + "' GID='" + CurrQuestionObj.PAGE + "' MDF='" + CurrQuestionObj.MDF + "' " + "AF='" +  CurrQuestionObj.AF + "' "  + "OIG='" +  CurrQuestionObj.OIG + "' "  
								+ "ITYPE='" +  CurrQuestionObj.ITYPE + "' " + "IVALUE='" +  CurrQuestionObj.IVALUE + "' " + "IHOUR='" +  CurrQuestionObj.IHOUR + "' " + "IMIN='" +  CurrQuestionObj.IMIN + "' "  + "INOON='" +  CurrQuestionObj.INOON + "' " + "IENQID='" + CurrQuestionObj.IENQID + "' " + "IMRNR='" + CurrQuestionObj.IMRNR + "' "+ "INRMO='" + CurrQuestionObj.INRMO + "' "
								+ "REQANS='" +  CurrQuestionObj.isRequiredAns + "' errMsgTxt='" + RXencodeXML(CurrQuestionObj.errMsgTxt) + "' " + BranchXML + " " + APIXML + CDFXML + ">";
			
			if(CurrQuestionObj.TYPE == 'BRANCH')			
				xmlQuestion += CurrQuestionObj.inpCondXML;
			
											
			idRowOpt = 1;
			idColOpt = 1;
			switch (CurrQuestionObj.TYPE) {
				case ONESELECTION:
				case ONETOTENANSWER:
				case STRONGAGREEORDISAGREE:
				case NETPROMTERSCORE:
					for (i = 0; i < CurrQuestionObj.Answers.length; i++) {
						if (CurrQuestionObj.Answers[i] != '') {          <!--- CurrQuestionObj.Answers[i].AVAL --->
							xmlQuestion += "<OPTION ID='" + idRowOpt + "' TEXT='" + RXencodeXML(CurrQuestionObj.Answers[i]) + "' AVAL='" +  RXencodeXML(CurrQuestionObj.AnswersVal[i]) + "'>0</OPTION>";
							idRowOpt++;
						}
					}  
				    break;
				default :
					break;
				};
				xmlQuestion += '</Q>';
										
				<!---console.log("xmlQuestion=" + xmlQuestion);--->
												
				return xmlQuestion;
		}
	
	
		function SaveQuestions(inpCPGID, XMLQuestion, ComType, position, questionId, ReloadPage) {
		 		$.ajax({
	 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=SaveQuestionsSurvey&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					  datatype: 'json',
	  				  async: false,
					  data:  { 
					  			INPBATCHID: BATCHIDSurvey, 
					  			inpXML: XMLQuestion, 
					  			INPTYPE: String(ComType), 
					  			POSITION: parseInt(position), 
					  			STARTNUMBERQ: startNumberQ, 
					  			INPQuestionID: questionId
		  			  },
					  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->
					  },
					  success:
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d2, textStatus, xhr ) 
							{
								<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
								var d = eval('(' + xhr.responseText + ')');
								
								<!--- Alert if failure --->
									if(d.DATA.RXRESULTCODE[0] == '-4'){
										window.location = "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home";
										<!--- Kill buttons  --->
										$('.button').hide();
									}	
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{											
										<!--- Check if variable is part of JSON result string --->
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{
											CURRRXResultCode = d.DATA.RXRESULTCODE[0];
											if(CURRRXResultCode > 0)
											{
												
												var NewLocation = '';
												
												reIndexQuestions();
												
												if(ReloadPage == 'true')
												{																							
													// location.reload();
													
													var QLableNav = '';
													
													if(position > 0)
														QLableNav = '&QNav=' + (parseInt(position) );
																										
													// NewLocation = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid="
													//				+ BATCHIDSurvey + "&PAGE=" + currPage + QLableNav;			
												
													NewLocation = '';
													
													<!--- Kill buttons  --->
													//$('.button').hide();		
													
													$('#EditQuestionModal').modal('hide');										
												}
												else
												{
												 	
												}
																												
												<!---
													console.log("inpCPGID=" + inpCPGID);
													console.log("NewLocation=" + NewLocation);
													console.log("position=" + position);
												--->
												
												<!--- Update Control Point Group data if any --->
												if(parseInt(inpCPGID) > 0)
													UpdateCPGData(inpCPGID, NewLocation, position + parseInt(startNumberQ)-2, 1);
												else
													UpdateCPGDataAboveBelow(NewLocation, position + parseInt(startNumberQ)-2, 1)
												
												
												<!---window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/builder/index?inpbatchid="
								            		+ BATCHIDSurvey + "&PAGE=" + currPage;--->
								            	return true;
											}
										}
										else
										{
										 
										}
									}
									else
									{
									   bootbox.alert("Unable save data to DB.Please check connection.");
									}
	
							}
				} );
			return false;
		}
		
		function SaveQuestionVoiceEmail(inpXMLEmail, inpXMLVoice, position, questionId, ReloadPage) {
				
		 		$.ajax({
	 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=SaveQuestionVoiceEmail&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					  datatype: 'json',
					  async: false,
					  data:  { 
					  			INPBATCHID : BATCHIDSurvey, 
					  			inpXMLEmail: inpXMLEmail,
					  			inpXMLVoice: inpXMLVoice, 
					  			POSITION: position, 
					  			STARTNUMBERQ: startNumberQ, 
					  			INPQuestionID: questionId
		  			  },
					  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->
					  },
					  async: false,
					  success:
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d2, textStatus, xhr ) 
							{
								<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
								var d = eval('(' + xhr.responseText + ')');
								
								<!--- Alert if failure --->
									if(d.DATA.RXRESULTCODE[0] == '-4'){
										window.location = "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home";
									}
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{	
										
										<!--- Check if variable is part of JSON result string --->
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{
											CURRRXResultCode = d.DATA.RXRESULTCODE[0];
											if(CURRRXResultCode > 0)
											{
												if(ReloadPage == 'true')
												{													
										////			location.reload();
												}
											}
										}
										else
										{
										}
									}
									else
									{
									   bootbox.alert("Unable save data to DB.Please check connection.");
									}
	
							}
				} );
	
			return false;
		}
				
		
		function ClearQuestion() {
			$("#QuestionDesc").val('');
	 		
			$(".answerValue").val('');
		}
		
<!---		<!--- Groan! - The ultimate in spahgetti code --->
		// Push new question informationt to list
		function pushNewQuestionInfor() {
			questionInfors.push(GetQuestionObject());
		}--->
						
		function  GetQuestionObject() {
			var question = new Object();
			question.Description = $("#QuestionDesc").val();
			question.TEXT = $("#QuestionDesc").val();
			question.TemplateDesc = $("#TemplateDesc").val();
			question.MDF = $("#MDF").val();
			question.QuestionType = $("#ddlQuestionType").val();
			question.TYPE = $("#ddlQuestionType").val();
			question.AF = $("#ddlDisplayOption").val();
			question.OIG = $("#inpOIGValue").val();
			question.IntervalType = $("#ddlIntervalType").val();
			question.ITYPE = $("#ddlIntervalType").val();
			question.IntervalValue = $("#inpIntervalValue").val();
			question.IVALUE = $("#inpIntervalValue").val();
			question.IntervalHour = $("#ddlHour").val();
			question.IHOUR = $("#ddlHour").val();
			question.IntervalMin = $("#ddlMinute").val();
			question.IMIN = $("#ddlMinute").val();
			question.IntervalNoon = $("#ddlNoon").val();
			question.INOON = $("#ddlNoon").val();
			question.IntervalExpiredNextRQ = $("#ddlIENQID").val();
			question.IENQID = $("#ddlIENQID").val();
			question.IMRNR = $("#IntervalMRNR").val();
			question.INRMO = $("#IntervalNRMO").val();
			question.IntervalMRNR = $("#IntervalMRNR").val();
			question.IntervalNRMO = $("#IntervalNRMO").val();
			
			question.API_TYPE = $("#API_TYPE").val();
			question.API_DOM = $("#API_DOM").val();
			question.API_DIR = $("#API_DIR").val();
			question.API_PORT = $("#API_PORT").val();
			question.API_DATA = $("#API_DATA").val();
			question.API_RA = $("#API_RA").val();
			question.API_ACT = $("#API_ACT").val();
			
			question.SCDF = $("#SCDF").val();
			
			<!---console.log('$("#IntervalMRNR").val() = ' + $("#IntervalMRNR").val());
			console.log('$("#IntervalNRMO").val() = ' + $("#IntervalNRMO").val());--->
					
			// Check comunication type
			if(communicationType == SURVEY_COMMUNICATION_ONLINE || communicationType == SURVEY_COMMUNICATION_BOTH || communicationType == SURVEY_COMMUNICATION_SMS){
				// Check required answer
				var isRequiredAnswer = $("#requiredAns").is(":checked");
			
				if(isRequiredAnswer){
					question.ErrMsgTxt = $("#errMsgText").val();
					question.RequiresAns = 1; // true - required
				}else{
					question.ErrMsgTxt = "";
					question.RequiresAns = 0; // false - dont require
				}
			}else{
				question.ErrMsgTxt = "";
				question.RequiresAns = 0; // false - dont require
			}
			
			
			question.Answers = new Array();
			question.AnswersVal = new Array();
			question.scriptId = $("#hidQuesScriptId").val();
	<!--- 		question.questionAudio = new Object();
			question.questionAudio.answerAudios = new Array();
			question.questionAudio.questionAudio = $("#hidQuesScriptId").val();
			question.questionAudio.question =  $("#QuestionDesc").val(); --->
			switch (question.QuestionType) {
				case ONESELECTION: {
					$.each($('.answerValue'), function (index, item) {
	                    question.Answers.push($(this).val());
	                    
	                    <!--- var audioPath = $("#hidScriptId_" + $(this).attr("customField")).val();
	                    if (audioPath != "") {
		                    var answerAudio = new Object();
		                    answerAudio.answer = $(this).val();
		                    answerAudio.audioPath = audioPath;
		                    
		                    question.questionAudio.answerAudios.push(answerAudio);
	                    } --->
	                });
					
					$.each($('.AVAL'), function (index, item) {
	                    question.AnswersVal.push($(this).val());
	                    
	                    <!--- var audioPath = $("#hidScriptId_" + $(this).attr("customField")).val();
	                    if (audioPath != "") {
		                    var answerAudio = new Object();
		                    answerAudio.answer = $(this).val();
		                    answerAudio.audioPath = audioPath;
		                    
		                    question.questionAudio.answerAudios.push(answerAudio);
	                    } --->
	                });
					
					break;
				}
				case ONETOTENANSWER: 
				case NETPROMTERSCORE: {
					var lowestValue = question.QuestionType ==  ONETOTENANSWER ? 1 : 0;
					var rateType = $('#ddlRateType').val();
					
					var highest = rateType == RECOMMENDATION ? EXTREMELYLIKELY : EXTREMELYSATISFIED;
					var lowest = rateType == RECOMMENDATION ? NOTATALLLIKELY : NOTATALLSATISFIED;
			
					for (var i = lowestValue; i <= 10; ++i) {
						if (i == 10) {
							question.Answers.push(i + ' ' + highest);
						}
						else if (i == lowestValue) {
							question.Answers.push(i + ' ' + lowest);
						}
						else {
							question.Answers.push(i);
						}
					}				
							
					break;
				}
				case STRONGAGREEORDISAGREE: {
					question.Answers.push(STRONGDISAGREE);
					question.Answers.push(DISAGREE);
					question.Answers.push(NEITHERDISAGREEORAGREE);
					question.Answers.push(AGREE);
					question.Answers.push(STRONGAGREE);
				}
			}
			
			if($("#ddlQuestionType").val() == TRAILER)
			{
				question.QuestionType = TRAILER
				question.TYPE = TRAILER
			}
			else if($("#ddlQuestionType").val() == INTERVAL)
			{
				question.QuestionType = INTERVAL
				question.TYPE = INTERVAL
			}
			else if($("#ddlQuestionType").val() == API)
			{
				question.QuestionType = API
				question.TYPE = API
			}
			else if($("#ddlQuestionType").val() == OPTIN)
			{
				question.QuestionType = OPTIN
				question.TYPE = OPTIN
			}
			else if($("#ddlQuestionType").val() == RESET)
			{
				question.QuestionType = RESET
				question.TYPE = RESET
			}
			else if($("#ddlQuestionType").val() == CDF)
			{
				question.QuestionType = CDF
				question.TYPE = CDF
			}
			else if($("#ddlQuestionType").val() == AGENT)
			{
				question.QuestionType = AGENT
				question.TYPE = AGENT
			}
			else if($("#ddlQuestionType").val() == STATEMENT)
			{
				question.QuestionType = STATEMENT
				question.TYPE = STATEMENT
			}
			else if($("#ddlQuestionType").val() == OTTPOST)
			{
				question.QuestionType = OTTPOST
				question.TYPE = OTTPOST
			}
			else
			{	<!--- Lots of Display Question Types are really just single selection in diferent formats --->
				question.QuestionType = $("#ddlQuestionType").val() == SHORTANSWER ? SHORTANSWER : ONESELECTION;
				question.TYPE = $("#ddlQuestionType").val() == SHORTANSWER ? SHORTANSWER : ONESELECTION;
			}
			
			question.LISTANSWER = new Array();
						
			for (var ii=0; ii < question.Answers.length; ii++)
			{
			
				var AnswersObj = new Object();
			
				AnswersObj.ID = ii + 1;
				AnswersObj.TEXT = question.Answers[ii];	
				AnswersObj.AVAL = question.AnswersVal[ii];				
				 
				question.LISTANSWER.push(AnswersObj);	
				
			}
								
			return question;
		}
		
		function VisibleAdvancedOptions(obj) {
			var display = $('#divAdvancedOptions').css('display');
			if (display == 'none') {
				$('#divAdvancedOptions').show();
				$(obj).html("Hide Advanced Options")
			}
			else {
				$('#divAdvancedOptions').hide();
				$(obj).html("Show Advanced Options")
			}
		}
		</script>
	   
    
	<script type="text/javascript">
		
		function requiresAnswer(){
			var isRequiredAnswer = $("#requiredAns").is(":checked");
			
			if(isRequiredAnswer){
				$("#requiredErrBox").show();
			}else{
				$("#requiredErrBox").hide();
				$("#errMsgText").val("This question requires an answer.");
			}
		}
				
		// function use to save the content of question
		function saveAndCloseDialog(dialog_id, position){
			
			if(!isValidQuestion()){
				return false;
			}
			
			var isQuestionSaved = SaveQuestionContent(position, true);
			if (isQuestionSaved) {
				
				$('#EditQuestionModal').modal('hide');
				location.reload();
		 	}
		}
		
				
		function reIndexQuestions(){
									
			<!---var position = 1;--->
			var position = parseInt(startNumberQ);
			var RelativePagePosition = 1
			$.each($('.q_box'), function(index, value) {
			    $(this).attr('id', 'q_' + RelativePagePosition);
				$(this).attr('rq', position);
				position = position + 1;
				RelativePagePosition = RelativePagePosition + 1;
			});
			
			<!--- position = 1;--->
			position = parseInt(startNumberQ);
			$.each($('.q_contents'), function(index, value) {
			    $(this).attr('id', 'QLabel_' + position);
				$(this).attr('rq', position);
				$(this).attr('rel1', position);
				position = position + 1;
			});
						
			position = parseInt(startNumberQ);
			$.each($('.label_'), function(index, value) {
				$(this).html("CP" + position);
				position = position + 1;
			});
			
			position = parseInt(startNumberQ);
			$.each($('.index_'), function(index, value) {
				$(this).html(position);
				position = position + 1;
			});
			
			<!--- startNumberQ is taken into account later durning add method --->
			<!--- Take into account extra objects added by Control Point Groups--->
			var addPosition = 1;
			<!---var addPosition = parseInt(startNumberQ);--->
			$.each($('.add_button'), function(index, value) {			   	
				$(this).attr('rel4', addPosition);
				$(this).attr('rel24', addPosition);
				
				if(parseInt($(this).attr("INPAFTER")) != 1 && parseInt($(this).attr("INPBEFORE")) != 1)
					addPosition = addPosition + 1;
						
			});
			
			<!--- startNumberQ is taken into account later durning add method --->
			addPosition = 1;
			<!---addPosition = parseInt(startNumberQ);--->
			$.each($('.add_branch_button'), function(index, value) {			   	
				$(this).attr('rel1', addPosition);
				
				if(parseInt($(this).attr("INPAFTER")) != 1 && parseInt($(this).attr("INPBEFORE")) != 1)
					addPosition = addPosition + 1;
		
			});
			
			<!--- Re-index delete buttons delete_q --->
			position = parseInt(startNumberQ);
			$.each($('.delete_q'), function(index, value) {
				$(this).attr('rq', position);
				position = position + 1;
			});		
			
			<!--- Redo edit questions too --->
						
			<!--- Reload Interval Expired Next RQ list --->						
		}
				
	 	function serialize(arr) {
			var s = "[";
			for(var i=0; i<arr.length; i++) {
				if(typeof(arr[i]) == "string") s += '"' + arr[i] + '"'
				else s += arr[i]
				if(i+1 < arr.length) s += ","
			}    
			s += "]"
			return s
		}

		function updateQuestion(BatchID, QuestionID, ArrAnswerID, ComType, GroupId, position, Reload)
		{			
			if(!isValidQuestion()){
				return false;
			}			
					
			var isRequiredAns = 0;
			var errMsgTxt = "";
			// If delivery method is email
			if(communicationType == SURVEY_COMMUNICATION_ONLINE || communicationType == SURVEY_COMMUNICATION_BOTH)
			{
				var isRequiredAnswer = $("#requiredAns").is(":checked");
				if(isRequiredAnswer)
				{
					isRequiredAns = 1;
					errMsgTxt = $("#errMsgText").val();
				}
			}
			
			var CurrQuestionObj = GetQuestionObject();
			
			CurrQuestionObj.RQ = parseInt(position) + parseInt(startNumberQ) - 1;
			CurrQuestionObj.ID = QuestionID;
			
			CurrQuestionObj.inpCondXML = "";
			CurrQuestionObj.PAGE = '<cfoutput>#PAGE#</cfoutput>';
			CurrQuestionObj.BOFNQ = "";
					
			CurrQuestionObj.LISTANSWER = JSON.stringify(CurrQuestionObj.LISTANSWER);
			
			<!---console.log(CurrQuestionObj.LISTANSWER);
			console.log(JSON.stringify(CurrQuestionObj.LISTANSWER));
			console.log(CurrQuestionObj.LISTANSWER);
			console.log(JSON.stringify(CurrQuestionObj.LISTANSWER));--->
			
			CurrQuestionObj.Conditions = new Array();
			CurrQuestionObj.Conditions = JSON.stringify(CurrQuestionObj.Conditions);			
		
			UniqueReqCount++;
			var data =  { 
							INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
							questionJSON : JSON.stringify(CurrQuestionObj),						
							startNumberQ: startNumberQ,
							indexNumber : position,
							DoesHavePermissionAddQuestion : '<cfoutput>#DoesHavePermissionAddQuestion#</cfoutput>',
							DoesHavePermissionEditQuestion : '<cfoutput>#DoesHavePermissionEditQuestion#</cfoutput>',
							DOESHAVEPERMISSIONDELETEQUESTION : '<cfoutput>#DOESHAVEPERMISSIONDELETEQUESTION#</cfoutput>',
							COMMUNICATIONTYPE : '<cfoutput>#COMMUNICATIONTYPE#</cfoutput>',
							PAGE : '<cfoutput>#PAGE#</cfoutput>',
							includeLI : 0,
							inpXMLControlString : '',
							UniqueReqCount : UniqueReqCount						
						};		
						   
					
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/SMSSurvey.cfc', 'generateQuestionBox', data, "Problem generating Question", function(d ) 
			{
				<!--- Update Display --->			
							
				if (ComType == EMAIL) 
				{
					var inpXMLEmail = generateXmlBaseQuestionType(CurrQuestionObj);
				}
				else if (ComType == SMS)
				{
					var inpXMLSMS = generateXmlBaseQuestionType(CurrQuestionObj);
				}
				else if (ComType == VOICE)
				{
					//before generate XML need update array of QID for generate valid QID later.
					<!--- for (i = 0; i < ArrAnswerID.length; i++) {
						QuestionVoiceID_ARRAY = delete_array(QuestionVoiceID_ARRAY, ArrAnswerID[i]);
					}; --->
					var inpXMLVoice = generateXmlVoiceBaseQuestionType(CurrQuestionObj);  					
				}
				else 
				{//BOTH
					//before generate XML need update array of QID for generate valid QID later.
					<!--- for (i = 0; i < ArrAnswerID.length; i++) {
						QuestionVoiceID_ARRAY = delete_array(QuestionVoiceID_ARRAY, ArrAnswerID[i]);
					};
					 --->
					var inpXMLEmail = generateXmlBaseQuestionType(CurrQuestionObj);
					var inpXMLSMS = generateXmlBaseQuestionType(CurrQuestionObj);
					var inpXMLVoice = generateXmlVoiceBaseQuestionType(CurrQuestionObj);  									
				 }					 
				
				 if(!Reload)	
				 {
														 
					<!---console.log($("#TableSurveyQuestions").find("#q_"+position));
					 <!---console.log($("#TableSurveyQuestions").filter("#q_"+position));--->
					console.log($("#q_"+position));
					
					console.log('$("#q_' + position + '")');--->
					 
					 
					$("#TableSurveyQuestions").find("#q_"+position).empty();			 
					<!---alert('yo');--->
					$("#TableSurveyQuestions").find("#q_"+position).html(d.QOUT);
												
					BindAddBranchLogicInline($("#TableSurveyQuestions").find("#q_"+position).find('.AddBranchLogicInline'));
					 
					<!--- One for add and one for edit for question - same class name edit_q --->
					$("#TableSurveyQuestions").find("#q_"+position).find('.edit_q').each(function( index ) {
						<!---console.log( index + ": " + $(this) );--->
											
						$(this).unbind();
						BindEditQuestionInline($(this));					
						
					});
					
					reIndexQuestions();					
				 
				 }
					  
				 updateXMLQuestion(BatchID, QuestionID, inpXMLEmail, inpXMLSMS, inpXMLVoice, ComType, GroupId, Reload);	
				
			});		
		
		}
		
		function showRecorder(hidScriptId) {
			var QID = '';
	  		var clientId = $(this).attr('promptValueControl');
	  		var data = {
							INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
							INPSURVEYNAME: '<cfoutput>#Replace(BATCHTITLE, "'", "\'")#</cfoutput>',
							INPSECONDLEVELNAME: 'questions'
						}
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyElementAudioData', data, "Create script error!", function(d ) {
				var url = '<cfoutput>#rootUrl#/#SessionPath#/rxds/flash/recordercontrol</cfoutput>?QID=' + QID + '&clientId=' + hidScriptId + '&callbackFunction=SaveQuestionVoiceRecord&<cfoutput>eleId=#SESSION.USERID#_</cfoutput>' + d.DATA.INPLIBID[0] + "_" + d.DATA.NEXTELEID[0];
				ShowVoiceRecordDialog(url);
			});
			
		}
		
		function SaveQuestionVoiceRecord(QID, scriptId) {
	
		}
		
		function showPlayer(obj) {
			var scriptId = $(obj).attr('scriptId');
			var url = '<cfoutput>#rootUrl#/#SessionPath#/rxds/flash/player</cfoutput>?scriptId=' + scriptId	 ;
			ShowVoiceRecordDialog(url, 'Recording playback', 150);
		}
	<!---	
		function showBranchQuestion() {
			var url = 'dsp_branchQuestion?inpbatchid=<cfoutput>#inpbatchid#&LEVELTYPE=Question&page=#page#</cfoutput>';
			OpenDialog(url, "Branch Question", 600, 750);
		}
		
		function showBranchPage() {
			var url = 'dsp_branchQuestion?inpbatchid=<cfoutput>#inpbatchid#&LEVELTYPE=Page&page=#page#</cfoutput>';
			OpenDialog(url, "Branch Page", 600, 750);
		}--->
	</script>
