<cfscript>
	TITLE2 = "Contact Preference";
	DESCIRPTION2 = "Please select which services you wish to stay informed about by checking the box next to the selected service.";
	
	TITLE3 = "Contact Method";
	DESCIRPTION3 = "Please check the contact method you prefer to be contacted by for the above services.";
	
	TITLE4 = "Desired Language";
	DESCIRPTION4 = "Please select the desired communication language you want your service announcements to be delivered.";

    TITLE6 = "Section Title";
    DESCIRPTION6 = "Section Description.";
</cfscript>
