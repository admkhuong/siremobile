<cfscript>
	// Titles
	SURVEY_UPDATE_TITLE = "Update a Survey";
	SURVEY_CREATE_TITLE = "Create a Survey";
	SURVEY_CUSTOMIZE_TITLE = "Customize Template";
	SURVEY_WELCOME_TITE = "Greetings";
	
	// Labels
	//Common
	SURVEY_LABEL_YES = "Yes";
	SURVEY_LABEL_NO = "No";
	SURVEY_LABEL_FEMALE = "Female";
	SURVEY_LABEL_MALE ="Male";
	SURVEY_LABEL_NEXT = "Next";
	SURVEY_LABEL_CANCEL = "Cancel";
	SURVEY_LABEL_UPDATE = "Update";
	SURVEY_LABEL_CREATE = "Create";
	SURVEY_LABEL_BACK = "Back";
	SURVEY_LABEL_SPEAKER = "Please select voice";
	
	// Step 1
	SURVEY_LABEL_NAME = "Survey Name";
	SURVEY_LABEL_EXPIRED = "Will the survey expire?";
	SURVEY_LABEL_DELIVERY = "Survey delivery method";
	SURVEY_LABEL_DELIVERY_WARNING = "Warning: This option cannot be undone.";
	SURVEY_LABEL_GENDER = "Please select voice gender";
	SURVEY_LABEL_ONLINE = "Online";
	SURVEY_LABEL_PHONE = "Phone";
	SURVEY_LABEL_END_AT = "ends at 11:59 PM";
	// Step 2
	SURVEY_LABEL_TITLE = "Survey Title";
	SURVEY_LABEL_LOGO = "Company Logo";
	SURVEY_LABEL_LOGO_TOOLTIP = "This is company's logo.";
	SURVEY_LABEL_IMAGE = "File Type: JPG, GIF or PNG     Image Size: 250px x 65px";
	SURVEY_LABEL_FOOTER = "Footer Text";
	SURVEY_LABEL_FOOTER_TOOLTIP = "Text will show up at the bottom of the survey";
	SURVEY_LABEL_GLOBAL_DISABLE = "Globally Disable Back Button";
	// Step 3
	SURVEY_LABEL_VOICE = "Voice";
	SURVEY_LABEL_ONLINE = "Online";
	SURVEY_LABEL_SMS = "SMS";
	SURVEY_LABEL_STARTSURVEY_PROMPT = "Start Survey Prompt";
	SURVEY_LABEL_ENDSURVEY_PROMPT = "End Survey Prompt";
	SURVEY_LABEL_WELCOME_TXT = "Welcome Page HTML";
	SURVEY_LABEL_THANKS_TXT = "Thank You Page HTML";
	// Create
	SURVEY_LABEL_WARNING_1 = "Are you creating the survey for a new or existing Campaign?";
	SURVEY_LABEL_WARNING_2 = "Warning: If you choose an existing Campaign, all elements inside RXSS will be deleted.";
	SURVEY_LABEL_NEW_CAMPAIGN = "New Campaign";
	SURVEY_LABEL_EXIST_CAMPAIGN = "Existing Campaign";
	SURVEY_LABEL_CHOOSE_CAMPAIGN = "Choose Campaign";
	
	// Error messages
	SURVEY_ERR_REQUIRED = "This field's required.";
	SURVEY_ERR_DATE = "Invalid Date, please input like format mm/dd/yyyy.";
	SURVEY_ERROR_PAST_DATE = "Invalid Date, please input a future date";
	SURVEY_ERR_DELIVERY = "Please choose survey delivery method.";
	
	// key values
	SURVEY_COMMUNICATION_ONLINE = "ONLINE";
	SURVEY_COMMUNICATION_PHONE = "VOICE";
	SURVEY_COMMUNICATION_BOTH = "BOTH";
	SURVEY_COMMUNICATION_BOTH_TITLE = "ONLINE and VOICE";
	SURVEY_COMMUNICATION_SMS = "SMS";
	
	VOICEPROMPT = 'Enter text to be said through out automated text reader or record your own.';
</cfscript>

<script language="javascript" type="text/javascript">
	// Key value
	var SURVEY_COMMUNICATION_ONLINE = '<cfoutput>#SURVEY_COMMUNICATION_ONLINE#</cfoutput>';
	var SURVEY_COMMUNICATION_PHONE = '<cfoutput>#SURVEY_COMMUNICATION_PHONE#</cfoutput>';
	var SURVEY_COMMUNICATION_BOTH = '<cfoutput>#SURVEY_COMMUNICATION_BOTH#</cfoutput>';
	var SURVEY_COMMUNICATION_SMS = '<cfoutput>#SURVEY_COMMUNICATION_SMS#</cfoutput>';
</script>
