<cfparam name="CPPUUID" default="" >
<cfparam name="inpContactTypeId" default="1">


<cfif NOT cppAgentPermission.havePermission>
	<cfset session.permissionError = HAVE_NOT_PERMISSION>
	<cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Agent_Title#">
</cfinvoke>



<script language="javascript">	
	$('#mainTitleText').text('<cfoutput>#Cpp_Title#</cfoutput>');
	$('#subTitleText').text('<cfoutput>#Cpp_Agent_Title#</cfoutput>');	
</script>


<cfoutput>

<style>
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.core.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.theme.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css");
	@import url("#rootUrl#/#PublicPath#/bootstrap/dist/css/customize.css");
</style>

<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">

</cfoutput>



<style>
	
	textarea, 
	pre
	{
		margin: 0;
		padding: 0;
		outline: 0;
		border: 0;
	}
	
	table.dataTable tr.odd, table.dataTable tr.odd td.sorting_1 {
	    background-color: #F3F9FD !important;
	}
	
	#SelectContactPopUp {
	    margin: 15px;
	}
	
	#tblListGroupContact_info {
	    padding-left: 2%;
	    width: 98%;
	}
	
	.paginate_button {
	    background-color: #46A6DD !important;
	    border: 1px solid #0888D1 !important;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    padding: 3px 8px !important;
	}
	a.paginate_active {
	    background-color: #0888D1 !important;
	    border: 1px solid #0888D1 !important;
	    border-radius: 5px;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    cursor: pointer;
	    padding: 3px 8px !important;
	}
	table.dataTable th {
	    border-right: 1px solid #CCCCCC;
	}
	
	table.dataTable thead tr {
	    background-color: #0888D1;
	    border-bottom: 2px solid #FA7D29;
	    color: #FFFFFF;
	    height: 23px;
	    line-height: 23px;
	}
	.datatables_paginate {
	    background-color: #EEEEEE;
	    border-radius: 0 0 3px 3px;
	    border-top: 2px solid #0888D1;
	    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
	    float: right;
	    height: 33px !important;
	    padding-top: 10px;
	    text-align: right;
	    width: 100%;
	}
	.datatables_info {
	    color: #666666;
	    font-size: 14px;
	    left: 12px;
	    position: relative;
	}
	
	.dataTables_info {
	    background-color: #EEEEEE;
	    border-top: 2px solid #46A6DD;
	    box-shadow: 3px 3px 5px #888888;
	    line-height: 39px;
	    margin-left: 21px;
	    padding-left: 12px;
	    position: relative;
	    top: 0 !important;
	    width: 1012px;
	}
	
	.paging_full_numbers {
	    height: 22px;
	    line-height: 22px;
	    margin-top: 10px;
	    position: absolute;
	    right: 30px;
	}

	table.dataTable td {
	    border-right: 1px solid #CCCCCC;
	    color: #666666;
	    font-size: 13px;
	}
	.paginate_button {
	    background-color: #46A6DD !important;
	    border: 1px solid #0888D1 !important;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    padding: 3px 8px !important;
	}
	a.paginate_active {
	    background-color: #0888D1 !important;
	    border: 1px solid #0888D1 !important;
	    border-radius: 5px;
	    box-shadow: 0 1px 0 #2AA3E9 inset;
	    color: #FFFFFF !important;
	    cursor: pointer;
	    padding: 3px 8px !important;
	}
	
	table.dataTable tr.even td.sorting_1 {
	    background-color: #FFF!important;
	}
			
</style>

<cfoutput>

	<div id="RemoveContactStringFromGroupContainer" class="stand-alone-content" style="width:1000px;">
        <div class="EBMDialog">
                              
            <form method="POST">
                            
                <div class="header">
                    <div class="header-text">Agent Manage CPP</div>                      
                </div>
               
                <div class="inner-txt-box">
       
                    <div class="inner-txt-hd"></div>
                    <div class="inner-txt">
                             
                                
                        <fieldset>
                        <legend>Contact info</legend>
      
                        <div class="inputbox-container">
                        
                            <label for="INPDESC">Contact Type <span class="small">Required</span></label>
                            <select id="inpContactTypeId" name="inpContactTypeId">
                                <option value="1" <cfif inpContactTypeId EQ 1>selected</cfif>>Voice</option>
                                <option value="2" <cfif inpContactTypeId EQ 2>selected</cfif>>e-mail</option>
                                <option value="3" <cfif inpContactTypeId EQ 3>selected</cfif>>SMS</option>
                            </select>  
                        </div>
                       
                        <div style="clear:both"></div>
    
                        <div class="inputbox-container">
                            <label for="INPDESC">Contact String <span class="small">Required</span></label>
                            <input id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" autofocus="autofocus" />
                        </div>
                        
                        <div style="clear:both"></div>
                        
                           
                        </fieldset>
                      
                      
                        <fieldset>
                        <legend>Group info</legend>
                        <div class="inputbox-container">
					        <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
					         <div class="wrapper-picker-container">        	    
					            <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
									<span id="inpGroupPickerDesc">Click here to select a contact list</span> 
									<a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   
								</div>                                            
					        </div>
					    </div>
                        
                        <div style="clear:both"></div>
                        
                        <div class="inputbox-container" style="width:400px;">                         
                             <a href="##" class="button filterButton small" id="inpSubmitRemoveContact" >Remove Contact From This Group</a>
                        </div>
                        
                        </fieldset>    
                       
                       
                        <fieldset>
                        <legend>Or...</legend>
                        
                        <div class="inputbox-container" style="width:400px;">                         
                           <!--- <a href="##" class="button filterButton small" id="inpSubmitRemoveContactAllGroups" >Remove Contact From All Groups</a>--->
                            
                            <a class="bluebuttonAuto small tooltipTypeIIBelow" id="inpSubmitRemoveContactAllGroups">Remove Contact From All Groups
                                <div>
                                    <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png" >
                                    <span class="customTypeII infoTypeII">
                                        <img height="20px" width="20px" alt="Information" src="#rootUrl#/#publicPath#/images/infoii.png">
                                        <em>Remove Contact From All Groups</em>
                                        This will search and remove the specifed contact string from all of the groups that this user account controls.
                                    </span>
                                </div>
                            </a>
                
                        </div>
                        
                        </fieldset>
                                                        
                    </div>
                    
                    <div style="clear:both"></div>
                    <div style="clear:both"></div>
                   
             
            	 </div>
                            
            </form>                                          
                
        </div>
		<input type="hidden" id="isTestKeyword" value="0"> 
		<input type="hidden" id="isEmitChatContent" value="0">
		<input type="hidden" id="Keyword" value="">     
	</div>    
</cfoutput>

<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	$(function() 
	{	
		$("#inpContactTypeId").selectbox();
	
			
		$("#CancelFormButton").click(function() { window.history.back(); });
		
		
		$('#inpSubmitRemoveContact').click(function(){
			DeleteContactsFromGroup();
		});		
		
		$('#inpSubmitRemoveContactAllGroups').click(function(){
			DeleteContactsFromAllGroups();
		});		
		
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 900,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true,
					close: function( event, ui ) {
				      	//now before close dialog, we should remove all styles which have been loaded into this page from external css file 
				      	removejscssfile("customize.css","css");
						$("#SelectContactPopUp").html('');
						if($dialogGroupPicker != null)
						{							
							$dialogGroupPicker.remove();
							$dialogGroupPicker = null;
						}
				     }
				}).load('../../../ems/dsp_select_contact');
			}
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $('#inpSubmitRemoveContact').position().left; 
    		var y = $('#inpSubmitRemoveContact').position().top - 10 - $(document).scrollTop();

    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		});
				
	});

	<!--- Delete contact string from group --->
	function DeleteContactsFromGroup()
	{		
		var INPCONTACTSTRING =  $("#inpContactString").val();
		var inpContactType = $("#inpContactTypeId").val();
		var INPGROUPID = $("#inpGroupPickerId").val();
		
		<!--- Will remove all contact from group - including duplicate contact Ids if allowed in group --->
		var INPCONTACTID = 0;  
	
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to delete this contact?\n(" + displayList + ") ";
		msg2 = "Delete contacts from your group.";		
				
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();	
					
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING), INPCONTACTTYPE: inpContactType, INPGROUPID: INPGROUPID, INPCONTACTID: INPCONTACTID},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0)
						{							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1)
							{
								
								<!--- alert success --->
								jAlert('Remove contact successful','Removed Contact from Group', function(result) {
						
									
								
								} );
					
							}
							else
							{						
								jAlert(d2.DATA.MESSAGE, 'Warning');
							}  						
						}
						
					}
				});
						
		 	}  
		});
		return false;
	}
	
	<!--- Delete contact string from all groups --->
	function DeleteContactsFromAllGroups()
	{		
		var INPCONTACTSTRING =  $("#inpContactString").val();
		var inpContactType = $("#inpContactTypeId").val();
		<!--- Will remove all contact from group - including duplicate contact Ids if allowed in group --->
		var INPCONTACTID = 0;  
	
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to delete this contact from all groups?\n(" + displayList + ") ";
		msg2 = "Delete contacts from all of your groups.";		
				
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();	
					
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromAllGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING), INPCONTACTTYPE: inpContactType, INPCONTACTID: INPCONTACTID},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0)
						{							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1)
							{
								
								<!--- alert success --->
								jAlert('Removed Contact from all user &apos;s Groups', 'Remove contact successful', function(result) {
															
								
								} );
							}	
							else
							{						
								jAlert(d2.DATA.MESSAGE, 'Warning');
							}  							
						}
					}
				});
						
		 	}  
		});
		return false;
	}
	
	function SelectGroup(groupId,groupName){
		$("#inpGroupPickerId").val(groupId);
		$("#inpGroupPickerDesc").html(groupName);
		$dialogGroupPicker.dialog('close');
		$("#GroupPickerList").html("");
		$("#GroupPickerList").remove();
	}
	
	//we use this function to remove js or css file which loaded dynamically from ajax, popup, etc
   	function removejscssfile(filename, filetype){
   		var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
   		var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
   		var allsuspects=document.getElementsByTagName(targetelement)
   		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	     		allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
   		}
  	}
	
	
	function Left(str, n){
		if (n <= 0)
		    return "";
		else if (n > String(str).length)
		    return str;
		else
		    return String(str).substring(0,n);
	}
	
	function Right(str, n){
	    if (n <= 0)
	       return "";
	    else if (n > String(str).length)
	       return str;
	    else {
	       var iLen = String(str).length;
	       return String(str).substring(iLen, iLen - n);
	    }
	}

</script>
