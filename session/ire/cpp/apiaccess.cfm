﻿<cfparam name="CPPUUID" default="0">
<style type="text/css">
	.ipFilterLabel{
		font-weight:bold;
		margin:10px
	}
	.ipfilterInput {
		width : 25px;
		margin-left:20px;
		margin-right:5px
	}
	.ipFilterFieldSet{
		width: 310px;
		height: 280px;
		padding:15px;
		margin-right:15px;
		border: 0px none;
	}
	.ipFilterFieldSet legend{
		font-weight:bold;
		margin-left:15px;
	}
	.ipFilterButton{
		border: 1px solid #0888D1;
	    color: #F7F7F7 !important;
		background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
		cursor:pointer;
		font-size:14px !important;
		height: 29px;
	}
	.ipFilterButton:hover{
		background-color:red;
	}
	.btn_back {
		display:none;
<!---	    display: inline-block;--->
	   	background-color: #006DCC;
	    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
	    background-repeat: repeat-x;
	    border: 1px solid #CCCCCC;
	    border-radius: 4px;
	    color: #FFFFFF !important;
	    cursor: pointer;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 0;
	    padding: 4px 12px;
	    text-align: center;
	    vertical-align: middle;
	}
	.btn_back:hover {
   		background-color: #0888D1;
	}
	.div-data{
	    border: 1px solid #CCCCCC;
	    border-radius: 4px;
	    float: left;
	    height: 27px;
	    width: 229px;
	}
	.left_box{
		background-color: #FFFFFF;
		border-radius: 4px 0 0 4px;
		border-right: 1px solid #CCCCCC;
		color: #666666;
		float: left;
		font-size: 12px;
		height: 27px;
		line-height: 27px;
		padding-left: 10px;
		position: relative;
		text-align: left;
		width: 85px;
	}
	.right_box{
		float: left;
		height: 27px;
		width: 133px;
	}
	.input_box{
	    border: 0 none;
		border-radius: 0 3px 3px 0 !important;
		color: #333333;
		float: left;
		font-size: 12px !important;
		height: 26px;
		margin-bottom: 0;
		padding: 0 11px 0 6px;
		width: 115px !important;
	}
	.info_box{
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/ire/images/info.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
	    cursor: pointer;
	    float: right;
	    height: 16px;
	    margin-right: 5px;
	    margin-top: 6px;
	    opacity: 0.4;
	    width: 16px;
	}
	.info_box:hover{
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/ire/images/info.png"); 
		background-repeat:no-repeat;
		opacity:1;
		filter:alpha(opacity=1);
	}
	.m_left_20{
		margin-left:20px;
	}
	.m_bottom_10{
		margin-bottom:10px;
	}
	.m_top{
		margin-top:10px;
	}
	.m_top_20{
		margin-top:20px;
	}
	.lbl{
		color: #0888D1;
	    
	    font-size: 14px;
	    font-weight: bold;
	}
	.ul-Ips{
		border:1px solid #CCC;
		border-radius: 3px 3px 0 0;
	}
	.div-Ips{
		width:350px;
		height:500px;
		overflow-y:auto;
	}
	.div-header{
		width:350px;
		background-color: #0888D1;
	    border: 0px solid #0C6599;
	    border-radius: 3px 3px 0 0;
		border-bottom:1px solid #F18822;
	    color: #FFFFFF;
	    float: left;
	    
	    font-size: 13px;
	    height: 30px;
	    line-height: 30px;
		text-indent:1em;
	}
	.item{
		color:#666666;
		font-size: 12px;
		padding: 2px 0 2px 0;
		text-indent:1em;
	}
	.item:hover{
		background-color: #4AA5D9 !important;
	}
	.odd{
		background-color: #F3F9FD !important;
	}
	.even{
		background-color: #fff !important;
	}
	.none-style-ul{
		list-style: none;
	}
	.content-hide{
		display:none;
	}
	.content-show{
		background-repeat:no-repeat;
		float:right;
		display:block;
		margin-right:10px;
		cursor:pointer;
	}
	.ui-tabs-active{
		background-color:#FFFFFF !important;
		color:#fff !important;
	}
	
	.ui-tabs-active a{
		color:#0888d1 !important;
	}
	.ui-widget-content{
		border: 1px solid #AAAAAA;
		width: 700px;
	}
	.ui-tabs .ui-tabs-nav{
		background: #DDDDDD;	
	}
	.ui-tabs .ui-tabs-panel{
		border:0px;
		min-height:236px;
		height:auto;
		padding-top:40px;
		padding-left:20px;
		padding-right:20px;
	}
	.ui-tabs-nav{
		height: 38px;
		border-left-width: 0;
	    border-right-width: 0;
	}
	.ui-tabs .ui-tabs-nav li{
	 	bottom: 0;
	    height: 30px;
	    margin-bottom: 0;
	    margin-left: 5px;
	    margin-top: 9px;
	    top: 0;
	    width: 170px;	
		background: #f0f0f0;
	}
	.ui-tabs .ui-tabs-nav li.ui-state-default{
		border-bottom:0px;
	}
	.ui-tabs .ui-tabs-nav li a{
		padding: 0;
		line-height:30px;
	    width: 168px;
		text-align:center;
		font-size:14px;
		font-weight:bold;
		color:#666666;
		
	}
	
	.ui-tabs-nav a,ui-tabs-nav a:link
	{
		height: 28px;
		color:#666666;	
		border-bottom:1px solid #AAA;
		border-left:1px solid #AAA;
		border-top:1px solid #AAA;
		border-right:1px solid #AAA;
		border-radius:5px 5px 0 0;	
	}
	.ui-tabs-nav .ui-tabs-active a{
		border-bottom:0px !important;
	}
	.testresult {
		color:red;
		
		font-size:14px;
		padding-top:5px;
	}
	.header-bar{
		background-color: #0888D1;
	    border: 1px solid #0E88CD;
	    border-radius: 5px 5px 0 0;
	    color: #FFFFFF;
	    
	    font-size: 14px;
	    font-weight: bold;
	    height: 30px;
	    line-height: 30px;
	    width: 700px;
		text-indent:10px;
	}
	
	#tabs{
		border-top-left-radius:0px !important;
		border-top-right-radius:0px !important;
	}
</style>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Api_Access_Title#">
</cfinvoke>

<cfsaveContent variable="htmlContent">
	<cfoutput>
		<div class="header-bar">Access</div>
		<div id="tabs">
			<ul>
				<li><a href="##tabs-1">IP FILTER</a></li>
				<li><a href="##tabs-2">SYSTEM PASSWORD</a></li>
			</ul>
			<div id="tabs-1">
				<p class="lbl m_bottom_10">IP Address Range</p>
				<div class="div-data">
					<div class="left_box">Start
						<div class="info_box"></div>
					</div>
					<div class="right_box">
						<input type="text" class="input_box" name="txtStart" id="txtStart" value = "" maxlength="15">
					</div>
		        </div>
		        <div class="div-data m_left_20">
		        	<div class="left_box">End
						<div class="info_box"></div>
					</div>
					<div class="right_box">
						<input type="text" class="input_box" name="txtEnd" id="txtEnd" value = "" maxlength="15">
					</div>
		        </div>
				<input type="hidden" id="updatingIP" value=""/>
               <button id="AddIPRangeButton" TYPE="button" class="ipFilterButton ui-corner-all m_left_20">Add to list</button>
               <button id="UpdateIPRangeButton" TYPE="button" class="ipFilterButton ui-corner-all">Update</button>
               <button id="Reset" TYPE="button" style='display:none' class="ipFilterButton ui-corner-all">Reset</button>
				<p class="lbl m_bottom_10 m_top">IP Address Range List</p>
				<div class="div-Ips">
					<div class="div-header">IP Address</div>
					<ul class="ul-Ips none-style-ul" id="IPRangeListDiv">
					</ul>
				</div>
				<button id="removeIPRangeButton" style="display:none;" TYPE="button" class="ipFilterButton ui-corner-all">Remove</button>
				<div style="clear:both"></div>
			</div>
			<div id="tabs-2">
				<div class="div-data">
					<div class="left_box">Password
						<div class="info_box"></div>
					</div>
					<div class="right_box">
						<input type="password" class="input_box" name="inpPassword" id="inpPassword" value = "">
					</div>
		        </div>
				<div style="clear:both"></div>
		        <div class="div-data m_top_20">
		        	<div class="left_box">Confirm
						<div class="info_box"></div>
					</div>
					<div class="right_box">
						<input type="password" class="input_box" name="inpConfirm" id="inpConfirm" value = "">
					</div>
		        </div>
				<div style="clear:both;">
					<input type="button" class="ipFilterButton ui-corner-all m_top_20" name="inpSave" id="inpSave" value="Save">
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/mcid/jquery.numbericbox.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="jsContent">
	<cfoutput>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/mcid/jquery.numbericbox.js"></script>
		<script TYPE="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/password_strength_plugin.js"></script>
	</cfoutput>
	<script>
		var editingRange='';
		$(function() {
			
			$("#tabs").tabs();
			$('#subTitleText').text('<cfoutput>#Cpp_Title# >> #Cpp_Api_Access_Title# #CPPUUID# </cfoutput>');
			$('#mainTitleText').text('ire');
			
			$("#AddIPRangeButton").click( function() { AddIPRange(); return false;  }); 	
			$("#UpdateIPRangeButton").click( function() { 
					var inpIPRange = $("#updatingIP").val();
					if(typeof(inpIPRange) == 'undefined' || inpIPRange == null) {
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("No IP Range was chosen", "Failure!", function(result) { } );
						return;	
					}
					$.alerts.okButton = '&nbsp;Confirm Update!&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure you want to update this IP Range?", "About to delete IP Range.", function(result) { 
						if(result)
						{	
							UpdateIPRange(inpIPRange);
						}
						return false;
					});	
				
			});
			<!--- Kill the new dialog --->
			$("#Reset").click( function() 
				{
					//IPFilterDialog.remove(); 
					$('#AddIPRangeForm').each (function(){
 						this.reset();
					});
					editingRange='';
					return false;
			  }); 	
			 $("#removeIPRangeButton").click( function() {
			 			 
					var inpIPRange = $("#inpIPRangeList").val();
					if(typeof(inpIPRange) == 'undefined' || inpIPRange == null) {
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("No IP Range was chosen", "Failure!", function(result) { } );
						return;	
					}
					
					$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure you want to delete this IP Range?", "About to delete IP Range.", function(result) { 
						if(result)
						{
							RemoveIPRange(inpIPRange);
						}
						return false;
					});		 		    
			});
		});
		
		function ValidateIpRange(){
			//validation
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			var startIpArr = $("#txtStart").val().split(".");
			var endIpArr = $("#txtEnd").val().split(".");
			
			//ip should have 4 octets
			if(startIpArr.length!=4){
				jAlert("IP range Invalid", "Failure!", function(result) { } );
				return false;
			}
			
			//ip should have 4 octets
			if(endIpArr.length!=4){
				jAlert("IP range Invalid", "Failure!", function(result) { } );
				return false;
			}
			
			//each octet should be numeric and first, second octet of each ip range should be equal 
			if(parseInt(startIpArr[0],10) != parseInt(endIpArr[0],10) || parseInt(startIpArr[1],10) != parseInt(endIpArr[1],10)) {
				jAlert("IP range Invalid", "Failure!", function(result) { } );
				return false;
			}
			
			if(parseInt(startIpArr[2],10) == parseInt(endIpArr[2],10) ){
				if((parseInt(startIpArr[3],10) - parseInt(endIpArr[3],10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return false;
				}
			}else{
				if((parseInt(startIpArr[2],10) - parseInt(endIpArr[2],10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return false;
				}
			}
		 	return true;
		}
		
		function AddIPRange()
		{
			if(!ValidateIpRange()) return false;
			
			var startIP=$("#txtStart").val();
			var endIP=$("#txtEnd").val();
			
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateIPFilter&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			type : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
				inpStartIp : startIP,
				inpEndIp : endIP
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									//jAlert("User Account has been updated.", "Success!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide();UpdateUserAccountDialog.remove(); } );
									$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });
									
									var style = $("#IPRangeListDiv li").length  %2 ==0?"even":"odd";
									var value=d.DATA.INPIPRANGE[0].replace("-"," to ");
									$("#IPRangeListDiv").append(BuildListItem(style,value,d.DATA.INPIPRANGE[0]));
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Add Failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});
			return false;
		}
		
		function RemoveIPRange(object)
		{
			var keepDelete = false;
			jConfirm( "Are you sure you want to delete this IP Range?", "About to delete IP Range.", function(result) {
				if(!result) return false;
				//get ip range from hidden field
				var inpIPRange = $(object).parent().find("input[type=hidden]").val();
				//validation
				$.ajax({
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=removeIPRange&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				type : 'post',
				data:  { 
					inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
					inpIPRange : inpIPRange
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {},
				success:
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{						
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									if(CurrRXResultCode > 0)
									{
										$.jGrowl("Remove sucessfully", { life:2000, position:"center", header:' Message' });
										//remove li item
										$(object).parent().remove();					
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Remove failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
									}
								}
								else
								{<!--- Invalid structure returned --->	
									
								}
							}
							else
							{<!--- No result returned --->						
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
					} 		
				});
			});
			return false;
		}	
		
		function getIpRangeList()
		{
			var temp="";
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=getIPFilterRanges&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			method : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>'
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									IPRangeList = d.DATA.IPFILTER[0].split(",");
									var ulHtml = ""
									for(i=0; i<IPRangeList.length-1; i++){
										var style = i%2==0?"even":"odd";
										var value=IPRangeList[i].replace("-"," to ");
										ulHtml += BuildListItem(style,value,IPRangeList[i]);
									};
									$("#IPRangeListDiv").append(ulHtml);
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert(d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				}
			});
			return false;
		}
		
		//this function is used to build list item data
		//style is class of li item, should be odd or even
		//value is text that display
		//hiddenVal is value used to manipulate database
		function BuildListItem(style,value,hiddenVal){
			
			var liItem = '<li class="item '+style+'">'+value+
							'<a class="content-show" onclick="RemoveIPRange(this);return false;">'+										
								'<img title="remove ip range" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/images/delete-transparent.png"/>'+
							'</a>'+
							'<a class="content-show" onclick="Edit(this);return false;">'+
								'<img title="edit ip range" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/images/icons16x16/edit_16x16.png"/>'+
							'</a>'+
							'<input type="hidden" value="'+hiddenVal+'"/>'+
						'</li>';
			return liItem;
		}
		
		function Edit(object){
			var inpIPRange = $(object).parent().find("input[type='hidden']").val();
			$("#txtStart").val(inpIPRange.split("-")[0]);
			$("#txtEnd").val(inpIPRange.split("-")[1]);
			$("#updatingIP").val(inpIPRange);
		}
		
		function UpdateIPRange(object)
		{
			//validation
			if(!ValidateIpRange()) return false;
			
			var startIP=$("#txtStart").val();
			var endIP=$("#txtEnd").val();
			
			var inpIPRange = $("#updatingIP").val();
			
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateIPFilter&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			type : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
				inpStartIp : startIP,
				inpEndIp : endIP,
				inpIPRange:inpIPRange
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									//jAlert("User Account has been updated.", "Success!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide();UpdateUserAccountDialog.remove(); } );
									$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });
									
									var style = $("#IPRangeListDiv li").length  %2 ==0?"even":"odd";
									var value=d.DATA.INPIPRANGE[0].replace("-"," to ");
									$("li input[type='hidden'][value='"+inpIPRange+"']").parent().replaceWith(BuildListItem(style,value,d.DATA.INPIPRANGE[0]));
									$("#updatingIP").val("");
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Update Failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});
			return false;
		}		
		
		$(document).ready(function() {
	
			getIpRangeList();
			$(".ipfilterInput").ForceNumericOnly();
			
			$("#inpPassword").passStrength();
			$("#inpSave").click(function(){
				var inpPassword = $('#inpPassword').val();
				var inpConfirm = $('#inpConfirm').val();
				var inpCppId = '<cfoutput>#CPPUUID#</cfoutput>';
				
				<!--- validate password --->
				if(inpPassword != inpConfirm){
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Password and confirm password not match", "Failure!", function(result) { } );
					return;
				}
				$.ajax({
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateSystemPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					type : 'post',
					data:  { 
						inpCppId : inpCppId,
						inpPassword : inpPassword,
						inpConfirm : inpConfirm
					},
					success:
						function(d) 
						{
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{						
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										var CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
										if(CurrRXResultCode > 0)
										{
											$.jGrowl(
												"Set password sucessfully", 
												{ 
													life:2000, 
													position:"center", 
													header:' Message', 
													close:function(){
														// redirect to cpp list
														document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/cpp';
													} 
												}
											
												);
											
										}
										else
										{
											var errMessages=d.DATA.MESSAGES[0];
											msgStr="";
											for(var i=0;i<errMessages.length;i++){
												msgStr+=(errMessages[i]+"\n");
											}
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert(msgStr, "Failure!", function(result) { } );												
										}
									}
									else
									{<!--- Invalid structure returned --->	
										
									}
								}
								else
								{<!--- No result returned --->						
									jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
								}
						} 		
					});
			});
		});
		
	
		function OnKeyPress(field, event)
		{
			var unicode=event.keyCode? event.keyCode : event.charCode
			if (unicode == 110 || unicode == 39 || unicode == 32) {
					field.value = field.value.replace('.','');
					if(field.value.length < 1){
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("You must input a value first", "Failure!", function(result) { field.select();} );
						return;
					}
				for (i = 0; i < field.form.elements.length; i++)
					if (field.form.elements[i].tabIndex == field.tabIndex+1) {
						field.form.elements[i].focus();
						if (field.form.elements[i].type == "text"){
							field.form.elements[i].select();
							break;
						}
				}
				return false;
			}
			return true;
		}
		function OnSelectRange(object,event)
		{
			iprange=object.value;
			startIp=iprange.split('-')[0];
			endIp=iprange.split('-')[1];
			for(i=0;i<startIp.split('.').length;i++){
				$("#inpStartIP"+(i+1)).val(startIp.split('.')[i]);
			}
			for(i=0;i<endIp.split('.').length;i++){
				$("#inpEndIP"+(i+1)).val(endIp.split('.')[i]);
			}
			editingRange=iprange;
		}
			
		function openIpfilter(CPPUUID){
			document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/apiAccess?CPPUUID=' + CPPUUID;
		}
		function openSystemPassword(CPPUUID){
			document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/cppSystemPassword?CPPUUID='+CPPUUID;
		}
		
		function BindHoverEvent(){
			$(document).on('mouseenter','.item', function (event) {
				$(this).find("a").removeClass("content-hide").addClass("content-show");
			});
			$(document).on('mouseleave','.item',  function(){
				$(this).find("a").removeClass("content-show").addClass("content-hide");
			});
		}
	</script>	
	<cfoutput>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<cfoutput>
		<style>
			@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		</style>
	</cfoutput>
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>
<a class="btn_back" href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/">Back</a>