<cfparam name="CPPUUID" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Api_Access_Title#">
</cfinvoke>

<input type="button" value="IP Filter" onclick="openIpfilter('<cfoutput>#CPPUUID#</cfoutput>'); return false;">
<input type="button" value="System password" onclick="openSystemPassword('<cfoutput>#CPPUUID#</cfoutput>'); return false;">
<cfsaveContent variable="htmlContent">
	<cfoutput>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/mcid/jquery.numbericbox.js"></script>
		<table id="tblIpfilter">
			<tr>
				<td>
					<form id="AddIPRangeForm" name="AddIPRangeForm" action="" method="POST">
					<fieldset class="ipFilterFieldSet">
							<legend>IP Address Range</legend>
							<p class="ipFilterLabel">Start</p>
							<div>
					        <input name="inpStartIP1" id="inpStartIP1" maxlength="3"  tabindex="1" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpStartIP2" id="inpStartIP2" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="2" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpStartIP3" id="inpStartIP3" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="3" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpStartIP4" id="inpStartIP4" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="4" class="ipfilterInput"/>
					        </div>
					        <p class="ipFilterLabel">End</p>
					        <div>
					        <input name="inpEndIP1" id="inpEndIP1" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="5" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpEndIP2" id="inpEndIP2" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="6" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpEndIP3" id="inpEndIP3" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="7" class="ipfilterInput"/>
					        <strong>.</strong>
					        <input name="inpEndIP4" id="inpEndIP4" maxlength="3" onkeyup="OnKeyPress(this,event);" tabindex="8" class="ipfilterInput"/>
					        </div>
			               <button id="AddIPRangeButton" TYPE="button" class="ipFilterButton ui-corner-all">Add</button>
			               <button id="UpdateIPRangeButton" TYPE="button" class="ipFilterButton ui-corner-all">Update</button>
			               <button id="Reset" TYPE="button" class="ipFilterButton ui-corner-all">Reset</button>
					</fieldset>
					</form>
				</td>
				<td>
					<form id="IPRangeListForm" name="IPRangeListForm" action="" method="POST">
					<fieldset  class="ipFilterFieldSet">
						<legend>IP Address Range List</legend>
						<div id="IPRangeListDiv">
						</div>
						<button id="removeIPRangeButton" TYPE="button" class="ipFilterButton ui-corner-all">Remove</button>
					</fieldset>
					</form>
				</td>
			</tr>
		</table>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="jsContent">
	<cfoutput>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/mcid/jquery.numbericbox.js"></script>
	</cfoutput>
	<script>
		var editingRange='';
		$(function() {
			
			$('#subTitleText').text('<cfoutput>#Cpp_Title# >> #Cpp_Api_Access_Title# #CPPUUID# </cfoutput>');
			$('#mainTitleText').text('ire');
			
			$("#AddIPRangeButton").click( function() { AddIPRange(); return false;  }); 	
			$("#UpdateIPRangeButton").click( function() { 
					var inpIPRange = $("#inpIPRangeList").val();
					if(typeof(inpIPRange) == 'undefined' || inpIPRange == null) {
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("No IP Range was chosen", "Failure!", function(result) { } );
						return;	
					}			
					$.alerts.okButton = '&nbsp;Confirm Update!&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure you want to update this IP Range?", "About to delete IP Range.", function(result) { 
						if(result)
						{	
							UpdateIPRange(inpIPRange);
						}
						return false;
					});	
				
			}); 	
			<!--- Kill the new dialog --->
			$("#Reset").click( function() 
				{
					//IPFilterDialog.remove(); 
					$('#AddIPRangeForm').each (function(){
 						this.reset();
					});
					editingRange='';
					return false;
			  }); 	
			 $("#removeIPRangeButton").click( function() {
			 			 
					var inpIPRange = $("#inpIPRangeList").val();
					if(typeof(inpIPRange) == 'undefined' || inpIPRange == null) {
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("No IP Range was chosen", "Failure!", function(result) { } );
						return;	
					}
					
					$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure you want to delete this IP Range?", "About to delete IP Range.", function(result) { 
						if(result)
						{	
							RemoveIPRange(inpIPRange);
						}
						return false;
					});		 		    
			}); 	
		});
		
		
		function AddIPRange()
		{
			//validation
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			if(parseInt($("#inpStartIP1").val(),10) != parseInt($("#inpEndIP1").val(),10) || parseInt($("#inpStartIP2").val(),10) != parseInt($("#inpEndIP2").val(),10)) {
				jAlert("IP range Invalid", "Failure!", function(result) { } );
				return
			}
			if(parseInt($("#inpStartIP3").val(),10) == parseInt($("#inpEndIP3").val(),10) ){
				if((parseInt($("#inpStartIP4").val(),10) - parseInt($("#inpEndIP4").val(),10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return;
				}
			}else{
				if((parseInt($("#inpStartIP3").val(),10) - parseInt($("#inpEndIP3").val(),10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return;
				}
			}
			
			var startIP=parseInt($("#inpStartIP1").val(),10)+"."+parseInt($("#inpStartIP2").val(),10)+"."+parseInt($("#inpStartIP3").val(),10)+"."+parseInt($("#inpStartIP4").val(),10);
			var endIP=parseInt($("#inpEndIP1").val(),10)+"."+parseInt($("#inpEndIP2").val(),10)+"."+parseInt($("#inpEndIP3").val(),10)+"."+parseInt($("#inpEndIP4").val(),10);
			
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateIPFilter&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			type : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
				inpStartIp : startIP,
				inpEndIp : endIP
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									//jAlert("User Account has been updated.", "Success!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide();UpdateUserAccountDialog.remove(); } );
									$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });
									temp = "";
									temp += '<option value="'+ d.DATA.INPIPRANGE[0] +'">'+ d.DATA.INPIPRANGE[0] + '</option>';
									//$("#inpIPRangeList").options[$("#inpIPRangeList").length] = new Option(d.DATA.INPIPRANGE[0], d.DATA.INPIPRANGE[0]);
									$("#inpIPRangeList").append(temp);
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Add Failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});
			return false;
		}
		
		function RemoveIPRange(inpIPRange)
		{
			//validation
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=removeIPRange&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			type : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
				inpIPRange : inpIPRange
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									$.jGrowl("Remove sucessfully", { life:2000, position:"center", header:' Message' });
									$("#inpIPRangeList option[value='"+ d.DATA.INPIPRANGE[0] +"']").remove();
									
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Remove failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
									
				} 		
				
			});
		
			return false;
	
		}	
		
		function getIpRangeList()
		{
	
			var temp="";
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=getIPFilterRanges&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			method : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>'
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									IPRangeList = d.DATA.IPFILTER[0].split(",");
									temp += '<select TYPE="select" name="inpIPRangeList" id="inpIPRangeList" size="15" style="margin-left:20px;width:250px;height:230px" onclick="OnSelectRange(this,event)"> ';
									for(i=0; i<IPRangeList.length-1; i++){
										var value=IPRangeList[i];
										var text=IPRangeList[i];
										temp += '<option value="'+ value +'">'+ text + '</option>';
									};
									temp += '</select>';	
									$("#IPRangeListDiv").append(temp);
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert(d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				}
			});
			return false;
		}
		
		function UpdateIPRange(inpIPRange)
		{
			//validation
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			if(parseInt($("#inpStartIP1").val(),10) != parseInt($("#inpEndIP1").val(),10) || parseInt($("#inpStartIP2").val(),10) != parseInt($("#inpEndIP2").val(),10)) {
				jAlert("IP range Invalid", "Failure!", function(result) { } );
				return
			}
			if(parseInt($("#inpStartIP3").val(),10) == parseInt($("#inpEndIP3").val(),10) ){
				if((parseInt($("#inpStartIP4").val(),10) - parseInt($("#inpEndIP4").val(),10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return;
				}
			}else{
				if((parseInt($("#inpStartIP3").val(),10) - parseInt($("#inpEndIP3").val(),10))>0) {
					jAlert("IP range Invalid", "Failure!", function(result) { } );
					return;
				}
			}
			
			var startIP=parseInt($("#inpStartIP1").val(),10)+"."+parseInt($("#inpStartIP2").val(),10)+"."+parseInt($("#inpStartIP3").val(),10)+"."+parseInt($("#inpStartIP4").val(),10);
			var endIP=parseInt($("#inpEndIP1").val(),10)+"."+parseInt($("#inpEndIP2").val(),10)+"."+parseInt($("#inpEndIP3").val(),10)+"."+parseInt($("#inpEndIP4").val(),10);
			
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateIPFilter&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			type : 'post',
			data:  { 
				inpCppId : '<cfoutput>#CPPUUID#</cfoutput>',
				inpStartIp : startIP,
				inpEndIp : endIP,
				inpIPRange:inpIPRange
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									//jAlert("User Account has been updated.", "Success!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide();UpdateUserAccountDialog.remove(); } );
									$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });
									temp = "";
									temp += '<option value="'+ d.DATA.INPIPRANGE[0] +'">'+ d.DATA.INPIPRANGE[0] + '</option>';
									//$("#inpIPRangeList").options[$("#inpIPRangeList").length] = new Option(d.DATA.INPIPRANGE[0], d.DATA.INPIPRANGE[0]);
									$("#inpIPRangeList").append(temp);
									$("#inpIPRangeList option[value='"+ inpIPRange +"']").remove();
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Update Failed\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});
			return false;
		}		
		
		$(document).ready(function() {
	
			getIpRangeList();
			$(".ipfilterInput").ForceNumericOnly();
			document.getElementById("inpStartIP1").focus();
		});
		
	
		function OnKeyPress(field, event)
		{
			var unicode=event.keyCode? event.keyCode : event.charCode
			if (unicode == 110 || unicode == 39 || unicode == 32) {
					field.value = field.value.replace('.','');
					if(field.value.length < 1){
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("You must input a value first", "Failure!", function(result) { field.select();} );
						return;
					}
				for (i = 0; i < field.form.elements.length; i++)
					if (field.form.elements[i].tabIndex == field.tabIndex+1) {
						field.form.elements[i].focus();
						if (field.form.elements[i].type == "text"){
							field.form.elements[i].select();
							break;
						}
				}
				return false;
			}
			return true;
		}
		function OnSelectRange(object,event)
		{
			iprange=object.value;
			startIp=iprange.split('-')[0];
			endIp=iprange.split('-')[1];
			for(i=0;i<startIp.split('.').length;i++){
				$("#inpStartIP"+(i+1)).val(startIp.split('.')[i]);
			}
			for(i=0;i<endIp.split('.').length;i++){
				$("#inpEndIP"+(i+1)).val(endIp.split('.')[i]);
			}
			editingRange=iprange;
		}
			
		function openIpfilter(CPPUUID){
			document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/apiAccess?CPPUUID=' + CPPUUID;
		}
		function openSystemPassword(CPPUUID){
			document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/cppSystemPassword?CPPUUID='+CPPUUID;
		}
	</script>	
	<cfoutput>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<style type="text/css">
		.ipFilterLabel{
			font-weight:bold;
			margin:10px
		}
		.ipfilterInput {
			width : 25px;
			margin-left:20px;
			margin-right:5px
		}
		.ipFilterFieldSet{
			width: 310px;
			height: 280px;
			border-radius:10px;
			padding:15px;
			margin-right:15px;
		}
		.ipFilterFieldSet legend{
			font-weight:bold;
			margin-left:15px;
		}
		.ipFilterButton{
			margin:20px 0 30px 0;
		}
		.btn_back {
		   	background-color: #006DCC;
		    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
		    background-repeat: repeat-x;
		    border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    display: inline-block;
		    font-size: 14px;
		    line-height: 20px;
		    margin-bottom: 0;
		    padding: 4px 12px;
		    text-align: center;
		    vertical-align: middle;
		}
		.btn_back:hover {
	   		background-color: #0888D1;
		}
	</style>
	<cfoutput>
		<style>
			@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		</style>
	</cfoutput>
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>
<a class="btn_back" href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/">Back</a>