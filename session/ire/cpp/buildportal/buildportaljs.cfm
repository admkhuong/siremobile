<cfoutput>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/custominput.jquery.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.core.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.position.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.widget.js"></script>

        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/jquery-ui1.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/style5.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/list.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.core.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.theme.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
        <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css" />

        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/js/jmutiselect/style.css" />

        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/dropdownchecklist/ui.dropdownchecklist-1.4-min.js"></script>
</cfoutput>
<cfinclude template="../../constants/cppConstants.cfm">
<style>
    #inpMultiSelection-menu, #inpType-menu{
        width:inherit !important;
    }
    .ui-selectmenu-menu-popup
    {
        width:449px !important;
    }
    .prefeItem{
        width:450px;
    }
    .mce-tinymce {
        position: static;
    }
	
	<!--- Batch Picker Styles --->
	#inpBatchDescA
	{
		float: left;
		width: 300px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		display: inline-block;
	}
	.wrapper-picker-container:after 
	{
		clear: both;
		content: "";
		display: table;
	}
	
	.wrapper-picker-container {
		float: left;
		height: 25px;
		margin-left: 0px;   
		text-align: left;
		width: 200px;
	}
	
	.wrapper-dropdown-picker:after {
		border-color: #2484C6 transparent;
		border-style: solid;
		border-width: 6px 6px 0;
		content: "";
		height: 0;
		margin-top: -3px;
		position: absolute;
		right: 15px;
		top: 50%;
		width: 0;
	}
	
	.wrapper-dropdown-picker {
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid rgba(0, 0, 0, 0.15);
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.1);
		color: #444444;
		cursor: pointer;
		font-size: 12px;
		height: 19px;
		padding: 6px 10px;
		position: relative;
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
		margin-top: 0px;
		width: 322px;	
	}
	
	.inputbox {
		float: left;
		height: auto;
		margin-left: 0px;
		text-align: left;
		width: 368px;
	}


</style>

<script type="text/javascript">
var countEditor=0;
var currentStepSetup = '<cfoutput>#CppSetupData.StepSetup_vch#</cfoutput>';
var currentCDFGroup = 0;
var countCDFGroup = 1;
var currentClassStep = 0;

	<!--- Global var for the Campaign picker dialog--->
	var $dialogCampaignPicker = null;
	
	<!--- allow multiple batch pickers on one page--->
	var $TargetBatchId = null;
	var $TargetBatchDesc = null;
	

$('input#sortResult').attr('value',currentStepSetup);

$(document).ready(function(){


<!---
$('.add-new-field').off();
    $( document ).on('click', $('.add-new-field'), function(){
        currentCDFGroup = $('.add-new-field').parent().parent().parent().parent().parent().attr("id");
        <!---console.log(currentCDFGroup);--->
        OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_choosefieldscdf",
            "Add field to CPP",
            "auto",
            920,
            "",
            false
        );
    });
--->

    $('.add-new-field').off();
    $('.add-new-field').live('click',function(){
        currentCDFGroup = $(this).parent().parent().parent().parent().parent().attr("id");
        <!---console.log(currentCDFGroup);--->
        OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_choosefieldscdf",
            "Add field to CPP",
            "auto",
            920,
            "",
            false
        );
    });
   
    $('span.create-field').off();
    $('span.create-field').live('click',function(){
        currentCDFGroup = $(this).parent().parent().parent().parent().parent().attr("id");
        OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Contacts/dsp_AddcustomdataField",
            "Add a Custom Data Field",
            "auto",
            500,
            "",
            false
        );
    });
   
    $('a.cdf-html').off();
    $('a.cdf-html').live('click', function(){
        var id =$(this).attr('rel');
        if(id.indexOf('.') > 0){
            id = id.replace(/\./g,'_');
            $('.'+ id + ' li.lihtnml').addClass('active');
            $('.'+ id + ' li.lipreview').removeClass('active');
            $('.'+ id + ' .tab-content .tab-html').show();
            $('.'+ id + ' .tab-content .tab-preview').hide();
        }else{
            $('#cdf-'+ id + ' li.lihtnml').addClass('active');
            $('#cdf-'+ id + ' li.lipreview').removeClass('active');
            $('#cdf-'+ id + ' .tab-content .tab-html').show();
            $('#cdf-'+ id + ' .tab-content .tab-preview').hide();
        }
    });
   
    $('a.cdf-preview').off();
    $('a.cdf-preview').live('click', function(){
        var id =$(this).attr('rel');
        if(id.indexOf('.') > 0){
            id = id.replace(/\./g,'_');
            $('.'+ id + ' .tab-content .tab-preview').empty();
            $('.'+ id + ' li.lihtnml').removeClass('active');
            $('.'+ id + ' li.lipreview').addClass('active');
            $('.'+ id + ' .tab-content .tab-html').hide();
            var field = '';
            $.ajax({dataType: 'json',
                async: false,
                type: "POST",
                url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/customdata.cfc?method=GetCDFItemByID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                data: {inpCDFID: $('.'+ id + ' .tab-content .tab-html .cdf-define').attr('rel')},
                success: function(d){
                    if(d.FIELDTYPE.length == 0){
                        field = '';
                    }
                    else if(d.FIELDTYPE == 0) {
                        field = '<input style="text" class="input_box">';
                    }
                    else if(d.FIELDTYPE == 1) {
                        field += '<select class="select-default-value" >';
                        $.each(d.DATA,function(i, val) {
                            field += '<option>'+val.DEFAULTVALUE+'</option>';
                        });
                        field += '</select>';
                    }
                }
            });
            var tabPreview = '';
            tabPreview += '<div class="box-preview"><div class="left_box_preview"><label>'+$('.'+ id + ' .tab-content .tab-html .inpDescriptionStep6').html()+'</label></div>';
            tabPreview += '<div class="right_box_preview">'+field+'</div></div>';
            $('.'+ id + ' .tab-content .tab-preview').append(tabPreview);

            $('.'+ id + ' .tab-content .tab-preview .select-default-value').selectmenu({
                style: 'popup',
                width: '200',
                menuWidth: '70',
                appendTo: ' .tab-preview .right_box_preview'
            });

            $('.'+ id + ' .tab-content .tab-preview').show();
        }else{
            $('#cdf-'+ id + ' .tab-content .tab-preview').empty();
            $('#cdf-'+ id + ' li.lihtnml').removeClass('active');
            $('#cdf-'+ id + ' li.lipreview').addClass('active');
            $('#cdf-'+ id + ' .tab-content .tab-html').hide();
            var field = '';
            $.ajax({dataType: 'json',
                async: false,
                type: "POST",
                url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/contacts/customdata.cfc?method=GetCDFItemByID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                data: {inpCDFID:  $('#cdf-'+ id +' .tab-content .tab-html .cdf-define').attr('rel')},
                success: function(d){
                    if(d.FIELDTYPE.length == 0){
                        field = '';
                    }
                    else if(d.FIELDTYPE == 0) {
                        field = '<input style="text" class="input_box">';
                    }
                    else if(d.FIELDTYPE == 1) {
                        field += '<select class="select-default-value" >';
                        $.each(d.DATA,function(i, val) {
                            field += '<option>'+val.DEFAULTVALUE+'</option>';
                        });
                        field += '</select>';
                    }
                }
            });
            var tabPreview = '';
            tabPreview += '<div class="box-preview"><div class="left_box_preview"><label>'+$('#cdf-'+ id + ' .tab-content .tab-html .inpDescriptionStep6').html()+'</label></div>';
            tabPreview += '<div class="right_box_preview">'+field+'</div></div>';
            $('#cdf-'+ id + ' .tab-content .tab-preview').append(tabPreview);
            $('#cdf-'+ id + ' .tab-content .tab-preview .select-default-value').selectmenu({
                style: 'popup',
                width: '200',
                menuWidth: '70',
                appendTo: ' .tab-preview .right_box_preview'
            });
            $('#cdf-'+ id + ' .tab-content .tab-preview').show();
        }
    });
    $('.inpDescriptionStep6').off();
    $('.inpDescriptionStep6').live('click',function(){
        var index = $(this).attr('rel');
        $(this).hide();
        $('#tempContainerDescriptionStep6-'+index).show();
        $('#inpTempDescriptionStep6-'+index).html($('#inpDescriptionStep6-'+index).html());
        tinymce.init({
            selector: "#inpTempDescriptionStep6-"+index,
            forced_root_block: "",
            force_br_newlines: true,
            force_p_newlines: false,
            auto_focus: "inpTempDescriptionStep6-"+index,
            convert_urls: false,
			content_css: "<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
            plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
            toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            setup: function(editor){
                editor.on('blur', function(e){
                    $('#inpDescriptionStep6-'+index).html(tinymce.EditorManager.get('inpTempDescriptionStep6-'+index).getContent());
                    setNormal();
                });
            }
        });
    });
    var titleStep2 = '';
    var descriptionStep2 = '';
    var titleStep3 = '';
    var descriptionStep3 = '';
    var titleStep4 = '';
    var descriptionStep4 = '';
    var titleStep6 = '';
    var descriptionStep6 = '';

    var clickTitleStep2 = false;
    var clickTitleStep3 = false;
    var clickTitleStep4 = false;

    var clickDescriptionStep2 = false;
    var clickDescriptionStep3 = false;
    var clickDescriptionStep4 = false;

    var contentStep1  = '<div class="contactpreferencespan">';
    contentStep1 += 'Select how you will indentify each individual customer. You may select from one of the predefined options or define your own.';
    contentStep1 += '</div>';

    var contentStep2 = '<div class="contactpreferencespan">';
    contentStep2 += 'Add options a customer may select as communication preferences. ';
    contentStep2 += 'Each option can be associated with a unique contact group. ';
    contentStep2 += 'Contact groups allow you to group the responses of customers so that further communication can be sent to them. ';
    contentStep2 += 'You may also allow customers to select one or more options. ';
    contentStep2 += 'Simply enable "Multiple Preference Selection" and a user will be able to select more than one. ';
    contentStep2 += '</div>';

    contentStep2 += '<div style="clear:both;"></div>';
    contentStep2 += '<hr />';
    contentStep2 += '<div id="boxTitleStep2" class="box bpinput">';
    contentStep2 += '<div id="LeftTitleStep2" class="left_box_Area">Section Title';
    contentStep2 += '<div id="form-right-protal">';
    contentStep2 += '</div>';
    contentStep2 += '</div>';
    contentStep2 += '<div class="right_box_Area">';
	
	if (typeof(contactPreferenceObj) != "undefined") {
		if (typeof(contactPreferenceObj.title) == "undefined") {
			titleStep2 = '<cfoutput>#TITLE2#</cfoutput>';
		}
		else {
			titleStep2 = contactPreferenceObj.title;
		}
		if (typeof(contactPreferenceObj.description) == "undefined") {
			descriptionStep2 = '<cfoutput>#DESCIRPTION2#</cfoutput>';
		}
		else {
			descriptionStep2 = contactPreferenceObj.description;
		}
	}else{
		titleStep2 = '<cfoutput>#TITLE2#</cfoutput>';
		descriptionStep2 = '<cfoutput>#DESCIRPTION2#</cfoutput>';
	}
	contentStep2 += '<div id="inpTitleStep2" name="inpTitleStep2" class="input_box">' + titleStep2 + '</div> ';
	contentStep2 += '</div>';
	contentStep2 += '</div>';
	contentStep2 += '<div class="tempInp" id="tempContainerTitleStep2"><textarea id="inpTempTitleStep2" name="inpTempTitleStep2" class="input_box tempTitle"></textarea></div>';
	contentStep2 += '<div style="clear:both;"></div>';
	
	contentStep2 += '<div id="boxDescriptionStep2" class="box bpinput">';
	contentStep2 += '<div id="LeftDescriptionStep2" class="left_box_Area">Section Description';
	contentStep2 += '<div id="form-right-protal">';
	contentStep2 += '</div>';
	contentStep2 += '</div>';
	contentStep2 += '<div class="right_box_Area">';
    contentStep2 += '<div id="inpDescriptionStep2" name="inpDescriptionStep2" class="input_box">'+descriptionStep2+'</div> ';
    contentStep2 += '</div>';
    contentStep2 += '</div>';
    contentStep2 += '<div class="tempInp" id="tempContainerDescriptionStep2"><textarea id="inpTempDescriptionStep2" name="inpTempDescriptionStep2" class="input_box tempDescription"></textarea></div>';
    contentStep2 += '<div style="clear:both;"></div>';
    contentStep2 += '<hr />';

    contentStep2 += '<div class="contactpreferencespan select_multiple_preference contactpreferencespanRegoin">';
    contentStep2 += '<span class="include-text">Enable multiple preference selection?</span>';
    contentStep2 += '<span class="include-select">';
    contentStep2 += '<select name="inpMultiSelection" id="inpMultiSelection">';
    contentStep2 += '<option value="1" <cfif CppSetupData.MultiplePreference_ti EQ 1> selected </cfif>>Yes</option>';
    contentStep2 += '<option value="0" <cfif CppSetupData.MultiplePreference_ti EQ 0> selected </cfif>>No</option>';
    contentStep2 += '</select>';
    contentStep2 += '</span>';
    contentStep2 += '</div>';
    contentStep2 += '<div class="contactpreferencespan select_multiple_preference contactpreferencespanRegoin">';
    contentStep2 += '<span class="include-text">Set custom values for each preference?</span>';
    contentStep2 += '<span class="include-select">';
    contentStep2 += '<select name="customValue" id="customValue">';
    contentStep2 += '<option value="1" <cfif CppSetupData.SetCustomValue_ti EQ 1> selected </cfif>>Yes</option>';
    contentStep2 += '<option value="0" <cfif CppSetupData.SetCustomValue_ti EQ 0> selected </cfif>>No</option>';
    contentStep2 += '</select>';
    contentStep2 += '</span>';
    contentStep2 += '</div>';
    contentStep2 += '<div style="clear:both;"></div>';
    contentStep2 += '<br />';
    contentStep2 += '<span>Preferences</span>';
    contentStep2 += '<fieldset><div id="listPreference"></div></fieldset>';


    var contentStep3 = '<div class="contactpreferencespan">';
    contentStep3 += 'Select the channels of communication that a customer may select. ';
    contentStep3 += 'These options will display as checkboxes and a user may input information for more than one option. ';
    contentStep3 += '</div>';
    contentStep3 += '<div style="clear:both;"></div>';

    contentStep3 += '<hr />';
    contentStep3 += '<div id="boxTitleStep3" class="box bpinput">';
    contentStep3 += '<div id="LeftTitleStep3" class="left_box_Area">Section Title';
    contentStep3 += '<div id="form-right-protal">';
    contentStep3 += '</div>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="right_box_Area">';
	if (typeof(contactObj) != "undefined") {
		if (typeof(contactObj.title) == "undefined") {
			titleStep3 = '<cfoutput>#TITLE3#</cfoutput>';
		}
		else {
			titleStep3 = contactObj.title;
		}
		
		if (typeof(contactObj.description) == "undefined") {
			descriptionStep3 = '<cfoutput>#DESCIRPTION3#</cfoutput>';
		}
		else {
			descriptionStep3 = contactObj.description;
		}
	}else{
		titleStep3 = '<cfoutput>#TITLE3#</cfoutput>';
		descriptionStep3 = '<cfoutput>#DESCIRPTION3#</cfoutput>';
	}
	contentStep3 += '<div id="inpTitleStep3" name="inpTitleStep3" class="input_box">' + titleStep3 + '</div> ';
    contentStep3 += '</div>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="tempInp" id="tempContainerTitleStep3"><textarea id="inpTempTitleStep3" name="inpTempTitleStep3" class="input_box tempTitle"></textarea></div>';
    contentStep3 += '<div style="clear:both;"></div>';

    contentStep3 += '<div id="boxDescriptionStep3" class="box bpinput">';
    contentStep3 += '<div id="LeftDescriptionStep3" class="left_box_Area">Section Description';
    contentStep3 += '<div id="form-right-protal">';
    contentStep3 += '</div>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="right_box_Area">';
	
    contentStep3 += '<div id="inpDescriptionStep3" name="inpDescriptionStep3" class="input_box">' + descriptionStep3 + '</div> ';
    contentStep3 += '</div>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="tempInp" id="tempContainerDescriptionStep3"><textarea id="inpTempDescriptionStep3" name="inpTempDescriptionStep3" class="input_box tempDescription"></textarea></div>';
    contentStep3 += '<div style="clear:both;"></div>';
    contentStep3 += '<hr />';

    contentStep3 += '<div class="voice" align="center">';
    contentStep3 += '<div style="width:auto; height:auto;">';
    contentStep3 += '<div class="btn voice-checkbox" style="float:left;">';
    contentStep3 += '<input name="inpContactMethodVoice" <cfif CppSetupData.VoiceMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodVoice" type="checkbox" value="1"/>';
    contentStep3 += '<label for="inpContactMethodVoice"> Voice </label>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="btn voice-checkbox" style="float:left;">';
    contentStep3 += '<input name="inpContactMethodSMS" <cfif CppSetupData.SMSMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodSMS" type="checkbox" value="1"/>';
    contentStep3 += '<label for="inpContactMethodSMS"> SMS </label>';
    contentStep3 += '</div>';
    contentStep3 += '<div class="btn voice-checkbox" style="float:left;">';
    contentStep3 += '<input name="inpContactMethodEmail" <cfif CppSetupData.EmailMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodEmail" type="checkbox" value="1"/>';
    contentStep3 += '<label for="inpContactMethodEmail"> E-Mail </label>';
    contentStep3 += '</div>';
    contentStep3 += '</div>';
    contentStep3 += '<div id="ValidateContactMethod" placeholder="validate" tabindex="-1" class="hide-div-validate">';
    contentStep3 += '<span id="ValidateContactMethod1"  style="color:red;">You must choose at least one method</span>';
    contentStep3 += '</div>';
    contentStep3 += '</div>';


    var contentStep4 = '<div class="contactpreferencespan">';
    contentStep4 += 'Allow customers to select their language preference. ';
    contentStep4 += 'This will display as a drop down single selection option to the customer. ';
    contentStep4 += '</div>';

    contentStep4 += '<hr />';
    contentStep4 += '<div id="boxTitleStep4" class="box bpinput">';
    contentStep4 += '<div id="LeftTitleStep4" class="left_box_Area">Section Title';
    contentStep4 += '<div id="form-right-protal">';
    contentStep4 += '</div>';
    contentStep4 += '</div>';
    contentStep4 += '<div class="right_box_Area">';
	if (typeof(languageObj) != "undefined") {
	 	if (typeof(languageObj.title) == "undefined") {
	 		titleStep4 = '<cfoutput>#TITLE4#</cfoutput>';
	 	}
	 	else {
	 		titleStep4 = languageObj.title;
	 	}
	 	
	 	if (typeof(languageObj.description) == "undefined") {
	 		descriptionStep4 = '<cfoutput>#DESCIRPTION4#</cfoutput>';
	 	}
	 	else {
	 		descriptionStep4 = languageObj.description;
	 	}
	}else{
		titleStep4 = '<cfoutput>#TITLE4#</cfoutput>';
		descriptionStep4 = '<cfoutput>#DESCIRPTION4#</cfoutput>';
	}
	
	contentStep4 += '<div id="inpTitleStep4" name="inpTitleStep4" class="input_box">' + titleStep4 + '</div> ';
    contentStep4 += '</div>';
    contentStep4 += '</div>';
    contentStep4 += '<div class="tempInp" id="tempContainerTitleStep4"><textarea id="inpTempTitleStep4" name="inpTempTitleStep4" class="input_box tempTitle"></textarea></div>';
    contentStep4 += '<div style="clear:both;"></div>';

    contentStep4 += '<div id="boxDescriptionStep4" class="box bpinput">';
    contentStep4 += '<div id="LeftDescriptionStep4" class="left_box_Area">Section Description';
    contentStep4 += '<div id="form-right-protal">';
    contentStep4 += '</div>';
    contentStep4 += '</div>';
    contentStep4 += '<div class="right_box_Area">';
	
    contentStep4 += '<div id="inpDescriptionStep4" name="inpDescriptionStep4" class="input_box">' + descriptionStep4 + '</div> ';
    contentStep4 += '</div>';
    contentStep4 += '</div>';
    contentStep4 += '<div class="tempInp" id="tempContainerDescriptionStep4"><textarea id="inpTempDescriptionStep4" name="inpTempDescriptionStep4" class="input_box tempDescription"></textarea></div>';

    contentStep4 += '<div style="clear:both;"></div>';
    contentStep4 += '<hr />';
    contentStep4 += '<div class="box">';
    contentStep4 += '<div class="left_box lang_left_box">';
    contentStep4 += '<span class="btn btn_select_lang">Select Language</span>';
    contentStep4 += '</div>';
    contentStep4 += '<div class="right_box">';
    contentStep4 += '<select class="selectlist" name="inpType" id="inpType" multiple="multiple">';
    contentStep4 += '<option value="ArabicBengali" <cfif ListContains(CppSetupLanguage, "ArabicBengali","|") > selected="selected"</cfif>>ArabicBengali</option>';
    contentStep4 += '<option value="Chinese"<cfif ListContains(CppSetupLanguage, "Chinese","|") > selected="selected"</cfif>>Chinese</option>';
    contentStep4 += '<option value="English"<cfif ListContains(CppSetupLanguage, "English","|") > selected="selected"</cfif>>English</option>';
    contentStep4 += '<option value="French"<cfif ListContains(CppSetupLanguage, "French","|") > selected="selected"</cfif>>French</option>';
    contentStep4 += '<option value="German"<cfif ListContains(CppSetupLanguage, "German","|") > selected="selected"</cfif>>German</option>';
    contentStep4 += '<option value="Hindi"<cfif ListContains(CppSetupLanguage, "Hindi","|") > selected="selected"</cfif>>Hindi</option>';
    contentStep4 += '<option value="Japanese"<cfif ListContains(CppSetupLanguage, "Japanese","|") > selected="selected"</cfif>>Japanese</option>';
    contentStep4 += '<option value="Korean"<cfif ListContains(CppSetupLanguage, "Korean","|") > selected="selected"</cfif>>Korean</option>';
    contentStep4 += '<option value="Portuguese"<cfif ListContains(CppSetupLanguage, "Portuguese","|") > selected="selected"</cfif>>Portuguese</option>';
    contentStep4 += '<option value="Russian"<cfif ListContains(CppSetupLanguage, "Russian","|") > selected="selected"</cfif>>Russian</option>';
    contentStep4 += '<option value="Spanish"<cfif ListContains(CppSetupLanguage, "Spanish","|") > selected="selected"</cfif>>Spanish</option>';
    contentStep4 += '</select>';
    contentStep4 += '</div>';
    contentStep4 += '</div>';


    var contentStep5 = '<div class="contactpreferencespan">';
    contentStep5 += '<div class="tabbable">';
    contentStep5 += '<ul class="nav nav-tabs">';
    contentStep5 += '<li class="active">';
    contentStep5 += '<a id="tab1" href="#tab1" data-toggle="tab">HTML</a>';
    contentStep5 += '</li>';
    contentStep5 += '<li>';
    contentStep5 += '<a id="tab2" href="#tab2" data-toggle="tab">Preview</a>';
    contentStep5 += '</li>';
    contentStep5 += '</ul>';
    contentStep5 += '<div class="tab-content">';
    contentStep5 += '<div class="tab-pane active" id="tab1">';
    contentStep5 += '<div class="customizeBox" id="custom-html">';
    contentStep5 += '<textarea class="customHtml portaladd-input-box1 mceEditor"></textarea>';
    contentStep5 += '</div>';
    contentStep5 += '</div>';
    contentStep5 += '<div class="tab-pane tab-preview-content" id="tab2"></div>';
    contentStep5 += '</div>';
    contentStep5 += '</div>';
    contentStep5 += '</div>';
	
	<!--- Define drop content for optional verification step --->
	var contentStep8 =  '<div class="contactpreferencespan">';
	contentStep8    +=  '	<div class="inputbox">';
	contentStep8    +=  '		<input type="hidden" name="inpBatchPickerIdA" id="inpBatchPickerIdA" value="<cfoutput>#CppSetupData.VerificationBatchId_bi#</cfoutput>">';
	contentStep8    +=  '		<label for="inpBatchDescA">Select a Campaign for Opt In Verfication</label>';
	contentStep8    +=  '		<div class="wrapper-picker-container BatchPicker" rel1="inpBatchDescA" rel2="inpBatchPickerIdA">';                              
	contentStep8    +=  '			<div class="wrapper-dropdown-picker input-box inpBatchA" tabindex="1"> <span id="inpBatchDescA"><cfoutput>#inpVerificationBatchDesc#</cfoutput></span> </div>';                                            
	contentStep8    +=  '		</div>';
	contentStep8    +=  '	</div>';						
    contentStep8    +=  '</div>';
	
	
	function getContentStep6(indexStep6){
		var contentStep6 = '';
		if (typeof(cdfObj) != "undefined") {
			if (typeof(cdfObj.title) == "undefined") {
				titleStep6 = '<cfoutput>#TITLE6#</cfoutput>';
			}
			else {
				titleStep6 = htmlDecode(cdfObj.title);
			}
			
			if (typeof(cdfObj.description) == "undefined") {
				descriptionStep6 = '<cfoutput>#DESCIRPTION6#</cfoutput>';
			}
			else {
				descriptionStep6 = htmlDecode(cdfObj.description);
			}
		}else{
			titleStep6 = '<cfoutput>#TITLE6#</cfoutput>';
			descriptionStep6 = '<cfoutput>#DESCIRPTION6#</cfoutput>';
		}
	    contentStep6 += '<div style="clear:both;"></div>';
	
	    contentStep6 += '<div class="box bpinput boxDescriptionStep6">';
	    contentStep6 += '<div class="left_box_Area LeftDescriptionStep6">Section Description';
	    contentStep6 += '<div id="form-right-protal">';
	    contentStep6 += '</div>';
	    contentStep6 += '</div>';
	    contentStep6 += '<div class="right_box_Area">';
		
	    contentStep6 += '<div name="inpDescriptionStep6" id="inpDescriptionStep6-'+indexStep6+'" rel="'+indexStep6+'" class="inpDescriptionStep6">' + descriptionStep6 + '</div> ';
	    contentStep6 += '</div>';
	    contentStep6 += '</div>';
	    contentStep6 += '<div class="tempInp tempContainerDescriptionStep6" id="tempContainerDescriptionStep6-'+indexStep6+'"><textarea id="inpTempDescriptionStep6-'+indexStep6+'" name="inpTempDescriptionStep6" class="input_box tempDescription"></textarea></div>';
	    return contentStep6;
	}
	
    function getContentStep5(index, content){
        var contentStep5 = '<div class="contactpreferencespan">';
        contentStep5 += '<div class="tabbable">';
        contentStep5 += '<ul class="nav nav-tabs">';
        contentStep5 += '<li class="active">';
        contentStep5 += '<a data-id="'+index+'" id="tab1'+index+'" href="#tab1'+index+'" data-toggle="tab">HTML</a>';
        contentStep5 += '</li>';
        contentStep5 += '<li>';
        contentStep5 += '<a data-id="'+index+'" id="tab2'+index+'" href="#tab2'+index+'" data-toggle="tab">Preview</a>';
        contentStep5 += '</li>';
        contentStep5 += '</ul>';
        contentStep5 += '<div class="tab-content">';
        contentStep5 += '<div class="tab-pane active" id="tab-html-'+index+'">';
        contentStep5 += '<div class="customizeBox" id="custom-html'+index+'">';
        contentStep5 += '<textarea class="customHtml customHtml'+ index +' portaladd-input-box1 mceEditor">'+content+'</textarea>';
        contentStep5 += '</div>';
        contentStep5 += '</div>';
        contentStep5 += '<div class="tab-pane tab-preview-content" id="tab-preview-'+index+'"></div>';
        contentStep5 += '</div>';
        contentStep5 += '</div>';
        contentStep5 += '</div>';
        return contentStep5;
    }
    function setContentStep6(indexStep6, title, description){
		
		title = htmlDecode(title);
		description = htmlDecode(description);
		
        var contentStep6 = '<div class="contactpreferencespan">';
        contentStep6 += '<div style="clear:both;"></div>';

        contentStep6 += '<div class="box bpinput boxDescriptionStep6">';
        contentStep6 += '<div class="left_box_Area LeftDescriptionStep6">Section Description';
        contentStep6 += '<div id="form-right-protal">';
        contentStep6 += '</div>';
        contentStep6 += '</div>';
        contentStep6 += '<div class="right_box_Area">';

        contentStep6 += '<div name="inpDescriptionStep6" id="inpDescriptionStep6-'+indexStep6+'" rel="'+indexStep6+'" class="inpDescriptionStep6">' + description + '</div> ';
        contentStep6 += '</div>';
        contentStep6 += '</div>';
        contentStep6 += '<div class="tempInp tempContainerDescriptionStep6" id="tempContainerDescriptionStep6-'+indexStep6+'"><textarea id="inpTempDescriptionStep6-'+indexStep6+'" name="inpTempDescriptionStep6" class="input_box tempDescription"></textarea></div>';
        contentStep6 += '<div style="clear:both;"></div>';
        contentStep6 += '<hr />';
        return contentStep6;
    }

        $('#subTitleText').text('Create CPP - Step 2 - Modules');
		$('#mainTitleText').text('Customer Preference Portal');
	
	        $("#btnCreateGroupContact").click(function(){
	    var groupName = $("#inpGroupContactName").val();
	        if(groupName == ""){
	    $(".groupRequire").show();
	}else{
	    $(".groupRequire").hide();
	        $.ajax({
	    type: "POST",
	    url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=addgroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    data:{
	        inpGroupDesc: groupName
	    },
	    dataType: "json",
	    success: function(d) {
	        if(d.DATA.RXRESULTCODE[0] > 0){
	            CloseDialog('createGroupContact');
	            listPreference();
	        }else{
	            jAlert(d.DATA.MESSAGE[0],"Create Group contact fail." );
	        }
	    }
	});
	}
	});

    var parentBeforeSort = '';
    var targetId = '';
    var idStep = '';
    $('.sortable-list').sortable({
        connectWith: '.sortable-list',
        handle: ".panel-heading",
        cursor: 'move',//other cursor: ["crosshair", "pointer", "text", "move", "wait", "help", "progress"],
        start:function(event,ui){
            var parentId = $(ui.item).parent().attr("id");
            var id = $(ui.item).attr("id");
            var classStep5 = $(ui.item).hasClass("classStep5");
            parentBeforeSort = $(ui.item).parent().attr("id");
            // check to contentPreferences or not
            if (parentId == "contentPreferences" && parentId == parentBeforeSort && classStep5) {
                // cut id
                idStep = $(ui.item).find("li.active>a").attr('id').substring(4, $(this).attr('id').length);
                targetId = $(ui.item).find("li.active>a").attr('id').substring(3, $(this).attr('id').length);
                <!---console.log("li.active>a#tab2"+idStep);--->
                $("a#tab2" + idStep).click(function(){
                    // hide tab1
                    $('#tab-html-'+targetId).hide();
                    $('#tab-preview-'+targetId).show();

                    var arialEditor = $("#custom-html" + idStep).find("textarea");
                    var idTinyMCE = arialEditor.attr("id");
                    $('div#tab2' + idStep).html(tinymce.EditorManager.get(idTinyMCE).getContent());
                });
                $("a#tab2" + idStep).trigger("click");
                tinymce.remove('textarea');
                tinymce.remove('.tempTitle');
                tinymce.remove('.tempDescription');
            }
        },
        stop:function(event, ui){
            // check stop drag
            var id = $(ui.item).attr("id");
            var parentId = $(ui.item).parent().attr("id");
            var classStep5 = $(ui.item).hasClass("classStep5");
            var classStep6 = $(ui.item).hasClass("classStep6");
            if (parentId == "contentPreferences" && parentId == parentBeforeSort && classStep5) {
                $('a#tab1' + idStep).click(function(){
                    //alert(tinyMCE.activeEditor.getContent());
                    // hide tab1
                    $('#tab-html-'+targetId).show();
                    $('#tab-preview-'+targetId).hide();
                });
                $('a#tab1' + idStep).click();
            }


            // check to contentPreferences or not
            if(parentId == "contentPreferences"){
                $(ui.item.parent().parent()).find(".panel-body").removeClass("displayNone"); // good
                if (id == "step1") {
                    if (parentId != parentBeforeSort) {
                        $(ui.item).find('.panel-heading').append('<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        AfterDrop();
                    }
                    if ((ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").children().length == 0) {
                        $(ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").append(contentStep1);
                        $('select#inpIncludePreference').selectmenu({
                            style: 'popup',
                            width: '70',
                            menuWidth: '70'
                        });
                    }
                }
                else
                if (id == "step2") {
                    if ((ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").children().length == 0) {
                        $(ui.item.parent().parent()).find("#contentPreferences>#" + id + ">div.panel-body").append(contentStep2);
                        $('select#customValue').selectmenu({
                            style: 'popup',
                            width: '70',
                            change: function(){
                                var customValue = $("#customValue").val();
                                if (customValue == 1) {
                                    $('.preference-name-txt').hide();
                                    $("input[name=preferenceValue]").show();
                                }
                                else {
                                    $('.preference-name-txt').show();
                                    $("input[name=preferenceValue]").hide();
                                }
                            },
                            appendTo: '.contactpreferencespanRegoin'
                        });
                        $('select#inpMultiSelection').selectmenu({
                            style: 'popup',
                            width: '70',
                            menuWidth: '70',
                            appendTo: '.contactpreferencespanRegoin'
                        });
                        listPreference();
                        $('select.preferenceDropdown').css('width', '399px');
                        if (parentId != parentBeforeSort) {
                            $('#inpTitleStep2').click(function(){
                                setNormal();
                                $(this).hide();
                                $('#tempContainerTitleStep2').show();
                                $('#inpTempTitleStep2').html($('#inpTitleStep2').html());
                                //alert('run');
                                tinymce.init({
                                    selector: "#inpTempTitleStep2",
                                    forced_root_block : "",
                                    force_br_newlines : true,
                                    force_p_newlines : false,
                                    convert_urls: false,
                                    auto_focus: "inpTempTitleStep2",
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor) {
                                        editor.on('blur', function(e) {
                                            // get data in tinyMCE to text area
                                            $('#inpTitleStep2').html(tinymce.EditorManager.get('inpTempTitleStep2').getContent());
                                            setNormal();
                                        });
                                    }
                                });
                            });

                            $('#inpDescriptionStep2').click(function(){
                                setNormal();
                                $(this).hide();
                                $('#tempContainerDescriptionStep2').show();
                                $('#inpTempDescriptionStep2').html($('#inpDescriptionStep2').html());
                                tinymce.init({
                                    //selector: "#inpTempTitleStep2",
                                    selector: "#inpTempDescriptionStep2",
                                    forced_root_block: "",
                                    force_br_newlines: true,
                                    force_p_newlines: false,
                                    auto_focus: "inpTempDescriptionStep2",
                                    convert_urls: false,
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor){
                                        editor.on('blur', function(e){
                                            $('#inpDescriptionStep2').html(tinymce.EditorManager.get('inpTempDescriptionStep2').getContent());
                                            setNormal();
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
                else
                if (id == "step3") {

                    if ((ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").children().length == 0) {
                        $(ui.item.parent().parent()).find("#contentPreferences>#" + id + ">div.panel-body").append(contentStep3);
                        if (parentId != parentBeforeSort) {
                            $('#inpTitleStep3').click(function(){
                                //alert('step3');
                                setNormal();
                                $(this).hide();
                                $('#tempContainerTitleStep3').show();
                                $('#inpTempTitleStep3').html($('#inpTitleStep3').html());
                                tinymce.init({
                                    selector: "#inpTempTitleStep3",
                                    //selector: "#inpTempDescriptionStep3",
                                    forced_root_block : "",
                                    force_br_newlines : true,
                                    force_p_newlines : false,
                                    auto_focus: "inpTempTitleStep3",
                                    convert_urls: false,
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor) {
                                        editor.on('blur', function(e) {
                                            $('#inpTitleStep3').html(tinymce.EditorManager.get('inpTempTitleStep3').getContent());
                                            setNormal();
                                        });

                                    }
                                });
                            });

                            $('#inpDescriptionStep3').click(function(){
                                //alert('step3');
                                setNormal();
                                $(this).hide();
                                $('#tempContainerDescriptionStep3').show();
                                $('#inpTempDescriptionStep3').html($('#inpDescriptionStep3').html());
                                tinymce.init({
                                    //selector: "#inpTempTitleStep3",
                                    selector: "#inpTempDescriptionStep3",
                                    forced_root_block: "",
                                    force_br_newlines: true,
                                    force_p_newlines: false,
                                    auto_focus: "inpTempDescriptionStep3",
                                    convert_urls: false,
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor){
                                        editor.on('blur', function(e){
                                            $('#inpDescriptionStep3').html(tinymce.EditorManager.get('inpTempDescriptionStep3').getContent());
                                            setNormal();
                                        });

                                    }
                                });
                            });
                        }
                    }
                    $('input#inpContactMethodVoice').custominput();
                    $('input#inpContactMethodSMS').custominput();
                    $('input#inpContactMethodEmail').custominput();
                }
                else
                if (id == "step4") {
                    if (parentId != parentBeforeSort) {
                        $(ui.item).find('.panel-heading').append('<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        AfterDrop();
                    }
                    if ((ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").children().length == 0) {
                        $(ui.item.parent().parent()).find("#contentPreferences>#" + id + ">div.panel-body").append(contentStep4);

                        $("select#inpType").dropdownchecklist({
                            width: 180,
                            maxDropHeight: 250,
                            explicitClose: '[Close]',
                            emptyText: "Please Select..."
                            /*, textFormatFunction: function(options) {
                             var selectedOptions = options.filter(":selected");
                             var countOfSelected = selectedOptions.size();
                             switch(countOfSelected) {
                             case 0: return "<i>Please Select...<i>";
                             case 1: return selectedOptions.text();
                             case options.size(): return "<b>All Language</b>";
                             default: return countOfSelected + " Languages";
                             }
                             }*/
                        });

                        $('select#inpLanguagePreference').selectmenu({
                            style: 'popup',
                            width: '70'
                        });

                        if (parentId != parentBeforeSort) {
                            $('#inpTitleStep4').click(function(){
                                setNormal();
                                $(this).hide();
                                $('#tempContainerTitleStep4').show();
                                $('#inpTempTitleStep4').html($('#inpTitleStep4').html());
                                tinymce.init({
                                    selector: "#inpTempTitleStep4",
                                    forced_root_block: "",
                                    force_br_newlines: true,
                                    force_p_newlines: false,
                                    //selector: "#inpTempDescriptionStep4",
                                    auto_focus: "inpTempTitleStep4",
                                    convert_urls: false,
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor){
                                        editor.on('blur', function(e){
                                            $('#inpTitleStep4').html(tinymce.EditorManager.get('inpTempTitleStep4').getContent());
                                            setNormal();
                                        });
                                    }
                                });
                            });

                            $('#inpDescriptionStep4').click(function(){
                                //alert('step4');
                                setNormal();
                                $(this).hide();
                                $('#tempContainerDescriptionStep4').show();
                                $('#inpTempDescriptionStep4').html($('#inpDescriptionStep4').html());
                                tinymce.init({
                                    //selector: "#inpTempTitleStep4",
                                    forced_root_block: "",
                                    force_br_newlines: true,
                                    force_p_newlines: false,
                                    selector: "#inpTempDescriptionStep4",
                                    auto_focus: "inpTempDescriptionStep4",
                                    convert_urls: false,
									content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                                    setup: function(editor){
                                        editor.on('blur', function(e){
                                            $('#inpDescriptionStep4').html(tinymce.EditorManager.get('inpTempDescriptionStep4').getContent());
                                            setNormal();
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
                else
                if (classStep5) {
                    if (parentId != parentBeforeSort) {
                        $(ui.item).find('.panel-heading').append('<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        AfterDrop();
                        countEditor += 1;
                        //$(ui.item.parent().parent()).find("#contentPreferences>div.classStep5>div.panel-body").html('');
                        $(ui.item).find("div.panel-body").addClass('customize-Html-modules').append(getContentStep5(countEditor, ""));
                        $(ui.item).attr('id', 'step5');
                        $(ui.item).attr('action', 'add');

                        tinymce.init({
                            selector: ".customHtml" + countEditor,
                            //selector: ".customHtml",
                            convert_urls: false,
							content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                            plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                            toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                            editor_selector: "mceEditor",
                            editor_deselector: "mceNoEditor"
                        });

                        $('a#tab2'+countEditor).click(function(e){
                            var $target = $(e.currentTarget), targetId = $target.data('id');

                            //alert(tinyMCE.activeEditor.getContent());
                            // hide tab1
                            $('#tab-html-'+targetId).hide();
                            $('#tab-preview-'+targetId).show();

                            var arialEditor = $("#custom-html"+targetId).find("textarea");
                            var idTinyMCE = arialEditor.attr("id");

                            $('#tab-preview-'+targetId).html(tinymce.EditorManager.get(idTinyMCE).getContent());
                        });

                        $('a#tab1'+countEditor).click(function(e){
                            var $target = $(e.currentTarget), targetId = $target.data('id');
                            //alert(tinyMCE.activeEditor.getContent());
                            // hide tab1
                            $('#tab-html-'+targetId).show();
                            $('#tab-preview-'+targetId).hide();
                        });

                        // render position of step5
                        //$('#contentPreferences').children()
                    }else{
                        //$("textarea",ui.item).tinymce(myconfig.tinymcesettings);
                        tinymce.init({
                            selector: ".customHtml",
                            convert_urls: false,
							content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                            plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                            toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        });
                    }
                }
				<!--- For the verification module--->
				else if (id == "step8") 
				{

					<!--- Drop module into portal --->
                    if ((ui.item.parent().parent()).find("#contentPreferences>div#" + id + ">div.panel-body").children().length == 0) 
					{
						<!--- Append  module as defined in variable contentStep8 --->
                        $(ui.item.parent().parent()).find("#contentPreferences>#" + id + ">div.panel-body").append(contentStep8);
                        
						<!--- If this is not just a sort and IS an intial drop - do special processing on new module --->
						if (parentId != parentBeforeSort) 
						{
							<!--- Allow this module to be deleted --->	
                            $(ui.item).find('.panel-heading').append('<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');

							<!--- Initialize any post drop click events --->
 							AfterDrop();
 
 
					 		<!--- Open a Campaign picker dialog - kill any other picker dialogs that may be open first--->
							<!--- Reuse current Campaign picker if its still there --->
							$(ui.item).find(".BatchPicker").click( function() 
							{			
																
								if($dialogCampaignPicker == null)
								{		
									<!--- Load content into a picker dialog--->			
									$dialogCampaignPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
										width: 1200,
										height: 'auto',
										modal: true,
										dialogClass:'GreyBG',
										autoOpen: false,
										title: 'Campaign Picker',
										draggable: true,
										resizable: true
									}).load('../../../campaign/act_campaignpicker');
								}
										
								<!--- Tie picker dialog to the biottom of the object that opend it.--->
								var x = $(this).position().left; 
								var y = $(this).position().top + 30 - $(document).scrollTop();
								$dialogCampaignPicker.dialog('option', 'position', [x,y]);			
						
								$TargetBatchDesc = $("#" + $(this).attr('rel1') ); 
								$TargetBatchId =  $("#" + $(this).attr('rel2') ); 
							
								$dialogCampaignPicker.dialog('open');
							}); 		
							
                        }
						
                    }
                    
                }
                else
                if (classStep6) {
                    if (parentId != parentBeforeSort) {
                        var indexStep6 = $("#contentouterdiv .block-add-field").length;
                        var regoin_cdf = '';
                        regoin_cdf += '<div class="tabbable">';
                            regoin_cdf += '<ul class="nav nav-tabs"><li class="lihtnml active"><a class="cdf-html" rel='+countCDFGroup+'>HTML</a></li><li class="lipreview"><a class="cdf-preview" rel='+countCDFGroup+'>Preview</a></li></ul>';
                            regoin_cdf += '<div class="tab-content">';
                                regoin_cdf += '<div class="tab-html">';
                                    regoin_cdf += '<div class="listFields">';
                                        regoin_cdf += '<label>Select a CDF</label>';
                                        regoin_cdf += '<div class="wrapper-dropdown-picker input-box add-new-field"> <span class="cdf-define" rel="">click here to select a CDF</span></div>';
                                        regoin_cdf += '<span class="create-field"></span>';
                                    regoin_cdf += '</div>';
                                regoin_cdf += '</div>';
                                regoin_cdf += '<div class="tab-preview" style="display: none"></div>';
                            regoin_cdf += '</div>';
                        regoin_cdf += '</div>';
                        $(regoin_cdf).insertAfter($(ui.item).find('.ui-state-default'));
						$(ui.item).attr("id","cdf-" + countCDFGroup);
						$(ui.item).addClass("block-add-field");
						countCDFGroup++;
                        $(ui.item).find('.panel-heading').append('<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
                        $(ui.item).attr('action', 'add');
                        var contentStep6 = getContentStep6(indexStep6+1);
                        $(ui.item).find("div.panel-body").append(contentStep6);
                        $($(ui.item).find("div.panel-body")).appendTo($(ui.item).find("div.tab-html"));

                    }
                    AfterDrop();
                }

                var countModulesInPreferences = $('#contentPreferences .panel-primary').length;
                if (countModulesInPreferences >= 1) {
                    $('#contentPreferences #drop_to').css("display", "none");
                }
                else {
                    $('#contentPreferences #drop_to').css("display", "block");
                }
            }

            if(parentId == "contentModules"){
                if (parentId != parentBeforeSort) {
                    //alert(parentId);
                    if ($('#contentModules>.classStep5').length >= 1) {
                        //alert($(ui.item).hasClass('classStep5'));
                        if ($(ui.item).attr('id') == 'step5' || $(ui.item).hasClass('classStep5')) {
                            if ($(ui.item).attr('action') == 'add') {
                                $(ui.item).remove();
                            }
                            else {
                                $(ui.item).attr('action', 'delete')
                            }
                            $(ui.item).addClass('displayNone');
                        }
                    }
					
					if ($('#contentModules>.classStep6').length >= 1) {
                        //alert($(ui.item).hasClass('classStep5'));
                        if ($(this).attr('id').substring(0, 4) == 'cdf-' || $(ui.item).hasClass('classStep6')) {
                            if ($(ui.item).attr('action') == 'add') {
                                $(ui.item).remove();
                            }
                            else {
                                $(ui.item).attr('action', 'delete')
                            }
                            $(ui.item).addClass('displayNone');
                        }
                    }

                    if ($(ui.item).attr('id') == 'step4' || $(ui.item).attr('id') == 'step1'){
                        $(ui.item).find('.panel-heading>.delete-html-module').remove();
                    }

                    // check modules container module or not
                    var countModulesInPreferences = $('#contentPreferences .panel-primary').length;
                    if (countModulesInPreferences >= 1) {
                        $('#contentPreferences #drop_to').css("display", "none");
                    }
                    else {
                        $('#contentPreferences #drop_to').css("display", "block");
                    }
                    $(ui.item).find(".panel-body").html("");
                    $(ui.item).find(".panel-body").addClass("displayNone");

                    // render all tinyMCE
                    tinymce.init({
                        selector: ".customHtml",
                        convert_urls: false,
						content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                        plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                        toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    });
                }
            }else if(parentId == "contentPreferences"){
                if (parentId != parentBeforeSort) {
                    if ($('#contentModules>.classStep5').length == 0) {
                        $('#contentModules').append('<div class="panel panel-primary classStep5"><div class="panel-heading ui-state-default"><span style="" class="panel-heading-icon heading-buildportal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Custom HTML Module</div><div class="panel-body displayNone"></div></div>');
                    }
					
					if ($('#contentModules>.classStep6').length == 0) {
                        $('#contentModules').append('<div class="panel panel-primary classStep6"><div class="panel-heading ui-state-default"><span style="" class="panel-heading-icon heading-buildportal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>CDF</div><div class="panel-body displayNone"></div></div>');
                    }
                }
            }

            tinymce.init({
                selector: ".tempTitle",
                forced_root_block : "",
                force_br_newlines : true,
                force_p_newlines : false,
                convert_urls: false,
				content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });

            tinymce.init({
                selector: ".tempDescription",
                forced_root_block : "",
                force_br_newlines : true,
                force_p_newlines : false,
                convert_urls: false,
				content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });


            // update hindden field
            var sortResult = '';
            var groups = $('#contentPreferences').children();
            groups.each(function(index){
                if ($(this).attr('id').length == 1) {
                    sortResult += $(this).attr('id') + ',';
                }
                else
                if ($(this).attr('id') == 'drop_to') {

                }
                else {
					if ($(this).attr('id').substring(0, 4) == 'cdf-') 
						var idStep = '6';
					else {
						var idStep = $(this).attr('id').substring(4, $(this).attr('id').length);
					}
                    sortResult += idStep + ',';
                }

            });

            $('input#sortResult').attr('value', sortResult);
            setNormal();
        }
    });

function BuildStepData(stepId,index){
    var contentStepData;//this var is defined to obtain html data of corresponding step
    var validStepId= true;//this var is defined to check whether input param "stepId" is valid
    var headingContent = "";
    switch(stepId)
    {
        case "step1":
            contentStepData = contentStep1;
            headingContent = "Login";
            break;
        case "step2":
            contentStepData = contentStep2;
            headingContent = "Contact preferences";
            break;
        case "step3":
            contentStepData = contentStep3;
            headingContent = "Contact methods";
            break;
        case "step4":
            contentStepData = contentStep4;
            headingContent = "Language preference";
            break;
	
		<!--- Verfication Messages --->
		case "step8":			
			contentStepData = contentStep8;
            headingContent = "Verification Messaging";			
			break;	
			
        default:
            break;
    }
	
	<!--- Special actions for Step 5 - why not in switch statement above ?!? --->
    if(stepId=="step5")
	{
	    contentStepData = getContentStep5(countEditor, "");
	    headingContent = "Custom HTML Module";
	}
	else if(stepId!="step1" && stepId!="step2" && stepId!="step3" && stepId!="step4" && stepId!="step5" && stepId!="step8")
	{
		var customHtmlData = <cfoutput>#CustomHtmlData#</cfoutput>;
		    for(var i =0 ; i< customHtmlData.length; i++){
		        if(customHtmlData[i].CUSTOMHTMLID == stepId){
		
		            contentStepData = getContentStep5(countEditor, customHtmlData[i].CUSTOMHTML);
		            break;
		        }
		    }
		    headingContent = "Custom HTML Module";
	}
	
    <!--- check step id, if not valid, not build data --->
    if(!validStepId)
		return false;
	
	<!--- hide drop to box--->
	$('#contentPreferences #drop_to').css("display","none");
	$("#"+stepId).remove();
	
	var tempStep = stepId.substring(4,stepId.length);
	
	//build html data of div step1 in preference box, which has detail data
	if(tempStep >= 5 && tempStep < 6)<!--- Why is this not just == 5 ?!? --->
	{
	    var stepData = '<div id="'+stepId+'" class="panel panel-primary classStep5" action="update"><div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+headingContent+'<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>';
	}
	else if(tempStep == 4 || tempStep == 1)
	{
	    var stepData = '<div id="'+stepId+'" class="panel panel-primary"><div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+headingContent+'<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>';
	}
	else if(tempStep == 8)
	{
	    var stepData = '<div id="'+stepId+'" class="panel panel-primary"><div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+headingContent+'<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>';
	}
	else if(tempStep >= 6 && tempStep < 7) <!--- Why is this not just == 6 ?!? --->
	{
		var CdfHtmlData = <cfoutput>#CdfHtmlData#</cfoutput>;
	    var title = '';
	    var description = '';
	    var data ={};
	    var selected = '';
	    $.each(CdfHtmlData.FIELD, function(i, item) {
	        if(item.IDSTEP == stepId) {
	            data[item.ID] = item.NAME;
                selected += "<span class='cdf-define' rel='" + item.ID + "'>"+ item.NAME + "</span>";
	            description = item.DESCRIPTION;
	        }
	    });

		currentClassStep = stepId.replace(/\./g,'_');
	    contentStepData = setContentStep6(index,title,description);
	    headingContent = "CDF";
	    var listFiled = '<div class="tabbable">';
        listFiled += '<ul class="nav nav-tabs"><li class="lihtnml active"><a class="cdf-html active" rel='+stepId+'>HTML</a></li><li class="lipreview"><a class="cdf-preview"rel='+stepId+'>Preview</a></li></ul>';
        listFiled += '<div class="tab-content">';
        listFiled += '<div class="tab-html">';
        listFiled += '<div class="listFields"><label>Select a CDF</label>';
        listFiled += '<div class="wrapper-dropdown-picker input-box add-new-field">' + selected + '</div>';
        listFiled += '<span class="create-field"></span> </div>';
	    var stepData = '<div id="'+stepId+'" class="panel panel-primary classStep6 block-add-field  '+ currentClassStep +'" action="update"><div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+headingContent+'<span style="" class="delete-html-module">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>'+listFiled;
	}
	else
	{
	    var stepData = '<div id="'+stepId+'" class="panel panel-primary '+ currentClassStep +'"><div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'+headingContent+'</div>';
	}

	<!--- Custom ixe body for HTML classes--->
    // Check template Step 5 tempStep
    if(tempStep >= 5 && tempStep < 6){ <!--- Why is this not just == 5 ?!? --->
        stepData += '<div class="panel-body customize-Html-modules">';
        stepData += contentStepData;
        stepData += '</div>';
        stepData += '</div>';
    }
    else{
        stepData += '<div class="panel-body">';
        <!--- Continue on with default actions --->
        stepData += contentStepData;
        stepData += '</div>';
        stepData += '</div>';
        if(tempStep >= 6 && tempStep < 7){
            stepData += '</div>';
            stepData += '<div class="tab-preview" style="display: none"></div>';
            stepData += '</div>';
            stepData += '</div>';
        }
    }
	<!---use prepend function to keep order of steps--->
    $("#preferences").find("#contentPreferences").prepend(stepData);
	
    <!---remove class display none of pabel body, that makes detail data showed--->
    $("#"+stepId+">div.panel-body").removeClass("displayNone");

    if (stepId == "step2") {
        $('#inpTitleStep2').click(function(){
            setNormal();
            $(this).hide();
            $('#tempContainerTitleStep2').show();
            $('#inpTempTitleStep2').html($('#inpTitleStep2').html());
            if(!clickTitleStep2){
                //alert('run');
                tinymce.init({
                    selector: "#inpTempTitleStep2",
                    forced_root_block : "",
                    force_br_newlines : true,
                    force_p_newlines : false,
                    convert_urls: false,
                    auto_focus: "inpTempTitleStep2",
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor) {
                        editor.on('blur', function(e) {
                            // get data in tinyMCE to text area
                            $('#inpTitleStep2').html(tinymce.EditorManager.get('inpTempTitleStep2').getContent());
                            setNormal();
                        });
                    }
                });
            }
            clickTitleStep2 = true;
        });

        $('#inpDescriptionStep2').click(function(){
            setNormal();
            $(this).hide();
            $('#tempContainerDescriptionStep2').show();
            $('#inpTempDescriptionStep2').html($('#inpDescriptionStep2').html());
            if (!clickDescriptionStep2) {
                tinymce.init({
                    //selector: "#inpTempTitleStep2",
                    selector: "#inpTempDescriptionStep2",
                    forced_root_block: "",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    auto_focus: "inpTempDescriptionStep2",
                    convert_urls: false,
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor){
                        editor.on('blur', function(e){
                            $('#inpDescriptionStep2').html(tinymce.EditorManager.get('inpTempDescriptionStep2').getContent());
                            setNormal();
                        });
                    }
                });
            }
            clickDescriptionStep2 = true;
        });
    }
    else
    if (stepId == "step3") {
        $('#inpTitleStep3').click(function(){
            //alert('step3');
            setNormal();
            $(this).hide();
            $('#tempContainerTitleStep3').show();
            $('#inpTempTitleStep3').html($('#inpTitleStep3').html());
            if (!clickTitleStep3) {
                tinymce.init({
                    selector: "#inpTempTitleStep3",
                    //selector: "#inpTempDescriptionStep3",
                    forced_root_block : "",
                    force_br_newlines : true,
                    force_p_newlines : false,
                    auto_focus: "inpTempTitleStep3",
                    convert_urls: false,
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor) {
                        editor.on('blur', function(e) {
                            $('#inpTitleStep3').html(tinymce.EditorManager.get('inpTempTitleStep3').getContent());
                            setNormal();
                        });
                    }
                });
            }
            clickTitleStep3 = true;
        });

        $('#inpDescriptionStep3').click(function(){
            //alert('step3');
            setNormal();
            $(this).hide();
            $('#tempContainerDescriptionStep3').show();
            $('#inpTempDescriptionStep3').html($('#inpDescriptionStep3').html());
            if (!clickDescriptionStep3) {
                tinymce.init({
                    //selector: "#inpTempTitleStep3",
                    selector: "#inpTempDescriptionStep3",
                    forced_root_block: "",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    auto_focus: "inpTempDescriptionStep3",
                    convert_urls: false,
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor){
                        editor.on('blur', function(e){
                            $('#inpDescriptionStep3').html(tinymce.EditorManager.get('inpTempDescriptionStep3').getContent());
                            setNormal();
                        });

                    }
                });
            }
            clickDescriptionStep3 = true;
        });
    }
    else
    if (stepId == "step4") {
        $('#inpTitleStep4').click(function(){
            setNormal();
            $(this).hide();
            $('#tempContainerTitleStep4').show();
            $('#inpTempTitleStep4').html($('#inpTitleStep4').html());
            if (!clickTitleStep4) {
                tinymce.init({
                    selector: "#inpTempTitleStep4",
                    forced_root_block: "",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    //selector: "#inpTempDescriptionStep4",
                    auto_focus: "inpTempTitleStep4",
                    convert_urls: false,
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor){
                        editor.on('blur', function(e){
                            $('#inpTitleStep4').html(tinymce.EditorManager.get('inpTempTitleStep4').getContent());
                            setNormal();
                        });
                    }
                });
            }
            clickTitleStep4 = true;
        });

        $('#inpDescriptionStep4').click(function(){
            //alert('step4');
            setNormal();
            $(this).hide();
            $('#tempContainerDescriptionStep4').show();
            $('#inpTempDescriptionStep4').html($('#inpDescriptionStep4').html());
            if (!clickDescriptionStep4) {
                tinymce.init({
                    //selector: "#inpTempTitleStep4",
                    forced_root_block: "",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    selector: "#inpTempDescriptionStep4",
                    auto_focus: "inpTempDescriptionStep4",
                    convert_urls: false,
					content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor"],
                    toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                    setup: function(editor){
                        editor.on('blur', function(e){
                            $('#inpDescriptionStep4').html(tinymce.EditorManager.get('inpTempDescriptionStep4').getContent());
                            setNormal();
                        });
                    }
                });
            }
            clickDescriptionStep4 = true;
        });
    }
    setNormal();
}

    //this function used to generate data of preference box
function InitPreferenceSteps(){
    //var tempData = '5.1,5.2,2,3,4,5.3';
    //alert('<cfoutput>#CppSetupData.STEPSETUP_VCH#</cfoutput>');
        if('<cfoutput>#CppSetupData.STEPSETUP_VCH#</cfoutput>'!=""){
    //if(tempData!=""){
var stepArr = ('<cfoutput>#CppSetupData.STEPSETUP_VCH#</cfoutput>').split(",");
    //var stepArr = tempData.split(",");
    var index =1;
    for(var i=stepArr.length;i>=0;i--){
        //alert(stepArr[i]);
        var divStepId = stepArr[i];
        //step1
        if(divStepId == 1){
            BuildStepData('step'+divStepId);
            $('select#inpIncludePreference').selectmenu({style:'popup',width:'70',menuWidth: '70'});
        }

        //step2
        if(divStepId == 2){
            BuildStepData('step'+divStepId);

            $('select#customValue').selectmenu({
                style: 'popup',
                width: '70',
                change: function() {
                    var customValue = $("#customValue").val();
                    if (customValue == 1) {
                        $('.preference-name-txt').hide();
                        $("input[name=preferenceValue]").show();
                    } else {
                        $('.preference-name-txt').show();
                        $("input[name=preferenceValue]").hide();
                    }
                },
                appendTo: '.contactpreferencespanRegoin'
            });
            $('select#inpMultiSelection').selectmenu({
                style: 'popup',
                width: '70',
                menuWidth: '70',
                appendTo: '.contactpreferencespanRegoin'
            });
            listPreference();
            $('select.preferenceDropdown').css('width','399px');
        }

        //step3
        if(divStepId == 3){
            BuildStepData('step'+divStepId);
        }
        //step4
        if(divStepId == 4){
            BuildStepData('step'+divStepId);
            $("select#inpType").dropdownchecklist({width: 180,maxDropHeight: 250, explicitClose: '[Close]', emptyText: "Please Select..."
			/*, textFormatFunction: function(options) {
                var selectedOptions = options.filter(":selected");
                var countOfSelected = selectedOptions.size();
                switch(countOfSelected) {
                    case 0: return "<i>Please Select...<i>";
                    case 1: return selectedOptions.text();
                    case options.size(): return "<b>All Language</b>";
                    default: return countOfSelected + " Languages";
                }
             }*/
            });
            $('select#inpLanguagePreference').selectmenu({
                style: 'popup',
                width: '70'
            });
        }
        //step5
        if(divStepId >= 5 && divStepId <6){
            countEditor+=1;
            BuildStepData('step'+divStepId);
            tinymce.init({
                selector: ".customHtml" + countEditor,
                convert_urls: false,
				content_css:"<cfoutput>#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css</cfoutput>",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor"
                ],
                toolbar: "forecolor backcolor | insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                editor_selector : "mceEditor",
                editor_deselector : "mceNoEditor"
            });

            $('a#tab2'+countEditor).click(function(e){
                var $target = $(e.currentTarget), targetId = $target.data('id');

                //alert(tinyMCE.activeEditor.getContent());
                // hide tab1
                $('#tab-html-'+targetId).hide();
                $('#tab-preview-'+targetId).show();

                var arialEditor = $("#custom-html"+targetId).find("textarea");
                var idTinyMCE = arialEditor.attr("id");

                $('#tab-preview-'+targetId).html(tinymce.EditorManager.get(idTinyMCE).getContent());
            });

            $('a#tab1'+countEditor).click(function(e){
                var $target = $(e.currentTarget), targetId = $target.data('id');
                //alert(tinyMCE.activeEditor.getContent());
                // hide tab1
                $('#tab-html-'+targetId).show();
                $('#tab-preview-'+targetId).hide();
            });
        }
        if(divStepId >= 6 && divStepId <7){
            BuildStepData('step'+divStepId,index);
            index ++;
        }
		
        if(divStepId == 8){
			<!---console.log('divStepId == 8');--->
            BuildStepData('step'+divStepId);
        }
    }
}
    AfterDrop();
}

    InitPreferenceSteps();

    $('#inptAddPreference').click(function(){
        if($('#customValue').is(':checked')){
            $('#preferenceValue').show();
        }else{
            //$('#preferenceValue').hide();
            $('#preferenceValue').show();
        }
        $('#addPreference').show();
        $('#inptAddPreference').hide();
    });

    $('#preferenceCancel').click(function(){
        $("#hideprefrence").hide();
        $("#addbu1").hide();
        $("#addp1").show();
        $('#preferenceValue').val('Type value in this box');
        $('#preferenceDesc').val('Type description in this box');
    });

        $('#preferenceSave').click(function(){
    var customPreferenceValue = '';
    if($('#preferenceDesc').val() == '' || $('#preferenceDesc').val() == 'Type description in this box'){
        jAlert('Please enter a preference before saving!', 'Error');
        return false;
    }
    if( $('#customValue').is(':checked') && ($('#preferenceValue').val() == '' || $('#preferenceValue').val() == 'Type value in this box')){
        jAlert('Preperence Value is required!', 'Error!');
        return false;
    }else{
        customPreferenceValue = $('#preferenceValue').val();
    }

    if($('#customValue').is(':checked') == false){
        customPreferenceValue ='';
    }

        $.ajax({
    type: "POST",
    url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=AddCppPreference&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    dataType: "json",
data: {
    inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
    inpPreferenceDescription : $('#preferenceDesc').val(),
    inpPreferenceValue: customPreferenceValue
},
success: function(d){
        if (d.ROWCOUNT > 0)
{

<!--- Check if variable is part of JSON result string --->
    if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
    {
        CurrRXResultCode = d.DATA.RXRESULTCODE[0];

        if(CurrRXResultCode > 0)
        {
            listPreference();
            $('#preferenceDesc').val('Type description in this box');
        }else{
            jAlert(d.DATA.MESSAGE[0], "Failure!");
        }
    }
}
}
});
});

    $('#AddMyOwn').click(function(){

        if(addTypeArr.length >= 9){
            jAlert('Cannot add more than 10 boxes!', "Failure!");
        }

        if($('#OwnType').val() == ''){
            jAlert('Please choose one Value Type', "Failure!");
            return false;
        }else{
            var addTypeItem = new Object();

            addTypeItem.type = $('#OwnType').val();

            var itemSize = 15;
            if($('#OwnSize').val() > 0){
                itemSize = $('#OwnSize').val();
            }
            addTypeItem.size = $('#OwnSize').val();
            addTypeArr.push(addTypeItem);
            addTypeHtml();
        }
    });

    $('#hyphens').click(function(){
        addTypeHtml();
    });

    $('#closeDialog').click(function(){
        addTypeArr = new Array();
        $('#myOwn_preview').html('');
        $('#OwnType').val('');
        $('#OwnSize').val(0);
    });

    $('#close_dialog').click(function(){
        addTypeArr = new Array();
        $('#myOwn_preview').html('');
        $('#OwnType').val('');
        $('#OwnSize').val(0);
    });


    $('#btnCreateMyOwn').click(function(){

        if($('#inpCreateMyOwn').val() == '' || $('#inpCreateMyOwn').val() == 'e.g., Cell Phone Number'){
            jAlert('Customer Identifier Name is required!', 'Error!');
            return false;
        }

        $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=AddCppIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json",
            data: {
                inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
                identifyGroupName : $('#inpCreateMyOwn').val(),
                cppIdentifyGroupStr : JSON.stringify(addTypeArr),
                hyphen: ($('#hyphens').is(':checked') ? 1 : 0)
            },
            success: function(d){
                if (d.ROWCOUNT > 0){

                <!--- Check if variable is part of JSON result string --->
                    if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
                    {
                        CurrRXResultCode = d.DATA.RXRESULTCODE[0];

                        if(CurrRXResultCode > 0)
                        {
                            addTypeArr = [];
                            $('#inpCreateMyOwn').val('e.g., Cell Phone Number');
                            $('#OwnType').val('');
                            $('#OwnSize').val(0);
                            identifyGroupHtml();
                            $('#myOwn_preview').html('');
                            CloseDialog('createMyOwn');
                        }else{
                            jAlert(d.DATA.MESSAGE[0], "Failure!");
                        }
                    }
                }
            }
        });
    });

    $('#inpSubmit').click(function(){
        var inpStepSetup = $('input#sortResult').val();
        var endStep = inpStepSetup.substr(inpStepSetup.length - 1);
        if(endStep==","){
            inpStepSetup = inpStepSetup.substr(0,inpStepSetup.length - 1);
        }
        var _totalTinyMCE=0;
        var inpStepSetupArray = inpStepSetup.split(",");

        for(j=0; j < inpStepSetupArray.length; j++){
            if(inpStepSetupArray[j]!="")
            {
                if(inpStepSetupArray[j].indexOf("5.")!=-1)
                {
                    _totalTinyMCE+=1;
                }
            }
        }
        if(!ValidateStep())return false;
        if(!ValidatePreferences())return false;

        //build steps order string
        var stepArray = $("#contentPreferences div.panel.panel-primary");//get all box in preferences box
        var preferenceStepOrder = "";//this var contains order of steps, after built, it should looks like '1,2,3,4' or '2,3,1,4', etc
        stepArray.each(function(index,value){
            preferenceStepOrder += value.getAttribute('id').replace("step","") + ((index != stepArray.length-1)?",":"");
        });
		
		<!--- Build preference list - create new groups as requested --->
        var groups = $('.group');
        var groupPreference = new Array();
        groups.each(function(index) {
			
			var thisgroupObj = $(this);
            var groupObj = {};
            groupObj.groupType = $(this).attr('updatetype'); // this for update, add or delete
            groupObj.preferenceId = $(this).find('input[name=preferenceId]').val();
            groupObj.preferenceValue = $(this).find('input[name=preferenceValue]').val();
            groupObj.preferenceDesc = $(this).find('input[name=preferenceDesc]').val();
            groupObj.groupId = $(this).find('select').val();

			<!--- Optiopn to add group on the fly - Group Name must be unique--->
			if(parseInt(groupObj.groupId) == 0)
			{
				<!---Create A new Group	--->
				$.ajax({
					type: "POST",
					url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=addgroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					dataType: "json",
					data: {
						inpGroupDesc: groupObj.preferenceDesc
					},
					success: function(d){
						if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];
								if(CurrRXResultCode > 0)
								{
									groupObj.groupId = d.DATA.INPGROUPID[0];
									thisgroupObj.find('input[name=preferenceValue]').val( d.DATA.INPGROUPID[0]);
								}
								else
								{
									jAlert(d.DATA.MESSAGE[0], "Failure!");
									
									if(thisgroupObj.val()==''||thisgroupObj.next("input").val()=='')
									{												
										var validateDiv = thisgroupObj.find("div[placeholder='validateDuplicate']");												
										validateDiv.removeClass("hide-div-validate");
										validateDiv.addClass("show-div-validate");
										result = false;												
										return false;
									}
								}
							}
						}
					}
				});
								
			}
				
            groupPreference.push(groupObj);
        });
		
		var inpIncludeIdentity = $('#contentPreferences #step1').length;
        var inpIncludeLanguage = $('#contentPreferences #step4').length;

        // get all content of tinyMCE block in cpp to push to a new array
        var content = new Array();

        // get all block step 5 in cpp
        var idArray = new Array();
        var actionArray = new Array();
        var allStep5 = $('.classStep5');
        var idTemp = '';

        allStep5.each(function(index) {
            idTemp = $(this).attr('id');
            var cusHtmlItem = {};
            if(typeof idTemp != "undefined"){
                if($(this).attr('id') == 'step5')
                    cusHtmlItem.id = "-1";
                else
                    cusHtmlItem.id = $(this).attr('id').substring(6, $(this).attr('id').length);
                cusHtmlItem.action = $(this).attr('action');
                if ($(this).parent().attr('id') == "contentPreferences") {
                    // get control id
                    var id = $(this).find('.tab-content textarea').attr('id');
                    // get content custom HTML
                    cusHtmlItem.content = htmlEscape(tinymce.EditorManager.get(id).getContent());
                }else{
                    cusHtmlItem.content = '';
                }
                content.push(cusHtmlItem);
            }
        });
		
		<!--- Read in current CDF data for storage --->
        var dataStep6 = {}
        var i = 1;
        $('.classStep6').each(function(){
			
		   <!---console.log($(this));--->
				
		   var tempObj = $(this).attr('id');
		   if(typeof(tempObj) != "undefined" && tempObj != ''){
		   	 	var data = {}
			   <!---console.log($(this).parent().parent());--->
	           if($(this).attr('id').substring(0, 4) == 'cdf-')
	                data['id'] = "-1";
	           else
	                data['id'] = $(this).attr('id').substring(6, $(this).attr('id').length);
	            data['action'] = $(this).attr('action');

				var tempJsonField = '{';
				var firstTime = 0;
				
                $(this).find('.listFields .cdf-define').each(function(){
                    if(firstTime != 0)
                        tempJsonField += ',';
                    tempJsonField += '"';
                    var idField = $(this).attr("rel");
                    if(idField == ""){
                        $(this).css('color','red');
                        $('html, body').animate({
                            scrollTop: $(this).offset().top - 100
                        }, 200);
                        dataStep6 = 'error';
                        return false;
                    }
                    tempJsonField += $(this).attr("rel");
                    tempJsonField += '":"';
                    tempJsonField += $(this).html();
                    tempJsonField += '"';
                    firstTime++;
                });
               if(dataStep6 == 'error'){
                   return false;
               }
                tempJsonField += "}";
                data['fields'] = jQuery.parseJSON(tempJsonField);
                <!---console.log(tempJsonField);--->
                data['title'] = "";
                data['description'] = htmlEscape($(this).find('.inpDescriptionStep6').html());
                dataStep6['group_'+i] = data;
                i++;

		   }
        });
		
        var titleStep2 = '';
        var descriptionStep2 = '';

        var titleStep3 = '';
        var descriptionStep3 = '';

        var titleStep4 = '';
        var descriptionStep4 = '';

        // get title and description of step 2,3,4
        if($('#inpTitleStep2').length > 0) {
            titleStep2 = htmlEscape($('#inpTitleStep2').html());
        }
        if($('#inpDescriptionStep2').length > 0) {
            descriptionStep2 = htmlEscape($('#inpDescriptionStep2').html());
        }
        if($('#inpTitleStep3').length > 0) {
            titleStep3 = htmlEscape($('#inpTitleStep3').html());
        }
        if($('#inpDescriptionStep3').length > 0) {
            descriptionStep3 = htmlEscape($('#inpDescriptionStep3').html());
        }
        if($('#inpTitleStep4').length > 0) {
            titleStep4 = htmlEscape($('#inpTitleStep4').html());
        }
        if($('#inpDescriptionStep4').length > 0) {
            descriptionStep4 = htmlEscape($('#inpDescriptionStep4').html());
        }
		var VerificationBatchId = "0";
		if(parseInt($('#inpBatchPickerIdA').val()) > 0) 
		{
            VerificationBatchId = $('#inpBatchPickerIdA').val();
        }
		if(dataStep6 == 'error'){
            return false;
        }
		
		
        if($('div#contentPreferences>div.classStep5>div.panel-body').children().length > 0 )
		{
			
			
            $.ajax({
                    type: "POST",
                    url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=UpdateCPPSETUP&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
                    dataType: "json",
                    data: {
                        inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
                        inpMultiplePreference : $('#inpMultiSelection').val(),
                        inpSetCustomValue: $('#customValue').val(),
                        inpIdentifyGroupId : $('[name=inpIdentifyGroupId]').val(),
                        inpUpdatePreference: $('#inpLogUpdate').is(':checked') ? 1 : 0,
                        inpVoiceMethod: $('#inpContactMethodVoice').is(':checked') ? 1 : 0,
                        inpSMSMethod: $('#inpContactMethodSMS').is(':checked') ? 1 : 0,
                        inpEmailMethod: $('#inpContactMethodEmail').is(':checked') ? 1 : 0,
                        inpIncludeLanguage: inpIncludeLanguage,
                        inpIncludeIdentity: inpIncludeIdentity,
                        inpStepSetup: inpStepSetup,
                        inpGroupPreference : JSON.stringify(groupPreference),
                        inpLanguage:$("#inpType").val()?JSON.stringify($("#inpType").val()):JSON.stringify(new Array()),
                        customHtml_txt : JSON.stringify(content),
                        currentStepSetup: currentStepSetup,
                        titleStep2: titleStep2,
                        descriptionStep2: descriptionStep2,
                        titleStep3: titleStep3,
                        descriptionStep3: descriptionStep3,
                        titleStep4: titleStep4,
                        descriptionStep4: descriptionStep4,
                        inpStep6: JSON.stringify(dataStep6),
                        inpVerificationBatchId : VerificationBatchId
                    },
                    success: function(d){
                        if (d.ROWCOUNT > 0){
                            <!--- Check if variable is part of JSON result string --->
                            if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
                            {
                                CurrRXResultCode = d.DATA.RXRESULTCODE[0];
                                if(CurrRXResultCode > 0){
                                    if(d.DATA.TYPE[0] == 2){
                                        $.alerts.okButton = '&nbsp;Continue&nbsp;';
                                        $.alerts.cancelButton = '&nbsp;Cancel&nbsp;';
                                        jConfirm(d.DATA.MESSAGE[0], "Not Just Yet",function(result){
                                            if(result){
                                                window.location.href ='<cfoutput>#nextUrl#</cfoutput>';
                                            }
                                        });
                                    }else{

                                    <cfoutput>
                                        var params = #params#;
                                            post_to_url('#nextUrl#', params, 'POST');
                                    </cfoutput>
                                    }

                                }else{
                                    jAlert(d.DATA.MESSAGE[0], "Failure!");
                                }
                            }
                        }
                    }
            });
        }
		else
		{
        $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=UpdateCPPSETUP&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json",
            data: {
                inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
                inpMultiplePreference : $('#inpMultiSelection').val(),
                inpSetCustomValue: $('#customValue').val(),
                inpIdentifyGroupId : $('[name=inpIdentifyGroupId]').val(),
                inpUpdatePreference: $('#inpLogUpdate').is(':checked') ? 1 : 0,
                inpVoiceMethod: $('#inpContactMethodVoice').is(':checked') ? 1 : 0,
                inpSMSMethod: $('#inpContactMethodSMS').is(':checked') ? 1 : 0,
                inpEmailMethod: $('#inpContactMethodEmail').is(':checked') ? 1 : 0,
                inpIncludeLanguage: inpIncludeLanguage,
                inpIncludeIdentity: inpIncludeIdentity,
                inpStepSetup: $('input#sortResult').val(),//$('#inpStepSetup').val(),
                inpGroupPreference : JSON.stringify(groupPreference),
                inpLanguage:$("#inpType").val()?JSON.stringify($("#inpType").val()):JSON.stringify(new Array()),
                customHtml_txt : '',
                currentStepSetup: currentStepSetup,
                titleStep2: titleStep2,
                descriptionStep2: descriptionStep2,
                titleStep3: titleStep3,
                descriptionStep3: descriptionStep3,
                titleStep4: titleStep4,
                descriptionStep4: descriptionStep4,
                inpStep6: JSON.stringify(dataStep6),
                inpVerificationBatchId : VerificationBatchId
            },
        success: function(d){
                if (d.ROWCOUNT > 0)
        {

        <!--- Check if variable is part of JSON result string --->
                if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
        {
            CurrRXResultCode = d.DATA.RXRESULTCODE[0];

                if(CurrRXResultCode > 0)
        {
                if(d.DATA.TYPE[0] == 2){
            $.alerts.okButton = '&nbsp;Continue&nbsp;';
            $.alerts.cancelButton = '&nbsp;Cancel&nbsp;';
                jConfirm(d.DATA.MESSAGE[0], "Not Just Yet",function(result){
                if(result){
                window.location.href ='<cfoutput>#nextUrl#</cfoutput>';
        }
        });
        }else{

        <cfoutput>
            var params = #params#;
                post_to_url('#nextUrl#', params, 'POST');
        </cfoutput>
        }

        }else{
            jAlert(d.DATA.MESSAGE[0], "Failure!");
        }
        }
        }
        }
        });
    }
    return false;
    });

    $("#inpBackUrl").click(function(){
        <cfoutput>
                var params = {CPPUUID:'1393378520'};
                    post_to_url('#backUrl#', params, 'POST');
        </cfoutput>
    });

    $("#hideimg").click(function(){
        $("#hideimg").hide();
        $("#hideimg1").show();
        $("#hideoption1").show();
        $("#hideoption").hide();
    });
    $("#hideimg1").click(function(){
        $("#hideimg").show();
        $("#hideimg1").hide();
        $("#hideoption1").hide();
        $("#hideoption").show();
    });

    $("#addp1").click(function(){
        $("#hideprefrence").show();
        $("#addbu1").show();
        $("#addp1").hide();
        $('#addp2').hide();
    });

    $("#addp2").click(function(){
        $("#hideprefrence").hide();
        $("#addbu1").hide();
        $("#addp1").show();
    });

    $("#inpLanguagePreference").click(function(){
        $("#languagepre").toggle();
    });

    var ddlanguge = new DropDown( $('#ddlanguge') );

    $('body').click(function(e){
        if ($(e.target).is('div.wrapper-dropdown-3')) {
            var isVisible = $(e.target).next().is(":visible");

            $(this).find($('.gDropDown')).hide();

            if (isVisible) {
                $(e.target).next().hide();
            }
            else {
                $(e.target).next().mouseover(function() {
                    $(".sortable").sortable("disable");
                });

                $(e.target).next().mouseout(function() {
                    $(".sortable").sortable("enable");
                });

                $(e.target).next().show();
            }
        } else {
            $(this).find($('.gDropDown')).hide();
        }
    });

    $(document).click(function() {
        // all dropdowns
        $('.wrapper-dropdown-3').removeClass('active');
    });

});
var addTypeArr = new Array();

listPreference();
identifyGroupHtml();
customValueCheckBoxStatus();
forcusBlur($('#preferenceValue'), 'Type value in this box');
forcusBlur($('#preferenceDesc'), 'Type description in this box');
forcusBlur($('#inpCreateMyOwn'), 'e.g., Cell Phone Number');

function addTypeHtml(){
    var typeItemHtml = '';
    for(index in addTypeArr){
        if( typeItemHtml != '' && $('#hyphens').is(':checked') ){
            typeItemHtml += ' - ' + ' <input type = "text" size ="15" maxlength ="' + addTypeArr[index].size + '" /> ';
        }else{
            typeItemHtml += ' <input type = "text" size ="15" maxlength ="' + addTypeArr[index].size + '" /> ';
        }
    }
    $('#myOwn_preview').html(typeItemHtml);
}

function identifyGroupHtml(){
        $.ajax({
    type: "POST",
    url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=GetIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    dataType: "json",
    data: {
        inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
    },
success: function(d){
        if (typeof d.DATA.DATA !== 'undefined') {
        if (typeof(d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT) == 'undefined') {
        buildportalControlHtml = '<span class="bold MultipleSelectText">Select Option</span>' +
        '<select name="inpIdentifyGroupId" id="inpIdentifyCustomer" class="MultipleSelectText">' +
        '<option value="-2" <cfif CppSetupData.IdentifyGroupId_int EQ -2>selected</cfif> > Billing Phone Number</option>' +
        '<option value="-1" <cfif CppSetupData.IdentifyGroupId_int EQ -1>selected</cfif> > Email Address</option>' +
        '</select>' +
        '<a href="#" class="underline" onclick="ShowDialog(\'createMyOwn\'); return false;">Create my own</a>';
}
else {
    var identifyHtml = '';

    for (iIdentify in d.DATA.IDENTIFYDATA[0].DATA.IDENTIFYID_INT) {
        if (d.DATA.DATA[0].DATA.HYPHEN_TI[0] == 1 && identifyHtml != '') {
            identifyHtml += ' - ' + ' <input type = "text" size ="15" /> ';
        }
        else {
            identifyHtml += ' <input type = "text" size ="15" /> ';
        }
    }
    buildportalControlHtml = '<span class="bold">' + d.DATA.DATA[0].DATA.GROUPNAME_VCH[0] + '</span>' +
            '<input type = "hidden" name="inpIdentifyGroupId" value ="' +
            d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0] +
            '" />' +
            identifyHtml +
            '<a href="#" class="underline" onclick="removeMyOwn(' +
            d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0] +
            '); return false;">Remove</a><br />';
}

    $('#buildportalControl').html(buildportalControlHtml);
    return false;
}else{
        if (typeof(d.DATA.IDENTIFYGROUPID_INT) == 'undefined') {
        buildportalControlHtml = '<span class="bold MultipleSelectText">Select Option</span>' +
        '<select name="inpIdentifyGroupId" id="inpIdentifyCustomer" class="MultipleSelectText">' +
        '<option value="-2" <cfif CppSetupData.IdentifyGroupId_int EQ -2>selected</cfif> > Billing Phone Number</option>' +
        '<option value="-1" <cfif CppSetupData.IdentifyGroupId_int EQ -1>selected</cfif> > Email Address</option>' +
        '</select>' +
        '<a href="#" class="underline" onclick="ShowDialog(\'createMyOwn\'); return false;">Create my own</a>';
}
else {
    var identifyHtml = '';

    for (iIdentify in d.DATA.IDENTIFYDATA[0].DATA.IDENTIFYID_INT) {
        if (d.DATA.DATA[0].DATA.HYPHEN_TI[0] == 1 && identifyHtml != '') {
            identifyHtml += ' - ' + ' <input type = "text" size ="15" /> ';
        }
        else {
            identifyHtml += ' <input type = "text" size ="15" /> ';
        }
    }
    buildportalControlHtml = '<span class="bold">' + d.DATA.DATA[0].DATA.GROUPNAME_VCH[0] + '</span>' +
            '<input type = "hidden" name="inpIdentifyGroupId" value ="' +
            d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0] +
            '" />' +
            identifyHtml +
            '<a href="#" class="underline" onclick="removeMyOwn(' +
            d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0] +
            '); return false;">Remove</a><br />';
}

    $('#buildportalControl').html(buildportalControlHtml);
    return false;
}
}
});
}

function removeMyOwn(groupId,CPPUID){
        $.ajax({
    type: "POST",
    url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=RemoveIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    dataType: "json",
    data: {
        inpIdentifyGroupId: groupId,
        inpCPPUUID: CPPUID
    },
success: function(d){
        if (d.ROWCOUNT > 0)
{

<!--- Check if variable is part of JSON result string --->
    if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
    {
        CurrRXResultCode = d.DATA.RXRESULTCODE[0];

        if(CurrRXResultCode > 0)
        {

            identifyGroupHtml();
        }else{
            jAlert(d.DATA.ERRMESSAGE[0], "Failure!");
        }
    }
}
}
});
}

function myDropdown(obj){
    //$(obj).next().toggle();
}


function onchangedSelect(obj,preId){
    return myDDSelect(obj,temp,preId);
}

function myDDSelect(obj,gId,preId){
    $(obj).parent().prev().text($(obj).text());
    $(obj).parent().prev().attr('rel',gId);
    $(obj).parent().hide();
    updatePreferenceContact(gId,preId);
    return false;
}

function buildGroupData(arrayGroup){
	
	<!---console.log('here a');--->
	
    var groupList = '<div class="wrapper-demo"><select class="preferenceDropdown">';
    if(arrayGroup.length > 0)
    {
        groupList = groupList + '<option value="0">Create a New Group...or Select from Below</option>';
        for(var index in arrayGroup){
            groupList = groupList + '<option value="' +arrayGroup[index].GROUPID+ '">' +arrayGroup[index].GROUPNAME+ '</option>';
        }
    }
    groupList = groupList + '</select></div>';
    return groupList;
}

function selectContact(arrContact, preItem, preArr){
    var contactSelectItem = '';
    var contactSelect = '<div class="wrapper-demo">';
    var contactSelected = '';

	contactSelectItem += '<option value="0">Create a New Group...</option>'

    for(indexContact in arrContact){

        var isInPreItems = false;
        var contactItem = arrContact[indexContact];

        if(contactItem.GROUPID == preItem.GROUPID_INT){
            contactSelectedStatus = true;
        } else{
            contactSelectedStatus = false;
        }

        if(!isInPreItems){
            if(contactSelectedStatus){
                contactSelectItem += '<option value="'+contactItem.GROUPID+'" selected="selected">' ;
                contactSelectItem += '<a href ="#">' +  contactItem.GROUPNAME + '</a>';
                contactSelectItem += '</option>';
            } else{
				contactSelectItem += '<option value="'+contactItem.GROUPID+'" >' ;
                contactSelectItem += '<a href ="#">' +  contactItem.GROUPNAME + '</a>';
                contactSelectItem += '</option>';
            }
        }

    }
    contactSelect =  contactSelect + '<div class="wrapper-dropdown-3" tabindex="1">' + contactSelected + '</div>'
            +'<select name="preferenceGroup" class="preferenceDropdown dropdown gDropDown active" id="preferenceGroup'+ preItem.PREFERENCEID_INT +'"  >'
            + contactSelectItem
            + '</select> </div> ';
    return contactSelect;

}

var arrayGroup = new Array();//this var is used to obtain groups
function listPreference(){
    $.ajax({
        type: "POST",
        url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=GetPreference&returnformat=plain&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
        dataType: "json",
        data: {
            inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
        },
        success: function(d){
        var listPreferenceHTML = '';
            if(d.ARR_PREFERENCE.length == 0){
        arrayGroup = d.ARR_CONTACT_GROUP;
        //if there is no contact group, show link to add contact group page
        if(d.ARR_CONTACT_GROUP.length == 0){
            listPreferenceHTML = "<div class = 'group show prefeItem'><a class='include-text' href='<cfoutput>#rootUrl#/#SessionPath#/contacts/addgroup</cfoutput>'>Add a contact group</a></div>";
        $('#listPreference').html(listPreferenceHTML);
        }else{
            // add blank value
            var inputValue = '<input name="preferenceValue" placeholder="Value" class="filter_val preference-name-txt" type="text" size="25">';
                if(<cfoutput>#CppSetupData.SETCUSTOMVALUE_TI#</cfoutput> == 0){
            inputValue = '<input name="preferenceValue" style="display:none;" placeholder="Value" class="filter_val preference-name-txt" type="text" size="25">';
            }
            listPreferenceHTML +=   '<div class = "group show prefeItem" updateType="add">'
                    + inputValue
                    + '<input name="preferenceDesc" placeholder="Description" type="text" class="filter_val preference-description-txt" value="" size="25">'
                    + '<input type="hidden" value="-1" name="preferenceId">'
                    + buildGroupData(arrayGroup)
                    + '<a href = "#" class ="preferenceAdd preferenceUpDown img16_16" onclick="preferenceAdd(); return false;">Add Preference</a>'
                    + '<span class="preferenceDelete hide">-</span>'
                    + '<a href = "#" class ="preferenceDelete hide img16_16" onclick="preferenceDelete(this); return false;">Remove Preference</a>'
                    + '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value or description can not be blank</span></div>'
                    + '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
                    + '<div placeholder="validateNotSelect" class="hide-div-validate"><span style="color:red;">Please select a group</span></div>'
                    + '</div>';
        }
        } else{
            arrayGroup = d.ARR_CONTACT_GROUP;
                for(index in d.ARR_PREFERENCE){
            var preItem =  d.ARR_PREFERENCE[index];
            var listPreferenceDown ='';
            var listPreferenceUp ='';

            if(index < d.ARR_PREFERENCE.length - 1){
                listPreferenceDown = 'preferenceDown';
            }
            if(index > 0){
                listPreferenceUp = 'preferenceUp';
            }

            var inputValue = '<input name="preferenceValue" placeholder="Value" type="text" id="preferenceValue' + preItem.VALUE_VCH + '" class="filter_val preference-name-txt" value="' + preItem.VALUE_VCH + '" size="25">';
                if(<cfoutput>#CppSetupData.SETCUSTOMVALUE_TI#</cfoutput> == 0){
            inputValue = '<input name="preferenceValue" style="display:none;" placeholder="Value" type="text" id="preferenceValue' + preItem.VALUE_VCH + '" class="filter_val preference-name-txt" value="' + preItem.VALUE_VCH + '" size="25">';
        }
            //if there is no contact group, show link to add contact group page
                if(d.ARR_CONTACT_GROUP.length ==0){
                listPreferenceHTML = "<div class = 'group show prefeItem'><a class='include-text' href='<cfoutput>#rootUrl#/#SessionPath#/contacts/addgroup</cfoutput>'>Add a contact group</a></div>";
            $('#listPreference').html(listPreferenceHTML);
        }else{
            // use update type flag to update when submit
            listPreferenceHTML +=   '<div class = "group show prefeItem" updateType="update">'
                    + inputValue
                    + '<input name="preferenceDesc" placeholder="Description" type="text" id="preferenceDesc' + preItem.DESC_VCH + '" class="filter_val preference-description-txt" value="' + preItem.DESC_VCH + '" size="25">'
                    + '<input type="hidden" value="'+preItem.PREFERENCEID_INT+'" name="preferenceId">'
                    + selectContact(d.ARR_CONTACT_GROUP,preItem,d.ARR_PREFERENCE)
                    + '<a href = "#" class ="preferenceAdd preferenceUpDown img16_16" onclick="preferenceAdd('+preItem.PREFERENCEID_INT+'); return false;">Add Preference</a>'
                    + '<span class="hide">-</span>'
                    + '<a href = "#" class ="preferenceDelete hide img16_16" onclick="preferenceDelete(this); return false;">Remove Preference</a>'
                    + '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value or description can not be blank</span></div>'
                    + '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
                    + '<div placeholder="validateNotSelect" class="hide-div-validate"><span style="color:red;">Please select a group</span></div>'
                    + '</div>';
        }

        }
    }

        $('#listPreference').html(listPreferenceHTML);
        //hide button Delete preference if there is less than or equal 1 group
        if ($("#listPreference").find("div.group.show").size() > 1) {
            $("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("hide");
            $("#listPreference").find("div.group.show>a.preferenceDelete").addClass("show");
            $("#listPreference").find("div.group.show>span").removeClass("hide");
            $("#listPreference").find("div.group.show>span").addClass("show");
        }
        $('input#inpContactMethodVoice').custominput();
        $('input#inpContactMethodSMS').custominput();
        $('input#inpContactMethodEmail').custominput();

        $('select.preferenceDropdown').selectmenu({
            style:'popup',
            width:'148',
            menuWidth: '148',
            appendTo: '#listPreference .wrapper-demo'
        });
    }
});
}

function customValueCheckBoxStatus(){
    if($('#customValue').is(':checked')){
        $('#listPreference').hide();
        $('#preferenceValue').show();
        $('.input-txt').width('158px');
    }else{
        $('#listPreference').show();
        $('#preferenceValue').show();
        $('.input-txt').width('158px');
    }
}

function preferenceAdd(){
	
	
    if(arrayGroup.length > 0)
    {
        var groupList = '<div class="wrapper-demo"><select class="preferenceDropdown">';
        groupList = groupList + '<option value="0">Create a New Group...or Select from Below</option>';
        for(var index in arrayGroup){
            groupList = groupList + '<option value="' +arrayGroup[index].GROUPID+ '">' +arrayGroup[index].GROUPNAME+ '</option>';
        }
        groupList = groupList + '</select></div>';

        var inputValue = '<input name="preferenceValue" placeholder="Value" type="text" class="filter_val preference-name-txt" value="" size="25">';
        if($('#customValue').val() == 0){
            inputValue = '<input name="preferenceValue" style="display:none" placeholder="Value" type="text" class="filter_val preference-name-txt" value="" size="25">';
        }
        // blank value
        var blankPreference =   '<div class="group show prefeItem" updateType="add">'
                + inputValue
                + '<input name="preferenceDesc" placeholder="Description"  type="text" id="preferenceDesc" class="filter_val preference-description-txt" value="" size="25">'
                + '<input name="preferenceId" type="hidden" value="-1" >'
                + groupList
                + '<a href="#" onclick="preferenceAdd(this); return false;" class ="preferenceAdd preferenceUpDown img16_16">Add Preference</a>'
                + '<span>-</span>'
                + '<a href="#" onclick="preferenceDelete(this); return false;" class ="preferenceDelete show img16_16">Remove Preference</a>'
                + '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value or description can not be blank</span></div>'
                + '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
                + '<div placeholder="validateNotSelect" class="hide-div-validate"><span style="color:red;">Please select a group</span></div>'
                + '</div>';

        $('#listPreference').append(blankPreference);
        //show button Delete preference if there is more than 2 groups
        if ($("#listPreference").find("div.group.show").size() > 1) {
            $("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("hide");
            $("#listPreference").find("div.group.show>a.preferenceDelete").addClass("show");
            $("#listPreference").find("div.group.show>span").removeClass("hide");
            $("#listPreference").find("div.group.show>span").addClass("show");
        }
        $('select.preferenceDropdown').selectmenu({style:'popup',width:'148',menuWidth: '148'});
    }
}

function preferenceDelete(preObject){
    // update preference type
    var preferenceBox = $(preObject).parent();
    if(preferenceBox.attr('updateType') != 'add'){
        preferenceBox.attr('updateType', 'delete');
        preferenceBox.removeClass("show");
        preferenceBox.addClass("hide");
    } else{
        preferenceBox.next("div[placeholder='validate']").remove();
        preferenceBox.next("div[placeholder='validateDuplicate']").remove();
        preferenceBox.next("div[placeholder='validateNotSelect']").remove();
        preferenceBox.remove();
    }

    if ($("#listPreference").find("div.group.show").size() <= 1) {
        $("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("show");
        $("#listPreference").find("div.group.show>a.preferenceDelete").addClass("hide");
        $("#listPreference").find("div.group.show>span").removeClass("show");
        $("#listPreference").find("div.group.show>span").addClass("hide");
        return false;
    }
}

function preferenceUpDown(preferenceId, preferenceType, withValue){
    if(
        ($('#preferenceDesc').val() != '' && $('#preferenceDesc').val() != 'Type description in this box')
        || ($('#preferenceValue').val() != '' && $('#preferenceValue').val() != 'Type value in this box')
        ){
        return false;
    }
    $.ajax({
        type: "POST",
        url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=preferenceUpDown&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
        dataType: "json",
        data: {
            preferenceId: preferenceId,
            preferenceType: preferenceType,
            widthValue: withValue
        },
        success: function(d){
            if (d.ROWCOUNT > 0){
                <!--- Check if variable is part of JSON result string --->
                if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){
                    CurrRXResultCode = d.DATA.RXRESULTCODE[0];
                    if(CurrRXResultCode > 0)
                    {
                        listPreference();
                    }else{
                        //jAlert(d.DATA.ERRMESSAGE[0], "Failure!");
                    }
                 }
            }
        }
    });
}

function DropDown(el) {
    this.ddlanguge = el;
    this.placeholder = this.ddlanguge.children('span');
    this.opts = this.ddlanguge.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}

function ValidateStep(){
    var existDesc = false;
    //Preferences box must contain 2 modules: Contact preferences and Contact methods
    var step2 = $("#contentPreferences #step2").length>0;//this is step 2 - Contact preferences
    var step3 = $("#contentPreferences #step3").length>0;//this is step 3 - Contact methods
    if(step2&&step3) return true;//if exist, return true

    //if false, alert error message
    jAlert("Preferences must contain Contact preferences and Contact methods. Please insert!", "Failure!");
    return false;
}

function ValidatePreferences(){
    var result = true;
    //reset all validate box
    $("div[placeholder='validate']").each(function(){
        $(this).removeClass("show-div-validate");
        $(this).addClass("hide-div-validate");
    });

    $("div[placeholder='validateDuplicate']").each(function(){
        $(this).removeClass("show-div-validate");
        $(this).addClass("hide-div-validate");
    });

    $("div[placeholder='validateNotSelect']").each(function(){
        $(this).removeClass("show-div-validate");
        $(this).addClass("hide-div-validate");
    });

    var inpVoiceMethod = $('#inpContactMethodVoice').is(':checked') ? 1 : 0;
    var inpSMSMethod = $('#inpContactMethodSMS').is(':checked') ? 1 : 0;
    var inpEmailMethod = $('#inpContactMethodEmail').is(':checked') ? 1 : 0;

    //One method must be chosen, if not show err box
    if(inpVoiceMethod+inpSMSMethod+inpEmailMethod <1){
        $('#ValidateContactMethod').removeClass("hide-div-validate");
        $('#ValidateContactMethod').addClass("show-div-validate");
        $('#ValidateContactMethod').attr("tabindex",-1).focus();
        result = false;
        return result;
    }

    //all description field should not be blank
    $("input[placeholder='Description']").each(function(index){
        var validateDiv = $(this).nextAll("div[placeholder='validate']:first");
        //check if div box parent is not classified as delete
        if(validateDiv.parent().attr("updateType")=="delete") return;
        //if description field is empty, show err mess
        if($(this).val()==''||$(this).next("input").val()==''){
            validateDiv.removeClass("hide-div-validate");
            validateDiv.addClass("show-div-validate");
            result = false;
            return false;
        }
        else{
            validateDiv.removeClass("show-div-validate");
            validateDiv.addClass("hide-div-validate");
        }
    });
    //if validate blank of description not pass, no need to validate duplicate, so return
    if(!result) return result;

    //if "set custom values for each preference" is yes, validate
    if($("a#customValue-button >span.ui-selectmenu-status").text() =='Yes'){
        $("input[placeholder='Value']").each(function(index){
            var validateDiv = $(this).nextAll("div[placeholder='validate']:first");
            //check if div box parent is not classified as delete
            if(validateDiv.parent().attr("updateType")=="delete") return;
			
			// if(validateDiv.parent().attr("updateType")=="delete") return;
			
            //if value field or description empty, show err mess
            if($(this).val()==''||$(this).next("input").val()==''){
                validateDiv.removeClass("hide-div-validate");
                validateDiv.addClass("show-div-validate");
                result = false;
                return false;
            }
            else{
                validateDiv.removeClass("show-div-validate");
                validateDiv.addClass("hide-div-validate");
            }
        });
    }
    //if validate blank not pass, no need to validate duplicate, so return
    if(!result) return result;

    //validate duplicate
    $("#listPreference select").each(function (i, element) {
        if($(element).parent().parent().attr('updatetype') == 'delete') return;
        
		
		var selectedVal = $(element).val();
        
		<!---console.log($(element).val());--->
		
		//alert(selectedVal);
        if(selectedVal == -1){
            validateNotSelect = $(element).parent().nextAll("div[placeholder='validateNotSelect']:first");
            validateNotSelect.removeClass("hide-div-validate");
            validateNotSelect.addClass("show-div-validate");
            result = false;
        }
        $("#listPreference select").each(function (j, subElement) {
			
			
            if (i!=j && selectedVal == $(subElement).val() && parseInt($(subElement).val()) != 0) {
                if($(subElement).parent().parent().attr('updatetype') == 'delete') return;
				
				
				
                var firstDiv = $(element).parent().nextAll("div[placeholder='validateDuplicate']:first");
                var secondDiv = $(subElement).parent().nextAll("div[placeholder='validateDuplicate']:first");
                firstDiv.removeClass("hide-div-validate");
                firstDiv.addClass("show-div-validate");
                secondDiv.removeClass("hide-div-validate");
                secondDiv.addClass("show-div-validate");
                result = false;
                return false;
            }
        });
        if(!result)return false;
    });
    return result;
}

function UpdateReferenceContent(id,value,description){
    // check value is not null
    // check description is not null
    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=UpdateReferenceContent&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json",
        data: {
            inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
            inpPreferenceId: id,
            inpPreferenceDescription : description,
            inpPreferenceValue: value
        },
        success: function(d){
            if (d.ROWCOUNT > 0){
                <!--- Check if variable is part of JSON result string --->
                if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
                {
                    CurrRXResultCode = d.DATA.RXRESULTCODE[0];
                    if(CurrRXResultCode > 0)
                    {
                        //listPreference();
                    }else{
                        //jAlert(d.DATA.ERRMESSAGE[0], "Failure!");
                    }
                }
            }
        }
    });
}

DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.ddlanguge.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}

window.onload=function(){
    $('select.preferenceDropdown').selectmenu({style:'popup',width:'148',menuWidth: '148'});
};

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}

function htmlDecodeMultiEncode(value){
	if (htmlDecode(value)) {
        return $('<div />').html(htmlDecode(value)).text();
    } else {
        return '';
    }
}

function AfterDrop(){
    $('.delete-html-module').click(function(){
        var objectClicked = $(this);
        jConfirm("Are you sure to delete this section?", "Confirm!", function(result) {
            if(result)
			{
				<!--- Allow module to be removed --->
                if(objectClicked.parent().parent().attr('id') == "step1" || objectClicked.parent().parent().attr('id') == "step4" || objectClicked.parent().parent().attr('id') == "step8")
				{
                    $('#contentModules').append(objectClicked.parent().parent());
                    objectClicked.parent().parent().find('.panel-body').html("");
                    objectClicked.parent().parent().find('.panel-body').addClass("displayNone");
                    objectClicked.parent().parent().find('.panel-heading>.delete-html-module').remove();
                }
				else
				{
                    $('#contentModules').append(objectClicked.parent().parent());
                    objectClicked.parent().parent().attr('action','delete');
                    objectClicked.parent().parent().addClass('displayNone');
                }
                
				objectClicked.parent().parent().remove();
                // update hindden field
                var sortResult = '';
                var groups = $('#contentPreferences').children();
                groups.each(function(index){
                    if ($(this).attr('id').length == 1) {
                        sortResult += $(this).attr('id') + ',';
                    }
                    else
                    if ($(this).attr('id') == 'drop_to') {

                    }
                    else {
                        if ($(this).attr('id').substring(0, 4) == 'cdf-')
                            var idStep = '6';
                        else {
                            var idStep = $(this).attr('id').substring(4, $(this).attr('id').length);
                        }

                        sortResult += idStep + ',';
                    }

                });

                $('input#sortResult').attr('value', sortResult);
            }
        });
    });
}

//this function set hide, show of title and description of step 2,3,4
function setNormal(){

    //tinymce.remove('.tempTitle');
    //tinymce.remove('.tempDescription');

    if($('#inpTitleStep2').length > 0){
        $('#inpTitleStep2').show();
        $('#tempContainerTitleStep2').hide();
    }
    if($('#inpDescriptionStep2').length > 0){
        $('#inpDescriptionStep2').show();
        $('#tempContainerDescriptionStep2').hide();
    }

    if($('#inpTitleStep3').length > 0){
        $('#inpTitleStep3').show();
        $('#tempContainerTitleStep3').hide();
    }
    if($('#inpDescriptionStep3').length > 0){
        $('#inpDescriptionStep3').show();
        $('#tempContainerDescriptionStep3').hide();
    }

    if($('#inpTitleStep4').length > 0){
        $('#inpTitleStep4').show();
        $('#tempContainerTitleStep4').hide();
    }
    if($('#inpDescriptionStep4').length > 0){
        $('#inpDescriptionStep4').show();
        $('#tempContainerDescriptionStep4').hide();
    }
    if($('.inpTitleStep6').length > 0){
        $('.inpTitleStep6').show();
        $('.tempContainerTitleStep6').hide();
    }
    if($('.inpDescriptionStep6').length > 0){
        $('.inpDescriptionStep6').show();
        $('.tempContainerDescriptionStep6').hide();
    }
}
	
	
	
	<!--- Cant find the end in the mess above - so for now I just started a new one --->
	$(document).ready(function(){
	
		<!--- Open a Campaign picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Campaign picker if its still there --->
		$(".BatchPicker").click( function() 
		{			
											
			if($dialogCampaignPicker == null)
			{		
				<!--- Load content into a picker dialog--->			
				$dialogCampaignPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 1200,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Campaign Picker',
					draggable: true,
					resizable: true
				}).load('../../../campaign/act_campaignpicker');
			}
					
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
			var y = $(this).position().top + 30 - $(document).scrollTop();
			$dialogCampaignPicker.dialog('option', 'position', [x,y]);			
	
			$TargetBatchDesc = $("#" + $(this).attr('rel1') ); 
			$TargetBatchId =  $("#" + $(this).attr('rel2') ); 
		
			$dialogCampaignPicker.dialog('open');
		}); 		
	
	});


</script>