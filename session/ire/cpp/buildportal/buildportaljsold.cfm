<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/custominput.jquery.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.core.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.position.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.widget.js"></script>
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/jquery-ui1.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/style5.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/list.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.core.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.theme.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css" />
	
	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/js/jmutiselect/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/js/jmutiselect/style.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/js/jmutiselect/jquery-ui.css" />
	
	<!---<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jmutiselect/jquery.multiselect.js"></script>--->
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/dropdownchecklist/ui.dropdownchecklist-1.4-min.js"></script>
		
	
</cfoutput>

<script type="text/javascript">
	$(document).ready(function(){		
		$("#inpType").dropdownchecklist({width: 385,maxDropHeight: 250, explicitClose: '[Close]', emptyText: "Please Select..."/*, textFormatFunction: function(options) {
                var selectedOptions = options.filter(":selected");
                var countOfSelected = selectedOptions.size();
                switch(countOfSelected) {
                    case 0: return "<i>Please Select...<i>";
                    case 1: return selectedOptions.text();
                    case options.size(): return "<b>All Language</b>";
                    default: return countOfSelected + " Languages";
                }
             }*/ 
             });
		//$('#selectlistLang input').custominput();
		$('select#inpLanguagePreference').selectmenu({style:'popup',width:'70'});	
		$('select#customValue').selectmenu({style:'popup',width:'70'});	
		$('select#inpIncludePreference').selectmenu({style:'popup',width:'70',menuWidth: '70'});		
		$('select#inpMultiSelection').selectmenu({style:'popup',width:'70',menuWidth: '70'});
		$('ul#inpLanguagePreference-menu').css('width', '70px');
		$('ul#inpType-menu').css('width','399px');
		$('ul#customValue-menu').css('width','70px');
						
		$('.voice input').custominput();
		$('#subTitleText').text('<cfoutput>#Cpp_Title# >> #Cpp_Build_Portal_Title# </cfoutput>');
		$('#mainTitleText').text('ire');
		
		$("#btnCreateGroupContact").click(function(){
			var groupName = $("#inpGroupContactName").val();
			if(groupName == ""){
				$(".groupRequire").show();
			}else{
				$(".groupRequire").hide();
			 	$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=addgroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:{
		    			inpGroupDesc: groupName
					},
		            dataType: "json", 
		            success: function(d) {
		            	if(d.DATA.RXRESULTCODE[0] > 0){
	            			CloseDialog('createGroupContact');
	            		 	listPreference();
		            	}else{
		            		jAlert(d.DATA.MESSAGE[0],"Create Group contact fail." );
		            	}
			        }
			    });
			}
		});
		
	 	$("#sortable").sortable({
	 		start: function(event, ui) {
	 			$('#step'+(ui.item.index() + 1)).find('.buildportalBox').css('border-style','dotted');
	 		},
		    stop: function(event, ui) {
		    	var stepPoistition = '';
		    	$('ul#sortable').find('li.getSortableStep').each(function(index){
		    		//$(this).attr('alt') find('span.step').text(index+2);
		    		if(stepPoistition == '' ){
		    			stepPoistition = $(this).attr('alt');
		    		}else{
		    			stepPoistition += ','+$(this).attr('alt') ;
		    		}
		    		$(this).attr('id', 'step'+(index + 1) );
		    	});
		    	$('#inpStepSetup').val(stepPoistition);
		    	$('.buildportalBox').css('border-style','solid');
		    	
		    }
		});

		$('#inptAddPreference').click(function(){
			if($('#customValue').is(':checked')){
				$('#preferenceValue').show();
			}else{
				//$('#preferenceValue').hide();
				$('#preferenceValue').show();
			}
			$('#addPreference').show();
			$('#inptAddPreference').hide();
		});
		
		$('#preferenceCancel').click(function(){
			$("#hideprefrence").hide();
			$("#addbu1").hide();
			$("#addp1").show();
			$('#preferenceValue').val('Type value in this box');
			$('#preferenceDesc').val('Type description in this box');
		});
		
		$('#preferenceSave').click(function(){
			var customPreferenceValue = '';
			if($('#preferenceDesc').val() == '' || $('#preferenceDesc').val() == 'Type description in this box'){
				jAlert('Please enter a preference before saving!', 'Error');
				return false;
			}
			if( $('#customValue').is(':checked') && ($('#preferenceValue').val() == '' || $('#preferenceValue').val() == 'Type value in this box')){
				jAlert('Preperence Value is required!', 'Error!');
				return false;
			}else{
				customPreferenceValue = $('#preferenceValue').val();
			}
			
			if($('#customValue').is(':checked') == false){
				customPreferenceValue ='';
			}
			
			$.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=AddCppPreference&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	            dataType: "json", 
	            data: {
	            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
	            	inpPreferenceDescription : $('#preferenceDesc').val(),
	            	inpPreferenceValue: customPreferenceValue
	            },
	            success: function(d){
	            	if (d.ROWCOUNT > 0) 
					{													
					
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
	            				listPreference();
	            				$('#preferenceDesc').val('Type description in this box');
	            			}else{
								jAlert(d.DATA.MESSAGE[0], "Failure!");							
							}
	            		}
	            	}
	            }
     		});
		});
		
		$('#AddMyOwn').click(function(){
			
			if(addTypeArr.length >= 9){
				jAlert('Cannot add more than 10 boxes!', "Failure!");
			}
			
			if($('#OwnType').val() == ''){
				jAlert('Please choose one Value Type', "Failure!");
				return false;
			}else{
				var addTypeItem = new Object();
				
				addTypeItem.type = $('#OwnType').val();
				
				var itemSize = 15;
				if($('#OwnSize').val() > 0){
					itemSize = $('#OwnSize').val();
				}
				addTypeItem.size = $('#OwnSize').val();
				addTypeArr.push(addTypeItem);
				addTypeHtml();
			}
		});
		
		$('#hyphens').click(function(){
			addTypeHtml();
		});
		
		$('#closeDialog').click(function(){
			addTypeArr = new Array();
			$('#myOwn_preview').html('');
			$('#OwnType').val('');
			$('#OwnSize').val(0);
		});
		
		$('#close_dialog').click(function(){
			addTypeArr = new Array();
			$('#myOwn_preview').html('');
			$('#OwnType').val('');
			$('#OwnSize').val(0);
		});
		
		$('#customValue').change(function(){
  			var temp = this.options[this.selectedIndex].value;
			if (temp == 1) {
				$('.preference-name-txt').hide();
				$("input[name=preferenceValue]").show();
			} else {
				$('.preference-name-txt').show();
				$("input[name=preferenceValue]").hide();
			}		
		});
		
		$('#btnCreateMyOwn').click(function(){
			
			if($('#inpCreateMyOwn').val() == '' || $('#inpCreateMyOwn').val() == 'e.g., Cell Phone Number'){
				jAlert('Customer Identifier Name is required!', 'Error!');
				return false;
			}
			
			$.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=AddCppIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	            dataType: "json", 
	            data: {
	            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
	            	identifyGroupName : $('#inpCreateMyOwn').val(),
	            	cppIdentifyGroupStr : JSON.stringify(addTypeArr),
	            	hyphen: ($('#hyphens').is(':checked') ? 1 : 0)
	            },
	            success: function(d){
	            	if (d.ROWCOUNT > 0) 
					{													
					
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
	            				addTypeArr = [];
	            				$('#inpCreateMyOwn').val('e.g., Cell Phone Number');
	            				$('#OwnType').val('');
	            				$('#OwnSize').val(0);
	            				identifyGroupHtml();
	            				$('#myOwn_preview').html('');
	            				CloseDialog('createMyOwn');
	            			}else{
								jAlert(d.DATA.MESSAGE[0], "Failure!");							
							}
	            		}
	            	}
	            }
     		});
		});
		
		$('#inpSubmit').click(function(){
			if(!ValidatePreferences())return false;
			if(!ValidateLanguage())return false;
			var groups = $('.group');
			var groupPreference = new Array();
			groups.each(function(index) {
				var groupObj = {};
				groupObj.groupType = $(this).attr('updatetype'); // this for update, add or delete
				groupObj.preferenceId = $(this).find('input[name=preferenceId]').val();
				groupObj.preferenceValue = $(this).find('input[name=preferenceValue]').val();
				groupObj.preferenceDesc = $(this).find('input[name=preferenceDesc]').val();
				groupObj.groupId = $(this).find('select').val();
				
				groupPreference.push(groupObj);
			});
			 $.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=UpdateCPPSETUP&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	            dataType: "json", 
	            data: {
	            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
	            	inpMultiplePreference : $('#inpMultiSelection').val(),
	            	inpSetCustomValue: $('#customValue').val(),
	            	inpIdentifyGroupId : $('[name=inpIdentifyGroupId]').val(),
	            	inpUpdatePreference: $('#inpLogUpdate').is(':checked') ? 1 : 0,
	            	inpVoiceMethod: $('#inpContactMethodVoice').is(':checked') ? 1 : 0,
	            	inpSMSMethod: $('#inpContactMethodSMS').is(':checked') ? 1 : 0,
	            	inpEmailMethod: $('#inpContactMethodEmail').is(':checked') ? 1 : 0,
	            	inpIncludeLanguage: $('#inpLanguagePreference').val(),
	            	inpIncludeIdentity:$('#inpIncludePreference').val(),
	            	inpStepSetup: $('#inpStepSetup').val(),
					inpGroupPreference : JSON.stringify(groupPreference),
					inpLanguage:$("#inpType").val()?JSON.stringify($("#inpType").val()):JSON.stringify(new Array())
	            },
	            success: function(d){
	            	if (d.ROWCOUNT > 0) 
					{													
					
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								if(d.DATA.TYPE[0] == 2){
									$.alerts.okButton = '&nbsp;Continue&nbsp;';
									$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';
									jConfirm(d.DATA.MESSAGE[0], "Not Just Yet",function(result){
										if(result){
											window.location.href ='<cfoutput>#nextUrl#</cfoutput>';
										}
									});
								}else{
									
									<cfoutput>
										var params = #params#;
										post_to_url('#nextUrl#', params, 'POST');
									</cfoutput>
								}
	            				
	            			}else{
								jAlert(d.DATA.MESSAGE[0], "Failure!");							
							}
	            		}
	            	}
	            }
			});
			return false;
		});
		
		$("#inpBackUrl").click(function(){
			<cfoutput>
				var params = {CPPUUID:'1393378520'};
				post_to_url('#backUrl#', params, 'POST');
			</cfoutput>
		});
		
		$("#hideimg").click(function(){
    		$("#hideimg").hide();
			$("#hideimg1").show();
			$("#hideoption1").show();
			$("#hideoption").hide();
  		});
		$("#hideimg1").click(function(){
    		$("#hideimg").show();
			$("#hideimg1").hide();
			$("#hideoption1").hide();
		 	$("#hideoption").show();
		});
		
		$("#addp1").click(function(){
		    $("#hideprefrence").show();
			$("#addbu1").show();
			$("#addp1").hide();
			$('#addp2').hide();
	 	});
	
		$("#addp2").click(function(){
		    $("#hideprefrence").hide();
			$("#addbu1").hide();
			$("#addp1").show();
		});
		
		$("#inpLanguagePreference").click(function(){
		    $("#languagepre").toggle();
  		});
  		
  		var ddlanguge = new DropDown( $('#ddlanguge') );
		
		$('body').click(function(e){
			if ($(e.target).is('div.wrapper-dropdown-3')) {
				var isVisible = $(e.target).next().is(":visible");
				
				$(this).find($('.gDropDown')).hide();

				if (isVisible) {
					$(e.target).next().hide();				
				}
				else {
					$(e.target).next().mouseover(function() {
						$("#sortable").sortable("disable");
					});
					
					$(e.target).next().mouseout(function() {
						$("#sortable").sortable("enable");
					});
					
			        $(e.target).next().show();
				}
		    } else {
		        $(this).find($('.gDropDown')).hide();
		    }
		});
		
		$(document).click(function() {
			// all dropdowns
			$('.wrapper-dropdown-3').removeClass('active');
		});
		
	});
	var addTypeArr = new Array();
	
	listPreference();
	identifyGroupHtml();
	customValueCheckBoxStatus();
	forcusBlur($('#preferenceValue'), 'Type value in this box');
	forcusBlur($('#preferenceDesc'), 'Type description in this box');
	forcusBlur($('#inpCreateMyOwn'), 'e.g., Cell Phone Number');
	
	function addTypeHtml(){
		var typeItemHtml = '';
		for(index in addTypeArr){
			if( typeItemHtml != '' && $('#hyphens').is(':checked') ){
				typeItemHtml += ' - ' + ' <input type = "text" size ="15" maxlength ="' + addTypeArr[index].size + '" /> ';
			}else{
				typeItemHtml += ' <input type = "text" size ="15" maxlength ="' + addTypeArr[index].size + '" /> ';
			}
		}
		$('#myOwn_preview').html(typeItemHtml);
	}
	
	function identifyGroupHtml(){
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=GetIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
            },
            success: function(d){
            	if(typeof(d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0]) == 'undefined'){
            		buildportalControlHtml = '<span class="bold MultipleSelectText">Select Option</span>'
						+ '<select name="inpIdentifyGroupId" id="inpIdentifyCustomer" class="MultipleSelectText">'
						+ '<option value="-2" <cfif CppSetupData.IdentifyGroupId_int EQ -2>selected</cfif> > Billing Phone Number</option>'
						+ '<option value="-1" <cfif CppSetupData.IdentifyGroupId_int EQ -1>selected</cfif> > Email Address</option>'
						+ '</select>'
						+ '<a href="#" class="underline" onclick="ShowDialog(\'createMyOwn\'); return false;">Create my own</a>';
            	}else{
            		var identifyHtml ='';
            		
            		for(iIdentify in d.DATA.IDENTIFYDATA[0].DATA.IDENTIFYID_INT){
            			if( d.DATA.DATA[0].DATA.HYPHEN_TI[0] == 1 && identifyHtml !='' ){
							identifyHtml += ' - ' + ' <input type = "text" size ="15" /> ';
						}else{
							identifyHtml += ' <input type = "text" size ="15" /> ';
						}
            		}
            		buildportalControlHtml = '<span class="bold">'+ d.DATA.DATA[0].DATA.GROUPNAME_VCH[0] +'</span>'
            			+ '<input type = "hidden" name="inpIdentifyGroupId" value ="' + d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0] + '" />'
            			+ identifyHtml
						+ '<a href="#" class="underline" onclick="removeMyOwn(' + d.DATA.DATA[0].DATA.IDENTIFYGROUPID_INT[0]+ '); return false;">Remove</a><br />';
            	}
            	
            	$('#buildportalControl').html(buildportalControlHtml);
            	return false;
            }
   		});
	}
	
	function removeMyOwn(groupId,CPPUID){
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=RemoveIdentifyGroup&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	inpIdentifyGroupId: groupId,
            	inpCPPUUID: CPPUID
            },
            success: function(d){
            	if (d.ROWCOUNT > 0) 
				{													
				
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{
							
            				identifyGroupHtml();
            			}else{
							jAlert(d.DATA.ERRMESSAGE[0], "Failure!");							
						}
            		}
            	}
            }
   		});
	}
	
	function myDropdown(obj){
		//$(obj).next().toggle();
	}
	
	
	function onchangedSelect(obj,preId){
		return myDDSelect(obj,temp,preId);
	}
	
	function myDDSelect(obj,gId,preId){
		$(obj).parent().prev().text($(obj).text());
		$(obj).parent().prev().attr('rel',gId);
		$(obj).parent().hide();
		updatePreferenceContact(gId,preId);
		return false;
	}
	
	function buildGroupData(){
		var groupList = '<div class="wrapper-demo"><select class="preferenceDropdown">';
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=Getgrouplist&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
			async:false,
            data: {
            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
            },
            success: function(d){
					//Check if variable is part of JSON result string								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						var arrayGroup = d.ARR_GROUP;
						if(CurrRXResultCode > 0)
						{
										
							for(var index in arrayGroup){
								groupList = groupList + '<option value="' +arrayGroup[index].GROUPID+ '">' +arrayGroup[index].GROUPNAME+ '</option>';
							}	
							groupList = groupList + '</select></div>'; 			
							
            			}else{
							jAlert(d.DATA.MESSAGE[0], "Failure!");							
						}
            	}
            }
 		});
		return groupList;
	}
	
	function selectContact(arrContact, preItem, preArr){
		var contactSelectItem = '';
		var contactSelect = '<div class="wrapper-demo">';
		var contactSelected = '';
		
		for(indexContact in arrContact){
			
			var isInPreItems = false;
			var contactItem = arrContact[indexContact];
			for(preIndex in preArr){
				if(preArr[preIndex].GROUPID_INT == contactItem.GROUPID && preArr[preIndex].GROUPID_INT != preItem.GROUPID_INT){
					isInPreItems = true;
				}
			}
			
			if(contactItem.GROUPID == preItem.GROUPID_INT){
				contactSelectedStatus = true;
			} else{
				contactSelectedStatus = false;
			}
			
			if(!isInPreItems){
				if(contactSelectedStatus){
					contactSelectItem += '<option value="'+contactItem.GROUPID+'" selected="selected">' ;
					contactSelectItem += '<a href ="#">' +  contactItem.GROUPNAME + '</a>';
					contactSelectItem += '</option>';
				} else{
					contactSelectItem += '<option value="'+contactItem.GROUPID+'" >' ;
					contactSelectItem += '<a href ="#">' +  contactItem.GROUPNAME + '</a>';
					contactSelectItem += '</option>';
				}
			}
			
		}
		contactSelect =  contactSelect + '<div class="wrapper-dropdown-3" tabindex="1">' + contactSelected + '</div>'
					+'<select name="preferenceGroup" class="preferenceDropdown dropdown gDropDown active" id="preferenceGroup'+ preItem.PREFERENCEID_INT +'"  >'
					+ contactSelectItem 
					+ '</select> </div> ';
		return contactSelect;
		
	}
	
	function listPreference(){
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=GetPreference&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
            },
            success: function(d){
            	var listPreferenceHTML = '';
				if(d.DATA.ARR_PREFERENCE[0].length == 0){
					// add blank value
					var inputValue = '<input name="preferenceValue" placeholder="Value" class="filter_val preference-name-txt" type="text" size="25">';
					if(<cfoutput>#CppSetupData.SETCUSTOMVALUE_TI#</cfoutput> == 0){
						inputValue = '<input name="preferenceValue" style="display:none;" placeholder="Value" class="filter_val preference-name-txt" type="text" size="25">';
					}
					listPreferenceHTML +=	'<div class = "group show" updateType="add">'
										+ inputValue
										+ '<input name="preferenceDesc" placeholder="Description" type="text" class="filter_val preference-description-txt" value="" size="25">'
										+ '<input type="hidden" value="-1" name="preferenceId">'
										+ buildGroupData()	    								
										+ '<a href = "#" class ="preferenceDelete hide img16_16" onclick="preferenceDelete(this); return false;" ></a>'
										+ '<a href = "#" class ="preferenceAdd preferenceUpDown img16_16" onclick="preferenceAdd(); return false;" ></a>'
										+ '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value and description can not be blank</span></div>'
										+ '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
	    								+ '</div>';
				} else{
	            	for(index in d.DATA.ARR_PREFERENCE[0]){
	            		var preItem =  d.DATA.ARR_PREFERENCE[0][index];
	            		var listPreferenceDown ='';
	            		var listPreferenceUp ='';
	            		
	            		
	            		if(index < d.DATA.ARR_PREFERENCE[0].length - 1){
	            			listPreferenceDown = 'preferenceDown';
	            		}
	            		if(index > 0){
	            			listPreferenceUp = 'preferenceUp';
	            		}
						
						var inputValue = '<input name="preferenceValue" placeholder="Value" type="text" id="preferenceValue' + preItem.VALUE_VCH + '" class="filter_val preference-name-txt" value="' + preItem.VALUE_VCH + '" size="25">';
						if(<cfoutput>#CppSetupData.SETCUSTOMVALUE_TI#</cfoutput> == 0){
							inputValue = '<input name="preferenceValue" style="display:none;" placeholder="Value" type="text" id="preferenceValue' + preItem.VALUE_VCH + '" class="filter_val preference-name-txt" value="' + preItem.VALUE_VCH + '" size="25">';
						}
						// use update type flag to update when submit
	            		listPreferenceHTML +=	'<div class = "group show" updateType="update">'
												+ inputValue
												+ '<input name="preferenceDesc" placeholder="Description" type="text" id="preferenceDesc' + preItem.DESC_VCH + '" class="filter_val preference-description-txt" value="' + preItem.DESC_VCH + '" size="25">'
												+ '<input type="hidden" value="'+preItem.PREFERENCEID_INT+'" name="preferenceId">'
	            								+ selectContact(d.DATA.ARR_CONTACT_GROUP[0],preItem,d.DATA.ARR_PREFERENCE[0])
												+ '<a href = "#" class ="preferenceDelete hide img16_16" onclick="preferenceDelete(this); return false;" ></a>'
												+ '<a href = "#" class ="preferenceAdd preferenceUpDown img16_16" onclick="preferenceAdd('+preItem.PREFERENCEID_INT+'); return false;" ></a>'
												+ '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value and description can not be blank</span></div>'
												+ '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
	            								+ '</div>';
	            	}
				}
            	
            	$('#listPreference').html(listPreferenceHTML);
				//hide button Delete preference if there is less than or equal 1 group
				if ($("#listPreference").find("div.group.show").size() > 1) {
					$("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("hide");
					$("#listPreference").find("div.group.show>a.preferenceDelete").addClass("show");
				}
            	$('.voice input').custominput();
				
				$('select.preferenceDropdown').selectmenu({style:'popup',width:'148',menuWidth: '148'});				
            }
   		});
	}
	
	function customValueCheckBoxStatus(){
		if($('#customValue').is(':checked')){
        	$('#listPreference').hide();
        	$('#preferenceValue').show();
        	$('.input-txt').width('158px');
        }else{
        	$('#listPreference').show();
			$('#preferenceValue').show();
        	$('.input-txt').width('158px');
         }
	}
	
	function preferenceAdd(){		
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=Getgrouplist&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>'
            },
            success: function(d){
					//Check if variable is part of JSON result string								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;
						var arrayGroup = d.ARR_GROUP;
						if(CurrRXResultCode > 0)
						{
							var groupList = '<div class="wrapper-demo"><select class="preferenceDropdown">';
							for(var index in arrayGroup){
								groupList = groupList + '<option value="' +arrayGroup[index].GROUPID+ '">' +arrayGroup[index].GROUPNAME+ '</option>';
							}
							groupList = groupList + '</select></div>'; 			
							
							var inputValue = '<input name="preferenceValue" placeholder="Value" type="text" class="filter_val preference-name-txt" value="" size="25">';
							if($('#customValue').val() == 0){
								inputValue = '<input name="preferenceValue" style="display:none" placeholder="Value" type="text" class="filter_val preference-name-txt" value="" size="25">';
							}
							// blank value
							var blankPreference =	'<div class="group show" updateType="add">'
											+ inputValue
											+ '<input name="preferenceDesc" placeholder="Description"  type="text" id="preferenceDesc" class="filter_val preference-description-txt" value="" size="25">'
											+ '<input name="preferenceId" type="hidden" value="-1" >'
											+ groupList 	        								
											+ '<a href="#" onclick="preferenceDelete(this); return false;" class ="preferenceDelete show img16_16" ></a>'
											+ '<a href="#" onclick="preferenceAdd(this); return false;" class ="preferenceAdd preferenceUpDown img16_16" ></a>'
											+ '<div id="validate" placeholder="validate" class="hide-div-validate"><span style="color:red;">Value and description can not be blank</span></div>'
											+ '<div placeholder="validateDuplicate" class="hide-div-validate"><span style="color:red;">Group can not be duplicate</span></div>'
	        								+ '</div>';
												 
							$('#listPreference').append(blankPreference);
							//show button Delete preference if there is more than 2 groups
							if ($("#listPreference").find("div.group.show").size() > 1) {
								$("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("hide");
								$("#listPreference").find("div.group.show>a.preferenceDelete").addClass("show");
							}
							$('select.preferenceDropdown').selectmenu({style:'popup',width:'148',menuWidth: '148'});			
            			}else{
							jAlert(d.DATA.MESSAGE[0], "Failure!");
						}
            	}
            }
 		});
		
	}
	
	function preferenceDelete(preObject){	
		// update preference type	
		var preferenceBox = $(preObject).parent();
		if(preferenceBox.attr('updateType') != 'add'){
			preferenceBox.attr('updateType', 'delete');
			preferenceBox.removeClass("show");
			preferenceBox.addClass("hide");
		} else{
			preferenceBox.next("div[placeholder='validate']").remove();
			preferenceBox.next("div[placeholder='validateDuplicate']").remove();
			preferenceBox.remove();
		}
		
		if ($("#listPreference").find("div.group.show").size() <= 1) {
			$("#listPreference").find("div.group.show>a.preferenceDelete").removeClass("show");
			$("#listPreference").find("div.group.show>a.preferenceDelete").addClass("hide");
			return false;
		}
	}
	
	function preferenceUpDown(preferenceId, preferenceType, withValue){
		if( 
			($('#preferenceDesc').val() != '' && $('#preferenceDesc').val() != 'Type description in this box')
			|| ($('#preferenceValue').val() != '' && $('#preferenceValue').val() != 'Type value in this box')
		 ){
			return false;
		}
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=preferenceUpDown&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	preferenceId: preferenceId,
            	preferenceType: preferenceType,
            	widthValue: withValue
            },
            success: function(d){
            	if (d.ROWCOUNT > 0) 
				{													
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						if(CurrRXResultCode > 0)
						{
            				listPreference();
            			}else{
							//jAlert(d.DATA.ERRMESSAGE[0], "Failure!");							
						}
            		}
            	}
            }
   		});
	}
	
	function DropDown(el) {
		this.ddlanguge = el;
		this.placeholder = this.ddlanguge.children('span');
		this.opts = this.ddlanguge.find('ul.dropdown > li');
		this.val = '';
		this.index = -1;
		this.initEvents();
	}
	
	function ValidateLanguage(){
		if ($("a#inpLanguagePreference-button >span.ui-selectmenu-status").text() == 'Yes') {
			if ($("#inpType").val()) {
				return true;
			}
			alert(1);
			return false;
		}
		return true;
	}
	
	function ValidatePreferences(){
		var result = true;
		//reset all validate box
		$("div[placeholder='validate']").each(function(){
			$(this).removeClass("show-div-validate");
			$(this).addClass("hide-div-validate");
		});
		
		$("div[placeholder='validateDuplicate']").each(function(){
			$(this).removeClass("show-div-validate");
			$(this).addClass("hide-div-validate");
		});
		
		var inpVoiceMethod = $('#inpContactMethodVoice').is(':checked') ? 1 : 0;
		var inpSMSMethod = $('#inpContactMethodSMS').is(':checked') ? 1 : 0;
		var inpEmailMethod = $('#inpContactMethodEmail').is(':checked') ? 1 : 0;
		
		//One method must be chosen, if not show err box
		if(inpVoiceMethod+inpSMSMethod+inpEmailMethod <1){
			$('#ValidateContactMethod').removeClass("hide-div-validate");
			$('#ValidateContactMethod').addClass("show-div-validate");
			$('#ValidateContactMethod').attr("tabindex",-1).focus();
			result = false;
			return result;
		}
		
		//if "set custom values for each preference" is yes, validate
		if($("a#customValue-button >span.ui-selectmenu-status").text() =='Yes'){
			$("input[placeholder='Value']").each(function(index){
				var validateDiv = $(this).nextAll("div[placeholder='validate']:first");
				//check if div box parent is not classified as delete
				if(validateDiv.parent().attr("updateType")=="delete") return;
				//if value field or description empty, show err mess
				if($(this).val()==''||$(this).next("input").val()==''){
					validateDiv.removeClass("hide-div-validate");
					validateDiv.addClass("show-div-validate");
					result = false;
					return false;
				}
				else{
					validateDiv.removeClass("show-div-validate");
					validateDiv.addClass("hide-div-validate");
				}
			});
		}
		//if validate blank not pass, no need to validate duplicate, so return
		if(!result) return result;
		
		//validate duplicate
		$("#listPreference select").each(function (i, element) {
			if($(element).parent().parent().attr('updatetype') == 'delete') return;
			var selectedVal = $(element).val();
			$("#listPreference select").each(function (j, subElement) {
				if (i!=j && selectedVal == $(subElement).val()) {
					if($(subElement).parent().parent().attr('updatetype') == 'delete') return;
					var firstDiv = $(element).parent().nextAll("div[placeholder='validateDuplicate']:first");
					var secondDiv = $(subElement).parent().nextAll("div[placeholder='validateDuplicate']:first");
					firstDiv.removeClass("hide-div-validate");
					firstDiv.addClass("show-div-validate");
					secondDiv.removeClass("hide-div-validate");
					secondDiv.addClass("show-div-validate");
					result = false;
					return false;
				}
			});
			if(!result)return false;
		});
		return result;
	}
	
	function UpdateReferenceContent(id,value,description){
		// check value is not null
		// check description is not null
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=UpdateReferenceContent&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: "json", 
            data: {
            	inpCPPUUID: '<cfoutput>#cppUUID#</cfoutput>',
				inpPreferenceId: id,
            	inpPreferenceDescription : description,
            	inpPreferenceValue: value
            },
            success: function(d){
            	if (d.ROWCOUNT > 0) 
				{													
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						if(CurrRXResultCode > 0)
						{
            				//listPreference();
            			}else{
							//jAlert(d.DATA.ERRMESSAGE[0], "Failure!");		
						}
            		}
            	}
            }
   		});
	}
	
	DropDown.prototype = {
		initEvents : function() {
			var obj = this;

			obj.ddlanguge.on('click', function(event){
				$(this).toggleClass('active');
				return false;
			});

			obj.opts.on('click',function(){
				var opt = $(this);
				obj.val = opt.text();
				obj.index = opt.index();
				obj.placeholder.text(obj.val);
			});
		},
		getValue : function() {
			return this.val;
		},
		getIndex : function() {
			return this.index;
		}
	}

	window.onload=function(){
		$('select.preferenceDropdown').selectmenu({style:'popup',width:'148',menuWidth: '148'});
	};
</script>