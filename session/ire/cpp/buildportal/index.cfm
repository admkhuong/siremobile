<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/ire/cpp.css');
		@import url('#rootUrl#/#PublicPath#/css/ire/customize_preferences.css');
	</style>
</cfoutput>
<cfparam name="cppUUID" default="">
<cfparam name="page" default="0">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Build_Portal_Title#">
</cfinvoke>

<cfif cppUUID eq "" && isDefined('Session.CPPUUID')>
	<cfset cppUUID = Session.CPPUUID>
</cfif>

<cflock timeout=20 scope="Session" type="Exclusive">
    <cfset Session.CPPUUID = CPPUUID>
</cflock>

<cfoutput>
<style type="text/css" media="screen">
	@import url("#rootUrl#/#PublicPath#/css/iconslist2.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css");
</style>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.core.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.position.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.widget.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/bootstrap/dist/js/bootstrap.min.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/cpp-cdf-manager.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>

<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
</cfoutput>

<!--- check permission --->
<cfset createCPPPermissionByCPPId = permissionObject.checkCppPermissionByCppId(CPPUUID,Cpp_Create_Title)>
<cfif NOT createCPPPermissionByCPPId.havePermission>
	<cfset session.permissionError = createCPPPermissionByCPPId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke method="GetCPPSETUP" component="#Session.SessionCFCPath#.ire.marketingcpp" returnvariable="GetCPPSETUP">
	<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>
<cfinvoke method="getCustomHtml" component="#Session.SessionCFCPath#.ire.marketingcpp" returnvariable="getCustomHtml">
	<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>
<cfinvoke method="GetCdfCppByID" component="#Session.SessionCFCPath#.ire.marketingcpp" returnvariable="getCdfHtml">
    <cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>
<cfset CustomHtmlData = SerializeJSON(getCustomHtml.DATA)>
<cfset CdfHtmlData = SerializeJSON(getCdfHtml)>
<cfset nextUrl ='#rootUrl#/#sessionPath#/ire/cpp/template/newui'>
<cfset backUrl ='#rootUrl#/#sessionPath#/ire/cpp/edit?CPPUUID=#cppUUID#'>

<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
<cfset params = StructNew()>
<cfset paramsStruct.CPPUUID = CPPUUID>
<cfif Page GT 0>
	<cfset paramsStruct.page = page>
	<cfset backUrl = backUrl & '&page=#page#'>
</cfif>
<cfset params = jSonUtil.serializeToJSON(paramsStruct)>
<!--- Need to add check for valid return value here --->
<cfset CppSetupData = GetCPPSETUP.DATA[1]>
<cfset CppSetupLanguage = (GetCPPSETUP.LANGDATA)>
<!---<cfdump var="#CppSetupLanguage#">--->
<!--- <cfdump var="#GetCPPSETUP.CPPDATA#"> --->

<!---<cfset titleStep2 = #GetCPPSETUP.CPPDATA[1].TITLE_VCH#>
<cfset titleStep3 = #GetCPPSETUP.CPPDATA[2].TITLE_VCH#>
<cfset titleStep4 = #GetCPPSETUP.CPPDATA[3].TITLE_VCH#>
<cfset descriptionStep2 = #GetCPPSETUP.CPPDATA[1].DESCRIPTION_VCH#>
<cfset descriptionStep3 = #GetCPPSETUP.CPPDATA[2].DESCRIPTION_VCH#>
<cfset descriptionStep4 = #GetCPPSETUP.CPPDATA[3].DESCRIPTION_VCH#>--->
<cfset cppDataQuery = GetCPPSETUP.CPPDATA>


<!--- Verification Section Data --->

<cfset inpVerificationBatchDesc = "click here to select a Campaign" />
<!--- If a CPP Verifcation Message Batch Id is specified - get it's Description for updating select box--->
<cfif CppSetupData.VerificationBatchId_bi GT 0>


    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarGetBatchDesc">
        <cfinvokeargument name="INPBATCHID" value="#CppSetupData.VerificationBatchId_bi#">
    </cfinvoke>

    <cfif RetVarGetBatchDesc.RXRESULTCODE GT 0>

    	<cfset inpVerificationBatchDesc = "#Replace(HTMLEditFormat(RetVarGetBatchDesc.DESC), '''', '`', 'ALL')#" />

	</cfif>

</cfif>

<script>
	function htmlDecode(value) {
	    if (value) {
	        return $('<div />').html(value).text();
	    } else {
	        return '';
	    }
	}
	var checkOldVersion = true; // variable check old version which have not title and description in step 2,3,4
	var languageObj = {}; // for store title and description of languague module
	var contactObj = {}; // for store title and description of contact module
	var contactPreferenceObj = {}; // for store title and description of contact preference module
	var cdfObj = {}; // for store title and description of CDF module
<cfloop query="cppDataQuery">

	//console.log('<cfoutput>#STEP_TYPE_TI#</cfoutput>');
	if('<cfoutput>#STEP_TYPE_TI#</cfoutput>'=='2'){
		contactPreferenceObj.title = htmlDecode('<cfoutput>#jsStringFormat(TITLE_VCH)#</cfoutput>');
		contactPreferenceObj.description = htmlDecode("<cfoutput>#jsStringFormat(DESCRIPTION_VCH)#</cfoutput>");
	}
	if('<cfoutput>#STEP_TYPE_TI#</cfoutput>'=='3'){
		contactObj.title = htmlDecode('<cfoutput>#jsStringFormat(TITLE_VCH)#</cfoutput>');
		contactObj.description = htmlDecode("<cfoutput>#jsStringFormat(DESCRIPTION_VCH)#</cfoutput>");
	}
	if('<cfoutput>#STEP_TYPE_TI#</cfoutput>'=='4'){
		languageObj.title = htmlDecode('<cfoutput>#jsStringFormat(TITLE_VCH)#</cfoutput>');
		languageObj.description = htmlDecode("<cfoutput>#jsStringFormat(DESCRIPTION_VCH)#</cfoutput>");
	}
    if('<cfoutput>#STEP_TYPE_TI#</cfoutput>'=='6'){
        cdfObj.title = htmlDecode('<cfoutput>#jsStringFormat(TITLE_VCH)#</cfoutput>');
        cdfObj.description = htmlDecode("<cfoutput>#jsStringFormat(DESCRIPTION_VCH)#</cfoutput>");
	}

</cfloop>
</script>
<div class="cppBox">
	<cfform name="cppSetup" method="post">
		<div id="main">
			<div id="contenrmain" >
				<div id="headermain">
			    	Setting up a Customer Preference Portal (CPP)
			    </div>
			    <div id="dividertop"></div>
			    <div id="contentouterdiv">
			    	<div id="content" >
			    		<cfinput type="hidden" value="#CppSetupData.Active_int#" name="inpCppActive" id="inpCppActive">
						<cfinput type="hidden" value="#CppSetupData.StepSetup_vch#" name="inpStepSetup" id="inpStepSetup">

						<h3>CUSTOMIZE PREFERENCES</h3><br/>
						Please begin creating your customer preference portal by adding at least one Contact Preference and one Contact Method.
You may drag modules from the "Modules" folder into the "Preferences" folder to add them. Drag to rearrange modules.
						<div class="clear-fix"></div>
						<br/>
						<div id="modules" class="panel panel-primary">
							<div class="panel-heading parent-heading">Modules</div>
							<div id="contentModules" class="panel-body sortable-list">
								<!--- step 1 --->
								<div id="step1" class="panel panel-primary">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Login</div>
								  <div class="panel-body displayNone">
								  </div>
								</div>

								<!--- step 4 --->
								<div id="step4" class="panel panel-primary">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Language preference</div>
								  <div class="panel-body displayNone">
								  </div>
								</div>

								<!--- step 3 --->
								<div id="step3" class="panel panel-primary">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Contact methods</div>
								  <div class="panel-body displayNone">
								  </div>
								</div>

								<!--- step 2 --->
								<div id="step2" class="panel panel-primary">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Contact preferences</div>
								  <div class="panel-body displayNone">
								</div>
								</div>

								<!--- step 5 --->
								<div class="panel panel-primary classStep5">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Custom HTML Module</div>
								  <div class="panel-body displayNone">
								</div>
								</div>

                                <!--- cpp cdf step 6 step type 6--->
                                <div class="panel panel-primary controlCDF classStep6">
									<div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>CDF</div>
                                    <!---<span id="add-multi-fields" class="add-multi-fields"></span>
                                    <span id="create-field" class="create-field"></span>--->
                                    <div class="panel-body displayNone">
                                	</div>
								</div>


								<!--- Step 8 Verification --->
								<div id="step8" class="panel panel-primary ClassVerfication">
								  <div class="panel-heading ui-state-default"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Verification Message</div>
								  <div class="panel-body displayNone"></div>
								</div>

							</div>
							<input type="hidden" value='' id="sortResult">
				</div>
				<div id="preferences" class="panel panel-primary">
					<div class="panel-heading parent-heading">Preferences</div>
					<div id="contentPreferences" class="panel-body sortable-list">
						<div id="drop_to">
							<span>DRAG MODULES HERE</span>
						</div>
					</div>
				</div>
				<div class="clear-fix"></div>
			</div>
		</div>
		<div id="backbutton">
			<div id="nextbutton">
				<a href="#" class="btn btn-primary dropdown-toggle btnapply" id ="inpSubmit">Next</a>
				<a href="<cfoutput>#backUrl#</cfoutput>" class="btn btn-primary dropdown-toggle btnapply" >Back</a>
			</div>
		</div>
	</cfform>
</div>


<div id="overlay" class="web_dialog_overlay"></div>
<div id="dialog_createMyOwn" class="web_dialog" style="display: none;">
	<div class='dialog_title'>
		<label><strong><span id='title_msg'>Create your own customer identifier</span></strong></label>
		<span id="closeDialog" onclick="CloseDialog('createMyOwn');">Close</span>
	</div>
	<form id="createMyOwnContainer">
		<div class="control">
			<label>Name</label>
			<input type="text" id="inpCreateMyOwn" value="e.g., Cell Phone Number" size="30" >
			<span class="createMyOwnRequire" style="display:none;color:red">This field is required</span>
			<div class="myOwn_desc">You may up to 10 custom boxes to create a customer identifier. For example you may create a phone number by adding three boxes of value type numeric, with a size of 3, 3 and 4 respectively </div>
			<div class="myOwn_add">
				<select name="OwnType" id= "OwnType">
					<option value="" selected>Value Type</option>
					<option value="1">Numeric Only</option>
					<option value="2">Letters Only</option>
					<option value="3">Alphanumeric</option>
				</select>
				<select name="OwnSize" id= "OwnSize">
					<option value="0" selected>Size</option>
					<cfloop from="1" to="60" index="index">
						<option value="<cfoutput>#index#</cfoutput>"><cfoutput>#index#</cfoutput></option>
					</cfloop>
				</select>
				<button id="AddMyOwn" onclick="return false;">Add</button>
			</div>
			<label>Preview</label>
			<div class="myOwn_preview" id="myOwn_preview"></div>
		</div>

		<div class='button_area'>
			<span class="hypenCheckbox">
				<input type="checkbox" checked="checked" name="hyphens" id="hyphens">
				<label for="hyphens">include hyphens between boxes</label>
			</span>
			<span class="myOwnButton">
				<button
					id="btnCreateMyOwn"
					class="ui-corner-all"
					type="button"
					onClick=""
				>Save</button>
				<button
					id="close_dialog"
					type="button"
					class="ui-corner-all"
					onClick="CloseDialog('createMyOwn');"
				>Cancel</button>
			</span>
		</div>
	</form>
</div>

<div id="dialog_createGroupContact" class="web_dialog EBMDialog" >
	<div class='dialog_title'><label>Create new group contact</label> <span id="closeDialog" onclick="CloseDialog('createGroupContact');">Close</span> </div>
	<form id="createNewContactContainer">
		<div class="control">
			<label>Group Name</label>
			<input type="text" id="inpGroupContactName" size="17">
			<span class="groupRequire" style="display:none;color:red">This field is required</span>
		</div>

		<div class='button_area'>
			<button
				id="btnCreateGroupContact"
				class="ui-corner-all"
				type="button"
				onClick=""
			>Add</button>

			<button
				id="close_dialog"
				type="button"
				class="ui-corner-all"
				onClick="CloseDialog('createGroupContact');"
			>Cancel</button>
		</div>
	</form>
</div>

<!--- Bug - too many open divs and not enough close divs --->
</div>
</div>
</div>


<cfinclude template="buildportalJs.cfm">
<style>
    table.dataTable tr.odd,
    table.dataTable tr.odd td.sorting_1{
        background-color: #f3f9fd!important;
    }
    table.dataTable tr.even td.sorting_1 {
        background-color: #fff !important;
    }
    #tblListGroupCDFS tbody tr td{
        word-break: keep-all;
    }
    a.selectCDFItemDetected{
        color: #0888d1;
    }
    a.selectCDFItemDetected:hover{
        text-decoration: none;
    }
    a.selectCDFItem{
       cursor: pointer;
    }
    #tblListGroupCDFS_info{
        margin-left: 0;
        padding-left: 0;
    }
	#main {
	   
	}
    span.cdf-define{
        display: inline-block;
        float: left;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 300px;
    }
    .box-preview{
        background-color: #ffffff;
        border: 1px solid #cccccc;
        border-radius: 5px;
        height: auto;
        min-height: 29px;
    }
    .left_box_preview label{
        padding: 10px;
    }
    .left_box_preview label ol{
        padding-left: 30px;
    }
    .right_box_preview .input_box{
        float: none;
        width: 97%;
    }
    .right_box_preview .selectmenucustom{
        float: none !important;
        width: 100% !important;
    }
    .right_box_preview .selectmenucustom .ui-selectmenu{
        width: 100% !important;
    }
    .classStep6 .tab-content{
        overflow: hidden;
    }
    #step4 .mtop11 .right_box{
        position: relative;
    }
	#step4 .box .right_box{
        position: relative;
    }
	
	.right_box_Area ul li{
		margin-left:10px;
	}
	.box-preview .left_box_preview ul li{
		margin-left:16px;
		
	}
</style>