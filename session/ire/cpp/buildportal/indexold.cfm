<style>
	#ddcl-inpType{
		padding-left:10px;
	}
	.selectlist option{
		padding-left:10px;
		padding-top:2px;
		padding-bottom:2px;
		width:90%;
	}
	.ui-dropdownchecklist-text{
		height:27px;
		line-height:27px;		
	}
	.right_box .ui-state-default{
		border:0px !important;
	}
	.ui-dropdownchecklist-item input{		
		margin-left:10px !important;
		margin-top:8px !important;
	}
	.ui-dropdownchecklist-item label{
		margin-left:30px;
		margin-top:-20px;
	}
</style>	
	
<cfparam name="cppUUID" default="">
<cfparam name="page" default="0">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Build_Portal_Title#">
</cfinvoke>

<cfif cppUUID eq "" && isDefined('Session.CPPUUID')>
	<cfset cppUUID = Session.CPPUUID>
</cfif>

<cflock timeout=20 scope="Session" type="Exclusive">
    <cfset Session.CPPUUID = CPPUUID>
</cflock>

<cfoutput>
<style type="text/css" media="screen">	
	@import url("#rootUrl#/#PublicPath#/css/iconslist2.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.core.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.theme.css");
	@import url("#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css");
	@import url("#rootUrl#/#PublicPath#/bootstrap/dist/css/customize2.css");
	@import url("#rootUrl#/#PublicPath#/css/ire/style5.css");
</style>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.core.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.position.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.widget.js"></script>
<script language="javascript" src="#rootUrl#/#PublicPath#/bootstrap/dist/js/bootstrap.min.js"></script>
</cfoutput>

<!--- check permission --->
<cfset createCPPPermissionByCPPId = permissionObject.checkCppPermissionByCppId(CPPUUID,Cpp_Create_Title)>
<cfif NOT createCPPPermissionByCPPId.havePermission>
	<cfset session.permissionError = createCPPPermissionByCPPId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke method="GetCPPSETUP" component="#Session.SessionCFCPath#.ire.marketingcpp" returnvariable="GetCPPSETUP">
	<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>

<cfset nextUrl ='#rootUrl#/#sessionPath#/ire/cpp/template/newui'>
<cfset backUrl ='#rootUrl#/#sessionPath#/ire/cpp/edit?CPPUUID=#cppUUID#'>

<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
<cfset params = StructNew()>
<cfset paramsStruct.CPPUUID = CPPUUID>
<cfif Page GT 0>
	<cfset paramsStruct.page = page>
	<cfset backUrl = backUrl & '&page=#page#'>
</cfif>
<cfset params = jSonUtil.serializeToJSON(paramsStruct)>
<!--- Need to add check for valid return value here --->
<cfset CppSetupData = GetCPPSETUP.DATA[1]>
<!---<cfset CppSetupLanguage = (GetCPPSETUP.LANGDATA[1])>--->

<cfset CppSetupLanguage = (GetCPPSETUP.LANGDATA)>

<!--- <cfdump var="#CppSetupData#"> --->
<div class="cppBox">
	<cfform name="cppSetup" method="post">
		<div id="main">
			<div id="contenrmain" >
				<div id="headermain">
			    	<span>Setting up a Customer Preference Portal (CPP)</span>
			    </div>
			    <div id="dividertop"></div>
			    <div id="contentouterdiv">
			    	<div id="content" >
			    		<cfinput type="hidden" value="#CppSetupData.Active_int#" name="inpCppActive" id="inpCppActive">
						<cfinput type="hidden" value="#CppSetupData.StepSetup_vch#" name="inpStepSetup" id="inpStepSetup">
						
						<h3>CUSTOMIZE PREFERENCES</h3><br/>
						Please begin creating your customer preference portal by adding at least one Contact Preferenceand one Contact Method. You may rearrange the order of the steps by dragging them.
						
						<!--- step 1 ---> 
						<div class="panel panel-primary">
						  <div class="panel-heading"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Login</div>
						  <div class="panel-body">
						   <div class="contactpreferencespan">
								Select how you will indentify each individual customer. You may select from one of the fredefined options or define your own.
							</div>
							<div class="small-panel-right">
								<span class="include-select">
									<select name="inpIncludePreference" id="inpIncludePreference">																					
										<option value="1" <cfif CppSetupData.IncludeIdentity_ti EQ 1> selected </cfif>>Yes</option>
										<option value="0" <cfif CppSetupData.IncludeIdentity_ti EQ 0> selected </cfif>>No</option>										
									</select>									
								</span>
								<span class="include-text">Include?</span>
							</div>
						  </div>
						</div>		
						
						<!--- step 4 --->
						<div class="panel panel-primary">
						  <div class="panel-heading"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Language preference</div>
						  <div class="panel-body">
						   <div class="contactpreferencespan">
								Allow customers to select their language preference. This will display as a drop down single selection option to the customer.
							</div>
							<div class="small-panel-right">		
								<span class="include-select">
									<select name="inpLanguagePreference" id="inpLanguagePreference">
										<option value="1" <cfif CppSetupData.IncludeLanguage_ti EQ 1> selected </cfif>>Yes</option>
					                    <option value="0" <cfif CppSetupData.IncludeLanguage_ti EQ 0> selected </cfif>>No</option>
									</select>									
								</span>
								<span class="include-text">Include?</span>		
							</div>
							<div style="clear:both;"></div>
							<hr />														
							<div class="box mtop11">
								<div class="left_box lang_left_box">
									<span class="btn btn_select_lang">Select Language</span>
									<div class="info_box" id="LayoutTypeInfo"></div>
								</div>
								<div class="right_box" id="selectlistLang">
									<select class="selectlist" name="inpType" id="inpType" multiple="multiple">
										<option value="ArabicBengali" <cfif ListContains(CppSetupLanguage, 'ArabicBengali','|') > selected="selected"</cfif>>ArabicBengali</option>
					                    <option value="Chinese"<cfif ListContains(CppSetupLanguage, 'Chinese','|') > selected="selected"</cfif>>Chinese</option>
					                    <option value="English"<cfif ListContains(CppSetupLanguage, 'English','|') > selected="selected"</cfif>>English</option>
					                    <option value="French"<cfif ListContains(CppSetupLanguage, 'French','|') > selected="selected"</cfif>>French</option>
					                    <option value="German"<cfif ListContains(CppSetupLanguage, 'German','|') > selected="selected"</cfif>>German</option>
					                    <option value="Hindi"<cfif ListContains(CppSetupLanguage, 'Hindi','|') > selected="selected"</cfif>>Hindi</option>
					                    <option value="Japanese"<cfif ListContains(CppSetupLanguage, 'Japanese','|') > selected="selected"</cfif>>Japanese</option>
					                    <option value="Korean"<cfif ListContains(CppSetupLanguage, 'Korean','|') > selected="selected"</cfif>>Korean</option>
					                    <option value="Portuguese"<cfif ListContains(CppSetupLanguage, 'Portuguese','|') > selected="selected"</cfif>>Portuguese</option>
					                    <option value="Russian"<cfif ListContains(CppSetupLanguage, 'Russian','|') > selected="selected"</cfif>>Russian</option>
					                    <option value="Spanish"<cfif ListContains(CppSetupLanguage, 'Spanish','|') > selected="selected"</cfif>>Spanish</option>
									</select>
								</div>
							</div>
						  </div>
						</div>	
						
						<!--- step 3 --->
						<div class="panel panel-primary">
						  <div class="panel-heading"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Contact methods</div>
						  <div class="panel-body">
						   <div class="contactpreferencespan">
						   		Select the channels of communication that a customer may select. These options will display as checkboxes and a user may input information for more than one option.
							</div>
							<div style="clear:both;"></div>
							<hr />
							<div class="voice" align="center">
								<div style="width:auto; height:auto;">
									<div class="btn voice-checkbox" style="float:left;">
										<input name="inpContactMethodVoice" <cfif CppSetupData.VoiceMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodVoice" type="checkbox" value="1"/> 
										<label for="inpContactMethodVoice"> Voice </label>
									</div>
									<div class="btn voice-checkbox" style="float:left;">
										<input name="inpContactMethodSMS" <cfif CppSetupData.SMSMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodSMS" type="checkbox" value="1"/>										
										<label for="inpContactMethodSMS"> SMS </label>
									</div>
									<div class="btn voice-checkbox" style="float:left;">
										<input name="inpContactMethodEmail" <cfif CppSetupData.EmailMethod_ti EQ 1>checked ="checked"</cfif> id="inpContactMethodEmail" type="checkbox" value="1"/> 
										<label for="inpContactMethodEmail"> E-Mail </label>										
									</div>
								</div>
								<div id="ValidateContactMethod" placeholder="validate" tabindex="-1" class="hide-div-validate"><span id="ValidateContactMethod1"  style="color:red;">You must choose at least one method</span></div>
							</div>
						  </div>
						</div>							
						
						<!--- step 2 --->
						<div class="panel panel-primary">
						  <div class="panel-heading"><span class="panel-heading-icon heading-buildportal" style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Contact preferences</div>
						  <div class="panel-body">
						   <div class="contactpreferencespan">
						   		Add options a customer may select as communication preferences. Each option can be associated with a unique contact group. Contact groups allow you to group the responses of customers so that further communication can be sent to them. You may also 
				              allow customers to select one or more options. Simply enable "Multiple Preference Selection" and a user will be able to select more than one.
							</div>
							<div style="clear:both;"></div>
							<hr />
							<div class="contactpreferencespan">								
								<span class="include-text include-text-preference">Enable multiple preference selection?</span>
								<span class="include-select">
								<select name="inpMultiSelection" id="inpMultiSelection">
									<option value="1" <cfif CppSetupData.MultiplePreference_ti EQ 1> selected </cfif>>Yes</option>
				                    <option value="0" <cfif CppSetupData.MultiplePreference_ti EQ 0> selected </cfif>>No</option>
								</select>									
								</span>
							</div>
							
							<div class="contactpreferencespan">
								<span class="include-text">Set custom values for each preference?</span>
								<span class="include-select">
									<select name="customValue" id="customValue">
										<option value="1" <cfif CppSetupData.SetCustomValue_ti EQ 1> selected </cfif>>Yes</option>
					                    <option value="0" <cfif CppSetupData.SetCustomValue_ti EQ 0> selected </cfif>>No</option>
									</select>
								</span>
							</div>
							<div style="clear:both;"></div>
							<br />
							
							<span>Preferences</span>
							<div style="clear:both;"></div>
							
							<fieldset>				                
                                <div id="listPreference"></div>
							</fieldset>
						  </div>
						</div>			
					</div>
				</div>
			</div>
		</div>
		<div id="backbutton">
			<div id="nextbutton">
				<span>
					<a href="#" class="btn btn-primary dropdown-toggle btnapply" id ="inpSubmit">Next</a>
					<a href="<cfoutput>#backUrl#</cfoutput>" class="btn btn-primary dropdown-toggle btnapply" >Back</a>
				</span>
			</div>
		</div>
	</cfform>
</div>

<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/ire/cpp.css');
	</style>
</cfoutput>	

<div id="overlay" class="web_dialog_overlay"></div>
<div id="dialog_createMyOwn" class="web_dialog" style="display: none;">
	<div class='dialog_title'>
		<label><strong><span id='title_msg'>Create your own customer identifier</span></strong></label> 
		<span id="closeDialog" onclick="CloseDialog('createMyOwn');">Close</span> 
	</div>
	<form id="createMyOwnContainer">
		<div class="control">
			<label>Name</label>
			<input type="text" id="inpCreateMyOwn" value="e.g., Cell Phone Number" size="30" >
			<span class="createMyOwnRequire" style="display:none;color:red">This field is required</span>
			<div class="myOwn_desc">You may up to 10 custom boxes to create a customer identifier. For example you may create a phone number by adding three boxes of value type numeric, with a size of 3, 3 and 4 respectively </div>
			<div class="myOwn_add">
				<select name="OwnType" id= "OwnType">
					<option value="" selected>Value Type</option>
					<option value="1">Numeric Only</option>
					<option value="2">Letters Only</option>
					<option value="3">Alphanumeric</option>
				</select>
				<select name="OwnSize" id= "OwnSize">
					<option value="0" selected>Size</option>
					<cfloop from="1" to="60" index="index">
						<option value="<cfoutput>#index#</cfoutput>"><cfoutput>#index#</cfoutput></option>
					</cfloop>
				</select>
				<button id="AddMyOwn" onclick="return false;">Add</button>
			</div>
			<label>Preview</label>
			<div class="myOwn_preview" id="myOwn_preview"></div>
		</div>
		
		<div class='button_area'>
			<span class="hypenCheckbox">
				<input type="checkbox" checked="checked" name="hyphens" id="hyphens">
				<label for="hyphens">include hyphens between boxes</label>
			</span>
			<span class="myOwnButton">
				<button 
					id="btnCreateMyOwn" 
					class="ui-corner-all" 
					type="button"
					onClick=""
				>Save</button>
				<button 
					id="close_dialog" 
					type="button" 
					class="ui-corner-all"
					onClick="CloseDialog('createMyOwn');"	
				>Cancel</button>
			</span>
		</div>
	</form>
</div>

<div id="dialog_createGroupContact" class="web_dialog EBMDialog" >
	<div class='dialog_title'><label>Create new group contact</label> <span id="closeDialog" onclick="CloseDialog('createGroupContact');">Close</span> </div>
	<form id="createNewContactContainer">
		<div class="control">
			<label>Group Name</label>
			<input type="text" id="inpGroupContactName" size="17">
			<span class="groupRequire" style="display:none;color:red">This field is required</span>
		</div>
		
		<div class='button_area'>
			<button 
				id="btnCreateGroupContact" 
				class="ui-corner-all" 
				type="button"
				onClick=""
			>Add</button>
			
			<button 
				id="close_dialog" 
				type="button" 
				class="ui-corner-all"
				onClick="CloseDialog('createGroupContact');"	
			>Cancel</button>
		</div>
	</form>
</div>

<cfinclude template="buildportalJs.cfm">