<cfparam name="CPPUUID" default="0">


<cfsaveContent variable="htmlContent">
	<cfoutput>
		<input type="button" value="IP Filter" onclick="openIpfilter('<cfoutput>#CPPUUID#</cfoutput>'); return false;">
		<input type="button" value="System password" onclick="openSystemPassword('<cfoutput>#CPPUUID#</cfoutput>'); return false;">
		<table cellspacing="5">
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input type="password" name="inpPassword" id="inpPassword" maxlength="255">
				</td>
			</tr>
			<tr>
				<td>
					Confirm:
				</td>
				<td>
					<input type="password" name="inpConfirm" id="inpConfirm" maxlength="255">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="button" class="ui-corner-all" name="inpSave" id="inpSave" value="Save">
					<a class="btn_back" href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/apiAccess?CPPUUID=<cfoutput>#CPPUUID#</cfoutput>">Back</a>
				</td>
			</tr>
		</table>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="jsContent">
		<script TYPE="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/password_strength_plugin.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#inpPassword").passStrength();
				$("#inpSave").click(function(){
					var inpPassword = $('#inpPassword').val();
					var inpConfirm = $('#inpConfirm').val();
					var inpCppId = '<cfoutput>#CPPUUID#</cfoutput>';
					
					<!--- validate password --->
					if(inpPassword != inpConfirm){
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Password and confirm password not match", "Failure!", function(result) { } );
						return;
					}
					$.ajax({
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=updateSystemPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						type : 'post',
						data:  { 
							inpCppId : inpCppId,
							inpPassword : inpPassword,
							inpConfirm : inpConfirm
						},
						success:
							function(d) 
							{
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{						
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{
											var CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
											if(CurrRXResultCode > 0)
											{
												$.jGrowl(
													"Set password sucessfully", 
													{ 
														life:2000, 
														position:"center", 
														header:' Message', 
														close:function(){
															// redirect to cpp list
															document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/cpp';
														} 
													}
												
													);
												
											}
											else
											{
												var errMessages=d.DATA.MESSAGES[0];
												msgStr="";
												for(var i=0;i<errMessages.length;i++){
													msgStr+=(errMessages[i]+"\n");
												}
												$.alerts.okButton = '&nbsp;OK&nbsp;';
												jAlert(msgStr, "Failure!", function(result) { } );												
											}
										}
										else
										{<!--- Invalid structure returned --->	
											
										}
									}
									else
									{<!--- No result returned --->						
										jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
									}
							} 		
						});
				});
			});
			function openIpfilter(CPPUUID){
				document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/apiAccess?CPPUUID=' + CPPUUID;
			}
			function openSystemPassword(CPPUUID){
				document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp/cppSystemPassword?CPPUUID='+CPPUUID;
			}
		</script>
		<cfoutput>
		<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>
		</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<cfoutput>
		<style>
			
			@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
			@import url('#rootUrl#/#PublicPath#/css/passwordMeter.css');
        </style>
	</cfoutput>
</cfsaveContent>
<style>
	.btn_back {
		   	background-color: #006DCC;
		    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
		    background-repeat: repeat-x;
		    border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    display: inline-block;
		    font-size: 14px;
		    line-height: 20px;
		    margin-bottom: 0;
		    padding: 4px 12px;
		    text-align: center;
		    vertical-align: middle;
		}
		.btn_back:hover {
	   		background-color: #0888D1;
		}
</style>
<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>
