<cfif NOT cppCreatePermission.havePermission>
	<cfset session.permissionError = cppCreatePermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Create_Title#">
</cfinvoke>

<cfoutput>
	<style type="text/css" media="screen">	
	</style>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.core.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.position.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.widget.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/bootstrap/dist/js/bootstrap.min.js"></script>
</cfoutput>

<!--- Get group contact --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.multilists2"
	 method="GetGroupWithContactsCount"
	 returnvariable="RetGroupContact">	
</cfinvoke>
<div id="main">
	<cfform name="cppSetup" method="post" >
		<div id="form-content" align="center">
			<div id="new_header">
				<div class="header-text">Setting up a Customer Preference Portal (CPP)</div>
			</div>
			<div id="inner-txt-box1">
				<div class="inner-txt-hd">Customer Preference Portal Setup</div>
				<div class="inner-txt">Please begin creating your customer preference portal by adding at least one Contact Preference and one Contact Method. You may rearrange the order of the steps by dragging them.</div>
				<div style="clear:both;"></div>
				<div class="box mtop30">
					<div class="left_box">Name</div>
					<div class="right_box">
						<input type="text" class="input_box" name="inpDescription" id="inpDescription">
					</div>
				</div>
				<div class="box mtop11" id="boxVanity">
					<div class="left_box" id="LeftVanity"><span style="float:left">Custom Url</span>
						<div class="info_box" id="VanityInfo"></div>
						<div id="form-right-protal">
							<div id="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip.gif"/></div>
							<div class="info-box"><span class="info-txt-blue">Information - Vanity</span><br /><span class="info-txt-black">Allow you to specify a custom name for your CPP site. URL will be https://CustomerPreferencePortal.com/vanity</span></div>
						</div>
					</div>
					<div class="right_box">
						<input type="text" class="input_box" name="inpVanity" id="inpVanity">
					</div>
				</div>
				<div id="vanityMessage" class="box_r mtop11">
					<div id="vanityText"></div>
				</div>
				<div style="clear:both"></div>
				<div class="box mtop11">
					<div class="left_box"><span style="float:left">Layout Type</span>
						<div class="info_box" id="LayoutTypeInfo"></div>
						<div style="clear:both"></div>
						<div id="form-right-protal">
							<div id="tool2"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip.gif"/></div>
							<div id="info-box1"><span id="info-txt-blue1">Information - Choose Layout</span><br /><span id="info-txt-black1">Customers may either fillout one short form or fill in information one section per page at a time.</span></div>
						</div>
					</div>
					<div class="right_box">
						<select name="inpType" id="inpTypePage">
							<option value="1" selected="selected">Single Page Layout</option>
							<option value="2" >Multiple Page Layout</option>
						</select>
						
					</div>
				</div>
				
				<div class="box_noborder mtop11">
					<div class="left_box_nobg"><span style="float:left">Require customers need to accept your terms of service?</span>
						<div class="info_box" id="RequireTermInfo"></div>
						<div id="form-term-right-protal">
							<div id="tool3"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip.gif"/></div>
							<div id="info-box2"><span id="info-txt-blue2">Information - Terms</span><br /><span id="info-txt-black2">Give your legal team the option to specify custom terms of service.</span></div>
						</div>
					</div>
					<div class="right_box_border">
						<select name="inpAcceptTerm" id="inpAcceptTerm">
							<option value="1">Yes</option>
							<option value="2" selected="selected" >No</option>
						</select>
					</div>
				</div>
				<div style="clear:both"></div>
				<div class="item editor txtdisplay" style="display:none;">
					<textarea name="inpHtmlEditor" id="inpHtmlEditor">Yes, I wish to receive alerts as listed above. My electronic signature authorizes you to deliver this information to me via the selected channels. I agree to the <a href="#">Terms &amp; Conditions</a> that apply for each channel.</textarea>
				</div>
			</div>			
			<div style="clear:both"></div>
		</div>
		<div class="submitCpp">
			<a href="#" class="button nextbtn blue" id= "inpSubmit" >Next</a>
		</div>
	</cfform>
</div>
<cfinclude template="../edit/cppForm.cfm">

<script type="text/javascript">
	var isAjaxLoading = false;
	$(document).ready(function(){
		if($('#inpVanity').val() !=""){
			checkVanity();
		}
		$('#subTitleText').text('Create CPP - Step 1 - Vanity');
		$('#mainTitleText').text('Customer Preference Portal');
		
		$('#inpVanity').change(function(){
			checkVanity();
		});
		
		$('#inpVanity').keyup(function(){
			checkVanity();
		});
		$('#inpSubmit').removeAttr("disabled");
		$('#inpSubmit').click(function(){
			if(!isAjaxLoading){
				isAjaxLoading = true;
				$('#inpSubmit').attr("disabled", "disabled");
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#sessionPath#/</cfoutput>cfc/ire/marketingcpp.cfc?method=addNewCpp",
		    		data:{
		    			inpType:$('#inpTypePage').val(),  
						inpCppDescription:$('input[name=inpDescription]').val(),
						inpTermService:$('#inpAcceptTerm').val(),
						inpHtmlTemplate:tinyMCE.activeEditor.getContent(),
						inpVanity: $('#inpVanity').val()
					},
		            dataType: "json", 
		            success: function(d) {
		            	isAjaxLoading = false;
		            	$('#inpSubmit').removeAttr("disabled");		            	
		            	if(d.RXRESULTCODE > 0){
		            		<cfoutput>
								var params = {};
								params.cppUUID = d.CPPUUID;
								post_to_url('#rootUrl#/#SessionPath#/ire/cpp/buildportal/', params, 'POST');
							</cfoutput>
	           			}else{
	           				jAlert(d.MESSAGE,"Please fill out the required fields" );
	           			}
			          }
			    });
			    return false;
			}
		});
	});
	
	function checkVanity(){
		if($.trim($('#inpVanity').val()) != '')
			{
				$('#tool2').css('margin-top', '135px');
				$('#info-box1').css('margin-top', '122px');
				$('#tool3').css('margin-top', '183px');
				$('#info-box2').css('margin-top', '171px');
				$("#vanityMessage").show();
       			$("#vanityText").removeClass();
       			$("#vanityText").addClass("vanityAvailable");	
       			
       			$("#vanityText").html("No spaces allowed in Vanity");
       			$('#vanityImage').show();
       			
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#sessionPath#/</cfoutput>cfc/ire/marketingcpp.cfc?method=checkVanity&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:{
		    			inpVanity:$('#inpVanity').val()
					},
		            dataType: "json", 
		            success: function(d) {
		            	$("#vanityText").html(d.DATA.MESSAGE[0]);
		            	$("#vanityMessage").show();
		            	if(d.DATA.RXRESULTCODE[0] > 0){
		            		
		            		if(d.DATA.IS_NOT_EXISTED[0]){		            			
		            			$("#vanityText").removeClass();
		            			$("#vanityText").addClass("vanityAvailable");
								
								$("#boxVanity").css("border", "1px solid #CCCCCC");
								$("#LeftVanity").css("border-right", "1px solid #CCCCCC");
								$("#inpVanity").css("background-color", "");
		            			
		            		}else{
		            			
		            			$("#vanityText").removeClass();
		            			$("#vanityText").addClass("vanityNotAvailable");
								$("#boxVanity").css("border", "1px solid red");
								$("#LeftVanity").css("border-right", "1px solid red");
								$("#inpVanity").css("background-color", "#F6E1E1");
		            		}
		         		}
			     	}
			     });		
			}else{
				$('#tool2').css('margin-top', '115px');
				$('#info-box1').css('margin-top', '102px');
				$('#tool3').css('margin-top', '163px');
				$('#info-box2').css('margin-top', '151px');
				$('#vanityImage').hide();
				$('#vanityText').html('');
			}
			
		
	}
</script>
