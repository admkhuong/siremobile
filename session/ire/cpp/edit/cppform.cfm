<cfoutput>
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
	<!--- <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/cpp.css" /> --->
	<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/custominput.jquery.js"></script>
</cfoutput>

<script type="text/javascript">

$(function() {
$( ".column" ).sortable({
connectWith: ".column"
});
$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
.find( ".portlet-header" )
.addClass( "ui-widget-header ui-corner-all" )
.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
.end()
.find( ".portlet-content" );
$( ".portlet-header .ui-icon" ).click(function() {
$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
});
$( ".column" ).disableSelection();
});
</script>
<script type="text/javascript">
	// Run the script on DOM ready:
	$(function(){
		$('input').custominput();
	});
	$(function(){
		$('select#inpTypePage').selectmenu({
			style:'popup',
			width: 385,
			format: addressFormatting
		});
		$('select#inpAcceptTerm').selectmenu({
			style:'popup',
			width: 110,
			format: addressFormatting
			});
	});
	//a custom format option callback
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	</script>
    <script>
$(document).ready(function(){
	
  $(".info-button").hover(function(){
    $(".info-box").toggle();
	$("#tool1").toggle();
	 $("#info-box1").hide();
	 $("#info-box2").hide();
  });
  
  $("#VanityInfo").hover(function(){
    $(".info-box").toggle();
	$("#tool1").toggle();
	 $("#info-box1").hide();
	 $("#info-box2").hide();
  });
  
 $("#LayoutTypeInfo").hover(function(){
    $("#info-box1").toggle();
	$("#tool2").toggle();
	//$("#tool2").css('margin-top', 122 + $('#vanityText').height() + 'px');
	//$("#info-box1").css('margin-top', 102 + $('#vanityText').height() + 'px');
	$(".info-box").hide();
	 $("#info-box2").hide();
  });
  
  $("#RequireTermInfo").hover(function(){
    $("#info-box2").toggle();
	$("#tool3").toggle();
	$("#info-box1").hide();
	$(".info-box").hide();
  });
});

$(document).ready(function(){	
	var accept_pageLoad = $("#inpAcceptTerm").val();
	if(parseInt(accept_pageLoad) == 1){
			$('.editor').show();
		}else{
			$('.editor').hide();
		}
	$('#inpAcceptTerm').change(function(){
		var accept = $("#inpAcceptTerm").val();
		if(parseInt(accept) == 1){
			$('.editor').show();
		}else{
			$('.editor').hide();
		}
	});	
	
//	tinyMCE.init({
//		 mode : "none",
//		 theme : "simple",
//		 width: "750px",
//		 height: "250px",
//		 visual : false,
//		 convert_urls : false,
//		 mode: "none",
//		 theme : "advanced",
//		 theme_advanced_toolbar_location : "bottom",		 
//		 setup : function(ed) { 
//		   ed.onInit.add(function(ed) { 
//		   		var dom = ed.dom;
//	            var doc = ed.getDoc();
//	        	
//	        	doc.addEventListener('blur', function(e) {
//	                content = tinyMCE.activeEditor.getContent();
//	            }, false);
//	            
//	            doc.addEventListener('click', function(e) {
//	            	content = tinyMCE.activeEditor.getContent();
//	            	if(content == "<p>Please enter the text to be display</p>"){
//		                tinyMCE.activeEditor.setContent("");
//	            	}
//	            }, false);
//		   }); 
//		 }
//    });
//			
//    tinyMCE.execCommand('mceAddControl', false, 'inpHtmlEditor');

<!---	tinymce.init({
		    selector: "textarea",
		    toolbar: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			width:"81%"
	});--->
    
	
	tinymce.init({
	mode : "textareas",	
	selector: "textarea",
	convert_urls: false,
	theme: "modern",
	plugins: [
		"advlist autolink lists link image charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen",
		"insertdatetime media nonbreaking save table contextmenu directionality",
		"emoticons template paste textcolor"
	],
	toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	toolbar2: "print preview media | forecolor backcolor emoticons",
	image_advtab: true
});

    $("#createGroupContact").click(function(){
    	ShowDialog("createGroupContact");
    	return false;
	});
	
});

<!---tinymce.init({
			mode : "textareas"
		});
		--->
		
		



</script>