<cfparam name="CPPUUID" type="string" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Launch_Title#">
</cfinvoke>

<!--- check permission --->
<cfset editCPPPermissionByCPPId = permissionObject.checkCppPermissionByCppId(CPPUUID,Cpp_edit_Title)>
<cfif NOT editCPPPermissionByCPPId.havePermission>
	<cfset session.permissionError = editCPPPermissionByCPPId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Launch_Title#">
</cfinvoke>

<!--- get cpp vanity --->

<cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingcpp" method="selectCppVanity" returnvariable="retCppVanity">
	<cfinvokeargument name="inpCPPUUID" value="#CPPUUID#">
</cfinvoke>

<cfset vanity = CPPUUID>
<cfif retCppVanity.VANITY NEQ "">
	<cfset vanity = retCppVanity.VANITY>
</cfif>

<cfoutput>
	<div class="surveyLaunchContent">
		<cfform>	
			<div class="surveyLaunchRow">
				<p>Copy, paste and email the web link below to your audience</p>
				<p class="weblinkText">
					<input type="button" value="Select link" onclick="javascript:txtOnlyVanity.focus();txtOnlyVanity.select();" ><br/>
					<textarea name="txtOnlyVanity" cols="100" rows="1">#CppRedirectDomain#/home/#vanity#</textArea>
				</p>	
			</div>	
			<div class="surveyLaunchRow">
				<p>Copy and paste the HTML code below to add your Web Link to any webpage:</p>
				<p class="weblinkLink"> 
					<input type="button" value="Select code" onclick="txtHTMLVanity.focus();txtHTMLVanity.select();" ><br/>
					<textarea name="txtHTMLVanity" cols="100" rows="1">&lt;a href="#CppRedirectDomain#/home/#vanity#"&gt; Click here to take CPP &lt;/a &gt;</textArea>
				</p>
			</div>	
			<!--- <div class="surveyLaunchRow">
				<p>Copy and paste the HTML code below to add your Web content to any webpage:</p>
				<p class="weblinkLink"> 
					<input type="button" value="Select code" onclick="txtHTMLJs.focus();txtHTMLJs.select();" ><br/>
					<cfset jscontent = "<div id = 'cppmonkey'> <script >
						$.get('#CppRedirectDomain#/session/dsp_home?inpCPPUUID=#CPPUUID#', function(data) {
							$('##cppmonkey').html(data);
						});
					</script></div>">
					<textarea name="txtHTMLJs" cols="100" rows="1">
						#jscontent#
					</textarea>
				</p>
			</div> --->
			<div class="surveyLaunchRow">
				<p>QR Code:</p>
				<p class="weblinkLink"> 
					<img id= "imgQrcode" src = "https://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=#CppRedirectDomain#/home/#vanity#">
				</p>
				<p class="weblinkLink"> 
					<a id="dowloadQrcode" href = "https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=#CppRedirectDomain#/home/#vanity#" target="_blank">
						<b>Download QRCode</b>
					</a>
				</p>
			</div>	
		</cfform>	
	</div>
</cfOutput>
												<div id = 'cppmonkey'> </div>
					
					

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Cpp_Title# >> #Cpp_Launch_Title# </cfoutput>');
	$('#mainTitleText').text('CPP Links');
	//$('#cppmonkey').load('http://cppseta.com/session/dsp_home?inpCPPUUID=3953024561');
</script>
