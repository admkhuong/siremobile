<!---<cfoutput>
		<style>
			@import url('#rootUrl#/#PublicPath#/css/cf_tableCPP.css') all;
		</style>
</cfoutput>	--->
<cfparam name="page" default="0">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Title#">
</cfinvoke>

<!--- Get group contact --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.multilists2"
	 method="GetGroupWithContactsCount"
	 returnvariable="RetGroupContact">	
</cfinvoke>
<!--- Create group filter list --->
<cfset arrayListGroup = ArrayNew(1)>
<cfset groupCount = 1>
<cfloop array="#RetGroupContact.ARR_GROUP#" index="groupIndex" >
	<cfset groupItem = StructNew()>
	<cfset groupItem.VALUE = groupIndex.GROUPID>
	<cfset groupItem.DISPLAY = groupIndex.GROUPNAME>
	<cfset arrayListGroup[groupCount] = groupItem>
	<cfset groupCount = groupCount + 1>
</cfloop>

<cfset StatusArray = [{value = 1, display = 'Active'}, {value = 0, display = 'Disabled'}]>
<cfset TypeArray = [{value = 2, display = 'Multi Page'}, {value = 1, display = 'Single Page'}]>

<link rel="stylesheet" media="screen" type="text/css" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/tabicons.css" />
<!--- Import library----->
<cfimport taglib="../../lib/grid" prefix="mb" />
<cfoutput>
	<cfscript>
		
		pageParam = '';
		if(page GT 0){
			pageParam ='&page=#page#';
		}
		
		htmlOption = '<a href="#rootUrl#/#SessionPath#/ire/cpp/template/newui?CPPUUID={%CPP_UUID_vch%}&showBack=false#pageParam#"><img class="ListIconLinks img16_16 template_16_16" title="Change Template" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" ></a>';		
		
		if(cppEditPermission.havePermission){
			htmlOption &= '<a href="#rootUrl#/#SessionPath#/ire/cpp/edit?CPPUUID={%CPP_UUID_vch%}#pageParam#"><img class="ListIconLinks img16_16 view_16_16" title="Edit CPP" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" ></a>';
		}
		
		if(cppEditPermission.havePermission || cppCreatePermission.havePermission){
			htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/launch/?CPPUUID={%CPP_UUID_vch%}"><img class="ListIconLinks img16_16 launch_online_survey_16_16" title="Launch CPP" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		if(cppEditPermission.havePermission || cppCreatePermission.havePermission){
			htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/preview?CPPUUID={%CPP_UUID_vch%}"><img class="ListIconLinks img16_16 preview_online_survey_16_16" title="CPP Preview" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		if(cppEditPermission.havePermission){
			htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="ListIconLinks img16_16 {%activeIframeStyle%}" title="{%activeIframeTitle%}" onclick="activeDeativePortal(this);" cppUUID="{%CPP_UUID_vch%}" type="{%activeFrameType%}" pageRedirect="{%PageRedirect%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		if(cppDeletePermission.havePermission){
			htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="deleteCPP ListIconLinks img16_16 delete_16_16" title="Delete CPP" pageRedirect="{%PageRedirect%}" rel="{%CPP_UUID_vch%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		if(cppEditPermission.havePermission){
			htmlOption = htmlOption & '<a href="##" onclick="return false;"><img class="ListIconLinks img16_16 {%activeApiAccessStyle%}" title="{%activeApiAccessTitle%}" onclick="activeDeativeApiAccess(this);" cppUUID="{%CPP_UUID_vch%}" type="{%activeApiAccessType%}" pageRedirect="{%PageRedirect%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		if(cppAgentPermission.havePermission){
			htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/agent/index?CPPUUID={%CPP_UUID_vch%}#pageParam#"><img class="ListIconLinks img16_16 agent_16_16" title="Agent Tools" cppUUID="{%CPP_UUID_vch%}" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		if(cppAgentPermission.havePermission){
			htmlOption = htmlOption & '<a href="#rootUrl#/#SessionPath#/ire/cpp/apiAccess?CPPUUID={%CPP_UUID_vch%}"><img class="ListIconLinks img18_18 api_18_18" title="API Access" src="#rootUrl#/#PublicPath#/images/dock/blank.gif"></a>';
		}
		
		
	</cfscript>
	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>
		
	<!--- Prepare column data---->
	<cfset arrNames = ['Unique ID','Description', 'Created Date', "Status", 'Type', 'Options']>	
	<cfset arrColModel = [
			{name='CPP_UUID_vch', width="70px", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.CPP_UUID_vch'}},				
			{name='Desc_vch', width="180px", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.Desc_vch'}},	
			{name='Created_dt',width="125px", sortObject= {isDefault='true', sortType="DESC", sortColName ='c.Created_dt'}},
			{name='Status',width="40px", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.IFrameActive_int'}},
			{name='Type',width="60px", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.Type_ti'}},
			{name='Options', width="100px", format = [htmlFormat]}
		   ] >	
		   
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Unique ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},	
			{VALUE='2', DISPLAY='Description', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Created Date', TYPE='CF_SQL_DATE', FILTERTYPE="DATE"},	
			{VALUE='4', DISPLAY='Type', TYPE='CF_SQL_TINYINT', FILTERTYPE="LIST",LISTOFVALUES = TypeArray},
			{VALUE='5', DISPLAY='Status', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST",LISTOFVALUES = StatusArray}
		]	
		>
	<cfset 
	filterFields = [
		'c.CPP_UUID_vch',
		'c.Desc_vch',
		'c.Created_dt',
		'c.type_ti',
		'activeCPP'
	]	
	>	
	
	<!--- <cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Survey ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='Survey Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Survey Type', TYPE='CF_SQL_VARCHAR', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = 'ONLINE', DISPLAY="ONLINE"},
														{VALUE = 'VOICE', DISPLAY="VOICE"}, 
														{VALUE = 'VOICE & ONLINE', DISPLAY="VOICE & ONLINE"}] },
			{VALUE='4', DISPLAY='Total Responses', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='5', DISPLAY='Total Voice', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='6', DISPLAY='Total Online', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='7', DISPLAY='Expiration Date', TYPE='CF_SQL_DATE', FILTERTYPE="DATE"}
		]
	>
	<cfset 
		filterFields = [
			'bas.BatchId_bi',
			'bas.DESC_VCH',
			'SurveyType',
			'CallResult.TotalOnlineAnswer',
			'CallResult.TotalVoiceAnswer',
			'TotalOnlineAnswer',
			"EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')"
		]	
	>	 --->
	
	
	<mb:table
		component="#LocalSessionDotPath#.cfc.ire.marketingcpp" 
		method="getCPPList" 
		class="cf_table"  
		colNames= #arrNames# 
		colModels= #arrColModel#
		name="CPPs"
		width = "100%"
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	  	FilterSetId="DefaulFSListCPP"
		>
	</mb:table>		
</cfoutput>
<script type="text/javascript">
	$(document).ready(function(){
		
		$('#subTitleText').text('<cfoutput>#Cpp_Title#</cfoutput>');
		$('#mainTitleText').text('ire');
		
		$('.deleteCPP').click(function(){
			$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var page = $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this CPP?", "Delete CPP", function(result) { 
				if(result)
				{	
					deleteCPP(CurrREL, page);
				}
				return false;																	
			});
		});
		
		
	
	});
	
	function deleteCPP(CurrREL, page){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=deleteCPP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { CPPUUID : CurrREL},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp?page=' + page;
									return false;
										
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("CPP has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
			//			$("#AddNewBatchSurveyList #loadingDlgAddBatchSurveyList").hide();
									
				} 		
				
			});
	}
	
	function activeDeativePortal(cpp){
		var uuid = $(cpp).attr("cppUUID");
		var activeFrameType = $(cpp).attr("type");
		var strConfirm = "Would you like to make this portal available to the public?";
		var strTitle = "Make portal available";
		if(activeFrameType == 1){
			strConfirm = "Would you like to take this portal offline? Doing so will prevent users from seeing or using it.";
			strTitle = "Take portal offline";
		}
		var page = $(cpp).attr('pageRedirect');
		
		jConfirm( strConfirm, strTitle, function(result) { 
			if(result)
			{	
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=activeDeactiveFrame&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						inpCppUUID : uuid,
						inpCurrentType : activeFrameType
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
					success:		
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d) 
						{
							<!--- Alert if failure --->
																										
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{						
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{							
										CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
										
										if(CurrRXResultCode > 0)
										{
											document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp?page=' + page;
											return false;
												
										}
										else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("Cpp iframe has not been changed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										}
									}
									else
									{<!--- Invalid structure returned --->	
										
									}
								}
								else
								{<!--- No result returned --->		
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
								}
						} 		
						
					});
			}
			return false;																	
		});
		
		return false;
	}
	
	function activeDeativeApiAccess(cpp){
		var uuid = $(cpp).attr("cppUUID");
		var activeApiAccessType = $(cpp).attr("type");
		var strConfirm = "Do you want to enable API access to this portal?";
		var strTitle = "Enable API access";
		if(activeApiAccessType == 1){
			strConfirm = "Do you want to disable API access to this portal?";
			strTitle = "Disable API access";
		}
		var page = $(cpp).attr('pageRedirect');
		
		jConfirm( strConfirm, strTitle, function(result) { 
			if(result)
			{	
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=activeDeactiveApiAccess&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						inpCppUUID : uuid,
						inpCurrentType : activeApiAccessType
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
					success:		
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d) 
						{
							<!--- Alert if failure --->
																										
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{						
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{							
										CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
										
										if(CurrRXResultCode > 0)
										{
											document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/cpp?page=' + page;
											return false;
												
										}
										else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("API Access has not been changed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										}
									}
									else
									{<!--- Invalid structure returned --->	
										
									}
								}
								else
								{<!--- No result returned --->		
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
								}
													
				
						} 		
						
					});
			}
			return false;																	
		});
		
		return false;
	}
	
	
</script>