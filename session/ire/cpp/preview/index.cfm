<cfparam name="inpCPPAID" default="0" >

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Cpp_Preview_Title#">
</cfinvoke>
<cfif structKeyExists(url,"CPPUUID") AND CPPUUID GT 0 AND IsNumeric(CPPUUID)>
    <!--- check permission --->
    <cfset editCPPPermissionByCPPId = permissionObject.checkCppPermissionByCppId(CPPUUID,Cpp_edit_Title)>

    <cfif NOT (editCPPPermissionByCPPId.havePermission OR cppCreatePermission.havePermission)>
        <cflocation url="#rootUrl#/#sessionPath#/account/home">
    </cfif>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#subTitleText').text('Preview');
            $('#mainTitleText').text('Customer Preference Portal');
        });
    </script>
    <cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingcpp" method="selectCppVanity" returnvariable="retCppVanity">
        <cfinvokeargument name="inpCPPUUID" value="#CPPUUID#">
    </cfinvoke>

    <cfset vanity = CPPUUID>
    <cfif retCppVanity.VANITY NEQ "">
        <cfset vanity = retCppVanity.VANITY>
    </cfif>
    <iframe src="<cfoutput>#CppRedirectDomain#/session/home.cfm?inpCPPUUID=#CPPUUID#&preview=1&inpCPPAID=#inpCPPAID#</cfoutput>"
            scrolling="auto" align="center" height = "1500px" width="100%" frameborder="0"
            id="CPPPRevieFrame" >
    </iframe>
<cfelse>
    <cflocation url="#rootUrl#/#sessionPath#/ire/cpp/" addtoken="false">
</cfif>