﻿<cfparam name="customCssText" default=""> 
<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jscssp/cssparser.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/codemirror/lib/codemirror.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/codemirror/mode/xml/xml.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/codemirror/mode/css/css.js"></script>
	<link rel="stylesheet" href="#rootUrl#/#PublicPath#/js/codemirror/lib/codemirror.css">
	<!---htmlmixed mode must stay after codemirror.js and xml.js--->
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/codemirror/mode/htmlmixed/htmlmixed.js"></script>
</cfoutput>

<div>
	<div style="width:100%">
		<textarea  id="cssBox" style="width:100%;border-width:1px;" ><cfoutput>#allTemplateData["CUSTOMCSS_TXT"][templateType+1]#</cfoutput></textarea>
	</div>
</div>
<script type="text/javascript">
	//replace all existing substring
	//this function should be used to replace some special char as \n \t \r etc
	function replaceAll(oldStr, removeStr, replaceStr, caseSenitivity){
	    if(caseSenitivity == 1){
	        cs = "g";
	        }else{
	        cs = "gi"; 
	    }
	    var myPattern=new RegExp(removeStr,cs);
	    newStr =oldStr.replace(myPattern,replaceStr);
	    return newStr;
	}
	
	$("#cssBox").keyup(function() {
		//reset style of all elements in boxPreview
		$("#boxPreview").find("*").attr("style","");
		InitStyle();	
		
		var source = document.getElementById("cssBox");
		//remove all newline to format data for passing into parser
		var ss = source.innerHTML;
		ss = replaceAll(ss,"\n","",1);
		ss = replaceAll(ss,"<br>","",1);
		ss = replaceAll(ss,"<br/>","",1);
		
		var parser = new CSSParser();
		var sheet = parser.parse(ss, false, true);

		if(sheet){
			for(var index in sheet.cssRules){
				if(!sheet.cssRules[index]) continue;
				if(!sheet.cssRules[index].declarations) continue;
				for(var ruleIndex in sheet.cssRules[index].declarations){
					if(!sheet.cssRules[index].declarations[ruleIndex]) continue;
					var nameCss=sheet.cssRules[index].declarations[ruleIndex].property;
					var valueCss=sheet.cssRules[index].declarations[ruleIndex].valueText;
					$("#boxPreview "+sheet.cssRules[index].mSelectorText).css(nameCss,valueCss);
				}
			}
		}
	});
</script>
