﻿<cfparam name="STEP" default="1" >
<div class="clear">
</div>
<cfinclude template="../../constants/cppConstants.cfm" >
<cfscript>
	/*Type of CPP*/
	SIZETYPE_SMALL = 0;
	SIZETYPE_MEDIUM = 1;
	SIZETYPE_LARGE = 2;
	SIZETYPE_CUSTOM = 3;
	LOGIN_STEP = 1;
	/*End Type of CPP*/
</cfscript>
<cfset cppUUID = trim(cppUUID)>
<cfset portalLeft = ''>
<cfset portalRight = ''>
<cfset maxPhone = 0>
<cfset maxSMS = 0>
<cfset maxEmail = 0>
<cfinvoke 
		component="#LocalSessionDotPath#.cfc.ire.marketingcpp" 
		method="GetCPPSETUPQUERY" 
		returnvariable="GetCPPSETUPQUERY">
		<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>
<!---<cfdump var="#GetCPPSETUPQUERY#" />--->

<cfinvoke 
	component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
	method="GetPreferenceQuery" 
	returnvariable="GetPreferenceQuery">
	<cfinvokeargument name="CPP_UUID" value="#GetCPPSETUPQUERY.CPP_UUID_vch#">
	<cfinvokeargument name="SetCustomValue" value="1">
</cfinvoke>

<cfinvoke 
	component="#LocalSessionDotPath#.cfc.ire.marketingcpp" 
	method="GetContactPreference" 
	returnvariable="retGetContactPreferenceValue">
	<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>

<cfinvoke 
	component="#LocalSessionDotPath#.cfc.ire.marketingcpp" 
	method="GetStepInfo" 
	returnvariable="retGetStepInfo">
	<cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
</cfinvoke>
<cfset StepInfoDATA = retGetStepInfo.DATA>
<cfset TitleStep2 = "">
<cfset DescriptionStep2 = "">
<cfset TitleStep3 = "">
<cfset DescriptionStep3 = "">
<cfset TitleStep4 = "">
<cfset DescriptionStep4 = "">

<cfif Arraylen(StepInfoDATA) GT 0>
	<cfloop array="#StepInfoDATA#" index="StepData">		
		<cfif StepData.StepId EQ 2>
			<cfif StepData.Title EQ "">
				<cfset TitleStep2 = TITLE2>
			<cfelse>
				<cfset TitleStep2 = StepData.Title>	
			</cfif>
			<cfif StepData.Description EQ "">				
				<cfset DescriptionStep2 = DESCIRPTION2>
			<cfelse>
				<cfset DescriptionStep2 = StepData.Description>	
			</cfif>
		</cfif>
		<cfif StepData.StepId EQ 3>
			<cfif StepData.Title EQ "">
				<cfset TitleStep3 = TITLE3>
			<cfelse>
				<cfset TitleStep3 = StepData.Title>	
			</cfif>
			<cfif StepData.Description EQ "">				
				<cfset DescriptionStep3 = DESCIRPTION3>
			<cfelse>
				<cfset DescriptionStep3 = StepData.Description>	
			</cfif>
		</cfif>
		<cfif StepData.StepId EQ 4>
			<cfif StepData.Title EQ "">
				<cfset TitleStep4 = TITLE4>
			<cfelse>
				<cfset TitleStep4 = StepData.Title>	
			</cfif>
			<cfif StepData.Description EQ "">				
				<cfset DescriptionStep4 = DESCIRPTION4>
			<cfelse>
				<cfset DescriptionStep4 = StepData.Description>	
			</cfif>
		</cfif>
	</cfloop>
<cfelse>
	<cfset TitleStep2 = TITLE2>
	<cfset DescriptionStep2 = DESCIRPTION2>
	<cfset TitleStep3 = TITLE3>
	<cfset DescriptionStep3 = DESCIRPTION3>
	<cfset TitleStep4 = TITLE4>
	<cfset DescriptionStep4 = DESCIRPTION4>
</cfif>	
<cfset data = retGetContactPreferenceValue.DATA>

<cfset CppSetupLanguage = GetCPPSETUPQUERY.LANGDATA>

<cfset i =1>

<cfset indexWithOutHtml = 1><!---this var is used to define stepcontent id which is not custom html --->
<div class="previewContent" id="boxPreview">
	<div class="innerPreviewBox previewStyle">
		<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
			<cfif listfind(GetCPPSETUPQUERY.StepSetup_vch,5) GT 0>
				<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,5))>
			</cfif>
		</cfif>		
		<cfset ArrayStep = ListTOArray(GetCPPSETUPQUERY.StepSetup_vch)>
		<cfloop array="#ArrayStep#" index="index">
			<div class="clear-both"></div>
			<input name="<cfoutput>stepVal#index#</cfoutput>" type="hidden" id="<cfoutput>stepVal#i#</cfoutput>" value ="<cfoutput>#index#</cfoutput>" >
			<div data-id="<cfoutput>#index#</cfoutput>" class = "cppPortalRow" id="stepContent<cfoutput>#i#</cfoutput>" <cfif index EQ LOGIN_STEP> style="display:none" </cfif>>
				<cfif index eq 2>
					<div class="clear-both"></div>
					<div class="stepCPP mtop43"><!---<span class="step">Step <cfoutput>#indexWithOutHtml#</cfoutput>:---><cfoutput>#HTMLStringFormat(TitleStep2)#</cfoutput><!---</span>---></div>
					<div class="clear-both"></div>
					<div class="stepNote">
						<cfoutput>#HTMLStringFormat(DescriptionStep2)#</cfoutput>
					</div>
                    <div class="clear-both"></div>
					<div id="CppContact">
						<div class="mleft15">
							<cfset jj = 0>										
							<cfloop array="#GetPreferenceQuery.DATA#" index="PreferenceItem">
								<cfset checked = false>
								<cfset jj++>
								<!--- check value selected --->
									
								<cfloop array="#data.PREFERENCE#" index="preIndex">
									<cfif #PreferenceItem.PREFERENCEID_INT# EQ preIndex>
										<cfset checked = "checked">
									</cfif>
								</cfloop>
                                
                                
<!---                                <cfif Session.CPPACOUNTID NEQ "0" AND ArrayLen(GetPreferenceQuery.DATA) GT 1>
                                	<cfif PreferenceItem.ONCHECKLIST EQ "Yes"> 
                                        <cfset checked = "checked">
                                	<cfelse>
                                        <cfset checked = "unchecked">
                                	</cfif>
                                </cfif>
--->								
								<div>
									<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
										<input type="checkbox" id="preference<cfoutput>#jj#</cfoutput>" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>" <cfoutput>#checked#</cfoutput> class="removeCheck">
									<cfelse>
										<input type="radio" id="preference<cfoutput>#jj#</cfoutput>" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>" <cfoutput>#checked#</cfoutput> class="removeCheck">
									</cfif>
									<label for="preference<cfoutput>#jj#</cfoutput>"><cfoutput>#PreferenceItem.Desc_vch#</cfoutput></label>
									<!--- IE bug bix - remove this <br/>--->
								</div>
								
							</cfloop>
							<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
								<cfif #Arraylen(GetPreferenceQuery.DATA)# GT 0>
									<div>
										<a class="checkevent" href="javascript:CheckEvent()" id="CheckEvent">Uncheck all</a>
									</div>
								</cfif>
							</cfif>
						</div>
						
					</div>
					<cfset i++>
					<cfset indexWithOutHtml++>
				</cfif>
				<div class="clear-both"></div>
				<!-----Identify does not make sense in cpp authentication ----->
				<cfif index eq 3>
					<div class="clear-both"></div>
					<div class="stepCPP mtop47"><!---<span class="step">Step <cfoutput>#indexWithOutHtml#</cfoutput>: ---><cfoutput>#HTMLStringFormat(TitleStep3)#</cfoutput><!---</span>---></div>
					<div class="clear-both"></div>
					<div class="stepNote">
						<cfoutput>#HTMLStringFormat(DescriptionStep3)#</cfoutput>
					</div>
					<div class="clear"></div>
					<cfif GetCPPSETUPQUERY.VOICEMETHOD_TI EQ 1>
						<cfset phoneArray = data.PHONE>									
						<cfset voiceCheck = "">
						<cfif ArrayLen(phoneArray) GT 0>
							<cfset voiceCheck = 'checked="true"'>
						</cfif>
						<div class="inputcontent mtop20">
							<div class="box">
								<div class="left_box">
									<input type = "checkbox" id="voiceCheck" name="voiceCheck" value="1" class="removeCheck" <cfoutput>#voiceCheck#</cfoutput> >
									<label for="voiceCheck">Voice Number</label>
								</div>
								<div class="right_box" id="inpVoiceNumber">
									<cfset voicenumberlist = "">
									
									<cfset del_phone_class = "delHide">
									<cfif ArrayLen(phoneArray) GT 1>
										<cfset del_phone_class = "delShow">
									</cfif>	
                                    
                                    <!--- Always show - new ui for opt out --->
                                    <cfset del_phone_class = "delShow">
                                    
									<cfif ArrayLen(phoneArray) GT 0>
										<cfset j = 1>
										<cfset inputClass="input_box">
										<cfloop array="#phoneArray#" index="phone">
											<cfif j GT 1>
												<cfset inputClass="input_box_last">
											</cfif>													
											<div id="VoiceNumnerPanel<cfoutput>#j#</cfoutput>">
												<input type="text" name="voiceText_<cfoutput>#j#</cfoutput>" id="voiceText_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#phone#</cfoutput>" >
											</div>
											<cfset voicenumberlist = voicenumberlist & j &",">
											<cfset j++>
										</cfloop>
									<cfelse>
										<div id="VoiceNumnerPanel1">														
											<input type="text" name="voiceText_1" id="voiceText_1" class="input_box maskPhone" size="4" value="" >
										</div>
										<cfset voicenumberlist = "1,">
									</cfif>
								</div>
							</div>
							<div id="actionVoice" class="action-voice-panel">
								<cfif ArrayLen(phoneArray) GT 0>
									<cfset j = 1>
									<cfloop array="#phoneArray#" index="phone">														
										<input type="button" name="DelVoiceNumber_<cfoutput>#j#</cfoutput>" id="DelVoiceNumber_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_phone_class#</cfoutput>" onclick="fnDelVoiceNumber('VoiceNumnerPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
										<cfset j++>
									</cfloop>
								<cfelse>
									<input type="button" name="DelVoiceNumber_1" id="DelVoiceNumber_1" class="delContact delHide" onclick="fnDelVoiceNumber('VoiceNumnerPanel1','1,');">	
								</cfif>
							</div>
							<div class="voice-add">							
								<input type="button" onclick="fnAddVoiceNumber()" id="AddVoiceNumber" name="AddVoiceNumber" class="addContact mTop43">									
							</div>	
							<input type="hidden" id="VoiceNumberList" name="VoiceNumberList" value="<cfoutput>#voicenumberlist#</cfoutput>" data-init="<cfoutput>#voicenumberlist#</cfoutput>">
						</div>
						<div class="clear"></div>
					</cfif>
					<cfif GetCPPSETUPQUERY.SMSMETHOD_TI EQ 1>
						<cfset smsArray = data.SMS>
						<cfset smsCheck = "">
						<cfif ArrayLen(smsArray) GT 0>
							<cfset smsCheck = 'checked="true"'>
						</cfif>
						<div class="inputcontent mtop10">
							<div class="box">
								<div class="left_box">
									<input type = "checkbox" id="smsCheck" name="smsCheck" value="1" class="removeCheck" <cfoutput>#smsCheck#</cfoutput> >
									<label for="smsCheck">SMS Number</label>
								</div>
								<div class="right_box" id="inpSMSNumber">
									<cfset smsnumberlist = "">
									<cfset del_sms_class = "delHide">
									<cfif ArrayLen(smsArray) GT 1>
										<cfset del_sms_class = "delShow">
									</cfif>	
                                    
                                    <!--- Always show - new ui for opt out --->
                                    <cfset del_sms_class = "delShow">
                                    
									<cfset inputClass = "input_box">
									<cfif ArrayLen(smsArray) GT 0>
										<cfset j = 1>													
										<cfloop array="#smsArray#" index="sms">
											<cfif j GT 1>
												<cfset inputClass = "input_box_last">
											</cfif>													
											<div id="SMSNumnerPanel<cfoutput>#j#</cfoutput>">
												<input type="text" name="smsText_<cfoutput>#j#</cfoutput>" id="smsText_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#sms#</cfoutput>" >
											</div>
											<cfset smsnumberlist = smsnumberlist & j &",">
											<cfset j++>
										</cfloop>
									<cfelse>
										<div id="SMSNumnerPanel1">														
											<input type="text" name="smsText_1" id="smsText_1" class="input_box input_mask mask_phone" size="4" value="" >
										</div>
										<cfset smsnumberlist = "1,">
									</cfif>
								</div>
							</div>
							<div id="actionSms" class="action-sms-panel">
								<cfif ArrayLen(smsArray) GT 0>
									<cfset j = 1>
									<cfloop array="#smsArray#" index="sms">														
										<input type="button" name="DelSMSNumber_<cfoutput>#j#</cfoutput>" id="DelSMSNumber_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_sms_class#</cfoutput>" onclick="fnDelSMSNumber('SMSNumnerPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
										<cfset j++>
									</cfloop>
								<cfelse>
									<input type="button" name="DelSMSNumber_1" id="DelSMSNumber_1" class="delContact delHide" onclick="fnDelSMSNumber('SMSNumnerPanel1','1,');">	
								</cfif>
							</div>
							<div class="sms-add">
								<input type="button" onclick="fnAddSMSNumber()" id="AddSMSNumber" name="AddSMSNumber" class="addContact mTop43">
							</div>	
							<input type="hidden" id="SMSNumberList" name="SMSNumberList" value="<cfoutput>#smsnumberlist#</cfoutput>" data-init="<cfoutput>#smsnumberlist#</cfoutput>">
						</div>
						<div class="clear"></div>
					</cfif>
					<cfif GetCPPSETUPQUERY.EMAILMETHOD_TI EQ 1>
						<cfset emailArray = data.EMAIL>
						<cfset emailCheck = "">
						<cfif ArrayLen(emailArray) GT 0>
							<cfset emailCheck = 'checked="true"'>
						</cfif>
						
						<div class="inputcontent mtop10">
							<div class="box">
								<div class="left_box">
									<input type = "checkbox" id="emailCheck" name="emailCheck" value="1" class="removeCheck" <cfoutput>#emailCheck#</cfoutput> >
									<label for="emailCheck">Email Address</label>
								</div>
								<div class="right_box" id="inpEmail">
								<cfset emailnumberlist = "">
								<cfset del_email_class = "delHide">
									<cfif ArrayLen(emailArray) GT 1>
										<cfset del_email_class = "delShow">
									</cfif>	
                                    
                                    <!--- Always show - new ui for opt out --->
                                    <cfset del_email_class = "delShow">                                                
                                    
									<cfset inputClass = "input_box">
									<cfif ArrayLen(emailArray) GT 0>
										<cfset j = 1>													
										<cfloop array="#emailArray#" index="email">
											<cfif j GT 1>
												<cfset inputClass = "input_box_last">
											</cfif>													
											<div id="EmailPanel<cfoutput>#j#</cfoutput>">
												<input type="text" name="email_<cfoutput>#j#</cfoutput>" id="email_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#email#</cfoutput>" >
											</div>
											<cfset emailnumberlist = emailnumberlist & j &",">
											<cfset j++>
										</cfloop>
									<cfelse>
										<div id="EmailPanel1">														
											<input type="text" name="email_1" id="email_1" class="input_box" size="4" value="" >
										</div>
										<cfset emailnumberlist = "1,">
									</cfif>
								</div>
							</div>
							<div id="actionEmail" class="action-email-panel">
								<cfif ArrayLen(emailArray) GT 0>
									<cfset j = 1>
									<cfloop array="#emailArray#" index="email">														
										<input type="button" name="DelEmail_<cfoutput>#j#</cfoutput>" id="DelEmail_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_email_class#</cfoutput>" onclick="fnDelEmail('EmailPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
										<cfset j++>
									</cfloop>
								<cfelse>
									<input type="button" name="DelEmail_1" id="DelEmail_1" class="delContact delHide" onclick="fnDelEmail('EmailPanel1','1,');">	
								</cfif>
							</div>
							<div class="email-add">
								<input type="button" onclick="fnAddEmail()" id="AddEmail" name="AddEmail" class="addContact mTop43">
							</div>	
							<input type="hidden" id="EmailList" name="EmailList" value="<cfoutput>#emailnumberlist#</cfoutput>" data-init="<cfoutput>#emailnumberlist#</cfoutput>">
						</div>
					</cfif>
					<cfset i++>
					<cfset indexWithOutHtml++>
				</cfif>
				<div class="mtop10">
					</div>
				<cfif index eq 4 AND GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 1 >
					<div class="clear-both"></div>
					<div class="stepCPP mtop47"><!---<span class="step">Step <cfoutput>#indexWithOutHtml#</cfoutput>: ---><cfoutput>#HTMLStringFormat(TitleStep4)#</cfoutput><!---</span>---></div>
					<div class="stepNote clear-both">
						<cfoutput>#HTMLStringFormat(DescriptionStep4)#</cfoutput>
					</div>
					<div class="domain"> 
						<div class="scrollable clear-both">
							<select name="language" id="language" class="select1 select-language" style="border:1px solid red;">
								<cfloop array="#CppSetupLanguage#" index="language">
									<option value="<cfoutput>#language#</cfoutput>"><cfoutput>#language#</cfoutput></option>	
								</cfloop>
							</select>
						</div>
					</div>
					<div class="clear-both" style=""></div>
					<cfset i++>
					<cfset indexWithOutHtml++>
				</cfif>
				
				<cfif Find("5.",index) GT 0>
					<cfset tmpArray = arrayNew(1)/>
					<cfset tmpArray = ListToArray(index,".")>
					<cfinvoke 
						component="#LocalSessionDotPath#.cfc.ire.marketingcpp" 
						method="GetCustomHtmlForPreview" 
						returnvariable="GetCustomHtml">
						<cfinvokeargument name="customHtmlId" value="#tmpArray[2]#">
					</cfinvoke>
					<cfif #GetCustomHtml.RXRESULTCODE# EQ 1>
						<!---<div class="stepCPP mtop38"><span class="step">Step <cfoutput>#i#</cfoutput>:</span><span class="stepDes"></span></div>--->
						<div class="clear-both">
							<cfoutput>#replaceList(GetCustomHtml.HTML, "&lt;,&gt;,&amp;,&quot;,&##39;", '<,>,&,",''')#</cfoutput>
						</div>
					</cfif>
				</cfif>

                <cfif Find("6.",index) GT 0>
                    <cfinvoke method="GetCdfCppByID" component="#Session.SessionCFCPath#.ire.marketingcpp" returnvariable="getCdfHtml">
                            <cfinvokeargument name="inpCPPUUID" value="#cppUUID#">
                    </cfinvoke>
                    <cfset nameStep = "step" & index>
                    <cfif IsDefined("getCdfHtml.FIELD") AND ArrayLen(getCdfHtml.FIELD)>
                        <cfset desc = '' />
                        <cfloop array="#getCdfHtml.FIELD#" index="fieldItem">
                            <cfif nameStep EQ fieldItem.IDSTEP>
                                <cfset desc = HTMLStringFormat(fieldItem.DESCRIPTION) />
                                <cfbreak/>
                            </cfif>
                        </cfloop>
                        <cfloop array="#getCdfHtml.FIELD#" index="fieldItem">
                            <cfif nameStep EQ fieldItem.IDSTEP>
                                <div class="CDF_BOX mtop10">
                                    <div class="CDF_DESC">
                                        <label><cfoutput>#desc#</cfoutput></label>
                                    </div>
                                    <div class="CDF_SELECT">
                                        <cfif fieldItem.FIELDTPYE EQ 1>
                                            <select name="default_value" class="select1 select-default-value">
                                                <cfloop array="#getCdfHtml.DEFAULTFIELDS#" index="value">
                                                    <cfif fieldItem.ID EQ value.CDFID>
                                                        <option value="<cfoutput>#value.ID#</cfoutput>"><cfoutput>#value.VALUE#</cfoutput></option>
                                                    </cfif>
                                                </cfloop>
                                            </select>
                                        <cfelse>
                                            <input  class="input_box" name="<cfoutput>#fieldItem.ID#</cfoutput>" style="text">
                                        </cfif>
                                    </div>
                                </div>
                            </cfif>
                        </cfloop>
                    </cfif>
                </cfif>
			</div>
            <div class="clear-both"></div>
            <div class="mtop10"> </div>
		</cfloop>
		<!---<div class="stepTitle">			
			Step1: Contact Preference
		</div>
		<div class="stepDes mTop15">
			Please select which services you wish to stay informed about by checking the box next to the selected service.
		</div>
		<div class="preferenceBox">
			<cfset preItem = 0>	
			<cfloop array="#GetPreferenceQuery.DATA#" index="PreferenceItem">
				<cfset preItem++>
				<div class="preferenceItem">
					<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
						<input class="custominput" type="checkbox" id="preference<cfoutput>#preItem#</cfoutput>" checked="checked" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>">
					<cfelse>
						<input class="custominput" type="radio" id="preference<cfoutput>#preItem#</cfoutput>" checked="checked" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>">
					</cfif>	
					<label for="preference<cfoutput>#preItem#</cfoutput>"><cfoutput>#PreferenceItem.Desc_vch#</cfoutput></label>
				</div>	
			</cfloop>
		</div>
		<div class="stepTitle mTop45">			
			Step2: Contact Method
		</div>
		<div class="stepDes mTop15">
			Please check the contact method you prefer to be contacted by for the above services.
		</div>
		<div class="inputcontent mTop20">
			<div class="box">
				<div class="left_box">
					<input class="custominput" type="checkbox" checked="checked" value="1" name="voiceCheck" id="voiceCheck">
					<label for="voiceCheck" class="">Voice Number</label>					
				</div>
				<div id="inpVoiceNumber" class="right_box">
					<div id="VoiceNumnerPanel1">														
						<input type="text" name="voiceText_1" id="voiceText_1" class="input_box maskPhone" size="4" value="" >
					</div>
				</div>
			</div>			
			<div id="actionVoice" class="actionPanel">
				<input type="button" name="DelVoiceNumber_1" id="DelVoiceNumber_1" class="delContact delHide" onclick="fnDelVoiceNumber('VoiceNumnerPanel1','1,');">
			</div>
			<div class="actionPanel">
				<input type="button" class="addContact" name="AddVoiceNumber" onclick="fnAddVoiceNumber()" id="AddVoiceNumber">
				<input type="hidden" id="VoiceNumberList" name="VoiceNumberList" value="1," data-init="1,">						
			</div>
		</div>
		<div class="clear"></div>
		<div class="inputcontent mTop10">
			<div class="box">
				<div class="left_box">
					<input class="custominput" type="checkbox" checked="checked" value="1" name="smsCheck" id="smsCheck">
					<label for="smsCheck" class="">SMS Number</label>					
				</div>
				<div id="inpSMSNumber" class="right_box">	
					<div id="SMSNumnerPanel1">	
						<input type="text" value="" size="4" class="input_box maskPhone" id="smsText_1" name="smsText_1">
					</div>					
				</div>
			</div>			
			<div id="actionSms" class="actionPanel">
				<input type="button" name="DelSMSNumber_1" id="DelSMSNumber_1" class="delContact delHide" onclick="fnDelSMSNumber('SMSNumnerPanel1','1,');">
			</div>
			<div class="actionPanel">
				<input type="button" class="addContact" name="AddSMSNumber" id="AddSMSNumber" onclick="fnAddSMSNumber()">
				<input type="hidden" id="SMSNumberList" name="SMSNumberList" value="1," data-init="1,">						
			</div>
		</div>
		<div class="clear"></div>
		<div class="inputcontent mTop10">
			<div class="box">
				<div class="left_box">
					<input class="custominput" type="checkbox" checked="checked" value="1" name="emailCheck" id="emailCheck">
					<label for="emailCheck" class="">Email Address</label>					
				</div>
				<div id="inpEmail" class="right_box">
					<div id="EmailPanel1">		
						<input type="text" value="" size="4" class="input_box maskPhone" id="email_1" name="email_1">
					</div>
				</div>
			</div>
			<div id="actionEmail" class="actionPanel">
				<input type="button" name="DelEmail_1" id="DelEmail_1" class="delContact delHide" onclick="fnDelEmail('EmailPanel1','1,');">
			</div>
			<div class="actionPanel">
				<input type="button" class="addContact" name="AddEmail" id="AddEmail" onclick="fnAddEmail()">
				<input type="hidden" id="EmailList" name="EmailList" value="1," data-init="1,">						
			</div>
		</div>
		<div class="clear"></div>--->
	</div>
</div>

<script>
	function CheckEvent(){
		var object = $("input[name='preferenceCheck']:checked");
		if(object.length > 0){
			$("input[name='preferenceCheck']").each(function(){
				$(this).removeAttr("checked");
				$('label[for='+$(this).attr('id')+']').removeClass('checked');				
			});
			$("#CheckEvent").html("Check all");	
		}
		else{
			$("input[name='preferenceCheck']").each(function(){
				$(this).attr("checked", "checked");
				$('label[for='+$(this).attr('id')+']').addClass('checked');				
			});
			$("#CheckEvent").html("Uncheck all");	
		}		
	}
</script>
<style type="text/css">
	.domain {
	    float: left;
	    height: auto;
	    padding-bottom: 0;
	    text-align: left;
	    width: 100%;
	    padding-top: 19px;
	    color: #666666;
	    font-size: 1em;
		padding-left:0px;
	}
	.ui-selectmenu {
	    border: 1px solid #CCCCCC;
	}
	.scrollable {
	    float: left;
	    height: 29px;
	    width: 250px;
	}
	#boxPreview a:hover, a:active
	{
		outline 0 none;	
	}
	.ui-state-hover{
		
	}
	.stepCPP {
	    float: left;	  
	    font-size: 1.4em;
	    text-align: left;
	    text-transform: none;
		word-break: break-all;
	}
	.clear-both{
		clear:both;
	}
	.custom-checkbox, .custom-radio{
		float:none;
	}
	.select-language{
		border: 1px solid red;
		display: none;
		float:left;
	}
	
	.clear{
		clear:both;
	}
	.action-sms-panel{
		width:24px;
		float:left;			
		margin-top:35px;
		
	}
	.action-voice-panel{
		width:24px;
		float:left;			
		margin-top:35px;
		
	}
	.action-email-panel{
		width:24px;
		float:left;			
		margin-top:35px;
		
	}
	.voice-add{
		float:left;
	}
	.sms-add{
		float:left;
	}
	.email-add{
		float:left;
	}
	.step ol{
		margin-left:40px;
	}	
	.stepNote ol{
		margin-left:40px;
	}
</style>

<cffunction name="HTMLStringFormat" access="public" output="No" >
	<cfargument name="string" type="string" required="Yes" >
	<cfset special = "&ndash;,&mdash;,&iexcl;,&iquest;,&quot;,&ldquo;,&rdquo;,&lsquo;,&rsquo;,&laquo;,&raquo;,&nbsp;,&amp;,&cent;,&copy;,&divide;,&gt;,&lt;,&micro;,&middot;,&para;,&plusmn;,&euro;,&pound;,&reg;,&sect;,&trade;,&yen;,&aacute;,&Aacute;,&agrave;,&Agrave;,&acirc;,&Acirc;,&aring;,&Aring;,&atilde;,&Atilde;,&auml;,&Auml;,&aelig;,&AElig;,&ccedil;,&Ccedil;,&eacute;,&Eacute;,&egrave;,&Egrave;,&ecirc;,&Ecirc;,&euml;,&Euml;,&iacute;,&Iacute;,&igrave;,&Igrave;,&icirc;,&Icirc;,&iuml;,&Iuml;,&ntilde;,&Ntilde;,&oacute;,&Oacute;,&ograve;,&Ograve;,&ocirc;,&Ocirc;,&oslash;,&Oslash;,&otilde;,&Otilde;,&ouml;,&Ouml;,&szlig;,&uacute;,&Uacute;,&ugrave;,&Ugrave;,&ucirc;,&Ucirc;,&uuml;,&Uuml;,&yuml;">
	<cfset normal = "#chr(8211)#,#chr(8212)#,#chr(161)#,#chr(191)#,#chr(34)#,#chr(8220)#,#chr(8221)#,#chr(39)#,#chr(39)#,#chr(171)#,#chr(187)#,#chr(32)#,#chr(38)#,#chr(162)#,#chr(169)#,#chr(247)#,#chr(62)#,#chr(60)#,#chr(181)#,#chr(183)#,#chr(182)#,#chr(177)#,#chr(8364)#,#chr(163)#,#chr(174)#,#chr(167)#,#chr(8482)#,#chr(165)#,#chr(225)#,#chr(193)#,#chr(224)#,#chr(192)#,#chr(226)#,#chr(194)#,#chr(229)#,#chr(197)#,#chr(227)#,#chr(195)#,#chr(228)#,#chr(196)#,#chr(230)#,#chr(198)#,#chr(231)#,#chr(199)#,#chr(233)#,#chr(201)#,#chr(232)#,#chr(200)#,#chr(234)#,#chr(202)#,#chr(235)#,#chr(203)#,#chr(237)#,#chr(205)#,#chr(236)#,#chr(204)#,#chr(238)#,#chr(206)#,#chr(239)#,#chr(207)#,#chr(241)#,#chr(209)#,#chr(243)#,#chr(211)#,#chr(242)#,#chr(210)#,#chr(244)#,#chr(212)#,#chr(248)#,#chr(216)#,#chr(245)#,#chr(213)#,#chr(246)#,#chr(214)#,#chr(223)#,#chr(250)#,#chr(218)#,#chr(249)#,#chr(217)#,#chr(251)#,#chr(219)#,#chr(252)#,#chr(220)#,#chr(255)#">
	<cfset formated = ReplaceList(arguments.string, special, normal)>
	<cfreturn formated>
</cffunction>