﻿<div class="cppLabel">
	How will you display your portal?
</div>
<div class="boxLarge mTop10 mBottom40">
	<div class="left_box">Display Portal
		<div id="DisplayInfo" class="info_box"></div>
	</div>
	<div class="right_box">
		<!---<input type="text" class="input_box" name="inpDescription" id="inpDescription">--->
		<select name="displayType_ti" id="displayType_ti">
			<option value="0" <cfif templateData.displayType_ti EQ 0> selected="selected"</cfif>>Embedded</option>
			<option value="1" <cfif templateData.displayType_ti EQ 1> selected="selected"</cfif>>External</option>
		</select>
	</div>
</div>
<div class="cppLabel">
	Domain Portal
</div>
<div class="boxLarge mTop10 mBottom40">
	<div class="left_box">Domain
		<div id="DomainInfo" class="info_box"></div>
	</div>
	<div class="right_box">
		<select name="dd" id="dd">
			<option value="0">CustomerPreferencePortal.com</option>
			<option value="1">CustomerPreferencePortal.com</option>
			<option value="1">CustomerPreferencePortal.com</option>
		</select>
	</div>
</div>
<div class="cppLabel">
	Set your portal's size
</div>
<div class="boxLarge mTop10">
	<div class="left_box">Portal Size
		<div id="SizeInfo" class="info_box"></div>
	</div>
	<div class="right_box">
		<select name="sizeType_int" id="sizeType_int" onchange="NewChangeEmbed(this);">
			<option value="0" <cfif templateData.sizeType_int EQ 0> selected="selected"</cfif>>Small</option>
			<option value="1" <cfif templateData.sizeType_int EQ 1> selected="selected"</cfif>>Medium</option>
			<option value="2" <cfif templateData.sizeType_int EQ 2> selected="selected"</cfif>>Large</option>
			<option value="3" <cfif templateData.sizeType_int EQ 3> selected="selected"</cfif>>Custom</option>
		</select>
	</div>
</div>
<div class="customSize mTop10" id="CustomDimesision">
	<div class="titleLeft">
		Custom Dimensisions
	</div>
	<div class="boxSmall">
		<div class="left_box">Width (px)
		</div>
		<div class="right_box">
			<input type="text" class="input_box" name="width_int" id="width_int" value = "<cfoutput>#templateData.width_int#</cfoutput>">
		</div>
	</div>
	<div class="boxSmall mLeft12">
		<div class="left_box">Height (px)
		</div>
		<div class="right_box">
			<input type="text" class="input_box" name="height_int" id="height_int" value = "<cfoutput>#templateData.height_int#</cfoutput>">
		</div>
	</div>
</div>
<div style="clear:both;"></div>

<div class="cppLabel mTop40">
	Select and customize template
</div>
<div class="boxLarge mTop10">
	<div class="left_box">Template
		<div id="TempalteInfo" class="info_box"></div>
	</div>
	<div class="right_box">
		<select name="Plain" id="Plain" onchange="NewChangeTemplateType(this);">
			<option value="0" <cfif templateType EQ 0> selected="selected"</cfif>>Plain</option>
			<option value="1" <cfif templateType EQ 1> selected="selected"</cfif>>Blue</option>
			<option value="2" <cfif templateType EQ 2> selected="selected"</cfif>>Orange</option>
			<option value="3" <cfif templateType EQ 3> selected="selected"</cfif>>Red</option>
			<option value="4" <cfif templateType EQ 4> selected="selected"</cfif>>Green</option>
			<option value="5" <cfif templateType EQ 5> selected="selected"</cfif>>Custom</option>
		</select>
	</div>
</div>

<div class="customSizeTemplate mTop10">
	<div class="titleLeft">
		<span class="customLabel">
		Font	</span>
	</div>
	<div class="customTemplate">
		<!---<div class="boxMedium mTop10">
			<div class="left_box">Font Size
			</div>
			<div class="right_box">
				<select name="dllFontSize" id="dllFontSize">
					<cfloop from="1" to="60" index="index">
						<option value="<cfoutput>#index#</cfoutput>"><cfoutput>#index#</cfoutput></option>
					</cfloop>
				</select>
			</div>
		</div>--->
		<div class="customSize mTop10" id="CustomDimesision">
			<div class="titleLeft">

			</div>
			<div class="boxSmall">
				<div class="left_box">Font Size
				</div>
				<div class="right_box">
					<select name="dllFontSize" id="dllFontSize" onchange="ApplyCssToPreviewBox();">
					<cfloop from="1" to="40" index="index">
						<option value="<cfoutput>#index#</cfoutput>"><cfoutput>#index#</cfoutput></option>
					</cfloop>
				</select>
				</div>
			</div>
			<div class="boxSmall mLeft12">
				<div class="left_box">Font Type
				</div>
				<div class="right_box">
					<select name="dllFontType" id="dllFontType" onchange="ApplyCssToPreviewBox();">
					<option value="pt">pt</option>
					<option value="px">px</option>
					<option value="inch">inch</option>
				</select>
				</div>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Font Family
			</div>
			<div class="right_box">
				<select name="dllFontFamily" id="dllFontFamily" onchange="ApplyCssToPreviewBox();">
					<option value="None">None</option>
					<option value="Arial Black, Gadget, sans-serif">Arial Black, Gadget, sans-serif</option>
					<option value="Bookman Old Style, serif">Bookman Old Style, serif</option>
					<option value="Comic Sans MS, cursive">Comic Sans MS, cursive</option>
					<option value="Courier New, Courier, monospace">Courier New, Courier, monospace</option>
					<option value="Garamond, serif">Garamond, serif</option>
					<option value="Arial, Sans-Serif">Arial, Sans-Serif</option>
				</select>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Font Weight
			</div>
			<div class="right_box">
				<select name="dllFontWeight" id="dllFontWeight" onchange="ApplyCssToPreviewBox();">
					<option value="Normal">Normal</option>
					<option value="Lighter">Lighter</option>
					<option value="Bold">Bold</option>
					<option value="Bolder">Bolder</option>
				</select>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Font Style
			</div>
			<div class="right_box">
				<select name="dllFontStyle" id="dllFontStyle" onchange="ApplyCssToPreviewBox();">
					<option value="Normal">Normal</option>
					<option value="Italic">Italic</option>
					<option value="Oblique">Oblique</option>
					<option value="Inherit">Inherit</option>
				</select>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Font Variant
			</div>
			<div class="right_box">
				<select name="dllFontVariant" id="dllFontVariant" onchange="ApplyCssToPreviewBox();">
					<option value="Normal">Normal</option>
					<option value="Small-caps">Small-caps</option>
					<option value="Inherit">Inherit</option>
				</select>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Font Color
			</div>
			<div class="right_box">
				<input type="text" class="color_input color colorpickinput" id="FontColor" />
			</div>
		</div>
	</div>
</div>

<div class="customSizeTemplate mTop20">
	<div class="titleLeft">
		<span class="customLabel">
		Background	</span>
	</div>
	<div class="customTemplate">
		<div class="boxMedium">
			<div class="left_box">Background Color
			</div>
			<div class="right_box">
				<input type="text" class="color_input color colorpickinput" id="BackgroundColor" />
			</div>
		</div>
		<div class="boxMedium mTop10 imgPreview">
			<div class="left_box imgPreview">Background Image
				<div class="tempImage">
					<span class='logo_preview' id='ImgPreview'></span>
				</div>
			</div>
			<div class="right_box">
				<span id="colorpick">
					<input TYPE="hidden" name="InpImgServerFile" id="InpImgServerFile" value="" />
					<input type="file" name="InpImgFile" id="InpImgFile" class="field_margin" onchange="ajaxupload();"/>
				</span>
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Image Repeat
			</div>
			<div class="right_box">
				<select name="dllImageRepeat" id="dllImageRepeat" onchange="ApplyCssToPreviewBox();">
					<option value="Image-Repeat">Image-Repeat</option>
					<option value="Yes">Yes</option>
					<option value="No">No</option>
				</select>
			</div>
		</div>
	</div>
</div>
<div class="customSizeTemplate mTop20 mBottom40">
	<div class="titleLeft">
		<span class="customLabel">
		Border	</span>
	</div>
	<div class="customTemplate">
		<div class="boxMedium">
			<div class="left_box">Border Color
			</div>
			<div class="right_box">
				<input type="text" class="color_input color colorpickinput" id="BorderColor" />
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Border width (px)
			</div>
			<div class="right_box">
				<input type="text" onchange="ApplyCssToPreviewBox();" class="input_box" name="BorderWidth" id="BorderWidth">
			</div>
		</div>
		<div class="boxMedium mTop10">
			<div class="left_box">Corner Radius (px)
			</div>
			<div class="right_box">
				<input type="text" onchange="ApplyCssToPreviewBox();" class="input_box" name="BorderRadius" id="BorderRadius">
			</div>
		</div>
	</div>
</div>
<input type="hidden" id ="templateId">
<input type="hidden" id ="templateName">