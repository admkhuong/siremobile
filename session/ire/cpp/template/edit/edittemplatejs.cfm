<script type="text/javascript">
	
	$('#colorSelector').ColorPicker({
		color: $('#fontColorTemp').val(),
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorSelector div').css('backgroundColor', '#' + hex);
			$('#fontColorTemp').val('#' + hex);
		}
	});
	
	$('#fontColorTemp').ColorPicker({flat: true});
	
	function editTemplate(parent, children){
		$('#parentTemp').val(parent);
		$('#childrenTemp').val(children);
		$('#fontTemp').val($('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('font-family'));
		$('#fontSizeTemp').val($('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('font-size'));
		$('#fontColorTemp').val($('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('color'));
		
		$('#colorSelector div').css('backgroundColor', $('#fontColorTemp').val());
		
		ShowDialog('templateOption');
	}
	
	function changeTemplate(){
		$('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('font-size',$('#fontSizeTemp').val());
		$('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('font-family',$('#fontTemp').val());
		$('.'+ $('#parentTemp').val() + $('#childrenTemp').val()).css('color',$('#fontColorTemp').val());
		CloseDialog('templateOption');
	}
	
	function saveTemplate(){
		
		var extraData ={
			
			templateId : '<cfoutput>#templateId#</cfoutput>',
			imgServerFile: $('#inpImgServerFile').val(),
			title: $('#tempTitle').val(),
			description: $('#tempDesc').val(),
           		
      		preTitleFont: $('.preTitle').css('font-family'),
      		preTitleSize: $('.preTitle').css('font-size'),
      		preTitleColor: $('.preTitle').css('color'),
      		
      		preDescFont: $('.preDesc').css('font-family'),
      		preDescSize: $('.preDesc').css('font-size'),
      		preDescColor: $('.preDesc').css('color'),
      		
      		preContentFont: $('.preContent').css('font-family'),
      		preContentSize: $('.preContent').css('font-size'),
      		preContentColor: $('.preContent').css('color'),
      		
      		contactTitleFont: $('.contactTitle').css('font-family'),
      		contactTitleSize: $('.contactTitle').css('font-size'),
      		contactTitleColor: $('.contactTitle').css('color'),
      		
      		contactDescFont: $('.contactDesc').css('font-family'),
      		contactDescSize: $('.contactDesc').css('font-size'),
      		contactDescColor: $('.contactDesc').css('color'),
      		
      		contactLabelFont: $('.contactLabel').css('font-family'),
      		contactLabelSize: $('.contactLabel').css('font-size'),
      		contactLabelColor: $('.contactLabel').css('color'),
      		
      		langTitleFont: $('.langTitle').css('font-family'),
      		langTitleSize: $('.langTitle').css('font-size'),
      		langTitleColor: $('.langTitle').css('color'),
      		
      		langDescFont: $('.langDesc').css('font-family'),
      		langDescSize: $('.langDesc').css('font-size'),
      		langDescColor: $('.langDesc').css('color'),
      		
      		langLabelFont: $('.langLabel').css('font-family'),
      		langLabelSize: $('.langLabel').css('font-size'),
      		langLabelColor: $('.langLabel').css('color'),
      		
      		langContentFont: $('.langContent').css('font-family'),
      		langContentSize: $('.langContent').css('font-size'),
      		langContentColor: $('.langContent').css('color')
		}
		/*
		var extraParram ='';
		
		$.each(extraData, function(i, item) {
		    extraParram += "'" + i + "'" + item;
		});
		*/
		
		$.ajax({
           	type: "POST",
           	enctype: "multipart/form-data",
           	url:"<cfoutput>#rootUrl#/#sessionPath#/cfc/ire/marketingcpp.cfc</cfoutput>?method=updateTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           	dataType: "json", 
           	data: extraData,
           	success: function(d){
	           	if (d.ROWCOUNT > 0) 
				{													
				
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						if(d.DATA.RXRESULTCODE[0] > 0)
						{
	           				jAlert(d.DATA.MESSAGE[0], "Success!", function(result){
	           					if(result){
	           						<cfoutput>
										var params = {
											'CPPUUID':'#CPPUUID#'
										<cfif page GT 0>
											,'page': '#page#'												
										</cfif>	
										};
										post_to_url('#rootUrl#/#sessionPath#/ire/cpp/template', params, 'POST');
									</cfoutput>
	           					}
	           				});	
	           				
	           			}else{
							jAlert(d.DATA.MESSAGE[0], "Failure!");		
						}
	           		}
	           	}
           		$('#accepCondition').removeAttr('checked');
      		}
  		});
	}
	
	function ajaxupload() {
		type = 'image';
		inFileId = 'inpImgFile';
		rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		$.ajaxFileUpload
		(
			{
				url: '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ire/cpp/template/edit/act_UploadLogo?inpFileId='+inFileId ,
				secureuri:false,
				fileElementId:inFileId,
				dataType: 'json',				
				complete:function()
				{					
					$("#deleteImgFile").click(function(){
						deleteImage();
					});
				},				
				success: function (data, status)
				{	
					if(type == 'image'){
						
						//alert(data.fileSize);
						//alert(data.clientFileExt);
						if ((data.clientFileExt.toUpperCase() == 'JPG') || (data.clientFileExt.toUpperCase() == 'GIF') || (data.clientFileExt.toUpperCase() == 'PNG')) {
							$('#inpImgServerFile').val(data.UniqueFileName);
							$('#imgPreview').empty();
							$('#imgPreview').append('<img src="' +rootUrl + '/' + sessionPath +'/ire/cpp/template/act_previewImage?serverfileName='+ data.UniqueFileName +'&userId=<cfoutput>#Session.UserId#</cfoutput>" height="65"/>');
							$('#imgPreview').append('<img src="' +rootUrl + '/' + publicPath +'/images/mb/bin_empty.png" style="cursor: pointer" title="remove logo" id="deleteImgFile" rel="'+ data.UniqueFileName +'"/>');
						} else {
							alert('File type not accepted.');
						}
						
					}else{
					}
				},
				error: function (data, status, e)
				{						
					alert('general Error ' + e);	
				}
			}
		);
		return false;
	}
	
	function deleteImage(){
		type = 'image';
		inFileId = 'inpImgFile';
		rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		var img = $("#deleteImgFile").attr("rel");
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=DeleteCPPTemplateImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { inpFile : img},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.RESULT == 'SUCCESS'){
						$('#inpImgServerFile').val("");
						$('#inpImgFile').val("");
						$('#imgPreview').html('');
						$('#imgPreview').empty();
					}
				} 		
			});
		
	}
	
</script>