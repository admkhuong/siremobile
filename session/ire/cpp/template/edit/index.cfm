<cfparam name="templateId" default="0" >
<cfparam name="CPPUUID" default="" >
<cfparam name="page" default="0" >
<cfset templateId = trim(templateId)>

<cfset templateUrl ='#rootUrl#/#sessionPath#/ire/cpp/template?CPPUUID=#CPPUUID#'>
<cfif Page GT 0>
	<cfset templateUrl &= '&page=#page#'>
</cfif>

<cfoutput>
	<link rel="stylesheet" href="#rootUrl#/#PublicPath#/cppPortal/css/default.css">
	<link rel="stylesheet" href="#rootUrl#/#PublicPath#/js/colorPicker/css/colorpicker.css" type="text/css" />
   	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#PublicPath#/js/colorPicker/css/layout.css" />
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/ire/style4.css" />
	
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/colorPicker/js/colorpicker.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/colorPicker/js/eye.js"></script>
   	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/colorPicker/js/utils.js"></script>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload_new.js"></script>
</cfoutput>

<cftry>
	<cfinclude template="templateStyle.cfm">
	<cfcatch>
		<cfoutput>#cfcatch.message#</cfoutput>
		<cfexit>
	</cfcatch>
</cftry>
<cfform method="post" name="cppLaunchForm" id="cppTemplateForm" enctype="multipart/form-data">
	
	<div id="containerTemplate">
		
		<div class = "cppPortalRow tempRow">
			<label><strong>Title:</strong></label>
			<div>
				<input TYPE="text" name="title" id="tempTitle" value="<cfoutput>#GetCppTemp.title_vch#</cfoutput>" size ="50" />
			</div>
		</div>
		
		<div class = "cppPortalRow tempRow">
			<label><strong>Desctiption:</strong></label>
			<div>
				<textarea name="description" id="tempDesc" cols="38" rows="5"><cfoutput>#GetCppTemp.description_vch#</cfoutput></textarea>
			</div>
		</div>
	
		<div class = "cppPortalRow tempRow">
			<label><strong>Template image:</strong></label>
			<div class="tempImage">
				<input TYPE="hidden" name="inpImgServerFile" id="inpImgServerFile" value="<cfoutput>#GetCppTemp.image_vch#</cfoutput>" />
				<cfinput 
					name="inpImgFile" 
					id="inpImgFile" 
					type="file"
					class="field_margin"
					onchange="javascript:ajaxupload()"
					 />
				<cfset imgServerPath = "#rxdsWebProcessingPath#\cpp/U#Session.USERID#\#GetCppTemp.image_vch#">
				<cfif FileExists(imgServerPath)>
					<cfset 
						img_path = "#rootUrl#/#sessionPath#/ire/cpp/template/edit/act_previewImage?"
							 & "serverfileName=#GetCppTemp.image_vch#&userId=#Session.UserId#">
				<cfelse>
					<cfset GetCppTemp.image_vch = "">
				</cfif>
			</div>
			<div class="tempImage">
				<span class='logo_preview' id='imgPreview'>
					<cfif trim(GetCppTemp.image_vch) neq ''>
						<img src="<cfoutput>#img_path#</cfoutput>" height="65">
						<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/bin_empty.png" 
							id="deleteImgFile" 
							rel="<cfoutput>#GetCppTemp.image_vch#</cfoutput>"
							onClick="deleteImage(); return false;" 
							style="cursor: pointer" 
							title="remove logo"
							/>
					</cfif>
				</span>
			</div>
		</div>
		<div class = "cppPortalRow" id ="containerLaunch">
			
			<h2 class="cppStepTitle preTitle" >
				Step 1:
				<span> (Contact Preference)</span> 
				<a href="#" onclick="editTemplate('pre', 'Title'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
			</h2>
			<div class="cppPortalDesc preDesc" >
				Please select which services you wish to stay informed about by checking the box next to the selected service.
				<a href="#" onclick="editTemplate('pre', 'Desc'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
			</div>
			<div class="cppPortalContent">
				<div class="cppPortalContentRow preContent" >
					<cfinput type = "checkbox" name="preferenceCheck" class="removeCheck" readOnly="true">
					<cfoutput>Preference description</cfoutput>
					<a href="#" onclick="editTemplate('pre', 'Content'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
				</div>
			</div>
			
			<h2 class="cppStepTitle contactTitle">
				Step 2: <span>(Contact Method)</span>
				<a href="#" onclick="editTemplate('contact', 'Title'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
			</h2>
			<div class="cppPortalDesc contactDesc" >
				<p>
					Please check the contact method you prefer to be contacted by for the above services.
					<a href="#" onclick="editTemplate('contact', 'Desc'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
				</p>
			</div>
			<div class="cppPortalContent">
				<div class="cppPortalContentRow">
					<input type = "checkbox" name="voiceCheck" value="1" class="removeCheck">
					<label class="label VoiceNumberText contactLabel" >Voice Number:</label>
					<a href="#" onclick="editTemplate('contact', 'Label'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
					<input type="text" name="voiceText1" class="removeText" size="4">
					- <input type="text" name="voiceText2" class="removeText" size="4">
					- <input type="text" name="voiceText3" class="removeText" size="4">
				</div>
			
				<div class="cppPortalContentRow">
					<input type = "checkbox" name="smsCheck" value="1" class="removeCheck">
					<label class="label SMSNumberText contactLabel">SMS Number:</label>
					<input type="text" name="smsText1" class="removeText" size="4">
					- <input type="text" name="smsText2" name="smsText2" class="removeText" size="4">
					- <input type="text" name="smsText3" class="removeText" size="4">
				</div>
			
				<div class="cppPortalContentRow">
					<input type = "checkbox" name="emailCheck" value="1" class="removeCheck" >
					<label class ="contactLabel" >Email Address:</label>
					<input type="text" name="emailText" class="removeText emailText">
				</div>
			</div>
			
			<h2 class="cppStepTitle langTitle">
				Step 3: <span>(Desired Language)</span>
				<a href="#" onclick="editTemplate('lang', 'Title'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
			</h2>
			<div class="cppPortalDesc langDesc" >
				Please select the desired communication language you want your service announcements to be delivered.
				<a href="#" onclick="editTemplate('lang', 'Desc'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
			</div>
			<div class="cppPortalContent">
				<div class="cppPortalContentRow">
					<label class ="langLabel">Select language:</label>
					<a href="#" onclick="editTemplate('lang', 'Label'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
					<br/>
					<select name="language" class ="langContent" >
						<option value="1">English</option>
					</select>
					<a href="#" onclick="editTemplate('lang', 'Content'); return false;"><img src ="<cfoutput>#rootUrl#/#publicPath#/css/images/icons16x16/edit_16x16.png</cfoutput>"></a>
					
				</div>
			</div>
		</div>
				
		
		<div id="nextbutton">
			<a href="#" class="button blue small" onclick="saveTemplate(); return false;">Save</a>
			<a href="<cfoutput>#templateUrl#</cfoutput>" class="button blue small" >Back</a>
		</div>
	</div>
	
</cfform>

<div id="overlay" class="web_dialog_overlay"></div>
<!--- Invite User Popup --->
<div id="dialog_templateOption" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Change Template Options</span></strong></label> <span id="closeDialog" onclick="CloseDialog('templateOption');">Close</span> </div>
	<cfform action="" method="POST" id="cppTemplateForm">
		<div class="dialog_content" style="text-align:left;width:95%">
			<cfinput type="hidden" id="parentTemp" name="parentTemp">
			<cfinput type="hidden" id="childrenTemp" name="childrenTemp">
			<div class="templateOptionRow">
				<label>Font family:</label>
				<select id="fontTemp" name="fontTemp">
				    <option value = "Arial,Helvetica,sans-serif"> Arial, Helvetica, sans-serif </option>
				    <option value = "Arial Black,Gadget,sans-serif"> Arial Black, Gadget, sans-serif</option>
				    <option value = "Bookman Old Style"> Bookman Old Style, serif</option>
				    <option value = "Comic Sans MS"> Comic Sans MS, cursive</option>
				    <option value = "Courier,monospace"> Courier, monospace</option>
				    <option value = "Courier New,Courier,monospace"> Courier New, Courier, monospace</option>
				    <option value = "Garamond,serif"> Garamond, serif</option>
				    <option value = "Georgia,serif"> Georgia, serif</option>
				    <option value = "Impact,Charcoal,sans-serif"> Impact, Charcoal, sans-serif</option>
				    <option value = "Lucida Console,Monaco,monospace"> Lucida Console, Monaco, monospace</option>
				    <option value = "Lucida Sans Unicode,Lucida Grande,sans-serif"> Lucida Sans Unicode, Lucida Grande, sans-serif</option>
				    <option value = "MS Sans Serif,geneva,sans-serif"> MS Sans Serif, geneva, sans-serif</option>
				    <option value = "MS Serif,New York,sans-serif"> MS Serif, New York, sans-serif</option>
				    <option value = "Palatino Linotype,Book Antiqua,Palatino,serif"> Palatino Linotype, Book Antiqua, Palatino, serif</option>
				    <option value = "Symbol,sans-serif"> Symbol, sans-serif</option>
				    <option value = "Tahoma,geneva, sans-serif"> Tahoma, geneva, sans-serif</option>
				    <option value = "Times New Roman,Times,serif"> Times New Roman, Times, serif</option>
				    <option value = "Trebuchet MS,Helvetica,sans-serif"> Trebuchet MS, Helvetica, sans-serif</option>
				    <option value = "Verdana,geneva,sans-serif"> Verdana, geneva, sans-serif</option>
				    <option value = "Webdings,sans-serif"> Webdings, sans-serif</option>
				    <option value = "Wingdings,Zapf Dingbats,sans-serif"> Wingdings, Zapf Dingbats, sans-serif</option>
				</select>
			</div>
			
			<div class="templateOptionRow">
				<label class="fontSizeLabel">Font size:</label>
				<select id="fontSizeTemp" name="fontSizeTemp">
					<cfloop from="1" to="50" index="index">
						<option value = "<cfoutput>#index#</cfoutput>px"> <cfoutput>#index#</cfoutput>px </option>
					</cfloop>
				</select>
			</div>
			
			<div class="templateOptionRow">
				<label class="fontColorLabel">Font color:</label>
				<span id="colorSelector" >
					<div></div>
				</span>
				<cfinput type="hidden" id="fontColorTemp" name="fontColorTemp">
			</div>
		</div>
		<div class="dialog_content" style="width:70%">
			<button  
				type="button" 
				class="somadesign1"
				onClick="changeTemplate(); return false;"	
			>OK</button>
			<button 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('templateOption');"	
			>CANCEL</button>
		</div>
	</cfform>
</div>

<cfinclude template="editTemplateJs.cfm">