<cfparam name="CPPUUID" default="" >
<cfparam name="Page" default="0">
<cfparam name="showBack" default="true">
<cfset CPPUUID = trim(CPPUUID)>

<cfset nextUrl ='#rootUrl#/#sessionPath#/ire/cpp'>
<cfset backUrl ='#rootUrl#/#sessionPath#/ire/cpp/buildportal?CPPUUID=#CPPUUID#'>
<cfif Page GT 0>
	<cfset nextUrl &= '?page=#page#'>
	<cfset backUrl &= '&page=#page#'>
</cfif>
<link rel="stylesheet" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cppPortal/css/changeTemplate.css">
<link  type="text/css" rel="stylesheet" href="<cfoutput>#rootUrl#/#PublicPath#/css/ire/style4.css</cfoutput>" />
<script type="text/javascript">
	$(document).ready(function(){
		$('#subTitleText').text('<cfoutput>#Cpp_Title# >> Change Template </cfoutput>');
		$('#mainTitleText').text('ire');
		
	});
	
	function changeTemp(tempId){
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=changeTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			inpCPPUUID: '<cfoutput>#CPPUUID#</cfoutput>',
    			inptemplate: tempId
			},
            dataType: "json", 
            success: function(d) {
            	if(d.DATA.RXRESULTCODE[0] > 0){
           		 	<cfoutput>
						var params = {
							'page': '#page#'												
						};
						post_to_url('#rootUrl#/#sessionPath#/ire/cpp', params, 'POST');
					</cfoutput>
            	}else{
            		jAlert(d.DATA.MESSAGE[0],"Change Cpp Tempalte fail");
            	}
	          }
	     });
	     return false;
	}
	
	function deleteTemp(tempId){
		jConfirm('Do you want delete this tempalte?',"Delete Cpp Tempalte", function(result){
			if(result){
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=deleteTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:{
		    			inpCPPUUID: '<cfoutput>#CPPUUID#</cfoutput>',
		    			inptemplate: tempId
					},
		            dataType: "json", 
		            success: function(d) {
		            	if(d.DATA.RXRESULTCODE[0] > 0){
		           		 	location.reload();
		            	}else{
		            		jAlert(d.DATA.MESSAGE[0],"Delete Cpp Tempalte fail");
		            	}
			          }
			     });
			     return false;
			}
		});
	}
	
	function customHtml(tempId){
		
		$.ajax({
	    	type: "POST",
	    	url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=getTemplateData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	   		data:{
	   			inpCPPUUID: '<cfoutput>#CPPUUID#</cfoutput>',
	   			inpTemplateId: tempId
			},
        	dataType: "json", 
        	success: function(d) {
	           	if(d.DATA.RXRESULTCODE[0] > 0){
	       			
	       			$('#customHtmlTempId').val(tempId);
					
					if(d.DATA.CUSTOMHTML[0] != ''){
						$('#customHtml').val(d.DATA.CUSTOMHTML[0]);
					}else{
						$('#customHtml').val('<center> <a href ="link"> <img src = "image"></a><h1>My Preference Portal</h1><br/><br/> {%portal%} </center>');
					}
					
					tinyMCE.init({
						 mode : "none",
						 theme : "simple",
						 width: "800px",
						 height: "250px",
						 visual : false,
						 convert_urls : false,
						 mode: "none",
						 theme : "advanced",
						 theme_advanced_toolbar_location : "bottom",		 
						 setup : function(ed) { 
						   ed.onInit.add(function(ed) { 
						   		var dom = ed.dom;
					            var doc = ed.getDoc();
					        	
					        	doc.addEventListener('blur', function(e) {
					                content = tinyMCE.activeEditor.getContent();
					            }, false);
					            
					            doc.addEventListener('click', function(e) {
					            	content = tinyMCE.activeEditor.getContent();
					            	if(content == "<p>Please enter the text to be display</p>"){
						                tinyMCE.activeEditor.setContent("");
					            	}
					            }, false);
						   }); 
						 }
				    });
							
				    tinyMCE.execCommand('mceAddControl', false, 'customHtml');
				    
				    $('#customHtmlcontainer').show();
	           	}else{
	           		jAlert(d.DATA.MESSAGE[0],"Change custom HTML tempalte fail");
	           	}
	   		}
	    });
	     
	}
	
	function saveCustomHTML(){
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=saveCustomHtml&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			inpCPPUUID: '<cfoutput>#CPPUUID#</cfoutput>',
    			inpTemplateId: $('#customHtmlTempId').val(),
    			inpCustomHTML: tinyMCE.activeEditor.getContent()
			},
            dataType: "json", 
            success: function(d) {
            	if(d.DATA.RXRESULTCODE[0] > 0){
        			location.reload();
            	}else{
            		jAlert(d.DATA.MESSAGE[0],"Change custom HTML tempalte fail");
            	}
	          }
	     });
	     return false;
	}
	
</script>

<cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingcpp" method="getTemplate" returnvariable="getTemplate">
	<cfinvokeargument name="inpCPPUUID" value="#CPPUUID#">
</cfinvoke>

<cfif getTemplate.RXRESULTCODE LT 0>
	<cfoutput>#getTemplate.MESSAGE#</cfoutput>
	<cfexit>
</cfif>

<a href = "<cfoutput>#rootUrl#/#sessionPath#/ire/cpp/template/edit?templateId=0</cfoutput>">Create Template </a>
<cfset baseImagePath = '#rxdsWebProcessingPath#\cpp/U#Session.USERID#'>
<cfset AllTemp = getTemplate.AllTemplate>
<ul id="listTemp">
	<cfloop query="AllTemp">
		<li>
			<img class="tempImages" src="<cfoutput>#rootUrl#/#sessionPath#/ire/cpp/template/edit/act_previewImage?serverfileName=<cfif IsDefined("image_vch")>#image_vch#</cfif>&userId=#session.userId#</cfoutput>" >
			<div class="tempContent">
				<div class="templateTitle"><cfoutput><cfif IsDefined("title_vch")>#title_vch#</cfif></cfoutput></div>
				<div class="templateDesc"><cfoutput><cfif IsDefined("description_vch")>#description_vch#</cfif></cfoutput></div>
				<div class="useTemplate"><a href ="##" onclick="customHtml(<cfoutput>#templateId_int#</cfoutput>); return false;">Custom HTML </a></div>
				<cfif IsDefined("getTemplate.currentTemp")>
					<cfif getTemplate.currentTemp NEQ templateId_int>
						<div class="useTemplate"><a href ="##" onclick="changeTemp('<cfoutput>#templateId_int#</cfoutput>'); return false;">Use Template </a></div>
					</cfif>
				</cfif>				
				<div class="useTemplate"><a href ="<cfoutput>#rootUrl#/#sessionPath#/ire/cpp/template/edit?templateId=#templateId_int#&CPPUUID=#CPPUUID#</cfoutput>" >Edit Template </a></div>
				<div class="useTemplate"><a href ="<cfoutput>#rootUrl#/#sessionPath#/ire/cpp/template/newui?templateId=#templateId_int#&CPPUUID=#CPPUUID#</cfoutput>" >Edit Template UI</a></div>
				<div class="useTemplate">
					<a href ="#" onclick="deleteTemp('<cfoutput>#templateId_int#</cfoutput>'); return false;" >
						<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/bin_empty.png"/>
					</a>
				</div>
			</div>
		</li>
	</cfloop>
</ul>

<div style="clear: both"></div>
<div id="customHtmlcontainer" style="display:none" >
	<div id="customHtmlTitle">Custom HTML</div>
	<div id="customHtmlDesc">You may include the portal any where on the page by typing {%portal%}. You may include it only once.</div>
	<input id="customHtmlTempId" type="hidden">
	<textarea id="customHtml"></textarea>
	<input type="button" value ="Save" onclick = "saveCustomHTML()">
</div>

<div id="backbutton">
	<div id="nextbutton">
		<a href="<cfoutput>#nextUrl#</cfoutput>" class="button blue small" >Finish</a>
		<cfif showBack>
			<a href="<cfoutput>#backUrl#</cfoutput>" class="button blue small" >Back</a>
		</cfif>
	</div>
</div>