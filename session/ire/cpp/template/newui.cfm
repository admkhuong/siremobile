<cfinclude template="../../../../public/paths.cfm" >
<cfparam name="cppUUID" default="">
<cfparam name="page" default="0">
<cfparam name="showBack" default="true">
<cfoutput>
	<style>		
		@import url('#rootUrl#/#publicPath#/css/ire/newuicss.css');
	</style>
</cfoutput>		
<cfset nextUrl ='#rootUrl#/#sessionPath#/ire/cpp'>
<cfset backUrl ='#rootUrl#/#sessionPath#/ire/cpp/buildportal?CPPUUID=#CPPUUID#'>
<cfif Page GT 0>
	<cfset nextUrl &= '?page=#page#'>
	<cfset backUrl &= '&page=#page#'>
</cfif>
<cfoutput>
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/cppui/css/form1.css" />
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/cppui/css/style3.css" />
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/cppui/css/list.css" />
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/cppui/css/stylecustom.css" />
	<link href="#rootUrl#/#PublicPath#/cppui/css/mccolorpicker.css" rel="stylesheet" type="text/css" />
	<link href="#rootUrl#/#PublicPath#/bootstrap/dist/css/customize.css" rel="stylesheet" type="text/css" />
	<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
    <!---<script src="#rootUrl#/#PublicPath#/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>---> 
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/cppui/js/form2-jquery.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/cppui/js/custominput.jquery.js"></script>
	<script src="#rootUrl#/#PublicPath#/cppui/js/mccolorpicker.js" type="text/javascript"></script>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload_new.js"></script>	
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/zclip/jquery.zclip.js"></script>
</cfoutput>


<!--<script type="text/javascript" src="js/jquery.js"></script>-->

<cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingcpp" method="getTemplate" returnvariable="getTemplate">
	<cfinvokeargument name="inpCPPUUID" value="#CPPUUID#">
</cfinvoke>

<cfset templateData = getTemplate.data>

<cfset allTemplateData = getTemplate.AllTemplate>
<cfset jsonTemplateData = SerializeJSON(allTemplateData)>
<cfset currentTemp = 'Plain'>
<cfset templateType = 0>
<cfif templateData.template_int GT 0>
	<cfloop query="allTemplateData">
		<cfif  templateId_int EQ templateData.template_int>
			<cfif lCase(templateName_vch) EQ 'plain'>
				<cfset templateType = 0>
			<cfelseif lCase(templateName_vch) EQ 'blue'>
				<cfset templateType = 1>
			<cfelseif lCase(templateName_vch) EQ 'orange'>
				<cfset templateType = 2>
			<cfelseif lCase(templateName_vch) EQ 'red'>
				<cfset templateType = 3>
			<cfelseif lCase(templateName_vch) EQ 'green'>
				<cfset templateType = 4>
			</cfif>
		</cfif>
	</cfloop>
</cfif>
<cfif getTemplate.RXRESULTCODE LT 0>
	<cfoutput>#getTemplate.MESSAGE#</cfoutput>
	<cfexit>
</cfif>
<div class="mainLookCpp">
	<div class="new_header">
		<div class="header-title">Setting up a Customer Preference Portal (CPP)</div>
	</div>
	<!---begin inner-content--->
	<div class="innerLookCpp">
		<div class="title-look">
			Customize Portal Look
		</div>
		<div class="des-look">
			You can choose to embed your portal on your own site or host it on one of our own. Based on your option you will be able to customize the look and feel.
		</div>
		<div class="pagecontainer">
			<div class="col_l">
				<div class="cpp_title_l">
					Customize
				</div>
				<div class="customizeBox">
					<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
							<li class="ui-state-default ui-corner-top"><a href="#tabs-1">SIMPLE</a></li>
							<li class="ui-state-default ui-corner-top"><a href="#tabs-2">ADVANCE</a></li>
							<li class="ui-state-default ui-corner-top"><a href="#tabs-3">HTML BUILDER</a></li>
						</ul>
						<div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
							<cfinclude template="dsp_simpletabpreview.cfm" >
							<div style="clear:both"></div>
						</div>
						<div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
							<cfinclude template="dsp_AdvanceTabPreview.cfm" >
						</div>
						<div id="tabs-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
							<cfinclude template="dsp_HTMLBuilder.cfm" >
						</div>
					</div>
				</div>
                
				<div style="clear:both;"></div>
			</div>
			
			<div class="col_r">
				<div class="cpp_title_r">
					Preview
				</div>	
				<div class="previewBox">
					<cfinclude template="dsp_PreviewBox.cfm" >
				</div>
				
				<div class="cpp_title_r mTop20">
					Embed Code
				</div>	
				<div class="embed-box">
					<cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingcpp" method="selectCppVanity" returnvariable="retCppVanity">
						<cfinvokeargument name="inpCPPUUID" value="#CPPUUID#">
					</cfinvoke>
					<cfset vanity = CPPUUID>
					<cfif retCppVanity.VANITY NEQ "">
						<cfset vanity = retCppVanity.VANITY>
					</cfif>
					<div class="padding-left-20 padding-top-10">Copy and paste this code to embed your CPP on your website.</div>
					<textarea type="text" id="portalCode"></textarea>
				</div>	
			</div>		
		</div>	
	</div>
	<div style="clear:both"></div>
	<!---end inner-content--->	
	<div class="footerLookCpp">
		<a href="#" id ="saveTemp" class="btn btn-primary dropdown-toggle btnapply mRight10">Finish</a>
        <cfif showBack>
            <a href="<cfoutput>#backUrl#</cfoutput>" class="btn btn-primary dropdown-toggle btnapply" style="margin-right:10px;">Back</a>
        </cfif>
	</div>
</div>

<!---<embed width="67" height="29" align="middle" wmode="transparent"  
	pluginspage="http://www.macromedia.com/go/getflashplayer" 
	type="application/x-shockwave-flash" allowfullscreen="false" allowscriptaccess="always" 
	name="copyPortal" bgcolor="#ffffff" quality="best" menu="false" loop="false" 
	src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/zclip/zeroclipboard.swf">--->
<cfinclude template="newUiJs.cfm">