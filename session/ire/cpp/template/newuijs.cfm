<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ 3>
	<cfset mtop = 37>
	<cfset small = true>
<cfelse>
	<cfset small = false>
	<cfset mtop = 15>
</cfif>
<script>
	$(document).ready(function() {		
		changeTemplateType(<cfoutput>#templateType#</cfoutput>, 0);
		var cssBox = document.getElementById("cssBox");
		//if there is no data in css editor box, get data from simple tab and apply
		if(cssBox.innerHTML == ''){
			ApplyCssToPreviewBox();
			//$(".codemirror").keyup();
		}
		else{
			ApplyAdvanceCss(cssBox.innerHTML);
			//$(".codemirror").keyup();
		}

		$("#VoiceNumberList").val("1,");
		$("#SMSNumberList").val("1,");
		$("#EmailList").val("1,");
		InitStyle();
	});
	var mtop = <cfoutput>#mtop#</cfoutput>
	var small = <cfoutput>#small#</cfoutput>
	var oStringMask = new Mask("(###) ###-####");
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];
	
		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
	<!--- http://jsfiddle.net/fnagel/GXtpC/ --->
	var SelectOptionFormatting = function(text, opt){
		var newText = text;
		<!--- Rplace anything () is capture group where $! is where it will go and [\s\S]* is everthing --->
		newText = newText.replace(/([\s\S]*)/, '<span class="ui-selectmenu-item-content">$1</span>');
		
		return newText;
	}
	
	var mycodemirror;//this is css editor variable that used in advance tab
	var customHTMLcodemirror;//this is css editor variable that used in custom HTML builder tab
	
	var currentTemp = 0;
	
	$(function() {	
		$('#subTitleText').text('Edit CPP - Step 3 - Style');
		$('#mainTitleText').text('Customer Preference Portal');
		
		$("#deleteImgFile").click(function(){
			deleteImage();
		});
		
		$('#saveTemp').click(function(){		
			$.ajax({
	           	type: "POST",
	           	enctype: "multipart/form-data",
	           	url:"<cfoutput>#rootUrl#/#sessionPath#/cfc/ire/marketingcpp.cfc</cfoutput>?method=updateNewTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	           	dataType: "json", 
	           	data: {
	           		cppUUID: '<cfoutput>#cppUUID#</cfoutput>',
	           		sizeType_int: $("#sizeType_int").val(),// $('input[name=sizeType_int]:checked').val(),
					width_int: $('#width_int').val(),
					height_int: $('#height_int').val(),
					displayType_ti: $("#displayType_ti").val(),//$('input[name=displayType_ti]:checked').val(),
					templateId_int : $('#templateId').val(),
					customHtml_txt : (customHTMLcodemirror)?customHTMLcodemirror.getValue():"{%portal%}"
	           	},
	           	success: function(d){
		           	if (d.ROWCOUNT > 0) 
					{													
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							if(d.DATA.RXRESULTCODE[0] > 0)
							{
								updateTemplate(1);
								updateTemplate(2);
								updateTemplate(3);
		           				jAlert('Edit CPP template success', "Success!", function(result){
									
		           					if(result){
		           						<!--- not change to postUrl because there's only page parametter --->
		           						location.href = '<cfoutput>#nextUrl#</cfoutput>';
		           					}
		           				});	
		           				
		           			}else{
								jAlert(d.DATA.MESSAGE[0], "Failure!");		
							}
		           		}
		           	}
	      		}
	  		});
			
		});
		
		$('#width_int').keyup(function(){
			changeEmbed(3);
		});
		
		$('#height_int').keyup(function(){
			changeEmbed(3);
		});
		
		$('#copyCode').zclip({
	        path:'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/zclip/zeroclipboard.swf',
	        copy:$('#portalCode').val()
	    });
	    
	});
	
	jsonTemplateData = <cfoutput>#jsonTemplateData#</cfoutput>;
	var jsonTemp = jsonTemplateData.DATA;
	
	
	function changeTemplateType(templateType, partType){
		var currentTemp = templateType;
		var jsonTempItem = jsonTemp[templateType];
		if( partType == 0 || (partType == 1)){	
			$('#radio-Plain'+(templateType+1)).attr('checked',true);
			$('#templateId').val(jsonTempItem[0]);			
			$('#templateName').html(jsonTempItem[1]);
			//$('#FontFamily').html(jsonTempItem[2]);
			var elmntFontFamily = document.getElementById('dllFontFamily');
			for(var i=0; i < elmntFontFamily.options.length; i++)
		  	{
			    if(elmntFontFamily.options[i].value === jsonTempItem[2]) {
			      elmntFontFamily.selectedIndex = i;
			      break;
			    }
		  	}
			$('select#dllFontFamily').selectmenu({
				style:'popup',
				width: 190,
				format: addressFormatting,
                appendTo: '.customSizeTemplate .right_box'
			});	
			//$('#FontSize').html(jsonTempItem[3]);			
			var elmntFontSize = document.getElementById('dllFontSize');		
			for(var i=0; i < elmntFontSize.options.length; i++)
		  	{
			    if(parseInt(elmntFontSize.options[i].value) === parseInt(jsonTempItem[3])) {
			      elmntFontSize.selectedIndex = i;				  
			      break;
			    }
		  	}
			$('select#dllFontSize').selectmenu({
				style:'popup',
				width: 82,
				format: addressFormatting,
                appendTo: '#CustomDimesision .right_box'
			});	
			//$('#fontSizeUnit').html(jsonTempItem[4]);
			var elmntFontType = document.getElementById('dllFontType');
			for(var i=0; i < elmntFontType.options.length; i++)
		  	{
			    if(elmntFontType.options[i].value === jsonTempItem[4]) {
			      elmntFontType.selectedIndex = i;
			      break;
			    }
		  	}
			$('select#dllFontType').selectmenu({
				style:'popup',
				width: 82,
				format: addressFormatting,
                appendTo: '#CustomDimesision .right_box'
			});	
			//$('#FontWeight').html(jsonTempItem[5]);
			var elmntFontWeight = document.getElementById('dllFontWeight');
			for(var i=0; i < elmntFontWeight.options.length; i++)
		  	{
			    if(elmntFontWeight.options[i].value === jsonTempItem[5]) {
			      elmntFontWeight.selectedIndex = i;
			      break;
			    }
		  	}
			$('select#dllFontWeight').selectmenu({
				style:'popup',
				width: 190,
				format: addressFormatting,
                appendTo: '.customSizeTemplate .right_box'
			});	
			//$('#FontStyle').html(jsonTempItem[6]);
			var elmntFontStyle = document.getElementById('dllFontStyle');
			for(var i=0; i < elmntFontStyle.options.length; i++)
		  	{
			    if(elmntFontStyle.options[i].value === jsonTempItem[6]) {
			      elmntFontStyle.selectedIndex = i;
			      break;
			    }
		  	}
			$('select#dllFontStyle').selectmenu({
				style:'popup',
				width: 190,
				format: addressFormatting,
                appendTo: '.customSizeTemplate .right_box'
			});	
			//$('#FontVariant').html(jsonTempItem[7]);
			var elmntFontVariant = document.getElementById('dllFontVariant');
			for(var i=0; i < elmntFontVariant.options.length; i++)
		  	{
			    if(elmntFontVariant.options[i].value === jsonTempItem[7]) {
			      elmntFontVariant.selectedIndex = i;
			      break;
			    }
		  	}
				$('select#dllFontVariant').selectmenu({
				style:'popup',
				width: 190,
				format: addressFormatting,
                appendTo: '.customSizeTemplate .right_box'
			});	
			$('#FontColor').val(jsonTempItem[14]);
			$('#FontColor').next().css("background-color",jsonTempItem[14]);
			$('#customCss').val(jsonTempItem[15]);
		}
		
		if( partType == 0 || (partType == 2)){
			$('#BackgroundColor').val(jsonTempItem[8]);
			$('#BackgroundColor').next().css("background-color",jsonTempItem[8]);
			$('#InpImgServerFile').val(jsonTempItem[9]);
			if(jsonTempItem[9] != ''){
				$('#ImgPreview').empty();
				$('#ImgPreview').append('<img id="ImagePreview" src="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ire/cpp/template/act_previewImage?serverfileName='+ jsonTempItem[9] +'&userId=<cfoutput>#Session.UserId#</cfoutput>" height="65"/>');
				$('#ImgPreview').append('<img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/mb/bin_empty.png" style="cursor: pointer" title="remove logo" id="deleteImgFile" rel="'+ jsonTempItem[9] +'"/>');
			}else{
				$('#ImgPreview').empty();
			}
			
			var elmnt = document.getElementById('dllImageRepeat');
			
			if(jsonTempItem[10] == 'Image-Repeat'){				
				for(var i=0; i < elmnt.options.length; i++)
			  	{
				    if(elmnt.options[i].value === jsonTempItem[10]) {
				      elmnt.selectedIndex = i;					  
				      break;
				    }
			  	}				
				//$('#dllImageRepeat').html(jsonTempItem[10]);
			}else if(jsonTempItem[10]){
				//$('#dllImageRepeat').html('Yes');
				for(var i=0; i < elmnt.options.length; i++)
			  	{
				    if(elmnt.options[i].value === "Yes") {
				      elmnt.selectedIndex = i;
				      break;
				    }
			  	}
			}else{
				for(var i=0; i < elmnt.options.length; i++)
			  	{
				    if(elmnt.options[i].value === "No") {
				      elmnt.selectedIndex = i;
				      break;
				    }
			  	}
				//$('#dllImageRepeat').html('No');
			}
		}
		
		if( partType == 0 || (partType == 3)){
			$('#BorderColor').val(jsonTempItem[11]);
			$('#BorderColor').next().css("background-color",jsonTempItem[11]);
			$('#BorderWidth').val(jsonTempItem[12]);
			$('#BorderRadius').val(jsonTempItem[13]);
		}
		
		//$("#custom-tmp").hide();
		
		$('select#dllImageRepeat').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});		
		$('input').custominput();
	}
	
	function resetTemplate(partType){
		changeTemplateType(currentTemp,partType);
	}
		
	function updateTemplate(partType){
		$.ajax({
	           	type: "POST",
	           	enctype: "multipart/form-data",
	           	url:"<cfoutput>#rootUrl#/#sessionPath#/cfc/ire/marketingcpp.cfc</cfoutput>?method=ChangeCPPTempateData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	           	dataType: "json", 
	           	data: {
	           		dataType : 1,
	           		partType : partType,
	           		CPP_UUID_VCH: '<cfoutput>#cppUUID#</cfoutput>',
					templateId_int : $('#templateId').val(),
					templateName_vch: $('#templateName').html(),
					fontFamily_vch : $('#dllFontFamily').val(),
					fontSize_int : $('#dllFontSize').val(),
					fontSizeUnit_vch : $('#dllFontType').val(),
					fontWeight_vch : $('#dllFontWeight').val(),
					fontStyle_vch : $('#dllFontStyle').val(),
					fontVariant_vch : $('#dllFontVariant').val(),
					fontColor_vch : $('#FontColor').val(),
					customCSS_txt : (!mycodemirror)?"":mycodemirror.getValue(),//get custom data from codemirror editor
					backgroundColor_vch : $('#BackgroundColor').val(),
					backgroundImage_vch : $('#InpImgServerFile').val(),
					imageRepeat_vch : $('#dllImageRepeat').val(),
					borderColor_vch : $('#BorderColor').val(),
					borderWidth_int : $('#BorderWidth').val(),
					borderRadius_int : $('#BorderRadius').val()
	           	},
	           	success: function(d){
		           	if (d.ROWCOUNT > 0) 
					{													
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							if(d.DATA.RXRESULTCODE[0] > 0)
							{
		           				
		           			}else{
								jAlert(d.DATA.MESSAGE[0], "Failure!");		
							}
		           		}
		           	}
	      		}
	  		});
	}
	
	function ajaxupload() {
		type = 'image';
		var	inFileId = 'InpImgFile';
		var rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		var sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		var publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		$.ajaxFileUpload
		(
			{
				url: '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ire/cpp/template/act_UploadLogo?inpFileId='+inFileId ,
				secureuri:false,
				fileElementId: inFileId,
				dataType: 'json',				
				complete:function()
				{					
					$("#deleteImgFile").click(function(){
						deleteImage();
					});
				},				
				success: function (data, status)
				{	
					if(type == 'image'){
						
						//alert(data.fileSize);
						//alert(data.clientFileExt);
						if ((data.clientFileExt.toUpperCase() == 'JPG') || (data.clientFileExt.toUpperCase() == 'GIF') || (data.clientFileExt.toUpperCase() == 'PNG')) {
							$('#InpImgServerFile').val(data.UniqueFileName);
							$('#ImgPreview').empty();
							$('#ImgPreview').append('<img id="ImagePreview" src="' +rootUrl + '/' + sessionPath +'/ire/cpp/template/act_previewImage?serverfileName='+ data.UniqueFileName +'&userId=<cfoutput>#Session.UserId#</cfoutput>" height="65"/>');
							$('#ImgPreview').append('<img src="' +rootUrl + '/' + publicPath +'/images/mb/bin_empty.png" style="cursor: pointer" title="remove logo" id="deleteImgFile" rel="'+ data.UniqueFileName +'"/>');
							ApplyCssToPreviewBox();
						} else {
							alert('File type not accepted.');
						}						
					}else{
					}
				},
				error: function (data, status, e)
				{						
					alert('general Error ' + e);	
				}
			}
		);
		return false;
	}

	function deleteImage(){
		type = 'image';
		inFileId = 'InpImgFile';
		rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		var img = $("#deleteImgFile").attr("rel");
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingcpp.cfc?method=DeleteCPPTemplateImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { inpFile : img},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.RESULT == 'SUCCESS'){
						$('#InpImgServerFile').val("");
						$('#InpImgFile').val("");
						$('#ImgPreview').html('');
						$('#ImgPreview').empty();
						ApplyCssToPreviewBox();
					}
				} 		
			});
		
	}

	changeEmbed(<cfoutput>#templateData.sizeType_int#</cfoutput>);
	
	function changeEmbed(embedType){
		var embedWidth = 0;
		var embedheight = 0;
		
		if (embedType == 0){
			embedWidth = 400;
			embedheight = 250;
		}else if (embedType == 1){
			embedWidth = 600;
			embedheight = 375;
		}else if (embedType == 2){
			embedWidth = 800;
			embedheight = 500;
		}else if (embedType == 3){
			embedWidth = $('#width_int').val();
			embedheight = $('#height_int').val();
		}
		
		$('#width_int').val(embedWidth);
		$('#height_int').val(embedheight);
			
		var embedText = '<iframe src="<cfoutput>#CppRedirectDomainPublic#/#CPPUUID#</cfoutput>" scrolling="auto" align="center" height = "'+embedheight+'px" width="'+embedWidth+'px" frameborder="0" id="CPPReviewFrame"></iframe>';
		
		$('#portalCode').text(embedText);
	}

	// tinyMCE.execCommand('mceAddControl', false, 'customHtml');
//	tinymce.init({
//		mode : "textareas",
//		editor_selector : "mceEditor",
//		editor_deselector : "mceNoEditor",
//		selector: "textarea",
//		convert_urls: false,
//		theme: "modern",
//		plugins: [
//			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
//			"searchreplace wordcount visualblocks visualchars code fullscreen",
//			"insertdatetime media nonbreaking save table contextmenu directionality",
//			"emoticons template paste textcolor"
//		],
//		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
//		toolbar2: "print preview media | forecolor backcolor emoticons",
//		image_advtab: true
//	});

	$(function() {
		$( "#tabs" ).tabs();
		$('select#displayType_ti').selectmenu({
			style:'popup',
			width: 348,
			format: addressFormatting,
            appendTo: '#tabs-1 .boxLarge .right_box'
		});	
		$('select#dd').selectmenu({
			style:'popup',
			width: 348,
			format: addressFormatting,
            appendTo: '#tabs-1 .boxLarge .right_box'
		});		
		$('select#sizeType_int').selectmenu({
			style:'popup',
			width: 348,
			format: addressFormatting,
            appendTo: '#tabs-1 .boxLarge .right_box'
		});		
		$('select#Plain').selectmenu({
			style:'popup',
			width: 348,
			format: addressFormatting,
            appendTo: '#tabs-1 .boxLarge .right_box'
		});		
		$('select#dllFontFamily').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});		
		$('select#dllFontSize').selectmenu({
			style:'popup',
			width: 82,
			format: addressFormatting,
            appendTo: '#CustomDimesision .right_box'
		});
		$('select#dllFontType').selectmenu({
			style:'popup',
			width: 82,
			format: addressFormatting
		});		
		$('select#dllFontWeight').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});	
		$('select#dllFontStyle').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});	
		$('select#dllFontVariant').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});	
		$('select#dllImageRepeat').selectmenu({
			style:'popup',
			width: 190,
			format: addressFormatting,
            appendTo: '.customSizeTemplate .right_box'
		});		
		//select all text in embed box whenever user click on
		$("#portalCode").click(function(){
			if (document.selection) {
	            var range = document.body.createTextRange();
	            range.moveToElementText(document.getElementById("portalCode"));
	            range.select();
	        } else if (window.getSelection) {
	            var range = document.createRange();
	            range.selectNode(document.getElementById("portalCode"));
	            window.getSelection().addRange(range);
	        }
		});

		InitCSSEditor();
		//update data in css editor 
		$('#ui-id-2').click(function(){
			if (mycodemirror) {
				mycodemirror.refresh();
			}
		});
		//refresh data for editor in custom html builder
		$('#ui-id-3').click(function(){
			if (customHTMLcodemirror) {
				customHTMLcodemirror.refresh();
			}
		});
	});

	function NewChangeEmbed(obj){
		var id=$(obj).val();
		changeEmbed(id);
	}
	function NewChangeTemplateType(obj){
		var id=$(obj).val();
		changeTemplateType(id, 0);
		ApplyCssToPreviewBox();
	}
	
	function ApplyCssToPreviewBox(){
		//Font Family 
		var fontFamily_vch = $('#dllFontFamily').val(),
		//Font Size
		fontSize_int = $('#dllFontSize').val(),
		//Font Type
		fontSizeUnit_vch = $('#dllFontType').val(),
		//Font Weight 
		fontWeight_vch = $('#dllFontWeight').val(),
		//Font Style
		fontStyle_vch = $('#dllFontStyle').val(),
		//Font Variant 
		fontVariant_vch = $('#dllFontVariant').val(),
		//Font Color
		fontColor_vch = $('#FontColor').val(),
		//Background Color 
		backgroundColor_vch = $('#BackgroundColor').val(),
		//Background Image
		backgroundImage_vch = $('#ImagePreview').attr("src"),
		//Image Repeat
		imageRepeat_vch = $('#dllImageRepeat').val(),
		//Border Color 
		borderColor_vch = $('#BorderColor').val(),
		//Border width (px) 
		borderWidth_int = $('#BorderWidth').val(),
		//Corner Radius (px)  
		borderRadius_int = $('#BorderRadius').val();
		
		var newLineAndTab = "\n\t";
		var cssText = ".previewStyle{"+newLineAndTab+"border:";
		cssText += borderWidth_int;
		cssText+= "px solid "
		cssText+= borderColor_vch;
		cssText+= ";"+newLineAndTab+"border-radius: ";
		cssText+= borderRadius_int;
		cssText+= "px;"		
		cssText+= newLineAndTab+"font-size:";
		cssText+= fontSize_int;
		cssText+= fontSizeUnit_vch;
		cssText+= ";";
		cssText+= newLineAndTab+"font-family: ";
		cssText+= fontFamily_vch;
		cssText+= ";";
		cssText+= newLineAndTab+"font-weight:";
		cssText+= fontWeight_vch;
		cssText+= ";";
		cssText+= newLineAndTab+"font-style:";
		cssText+= fontStyle_vch;
		cssText+= ";";
		cssText+= newLineAndTab+"font-variant:";
		cssText+= fontVariant_vch;
		cssText+= ";";
		cssText+= newLineAndTab+"color:";
		cssText+= fontColor_vch;
		cssText+= ";";
		
		cssText += newLineAndTab;
		cssText+= "background:";
		
		if ($("#ImgPreview").html()=='') {			
			cssText += backgroundColor_vch;			
		}
		else {		
			cssText += backgroundImage_vch == "" ? "" : " url('" + $("#ImagePreview").attr("src") + "')";
			if(imageRepeat_vch=="Yes")
			{
				cssText += " repeat";	
			}
			else
			{
				cssText += " no-repeat";
			}
			cssText += ";";
		}
		cssText+= "\n}\n";
		cssText+= "/*style for step title*/";
		cssText+= "\n";
		cssText+= ".step{";
		cssText+= newLineAndTab+"font-size:21px;";
		cssText+= newLineAndTab+"font-weight:bold;";
		cssText+= "\n}\n";
		cssText+= "/*style for step description*/";
		cssText+= "\n";
		cssText+= ".stepNote{";
		<!---cssText+= newLineAndTab+"font-size:13px;";--->
		cssText+= newLineAndTab+"font-weight:normal;";
		cssText+= "\n}\n";
		
		var cssBox = document.getElementById("cssBox");
		cssBox.innerHTML = cssText;
		ApplyAdvanceCss(cssText);
		if (mycodemirror) {
			mycodemirror.setValue(cssText);
		}
	}
	
	//this function inits css editor in advance tab and custom HTML builder tab
	function InitCSSEditor(){
		mycodemirror = codemirror.fromTextArea(document.getElementById("cssBox"),{
			mode:"text/css",
			lineNumbers:true,
			showCursorWhenSelecting:true,
			tabSize:6,
			onKeyEvent: function(e , s){
                if (s.type == "keyup")
                {
					//apply css
					ApplyAdvanceCss(mycodemirror.getValue());
                }
            }
		});
		
		customHTMLcodemirror = codemirror.fromTextArea(document.getElementById("txtCustomHTMLBuilder"),{
			mode:"text/html",
		    htmlMode: true,
			lineNumbers:true,
			showCursorWhenSelecting:true,
			tabSize:6
		});
	}
	
	function ApplyAdvanceCss(cssValue){
		//loop array
		$("#boxPreview .cppPortalRow").each(function(){
			if($(this).data("id") < 2)
			{
				//reset style of all child elements in this element
				var previewBoxItems = $(this).find("*");
				for (var i = 0; i < $(this).find("*").length; i++) {
					var itemId = $(previewBoxItems[i]).attr("id");
					if(itemId == "language"||$(previewBoxItems[i]).parent().attr("id")=="language"){
						continue;
					}
					else{
						
						$(previewBoxItems[i]).attr("style","");
					}
				}
			}
		});
		
		//generate select menu here to prevent clear css from above
		$('select#language').css("display","none");
        $('.select-default-value').css("display","none");
        $('.select-default-value').selectmenu({
            style:'popup',
            width: 279,
            format: SelectOptionFormatting,
            appendTo: '.cppPortalRow .right_box'
        });
		if($("select#language").selectmenu()){
				$("select#language").selectmenu("destroy").selectmenu({
				style:'popup',
				width: 176,
				format: addressFormatting,
                appendTo: '.cppPortalRow .right_box'
			});
		}
		else{
			$("select#language").selectmenu({
				style:'popup',
				width: 176,
				format: addressFormatting
			});
		}
		InitStyle();
		var source = document.getElementById("cssBox");
		//remove all newline to format data for passing into parser
		var ss = cssValue;
		ss = replaceAll(ss,"\n","",1);
		ss = replaceAll(ss,"<br>","",1);
		ss = replaceAll(ss,"<br/>","",1);
		
		var parser = new CSSParser();
		var sheet = parser.parse(ss, false, true);

		if(sheet){
			for(var index in sheet.cssRules){
				if(!sheet.cssRules[index]) continue;
				if(!sheet.cssRules[index].declarations) continue;
				for(var ruleIndex in sheet.cssRules[index].declarations){
					if(!sheet.cssRules[index].declarations[ruleIndex]) continue;
					var nameCss=sheet.cssRules[index].declarations[ruleIndex].property;
					var valueCss= htmlDecode(sheet.cssRules[index].declarations[ruleIndex].valueText);
					$("#boxPreview "+sheet.cssRules[index].mSelectorText).css(nameCss,valueCss);
				}
			}
		}
	}
	
	
	
	//start add or del Voice number contact
	var VoiceNumberIndex = 1;	
	if(document.getElementById("voiceText_1")){
		oStringMask.attach(document.getElementById("voiceText_1"));
	}
	
	var newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
	
	function fnAddVoiceNumber() {
		if(small){
			$("#actionVoice").css("margin-top", "29px");	
		}
		var voiceNumber = $("#VoiceNumberList").val();
		VoiceNumberIndex++;
		var row = "<div id='VoiceNumnerPanel"+VoiceNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='voiceText_"+VoiceNumberIndex.toString()+"' id='voiceText_"+VoiceNumberIndex.toString()+"' class=\"input_box_last\" size=\"4\" value=\"\">"+
		"</div>";
		var action = "<input type=\"button\" name='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' id='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' class=\"delContact\" onclick=\"fnDelVoiceNumber('VoiceNumnerPanel"+VoiceNumberIndex.toString()+"','"+VoiceNumberIndex.toString()+",')\">";
		$("#inpVoiceNumber").append(row);
		$("#actionVoice").append(action);
		$("#VoiceNumberList").val(voiceNumber + VoiceNumberIndex.toString() + ",");		
		if(voiceNumber.length > 0)
		{
			var voice = voiceNumber.split(",");
			if(voice.length >= 2){
				$("#DelVoiceNumber_"+voice[0]).removeClass("delHide").addClass("delShow");
			}
			$("#AddVoiceNumber").css("margin-top", (voice.length) * 28 + parseInt(mtop));
		}
		newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
		oStringMask.attach(document.getElementById("voiceText_"+VoiceNumberIndex));
		return false;
	}; 
	
	function fnDelVoiceNumber(delPanel, index)
	{
		var voiceNumber = $("#VoiceNumberList").val();
		$("#"+delPanel).remove();
		$("#DelVoiceNumber_"+index.replace(",","")).remove();
		$("#VoiceNumberList").val(voiceNumber.replace(index,""));
		
		var newVoiceNumber = $("#VoiceNumberList").val();
		if(newVoiceNumber.length > 0)
		{
			var voice = newVoiceNumber.split(",");
			var newVoiceNumberBeforeDelArray = newVoiceNumberBeforeDel.split(",");
			var firstIndex = newVoiceNumberBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#voiceText_"+voice[0]).removeClass("input_box_last").addClass("input_box");	
			}						
			<!--- Allow delete to stay - user uses this to opt out --->
			if(voice.length == 2){
				<!---$("#DelVoiceNumber_"+voice[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionVoice").css("margin-top", "0px");
				}
			}					
			
			$("#AddVoiceNumber").css("margin-top", (voice.length - 1) * 28 + parseInt(mtop));
		}
		newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
		
	}
	
	//end add or del Voice number contact
	
	
	//start add or del SMS number contact
	var  SMSNumberIndex = 1;
	if(document.getElementById("smsText_1")){
		oStringMask.attach(document.getElementById("smsText_1"));
	}
	var newSMSNumberBeforeDel = $("#SMSNumberList").val();
	function fnAddSMSNumber() {
		SMSNumberIndex++;
		if (small) {
			$("#actionSms").css("margin-top", "29px");
		}
		var row = "<div id='SMSNumnerPanel"+SMSNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='smsText_"+SMSNumberIndex.toString()+"' id='smsText_"+SMSNumberIndex.toString()+"' class=\"input_box_last\" size=\"4\" value=\"\">"+
		"</div>";
		var action = "<input type=\"button\" name='DelSMSNumber_"+SMSNumberIndex.toString()+"' id='DelSMSNumber_"+SMSNumberIndex.toString()+"' class=\"delContact\" onclick=\"fnDelSMSNumber('SMSNumnerPanel"+SMSNumberIndex.toString()+"','"+SMSNumberIndex.toString()+",')\">";
		$("#inpSMSNumber").append(row);
		$("#actionSms").append(action);
		var SMSNumber = $("#SMSNumberList").val();
		$("#SMSNumberList").val(SMSNumber + SMSNumberIndex.toString() + ",");
		
		if(SMSNumber.length > 0)
		{
			var sms = SMSNumber.split(",");
			if(sms.length >= 2){
				$("#DelSMSNumber_"+sms[0]).removeClass("delHide").addClass("delShow");
			}
			$("#AddSMSNumber").css("margin-top", (sms.length) * 28 + parseInt(mtop));
		}
		newSMSNumberBeforeDel = $("#SMSNumberList").val();
		oStringMask.attach(document.getElementById("smsText_"+SMSNumberIndex));
		return false;
	}; 
	
	function fnDelSMSNumber(delPanel, index)
	{
		$("#"+delPanel).remove();
		var SMSNumber = $("#SMSNumberList").val();
		$("#DelSMSNumber_"+index.replace(",","")).remove();
		$("#SMSNumberList").val(SMSNumber.replace(index,""));
		
		var newSMSNumber = $("#SMSNumberList").val();
		if(newSMSNumber.length > 0)
		{
			var sms = newSMSNumber.split(",");
			var newSMSNumberBeforeDelArray = newSMSNumberBeforeDel.split(",");
			var firstIndex = newSMSNumberBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#smsText_"+sms[0]).removeClass("input_box_last").addClass("input_box");	
			}
			if(sms.length == 2){
				
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelSMSNumber_"+sms[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionSms").css("margin-top", "0px");
				}
			}
			$("#AddSMSNumber").css("margin-top", (sms.length - 1) * 28 + parseInt(mtop));
		}
		newSMSNumberBeforeDel = $("#SMSNumberList").val();
	}
	
	//end add or del SMS number contact
	
	
	//start add or del Email contact
	var  EmailIndex = <cfoutput>#maxEmail#</cfoutput>;
	var newEmailBeforeDel = $("#EmailList").val();
	function fnAddEmail() {		
	
		EmailIndex++;
		if (small) {
			$("#actionEmail").css("margin-top", "29px");
		}
		var row = "<div id='EmailPanel"+EmailIndex.toString()+"'>"+
			"<input type=\"text\" name='email_"+EmailIndex.toString()+"' id='email_"+EmailIndex.toString()+"' class=\"input_box_last\" value=\"\">"+
		"</div>";		
		var action = "<input type=\"button\" name='DelEmail_"+EmailIndex.toString()+"' id='DelEmail_"+EmailIndex.toString()+"' class=\"delContact\" onclick=\"fnDelEmail('EmailPanel"+EmailIndex.toString()+"','"+EmailIndex.toString()+",')\">";
		$("#inpEmail").append(row);		
		$("#actionEmail").append(action);
		var Emailnumber = $("#EmailList").val();
		
		if(typeof Emailnumber != "undefined"){
			$("#EmailList").val(Emailnumber + EmailIndex.toString() + ",");		
			if(Emailnumber.length > 0)
			{
				var email = Emailnumber.split(",");
				if(email.length >= 2){
					$("#DelEmail_"+email[0]).removeClass("delHide").addClass("delShow");
				}
				$("#AddEmail").css("margin-top", (email.length) * 28 + parseInt(mtop));			
			}
			newEmailBeforeDel = $("#EmailList").val();
		}		
		return false;
	}; 
	
	function fnDelEmail(delPanel, index)
	{
		$("#"+delPanel).remove();
		$("#EmailList").val($("#EmailList").val().replace(index,""));
		$("#DelEmail_"+index.replace(",","")).remove();
		var newEmailNumber = $("#EmailList").val();
		if(newEmailNumber.length > 0)
		{
			var email = newEmailNumber.split(",");
			var newEmailBeforeDelArray = newEmailBeforeDel.split(",");
			var firstIndex = newEmailBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#email_"+email[0]).removeClass("input_box_last").addClass("input_box");	
			}
			if(email.length == 2){
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelEmail_"+email[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionEmail").css("margin-top", "0px");
				}
			}
			$("#AddEmail").css("margin-top", (email.length-1) * 28 + parseInt(mtop));
		}
		newEmailBeforeDel = $("#EmailList").val();
	}
	
	function OnColorChanged(selectedColor, colorPickerIndex) {
		ApplyCssToPreviewBox();	 
	}
	
	function InitStyle(){
		//voice contact
		var voiceNumber = $("#VoiceNumberList").val();
		var voice = (voiceNumber)?voiceNumber.split(","):new Array();		
		if (voice.length > 2) {			
			$("#AddVoiceNumber").css("margin-top", (voice.length - 2) * 28 + parseInt(mtop));
		}
		else{
			$("#AddVoiceNumber").attr("style","");
		}
		//sms contact
		var SMSNumber = $("#SMSNumberList").val();		
		var sms = (SMSNumber)?SMSNumber.split(","):new Array();
		if (sms.length > 2) {			
			$("#AddSMSNumber").css("margin-top", (sms.length - 2) * 28 + parseInt(mtop));
		}
		else{
			$("#AddSMSNumber").attr("style","");
		}
		//email contact
		var Emailnumber = $("#EmailList").val();
		if(Emailnumber){
			var email = Emailnumber.split(",");		
			if (email.length > 2) {			
				$("#AddEmail").css("margin-top", (email.length - 2) * 28 + parseInt(mtop));
			}
			else{
				$("#AddEmail").attr("style","");
			}
		}
	}
	//end add or del Email contact
	function htmlDecode(str) {
	    return String(str)
	            .replace('&amp;','&')
	            .replace('&quot;','"')
	            .replace('&#39;',"'")
	            .replace( '&lt;','<')
	            .replace('&gt;', '>');
	}
</script>