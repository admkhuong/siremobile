<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#UnitTest_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="ire">
</cfinvoke>

<cfoutput>
	<a href='#rootUrl#/#SessionPath#/ire/survey/surveylist/'>Surveys</a>
	<a href='#rootUrl#/#SessionPath#/ire/cpp/cpp'>CPP</a>
	<a href='#rootUrl#/#SessionPath#/ire/lifecycle/lifecycle'>Life Cycle</a>
</cfoutput>

<script type="text/javascript">
	
	$(function(){
		$('#subTitleText').text('List CPP');
		$('#mainTitleText').text('Customer Preference Portal');
	});
	
</script>
