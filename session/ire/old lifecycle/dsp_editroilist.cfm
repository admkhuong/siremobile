<cfparam name="INPLCELISTID" default="0">
<cfparam name="LCEITEMID" default="0">
<cfset lOIspace="&nbsp;&nbsp;&nbsp;">

<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

<script>
	$(function() {
		$.getJSON("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=UpdateLCELinkExternalContentBatchId&batchId=" + DefaultBatchId + "&returnformat=json&queryformat=column&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>", {}, function(data) {
			if (data.DATA.RESULT == 'true') {
				BindROIData();
			}
			
		});	
	});
	
	function BindROIData() {
		$("#rOIList_LifeCycleEvent").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=GetROIListForLCEItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>',
			datatype: "json",
			height: 385,
			colNames:['Select', 'ID', 'Company Id', 'Description', 'Customers', 'Percent', 'Average Customer Profits', 'Total Profits'],
			colModel:[			
				{name: 'DisplayOptions', index: 'DisplayOptions', width: 30, editable: false, sortable: false, resizable: false, align: 'center' },
				{name: 'CompanyId_int', index: 'CompanyId_int', width: 100, editable: true, resizable: false, align: 'center'},
				{name: 'LCEROIID_int', index: 'LCEROIID_int', width: 100, editable: false, resizable: false, align: 'center'},
				{name: 'Desc_vch', index: 'Desc_vch', width: 100, editable: true, resizable: false, align: 'center'},
				{name: 'Customers_int', index: 'Customers_int', width: 100, editable: true, resizable: false, align: 'center'},
				{name: 'Percent_int', index: 'Percent_int', width: 100, editable: true, resizable: false, align: 'center'},
				{name: 'AveCustomerProfits_int', index: 'AveCustomerProfits_int', width: 100, editable: true, resizable: false, align: 'center'},
				{name: 'TotalProfits_int', index: 'TotalProfits_int', width: 100, editable: true, resizable: false, align: 'center'}			
			],
			rowNum: 15,
		//   	rowList: [20,4,8,10,20,30],
			mtype: "POST",
			pager: $('#pagerDiv'),
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current Campaigns Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			sortname: 'Created_dt',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",	
			multiselect: false,
			ondblClickRow: function(LCEROIID_int){  },
			onSelectRow: function(LCEROIID_int) {
			},
			loadComplete: function(data) { 		
			},
			<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
	
			jsonReader: {
				root: "ROWS", //our data
				page: "PAGE", //current page
			  	total: "TOTAL", //total pages
				records:"RECORDS", //total records
			  	cell: "", //not used
			  	id: "0" //will default second column as ID
	 		}	
		});
	}
	
	
	function SelectROIForLCEExternalContent(lCEROIId, companyId_int, desc_vch, customers_int, percent_int, aveCustomerProfits_int, totalProfits_int){
			
		$.getJSON("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=AddROIToLCELinkExternalContent&lCEROI_id=" + lCEROIId + "&isChecked=" + $("#SelectROI" + lCEROIId).is(':checked') + "&returnformat=json&queryformat=column&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>", {}, function(res,code) {
			if (res.DATA.RESULT)
			{
				if ($("#SelectROI" + lCEROIId).is(':checked'))
				{
					var liTag = document.createElement("li");
					liTag.id = 'li_' + lCEROIId;
					liTag.className = "ui-state-default";
					liTag.innerHTML = lCEROIId + '<cfoutput>#lOIspace#</cfoutput>' + companyId_int + 
												'<cfoutput>#lOIspace#</cfoutput>' + desc_vch + 
												'<cfoutput>#lOIspace#</cfoutput>' + customers_int + 
												'<cfoutput>#lOIspace#</cfoutput>' + percent_int + 
												'<cfoutput>#lOIspace#</cfoutput>' + aveCustomerProfits_int +
												'<cfoutput>#lOIspace#</cfoutput>' + totalProfits_int;
					document.getElementById('sortable').appendChild(liTag);
				}
				else
				{	
					var d = document.getElementById('sortable');
					var olddiv = document.getElementById('li_' + lCEROIId);
					d.removeChild(olddiv);
				}
			}
			else
			{
				alert('Some error occured!!');
				//revert the checkbox action
				if ($("#SelectROI" + lCEROIId).is(':checked'))
					$("#SelectROI" + lCEROIId).attr('checked', false);
				else
					$("#SelectROI" + lCEROIId).attr('checked', true);
				
			}
		});
	}
</script>

<style type="text/css">
	ul{
		list-style-type:none;
	}
	li{
		padding:2px 0px 2px 5px;
	}
</style>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Select ROI</a></li>
		<li><a href="#tabs-2">ROI List</a></li>
	</ul>
	<div id="tabs-1">
		<div style="text-align:left;">
            <table id="rOIList_LifeCycleEvent"></table>
        </div>
	</div>
	<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
		select 	ProgramInfo_vch
		from	simpleobjects.lcelinksexternalcontent
		where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEITEMID#">
				and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTID#">
	</cfquery>
	<cfset lstROIIds = "">
	<div id="tabs-2">
		<ul id="sortable">
				<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
					<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
					<cfif Arraylen(Ids) GT 1>
						<cfloop index = "listElement" list = "#Ids[2]#" delimiters = ","> 
							<cfquery name="findSelectedROI" datasource="#Session.DBSourceEBM#">
						    	SELECT 	LCEROIID_int, 
										CompanyId_int, 
										Desc_vch, 
										Customers_int,
										Percent_int,
										AveCustomerProfits_int, 
										TotalProfits_int
						        FROM	simpleobjects.lceroi
						        WHERE	LCEROIID_int =  #listElement#
						    </cfquery>
				
				            <cfoutput query="findSelectedROI">
				            	<li id="li_#LCEROIID_int#" class="ui-state-default">
					            	#LCEROIID_int##lOIspace#
		            				#CompanyId_int##lOIspace#
		            				#Desc_vch##lOIspace#
		            				#Customers_int##lOIspace#
		            				#Percent_int##lOIspace#
		            				#AveCustomerProfits_int##lOIspace#
		            				#TotalProfits_int##lOIspace#
								</li>
				            </cfoutput>
						       
						</cfloop>
					</cfif>
				</cfloop>
			
		</ul>
	</div>
    
</div>
