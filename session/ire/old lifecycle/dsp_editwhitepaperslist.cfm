<cfparam name="INPLCELISTID" default="0">
<cfparam name="LCEITEMID" default="0">

<script>
	var batchId = "";
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

<script>
	$(function() {
		$.getJSON("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=UpdateLCELinkExternalContentBatchId&batchId=" + DefaultBatchId + "&returnformat=json&queryformat=column&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>", {}, function(data) {
			if (data.DATA.RESULT == 'true') {
				BindWhitePapersData();
			}
			
		});	
	});
	
	function BindWhitePapersData()
	{
		$("#whitePapersList_LifeCycleEvent").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=GetWhitePapersListForLCEItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>',
			datatype: "json",
			height: 385,
			colNames:['Select', 'ID', 'Content', 'Description'],
			colModel:[			
				{name: 'Select', index: 'Options', width: 30, editable: false, sortable: false, resizable: false, align: 'center' },
				{name: 'LCEWhitePapersID_int', index: 'LCEWhitePapersID_int', width: 30, editable: false, resizable: false, align: 'center'},
				{name: 'Content', index: 'Content', width: 280, editable: true, resizable: false, align: 'center'},
				{name: 'Desc_vch', index: 'Desc_vch', width: 280, editable: true, resizable: false, align: 'center'}		
			],
			rowNum: 15,
		//   	rowList: [20,4,8,10,20,30],
			mtype: "POST",
			pager: $('#whitePagerDiv'),
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current Campaigns Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			sortname: 'LCEWhitePapersID_int',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",	
			multiselect: false,
			jsonReader: {
				root: "ROWS", //our data
				page: "PAGE", //current page
			  	total: "TOTAL", //total pages
				records:"RECORDS", //total records
			  	cell: "", //not used
			  	id: "0" //will default second column as ID
	 		}	
		});
	}
	function SelectWhitePapersForLCEExternalContent(lCEWhitePapersId, desc){
		
		$.getJSON("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=AddWhitePapersToLCELinkExternalContent&lCEWhitePapers_id=" + lCEWhitePapersId + "&isChecked=" + $("#SelectWhitePapers" + lCEWhitePapersId).is(':checked') + "&returnformat=json&queryformat=column&lCEListId=<cfoutput>#INPLCELISTID#</cfoutput>&lCEItemId=<cfoutput>#LCEITEMID#</cfoutput>", {}, function(res,code) {
			if (res.DATA.RESULT)
			{
				if ($("#SelectWhitePapers" + lCEWhitePapersId).is(':checked'))
				{
					var liTag = document.createElement("li");
					liTag.id = 'li_' + lCEWhitePapersId;
					liTag.className = "ui-state-default";
<!--- 					liTag.innerHTML = lCEWhitePapersId + '&nbsp;&nbsp;&nbsp;' + $('#s_content_' + lCEWhitePapersId).html(); --->
					liTag.innerHTML = lCEWhitePapersId + '&nbsp;&nbsp;&nbsp;' + desc;
					document.getElementById('sortable').appendChild(liTag);
				}
				else
				{	
					var d = document.getElementById('sortable');
					var olddiv = document.getElementById('li_' + lCEWhitePapersId);
					d.removeChild(olddiv);
				}
			}
			else
			{
				alert('Some error occured!!');
				//revert the checkbox action
				if ($("#SelectWhitePapers" + lCEWhitePapersId).is(':checked'))
					$("#SelectWhitePapers" + lCEWhitePapersId).attr('checked', false);
				else
					$("#SelectWhitePapers" + lCEWhitePapersId).attr('checked', true);
				
			}
		});
	}
</script>

<style type="text/css">
	ul{
		list-style-type:none;
	}
	li{
		padding:2px 0px 2px 5px;
	}
</style>


<script type="text/javascript">
	$(function() {
		$( "#sortable" ).sortable({
			revert: true,
			zIndex: 99999,
			opacity: 0.6,
			stop: function(event, ui) {
					order = [];
					$('ul#sortable').children('li').each(function(idx, elm) {
					  order.push(elm.id)
					}); 
						
				$.getJSON("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=changeOrder&order="+order+"&returnformat=json&queryformat=column", {}, function(res,code) 
					{
						if(parseInt(res.DATA.ID) != 1)
							alert('Some error occured!!');				
					});
				
		
					
				}
		});
		
		//alert($('#sortable').sortable('serialize'))
		<!---$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});--->
		<!---$( "ul, li" ).disableSelection();--->
	});
</script>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Select White Papers</a></li>
		<li><a href="#tabs-2"> White Papers List</a></li>
	</ul>
	<div id="tabs-1">
		<div style="text-align:left;">
            <table id="whitePapersList_LifeCycleEvent"></table>
			<div id="whitePagerDiv"></div>
        </div>
	</div>
	<cfquery name="externalData" datasource="#Session.DBSourceEBM#">
		select 	ProgramInfo_vch
		from	simpleobjects.lcelinksexternalcontent
		where	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				and LCEItemId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LCEITEMID#">
				and LCEListId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLCELISTID#">
	</cfquery>
	<cfset lstWhitePapersIds = "">
	<div id="tabs-2">
		<ul id="sortable">
			<cfloop query = "externalData" startRow = "1" endRow = "#externalData.RecordCount#">
				<cfset Ids = ToString("#externalData.ProgramInfo_vch#").Split(";")/>
				<cfif Arraylen(Ids) GT 0>
					<cfloop index = "listElement" list = "#Ids[1]#" delimiters = ","> 
						<cfquery name="findSelectedWhitePapers" datasource="#Session.DBSourceEBM#">
					    	SELECT 	LCEWhitePapersID_int, Content, Desc_vch
					        FROM	simpleobjects.lcewhitepapers
					        WHERE	LCEWhitePapersID_int =  #listElement#
					    </cfquery>
			            <cfoutput query="findSelectedWhitePapers">
			            	<li id="li_#LCEWhitePapersID_int#" class="ui-state-default">#LCEWhitePapersID_int#&nbsp;&nbsp;&nbsp;#Content#</li>
			            </cfoutput>
					</cfloop>
				</cfif>
			</cfloop>
		</ul>
	</div>
    
</div>
