<cfparam name="INPBATCHID" default="0">
<cfinclude template="cx_mcid_tree.cfm">


<!---<div id="TemplateChannelSelectRadio" style="padding-left:20px;" align="center">
	<h1>Choose your communications channel you wish the template to target.</h1>
	<div style="display:inline; padding-right:20px;"><input name="VoiceTemplate" type="checkbox" value="VOICE"/> Voice </div>
    <div style="display:inline; padding-right:20px;"><input name="eMailTemplate" type="checkbox" value="EMAIL"/> eMail </div>
    <div style="display:inline; padding-right:20px;"><input name="SMSTemplate" type="checkbox" value="SMS"/> SMS  </div>
	
	
	reach, acquisition, conversion, retention, and loyalty
	
	ire analysts have developed a matrix that breaks the customer life cycle into five distinct steps: reach, acquisition, conversion, retention, and loyalty. In layman's terms, this means getting a potential customer's attention, teaching them what you have to offer, turning them into a paying customer, and then keeping them as a loyal customer whose satisfaction with the product or service urges other customers to join the cycle. The customer life cycle is often depicted by an ellipse, representing the fact that customer retention truly is a cycle and the goal of effective CRM is to get the customer to move through the cycle again and again.

Related glossary terms:sales cycle, silent attrition, CRM (customer relationship management), Hospitality Financial and Technology Professionals (HFTP), one-to-one-ire (1:1 ire), Hospitality Information Technology Association (HITA), chief customer officer (CCO), loyalty card program, coalition loyalty program, transactional ire



</div>--->

<div id="EBMVoiceDynamicContent_PickerDiv"></div>

<script type="text/javascript">
var SERVICE_DATA = {
	title: 'Best Practices - Life Cycle Events',
	items: [	
		/* MessageBroadcast Services  */
		{
			title: "Reach",
			items: [
				{
					title: "Click for the Expert",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='9494000553' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "<CCD CID='9494283111' SCDPL='100' DRD='2' RMin='1' ESI='<cfoutput>#ESIID#</cfoutput>'>0</CCD>",
					rel2: "",
					rel3: ""
				},
				{
					title: "Click for the Expert II",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='{%CustomField1_vch%}' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "<CCD CID='{%CustomField2_vch%}' SCDPL='100' DRD='2' RMin='1' ESI='<cfoutput>#ESIID#</cfoutput>'>0</CCD>",
					rel2: "",
					rel3: ""
				},
				{
					title: "Click for the Expert III",
					xml: "<RXSS><ELE BS='1' CK1='10' CK2='(1)' CK3='NR' CK4='(1,2)' CK5='-1' CK6='3' CK7=' 3' CK8='10' CK9='' DESC='Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='90' Y='80'><ELE ID='TTS' RXBR='16' RXVID='1'>Hello, there is a customer waiting for help on the EBM web site. Press 1 to accept and connect the call now.</ELE></ELE><ELE BS='1' CK1='9494000553' CK2='0' CK3='0' CK4='' CK5='' CK6='0' CK7='' CK8='0' DESC='Please hold while I transfer your call' DI='0' DS='0' DSE='0' DSUID='10' LINK='' QID='2' RXT='4' X='240' Y='319.99998474121094'><ELE ID='TTS' RXBR='16' RXVID='1'>Please hold while I transfer your call</ELE></ELE><ELE BS='1' CK1='0' CK5='-1' DESC='I am sorry that is an invalid response' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='3' RXT='1' X='230' Y='180'><ELE ID='TTS' RXBR='16' RXVID='1'>I am sorry that is an invalid response</ELE></ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},		
	/* Financial Services */
		{
			
			title: "Acquisition",
			items: [
				{
					title: "Wire Transfer",
					xml: "<RXSS><ELE BS='0' CK1='0' CK2='3,1,2' CK3='NA' CK4='(1,2),(2,3),(3,5)' CK5='-1' CK6='0' CK7='0' CK8='0' CK9='25' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='105' Y='72.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='4' DESC='For More Information Option 1' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RXT='1' X='20' Y='299.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK2='0' CK3='0' CK4='0' CK5='4' CK6='0' CK7='0' CK8='0' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='3' RXT='4' X='280' Y='230'>0</ELE><ELE BS='0' CK1='0' CK5='-1' DESC='Goobye!' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RXT='1' X='346.9999694824219' Y='456.99998474121094'>0</ELE><ELE BS='0' CK5='4' DESC='Description Not Specified' DSUID='10' LINK='0' QID='5' RXT='5' X='410' Y='80'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Overdue",
					xml: "<RXSS><ELE BS='0' CK1='0' CK2='2,1' CK3='NA' CK4='(1,2),(2,3)' CK5='-1' CK6='0' CK7='0' CK8='0' CK9='25' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='2' X='105' Y='72.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='4' DESC='For More Information Option 1' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RXT='1' X='105' Y='300'>0</ELE><ELE BS='0' CK1='0' CK2='0' CK3='0' CK4='0' CK5='4' CK6='0' CK7='0' CK8='0' DESC='Description Not Specified' DI='0' DS='0' DSE='0' DSUID='10' LINK='0' QID='3' RXT='4' X='298.9999694824219' Y='126.99998474121094'>0</ELE><ELE BS='0' CK1='0' CK5='-1' DESC='Goobye!' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RXT='1' X='346.9999694824219' Y='456.99998474121094'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Received",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},	
	/* Utilities */
		{
			title: "Conversion",
			items: [
				{
					title: "Late Payment",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Collections",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Payment Due",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Healthcare */
		{
			title: "Retention",
			items: [
				{
					title: "Appointment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Lab Results with Authentication for appropriate recipient",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Rx Refill Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		},
	/* Real Estate */
		{
			title: "Loyalty",
			items: [
				{
					title: "Missing Forms",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Escrow Updates",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Appointment Reminders",
					xml: "<RXSS><ELE QID='1' RXT='1' BS='0' DSUID='10' DS='0' DSE='0' DI='0' CK1='0' CK5='-1' X='50' Y='0' LINK='0' DESC='Description Not Specified'>0</ELE></RXSS>",
					CCD: "",
					rel2: "",
					rel3: ""
				}
			]
		}
	]
};
</script>

<script type="text/javascript">

///////////////////////////////////////////////////////////
// Sample
$(function(){
	$.cx_mcid_tree({
		data: SERVICE_DATA,
		clazz: "cx_mcid_tree",
		itemWidth: 80,
		itemHeight: "auto",
		itemLength: 24,
		onClick: function(item){
			$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			jConfirm(
				"Are you sure you want to replace your existing content with this template?",
				"About to apply template.",
				function(result){
					if(result){
						if (item.xml == '') {
							// WriteRXSSXMLString_Template('<RXSS></RXSS>');
						} else {
							// WriteRXSSXMLString_Template(item.xml, item.CCD);
						}						
						
						//$("#SMSStructureMain #inpCK1").val(item.xml);
						//$("#SMSStructureMain #inpCK2").val(item.rel2);
						//$("#SMSStructureMain #inpCK3").val(item.rel3);
						//	CreateTemplatePickerDialogVoiceTools.remove();
					}
					return false;
				}
			);
		},
		onDraw: function(div){
			$("._cx_mcid_tree_container_").remove();
			var container = $('#EBMVoiceDynamicContent_PickerDiv');
			container.html(div).attr("class","_cx_mcid_tree_container_");
			container = null;
		}
	});
});
</script>