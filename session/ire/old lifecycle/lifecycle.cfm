<cfimport taglib="../../lib/grid" prefix="mb">
<cfparam name="inpGroupId" default="0">
<cfparam name="inpShowSocialmediaOptions" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#LifeCycle_Title#">
</cfinvoke>

<cfoutput>
	
	<cfscript>
		htmlOption =  "<a href ='#rootUrl#/#SessionPath#/ire/lifeCycle/lifeCycleView?INPLCELISTID={%LCEListId_int%}' ><img class='ListIconLinks img18_18 preview_18_18' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='View LCE'></a>";
		htmlOption = htmlOption & "<a href ='#rootUrl#/#SessionPath#/ire/lifeCycle/lifeCycleEdit?INPLCELISTID={%LCEListId_int%}' ><img class='ListIconLinks img18_18 edit_18_18' rel='{%LCEListId_int%}}' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' title='Edit LCE'></a>";
		htmlOption = htmlOption & "<img onclick = 'DelLCEList({%LCEListId_int%},{%PageRedirect%})' class='deleteCPP ListIconLinks img18_18 delete_18_18' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' width='16' height='16' rel='{%LCEListId_int%}}' title='Delete LCE'";
		
	</cfscript>
	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>
		
	<!--- Prepare column data---->
	<cfset arrNames = ['List Id','Description','Created Date', 'Last Updated Date', 'Options'] >	
	
	<cfset arrColModel = [			
			{name='LCEListId_int', width="10%"},
			{name='Desc_vch', width="40%"},	
			{name='Created_dt',width="15%"},
			{name='LASTUPDATED_DT', width="15%"},
			{name='Options', width="20%", format = [htmlFormat]}
		   ] >	 
		   
	<mb:table 
		component="#LocalSessionDotPath#.cfc.ire.LifeCycle"
		method="GetLCELists" 
		class="cf_table"  
		width="100%"
		colNames= #arrNames# colModels= #arrColModel#
		name="LCEs"
	>
	</mb:table>		
</cfoutput> 

<script>

	$('#subTitleText').text('<cfoutput>#LifeCycle_Title#</cfoutput>');
	$('#mainTitleText').text('ire');

	function DelLCEList(INPLCELISTID,PageRedirect)
	{
	//	$("#loadingDlgAddBatchLCEList").show();	
		jConfirm( "Are you sure you want to delete this Life Cycle Events map?", "Delete Life Cycle Events map", function(result) { 	
			if(result){
				$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/lifecycle.cfc?method=DeleteLCEList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { INPLCELISTID : INPLCELISTID},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{						
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										document.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/lifeCycle/lifeCycle?page='+PageRedirect;
										return false;
											
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("LCE has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
									}
								}
								else
								{<!--- Invalid structure returned --->	
									
								}
							}
							else
							{<!--- No result returned --->		
								$.alerts.okButton = '&nbsp;OK&nbsp;';					
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
												
				//			$("#AddNewBatchLCEList #loadingDlgAddBatchLCEList").hide();
										
					} 		
					
				});
			}
				
		});
		
		return false;
	}

</script>
