<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#LifeCycle_Create_Title#">
</cfinvoke>

<cfoutput>
        
<div id='AddNewLCEListMainNavigator' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Add a New Life Cyle Event Map</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/LCEListNewIconWeb.png" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul>
         		<li>Give your <i>Map</i> a title you can use to uniquely distiguish it.</li>
                <li>Press "Add" button when finished.</li>
                <li>Press "Cancel" button to exit without creating a new one.</li>               
            </ul>   
            
            <BR />
            <BR />
            <i>DEFINITION:</i> A Life Cycle Event Map is a logical content container for managing single or multiple Life Cycle Events and thier associated messaging content.
                   
        </div>
        
	</div>
    
    <div id="RightStage">
        <cfform action="#rootUrl#/#SessionPath#/cfc/ire/lifeCycle.cfc?method=AddNewLCEList" method="post" >
        
         <input TYPE="hidden" name="INPLCEListID" id="INPLCEListID" value="0" />
        
                <label>Map Title
                <span class="small">Used as a helpful reminder of what this map is all about.</span>
                </label>
                <cfInput TYPE="text" name="Desc_vch" id="inpLCEListDesc" size="130" class="ui-corner-all" required="true" validateat="onSubmit, onServer" message="Map Title is required!" >
                <BR>
				<cfinput id="AddNewLCEListButton" TYPE="submit" class="ui-corner-all"  value="Add" name="submit" >
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
        </cfForm>      
    
    </div>

</div>

</cfoutput>

<script TYPE="text/javascript">
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#LifeCycle_Title# >> #LifeCycle_Create_title#</cfoutput>');
		$('#mainTitleText').text('ire');
			
		$("#AddNewLCEListMainNavigator #Cancel").click( function(){
			window.location.href = "<cfoutput>#rootUrl#/#sessionPath#/ire/lifeCycle/lifeCycle</cfoutput>"
			return false;
	  	}); 	
	} );
		
</script>
