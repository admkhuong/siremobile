<cfparam name="INPLCELISTID" default="0">

<cfparam name="inpSA" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#LifeCycle_edit_Title#">
</cfinvoke>

<cfoutput>
	<style>
		@import url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/bb.ui.jqgrid.css');
		@import url('#rootUrl#/#PublicPath#/css/ire/lifecycle.css');
	</style>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/cloud-carousel.1.0.5.RXMod.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jqGrid.min.Light.RXMod.js" ></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/grid.locale-en.js" ></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery.jqGrid.min.Light.RXMod.js"></script>

</cfoutput>
<script type="text/javascript">
	
	$('#subTitleText').text('<cfoutput>#LifeCycle_Title# >> #LifeCycle_edit_title#</cfoutput>');
	$('#mainTitleText').text('ire');
	$('#menu_left').height(700);
	
	var DefaultBatchId = 0;
	var sipPos_LCEAdd = 175;
	
	$(function() 
	{					
		ReloadLCEListData("#LCELocal #carousel_LCEListItems");
		
		$("#AddNewLCEToListButton").click(function(){			
			AddNewLCE();			
			
		});
							
		$("#ChooseNewLCEToListButton").click(function(){			
			TemplatePickerDialogLCEListItems();			
			
		});								

		<!--- Animated filters tab--->
		$("#panel_tab_LCEAdd").click(function(e) {
			e.preventDefault();
			$("#panel_LCEAdd").animate({ left: sipPos_LCEAdd }, 565, 'linear', function()
			 {
				if(sipPos_LCEAdd == 175) 
				{ 
					sipPos_LCEAdd = -300; 
					$("#panel_tab_LCEAdd").html('C<BR/>l<BR/>o<BR/>s<BR/>e<BR/><BR/>');
				}
				else
				{
					 sipPos_LCEAdd = 175; 
					 $("#panel_tab_LCEAdd").html('A<BR/>d<BR/>d<BR/><BR/>L<BR/>C<BR/>E<BR/>');
				}
			});
		});
	
		$("#loadingDlgAddLCEToList").hide();

	
	});
		
	<!--- Initialize carousel based on expected size --->
	function InitLCECarousel(inpObjName, inpItemCount)
	{
		
		var inpyRadius = 300;
		var inpxRadius = 700;
		
		if(inpItemCount > 0 && inpItemCount <= 5)
		{			
			inpyRadius = 150;	
			inpxRadius = 400;					
		}
		else if(inpItemCount > 5 && inpItemCount <= 10)
		{
			inpyRadius = 300;
			inpxRadius = 700;			
		}
		else
		{			
			inpyRadius = 450;
			inpxRadius = 800;
		}		
					
		if(inpItemCount > 0)	
		{			
			$(inpObjName).CloudCarousel(		
					{			
								
						xPos: 600,
						yPos: 280,
						yRadius: inpyRadius,
						xRadius: 700,
						buttonLeft: $("#left-but2"),
						buttonRight: $("#right-but2"),
						altBox: $("#alt-text2"),
						titleBox: $("#title-text2"),
						reflHeight: 65,
						reflGap: 4,
						reflOpacity: 0.5,
						FPS: 30,
						speed:0.20,
						minScale: 0.4,
						bringToFront: true,
						mouseWheel: true				
					}
				);

		}
	}
				
			
<!--- inpObjName = "#LCELocal #carousel_LCEListItems"--->			
function ReloadLCEListData(inpObjName, inpCallBack)
{
	$("#loadingLCEListItems").show();		
			
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=GetLCEListData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {INPLCELISTID : <cfoutput>#INPLCELISTID#</cfoutput>},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var LCEListItems = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								if(CurrRXResultCode > 0)
								{								
								// <img class="del_LCEItemRow ListIconLinks" rel="' + d.DATA.LCEITEMID_INT[x] + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons16x16/delete_16x16.png" width="16px" height="16px">
							// <img class="del_LCEItemRow ListIconLinks" rel="' + d.DATA.LCEITEMID_INT[x] + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons16x16/delete_16x16.png" width="16" height="16">
				
									<!--- Start a carosel item --->
									LCEListItems += '<div class = "cloudcarousel" >';
									
									<!--- Delete option--->
									LCEListItems += '	<div class="del_LCEItemRow carouseloutrider" rel="' + d.DATA.LCEITEMID_INT[x] + '" rel2="' + d.DATA.DESC_VCH[x] + '"></div>';
									
									LCEListItems += '	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/LCEBGOvalWeb.png" alt="' + d.DATA.DESC_VCH[x] + '" title="' + d.DATA.DESC_VCH[x] + '" class="cloudcarouselIMG" />';
									LCEListItems += '	<div class="caption CenterTextBubble red" alt="x">' + d.DATA.DESC_VCH[x] + '</div>';
									LCEListItems += '	<div class="caption lower center spacy">Planet Jeff</div>';
									
									<!--- Outriders--->
									LCEListItems += '	<div class="carouseloutrider carouseloutrider1" onclick="EditBatchListForProjectId(\'' + d.DATA.LCEITEMID_INT[x] + '\')" id="LCEProjectIds"><div class="CenterTextBubble red">Project IDs</div></div>';
									LCEListItems += '	<div class="carouseloutrider carouseloutrider2" id="LCEExternal"><div class="CenterTextBubble red">External Content</div></div>';
									LCEListItems += '	<div class="carouseloutrider carouseloutrider3" onclick="EditWhitePapersList(\'' + d.DATA.LCEITEMID_INT[x] + '\')" id="LCEWhitePager"><div class="CenterTextBubble red">White Papers</div></div>';
									LCEListItems += '	<div class="carouseloutrider carouseloutrider4" onclick="EditROIList(\'' + d.DATA.LCEITEMID_INT[x] + '\')" id="LCEROTCAL"><div class="CenterTextBubble red">ROI Calculators</div></div>';
									LCEListItems += '	<div class="carouseloutrider carouseloutrider5" id="LCEOtherStuff"><div class="CenterTextBubble red">Other Stuff</div></div>';
									
									<!--- Close carosel item --->
									LCEListItems += '</div>';
				
									//  LCEListItems += '<div class="del_LCEItemRow"></div><div class = "cloudcarousel" ><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/LCEBGOvalWeb.png" alt="' + d.DATA.DESC_VCH[x] + '" title="' + d.DATA.DESC_VCH[x] + '" class="cloudcarouselIMG" /><div class="caption CenterTextBubble red" alt="x">' + d.DATA.DESC_VCH[x] + '</div><div class="caption lower center spacy">Planet Jeff</div><div class="carouseloutrider carouseloutrider1"><div class="CenterTextBubble red">Project IDs</div></div><div class="carouseloutrider carouseloutrider2"><div class="CenterTextBubble red">External Content</div></div><div class="carouseloutrider carouseloutrider3"><div class="CenterTextBubble red">White Papers</div></div><div class="carouseloutrider carouseloutrider4"><div class="CenterTextBubble red">ROI Calculators</div></div><div class="carouseloutrider carouseloutrider5"><div class="CenterTextBubble red">Other Stuff</div></div></div>'
									
				
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								 LCEListItems += '<div class = "cloudcarousel" ><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/LCEBGOvalWeb.png" alt="' + d.DATA.DESC_VCH[x] + '" title="' + d.DATA.DESC_VCH[x] + '" class="cloudcarouselIMG" /><div class="caption CenterTextBubble red" alt="x">' + d.DATA.DESC_VCH[0] + '</div><div class="caption lower center spacy">Planet Jeff</div><div class="carouseloutrider carouseloutrider1"><div class="CenterTextBubble red">Project IDs</div></div><div class="carouseloutrider carouseloutrider2"><div class="CenterTextBubble red">External Content</div></div><div class="carouseloutrider carouseloutrider3"><div class="CenterTextBubble red">White Papers</div></div><div class="carouseloutrider carouseloutrider4"><div class="CenterTextBubble red">ROI Calculators</div></div><div class="carouseloutrider carouseloutrider5"><div class="CenterTextBubble red">Other Stuff</div></div></div>'
							}
														
							<!--- Allow differnet html elements to get filled up--->	
							$("#" + inpObjName).empty();
							$("#" + inpObjName).html(LCEListItems);
							
							InitLCECarousel(inpObjName, d.ROWCOUNT)				
						
							$("#carousel_LCEListItems .del_LCEItemRow").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 					$("#carousel_LCEListItems .del_LCEItemRow").click( function() { RemoveLCEItem($(this).attr('rel'), $(this).attr('rel2')); } );
							<!--- Each item is mapped backed to one or more Campaigns/BatchIDs  --->
							$("#carousel_LCEListItems #LCEProjectIds").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
																		
							if(typeof(inpCallBack) != 'undefined')
		  						inpCallBack();
								
							$("#loadingLCEListItems").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingLCEListItems").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}
	
	
	function AddNewLCE()
	{					
		$("#loadingDlgAddLCEToList").show();		
	
		 $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=AddNewLCEToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpLCEDesc : $("#inpLCEItemDesc").val(), INPLCELISTID : <cfoutput>#INPLCELISTID#</cfoutput>},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
		  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{										
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("LCE Item has been added.", "Success!", function(result) 
																			{ 
																				$("#loadingDlgAddLCEToList").hide();
																				ReloadLCEListData("#LCELocal #carousel_LCEListItems");
																				
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("LCE Item has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddLCEToList").hide();
			} 		
					
		});		
	
		return false;

	}
	
	
	
	function RemoveLCEItem(inpListItemId, itemDesc)
	{			
	
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "About to delete\n(" + itemDesc + ") \n\nAre you absolutely sure?", "About to delete LCE item from your list.", function(result) { if(!result){return;}else{	
	
			
			$("#loadingDlgAddLCEToList").show();		
		
			 $.ajax({
			  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/LifeCycle.cfc?method=DeleteLCEListItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			  dataType: 'json',
			  data:  {INPLCELISTITEMID : inpListItemId, INPLCELISTID : <cfoutput>#INPLCELISTID#</cfoutput>},					  
			  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			  success:
			  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{										
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("LCE Item has been removed.", "Success!", function(result) 
																				{ 
																					$("#loadingDlgAddLCEToList").hide();
																					ReloadLCEListData("#LCELocal #carousel_LCEListItems");
																					
																				} );								
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("LCE Item has NOT been removed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
								
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
						$("#loadingDlgAddLCEToList").hide();
				} 		
						
			});		
		
		}  } );<!--- Close alert here --->
		
	
		return false;

	}
	
	
	
<!--- Global so popup can refernece it to close it--->
var CreateTemplatePickerDialogLCEListItems = 0;

function TemplatePickerDialogLCEListItems()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';

	ParamStr = '?INPLCELISTID=' + encodeURIComponent(<cfoutput>#INPLCELISTID#</cfoutput>);
	
	<!--- Erase any existing dialog data --->
	if(CreateTemplatePickerDialogLCEListItems != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateTemplatePickerDialogLCEListItems.remove();
		CreateTemplatePickerDialogLCEListItems = 0;
	}
					
	CreateTemplatePickerDialogLCEListItems = $('<div></div>').append($loading.clone());
	
	CreateTemplatePickerDialogLCEListItems
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/LifeCycle/dsp_TemplatePicker' + ParamStr)
		.dialog({
			modal : true,
			title: 'Life Cycle Event Template Tool',
			width: 1250,
			minHeight: 200,
			height: 'auto',
			zIndex: 2600,
			//position: 'top' 
			position: 'top'
		});

	CreateTemplatePickerDialogLCEListItems.dialog('open');

	return false;		
}

function EditBatchListForProjectId(lCEItemId) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	formEditBatchList = $('<div></div>').append($loading.clone());
	var paramStr = '?INPLCELISTID=<cfoutput>#INPLCELISTID#</cfoutput>&LCEITEMID=' + lCEItemId;

	formEditBatchList
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/LifeCycle/dsp_EditBatchList' + paramStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Edit Batch List',
			width: 680,
			minHeight: 500,
			height: 'auto',
			position: 'top' ,
			close: function() 
			{ 
				$(this).dialog('destroy');
				$(this).remove();
				
			} 
		});

	formEditBatchList.dialog('open');
}

function EditWhitePapersList(lCEItemId) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	formEditWhitePapersList = $('<div></div>').append($loading.clone());
	var paramStr = '?INPLCELISTID=<cfoutput>#INPLCELISTID#</cfoutput>&LCEITEMID=' + lCEItemId;

	formEditWhitePapersList
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/LifeCycle/dsp_EditWhitePapersList' + paramStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Edit White Paper List',
			width: 690,
			minHeight: 500,
			height: 'auto',
			position: 'top' ,
			close: function() 
			{ 
				$(this).dialog('destroy');
				$(this).remove();
			}
		});

	formEditWhitePapersList.dialog('open');
}

function EditROIList(lCEItemId) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	formEditROIList = $('<div></div>').append($loading.clone());
	var paramStr = '?INPLCELISTID=<cfoutput>#INPLCELISTID#</cfoutput>&LCEITEMID=' + lCEItemId;

	formEditROIList
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/LifeCycle/dsp_EditROIList' + paramStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Edit ROI List',
			width: 800,
			minHeight: 500,
			height: 'auto',
			position: 'top' ,
			close: function() 
			{ 
				$(this).dialog('destroy');
				$(this).remove();
				
			}
		});

	formEditROIList.dialog('open');
}

function IsNumber(value){
    if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
        return true;
    } else {
        return false;
    }
}

</script>

<cfoutput>

<div id="LCEListMenuLeft">

  <div id="panel_LCEAdd" style="z-index:999;">
          <a href="##" id="panel_tab_LCEAdd">A<BR/>d<BR/>d<BR/><BR/>L<BR/>C<BR/>E<BR/></a>
          
          	 <!---     <div id='BuddyListSubMenu' style="text-align:left; margin: 5px 0 5px 0;">
	  
                        Desc <input type="text" id="NewLCEDesc" name="NewLCEDesc" value="10" class="ui-corner-all" style="width:50px; text-align:center;" />
                		<br />
                		<button id="AddLCEToListButton">Add</button> <button id="CancelAddLCEToListButton">Cancel</button>
				        
                </div>--->
				
								<!---
								
								http://searchcrm.techtarget.com/definition/customer-life-cycle
												
								
								In my previous post, I talked about how poor data quality associated with the product and service life cycle, especially across the accuracy, completes, timeliness, and currency dimensions, could impact the ability to execute opportunities for proactive customer retention. But retention opportunities are not limited to interactions with the customer in reference to the life cycle of the product; the customer's own life cycle events will also present opportunities for positive interactions. As an example, a month before my son's birthday, he gets a letter from a local toy store with a $5 coupon.
 
There are many types of life cycle events that could trigger an interaction – birthdays, anniversaries, changes in marital status, birth of a child. And effective management of customer data provides ample opportunity for customer outreach, especially for businesses with an expectation for brand loyalty. And with the right information, a company can extend the customer relationship over a long period of time. Another good example is the company that makes diapers – after each of our children was born, they consistently sent us coupons for different sized diapers in relation to our baby's age.
 
On the other hand, a company that does not internalize knowledge about life cycle events is less likely to maintain the relationship. A good example is an airline we used when our oldest child was about one year old. On our trip to Florida, we bought her a ticket, and because she was under the age of 2, we were able to purchase an "infant's seat." At the same time we enrolled her in the airlines' frequent flyer program. Not long after our trip, she got an email from a bank telling her that she was pre-approved for an airline affinity credit card. Needless to say, this demonstrated a failure to share accurate information across the spectrum of participants – the frequent flyer program should not have given the bank the names of individuals younger than 2! This last example again demonstrates the value of not just maintaining quality data, but maintaining high quality information management processes. For more on this topic, read my new white paper on


ire analysts Jim Sterne and Matt Cutler have developed a matrix that breaks the customer life cycle into five distinct steps: reach, acquisition, conversion, retention, and loyalty. In layman's terms, this means getting a potential customer's attention, teaching them what you have to offer, turning them into a paying customer, and then keeping them as a loyal customer whose satisfaction with the product or service urges other customers to join the cycle. The customer life cycle is often depicted by an ellipse, representing the fact that customer retention truly is a cycle and the goal of effective CRM is to get the customer to move through the cycle again and again.

Related glossary terms:sales cycle, silent attrition, CRM (customer relationship management), Hospitality Financial and Technology Professionals (HFTP), one-to-one-ire (1:1 ire), Hospitality Information Technology Association (HITA), chief customer officer (CCO), loyalty card program, coalition loyalty program, transactional ire


There are many types of life cycle events that could trigger an interaction – birthdays, anniversaries, changes in marital status, birth of a child. And effective management of customer data provides ample opportunity for customer outreach, especially for businesses with an expectation for brand loyalty. And with the right information, a company can extend the customer relationship over a long period of time. Another good example is the company that makes diapers – after each of our children was born, they consistently sent us coupons for different sized diapers in relation to our baby's age.
 
On the other hand, a company that does not internalize knowledge about life cycle events is less likely to maintain the relationship. A good example is an airline we used when our oldest child was about one year old. On our trip to Florida, we bought her a ticket, and because she was under the age of 2, we were able to purchase an "infant's seat." At the same time we enrolled her in the airlines' frequent flyer program. Not long after our trip, she got an email from a bank telling her that she was pre-approved for an airline affinity credit card. Needless to say, this demonstrated a failure to share accurate information across the spectrum of participants – the frequent flyer program should not have given the bank the names of individuals younger than 2! This last example again demonstrates the value of not just maintaining quality data, but maintaining high quality information management processes. For more on this topic, read my new white paper on 




								--->
								
					<div id="LeftMenuLCE">
				
						<div width="100%" align="center" style="margin-bottom:5px;"><h1>Add a New Life Cycle Event To List</h1></div>
				
						<div width="100%" align="center" style="margin-bottom:20px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/LifeCycleWeb128x128.png" /></div>
				
						<div style="margin: 10px 5px 10px 20px; line-height: 20px;" class="rxform2">
							<ul>
								<li>Choose your <i>Life Cycle Event (LCE)</i> from a list we have put together</li>                                 
                                <button id="ChooseNewLCEToListButton" TYPE="button" class="ui-corner-all">Choose</button>                              
								<BR />
                                <BR />
                                OR...                                
                                <BR />
                                <BR />
								<li>Add your own</li>
                                <label>Life Cycle Event (LCE)
                                <span class="small">Should be unique.</span>
                                </label>
                                <BR />                               
                                <input TYPE="text" name="inpLCEItemDesc" id="inpLCEItemDesc" class="ui-corner-all" style="display:inline; width:300px;" />
                                <button id="AddNewLCEToListButton" TYPE="button" class="ui-corner-all" style="display:inline">Add</button>
                                
                                <div id="loadingDlgAddLCEToList" style="display:inline;">
                                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                                </div>
                    
							</ul>
						     
							<i>DEFINITION:</i> In customer relationship management (CRM), a customer <b>Life Cycle Event (LCE)</b> is a term used to describe the steps a customer progressively goes through when considering, purchasing, using, and maintaining loyalty to a product or service.
							
						</div>
										
					</div>
				
     </div>
     
     
  	<div id="buttons2">
		<!--- Define left and right buttons. --->
        <div id="right-but2" class="carouselRight"></div>
		<div id="left-but2" class="carouselLeft"></div>
    </div>
        
	<div id="text2">
            <!--- Define elements to accept the alt and title text from the images. --->
                   
            <div id="title-text2" style="display: block;"></div>
			<div id="alt-text2" style="display: block;"></div>

	</div>
</div>

<div id="LCELocal">


 		        
        <!--- http://www.professorcloud.com/mainsite/carousel.htm --->
		<!--- This is the container for the carousel. --->
        <div id="carousel_LCEListItems" >            
            
                         
        </div>
        
        <BR />
        <BR />
        
  
    
    
    
  
</div>

</cfoutput>