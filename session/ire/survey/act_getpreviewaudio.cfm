<cfparam name="audioId" default="" />
<cfparam name="recordedAudioId" default="" />
<cfinclude template="../../../public/paths.cfm">
 <cfinclude template="../../lib/audio/inc_rangeDownloadWav.cfm">

<cfset outFileName = "previewSurvey.mp3">
<cfset CURRETAG = "">

<cfparam name="Session.USERID" default="">
<cfset ListenUserId = 0>


<cfif Session.USERID GT 0>
	<cfset ListenUserId = Session.USERID>
</cfif>
<!--- Share audio file from BabbleSphere --->
<cfset audioType = "wav">
<cfif audioId NEQ "">
	<cfset ids = ListToArray(audioId, "_") />
	<cfset inpUserId = ids[1] /> 
	<cfset inpBatchId = ids[2] /> 
	<cfset inpTimestamp = ids[3] /> 
	<cfset MainAudioFile = "#application.baseDir#/TTS/u#inpUserId#/#inpBatchId#/tts_audio_#inpTimestamp#.mp3" />
	<cfset audioType = "mp3">
</cfif>

<cfif recordedAudioId NEQ "">
	<cfset ids = ListToArray(recordedAudioId, "_") />
	<cfset userId = ids[1] />
	<cfset libId = ids[2] />
	<cfset eleId = ids[3] />
	<cfset scrId = ids[4] />
	<cfset MainAudioFile = "#rxdsLocalWritePath#/U#userId#/L#libId#/E#eleId#\RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3" />
	<cfset audioType = "mp3">
</cfif>

<cfif isDefined("MainAudioFile")>
	<cftry>
		<cfif FileExists(MainAudioFile)>
			<cfset returnAudio(MainAudioFile, audioType)>
	    </cfif>
   
		<cfcatch>
			<cfdump var="#cfcatch#">
		</cfcatch>
	</cftry>
</cfif>