<cfparam name="inpFileId" default="">
<cfset r = structNew()>
<cfset r["msg"] = "Failed to process file upload">
<cfset r["error"] = "Unhandled Error">
<cfset r["UniqueFileName"] = "error">
<cfset r["OrgFileName"] = "error">

<cfoutput>

	<cftry> 
    
	    <!--- Validate session still in play - handle gracefully if not --->
	     <cfif Session.USERID LT "1"  >
              <cfthrow MESSAGE="User Session Has Expired - Please relog in." TYPE="Any" extendedinfo="" errorcode="-2">
         </cfif>    
                    
		<!--- Does user exist? Auto add/repair system directory structure--->
        <!--- Only works with current session user --->
		<!--- rxdsLocalWritePath --->
        <cfif !DirectoryExists("#rxdsWebProcessingPath#\survey/U#Session.USERID#")>
			<cfdirectory action="create" directory="#rxdsWebProcessingPath#\survey/U#Session.USERID#">
		</cfif>

        <cfif structKeyExists(form, "#inpFileId#") and len(form["#inpFileId#"])>
			<cftry>
	        	<cffile action="upload" 
	                    filefield="#inpFileId#" 
	                    destination="#rxdsWebProcessingPath#\survey/U#Session.USERID#\" 
	                    nameconflict="makeunique"
	                    	<!--- accept="image/jpg" --->
	                    result = "CurrUpload"> 
	             <!---       
	            <cfset sourceFile = "#rxdsWebProcessingPath#\survey/U#Session.USERID#\#inpImgServerFile#">
	           	<cfif FileExists(#sourceFile#)>
	           		<cfscript>
			            FileDelete(#sourceFile#);
						</cfscript>
		        </cfif>
		        --->
				<!---<cfset cfImage = imageNew(#rxdsWebProcessingPath#\survey/U#Session.USERID#\#CurrUpload.serverFile#)>--->
				<cfset r["fileSize"] = "#CurrUpload.fileSize#">
				<cfset r["clientFileExt"] = "#CurrUpload.clientFileExt#">
				 
				<cfset r["msg"] = "Upload successfully.">
				<cfset r["error"] = "-1">
				<cfset r["UniqueFileName"] = "#CurrUpload.serverFile#">
				<cfset r["OrgFileName"] = "#CurrUpload.clientFile#">	
				<cfcatch>
					<cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail# -#rxdsWebProcessingPath#\survey/U#Session.USERID#">            
				</cfcatch>
			</cftry>
		</cfif>                       
    <cfcatch TYPE="any">
                  
        <cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#">            
    
    </cfcatch>
            
    </cftry>

</cfoutput>


<!--- Output results in JSON format --->
<cfoutput>#serializeJSON(r)#</cfoutput>
