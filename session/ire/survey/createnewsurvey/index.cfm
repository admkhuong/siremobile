
<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Survey_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #Create_Survey_Title#</cfoutput>');
	$('#subTitleText').html('Create a new Survey');	
</script>


<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Create_Survey_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Create_Survey_Title#">
</cfinvoke>

<!--- Import constant variables ---->
<cfinclude template="../../constants/surveyConstants.cfm">

<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
	</style>
</cfoutput>


<cfoutput>
	   
       
	<div id='AddNewSurveyListMainNavigator' class="RXForm">
	    
	    <div id="RightStage">
	                       
	        <cfform id="AddSurveyForm" name="AddSurveyForm" action="" method="POST">
	         <input TYPE="hidden" name="INPLCEListID" id="INPLCEListID" value="0" />
	        
	                <div class="left clear field_padding field_pdBottom0">
		                <label><strong><cfoutput>#SURVEY_LABEL_NAME#</cfoutput></strong>
		                <!--- <span class="small">Used as a helpful reminder of what this survey is all about.</span> --->
		                </label>
					</div>       
					<div class="left clear field_padding field_pdTop0">
		                <input TYPE="text" name="inpSurveyDesc" id="inpSurveyDesc" size="50"/> 
		                <br>
		                <label style="display: none;" class="error" id="errSurveyTitle">
						<strong>This field's required.</strong></label>
					</div>
					
					<div id="UpdateBatch" style="display:none;" class="clear left field_padding">
						<label><cfoutput>#SURVEY_LABEL_CHOOSE_CAMPAIGN#</cfoutput></label>
						<select id="listBatchId"></select>
					</div>
	                <div class="clear left field_padding">
						<label><strong><cfoutput>#SURVEY_LABEL_EXPIRED#</cfoutput></strong></label>
					</div>
					<div class="clear left field_padding">
						<input name="IsExpire" id="rdoNoExpire" 
							class="left margin_left_5px" type="radio" value="NO" 
							onclick="ChangeExpirationOption(this)" checked="checked">
						<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_NO#</cfoutput></span>
						<input name="IsExpire" id="rdoYesExpire" class="left margin_left_20px" 
							type="radio" onclick="ChangeExpirationOption(this)" value="YES"
							checked=""
							>
						<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_YES#</cfoutput></span>
					</div>			
					<div class="left clear field_padding" style="display: none;" id="Expiration_Date_Picker">
						
						<!---<cfinput 
							type="datefield" 
							value="" 
							name="SURVEY_EXPIRATION_DATE" 
							id="SURVEY_EXPIRATION_DATE" 
							validate="date" 
							mask="mm/dd/yyyy"
							maxlength="10"
							message="#SURVEY_ERR_DATE#"
							onChange="IsValidExpirationDate(); return false;"
						 /> --->
						 
						 <input type="text" id="SURVEY_EXPIRATION_DATE" name="SURVEY_EXPIRATION_DATE" value="" onchange="IsValidExpirationDate(); return false;" maxlength="10">
						 
						<br /><span class="left expiry_date_time"><cfoutput>#SURVEY_LABEL_END_AT#</cfoutput></span>
						<div class="left clear">
							<label id="lblInvalidExpirationDateError" class="error" style="display: none;">
								<cfoutput>#SURVEY_ERR_DATE#</cfoutput></label>
							<label id="lblInvalidPastDateError" class="error" style="display: none;">
								<cfoutput>#SURVEY_ERROR_PAST_DATE#</cfoutput></label>	
						</div>
					</div>
					<div class="field_padding left clear">
						<label class="qe-label-a left"><strong><cfoutput>#SURVEY_LABEL_DELIVERY#</cfoutput></strong></label>
						<label class='warning'><cfoutput>#SURVEY_LABEL_DELIVERY_WARNING#</cfoutput></label>
						<div class="clear left">
                        
							<input name="chkTypeRadio" id="chkTypeRadio" class="left margin_left_5px" type="radio" value="#SURVEY_COMMUNICATION_ONLINE#">
							<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_ONLINE#</cfoutput></span>
							
                            <input name="chkTypeRadio" id="chkTypeRadio" class="left margin_left_20px" type="radio" value="#SURVEY_COMMUNICATION_PHONE#">
							<span class="left margin_left_5px" id="spnVoice" name="spnVoice"><cfoutput>#SURVEY_LABEL_PHONE#</cfoutput></span>
							
                            <input name="chkTypeRadio" id="chkTypeRadio" class="left margin_left_20px" type="radio" value="#SURVEY_COMMUNICATION_SMS#">
							<span class="left margin_left_5px" id="spnSMS" name="spnSMS"><cfoutput>#SURVEY_LABEL_SMS#</cfoutput></span>
							                            
                            <br>
							<label class="error" id="lblSurveyDeliveryMethodError" style="display: none;">
								<strong>Please choose survey delivery method!</strong></label>
						</div>
					</div>
					<div id="gender_container" style="display:none">
						<div class="clear left field_padding">
							<label><strong><cfoutput>#SURVEY_LABEL_SPEAKER#</cfoutput></strong></label>
						</div>
						<div class="clear left field_padding">
							<select id="inpSpeaker" name="inpSpeaker">
								<option value="1">Tom</option>
								<option value="2">Samantha</option>
								<option value="3">Paulina</option>
								<option value="4">Mary</option>
								<option value="5">Sam</option>
								<option value="6">Mike</option>
							</select>
						</div>	
					</div>
	
			        <div class="CreateSurveyButton  left clear">
	                	<button id="cancel" 
								TYPE="button" 
								class="ui-corner-all" 
								onclick="cancelCreate()">
									<cfoutput>#SURVEY_LABEL_CANCEL#</cfoutput>
							</button>
						<span>or 
							<button id="NextToEditSurveyContent" TYPE="button" class="ui-corner-all" ><cfoutput>#SURVEY_LABEL_NEXT#</cfoutput></button>
						</span>
	          		</div>
	        </cfform>
	    
	    </div>
	
	</div>

</cfoutput>

<script language="javascript" type="text/javascript">
	//window.onbeforeunload= function() { }   
	window.history.forward();
	function noBack() { window.history.forward(); }

	$(function() {
		
		$( "#SURVEY_EXPIRATION_DATE" ).datepicker();
		
		$('#chkVoice').click(function(){
			if($('#chkVoice').is(':checked')){
				$('#gender_container').show();
			}else{
				$('#gender_container').hide();
			}
		})
			
		$('#AddSurveyForm').validate({
			rules: {
					 inpSurveyDesc: "required"
				   }
		});
		
		//if($('#rdoNoExpire').is(':checked')){
			$("#rdoNoExpire").click();
		//}else{
		//	$("#rdoYesExpire").click();
		//}
		
		$("#AddNewSurveyListMainNavigator #NextToEditSurveyContent").click( function() { 
			var isValidgeneralInfo = ValidateSurveyTitle();
			var isValidCommunicationType = ValidateCommunicationType();
			
			//alert(IsValidExpirationDate());
			//return;
			var isValidExpirationDate = $('#rdoNoExpire').is(':checked') || IsValidExpirationDate();
			
			if(isValidgeneralInfo && isValidCommunicationType && isValidExpirationDate){
				    
			    <!--- var batchOpt = $('input[name=inpCreateOrUpdateBatch]:checked').val(); --->
			    var batchId = '';
			    var batchDesc = '';
			    
			    batchDesc = $("#inpSurveyDesc").val(); <!---RXencodeXML($("#inpSurveyDesc").val());--->
			    
			    expiration_Time = $('#rdoNoExpire').is(':checked') ? '' : $("#SURVEY_EXPIRATION_DATE").val();
				
			    var communicationType;
			    
				communicationType = $('input:radio[name=chkTypeRadio]:checked').val();
												
			    var voice = $('#inpSpeaker').val();
												
		  		SaveSurveyInfo(batchDesc, expiration_Time, batchId, batchDesc, 1, communicationType, voice);

				return false; 					
			}
			else
			{
				return false;  
			}
		
		}); 	
		
		<!--- Kill the new dialog --->
		$("#AddNewSurveyListMainNavigator #Cancel").click( function() 
		{
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/';
			return false;
		}); 	
		  
		$("#loadingDlgAddSurveyListMainNavigator").hide();	

		//set default date time base TimeZone USA GMT-7
		//$("#SURVEY_EXPIRATION_DATE").val(calcTime('-7'));
		
		//SelectDate();
		<!--- $('#SURVEY_END_NOW_TIME').timepicker({	
			showPeriod: false,   
			showPeriodLabels: false,
			amPmText: ['AM', 'PM'],
	 	    hours: {
        				starts: 0,               
						ends: 11                 
					},
			minutes: {
						starts: 0,                
						ends: 55,   
						interval: 5 
					},
			rows: 4
		});	 --->
		<!--- $("#SURVEY_END_NOWT").timepicker(); --->
		LoadBatchIDToForm();
	});
	
	<!---
	
	function calcTime(offset) {
	    // create Date object for current location
	    d = new Date();
	    d.setDate(d.getDate() + 30);
	    // get UTC time in msec
	    utc = d.getTime() + 2592000 + (d.getTimezoneOffset() * 60000); 
	    // create new Date object for different city
	    // using supplied offset
	    nd = new Date(utc + (3600000*offset));
	    // return time as a string
    	var curr_year = nd.getFullYear();
		var curr_date = nd.getDate();	
		var curr_month = nd.getMonth() + 1;

		return curr_year + "-" + curr_month + "-" + curr_date ;	
	}
	
	function SetSetyleForPicker() {
		$(".ui-datepicker-trigger").addClass("date_picker")
		$(".ui-datepicker-trigger").addClass("date_picker")
	}
	--->
	function ValidateCommunicationType() { 
		if ($('input:radio[name=chkTypeRadio]:checked').val() == ""){
	  		$('#lblSurveyDeliveryMethodError').show();
	  		return false;
	  	}
	  	else {
	  		$('#lblSurveyDeliveryMethodError').hide();
		  	return true;
		}
		
	}
	
	function ValidateSurveyTitle() { 
		if ($.trim($('#inpSurveyDesc').val()) == "") {
	  		$('#errSurveyTitle').show();
	  		return false;
	  	}
	  	else {
	  		$('#errSurveyTitle').hide();
		  	return true;
		}
		
	}
	<!---
	function SelectDate() {
		var date = $('#SURVEY_EXPIRATION_DATE').val();
		
		var elements = date.split('-');
		
		$('#txtMonth').val(elements[1]);
		$('#txtDate').val(elements[2]);
		$('#txtYear').val(elements[0]);
	}
	--->
	//add a new Survey
	function SaveSurveyInfo(SurveyDesc, ExpireDate, batchId, BatchDesc, inpMethod, CommunicationType, voice) {
		
		var nextStep = 3;
		inpXML = "<HEADER/>";
		inpXML += "<BODY/>";
		inpXML += "<FOOTER/>";
		
		
		if (CommunicationType == '<cfoutput>#SURVEY_COMMUNICATION_SMS#</cfoutput>') {
			inpXML += "<RXSSCSC DESC='" + SurveyDesc + "' GN='1' X='200' Y='200'>";		
			inpXML += "</RXSSCSC>";
			
			nextStep = 3;
		}
		
		if (CommunicationType == '<cfoutput>#SURVEY_COMMUNICATION_PHONE#</cfoutput>')
		{
			<!--- CommunicationType == '<cfoutput>#SURVEY_COMMUNICATION_ONLINE#</cfoutput>' ? '<RXSS></RXSS>' : --->
			inpXML +=  genDefaultVoiceQuestion(voice);
			nextStep = 3;		
		}
		
		if (CommunicationType == '<cfoutput>#SURVEY_COMMUNICATION_ONLINE#</cfoutput>') {
			inpXML += "<RXSSEM DESC='" + SurveyDesc + "' GN='1' X='200' Y='200'>";		
			inpXML += "</RXSSEM>";
			
			nextStep = 2;
		}
		
		<!--- Set survey configuration options--->
		inpXML += "<CONFIG WC='1' BA='1' THK='' CT='" + CommunicationType + "' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='" + voice + "'>0</CONFIG>";
		inpXML += "<EXP DATE='" + ExpireDate + "'>0</EXP>";
			
		
 		$.ajax({
			  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=AddNewSurvey&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
			  datatype: 'json',
			  data:  { INPBATCHID : batchId, INPBATCHDESC : BatchDesc, inpXML:inpXML},
			  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown); alert('qq');--->},
			  success:
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d2, textStatus, xhr ) 
					{
						<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
							var d = eval('(' + xhr.responseText + ')');
						
							<!--- Alert if failure --->
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{									
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{   
											batchId= d.DATA.INPBATCHID[0];
											//LoadFormEditSurvey(BatchID,SurveyEmailDesc);
											//AddnewSurveyStep2(batchId,SurveyDesc,inpMethod);
											var ParamStr = '?inpbatchid=' + encodeURIComponent(batchId) + '&STEP=' + nextStep + '&IsCreated=1' + '&INPCOMTYPE=' + CommunicationType
											window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editSurvey/' + ParamStr;
									}else{
										alert(d.DATA.MESSAGE[0]);
										return false
									}	
								}
								else
								{
								 	 $.alerts.okButton = '&nbsp;OK&nbsp;';
									 // jAlert("Failure.", "Can't Save Survey to DB"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);
									 alert(d.DATA.MESSAGE[0]);
								}
							}
							else
							{
							   alert("Unable save data to DB.Please check connection.");
							}

					}
		});

		return false;
	}
	
	function genDefaultVoiceQuestion(voice) {
		var defaultVoiceQuestion = "<RXSS><ELE BS='1' CK1='0' CK5='-1' DESC='' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RXT='1' X='150' Y='250'>";
		defaultVoiceQuestion += "<ELE ID='TTS' RXBR='16' RXVID='" + voice + "'></ELE>";
        defaultVoiceQuestion += "</ELE></RXSS>";
        return defaultVoiceQuestion;
	}
	<!--- add news survey step 2 --->
	var AddnewSurveyStep2Dialog = 0;
	function AddnewSurveyStep2(batchId,surveyDesc,inpMethod) {
		
		var ParamStr = '?inpbatchid='+batchId;
		if (surveyDesc) {
			ParamStr += '&INPSURVEYDESC='+RXencodeSpace(surveyDesc);
		}
		ParamStr += '&INPMETHOD='+inpMethod;
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
			
		window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/customizeTemplate' + ParamStr;				
		return false;

	}	
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	
	function LoadBatchIDToForm(){
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=getListBatchID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							<!--- Add MCIDs to stage --->
							CURRRXResultCode = d.DATA.RXRESULTCODE[0];
							if(CURRRXResultCode > 0)
							{
								var html='';
								data=d.DATA.LSTBATCH[0];
								for(i=0;i<data.length;i++){
									bid=data[i].ID;
									desc=data[i].DESC;
									html += '<option value="'+ bid +'">'+ desc + '</option>';
								}
								$("#listBatchId").append(html);
							}
						}
						
					}
			} );
	
	}
	//switch Batch Option when user choose New or Use Existing Batch
	function switchBatchOption(option){
			//$("#AddSurveyForm #CreateNewBatch").css('display','none');
			//$("#AddSurveyForm #UpdateBatch").css('display','none');
			//$("#AddSurveyForm #"+option).css('display','');

			//var selectitems = document.getElementById("listBatchId");
    		//var items = selectitems.getElementsByTagName("option");
    		//console.log(items.length);
			if ((option == 'UpdateBatch')&&($("#listBatchId")[0].length == 0)) {
				//$.jGrowl("Batch does not exist.", { life:1000, position:"center", header:' Message' });
				$("#AddSurveyForm #"+option).hide();
				$("#AddSurveyForm #inpUpdateBatch").attr("selected","");
				$("#AddSurveyForm #inpCreated").attr("checked","checked");
				$("#AddSurveyForm #CreateNewBatch").focus();
			} else {
				$("#AddSurveyForm #CreateNewBatch").hide();
				$("#AddSurveyForm #UpdateBatch").hide();
				$("#AddSurveyForm #"+option).show();
			}

	}
	
	function cancelCreate(){
		if (document.referrer == "") {
		    window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/";
		} else {
		    history.back();
		}
	}

	function ChangeExpirationOption(obj) {
		var display = $(obj).val() == 'YES' ? '' : 'none';
		
		$("#Expiration_Date_Picker").css('display', display);
	}
	
	function IsValidExpirationDate(){
		$("#lblInvalidExpirationDateError").hide();
		$('#lblInvalidPastDateError').hide();
		
		if($('#SURVEY_EXPIRATION_DATE').val() == "") {	  
			$("#lblInvalidExpirationDateError").show();
	  		return false;
	  	}
	  	else {
	  		var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/; // format dd/mm/yyyy
	  		if($('#SURVEY_EXPIRATION_DATE').val().match(re)){
	  			//alert(1);
	  			var fullDate = new Date()
				//convert month to 2 digits
				var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
				  
				var currentDate = twoDigitMonth + '/' + fullDate.getDate() + "/" + fullDate.getFullYear();
				
				var now = Date.parse(currentDate, 'mm/dd/yyyy');
				
				var expiredDate = Date.parse($('#SURVEY_EXPIRATION_DATE').val(), 'mm/dd/yyyy');
		
				var diff = expiredDate - now;
	  			
	  			if(diff <= 0){
					$('#lblInvalidPastDateError').show();
	  				return false;
	  			}else{
	  				$("#lblInvalidExpirationDateError").attr("style", "display: none;");
	  				return true;
	  			}
	  			
	  		}else{
	  			$("#lblInvalidExpirationDateError").show();
	  			return false;
	  		}
	  	}
	}
</script>

<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/utility.css');
	</style>
</cfoutput>