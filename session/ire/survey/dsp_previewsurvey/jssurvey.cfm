<cfoutput>
	<script language="javascript" src="#rootUrl#/#sessionPath#/ire/survey/js/survey.js"></script>
</cfoutput>
<script TYPE="text/javascript">

	var AllQuestions;
	<!--- Load all xml Questions of previous Survey --->
	function getURLParameter(name) {
	    return decodeURI(
	        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
	    );
	}
	
	var groups;
	var isDisableBackButton = false;
	function LoadSurveyQuestions(BATCHID){
		$.ajax({
			type:"POST",
			async:false,
			url:"<cfoutput>/#sessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=ReadXMLEMAIL&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				<!--- UUID : UUID	//'039739d0-673f-11e1-b6f1-20cf30ae4933', --->
				BATCHID : BATCHID
			},
			dataType: "json", 
	       	success: function(d2, textStatus, xhr) {
	        	if (d2.DATA.RXRESULTCODE > 0) {
	       			var data = d2.DATA.ARRAYQUESTION[0];
	       			var BATCHID = d2.DATA.INPBATCHID[0];
	       			<!--- JSON.stringify not supported IE7,IE6
	       			http://msdn.microsoft.com/en-us/library/cc836459%28VS.85%29.aspx
	       			 --->
	       			AllQuestions = JSON.stringify(data);
					isDisableBackButton = d2.DATA.ISDISABLEBACKBUTTON == '0';
	       			groups = GroupByPage(data);
					var QNo = 1;
	           	 	for (var i = 0; i < groups.length; i++) { 
	           	 		var questions = groups[i].questions;
	           	 		questions.sort(function(a,b) { return a.index - b.index });
				        groups[i].questionList = AddNewQuestion(BATCHID, groups[i], QNo);
				        
				        QNo += groups[i].questionList.length
						<!--- else {
						    for(j=0 ; j< LISTCASE[1].LISTANSWER.length; j++){
							    AnswerChoices+=LISTCASE[1].LISTANSWER[j].TEXT+'\n';
							    AnswerID+=LISTCASE[1].LISTANSWER[j].ID+'\n';
								    
					        };	
					        if(typeof(LISTCASE) != 'undefined'){
						    	for(j=0 ; j< LISTCASE[0].LISTANSWER.length; j++){
						    		
						    		if (LISTCASE[0].LISTANSWER[j].TEXT) {
						    			ColumnChoices += LISTCASE[0].LISTANSWER[j].TEXT+'\n';
						    		}
								     
					            }
					    	}
					    	AddNewQuestion(BATCHID,QuestionDesc,QuestionType,AnswerChoices,AnswerID,ColumnChoices,QuestionID)
						} --->
					    <!--- AddNewQuestion(QuestionDesc,QuestionType,QuestionChoices,ColumnChoices,QuestionID) --->
					}
					
	          	} 
	          	else {
	           		$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", d2.DATA.MESSAGE[0]);
					$("#TableSurveyItems").append(d2.DATA.MESSAGE[0]);
	           		return false;
	          	}
	
			}
		});
		return false;
	}
		

	function GroupByPage(questions) {
		var groups = new Array();
		for (var i = 0; i < questions.length; ++i) {
			questions[i].index = i;
			var group = GetGroupById(groups, questions[i].GROUPID);
			
			if (group == null) {
				group = new Object();
				group.GROUPID = questions[i].GROUPID;
				group.questions = new Array();
				group.questions.push(questions[i]);

				groups.push(group);
			}
			else {
				group.questions.push(questions[i]);
			}
		}
		
		groups.sort(function(a,b) { return a.GROUPID - b.GROUPID });

		return groups;
	}
		
	function GetGroupById(groups, groupId) {
		for (var i = 0; i < groups.length; ++i) {
			if (groups[i].GROUPID == groupId) {
				return groups[i];
			}
		}
		return null;
	}
	
	function Question(id, text, requiredans, errmesgtxt) {
		var result = new Object();
		result.ID = id;
		result.TEXT = text;
		result.REQUIREDANS = requiredans;
		result.ERRMSGTXT = errmesgtxt;
		
		return result;
	}
	function GroupByRateType(questions) {
		var result = new Array();
		for (var i = 0; i < questions.length; ++i) {
			questions[i].questionList = new Array();
			questions[i].questionList.push(Question(questions[i].ID, questions[i].TEXT, questions[i].REQUIREDANS, questions[i].ERRMSGTXT));
			
			result.push(questions[i]);
			var j = i + 1;
			var isSameRateType = true;
			while (j < questions.length && isSameRateType) {
				isSameRateType = IsSameRateType(questions[i], questions[j]);
				if (isSameRateType) {
					questions[i].TYPE = "MATRIXONE"
					questions[i].questionList.push(Question(questions[j].ID, questions[j].TEXT, questions[j].REQUIREDANS, questions[j].ERRMSGTXT));
					j++;
				}
			}
			i = j - 1;
		}
		
		return result;
	}
	
	function IsSameRateType(question1, question2) {
		if (question1.ANSWERS.length != question2.ANSWERS.length || question1.ANSWERS.length == 0) {
			return false;
		}
		
		if (IsSTRONGAGREEORDISAGREEQuestion(question1.ANSWERS) && IsSTRONGAGREEORDISAGREEQuestion(question2.ANSWERS)) {
			return true;
		}
		
		if ((IsONETOTENANSWERSQuestion(question1.ANSWERS, false, true) && IsONETOTENANSWERSQuestion(question2.ANSWERS, false, true)) || (IsONETOTENANSWERSQuestion(question1.ANSWERS, false, false) && IsONETOTENANSWERSQuestion(question2.ANSWERS, false, false)) || (IsONETOTENANSWERSQuestion(question1.ANSWERS, true, true) && IsONETOTENANSWERSQuestion(question2.ANSWERS, true, true)) || (IsONETOTENANSWERSQuestion(question1.ANSWERS, true, false) && IsONETOTENANSWERSQuestion(question2.ANSWERS, true, false))) {
			return true;
		}
		
		return false;
	}
	
	function GetAnswerList(question) {
		var result = new Array();
		for (var i = 0; i < question.ANSWERS.length; ++i) {
			result.push(question.ANSWERS[i].TEXT);
		}
		return result;
	}
	function AddNewQuestion(BATCHID, Group, index){
		<!--- QuestionID undefined when create a new question.It will be assigned by max QuestionID--->
		var isNew = false;//check question is new or not.
		var inpHTML = '';
		var questionList = GroupByRateType(Group.questions);
<!--- 		if (questionList[0].PROMPT) {
			 inpHTML += '<span id="prompt' + questionList[0].GROUPID + '" class="step" >';
			 inpHTML += '<div class="question_body">';
			 inpHTML += '<div class="question_header"><h3 class="question_header_text" id="qShortHeader">Prompt</h3></div>';
			 inpHTML += '<div class="question_header_text">';
			 inpHTML += ''+ questionList[0].PROMPT[0];
			 inpHTML += '<input type="hidden" name="PROMPT_'+questionList[0].GROUPID+'" id="" value="'+questionList[0].PROMPT[1]+'" />';
			 inpHTML += '</div></div>';

			 inpHTML += '</span>'; --->
		//generate HTML question and push it to form edit survey questions.
	    inpHTML += '<span id="' + Group.GROUPID + '" class="step" >';
	    var no = index;
	    var groupQuestionCount = 0;
	    for (var i = 0; i < questionList.length; ++i) {
	    	var question = questionList[i];
			var txthelp = '';
			if (question.TYPE == 'SHORTANSWER') {
				txthelp = 'Answer';
			}
			<!--- switch (question.TYPE) {
				case 'ONESELECTION':
				case 'ONETOTENANSWER':
				case 'STRONGAGREEORDISAGREE':
					txthelp = 'Choose one of the following answers';
					break;
				case 'SHORTANSWER':
					txthelp = 'Input content';
					break;
				default:
					txthelp = 'Choose one of the following answers';
					break;
			} --->
	
		     inpHTML += '<tr id="' + question.ID + '" class="trsort"><td>';
			 inpHTML += '<div class="ed_QstHdr question_body"  id="Q_' + question.ID + '" type="' + question.TYPE + '">';
			 
			 var required = "";
		   		
	   		 if(question.REQUIREDANS == 1){
	   			required = "<font style=''> *</font>";
	   		 }
			 
			 if (question.TYPE != 'MATRIXONE') {
				 inpHTML += '<div class="question_header"><h3 id="qShortHeader" class="question_header_text question_header_text">' 
				 + no + '.' + required + ' ' + question.TEXT  + '</h3></div>';
			 }
			 else {
				 inpHTML += '<div class="question_header"><h3 id="qShortHeader" class="question_header_text"></h3></div>';
			 }
			 if (question.TYPE == 'SHORTANSWER') {
				 inpHTML += '';
			 }
			 inpHTML += '<div class="qBody question_header_text">';
			 inpHTML += generateHtmlBaseQuestionType(question,  i + index + groupQuestionCount);
			 if (question.TYPE == 'MATRIXONE') {
				 groupQuestionCount += question.questionList.length - 1;
			 }
			 inpHTML += '</div>';
			 inpHTML += '<div class="clear"></div>';
			 inpHTML += '</div>';
			 inpHTML += '</td></tr>';	 
			 
			 inpHTML += '<div class="question_footer"/>';
			 
			 no += 1;
			if (question.questionList != null & question.questionList.length > 0) {
				no += question.questionList.length - 1;
			}
	 		}
		
		inpHTML += '</span>';

		$("#TableSurveyItems").append(inpHTML);
		//$("#SurveyForm").formwizard("update_steps");
		$("#TableSurveyItems").attr("BATCHID", BATCHID);
		
		return Group.questions;
	}
	
	function isValidRadioInput(q_id, required){
		var isChecked = false;
		$('input:radio[name=inpRadio_' + q_id + ']:checked').each(function() {
			isChecked = true;
		    $("#err_" + q_id).hide();
		});
		if(!isChecked && required == 1){
		    	$("#err_" + q_id).show();
		    	isAnswerAllRequired = false;
		}
	}
	
	function isValidCommentAns(q_id){
		if($("#inpComment_" + q_id).val().trim() == ''){
			$("#err_" + q_id).show();
		}else{
			$("#err_" + q_id).hide();
		}
	}
	
	function generateHtmlBaseQuestionType(question, index) {
		var html = '';
		var QuestionType = question.TYPE;

		switch (QuestionType) {
			case 'ONESELECTION':
			case 'ONETOTENANSWER':
			case 'STRONGAGREEORDISAGREE':
			case 'NETPROMTERSCORE':
				var required = "";
		   		
		   		if(question.REQUIREDANS == 1){
		   			required = "required";
		   			html+= '<br /><label class="error" style="display: none" id="err_' + question.ID + '"><strong>' 
		   				+ question.ERRMSGTXT + '</strong></label>';
		   		}
				for (var i = 0; i <question.ANSWERS.length; ++i) {
					answer = question.ANSWERS[i];
					if (answer != '') {
			        	html += '<div class="qOption">';
			           	if (i == 0) {
							html += '<input type="radio" name="inpRadio_' + question.ID + '" id="inpRadio_' + question.ID + '_' + i 
									+ '" class="rb MarRight5 required" value="' + answer.ID 
										+ '">';
		           	    }
		           	    else {
			           		html += '<input type="radio"  name="inpRadio_' + question.ID + '" id="inpRadio_' + question.ID + '_' + i 
			           				+ '" class="rb MarRight5 required" value="' + answer.ID 
			           					+ '">';
		           	    }
						html += '<label class="rb_off answer_details_text" for="input_' + i + '">' + answer.TEXT + '</label>';
						html += '</div>';
		           	   }
				   };
		      	   break;
		   <!--- case 'MULTISELECTION':
		           ansOpt=AnswerChoices.split('\n');
		           idsOpt=AnswerID.split('\n');
		           for(i=0 ; i< ansOpt.length; i++){
		           	   if(ansOpt[i]!=''){
		           	   	
			           	   html+='<div class="qOption" >';
				   		   html+='<input type="checkbox"  name="inpCheckBox'+QuestionID+'" id="input_'+i+'" class="rb required" value="'+idsOpt[i]+'">';
						   html+='<label class="rb_off" >'+ansOpt[i]+'</label>';
						   html+='</div>';
						   
		           	   }
				   };	
		      	   break; --->
		   case 'SHORTANSWER':
		   		var required = "";
		   		
		   		if(question.REQUIREDANS == 1){
		   			required = "required";
		   			html+= '<label class="error" style="display: none" id="err_' + question.ID + '"><strong>' 
		   				+ question.ERRMSGTXT + '</strong></label>';
		   		}
	   			html+='<textarea id="inpComment_' + question.ID + '"   class="qOption required"  cols="60" rows="2"></textarea>';	
	   			
	   			break;
		   case 'MATRIXONE':
				html = html + '<table cellspacing="0" cellpadding="0" border="0" style="width:100%;">';		
				html = html + '<thead><tr class="matrix_question_header"><th style="width:20%; background-color:white" class="matrix_question"></th>';
				
				var width = 80/question.ANSWERS.length;
				for (var i = 0; i < question.ANSWERS.length; i++) {
					question.ANSWERS[i].TEXT = GetAnswerLabel(question.ANSWERS[i].TEXT);

					html = html + '<th style="width:' + width + '%;" scope="col" class="matrix_answer matrix_question matrix_question_top">' + question.ANSWERS[i].TEXT + '</th>';
		        }
				html = html + '</tr></thead>';
				html = html + '<tbody>';
				var errMsg = "";
		        for (var i = 0; i < question.questionList.length; i++) {
		        	var tr = i%2 == 0 ? '<tr class="matrixAltRow">' : '<tr class="matrixAltRow" style="background-color: #EFF3F7;">';
		        	html = html + tr;
		        	
		        	var star = "";
		   			
			   		if(question.questionList[i].REQUIREDANS == 1){
			   			star = "<font style=''> *</font>";
			   			errMsg += '<label class="error" style="display: none" id="err_' 
			   					+ question.questionList[i].ID + '"><strong>' 
		   						+ question.questionList[i].TEXT +  ': ' + question.questionList[i].ERRMSGTXT + '</strong></label>';
			   		}
		        	
				    html = html + '<th align="left" scope="row" value="' + question.questionList[i].ID 
				    	+ '" class="matrix_answer matrix_question matrix_question_left" style="padding: 5px;">'
				    	+ (index + i) + '. '
				    	+ question.questionList[i].TEXT + star + '</th>';
					for (var j = 0; j < question.ANSWERS.length; j++) {
						html = html + '<td align="center" class="matrix_question"><div class="qOption">';
						html = html + '<input type="radio" value="' + question.ANSWERS[j].ID + '" id="input_' 
									+ question.ANSWERS[j].ID + '_' + question.questionList[i].ID 
									+ '" name="inpRadio_' + question.questionList[i].ID 
									+ '" class="rb MarRight5 required " >';
						html = html + '<label class="rb_off" id="linput_324115045_60_4141780064_4141780060" for="input_324115045_60_4141780064_4141780060">';
						html = html + '</label></div></td>';
					}
					html = html + '</tr>';
			    }
			    html = html + '</tbody></table>';
			    if(errMsg != ""){
			    	html = errMsg + html;
			    }
			    break;
		    default :
		    	break;
		};
	    html += '<input id="questionType" type="hidden" name="questionType" value="' + QuestionType + '">';
		return html;	   
	}
	
	var STRONGDISAGREE = "Strong Disagree";
	var DISAGREE = "Disagree";
	var NEITHERDISAGREEORAGREE = "Neither Disagree or Agree";
	var AGREE = "Agree";
	var STRONGAGREE = "Strong Agree";
	var EXTREMELYLIKELY = "Extremely likely";
	var NOTATALLLIKELY = "Not at all likely";
	var EXTREMELYSATISFIED = "Extremely satisfied";
	var NOTATALLSATISFIED = "Not at all satisfied";
	
	function GetSurveyQuestionCount() {
		var result = 0;
		for (var i = 0; i < groups.length; ++i) {
			result += groups[i].questions.length;
		}
		return result;
	}
	
	function GetQuestionsByPageIndex(pageIndex) {
		var result = 0;
		for (var i = 0; i <= pageIndex; ++i) {
			result += groups[i].questions.length;
		}
		return result;
	}
	
	function promptMessage(index, type) {
		if (groups == undefined || index >= groups.length) {
			return;
		}
		
		if (groups.length - 1 == index) {
			$('.ui-progressbar-value').addClass('completion_survey');
			$('#spnProgressBarValue').css('color', "white");
		}
		else {
			$('.ui-progressbar-value').removeClass('completion_survey');
			$('#spnProgressBarValue').css('color', "#C6C6C6");
		}
		var group = groups[index];
		if (!isDisableBackButton) {
			if (group.questionList[0].PROMPT != undefined && group.questionList[0].PROMPT != null) {
				if (group.questionList[0].PROMPT[1] == 1) {
					$('.jw-button-previous').hide();
				}
				<!--- else {
					$('.jw-button-previous').show();
				} --->
			}
			<!--- else {
				$('.jw-button-previous').show();
			} --->
		}
		else {
			$('.jw-button-previous').hide();
		}
	}			
</script>
