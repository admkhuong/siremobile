<cfparam name="BATCHID" default="-1">
<cfparam name="UUID" default="-1">
<cfparam name="PREVIEW" default="0">
<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
	<script src="#rootUrl#/#PublicPath#/js/rxxml.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
	<style type="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey/finish.css');
	</style>
</cfoutput>

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#View_Survey_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<!--- check config enable file start ---->
<cfinvoke 
     component="#Session.SessionCFCPath#.ire.marketingSurveyTools"
     method="getConfigPage"
     returnvariable="configPage">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
</cfinvoke>

<cfif configPage.TYPE GT 0>
	<cfif (NOT StructKeyExists(configPage, "WELCOMEPAGE")) OR configPage.WELCOMEPAGE EQ ''>
	<script>
	$(function () {
	});
	</script>
	</cfif>
</cfif>
<script>
var UUID = '<cfoutput>#UUID#</cfoutput>';
var BATCHID = <cfoutput>#BATCHID#</cfoutput>;
$(function () {
	$("#startsurvey").click(function() {
		$.ajax({
			type: "POST",
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=SaveAnswerSurvey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				BATCHID : BATCHID,
				UUID: UUID,
				inpXML: 'NA'			
				},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) {
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined") {
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0) {
									window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/dsp_previewsurvey/?BATCHID=<cfoutput>#BATCHID#</cfoutput>&UUID=<cfoutput>#UUID#</cfoutput>"
								}
								else {
									jAlert("Survey has NOT been save.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else {
							<!--- No result returned --->		
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}			
				} 		
		});
		
	});
	
});
</script>
<!--- CSS Section--->


<cfsavecontent variable="surveyContent">
	<div class="surveyTitle">
		<div class="surveyBorder">Welcome to
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				<cfoutput>#configPage.SURVEYTITLE#</cfoutput>
			</cfif>
		</div>
		<div class="surveyContent">
		<cfif StructKeyExists(configPage, "WELCOMEPAGE") AND configPage.WELCOMEPAGE NEQ ""> 
			<cfoutput>#configPage.WELCOMEPAGE#</cfoutput>
		<cfelse>
			Welcome! Please press start survey to begin!
		</cfif>
		</div>
		<div class="surveyStartButon" align="center">
			<button id="startsurvey" >Start survey</button>
		</div>
		
	</div>
</cfsavecontent>
<cfsavecontent variable="surveyHeader">
	<cfoutput>
	<div class="logoContainer" >
	<cfif StructKeyExists(configPage, "LOGO") AND configPage.LOGO NEQ ''>
		<img src="#rootUrl#/#sessionPath#/ire/survey/act_previewImage?serverfileName=#configPage.LOGO#&userId=#configPage.USERID#" height='65'/>
	<cfelse>
		<img src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm.png" height="54px">
	</cfif>
	<cfif StructKeyExists(configPage, "SURVEYTITLE")>
		<span id='survey_title'>#configPage.SURVEYTITLE#</span>
	</cfif>
	</div>
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="surveyFooter">
	<div id="surveyFooter" class="surveyBorder">
	<cfif StructKeyExists(configPage, "FOOTERTEXT") AND configPage.FOOTERTEXT NEQ ''>
		<cfoutput>#configPage.FOOTERTEXT#</cfoutput>
	</cfif>
	</div>
</cfsavecontent>

<cfset displaySurvey = 'block'>

<cfoutput>
<html>
	<head>
		<!--- fixbug JSON.stringify in ie8 --->
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				#configPage.SURVEYTITLE#
			</cfif>
		</title>
	</head>
	<body>
		<div id="surveyContent">
		<!---
		<cfif Session.TemplateContent NEQ ''>
			<cfset surveyFullContent = Replace(Session.TemplateContent, "{%surveyContent%}", "#surveyContent#") />
		<cfelse>
			<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
		</cfif>
		--->
			<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
			<table width="70%" style="margin:auto;">
					<tr>
						<td>#surveyHeader#</td>
					</tr>
					<tr>
						<td>#surveyContent#</td>
					</tr>
					<tr>
						<td>#surveyFooter#</td>
					</tr>
				</table>
		</div>
	</body>
</html>
</cfoutput>