<cfparam name="INPBATCHID" default="0">
<cfparam name="inpCPGID" default="0">
<cfparam name="INPTYPE" default="">
<cfparam name="INPPAGE" default="1">

<!---
<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
<cfset query = surveyObj.ReadXMLQuestions1(INPBATCHID)>
--->

<!--- Read question data this RESPONSE keys off of --->        
<cfinvoke method="ReadXMLQuestionsOptimized" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="query">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<style>
	#branching .question_label {
	    padding-left: 20px;
	    width: 25%;		
	}
	
	.sbHolder
	{
		width:428px;	
		border-radius: 3px 3px 3px 3px;			
	}
	
	<!--- 30 less than sbHolder --->	
	.sbSelector
	{
		width:400px;		
	}
	
	.sbOptions
	{
		max-height:250px !important;		
	}
	
</style>
	
<cfoutput>

	<div class="EBMDialog">
        
        <div class="inner-txt-box">
        
            <div style="padding-bottom: 10px; padding-top: 10px;">
                    
                <!--- BrachOptionFalseNextQuestion --->        
                <div class="clear row_padding_top">
                    <div class="question_label">
                        <label class="qe-label-a">Insert data after Control Point selected below</label>
                    </div>
                    <div class="left">
                        <select id="ddlCPGIA" style="width: 350px; max-height:50px;" onchange="">	
                            <option value="0" selected>-- Place at top --</option>
                            <cfloop array="#query.ARRAYQUESTION#" index="question">
                                <cfif question.TYPE NEQ "GARBAGE">
                                    <!--- Get RQ for CP copy - different than ID --->	
                                    <option value="<cfoutput>#question.RQ#</cfoutput>" >
                                        <cfoutput>CP #question.RQ#.#question.TEXT#</cfoutput>
                                    </option>
                                </cfif>
                            </cfloop>
                        </select>
                    </div>
                </div>    
            
            </div>
            <div id="loadingDlgRenameCPG" style="display:inline;">
                <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
            <div class='button_area' style="padding-bottom: 10px;">
                <button 
                    id="btnRenameCPG" 
                    type="button" 
                    class="ui-corner-all survey_builder_button"
                >Save</button>
                <button 
                    id="Cancel" 
                    class="ui-corner-all survey_builder_button" 
                    type="button"
                >Cancel</button>
            </div>
		</div>
	</div>


</cfoutput>

<script TYPE="text/javascript">
	function SaveCPG(INPBATCHID) {
		
		$("#loadingDlgRenameCPG").show();		
			
		if($("#inpDesc").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Control Point Group description has not been updated.\n"  + "New description can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameCPG").hide();	
			return;	
		}
			
		var data =  { 
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPTYPE: '<cfoutput>#INPTYPE#</cfoutput>',
						inpCPGID: '<cfoutput>#inpCPGID#</cfoutput>', 
						inpInsertAfterQID: $("#ddlCPGIA").val()
					};		
					   
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CopyControlPointGroup', data, "Control Point Group data has not been copied!", function(d ) {
			<!--- Update Display --->
			$("#TableSurveyQuestions div[CPGID='<cfoutput>#inpCPGID#</cfoutput>']").find('.DescText').html($("#inpDesc").val());
			
			jAlert("Control Point Group data has been copied", "Success!", function(result) { 	
			
				var QLableNav = '';
				
				if(parseInt($("#ddlCPGIA").val() ) > 0)
					QLableNav = '&QNav=' + (parseInt($("#ddlCPGIA").val()) + 1 );
																	
				NewLocation = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editquestions/index?inpbatchid="
								+ '<cfoutput>#INPBATCHID#</cfoutput>' + "&PAGE=" + '<cfoutput>#INPPAGE#</cfoutput>' + QLableNav;	
																	
				window.location = NewLocation;																							
				closeDialog(); 
			});
		});		
	
	}
	
	$(function() {	
		$("#btnRenameCPG").click(function() { 
			SaveCPG(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#Cancel").click(function() {
			closeDialog(); 
			return false;
	  	}); 	
		
		<!--- Init after setting selected --->
		$("#ddlCPGIA").selectbox();
		
		
		$("#loadingDlgRenameCPG").hide();	
	});
		
		
	
</script>


<style>

#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}




</style> 

<cfoutput>
 
</cfoutput>
