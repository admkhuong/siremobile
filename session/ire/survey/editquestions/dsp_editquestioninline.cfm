<cfparam name="INPBATCHID" default="0">
<cfparam name="INPPOSITION" default="1">
<cfparam name="INPPAGE" default="1">
<cfparam name="INPQID" default="0">
<cfparam name="INPCPGID" default="0">
<cfparam name="INPAFTER" default="0">
<cfparam name="INPBEFORE" default="0">
<cfparam name="INPCOMTYPE" default="SMS">
<cfparam name="INPADDQuestionAtPostition" default="0">

<cfinclude template="/#sessionPath#/ire/constants/surveyConstants.cfm">


<cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
    SELECT
        CdfId_int,
        CdfName_vch as VariableName_vch,
        CdfDefaultValue_vch as DEFAULTVALUE
    FROM
        simplelists.customdefinedfields
    WHERE
        simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
    ORDER BY
        VariableName_vch DESC
</cfquery>
            
              
<!--- To get to edit mode supply a INPQID --->               
<cfif INPQID GT 0>

	<!--- Read question data for this BRANCH  --->        
    <cfinvoke method="GetQuestion" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarReadQuestionDataByIdEdit">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
        <cfinvokeargument name="INPQUESTIONID" value="#INPQID#">
        <cfinvokeargument name="INPTYPE" value="#INPCOMTYPE#">
    </cfinvoke>
    
    <!---<cfdump var="#RetVarReadQuestionDataByIdEdit#">---> 
   
   <cfif RetVarReadQuestionDataByIdEdit.RXRESULTCODE LT 1> 
   
   		<cfset RetVarReadQuestionDataByIdEdit = {}>    	    
		<cfset RetVarReadQuestionDataByIdEdit.QUESTION_TEXT = ""> 
        <cfset RetVarReadQuestionDataByIdEdit.QUESTION_TYPE = "SHORTANSWER">    
        <cfset RetVarReadQuestionDataByIdEdit.ARRQUESTION_ANS_ID = ArrayNew(1)>
        <cfset RetVarReadQuestionDataByIdEdit.AUDIOSCRIPT = ""> 
        <cfset RetVarReadQuestionDataByIdEdit.QUESTION_ANS_LIST = ArrayNew(1)> 
        <cfset RetVarReadQuestionDataByIdEdit.ISINVALID = ""> 
        <cfset RetVarReadQuestionDataByIdEdit.ISERROR = ""> 
        <cfset RetVarReadQuestionDataByIdEdit.NUMBEROFREPEATS = 0> 
        <cfset RetVarReadQuestionDataByIdEdit.NUMBEROFSECONDS = 0> 
        <cfset RetVarReadQuestionDataByIdEdit.AF = "NOFORMAT">   
        <cfset RetVarReadQuestionDataByIdEdit.OIG = 0>
        <cfset RetVarReadQuestionDataByIdEdit.API_TYPE = "GET">
        <cfset RetVarReadQuestionDataByIdEdit.API_DOM = "somwhere.com">
        <cfset RetVarReadQuestionDataByIdEdit.API_DIR = "someplace/something">
        <cfset RetVarReadQuestionDataByIdEdit.API_PORT = "80">
        <cfset RetVarReadQuestionDataByIdEdit.API_DATA = "">
        <cfset RetVarReadQuestionDataByIdEdit.API_RA = "">
        <cfset RetVarReadQuestionDataByIdEdit.API_ACT = "">
        <cfset RetVarReadQuestionDataByIdEdit.SCDF = "">	
        <cfset RetVarReadQuestionDataByIdEdit.ITYPE = "MINUTES">
        <cfset RetVarReadQuestionDataByIdEdit.IMRNR = 0>
        <cfset RetVarReadQuestionDataByIdEdit.INRMO = "END"> 
        <cfset RetVarReadQuestionDataByIdEdit.IVALUE = 0> 
		<cfset RetVarReadQuestionDataByIdEdit.IHOUR = 0> 
    	<cfset RetVarReadQuestionDataByIdEdit.IMIN = 0> 
    	<cfset RetVarReadQuestionDataByIdEdit.INOON = 0> 
        <cfset RetVarReadQuestionDataByIdEdit.IENQID = 0> 
   
   </cfif>

<cfelse>
	
    <cfset RetVarReadQuestionDataByIdEdit = {}>    	    
	<cfset RetVarReadQuestionDataByIdEdit.QUESTION_TEXT = "">
    <cfset RetVarReadQuestionDataByIdEdit.QUESTION_TYPE = "SHORTANSWER">
    <cfset RetVarReadQuestionDataByIdEdit.ARRQUESTION_ANS_ID = ArrayNew(1)>
    <cfset RetVarReadQuestionDataByIdEdit.AUDIOSCRIPT = ""> 
    <cfset RetVarReadQuestionDataByIdEdit.QUESTION_ANS_LIST = ArrayNew(1)> 
    <cfset RetVarReadQuestionDataByIdEdit.ISINVALID = ""> 
    <cfset RetVarReadQuestionDataByIdEdit.ISERROR = ""> 
    <cfset RetVarReadQuestionDataByIdEdit.NUMBEROFREPEATS = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.NUMBEROFSECONDS = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.AF = "NOFORMAT"> 
    <cfset RetVarReadQuestionDataByIdEdit.OIG = 0>
    <cfset RetVarReadQuestionDataByIdEdit.API_TYPE = "GET">
	<cfset RetVarReadQuestionDataByIdEdit.API_DOM = "somwhere.com">
    <cfset RetVarReadQuestionDataByIdEdit.API_DIR = "someplace/something">
    <cfset RetVarReadQuestionDataByIdEdit.API_PORT = "80">
    <cfset RetVarReadQuestionDataByIdEdit.API_DATA = "">
    <cfset RetVarReadQuestionDataByIdEdit.API_RA = "STORE">
    <cfset RetVarReadQuestionDataByIdEdit.API_ACT = "JSON">
    <cfset RetVarReadQuestionDataByIdEdit.SCDF = "">	
    <cfset RetVarReadQuestionDataByIdEdit.ITYPE = "MINUTES"> 
    <cfset RetVarReadQuestionDataByIdEdit.IMRNR = 0>
    <cfset RetVarReadQuestionDataByIdEdit.INRMO = "END"> 
    <cfset RetVarReadQuestionDataByIdEdit.IVALUE = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.IHOUR = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.IMIN = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.INOON = 0> 
    <cfset RetVarReadQuestionDataByIdEdit.IENQID = 0> 
    
</cfif>


<!--- <cfdump var="#RetVarReadQuestionDataByIdEdit#"> --->

<style>
		
	.sbHolder
	{
		width:453px;	
		border-radius: 3px 3px 3px 3px;		
	}
	
	<!--- 30 less than sbHolder --->	
	.sbSelector
	{
		width:425px;		
	}
	
	.padding_left5 .sbHolder
	{
		width:125px;		
	}
	
	
	.padding_left5 .sbSelector
	{
		width:110px;		
	}
	
	.sbOptions
	{
		max-height:250px !important;		
	}
	
	.EBMDialog .inputbox-container 
	{
 	   width: 450px;
	}
	
	.padding_left5 {
		padding-right: 5px;
		float: left;
	}

	form input {
    	text-indent: 10px;
	}
	
	.wrapper-dropdown-picker {
			
		background-color: #FFFFFF;
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px;		
		font-size: 12px;
		height: 20px;
		position: static;
		width: 455px;	
		line-height: 20px;
    	outline: medium none;
	    overflow: hidden;
   	 	position: absolute;
    	text-indent: 10px;	
			
	}
	.wrapper-picker-container{
		 margin-left: 0px;
	} 

</style>


<script type="text/javascript">
	
	<!--- array for storing temp Questions infor --->
	var questionInfors;
	var LocalPostion = <cfoutput>#INPPOSITION#</cfoutput>
	var editFunction = "";
	var $dialogGroupPicker;
	
	$(function() 
	{		
			
		$("#q_des_err").hide();
	
		<!---initiate new array for questions--->
		questionInfors = new Array();
				
	
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();			
		});	
		
				
		$("#ddlQuestionType").val("<cfoutput>#RetVarReadQuestionDataByIdEdit.QUESTION_TYPE#</cfoutput>");
	
		<!--- Set as class here for option set as ID in InitEditQuestionForm for only option --->
		$(".save_continue").click(function(){
						
			<!--- Add new question by defaul - will be over ridden in InitEditQuestionForm() if this is an edit --->	
					
			if(!isValidQuestion())
			{					
				return false;
			}
						
			var isQuestionSaved = SaveQuestionContent(LocalPostion, $(this).attr('rel1'));
			
			if (isQuestionSaved)
			{						
				//if($(this).attr('rel1'))					
				//;//	$QuestionLogicInlineDialog.dialog('close');
											
				if(isLastPage)
				{
					allowAddPage = 'true';
				}
				
				<!--- Re-in intitalize current dialog--->
				LocalPostion = LocalPostion + 1;
				questionInfors = new Array();
				InitSingleAnswerList();	
				
			}							
		});		
		
			
		<cfif INPQID GT 0>
			InitEditQuestionForm();			
		<cfelse>
			InitSingleAnswerList();	
			ShowQuestionDetails(0);
		</cfif>
		
		$("#API_TYPE").val('<cfoutput>#RetVarReadQuestionDataByIdEdit.API_TYPE#</cfoutput>');
		$("#API_RA").val('<cfoutput>#RetVarReadQuestionDataByIdEdit.API_RA#</cfoutput>');
		$("#API_ACT").val('<cfoutput>#RetVarReadQuestionDataByIdEdit.API_ACT#</cfoutput>');
		
		
		$("#SCDF").selectbox();	
		
		
		<!--- Init after setting selected --->
		$("#ddlQuestionType").selectbox();
		$("#ddlDisplayOption").selectbox();	
		$("#ddlIntervalType").selectbox();	
		$("#API_TYPE").selectbox();	
		$("#API_ACT").selectbox();	
		$("#API_RA").selectbox();	
		$("#inpIntervalValue").selectbox();	
		$("#ddlHour").selectbox();	
		$("#ddlMinute").selectbox();	
		$("#ddlNoon").selectbox();	
		$("#ddlIENQID").selectbox();
		$("#IntervalMRNR").selectbox();
		$("#IntervalNRMO").selectbox();
			
		<!--- Load questions with Ajax so page runs quicker --->
		LoadQuestions();
				
		$('#EditQuestionLogicInlineContainer #inpSubmit').click(function(){
			
			
		});			 	
				
		$("#EditQuestionLogicInlineContainer #CancelFormButton").click(function() { $QuestionLogicInlineDialog.dialog('close') });
		$("#EditQuestionLogicInlineContainer #closeDialog").click(function() { $QuestionLogicInlineDialog.dialog('close') });
		
	
		
		$("#IntervalOptionsLink").click(function(){
			$("#INTERVALSELECTSUB").toggle();
		});		
		
		
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 900,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true,
					close: function( event, ui ) {
				      	//now before close dialog, we should remove all styles which have been loaded into this page from external css file 
				      	removejscssfile("customize.css","css");
						$("#SelectContactPopUp").html('');
						if($dialogGroupPicker != null)
						{							
							$dialogGroupPicker.remove();
							$dialogGroupPicker = null;
						}
				     }
				}).load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/dsp_select_contact');
			}
			
		<!---	console.log(this);
			console.log($(this).offset());
			console.log($(this).position().left);
			console.log($(this).position().top);
			console.log($(this).position().top + 30 +$(this).offset().top);
			console.log($(this).position().top + 30 - $(document).scrollTop());			
			console.log($dialogGroupPicker.dialog("widget").position().left);			
			console.log($dialogGroupPicker.position().top);
			--->
				
			<!--- Tie picker dialog to the biottom of the object that opend it. - $(document).scrollTop()--->
			var x = $(this).offset().left; 
    		var y = $(this).offset().top - $(document).scrollTop() + 20;
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		});
		
		
										
	});
	
	function SelectGroup(groupId,groupName)
	{
		$("#inpOIGValue").val(groupId);
		$("#inpGroupPickerDesc").html(groupName);
		$dialogGroupPicker.dialog('close');
		$("#GroupPickerList").html("");
		$("#GroupPickerList").remove();
		
		if(parseInt(groupId) > 0)
			$("#inpOIGValueHeader").html("Group Id: " + groupId + " - " + groupName);
				
	}
		
		
	<!---//we use this function to remove js or css file which loaded dynamically from ajax, popup, etc--->
   	function removejscssfile(filename, filetype){
   		var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
   		var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
   		var allsuspects=document.getElementsByTagName(targetelement)
   		for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
			if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
	     		allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
   		}
  	}
	
	function isValidQuestion()
	{
		var isValid = true;
		
		if($("#ddlQuestionType").val() != CDF && $("#ddlQuestionType").val() != OPTIN && $("#ddlQuestionType").val() != TRAILER){
			
			if(!isValidQuestionTitle()){
				isValid = false;
			}
		}
		
		if($("#ddlQuestionType").val() == ONESELECTION){
			var i = 0;
			for(i = 1; i <= answerCount; i++){
				if($.trim($("#txtAnswers_" + i).val()) == ''){
					isValid = false;
					$("#ans_err_" + i).show();
				}else{
					$("#ans_err_" + i).hide();
				}
			}
		}
		return isValid;
	}
		
	// validate CP title
	function isValidQuestionTitle()
	{
		if($.trim($("#QuestionDesc").val()) == ''){
			$("#q_des_err").show();
						
			//$("#QuestionDesc").show();
			return false;
		}else{
			//$("#QuestionDesc").show();
			$("#q_des_err").hide();
			return true;
		}
	}
		
			
	function AddCondition(inpEditMode) 
	{		
			
		
	}
		
	var answerCount = 2;
	function InitSingleAnswerList()	{
				
		answerCount = 2;
		
		$('#divAnswerList').empty();
		for(var i = 1; i <= 2; ++i) {
			var answer = CreateAnswerObject(i, '', '');
			try
			{
				$("#tmplAnswerItem").tmpl(answer).appendTo('#divAnswerList');		
			}
			catch(error)			
			{
				
			}
		}
	}
	
	function AddMoreAnswer() {
		if (isValidQuestion()) {
			answerCount++;
			var answer = CreateAnswerObject(answerCount, '', '');
			$("#tmplAnswerItem").tmpl(answer).appendTo('#divAnswerList');
			$("#txtAnswers_#"  + answerCount).focus();
			$("#ONESELECTIONOLD").scrollTop($("#ONESELECTIONOLD")[0].scrollHeight);
			
			if ($('.answerValue').length >= 10) {
				$('#divBetweenKeyPressTime').show();
			}
		}
	}
	
	function CreateAnswerObject(i, desc, audioPath) {
		var answer = new Object();
		answer.i = i;
		answer.answerDesc = desc;
		answer.audioPath = audioPath;
		if (audioPath != undefined & audioPath != "") {
			answer.haveAudio = true;
		}
		else {
			answer.haveAudio = false;
		}
		return answer;
	}
	
	function RemoveAnswer(i) {
		$('#divAnswerItem_' + i).remove();
		
		if ($('.answerValue').length < 10) {
			$('#divBetweenKeyPressTime').hide();
		}
	}
	
	<!--- Convert Voice questions to EBM Standard question types --->
	function ConvertVoiceQuestionTypeToStandard(questionType) 
	{
		switch(questionType) 
		{
			case "rxt2": 
			case "rxt6": 
			{
				return ONESELECTION;
			}
			case "rxt3": 
			{
				return SHORTANSWER;
			}
			default: 
			{
				return '';
			}
		}
	}		
		
	function InitEditQuestionForm() 
	{				
		<cfoutput>
			var BatchID = '#INPBATCHID#';
			var QuestionID = '#INPQID#';
			var QuestionText = '#RetVarReadQuestionDataByIdEdit.QUESTION_TEXT#';
			var QuestionType = '#RetVarReadQuestionDataByIdEdit.QUESTION_TYPE#';
			
			<cfif ArrayLen(RetVarReadQuestionDataByIdEdit.ARRQUESTION_ANS_ID) GT 0>
				var ArrAnswerID = #RetVarReadQuestionDataByIdEdit.ARRQUESTION_ANS_ID[0]#;
			<cfelse>
				var ArrAnswerID = {};	
			</cfif>
			
			<cfif Len(RetVarReadQuestionDataByIdEdit.AUDIOSCRIPT) GT 0>
				var scriptId = '#RetVarReadQuestionDataByIdEdit.AUDIOSCRIPT#';
			<cfelse>
				var scriptId = 0;	
			</cfif>
			
			<cfif ArrayLen(RetVarReadQuestionDataByIdEdit.QUESTION_ANS_LIST) GT 0>
				#toScript(RetVarReadQuestionDataByIdEdit.QUESTION_ANS_LIST, 'Answers')#;	
			<cfelse>
				var Answers = {};							
			</cfif>
						
			var isInvalid = '#RetVarReadQuestionDataByIdEdit.ISINVALID#';
			var isError = '#RetVarReadQuestionDataByIdEdit.ISERROR#';
			var numberOfRepeats = '#RetVarReadQuestionDataByIdEdit.NUMBEROFREPEATS#';
			var numberOfSeconds = '#RetVarReadQuestionDataByIdEdit.NUMBEROFSECONDS#';
			var AF = '#RetVarReadQuestionDataByIdEdit.AF#';
			var OIG = '#RetVarReadQuestionDataByIdEdit.OIG#';
			var ComType = '#INPCOMTYPE#';			
			var isRequiredAns = 0;
			var errMsgTxt = '';		
			var IntervalType = '#RetVarReadQuestionDataByIdEdit.ITYPE#';
			
			<!--- MRNR = Max Repeat No Response --->
			var IntervalMRNR = '#RetVarReadQuestionDataByIdEdit.IMRNR#';
			
			<!--- NRM = No Response Max Option --->
			var IntervalNRMO = '#RetVarReadQuestionDataByIdEdit.INRMO#';
			
			var IntervalValue = '#RetVarReadQuestionDataByIdEdit.IVALUE#';
			var IntervalHour = '#RetVarReadQuestionDataByIdEdit.IHOUR#';
			var IntervalMin = '#RetVarReadQuestionDataByIdEdit.IMIN#';
			var IntervalNoon = '#RetVarReadQuestionDataByIdEdit.INOON#';
			var IntervalExpiredNextQID = '#RetVarReadQuestionDataByIdEdit.IENQID#';
									
		</cfoutput>
		
		if (QuestionType.indexOf('rxt') != -1) 
		{
			QuestionType = ConvertVoiceQuestionTypeToStandard(QuestionType);
		}		
			
		<!--- This is goofy - what is going on here - May not exist? Handel this in the CFC instead?--->		
		try
		{
			isRequiredAns = RetVarReadQuestionDataByIdEdit.ISREQUIREANS;
			errMsgTxt = RetVarReadQuestionDataByIdEdit.ERRMSGTXT;
		}
		catch(error)
		{
			isRequiredAns = 0;
			errMsgTxt = '';
		}

		QuestionText = RXdecodeXML(QuestionText);
			
		<!---Change attr onclick of save & close button--->
		editFunction = function() {	
			<!--- Values from dialog are populate in the method generateXmlHtmlAferEdit(...) --->					
    		updateQuestion(BatchID, QuestionID, ArrAnswerID, ComType, currPage, LocalPostion, false); 
			return false;
		};

		$("#save_continue").unbind();

		$("#save_continue").click(function(){	
			editFunction();			 
		});				
				
		if(isRequiredAns == 1){
			$("#requiredAns").attr("checked", "checked");
			$("#requiredErrBox").show();
			$("#errMsgText").val(errMsgTxt);
		}
				
		InitSingleAnswerList();	

		$("#QuestionDesc").val(br2nl(QuestionText));
		
		$("#hidQuesScriptId").val(scriptId);
		
		if (QuestionType == "STATEMENT") {
			$("#ddlQuestionType").val(STATEMENT)
			QuestionType = STATEMENT;
			Answers = {};				
			
		}
		else if (QuestionType == "OTTPOST") {
			$("#ddlQuestionType").val(OTTPOST)
			QuestionType = OTTPOST;
			Answers = {};				
			
		}
		else if (QuestionType == "INTERVAL") {
			$("#ddlQuestionType").val(INTERVAL)
			QuestionType = INTERVAL;
			Answers = {};	
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);				
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
						
		}		
		else if (QuestionType == "API") {
			$("#ddlQuestionType").val(API)
			QuestionType = API;
			Answers = {};							
			
		}		
		else if (QuestionType == "OPTIN") {
			$("#ddlQuestionType").val(OPTIN)
			QuestionType = OPTIN;
			Answers = {};		
			$("#inpOIGValue").val(OIG);		
			
			if(parseInt(OIG) > 0)
				$("#inpOIGValueHeader").html("Group Id: " + OIG);
			
		}	
		else if (QuestionType == "CDF") {
			$("#ddlQuestionType").val(CDF)
			QuestionType = CDF;
			Answers = {};		
			$("#inpOIGValue").val(SCDF);
		}		
		else if (QuestionType == "TRAILER") {
			$("#ddlQuestionType").val(TRAILER)
			QuestionType = TRAILER;
			Answers = {};				
			
		}	
		else if (QuestionType == "SHORTANSWER") {
			$("#ddlQuestionType").val(SHORTANSWER)
			QuestionType = SHORTANSWER;
			Answers = {};	
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);	
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');			
			
		}			
		else if (IsONETOTENANSWERSQuestion(Answers, false, true)) {
			QuestionType = ONETOTENANSWER;
			$("#ddlRateType").val(RECOMMENDATION)
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);	
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		else if (IsONETOTENANSWERSQuestion(Answers, true, true)) {
			QuestionType = NETPROMTERSCORE;
			$("#ddlRateType").val(RECOMMENDATION)
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);	
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		else if (IsONETOTENANSWERSQuestion(Answers, false, false)){
			$("#ddlRateType").val(SATISFACTION)
			QuestionType = ONETOTENANSWER;
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);	
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		else if (IsONETOTENANSWERSQuestion(Answers, true, false)) {
			$("#ddlRateType").val(SATISFACTION)
			QuestionType = NETPROMTERSCORE;
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);	
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		else if (Answers.length == 0) {
			QuestionType = SHORTANSWER;
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);	
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		else if (IsSTRONGAGREEORDISAGREEQuestion(Answers)) {
			QuestionType = STRONGAGREEORDISAGREE;
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);	
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
						
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}			
		else 
		{
			QuestionType = ONESELECTION;
			
			var answerList = new Array();
						
			if(typeof(Answers.length) == "undefined")
			{
				
				
			}
			else				
			for (i = 0; i < Answers.length; i++) 
			{
				if (Answers[i] != '') 
				{
					answerList.push(CreateAnswerObject(i + 1, Answers[i].text, Answers[i].AUDIOPATH));
				}
			}
			
			if(typeof(Answers.length) != "undefined")
			{
				$('#divAnswerList').empty();
				$("#tmplAnswerItem").tmpl(answerList).appendTo('#divAnswerList');
			}
			
			$("#ddlIntervalType").val(IntervalType);
			$("#inpIntervalValue").val(IntervalValue);	
			$("#IntervalMRNR").val(IntervalMRNR);
			$("#IntervalNRMO").val(IntervalNRMO);
			$("#ddlHour").val(IntervalHour);	
			$("#ddlMinute").val(IntervalMin);	
			$("#ddlNoon").val(IntervalNoon);
									
			<!--- IENQID --->
			CurrIENQID = $("#ddlIENQID");
			CurrIENQID.selectbox('detach');
			CurrIENQID.val(IntervalExpiredNextQID);
			CurrIENQID.selectbox('attach');
		}
		
		if (Answers.length > 0) {
			$("#ddlNumberOfInvalidRepeats").val(numberOfRepeats)
		}			
				
		
		$("#ddlQuestionType").val(QuestionType);
		ShowQuestionDetails(0);
		
			
		if(QuestionType != "STATEMENT" || QuestionType != "TRAILER" || QuestionType != "API" || QuestionType != "OPTIN" || QuestionType != "CDF" || QuestionType != "OTTPOST" )
		if(parseInt(IntervalValue) > 0 || parseInt(IntervalHour) > 0 || parseInt(IntervalMin) > 0 || parseInt(IntervalExpiredNextQID) > 0 )
		{						
			$('#INTERVALSELECT').show();
			$('#INTERVALSELECTSUB').show();
			$('#IntervalOptions').show();
		}
			
		
		if (Answers.length >= 10) {
			$("#ddlNumberOfBetweenKeyPresses").val(numberOfSeconds)
			$('#divBetweenKeyPressTime').show();	
		}
		if(communicationType != SURVEY_COMMUNICATION_ONLINE){
			$('#chkInvalid').attr('checked', isInvalid); 
			$('#chkError').attr('checked', isError); 
		}
		
		<cfif LEN(RetVarReadQuestionDataByIdEdit.AF) GT 0>
			$("#ddlDisplayOption").val('<cfoutput>#RetVarReadQuestionDataByIdEdit.AF#</cfoutput>');
		<cfelse>
			$("#ddlDisplayOption").val('NUMERIC');
		</cfif>
		
		
	}
	
	function ShowIntervalDetails()
	{
		var IntervalType = $('#ddlIntervalType').val();
		
		switch (IntervalType) 
		{
			case "DATE":
			
				$("#IntervalValueSelect").html($("#tmplInterValueDate").tmpl(1));
				$('#inpIntervalValue').datepicker({
					numberOfMonths: 2,
					showButtonPanel: true,
					dateFormat: 'yy-mm-dd'
				});		
			
				$("#Intervaltime").show();
				
				break;
				
			case "HOURS":
			case "MINUTES":
			
				$("#Intervaltime").hide();					
			
				break;
			
			<!--- Set default to 10:00 AM when choosing these objects the first time --->	
			case "DAYS":
			case "WEEKS":
			case "MONTHS":
								
					CurrObj = $("#ddlHour");
					
					if(CurrObj.val() == '0')
					{
						CurrObj.selectbox('detach');
						CurrObj.val('10');
						CurrObj.selectbox('attach');
						
						CurrObj = $("#ddlMinute");
						CurrObj.selectbox('detach');
						CurrObj.val('00');
						CurrObj.selectbox('attach');
						
						CurrObj = $("#ddlNoon");
						CurrObj.selectbox('detach');
						CurrObj.val('00');
						CurrObj.selectbox('attach');
					}
			
				break;
				
			default:
		
				$("#IntervalValueSelect").html($("#tmplInterValueSelectBox").tmpl(1));
				$("#inpIntervalValue").selectbox();					
			
				$("#Intervaltime").show();
				
				break;										
		}
				
	}
		
	function ShowQuestionDetails(inpChange) 
	{
		
		<!--- Reset interval to off on question type change --->
		if(inpChange > 0)
		{	
						
			<!--- Buff --->
			CurrBuff = $("#ddlIntervalType");
			CurrBuff.selectbox('detach');
			CurrBuff.val('MINUTES');
			CurrBuff.selectbox('attach');
			
			<!--- Buff --->
			CurrBuff = $("#inpIntervalValue");
			CurrBuff.selectbox('detach');
			CurrBuff.val('0');
			CurrBuff.selectbox('attach');
			
			<!--- Buff --->
			CurrBuff = $("#ddlHour");
			CurrBuff.selectbox('detach');
			CurrBuff.val('0');
			CurrBuff.selectbox('attach');
			
			<!--- Buff --->
			CurrBuff = $("#ddlMinute");
			CurrBuff.selectbox('detach');
			CurrBuff.val('0');
			CurrBuff.selectbox('attach');
			
			<!--- Buff --->
			CurrBuff = $("#ddlNoon");
			CurrBuff.selectbox('detach');
			CurrBuff.val('0');
			CurrBuff.selectbox('attach');
						
			<!--- Buff --->
			CurrBuff = $("#ddlIENQID");
			CurrBuff.selectbox('detach');
			CurrBuff.val('0');
			CurrBuff.selectbox('attach');
			
			<!--- Buff --->
			$("#IntervalMRNR").val(2);
			$("#IntervalNRMO").val("END");
			
			
			
		}
		
		var questionType = $('#ddlQuestionType').val();
		
		switch (questionType) 
		{
			case ONESELECTION: 
			{
				$('#INTERVALSELECT').show();
				$('#INTERVALSELECTSUB').hide();
				$('#IntervalOptions').show();
				$('#ONESELECTIONCONTAINER').show();
				$('#ONETOTENANSWER').hide();
				$('#AnswerDisplayOption').show();
				$('#AnswerStoreCDFOption').hide();
				$('#ERRORHANDLER').show();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				$('#divBetweenKeyPressTime').hide();
				break;
			}
			case ONETOTENANSWER: 
			case NETPROMTERSCORE: 
			{ 
				$('#INTERVALSELECT').show();
				$('#INTERVALSELECTSUB').hide();
				$('#IntervalOptions').show();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').show();				
				$('#ERRORHANDLER').show();
				$('#AnswerDisplayOption').show();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				
				$('#divBetweenKeyPressTime').show();
				break;
			}
			case SHORTANSWER: 
			{
				$('#INTERVALSELECT').show();
				$('#INTERVALSELECTSUB').hide();
				$('#IntervalOptions').show();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				
				break;
			}
			case OTTPOST:
			case AGENT:
			case TRAILER:
			case STATEMENT:
			{
				$('#INTERVALSELECT').hide();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				break;
			}
			case API:
			{
				$('#INTERVALSELECT').hide();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").show();	
				break;
			}
			case OPTIN:
			{
				$('#INTERVALSELECT').hide();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").show();	
				$("#APISELECT").hide();	
				break;
			}
			case CDF:
			{
				$('#INTERVALSELECT').hide();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#AnswerStoreCDFOption').show();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				break;
			}
			case INTERVAL:
			{
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').hide();
				$('#AnswerDisplayOption').hide();
				$('#INTERVALSELECT').show();
				$('#INTERVALSELECTSUB').show();
				$('#IntervalOptions').show();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				
				break;
			}
			case STRONGAGREEORDISAGREE: 
			{
				$('#INTERVALSELECT').show();
				$('#INTERVALSELECTSUB').hide();
				$('#IntervalOptions').show();
				$('#ONESELECTIONCONTAINER').hide();
				$('#ONETOTENANSWER').hide();
				$('#ERRORHANDLER').show();
				$('#AnswerDisplayOption').show();
				$('#AnswerStoreCDFOption').hide();
				$("#OIGSELECT").hide();	
				$("#APISELECT").hide();	
				
				$('#divBetweenKeyPressTime').hide();
				break;
			}
		}
	}
	
	<!---Save question content--->
	function SaveQuestionContent(position, isReload){
								
		<!--- Validate form --->						
		if ($("#QuestionContainer").valid()) 
		{
			var isRequiredAnswer = $("#requiredAns").is(":checked");
		
			if(isRequiredAnswer){
				// Check errMsgTxt
				if($("#errMsgText").val() == ''){
					$("#errMsgText").focus();
					$("#errRequiredMsg").show();
					return false;
				}else{
					$("#errRequiredMsg").hide();
				}
			}			
			
			questionInfors.push(GetQuestionObject());
			
			if (communicationType != null) 
			{
				
				SaveQuestionsToXML(communicationType, position, isReload);
				
				if (communicationType != SURVEY_COMMUNICATION_PHONE)
				{
					//$("#pager2").show();
				}
				questionInfors = new Array();
			}
			//CloseDialog('createQ');
			
			if(isReload){				
				window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editquestions/index?inpbatchid=" + BATCHIDSurvey + "&PAGE=" + currPage;
			}
			
			<!---//reIndexQuestions();--->
			return true; 	
		}
		else
		{
			 return false;
		}
	}
			
	function SaveQuestionsToXML(communicationType, position, ReloadPage) 
	{	
	
		var isInvalidResponse = false;
		var isErrorResponse = false;
		
		if(communicationType == SURVEY_COMMUNICATION_ONLINE || communicationType == SURVEY_COMMUNICATION_SMS)
		{
		
		}
		else
		{
			isInvalidResponse = $('#chkInvalid').is(':checked');
			isErrorResponse = $('#chkError').is(':checked');
		}
		
		var GroupId = currPage;			
				
		$.each(questionInfors, function(dataItemindex, dataItem)
		{	
			if (communicationType == BOTH)
			{//generate both xml voice and email and SMS
				<cfoutput>			
					AddNewQuestionVoiceEmail('<cfoutput>#INPBEFORE#</cfoutput>', '<cfoutput>#INPAFTER#</cfoutput>', '<cfoutput>#INPCPGID#</cfoutput>', dataItem.Description, dataItem.QuestionType, dataItem.Answers, dataItem.AF, dataItem.scriptId, isInvalidResponse, isErrorResponse, #INPQID#, GroupId, position, dataItem.RequiresAns, dataItem.ErrMsgTxt, dataItem.IntervalType, dataItem.IntervalValue, dataItem.IntervalHour, dataItem.IntervalMin, dataItem.IntervalNoon, dataItem.IntervalExpiredNextQID, dataItem.IntervalMRNR, dataItem.IntervalNRMO, dataItem.OIG, ReloadPage);
				</cfoutput>
				pageQuestionNumber++;
			} 
			else
			{				
				<cfoutput>
					AddNewQuestion('<cfoutput>#INPBEFORE#</cfoutput>', '<cfoutput>#INPAFTER#</cfoutput>', '<cfoutput>#INPCPGID#</cfoutput>', dataItem.Description, dataItem.QuestionType, dataItem.Answers, dataItem.AF, dataItem.scriptId, communicationType, isInvalidResponse, isErrorResponse, #INPQID#, GroupId, position, dataItem.RequiresAns, dataItem.ErrMsgTxt, dataItem.IntervalType, dataItem.IntervalValue, dataItem.IntervalHour, dataItem.IntervalMin, dataItem.IntervalNoon, dataItem.IntervalExpiredNextQID, dataItem.IntervalMRNR, dataItem.IntervalNRMO, dataItem.OIG, ReloadPage, '', 0, new Array()); 
				</cfoutput>
				pageQuestionNumber++;
			}
		});	
	}
	
	<!--- Async load question for select box - makes UI respond faster --->
	function LoadQuestions() 
	{				
		$("#loadingDlgQEdit").show();		
			
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=ReadXMLQuestionsOptimized&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  	{ 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>'
	   			},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {$("#loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{						
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{
														
							$('#ddlIENQID').remove();
							$('#ddlIENQIDContainer').html('<select id="ddlIENQID" class="ddlIENQID" style="width: 350px; max-height:50px;"></select>');
							
							var CurrIENQID = $('#ddlIENQID');
							
							CurrIENQID.append('<option value="0" selected>-- Default to Next Question --</option>')						
							
							$("#loadingDlgQEdit").hide();								
							
							for (var i=0; i<d.DATA.ARRAYQUESTION[0].length; i++) 
							{
								CurrIENQID.append('<option value="' + d.DATA.ARRAYQUESTION[0][i].ID + '">CP' + d.DATA.ARRAYQUESTION[0][i].RQ + '. '+ d.DATA.ARRAYQUESTION[0][i].TEXT + '</option>');
							}
							
							var IntervalExpiredNextQID = '<cfoutput>#RetVarReadQuestionDataByIdEdit.IENQID#</cfoutput>';

							CurrIENQID.val(IntervalExpiredNextQID);
								
							CurrIENQID.selectbox(
							{
									onOpen: function (inst) 
									{																					
										var CurrObjId = 'sbOptions_' + $(inst).attr('uid');
										var CurrSelection = "a[rel='" + $('#ddlIENQID').val() + "']";
										
										if($("#" + CurrObjId).find(CurrSelection).length > 0)
											$("#" + CurrObjId).scrollTop( $("#" + CurrObjId).find(CurrSelection).position().top);
									}
							 });	
						}
						else
						{
							<!---$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Load questions failed  saved.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							--->		
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
				}
				else
				{<!--- No result returned --->						
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
									
				$("#loadingDlgQEdit").hide();								
			} 		
			
		});
		
		return false;
	}
	
	
</script>

<cfoutput>

	<div id="EditQuestionLogicInlineContainer" style="width:500px;">
        <div class="EBMDialog">
                              
            <form id="QuestionContainer">
			<div class="EBMDialog">
            
                <div class="inner-txt-box">
                                   
                    <div class="clear">
                        <div class="">
                            <label class="qe-label-a">Interaction Text:</label> 
                            <label class='ErrorColor' id='q_des_err'>*This field is required.</label>
                        </div>
                        <div class="left">
                            <input type="hidden" id="hidQuesScriptId">
                            <cfif INPCOMTYPE eq SURVEY_COMMUNICATION_PHONE or INPCOMTYPE eq SURVEY_COMMUNICATION_BOTH>
                                <div class="right">
                                    <a href ="##" class ="voiceRecord editQVoiceRecode" id="question_voicerecord" onclick="showRecorder('hidQuesScriptId')"> </a>
                                </div>
                            </cfif>
                        </div>
                        <div class="left question_control">
                            <textarea  
                                maxlength="1000"
                                id="QuestionDesc" name="QuestionDesc" role="textbox" aria-autocomplete="list" 
                                aria-haspopup="true"
                                rows="3" cols="40"	
                                class="textarea" style="width:435px;"	
                            >#RetVarReadQuestionDataByIdEdit.QUESTION_TEXT#</textarea>
                            
                        </div>
                    </div>
                    
                    <div class="clear row_padding_top">
                        <div class="">
                            <label class="qe-label-a">Interaction Type:</label>
                        </div>
                        <div class="left">
                            <select id="ddlQuestionType" onchange="ShowQuestionDetails(1)">	
                                <option value="SHORTANSWER" selected="selected">Short Answer</option>
                                <option value="ONESELECTION">Single	Selection Question</option>
                                <option value="ONETOTENANSWER">1-10 Answer</option>
                                <option value="STRONGAGREEORDISAGREE">Strongly Agree or Disagree</option>
                                <option value="NETPROMTERSCORE">Net Promoter Score</option>
                                <option value="TRAILER">Trailer</option>
                                
                                <cfif INPCOMTYPE eq SURVEY_COMMUNICATION_SMS>
                                
                                    <option value="STATEMENT">Statement</option>
                                    <option value="INTERVAL">Interval</option>
                                    <option value="API">API</option>
                                    <option value="OPTIN">OPTIN</option> 
                                    <option value="CDF">Custom Data Field Capture</option>     
                                    <option value="OTTPOST">Over The Top POST</option> 
                                    <option value="AGENT">Agent Assisted</option>                                   
                                
                                </cfif>
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="clear row_padding_top" id="AnswerDisplayOption">
                        <div class="">
                            <label class="qe-label-a">Answer Display Option:</label>
                        </div>
                        <div class="left">
                            <select id="ddlDisplayOption">	
                                <option value="NOFORMAT" selected="selected">No Formatting</option>
                                <option value="NUMERIC">1 for ..., 2 for ..., 3 for... </option>
                                <option value="NUMERICPAR">1) ..., 2) ..., 3) ... </option>
                                <option value="ALPHA">A for ..., B for ..., C for ...</option>
                                <option value="ALPHAPAR">A) ..., B) ..., C) ...</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="clear row_padding_top" id="AnswerStoreCDFOption">
                        <div class="">
                            <label class="qe-label-a">Store Answer in Custom Data Field:</label>
                        </div>
                        <div class="left">
                            <select id="SCDF">	                            
                            		<option value="" selected="selected">None Selected - Required</option> 
                            		<cfloop query="GetCustomFields" >
                                        <option value="#GetCustomFields.VariableName_vch#" <cfif RetVarReadQuestionDataByIdEdit.SCDF EQ GetCustomFields.VariableName_vch>SELECTED</cfif> >
                                            #GetCustomFields.VariableName_vch#
                                        </option>
                                    </cfloop>
                            </select>
                        </div>
                    </div>
                                           
                    <div id="questionBuilder" class="qSetting questionBuilder row_margin_top clear" style="min-height:15px;"></div>
                                                                             
                    <div id="ONESELECTIONCONTAINER" style="display:none">
                    
                        <div class="left InfoBarTitle" style="width: 500px;">
                            
                                <label class="qe-label-a" style="display:inline;">Answers:</label>
                                                           
                                <span class="submit">
                                    <a class="button filterButton small" id="AddMoreAnswer" onclick="AddMoreAnswer()" style="padding: 5px 20px;">Add more answers</a>
                                </span>
                        </div>
                        
                        <BR />
                            
                        <div id="ONESELECTIONOLD" class="one_selection">
                            <!---<div class="left InfoBarTitle">Answers</div>--->
                            <div id="divAnswerList"></div>
                           <!--- <div class="clear left answer" style="padding-top: 5px">
                                <a class="button filterButton small" id="AddMoreAnswer" onclick="AddMoreAnswer()" >Add more answers</a>
                            </div>--->
                        </div>
                    
                    </div>
                    
                    <div id="ONETOTENANSWER" style="display:none">
                        <div class="answer_padding">
                            <div class="left ">
                                <label class="qe-label-a" id="QuestionDescHeader">Rating Type:</label>
                            </div>
                            <div class="left">
                                <select id="ddlRateType">	
                                    <option value="RECOMMENDATION" selected="selected">Recommendation</option>
                                    <option value="SATISFACTION">Satisfaction</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div id="OIGSELECT" style="display:none">
                        <div class="clear row_padding_top">
                            <div class="">
                                <label class="qe-label-a" id="inpOIGValueHeader">Group Id:</label>
                            </div>
                                                      
                            <div class="left">
                            
                            <input type="hidden" name="inpOIGValue" id="inpOIGValue" value="0">
                            <div class="wrapper-picker-container">        	    
                                <div class="wrapper-dropdown-picker GroupPicker" tabindex="1"> 
                                    <span id="inpGroupPickerDesc">Click here to select a contact list</span> 
                                    <a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   
                                </div>                                            
                            </div>
                                           
                            </div>
                        </div>
                    </div>
                    
                    
                    <div id="APISELECT" style="display:none">
                    
                    
				 		<!--- 
							    // RXSSCK1[CurrMCID] = POST or GET - default is GET
								// RXSSCK2[CurrMCID] = Content type - default is text/xml
								// RXSSCK3[CurrMCID] = Port - default is 80
								// RXSSCK4[CurrMCID] = Response Map - Optional - partial string match so be careful - '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'  - escape  commas and parentesis with {"&lp;", "("} {"&rp;", ")"} {"&com;'", ","}
								// RXSSCK5[CurrMCID] = Next QID
								// RXSSCK6[CurrMCID] = Domain
								// RXSSCK7[CurrMCID] = Directory/File
								// RXSSCK8[CurrMCID] = Additional Content to send - XML, SOAP, ETC
								// Escape ("&lt;", "<") ("&gt;", ">") ("&apos;", "'") ("CHR(13)", CHAR(13)) Carrige Return) ("CHR(10)", CHAR(10)) Line Feed) ("&quot;", " double quote) ("&amp;", "&")                
								// Continue on in survey                        
                        --->
                    
                   		<div class="clear row_padding_top">
                            <div class="">
                                <label class="qe-label-a">Result Action</label>
                            </div>
                                                      
                            <div class="left">
                                <select id="API_RA">	
                                    <option value="SEND" selected="selected">Send result data to user</option>
                                    <option value="STORE">Just Store Results</option>                                 
                                </select>
                            </div>
                         
                        </div>
                                                
                        <div class="clear row_padding_top">
                            <div class="">
                                <label class="qe-label-a">REST API VERB</label>
                            </div>
                                                      
                            <div class="left">
                                <select id="API_TYPE">	
                                    <option value="GET" selected="selected">GET</option>
                                    <option value="POST">POST</option>
                                   <!--- <option value="PUT">PUT</option>
                                    <option value="DELETE">DELETE</option>--->
                                </select>
                            </div>
                         
                        </div>
                       
                        <div class="clear row_padding_top">   
                            
                            <div class="">
                                <label class="qe-label-a">Domain</label>
                            </div>
                                                      
                            <div class="left">
                                <input type="text" name="API_DOM" id="API_DOM" value="#RetVarReadQuestionDataByIdEdit.API_DOM#" />
                            </div>                            
                            
                        </div>
                        
                        <div class="clear row_padding_top">   
                            
                            <div class="">
                                <label class="qe-label-a">Directory/File</label>
                            </div>
                                                      
                            <div class="left">
                                <input type="text" name="API_DIR" id="API_DIR" value="#RetVarReadQuestionDataByIdEdit.API_DIR#" />
                            </div>                            
                            
                        </div>
                             
                        <div class="clear row_padding_top">   
                            
                            <div class="">
                                <label class="qe-label-a">PORT</label>
                            </div>
                                                      
                            <div class="left">
                                <input type="text" name="API_PORT" id="API_PORT" value="#RetVarReadQuestionDataByIdEdit.API_PORT#" />
                            </div>                            
                            
                        </div>
                        
                        
                        <div class="clear row_padding_top">
                            <div class="">
                                <label class="qe-label-a">Additional Content Type</label>
                            </div>
                                                      
                            <div class="left">
                                <select id="API_ACT">	
                                    <option value="TEXT" selected="selected">Plain Text</option>
                                    <option value="JSON">JSON</option>  
                                    <option value="XML">XML</option>                                 
                                </select>
                            </div>
                         
                        </div>
                        
                        <div class="clear row_padding_top">   
                            
                            <div class="">
                                <label class="qe-label-a">Additional Content To Send</label>
                            </div>
                                                      
                            <div class="left">
                                <textarea  
                                    maxlength="1000"
                                    id="API_DATA" name="API_DATA" role="textbox" aria-autocomplete="list" 
                                    aria-haspopup="true"
                                    rows="3" cols="40"	
                                    class="textarea" style="width:435px;"	
                                >#RetVarReadQuestionDataByIdEdit.API_DATA#</textarea>
                            </div>                            
                            
                        </div>           
                        
                                     
                        
                    </div>
                    
                    
                    
                    <div id="INTERVALSELECT" style="display:none">
                    	
                        <div class="clear" id="IntervalOptions" style="padding-top:20px; padding-bottom:0px;">
                                <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="IntervalOptionsLink">Interval Options</span></label>
                        </div>        
                        
                    
                    	<div id="INTERVALSELECTSUB" style="">
                    		                        
                            <div class="clear row_padding_top">
                                <div class="">
                                    <label class="qe-label-a">Interval Type:</label>
                                </div>
                                <div class="left">
                                    <select id="ddlIntervalType" onchange="ShowIntervalDetails()">	                                    
                                        <option value="SECONDS">Seconds(s)</option>
                                        <option value="MINUTES">Minute(s)</option>
                                        <option value="HOURS">Hour(s)</option>
                                        <option value="DAYS">Day(s)</option>
                                        <option value="WEEKS">Week(s)</option>
                                        <option value="MONTHS">Month(s)</option>   
                                        <option value="DATE">Specific Date</option> 
                                        <option value="SUN">Sunday</option> 
                                        <option value="MON">Monday</option> 
                                        <option value="TUE">Tuesday</option> 
                                        <option value="WED">Wednesday</option> 
                                        <option value="THU">Thursday</option> 
                                        <option value="FRI">Friday</option> 
                                        <option value="SAT">Saturday</option>                                                                                                            
                                    </select>
                                </div>
                            
                            </div>
                            
                            <div class="clear row_padding_top">
                            
                                <div class="">
                                    <label class="qe-label-a">Interval Value:</label>
                                </div>
                                <div class="left" id="IntervalValueSelect">
                                    <select id="inpIntervalValue">	                                    
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>
                                        <option value="36">36</option>
                                        <option value="37">37</option>
                                        <option value="38">38</option>
                                        <option value="39">39</option>
                                        <option value="40">40</option>
                                        <option value="41">41</option>
                                        <option value="42">42</option>
                                        <option value="43">43</option>
                                        <option value="44">44</option>
                                        <option value="45">45</option>
                                        <option value="46">46</option>
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                        <option value="49">49</option>
                                        <option value="50">50</option>
                                        <option value="51">51</option>
                                        <option value="52">52</option>
                                        <option value="53">53</option>
                                        <option value="54">54</option>
                                        <option value="55">55</option>
                                        <option value="56">56</option>
                                        <option value="57">57</option>
                                        <option value="58">58</option>
                                        <option value="59">59</option>
                                        <option value="60">60</option>                                                                                                       
                                    </select>
                                </div>
                                                      
                            </div>
                            
                             <div class="clear row_padding_top" id="Intervaltime">
                                <div class="">
                                    <label class="qe-label-a">Interval Time: Optional - Time zone Specific to Device Address</label>
                                </div>
                                <div class="left">
                                    <!---<select id="ddlIntervalTime">	 --->                                   
                                      
                                    <div class="padding_left5">  
                                        <select id="ddlHour" class="hour" customfield="start">
                                            <option value="0" SELECTED="SELECTED">Select Hour</option>
                                            <option value="00">12</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                   </div>
                                   
                                   <div class="padding_left5">
                                        <select id="ddlMinute" class="minute">
                                            <option value="0" SELECTED="SELECTED">Select Minute</option>
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                        </select>
                                    </div>
                                    
                                    <div class="padding_left5">    
                                        <select id="ddlNoon" class="noon_list" customfield="start">
                                            <option value="0" SELECTED="SELECTED">Select AM or PM</option>
                                            <option value="00">AM</option>
                                            <option value="01">PM</option>
                                        </select>
                                    </div>    
    
    
                                </div>
							
                            </div>
                                                   
                            <div class="clear row_padding_top" id="IntervalExpiredNextQID">
                            
                                <div class="">
                                    <label class="qe-label-a">Interval Expired: Optional - Next Question to Branch to</label>
                                </div>
                                
                                <div class="left" id="ddlIENQIDContainer">
                                
                                    <select id="ddlIENQID" class="ddlIENQID" style="width: 350px; max-height:50px;">	
                                        
                                        <option value="0" selected>-- Loading Data ... --</option>
                                                                        
                                    <!---  DONT NEED TO CALL THIS IF IT IS STILL AVAILABLE FROM THE TOP PAGE ?
									  <cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
                                      <cfset query = surveyObj.ReadXMLQuestions1(INPBATCHID)>
        							--->
                                  <!---  
                                        <cfloop array="#query.ARRAYQUESTION#" index="question">
                                            <cfif question.TYPE NEQ "GARBAGE">
                                                                                        
                                                <option value="<cfoutput>#question.ID#</cfoutput>">
                                                    <cfoutput>CP #question.RQ#. #question.TEXT#</cfoutput>
                                                </option>
                                                 
                                            </cfif>
                                        </cfloop>--->
                                        
                                    </select>
                                    
                                </div>
                                
                            </div>
                            
                            <div class="clear row_padding_top" id="IntervalMRNRNextQID">
                            
                                <div class="">
                                    <label class="qe-label-a">Max Repeat No Response - Only if Next Question to Branch to is the Same</label>
                                </div>
                                
                                <div class="left" id="IntervalMRNRContainer">
                                
                                     <select id="IntervalMRNR" class="IntervalMRNR" style="width: 350px; max-height:50px;">	                                        
                                        <option value="0" selected>0</option>
                                        <option value="1" selected>1</option>
                                        <option value="2" selected>2</option>
                                        <option value="3" selected>3</option>
                                     </select>
                                      
                                    
                                </div>
                                
                            </div>
                            
                            <div class="clear row_padding_top" id="IntervalNRMONextQID">
                            
                                <div class="">
                                    <label class="qe-label-a">No Response Max Option - Only if Next Question to Branch to is the Same</label>
                                </div>
                                
                                <div class="left" id="IntervalNRMOContainer">
                                    
                                    <select id="IntervalNRMO" class="IntervalNRMO" style="width: 350px; max-height:50px;">	                                        
                                        <option value="END" selected>End Interactive Campaign</option>
                                        <option value="NEXT" selected>Proceed to Next Question</option>
                                    </select>
                                     
                                </div>
                                
                            </div>
                        
                        </div>
                        
                    </div>
                    
                    <cfif INPCOMTYPE eq SURVEY_COMMUNICATION_PHONE or INPCOMTYPE eq SURVEY_COMMUNICATION_BOTH>
                        <cfoutput>
                            <div class="clear" id="ERRORHANDLER" style="display: none;">
                                <div class="clear advanced_option_label advanced_option_padding" onclick="VisibleAdvancedOptions(this)">
                                    Show Advanced Options
                                </div>
                                <div id="divAdvancedOptions" style="display: none;">
                                    <div class="clear"><input type="checkbox" id="chkInvalid" checked="checked">Play generic invalid response message</div>
                                    <div class="clear"><input type="checkbox" id="chkError" checked="checked">Play generic no response message</div>
                                    <div class="clear advanced_option_padding" style="padding-left:5px; padding-top:10px">
                                        <select id="ddlNumberOfInvalidRepeats">	
                                            <cfloop index="i" from="1" to="99">
                                                <cfif i NEQ 3>
                                                    <option value="#i#">#i#</option>
                                                <cfelse>
                                                    <option value="#i#" selected="selected">#i#</option>
                                                </cfif>
                                            </cfloop>
                                        </select>
                                        Number of repeats for invalid response.
                                    </div>
                                    
                                    <div class="clear advanced_option_padding" style="display: none;" id="divBetweenKeyPressTime">
                                        <select id="ddlNumberOfBetweenKeyPresses">	
                                            <cfloop index="i" from="0" to="60">
                                                <cfif i NEQ 3>
                                                    <option value="#i#">#i#</option>
                                                <cfelse>
                                                    <option value="#i#" selected="selected">#i#</option>
                                                </cfif>
                                            </cfloop>
                                        </select>
                                        Maximum number of seconds between key presses.
                                    </div>
                                </div>
                                
                                <div class="clear advanced_option_label advanced_option_padding" onclick="showBranchQuestion()" style="font-weight: bold;">
                                    Add Question Condition
                                </div>
                            </div>
                        </cfoutput>
                    </cfif>
                    <cfif INPCOMTYPE eq SURVEY_COMMUNICATION_ONLINE or INPCOMTYPE eq SURVEY_COMMUNICATION_BOTH>
                        <cfoutput>
                            <div class="clear" id="requiredDiv">
                                <div>
                                    <input 
                                        type="checkbox" 
                                        id="requiredAns" 
                                        name="requiredAns" 
                                        onclick="requiresAnswer()"
                                        >
                                    <span><strong>Requires an answer to this question</strong> (Optional)</span>
                                </div>
                                <div id="requiredErrBox" style="display: none;">
                                    <span>When the question is not answered. Display this error message.</span> 
                                    <br />
                                    <span id="errRequiredMsg" class="errMsg" style="color: red; display: none;">
                                        <strong>This field is required.</strong>
                                    </span><br />
                                    <textarea
                                        id="errMsgText"
                                        name="errMsgText"
                                        class=""
                                        cols="46"
                                        rows="6"		
                                    >This question requires an answer.</textarea>
                                </div>
                            </div>
                        </cfoutput>
                    </cfif>
                    
                </div>
            
            </div>
            
            <div class="inner-txt-box">
                <div class="submit">                         
                                            
                        <!--- To get to edit mode supply a INPQID --->               
                        <cfif INPQID GT 0>
                            <a class="button filterButton small" id="CancelFormButton" >Cancel</a>
                        <cfelse>
                            <a class="button filterButton small" id="CancelFormButton" >Close</a>
                        </cfif>
                        
                        <!--- To get to edit mode supply a INPQID --->               
                        <cfif INPQID GT 0>
                            <a class="button filterButton small" id="save_continue" rel1="true" >Save</a>
                        <cfelse>
                            <a class="button filterButton small save_continue" rel1="true" >Add Question</a>
                        </cfif>
                        
                    <!---    <!--- To get to edit mode supply a INPQID --->               
                        <cfif INPQID GT 0>
                            
                        <cfelse>
                            <a class="button filterButton small save_continue" rel1="false" >Save and Continue</a>
                        </cfif>--->
                            
                </div>
            
            </div>
            
		</form>
        </div>     
	</div>    
</cfoutput>


<script id="tmplAnswerItem" type="text/x-jquery-tmpl">
	<div class="clear row_padding_top" id="divAnswerItem_${i}">
		<div class="left answer">
			<textarea 
					id="txtAnswers_${i}" 
					name="txtAnswers_${i}" 
					customField="${i}" 
					class="answerValue notranslate qe-text-input textarea left" 
					cols="45" rows="3">${answerDesc}</textarea>	
			<label class='error' id='ans_err_${i}'>*This field is required.</label>
		</div>
		{{if i > 2}}
		<div class="left" style="margin: 5px 0 0; padding-left: 5px;">
			<input type="button" value="x" class="pointer" onclick="RemoveAnswer('${i}')"/>
		</div>
		{{/if}}
	</div>
</script>

      
<script id="tmplInterValueSelectBox" type="text/x-jquery-tmpl">
	<select id="inpIntervalValue">	                                    
		<option value="0">0</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		<option value="31">31</option>
		<option value="32">32</option>
		<option value="33">33</option>
		<option value="34">34</option>
		<option value="35">35</option>
		<option value="36">36</option>
		<option value="37">37</option>
		<option value="38">38</option>
		<option value="39">39</option>
		<option value="40">40</option>
		<option value="41">41</option>
		<option value="42">42</option>
		<option value="43">43</option>
		<option value="44">44</option>
		<option value="45">45</option>
		<option value="46">46</option>
		<option value="47">47</option>
		<option value="48">48</option>
		<option value="49">49</option>
		<option value="50">50</option>
		<option value="51">51</option>
		<option value="52">52</option>
		<option value="53">53</option>
		<option value="54">54</option>
		<option value="55">55</option>
		<option value="56">56</option>
		<option value="57">57</option>
		<option value="58">58</option>
		<option value="59">59</option>
		<option value="60">60</option>                                                                                                       
	</select>
</script>
        
<script id="tmplInterValueDate" type="text/x-jquery-tmpl">
	<cfoutput>
	  	<input TYPE="text" name="inpIntervalValuet" id="inpIntervalValue" value="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" class="" style="width:85px; border:none; box-shadow:none; text-decoration: underline; cursor: pointer;" title="Calander date queued questions are eligible to continue processesing."/> 
	</cfoutput>	
</script>
                
    
    
    
    

