<cfparam name="INPBATCHID" default="0">
<cfparam name="QNav" default="0">


<cfif not structkeyExists(URL, "PAGE")>
	<cfset page = 1>
<cfelse>
	<cfset page = StructFind(URL, "PAGE")>
	<cfif isNumeric(page) eq false>
		<cfset page = 1>
	</cfif>
</cfif>


<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
<!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
<cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
</cfinvoke> 

<cfif RetVarXML.RXRESULTCODE LT 0>
	
    <div>
    	No campaign data found.    
    </div>

	<cfabort/>

</cfif>


<cfinvoke component="#LocalSessionDotPath#..cfc.ire.marketingSurveyTools" method="GetStartQ" returnvariable="RetValGetStartQ">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
    <cfinvokeargument name="GROUPID" value="#page#">
</cfinvoke>

<cfset startNumberQ = RetValGetStartQ.STARTNUMBERQ>



<!--- Read question GetQAggregateData  --->        
<cfinvoke method="GetQAggregateData" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarGetQAggregateData">
    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">    
    <cfinvokeargument name="GROUPID" value="#PAGE#"> 
</cfinvoke>


<cfif structkeyExists(RetVarGetQAggregateData, 'COMTYPE')>
	<cfset communicationType = RetVarGetQAggregateData.COMTYPE>
<cfelse>
	<cfset communicationType = 'NAN'>
</cfif>


<!--- On page load - draw all Control Points --->



<!--- Add new control point --->


<!--- Remove control point --->


<!--- Edit control point --->

<head>

	<style type="text/css">
    
    
        #InteractiveControlPoints
        {
            position:relative;	
            
        }
        
        #container0 {
            position: absolute;
            height: 100px;
            width: 100px;
            left:0;
            top:0;
            border: 1px solid black;
            margin: 10px;
        }
        
         #container1 {
            position: absolute;
            height: 100px;
            width: 100px;
            left:200px;
            top:100px;
            border: 1px solid black;
            margin: 10px;
        }
        
        
    	
	 .ControlPoint {
            position: absolute;
            height: 100px;
            width: 100px;
            left:0;
            top:0;
            border: 1px solid black;
            margin: 10px;
        }
		
		
        
    </style>


    
    <cfoutput>
    
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jsPlumb-1.5.2-min.js"></script>
    
    </cfoutput>


  <script type="text/javascript">
  
	   $(document).ready(function(){
		   
	
		jsPlumb.draggable($("#container0"), {
			  containment:"InteractiveControlPoints"
			});
			
			jsPlumb.draggable($("#container1"), {
			  containment:"main"
			});
			
			var e0 = jsPlumb.addEndpoint("container0"),
			e1 = jsPlumb.addEndpoint("container1");
				
			jsPlumb.connect({ source:e0, target:e1 });
			
			
		});


		function AddNewControlPoint(inpCPID, inpX, inpY)
		{						
			var newControlPoint = $('<li>').attr('id', 'q_' + inpCPID).addClass('ControlPoint');
			
			<!--- Set new objects default position --->
			newControlPoint.css({ 'top': inpX, 'left': inpY });
			
			
			<!--- Add new control ppoint to the stage --->
			$('#InteractiveControlPoints').append(newControlPoint);
			
		}
		

	function AsyncLoadQuestions()
	{						
		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						GROUPID : '<cfoutput>#PAGE#</cfoutput>'
					};
					
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'ReadXMLQuestions1', data, "Page has not been loaded", function(d) 
		{
			<!--- User feedback - loading --->
			$("#PageLoadingInfo").html('Page Loading... <span id="PageTotalSoFar">' + ii + '</span> of <span id="PageTotalToLoad">' + d.DATA.ARRAYQUESTION[0].length + '</span>');	
			
			if(d.DATA.ARRAYQUESTION[0].length == 0)
			{
				$("#PageLoadingInfo").html('');		
			}
										
			<!--- Loop over results --->
			for (var ii=0; ii <  d.DATA.ARRAYQUESTION[0].length; ii++)
			{					
			
				$("#PageTotalSoFar").html('' + (ii+1));
						
				<!--- Call shared cfc method to get display content - use for page load as well as AJAX updates --->
				
				var CurrQuestionObj = new Object();
				CurrQuestionObj.Description = d.DATA.ARRAYQUESTION[0][ii].TEXT;
				CurrQuestionObj.TEXT = d.DATA.ARRAYQUESTION[0][ii].TEXT;
				CurrQuestionObj.CurrQuestionObjType = d.DATA.ARRAYQUESTION[0][ii].TYPE;
				CurrQuestionObj.TYPE = d.DATA.ARRAYQUESTION[0][ii].TYPE;
				CurrQuestionObj.AF = d.DATA.ARRAYQUESTION[0][ii].AF;
				CurrQuestionObj.IntervalType = d.DATA.ARRAYQUESTION[0][ii].ITYPE;
				CurrQuestionObj.ITYPE = d.DATA.ARRAYQUESTION[0][ii].ITYPE;
				CurrQuestionObj.IntervalValue = d.DATA.ARRAYQUESTION[0][ii].IVALUE;
				CurrQuestionObj.IVALUE = d.DATA.ARRAYQUESTION[0][ii].IVALUE;
				CurrQuestionObj.IntervalHour = d.DATA.ARRAYQUESTION[0][ii].IHOUR;
				CurrQuestionObj.IHOUR = d.DATA.ARRAYQUESTION[0][ii].IHOUR;
				CurrQuestionObj.IntervalMin = d.DATA.ARRAYQUESTION[0][ii].IMIN;
				CurrQuestionObj.IMIN = d.DATA.ARRAYQUESTION[0][ii].IMIN;
				CurrQuestionObj.IntervalNoon = d.DATA.ARRAYQUESTION[0][ii].INOON;
				CurrQuestionObj.INOON = d.DATA.ARRAYQUESTION[0][ii].INOON;
				CurrQuestionObj.IntervalExpiredNextRQ = d.DATA.ARRAYQUESTION[0][ii].IENQID;
				CurrQuestionObj.IENQID = d.DATA.ARRAYQUESTION[0][ii].IENQID;		
				CurrQuestionObj.ErrMsgTxt = d.DATA.ARRAYQUESTION[0][ii].ERRMSGTXT;
				CurrQuestionObj.RequiresAns = d.DATA.ARRAYQUESTION[0][ii].REQUIREDANS; 
				CurrQuestionObj.scriptId = '';
				CurrQuestionObj.LISTANSWER = JSON.stringify(d.DATA.ARRAYQUESTION[0][ii].LISTANSWER);	
				CurrQuestionObj.Conditions = JSON.stringify(d.DATA.ARRAYQUESTION[0][ii].CONDITIONS);
				CurrQuestionObj.RQ = parseInt(ii+1) + parseInt(startNumberQ) - 1;
				CurrQuestionObj.ID = d.DATA.ARRAYQUESTION[0][ii].ID;
				CurrQuestionObj.inpCondXML = '';
				CurrQuestionObj.PAGE = '<cfoutput>#PAGE#</cfoutput>';
				CurrQuestionObj.BOFNQ = d.DATA.ARRAYQUESTION[0][ii].BOFNQ;
				
				var data2 =  { 
								INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
								questionJSON : JSON.stringify(CurrQuestionObj),						
								startNumberQ: '<cfoutput>#startNumberQ#</cfoutput>' ,
								indexNumber : ii + 1,
							//	DoesHavePermissionAddQuestion : '<cfoutput>#DoesHavePermissionAddQuestion#</cfoutput>',
							//	DoesHavePermissionEditQuestion : '<cfoutput>#DoesHavePermissionEditQuestion#</cfoutput>',
							//	DOESHAVEPERMISSIONDELETEQUESTION : '<cfoutput>#DOESHAVEPERMISSIONDELETEQUESTION#</cfoutput>',
							//	COMMUNICATIONTYPE : '<cfoutput>#COMMUNICATIONTYPE#</cfoutput>',
								PAGE : '<cfoutput>#PAGE#</cfoutput>',
								includeLI : 1,
								inpXMLControlString : '',
								UniqueReqCount : ii + 1						
							};	
				
							
				<!--- Run stuff on page loading complete --->	
				if( (d.DATA.ARRAYQUESTION[0].length - 1) == ii )
				{
					
						AddNewControlPoint(inpCPID, inpX, inpY)
						$("#PageLoadingInfo").html('');						
						
								
					
				}
				else
				{
					AddNewControlPoint(inpCPID, inpX, inpY);
									
				}
			
				
			}
			
		}); 
		
		
 	</script>
</head>


<body>
 
   
    <ul id="InteractiveControlPoints"> 
       <li id="container0">
       </li>
       <li id="container1">
       </li>
    </ul>
       
</body>




















