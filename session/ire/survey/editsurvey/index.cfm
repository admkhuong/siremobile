<cfparam name="INPCOMTYPE" default="">
<cfparam name="INPBATCHID" default="0">

<cfif INPCOMTYPE EQ "SMS">
	<!--- Skip to edit questions--->
	<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editquestions/index?inpbatchid=#INPBATCHID#" addtoken="no">
</cfif>

<!-- Import constant variables --->
<cfinclude template="../../constants/surveyConstants.cfm">
<cfinclude template="../../../lib/xml/xmlUtil.cfc">
<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
	</style>
    
    <!--- include tinyMCE locally - conflicting versions old vs new--->
	<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
    <!---<script src="#rootUrl#/#PublicPath#/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>--->
    
</cfoutput>
<!--- Get data --->
<cfif INPBATCHID EQ "0">
	<cflocation url="#rootUrl#/#sessionPath#/ire/survey/surveylist/">
<cfelse>
	<cfif not isnumeric(INPBATCHID)>
		<cflocation url="#rootUrl#/#sessionPath#/ire/survey/surveylist/">
	<cfelse>
		<!--- Get Survey Tool Object --->
		<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
	</cfif>
</cfif>

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#edit_Survey_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#edit_Survey_Title#">
</cfinvoke>

<cfset isCreated = false>
<cfif structkeyExists(URL, 'IsCreated')>
	<cfset isCreated = true>
<cfelse>
	<cfset isCreated = false>
</cfif>

<!--- Check step --->
<cfif not structkeyExists(URL, "STEP")>
	<cfset curentStep = 1 />
<cfelse>
	<cfset curentStep = structfind(URL, "STEP") />
</cfif>
<cfif isnumeric(curentStep) eq false>
	<cflocation url="#rootUrl#/#sessionPath#/ire/survey/surveylist/">
<cfelse>
	<!--- Prepare data for each page --->
	<cfswitch expression="#curentStep#">
		<!--- Step 1 Edit Basic Information --->
		<cfcase value="1">
			<!--- Get Survey Basic information --->
			<cfset currentSurvey = surveyObj.GetBasicSurveyInfo(INPBATCHID) />
			<!--- Check Expired date --->
			<cfif currentSurvey.EXPIREDATE eq "">
				<cfset noExpired="checked" />
				<cfset Expired="false" />
			<cfelse>
				<cfset Expired="checked" />
				<cfset noExpired="false" />
			</cfif>
			<!--- Check Communication Types --->
			<cfset EMAIL="false" />
			<cfset VOICE="false" />
			<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_ONLINE>
				<cfset EMAIL="checked" />
			</cfif>
			<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_PHONE>
				<cfset VOICE="checked" />
			</cfif>
			<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_BOTH>
				<cfset VOICE="checked" />
				<cfset EMAIL="checked" />
			</cfif>
			
			<cfinclude template="steps/step#curentStep#.cfm">
		</cfcase>
		<!--- Step 2 --->
		<cfcase value="2">
			<!--- Get data to edit template --->
			<cfset currentSurvey = surveyObj.GetSurveyCustomize(INPBATCHID) />
			<cfset cbDisableBackVal = false>
			
			<cfif currentSurvey.INPENABLEBACK eq "0">
				<cfset cbDisableBackVal = true>
			<cfelse>
				<cfset cbDisableBackVal = false>
			</cfif>
			<cfif CGI.REQUEST_METHOD neq 'POST'>
				<!--- Do nothing --->
			<cfelse>
				<!--- Validate Form --->
				<cfif StructIsEmpty(FORM) eq true or INPBATCHID neq FORM.INPBATCHID>
					<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=1">
				<cfelse>
					<cfset SaveStep1SurveyInfo()>
				</cfif>
			</cfif>
			<cfinclude template="steps/step#curentStep#.cfm">
		</cfcase>
		<cfcase value="3">
			<cfset currentSurvey = surveyObj.getSurveyPage(INPBATCHID) />
			<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_PHONE>
				<cfset SaveStep1SurveyInfo()>
			</cfif>
			<cfif CGI.REQUEST_METHOD neq 'POST'>
				<!--- do nothing --->
			<cfelse>
				<cfif StructIsEmpty(FORM) eq true or INPBATCHID neq FORM.INPBATCHID>
					<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=2">
				<cfelse>
					<cfif currentSurvey.COMMUNICATIONTYPE neq SURVEY_COMMUNICATION_PHONE>
						<cfset oldSurvey = surveyObj.GetSurveyCustomize(INPBATCHID) />
						<cfset Request.errMsg = '' />
						<cfset INPBATCHID = FORM.INPBATCHID />
						<cfset surveyTitle = encodeStringXml(FORM.txtSurveyName) />
						
						<cfset txtFooterText = encodeStringXml(FORM.txtFooterText) />
						<cfset errMsg = "" />
						<cfset logoUrl = "" />
						<cfif StructKeyExists(FORM, "inpImgServerFile")>
							<!--- 
							<cffile 
							action="upload"
							filefield="inpImgFile"
							destination="#LogoSurveyUploadPath#"
							result="fileupload"
							nameconflict="makeunique"
							>
						
							<cfif fileupload.fileWasSaved>
								<cfset isValidImage = IsImageFile("#fileUpload.serverDirectory#/#fileUpload.serverFile#")>
								<cfset extFile = fileupload.SERVERFILEEXT>
								<cfif extFile neq 'jpg' and extFile neq 'gif' and extFile neq 'png'>
									<cfset isValidImage = false>
								</cfif>
								<cfif isValidImage eq true>
									<cfset logoUrl = "#fileUpload.serverFile#" />
								<cfelse>
									<cffile action="delete" 
										file="#fileUpload.serverDirectory#/#fileUpload.serverFile#">
									<cfset errMsg = errMsg & "Invalid Image File!<br />" />
								</cfif>
								<cfimage action="read" 
									source="#fileUpload.serverDirectory#/#fileUpload.serverfile#" 
									name="uploadedImage"
									> 
								<cfif ImageGetHeight(uploadedImage) gt 65 or ImageGetWidth(uploadedImage) gt 250>
									<cffile action="delete" 
										file="#fileUpload.serverDirectory#/#fileUpload.serverFile#">
									<cfset errMsg = errMsg & "Invalid Image Size!<br />" />
								</cfif>
								
							</cfif>--->
							<cfset logoUrl = FORM.inpImgServerFile>
						<cfelse>
							<cfset logoUrl = oldSurvey.INPIMGUPLOADLOGO>
						</cfif>
						<cfif len(Trim(errMsg)) gt 0>
							<cfset Session.errMsg = errMsg />
							<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=2">
						<cfelse>
							<!--- Update survey tempalte --->
							<cfset result = 
								surveyObj.UpdateSurveyCustomize(
									INPBATCHID,
									surveyTitle,
									txtFooterText,
									logoUrl
								) />

							<!--- <cfif result.RXRESULTCODE eq 1>
								<cfset currentSurvey = surveyObj.getSurveyPage(INPBATCHID) />
							<cfelse>
								<cfset Session.errMsg = "Update survey fails!<br />" />
								<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=2">
							</cfif> --->
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfinclude template="steps/step#curentStep#.cfm">
		</cfcase>
		<!--- Update welcome & thanks text --->
		<cfcase value="4">
			<cfif CGI.REQUEST_METHOD neq 'POST'>
				<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=3">
			<cfelse>
				<cfset currentSurvey = surveyObj.getSurveyPage(INPBATCHID) />
				<cfif StructIsEmpty(FORM) eq true or INPBATCHID neq FORM.INPBATCHID>
					<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=3">
				<cfelse>
					<cfset INPBATCHID = FORM.INPBATCHID />
					<cfset txtStartPrompt = "">
					<cfset txtEndPrompt = "">
					<cfset StartPromptScriptId = ""/>
					<cfset EndPromptScriptId = ""/>
					
					<cfset txtWelcomeText = ""/>
					<cfset txtThankYouText = ""/>
					
					<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_ONLINE>
						<cfset txtStartPrompt = encodeStringXml(FORM.txtStartPrompt)>
						<cfset txtEndPrompt = encodeStringXml(FORM.txtEndPrompt)>
						<cfif txtStartPrompt EQ VOICEPROMPT>
							<cfset txtStartPrompt = "">
						</cfif>
						
						<cfif txtEndPrompt EQ VOICEPROMPT>
							<cfset txtEndPrompt = "">
						</cfif>
						<cfset StartPromptScriptId = FORM.hidWelcomeScriptId/>
						<cfset EndPromptScriptId = FORM.hidThankYouScriptId/>
					</cfif>
					<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_PHONE>
						<cfset txtWelcomeText = encodeStringXml(FORM.txtWelcomeText) />
						<cfset txtThankYouText = encodeStringXml(FORM.txtThankYouText) />				
					</cfif>
					<cfset Session.errMsg = '' />
					<cfset errMsg = ''>
					<cfif isCreated eq true>
						<cfset isCreatedParam = "&IsCreated=1">
					<cfelse>
						<cfset isCreatedParam = "">
					</cfif>	
					
					<cfset result = 
						surveyObj.UpdateSurveyPage(
							INPBATCHID,
							txtStartPrompt,
							txtEndPrompt,
							StartPromptScriptId,
							EndPromptScriptId,					
							txtWelcomeText,
							txtThankYouText)/>
					<cfif result.RXRESULTCODE eq 1>
						<!--- <cfif isCreated eq true> --->
							<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editquestions/index?inpbatchid=#INPBATCHID#">
						<!--- <cfelse>
							<cflocation url="#rootUrl#/#sessionPath#/ire/survey/survey.cfm">
						</cfif> --->
					<cfelse>
						<cfset Session.errMsg = "Update survey fails!<br />" />
						<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=3#isCreatedParam#">
					</cfif>
				</cfif>
			</cfif>
		</cfcase>
		<cfdefaultcase><cflocation url="#rootUrl#/#sessionPath#/ire/survey/surveylist/"></cfdefaultcase>
	</cfswitch>
	
</cfif>
<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/utility.css');
	</style>
</cfoutput>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		
		$('#subTitleText').text('<cfoutput>#Survey_Title#  >>  #edit_Survey_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Marketing_Title#</cfoutput>');
		
//		tinyMCE.init({
//			mode : "none",
//			theme : "simple",
//			width: "840px",
//			height: "250px",
//			visual : false,
//			convert_urls : false,
//			mode: "none",
//			theme : "advanced",
//			force_p_newlines: true,
//			theme_advanced_toolbar_location : "bottom" 
//	    });
	    
	    //tinyMCE.execCommand('mceAddControl', false, 'txtFooterText');
		tinymce.init({
		    selector: "textarea",
		    toolbar: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			width:"81%"
		});
	    
	});
	
	window.onbeforeunload= function() { }   
	window.history.forward(0);
</script>

<cffunction name="SaveStep1SurveyInfo" output="false" hint="Save survey info at step 1">
	<cfif StructKeyExists(FORM,"INPBATCHID")>
		<cfset Request.errMsg = '' />
		<cfset INPBATCHID = FORM.INPBATCHID />
		<cfset inpSurveyDesc = FORM.inpSurveyDesc />
		
		<cfset IsExpire = FORM.IsExpire />
		
		<cfset SURVEY_EXPIRATION_DATE = FORM.SURVEY_EXPIRATION_DATE />
		
		<cfif StructKeyExists(FORM, 'chkEmail')>
			<cfset chkEmail = FORM.chkEmail />
		<cfelse>
			<cfset chkEmail = "" />
		</cfif>
		<cfif StructKeyExists(FORM, 'chkVoice')>
			<cfset chkVoice = FORM.chkVoice />
		<cfelse>
			<cfset chkVoice = "" />
		</cfif>
		
		<cfset communicationType = "" />
		<cfif chkEmail neq '' and chkVoice eq ''>
			<cfset communicationType = SURVEY_COMMUNICATION_ONLINE />
		</cfif>
		<cfif chkVoice neq '' and chkEmail eq ''>
			<cfset communicationType = SURVEY_COMMUNICATION_PHONE />
		</cfif>
		<cfif chkVoice neq '' and chkEmail neq ''>
			<cfset communicationType = SURVEY_COMMUNICATION_BOTH />
		</cfif>
		<cfset errMsg = "" />
		<cfif len(Trim(inpSurveyDesc)) eq 0>
			<cfset errMsg = errMsg & "Invalid Title.<br />" />
		</cfif>
		
		<cfif isExpire eq 'YES'>
			<cfif SURVEY_EXPIRATION_DATE eq ''>
				<cfset errMsg = errMsg & "Invalid Expired Date!<br />">
			<cfelse>
				<cfset datecom = 
						DateCompare(
							DateFormat(SURVEY_EXPIRATION_DATE, "MM/DD/YYYY"), 
							DateFormat(Now(), "MM/DD/YYYY")) 
					/>
				<cfif datecom lte 0>
					<cfset errMsg = errMsg & "Invalid Expired Date. Expired Date cannot earlier than current date.<br />" />
				</cfif>
				<cfset SURVEY_EXPIRATION_DATE = SURVEY_EXPIRATION_DATE & " 23:59">
			</cfif>
		<cfelse>
			<cfset SURVEY_EXPIRATION_DATE = "" />
		</cfif>
		<cfif len(Trim(errMsg)) gt 0>
			<cfset Session.errMsg = errMsg />
			<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=1">
		<cfelse>
			<!--- Update basic information--->
			<cfset oldSurvey = surveyObj.GetBasicSurveyInfo(INPBATCHID) />
			<cfset dataout = 
				surveyObj.UpdateSurveyListDescExpDate(
				INPBATCHID, 
				inpSurveyDesc, 
				SURVEY_EXPIRATION_DATE,
				oldSurvey.COMMUNICATIONTYPE
				) />
			<cfif dataout.RXRESULTCODE eq 1>
			<cfelse>
				<cfset Session.errMsg = "Update survey fails!<br />" />
				<cflocation url="#rootUrl#/#sessionPath#/ire/survey/editSurvey/?inpbatchid=#INPBATCHID#&STEP=1">
			</cfif>
		</cfif>
	</cfif>
</cffunction>