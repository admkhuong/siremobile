<!-- Start generate edit form --->
<div class="label"><h1>
		<span>
			<cfoutput>#SURVEY_UPDATE_TITLE#</cfoutput> 
		</span>
	</h1></div>
<cfif isCreated eq true>
	<cfset isCreatedParam = "&IsCreated=1">
<cfelse>
	<cfset isCreatedParam = "">
</cfif>
<div class='msg_box'>
	<cfif StructKeyExists(Session, 'errMsg') and len(Trim(Session.errMsg)) gt 0 >
		<cfoutput>
			<h2 class='err_msg'><span>
					#Trim(Session.errMsg)#
			</h2></span> 
		</cfoutput>
	</cfif>
	<cfset Session.errMsg = '' />
</div>
<div id='EditSurveyListMainNavigator' class="RXForm">
	<div id="RightStage">
		<cfset nextStep = 2>
		<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_PHONE>
			<cfset nextStep = 3>
		</cfif>
		
		<!---<cfform 
			action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=#nextStep##isCreatedParam#"
			name="EditSurveyForm" 
			id="EditSurveyForm"
			method="POST"
			onsubmit="validateForm(); return false;"
			>--->
			
		<cfoutput>
			<form id="EditSurveyForm" name="EditSurveyForm" action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=#nextStep##isCreatedParam#" method="POST"></cfoutput>
			<input type="hidden" name="currentStep" id="currentStep" value="1" validate="integer" required="true">
			<cfoutput><input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#currentSurvey.INPBATCHID#" validate="integer" required="true"></cfoutput>
			<!---<cfinput type="hidden" name="currentStep" id="currentStep" value="1" validate="integer" required="true" />--->
			<!---<cfinput type="hidden" name="INPBATCHID" id="INPBATCHID" value="#currentSurvey.INPBATCHID#" validate="integer" required="true" />--->
			<div class="clear left field_padding">
				<label><strong><cfoutput>#SURVEY_LABEL_NAME#</cfoutput></strong></label>
				<!---<cfinput 
					name="inpSurveyDesc" 
					id="inpSurveyDesc" 
					type="text" 
					size="30" 
					value="#currentSurvey.SURVEYDESC#"
					label="Survey Name"
					onchange="isValidTitle()"
					onkeyup="isValidTitle()"
					class="field_margin"
					 /><br />--->
				<cfoutput><input 
					id="inpSurveyDesc" 
					class="field_margin" 
					type="text" 
					name="inpSurveyDesc" 
					value="#currentSurvey.SURVEYDESC#" 
					checked="#noExpired#" 
					onchange="isValidTitle()"
					onkeyup="isValidTitle()"></cfoutput>
				<label id="errSurveyTitle" class="error" style="display: none; color: red">
				<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong></label>
			</div>
			<div class="clear left field_padding">
				<label><strong><cfoutput>#SURVEY_LABEL_EXPIRED#</cfoutput></strong></label>
				<!---<cfinput 
					type="radio" 
					name="IsExpire" 
					id="rdoNoExpire" 
					class="left margin_left_5px"
					value="NO"
					onclick="javacript:ChangeExpirationOption(this)"
					checked="#noExpired#"
					label="#SURVEY_LABEL_NO#"
					 />--->
				<cfoutput><input 
					id="rdoNoExpire" 
					class="left margin_left_5px" 
					type="radio" 
					name="IsExpire" 
					value="NO" 
					onclick="javacript:ChangeExpirationOption(this)"></cfoutput>
				<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_NO#</cfoutput></span>
				<!---<cfinput 
					type="radio" 
					name="IsExpire" 
					id="rdoYesExpire" 
					class="left margin_left_5px"
					value="YES"
					onclick="javacript:ChangeExpirationOption(this)"
					checked="#Expired#"
					label="#SURVEY_LABEL_YES#"
					 />--->
				<cfoutput><input 
					id="rdoYesExpire" 
					class="left margin_left_5px" 
					type="radio" 
					name="IsExpire" 
					value="YES" 
					onclick="javacript:ChangeExpirationOption(this)"></cfoutput>
				<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_YES#</cfoutput></span>
			</div>
			<cfif Expired eq false>
				<cfset expired_date_display = "none" />
			<cfelse>
				<cfset expired_date_display = "block" />
			</cfif>
			<div class="left clear field_padding" 
				style="display: <cfoutput>#expired_date_display#;</cfoutput>" 
				id="Expiration_Date_Picker">
				<!---<cfinput 
					type="datefield" 
					value="#DateFormat(currentSurvey.EXPIREDATE, "MM/DD/YYYY")#" 
					name="SURVEY_EXPIRATION_DATE" 
					id="SURVEY_EXPIRATION_DATE" 
					validate="date" 
					onchange="isValidDate()"
					message="#SURVEY_ERR_DATE#"
				 /> --->
				 <input type="text" id="SURVEY_EXPIRATION_DATE" name="SURVEY_EXPIRATION_DATE" value="<cfoutput>#DateFormat(currentSurvey.EXPIREDATE, "MM/DD/YYYY")#</cfoutput>" onchange="IsValidExpirationDate(); return false;" maxlength="10">
				<br />
				<span class="left expiry_date_time" style="color: #007EE7;">
					<cfoutput>#SURVEY_LABEL_END_AT#</cfoutput>
				</span><br />
				<label class="error" id="errDateMsg" style="color: red; display: none;">
					<strong><cfoutput>#SURVEY_ERR_DATE#</cfoutput></strong></label>
				<label id="lblInvalidPastDateError" class="error" style="display: none;">
					<strong><cfoutput>#SURVEY_ERROR_PAST_DATE#</cfoutput></strong></label>	
			</div>
			<div class="field_padding left clear">
				<label class="qe-label-a left"><strong><cfoutput>#SURVEY_LABEL_DELIVERY#</cfoutput></strong></label>
				<div class="clear left">
					<!---<cfinput
						name="chkEmail"
						id="chkEmail"
						onclick="ValidateCommunicationType()"
						class="left margin_left_5px"
						type="checkbox"
						value="#SURVEY_COMMUNICATION_ONLINE#"
						checked="#EMAIL#"
						disabled="true"
						 />--->
					<cfoutput><input type="checkbox" 
						id="chkEmail" 
						name="chkEmail" 
						onclick="ValidateCommunicationType()"
						class="left margin_left_5px"
						value="#SURVEY_COMMUNICATION_ONLINE#"
						checked="#EMAIL#"
						disabled="true"></cfoutput>
					<span class="left margin_left_5px"><cfoutput>#SURVEY_LABEL_ONLINE#</cfoutput></span>
					<!---<cfinput
						name="chkVoice"
						id="chkVoice"
						onclick="ValidateCommunicationType()"
						class="left margin_left_20px"
						type="checkbox"
						value="#SURVEY_COMMUNICATION_PHONE#"
						checked="#VOICE#"
						disabled="true"
						 />--->
					<cfoutput><input 
						name="chkVoice"
						id="chkVoice"
						onclick="ValidateCommunicationType()"
						class="left margin_left_20px"
						type="checkbox"
						value="#SURVEY_COMMUNICATION_PHONE#"
						checked="#VOICE#"
						disabled="true"
						></cfoutput>
					<span class="left margin_left_5px" id="spnVoice" name="spnVoice"><cfoutput>#SURVEY_LABEL_PHONE#</cfoutput></span>
					<br>
					<label id="lblSurveyDeliveryMethodError" class="error" for="SurveyDeliveryMethod" style="display: none; color: red;">
						<strong><cfoutput>#SURVEY_ERR_DELIVERY#</cfoutput></strong>
					</label>
				</div>
			</div>
			
			<div id ="gender_container" style="display:none">
				<div class="clear left field_padding">
					<label><strong><cfoutput>#SURVEY_LABEL_SPEAKER#</cfoutput></strong></label>
				</div>
				<div class="clear left field_padding">
					<cfset selected = 'selected="true"'>
					<select id="inpSpeaker" name="inpSpeaker" disabled="true">
						<option value="1" <cfif currentSurvey.VOICE EQ 1><cfoutput>#selected#</cfoutput></cfif>>Tom</option>
						<option value="2" <cfif currentSurvey.VOICE EQ 2><cfoutput>#selected#</cfoutput></cfif>>Samantha</option>
						<option value="3" <cfif currentSurvey.VOICE EQ 3><cfoutput>#selected#</cfoutput></cfif>>Paulina</option>
						<option value="4" <cfif currentSurvey.VOICE EQ 4><cfoutput>#selected#</cfoutput></cfif>>Mary</option>
						<option value="5" <cfif currentSurvey.VOICE EQ 5><cfoutput>#selected#</cfoutput></cfif>>Sam</option>
						<option value="6" <cfif currentSurvey.VOICE EQ 6><cfoutput>#selected#</cfoutput></cfif>>Mike</option>
					</select>
				</div>		
			</div>
			<div class="CreateSurveyButton  left clear">
				<button id="cancel" 
					TYPE="button" 
					class="ui-corner-all" 
					onclick="cancelCreate()">
						<cfoutput>#SURVEY_LABEL_CANCEL#</cfoutput>
				</button>
				<span id="Cancel">or</span>
				<!---<cfinput
					type="submit"
					name=""
					class="ui-corner-all"
					value="#SURVEY_LABEL_NEXT#"
					label="#SURVEY_LABEL_NEXT#"
					id="NextToEditSurveyContent"
					onclick="validateForm(); return false;"
					 />--->
				
				<cfoutput><input type="submit" class="ui-corner-all"
					value="#SURVEY_LABEL_NEXT#"
					label="#SURVEY_LABEL_NEXT#"
					id="NextToEditSurveyContent"
					onclick="validateForm(); return false;"></cfoutput>
			</div>
		<!---</cfform>--->
		</form>
	</div>
</div>

<script type="text/javascript">
	
	$(function() {
		$( "#SURVEY_EXPIRATION_DATE" ).datepicker();
		
		if($('#chkVoice').is(':checked')){
			$('#gender_container').show();
		}else{
			$('#gender_container').hide();
		}
		
		if("<cfoutput>#noExpired#</cfoutput>" == "checked"){
			$("#rdoNoExpire").attr('checked', true);
		}
		
		if("<cfoutput>#Expired#</cfoutput>" == "checked"){			
			$("#rdoYesExpire").attr('checked', true);
		}
		
	});
	
	function cancelCreate(){
		//window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/';
		if (document.referrer == "") {
		   	window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/";
		} else {
		    history.back()
		}
		return false;
	}
	
	function IsValidExpirationDate(){
		$("#errDateMsg").hide();
		$('#lblInvalidPastDateError').hide();
		
		if($('#SURVEY_EXPIRATION_DATE').val() == "") {	  
			$("#errDateMsg").show();
	  		return false;
	  	}
	  	else {
	  		var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/; // format dd/mm/yyyy
	  		if($('#SURVEY_EXPIRATION_DATE').val().match(re)){
	  			//alert(1);
	  			var fullDate = new Date()
				//convert month to 2 digits
				var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
				  
				var currentDate = twoDigitMonth + '/' + fullDate.getDate() + "/" + fullDate.getFullYear();
				
				var now = Date.parse(currentDate, 'mm/dd/yyyy');
				
				var expiredDate = Date.parse($('#SURVEY_EXPIRATION_DATE').val(), 'mm/dd/yyyy');
		
				var diff = expiredDate - now;
	  			
	  			if(diff <= 0){
					$('#lblInvalidPastDateError').show();
	  				return false;
	  			}else{
	  				$("#errDateMsg").hide();
	  				return true;
	  			}
	  			
	  		}else{
	  			$("#errDateMsg").show();
	  			return false;
	  		}
	  	}
	}
	function isValidTitle(){
		var title = $("#inpSurveyDesc").val().trim();
		if(title == ''){
			$("#errSurveyTitle").show();
			return false;
		}else{
			$("#errSurveyTitle").hide();
			return true;
		}
	}
	function validateForm(){	
		var isValidDesc = isValidTitle();

		var isValidExpired = $("#rdoNoExpire").is(":checked")? true : IsValidExpirationDate();
		
		if(isValidDesc && isValidExpired){
			$("#EditSurveyForm").submit();
		}else{
			return false;
		}
	}
</script>

<!--- Javascript to handle actions --->
<script type="text/javascript">
	function ChangeExpirationOption(obj) {
		var display = $(obj).val() == 'YES' ? '' : 'none';
		
		$("#Expiration_Date_Picker").css('display', display);
	}
	// Check comunication type
	function ValidateCommunicationType() { 
		if (!$('#chkEmail').is(':checked') && !$('#chkVoice').is(':checked')) {
	  		$('#lblSurveyDeliveryMethodError').show();
	  		return false;
	  	}
	  	else {
	  		$('#lblSurveyDeliveryMethodError').hide();
		  	return true;
		}
	}
</script> 
