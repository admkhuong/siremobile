<cfoutput>
<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload_new.js"></script>
<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine.js" type="text/javascript" charset="utf-8"></script>
</cfoutput>
<div class="label">
	<h1>
		<span>
		<cfoutput>#SURVEY_CUSTOMIZE_TITLE#</cfoutput>
		</span>
	</h1>
</div>
<div class='msg_box'>
	<cfif StructKeyExists(Session, 'errMsg') and len(Trim(Session.errMsg)) gt 0 >
		<cfoutput>
			<h2 class='err_msg'><span>
					#Trim(Session.errMsg)#
			</h2></span>
		</cfoutput>
	</cfif>
	<cfset Session.errMsg = ''>
</div>
<cfif isCreated eq true>
	<cfset isCreatedParam = "&IsCreated=1">
<cfelse>
	<cfset isCreatedParam = "">
</cfif>

<!---<cfform name="CustomizeTemplateForm" 
	id="CustomizeTemplateForm"
	method="POST" 
	action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=3#isCreatedParam#"
	enctype="multipart/form-data"
	>--->
	<form 
		id="CustomizeTemplateForm" 
		name="CustomizeTemplateForm" 
		action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=3#isCreatedParam#" 
		method="POST" 
		enctype="multipart/form-data">
	<div class=" field_padding">
		<!---<cfinput type="hidden" name="currentStep" id="currentStep" value="2" validate="integer" required="true" />
		<cfinput type="hidden" name="INPBATCHID" id="INPBATCHID" value="#currentSurvey.INPBATCHID#" validate="integer" required="true" />--->
		<input type="hidden" name="currentStep" id="currentStep" value="2" validate="integer" required="true">
		<cfoutput><input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#currentSurvey.INPBATCHID#" validate="integer" required="true"></cfoutput>
		<div class="clear field_padding">
			<div class="left">
				<label><strong><cfoutput>#SURVEY_LABEL_TITLE#</cfoutput></strong></label>
				<!---<cfinput 
					name="txtSurveyName" 
					id="txtSurveyName" 
					type="text" 
					size="50" 
					value="#currentSurvey.INPSURVEYNAME#"
					label="Survey Name"
					onchange="isValidTitle()"
					onkeyup="isValidTitle()"
					class="field_margin"
					tooltip="This is what shows up in browser's tab/window title"
					 />--->
					 
				<cfoutput><input 
					name="txtSurveyName" 
					id="txtSurveyName" 
					type="text" 
					size="50" 
					value="#currentSurvey.INPSURVEYNAME#"
					label="Survey Name"
					onchange="isValidTitle()"
					onkeyup="isValidTitle()"
					class="field_margin"
					tooltip="This is what shows up in browser's tab/window title"></cfoutput>
				<br />
				<label class="error" style="color: red; display: none" id="errSurveyTitle">
						<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong></label>
			</div>
		
			<!--- <div class="right">
				<label><strong><cfoutput>#SURVEY_LABEL_GLOBAL_DISABLE#</cfoutput></strong></label>
				<cfinput
					name="cbDisableBack"	
					type="checkbox"
					id="cbDisableBack"
					class="field_margin"
					checked="#cbDisableBackVal#"
					 style="margin-left: 1px;"/>
				<span style="padding-left: 3px;" for="cbDisableBack"><cfoutput>#SURVEY_LABEL_YES#</cfoutput></span>
			</div> --->
		</div>
		<div class="clear left field_padding">
			<label><strong><cfoutput>#SURVEY_LABEL_LOGO#</cfoutput></strong></label>
			<div class='upimg_block'>
			<!---<cfinput 
				name="inpImgFile" 
				id="inpImgFile" 
				type="file"
				value="#currentSurvey.INPIMGUPLOADLOGO#"
				class="field_margin"
				tooltip="#SURVEY_LABEL_LOGO_TOOLTIP#"
				onchange="javascript:ajaxupload()"
				 /><br />--->
			<cfoutput><input 
				name="inpImgFile" 
				id="inpImgFile" 
				type="file"
				value="#currentSurvey.INPIMGUPLOADLOGO#"
				class="field_margin"
				tooltip="#SURVEY_LABEL_LOGO_TOOLTIP#"
				onchange="javascript:ajaxupload()"><br /></cfoutput>
			<input TYPE="hidden" name="inpImgServerFile" id="inpImgServerFile" value="<cfoutput>#currentSurvey.INPIMGUPLOADLOGO#</cfoutput>" />
			<cfoutput>#SURVEY_LABEL_IMAGE#</cfoutput>
			</div>
			<cfset imgServerPath = "#rxdsWebProcessingPath#\survey/U#Session.USERID#\#currentSurvey.INPIMGUPLOADLOGO#">
			<cfif FileExists(imgServerPath)>
				<cfset 
					img_path = "#rootUrl#/#sessionPath#/ire/survey/act_previewImage?"
						 & "serverfileName=#currentSurvey.INPIMGUPLOADLOGO#&userId=#Session.UserId#">
			<cfelse>
				<cfset currentSurvey.INPIMGUPLOADLOGO = "">
			</cfif>
			
			<cfoutput>
				<span class='logo_preview' id='imgPreview'>
					<cfif trim(currentSurvey.INPIMGUPLOADLOGO) neq ''>
						<img src="#img_path#" height="65">
						<img src="#rootUrl#/#PublicPath#/images/mb/bin_empty.png" 
							id="deleteImgFile" 
							rel="#currentSurvey.INPIMGUPLOADLOGO#"
							onClick="deleteImage(); return false;" 
							style="cursor: pointer" 
							title="remove logo"
							/>
					<cfelse>
						
					</cfif>
				</span>
			</cfoutput>
			
		</div>
		<div class="clear left field_padding non_padding_bottom">
			<label><strong><cfoutput>#SURVEY_LABEL_FOOTER#</cfoutput></strong></label>
			<cfoutput>#SURVEY_LABEL_FOOTER_TOOLTIP#</cfoutput>
		</div>
		<div class="clear left field_padding non_padding_top">
			<!---<cftextarea 
				name="txtFooterText" 
				richtext="false"
				id="txtFooterText"
				style="height: 120px; width: 80%;"
				class="field_margin"
				value="#currentSurvey.INPFOOTERTEXT#"
				></cftextarea>--->
				
				<cfoutput><textarea id="txtFooterText" name="txtFooterText" style="height: 120px; width: 80%;" class="field_margin"> #currentSurvey.INPFOOTERTEXT# </textarea> </cfoutput>
		</div>
		
		<div class="CreateSurveyButton clear right MarRight50">
			<button id="Back" 
				TYPE="button" 
				class="ui-corner-all" 
				onclick="backToStep('<cfoutput>#currentSurvey.INPBATCHID#</cfoutput>', 1)">
					<cfoutput>#SURVEY_LABEL_BACK#</cfoutput>
			</button>
			<!---<cfinput
				type="submit"
				name=""
				class="ui-corner-all"
				value="#SURVEY_LABEL_NEXT#"
				label="#SURVEY_LABEL_NEXT#"
				id="NextToEditSurveyContent"
				onclick="validateForm(); return false;"
				 />--->
				 
			<cfif isCreated eq true>
				<cfoutput><input 
					type="submit"
					name=""
					class="ui-corner-all"
					value="#SURVEY_LABEL_CREATE#"
					label="#SURVEY_LABEL_CREATE#"
					id="NextToEditSurveyContent"
					onclick="validateForm(); return false;"></cfoutput>
			<cfelse>
				<cfoutput><input 
					type="submit"
					name=""
					class="ui-corner-all"
					value="#SURVEY_LABEL_UPDATE#"
					label="#SURVEY_LABEL_UPDATE#"
					id="NextToEditSurveyContent"
					onclick="validateForm(); return false;"></cfoutput>	
			</cfif>
		</div>
	</div>
</form>
<!---</cfform>--->
<script type="text/javascript">
	function isValidTitle(){
		
		var title = $("#txtSurveyName").val();
		
		if(title == ''){
			$("#errSurveyTitle").show();
			return false;
		}else{
			$("#errSurveyTitle").hide();
			return true;
		}
	}
	
	function validateForm(){
		if(isValidTitle()){
			$("#CustomizeTemplateForm").submit();
		}else{
			return false;
		}
	}
</script>

<script type="text/javascript">
	function backToStep(id, step){
		var ParamStr = '?inpbatchid=' + id + '&STEP=' + step + '<cfoutput>#isCreatedParam#</cfoutput>'
		window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editSurvey/' + ParamStr;
	}
	<!--- ajax upload --->
	function deleteImage(){
		type = 'image';
		inFileId = 'inpImgFile';
		rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		var img = $("#deleteImgFile").attr("rel");
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=DeleteSurveyLogo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { inpFile : img},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.RESULT == 'SUCCESS'){
						$('#inpImgServerFile').val("");
						$('#inpImgFile').val("");
						$('#imgPreview').html('');
						$('#imgPreview').empty();
					}
				} 		
			});
		
	}
	function ajaxupload() {
		type = 'image';
		inFileId = 'inpImgFile';
		rootUrl = '<cfoutput>#rootUrl#</cfoutput>';
		sessionPath = '<cfoutput>#sessionPath#</cfoutput>';
		publicPath = '<cfoutput>#PublicPath#</cfoutput>';
		$.ajaxFileUpload
		(
			{
				url: '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ire/survey/act_UploadLogo?inpFileId='+inFileId ,
				secureuri:false,
				fileElementId:inFileId,
				dataType: 'json',				
				complete:function()
				{					
					$("#deleteImgFile").click(function(){
						deleteImage();
					});
				},				
				success: function (data, status)
				{	
					if(type == 'image'){
						
						//alert(data.fileSize);
						//alert(data.clientFileExt);
						if ((data.clientFileExt.toUpperCase() == 'JPG') || (data.clientFileExt.toUpperCase() == 'GIF') || (data.clientFileExt.toUpperCase() == 'PNG')) {
							$('#inpImgServerFile').val(data.UniqueFileName);
							$('#imgPreview').empty();
							$('#imgPreview').append('<img src="' +rootUrl + '/' + sessionPath +'/ire/survey/act_previewImage?serverfileName='+ data.UniqueFileName +'&userId=<cfoutput>#Session.UserId#</cfoutput>" height="65"/>');
							$('#imgPreview').append('<img src="' +rootUrl + '/' + publicPath +'/images/mb/bin_empty.png" style="cursor: pointer" title="remove logo" id="deleteImgFile" rel="'+ data.UniqueFileName +'"/>');
						} else {
							alert('File type not accepted.');
						}
						
					}else{
					}
				},
				error: function (data, status, e)
				{						
					alert('general Error ' + e);	
				}
			}
		);
		return false;
	}
</script>
