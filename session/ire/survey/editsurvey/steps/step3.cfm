<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/utility.css');
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css');
	</style>
</cfoutput>

<div class="label">
	<h1>
		<span>
			<cfoutput>#SURVEY_WELCOME_TITE#</cfoutput>
		</span>
	</h1>
</div>
<div class='msg_box'>
	<cfif StructKeyExists(Session, 'errMsg') and len(Trim(Session.errMsg)) gt 0 >
		<cfoutput>
			<h2 class='err_msg'>
				<span>
					#Trim(Session.errMsg)#
				</span>
			</h2>
		</cfoutput>
	</cfif>
	<cfset Session.errMsg = ''>
</div>
<cfif isCreated eq true>
	<cfset isCreatedParam = "&IsCreated=1">
<cfelse>
	<cfset isCreatedParam = "">
</cfif>
<!---<cfform name="CustomizeWelcome" 
	id="CustomizeWelcome" 
	method="POST" 
	action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=4#isCreatedParam#">--->
	<cfoutput><form 
	name="CustomizeWelcome" 
	id="CustomizeWelcome" 
	method="POST" 
	action="?inpbatchid=#currentSurvey.INPBATCHID#&STEP=4#isCreatedParam#"></cfoutput>
	<div class=" field_padding">
		<!---<cfinput type="hidden" name="currentStep" id="currentStep" value="2" validate="integer" required="true" />
		<cfinput type="hidden" name="INPBATCHID" id="INPBATCHID" value="#currentSurvey.INPBATCHID#" validate="integer" required="true" />--->
		<cfoutput><input
			type="hidden" 
			name="currentStep" 
			id="currentStep" 
			value="2" 
			validate="integer" 
			required="true"
		>
		
		<input
			type="hidden" 
			name="INPBATCHID" 
			id="INPBATCHID" 
			value="#currentSurvey.INPBATCHID#" 
			validate="integer" 
			required="true"
		>
		</cfoutput>
		<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_ONLINE>
			<div class="clear left field_padding voice_label">
				<cfoutput>#SURVEY_LABEL_VOICE#</cfoutput> 
			</div>
	
			<div class="survey_label clear left field_padding non_padding_bottom">
				<label><strong><cfoutput>#SURVEY_LABEL_STARTSURVEY_PROMPT#</cfoutput></strong></label>
				<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_ONLINE>
					<!---<cfinput type="hidden" name="hidWelcomeScriptId" id="hidWelcomeScriptId" value="#currentSurvey.WELCOMESCRIPT#">--->
					<cfoutput><input type="hidden" name="hidWelcomeScriptId" id="hidWelcomeScriptId" value="#currentSurvey.WELCOMESCRIPT#"></cfoutput>
					<a href ="#" class ="voiceRecord thankyouVoiceRecode" QID="1" promptValueControl="hidWelcomeScriptId"> </a>
						<img alt="" id="imgWelcomeRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
						<cfif currentSurvey.WELCOMESCRIPT EQ "">
							display:none;
						</cfif>		
						" scriptId="<cfoutput>#currentSurvey.WELCOMESCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
				</cfif>
			</div>
			<cfif trim(currentSurvey.STARTSURVEYPROMPT) EQ ''>
				<cfset currentSurvey.STARTSURVEYPROMPT = VOICEPROMPT>
			</cfif>
			<cfif trim(currentSurvey.ENDSURVEYPROMPT) EQ ''>
				<cfset currentSurvey.ENDSURVEYPROMPT = VOICEPROMPT>
			</cfif>
			<div class="clear left field_padding non_padding_top">
				<!---<cftextarea 
					name="txtStartPrompt" 
					richtext="false"
					id="txtStartPrompt"
					class="field_margin"
					style="height: 120px; width: 80%;"
					value="#currentSurvey.STARTSURVEYPROMPT#"
					/>--->
				<cfoutput><textarea id="txtStartPrompt" name="txtStartPrompt" style="height: 120px; width: 80%;" class="field_margin"> #currentSurvey.STARTSURVEYPROMPT# </textarea> </cfoutput>
				<label class="error" style="color: red; display: none" id="errSurveyWelcome">
					<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong>
				</label>	
			</div>
			<div class="survey_label clear left field_padding non_padding_bottom">
				<label><strong><cfoutput>#SURVEY_LABEL_ENDSURVEY_PROMPT#</cfoutput></strong></label>
				<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_ONLINE>
					<!---<cfinput type="hidden" id="hidThankYouScriptId" name="hidThankYouScriptId" value="#currentSurvey.THANKYOUSCRIPT#">--->
					<cfoutput><input type="hidden" id="hidThankYouScriptId" name="hidThankYouScriptId" value="#currentSurvey.THANKYOUSCRIPT#"></cfoutput>
					<a href ="#" class ="voiceRecord thankyouVoiceRecode" QID="4" promptValueControl="hidThankYouScriptId"> </a>
					<img alt="" id="imgThankYouRecord" onclick="showPlayer(this)" title="Recording playback" style="cursor: pointer; padding-left: 5px;
					<cfif currentSurvey.THANKYOUSCRIPT EQ "">
					display:none;
					</cfif>		
					"scriptId="<cfoutput>#currentSurvey.THANKYOUSCRIPT#</cfoutput>"width="20" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/mb/speaker.png"/>
					
					
				</cfif>
			</div>
			<div class="clear left field_padding non_padding_top">
				<!---<cftextarea 
					name="txtEndPrompt" 
					richtext="false"
					id="txtEndPrompt"
					class="field_margin"
					style="height: 120px; width: 80%;"
					value="#currentSurvey.ENDSURVEYPROMPT#"
					/>--->
				<cfoutput><textarea id="txtEndPrompt" name="txtEndPrompt" style="height: 120px; width: 80%;" class="field_margin"> #currentSurvey.ENDSURVEYPROMPT# </textarea> </cfoutput>
				<label class="error" style="color: red; display: none" id="errSurveyThanks">
					<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong>
				</label>
			</div>
		</cfif>
		<cfif currentSurvey.COMMUNICATIONTYPE EQ SURVEY_COMMUNICATION_BOTH>
			<div class="survey_Type_separation"></div>
		</cfif>
		<cfif currentSurvey.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_PHONE>
			<!--- Start ONLINE Region --->
			<div class="clear left field_padding voice_label">
				<cfoutput>#SURVEY_LABEL_ONLINE#</cfoutput> 
			</div>
			
			<div class="survey_label clear left field_padding non_padding_bottom">
				<label><strong><cfoutput>#SURVEY_LABEL_WELCOME_TXT#</cfoutput></strong></label>
			</div>
			<div class="clear left field_padding non_padding_top">
				<!---<cftextarea 
					name="txtWelcomeText" 
					richtext="false"
					id="txtWelcomeText"
					class="field_margin"
					style="height: 120px; width: 80%;"
					value="#currentSurvey.WELCOMETEXT#"
					/>--->
				<cfoutput><textarea id="txtWelcomeText" name="txtWelcomeText" style="height: 120px; width: 80%;" class="field_margin"> #currentSurvey.WELCOMETEXT# </textarea> </cfoutput>
				<label class="error" style="color: red; display: none" id="errSurveyWelcome">
					<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong>
				</label>	
			</div>
			<div class="survey_label clear left field_padding non_padding_bottom">
				<label><strong><cfoutput>#SURVEY_LABEL_THANKS_TXT#</cfoutput></strong></label>
			</div>
			<div class="clear left field_padding non_padding_top">
				<!---<cftextarea 
					name="txtThankYouText" 
					richtext="false"
					id="txtThankYouText"
					class="field_margin"
					style="height: 120px; width: 80%;"
					value="#currentSurvey.THANKYOUTEXT#"
					/>--->
				<cfoutput><textarea id="txtThankYouText" name="txtThankYouText" style="height: 120px; width: 80%;" class="field_margin"> #currentSurvey.THANKYOUTEXT# </textarea> </cfoutput>
				<label class="error" style="color: red; display: none" id="errSurveyThanks">
					<strong><cfoutput>#SURVEY_ERR_REQUIRED#</cfoutput></strong>
				</label>
			</div>
		</cfif>
		<!--- End of ONLINE Region --->
		<cfset backStep = 2>
		<cfif currentSurvey.COMMUNICATIONTYPE eq SURVEY_COMMUNICATION_PHONE>
			<cfset backStep = 1>
		</cfif>
		<div class="CreateSurveyButton clear right MarRight50">
			<button id="Back" 
				TYPE="button" 
				class="ui-corner-all" 
				onclick="backToStep('<cfoutput>#currentSurvey.INPBATCHID#</cfoutput>', <cfoutput>#backStep#</cfoutput>)">
					<cfoutput>#SURVEY_LABEL_BACK#</cfoutput>
			</button>
			<!---<cfinput
				type="submit"
				name=""
				class="ui-corner-all"
				value="#SURVEY_LABEL_NEXT#"
				label="#SURVEY_LABEL_NEXT#"
				id="NextToEditSurveyContent"
				 />--->
				 
			 <cfif isCreated eq true>
				<cfoutput><input type="submit"
				name=""
				class="ui-corner-all"
				value="#SURVEY_LABEL_CREATE#"
				label="#SURVEY_LABEL_CREATE#"
				id="NextToEditSurveyContent"></cfoutput>
			<cfelse>
				<cfoutput><input type="submit"
				name=""
				class="ui-corner-all"
				value="#SURVEY_LABEL_UPDATE#"
				label="#SURVEY_LABEL_UPDATE#"
				id="NextToEditSurveyContent"></cfoutput>
			</cfif>
			
		</div>
	</div>
<!---</cfform>--->
</form>
<script type="text/javascript">
	var surveyName =  '<cfoutput>#Replace(currentSurvey.INPBATCHDESC, "'", "\'")#</cfoutput>';
	function backToStep(id, step){
		var ParamStr = '?inpbatchid=' + id + '&STEP=' + step + '<cfoutput>#isCreatedParam#</cfoutput>'
		window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/editSurvey/' + ParamStr;
	}
	<!--- // Validate welcome text
	function isValidWelcomeTxt(){
		var isValidWelcome = false;
		if($("#txtWelcomeText").val() == ''){
			isValidWelcome = false;
			$("#errSurveyWelcome").show();
		}else{
			isValidWelcome = true;
			$("#errSurveyWelcome").hide();
		}
		return isValidWelcome;
	}
	// Validate thanks you text
	function isValidThanksTxt(){
		var isValidThanks = false;
		if($("#txtThankYouText").val()== ''){
			isValidThanks = false;
			$("#errSurveyThanks").show();
		}else{
			isValidThanks = true;
			$("#errSurveyThanks").hide();
		}
		return isValidThanks;
	}
	// validate form
	function validateForm(){
		var isValidWelcome = isValidWelcomeTxt();
		var isValidThanks = isValidThanksTxt();
		if(isValidWelcome && isValidThanks){
			$("#CustomizeWelcome").submit();
		}else{
			return false;
		}
	} --->
	
	//tinyMCE.execCommand('mceAddControl', false, 'txtWelcomeText');
	//tinyMCE.execCommand('mceAddControl', false, 'txtThankYouText');
	
	tinymce.init({
		    selector: "textarea",
		    toolbar: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			editor_selector : "mceEditor",
            editor_deselector : "mceNoEditor"
		});

	
	$("#welcome_voicerecord").click( function() {
		showWelcomeRecorder();
	});
	
	$("#thankyou_voicerecord").click( function() {
		showThankYouRecorder();
	});
	
	$(".thankyouVoiceRecode").click(function() {
  		var QID = $(this).attr('QID');
  		var clientId = $(this).attr('promptValueControl');
  		var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPSURVEYNAME: surveyName,
						INPSECONDLEVELNAME: 'prompts'
					}
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyElementAudioData', data, "Create script error!", function(d ) {
			var url = '<cfoutput>#rootUrl#/#SessionPath#/rxds/flash/recordercontrol</cfoutput>?QID=' + QID + '&clientId=' + clientId + '&<cfoutput>eleId=#SESSION.USERID#_</cfoutput>' + d.DATA.INPLIBID[0] + "_" + d.DATA.NEXTELEID[0];
			ShowVoiceRecordDialog(url);
		});
  	});
  	
  	<!--- function SaveSystemPromptVoice(promptType, scriptId) {
		var data = {
						INPBATCHID : '<cfoutput>#currentSurvey.INPBATCHID#</cfoutput>',
						INPSCRIPTID : scriptId,
						INPPROMPTMCID: 1,
						INDESC: promptType,
					}
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyPromptAudioData', data, "Create script error!", function(d ) {
		});
	} --->
	
	function SaveSystemPromptVoice(QID, scriptId) {
		var desc = ""
		switch (QID) {
			case "1": {
				desc = "Welcome";
				if (scriptId != "") {
					$('#imgWelcomeRecord').show();
					$('#imgWelcomeRecord').attr('scriptId', scriptId)
				}
				else {
					$('#imgWelcomeRecord').hide();
				}
				break;
			}
			case "4": {
				desc = "Thank You";
				
				if (scriptId != "") {
					$('#imgThankYouRecord').show();
					$('#imgThankYouRecord').attr('scriptId', scriptId)
				}
				else {
					$('#imgThankYouRecord').hide();
				}
				break;
			}
			default: {
				break;
			}
		}			
		
		<!--- var data = {
						INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
						INPDESC: desc,
						INPPROMPTMCID: QID,
						INPSCRIPTID: scriptId
					}
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc', 'CreateSurveyPromptAudioData', data, "Create script error!", function(d ) {
		}); --->
	}
	
	function showPlayer(obj) {
		var scriptId = $(obj).attr('scriptId');
		var url = '<cfoutput>#rootUrl#/#SessionPath#/rxds/flash/player</cfoutput>?scriptId=' + scriptId	 ;
		ShowVoiceRecordDialog(url, 'Recording playback', 150);
	}
	
</script>
