var STRONGDISAGREE = "Strong Disagree";
var DISAGREE = "Disagree";
var NEITHERDISAGREEORAGREE = "Neither Disagree or Agree";
var AGREE = "Agree";
var STRONGAGREE = "Strong Agree";
var EXTREMELYLIKELY = "Extremely likely";
var NOTATALLLIKELY = "Not at all likely";
var EXTREMELYSATISFIED = "Extremely satisfied";
var NOTATALLSATISFIED = "Not at all satisfied";
function IsONETOTENANSWERSQuestion(answers, isScoreType, isRecommendation) {
		var numberOfAnswers = isScoreType ? 11 : 10;
		var lowestValue = isScoreType ? 0 : 1;
		if (answers.length != numberOfAnswers) {
			return false;
		}
		
		var highest = isRecommendation ? EXTREMELYLIKELY : EXTREMELYSATISFIED;
		var lowest = isRecommendation ? NOTATALLLIKELY : NOTATALLSATISFIED;
		for (var i = 10; i >= lowestValue; --i) {
			if (i == 10) {
				if ((i + ' ' + highest) != answers[10 - i].TEXT) {
					return false;
				}
			}
			else if (i == lowestValue) {
				if ((i + ' ' + lowest) != answers[10 - i].TEXT) {
					return false;
				}
			}
			else {
				if (i  != answers[10 - i].TEXT) {
					return false;
				}
			}
		}
		return true;
	}
	
	function IsSTRONGAGREEORDISAGREEQuestion(answers) {
		if (answers.length != 5) {
			return false;
		}
		
		if (answers[0].TEXT != STRONGDISAGREE) {
			return false;
		}
		
		if (answers[1].TEXT != DISAGREE) {
			return false;
		}
		
		if (answers[2].TEXT != NEITHERDISAGREEORAGREE) {
			return false;
		}
		
		if (answers[3].TEXT != AGREE) {
			return false;
		}
		
		if (answers[4].TEXT != STRONGAGREE) {
			return false;
		}
		
		return true;
	}
	var EXTREMELYLIKELY = "Extremely likely";
	var NOTATALLLIKELY = "Not at all likely";
	var EXTREMELYSATISFIED = "Extremely satisfied";
	var NOTATALLSATISFIED = "Not at all satisfied";
	function GetAnswerLabel(answer) {
		answer = String(answer);
		if (answer.indexOf(EXTREMELYLIKELY) != -1 || answer.indexOf(NOTATALLLIKELY) != -1 || answer.indexOf(EXTREMELYSATISFIED) != -1 || answer.indexOf(NOTATALLSATISFIED) != -1) {
			return answer.substring(answer.indexOf(' ') + 1) + "</br>" + answer.substring(0, answer.indexOf(' '));
		}
		return answer;
	}