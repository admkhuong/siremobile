<cfparam name="batchId" default="0">

<div style="clear:both"></div>
<div id="messageVirtual"></div>

<div id="jp_container_N" class="jp-video jp-video-270p">
	<div class="jp-type-playlist">
		<div id="jquery_jplayer_N" class="jp-jplayer"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
			</div>
			<div class="jp-interface">
				<div class="jp-progress">
					<div class="jp-seek-bar">
						<div class="jp-play-bar"></div>
					</div>
				</div>
				<div class="jp-current-time"></div>
				<div class="jp-duration"></div>
				<div class="jp-controls-holder">
					<ul class="jp-controls">
						<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
						<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
						<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
						<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
						<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
						<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
						<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
						<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
					</ul>
					<div class="jp-volume-bar">
						<div class="jp-volume-bar-value"></div>
					</div>
					<ul class="jp-toggles">
						<li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
						<li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
						<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
						<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
						<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
						<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
					</ul>
				</div>
				<div class="jp-title">
					<ul>
						<li></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="jp-playlist">
			<ul>
				<!-- The method Playlist.displayPlaylist() uses this unordered list -->
				<li></li>
			</ul>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>

<div id="loadingAjax"></div>

<div class="dialerContainer">
	<div class="RounderBg">
		<div class="dialerRounder">
			<div class="surveyName" id="surveyName"></div>
			<div class="surveyReady"></div>
			<div id="voiceText"></div>
		</div>
		<div class="numberBg">
			<div class="numberCell" id="numberCell1" alt='1'>
				<div class="numberDial">1</div>
			</div>
			<div class="numberCell" id="numberCell2" alt='2'>
				<div class="numberDial">2</div>
				<div class="alphabetDial">ABC</div>
			</div>
			<div class="numberCell" id="numberCell3" alt='3'>
				<div class="numberDial">3</div>
				<div class="alphabetDial">DEF</div>
			</div>
		</div>
		<div class="numberBg">
			<div class="numberCell" id="numberCell4" alt='4'>
				<div class="numberDial">4</div>
				<div class="alphabetDial">GHI</div>
			</div>
			<div class="numberCell" id="numberCell5" alt='5'>
				<div class="numberDial">5</div>
				<div class="alphabetDial">JKL</div>
			</div>
			<div class="numberCell" id="numberCell6" alt='6'>
				<div class="numberDial">6</div>
				<div class="alphabetDial">MNO</div>
			</div>
		</div>
		<div class="numberBg">
			<div class="numberCell" id="numberCell7" alt='7'>
				<div class="numberDial">7</div>
				<div class="alphabetDial">PQR</div>
			</div>
			<div class="numberCell" id="numberCell8" alt='8'>
				<div class="numberDial">8</div>
				<div class="alphabetDial">TUV</div>
			</div>
			<div class="numberCell" id="numberCell9" alt='9'>
				<div class="numberDial">9</div>
				<div class="alphabetDial">WXYZ</div>
			</div>
		</div>
		<div class="numberBg">
			<div class="numberCell" id="numberCellStar" alt='*'>
				<div class="numberDial">*</div>
			</div>
			<div class="numberCell" id="numberCell0" alt='0'>
				<div class="numberDial">0</div>
			</div>
			<div class="numberCell" id="numberCell#" alt='#'>
				<div class="numberDial">#</div>
			</div>
		</div>
		
		<div class="voiceDetail">Voice Survey Details</div>
		<div class="quetionPlaying">Question Playing: 4. Number of Options: 10.</div>
		
		<div class="surveyButton startSurvey" onclick ="startSurvey();">Start Survey</div>
		<div class="surveyButton stopSurvey" onclick="stopSurvey();">Stop Survey</div>
		<div class="surveyButton restartSurvey" onclick ="restartSurvey();">Restart Survey</div>
		
		<div class="sliderContailer">
			<div class="micIcon"></div>
			<div class ="voiceSlider" id="micSlider"></div>
		</div>
		
		<div style="clear:both"></div>
		
		<div class="sliderContailer">
			<div class="volumeIcon"></div>
			<div class ="voiceSlider" id="volumeSlider"></div>
		</div>
		
	</div>
</div>

<link href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/survey/previewVoice.css" rel="stylesheet">
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/jPlayer/js/jquery.jplayer.min.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/jPlayer/js/jplayer.playlist.min.js"></script>
<link href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/jPlayer/skin/blue.monday/jplayer.blue.monday.css" rel="stylesheet">

<cfinclude template="previewVoiceJs.cfm">