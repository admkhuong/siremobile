
<script type="text/javascript">
	
	var questionArr;
	var errorStr;
	var invalidStr;
	var configStr;
	var welcomeStr;
	var thankYouStr;
	var questionNumber = -1;
	var optionNumber = 0;
	var betweenKeyPress = 0;
	var numberOfRepeat = 0;
	var messageVirtualText = '';
	var voiceText ='';
	var keyTextGTTen = '';
	var setTimeOutFn;
	var startSurveyStatus = false;
	var surveyFinishStatus = false;
	var isLoadingAjax = false;
	var numberOfInvalid = 0;
	var numberSecondOfNoResponse = 5000;
	var numberOfNoResponse =0;
	var noResponseTimeOut;
	var currentVoiceText ='';
	var stopClicked = false;
	var endCount = 0;
	var myPlaylist;
	var listFile = [];
	var isPress = false;
	var dtmfTone = "";
	var mouseDownTime;
	var keyChar ='';

	var VOICEANSWERCOOKIE = "VOICEANSWERCOOKIE";
	var voiceAnswers = $.cookie(VOICEANSWERCOOKIE);

	$(document).ready(function(){
		
		$('#subTitleText').text('<cfoutput>#Survey_Title#</cfoutput> >> Preview Voice Survey');
		$('#mainTitleText').text('ire');
		addLoadingAjaxFn();
		
		$.ajax({
			type:"POST",
			async:false,
			url:"<cfoutput>/#sessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=ReadXMLVOICE&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				BATCHID : <cfoutput>#batchId#</cfoutput>
			},
			dataType: "json", 
	       	success: function(d2) {
	       		if(d2.DATA.RXRESULTCODE > 0){
					questionArr = d2.DATA.ARRAYQUESTION[0];
					errorStr = d2.DATA.ARRAYERROR[0];
					invalidStr = d2.DATA.ARRAYINVALID[0];
					configStr =  d2.DATA.CONFIG[0];
					welcomeStr =  d2.DATA.WELCOME[0];
					thankYouStr =  d2.DATA.THANKYOU[0];
					$('#surveyName').html(configStr.SURVEYNAME);
					$('#messageVirtual').html(welcomeStr.voiceText);
					welcomeFile = welcomeStr.AUDIOFILEPATH;
					
					$.ajax({
						type: "POST",
						url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/textToSpeech.cfc?method=SaveTextToAudio&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType: 'text',
						data:  { 
							inpText : RXdecodeXML(welcomeStr.VOICETEXT),
							inpBatchid : '<cfoutput>#batchId#</cfoutput>',
							inpVoice: RXdecodeXML(configStr.VOICE)
						},					  
						success: function(d) 
						{
							
							if(welcomeStr.AUDIOFILEPATH == ''){
								welcomeFile = d
							} 	
							
							currentVoiceText = invalidStr.DESC;
							myPlaylist = new jPlayerPlaylist({
								jPlayer: "#jquery_jplayer_N",
								cssSelectorAncestor: "#jp_container_N"
							}, 
							[
								{
									mp3: welcomeFile
								}
							], 
							{
								playlistOptions: {
									autoPlay: true
								},
								ended: function(){
									stopClicked = false;
									if(questionNumber == -1){
										startSurvey();
									}
									endCount ++;
									if($('.jp-playlist ul li:last-child').attr('class') == 'jp-playlist-current' && endCount >= $('.jp-playlist ul li').length ){
										if(typeof(questionArr[questionNumber]) != 'undefined' 
<!--- 											&& questionArr[questionNumber].QUESTION_TYPE != 'rxt3' 
											&& questionArr[questionNumber].QUESTION_TYPE != 'BOTHrxt3' --->
											&& currentVoiceText != invalidStr.DESC
										){
											noResponseTimeOutFn();
										}
										endCount = 0;
									}
								},
								swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
								supplied: "mp3"
							});
							
						}	
					});
					
					//myPlaylist.option("volume", $( "#volumeSlider" ).slider( "value" ));
					
					removeLoadingAjaxFn();
					
					$("#volumeSlider").slider({
						value: 60,
						slide: function( event, ui ) {
							myPlaylist.option("volume", ui.value/100);
			            }
					});
					
					$("#micSlider").slider({
						value: 50
					});
					
	       		}else{
	       			jAlert(d2.DATA.MESSAGE);
	       		}
			}
		});
		
		$(".numberCell").mousedown(function(){
			var d = new Date();
			mouseDownTime = d.getTime(); 
		}).mouseup(function(){
			currentKey = $(this).attr('alt')
			getDtmfTone(currentKey);
			keyboardPress(currentKey);
		});
		
		$(document).keypress(function(e){
			keyChar = String.fromCharCode(e.which);
			if((keyChar >= 0 && keyChar <= 9) || keyChar == '#' || keyChar == '*' ) {
				var d = new Date();
				mouseDownTime = d.getTime();
			}
		}).keyup(function(e){
			keyCharUp = keyChar;
			if((keyChar >= 0 && keyChar <= 9) || keyChar == '#' || keyChar == '*' ){
				keyChar ='';
				getDtmfTone(keyCharUp);
				keyboardPress(keyCharUp);
				keyCharUp ='';
			}
		}); 
		
	});
	
	function getDtmfTone(currentKey){
		var d = new Date();
		mousePressTime = d.getTime() -  mouseDownTime;
		currentDTMF = currentKey;
		
		dtmfType = 'long';
		
		if(mousePressTime <= 1000){
			dtmfType = 'short';
		}
		
		
		if(currentKey == '*'){
			currentDTMF = 'star';
		}else if(currentKey == '#'){
			currentDTMF = 'pound';
		}
		
		dtmfTone = "<cfoutput>#rootUrl#/#publicPath#/files/dtmfTone/"+dtmfType+"/DTMF-" + currentDTMF + ".mp3</cfoutput>";
	}
	
	function timeToSeconds(time) {
	    time = time.split(/:/);
	    if(time.length == 2){
	    	second = time[0] * 60 + time[1];
	    }else if(time.length == 3){
	    	second = time[0] * 3600 + time[1] * 60 + time[2];
	    }
	    return second;
	}
	
	function playJPlayer(text,path){
		var voice = configStr.VOICE;
		clearTimeout(noResponseTimeOut);
		
		$.ajax({
			type: "POST",
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/textToSpeech.cfc?method=SaveTextToAudio&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'text',
			data:  { 
				inpText : RXdecodeXML(text),
				inpBatchid : '<cfoutput>#batchId#</cfoutput>',
				inpVoice: voice
			},					  
			success: function(d3) 
			{
				currentVoiceText = text;
				listFile = [];
				if(isPress){
					listFile = [
						{
							mp3 : dtmfTone
						}
					];
				}
				if(path != ''){
					listFile.push(
						{
							mp3 : path
						}
					);
				}
					
				if(text != ''){
					listFile.push(
						{
							mp3 : d3
						}
					);
				}
				isPress = false;
				myPlaylist.setPlaylist(listFile);
			} 		
		});
		
	}
	
	function validKeyboardPressNotFinish(keyText){
		var isValid = true;
		if(isLoadingAjax){
			isValid = false;
		}
		if(surveyFinishStatus){
			printMessage('Survey finished. Please click Start Survey button', keyText, '');
			isValid = false;
		}
		
		if(!startSurveyStatus && questionNumber > -1){
			printMessage('Please click Start Survey button', keyText, '');
			isValid = false;
		}
		
		return isValid;
	}
	
	function validKeyboardPress(keyText){
		var isValid = validKeyboardPressNotFinish(keyText);
		if(questionArr.length <=  questionNumber + 1){
			printMessage(thankYouStr.VOICETEXT, keyText, thankYouStr.AUDIOFILEPATH);
			surveyFinishStatus = true;
			isValid = false;
		}
		return isValid;
	}
	
	function printMessage(text, key, path){
		messageVirtualText = text  + '<br/>' + messageVirtualText;
		$('#messageVirtual').html(messageVirtualText);
		playJPlayer(text, path);
		voiceText = '';
		if(key != ''){
			
			voiceText += "" + key;
			
			if(voiceText.length > 13){
				voiceText.substring(voiceText.length - 14, voiceText.length-1);
			}
			$('#voiceText').html(voiceText);
		}
	}

	function keyboardPress(keyText){
		isPress = true;
		clearTimeout(noResponseTimeOut);
		
		var isValidKey = validKeyboardPressNotFinish(keyText);
		if(!isValidKey)
			return false; 
		if(keyText == '*'){
			clearTimeout(setTimeOutFn);
			if(questionNumber < 0){
				printMessage(RXdecodeXML(configStr.WT), '*', '');
			}else{
				printMessage(questionArr[questionNumber].VOICETEXT, keyText, questionArr[questionNumber].AUDIOFILEPATH);
			}
			return false;
		}
		
		if(questionNumber >= questionArr.length){
			printMessage('Please click Start Survey button', keyText, '');
			startSurveyStatus = false;
			questionNumber = -1;
			numberOfInvalid = 0;
			numberOfNoResponse = 0;
		}else{
			if(questionArr[questionNumber].QUESTION_TYPE == 'rxt3' || questionArr[questionNumber].QUESTION_TYPE == 'BOTHrxt3'){
				if(keyText == '#'){
					if(questionArr.length <=  questionNumber + 1){
						printMessage(thankYouStr.VOICETEXT, keyText, thankYouStr.AUDIOFILEPATH);
						surveyFinishStatus = true;
						return false;
					}
					
					addAnswerToLibrary(questionArr[questionNumber].QID, questionArr[questionNumber].QUESTION_TYPE, '');
					numberOfNoResponse = 0;
					questionNumber ++;
					printMessage(questionArr[questionNumber].VOICETEXT, keyText, questionArr[questionNumber].AUDIOFILEPATH);
				}else{
					printMessage(invalidStr.VOICETEXT, keyText, invalidStr.AUDIOFILEPATH);
				}	
			}else{
				if(questionArr[questionNumber].QUESTION_ANS_LIST.length >= 10){
					if(keyTextGTTen == '' || keyTextGTTen >= 10){
						keyTextGTTen = keyText;
					}else{
						keyTextGTTen += ''+ keyText;
					}
					
					if(keyTextGTTen >= 10){
						clearTimeout(setTimeOutFn);
						notShortAnswerKeyPress(keyTextGTTen);
					}else{
						setTimeOutFn = setTimeout(
							function(){
								notShortAnswerKeyPress(keyTextGTTen);
							} , 
							questionArr[questionNumber].NUMBEROFSECONDS * 1000 
						);
					}
				}else{
					notShortAnswerKeyPress(keyText);
				}
			}
		}
	}
	
	function noResponseTimeOutFn() {
		if (numberOfNoResponse >= 5){
			addAnswerToLibrary(questionArr[questionNumber].QID, questionArr[questionNumber].QUESTION_TYPE, '-1');
			numberOfNoResponse = 0;
			clearTimeout(noResponseTimeOut);
			goNextQuestion('');
			questionNumber ++;
			if(typeof(questionArr[questionNumber]) != 'undefined'){
				printMessage(questionArr[questionNumber].VOICETEXT, '', '');
			}
			return false;
		}
		numberOfNoResponse ++;
		if (typeof(questionArr[questionNumber]) != 'undefined'){
			if (questionArr[questionNumber].QUESTION_TYPE != 'rxt3' && questionArr[questionNumber].QUESTION_TYPE != 'BOTHrxt3') {
				if(startSurveyStatus && questionArr[questionNumber].ISERROR){
					noResponseTimeOut = setTimeout(
						function(){
							printMessage(errorStr.VOICETEXT, '', errorStr.AUDIOFILEPATH);
						} , 
						questionArr[questionNumber].NUMBEROFSECONDSFORNORESPONSE * 1000
					);	
				}
			}
			else {
				if (numberOfNoResponse != 2) {
					noResponseTimeOut = setTimeout(
						function(){
							printMessage(errorStr.VOICETEXT, '', errorStr.AUDIOFILEPATH);
						} , 
						3 * 1000
					);
				}
				else {
					noResponseTimeOut = setTimeout(
						function(){
							printMessage("Warning - no microphone was detected so we are unable to record response. Accept this recording to move on in this sample survey", '', "");
						} , 
						3 * 1000
					);								
				}
			}
			
		} 
	}
	
	function goNextQuestion(keyText){
		var isValidKey = validKeyboardPress(keyText);
		if(!isValidKey)
			return false;
		if(questionArr.length <=  questionNumber + 1){
			console.log(thankYouStr.VOICETEXT, keyText, thankYouStr.AUDIOFILEPATH);
			printMessage(thankYouStr.VOICETEXT, keyText, thankYouStr.AUDIOFILEPATH);
			surveyFinishStatus = true;
			numberOfInvalid = 0;
			return false;
		}
	}
	
	function notShortAnswerKeyPress(keyText){
		if(jQuery.inArray(parseInt(keyText), questionArr[questionNumber].ARRQ)>=0 ){
			goNextQuestion(keyText);
			<!--- save answer of this question --->
			var question = questionArr[questionNumber];

			var answerId = getAnswerByKey(keyText, question.VOICETEXT)
			addAnswerToLibrary(question.QID, question.QUESTION_TYPE, answerId);
			numberOfNoResponse = 0;
			questionNumber ++;
			if(typeof(questionArr[questionNumber]) != 'undefined'){
				printMessage(questionArr[questionNumber].VOICETEXT, keyText, questionArr[questionNumber].AUDIOFILEPATH);
			}
			
			keyTextGTTen ='';
			numberOfInvalid = 0;
			return false;
		}else{
			numberOfInvalid ++;		
			if(numberOfInvalid >= questionArr[questionNumber].NUMBEROFREPEATS){
				
				goNextQuestion(keyText);
				numberOfNoResponse = 0;
				questionNumber ++;
				if(typeof(questionArr[questionNumber]) != 'undefined'){
					printMessage(questionArr[questionNumber].VOICETEXT, keyText, questionArr[questionNumber].AUDIOFILEPATH);
				}
				numberOfInvalid = 0;
				return false;
			}
			if(questionArr[questionNumber].ISINVALID){
				printMessage(invalidStr.VOICETEXT, keyText, '');
			}
		}
	}
	
	function startSurvey(){
		if(isLoadingAjax){
			return false;
		}
		if(surveyFinishStatus){
			questionNumber = -1;
			numberOfInvalid = 0;
			numberOfNoResponse = 0;
			surveyFinishStatus = false;
		}
		if(questionNumber == -1){
			printMessage(questionArr[0].VOICETEXT, '', questionArr[0].AUDIOFILEPATH);
			numberOfNoResponse = 0;
			questionNumber ++;
		}else{
			printMessage(questionArr[questionNumber].VOICETEXT, '', questionArr[questionNumber].AUDIOFILEPATH);
		}
		startSurveyStatus = true;
	}
	
	function stopSurvey(){
		if(isLoadingAjax){
			return false;
		}
		myPlaylist.pause();
		clearTimeout(noResponseTimeOut);
		startSurveyStatus = false;
	}
	
	function restartSurvey(){
		if(isLoadingAjax){
			return false;
		}
		surveyFinishStatus = false;
		startSurveyStatus = true;
		questionNumber = 0;
		numberOfInvalid = 0;
		numberOfNoResponse = 0;
		$('#messageVirtual').html(questionArr[0].VOICETEXT, '');
		playJPlayer(questionArr[0].VOICETEXT, questionArr[0].AUDIOFILEPATH);
		voiceText = '';
		messageVirtualText= '';
		$('#voiceText').html('');
		keyTextGTTen ='';
	}
	
	function addLoadingAjaxFn(){
		isLoadingAjax = true;
		$('#loadingAjax').addClass('loadingAjax');
		$('.surveyReady').html('');
	}
	
	function removeLoadingAjaxFn(){
		isLoadingAjax = false;
		$('#loadingAjax').removeClass('loadingAjax');
		$('.surveyReady').html('Ready');
	}
	
	function addAnswerToLibrary(QuestionID, QuestionType, AnswerChoices) {
		var RT = 3;
		if (voiceAnswers == undefined || voiceAnswers == null) {
			voiceAnswers = "";	
		}
		var answer = generateXmlBaseQuestionType(QuestionID, RT, QuestionType, AnswerChoices);
		voiceAnswers += answer;
		$.cookie(VOICEANSWERCOOKIE, voiceAnswers, { path : '/' })
	}
	function generateXmlBaseQuestionType(QuestionID, RT, QuestionType, AnswerChoices) {
		var xmlQuestion = '';
	
		if (QuestionType.indexOf('rxt3') == -1) {
			xmlQuestion += "<Q BATCHID='<cfoutput>#batchId#</cfoutput>' ID='" + QuestionID + "' RT='" + RT + "' CP='1'>" + AnswerChoices + "</Q>";				
		}
		else {
			xmlQuestion += "<Q BATCHID='<cfoutput>#batchId#</cfoutput>' ID='" + QuestionID + "' RT='" + RT + "' CP='3'></Q>"; 			
		}
		return xmlQuestion;
	}	
	
	function getAnswerByKey(keyPress, question) {
		var result = '';
		var position = question.indexOf('Press ' + keyPress + ' ');
		for (var i = position; i < question.length; ++i) {
			if (question[i] == ',') {
				result = question.substring(position + 12, i);
				break;		
			}
			else if (i == question.length - 1) {
				result = question.substring(position + 12, i + 1);
				break;
			}
		}
		return result;
		
	}
</script>