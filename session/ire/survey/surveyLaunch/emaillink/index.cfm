<cfparam name="INPBATCHID" type="string" default="0">
<cfparam name="selectedGroup" default="-1">
<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfoutput>
	<script src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js" type="text/javascript"></script>
</cfoutput>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Launch_Survey_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Survey_Email_Title#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.Multilists" method="GetGroupData" returnvariable="GetGroupData">

<cfoutput>
<style>
	@import url('#rootUrl#/#PublicPath#/css/survey/sendEmail.css');
</style>
</cfoutput>
<script type="text/javascript">
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Survey_Title#  >> #Launch_Survey_Title#  >>  #Survey_Email_Title#</cfoutput>');
		$('#mainTitleText').text('ire');
		
		$('#groupIdVal').val(0);
		
		$('#addRecipients').click(function(){
			$('#getGroupData').toggle();
			if($('#groupIdVal').val() == 0  ){
				$('#groupIdVal').val(1);
			}else if($('#groupIdVal').val() == 1  ){
				$('#groupIdVal').val(0);
			}
		});
	});
</script>


	<div class="surveyLaunchContent">
		<h1>Send Email Survey</h1>
		<div class="emailBox">
			<div> REMINDER: Before we can send your survey, you need to do the following: </div>
			<ol>
				<li> Add recipients to your list.</li>
				<li> Create a message for us to send.</li>
			</ol>
		</div>
		
		<div style="clear: both"></div>
		<cfform>
			<div class="emailBox">
				<div class="emailBoxTitle">Recipients</div>
				<div>
					<cfoutput>
						<select name="getGroupData" id="getGroupData">
							<option value="0" <cfif selectedGroup EQ 0>selected</cfif>>All Contacts</option>
							<cfloop query="GetGroupData">
								<option value="#GetGroupData.GROUPID#"  <cfif GetGroupData.GROUPID EQ selectedGroup>selected</cfif>>#htmlEditFormat(GetGroupData.GROUPNAME)#</option>
							</cfloop>
						</select>
					</cfoutput>
				</div>
				<div><a href="#" onclick="return CreateNewGroup();" id="createGroupContact">Create group and contacts</a></div>
			</div>	
			
			<div class="emailBox">
				<div class="emailBoxTitle">Email Message</div>
				<div id="emailContent">
					<div class="contentItem">
						<div class="label">From:</div>
						<div class="control">
							<input name="inpEmailFrom" id="inpEmailFrom" type="text"/>
						</div>
					</div>
					<div class="contentItem">
						<div class="label">Subject:</div>
						<div class="control">
							<input name="inpEmailSubject" id="inpEmailSubject" type="text"/>
						</div>
					</div>
					<div class="contentItem">
						<div class="label"></div>
						<div class="control">
							<textarea name="inpEmailContent" id="inpEmailContent">
								<p>We are conducting a survey, and your response would be appreciated.</p>
								<p>Here is a link to the survey: <cfoutput>#rootUrl#/#PublicPath#/surveys/start/?BATCHID=#INPBATCHID#&UUID=REPLACETHISUUID</cfoutput></p>
								<p>This link is uniquely tied to this survey and your email address. Please do not forward this message.</p>
								<p>Thanks for your participation!</p>
							</textarea>
						</div>
					</div>
					
					<div class="contentItemText">
						<div class="bold">
							Interested in further personalizing your message?
						</div>
						<div class="normal">
							We support four custom tags to allow you to personalize your message, similar to mail merge in a word processing application, Just add one of the following tags to the subject or body of your message, and we will replace the tag with the actual text when the message is sent
						</div>
					</div>
					
					<div class="contentItemText">
						<div class="normal">
							[FirstName] We will replace this tag with the recipient's first name.
						</div>
						<div class="normal">
							[LastName] We will replace this tag with the recipient's last name. 
						</div>
						<div class="normal">
							[Email] We will replace this tag with the recipient's actual email address. 
						</div>
						<div class="normal">
							[customdata] We will replace this tag with the text that you have stored in that recipient's custom value field. 
						</div>
					</div>
				</div>
			</div>
			<div style="clear: both"></div>
			<div class="buttonBar">
				<input type="button" value="Save" class="save">
				<input type="button" value="Cancel" class="cancel">
				<input type="button" value="Send" class="send">
			</div>
		</cfform>
		
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			
//			tinyMCE.init({
//			 mode : "none",
//			 theme : "simple",
//			 width: "800px",
//			 height: "250px",
//			 visual : false,
//			 convert_urls : false,
//			 mode: "none",
//			 theme : "advanced",
//			 theme_advanced_toolbar_location : "bottom",		 
//			 setup : function(ed) { 
//			   ed.onInit.add(function(ed) { 
//			   		var dom = ed.dom;
//		            var doc = ed.getDoc();
//		        	
//		        	<!--- doc.addEventListener('blur', function(e) {
//		                content = tinyMCE.activeEditor.getContent();
//		            }, false);
//		            
//		            doc.addEventListener('click', function(e) {
//		            	content = tinyMCE.activeEditor.getContent();
//		            	if(content == "<p>Please enter the text to be display</p>"){
//			                tinyMCE.activeEditor.setContent("");
//		            	}
//		            }, false); --->
//			   }); 
//			 }
//		    });
//					
//		    tinyMCE.execCommand('mceAddControl', false, 'inpEmailContent');

		tinymce.init({
		    selector: "textarea",
		    toolbar: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			width:"81%"
		});
		    
		    $(".send").click(function (){ 
				sendMail('groups');
			}); 
			
			$(".save").click(function (){ 
				SaveEmailTemplate();
			}); 
			
			$('.cancel').click(function(){
				if (document.referrer == "") {
				    window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/surveylist/";
				} else {
				    history.back();
				}
			});
		});
		
		function sendMail(type) {
			var data = { 
						TYPE : type,
						SUBJECT : $("#inpEmailSubject").val(),
						SENDER : $("#inpEmailFrom").val(),
						EMAILLIST : "",
						CONTENT : RXencodeXML(tinyMCE.activeEditor.getContent()),
						GROUPID : $('#getGroupData').val(),
						INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput>			
						};
						
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc', 'sendMail', data, "Email has NOT sent", function(d) {
				jAlert("Sent Successfully");
			}); 
		}
		
		function SaveEmailTemplate() {
			var from = $('#inpEmailFrom').val();
			var subject = $('#inpEmailSubject').val();
			var body = RXencodeXML(tinyMCE.activeEditor.getContent());
			var INPMCIDXML = "<DM BS='0' DSUID='<cfoutput>#SESSION.UserID#</cfoutput>' DESC='Description Not Specified' LIB='0' MT='3' PT='12'><ELE QID='1' DESC='Description Not Specified' RXT='13' BS='0' DSUID='10' CK1='0' CK2='' CK3='' CK4='" + body + "' CK5='" + from + "' CK6='' CK7='' CK8='" + subject + "' CK9='' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' X='0' Y='0' LINK='0'>0</ELE></DM>";
			var data = { 
						INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput>,
						INPCOMMTYPE : 3,
						inpXPOS : 200,
						inpYPOS : 50,
						maxCOMMSID : 1,
						INPMCIDXML : INPMCIDXML			
						};
			
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc', 'AddNewCOMMID', data, "Email Template has not saved", function() {
				jAlert("Saved Successfully");
			}); 
		}
		
		function CreateNewGroup() {
			<cfoutput>
				var params = {
					'INPBATCHID':'#INPBATCHID#',
					selectedGroup:$('##getGroupData').val(),
					page: 'emailLink'
				};
				post_to_url('#rootUrl#/#sessionPath#/contacts/addgroup', params, 'POST');
			</cfoutput>			
			
			return false;
		}
	</script>
