<cfinclude template="../../constants/surveyConstants.cfm">
<cfparam name="INPBATCHID" type="string" default="0">
<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Launch_Survey_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>
<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message />
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory"><cfinvokeargument name="pageTitle" value="#Launch_Survey_Title#"></cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools" method="GetBasicSurveyInfo" returnvariable="getSurveyInfo"><cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"></cfinvoke>
<style>
	@import url( 
	<cfoutput>'#rootUrl#/#PublicPath#</cfoutput>
	/css/survey/surveyLaunch.css'); 
</style>
<script>
	function surveyNextStep(){
		var surveyLinkVal = $('input[name=surveyLink]:checked').val();
		if(surveyLinkVal ==1){
					
		<cfoutput>
			var params = { 'INPBATCHID':'#INPBATCHID#' }; post_to_url('#rootUrl#/#sessionPath#/ire/survey/surveyLaunch/weblink/', params, 'POST'); 
		</cfoutput>
		}
		else if (surveyLinkVal == 2) {
		<cfoutput>
			var params = { 'INPBATCHID':'#INPBATCHID#' }; post_to_url('#rootUrl#/#sessionPath#/ire/survey/surveyLaunch/emailLink/', params, 'POST'); 
		</cfoutput>
		}
		else if (surveyLinkVal == 3) { 
		<cfoutput>
			var params = { 'INPBATCHID':'#INPBATCHID#' }; post_to_url('#rootUrl#/#sessionPath#/ire/survey/surveyLaunch/website/', params, 'POST'); 
		</cfoutput>
		}
		else if (surveyLinkVal == 4) { 
		<cfoutput>
			var params = { 'INPBATCHID':'#INPBATCHID#' }; post_to_url('#rootUrl#/#sessionPath#/ire/survey/surveyLaunch/facebook/', params, 'POST'); 
		</cfoutput>
		}
	}
	$('#subTitleText').text('<cfoutput>#Survey_Title# >> #Launch_Survey_Title#</cfoutput>'); 
	$('#mainTitleText').text('ire'); 
</script> 

<cfif getSurveyInfo.COMMUNICATIONTYPE NEQ SURVEY_COMMUNICATION_PHONE>
	<div class="surveyLaunchContent">
		<cfform id="surveyInfo">
			<div class="surveyLaunchRow">
				<cfinput type="radio" name="surveyLink" value="1" id="surveyLink_weblink" checked="checked" />
				<label for="surveyLink_weblink">Web Link</label>
				<br/>
				<span>Create a Web Link to send via email or post to your website.</span>
				<br/>
			</div>
			<!--- <div class="surveyLaunchRow">
				<cfinput type="radio" name="surveyLink" value="2" id="surveyLink_email"> 
				<label for="surveyLink_email">Email</label><br/>
				<span>Create custom email invitations and track who response in your list.</span><br/>
				</div> --->
			<div class="surveyLaunchRow">
				<cfinput type="radio" name="surveyLink" value="3" id="surveyLink_website" />
				<label for="surveyLink_website">Website</label>
				<br/>
				<span>
					Embed your survey on your website or display your survey in a popup window. 
				</span>
				<br/>
			</div>
			<div class="surveyLaunchRow">
				<cfinput type="radio" name="surveyLink" value="4" id="surveyLink_facebook" />
				<label for="surveyLink_facebook">Share on Facebook</label>
				<br/>
				<span>
					Post your survey on your Facebook Wall or friends, or embed on your Page. 
				</span>
				<br/>
			</div>
			<div class="surveyLaunchRow">
				<cfinput type="button" value="Next Step" name="nextStep" id="nextStep" onclick ="surveyNextStep()" />
			</div>
		</cfform>
	</div>
</cfif>
