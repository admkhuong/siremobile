<cfparam name="INPBATCHID" type="string" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Launch_Survey_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Survey_Web_Link_Title#">
</cfinvoke>

<cfquery name="GetUUID" datasource="#Session.DBSourceEBM#">
	SELECT UUID() as UUID
</cfquery>
<cfoutput>
<style>
	@import url('#rootUrl#/#PublicPath#/css/survey/surveyLaunch.css');
</style>

<div class="surveyLaunchContent">
	<h1>Your Survey Web Link</h1>
	<cfform>
		<div class="surveyLaunchRow">
			<p>Copy, paste and email the web link below to your audience</p>
			<p class="weblinkText">
				<input type="button" value="Select code" onclick="javascript:txtOnly.focus();txtOnly.select();" ><br/>
				<textarea name="txtOnly" cols="100" rows="1">#rootUrl#/#PublicPath#/surveys/start/?BATCHID=#INPBATCHID#&UUID=#GetUUID.UUID#</textArea>
			</p>	
		</div>	
		<div class="surveyLaunchRow">
			<p>Copy and paste the HTML code below to add your Web Link to any webpage:</p>
			<p class="weblinkLink"> 
				<input type="button" value="Select code" onclick="txtHTML.focus();txtHTML.select();" ><br/>
				<textarea name="txtHTML" cols="100" rows="1">&lt;a href="#rootUrl#/#PublicPath#/surveys/start/?BATCHID=#INPBATCHID#&UUID=#GetUUID.UUID#"&gt; Click here to take survey &lt;/a &gt;</textArea>
			</p>
		</div>	
	</cfform>
</div>
</cfOutput>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Survey_Title#  >> #Launch_Survey_Title#  >>  #Survey_Web_Link_Title#</cfoutput>');
	$('#mainTitleText').text('ire');
</script>