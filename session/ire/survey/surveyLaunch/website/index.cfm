<cfparam name="INPBATCHID" type="string" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Launch_Survey_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Survey_Website_Title#">
</cfinvoke>

<cfquery name="GetUUID" datasource="#Session.DBSourceEBM#">
	SELECT UUID() as UUID
</cfquery>

<cfoutput>
<style>
	@import url('#rootUrl#/#PublicPath#/css/survey/surveyLaunch.css');
</style>
</cfoutput>
<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/mcid/jquery.numbericbox.js"></script>
<script type="text/javascript">
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Survey_Title#  >> #Launch_Survey_Title#  >>  #Survey_Website_Title#</cfoutput>');
		$('#mainTitleText').text('ire');
		
		$('#surveyWidth').ForceNumericOnly();
		$('#surveyHeight').ForceNumericOnly();
		
		$('#createWebSite').click(function(){
			var width = $('#surveyWidth').val();
			var height = $('#surveyHeight').val();
			
			if(width <300){
				alert('Survey width cannot less than 300!');
				return false;
			}
			if(width > 1000){
				alert('Survey width cannot greater than 300!');
				return false;
			}
			
			if(height <300){
				alert('Survey height cannot less than 300!');
				return false;
			}
			if(height > 1000){
				alert('Survey height cannot greater than 300!');
				return false;
			}
			
			var frameSrc = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/surveys/start/?BATCHID=<cfoutput>#INPBATCHID#</cfoutput>&UUID=<cfoutput>#GetUUID.UUID#</cfoutput>';
			
			var frameText = 'src="'+frameSrc+'" name="Online Survey" scrolling="auto"  align="center" height = "'+height+'px" width="'+width+'px"';
			
			if($('#showBorder').is(':checked')){
				frameText += 'frameborder="yes"';
			}
			
			$('#websiteContent').html('&lt;iframe '+frameText+'&gt;&lt;/iframe&gt;');
			$('#websiteContent').show();
		});	
	});
</script>

<cfoutput>	
	<div class="surveyLaunchContent">
		<div class="surveyLaunchRow websiteOption">
			<span>Display Options</span>
			<p>Please define how you'd like survey to appear to your website. </p>
		</div>
		<cfform>
			
			<div class="surveyLaunchRow widthHeight">
				<label>Width:</label>
				<cfinput type="text" size="2" value="500" name="surveyWidth" id="surveyWidth">
				<span>(required: number 300-1000 )</span>
			</div>	
			<div class="surveyLaunchRow widthHeight">
				<label>Height:</label>
				<cfinput type="text" size="2" value="350" name="surveyHeight" id="surveyHeight">
				<span>(required: number 300-1000 )</span>
			</div>	
			<div class="surveyLaunchRow surveyOption">
				<p> 
					<cfinput type="checkbox" name="showBorder" id = "showBorder">
					<label for="showBorder">Show border</label>
				</p>
			</div>
			<div class="surveyLaunchRow ">
				<cfinput type="button" name="create" value="create" id="createWebSite">
			</div>
			<div class="surveyLaunchRow" id="websiteContent" style="display:none"></div>
			
		</cfform>
	</div>
</cfOutput>