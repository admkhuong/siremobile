<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset surveyPermission = permissionObject.havePermission(Survey_Title)>
<cfset surveyEditPermission = permissionObject.havePermission(edit_Survey_Title)>
<cfset surveyQuestionPermission = permissionObject.havePermission(Survey_Question_Title)>
<cfset surveyLaunchPermission = permissionObject.havePermission(Launch_Survey_Title)>
<cfset surveyViewPermission = permissionObject.havePermission(View_Survey_Title)>
<cfset surveyDeletePermission = permissionObject.havePermission(delete_Survey_Title)>

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Survey_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> List</cfoutput>');
	$('#subTitleText').html('<cfoutput></cfoutput>');
</script>


<cfif NOT surveyPermission.havePermission>
	<cfset session.permissionError = surveyPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Survey_Title#">
</cfinvoke>

<cfparam name="page" default="1">

<cfset newCampaign="STARTNEWCAMPAIGN">

<cfimport taglib="../../../lib/grid" prefix="mb" />

<!-- generate survey list using custom table -->
<cfscript>

	htmlLaunchSurvey = '';

	if(surveyLaunchPermission.havePermission){
		htmlLaunchSurvey = htmlLaunchSurvey & "{%SurveyLaunchLink%}";
	}

	htmlPreviewSurvey = '';
	if(surveyViewPermission.havePermission){
		htmlPreviewSurvey = htmlPreviewSurvey & "{%SurveyPreviewLink%}";
	}

	editSurveyLink = "<a href='#rootUrl#/#SessionPath#/ire/survey/editSurvey/?inpbatchid={%SurveyListId_int%}&INPCOMTYPE={%ComType%}'><img class='ListIconLinks img16_16 view_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Edit Survey'/></a>";
	editquestionsLink = "<a href='#rootUrl#/#SessionPath#/ire/survey/editquestions/index?inpbatchid={%SurveyListId_int%}'><img class='ListIconLinks img16_16 question_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Survey Question'/></a>";
	deleteSurveyLink = "<a href='##' onclick='return false;'><img class='del_RowSurveyList ListIconLinks img16_16 delete_16_16' pageRedirect='{%pageRedirect%}' rel='{%SurveyListId_int%}' title='Delete Survey' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' alt='Survey Delete'/></a>";

	htmlOption = '';
	if(surveyEditPermission.havePermission){
		htmlOption = htmlOption & editSurveyLink;
	}
	if(surveyQuestionPermission.havePermission){
		htmlOption = htmlOption & editquestionsLink;
	}
	if(surveyDeletePermission.havePermission){
		htmlOption = htmlOption & deleteSurveyLink;
	}

	htmlLaunchSurveyFormat ={
		name ='normal',
		html = '#htmlLaunchSurvey#'
	};

	htmlPreviewSurveyFormat ={
		name ='normal',
		html = '#htmlPreviewSurvey#'
	};

	htmlFormat = {
		name ='normal',
		html = '#htmlOption#'
	};

</cfscript>


<!-- Set Column names and column models -->
<cfset colNames = ['Campaign ID','Survey Title','Expiration Date']>
<cfset colModels = [
		{name='SurveyListId_int', width="12%",sortObject= {isDefault='false', sortType="ASC", sortColName ='bas.BatchId_bi'}},
		{name='Desc_vch', width="26%", sortObject= {isDefault='false', sortType="ASC", sortColName ='bas.DESC_VCH'}},
		{name='Expire_Dt',width="10%", sortObject= {isDefault='false', sortType="ASC", sortColName ='ExpirationDate_dt'}}
	   ]>
<cfif session.userrole EQ 'SuperUser'>
	<cfset ArrayAppend(colNames, "Owner ID")>
	<cfset ArrayAppend(colModels, {name='UserId_int',index='UserId_int',width="10%", sortObject= {isDefault='false', sortType="ASC", sortColName ='u.UserId_int'}})>
	<cfset ArrayAppend(colNames, "Company ID")>
	<cfset ArrayAppend(colModels, {name='CompanyName_vch',index='CompanyName_vch',width="12%", sortObject= {isDefault='false', sortType="ASC", sortColName ='c.CompanyName_vch'}})>
</cfif>
<cfif htmlOption NEQ '' >
	<cfset arrayAppend(colNames, 'Launch Survey')>
	<cfset arrayAppend(colModels, {name='Options', width="10%", format=[htmlLaunchSurveyFormat]})>

	<cfset arrayAppend(colNames, 'Preview Survey')>
	<cfset arrayAppend(colModels, {name='Options', width="10%", format=[htmlPreviewSurveyFormat]})>

	<cfset arrayAppend(colNames, 'Options')>
	<cfset arrayAppend(colModels, {name='Options', width="auto", format=[htmlFormat]})>
</cfif>

<!-- generate list -->
<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Campaign ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='Survey Title', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Expiration Date', TYPE='CF_SQL_DATE', FILTERTYPE="DATE", EXCEPTIONCASE="Never"}
		]
	>
	<cfset
		filterFields = [
			'bas.BatchId_bi',
			'bas.DESC_VCH',
			"EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')"
		]
	>

	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(filterFields, "u.UserId_int")>
		<cfset ArrayAppend(filterKeys, {VALUE='4', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"})>
		<cfset listValues = [{DISPLAY='Value 1', VALUE="1"}, {DISPLAY='Value 2', VALUE="2"}, {DISPLAY='Value 3', VALUE="3"}]>
		<cfset ArrayAppend(filterFields, "c.CompanyName_vch")>
		<cfset ArrayAppend(filterKeys, {VALUE='5', DISPLAY='Company ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT" })>
	</cfif>
<cfoutput>
	<mb:table
		component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
		method="GetSurveyMCContent"
		colNames="#colNames#"
		colModels="#colModels#"
		page="#page#"
		width="100%"
		name="surveys"
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
		>
	</mb:table>
</cfoutput>

<script type="text/javascript">


	function showSurveyErrorMessage(obj,batchId) {
		$.alerts.okButton = '&nbsp;Ok&nbsp;';
		if ($(obj).attr('comtype') == 'VOICE') {
			<cfoutput>
				var params = {
					'batchId':batchId
				};
				post_to_url('#rootUrl#/#sessionPath#/ire/survey/previewVoice/', params, 'POST');
			</cfoutput>
		}
		else {
			jAlert("You must add at least one question before you can view the survey.");
		}
		return false;
	}

	// Event tracker
	// Delete survey
 	$(".del_RowSurveyList").click( function() {


		$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';

		var CurrREL = $(this).attr('rel');

		var page = $(this).attr('pageRedirect');

		$.ajax({
			type:"POST",
			async: false,
			url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/permission.cfc?method=checkBatchPermissionByBatchId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				INPBATCHID : CurrREL,
				OPERATOR : '<cfoutput>#delete_Survey_Title#</cfoutput>'
			},
			dataType: "json",
			success: function(data) {
				if(data.HAVEPERMISSION){
					jConfirm( "Are you sure you want to delete this Survey?", "Delete Survey", function(result) {
						if(result)
						{
							DelSurveyList(CurrREL, page);
						}
						return false;
					});
				}else{
					$.alerts.okButton = '&nbsp;Ok&nbsp;';
					jAlert(data.MESSAGE);
					return false;
				}
			}
       	});



	} );

	/* Delete survey function */
	function DelSurveyList(INPSurveyLISTID, page)
	{
	//	$("#loadingDlgAddBatchSurveyList").show();

		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		dataType: 'json',
		data:  { INPBATCHID : INPSurveyLISTID ,TYPEBATCH:1},
		error: function(XMLHttpRequest, textStatus, errorThrown) {},
		success:
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->

					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0)
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];

							if(CurrRXResultCode > 0)
							{
								reloadPage();
								return false;

							}
							else
							{
					            $.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Survey has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!");
							}
						}
						else
						{<!--- Invalid structure returned --->

						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}

			}

		});

		return false;
	}
	/* End delete function*/

	function reloadPage(){

		<cfif StructKeyExists(URL, 'page')>
			<cfset pageUrl = "?page=" & URL.page>
		<cfelse>
			<cfset pageUrl = "">
		</cfif>
		var submitLink = "<cfoutput>#rootUrl#/#SessionPath#/ire/survey/surveylist/index#pageUrl#</cfoutput>";
		<!---reIndexFilterID();--->
		$("#filters_content").attr("action", submitLink);
		// Check if dont user filter
		if($("#isClickFilter").val() == 0){
			$("#totalFilter").val(0);
		}
		$("#filters_content").submit();

	}


	<!--- $.ajax({
			type:"POST",
			async: false,
			url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=CreateSurveyQuestionAudioData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				INPBATCHID : 725,
				INPAUDIONAME : '1',
				INPSURVEYNAME: 'survey',
				INDESC: 'ques2',
				INQUESTID: '2'
			},
			dataType: "json",
			success: function(data) {
			}
	}); --->

	<!--- $.ajax({
			type:"POST",
			async: false,
			url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ire/marketingSurveyTools.cfc?method=CreateSurveyAnswerAudioData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				INPBATCHID : 725,
				INPAUDIONAME : '1',
				INPSURVEYNAME: 'survey',
				INDESC: 'ans1',
				INQUESTID: '1'
			},
			dataType: "json",
			success: function(data) {
			}
	}); --->
</script>
