/**
 * 
 */
	$(function () {
		var INPCPPUUID = "<cfoutput>#INPCPPUUID#</cfoutput>";
		LoadCPP(INPCPPUUID);
		loadreporting();
		$('#inp_StartDate').datepicker({
			numberOfMonths : 1,
			showButtonPanel : true,
			dateFormat : 'yy-mm-dd'
		});
		$('#inp_EndDate').datepicker({
			numberOfMonths : 1,
			showButtonPanel : true,
			dateFormat : 'yy-mm-dd'
		});
		
	});
	function LoadCPP(INPCPPUUID) {
		var temp = "";
		$.ajax({
			type : "POST",
			async : false,
			url : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/CPP.cfc?method=GetReportCPP&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data : {
				UUID : INPCPPUUID
			},
			dataType : "json",
			success : function (d) {
				data = d.DATA.CPPDATA[0];
				for (var i = 0; i < data.length; i++) {
					temp += '<tr>';
					temp += '<td>' + (i + 1) + '</td>';
					temp += '<td>' + data[i].GROUP_ID + '</td>';
					temp += '<td>' + data[i].GROUP_NAME + '</td>';
					temp += '<td>' + data[i].COUNT_MCONTACT + '</td>';
					temp += '</tr>';
				}
				$('#tbl_cpp_report').append(temp);
				
			}
		});
	}
	
	function loadreporting() {
		var LastSelBatchesSurveyList = '';
		$("#listReport").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/CPP.cfc?method=GetContactData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
			datatype: "json",
			height: 385,
			colNames:['Id', 'Groups', 'Type', 'Contact String', 'First name', 'Last name', 'Email', 'Updated', 'Access'],
			colModel:[			
				{name:'UniqueCustomer_UUID_vch',index:'UniqueCustomer_UUID_vch', width:100, editable:false, resizable:true},
				{name:'GroupList_vch',index:'GroupList_vch', width:80, editable:true, resizable:true},
				{name:'ContactTypeId_int',index:'ContactTypeId_int',formatter: fmtContactType, width:80, editable:true, resizable:true},
				{name:'ContactString_vch',index:'ContactString_vch', width:100, editable:true, resizable:true},
				{name:'FirstName_vch',index:'FirstName_vch', width:80, editable:true, resizable:true},
				{name:'LastName_vch',index:'LastName_vch', width:80, editable:true, resizable:true},
				{name:'CPPID_vch',index:'CPPID_vch', width:150, editable:true, resizable:false},
				{name:'LastUpdated_dt',index:'LastUpdated_dt', width:90, editable:true, align:'center', resizable:false},
				{name:'LastAccessed_dt',index:'LastAccessed_dt', width:90, editable:true, align:'center', resizable:false}
			],
			rowNum:8,
		   	rowList:[20,4,8,10,20,30],
			mtype: "POST",
			pager: $('#pagerDiv'),
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current Survey Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			sortname: 'UniqueCustomer_UUID_vch',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",	
			multiselect: false,
			onSelectRow: function(UniqueCustomer_UUID_vch){
					if(UniqueCustomer_UUID_vch && UniqueCustomer_UUID_vch!==LastSelBatchesSurveyList){
						$('#listReport').jqGrid('restoreRow',LastSelBatchesSurveyList);
						//$('#ListSurveyList').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEditSurveyList,'', '');
						LastSelBatchesSurveyList=UniqueCustomer_UUID_vch;
					}
				},
			 loadComplete: function(data){ 		
			 	 
					$(".view_ROWSurveyReportTool").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			 		$(".view_ROWSurveyReportTool").click( function() { ViewReportToolDialogSurveyList($(this).attr('rel'),$(this).attr('rel2')); } );			
		
					<!--- Add a filter row--->
					
					var listReportSearchFilterRow = ''+
					'<tr class="ui-widget-content jqgrow ui-row-ltr" role="row" id="listReportSearchFilterRow" aria-selected="true">'+
					'	<td aria-describedby="" style="" role="gridcell"></td>'+
					'	<td aria-describedby="listReport_Desc_vch" title=""  align="left" style="text-align:left;" role="gridcell">'+
					'Descritpion Filter<BR><input type="text" class="ui-corner-all" style="width:240px; display:inline; margin-bottom:3px;" value="' + data.NOTES_MASK + '" id="search_notes">'+
					'	</td>'+
					'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
					'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
					'	<td aria-describedby="" title="" style="display:none;" role="gridcell">&nbsp;</td>'+
					'</tr>'
	
	
					<!--- Append to end of table body - nice and tight to the form - be sure structure is correct--->
					$("#listReport tbody").append(listReportSearchFilterRow);
					
					<!--- Bind all search functionality to new row --->
					$("#listReport #search_notes").unbind();
					$("#listReport #search_notes").keydown(function(event) { doSearchSurveyList(arguments[0]||event,'search_notes') }); 
					
					<!--- Reset the focus to an inline filter box--->
					if(typeof(lastObj_SurveyList) != "undefined" && lastObj_SurveyList != "")
					{					
						<!--- Stupid IE not rendering yet so call again in 10 ms--->
						setTimeout(function() 
						{ 
						
							 var LocalFocus = $("#listReport #" + lastObj_SurveyList);
							 if (LocalFocus.setSelectionRange) 
							 {         
								 //... then use it        
								 //  (Doesn't work in IE)
								 //  Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.  
								   var len = $(LocalFocus).val().length * 2;         
								   LocalFocus.setSelectionRange(len, len);         
							 }        
							 else
	  			          	 {
							     //... otherwise replace the contents with itself         
								 //  (Doesn't work in Google Chrome)     
								 $(LocalFocus).focus(); 
								 var str = $(LocalFocus).val();
								 $(LocalFocus).val("");    
								 $(LocalFocus).val(str);        
						     }         
							 
							 // Scroll to the bottom, in case we're in a tall textarea
							 // (Necessary for Firefox and Google Chrome)
							 LocalFocus.scrollTop = 999999; 
							 
							
							lastObj_SurveyList = undefined;
							$("#listReport #" + lastObj_SurveyList).focus(); 
						}, 10);
					}
				
					
		},	

			<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
	
			  jsonReader: {
				  root: "ROWS", //our data
				  page: "PAGE", //current page
				  total: "TOTAL", //total pages
				  records:"RECORDS", //total records
				  cell: "", //not used
				  id: "0" //will default second column as ID
				  }	
		});
	}
	function fmtContactType(cellvalue, options, rowObject) {
		// format the cellvalue to new format
		var contactType = [
			"Voice", "E-mail", "SMS"
		];
		return contactType[cellvalue - 1];
	}