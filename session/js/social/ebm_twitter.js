
/**
 * Publish to twitter wall.
 * "to" parameter is used for posting to friends twitter. 
 * if "to" is undefined ==> post to current login twitter user friend
 * @param INPBATCHID
 * @param rootUrl
 * @param sessionPath
 * @param publicPath
 * @param to
 */
function ebm_publish_to_twitter_wall(INPBATCHID, rootUrl, sessionPath, publicPath, recipient, jqgridId){
    $.ajax({
       type: "POST",
       async: false,
       url: rootUrl + "/" + sessionPath + "/cfc/mcidtoolsii.cfc?method=GetTwitterContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
	   data: {
		   INPBATCHID : INPBATCHID
	   },
       dataType: "json", 
       success: function(d) {
       		if (parseInt(d.ROWCOUNT) > 0){
           		var twitterObject = d.DATA.TWITTER_OBJ[0];
           		ebm_publish_twitter_object(twitterObject, rootUrl, sessionPath, publicPath, INPBATCHID, recipient, jqgridId);
       		}
		 }
	  });
}	

function ebm_publish_to_twitter_friends_wall(INPBATCHID, rootUrl, sessionPath, publicPath, recipients, jqgridId){
    $.ajax({
       type: "POST",
       async: false,
       url: rootUrl + "/" + sessionPath + "/cfc/mcidtoolsii.cfc?method=GetTwitterContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
	   data: {
		   INPBATCHID : INPBATCHID
	   },
       dataType: "json", 
       success: function(d) {
       		if (parseInt(d.ROWCOUNT) > 0){
           		var twitterObject = d.DATA.TWITTER_OBJ[0];
           		
           		$.each(recipients, function (index, recipient) {
           			ebm_publish_twitter_object(twitterObject, rootUrl, sessionPath, publicPath, INPBATCHID, recipient, jqgridId, recipient.screenName);
           	    });
           		
       		}
		 }
	  });
}	

function ebm_publish_to_twitter_contacts_wall(INPBATCHID, rootUrl, sessionPath, publicPath, recipients, jqgridId){
    $.ajax({
       type: "POST",
       async: false,
       url: rootUrl + "/" + sessionPath + "/cfc/mcidtoolsii.cfc?method=GetTwitterContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
	   data: {
		   INPBATCHID : INPBATCHID
	   },
       dataType: "json", 
       success: function(d) {
       		if (parseInt(d.ROWCOUNT) > 0){
           		var twitterObject = d.DATA.TWITTER_OBJ[0];
           		
           		$.each(recipients, function (index, recipient) {
           			ebm_publish_twitter_object(twitterObject, rootUrl, sessionPath, publicPath, INPBATCHID, recipient, jqgridId);
           	    });
           		
       		}
		 }
	  });
}	


function ebm_publish_twitter_object(twitterObject, rootUrl, sessionPath, publicPath, batchId, recipient, jqgridId, toFriend){
	var type = twitterObject.TYPE;
	// 
	var publishObject = {};
	if (type == 'image'){
		publishObject = {
			 message: twitterObject.IMGSTATUS,
			 link: twitterObject.IMGSRC,
			 type: 'image',
			 recipient: recipient,
			 batchId: batchId
		};
	}
	if (type == 'text'){
		publishObject = {
			message: twitterObject.TXTSTATUS,
			link: '',
			type: 'text',
			recipient: recipient,
			batchId: batchId
		};
	}
	if (type == 'babble'){
		var audioSrc = babbleSphereDomain + '/public/share/dsp_ShareBabble.cfm?scrId=' + $.base64.encode(twitterObject.BBSCRIPT);
		publishObject = {
			message: twitterObject.BBSTATUS,
			link: audioSrc,
			type: 'babble',
			recipient: recipient,
			batchId: batchId
		};
	}
	if (type == 'audio'){
		var audioSrc = babbleSphereDomain +'/public/share/dsp_ShareBabble.cfm?filename='+ $.base64.encode(twitterObject.AUSCRIPT)+ '&uid=' + userId;
		publishObject = {
			message: twitterObject.AUSTATUS,
			link: audioSrc,
			type: 'babble',
			recipient: recipient,
			batchId: batchId
		};
	}
	publishObject.type = twitterObject.TYPE;
	
	if (toFriend != undefined) {
		publishObject.message = '@' + toFriend + ' ' + publishObject.message;
	}
	
	// publish
	 $.ajax({
	       type: "POST",
	       url: rootUrl + "/" + sessionPath + "/cfc/socialmedia.cfc?method=postToTwitterWall&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		   data: {
			   publishObject : JSON.stringify(publishObject)
		   },
		   async: false,
	       dataType: "json", 
	       success: function(d) {
	       		if (parseInt(d.ROWCOUNT) > 0){
	           		$.jGrowl("Data has been posted to your wall.", { life:2000, position:"center"});
					// Reload grid
					$('#' + jqgridId).trigger("reloadGrid");
	       		}
			 }
		  });
}

