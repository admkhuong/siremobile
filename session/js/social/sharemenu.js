$(document).ready(function(){
	$(".publish_TwitterBatches").click( function() { 
		var $twitterSharingDialog = $('<div id="twitterDialog"></div>');
		$twitterSharingDialog.load(rootUrl + '/' + sessionPath + '/batch/social/dsp_twitter_sharing.cfm?inpbatchid='+ batchId)
			.dialog({
				modal : true,
				title: 'Twitter Sharing',
				width: 600,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close:function(){
					$twitterSharingDialog.remove();
				}
			});
		$('#shareSocialDialog').remove();
	});
});