$(document).ready(function(){
	$("#listTwitterContact").jqGrid({        
		url:rootUrl + '/' + sessionPath + '/cfc/socialmedia.cfc?method=getTwitterContactList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&batchId='+ batchId,
		datatype: "json",
		height: 400,
		colNames:['image','Name', 'Options'],
		colModel:[			
			{name:'image',index:'image', width:100, editable:false},
			{name:'name',index:'name', width:300, editable:false},
			{name:'options',index:'options', width:70, editable:false}
		],
		rowNum:5,
	   	rowList:[5,10,15,30],
		mtype: "POST",
		pager: $('#pagerTwitterContactList'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		viewrecords: true,
		sortorder: "asc",
	 	loadComplete: function(data){
	 		if (data.ISSHOWPUBLISH) {
	 			$('.publishTwitterContactWall').unbind('click');
		 		$('.publishTwitterContactWall').click(function(){
		 			PostToTwitterContactsWall();
		 		});
	 		}
	 		else { 
	 			$('.publishTwitterContactWall').hide();
	 		}
			return false;	 
		},		
		jsonReader: {
		  root: "ROWS", //our data
		  page: "PAGE", //current page
		  total: "TOTAL", //total pages
		  records:"RECORDS", //total records
		  cell: "", //not used
		  id: "0" //will default first column as ID
		  }	
	});
	
});

function PostToTwitterContactsWall() {
	var friends = new Array();
	$.each($("INPUT[name='chkFacebookContact']"), function (index, item) {
		var twitterId = $(item).attr('twitterId');
		var screenName = $(item).val();
		var oauthToken = $(item).attr('oauthtoken');
		var oauthtokensecret = $(item).attr('oauthtokensecret');
		
        if ($(item).attr('checked') == 'checked') {
        	
        	var friend = {
	 				id               : twitterId,
	 				screenName       : screenName,
	 				oauthToken       : oauthToken,
	 				oauthtokensecret :oauthtokensecret,
	 				type             : 'contacts'
	 			};
        	friends.push(friend);
        }
    });
	if (friends.length == 0) {
		jAlert("Please choose contacts to publish.\n", "Failure!", function(result) { return false; } );
		return;
	}
	
	ebm_publish_to_twitter_contacts_wall(batchId, rootUrl, sessionPath, publicPath, friends, 'listTwitterContact');
}