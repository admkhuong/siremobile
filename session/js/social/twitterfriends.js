$(document).ready(function(){
	jQuery("#listTwitterFriend").jqGrid({        
		url:rootUrl + '/' + sessionPath + '/cfc/socialmedia.cfc?method=getTwitterFriendList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&batchId='+ batchId,
		datatype: "json",
		height: 400,
		colNames:['image','Name', 'Options'],
		colModel:[			
			{name:'image',index:'image', width:100, editable:false},
			{name:'name',index:'name', width:300, editable:false},
			{name:'options',index:'options', width:70, editable:false}
		],
		rowNum:5,
	   	rowList:[5,10,15,30],
		mtype: "POST",
		pager: $('#pagerTwitterFriendList'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		viewrecords: true,
		sortorder: "asc",
	 	loadComplete: function(data){
	 		if (data.ISSHOWPUBLISH) {
	 			$('.publishTwitterFriendWall').unbind('click');
		 		$('.publishTwitterFriendWall').click(function(){
		 			PostToFriendsWall();
		 		});
	 		}
	 		else { 
	 			$('.publishTwitterFriendWall').hide();
	 		}
			return false;		 
		},		
		jsonReader: {
		  root: "ROWS", //our data
		  page: "PAGE", //current page
		  total: "TOTAL", //total pages
		  records:"RECORDS", //total records
		  cell: "", //not used
		  id: "0" //will default first column as ID
		  }	
	});
	
});

function PostToFriendsWall() {
	var friends = new Array();
	$.each($("INPUT[type='checkbox']"), function (index, item) {
		var twitterId = $(item).attr('twitterId');
		var screenName = $(item).val();
		
        if ($(item).attr('checked') == 'checked') {
        	
        	var friend = {
	 				id : twitterId,
	 				screenName : screenName,
	 				type: 'friends'
	 			};
        	friends.push(friend);
        }
    });
	if (friends.length == 0) {
		jAlert("Please choose friends to publish.\n", "Failure!", function(result) { return false; } );
		return;
	}
	
	ebm_publish_to_twitter_friends_wall(batchId, rootUrl, sessionPath, publicPath, friends, 'listTwitterFriend');
}