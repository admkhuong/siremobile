function bindData(){
	$("#twitterWall").jqGrid({  
		url: rootUrl + '/' + sessionPath+ '/cfc/socialmedia.cfc?method=getTwitterWallList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&batchId='+ batchId,
		datatype: "json",
		height: 150,
		colNames:['image','Name', 'options'],
		colModel:[			
			{name:'image',index:'image', width:100, editable:false},
			{name:'name',index:'name', width:300, editable:false},
			{name:'options',index:'options', width:100, editable:false}
		],
		rowNum:5,
	   	rowList:[5,10,15,30],
		mtype: "POST",
		pager: $('#pagerTwitterWall'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		viewrecords: true,
		sortorder: "asc",
	 	loadComplete: function(data){
		},		
	  	jsonReader: {
		  root: "ROWS", //our data
		  page: "PAGE", //current page
		  total: "TOTAL", //total pages
		  records:"RECORDS", //total records
		  cell: "", //not used
		  id: "0" //will default first column as ID
	  	}	
	});
}

function publishData(){
	var batchId = $('.publishTwitterWall').attr('batchId');
	var twitterId = $('.publishTwitterWall').attr('rel'); // send message to friend has 
	var recipient = {
		id : twitterId,
		type: 'me'
	};
	ebm_publish_to_twitter_wall(batchId, rootUrl, sessionPath, publicPath, recipient, 'twitterWall');
}