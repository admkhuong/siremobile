<cffunction name="returnAudio" returnType="void" output="yes">
	<cfargument name="file" type="string" required="true" hint="path to file">
	<cfargument name="audioType" type="string" required="true" hint="type of audio">
	<cfset var l = {}>
	<cfset l.request = GetHttpRequestData()>
	
 	<cffile action="readbinary" file="#ARGUMENTS.file#" variable="l.theData">
 	
	<cfset l.size = arrayLen(l.theData)>
	<cfset l.length = l.size>
	<cfset l.start  = 0>
	<cfset l.end = l.size - 1>
	
	<cfheader name="Accept-Ranges" value="bytes">
	
	<cfset l.c_start = l.start>
	<cfset l.c_end = l.end>
	
	<!--- Validate the requested range and return an error if it's not correct. --->
	<cfif l.c_start gt l.c_end || l.c_start gt (l.size - 1) || l.c_end gte l.size>
		<cfheader statusCode = "416" statusText = "Requested Range Not Satisfiable">
		<cfheader name="Content-Range" value="bytes #l.start#-#l.end#/#l.size#">
		<!--- (?) Echo some info to the client? --->
		<cfabort>
	</cfif>
	
	<cfset l.start = l.c_start>
	<cfset l.end = l.c_end>
	<cfset l.length = l.end - l.start + 1><!--- Calculate new content length --->
	
	<cfheader statusCode = "206" statusText = "Partial Content">
	<cfheader name="Content-disposition" value="attachment; filename=PreviewSurvey.#audioType#">
	<cfscript>
	    context = getPageContext();
	    context.setFlushOutput(false);
	    response = context.getResponse().getResponse();
	    response.setContentType("audio/#audioType#");
	    response.setContentLength(l.length);
	</cfscript>
		
	
	<!--- Notify the client the byte range we'll be outputting --->
	<cfheader name="Content-Range" value="bytes #l.start#-#l.end#/#l.size#">
	<!--- <cfheader name="Content-Length" value="#l.length#"> --->
	
 	<cfscript>
		// Start buffered download
		out = response.getOutputStream();
		
		// write the portion requested
		out.write(l.theData, javacast('int', l.start), javacast('int', l.length));
		out.flush();
		out.close();
 	</cfscript>
</cffunction>