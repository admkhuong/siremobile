
<!---
	Check to see which execution mode the tag is running in.
	We won't have access to the child tag data until we are
	in the End tag mode.
--->


<!---

Updrade 2013-05-12 by JLP
attributes.FilterSetId added to all methods
Added surrounding Div to to table

This is so multiple instances of filters and tables can co-exist,
so I can include pickers in jquery dialogs on same page that already has filtered data.

--->

<cfparam name="FORM.TOTALFILTER" default="0">
<cfparam name="FORM.isHideFilter" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="INPGROUPID" default="0">


<cfset StartCommaFlag = 0>

<CFLOOP collection="#FORM#" item="whichField">
	<cfif StartCommaFlag GT 0>
		<cfset FormParams = FormParams & ",#whichField#:'#FORM[whichField]#'">
    <cfelse>
    	<cfset FormParams = "#whichField#:'#FORM[whichField]#'">
        <cfset StartCommaFlag = 1>
    </cfif>        
</CFLOOP>
                    
<cfset FILTER_KEY_ = "filter_key_">
<cfset FILTER_VALUE_ = "txtFilterValue_">
<cfset FILTER_DATE_VALUE_ = "txtFilterDate_">
<cfset FILTER_LIST_VALUE_ = "cboFilterValue_">
<cfset SPNERROR = "spnError">
<cfset FILTER_VAL_CTRL_ = "filter_val_ctrl_">
<cfset LIST = "LIST">
<cfset TEXT = "TEXT">
<cfset DATE = "DATE">
<cfset INTEGER = "INTEGER">
<cfset ISNOTNUMBER = "Is not a number">
<cfset ISNOTDATE = "Is not a date">
<cfset CANNOTBEEMPTY = "Cannot be empty">
<cfset FIELDREQUIRED = "This field is required">
<cfset jSonUtil = createObject("component", "#LocalSessionDotPath#.lib.json.JSONUtil").init() />
<cfset
		operatorKeys = [
			{VALUE='=', DISPLAY='Is', INLIST='#TEXT#,#INTEGER#,#DATE#,#LIST#'},
			{VALUE='<>', DISPLAY='Is Not', INLIST='#TEXT#,#INTEGER#,#DATE#,#LIST#'},
			{VALUE='>', DISPLAY='Is Greater Than', INLIST='#INTEGER#'},
			{VALUE='<', DISPLAY='Is Less Than', INLIST='#INTEGER#'},
			{VALUE='<', DISPLAY='Is Before', INLIST='#DATE#'},
			{VALUE='>', DISPLAY='Is After', INLIST='#DATE#'},
			{VALUE='LIKE', DISPLAY='Similar To', INLIST='#TEXT#'}
		]>
<cfset validFieldType = [
			'CF_SQL_BIGINT',
			'CF_SQL_BIT',
			'CF_SQL_CHAR',
			'CF_SQL_BLOB',
			'CF_SQL_CLOB',
			'CF_SQL_DATE',
			'CF_SQL_DECIMAL',
			'CF_SQL_DOUBLE',
			'CF_SQL_FLOAT',
			'CF_SQL_IDSTAMP',
			'CF_SQL_INTEGER',
			'CF_SQL_LONGVARCHAR',
			'CF_SQL_MONEY',
			'CF_SQL_MONEY4',
			'CF_SQL_NUMERIC',
			'CF_SQL_REAL',
			'CF_SQL_REFCURSOR',
			'CF_SQL_SMALLINT',
			'CF_SQL_TIME',
			'CF_SQL_TIMESTAMP',
			'CF_SQL_TINYINT',
			'CF_SQL_VARCHAR'
		]>
		<cfloop array="#attributes.filterKeys#" index="filterField">
			<cfif not StructKeyExists(filterField, "FILTERTYPE")>
				<cfset filterField.FILTERTYPE = #TEXT#>
			</cfif>
			<cfif not StructKeyExists(filterField, "EXCEPTIONCASE")>
				<cfset filterField.EXCEPTIONCASE = "">
			</cfif>
		</cfloop>	


<cfswitch expression="#THISTAG.ExecutionMode#">
 
	<cfcase value="Start">
		<cfparam 
			name="attributes.filterkeys" 
			type="array" default="#ArrayNew(1)#"
		>
		<cfparam 
			name="attributes.filterFields" 
			type="array" default="#ArrayNew(1)#"
		>
	</cfcase>
 
 	<!---- If has a close tag----->
	<cfcase value="End">
 		<input type="hidden" value="" id="SortColumn" name="SortColumn">
		<input type="hidden" value="" id="SortType" name="SortType">
	  <cfoutput>
			<script id="tmplFilterItem" type="text/x-jquery-tmpl">
				<div class="row" id="filter_row_${i}" value="${i}" >
					<select id="#FILTER_KEY_#${i}" class="filter_key" name="#FILTER_KEY_#${i}" onchange="#attributes.FilterSetId#ChangeFilterDataType(this, ${i})">
						<cfloop array="#attributes.filterkeys#" index="key">
							<option value="#key.VALUE#" filterType="#key.FILTERTYPE#" exceptionCase="#key.EXCEPTIONCASE#">#key.DISPLAY#</option>
						</cfloop>
					</select>
					<select id="filter_operator_${i}" class="filter_operator operator" name="filter_operator_${i}">
						<!--- <cfloop array="#operatorkeys#" index="key">
							<option value="#key.VALUE#">#key.DISPLAY#</option>
						</cfloop> --->
					</select>
					<div class="#FILTER_VAL_CTRL_#${i} filter_value_content field_value_body" inType="#TEXT# #INTEGER#">
						<input type="text" name="#FILTER_VALUE_#${i}" class="filter_val" id="#FILTER_VALUE_#${i}">
						<span id="#SPNERROR##FILTER_VALUE_#${i}" class="filter_value_error">#ISNOTNUMBER#</span>
					</div>
					<div class="#FILTER_VAL_CTRL_#${i} filter_date_value_content field_value_body" inType="#DATE#">
						<input type="text" name="#FILTER_DATE_VALUE_#${i}" class="filter_date_val" id="#FILTER_DATE_VALUE_#${i}"/>
						<span id="#SPNERROR##FILTER_DATE_VALUE_#${i}" class="filter_date_value_error"></br>#ISNOTDATE#</span>
					</div>	
					<div class="#FILTER_VAL_CTRL_#${i} filter_list_value_content field_value_body" inType="#LIST#">
						<select name="#FILTER_LIST_VALUE_#${i}" class="filter_list_val" id="#FILTER_LIST_VALUE_#${i}">
						</select>
					</div>
					<div class="action">
						<a href='##' onClick='#attributes.FilterSetId#AddFilterRow(${i}); return false;' class="add_filter left">
							<img class='filter_add_button ListIconLinks img24_24 add_filter_18_18' 
								src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Add Filter'/>
						</a>
						<a href='##'onClick='#attributes.FilterSetId#deleteFilter("filter_row_${i}"); return false;' class='delete_filter left'>
							<img class='filter_add_button ListIconLinks img24_24 delete_filter_18_18' 
								src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Delete Filter'/>
						</a>
					</div>
				</div>
			</script>
			<cfset showFilterVal = 'block'>
			<cfset hideFilterVal = 'none'>
			<cfset isHideFilter = 0>
			<cfset isClickFilter = 0>
		  	<cfif Isdefined("attributes.filterkeys") AND ArrayLen(attributes.filterkeys) GT 0>
			  	<cfset currentURL = getPageContext().getRequest().getRequestURI()>			
        		<cfset sortData = ArrayNew(1)>
				<cfloop index="intRow" from="1" to="#ArrayLen(attributes.colModels)#">
				 	<cfset ColModel = attributes.colModels[intRow]>
					  <cfif structKeyExists(ColModel,'sortObject')>
							<cfset currSort = ColModel.sortObject>
							<cfif structKeyExists(currSort,'isDefault') AND currSort.isDefault EQ 'true'>
								<cfset sortItem = StructNew()>
			                    <cfset sortItem.SORT_COLUMN = currSort.sortColName>
			                    <cfset sortItem.SORT_TYPE = currSort.sortType>
								<cfset sortData[1] = sortItem>
							</cfif>	
					  </cfif>
				</cfloop>				
				<!--- This should not assume all posts are from form - Ive been replacing som GETs with POSTs -- JLP - added a default cfparam at top of page --->  
                <cfif CGI.REQUEST_METHOD EQ 'POST' >
					<cfset sortData = ArrayNew(1)>
					<cftry>
        	            <cfset sortItem = StructNew()>
	                    <cfset sortItem.SORT_COLUMN = FORM["SortColumn"]>
	                    <cfset sortItem.SORT_TYPE = FORM["SortType"]>
						<cfset sortData[1] = sortItem>
                    <cfset filterData = ArrayNew(1)>        
                    <cfcatch type="Any" >
                    </cfcatch>
                    </cftry>				 	
					
                    <!--- generate filter conditions --->
                    <cfset totalFilters = FORM.TOTALFILTER>
                    <cfif totalFilters GT 0>
                        <cfset isClickFilter = 1>
                    </cfif>
                    <cfif FORM.isHideFilter EQ 1>
                        <cfset showFilterVal = 'none'>
                        <cfset hideFilterVal = 'block'>
                        <cfset isHideFilter = 1>
                    </cfif>
                    <cfloop from="1" to="#totalFilters#" index="index">
                        <cfset filterItem = StructNew()>
                        <cfset filterItem.FIELD_NAME = attributes.filterFields[FORM["#FILTER_KEY_#" & (index - 1)]]>
                        <cfset filterItem.OPERATOR = FORM["FILTER_OPERATOR_" & (index - 1)]>
                        <cfset filterItem.FIELD_VAL = TRIM(FORM[GetFilterValue(index)])>
                        <cfset filterItem.FIELD_INDEX = TRIM(FORM["#FILTER_KEY_#" & (index - 1)])>
                        <!--- Check by filter keys --->
                        <cfloop array="#attributes.filterkeys#" index="key">
                            <cfif key.VALUE EQ FORM["#FILTER_KEY_#" & (index - 1)]>
                                <cfif ArrayFind(validFieldType, key.TYPE)>
                                    <cfset filterItem.FIELD_TYPE = key.TYPE>
                                <cfelse>
                                    <cfset filterItem.FIELD_TYPE = 'CF_SQL_VARCHAR'>
                                </cfif>
                            </cfif>
                        </cfloop>
                        <!--- trim all space characters --->
                        <cfset filterItem.FIELD_VAL = replace(TRIM(filterItem.FIELD_VAL),'\','\\','ALL')>
                        <cfset filterItem.FIELD_VAL = replace(filterItem.FIELD_VAL,'"','\"','ALL')>						
						<!---<cfset filterItem.FIELD_VAL = UrlEncodedFormat(replace(filterItem.FIELD_VAL,'"','\"','ALL'))>--->
                        <!--- <cfset filterItem.FIELD_VAL = "#UrlEncodedFormat(replace(TRIM(filterItem.FIELD_VAL),'\','\\','ALL'))#"> --->
                        <cfset filterData[index] = filterItem>
                    </cfloop>
                    
                    <cfset currentURL = getPageContext().getRequest().getRequestURI()>
                    <cfset queryString = getPageContext().getRequest().getQueryString()>
                    <cfif Isnull(queryString)>
                        <cfset queryString = ''>
                    </cfif>
                    <cfset query = "query=" & UrlEncodedFormat(jSonUtil.serializeToJSON(filterData)) & "&sort=" & UrlEncodedFormat(jSonUtil.serializeToJSON(sortData))>
                    
                    <cfif NOT StructKeyExists(url, 'query')>
                        <cfif queryString EQ "">
                            <cfset newUrl = currentURL & "?" & query>
                        <cfelse>
                            <cfset newUrl = currentURL & "?" & query & "&" & queryString>
                        </cfif>                        
                    <cfelse>                    
                    	<cfset query = query & "&">
                        <cfset newUrl = currentURL & "?" &  reReplaceNoCase(queryString, "query=[^&]+&?", query) &  reReplaceNoCase(queryString, "sort=[^&]+&?", query) & reReplaceNoCase(queryString, "INPGROUPID=[^&]+&?", "") & reReplaceNoCase(queryString, "INPBATCHID=[^&]+&?", "")>
                    </cfif>
                    
                    <cfset newUrl = replaceNoCase(newUrl, "index", "", "all")>
                  <!---  
                    <cfif NOT StructKeyExists(url, 'INPBATCHID')>                       
                    	<cfset newUrl = newUrl & "&INPBATCHID=#INPBATCHID#">                     
                    </cfif>   
                     
                    <cfif NOT StructKeyExists(url, 'INPGROUPID')>                       
                      	<cfset newUrl = newUrl & "&INPGROUPID=#INPGROUPID#">                     
                    </cfif>   --->
                    
                    <!---<cfdump var="#url#">--->
                    <!---<cfexit>
                    <cflocation url="#newUrl#" addtoken="no">--->
                 
                 <!--- &INPBATCHID=#INPBATCHID#&INPGROUPID=#INPGROUPID# --->
             
              <!--- infinite loop if I try to post here ???? because <cfif CGI.REQUEST_METHOD EQ 'POST'> --->
              <!---  <script type="text/javascript" language="javascript">				
					<cfoutput>				
						var params = {#FormParams#};											
						post_to_url('#newUrl#&YYY=YYY', params, 'POST');			
					</cfoutput>					
				</script>--->
                
			  </cfif> 
			  <div class="filter_box" id="filter_box">
				<div class="actions_filter_box">
					<span class="filter_title">Filter(s)</span>
					<span class="filter_action" id="show_action" style="display:#hideFilterVal#"><a href="##" onclick="#attributes.FilterSetId#showMbFilters(); return false;">Show Filter Fields</a></span>
					<span class="filter_action" id="hide_action" style="display:#showFilterVal#"><a href="##" onclick="#attributes.FilterSetId#hideMbFilters(); return false;">Hide Filter Fields</a></span>	
				</div>
				<div class="filter_content" id="fil_content" style="display:#showFilterVal#">
					<cfinput type="hidden" name="totalFilter" validate="integer" value="0" id="totalFilter">
					<cfinput type="hidden" name="isHideFilter" validate="integer" value="#isHideFilter#" id="isHideFilter">
					<cfinput type="hidden" name="isClickFilter" validate="integer" value="#isClickFilter#" id="isClickFilter">
                    <cfinput type="hidden" name="INPBATCHID" value="#INPBATCHID#">
                    <cfinput type="hidden" name="INPGROUPID" value="#INPGROUPID#">
					<cfoutput>
						<cfif arrayLen(filterData) GT 0 AND filterData[1].FIELD_VAL NEQ ''>
							<div id="filter_results">
								You applied #arrayLen(filterData)# <cfif arrayLen(filterData) EQ 1>filter<cfelse>filters</cfif>. 
								Showing <span id='totalFilterResult'></span> results.
							</div>
						</cfif>
					</cfoutput>
					<div id="filter_rows">
					</div>
					<div id="filter_submits">
						<div class="row">
							<a href="##" onClick="#attributes.FilterSetId#Filter(); return false;" class="button filterButton small" >Filter</a>
							<a href="##" onClick="#attributes.FilterSetId#ClearFilter(); return false;" class="button filterButton small" >Clear</a>
						</div>
					</div>
				</div>
			</div>
		  </cfif>
		
	  </cfoutput>
	</cfcase>
 
</cfswitch>

<cffunction name="GetFilterValue">
	<cfargument name="index" required="yes">
	<cfset filterType = attributes.filterkeys[FORM["#FILTER_KEY_#" & (index - 1)]].FILTERTYPE>
	<cfif filterType EQ "#TEXT#" OR filterType EQ "#INTEGER#">
		<cfreturn FILTER_VALUE_ & (index - 1)>
	<cfelseif filterType EQ "#DATE#">
		<cfreturn FILTER_DATE_VALUE_ & (index - 1)>
	<cfelse>
		<cfreturn FILTER_LIST_VALUE_ & (index - 1)>
	</cfif>
</cffunction>
<cfoutput>
	<script type="text/javascript" language="javascript">
		function <cfoutput>#attributes.FilterSetId#</cfoutput>clickAll(obj){
			if(obj.checked){
				$.each($('###attributes.FilterSetId# .chek_box'), function(index, value) {
					$(this).attr('checked', true);
				});
			}else{
				$.each($('###attributes.FilterSetId# .chek_box'), function(index, value) {
					$(this).attr('checked', false);
				});
			}
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>clickCheckBox(obj){
			if(obj.checked){
				var isCheckAll = true;
				
				$.each($('###attributes.FilterSetId# .chek_box'), function(index, value) {
					if(!$(this).attr('checked')){
						isCheckAll = false;
					}
				});
				if(isCheckAll == true){
					$("###attributes.FilterSetId# .all_chek_box").attr('checked', true);
				}
			}else{
				
				$("###attributes.FilterSetId# .all_chek_box").attr('checked', false);
			}
		}		
		  	
		function <cfoutput>#attributes.FilterSetId#</cfoutput>showMbFilters(){
			$("###attributes.FilterSetId# ##hide_action").show();
			$("###attributes.FilterSetId# ##fil_content").show();
			$("###attributes.FilterSetId# ##show_action").hide();
			<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
			$("###attributes.FilterSetId# ##isHideFilter").val(0);
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>hideMbFilters(){
			$("###attributes.FilterSetId# ##hide_action").hide();
			$("###attributes.FilterSetId# ##fil_content").hide();
			$("###attributes.FilterSetId# ##show_action").show();
			$("###attributes.FilterSetId# ##totalFilter").val(0);
			$("###attributes.FilterSetId# ##isHideFilter").val(1);
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>AddFilterRow(position) {
			var newFilterId = $('###attributes.FilterSetId# ##filter_rows .row').length + 1;
			<cfoutput>#attributes.FilterSetId#</cfoutput>generateNewFilterRow(newFilterId, position);
			<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
			$("###attributes.FilterSetId# ##isClickFilter").val(1);
			<cfoutput>#attributes.FilterSetId#</cfoutput>CreateDatePickerOnRow(position + 1);
			<cfoutput>#attributes.FilterSetId#</cfoutput>ChangeFilterDataType($('###attributes.FilterSetId# ###FILTER_KEY_#' + (position + 1)), position + 1);
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>generateNewFilterRow(newID, position){
			var filter = <cfoutput>#attributes.FilterSetId#</cfoutput>CreateFilterObject(newID);
			//$("##tmplFilterItem").tmpl(filter).appendTo('##filter_rows');
			$("###attributes.FilterSetId# ##filter_row_" +  position).after($("###attributes.FilterSetId# ##tmplFilterItem").tmpl(filter));
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>CreateFilterObject(i) {
			var filter = new Object();
			filter.i = i;
			return filter;
		}
		
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID(){
			// Filter keys
			$.each($('###attributes.FilterSetId# .filter_key'), function(index, value) {
				$(this).attr('name', '#FILTER_KEY_#' + index);
				$(this).attr('id', '#FILTER_KEY_#' + index);
				$(this).attr('onchange', '<cfoutput>#attributes.FilterSetId#</cfoutput>ChangeFilterDataType(this,' + index + ')');
			});
			
			$.each($('###attributes.FilterSetId# .filter_date_val'), function(index, value) {
				$(this).attr('name', '#FILTER_DATE_VALUE_#' + index);
				$(this).attr('id', '#FILTER_DATE_VALUE_#' + index);
			});
			
			$.each($('###attributes.FilterSetId# .filter_list_val'), function(index, value) {
				$(this).attr('name', '#FILTER_LIST_VALUE_#' + index);
				$(this).attr('id', '#FILTER_LIST_VALUE_#' + index);
			});
			
			
			$.each($('###attributes.FilterSetId# .filter_value_content'), function(index, value) {
				$(this).attr('class', '#FILTER_VAL_CTRL_#' + index + ' filter_value_content field_value_body');
			});
			
			$.each($('###attributes.FilterSetId# .filter_date_value_error'), function(index, value) {
				$(this).attr('id', '#SPNERROR##FILTER_DATE_VALUE_#' + index);
			});
			
			$.each($('###attributes.FilterSetId# .filter_value_error'), function(index, value) {
				$(this).attr('id', '#SPNERROR##FILTER_VALUE_#' + index);
			});
						
			$.each($('###attributes.FilterSetId# .filter_date_value_content'), function(index, value) {
				$(this).attr('class', '#FILTER_VAL_CTRL_#' + index + ' filter_date_value_content field_value_body');
			});
			
			$.each($('###attributes.FilterSetId# .filter_list_value_content'), function(index, value) {
				$(this).attr('class', '#FILTER_VAL_CTRL_#' + index + ' filter_list_value_content field_value_body');
			});										
			
			// Filter operators
			$.each($('###attributes.FilterSetId# .filter_operator'), function(index, value) {
				$(this).attr('name', 'filter_operator_' + index);
				$(this).attr('id', 'filter_operator_' + index);
			});
			// Filter values
			$.each($('###attributes.FilterSetId# .filter_val'), function(index, value) {
				$(this).attr('name', '#FILTER_VALUE_#' + index);
				$(this).attr('id', '#FILTER_VALUE_#' + index);
			});
			// Delete filters
			$.each($('###attributes.FilterSetId# .delete_filter'), function(index, value) {
				var onClikFunc = '<cfoutput>#attributes.FilterSetId#</cfoutput>deleteFilter("filter_row_' + index + '"); return false;';
				$(this).attr('onclick', onClikFunc);
			});
			// Rows
			$.each($('###attributes.FilterSetId# .row'), function(index, value) {
				$(this).attr('value', index);
				$(this).attr('id', 'filter_row_' + index);
			});
			// Add row func
			$.each($('###attributes.FilterSetId# .add_filter'), function(index, value) {
				var onClikFunc = '#attributes.FilterSetId#AddFilterRow(' + index + '); return false;';
				$(this).attr('onclick', onClikFunc);
			});
			// Total filter
			$("###attributes.FilterSetId# ##totalFilter").val($('###attributes.FilterSetId# ##filter_rows .row').length);
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>deleteFilter(filterID){
			if($('###attributes.FilterSetId# ##filter_rows .row').length <= 1){
				//jAlert('Cannot remove the last filter.', 'Warning');
				return false;
			}
			var totalFilters = parseInt($("###attributes.FilterSetId# ##totalFilter").val());
			for (var i = 0; i < totalFilters; ++i) {
				<cfoutput>#attributes.FilterSetId#</cfoutput>RemoveDatePickerOnRow(i);
			}
			$("###attributes.FilterSetId# ##" + filterID).remove();
			<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
			
			totalFilters = parseInt($("###attributes.FilterSetId# ##totalFilter").val());
			for (var i = 0; i < totalFilters; ++i) {
				<cfoutput>#attributes.FilterSetId#</cfoutput>CreateDatePickerOnRow(i);
			}
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>Filter() {
		
			<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
			$("###attributes.FilterSetId# ##isClickFilter").val(1);
			if($("###attributes.FilterSetId# ##currSortCol").val()!="" && $("###attributes.FilterSetId# ##currSortType").val() !=""){
				$("###attributes.FilterSetId# ##SortColumn").val($("###attributes.FilterSetId# ##currSortCol").val());
				$("###attributes.FilterSetId# ##SortType").val($("###attributes.FilterSetId# ##currSortType").val());
			}
			if (<cfoutput>#attributes.FilterSetId#</cfoutput>Validate()) {
				$("###attributes.FilterSetId# ##filters_content").submit();
			}
				
		}
				
		function gridOrdering(obj, column, sort){
			<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
			var isValidated = false;
			if ($('###attributes.FilterSetId# ##filter_rows .row').length > 1) {
				$("###attributes.FilterSetId# ##isClickFilter").val(1);
				if (<cfoutput>#attributes.FilterSetId#</cfoutput>Validate()) {
					isValidated = true;
				}
			}
			else {
				$("###attributes.FilterSetId# ##isClickFilter").val(0);
				isValidated = true;
			}
			if(isValidated == true)
			{
				$("###attributes.FilterSetId# ##SortColumn").val(column);
				$("###attributes.FilterSetId# ##SortType").val(sort);
				
				$("###attributes.FilterSetId# ##filters_content").submit();
			}
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>ClearFilter() {
			<!---<cfset currentURL = getPageContext().getRequest().getRequestURI()>
			<cfset queryString = getPageContext().getRequest().getQueryString()>
			<cfif Isnull(queryString)>
				<cfset queryString = ''>
			</cfif>
			
			<cfif StructKeyExists(url, 'INPBATCHID')>
				<cfset queryString = reReplaceNoCase(queryString, "INPBATCHID=[^&]+&?", "")>				
			</cfif>
			
			<cfif StructKeyExists(url, 'INPGROUPID')>
				<cfset queryString = reReplaceNoCase(queryString, "INPGROUPID=[^&]+&?", "")>				
			</cfif>
			
			<cfif StructKeyExists(url, 'query')>
				<cfset queryString = reReplaceNoCase(queryString, "query=[^&]+&?", "")>				
			</cfif>
			
			<cfif StructKeyExists(url, 'page')>
				<cfset queryString = reReplaceNoCase(queryString, "page=[^&]+&?", "")>				
			</cfif>
			<cfset currentURL = replaceNoCase(currentURL, "index", "", "all")>--->
						
			<!---window.location = '<cfoutput>#currentURL#</cfoutput>?' + '<cfoutput>#queryString#</cfoutput>'	--->		

			<cfset FormParams = "INPBATCHID:'#INPBATCHID#', INPGROUPID:'#INPGROUPID#' ">

			<cfoutput>	

				var params = {#FormParams#};								
			
				<!---post_to_url('#currentURL#?#queryString#&SSS=SSS', params, 'POST');		--->
				post_to_url('#currentURL#', params, 'POST');		
			</cfoutput>				

		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>ShowOperatorsByDataType(index, dataType) {
			var operators = new Array();
			var operatorKeys = <cfoutput>#serializeJSON(operatorKeys)#</cfoutput>
			for (var i = 0; i < operatorKeys.length; i++) {
				if (operatorKeys[i].INLIST.indexOf(dataType) > -1){
					operators.push(operatorKeys[i])
				}
			}
			
			$("###attributes.FilterSetId# ##filter_operator_" + index).find('option').remove();
			
			for (var i = 0; i < operators.length; i++) {
				$("###attributes.FilterSetId# ##filter_operator_" + index).append(new Option(operators[i].DISPLAY, operators[i].VALUE));
			}
			
			$.each($('###attributes.FilterSetId# .#FILTER_VAL_CTRL_#' + index), function(index, value) {
				if ($(this).attr('inType').indexOf(dataType) > -1) {
					$(this).show();
				}
				else {
					$(this).hide();
				}
			});
				
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>ChangeFilterDataType(obj, index) {
			var filterType = $(obj).find(":selected").attr('filterType');
			<cfoutput>#attributes.FilterSetId#</cfoutput>ShowOperatorsByDataType(index, filterType)
			<cfoutput>#attributes.FilterSetId#</cfoutput>CreateListOnRow(index, $(obj).val());
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>CreateDatePickerOnRow(index) {
			$('###attributes.FilterSetId# ###FILTER_DATE_VALUE_#' + index).datepicker({
				numberOfMonths: 1,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy',
				defaultDate: $('###attributes.FilterSetId# ###FILTER_DATE_VALUE_#' + index).val(),
				showOn: 'button',
				buttonImage: "#rootUrl#/#PublicPath#/images/date_picker.jpg",
		        buttonImageOnly: true,
		        constrainInput: false
			});	
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>RemoveDatePickerOnRow(index) {
			$('###attributes.FilterSetId# ###FILTER_DATE_VALUE_#' + index).datepicker("destroy");
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>CreateListOnRow(index, keyValue) {
			var filterKeys = <cfoutput>#serializeJSON(attributes.filterkeys)#</cfoutput>
			$.each(filterKeys, function(iIndex, filterItem) {
				if (filterItem.VALUE == keyValue && filterItem.FILTERTYPE == '#LIST#') {
					$("###attributes.FilterSetId# ###FILTER_LIST_VALUE_#" + index).find('option').remove();
					$.each(filterItem.LISTOFVALUES, function(jIndex, listItem) {
						$("###attributes.FilterSetId# ###FILTER_LIST_VALUE_#" + index).append(new Option(listItem.DISPLAY, listItem.VALUE));
					});
				}
			});
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>Validate() {
			var count = parseInt($('###attributes.FilterSetId# ##totalFilter').val());
			var result = true;
			for (var i = 0; i < count; ++i) {
				var filterType = $('###attributes.FilterSetId# ###FILTER_KEY_#' + i).find(":selected").attr('filterType');
				var exceptionCase = $('###attributes.FilterSetId# ###FILTER_KEY_#' + i).find(":selected").attr('exceptionCase');
				var filterValue = $('###attributes.FilterSetId# ###FILTER_VALUE_#' + i).val();
				if (filterType == '#INTEGER#' || filterType == '#TEXT#') {
					if ($.trim(filterValue) == '') {
						result = false;
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).html('#FIELDREQUIRED#')
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).show();
						continue;
					}
					else {
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).hide();
					}
				}
				else if (filterType == '#DATE#') {
					filterValue = $('###attributes.FilterSetId# ###FILTER_DATE_VALUE_#' + i).val();
					if ($.trim(filterValue) == '') {
						result = false;
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_DATE_VALUE_#' + i).html('#FIELDREQUIRED#')
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_DATE_VALUE_#' + i).show();
						continue;
					}
					else {
						$('###SPNERROR##FILTER_DATE_VALUE_#' + i).hide();
					}
				}
				
				if (filterType == '#INTEGER#') {
					if (filterValue != '' ) {
						if (!IsNumber(filterValue) & !<cfoutput>#attributes.FilterSetId#</cfoutput>IsExceptionCase(filterValue, exceptionCase)) {
							result = false;
							$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).html('#ISNOTNUMBER#')
							$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).show();
						}
					}
				}
				else if (filterType == '#DATE#') {
					if (filterValue != '' & !<cfoutput>#attributes.FilterSetId#</cfoutput>IsExceptionCase(filterValue, exceptionCase)) {
						if (!IsDate(filterValue)) {
							result = false;
							$('###attributes.FilterSetId# ###SPNERROR##FILTER_DATE_VALUE_#' + i).html('#ISNOTDATE#')
							$('###attributes.FilterSetId# ###SPNERROR##FILTER_DATE_VALUE_#' + i).show();
						}
					}
				}
				else if (filterType == '#TEXT#') {
					var operator = $('###attributes.FilterSetId# ##filter_operator_' + i).val();
					if (operator == 'LIKE' & filterValue == '') {
						result = false;
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).html('#CANNOTBEEMPTY#')
						$('###attributes.FilterSetId# ###SPNERROR##FILTER_VALUE_#' + i).show();
					}
				}
			}
			return result;
		}
		
		function <cfoutput>#attributes.FilterSetId#</cfoutput>IsExceptionCase(filterValue, exceptionCase) {
			var isExceptionCase = false;
			if (exceptionCase != '') {
				var exceptionValues = exceptionCase.split(',');
				for (var i = 0; i < exceptionValues.length; ++i) {
					if (filterValue.toUpperCase() == exceptionValues[i].toUpperCase()) {
						isExceptionCase = true;
					}
				}
			}
			
			return  isExceptionCase;
		}
		function <cfoutput>#attributes.FilterSetId#</cfoutput>InitFilter() {
			var filterData = <cfoutput>#jSonUtil.serializeToJSON(filterData)#</cfoutput>;
			if (filterData.length == 0) {
				var filter = <cfoutput>#attributes.FilterSetId#</cfoutput>CreateFilterObject(0);
				
				$("###attributes.FilterSetId# ##tmplFilterItem").tmpl(filter).appendTo('###attributes.FilterSetId# ##filter_rows');
				<cfoutput>#attributes.FilterSetId#</cfoutput>CreateDatePickerOnRow(0);
				
				<cfoutput>#attributes.FilterSetId#</cfoutput>ChangeFilterDataType($('###attributes.FilterSetId# ###FILTER_KEY_#' + 0), 0);
			}
			else {
				var filterKeys = <cfoutput>#serializeJSON(attributes.filterKeys)#</cfoutput>;
				$.each(filterData, function(index, filterItem) {
					var filter = <cfoutput>#attributes.FilterSetId#</cfoutput>CreateFilterObject(index);
					
					$("###attributes.FilterSetId# ##tmplFilterItem").tmpl(filter).appendTo('###attributes.FilterSetId# ##filter_rows');
					<cfoutput>#attributes.FilterSetId#</cfoutput>CreateDatePickerOnRow(index);
					$.each(filterKeys, function(j, keyItem) {
						if (keyItem.VALUE == filterItem.FIELD_INDEX) {
							$('###attributes.FilterSetId# ###FILTER_KEY_#' + index).val(filterItem.FIELD_INDEX);
						}
					});
					<cfoutput>#attributes.FilterSetId#</cfoutput>ChangeFilterDataType($('###attributes.FilterSetId# ###FILTER_KEY_#' + index), index);
					$('###attributes.FilterSetId# ##filter_operator_' + index).val(filterItem.OPERATOR);
					
					var filterType = $('###attributes.FilterSetId# ###FILTER_KEY_#' + index).find(":selected").attr('filterType');
					if (filterType == '#TEXT#' || filterType == '#INTEGER#') {
						 $('###attributes.FilterSetId# ###FILTER_VALUE_#' + index).val(filterItem.FIELD_VAL);
					}
					else if (filterType == '#DATE#') {
						 $('###attributes.FilterSetId# ###FILTER_DATE_VALUE_#' + index).val(unescape(filterItem.FIELD_VAL));
					}
					else {
						 $('###attributes.FilterSetId# ###FILTER_LIST_VALUE_#' + index).val(filterItem.FIELD_VAL);
					}
				});
			}
		}
		
		$(document).ready(function(){
			
				<!--- Fix filter post action to remove .cfm so mod-rewrite does not lose post dat on re-direct--->
				var MRActionStrBuff = $("###attributes.FilterSetId# ##filters_content").attr('action');
				// added check undefine
				if(typeof(MRActionStrBuff) != 'undefined'){
					var MRActionStr = MRActionStrBuff.replace(".cfm","");		
					$("###attributes.FilterSetId# ##filters_content").attr('action', MRActionStr);			
				}
												
			
			
				<!--- for debugging only 
					$("###attributes.FilterSetId# ##filters_content").live("submit", function(event) {
					$form = $(this);
				
					alert('the action is: ' + $form.attr('action'));
				});
				--->



			<!--- Hide jumpy CSS while filter is loading - surrond content with a div like <div id="coverfilterload" style="padding:10px; display:none;"> --->
			$("##coverfilterload").hide();
			
			<cfoutput>#attributes.FilterSetId#</cfoutput>InitFilter();
			
			
			<cfoutput>#attributes.FilterSetId#</cfoutput>
			
			
			$("##coverfilterload").show();
		
		});	
	</script>
</cfoutput>
