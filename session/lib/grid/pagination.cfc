<cfcomponent output="false">

<!---

      Pagination.cfc
/---------^---------------------------------------------------------------------------------------\
Nathan Strutz | strutz@gmail.com | http://www.dopefly.com/
Purpose: Provides customizable "nextN"-style navigation
License: BSD



      History
/---------^---------------------------------------------------------------------------------------\
Version 1.0 - 9/20/2008 - Release!



      Enhancements under consideration
/---------^---------------------------------------------------------------------------------------\
"Bridging" numbers like amazon - 1 number out of range will show if it bridges the "..." gap.
	// � Previous|Page:1 2 3 ... |Next �        page 1, 1000 pages total
	// � Previous|Page:1 2 3 4 5 ... |Next �    page 4  <--- this is a sticky #2!
	// � Previous|Page:1 ... 5 6 7 ... |Next �  page 6
Separate Left and Right (beginning and end) numeric padding - does anyone care?
Show exactly this number maximum, e.g. sticky is 5 but we are on pg.1, show 11 on pg.1 and 5
Number separators, 1 | 2 | 3 or 1, 2, 3



      Pagination learning, samples and further reading
/---------^---------------------------------------------------------------------------------------\
http://kurafire.net/log/archive/2007/06/22/pagination-101
http://developer.yahoo.com/ypatterns/pattern.php?pattern=itempagination
http://www.smashingmagazine.com/2007/11/16/pagination-gallery-examples-and-good-practices/
http://woork.blogspot.com/2008/03/perfect-pagination-style-using-css.html



      Suggested Use Example:
/---------^---------------------------------------------------------------------------------------\
<cfset pagination = createObject("component", "Pagination").init() />
<cfset pagination.setQueryToPaginate(myQuery) /><!--- required --->
<cfset pagination.setBaseLink("/app/photolist?year=#year#") /><!--- recommended --->
<cfset pagination.setItemsPerPage(25) /><!--- default is 10 --->
<cfset pagination.setShowNumericLinks(true) /><!--- default is false, will not display numbers --->

#pagination.getRenderedHTML()#

<cfoutput query="myQuery" startrow="#pagination.getStartRow()#" maxrows="#pagination.getMaxRows()#">
	... loop over records ...
</cfoutput>

#pagination.getRenderedHTML()#



     Copyright Notice (BSD License)
/---------^---------------------------------------------------------------------------------------\
Copyright (c) 2008, Nathan Strutz

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of
	  conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of
	  conditions and the following disclaimer in the documentation and/or other materials provided
	  with the distribution.
    * Neither the name of Pagination.cfc nor the names of its contributors may be used to endorse or
	  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--->

	<cffunction name="init" access="public" output="false" returntype="Pagination">

		<!--- local storage defaults --->
		<cfset variables.my = structNew() />

		<!--- Visual Options --->
		<cfset setShowPrevNextHTML(true)>
		<cfset setPreviousLinkHTML("&lt; ") />
		<cfset setNextLinkHTML(" &gt;") />
		<cfset setShowPrevNextDisabledHTML(true)>
		<cfset setPreviousLinkDisabledHTML("<a href='##' onclick='return false;'>" & getPreviousLinkHTML() & "</a>&nbsp;") />
		<cfset setNextLinkDisabledHTML("<a href='##' onclick='return false;'>" & getNextLinkHTML() & "</a>&nbsp;") />

		<cfset setShowFirstLastHTML(true)>
		<cfset setFirstLinkHTML("&##171; First") />
		<cfset setLastLinkHTML("Last &##187;") />
		<cfset setShowFirstLastDisabledHTML(true)>
		<cfset setFirstLinkDisabledHTML("<a href='##' onclick='return false;'>" & getFirstLinkHTML() & "</a>&nbsp;") />
		<cfset setLastLinkDisabledHTML("<a href='##' onclick='return false;'>" & getLastLinkHTML() & "</a>&nbsp;") />

		<cfset setShowNumericLinks(false) />
		<cfset setNumericDistanceFromCurrentPageVisible(3) />
		<cfset setNumericEndBufferCount(2) />
		<cfset setShowMissingNumbersHTML(true) />
		<cfset setMissingNumbersHTML("...") />
		<cfset setBeforeNumericLinksHTML("") />
		<cfset setBeforeNextLinkHTML("") />

		<cfset setClassName("") />
		<!--- Configuration Options --->

		<cfset setItemsPerPage(10) />
		<cfset setBaseLink("") />
		<cfset setUrlPageIndicator("pagenumber") />
		<cfset setCompressHTML(false) />
		<!--- Caculated Options --->
		<cfset variables.my.TotalNumberOfPages = 1 />
		<cfset variables.my.StartRow = 1 />
		<cfset variables.my.CurrentPage = 1 />
		<cfset variables.my.EndRow = 10 />
		<!--- Resulting data --->
		<cfset variables.my.RenderedHTML = "" />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
		<cfset variables.my.paginationTextRendered = false />

		<!--- locical shortcut: Items Per Page = cfquery MaxRows --->
		<cfset variables.getMaxRows = variables.getItemsPerPage />
		<cfset this.getMaxRows = this.getItemsPerPage />
		<cfset variables.setMaxRows = variables.setItemsPerPage />
		<cfset this.setMaxRows = this.setItemsPerPage />

		<cftrace category="Pagination" text="Initialized Pagination CFC" />

		<cfreturn this />
	</cffunction>


<!--- -------------- Visual Options -------------- --->
	<cffunction name="getSortData" returntype="string" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfreturn UrlEncodedFormat(variables.my.SortData) />
	</cffunction>
	<cffunction name="setSortData" returntype="void" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfargument name="_SortData" type="string" />
		<cfset variables.my.SortData = arguments._SortData />
		<cfset variables.my.rendered = false />
	</cffunction>
	<cffunction name="getFilterData" returntype="string" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfreturn UrlEncodedFormat(variables.my.FilterData) />
	</cffunction>
	<cffunction name="setFilterData" returntype="void" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfargument name="_FilterData" type="string" />
		<cfset variables.my.FilterData = arguments._FilterData />
		<cfset variables.my.rendered = false />
	</cffunction>
	<cffunction name="getShowPrevNextHTML" returntype="boolean" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfreturn variables.my.ShowPrevNextHTML />
	</cffunction>
	<cffunction name="setShowPrevNextHTML" returntype="void" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfargument name="_ShowPrevNextHTML" type="boolean" />
		<cfset variables.my.ShowPrevNextHTML = arguments._ShowPrevNextHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getPreviousLinkHTML" returntype="string" output="false" access="public" hint="Text for the previous link word. Default is '&lt; Previous'.">
		<cfreturn variables.my.PreviousLinkHTML />
	</cffunction>
	<cffunction name="setPreviousLinkHTML" output="false" access="public" hint="Text for the previous link word. Default is '&lt; Previous'.">
		<cfargument name="_PreviousLinkHTML" type="string" />
		<cfset variables.my.PreviousLinkHTML = arguments._PreviousLinkHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getNextLinkHTML" returntype="string" output="false" access="public" hint="Text for the next link word. Default is 'Next &gt;'.">
		<cfreturn variables.my.NextLinkHTML />
	</cffunction>
	<cffunction name="setNextLinkHTML" output="false" access="public" hint="Text for the next link word. Default is 'Next &gt;'.">
		<cfargument name="_NextLinkHTML" type="string" />
		<cfset variables.my.NextLinkHTML = arguments._NextLinkHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getShowPrevNextDisabledHTML" returntype="string" output="false" access="public" hint="Option to display Previous and Next text even when the link is not available.">
		<cfreturn variables.my.ShowPrevNextDisabledHTML />
	</cffunction>
	<cffunction name="setShowPrevNextDisabledHTML" returntype="void" output="false" access="public" hint="Option to display Previous and Next text even when the link is not available.">
		<cfargument name="_ShowPrevNextDisabledHTML" type="string" />
		<cfset variables.my.ShowPrevNextDisabledHTML = arguments._ShowPrevNextDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getPreviousLinkDisabledHTML" returntype="string" output="false" access="public" hint="Text for the previous link word when the link is disabled. Default is the same as PreviousLinkHTML.">
		<cfreturn variables.my.PreviousLinkDisabledHTML />
	</cffunction>
	<cffunction name="setPreviousLinkDisabledHTML" returntype="void" output="false" access="public" hint="Text for the previous link word when the link is disabled. Default is the same as PreviousLinkHTML.">
		<cfargument name="_PreviousLinkDisabledHTML" type="string" />
		<cfset variables.my.PreviousLinkDisabledHTML = arguments._PreviousLinkDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getNextLinkDisabledHTML" returntype="string" output="false" access="public" hint="Text for the next link word when the link is disabled. Default is the same as NextLinkHTML.">
		<cfreturn variables.my.NextLinkDisabledHTML />
	</cffunction>
	<cffunction name="setNextLinkDisabledHTML" returntype="void" output="false" access="public" hint="Text for the next link word when the link is disabled. Default is the same as NextLinkHTML.">
		<cfargument name="_NextLinkDisabledHTML" type="string" />
		<cfset variables.my.NextLinkDisabledHTML = arguments._NextLinkDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getShowFirstLastHTML" returntype="boolean" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfreturn variables.my.ShowFirstLastHTML />
	</cffunction>
	<cffunction name="setShowFirstLastHTML" returntype="void" output="false" access="public" hint="Option to display next and previous buttons.">
		<cfargument name="_ShowFirstLastHTML" type="boolean" />
		<cfset variables.my.ShowFirstLastHTML = arguments._ShowFirstLastHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getFirstLinkHTML" returntype="string" output="false" access="public" hint="Text for the First link word. Default is '&lt; First'.">
		<cfreturn variables.my.FirstLinkHTML />
	</cffunction>
	<cffunction name="setFirstLinkHTML" output="false" access="public" hint="Text for the First link word. Default is '&lt; First'.">
		<cfargument name="_FirstLinkHTML" type="string" />
		<cfset variables.my.FirstLinkHTML = arguments._FirstLinkHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getLastLinkHTML" returntype="string" output="false" access="public" hint="Text for the Last link word. Default is 'Last &gt;'.">
		<cfreturn variables.my.LastLinkHTML />
	</cffunction>
	<cffunction name="setLastLinkHTML" output="false" access="public" hint="Text for the Last link word. Default is 'Last &gt;'.">
		<cfargument name="_LastLinkHTML" type="string" />
		<cfset variables.my.LastLinkHTML = arguments._LastLinkHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getShowFirstLastDisabledHTML" returntype="string" output="false" access="public" hint="Option to display Previous and Next text even when the link is not available.">
		<cfreturn variables.my.ShowFirstLastDisabledHTML />
	</cffunction>
	<cffunction name="setShowFirstLastDisabledHTML" returntype="void" output="false" access="public" hint="Option to display Previous and Next text even when the link is not available.">
		<cfargument name="_ShowFirstLastDisabledHTML" type="string" />
		<cfset variables.my.ShowFirstLastDisabledHTML = arguments._ShowFirstLastDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getFirstLinkDisabledHTML" returntype="string" output="false" access="public" hint="Text for the First link word when the link is disabled. Default is the same as FirstLinkHTML.">
		<cfreturn variables.my.FirstLinkDisabledHTML />
	</cffunction>
	<cffunction name="setFirstLinkDisabledHTML" returntype="void" output="false" access="public" hint="Text for the First link word when the link is disabled. Default is the same as FirstLinkHTML.">
		<cfargument name="_FirstLinkDisabledHTML" type="string" />
		<cfset variables.my.FirstLinkDisabledHTML = arguments._FirstLinkDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getLastLinkDisabledHTML" returntype="string" output="false" access="public" hint="Text for the Last link word when the link is disabled. Default is the same as LastLinkHTML.">
		<cfreturn variables.my.LastLinkDisabledHTML />
	</cffunction>
	<cffunction name="setLastLinkDisabledHTML" returntype="void" output="false" access="public" hint="Text for the Last link word when the link is disabled. Default is the same as LastLinkHTML.">
		<cfargument name="_LastLinkDisabledHTML" type="string" />
		<cfset variables.my.LastLinkDisabledHTML = arguments._LastLinkDisabledHTML />
		<cfset variables.my.rendered = false />
	</cffunction>


	<cffunction name="getShowNumericLinks" returntype="boolean" output="false" access="public" hint="Option to show numeric pagination links. False only displays prev/next links. Default is false.">
		<cfreturn variables.my.ShowNumericLinks />
	</cffunction>
	<cffunction name="setShowNumericLinks" output="false" access="public" hint="Option to show numeric pagination links. False only displays prev/next links. Default is false.">
		<cfargument name="_ShowNumericLinks" type="boolean" />
		<cfset variables.my.ShowNumericLinks = arguments._ShowNumericLinks />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getNumericDistanceFromCurrentPageVisible" returntype="numeric" output="false" access="public" hint="The amount of numbers displayed in sequence to the immediate left and right of the currently selected page number. Default is 3.">
		<cfreturn variables.my.NumericDistanceFromCurrentPageVisible />
	</cffunction>
	<cffunction name="setNumericDistanceFromCurrentPageVisible" output="false" access="public" hint="The amount of numbers displayed in sequence to the immediate left and right of the currently selected page number. Default is 3.">
		<cfargument name="_NumericDistanceFromCurrentPageVisible" type="numeric" />
		<cfset variables.my.NumericDistanceFromCurrentPageVisible = arguments._NumericDistanceFromCurrentPageVisible />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getNumericEndBufferCount" returntype="numeric" output="false" access="public" hint="The count of numbers displayed at the beginning and end of the numeric pagination. Default is 2.">
		<cfreturn variables.my.NumericEndBufferCount />
	</cffunction>
	<cffunction name="setNumericEndBufferCount" output="false" access="public" hint="The count of numbers displayed at the beginning and end of the numeric pagination. Default is 2.">
		<cfargument name="_NumericEndBufferCount" type="numeric" />
		<cfset variables.my.NumericEndBufferCount = arguments._NumericEndBufferCount />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getShowMissingNumbersHTML" returntype="boolean" output="false" access="public" hint="Option to show number skipped missing dots, i.e. 1 2 ... 5 6.">
		<cfreturn variables.my.ShowMissingNumbersHTML />
	</cffunction>
	<cffunction name="setShowMissingNumbersHTML" returntype="void" output="false" access="public" hint="Option to show number skipped missing dots, i.e. 1 2 ... 5 6.">
		<cfargument name="_ShowMissingNumbersHTML" type="boolean" />
		<cfset variables.my.ShowMissingNumbersHTML = arguments._ShowMissingNumbersHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getMissingNumbersHTML" returntype="string" output="false" access="public" hint="HTML to put when a number is skipped (out of range). Default is '...'.">
		<cfreturn variables.my.MissingNumbersHTML />
	</cffunction>
	<cffunction name="setMissingNumbersHTML" returntype="void" output="false" access="public" hint="HTML to put when a number is skipped (out of range). Default is '...'.">
		<cfargument name="_MissingNumbersHTML" type="string" />
		<cfset variables.my.MissingNumbersHTML = arguments._MissingNumbersHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getBeforeNumericLinksHTML" returntype="string" output="false" access="public">
		<cfreturn variables.my.BeforeNumericLinksHTML />
	</cffunction>
	<cffunction name="setBeforeNumericLinksHTML" output="false" access="public">
		<cfargument  name="_BeforeNumericLinksHTML" type="string" />
		<cfset variables.my.BeforeNumericLinksHTML = arguments._BeforeNumericLinksHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getBeforeNextLinkHTML" returntype="string" output="false" access="public">
		<cfreturn variables.my.BeforeNextLinkHTML />
	</cffunction>
	<cffunction name="setBeforeNextLinkHTML" output="false" access="public">
		<cfargument  name="_BeforeNextLinkHTML" type="string" />
		<cfset variables.my.BeforeNextLinkHTML = arguments._BeforeNextLinkHTML />
		<cfset variables.my.rendered = false />
	</cffunction>

	<cffunction name="getClassName" returntype="string" output="false" access="public" hint="CSS class name to put on highest level pagination HTML element.">
		<cfreturn variables.my.ClassName />
	</cffunction>
	<cffunction name="setClassName" output="false" access="public" hint="CSS class name to put on highest level pagination HTML element.">
		<cfargument  name="_ClassName" type="string" />
		<cfset variables.my.ClassName = arguments._ClassName />
		<cfset variables.my.rendered = false />
	</cffunction>

<!--- -------------- Configuration Options -------------- --->


	<cffunction name="getItemsPerPage" returntype="numeric" output="false" access="public" hint="Number of records per page to display. Default is 10.">
		<cfreturn variables.my.ItemsPerPage />
	</cffunction>
	<cffunction name="setItemsPerPage" output="false" access="public" hint="Number of records per page to display. Default is 10.">
		<cfargument name="_ItemsPerPage" type="numeric" />
		<cfset variables.my.ItemsPerPage = arguments._ItemsPerPage />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
	</cffunction>

	<cffunction name="getBaseLink" returntype="string" output="true" access="public" hint="Base link to current and/or subsequent pages not including the URL page indicator. This property is required!">
   
		<!--- tranglt --->
        <cfset tmp ="#CompareNoCase(Right(variables.my.BaseLink,10),'/index')#" />
        <cfif tmp EQ 0 >
            <cfset variables.my.BaseLink = Left(variables.my.BaseLink,Len(variables.my.BaseLink)-10) />
        </cfif>
        <!--- end tranglt --->
		<cfreturn variables.my.BaseLink />
	</cffunction>
	<cffunction name="setBaseLink" output="true" access="public" hint="Base link to current and/or subsequent pages not including the URL page indicator. This property is required!">
		<cfargument name="_BaseLink" type="string" />

		<cfif find("?", arguments._BaseLink)>
			<!--- a query string exists, so we will append to it --->
			<cfset arguments._BaseLink = _BaseLink & "&amp;" />
		<cfelse>
			<!--- a query string does not exist, so we will create one --->
			<cfset arguments._BaseLink = _BaseLink & "?" />
		</cfif>
		<cfset variables.my.BaseLink = arguments._BaseLink />
        <cfset variables.my.BaseLink = replaceNoCase(variables.my.BaseLink, "index", "", "all")>
		<cfset variables.my.rendered = false />
		

	</cffunction>
	<cffunction name="findBaseLink" returntype="string" output="false" access="private" hint="Attempts to determine your base link based CGI variables - this method is not reliable but works in a pinch.">
		<cfset var baseLink = cgi.script_name & "?" & cgi.query_string />
		<cftrace category="Pagination" text="Dynamically creating base link" />
		<cfset baseLink = reReplace(baseLink, "(\?|(&(amp;)))$", "") />
		<cfset baseLink = reReplace(baseLink, "(&(amp;)?|\?)?#getUrlPageIndicator()#=\w+", "", "ALL") />
		<!--- tranglt --->
        <cfset tmp ="#CompareNoCase(Right(baseLink,10),'/index')#" />
        <cfif tmp EQ 0 >
            <cfset baseLink = Left(baseLink,Len(baseLink)-10) />
        </cfif>
        <!--- end tranglt --->
		<cfreturn baseLink />
	</cffunction>

	<cffunction name="getUrlPageIndicator" returntype="string" output="false" access="public" hint="The URL variable to track which page we currently are on, default is 'pagenumber'.">
		<cfreturn variables.my.UrlPageIndicator />
	</cffunction>
	<cffunction name="setUrlPageIndicator" output="false" access="public" hint="The URL variable to track which page we currently are on, default is 'pagenumber'.">
		<cfargument name="_UrlPageIndicator" type="string" />
		<cfset variables.my.UrlPageIndicator = arguments._UrlPageIndicator />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
	</cffunction>

	<cffunction name="getCompressHTML" returntype="boolean" output="false" access="public" hint="Option to compress the HTML Output">
		<cfreturn variables.my.CompressHTML />
	</cffunction>
	<cffunction name="setCompressHTML" returntype="void" output="false" access="public" hint="Option to compress the HTML Output">
		<cfargument name="_CompressHTML" type="boolean" />
		<cfset variables.my.CompressHTML = arguments._CompressHTML />
		<cfset variables.my.rendered = false />
	</cffunction>


<!--- -------------- Internally Managed and Calculated Options (only getters are public) -------------- --->
	<cffunction name="getTotalNumberOfPages" returntype="numeric" output="false" access="public" hint="The calculated total number of pages based on number of records and records per page.">
		<!--- has to be calculated --->
		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>
		<cfreturn variables.my.TotalNumberOfPages />
	</cffunction>
	<cffunction name="setTotalNumberOfPages" output="false" access="public" hint="The calculated total number of pages based on number of records and records per page.">
		<cfargument name="_TotalNumberOfPages" type="numeric" />
		<cfset variables.my.TotalNumberOfPages = arguments._TotalNumberOfPages />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
	</cffunction>

	<cffunction name="getCurrentPage" returntype="numeric" output="false" access="public" hint="Current page number the user is viewing.">
		<!--- has to be calculated --->
		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>
		<cfreturn variables.my.CurrentPage />
	</cffunction>
	<cffunction name="setCurrentPage" returntype="void" output="false" access="private" hint="Current page number the user is viewing.">
		<cfargument name="_CurrentPage" type="numeric" />
		<cfset variables.my.CurrentPage = arguments._CurrentPage />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
		<!--- TODO: make this pubilc? would force reconfiguration, but doable --->
	</cffunction>

	<cffunction name="getStartRow" returntype="numeric" output="false" access="public" hint="The calculated starting row for your cfoutput tag.">
		<!--- has to be calculated --->
		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>
		<cfreturn variables.my.StartRow />
	</cffunction>
	<cffunction name="setStartRow" output="false" access="private" hint="The calculated starting row for your cfoutput tag.">
		<cfargument name="_StartRow" type="numeric" />
		<cfset variables.my.StartRow = arguments._StartRow />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
		<!--- TODO: make this pubilc? would force reconfiguration, but doable --->
	</cffunction>

	<cffunction name="getEndRow" returntype="numeric" output="false" access="public" hint="The calculated last row number that will be displayed on the current page.">
		<!--- has to be calculated --->
		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>
		<cfreturn variables.my.EndRow />
	</cffunction>
	<cffunction name="setEndRow" returntype="void" output="false" access="private" hint="The calculated last row number that will be displayed on the current page.">
		<cfargument name="_EndRow" type="numeric" />
		<cfset variables.my.EndRow = arguments._EndRow />
		<cfset variables.my.rendered = false />
		<cfset variables.my.configured = false />
		<!--- TODO: EndRow could be public and set instead of (or in addition to) ItemsPerPage, itemsPerPage would then be recalculated - logic could go both ways --->
	</cffunction>

	<cffunction name="getTotalNumberOfItems" returntype="numeric" output="false" access="public" hint="The total number of records, indexes, keys, etc., in the given data to paginate.">
		<!--- has to be calculated --->
		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>
		<cfreturn variables.my.TotalNumberOfItems />
	</cffunction>
	<cffunction name="setTotalNumberOfItems" output="false" access="private" hint="The total number of records, indexes, keys, etc., in the given data to paginate.">
		<cfargument  name="_TotalNumberOfItems" type="numeric" />
		<cfset variables.my.TotalNumberOfItems = arguments._TotalNumberOfItems />
	</cffunction>

	<cffunction name="getFirstPageLink" returntype="string" output="false" access="public" hint="Link to first page">
		<cfreturn TRIM("#getBaseLink()##getUrlPageIndicator()#=1") />
	</cffunction>

	<cffunction name="getPreviousPageLink" returntype="string" output="false" access="public" hint="Link to previous sequential page">
		<cfreturn TRIM("#getBaseLink()##getUrlPageIndicator()#=#max(1, getCurrentPage()-1)#") />
	</cffunction>

	<cffunction name="getNextPageLink" returntype="string" output="false" access="public" hint="Link to next sequential page">
		<cfreturn TRIM("#getBaseLink()##getUrlPageIndicator()#=#min(getTotalNumberOfPages(), getCurrentPage()+1)#") />
	</cffunction>

	<cffunction name="getLastPageLink" returntype="string" output="false" access="public" hint="Link to last page">
		<cfreturn TRIM("#getBaseLink()##getUrlPageIndicator()#=#getTotalNumberOfPages()#") />
	</cffunction>
	
	<cffunction name="getFirstPageNumber" returntype="numeric" output="false" access="public" hint="Link to first page">
		<cfreturn 1 />
	</cffunction>

	<cffunction name="getPreviousPageNumber" returntype="numeric" output="false" access="public" hint="Link to previous sequential page">
		<cfreturn max(1, getCurrentPage()-1) />
	</cffunction>

	<cffunction name="getNextPageNumber" returntype="numeric" output="false" access="public" hint="Link to next sequential page">
		<cfreturn min(getTotalNumberOfPages(), getCurrentPage()+1) />
	</cffunction>

	<cffunction name="getLastPageNumber" returntype="numeric" output="false" access="public" hint="Link to last page">
		<cfreturn getTotalNumberOfPages() />
	</cffunction>


<!--- -------------- Where the real work happens -------------- --->

	<cffunction name="hasMinimumAttributesToRender" returntype="boolean" access="public" hint="Determines if the requirements are met for rendering the pagination HTML.">
		<!--- either was already rendered, or if not, has the minimum requirements for rendering --->
		<cftry>
			<cfreturn variables.my.rendered or len(variables.my.baseLink) and (isQuery(variables.my.queryToPaginate) OR isArray(variables.my.arrayToPaginate) OR isStruct(variables.my.structToPaginate) ) />
			<cfcatch><cfreturn false /></cfcatch>
		</cftry>
	</cffunction>


	<cffunction name="getRenderedHTML" returntype="string" output="false" access="remote" hint="Creates and returns pagination HTML output.">
		<cfset var renderedOutput = "" />

		<cftrace category="Pagination" text="Call to render HTML" />


		<cfif not variables.my.configured>
			<cfset configureInputs() />
		</cfif>

		<!--- check if this is the 2nd+ attempt at same render, return previous render --->
		<cfif variables.my.rendered>
			<cfreturn variables.my.renderedHTML />
		</cfif>

		<!--- 1 page = no pagination necessary --->
		<cfif getTotalNumberOfPages() EQ 1>
			<cfsavecontent variable="renderedOutput"><div class="pagination empty">&nbsp;</div></cfsavecontent>
		<cfelse>
			<cfset renderedOutput = renderHTML() />
		</cfif>

		<cfif getCompressHTML()>
			<cfset renderedOutput = compressHTML(renderedOutput) />
		</cfif>

		<cfset variables.my.renderedHTML = renderedOutput />
		<cfset variables.my.rendered = true />

		<cfreturn renderedOutput />
	</cffunction>


	<cffunction name="configureInputs" returntype="void" output="false" access="private" hint="Sets the query start row and makes sure your URL page number indicator is not broken.">
		<cfscript>
			var urlPageNo = 1;
			var numberOfRecords = 0;

			if (structKeyExists(form, getUrlPageIndicator())) {
				urlPageNo = form[getUrlPageIndicator()];
			}

			// safety measure for URL pagination variable
			if (not isNumeric(urlPageNo) or urlPageNo LT 1) {
				urlPageNo = 1;
			} else if (urlPageNo GT variables.my.TotalNumberOfPages) {
				// url page number is higher than number of pages possible
				urlPageNo = variables.my.TotalNumberOfPages;
			}


			// set the actual URL variable
			variables.my.currentPage = urlPageNo;
			form[getUrlPageIndicator()] = urlPageNo;

			// set query start row for the currently selected page number
			setStartRow( (urlPageNo - 1) * getItemsPerPage() + 1 );
			// set query end row - this is the last row number visible on the page
			setEndRow( min(urlPageNo * getItemsPerPage(), numberOfRecords) );

			// make sure there is a base link
			if (getBaseLink() EQ "?" or getBaseLink() EQ "&amp;") {
				setBaseLink(findBaseLink());
			}

			// We have now been configured
			variables.my.configured = true;
		</cfscript>
	</cffunction>


	<cffunction name="renderHTML" returntype="string" output="false" access="private" hint="Creates the pagination HTML output.">
		<cfset var renderedOutput = "" />
		<cfset var displayDots = true />
		<cfset var i = 0 />

		<cftrace category="Pagination" text="Rendering fresh HTML" />

		<cfsavecontent variable="renderedOutput">
			<cfoutput>
				<div class="pagination #getClassName()#">
					<!--- Page #getCurrentPage()# of #getTotalNumberOfPages()# --->
				<cfif getCurrentPage() GT 1 and getShowFirstLastHTML()>
					<a href="#FormatUrl(INPURL = getFirstPageLink())#"  filterData="#getFilterData()#" page="#getFirstPageNumber()#" sortData="#getSortData()#" class="first page_button" >#getFirstLinkHTML()#</a>&nbsp;
				<cfelseif getShowFirstLastDisabledHTML()>
					<span class="first">#getFirstLinkDisabledHTML()#</span>
				</cfif>
				<cfif getCurrentPage() GT 1 and getShowPrevNextHTML()>
					&nbsp;<a href="#FormatUrl(INPURL = getPreviousPageLink())#"  filterData="#getFilterData()#" page="#getPreviousPageNumber()#" sortData="#getSortData()#" class="previous page_button" >#getPreviousLinkHTML()#</a>&nbsp;
				<cfelseif getShowPrevNextDisabledHTML()>
					<span class="previous">#getPreviousLinkDisabledHTML()#</span>
				</cfif>
				<cfif getShowNumericLinks()>
					#getBeforeNumericLinksHTML()#
					#renderNumericLinksHTML()#
				</cfif>
				#getBeforeNextLinkHTML()#
				<cfif getCurrentPage() LT getTotalNumberOfPages() and getShowPrevNextHTML()>
					&nbsp;<a href="#FormatUrl(INPURL = getNextPageLink())#"  filterData="#getFilterData()#" page="#getNextPageNumber()#" sortData="#getSortData()#" class="next page_button" >#getNextLinkHTML()#</a>&nbsp;
				<cfelseif getShowPrevNextDisabledHTML()>
					<span class="next">#getNextLinkDisabledHTML()#</span>
				</cfif>
				<cfif getCurrentPage() LT getTotalNumberOfPages() and getShowFirstLastHTML()>
					&nbsp;<a href="#FormatUrl(INPURL = getLastPageLink())#"  filterData="#getFilterData()#" page="#getLastPageNumber()#" sortData="#getSortData()#" class="last page_button" >#getLastLinkHTML()#</a>&nbsp;
				<cfelseif getShowFirstLastDisabledHTML()>
					<span class="last">#getLastLinkDisabledHTML()#</span>
				</cfif>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn renderedOutput />
	</cffunction>


	<cffunction name="renderNumericLinksHTML" returntype="string" output="false" access="private" hint="Creates the pagination numeric links HTML output. Separate from renderHTML so it can be called from an extended method easier, or overwritten if desired.">
		<cfset var renderedOutput = "" />
		<cfset var displayDots = true />
		<cfset var i = 0 />

		<cftrace category="Pagination" text="Rendering fresh HTML" />

		<cfsavecontent variable="renderedOutput">
			<cfoutput>
				<cfloop from="1" to="#getTotalNumberOfPages()#" index="i">
					<!--- TODO: getFixedMaximumNumbersShown does not work, but could, with some polish
					 	OR (getFixedMaximumNumbersShown() and getFixedMaximumNumbersShown() GT getNumericEndBufferCount() * 2 and abs( getCurrentPage() - i ) LTE ceiling((getFixedMaximumNumbersShown()-getNumericEndBufferCount()*2)/2))
					 --->
					<cfif (i LTE getNumericEndBufferCount()) or (i GT (getTotalNumberOfPages()-getNumericEndBufferCount())) or (abs( getCurrentPage() - i ) LTE getNumericDistanceFromCurrentPageVisible())>
						<!--- first 2 numbers or last 2 numbers, or within n either direction of the selected page --->
						<cfif getCurrentPage() NEQ i>
                        	<cfset CurrURLFix = "#getBaseLink()##getUrlPageIndicator()#=#i#">							
                        	<a href="#FormatUrl(INPURL = TRIM(CurrURLFix))#"  filterData="#getFilterData()#" sortData="#getSortData()#" page="#i#" class="page_button">#i#</a>
						<cfelse>
							<span class="current">#i#</span>
						</cfif>
						<cfset displayDots = false />
					<cfelse>
						<cfif not displayDots>
							<cfif getShowMissingNumbersHTML()>
								<!--- show ... when numbers are skipped --->
								#getMissingNumbersHTML()#
							</cfif>
							<cfset displayDots = true />
						</cfif>
					</cfif>
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfreturn renderedOutput />
	</cffunction>

	<cffunction name="FormatUrl" returntype="string" output="false" access="private" hint="Reformat url">
		<cfargument name="INPURL" type="string" required="true" />
		
		<cfset result = "">
		<cfset isStartParam = false>
		<cfloop index="idx" from="1" to="#len(INPURL)#">
			<cfset character = mid(INPURL, idx, 1)>
			<cfif character EQ "&" OR character EQ "?">
				<cfif NOT isStartParam>
					<cfset character = "?">
					<cfset isStartParam = true>
				<cfelse>
					<cfset character = "&">
				</cfif>
			</cfif>
			<cfset result = result & character>
		</cfloop>
		
		<cfreturn '##'>
	</cffunction>
	<cffunction name="compressHTML" returntype="string" output="false" access="private" hint="Compresses any HTML given.">
		<cfargument name="htmlToCompress" type="string" required="true" />
		<cfset var compressedHTML = htmlToCompress />

		<!--- put tags together --->
		<cfset compressedHTML = reReplace(htmlToCompress, "\>\s+\<", "> <", "ALL") />
		<!--- take out extra spaces --->
		<cfset compressedHTML = reReplace(htmlToCompress, "\s{2,}", chr(13), "ALL") />

		<cfreturn compressedHTML />
	</cffunction>

</cfcomponent>