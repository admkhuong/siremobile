<cfparam name="query" default="">
<cfparam name="INPGROUPID" default="0">

<!--- http://www.bennadel.com/blog/654-Building-A-Table-With-Nested-ColdFusion-Custom-Tags-And-A-ColdFusion-Component.htm --->
<!---
	Check to see which execution mode the tag is running in.
	We won't have access to the child tag data until we are
	in the End tag mode.
--->


<!---

To add sort ordering to headers

Add an additional field to the colModels array
The name must be the DB filed as seen in the query
Display will be determined by the names array
<cfset arrNames = ['Contact String', 'Type', 'Note']>	
<cfset arrColModel = [				
				{name='CONTACTSTRING_VCH', width="230px", sidx="simplelists.contactstring.contactstring_vch"},
				{name='CONTACTTYPENAME_VCH', width="320px", sidx="simplelists.contactstring.contacttype_int"},
				{name='USERSPECIFIEDDATA_VCH',width="125px", sidx="simplelists.contactstring.userspecifieddata_vch"}	
			   ] >
			   
Parameters for sidx and sord need to be specified so they are passed to CFC
<cfset arrParams = [{name='inpGroupId',value="#INPGROUPID#"},{name='sidx',value="#sidx#"},{name='sord',value="#sord#"} ]>		
		
		

And of course CFC needs to accept and support the extra sidx sord parameters
cfc needs to check for SQL injection - 

Calling page needs to cfparams at the top to accept post data
<cfparam name="sidx" default="simplelists.contactstring.contactstring_vch">
<cfparam name="sord" default="ASC">



			   
--->

<cfif query EQ "">
	<cfset filterData = ArrayNew(1)>
<cfelse>
	<cfset filterData = DeserializeJSON(UrlDecode(query))>
	<!--- <cfloop array="#filterData#" index="filterItem">
		<cfset filterItem.FIELD_VAL = UrlDecode(filterItem.FIELD_VAL)>
	</cfloop> --->
</cfif>
<cfinclude template="../../../public/paths.cfm" >

<cfswitch expression="#THISTAG.ExecutionMode#">
 
	<cfcase value="Start">
 		<cfparam
			name="attributes.component"
			type="string" default=""
			/>
		<cfparam
			name="attributes.method"
			type="string" default=""
			/>
		<cfparam
			name="attributes.width"
			type="string" default=""
			/>	
		<cfparam
			name="attributes.name"
			type="string" default=""
			/>		
		<cfparam
			name="attributes.colNames"
			type="array" 
			/>
		<cfparam
			name="attributes.colModels"
			type="array" 
			/>
		
		<cfparam
			name="attributes.colSorts" default= #ArrayNew(1)#
			type="array" 
		/>
		<cfparam
			name="attributes.defaultSortColumn"
			type="integer" default="0"
		/>	
		<cfparam
			name="attributes.defaultSortType"
			type="string" default="DESC"
		/>	
		
		<cfparam
			name="attributes.isPaging"
			type="boolean" default="true" 
			/>	
		<cfparam 
			name="attributes.params"
			type="array" default= #ArrayNew(1)#
		/>
		<cfparam 
			name="attributes.filterkeys" 
			type="array" default="#ArrayNew(1)#"
		/>
		<cfparam 
			name="attributes.filterFields" 
			type="array" default="#ArrayNew(1)#"
		/>
		<cfparam
			name="attributes.topHtml"
			type="string" default=""
			/>
		<cfparam
			name="attributes.tableId"
			type="string" default=""
			/>
            
        <cfparam
			name="attributes.FilterSetId"
			type="string" default="DefaulFS"
			/>   
		<cfparam
			name="attributes.RowClass"
			type="string" default=""
			/> 	 
	</cfcase>
 
 	<!---- If has a close tag----->
	<cfcase value="End">
 		<!--- Get information from database---->
	  <cfscript>
		urlPageNo = 1;
		
		paramPage = attributes.tableId & 'page';
		
		if (structKeyExists(form, paramPage)) {
			urlPageNo = form[paramPage];
		} 
		
		// safety measure for URL pagination variable
		if (not isNumeric(urlPageNo) or urlPageNo LT 1) {
			urlPageNo = 1;
		} 
	  </cfscript>
	  <cfif structKeyExists(form,"filterData")>
      	  <cfset filterData = DeserializeJSON(DeserializeJSON(UrlDecode(form.filterData)))>
	  </cfif>
	  <cfoutput>
		
 			<div id="#attributes.FilterSetId#">
                   
        
          	<cfif Isdefined("attributes.filterkeys") AND ArrayLen(attributes.filterkeys) GT 0>
				<cfset currentURL = getPageContext().getRequest().getRequestURI()>	
                <!--- tranglt --->
                <cfset tmp ="#CompareNoCase(Right(currentURL,10),'/index')#" />
                <cfif tmp EQ 0 >
                	<cfset currentURL = Left(currentURL,Len(currentURL)-9) />
                </cfif>
                <!--- end tranglt --->
                
                    <cfform id="filters_content">
                        <cfinclude template="filter.cfm" >
                    </cfform>
               
			</cfif>
            
       <!--- <cfdump var="#attributes.params#">
            
		<cfif ArrayLen(attributes.params) GT 0>
           <cfloop array="#attributes.params#" index="paramIndex">
                <cfdump var="#paramIndex.name#">
                <cfdump var="#paramIndex.value#">
           </cfloop>
        </cfif>--->
		  
		  <cfif structKeyExists(form,"sortData")>
		  	  <cfset sortData = DeserializeJSON(DeserializeJSON(UrlDecode(form.sortData)))>
		  </cfif>

          <cfset Currsidx = "">     
          <cfset Currsord = "">
		  <cfinvoke 
		       component="#attributes.component#"
		       method="#attributes.method#"
		       returnvariable="dbResult">     
			     <cfinvokeargument name="page" value="#urlPageNo#"/>  
			   <cfif ArrayLen(attributes.params) GT 0>
				   <cfloop array="#attributes.params#" index="paramIndex">
                   		
                        <!--- Better of here as optional parameters so invlaid args are not passed to cfc with no sidx or sord params--->
                        <cfif UCASE("#paramIndex.name#") EQ "SIDX">
                        	<cfset Currsidx = "#TRIM(paramIndex.value)#">
                        </cfif>
                        
                        <cfif UCASE("#paramIndex.name#") EQ "SORD">
                        	<cfset Currsord = "#TRIM(paramIndex.value)#">
                        </cfif>
                   
                   		<!--- Crashes on parameters that have vlaues that are empty strings --->
                   		<cfif LEN(TRIM(paramIndex.value)) GT 0>
    						<cfinvokeargument name="#paramIndex.name#" value="#paramIndex.value#"/>  
                        </cfif>
				   </cfloop>
			   </cfif>
			   	<cfif Arraylen(filterData) GT 0>
				 	<cfinvokeargument name="filterData" value="#filterData#">
					 
				</cfif>
				<cfif Arraylen(attributes.colSorts) GT 0>
					<cfinvokeargument name="sortData" value="#sortData#">
				</cfif>
				<cfset isSortable = false />
				<cfloop index="intRow" from="1" to="#ArrayLen(attributes.colModels)#">
				 	<cfset ColModel = attributes.colModels[intRow]>
					  <cfif structKeyExists(ColModel,'sortObject')>
							<cfset isSortable = true />
							<cfbreak>							
					  </cfif>
				</cfloop>
				<cfif isSortable>
					<cfinvokeargument name="sortData" value="#sortData#">
				</cfif>
		  </cfinvoke> 
          <!---<cfdump var="#dbResult#">--->
          
		  	<cfif attributes.topHtml NEQ ''>
		  		#attributes.topHtml#
		  	</cfif>


			<!--- Set hidden form values --->
			
			<table cellpadding="0"  cellspacing="0" class="cf_table" style="width: #attributes.width#">
				<!--- Header name----->
				<tr>
				<cfloop index="intRow" from="1" to="#ArrayLen(attributes.colNames)#">
					<cfif attributes.colNames[intRow] EQ 'CHECKBOX'>
						<th style="font-family:Verdana; color:##ffffff;border-bottom: 2px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');">
							<input type="checkbox" id="chek_box_all" class="all_chek_box" onclick="#attributes.FilterSetId#clickAll(this)">
						</th>
					<cfelse>                    
                    	<cfif StructKeyExists(attributes.colModels[intRow],"sidx")>
                        
                        	<cfif CompareNoCase(Currsidx,"#attributes.colModels[intRow].sidx#") EQ 0> 
                            
                            	<cfif UCASE(TRIM(Currsord)) EQ "ASC">
    	                        	<cfset CurrImgArrow = '<img style="display:inline;" src="#rootUrl#/#publicPath#/images/navigation/arrow_up_blue.png" />'>	    
        						<cfelseif UCASE(TRIM(Currsord)) EQ "DESC">
        							<cfset CurrImgArrow = '<img style="display:inline;" src="#rootUrl#/#publicPath#/images/navigation/arrow_down_blue.png" />'>
	                            <cfelse>
                                	<cfset CurrImgArrow = ''>	
                                </cfif>
                                <cfset currColModel = attributes.colModels[intRow]>
                    			<cfif structKeyExists(currColModel,'sortObject')>
									<cfset currSort = currColModel.sortObject>
									<th style="font-family:Verdana; color:##ffffff;border-bottom: 2px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#; text-align:center;" class="SortHeader" rel1="#attributes.colModels[intRow].sidx#" rel2="#Currsord#">
								 		<div style="position: relative;" id="#currSort.sortColName#" title="Click to order this column"  href="javascript:void(0);" onclick="gridOrdering(this,'#currSort.sortColName#','#currSort.sortType#')">
								 		#attributes.colNames[intRow]# #CurrImgArrow# &nbsp;
										<cfif currSort.isDefault EQ 'true'>
											<cfif currSort.sortType EQ 'DESC'>
												<img style="position:absolute; right: 1px;top: 8px;float:right;" valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_desc.png">
											<cfelse>
												<img style="position:absolute; right: 1px;top: 8px;float:right;" valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_asc.png">
											</cfif>
										<cfelse>
											<img style="position:absolute; right: 1px;top: 8px;float:right;" valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_none.png">
										</cfif>
										</div>
										
									</th>
								<cfelse>
								 	<th style="font-family:Verdana; color:##ffffff;border-bottom: 3px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#; text-align:center;" class="SortHeader" rel1="#attributes.colModels[intRow].sidx#" rel2="#Currsord#">
								 		#attributes.colNames[intRow]# #CurrImgArrow#
									</th>
								</cfif>
                            <cfelse>
								<cfset currColModel = attributes.colModels[intRow]>
            					<cfif structKeyExists(currColModel,'sortObject')>		
									<cfset currSort = currColModel.sortObject>							
									<th style="font-family:Verdana; color:##ffffff;border-bottom: 3px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#;" class="SortHeader" rel1="#attributes.colModels[intRow].sidx#" rel2="">
										<div style="position: relative;" id="#currSort.sortColName#" title="Click to order this column" href="javascript:void(0);" onclick="gridOrdering(this,'#currSort.sortColName#','#currSort.sortType#')">
										#attributes.colNames[intRow]# &nbsp;										
										<cfif currSort.isDefault EQ 'true'>
											<cfif currSort.sortType EQ 'DESC'>
												<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_desc.png">
											<cfelse>
												<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_asc.png">
											</cfif>
										<cfelse>
											<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_none.png">
										</cfif>
										</div>
									</th>
								<cfelse>
								 	<th style="font-family:Verdana; color:##ffffff;border-bottom: 3px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#;" class="SortHeader" rel1="#attributes.colModels[intRow].sidx#" rel2="">
										#attributes.colNames[intRow]#
									</th>
								</cfif>
                        	</cfif>    
                        <cfelse>
							 <cfset currColModel = attributes.colModels[intRow]>
                    			<cfif structKeyExists(currColModel,'sortObject')>
									<cfset currSort = currColModel.sortObject>
									<th style="font-family:Verdana; color:##ffffff;border-bottom: 3px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#;">
										<div style="cursor:pointer; position: relative;" id="#currSort.sortColName#" title="Click to order this column" href="javascript:void(0);" onclick="gridOrdering(this,'#currSort.sortColName#','#currSort.sortType#')">
										#attributes.colNames[intRow]# &nbsp;
										<cfif currSort.isDefault EQ 'true'>
											<cfif currSort.sortType EQ 'DESC'>
												<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_desc.png">
											<cfelse>
												<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_asc.png">
											</cfif>
										<cfelse>
											<img style="position:absolute; right: 1px;top: 8px;float:right;"  valign="middle" src="#rootUrl#/#publicPath#/images/dock/sort_none.png">
										</cfif>
										</div>
									</th>
                        	<cfelse>
							 	<th style="font-family:Verdana; color:##ffffff;border-bottom: 3px solid ##FF6633;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_table.jpg');width: #attributes.colModels[intRow].width#;">#attributes.colNames[intRow]#</th>
							</cfif>
                        </cfif>    
					</cfif>
				</cfloop>
				</tr>
				<!---set current sort column and sort type--->
					<cfif StructKeyExists(dbResult, 'SORT_COLUMN')>
						<input type="hidden" id="currSortCol" name="currSortCol" value="#dbResult.SORT_COLUMN#" >
						<input type="hidden" id="currSortType" name="currSortType" value="#dbResult.SORT_TYPE#" >
					<cfelse>
						<input type="hidden" id="currSortCol" name="currSortCol" value="" >
						<input type="hidden" id="currSortType" name="currSortType" value="" >
					</cfif>
				<!---end set current sort column and sort type--->	
				
				
				<!--- Fill data from database----->
				<cfset i = 0>
				<cfloop array="#dbResult.ROWS#" index="rowData">
				<cfset i = i + 1 >
				<tr class="#attributes.RowClass#">
					<cfloop index="intRow" from="1" to="#ArrayLen(attributes.colModels)#" step="1">	
						<!--- add more custom class for table data----->
						<td style="width: #attributes.colModels[intRow].width#; <cfif i % 2 eq 0 >background-color:##f3f9fd;</cfif> border-left:1px solid ##ccc;" <cfif StructKeyExists(attributes.colModels[intRow], 'class')>class="#attributes.colModels[intRow].class#"</cfif> <cfif StructKeyExists(attributes.colModels[intRow], 'align')>align="#attributes.colModels[intRow].Align#"</cfif>>
							<cfif attributes.colModels[intRow].name EQ 'CHECKBOX'>
							
								<cfset i = 1>
								<cfset chek_Value = "">
								<cfloop array="#attributes.colModels[intRow].keys#" index="key">
									<cfif i EQ ArrayLen(attributes.colModels[intRow].keys)>
										<cfset chek_Value = chek_Value & "#rowData[key]#">
									<cfelse>
										<cfset chek_Value = chek_Value & "#rowData[key]#;">
									</cfif>
									<cfset i = i + 1>
								</cfloop>
								<cfif NOT StructKeyExists(attributes.colModels[intRow],"isCheckShowVal")>
									<input type="checkbox" id="#chek_Value#" name="checkbox" class="chek_box" 
											value="<cfoutput>#chek_Value#</cfoutput>" 
											onclick="#attributes.FilterSetId#clickCheckBox(this)">
								<cfelse>
									<cfif rowData[attributes.colModels[intRow].isCheckShowVal]>
										<input type="checkbox" id="#chek_Value#" name="checkbox" class="chek_box" 
											value="<cfoutput>#chek_Value#</cfoutput>" 
											onclick="#attributes.FilterSetId#clickCheckBox(this)">
									</cfif>
								</cfif>
							<cfelse>
								<cfif StructKeyExists(attributes.colModels[intRow], "format")>
									<cfloop array="#attributes.colModels[intRow].format#" index="formatIndex">
                                    
                                    <!---	<cfdump var="#attributes.colModels#">
                                        <BR />XXX
                                        <cfdump var="#formatIndex#">
                                        <BR />YYY
                                        <cfdump var="#attributes.colModels[intRow].name#">
                                    	<BR />ZZZ
                                    	<cfdump var="#rowData#">--->
                                    
										<!--- Match exactly format and replace----->
										<cfif formatIndex.name EQ rowData[attributes.colModels[intRow].name]>
											<cfset keyList= GetReplaceKeys(formatIndex.html)>
											<cfset cusFormat = formatIndex.html>
											<cfloop index = "key" list = "#keyList#"> 
											    <cfset cusFormat = Replace(cusFormat, "{%#key#%}","#rowData[key]#","all")>
											</cfloop>
											#cusFormat#
										</cfif>
									</cfloop>
								<cfelse>
										<!---<cfdump var="#attributes.colModels#">	
                                        <cfdump var="#intRow#">
                                        <cfdump var="#rowData#">
                                        <cfdump var="#attributes.colModels[intRow]#">--->
                                                                        
                                	<cfif StructKeyExists(attributes.colModels[intRow], "isHtmlEncode") AND NOT attributes.colModels[intRow].isHtmlEncode>
										#rowData[attributes.colModels[intRow].name]#
									<cfelse>
                                        #HtmleditFormat(rowData[attributes.colModels[intRow].name])#										
									</cfif>
								</cfif>
							</cfif>
							
						</td>
					</cfloop>
				</tr>
				</cfloop>
				
				<!--- For pagination---->
				<cfset 	pagination = createObject("component", "Pagination").init() />
				<cfif dbResult.RECORDS GT 0>
					<tr class="table_footer">
						<td colspan="50" style="height:46px;margin:0; border-top:3px solid ##0099cc; padding-right: 10px;background-image:url('#rootUrl#/#PublicPath#/images/backgroud_bottom_table.jpg');">
						
						<cfif attributes.isPaging>
							
							
							<cfscript>
								totalPage = dbResult.TOTAL;
								if(totalPage == 0){
									totalPage = 1;
								}
								if(!isDefined("sortData")){
									sortData = ArrayNew(1);
								}
								pagination.setItemsPerPage(ArrayLen(dbResult.ROWS));
								
								pagination.setTotalNumberOfPages(totalPage);
								pagination.setClassName('flickr');
								pagination.setShowNumericLinks(true);
								pagination.setNumericDistanceFromCurrentPageVisible(3);
								pagination.setNumericEndBufferCount(0);
								pagination.setShowMissingNumbersHTML(true);
								pagination.setURLPageIndicator(attributes.tableId & 'page');
								pagination.setFilterData(serializeJSON(filterData));
								pagination.setSortData(serializeJSON(sortData));
							</cfscript>
							<div class='cf_table_message'>
								<span id="totalItem" style="display:none">#dbResult.RECORDS#</span>
								<cfif pagination.getTotalNumberOfPages() EQ pagination.getCurrentPage()>
									#dbResult.RECORDS - pagination.getItemsPerPage() + 1# - #dbResult.RECORDS# of #dbResult.RECORDS#
								<cfelse>
									#pagination.getStartRow()# - #pagination.getStartRow() + pagination.getItemsPerPage() - 1# of #dbResult.RECORDS# 
								</cfif>
								
						  	</div>
						  	 <!---- Paging area------>
						  	 <div class="cf_table_paper">
							 	 #pagination.getRenderedHTML()#
							</div>
						</cfif>
						</td>
					
				</cfif>
			</table>
			<script type="text/javascript">
				
				<!--- Make current filter data available to jQuery AJAX calls--->
				<cfoutput>						
					var #toScript(filterData, attributes.FilterSetId & 'filterDataArr')#;					
					var #attributes.FilterSetId#filterData = '';
				</cfoutput>	
				
				$(document).ready(function(){
					$("###attributes.FilterSetId# .page_button").click(function(){
						
						var filterData = decodeURI($(this).attr('filterData')); 
						var sortData = decodeURI($(this).attr('sortData')); 
						var page= $(this).attr('page');
					
						var data = {
							filterData : JSON.stringify(filterData),
							sortData : JSON.stringify(sortData),
							INPGROUPID: '<cfoutput>#INPGROUPID#</cfoutput>', <!--- Added for Group navigation is there a better way to pass this param via form post vs just including it by default? --->
							"<cfoutput>#attributes.tableId & 'page'#</cfoutput>" : page
						};
						<cfset currentURL = getPageContext().getRequest().getRequestURI()>	
						<cfset tmp ="#CompareNoCase(Right(currentURL,10),'/index')#" />
		                <cfif tmp EQ 0 >
		                	<cfset currentURL = Left(currentURL,Len(currentURL)-9) />
		                </cfif>
						post_to_url('', data, 'POST');
						return false;
					});
					
					$('##<cfoutput>#attributes.FilterSetId#</cfoutput> ##totalFilterResult').html('#dbResult.RECORDS#');		
					
					<!--- CF converts to a complex javascript type that needs to be converted to json in order to pass to a cfc--->
					<cfoutput>#attributes.FilterSetId#</cfoutput>filterData = JSON.stringify( <cfoutput>#attributes.FilterSetId#</cfoutput>filterDataArr );
										
					<!--- Set up sorting events    ##<cfoutput>#attributes.FilterSetId#</cfoutput>  ###attributes.FilterSetId# .cf_table --->
					$("###attributes.FilterSetId# .SortHeader").click(function(){
						
						<!--- Reload page under new sort conditions --->
						
						var Nextsidx = $(this).attr("rel1");
						var Nextsord = "";
						
						if($(this).attr("rel2") == "")
							Nextsord = "ASC";
						else if($(this).attr("rel2") == "DESC")
							Nextsord = "ASC";
						else
							Nextsord = "DESC";
																					
						
						<!--- Resend current filters with new sort info --->
												
						var submitLink = $(this).attr("href");
						var filterForm = $("##<cfoutput>#attributes.FilterSetId#</cfoutput> ##filters_content"); <!---document.getElementById("filters_content");--->
						if(filterForm == null)
						{														
							<cfset StartCommaFlag = 0>							
							<cfset OutParams = "">
							<cfif ArrayLen(attributes.params) GT 0>
							   <cfloop array="#attributes.params#" index="paramIndex">
										
									<cfif StartCommaFlag GT 0>
										<cfset OutParams = OutParams & ", #paramIndex.name#:'#TRIM(paramIndex.value)#'">
									<cfelse>
										<cfset OutParams = OutParams & "#paramIndex.name#:'#TRIM(paramIndex.value)#'">
										<cfset StartCommaFlag = 1>
									</cfif>  								
									
							   </cfloop>
							</cfif>
		
										
							<cfoutput>		
																	
								var params = {#OutParams#};
														
								params["sord"] = $(this).attr("rel1");
								params["sidx"] = Nextsord;															
																
								post_to_url(submitLink, params, 'POST');			
							</cfoutput>
				
						}
						else
						{
							<cfoutput>#attributes.FilterSetId#</cfoutput>reIndexFilterID();
							$("###attributes.FilterSetId# ##filters_content").attr("action", submitLink);
							
							// Check if dont user filter
							if($("###attributes.FilterSetId# ##isClickFilter").val() == 0)
							{
								$("###attributes.FilterSetId# ##totalFilter").val(0);
							}
							
							<!---$('###attributes.FilterSetId# ##sidx').val(Nextsidx);
							$('###attributes.FilterSetId# ##sord').val(Nextsord);--->
							
							
							var inputsidx = document.createElement('input');
							inputsidx.type = 'hidden';
							inputsidx.name = 'sidx';
							inputsidx.value = Nextsidx;
							$("###attributes.FilterSetId# ##filters_content").append(inputsidx);
	
	
							var inputsord = document.createElement('input');
							inputsord.type = 'hidden';
							inputsord.name = 'sord';
							inputsord.value = Nextsord;
							$("###attributes.FilterSetId# ##filters_content").append(inputsord);
	
							
							$("###attributes.FilterSetId# ##filters_content").submit();
						 }
					
					
						
					});
					
					
					//begin reset current sort
					var currSortCol = $("###attributes.FilterSetId# ##currSortCol").val();
					var currSortType = $("###attributes.FilterSetId# ##currSortType").val();
					if(currSortCol !='' && currSortType !='')
					{
						var obj =$("###attributes.FilterSetId# .cf_table tbody tr th div[id='"+currSortCol+"']");
						$("###attributes.FilterSetId# .cf_table tbody tr th").each(function(){
							$(this).find('img').attr("src", '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/dock/sort_none.png');
						})
						if(currSortType == "DESC"){
							$(obj).attr("onclick","gridOrdering(this,'" + currSortCol + "','ASC')");
							$(obj).find('img').attr("src", '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/dock/sort_desc.png');
						}
						else{
							$(obj).attr("onclick","gridOrdering(this,'" + currSortCol + "','DESC')");
							$(obj).find('img').attr("src", '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/dock/sort_asc.png'); 
						}
					}  
					//end reset current sort
				});
				
				
			</script>
            
            
           </div> <!---<div id="#attributes.FilterSetId#">--->
		</cfoutput>
	</cfcase>
 
</cfswitch>

<cffunction name="GetReplaceKeys">
    <cfargument name="object" required="yes" default="">
	
	<cfset results = ""/>        

	<cfloop index="idx" from="1" to="#len(object) - 1#">
		<cfset startMarkChars = mid(object, idx, 2)>
		<cfif startMarkChars EQ "{%">
			<cfloop index="jdx" from="#idx + 2#" to="#len(object) - 1#">
				 <cfset startMarkChars = mid(object, jdx, 2)>
				<cfif startMarkChars EQ "%}">
					<cfset element =  mid(object, idx + 2,  jdx - idx - 2)>
					<cfif Listfind(results, element) EQ 0>
						<cfset results = ListAppend(results, element)>
					</cfif>
					<cfset idx = jdx + 1> 
					<cfbreak>
				</cfif> 
			</cfloop>
		</cfif>
	</cfloop>
	<cfreturn results>
</cffunction>

