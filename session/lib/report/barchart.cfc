﻿<cfcomponent name="Barchart" hint="draw bar chart with return data in 2 types fusion chart and high chart" output="false">
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../../cfc/Includes/Functions.cfm" >
    <cfinclude template="../../cfc/Includes/PageLayout.cfm">
    <cfinclude template="../../cfc/Includes/FusionCharts.cfm">
	
	<cffunction name="init" hint="default contructor function">
		<cfset variables.title = "">
        <cfset variables.ResetLink = "">
        <cfset variables.DashboardHeader = "">
		<cfset variables.chartId= "">
		<cfset variables.chartType = 1>
		<cfset variables.data = ArrayNew(1)>
		<cfset variables.category = ArrayNew(1)>
		<cfset variables.yAxisTitle = "">
        <cfset variables.xAxisTitle = "">
		<cfset variables.chartWidth="700">
		<cfset variables.chartHeight="300">
		<cfset variables.averageAvailable = false>
	     
		<!--- MORE COLORS http://colorschemedesigner.com/ MONO
        BASE COLORS F18922 8DC050 785A3A 1A8FC9 681D2A FFFA00
		
		rEGULAR
		
		 "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73",
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300" 
			   
		iNVERTED 
 			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73" 
			   
        --->
        <cfset variables.colorArr= [
			  
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300" 
			]>  
            
		<cfreturn this>
	</cffunction>
	
	<cffunction name="drawChart">
		<cfif variables.chartType EQ 1>
			<cfreturn drawFusionChart()/>
		<cfelseif  variables.chartType EQ 2>
			<cfreturn drawHighChart()/>	
		<cfelse>
			<cfreturn drawAmChart()/>	
		</cfif>
	</cffunction>
	
	<cffunction name="drawFusionChart" returntype="any" access="public" hint="Draw option for fusionchart">
    	<cfset strXML = ""> 
	    <cfset strXML = "<chart yAxisName='#getYAxisTitle()#' formatNumberScale='1' placeValuesInside='1' caption='#getTitle()#' decimals='0'  showBorder='0'>">
      	<cfif arraylen(variables.data) gt 0>
		   	<cfloop index="i" from="1" to="#Arraylen(getData())#">
           		<cfset strXML = strXML & "<set label='" & #getCategory()[i]# & "' value='" & #getData()[i]# & "' />">
           	</cfloop>
           	<cfif getAverageAvailable()>
			   	<cftry>
       				<cfset strXML = strXML & "<trendlines><line startValue='#evaluate(arraysum(getData())/Arraylen(getData()))#' displayValue='AVG' color='009900' valueOnRight='1' /></trendlines>">
                	<cfcatch type="expression">
                    	<cfset strXML = strXML & "<trendlines><line startValue='0' displayValue='AVG' color='009900' valueOnRight='1' /></trendlines>">
                	</cfcatch>
                </cftry>
		   	</cfif>
       </cfif>
       <cfset strXML = strXML & "</chart>">
		<cfreturn renderChartHTML("#rootUrl#/#sessionPath#/reporting/FusionCharts/Column3D.swf", "", strXML, "#getChartId()#", '#getWidth()#', '#getHeight()#', false, false)>
	</cffunction>
	
	<cffunction name="drawHighChart" returntype="any" access="public" hint="Draw option for fusionchart" output="true">
		<cfset averageArr = ArrayNew(1)>
		<cfif Arraylen(getData()) gt 0 >
			<cfset averageData = 0>
			<cftry>
		        <cfset averageData = evaluate(arraysum(getData())/Arraylen(getData()))>
	        <cfcatch type="Any" >
				<cfset averageData = 0>
	        </cfcatch>
	        </cftry>
	        
	        <cfset colorColumnData = ArrayNew(1)>
	        <cfloop index = i from="1" to ="#Arraylen(getData())#">
				<cfset ArrayAppend(averageArr,averageData)>
				<!---set column color--->
				<cfset item = {
				 	"y"= getData()[i],
	                "color"= variables.colorArr[i%(arraylen(variables.colorArr)) + 1]
				 }>
				 <cfset ArrayAppend(colorColumnData, item)>
			</cfloop>
        </cfif>
        
		<cfset highChart = {
			        "chart"= {
			        },
			        "title"= {
			            "text"= '#getTitle()#',
			            "x"= -20 
			        },
			        "xAxis"= {
			            "categories"= getCategory()
			        },
			        "yAxis"= {
			        	"min"= 0,
			            "title"= {
			                "text"= '#getYAxisTitle()#'
			            }
			        },
			        "plotOptions"= {
		                "column"= {
		                    "pointPadding"= 0.2,
		                    "borderWidth"= 0
		                }
		            },
			         "tooltip"= {
			    	    "pointFormat"= '<b>{point.y:.0f}</b>'
			        },
			        "legend"= {
		                "enabled"= false
		            },
			        "series"= [
						{
				            "name"= '',
				            "data"= colorColumnData,
				            "type"="column"
				        }
					]
			    }>
	    <cfif getAverageAvailable() EQ true>
			<cfset avg = {	"type"= 'line',
			                "name"= 'Avarage Line',
			                "data"= averageArr,
			                "marker"= {
			                    "enabled"= false
			                },
			                "states"= {
			                    "hover"= {
			                        "lineWidth"= 0
			                    }
			                },
			                "enableMouseTracking"= true
		                }>
			<cfset ArrayAppend(highChart["series"] ,avg)>
		</cfif>
	    <cfreturn highChart />
	</cffunction>
	
    
    <!--- to get lables ... chart.addLabel(20,"100%","Fees $","center",11,"##000000",270); --->
	<cffunction name="drawAmChart" returntype="any" access="public" hint="Draw option for amChart" output="true">
		<cfset amChart="">
		<cfif Arraylen(getData()) gt 0 >
			<cfset averageData = 0>
			<cftry>
		        <cfset averageData = evaluate(arraysum(getData())/Arraylen(getData()))>
	        <cfcatch type="Any" >
				<cfset averageData = 0>
	        </cfcatch>
	        </cftry>
	        
			<cfsavecontent variable="amChartContent">
				var chart;
			    var chartData = [
			    <cfloop index = i from="1" to="#Arraylen(getData())#">
					{<cfoutput>category:'#getcategory()[i]#',
								data:'#getdata()[i]#'</cfoutput>,
								avg:'<cfoutput>#averageData#</cfoutput>',
								<!---color:  '<cfoutput>#variables.colorArr[i%(arraylen(variables.colorArr)) + 1]#</cfoutput>'--->
                                color: [ <cfoutput>'#variables.colorArr[i%(arraylen(variables.colorArr)) + 1]#', '#variables.colorArr[(i+12)%(arraylen(variables.colorArr)) + 1]#'   </cfoutput>  ]
								}
					<cfif #i# lt #Arraylen(getData())#>,</cfif>
				</cfloop>
			    ];
				chart = new AmCharts.AmSerialChart();
				chart.dataProvider = chartData;
				chart.categoryField = 'category';
				chart.startDuration = 1;
                
                <cfif TRIM(variables.yAxisTitle) NEQ "">
					chart.addLabel(3,"100%","#variables.yAxisTitle#","center",11,"##000000",270);
                </cfif>
                                              
                <cfif TRIM(variables.xAxisTitle) NEQ "">
					chart.addLabel("3","!15","#variables.xAxisTitle#","center",11,"##000000",0);
                </cfif>
                
                <cfif TRIM(variables.title) NEQ "">
                	<!---chart.addLabel(50, 40, "Beer Consumption by country", "left", 15, "##000000", 0, 1, true);--->                
					chart.addLabel(5,3,"#variables.title#","center",12,"##000000",0,1,true);
                </cfif>
                    
				var categoryAxis = chart.categoryAxis;
				categoryAxis.labelRotation = 45;
				categoryAxis.gridPosition = 'start';
				chart.startDuration = 1;
                
                chart.depth3D = 20;
			    chart.angle = 30;
                
				var graph = new AmCharts.AmGraph();
				graph.valueField = 'data';
				graph.balloonText = '[[category]]: [[value]]';
				graph.type = 'column';
				graph.lineAlpha = 0;
				graph.fillAlphas = 0.8;
			  	graph.colorField = "color";
				chart.addGraph(graph);
				
				<cfif getAverageAvailable() EQ true>
					var avgGraph = new AmCharts.AmGraph();
	                avgGraph.type = "line";
	                avgGraph.title = "average";
	                avgGraph.valueField = "avg";
	                avgGraph.lineThickness = 2;
	                avgGraph.bullet = "round";
	                chart.addGraph(avgGraph);
				</cfif>
                
               <!--- 
                 var legend = new AmCharts.AmLegend();
                                legend = new AmCharts.AmLegend();
                                legend.position = "bottom";
                                legend.align = "right";
                                legend.markerType = "square";
                                legend.valueText = "aa";
                                legend.labelText = "RESET";
                chart.addLegend(legend);
                --->
                                                
                <cfoutput>
					<cfif variables.ResetLink NEQ "" >
                        #variables.ResetLink#
                    </cfif> 
                    
                    <cfif variables.DashboardHeader NEQ "" >
                        #variables.DashboardHeader#
                    </cfif> 
                </cfoutput>
            
                
			</cfsavecontent>
			<cfset amChart = amChart & amChartContent>
		</cfif>
		<cfreturn amChart>
	</cffunction>
	
    <cffunction name="setResetLink" returntype="void" output="false" access="public" hint="Set optional Reset Link for chart">
		<cfargument name="inpResetLink" type="string" />
		<cfset variables.ResetLink = arguments.inpResetLink >
	</cffunction>
    
    <cffunction name="setDashboardHeader" returntype="void" output="false" access="public" hint="Set optional title for Dashboard header">
		<cfargument name="inpDashboardHeader" type="string" />
		<cfset variables.DashboardHeader = arguments.inpDashboardHeader >
	</cffunction>
    
    <cffunction name="setAverageAvailable" returntype="void" output="false" access="public" hint="Set Average Available for chart">
		<cfargument name="_averageAvailable" type="boolean" />
		<cfset variables.averageAvailable = arguments._averageAvailable >
	</cffunction>
	
	<cffunction name="getAverageAvailable" returntype="boolean" access="public" hint="Get average available of chart">
		<cfreturn variables.averageAvailable>
	</cffunction>

	<cffunction name="setWidth" returntype="void" output="false" access="public" hint="Set width for chart">
		<cfargument name="_chartWidth" type="string" />
		<cfset variables.chartWidth = arguments._chartWidth >
	</cffunction>
	
	<cffunction name="getWidth" returntype="string" access="public" hint="Get width of chart">
		<cfreturn variables.chartWidth>
	</cffunction>
	
	<cffunction name="setHeight" returntype="void" output="false" access="public" hint="Set Height for chart">
		<cfargument name="_chartHeight" type="string" />
		<cfset variables.chartHeight = arguments._chartHeight >
	</cffunction>
	
	<cffunction name="getHeight" returntype="string" access="public" hint="Get Height of chart">
		<cfreturn variables.chartHeight>
	</cffunction>

	<cffunction name="setYAxisTitle" returntype="void" output="false" access="public" hint="Set yAxisTitle for chart">
		<cfargument name="_yAxisTitle" type="string" />
		<cfset variables.yAxisTitle = arguments._yAxisTitle >
	</cffunction>
	
	<cffunction name="getYAxisTitle" returntype="string" access="public" hint="Get yAxisTitle of chart">
		<cfreturn variables.yAxisTitle>
	</cffunction>
    
    <cffunction name="setXAxisTitle" returntype="void" output="false" access="public" hint="Set xAxisTitle for chart">
		<cfargument name="_xAxisTitle" type="string" />
		<cfset variables.xAxisTitle = arguments._xAxisTitle >
	</cffunction>
	
	<cffunction name="getXAxisTitle" returntype="string" access="public" hint="Get xAxisTitle of chart">
		<cfreturn variables.xAxisTitle>
	</cffunction>

	<cffunction name="setChartId" returntype="void" output="false" access="public" hint="Set Id for chart">
		<cfargument name="_chartId" type="string" />
		<cfset variables.chartId = arguments._chartId >
	</cffunction>
	
	<cffunction name="getChartId" returntype="string" access="public" hint="Get id of chart">
		<cfreturn variables.chartId>
	</cffunction>

	<cffunction name="setChartType" returntype="void" output="false" access="public" hint="Set type for chart">
		<cfargument name="_chartType" type="string" />
		<cfset variables.chartType = arguments._chartType >
	</cffunction>
	
	<cffunction name="getChartType" returntype="string" access="public" hint="Get type of chart">
		<cfreturn variables.chartType>
	</cffunction>
	
	<cffunction name="setTitle" returntype="void" output="false" access="public" hint="Set title for chart">
		<cfargument name="_Title" type="string" />
		<cfset variables.title = arguments._Title >
	</cffunction>
	
	<cffunction name="getTitle" returntype="string" access="public" hint="Get title for chart">
		<cfreturn variables.title>
	</cffunction>
	
	<cffunction name="setData" returntype="void" output="false" access="public" hint="Set data for chart, number of items of data must equal category's">
		<cfargument name="_data" type="Array" />
		<cfset variables.data = arguments._data >
	</cffunction>
	
	<cffunction name="getData" returntype="array" output="false" access="public" hint="get data for chart">
		<cfreturn variables.data>
	</cffunction>
	
	<cffunction name="setCategory" returntype="void" output="false" access="public" hint="Set category for chart, number of items of category must equal data's ">
		<cfargument name="_category" type="Array" />
		<cfset variables.category = arguments._category >
	</cffunction>
	
	<cffunction name="getcategory" returntype="array" output="false" access="public" hint="get category for chart">
		<cfreturn variables.category>
	</cffunction>
</cfcomponent>