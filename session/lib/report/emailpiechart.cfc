﻿<cfcomponent displayname="Piechart" hint="draw piechart with return data in 2 types fusion chart and high chart" output="false">
	<cfinclude template="../../../public/paths.cfm" >
	<cfinclude template="../../cfc/Includes/Functions.cfm" >
    <cfinclude template="../../cfc/Includes/PageLayout.cfm">
    <!--- <cfinclude template="../../cfc/Includes/FusionCharts.cfm"> --->
    
	<cffunction name="init" hint="default contructor function">
		<!---set default value--->
		<cfset variables.title = "">
		<cfset variables.chartType = 1>
		<cfset variables.data = ArrayNew(1)>
         
		
        <cfset variables.colorArr= [
			  
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300" 
			]>
                		
		<cfreturn this/>
	</cffunction>

	<cffunction name="drawChart" returntype="any" access="public" hint="Draw option">
	    <cfargument name="ShowLegend" type="string" default="false" required="no"/>
		<cfif variables.chartType EQ 1>
			<cfreturn drawFusionChart()/>
		<cfelseif  variables.chartType EQ 2>
			<cfreturn drawHighChart()/>	
		<cfelse>
			<cfreturn drawAmChart(ShowLegend)/>	
		</cfif>
	</cffunction>
	
	
	
	<cffunction name="drawAmChart" returntype="string" access="public" hint="Draw option for amChart">
    	<cfargument name="ShowLegend" type="string" default="false"/>
    	<cfset amChart="">
		
		<cfset chartData = Arraynew(1)>
		<cfloop index="i" from="1" to="#Arraylen(getData())#">
			
			<cfset tempData = {}>				
			<cfset tempData.category =	#getData()[i][1]#>		
			<cfset tempData.value =	#getData()[i][2]#>		
			<cfset ArrayAppend(chartData,tempData)>				
		</cfloop>
		
		<cfsavecontent variable="amChartContent">
        	var chart;
            var legend;
           
			var chartData;
 			<cfoutput>#toScript(chartData, "chartData")#</cfoutput>
            
            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = chartData;
            chart.titleField = "category";
            chart.valueField = "value";
            chart.outlineColor = "#FFFFFF";
            chart.outlineAlpha = 0.8;
            chart.outlineThickness = 2;
            chart.gradientRatio = [-0.2,0.2];
            // this makes the chart 3D
            chart.depth3D = 15;
            chart.angle = 30;
            chart.colors = [ <cfoutput>"#variables.colorArr[1]#"<cfloop index="i" from="2" to="#Arraylen(variables.colorArr)#">, "#variables.colorArr[i]#"</cfloop></cfoutput> ];
            
            <cfif CompareNoCase(ShowLegend,"true") EQ 0>  
				<!--- // LEGEND--->
                legend = new AmCharts.AmLegend();
                legend.align = "center";
                legend.markerType = "circle";
                chart.addLegend(legend);
            </cfif>
            
		</cfsavecontent>
		<cfset amChart = amChart & amChartContent>
		<cfreturn amChart>
	</cffunction>
	
	
	
	<cffunction name="setTitle" returntype="void" output="false" access="public" hint="Set title for chart">
		<cfargument name="_Title" type="string" />
		<cfset variables.title = arguments._Title >
	</cffunction>
	
	<cffunction name="getTitle" returntype="string" access="public" hint="Get title for chart">
		<cfreturn variables.title>
	</cffunction>
	
	<cffunction name="setData" returntype="void" output="false" access="public" hint="Set title for chart">
		<cfargument name="_Data" type="Array" />
		<cfset variables.data = arguments._Data >
	</cffunction>

	<cffunction name="getData" returntype="Array" access="public" hint="get data for chart">
		<cfreturn variables.data>
	</cffunction>
	
	<cffunction name="setChartType" returntype="void" output="false" access="public" hint="Set chart type to draw 1 for fusion chart, 2 for high chart">
		<cfargument name="_ChartType" type="numeric" />
		<cfset variables.chartType = arguments._ChartType >
	</cffunction>

	<cffunction name="getChartType" returntype="numeric" access="public" hint="Get chart type">
		<cfreturn variables.chartType>
	</cffunction>
</cfcomponent>