<cffunction name="init" output="false"><cfreturn this /></cffunction>

<cffunction name="encodeStringXml" access="public" returntype="string" output="false" hint="Encode several special characters that make xml invalid">
	<cfargument	name="string" type="string" required="true" hint="" />
	<cfset string = Replace(string, '&', '&amp;', 'ALL') />
	<cfset string = Replace(string, '"', '&quot;', 'ALL') />
	<cfset string = Replace(string, '<', '&lt;', 'ALL') />
	<cfset string = Replace(string, '>', '&gt;', 'ALL') />
	<cfset string = Replace(string, "'", '&apos;', 'ALL') />
	<cfreturn string />
</cffunction>


<cffunction name="decodeStringXml" access="public" returntype="string" 	output="false" hint="Decode xml string">
	<cfargument name="string" type="any" required="true" hint="" />
	<cfset string = Replace(string, '&amp;', '&', 'ALL') />
	<cfset string = Replace(string, '&quot;', '"', 'ALL') />
	<cfset string = Replace(string, '&lt;', '<', 'ALL') />
	<cfset string = Replace(string, '&gt;', '>', 'ALL') />
	<cfset string = Replace(string, '&apos;', "'", 'ALL') />
	<cfreturn string />
</cffunction>
