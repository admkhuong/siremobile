
<!--- CU Custom Upload --->

<!--- 0=delimited 1=fixed field length --->
<cfset ETLFileFormat = 0>
<cfset ETLNumberOfFields = 0>



<!--- Look up file format defintion from DB--->


<cfparam name="inpGroupId" default="0">
<cfparam name="inpSocialmediaFlag" default="0">
<cfparam name="inpETLDefID" default="0">
<cfparam name="fileUploadName" default="">

<!--- Set here only so if changes can be quickly updated --->
<!---<cfinclude template="../data_ListPaths.cfm">--->
<cfinclude template="../../../public/paths.cfm" >
<cfinclude template="../../lib/excel/POIUtility.cfc" >

<cfset r = structNew()>
<cfset r["msg"] = "Failed to process file upload">
<cfset r["error"] = "Unhandled Error">
<cfset r["UniqueFileName"] = "error">
<cfset r["OrgFileName"] = "error">
<cfset r["dataTable"] = "">
<cfset dataReturn = "">
<cfoutput>

	<cftry> 
    
	    <!--- Validate session still in play - handle gracefully if not --->
	     <cfif Session.USERID LT "1"  >
              <cfthrow MESSAGE="User Session Has Expired - Please relog in." TYPE="Any" extendedinfo="" errorcode="-2">
         </cfif>    
                    
		<!--- Bug in coldfusion 8 where make unique on move file doesn't work - at least on share paths--->       
        <!--- Is there a file --->
        	 		
        	<cfset r["error"] = "Unhandled Error in File Upload">
            
            <!--- Pass in all variables needed to finish processing even if session expires - long uploads --->
        	    	<!--- Create\Validate path in DB  - mySQL on DB reuires escaped back slashes --->
            <cfset ImportFileOnDB = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileUploadName#">        
        			<!--- Build import structure based on inpETLDefID --->
            <cfinvoke 
	              component="#Session.SessionCFCPath#.MultiLists"
	              method="GetETLDefinitions"
	              returnvariable="RetValETLData">                         
                <cfinvokeargument name="inpETLDEFID" value="#inpETLDefID#"/>
             	<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
            </cfinvoke>
                                                   
    		 <cfset fieldLengthInfo = ArrayNew(1)>
                    <!--- Parse for data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset myxmldocResultDoc = XmlParse("<myFDDoc>" & #RetValETLData.FIELDDEFINITIONS_XML# & "</myFDDoc>")>
                           
                          <cfcatch TYPE="any">
                          
                            <cfset DebugStr = "Bad data 1">
                          
                            <!--- Squash bad data  --->    
                            <cfset myxmldocResultDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                       
                         </cfcatch>              
                    </cftry> 
                    
                    
                  	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/myFDDoc/FD")>
            
         		<cfloop array="#selectedElements#" index="CurrFDXML">
           		    
			<!--- Parse for FD data --->                             
                  	<cftry>
                        <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    	<cfset XMLFDDoc = XmlParse(#CurrFDXML#)>
	                         
                   	<cfcatch TYPE="any">
                         <!--- Squash bad data  --->    
                         <cfset XMLFDDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                        
                    </cfcatch>              
                  	</cftry> 
                                                     
               	 	<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@ID")>
					<cfif ArrayLen(selectedElementsII) GT 0>
	                    <cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                <cfelse>
	                    <cfset CURRID = "0">                        
	                </cfif>                         
                 
					<cfset fieldObj = {start = 0, length = 0, desc=""}>
				
					<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@START")>
					<cfif ArrayLen(selectedElementsII) GT 0>
	                	<cfset CURRSTART = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
					<cfset fieldObj.start = CURRSTART>	
	                </cfif>
			
					<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@LENGTH")>
					<cfif ArrayLen(selectedElementsII) GT 0>
	                    <cfset CURRLENGTH = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                	<cfset fieldObj.length = CURRLENGTH>
	                </cfif>		
	                
	                <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@DESC")>
					<cfif ArrayLen(selectedElementsII) GT 0>
	                    <cfset CURRDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                	<cfset fieldObj.desc = CURRDESC>
	                </cfif>		
	                				
					<cfset ArrayAppend(fieldLengthInfo, fieldObj)>
            	</cfloop>
	
			<!--- Load Stage table with data Section--->
			<!--- Load data from separator file type--->
			<!--- Uploaded File Type: Seperate value --->
			<cfif RetValETLData.FILETYPEID EQ 0>
				<cfset LineTerminator = "\r\n">                        
                <!--- http://en.wikipedia.org/wiki/Newline --->
                <cfswitch expression="#RetValETLData.LINETERMINATOR#">
                	<cfcase value="WIN"><cfset CurrLineTerminator = "#chr(10)##chr(13)#"></cfcase>
                    <cfcase value="UNIX"><cfset CurrLineTerminator = "#chr(13)#"></cfcase>                            
                    <cfdefaultcase><cfset CurrLineTerminator = "#3#"></cfdefaultcase>
                </cfswitch>
            	<cfset LinesRead = 0>  
                <cfset ImportLineErrorCount = 0> 
				<cfset dataReturn = StructNew()>
	            <cfset dataReturn.DATA = ArrayNew(1)>      
	            <cfset dataReturn.TYPE = "SEP">     
	            <!--- Use the new cfloop route insted of FileReadLine to specify different end of line characters --->
				<cfset dataReturn.COLUMNNAMES = ArrayNew(1) >
				<cfloop index="fieldInfo" array="#fieldLengthInfo#">
					<cfset ArrayAppend(dataReturn.COLUMNNAMES, fieldInfo.DESC) >
				</cfloop>
				
				<cfloop file="#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileUploadName#" index="CurrRecordList" >
					
					<cfset LinesRead = LinesRead + 1> 
					<!--- NOTE: Split function is not worked with | character---->
					
<!--- 					<cfset CurrRecordList = ReplaceNoCase(CurrRecordList, TRIM(RetValETLData.FIELDSEPARATOR), '~')> --->
                    <!--- Validate length of fields --->
					<cfset rowValue = ArrayNew(1)>
					<cfset arrayField = CurrRecordList.Split(",")>
					<cfif ArrayLen(arrayField) EQ RetValETLData.NUMBEROFFIELDS>
						<cfloop array="#arrayField#" index="value">
							<cfset ArrayAppend(rowValue, value)>
						</cfloop>
					</cfif>
                    <cfset ArrayAppend(dataReturn.DATA, rowValue)>
                </cfloop>
			</cfif>
			<!--- Load fixed field length --->
			<cfif RetValETLData.FILETYPEID EQ 1>
				<cfset LineTerminator = "\r\n">                        
                <!--- http://en.wikipedia.org/wiki/Newline --->
                <cfswitch expression="#RetValETLData.LINETERMINATOR#">
                	<cfcase value="WIN"><cfset CurrLineTerminator = "#chr(10)##chr(13)#"></cfcase>
                    <cfcase value="UNIX"><cfset CurrLineTerminator = "#chr(13)#"></cfcase>                            
                    <cfdefaultcase><cfset CurrLineTerminator = "#3#"></cfdefaultcase>
                </cfswitch>
            	<cfset LinesRead = 0>  
                <cfset ImportLineErrorCount = 0> 
				
				<cfset dataReturn = StructNew()>
	            <cfset dataReturn.DATA = ArrayNew(1)>      
	            <cfset dataReturn.TYPE = "FL">     
				<!--- Use the new cfloop route insted of FileReadLine to specify different end of line characters --->
				<cfset dataReturn.COLUMNNAMES = ArrayNew(1) >
				<cfloop index="fieldInfo" array="#fieldLengthInfo#">
					<cfset ArrayAppend(dataReturn.COLUMNNAMES, fieldInfo.DESC) >
				</cfloop>
                <cfloop file="#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileUploadName#" index="CurrRecordList" >
					<cfset LinesRead = LinesRead + 1> 
					
                    <!--- Process Records--->
					<!---Calculate max field length--->
					<cfset maxRowLength = 0>
					<cfloop array="#fieldLengthInfo#" index="fieldLength">
						<cfset maxRowLength = maxRowLength + fieldLength.LENGTH>
					</cfloop>
                   	<!--- Clean up NULLS so list length calculation works properly --->	
                   	<cfloop index="i" from="1" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
			 			<cfset CurrRecordList = Replace(CurrRecordList, "#TRIM(RetValETLData.FIELDSEPARATOR)##TRIM(RetValETLData.FIELDSEPARATOR)#", "#TRIM(RetValETLData.FIELDSEPARATOR)#NULL#TRIM(RetValETLData.FIELDSEPARATOR)#", "all")>				                      
                    </cfloop>
                        <!--- Validate length of fields --->
					<cfset rowValue = ArrayNew(1)>

                        <cfloop index="i3" from="1" to="#RetValETLData.NUMBEROFFIELDS#" step="1">
							<cfif len(CurrRecordList) LTE maxRowLength> 
								<cfset ArrayAppend(rowValue, TRIM(Mid(CurrRecordList, fieldLengthInfo[i3].START, fieldLengthInfo[i3].LENGTH)))>	                          		
								<cfif #i3# EQ #RetValETLData.NUMBEROFFIELDS#>
									<cfset ArrayAppend(rowValue, '<font color="blue"><b>YES</b></font>')>
								</cfif>
							<cfelse>
								<cfif #i3# EQ #RetValETLData.NUMBEROFFIELDS#>
									<cfset ArrayAppend(rowValue, TRIM(Mid(CurrRecordList, fieldLengthInfo[i3].START, fieldLengthInfo[i3].LENGTH+len(CurrRecordList)-maxRowLength)))>
									<cfset ArrayAppend(rowValue, '<font color="red"><b>NO</b></font>')>
								<cfelse>
									<cfset ArrayAppend(rowValue, TRIM(Mid(CurrRecordList, fieldLengthInfo[i3].START, fieldLengthInfo[i3].LENGTH)))>
								</cfif>
							</cfif>
                        </cfloop>
                    <cfset ArrayAppend(dataReturn.DATA, rowValue)>
                </cfloop>	
			</cfif>
             <cfif RetValETLData.FILETYPEID EQ 2>
            	<cfset LinesRead = 0>  
                <cfset ImportLineErrorCount = 0> 
				<cfset arrSheets = ReadExcel( 
					FilePath = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileUploadName#"
				) />
				<cfset firstSheet = arrSheets[1]>	
				<cfset dataReturn = firstSheet >
				<cfset dataReturn.type = "XLS" >	
			</cfif>                            
			<cfset dataReturn.SKIPLINE = RetValETLData.SKIPLINES >
            <cfset r["UniqueFileName"] = "#fileUploadName#">
            <cfset r["OrgFileName"] = "#fileUploadName#">
            <!--- Spin off a thread to handle processing --->
            <cfset r["msg"] = "File uploaded. #fileUploadName# ">
            <cfset r["error"] = "">
			<cfset r["dataTable"] = "#dataReturn#">
    <cfcatch TYPE="any">
        <cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#">            
    </cfcatch>
    </cftry>
</cfoutput>


<!--- Output results in JSON format --->
<!---<MM:DECORATION HILITECOLOR="Dyn Untranslated Color">{r}</MM:DECORATION>--->
<cfoutput>#serializeJSON(r)#</cfoutput>