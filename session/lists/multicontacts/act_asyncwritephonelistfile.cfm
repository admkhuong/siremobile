
<!---
Alternate tools and research
http://pirlwww.lpl.arizona.edu/software/PIRL_Java_Packages/PIRL/Database/Fields_Map.html


--->


<!---


Assumtions and Caveats
Max data length is 2048


Fixed Field or Delimited - assume delimted for now


How many fields? - Build stage table based on number of 


Preview file 

Auto guess format - suggestion?

Do you have a unique customer id?

Define custom field map - allow user to define mnemonic for each field input

Save File definition for future use - does it already exist the same?

Number of rows to skip before start processing

What is the end of line character?
DOS - \r\n
Unix - \r
other?



File Feeds:
SFTP - 
	Name of file to look for - unique naming conventions
	Directory
	Frequency to look
	User name
	Password - encrypted by cold fusion
	Batch Id to push new data to


CREATE TABLE `rxmultilist` (
  `UserId_int` int(11) NOT NULL,
  `ContactTypeId_int` int(4) NOT NULL,
  `TimeZone_int` smallint(6) NOT NULL,
  `CellFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `OptInFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `SourceKey_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LASTUPDATED_DT` datetime DEFAULT NULL,
  `LastAccess_dt` datetime DEFAULT NULL,
  `CustomField1_int` decimal(11,4) DEFAULT NULL,
  `CustomField2_int` decimal(11,4) DEFAULT NULL,
  `CustomField3_int` decimal(11,4) DEFAULT NULL,
  `CustomField4_int` decimal(11,4) DEFAULT NULL,
  `CustomField5_int` decimal(11,4) DEFAULT NULL,
  `CustomField6_int` decimal(11,4) DEFAULT NULL,
  `CustomField7_int` decimal(11,4) DEFAULT NULL,
  `CustomField8_int` decimal(11,4) DEFAULT NULL,
  `CustomField9_int` decimal(11,4) DEFAULT NULL,
  `CustomField10_int` decimal(11,4) DEFAULT NULL,
  `UniqueCustomer_UUID_vch` varchar(36) DEFAULT NULL,
  `ContactString_vch` varchar(255) NOT NULL DEFAULT '',
  `LocationKey1_vch` varchar(255) DEFAULT NULL,
  `LocationKey2_vch` varchar(255) DEFAULT NULL,
  `LocationKey3_vch` varchar(255) DEFAULT NULL,
  `LocationKey4_vch` varchar(255) DEFAULT NULL,
  `LocationKey5_vch` varchar(255) DEFAULT NULL,
  `LocationKey6_vch` varchar(255) DEFAULT NULL,
  `LocationKey7_vch` varchar(255) DEFAULT NULL,
  `LocationKey8_vch` varchar(255) DEFAULT NULL,
  `LocationKey9_vch` varchar(255) DEFAULT NULL,
  `LocationKey10_vch` varchar(255) DEFAULT NULL,
  `SourceKey_vch` varchar(255) NOT NULL DEFAULT '',
  `grouplist_vch` varchar(512) NOT NULL DEFAULT '0,',
  `CustomField1_vch` varchar(2048) DEFAULT NULL,
  `CustomField2_vch` varchar(2048) DEFAULT NULL,
  `CustomField3_vch` varchar(2048) DEFAULT NULL,
  `CustomField4_vch` varchar(2048) DEFAULT NULL,
  `CustomField5_vch` varchar(2048) DEFAULT NULL,
  `CustomField6_vch` varchar(2048) DEFAULT NULL,
  `CustomField7_vch` varchar(2048) DEFAULT NULL,
  `CustomField8_vch` varchar(2048) DEFAULT NULL,
  `CustomField9_vch` varchar(2048) DEFAULT NULL,
  `CustomField10_vch` varchar(2048) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,
  `SourceString_vch` text,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserId_int`,`ContactString_vch`,`SourceKey_vch`,`ContactTypeId_int`),
  KEY `IDX_OptInFlag_int` (`OptInFlag_int`),
  KEY `IDX_grouplist_vch` (`grouplist_vch`),
  KEY `IDX_ContactTypeId_int` (`ContactTypeId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE TABLE `etldefinitions` (
  `ETLID_int` int(11) NOT NULL,
  `UserId_int` int(11) NOT NULL,
  `FileType_int` int(4) NOT NULL DEFAULT '0',
  `NumberOfFields_int` int(4) DEFAULT '1',
  `SkipLines_int` int(11) DEFAULT '0',
  `SkipErrors_int` int(11) DEFAULT '0',
  `FieldSeparator_vch` varchar(50) DEFAULT ',',
  `LineTerminator_vch` varchar(255) DEFAULT NULL,
  `Desc_vch` varchar(255) DEFAULT NULL,
  `FieldDefinitions_XML` text,
  `Created_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Active_int` int(4) DEFAULT '1',
  PRIMARY KEY (`ETLID_int`,`UserId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE TABLE `simplephonelistuploadmonitor` (
  `UploadId_int` int(11) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) NOT NULL,
  `GroupId_int` int(11) NOT NULL DEFAULT '0',
  `fileSize_int` int(11) NOT NULL DEFAULT '-1',
  `FileStagedToDB_int` int(11) NOT NULL DEFAULT '0',
  `RecordsInFile_int` int(11) NOT NULL DEFAULT '-1',
  `DuplicatesInFile_int` int(11) NOT NULL DEFAULT '-1',
  `ERRORCODE_INT` int(11) NOT NULL DEFAULT '0',
  `ProcessedRecordCount_int` int(11) NOT NULL DEFAULT '0',
  `CountElligiblePhone_int` int(11) NOT NULL DEFAULT '0',
  `CountElligibleEMail_int` int(11) NOT NULL DEFAULT '0',
  `CountElligibleSMS_int` int(11) NOT NULL DEFAULT '0',
  `ErrorMsg_vch` varchar(1024) NOT NULL DEFAULT '',
  `ORIGINALFILENAME_VCH` varchar(1024) NOT NULL DEFAULT '',
  `UniqueFileName_vch` varchar(1024) NOT NULL DEFAULT '',
  `DebugInfo_vch` text,
  PRIMARY KEY (`UploadId_int`),
  KEY `UniqueFileName_vch` (`UniqueFileName_vch`(767)),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1$$





--->

<!--- CU Custom Upload --->

<!--- 0=delimited 1=fixed field length --->
<cfset ETLFileFormat = 0>
<cfset ETLNumberOfFields = 0>



<!--- Look up file format defintion from DB--->





<cfparam name="inpGroupId" default="0">
<cfparam name="inpSocialmediaFlag" default="0">
<cfparam name="inpETLDefID" default="0">
<cfparam name="fileUploadName" default="">
<cfparam name="fileServerUploadName" default="">
<cfparam name="fileUploadSize" default="">


<!--- Set here only so if changes can be quickly updated --->
<!---<cfinclude template="../data_ListPaths.cfm">--->
<cfinclude template="../../../public/paths.cfm" >
<cfinclude template="../../lib/excel/POIUtility.cfc" >

<cfset r = structNew()>
<cfset r["msg"] = "Failed to process file upload">
<cfset r["error"] = "Unhandled Error">
<cfset r["UploadId"] = "-1">
<cfset r["UniqueFileName"] = "error">
<cfset r["OrgFileName"] = "error">
<cfoutput>

	<cftry> 
    
	    <!--- Validate session still in play - handle gracefully if not --->
	     <cfif Session.USERID LT "1"  >
              <cfthrow MESSAGE="User Session Has Expired - Please relog in." TYPE="Any" extendedinfo="" errorcode="-2">
         </cfif>    
                    
		
        	<cfset r["error"] = "Unhandled Error in File Upload">
        
			<!--- Start a new upload processing entry --->
		    <cfquery name="UploadListData" datasource="#Session.DBSourceEBM#">
                    INSERT INTO
                        simplelists.simplephonelistuploadmonitor
                        (UserId_int, GroupId_int, ORIGINALFILENAME_VCH, UniqueFileName_vch, fileSize_int, ErrorMsg_vch)
                    VALUES
                        (#Session.USERID#, #inpGroupId#, '#fileUploadName#', '#fileServerUploadName#', #fileUploadSize#, '')                                  
            </cfquery>  
            
            <!--- DB independent auto increment uid retrieval--->
            <cfquery name="GetUID" datasource="#Session.DBSourceEBM#">
                    SELECT
                        MAX(UploadId_int) as NextUploadId
                    FROM
                        simplelists.simplephonelistuploadmonitor
                    WHERE
                        UserId_int = #Session.USERID#  
                    AND
                        UniqueFileName_vch = '#fileServerUploadName#'                           	                               
            </cfquery>  
            
            
            <!--- Pass in all variables needed to finish processing even if session expires - long uploads --->
            <cfthread action="run" name="t#GetUID.NextUploadId#" inpClientFileName="#fileUploadName#" inpServerFileName="#fileServerUploadName#"  inpDBLocalPath="#PhoneListDBLocalWritePath#" inpGroupId="#inpGroupId#" inpUploadId="#GetUID.NextUploadId#" inpUserId="#Session.USERID#" inpDBSource="#Session.DBSourceEBM#">
            
                <cftry>
            
        	    	<!--- Create\Validate path in DB  - mySQL on DB reuires escaped back slashes --->
                    <cfset ImportFileOnDB = "#inpDBLocalPath#\\\/U#inpUserId#\\\\#inpServerFileName#">        
        
        
        
        			<!--- Build import structure based on inpETLDefID --->
			
            		<!--- --->
                    <cfinvoke 
                     component="#Session.SessionCFCPath#.MultiLists"
                     method="GetETLDefinitions"
                     returnvariable="RetValETLData">                         
                        <cfinvokeargument name="inpETLDEFID" value="#inpETLDefID#"/>
                     	<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                    </cfinvoke>
                                                            
                    <cfif RetValETLData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="ETL Definitions Error" TYPE="Any" detail="#RetValETLData.MESSAGE# - #RetValETLData.ERRMESSAGE#" errorcode="-5">                        
                    </cfif>  
                                   
					<cfset ETLCreateTableString = "">
                    <cfset ETLCreateTableString = ETLCreateTableString & "CREATE TABLE simplexuploadstage.`simplephonelist_stage_#inpuploadid#_#inpuserid#` ( " >
          			<cfset ETLCreateTableString = ETLCreateTableString & " `UID` INTEGER(11) NOT NULL AUTO_INCREMENT," >
                                      
                    <cfset fieldLengthInfo = ArrayNew(1)>
                    <!--- Parse for data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset myxmldocResultDoc = XmlParse("<myFDDoc>" & #RetValETLData.FIELDDEFINITIONS_XML# & "</myFDDoc>")>
                           
                          <cfcatch TYPE="any">
                          
                            <cfset DebugStr = "Bad data 1">
                          
                            <!--- Squash bad data  --->    
                            <cfset myxmldocResultDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                       
                         </cfcatch>              
                    </cftry> 
                    
                    
                  	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/myFDDoc/FD")>
            
            		<cfloop array="#selectedElements#" index="CurrFDXML">
              		    
						<!--- Parse for FD data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLFDDoc = XmlParse(#CurrFDXML#)>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLFDDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                        
                             </cfcatch>              
                        </cftry> 
                                                        
                        <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@ID")>
                    
						<cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRID = "0">                        
                        </cfif>                         
                    
						<cfset ETLCreateTableString = ETLCreateTableString & "`UploadField_#CURRID#_vch` VARCHAR(2048) NOT NULL DEFAULT '',">   
                        <cfset ETLCreateTableString = ETLCreateTableString & "KEY `UploadField_#CURRID#_vch` (`UploadField_#CURRID#_vch`),">   
                         
						<cfset fieldObj = {start = 0, length = 0}>
						
						<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@START")>
						<cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRSTART = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
							<cfset fieldObj.start = CURRSTART>	
                        </cfif>
						
						<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@LENGTH")>
						<cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRLENGTH = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        	<cfset fieldObj.length = CURRLENGTH>
                        </cfif>						
						<cfset ArrayAppend(fieldLengthInfo, fieldObj)>
                    </cfloop>
                                        
                    <!---  <cfset ETLCreateTableString = ETLCreateTableString & "">  --->
					<cfset ETLCreateTableString = ETLCreateTableString & " PRIMARY KEY (`UID`) ">
					<cfset ETLCreateTableString = ETLCreateTableString & " )ENGINE=innoDB">
                       
                    
                    <cfquery name="CreateUploadStage" datasource="#inpDBSource#">
						#ETLCreateTableString#
                    </cfquery>         
    
    				<!--- Load Stage table with data Section--->
					<!--- Load data from separator file type--->
					<cfif RetValETLData.FILETYPEID EQ 0 OR TRIM(RetValETLData.FIELDSEPARATOR) EQ '|'>
						<cfif RetValETLData.SKIPERRORS GT 0>
                    <!--- One line at a time load Section --->
                    
	                    	<cfset LineTerminator = "\r\n">                        
	                        
	                        <!--- http://en.wikipedia.org/wiki/Newline --->
	                        <cfswitch expression="#RetValETLData.LINETERMINATOR#">
	                        	<cfcase value="WIN"><cfset CurrLineTerminator = "#chr(10)##chr(13)#"></cfcase>
	                            <cfcase value="UNIX"><cfset CurrLineTerminator = "#chr(13)#"></cfcase>                            
	                            <cfdefaultcase><cfset CurrLineTerminator = "#RetValETLData.LINETERMINATOR#"></cfdefaultcase>
	                        </cfswitch>
	                    
	                    
	                    	<cfset LinesRead = 0>  
	                        <cfset ImportLineErrorCount = 0> 
	                   <!---     
	                        <cfthrow MESSAGE="Here is the list file" TYPE="Any" detail="#PhoneListUploadLocalWritePath#/U#Session.USERID#/#CurrUpload.serverFile#" errorcode="-5">
	                 
					    <cfset NewFileName = "\\\\10.0.1.114\\PhoneListUploads\/U#Session.USERID#/\#CurrUpload.serverFile#">
	                        
	                        <cfthrow MESSAGE="Here is the list file" TYPE="Any" detail="#NewFileName#" errorcode="-5">
							delimiters="#chr(10)##chr(13)#"
	                   
					     <cfthrow MESSAGE="Here is the CurrRecordList" TYPE="Any" detail="#CurrRecordList#" errorcode="-5">
						 
					 --->   
							<!--- Use the new cfloop route insted of FileReadLine to specify different end of line characters --->
	                        <cfloop file="#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileServerUploadName#" index="CurrRecordList" >
								<cfset CurrRecordList = Replace(CurrRecordList, '|', '====')>
								<cfset newSep = '===='>
								<cfset LinesRead = LinesRead + 1> 
	                            							                             
	                            <!--- Process Records--->
								<cfif LinesRead GT #RetValETLData.SKIPLINES#>
	                            								                            
	                                <cfif RIGHT(CurrRecordList, 1) EQ "#newSep#">
										 <cfset CurrRecordList = CurrRecordList & "NULL">
	                                </cfif>
	                                                     
	                                <cfif LEFT(CurrRecordList, 1) EQ "#newSep#">
	                                    <cfset CurrRecordList = "0" & CurrRecordList>				
	                                </cfif>
	                                
	                                
	                                <!--- Validate number of fields --->
	                                <cfif listlen(#CurrRecordList#,'#newSep#') EQ #RetValETLData.NUMBEROFFIELDS#> 
	                                
	                                	<!--- Build Insert Statement--->
	                                    <cfset LoadInsertStatement = "">
	                                                                        
	                                    <cfset LoadInsertStatement = LoadInsertStatement & "INSERT INTO simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ">   
	                                    <cfset LoadInsertStatement = LoadInsertStatement & "( UploadField_1_vch">
	                        
	                                    <cfloop index="i2" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
	                                        <cfset LoadInsertStatement = LoadInsertStatement & ", UploadField_#i2#_vch">				                      
	                                    </cfloop>
	                        
				                        <cfset LoadInsertStatement = LoadInsertStatement & ") ">   
	                                                                                                            
	                                    <cfset LoadInsertStatement = LoadInsertStatement & "VALUES ">   
	                                    
	                                                          
	                                    <cfif ListGetAt(CurrRecordList,1, newSep ) EQ "NULL">                                        
	                                        <cfset LoadInsertStatement = LoadInsertStatement & "( ''">
	                                    <cfelse>                                                                                    
	                                        <cfset LoadInsertStatement = LoadInsertStatement & "( '#ListGetAt(CurrRecordList,1, newSep )#'">
	                                    </cfif>			 
	                            
	                                    <cfloop index="i3" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">    
	                                    	
	                                        <cfif ListGetAt(CurrRecordList,i3, TRIM(RetValETLData.FIELDSEPARATOR) ) EQ "NULL">                                        
		                                        <cfset LoadInsertStatement = LoadInsertStatement & ", ''">
	                                        <cfelse>                                                                                    
	                                        	<cfset LoadInsertStatement = LoadInsertStatement & ", '#ListGetAt(CurrRecordList,i3, newSep)#'">	                                        
	                                        </cfif>			                      
	                                    </cfloop>
	                                    
	                                    <cfset LoadInsertStatement = LoadInsertStatement & ") ">     
	                                    
	                                    <cfquery name="SingleInsertIntoListStage" datasource="#inpDBSource#">
	                                        #PreserveSingleQuotes(LoadInsertStatement)#					 
	                                    </cfquery>	  	
	                                
	                                <cfelse>
	                                	<cfset ImportLineErrorCount = ImportLineErrorCount + 1> 
	                                    
	                                    <!--- If too many errors - Stop --->
	                                    <cfif ImportLineErrorCount GT RetValETLData.SKIPERRORS>
	                                        <cfthrow MESSAGE="Too many invalid lines of data" TYPE="Any" detail="#RetValETLData.SKIPERRORS# Errors Reached" errorcode="-5">                        
	           							</cfif>
	                                
	                                </cfif>
	                            
	                            </cfif>
	                                
	                            <!--- Update status 1000 lines at a time --->                            
	                            <cfif LinesRead GT 0 AND (LinesRead MOD 1000) EQ 0> 
	                                                        
	                             	<!--- Status update... ---> 
	                                <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                                    UPDATE
	                                       simplelists.simplephonelistuploadmonitor
	                                    SET
	                                       ProcessedRecordCount_int = #LinesRead#
	                                    WHERE
	                                       UploadId_int = #inpUploadId#
	                                </cfquery>  
	                    
	                            </cfif>
									
	                        </cfloop>
	
							<!--- Status update... ---> 
	                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                            UPDATE
	                               simplelists.simplephonelistuploadmonitor
	                            SET
	                               ProcessedRecordCount_int = #LinesRead#
	                            WHERE
	                               UploadId_int = #inpUploadId#
	                        </cfquery>  
	  
	                    <!--- One line at a time load Section --->                    
	                    <cfelse>
	                    <!--- Bulk Load Section --->
	
							<cfset LineTerminator = "\r\n">                        
	                        
	                        <!--- http://en.wikipedia.org/wiki/Newline --->
	                        <cfswitch expression="#RetValETLData.LINETERMINATOR#">
	                        	<cfcase value="WIN"><cfset CurrLineTerminator = "\r\n"></cfcase>
	                            <cfcase value="UNIX"><cfset CurrLineTerminator = "\n"></cfcase>                            
	                            <cfdefaultcase><cfset CurrLineTerminator = "#RetValETLData.LINETERMINATOR#"></cfdefaultcase>
	                        </cfswitch>
	                     
	                        <cfset ETLBulkLoadTableString = "">
	                        <!---<cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "">--->  
	        
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "LOAD DATA LOCAL INFILE '#ImportFileOnDB#' ">
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "INTO TABLE simplexuploadstage.`simplephonelist_stage_#inpuploadid#_#inpuserid#` ">
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "FIELDS TERMINATED BY '#TRIM(RetValETLData.FIELDSEPARATOR)#' OPTIONALLY ENCLOSED BY '""' LINES TERMINATED BY '#CurrLineTerminator#' ">
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "IGNORE #RetValETLData.SKIPLINES# LINES ">
	                       
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & "( UploadField_1_vch">
	                        
	                        <cfloop index="i" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
	                            <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & ", UploadField_#i#_vch">				                      
	                        </cfloop>
	                       
	                        <cfset ETLBulkLoadTableString = ETLBulkLoadTableString & ") ">                   
	                        
	                        <!---<cfthrow MESSAGE="Here is the bulk statement" TYPE="Any" detail="#ETLBulkLoadTableString#" errorcode="-5">  --->                      
	           					
	                        <cfquery name="InsertIntoListStage" datasource="#inpDBSource#">
								#PreserveSingleQuotes(ETLBulkLoadTableString)#	 
	                        </cfquery>	  
	                                        	
	                    <!--- Bulk Load Section --->
	                    </cfif>
					</cfif>
					<!--- Load fixed field length --->
					<cfif RetValETLData.FILETYPEID EQ 1>
						<cfset LineTerminator = "\r\n">                        
                        <!--- http://en.wikipedia.org/wiki/Newline --->
                        <cfswitch expression="#RetValETLData.LINETERMINATOR#">
                        	<cfcase value="WIN"><cfset CurrLineTerminator = "#chr(10)##chr(13)#"></cfcase>
                            <cfcase value="UNIX"><cfset CurrLineTerminator = "#chr(13)#"></cfcase>                            
                            <cfdefaultcase><cfset CurrLineTerminator = "#RetValETLData.LINETERMINATOR#"></cfdefaultcase>
                        </cfswitch>
                    	<cfset LinesRead = 0>  
                        <cfset ImportLineErrorCount = 0> 
	                  
						<!--- Use the new cfloop route insted of FileReadLine to specify different end of line characters --->
                        <cfloop file="#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileServerUploadName#" index="CurrRecordList" >
							<cfset LinesRead = LinesRead + 1> 
							
                            <!--- Process Records--->
							<cfif LinesRead GT #RetValETLData.SKIPLINES#>
								<!---Calculate max field length--->
								<cfset maxRowLength = 0>
								<cfloop array="#fieldLengthInfo#" index="fieldLength">
									<cfset maxRowLength = maxRowLength + fieldLength.LENGTH>
								</cfloop>
	                           	<!--- Clean up NULLS so list length calculation works properly --->	
	                           	<cfloop index="i" from="1" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
							 		<cfset CurrRecordList = Replace(CurrRecordList, "#TRIM(RetValETLData.FIELDSEPARATOR)##TRIM(RetValETLData.FIELDSEPARATOR)#", "#TRIM(RetValETLData.FIELDSEPARATOR)#NULL#TRIM(RetValETLData.FIELDSEPARATOR)#", "all")>				                      
	                            </cfloop>
                                
	                            <!--- Validate length of fields --->
	                            <cfif len(CurrRecordList) LTE  maxRowLength> 
	                        		<!--- Build Insert Statement--->
	                            	<cfset LoadInsertStatement = "">
		                            <cfset LoadInsertStatement = LoadInsertStatement & "INSERT INTO simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ">   
		                            <cfset LoadInsertStatement = LoadInsertStatement & "( UploadField_1_vch">
		                            <cfloop index="i2" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
		                                <cfset LoadInsertStatement = LoadInsertStatement & ", UploadField_#i2#_vch">				                      
		                            </cfloop>
			                        <cfset LoadInsertStatement = LoadInsertStatement & ") ">   
		                            <cfset LoadInsertStatement = LoadInsertStatement & "VALUES ">   
	                            
		                            <cfset LoadInsertStatement = LoadInsertStatement & "( '#TRIM(Mid(CurrRecordList, fieldLengthInfo[1].START, fieldLengthInfo[1].LENGTH))#'">
		                            <cfloop index="i3" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">    
	                                	<cfset LoadInsertStatement = LoadInsertStatement & ", '#TRIM(Mid(CurrRecordList, fieldLengthInfo[i3].START, fieldLengthInfo[i3].LENGTH))#'">	                                        
		                            </cfloop>
		                            <cfset LoadInsertStatement = LoadInsertStatement & ") ">    
		                            <cfquery name="SingleInsertIntoListStage" datasource="#inpDBSource#">
		                                #PreserveSingleQuotes(LoadInsertStatement)#					 
		                            </cfquery>	  	
	                            <cfelse>
	                            	<cfset ImportLineErrorCount = ImportLineErrorCount + 1> 
	                                <!--- If too many errors - Stop --->
	                                <cfif ImportLineErrorCount GT RetValETLData.SKIPERRORS>
	                                    <cfthrow MESSAGE="Too many invalid lines of data" TYPE="Any" detail="#RetValETLData.SKIPERRORS# Errors Reached" errorcode="-5">                        
	       							</cfif>
	                            </cfif>
                      	</cfif>
                        <!--- Update status 1000 lines at a time --->                            
                        <cfif LinesRead GT 0 AND (LinesRead MOD 1000) EQ 0> 
                                                    
                         	<!--- Status update... ---> 
                            <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                                UPDATE
                                   simplelists.simplephonelistuploadmonitor
                                SET
                                   ProcessedRecordCount_int = #LinesRead#
                                WHERE
                                   UploadId_int = #inpUploadId#
                            </cfquery>  
                        </cfif>
                        </cfloop>
	
							<!--- Status update... ---> 
	                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                            UPDATE
	                               simplelists.simplephonelistuploadmonitor
	                            SET
	                               ProcessedRecordCount_int = #LinesRead#
	                            WHERE
	                               UploadId_int = #inpUploadId#
	                        </cfquery>  
				</cfif>
                <cfif RetValETLData.FILETYPEID EQ 2>
						
                    	<cfset LinesRead = 0>  
                        <cfset ImportLineErrorCount = 0> 
						
						<cfset arrSheets = ReadExcel( 
								FilePath = "#PhoneListUploadLocalWritePath#/U#Session.USERID#/#fileServerUploadName#"
								) />
							
						<cfif ArrayLen( arrSheets ) GT 0>
							<cfset firstSheet = arrSheets[1]>	
							
								<cfloop query="firstSheet.Query">
								
									<cfset LinesRead = LinesRead + 1> 
							
		                            <!--- Process Records--->
									<cfif LinesRead GT #RetValETLData.SKIPLINES#>
										
			                            <!--- Validate length of fields --->
		                        		<!--- Build Insert Statement--->
		                            	<cfset LoadInsertStatement = "">
			                            <cfset LoadInsertStatement = LoadInsertStatement & "INSERT INTO simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ">   
			                            <cfset LoadInsertStatement = LoadInsertStatement & "( UploadField_1_vch">
			                            <cfloop index="i2" from="2" to="#RetValETLData.NUMBEROFFIELDS#" step="1">                   
			                                <cfset LoadInsertStatement = LoadInsertStatement & ", UploadField_#i2#_vch">				                      
			                            </cfloop>
				                        <cfset LoadInsertStatement = LoadInsertStatement & ") ">   
			                            <cfset LoadInsertStatement = LoadInsertStatement & "VALUES ">   
							            <cfloop index="col" list="#columnlist#">
								            <cfif col EQ "COLUMN1">
									            <cfset LoadInsertStatement = LoadInsertStatement & "( '#firstSheet.Query[col][currentRow]#'">
							            	<cfelse>
							            		<cfset LoadInsertStatement = LoadInsertStatement & ", '#firstSheet.Query[col][currentRow]#'">
									        </cfif>
			                            </cfloop>
			                            <cfset LoadInsertStatement = LoadInsertStatement & ") ">   
			                            
			                            <cfquery name="SingleInsertIntoListStage" datasource="#inpDBSource#">
			                                #PreserveSingleQuotes(LoadInsertStatement)#					 
			                            </cfquery>	 
		                      	</cfif>
		                        <!--- Update status 1000 lines at a time --->                            
		                        <cfif LinesRead GT 0 AND (LinesRead MOD 1000) EQ 0> 
		                                                    
		                         	<!--- Status update... ---> 
		                            <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
		                                UPDATE
		                                   simplelists.simplephonelistuploadmonitor
		                                SET
		                                   ProcessedRecordCount_int = #LinesRead#
		                                WHERE
		                                   UploadId_int = #inpUploadId#
		                            </cfquery>  
		                        </cfif>
							</cfloop>
						</cfif>
						<!--- Status update... ---> 
                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                            UPDATE
                               simplelists.simplephonelistuploadmonitor
                            SET
                               ProcessedRecordCount_int = #LinesRead#
                            WHERE
                               UploadId_int = #inpUploadId#
                        </cfquery>  
				</cfif>                            
                    <!--- Get Raw Count--->             
                    <cfquery name="UpdateUploadRawCount" datasource="#inpDBSource#">
                        SELECT
                        	COUNT(*) AS TotalCountRaw
                        FROM
                           simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#                   
                    </cfquery>  
                     
                    <!--- Status update... ---> 
                    <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                        UPDATE
                           simplelists.simplephonelistuploadmonitor
                        SET
                           FileStagedToDB_int = 1,
                           RecordsInFile_int = #UpdateUploadRawCount.TotalCountRaw#
                        WHERE
                           UploadId_int = #inpUploadId#
                    </cfquery>  
                    
           
               		<!--- ADD UNIQUE INDEX(DialString_vch)       alt Hack to remove duplicates - quicker than manual temp tables for now - only works on DBs that support ALTER IGNORE --->             
                   	<!--- ... Not used for now --->
					
					<!--- Alter table - add columns --->             
                    <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">                   
						ALTER IGNORE TABLE 
                        	simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#           
                        ADD CleanFlag_int INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD TimeZone_int INTEGER(11) DEFAULT 0 NOT NULL,
                        ADD State_vch VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD City_vch VARCHAR(255) DEFAULT '' NOT NULL,
                        ADD CellFlag_int INTEGER(11) DEFAULT 0, 
                        ADD Country_vch VARCHAR(2) DEFAULT '' NOT NULL,
                        ADD INDEX (CleanFlag_int),
                        ADD INDEX (TimeZone_int),
                        ADD INDEX (State_vch),
                        ADD INDEX (City_vch),
                        ADD INDEX (CellFlag_int),
                        ADD INDEX (Country_vch)                                                
    				</cfquery>
                                                               
<!--- Transformations \ Deduplication \ Build Transfer to Main DB Strings--->    
					<cfset TransferSourceColumns = "">
                    <cfset TransferTargets = "">
                    <cfset AddTSC = 0>
                    <cfset ListPhoneCol = ArrayNew(1) >
                    <cfset ListSMSCol = ArrayNew(1) >
                    <cfset ListEMailCol = ArrayNew(1)>
                    <cfset TZCol = "">
                    <cfset StateCol = "">
                    <cfset CityCol = "">
                              
                    <!--- Just do the phone ones for now --->        
                              
                    <!--- Parse for data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset myxmldocResultDoc = XmlParse("<myFDDoc>" & #RetValETLData.FIELDDEFINITIONS_XML# & "</myFDDoc>")>
                           
                          <cfcatch TYPE="any">
                          
                            <cfset DebugStr = "Bad data 1">
                          
                            <!--- Squash bad data  --->    
                            <cfset myxmldocResultDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                       
                         </cfcatch>              
                    </cftry> 
                    
                    
                  	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/myFDDoc/FD")>
            
            		<cfloop array="#selectedElements#" index="CurrFDXML">
              		    
						<!--- Parse for FD data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLFDDoc = XmlParse(#CurrFDXML#)>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLFDDoc = XmlParse("<myFDDoc>BadData</myFDDoc>")>                        
                             </cfcatch>              
                        </cftry> 
                                                        
                        <cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@ID")>
                    
						<cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        <cfelse>
                            <cfset CURRID = "0">                        
                        </cfif>     
                    
                    	<cfif CURRID GT 0>
                       		
                        	<cfset CurrColumnName = "UploadField_#CURRID#_vch">	
                        
							<cfset selectedElementsIII = XmlSearch(XMLFDDoc, "/FD/LINK/@LinkTarget")>
                        
<!--- What about multiple links? --->
                            <cfif ArrayLen(selectedElementsIII) GT 0>
                                <cfset CURRLINKTARGET = selectedElementsIII[ArrayLen(selectedElementsIII)].XmlValue>                               
                            <cfelse>
                                <cfset CURRLINKTARGET = "0">                        
                            </cfif>     
  			        		<cfif CURRLINKTARGET EQ "1">
								<cfset ArrayAppend(ListPhoneCol, CurrColumnName)>
                            	<cfset inpAllowIVR = 1>
                            	<cfinclude template="Transformations\act_Transform_PhoneNumbers.cfm">
                            </cfif> 
                            
                            <cfif CURRLINKTARGET EQ "2">
	                            <cfset ArrayAppend(ListEMailCol, CurrColumnName)>
           		               	<cfinclude template="Transformations\act_Transform_eMail.cfm">
                            </cfif>   
                            
                            <cfif CURRLINKTARGET EQ "3">
	                            <cfset ArrayAppend(ListSMSCol, CurrColumnName)>
                            	<cfset inpAllowIVR = 0> <!--- No IVR for SMS --->
                            	<cfinclude template="Transformations\act_Transform_PhoneNumbers.cfm">
                            </cfif>
                            
                            <cfif CURRLINKTARGET EQ "301">
	                            <cfset TZCol = CurrColumnName>
                              	<cfinclude template="Transformations\act_Transform_TZ.cfm">
                            </cfif>    
                            
							<cfif (CURRLINKTARGET EQ 101) OR (CURRLINKTARGET EQ 102) OR (CURRLINKTARGET EQ 103) OR (CURRLINKTARGET EQ 104) OR (CURRLINKTARGET EQ 105) OR (CURRLINKTARGET EQ 106) OR (CURRLINKTARGET EQ 107) OR (CURRLINKTARGET EQ 108) OR (CURRLINKTARGET EQ 109) OR (CURRLINKTARGET EQ 110)>
                              	<cfinclude template="Transformations\act_Transform_Number.cfm">
                            </cfif> 
							                            
                            <!--- Add to transfer string if not a contact--->
                            <cfif (CURRLINKTARGET NEQ 0) AND (CURRLINKTARGET NEQ 1) AND (CURRLINKTARGET NEQ 2) AND (CURRLINKTARGET NEQ 3) >
                                <cfinclude template="Transformations\act_TransferTargets.cfm">                                                                 
                            </cfif>                                                
                            
							<cfif CURRLINKTARGET NEQ 0>
								<cfset selectedElementsIV = XmlSearch(XMLFDDoc, "/FD/@DEDUPE")>
	                            <cfif ArrayLen(selectedElementsIV) GT 0>
	                                <cfset CURRDEDUPE = selectedElementsIV[ArrayLen(selectedElementsIV)].XmlValue>
	                            <cfelse>
	                                <cfset CURRDEDUPE = "0">                        
	                            </cfif>     
	                    
	                    		<cfif CURRDEDUPE EQ "1">                            	
	                            	<cfinclude template="Transformations\act_Transform_Dedupe.cfm">
	                            </cfif>     
								                                                    
	                        	<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@CASETRANSFORM")>
								<cfif ArrayLen(selectedElementsII) GT 0>
									<cfset caseTransform = selectedElementsII[1].XmlValue>
									<cfif caseTransform EQ 'upper'>
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = UPPER(#CurrColumnName#)
										</cfquery>
									</cfif>
									<cfif caseTransform EQ 'lower'>
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = LOWER(#CurrColumnName#)
										</cfquery>
									</cfif>
								</cfif>
								
								<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@LOWERLIMIT")>
								<cfif ArrayLen(selectedElementsII) GT 0>
									<cfset lowerLimit = selectedElementsII[1].XmlValue>
									<cfif lowerLimit NEQ ''>
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = #lowerLimit# WHERE #CurrColumnName# < #lowerLimit#
										</cfquery>
									</cfif>
								</cfif>
								
								<cfset selectedElementsII = XmlSearch(XMLFDDoc, "/FD/@UPPERLIMIT")>
								<cfif ArrayLen(selectedElementsII) GT 0>
									<cfset upperLimit = selectedElementsII[1].XmlValue>
									<cfif upperLimit NEQ ''>
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = #upperLimit# WHERE #CurrColumnName# > #upperLimit#
										</cfquery>
									</cfif>
								</cfif>
								
								<cfset selectedElementsII = XmlSearch(CurrFDXML, "./RT")>
								<cfif ArrayLen(selectedElementsII) GT 0>
									<cfloop array="#selectedElementsII#" index="rtElement">
										<cfset txtSource = rtElement.XmlAttributes.SRC>
										<cfset txtDestination = rtElement.XmlAttributes.DEST >
										
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = REPLACE(#CurrColumnName#, '#txtSource# ', '#txtDestination# ');
										</cfquery>
										<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
											UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = REPLACE(#CurrColumnName#, '#txtSource#', '#txtDestination#') WHERE #CurrColumnName# LIKE '%#txtSource#';
										</cfquery>
									</cfloop>
								</cfif>
							</cfif>
                        </cfif>
                    		
                    </cfloop>
                                   
				    <cfset DuplicateScrubbing.TotalCountDuplicates = 0>
			        <!--- Get After Dedupe Count CleanFlag_int=1 --->             
                    <cfquery name="DuplicateScrubbing" datasource="#inpDBSource#">
                        SELECT
                        	COUNT(*) AS TotalCountDuplicates
                        FROM
                           simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#    
                        WHERE
           	               CleanFlag_int = 1 
                    </cfquery>  
				    
                    <!--- Status update... ---> 
                    <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                        UPDATE
                           simplelists.simplephonelistuploadmonitor
                        SET
                           DuplicatesInFile_int = #DuplicateScrubbing.TotalCountDuplicates#
                        WHERE
                           UploadId_int = #inpUploadId#
                    </cfquery>  
                    
                   	<!--- Get scrub counts --->
                    <!--- Move Phone Data --->
                    <cfif ArrayLen(ListPhoneCol) GT 0>
                        <cfloop array="#ListPhoneCol#" index="PhoneCol">
		                    <!--- Pass one mapped data - pass two unmapped data? or CASE statements? --->
		                    <!---              <cfif TZCol NEQ "">
	                                CASE WHEN
	                                State_vch,
	                                City_vch, 
	                                </cfif>--->        
									
	                        <!---Elligible Phone Numbers for transfer ---> 
	                        <cfquery name="GetTotalElliglePhoneCounts" datasource="#inpDBSource#">
	                             SELECT
	                               COUNT(*) AS TotalCount                                                                                      
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #PhoneCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 1)
	                             AND 
	                                CleanFlag_int = 0
	                        </cfquery>                                              
									
	                        <!--- Status update... ---> 
	                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                            UPDATE
	                               simplelists.simplephonelistuploadmonitor
	                            SET
	                               CountElligiblePhone_int = #GetTotalElliglePhoneCounts.TotalCount#
	                            WHERE
	                               UploadId_int = #inpUploadId#
	                        </cfquery>                 
	                        
	                        <!--- Add clean flag for already on list --->
	                        <cfquery name="AlreadyOnListScrubbing" datasource="#inpDBSource#">
	                            UPDATE
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                            SET 
	                                CleanFlag_int = 2
	                            WHERE 
	                                #PhoneCol# IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 1)
	                            AND 
	                                CleanFlag_int = 0			
	                        </cfquery>        
							<!--- Move non duplicates to main list --->
	                        <cfquery name="AddToPhoneList" datasource="#inpDBSource#">
	                            INSERT INTO 
	                                simplelists.rxmultilist
	                                (UserId_int, ContactTypeId_int, ContactString_vch, TimeZone_int, CellFlag_int, SourceString_vch, grouplist_vch, SourceKey_vch, Created_dt #TransferTargets# )
	                            SELECT
	                                #inpUserId#,
	                                1,
	                                #PhoneCol#,
	                                TimeZone_int,
	                                CellFlag_int,
	                                '#inpClientFileName#',
	                                '0,#inpGroupId#,',
	                                0,
	                                NOW()
	                                #TransferSourceColumns#                                                                                            
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #PhoneCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 1)
	                             AND 
	                                CleanFlag_int IN (0,2)
	                        </cfquery>    
	                        
	                       	<!--- Add duplicates to group --->
							<cfif inpGroupId GT 0>           
	                            <!--- Move duplicates that were already on the main list to specified group --->
	                            <cfquery name="AddToGroup" datasource="#inpDBSource#">         
	                                UPDATE simplelists.rxmultilist sx JOIN
	                                       simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ded ON (ded.#PhoneCol# = sx.ContactString_vch)
	                                SET 
	                                  grouplist_vch = CONCAT( grouplist_vch , '#inpGroupId#,')  
	                                WHERE 
	                                    sx.UserId_int = #inpUserId#
	                                AND sx.grouplist_vch NOT LIKE '%,#inpGroupId#,%' 
	                                AND sx.grouplist_vch NOT LIKE '#inpGroupId#,%' 
	                            </cfquery>
	                        </cfif>
						</cfloop>       
                    <cfelse>
                    
                        <!--- Status update... ---> 
                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                            UPDATE
                               simplelists.simplephonelistuploadmonitor
                            SET
                               CountElligiblePhone_int = 0
                            WHERE
                               UploadId_int = #inpUploadId#
                        </cfquery>      
                    
                    </cfif>
                    
                    
                    <!--- Move eMAil Data --->
                    <cfif ArrayLen(ListEMailCol) GT 0>
                       	<cfloop array="#ListEMailCol#" index="eMailCol">
		                    <!--- Pass one mapped data - pass two unmapped data? or CASE statements? --->
		                    <!---              <cfif TZCol NEQ "">
	                                CASE WHEN
	                                State_vch,
	                                City_vch, 
	                                </cfif>--->        
	                                                                                                             
									
	                        <!---Elligible Phone Numbers for transfer ---> 
	                        <cfquery name="GetTotalElligleEMailCounts" datasource="#inpDBSource#">
	                             SELECT
	                               COUNT(*) AS TotalCount                                                                                      
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #eMailCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 2)
	                             AND 
	                                CleanFlag_int = 0
	                        </cfquery>                                              
									
	                        <!--- Status update... ---> 
	                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                            UPDATE
	                               simplelists.simplephonelistuploadmonitor
	                            SET
	                               CountElligibleEMail_int = #GetTotalElligleEMailCounts.TotalCount#
	                            WHERE
	                               UploadId_int = #inpUploadId#
	                        </cfquery>                 
	                        
	                        <!--- Add clean flag for already on list --->
	                        <cfquery name="AlreadyOnListScrubbing" datasource="#inpDBSource#">
	                            UPDATE
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                            SET 
	                                CleanFlag_int = 2
	                            WHERE 
	                                #eMailCol# IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 2)
	                            AND 
	                                CleanFlag_int = 0			
	                        </cfquery>    
	                                
							<!--- Move non duplicates to main list --->
	                        <cfquery name="AddToPhoneList" datasource="#inpDBSource#">
	                            INSERT INTO 
	                                simplelists.rxmultilist
	                                (UserId_int, ContactTypeId_int, ContactString_vch, TimeZone_int, CellFlag_int, SourceString_vch, grouplist_vch, SourceKey_vch, Created_dt#TransferTargets# )
	                            SELECT
	                                #inpUserId#,
	                                2,
	                                #eMailCol#,
	                                TimeZone_int,
	                                CellFlag_int,
	                                '#inpClientFileName#',
	                                '0,#inpGroupId#,',
	                                0,
	                                NOW()
	                                #TransferSourceColumns#                                                                                            
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #eMailCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 2)
	                             AND 
	                                CleanFlag_int IN (0,2) <!--- Could be a duplicate number but not a duplicate eMail or SMS--->
	                        </cfquery>    
	                        
	                        <!--- Add duplicates to group --->
							<cfif inpGroupId GT 0>           
	                            <!--- Move duplicates that were already on the main list to specified group --->
	                            <cfquery name="AddToGroup" datasource="#inpDBSource#">         
	                                UPDATE simplelists.rxmultilist sx JOIN
	                                       simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ded ON (ded.#eMailCol# = sx.ContactString_vch)
	                                SET 
	                                  grouplist_vch = CONCAT( grouplist_vch , '#inpGroupId#,')  
	                                WHERE 
	                                    sx.UserId_int = #inpUserId#
	                                AND sx.grouplist_vch NOT LIKE '%,#inpGroupId#,%' 
	                                AND sx.grouplist_vch NOT LIKE '#inpGroupId#,%' 
	                            </cfquery>
	                        </cfif>
						</cfloop>        
                    <cfelse>
                        <!--- Status update... ---> 
                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                            UPDATE
                               simplelists.simplephonelistuploadmonitor
                            SET
                               CountElligibleEMail_int = 0
                            WHERE
                               UploadId_int = #inpUploadId#
                        </cfquery>      
                    
                    </cfif>
                    
                    <!--- Move SMS Data --->
                    <cfif ArrayLen(ListSMSCol) GT 0 >
                        <cfloop array="#ListSMSCol#" index="SMSCol">
		                    <!--- Pass one mapped data - pass two unmapped data? or CASE statements? --->
		                    <!---              <cfif TZCol NEQ "">
	                                CASE WHEN
	                                State_vch,
	                                City_vch, 
	                                </cfif>--->        
	                                                                                                             
									
	                        <!---Elligible Phone Numbers for transfer ---> 
	                        <cfquery name="GetTotalElliglSMSCounts" datasource="#inpDBSource#">
	                             SELECT
	                               COUNT(*) AS TotalCount                                                                                      
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #SMSCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 3)
	                             AND 
	                                CleanFlag_int = 0
	                        </cfquery>                                              
									
	                        <!--- Status update... ---> 
	                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
	                            UPDATE
	                               simplelists.simplephonelistuploadmonitor
	                            SET
	                               CountElligibleSMS_int = #GetTotalElliglSMSCounts.TotalCount#
	                            WHERE
	                               UploadId_int = #inpUploadId#
	                        </cfquery>                 
	                        
	                        <!--- Add clean flag for already on list --->
	                        <cfquery name="AlreadyOnListScrubbing" datasource="#inpDBSource#">
	                            UPDATE
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                            SET 
	                                CleanFlag_int = 2
	                            WHERE 
	                                #SMSCol# IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 3)
	                            AND 
	                                CleanFlag_int = 0			
	                        </cfquery>
	    
							<!--- Move non duplicates to main list --->
	                        <cfquery name="AddToPhoneList" datasource="#inpDBSource#">
	                            INSERT INTO 
	                                simplelists.rxmultilist
	                                (UserId_int, ContactTypeId_int, ContactString_vch, TimeZone_int, CellFlag_int, SourceString_vch, grouplist_vch, SourceKey_vch, Created_dt #TransferTargets# )
	                            SELECT
	                                #inpUserId#,
	                                3,
	                                #SMSCol#,
	                                TimeZone_int,
	                                CellFlag_int,
	                                '#inpClientFileName#',
	                                '0,#inpGroupId#,',
	                                0,
	                                NOW()
	                                #TransferSourceColumns#                                                                                            
	                             FROM
	                                simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
	                             WHERE
	                                 #SMSCol# NOT IN (SELECT ContactString_vch FROM simplelists.rxmultilist WHERE UserId_int = #inpUserId# AND ContactTypeId_int = 3)
	                             AND 
	                                CleanFlag_int IN (0,2) <!--- Could be a duplicate number but not a duplicate eMail or SMS--->
	                        </cfquery>    
	                        
	                        <!--- Add duplicates to group --->
							<cfif inpGroupId GT 0>           
	                            <!--- Move duplicates that were already on the main list to specified group --->
	                            <cfquery name="AddToGroup" datasource="#inpDBSource#">         
	                                UPDATE simplelists.rxmultilist sx JOIN
	                                       simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ded ON (ded.#SMSCol# = sx.ContactString_vch)
	                                SET 
	                                  grouplist_vch = CONCAT( grouplist_vch , '#inpGroupId#,')  
	                                WHERE 
	                                    sx.UserId_int = #inpUserId#
	                                AND sx.grouplist_vch NOT LIKE '%,#inpGroupId#,%' 
	                                AND sx.grouplist_vch NOT LIKE '#inpGroupId#,%' 
	                            </cfquery>
	                        </cfif>
						</cfloop>         
                    <cfelse>
                    
                        <!--- Status update... ---> 
                        <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                            UPDATE
                               simplelists.simplephonelistuploadmonitor
                            SET
                               CountElligibleSMS_int = 0
                            WHERE
                               UploadId_int = #inpUploadId#
                        </cfquery>      
                    </cfif>
                      	
  <!--- This needs to be dynamic based on ETL defs          	
            	 	<!--- Move non duplicates to main list --->
                    <cfquery name="AddToPhoneList" datasource="#inpDBSource#">
                        INSERT INTO 
                        	simplelists.simplephonelist
                            (UserId_int, ContactString_vch, TimeZone_int, State_vch, City_vch, CellFlag_int, UserSpecifiedData_vch, grouplist_vch )
        				SELECT
                        	#inpUserId#,
                            ContactString_vch,
                            TimeZone_int,
                            State_vch,
                            City_vch,
                            CellFlag_int,
                            '#inpClientFileName#',
                            '#inpGroupId#,'
                         FROM
                         	simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#
                         WHERE
                         	 ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.simplephonelist WHERE UserId_int = #inpUserId#)
                         AND 
                         	CleanFlag_int = 0
                   	</cfquery> 
   --->
               
              
                                             
<!---  New groupiong logic - moved to each contact type section         
                  <!--- 	<!--- Add duplicates to group --->
                   	<cfif inpGroupId GT 0>           
	               	    <!--- Move duplicates that were already on the main list to specified group --->
    	                <cfquery name="AddToGroup" datasource="#inpDBSource#">         
                            UPDATE simplelists.simplephonelist sx JOIN
                                   simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# ded ON (ded.PhoneCol = sx.ContactString_vch)
                            SET 
                              grouplist_vch = CONCAT( grouplist_vch , '#inpGroupId#,')  
                            WHERE 
                                sx.UserId_int = #inpUserId#
                            AND sx.grouplist_vch NOT LIKE '%,#inpGroupId#,%' 
                            AND sx.grouplist_vch NOT LIKE '#inpGroupId#,%' 
	                    </cfquery>
                   	</cfif>--->  
                    
 --->                   
 
 
 <!---  Soical media Stuff     
 		             <cfif inpSocialmediaFlag GT 0>
					                        
                        <!--- Update anybody from current upload not already on current list who is waiting for me to add them to my list---> 
                        <!--- Get any other users using this as their primary phone number --->
                        <CFQUERY name="getExistingUsers" datasource="#Session.DBSourceEBM#">
                            SELECT								
                                UserId_int,
                                PrimaryPhoneStr_vch                                
                            FROM 
                                simpleobjects.useraccount
                            WHERE 
                                PrimaryPhoneStr_vch IN (SELECT ContactString_vch FROM simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# WHERE ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.simplephonelist WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">) AND CleanFlag_int = 0)             
                            AND
                            	UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">    
                                
                        </CFQUERY>
                        
                        <!--- Can be more than one user --->                        
                        <cfloop query="getExistingUsers">
                        
                        	<!--- Update remote buddy flags where open invitation ---> 
                            <cfquery name="UpdateRemoteBuddyStatus" datasource="#Session.DBSourceEBM#">
                                UPDATE 
                                    simplelists.simplephonelist
                                SET
                                    BuddyFlag_int = 3                                
                                WHERE
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getExistingUsers.UserId_int#">
                                AND
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getExistingUsers.PrimaryPhoneStr_vch#">	
                            </cfquery>  
                            
                            <!--- Get any other users using this as their primary phone number --->
                            <cfquery name="getCount" datasource="#Session.DBSourceEBM#">
                                SELECT								
                                    COUNT(*) AS TOTALCOUNT
                                FROM 
                                    simplelists.simplephonelist
                                WHERE 
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.PrimaryPhone#">
                                AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getExistingUsers.UserId_int#">
                            </cfquery>
                        
                            <cfif getCount.TOTALCOUNT GT 0> 
                        
                                <!--- If on remote list - update to opt in--->
                                <cfquery name="UpdateLocalBuddyStatus" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.simplephonelist
                                    SET
                                        BuddyFlag_int = 3                                
                                    WHERE                
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                   
                                    AND    
                                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getExistingUsers.PrimaryPhoneStr_vch#">
                                </cfquery>  
                                
                            </cfif>
                            
                        </cfloop>
                          
                    </cfif>          
 --->                        
                
                    <!--- Export or save scrubbed numnbers goes here--->

                    <!--- Delete Stage Table as part of process later on ... --->
                 <!---   <cfquery name="DropStageTable" datasource="#inpDBSource#">
                        DROP TABLE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#                        
                    </cfquery>  
                 --->
                    
                    
                <cfcatch TYPE="any">
                
                    <cfquery name="UpdateUploadStatus" datasource="#inpDBSource#">
                        UPDATE
                           simplelists.simplephonelistuploadmonitor
                        SET
                           ERRORCODE_INT = -6,
                           ErrorMsg_vch = '#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#' 
                        WHERE
                           UploadId_int = #inpUploadId#
                    </cfquery>  
                
                </cfcatch>
                
                </cftry>
            
            </cfthread>
                
            <cfset r["UploadId"] = "#GetUID.NextUploadId#">
            <cfset r["UniqueFileName"] = "#fileServerUploadName#">
            <cfset r["OrgFileName"] = "#fileUploadName#">
            <!--- Spin off a thread to handle processing --->
            <cfset r["msg"] = "File uploaded. #fileUploadName# ">
            <cfset r["error"] = "">
                               
    <cfcatch TYPE="any">
                  
        <cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail# ">            
    
    </cfcatch>
            
    </cftry>

</cfoutput>


<!--- Output results in JSON format --->
<!---<MM:DECORATION HILITECOLOR="Dyn Untranslated Color">{r}</MM:DECORATION>--->
<cfoutput>#serializeJSON(r)#</cfoutput>