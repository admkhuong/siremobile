
<cfparam name="inpContactTypeId" default="1">
<cfparam name="inpNotes" default="">

<script TYPE="text/javascript">
		var $items = $('#vtab>ul>li.VoiceVTab');
		$items.addClass('selected');
var OldGroupName = "";

	function AddNewContactStringToList()
	{					
		$("#loadingDlgAddPhoneNumber").show();		
		
		$.ajax({
 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=AddContactStringToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
				  datatype: 'json',
				  data:  {inpGroupId : $("#AddNewContactStringToListDiv #inpGroupId6").val(), inpContactTypeId : $("#AddNewContactStringToListDiv #inpContactTypeId").val(), inpContactString : $("#AddNewContactStringToListDiv #inpContactString").val(), inpUserSpecifiedData : $("#AddNewContactStringToListDiv #inpUserSpecifiedData").val(), INP_SOURCEKEY : 0 },					  
				  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				  success:
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d2) 
						{							
							<!--- if .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
							var d = eval('(' + d2 + ')');
							
							<!--- Alert if failure --->
																										
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{														
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{							
										CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
										
										if(CurrRXResultCode > 0)
										{
											if(CurrRXResultCode == 2)
											{	
												$("#AddContactStringForm #AddStatus").html("<br/>Warning! Contact String (" + d.DATA.INPCONTACTSTRING[0] + ") already on the list.");
											}
											else
											{
												//gridReload_MCContacts();
												///redirect contact
												window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/mCListManager';
												$("#AddContactStringForm #AddStatus").html("<br/>Success! (" + d.DATA.INPCONTACTSTRING[0] + ") has been added to the list.");
																								
											}
										}
										else
										{
											$("#AddContactStringForm #AddStatus").html("<br/>Phone (" + d.DATA.INPCONTACTSTRING[0] + ") number has NOT been added to the list.<br/>" + d.DATA.MESSAGE[0] + "<br/>" + d.DATA.ERRMESSAGE[0] + "");								
										}							
									}
									else
									{<!--- Invalid structure returned --->	
										
									}
								}
								else
								{<!--- No result returned --->						
									jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
								}
													
								$("#loadingDlgAddPhoneNumber").hide();
						}
								
			} );		
	
		return false;

	}
	
	
	$(function()
	{	
		$("#AddNewContactStringToListDiv #AddNewContactStringToListButton").click( function() { AddNewContactStringToList(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#AddNewContactStringToListDiv #Cancel").click( function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/mCListManager';
			 return false;  
		}); 	
		

		
		$("#inpGroupId6").change(function() 
		{ 
			OldGroupName = $("#inpGroupId6 option:selected").text();
			LastGroupId2 = $("#inpGroupId6").val();
			$("#inpGroupDesc").val($("#inpGroupId6 option:selected").text());
		});
			
		$("#loadingDlgAddPhoneNumber").hide();
				
				
	} );
		
		
</script>


<style>

#AddNewContactStringToListDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 345px;
	height: 345px;
	font-size:12px;
}


#AddNewContactStringToListDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#AddNewContactStringToListDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#AddNewContactStringToListDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}



</style> 


<cfoutput>
        
<div id='AddNewContactStringToListDiv' class="RXForm">
    
    <div id="RightStage">
        <form id="AddContactStringForm" name="AddContactStringForm" action="" method="POST">
                
        <!---        #Session.USERID#      ---> 
                  
                <label>Contact Type <span class="small">Required</span> </label>
                <select id="inpContactTypeId" class="ui-corner-all" >
                    <option value="1" <cfif inpContactTypeId EQ 1>selected</cfif>>Voice</option>
                    <option value="2" <cfif inpContactTypeId EQ 2>selected</cfif>>e-mail</option>
                    <option value="3" <cfif inpContactTypeId EQ 3>selected</cfif>>SMS</option>
                </select>  
                
                <label>Contact String <span class="small">Required</span> </label>
                <input type="text" name="inpContactString" id="inpContactString" size="255" class="ui-corner-all" /> 
                   
                <label>Notes
                <span class="small">Optional Notes or Data</span>
                </label>
                <input type="text" name="inpUserSpecifiedData" id="inpUserSpecifiedData" size="255" class="ui-corner-all" value="#inpNotes#" /> 
            
                <label>Current Source <span class="small">Optional</span></label>
                <select name="inpSourceMask_MCContactsAddContact" id="inpSourceMask_MCContactsAddContact" class="ui-corner-all"></select>
                
                
                <label>Select a Group</label>
                <span class="small">Optional</span>
                <select id="inpGroupId6" name="inpGroupId6" class="ui-corner-all"></select>
                
                <BR />
                       
                <button id="AddNewContactStringToListButton" TYPE="button" class="ui-corner-all">Add</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Done</button>
                               
                <div id="loadingDlgAddPhoneNumber" style="display:inline;">
                    <img class="LoadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
        
                <label id="AddStatus" style="color:##933"></label>
        
                
        </form>

	</div>


</div>

</cfoutput>

