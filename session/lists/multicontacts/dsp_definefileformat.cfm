<cfparam name="inpETLDefID" default="0">

<cfoutput>

	<!--- http://net.tutsplus.com/tutorials/javascript-ajax/an-introduction-to-the-raphael-js-library/ --->
	<!--- Replace with compact version in production - raphaelx.js --->
<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/MCID/raphael.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/mcid/jquery.numbericbox.js"></script>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/jquery.jgrowl.js"></script>


</cfoutput>


<!--- 

Preview file 

Auto guess format - suggestion?


XML Field Definitions
<FD id='1' desc='Upload Field 1'><LINK>1</LINK><LINK>2</LINK><TR>1</TR></FD>


--->

<script>

	var NumberUploadFields = 14;
	var HeightOfStandardFieldsList = 1000;
	var HeightOfSingleUploadField = 30;
	var UploadListTop = 0;
	var UploadListOffset = 648;
	var StandardListOffset = 325;
	var UploadFieldLineColorOff = '#006';
	var UploadFieldLineColorOn = '#09F';
		
	$(function() {
			 
		<!--- ETL Description--->
		<!---http://www.appelsiini.net/projects/jeditable --->
		$('#ETLDescription').editable( function(value, settings) { 
									// console.log(this);
									// console.log(value);
									// console.log(settings);
									 return(value);
 								 },{ 					
								  indicator : "<img src='../../public/images/loading-small.gif'>",
								  tooltip   : "Click to edit description...",
								  width : 150,
								  height : 15,
								  event     : "click",
								  type      : 'text',							
								  submit    : 'OK',
								  cancel    : 'Cancel',
								  name : "inpNewValue",
								  onblur : 'ignore',
								  data: 	function(value, settings) 
								  			{												
																					
										  		return value;
											},
								 onreset : 	function(value, settings) 
								 		   	{
										
												<!--- Reset need to refer back to the parent object--->
																					
									  			return value;
											},

								  callback : function(value, settings)
											 {												 			
												
											 }
						
							  });
	
				
		<!--- File Type--->
		$("#FileTypeSVDiv").hide();
		$("input[name='FileType']").change(function(){
			
			// console.log($("input[name='LineTerminator']:checked").val());
			
			if ($("input[name='FileType']:checked").val() == '0')
			{
				$("#FileTypeSVDiv").show();
			}
			else
			{
				$("#FileTypeSVDiv").hide();
			}
			
			if ($("input[name='FileType']:checked").val() == '1')
			{
				$(".UploadFieldLength").css("display","inline");
			}
			else
			{
				$(".UploadFieldLength").hide();
			}
			
		});
		
		<!--- Line Termination --->
		$("#LineTerminatorOtherDiv").hide();
		$("input[name='LineTerminator']").change(function(){
			
			// console.log($("input[name='LineTerminator']:checked").val());
			
			if ($("input[name='LineTerminator']:checked").val() == 'OTHER')
				$("#LineTerminatorOtherDiv").show();
			else if ($("input[name='LineTerminator']:checked").val() == 'b')
				
				;
			else
				$("#LineTerminatorOtherDiv").hide();
		});
		
		<!--- Number of fields--->
		$("#NumberOfFieldsChooser").hide();		
		$("#NumberOfFieldsChange").click(function(){
			
			$("#NumberOfFields").val(NumberUploadFields);
			$("#NumberOfFieldsChooser").show();
			
			return;
		
			// console.log($("input[name='LineTerminator']:checked").val());
						
		});

		$("#NumberOfFieldsChooserOK").click(function(){
			
			if(parseInt($("#NumberOfFields").val()) != NumberUploadFields)
			{
				$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				$.alerts.confirm( "Changing the number of fields will erase all mappings! \n\nAre you sure?", "About to delete current mappings and set number of fields to " + parseInt($("#NumberOfFields").val()) + ".", 
				function(result) 
				{
					if(!result)
					{   return;}
					else
					{	
					
						ClearAllFields(NumberUploadFields);
						
						NumberUploadFields = parseInt($("#NumberOfFields").val());
						StageCanvas.clear();
						AddUploadFields();
						$("#NumberOfFieldsChooser").hide();		
					}
				});				
				
			}
			else
			{
				$("#NumberOfFieldsChooser").hide();	
			}
		});
		
		$("#NumberOfFieldsChooserCancel").click(function(){
			$("#NumberOfFieldsChooser").hide();
		});
			
		<!--- Skip Lines--->
		$("#NumberOfLinesToSkipDiv").hide();
		$("input[name='SkipLines']").change(function(){
			
			// console.log($("input[name='LineTerminator']:checked").val());
			
			if ($("input[name='SkipLines']:checked").val() == 'SkipLines')
				$("#NumberOfLinesToSkipDiv").show();		
			else
			{
				$("#NumberOfLinesToSkip").val('0');
				$("#NumberOfLinesToSkipDiv").hide();
			}
		});
		
		
		$("#NumberOfErrorsToSkipDiv").hide();
		$("input[name='SkipErrors']").change(function(){
			
			// console.log($("input[name='LineTerminator']:checked").val());
			
			if ($("input[name='SkipErrors']:checked").val() == 'SkipErrors')
				$("#NumberOfErrorsToSkipDiv").show();		
			else
			{
				$("#NumberOfErrorsToSkip").val('0');
				$("#NumberOfErrorsToSkipDiv").hide();
			}
		});
							
		
		<!--- Define what to do if correct object is dropped onto stage --->  
		$( "#StandardFields .StandardField, #NumericFields .StandardField, #LocationFields .StandardField, #TextFields .StandardField" ).droppable({
			<!--- Custom fuction to accept both menu items and stage objects --->
			accept:  function(d) { 	if( d.hasClass("UploadFieldContainer") ) return true; },
			greedy: false,
			drop: function( event, ui ) {
				// console.log('Dropped ' + $(ui.draggable).attr('rel') + ' on ' + $(this).attr('rel'));
				// console.log(ui);			
						
				var SourceBoxWidth = 200;
				var SourceBoxHeight = 12;

				var Quadrant = 2;

				var StartX = parseInt($('#UploadedFields .UploadFieldContainer[rel="' +  $(ui.draggable).attr('rel') + '"]').position().left + SourceBoxWidth) + UploadListOffset;			
				var StartY = parseInt($('#UploadedFields .UploadFieldContainer[rel="' +  $(ui.draggable).attr('rel') + '"]').position().top + (SourceBoxHeight/2) + UploadListTop) + 15 + parseInt($("#UploadedFields").css("top")) ;
				var StopX = parseInt(ui.position.left + 5);
				var StopY = parseInt(ui.position.top + (SourceBoxHeight/2) + UploadListTop) + 15 + parseInt($("#UploadedFields").css("top")) ;

				if(StopX > 90)
					StopX = 90;	
				
				if( (ui.position.left + SourceBoxWidth + StandardListOffset + 5) < UploadListOffset)
				{
					Quadrant = 1;
					StartX = parseInt($('#UploadedFields .UploadFieldContainer[rel="' +  $(ui.draggable).attr('rel') + '"]').position().left) + UploadListOffset -13;
		   		 	StopX = parseInt(ui.position.left + SourceBoxWidth - 15);
										
					StopX = -80;	
				}
					
				DrawLinkToStandardField( (ui.draggable).attr('rel'), $(this).attr('rel'), StartX, StartY, StopX, StopY, Quadrant,'normal','','','')
			}
		});
	
		<!--- Save current changes to DB --->
		$("#SaveETL").click(function(){
			SaveData();
			return;
		});
		
		
		<!--- Cancel and or Exit current changes to DB --->
		$("#CancelETL").click(function(){
			
			<!--- Check for unsaved changes--->
		
		
			
			
			CreateDefineImportDialog.remove(); 
			return false;									
		});
		
				
		$("#DefineImportContainerFOUC").toggleClass("ui-tabs-hide");
	
		
	
		StageCanvas = new Raphael(document.getElementById('DefineImportContainer'));  
		
		StageCanvas.canvas.style.position = "absolute"; 
		StageCanvas.canvas.style.zIndex = "1500"; 
			
		<!---  set values from DB --->
 		if(1>0)
		{
			LoadETLDefs();
		}
		else
		{
			<!--- Add upload fields --->
			AddUploadFields();
	
			SetSeparator(0);
			SetLineTerminator(0);
			$("#NumberOfLinesToSkip").val(0);
			$("#NumberOfErrorsToSkip").val(0);
		}
		
		
		
		$("#loadingDlgUpdateETL").hide();
			 
	});

	function SetSeparator(inpType, inpSV)
	{
		switch(inpType)
		{
			<!--- Separator--->
			case 0: 
			
				$('[name="FileType"]').filter('[value="0"]').attr("checked","checked");
				$("#DefineImportContainer #LeftMenu #FileTypeSV").val(inpSV);
				$("#FileTypeSVDiv").show();
				break;
			
			<!--- Fixed Field Length--->
			case 1:
				$('[name="FileType"]').filter('[value="1"]').attr("checked","checked");  
				$("#FileTypeSVDiv").hide();
				break;
				
			<!--- Excel --->	
			case 2:
				$('[name="FileType"]').filter('[value="2"]').attr("checked","checked");
				$("#FileTypeSVDiv").hide();
				break;				
				
			default:
				$('[name="FileType"]').filter('[value="0"]').attr("checked","checked"); 
				$("#FileTypeSVDiv").show();
				break;				
		}	
	}


	function SetLineTerminator(inpType, inpOther)
	{
		switch(inpType)
		{
			<!--- Windows --->
			case 0: 			
				$('[name="LineTerminator"]').filter('[value="WIN"]').attr("checked","checked"); 
				$("#LineTerminatorOtherDiv").hide();
				break;
			
			<!--- Unix --->
			case 1:
				$('[name="LineTerminator"]').filter('[value="UNIX"]').attr("checked","checked");  
				$("#LineTerminatorOtherDiv").hide();
				break;
				
			<!--- Other --->	
			case 2:
				$('[name="LineTerminator"]').filter('[value="OTHER"]').attr("checked","checked");
				$("#DefineImportContainer #LeftMenu #LineTerminatorOther").val(inpOther)
				$("#LineTerminatorOtherDiv").show();
				break;				
				
			<!--- Windows --->	
			default:
				$('[name="LineTerminator"]').filter('[value="OTHER"]').attr("checked","checked"); 
				$("#LineTerminatorOtherDiv").hide();
				break;				
		}	
	}
	
	function ClearAllFields(OldNumberOfFields)
	{
		var i = 1;
		for(i=1;i<=NumberUploadFields;i++)
		{
			if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LineObj')) != "undefined")
			{					
					if($('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LineObj') != '')
					{	
						$('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LineObj').remove();
						$('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LineObj', '');					
					}		
			}
				
			if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('TempLineObj')) != "undefined")
			{					
					if($('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('TempLineObj') != '')
					{	
						$('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('TempLineObj').remove();
						$('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('TempLineObj', '');					
					}		
			}
				
			$('#UploadedFields .UploadField[rel="' + i + '"]').remove();
			$('#UploadedFields .UploadFieldLength[rel="' + i + '"]').remove();	
			$('#UploadedFields .UploadFieldContainer').remove();
			//clear transformation T link
			$('.editTrans').remove();
		}
	}

	function AddUploadFields()
	{
		var i = 1;
		for(i=1;i<=NumberUploadFields;i++)
		{
			var NewFieldString ='<div class="UploadFieldContainer" rel="' + i + '" links=""><div class="UploadField" rel="' + i + '" links="">Upload Field ' + i + '</div>';
				NewFieldString += '<input type="text" style="display:none;"  class="UploadFieldLength" tabindex="'+ i +'" rel="'+ i +'" size="3" maxlength="4" placeholder="Length" /></div>';
			$("#UploadedFields").append(NewFieldString);
			<!--- Set draggable for field items to create new link --->
			$('#UploadedFields .UploadFieldContainer[rel="' +  i + '"]').draggable({ revert: true, containment: "#DefineImportContainer", opacity: 0.8, helper: "clone", snap: ".StandardField", snapMode: "inner"});
		
		
			<!--- Modify object as it is being dragged --->
			$('#UploadedFields .UploadField[rel="' +  i + '"]').bind( "dragstart", function(event, ui) 
			{
				// $(this).css("min-width", "0");
				// $(this).css("width", "auto");
			});
			
			<!--- Modify object as it is being dragged --->
			$('#UploadedFields .UploadFieldContainer[rel="' +  i + '"]').bind( "dragstop", function(event, ui) 
			{								
					
				if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + $(this).attr('rel') + '"]').data('TempLineObj')) != "undefined")
				{					
					if($('#UploadedFields .UploadFieldContainer[rel="' + $(this).attr('rel') + '"]').data('TempLineObj') != '')
					{	
						$('#UploadedFields .UploadFieldContainer[rel="' + $(this).attr('rel') + '"]').data('TempLineObj').remove();
						$('#UploadedFields .UploadFieldContainer[rel="' + $(this).attr('rel') + '"]').data('TempLineObj', '');					
					}		
				}
			});
			
			
			<!--- Redraw line while dragging  - events are start, drag, stop --->
			$('#UploadedFields .UploadFieldContainer[rel="' +  i + '"]').bind( "drag", function(event, ui) 
			{
				var SourceBoxWidth = 200;
				var SourceBoxHeight = 12;
							
				// console.log(ui);			
										
				var Quadrant = 2;
	
				if( (ui.position.left) < -(SourceBoxWidth + 95) )
						ui.position.left = -(SourceBoxWidth + 95);
															
				var StartX = parseInt($(this).position().left + SourceBoxWidth) + UploadListOffset;			
				var StartY = parseInt($(this).position().top + (SourceBoxHeight/2) + UploadListTop) + 15 + parseInt($("#UploadedFields").css("top"));
				var StopX = parseInt(ui.position.left);
				var StopY = parseInt(ui.position.top + (SourceBoxHeight/2) + UploadListTop) + 15 + parseInt($("#UploadedFields").css("top")) ;
	
				if(StopX > 90)
					StopX = 90;		
			
				<!--- Left Side object--->
				if(ui.position.left < 0)
				{
					Quadrant = 1;
					StartX = parseInt($(this).position().left - 13) + UploadListOffset;
		
					if(StopX < -80)
						StopX = -80;						
				}
				
				DrawLinkToStandardField( $(this).attr('rel'), 0, StartX, StartY, StopX, StopY, Quadrant,'normal','','','')
				
			});
			$('#UploadedFields .UploadFieldLength[rel="' +  i + '"]').ForceNumericOnly();
		}
		
		<!--- Set display number of fields --->
		$("#NumberOfFieldsH1").html("Number Of Fields (" + NumberUploadFields + ")");
		
		<!---http://www.appelsiini.net/projects/jeditable --->
		$('.UploadField').editable( function(value, settings) { 
									// console.log(this);
									// console.log(value);
									// console.log(settings);
									 return(value);
 								 },{ 					
								  indicator : "<img src='../../public/images/loading-small.gif'>",
								  tooltip   : "Doubleclick to edit...",
								  width : 82,
								  height : 15,
								  event     : "dblclick",
								  type      : 'text',							
								  submit    : 'OK',
								  cancel    : 'Cancel',
								  name : "inpNewValue",
								  onblur : 'ignore',
								  data: 	function(value, settings) 
								  			{					
												$(this).css('height', 'auto');
												$(this).css('min-height', 'auto');							
												
										//		console.log('data');
								
																	
										  		return value;
											},
								  onreset : 	function(value, settings) 
								 		   	{
												$(this).css('height', '150px');
												$(this).css('min-height', '150px');
												
											//	console.log('onreset');
								
												<!--- Reset need to refer back to the parent object--->
																					
									  			return value;
											},

								  callback : function(value, settings)
											 {												 			
											//	console.log('callback');
												
												$(this).css('height', '150px');
												$(this).css('min-height', '150px');
											 }
						
							  });
							  
		CenterUploadFieldsList();
		
		if ($("input[name='FileType']:checked").val() == '1')
		{
			$('#UploadedFields .UploadFieldLength').css("display","inline");
		}
	}

	function CenterUploadFieldsList()
	{						
		<!---UploadListTop = (HeightOfStandardFieldsList -	(NumberUploadFields*HeightOfSingleUploadField) ) / 2;--->
		UploadListTop = 0;
		
		var HeightOfStandardFieldsList =  parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 20;
					
		$("#StandardFieldsBackground").css("top", parseInt($("#StandardFields").css("top")) + 20);
		$("#StandardFieldsBackground").css("min-width", $("#StandardFields").css("width") );
		$("#StandardFieldsBackground").css("width", $("#StandardFields").css("width") );
		$("#StandardFieldsBackground").css("min-height", parseInt( $("#StandardFields").css("height") ) - 20 );
		$("#StandardFieldsBackground").css("height", parseInt( $("#StandardFields").css("height") ) - 20);		
				
		$("#NumericFieldsBackground").css("top", parseInt($("#NumericFields").css("top")) + 20 );
		$("#NumericFieldsBackground").css("min-width", $("#NumericFields").css("width") );
		$("#NumericFieldsBackground").css("width", $("#NumericFields").css("width") );
		$("#NumericFieldsBackground").css("min-height", parseInt( $("#NumericFields").css("height") ) - 20 );
		$("#NumericFieldsBackground").css("height", parseInt( $("#NumericFields").css("height") ) - 20);
		
		$("#TextFields").css("top", parseInt($("#NumericFields").css("top")) + parseInt($("#NumericFields").css("height")) + 20);
		$("#TextFieldsBackground").css("top", parseInt($("#NumericFields").css("top")) + parseInt($("#NumericFields").css("height")) + 40);
		$("#TextFieldsBackground").css("min-width", $("#TextFields").css("width") );
		$("#TextFieldsBackground").css("width", $("#TextFields").css("width") );
		$("#TextFieldsBackground").css("min-height", parseInt( $("#TextFields").css("height") ) - 20 );
		$("#TextFieldsBackground").css("height", parseInt( $("#TextFields").css("height") ) - 20);	
		
		<!--- Align along bottom      --->
		var LocationFieldsTop =  parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 20 - parseInt( $("#LocationFields").css("height") )
		$("#LocationFields").css("top", LocationFieldsTop);
		$("#LocationFieldsBackground").css("top", parseInt($("#LocationFields").css("top")) + 20);
		$("#LocationFieldsBackground").css("min-width", $("#LocationFields").css("width") );
		$("#LocationFieldsBackground").css("width", $("#LocationFields").css("width") );
		$("#LocationFieldsBackground").css("min-height", parseInt( $("#LocationFields").css("height") ) - 20 );
		$("#LocationFieldsBackground").css("height", parseInt( $("#LocationFields").css("height") ) - 20);
		
		if(HeightOfStandardFieldsList > parseInt($("#UploadedFields").css("height")) )
		{
			$("#UploadedFields").css("top", (HeightOfStandardFieldsList - parseInt($("#UploadedFields").css("height")) ) / 2 );
	
			$("#DefineImportContainer").css("min-height", parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 60 );
			$("#DefineImportContainer").css("height", parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 60);
			
			$("#DefineImportContainerFOUC").css("min-height", parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 60 );
			$("#DefineImportContainerFOUC").css("height", parseInt( $("#TextFields").css("height") ) + parseInt( $("#NumericFields").css("height") ) + 60);	
		}
		else
		{
			$("#DefineImportContainer").css("min-height",  parseInt( $("#UploadedFields").css("height") ) + 60 );
			$("#DefineImportContainer").css("height", parseInt( $("#UploadedFields").css("height") ) + 60 );		
			
			$("#DefineImportContainerFOUC").css("min-height",  parseInt( $("#UploadedFields").css("height") ) + 60 );
			$("#DefineImportContainerFOUC").css("height", parseInt( $("#UploadedFields").css("height") ) + 60 );				
	
			<!--- Resize canvas--->
			StageCanvas.setSize(parseInt($("#DefineImportContainer").css("width")), parseInt($("#DefineImportContainer").css("height")) + 20  )
	
			
		}
		
	//	 $("#DefineImportContainer #LeftMenu").css("min-height", parseInt($("#DefineImportContainer").css("height")) + 40 );
	//	 $("#DefineImportContainer #LeftMenu").css("height", parseInt($("#DefineImportContainer").css("height")) + 40);			
		
		
		$("#UploadedFieldsBackGround").css("top", parseInt($("#UploadedFields").css("top")) + 20);		
		$("#UploadedFieldsBackGround").css("min-width", $("#UploadedFields").css("width") );
		$("#UploadedFieldsBackGround").css("min-height", $("#UploadedFields").css("height") );		
		$("#UploadedFieldsBackGround").css("width", $("#UploadedFields").css("width") );
		$("#UploadedFieldsBackGround").css("height", $("#UploadedFields").css("height") );
						
	}
	
	<!--- Draw line between objects  --->
	function DrawLinkToStandardField(source, LinkTarget, StartX, StartY, StopX, StopY, Quadrant, caseTransform, lowerLimit, upperLimit, listReplaceText)
	{
		var ConnectLine = "undefined";
		
		<!---console.log($('#UploadedFields .UploadField[rel="' + source + '"]').attr('rel'))--->
		
		if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj')) != "undefined")
		{
				if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj') != '')
				{						
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj').remove();
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj', '');					
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LinkTarget', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StartX', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StartY', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StopX', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StopY', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('Quadrant', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit', '');	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText', '');
				}
		}
	
		if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj')) != "undefined")
		{					
				if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj') != '')
				{	
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj').remove();
					$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj', '');					
				}		
		}
		
		
		var LineWidthAdjust = 2;
		<!--- Note : if line width is 3 then center y by adding 1--->
		StartY = parseInt(StartY) + LineWidthAdjust;
		StopY = parseInt(StopY) + LineWidthAdjust;
		
		
		switch(parseInt(Quadrant))
		{
		
			case 1:
				
				ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (StopX) + " " +  (StopY-StartY) + " l -5 0");
				
				break;
						
			case 2:
				
				ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 5 0 l " + (StopX-11) + " " +  (StopY-StartY) + " l 5 0");
				
				break;
				
			case 3:
				
				ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 5 0 l " + (StopX-11) + " " +  (StopY-StartY) + " l 5 0");
				
				break;
				
			case 4:
			
				ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l -5 0 l " + (StopX-11) + " " +  (StopY-StartY) + " l 5 0");
				
				break;
				
			default:
			
				ConnectLine = StageCanvas.path("M " + StartX + " " +  StartY + " l 5 0 l " + (StopX-11) + " " +  (StopY-StartY) + " l 5 0");
			
				break;	
						
		}
		
		
		if(ConnectLine != "undefined")
		{
		
			ConnectLine.attr({stroke:UploadFieldLineColorOff, 'stroke-width':4});
			// ConnectLine.toFront();
						
			<!--- Highlight when hover to the connection  - Use jquery object conversion to handle hover properly in IE --->
			$(ConnectLine[0]).hover(function()
			{
				ConnectLine.attr({stroke:UploadFieldLineColorOn});
				ConnectLine.toFront();
			}, function()
			{
				ConnectLine.attr({stroke: UploadFieldLineColorOff});
			});
						 
			
			if(LinkTarget == 0)
			{
							
				<!--- Store refernce to object in the start object's data as the line obj --->
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj', ConnectLine);
				
			}
			else
			{		
			
			
				<!--- add a double click event to line --->
				ConnectLine.dblclick(function (event) 
				{
					jConfirm( "Are you sure that you want to delete this line?", "Delete line data transformation.", function(result) { 
						if(result)
						{	
							ConnectLine.remove();	
							if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj').remove();
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj', '');					
								}		
							}
								
							if (typeof($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj').remove();
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('TempLineObj', '');					
								}		
							}
							if (typeof($('#UploadedFields .UploadFieldContainer[rel=' + source + ']').data('CaseTransform')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform', '');					
								}		
							}
							
							if (typeof($('#UploadedFields .UploadFieldContainer[rel=' + source + ']').data('LowerLimit')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit', '');					
								}		
							}
							
							if (typeof($('#UploadedFields .UploadFieldContainer[rel=' + source + ']').data('UpperLimit')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit', '');					
								}		
							}
							
							if (typeof($('#UploadedFields .UploadFieldContainer[rel=' + source + ']').data('ListReplaceText')) != "undefined")
							{					
								if($('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText') != '')
								{	
									$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText', '');
								}		
							}
							
							$('.editTrans[source='+ source +']').remove();
						}
						return false;																	
					});
					
				});			
							
				<!--- Store refernce to object in the start object's data as the line obj --->
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LineObj', ConnectLine);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LinkTarget', LinkTarget);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StartX', StartX);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StartY', StartY - LineWidthAdjust);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StopX', StopX);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('StopY', StopY - LineWidthAdjust);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('Quadrant', Quadrant);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform', caseTransform);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit', lowerLimit);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit', upperLimit);
				$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText', listReplaceText);
			}
		}
		var sideValue = 237;
		if(Quadrant == 2){
			sideValue = -8;
		}
		<!--- Remove previous Transformation link --->
		$(".editTrans[source="+ source +"]").remove();
		var template = '<span class="editTrans" title="Edit Transformation" source="'+ source +'" target="'+ LinkTarget +'" style="cursor:pointer;color:red;font-size:14px;font-weight:bold;position:absolute;left:'+ sideValue +'px;font-weight:bold">T</span>';		
		$('.StandardField[rel="' + LinkTarget + '"]').append(template);
		
		// Remove previous click event
		$('.StandardField[rel="' + LinkTarget + '"] .editTrans').unbind('click');
		// Register even edit transformation
		$('.StandardField[rel="' + LinkTarget + '"] .editTrans').click(function() {
			editTransformation(source);
		});
	}
	
	function editTransformation(source){
		var caseTransform = $('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform');
		var isUpper = false;
		var isLower = false;
		var isNormal = false;
		
		if(caseTransform == 'upper'){
			isUpper = true;
		}
		if(caseTransform == 'lower'){
			isLower = true;
		}
		if(caseTransform == 'normal'){
			isNormal = true;
		}
		
		var upperLimit = $('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit');
		var lowerLimit = $('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit');
		if(upperLimit == 'undefined'){
			upperLimit = '';
		}
		if(lowerLimit == 'undefined'){
			lowerLimit = '';
		}
		var template = '<div class="editScriptFrm" id="transformationForm">';
		template +=	' 	<div class="frm_item">';
		template += '		<div class="frm_label">Text transform</div>';
		template += '		<div class="frm_control">';
		if(isUpper || isLower){
			template +=	'		Normal <input type="radio" name="inpTransform" value="normal">';
		} else{
			template +=	'		Normal <input type="radio" name="inpTransform" value="normal" checked>';
		}
		
		if(isLower){
			template +=	'		Lower <input type="radio" name="inpTransform" value="lower" checked>';
		}else{
			template +=	'		Lower <input type="radio" name="inpTransform" value="lower" >';		
		}
		
		if(isUpper){
			template +=	'		Upper <input type="radio" name="inpTransform" value="upper" checked>';
		}
		else{
			template +=	'		Upper <input type="radio" name="inpTransform" value="upper" >';
		}
		
		template += '   </div>';
		template += ' </div>';
		
		template +=	' <div class="frm_item">';
		template += '	<div class="frm_label">Lower limit</div>';
		template += '	<div class="frm_control">';
		template +=	'		<input type="text" name="inpTransformLowerLimit" id="inpTransformLowerLimit" value="'+ lowerLimit +'" >';
		template += '   </div>';
		template += ' </div>';
		
		template +=	' <div class="frm_item">';
		template += '	<div class="frm_label">Upper limit</div>';
		template += '	<div class="frm_control">';
		template +=	'		<input type="text" name="inpTransformUpperLimit" id="inpTransformUpperLimit" value="'+ upperLimit +'" >';
		template += '   </div>';
		template += ' </div>';
		
		template +=	'<div class="frm_item" style="position: relative; top: 25px; width: 40px;">';
		template += '	<div class="frm_label">Text Replace <br /><a id="addMoreTextReplace">More</a></div>';
		template += '</div>';
		
		var EditTransformationDialog = $(template);
		EditTransformationDialog
			.dialog({
				modal : true,
				title: 'Transformation Selection',
				close: function() {
					 EditTransformationDialog.remove(); 
					 EditTransformationDialog = 0; 
				},
				position:'top',
				width: 340,
				height: 250,
				buttons: [
				    {
						text: "Save",
						click: function(event)
						{
							SaveLinkTransformation(source);
							EditTransformationDialog.remove(); 
					 		EditTransformationDialog = 0; 	
						}
					},
					{
						text: "Cancel",
						click: function(event)
						{
							EditTransformationDialog.remove(); 
					 		EditTransformationDialog = 0; 
						}
					}
				]
			});
	
		$("#inpTransformLowerLimit").ForceNumericOnly();
		$("#inpTransformUpperLimit").ForceNumericOnly();
		
		var listReplaceText = $('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText');
		for(indexRT in listReplaceText){
			generateTextPlace(source, listReplaceText[indexRT].source, listReplaceText[indexRT].destination);
		}
		if(typeof(listReplaceText) == 'undefined' || listReplaceText == ''){
			generateTextPlace(source, '', '');
		}
		// Register event to add more 
		$("#addMoreTextReplace").click(function(){
			generateTextPlace(source, '', '');
		});
	}
	
	// generate template replace text
	function generateTextPlace(source, srcText, destText){
		if(typeof($("#transformationForm").data("replaceCounter")) == 'undefined'){
			$("#transformationForm").data("replaceCounter", 1);
		}
		var replaceCounter = $("#transformationForm").data("replaceCounter");
		// Create blank field template 
		var textReplaceTemp =	' <div class="frm_item" id="replaceTextDestination'+ replaceCounter +'">';
			textReplaceTemp += '	<div class="frm_label">&nbsp;</div>';
			textReplaceTemp += '	<div class="frm_control">';
			textReplaceTemp += '		<input type="text" name="inpTextSource" id="inpTextSource" value="'+ srcText +'" style="width:80px">';
			textReplaceTemp += '	  To <input type="text" name="inpTextDestination" id="inpTextDestination" value="'+ destText +'" style="width:80px">';
			textReplaceTemp += '		<span style="cursor:pointer" title="Remove this replace text field" id="removeReplaceText" rel="replaceTextDestination'+ replaceCounter +'"> [ X ]</span>';
			textReplaceTemp += '   </div>';
			textReplaceTemp += ' </div>';
			textReplaceTemp += '</div>';
			
		$("#transformationForm").append(textReplaceTemp);
		var currentHeight = $("#transformationForm").height();
		$("#transformationForm").height(currentHeight + 27);
		$("#transformationForm").data("replaceCounter", (replaceCounter + 1));
		
		// Register event for delete text replace
		$('#removeReplaceText[rel=replaceTextDestination'+ replaceCounter +']').click(function(){
			$('#replaceTextDestination'+ replaceCounter).remove();
			$("#transformationForm").height($("#transformationForm").height() - 27);
		});	
	}
	
	// Save link transformation
	function SaveLinkTransformation(source){
		// Save data case transformation
		var caseTransform = $("#transformationForm input[name=inpTransform]:checked").val();
		$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('CaseTransform', caseTransform);
		// Save data lower limit
		var lowerLimit = $("#inpTransformLowerLimit").val();
		$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('LowerLimit', lowerLimit);
		// Save data transformation upper limit
		var upperLimit = $("#inpTransformUpperLimit").val();
		$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('UpperLimit', upperLimit);
		// Save data replace text transformation
		var index = 0;
		var arrTextReplace = new Array();
       	$("#inpTextSource[name=inpTextSource]").each(function(){
        	var value = $("#inpTextDestination[name=inpTextDestination]").get(index);
        	var objTextReplace = {};
        	objTextReplace.source = $(this).val();
        	objTextReplace.destination = $(value).val();
        	if(!(objTextReplace.source == '')){
	        	arrTextReplace.push(objTextReplace);
        	}
       		index ++;
       	});
       	$('#UploadedFields .UploadFieldContainer[rel="' + source + '"]').data('ListReplaceText', arrTextReplace);
	}
	
	function WriteLinksToString()
	{
		var OutPutXML = "";
		var BuffA;
		var BuffB;
		var BuffC;
		
		<!---
		
		XML Field Definitions
		<FD ID='1' DESC='Upload Field 1'><LINK>1</LINK><LINK>2</LINK><TR>1</TR></FD>
		
		var i = 1;
		for(i=1;i<=NumberUploadFields;i++)
		{

			if (typeof($('#UploadedFields .UploadField[rel="' + i + '"]').data('LineObj')) != "undefined")
				$('#UploadedFields .UploadField[rel="' + i + '"]').data('LineObj').remove();
	
			if (typeof($('#UploadedFields .UploadField[rel="' + i + '"]').data('TempLineObj')) != "undefined")
				$('#UploadedFields .UploadField[rel="' + i + '"]').data('TempLineObj').remove();
				
			$('#UploadedFields .UploadField[rel="' + i + '"]').remove();	
			
				$('#UploadedFields .UploadField[rel="' + i + '"]').data('LineObj', ConnectLine);
				$('#UploadedFields .UploadField[rel="' + i + '"]').data('LinkTarget', LinkTarget);
				
		}
		
		--->
		
		var i = 1;
		var start = 1;
		for(i=1;i<=NumberUploadFields;i++)
		{
			var length = '';
			
			if ($("input[name='FileType']:checked").val() == '1')
			{
				if(!isNaN(parseInt($('#UploadedFields .UploadFieldLength[rel="' + i + '"]').val())))
				{
					length = parseInt($('#UploadedFields .UploadFieldLength[rel="' + i + '"]').val());
				} 
			}
			if(i == 1){
				start = 1;
			} else {
				start = start + parseInt($('#UploadedFields .UploadFieldLength[rel="' + (i - 1)+ '"]').val());
			}
			
			// Get transformation information
			var caseTransform = $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('CaseTransform');
			if(typeof(caseTransform) == 'undefined'){
				caseTransform = "normal";
			}
			
			var lowerLimit = $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LowerLimit');
			var upperLimit = $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('UpperLimit');
			<!--- Use local defined RXencodeXML--->
			var desc=RXencodeXML( $('#UploadedFields .UploadField[rel="' + i + '"]').html() );
			if(typeof($("#UploadedFields .UploadField[rel='" + i + "'] input[name=inpNewValue]").val()) != "undefined"){
				desc = $("#UploadedFields .UploadField[rel='" + i + "'] input[name=inpNewValue]").val();
			}
			
			OutPutXML = OutPutXML + "<FD ID='" + i + "' START='"+ start +"' LENGTH='"+ length +"' DESC='" + desc + "' CASETRANSFORM='"+ caseTransform +"' LOWERLIMIT='"+ lowerLimit +"' UPPERLIMIT='"+ upperLimit +"' "
			var linkTarget = $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LinkTarget');
			if(parseInt(linkTarget)){
				if(linkTarget == 1 || linkTarget == 2 || linkTarget == 3){
					OutPutXML = OutPutXML + " DEDUPE='1' ";
				}
			}
			// Close xml 
			OutPutXML = OutPutXML + ">";
			
			// generate xml replace string
			var listReplaceText = $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('ListReplaceText') ;
			if(typeof(listReplaceText) != 'undefined'){
				for(indexRT in listReplaceText){
					OutPutXML = OutPutXML + "<RT SRC='"+ listReplaceText[indexRT].source +"' DEST='" + listReplaceText[indexRT].destination +"'>";
					OutPutXML = OutPutXML + "</RT>";
				}
			}
					
			if( parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LinkTarget') ) > 0 )
			{
				OutPutXML = OutPutXML + "<LINK " +
				"LinkTarget='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LinkTarget') ) + "' " + 
				"StartX='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('StartX') ) + "' " + 
				"StartY='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('StartY') ) + "' " + 
				"StopX='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('StopX') ) + "' " + 
				"StopY='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('StopY') ) + "' " + 
				"Quadrant='" + parseInt( $('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('Quadrant') ) + "' " + 
				">" +
				"</LINK>";				
			}
			
			OutPutXML = OutPutXML + "</FD>"
			
		}
		
		return OutPutXML;
	}
	
	function SaveData()
	{
		var isHasEmptyTarget = false;
		var listEmptyTargetField = new Array();
		var isDuplicateTarget = false;
		
		for(i=1; i<= NumberUploadFields;i++){
			var target = parseInt($('#UploadedFields .UploadFieldContainer[rel="' + i + '"]').data('LinkTarget'));
			
			for(z=1; z<= NumberUploadFields;z++){
				var targetCompare = parseInt($('#UploadedFields .UploadFieldContainer[rel="' + z + '"]').data('LinkTarget'));
				// Target= 1,2,3 is contact field target can be duplicated, others target are not duplicated
				if(targetCompare > 3 && i != z){
					if(targetCompare == target){
						isDuplicateTarget = true;
					}
				}
			}
		}
		
		if(isDuplicateTarget){
			jAlert('"Text Fields", "Location Fields" and "Numeric Fields" targets can not be duplicated.', "Duplicate target.");	
		} else {
			UpdateETLDB();
		}
	}
	
	function UpdateETLDB()
	{					
		$("#loadingDlgUpdateETL").show();		
		var inpFieldDefinitionsXML = WriteLinksToString();
		var description = $("#DefineImportContainer #LeftMenu #ETLDescription").html();
		if(typeof($("#DefineImportContainer #LeftMenu #ETLDescription input[name=inpNewValue]").val()) != 'undefined'){
			description = $("#DefineImportContainer #LeftMenu #ETLDescription input[name=inpNewValue]").val();
		}
	  	$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=UpdateETLDefinitions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			type: 'POST',
			data:
			{ 
				inpETLDefID: $("#DefineImportContainer #LeftMenu #inpETLDefID").val(), 
				inpFileType : $("#DefineImportContainer #LeftMenu input[name='FileType']:checked").val(), 
				inpFieldSeparator : $("#DefineImportContainer #LeftMenu #FileTypeSV").val(), 
				inpNumberOfFields: $("#DefineImportContainer #LeftMenu #NumberOfFields").val(), 
				inpSkipLines: $("#DefineImportContainer #LeftMenu #NumberOfLinesToSkip").val(), 
				inpSkipErrors: $("#DefineImportContainer #LeftMenu #NumberOfErrorsToSkip").val(), 
				inpLineTerminator: $("#DefineImportContainer #LeftMenu input[name='LineTerminator']:checked").val(),
				inpLineTerminatorOther: $("#DefineImportContainer #LeftMenu #LineTerminatorOther").val(), 
				inpDesc: description, 
				inpFieldDefinitionsXML: inpFieldDefinitionsXML
			}, 
			error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					<!---console.log(textStatus, errorThrown);--->
					jAlert("Data Import Definitions have NOT been updated.\n"  + textStatus + "\n" + errorThrown, "Failure!");
				},					  
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")						
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{		
									$.jGrowl("Data Import Definitions have been updated.", { life:1000, position:"center"});
									$("#loadingDlgUpdateETL").hide();																				
								}
								else
								{
									
									jAlert("Data Import Definitions have NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!");
									$("#loadingDlgUpdateETL").hide();																				
								}
								
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
							jAlert("No Response from the remote server. Check your connection and try again.", "Error.");	
						}
						
						$("#loadingDlgUpdateETL").hide();
				}
		});		
	
		return false;
	}
	
	
		
	function LoadETLDefs()
	{
		$("#loadingDlgUpdateETL").show();		
						
		$.ajax({
		url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GetETLDefinitions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		dataType: 'json',
		type: 'POST',
		data:  { inpETLDefID: $("#DefineImportContainer #LeftMenu #inpETLDefID").val() },					  
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			<!---console.log(textStatus, errorThrown);--->
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Data Import Definitions have NOT been loaded properly. .\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { $("#loadingDlgUpdateETL").hide(); } );													
		},
		success:
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{																									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{						
									
							if(typeof(d.DATA.FILETYPEID[0]) != "undefined")								
							{									
								SetSeparator(d.DATA.FILETYPEID[0], d.DATA.FIELDSEPARATOR[0]);											
							}
								
							if(typeof(d.DATA.NUMBEROFFIELDS[0]) != "undefined")						
							{																	
								ClearAllFields(NumberUploadFields);
								
								NumberUploadFields = parseInt(d.DATA.NUMBEROFFIELDS[0]);
							
								$("#DefineImportContainer #LeftMenu #NumberOfFields").val(NumberUploadFields);
							
								AddUploadFields();
								
							}								
							
							if(typeof(d.DATA.SKIPLINES[0]) != "undefined")								
							{									
								$("#DefineImportContainer #LeftMenu #NumberOfLinesToSkip").val(d.DATA.SKIPLINES[0]);
							
								if(parseInt(d.DATA.SKIPLINES[0]) > 0)
								{										
									$('[name="SkipLines"]').attr("checked","checked"); 
									$("#NumberOfLinesToSkipDiv").show();		
								}
							}
							
							if(typeof(d.DATA.SKIPERRORS[0]) != "undefined")								
							{									
								$("#DefineImportContainer #LeftMenu #NumberOfErrorsToSkip").val(d.DATA.SKIPERRORS[0]);
							
								if(parseInt(d.DATA.SKIPERRORS[0]) > 0)
								{										
									$('[name="SkipErrors"]').attr("checked","checked"); 
									$("#NumberOfErrorsToSkipDiv").show();		
								}
							}
							
							if(typeof(d.DATA.LINETERMINATOR[0]) != "undefined")								
							{									
								if(d.DATA.LINETERMINATOR[0] == "WIN")
									SetLineTerminator(0, '');
								else if(d.DATA.LINETERMINATOR[0] == "UNIX")
									SetLineTerminator(1, '');
								else
									SetLineTerminator(2, d.DATA.LINETERMINATOR[0]);												
							}
																
							if(typeof(d.DATA.DESC[0]) != "undefined")								
								$("#DefineImportContainer #LeftMenu #ETLDescription").html(d.DATA.DESC[0]);
							else
								$("#DefineImportContainer #LeftMenu #ETLDescription").html('New');	
								
							if(typeof(d.DATA.FILETYPEID[0]) != 'undefined' && d.DATA.FILETYPEID[0] == 1){
								$('#UploadedFields .UploadFieldLength').css("display","inline");	
							}	
							if(typeof(d.DATA.FIELDDEFINITIONS_XML[0]) != "undefined")								
							{	
								// console.log(d.DATA.FIELDDEFINITIONS_XML[0])	
								
								FieldData = $.parseXML("<myFDDoc>" + d.DATA.FIELDDEFINITIONS_XML[0] + "</myFDDoc>");	
								$FieldDataXML = $(FieldData);    
																
								$FieldDataXML.find("FD").each(function() 
								{
									// console.log($(this).attr("ID"));
									var CurrID = $(this).attr("ID");
									
									<!--- Process Descriptions --->
									$('#UploadedFields .UploadField[rel="' + CurrID + '"]').html($(this).attr("DESC"));
									$('#UploadedFields .UploadFieldLength[rel="' + CurrID + '"]').val($(this).attr("LENGTH"));
									var caseTransform = $(this).attr("CASETRANSFORM");
									var lowerLimit = $(this).attr("LOWERLIMIT");
									var upperLimit = $(this).attr("UPPERLIMIT");
									
									var listReplaceText = new Array();
									$(this).find("RT").each(function()
									{									
										var objReplaceText = {};
										objReplaceText.source = $(this).attr("SRC");
										objReplaceText.destination = $(this).attr("DEST");
										listReplaceText.push(objReplaceText);
									});
									
									<!--- Process Links--->
									$(this).find("LINK").each(function()
									{																				
										<!--- Get LinkTarget--->										
										DrawLinkToStandardField(CurrID, $(this).attr("LinkTarget"), $(this).attr("StartX"), 
											$(this).attr("StartY"), $(this).attr("StopX"), $(this).attr("StopY"),
											$(this).attr("Quadrant"), caseTransform, lowerLimit, upperLimit,listReplaceText);
									});
									
								});
																								
							}
																							 
						}
																									
						$("#loadingDlgUpdateETL").hide();
										
					}
					else
					{<!--- Invalid structure returned --->	
						$("#loadingDlgUpdateETL").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					jAlert("No Response from the remote server. Check your connection and try again.", "Error.");	
					$("#loadingDlgUpdateETL").hide();
				}				
			} 		
					
		});
	
		return false;
		
	}


</script>

<style>


#DefineImportContainerFOUC
{
<!---	position:absolute;
	top: 0;
	left: 0;	
	min-height: 715px;
	height: 715px;--->
	
	position:absolute;
	top: 0;
	left: 0;	
	width:1150px;
	z-index:1100;
	min-width:1150px;
	min-height: 100%;
	height: 100%;
}


#DefineImportContainer
{			
	width:1150px;
	min-width:1150px;
	z-index:1200;
	font-size:12px;
	min-height: 100%;
	height: 100%;
}

#DefineImportContainer #LeftMenu
{
	width:270px;
	min-width:270px;
	background-color:#FC9;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
	color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(255,206,153))
	
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(255,206,153)
	);
	position:absolute;
	top:-8;
	left:-17;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#DefineImportContainer h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

#NumberOfFieldsChooser
{
	position:absolute;
	top: 10;
	left: 25;
	min-width: 250px;
	width: 250px;
	min-height: 50;
	height: 50px;
	background-color:#FFF;
	border: 1px solid #333;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	padding:10px;
}

#StandardFields
{
	position:absolute;
	top: 0px;
	left: 325px;	
	z-index:2100;	
	padding:5px;
	margin:5px;	
}

#LocationFields
{
	position:absolute;
	top: 200px;
	left: 325px;	
	z-index:2100;	
	padding:5px;
	margin:5px;	
}

#UploadedFields 
{	
	position:absolute;
	top: 0px;
	left: 625px;
	z-index:2100;	
	padding:5px;
	margin:5px;
}

#NumericFields
{
	position:absolute;
	top: 0px;
	left: 925px;	
	z-index:2100;
	padding:5px;
	margin:5px;
}


#TextFields
{
	position:absolute;
	top: 200px;
	left: 925px;	
	z-index:2100;
	padding:5px;
	margin:5px;			
}


#StandardFieldsBackground 
{	
	position:absolute;
	top: 0px;
	left: 325px;
	padding:5px;
	margin:5px;
	height:100px;
	min-height:100px;	
	background-color: #066;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
	color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(0,145,145))
	
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(0,145,145)
	);
	width:280px;
	min-width:280px;
	border: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:1200;
}


#LocationFieldsBackground 
{	
	position:absolute;
	top: 200px;
	left: 325px;
	padding:5px;
	margin:5px;
	height:100px;
	min-height:100px;
	background-color: #066;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
	color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(0,145,145))
	
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(0,145,145)
	);
	width:280px;
	min-width:280px;
	border: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:1200;	
}

#UploadedFieldsBackGround 
{	
	position:absolute;
	top: 0px;
	left: 625px;
	padding:5px;
	margin:5px;
	height:800px;
	min-height:800px;	
	background-color: #CF6;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(206,255,102))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(206,255,102)
	);
	width:280px;
	min-width:280px;
	border: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:1200;	
}

#NumericFieldsBackground 
{	
	position:absolute;
	top: 0px;
	left: 925px;
	padding:5px;
	margin:5px;
	height:100px;
	min-height:100px;	
	background-color: #066;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
	color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(0,145,145))
	
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(0,145,145)
	);
	width:280px;
	min-width:280px;
	border: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:1200;
}


#TextFieldsBackground 
{	
	position:absolute;
	top: 200px;
	left: 925px;
	padding:5px;
	margin:5px;
	height:100px;
	min-height:100px;	
	background-color: #066;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
	color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(0,145,145))
	
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(0,145,145)
	);
	width:280px;
	min-width:280px;
	border: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:1200;
}

#UploadedFields .UploadFieldContainer
{
	width:200px;
	min-width:200px;
	border: 1px solid #333;
	background-color:#FFF;
	padding:5px;
	margin:5px 5px 5px 5px;
	font-size:10px;
	font-weight:bold;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;
	z-index:2200;	
	overflow:hidden;
	height:15px;
	min-height:15px;
}
#UploadedFields .UploadField
{
	display:inline;
	width: 160px;
	height: 17px;
	float:left;
	margin-left:3px;
	position:absolute;
}
#UploadedFields .UploadFieldLength
{
	float:right;
}

#StandardFields .StandardField, #NumericFields .StandardField, #LocationFields .StandardField, #TextFields .StandardField 
{
	width:200px;
	min-width:200px;	
	border: 1px solid #333;
	background-color:#FFF;
	padding:5px;
	margin:5px;	
	font-size:10px;
	font-weight:bold;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	border-radius: 20px 20px 20px 20px;	
	z-index:2200;	
}

</style>



<!---
  `UserId_int` int(11) NOT NULL,
  `ContactTypeId_int` int(4) NOT NULL,
  `TimeZone_int` smallint(6) NOT NULL,
  `CellFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `OptInFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `SourceKey_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LASTUPDATED_DT` datetime DEFAULT NULL,
  `LastAccess_dt` datetime DEFAULT NULL,
  `CustomField1_int` int(11) DEFAULT NULL,
  `CustomField2_int` int(11) DEFAULT NULL,
  `CustomField3_int` int(11) DEFAULT NULL,
  `CustomField4_int` int(11) DEFAULT NULL,
  `CustomField5_int` int(11) DEFAULT NULL,
  `CustomField6_int` int(11) DEFAULT NULL,
  `CustomField7_int` int(11) DEFAULT NULL,
  `CustomField8_int` int(11) DEFAULT NULL,
  `CustomField9_int` int(11) DEFAULT NULL,
  `CustomField10_int` int(11) DEFAULT NULL,
  `UniqueCustomer_UUID_vch` varchar(36) DEFAULT NULL,
  `ContactString_vch` varchar(255) NOT NULL DEFAULT '',
  `LocationKey1_vch` varchar(255) DEFAULT NULL,
  `LocationKey2_vch` varchar(255) DEFAULT NULL,
  `LocationKey3_vch` varchar(255) DEFAULT NULL,
  `LocationKey4_vch` varchar(255) DEFAULT NULL,
  `LocationKey5_vch` varchar(255) DEFAULT NULL,
  `LocationKey6_vch` varchar(255) DEFAULT NULL,
  `LocationKey7_vch` varchar(255) DEFAULT NULL,
  `LocationKey8_vch` varchar(255) DEFAULT NULL,
  `LocationKey9_vch` varchar(255) DEFAULT NULL,
  `LocationKey10_vch` varchar(255) DEFAULT NULL,
  `SourceKey_vch` varchar(255) NOT NULL DEFAULT '',
  `grouplist_vch` varchar(512) NOT NULL DEFAULT '0,',
  `CustomField1_vch` varchar(2048) DEFAULT NULL,
  `CustomField2_vch` varchar(2048) DEFAULT NULL,
  `CustomField3_vch` varchar(2048) DEFAULT NULL,
  `CustomField4_vch` varchar(2048) DEFAULT NULL,
  `CustomField5_vch` varchar(2048) DEFAULT NULL,
  `CustomField6_vch` varchar(2048) DEFAULT NULL,
  `CustomField7_vch` varchar(2048) DEFAULT NULL,
  `CustomField8_vch` varchar(2048) DEFAULT NULL,
  `CustomField9_vch` varchar(2048) DEFAULT NULL,
  `CustomField10_vch` varchar(2048) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,
  `SourceString_vch` text,  
  PRIMARY KEY (`UserId_int`,`ContactString_vch`,`SourceKey_vch`),
  KEY `IDX_OptInFlag_int` (`OptInFlag_int`),
  KEY `IDX_grouplist_vch` (`grouplist_vch`),
  KEY `IDX_ContactTypeId_int` (`ContactTypeId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$
--->


<!--- Find me / Follow me ?
style="position:relative; overflow: hidden;"
--->

<cfoutput>
<!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)---> 
<div id="DefineImportContainerFOUC" class="ui-tabs-hide" >
      
    <div id="DefineImportContainer">
        
        <div id="LeftMenu">
        	<input type="hidden" id="inpETLDefID" value="#inpETLDefID#" />
        
	        <div width="100%" align="center" style="margin-bottom:5px;"><h1>Data Import Definitions</h1></div>
        
        	<div width="100%" align="center" style="margin-bottom:20px;"><img class = "ETLSymbolWeb" src="../../public/images/dock/blank.gif" width="45%" height="45%"/></div>
            
            <div style="margin: 10px 5px 10px 0px">
            	<h1>Description</h1>
<!---                <div id="ETLDescription" style="height:15px; min-height:15px;">This is a test...</div>--->
                <a id="ETLDescription" style="height:15px; min-height:15px;">Default Description</a>
                
            </div>
            
            <div id="FileTypeDiv">
                <h1>File Type</h1>
                <BR />
                <div style="height:15px; min-height:15px; width:100%; position:relative;">
                    <div style="display:inline; height:15px; min-height:15px;"><input name="FileType" type="radio" value="0"/> Separated Values</div> 
                    <div id="FileTypeSVDiv" style="position:absolute; top:0px; left:135px; display:inline;">Specify: <input type="text" id="FileTypeSV" name="FileTypeSV" value="," class="ui-corner-all" style="width:50px; text-align:center;" /></div> 
            	</div>
                              
                <div style="height:15px; min-height:15px;"><input name="FileType" type="radio" value="1"/> Fixed Field Length</div>
                <div style="height:15px; min-height:15px;"><input name="FileType" type="radio" value="2"/> MS Excel</div>
            </div>    
                                  
            <BR />
            <BR />
            
            <div id="LineTerminatorDiv" style="height:15px; min-height:50px;">
                <h1>Line Terminator</h1>  
                <BR />
                <input name="LineTerminator" type="radio" value="WIN"/> Windows <BR/>
                <input name="LineTerminator" type="radio" value="UNIX"/> Unix <BR/>
                <input name="LineTerminator" type="radio" value="OTHER"/> Other 
                <div id="LineTerminatorOtherDiv" style="display:inline; margin-left:10px;">Specify: <input type="text" id="LineTerminatorOther" name="LineTerminatorOther" class="ui-corner-all" style="width:50px; text-align:center;" /> </div>
            </div>        
            
            <BR />
            <BR />
            
           <div style="height:50px; min-height:50px; width:100%; position:relative;">
                <h1>Advanced</h1>  
                <BR />
                
                <div style="height:18px; min-height:18px; width:100%; position:relative; margin-bottom:3px;">
                    <div style="display:inline; height:15px; min-height:15px; "><input name="SkipLines" type="checkbox" value="SkipLines"/> Skip Lines </div>
                    <div id="NumberOfLinesToSkipDiv" style="position:absolute; top:0px; left:135px; display:inline;">Specify: <input type="text" id="NumberOfLinesToSkip" name="NumberOfLinesToSkip" value="0" style="width:50px; text-align:center;" class="ui-corner-all"/></div> 
          		</div>
          
                <div style="height:18px; min-height:18px; width:100%; position:relative;  margin-bottom:3px;">
                    <div style="display:inline; height:15px; min-height:15px;"><input name="SkipErrors" type="checkbox" value="SkipErrors"/> Skip Errors</div> 
                    <div id="NumberOfErrorsToSkipDiv" style="position:absolute; top:0px; left:135px; display:inline;">Specify: <input type="text" id="NumberOfErrorsToSkip" name="NumberOfErrorsToSkip" value="0" style="width:50px; text-align:center;" class="ui-corner-all"/></div> 
            	</div>
                
            </div>      
            
            <BR />
            <BR />
            
            <BR />
            <BR />
            
            <div id="NumberOfFieldsDiv" style="height:50px; min-height:50px; width:100%;">
                                              
                <h1 id="NumberOfFieldsH1">Number of Fields</h1>  <a id="NumberOfFieldsChange">Change</a>                              
               
                <div id="NumberOfFieldsChooser" align="center">
                    
                    How Many Fields? <input type="text" id="NumberOfFields" name="NumberOfFields" value="10" class="ui-corner-all" style="width:50px; text-align:center;" />
                    <br />
                    <button id="NumberOfFieldsChooserOK">OK</button> <button id="NumberOfFieldsChooserCancel">Cancel</button>
                    
                </div>
                
            </div>    
            
            <BR />
            <BR />
            
            <div style="width:100%" align="right">	
                <div id="loadingDlgUpdateETL" style="display:inline;">
                    <img class="loadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
        		       
	            <button id="SaveETL">Save</button> 
                <button id="CancelETL">Cancel / Exit</button>    
            </div>       
            
            
        </div>
        
        
        <div id="StandardFieldsBackground"></div>
        <div id="StandardFields">	
            <h1>Standard Fields</h1>        
            <div class="StandardField" rel="1">Phone Number</div>
            <div class="StandardField" rel="2">eMail Address</div>
            <div class="StandardField" rel="3">SMS Number</div>
        </div>
        
        <div id="LocationFieldsBackground"></div>
        <div id="LocationFields">	
            <h1>Location Fields</h1>
            <div class="StandardField" rel="301">Location Key 1 - Time Zone</div>
            <div class="StandardField" rel="302">Location Key 2 - Address Line 1</div>
            <div class="StandardField" rel="303">Location Key 3 - Address Line 2</div>
            <div class="StandardField" rel="304">Location Key 4 - City</div>
            <div class="StandardField" rel="305">Location Key 5 - State</div>
            <div class="StandardField" rel="306">Location Key 6 - Postal Code</div>
            <div class="StandardField" rel="307">Location Key 7</div>
            <div class="StandardField" rel="308">Location Key 8</div>
            <div class="StandardField" rel="309">Location Key 9</div>
            <div class="StandardField" rel="310">Location Key 10</div>   
         </div>   
       
        <div id="UploadedFieldsBackGround"></div>
        <div id="UploadedFields">
        
                <h1>Source Fields</h1>  
                <br />
                <!--- Dynamically Populated--->        
            
        </div>
        
        
        <div id="NumericFieldsBackground"></div>
        <div id="NumericFields">
            <h1>Numeric Fields</h1>
            <div class="StandardField" rel="101">Custom Number Field 1</div>
            <div class="StandardField" rel="102">Custom Number Field 2</div>
            <div class="StandardField" rel="103">Custom Number Field 3</div>
            <div class="StandardField" rel="104">Custom Number Field 4</div>
            <div class="StandardField" rel="105">Custom Number Field 5</div>
            <div class="StandardField" rel="106">Custom Number Field 6</div>
            <div class="StandardField" rel="107">Custom Number Field 7</div>
            <div class="StandardField" rel="108">Custom Number Field 8</div>
            <div class="StandardField" rel="109">Custom Number Field 9</div>
            <div class="StandardField" rel="110">Custom Number Field 10</div>
        </div>
        
        
        <div id="TextFieldsBackground"></div>
        <div id="TextFields">                   
            <h1>Text Fields</h1>
            <div class="StandardField" rel="201">Custom Text Field 1</div>
            <div class="StandardField" rel="202">Custom Text Field 2</div>
            <div class="StandardField" rel="203">Custom Text Field 3</div>
            <div class="StandardField" rel="204">Custom Text Field 4</div>
            <div class="StandardField" rel="205">Custom Text Field 5</div>
            <div class="StandardField" rel="206">Custom Text Field 6</div>
            <div class="StandardField" rel="207">Custom Text Field 7</div>
            <div class="StandardField" rel="208">Custom Text Field 8</div>
            <div class="StandardField" rel="209">Custom Text Field 9</div>
            <div class="StandardField" rel="210">Custom Text Field 10</div>    
        </div>
        
    </div>
	
</div>
</cfoutput>