
<script TYPE="text/javascript">
			
		
	$(function()
	{	
			<!--- In MC List manager for reuse--->
	 		ReloadETLData('#EtidELTListDiv #inpETLIDSelDiv', 1)
	 
	 
			$("#EtidELTListDiv #EditETLButton").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$("#EtidELTListDiv #Cancel").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			
			$("#EtidELTListDiv #EditETLButton").click( function() { DefineImportDialog($("#EtidELTListDiv #inpETLIDSelDiv #inpETLIDUploadSel option:selected").val()); CreateEditETLDialog.remove(); return false;  }); 	
			
			<!--- Kill the new dialog --->
			$("#EtidELTListDiv #Cancel").click( function() 
				{
						$("#loadingDlgEtidELTList").hide();					
						CreateEditETLDialog.remove(); 
						return false;
				}); 		
			  
			
			$("#loadingETLList").hide();	
	
	  	  
	} );
		
		
</script>


<style>

#EtidELTListDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 345px;
	height: 345px;
	font-size:12px;
}


#EtidELTListDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}

#EtidELTListDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#EtidELTListDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 



<cfoutput>
        
<div id='EtidELTListDiv' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Edit an Existing Data Import Definition</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "contact-new-icon_web" src="../../public/images/dock/blank.gif" /></div>

		<div style="margin: 10px 5px 10px 20px; line-height: 20px;">
         	<ul>
         		<li>Choose an existing Data Import Definition from the drop down list.</li>
                <li>Optionally start a new one by selecting start new</li>                
            </ul>
        </div>
            
	</div>
    
    <div id="RightStage">
             
        <form id="EditETLForm" name="EditETLForm" action="" method="POST">
        
                <label>Select a Data Import Definition</label>
                        
                <div id='inpETLIDSelDiv'>
                    <select id="inpETLIDUploadSel" name="inpETLIDUploadSel" class="ui-corner-all"></select>
                </div>
                        
                <br/>
                        
                <button id="EditETLButton" TYPE="button" class="ui-corner-all">Edit Now</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
                <div id="loadingETLList" style="display:inline;">
                    <img class="loadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
                        
        </form>

	</div>


</div>

</cfoutput>






















