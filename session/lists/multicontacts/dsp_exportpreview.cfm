<cfparam name="sidx" default="ContactString_vch">
<cfparam name="sord" default="ASC">
<cfparam name="dialstring_mask" default="">
<cfparam name="MCCONTACT_MASK" default="">
<cfparam name="inpSourceMask" default="">
<cfparam name="notes_mask" default=""> 
<cfparam name="type_mask" default="0"> 
<cfparam name="INPCONTACTTYPE" default="0"> 
<cfparam name="INPGROUPID" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="inpDoRules" default="0">

<!--- Validate Session User---> 
<cfif Session.USERID GT 0>

<cfif INPCONTACTTYPE GT 0>
	<cfset type_mask = INPCONTACTTYPE>
</cfif>
     
     
	<cfif inpDoRules EQ 0 OR INPBATCHID EQ 0>
    
        <cfset RulesgeneratedWhereClause = "">
    
    <cfelse>
    
        <cfset NEXTBATCHID = INPBATCHID>
        
        <cfinclude template="../../CFC/Includes/act_ProcessDistributionRules.cfm">
    
    </cfif>
                    
                    
    <!--- Cleanup SQL injection --->
                         
    <!--- Get data --->
    <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
        SELECT
            ContactString_vch,
            UserSpecifiedData_vch,
            ContactTypeId_int
        FROM
            simplelists.rxmultilist
        WHERE                
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                  
		<cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
            AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
        </cfif>
        
        <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
            AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
        </cfif>              
        
        <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
        </cfif>    
        
        <cfif type_mask NEQ "" AND type_mask NEQ "undefined" AND type_mask NEQ "0">
            AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#type_mask#">        
        </cfif>     
        
        <!--- Move group into main list for easier select?--->
        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
            AND 
                ( grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%' )                        
        </cfif>  
        
        <cfif RulesgeneratedWhereClause NEQ "">
         	#PreserveSingleQuotes(RulesgeneratedWhereClause)#
       	</cfif>
       
        <cfif sidx NEQ "" AND sord NEQ "">
            ORDER BY #sidx# #sord#
        </cfif>
                    
    </cfquery>
    
    
    
    <body>
    <!---
    <cfheader name="Content-Disposition" 
    value="inline;filename=PhoneListExport_G#inpGroupID#.xls">
    <cfcontent TYPE="application/vnd.ms-excel">    
        --->
        
        <div id="ExportPreviewContainer" style="width:500px; max-height:600px; overflow-y: scroll; overflow-x: hidden;">
        
        <!---	<div style="display:inline; width:250px; min-width:250px; overflow:hidden;">Phone Number</div><div style="display:inline; width:250px; min-width:250px; overflow:hidden;">Notes</div>
        
         	<cfoutput query="GetNumbers">
    
			<cfset DisplayOutPutNumberString = #ContactString_vch#>
            
              <!--- Leave e-mails alone--->
                <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>
                        
                    <!--- North american number formating--->
                    <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                        <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                    </cfif>
                
                </cfif>
                        
            
           	<BR><div style="display:inline; width:50%; overflow:hidden;">#DisplayOutPutNumberString#</div><div style="display:inline; width:50%; overflow:hidden;">#UserSpecifiedData_vch#</div> 
           
            </cfoutput>--->
    
    
        
            <table border="0" width="500px">
            <tr>
            
             <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>                        
                   <th width="250px" align="left">Phone Number</th><th width="250px" align="left">Notes</th>                	
             <cfelse>                    
                    <th width="250px" align="left">eMail Address</th><th width="250px" align="left">Notes</th>                
             </cfif>
                
            
            </tr>
            
            <cfset CurrRowNumber = 0>
            <cfoutput query="GetNumbers">
            
            <cfset CurrRowNumber = CurrRowNumber + 1>
            
            <cfset DisplayOutPutNumberString = #ContactString_vch#>
            
              <!--- Leave e-mails alone--->
                <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>
                        
                    <!--- North american number formating--->
                    <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                        <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                    </cfif>
                	
                    <cfelse>
                    
                    
                
                </cfif>
                        
                <cfif (CurrRowNumber MOD 2) GT 0>
            		<tr bgcolor="##00CC99">                     
                <cfelse>
                	 <tr>                
                </cfif>         
           
            <td style="padding: 1px 1px 1px 5px;">#DisplayOutPutNumberString#</td><td style="padding: 1px 1px 1px 5px;">#LEFT(UserSpecifiedData_vch, 100)#</td> 
            </tr>
            </cfoutput>
            </table>
    
        
        </div>
        
        
        <!---
        
        
        
        
        
        
        
    <table border="2">
    <tr>
    <td> Phone Number </td><td> Notes </td>
    </tr>
    <cfoutput query="GetNumbers">
    
    <cfset DisplayOutPutNumberString = #ContactString_vch#>
    
      <!--- Leave e-mails alone--->
        <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>
                
			<!--- North american number formating--->
            <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
            </cfif>
        
        </cfif>
                
    <tr>
    <td>#DisplayOutPutNumberString#</td><td>#UserSpecifiedData_vch#</td> 
    </tr>
    </cfoutput>
    </table>
    --->
 <!---   </cfcontent>--->

	</body>

</cfif>
