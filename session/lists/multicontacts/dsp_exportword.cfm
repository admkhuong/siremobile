<cfparam name="sidx" default="ContactString_vch">
<cfparam name="sord" default="ASC">
<cfparam name="dialstring_mask" default="">
<cfparam name="MCCONTACT_MASK" default="">
<cfparam name="inpSourceMask" default="">
<cfparam name="notes_mask" default=""> 
<cfparam name="type_mask" default="0"> 
<cfparam name="INPGROUPID" default="0">

<!--- Validate Session User---> 
<cfif Session.USERID GT 0>
     
    <!--- Cleanup SQL injection --->
    
    <!--- Get data --->
    <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
        SELECT
            ContactString_vch,
            UserSpecifiedData_vch,
            ContactTypeId_int
        FROM
            simplelists.rxmultilist
        WHERE                
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                  
		<cfif MCCONTACT_MASK NEQ "" AND MCCONTACT_MASK NEQ "undefined">
            AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#MCCONTACT_MASK#%">              
        </cfif>
        
        <cfif notes_mask NEQ "" AND notes_mask NEQ "undefined">
            AND UserSpecifiedData_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#notes_mask#%">            
        </cfif>              
        
        <cfif inpSourceMask NEQ "" AND inpSourceMask NEQ "undefined">
            AND SourceKey_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpSourceMask#%">        
        </cfif>    
        
        <cfif type_mask NEQ "" AND type_mask NEQ "undefined" AND type_mask NEQ "0">
            AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#type_mask#">        
        </cfif>     
        
        <!--- Move group into main list for easier select?--->
        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
            AND 
                ( grouplist_vch LIKE '%,#INPGROUPID#,%' OR grouplist_vch LIKE '#INPGROUPID#,%' )                        
        </cfif>  
       
        <cfif sidx NEQ "" AND sord NEQ "">
            ORDER BY #sidx# #sord#
        </cfif>
                    
    </cfquery>
    
    <body>
     
    <cfheader name="Content-Disposition" 
    value="attachment;filename=PhoneListExport_G#inpGroupID#.doc">
    <cfcontent TYPE="application/msword">    
    
    <table border="2">
    <tr>
    <td> Phone Number </td><td> Notes </td>
    </tr>
    <cfoutput query="GetNumbers">
    
    <cfset DisplayOutPutNumberString = #ContactString_vch#>
    
      <!--- Leave e-mails alone--->
        <cfif GetNumbers.ContactTypeId_int EQ 1 OR GetNumbers.ContactTypeId_int EQ 3>
                
			<!--- North american number formating--->
            <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
            </cfif>
        
        </cfif>
                
    <tr>
    <td>#DisplayOutPutNumberString#</td><td>#UserSpecifiedData_vch#</td> 
    </tr>
    </cfoutput>
    </table>
    
    </cfcontent>
    </body>

</cfif>
