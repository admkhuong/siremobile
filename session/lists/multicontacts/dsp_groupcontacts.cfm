<script TYPE="text/javascript">

var OldGroupName = "";

	function GroupContacts()
	{					
		$("#loadingDlgAddToGroup").show();		
		
		<!--- Get currently selected phone strings --->	
		var INPCONTACTLIST = $("#MCStringList").jqGrid('getGridParam','selarrrow');

		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GroupContacts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpGroupId: $("#inpGroupId4").val(), INPCONTACTLIST: String(INPCONTACTLIST) }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{			
						
							
																												
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.jGrowl("Selected Phone Numbers have been added to Group " + OldGroupName + ".", 
					            { 
						             life:2000, 
						             position:"center",
						             header:'Success!',
						             beforeClose:function(e,m,o){
						                $("#loadingDlgAddToGroup").hide();
										<!--- Populate Group list box from AJAX--->																					
										$CreateGroupContactsDialog.remove(); 
						             }
					            });																	
							}
							else
							{
					            $.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Group has NOT been deleted. \n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddToGroup").hide();	
			} );		
	
		return false;

	}
	
	
	$(function()
	{				
			$("#GroupContactsDiv #GroupContactsButton").click( function() { 
			
				$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				$.alerts.confirm( "About to add to " + OldGroupName + ".\n\nAre you absolutely sure?", "About to add numbers to group information.", function(result) { if(!result){$("#loadingDlgAddToGroup").hide(); return;}else{	
	
				GroupContacts(); return false;  
				
				 }  } );<!--- Close alert here --->
			
			}); 	
			
			<!--- Kill the new dialog --->
			$("#GroupContactsDiv #Cancel").click( function() 
				{
						$("#loadingDlgAddToGroup").hide();					
						$CreateGroupContactsDialog.remove(); 
						return false;
				}); 		
			  
			  
			ReloadMCContactsGroupData("inpGroupId4", 2);
		  
			$("#inpGroupId4").change(function() 
			{ 
				OldGroupName = $("#inpGroupId4 option:selected").text();
				LastGroupId3 = $("#inpGroupId4").val();
			});
			
			$("#loadingDlgAddToGroup").hide();	
	
		  
	} );
		
		
</script>


<style>

#GroupContactsDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 375px;
	height: 375px;
	font-size:12px;	
}

#GroupContactsDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#GroupContactsDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#GroupContactsDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 

<cfoutput>
        
<div id='GroupContactsDiv' class="RXForm">
    
    
    <div id="LeftMenu">
    
            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Add Selected Contacts to a Group</h1></div>
    
            <div width="100%" align="center" style="margin-bottom:20px;"><img class = "Group_Web128x128" src="../../public/images/dock/blank.gif" /></div>
    
            <div style="margin: 10px 5px 10px 20px; line-height: 20px;">
                <ul>
                    <li>Choose a <i>Group</i> from the drop down list.</li>
                    <li>Press "Add Numbers to Group" button when ready.</li>
                    <li>Press "Cancel" button to exit without adding Group.</li>
                </ul>
                
                <BR />
                <BR />
                <i>DEFINITION:</i> A Group is a logical container for managing lists of single or multi-channel contacts.
            
            </div>
                
     </div>
        
     <div id="RightStage">
        
        <form id="GroupContactsForm" name="GroupContactsForm" action="" method="POST">
        
                <label>Select a group to add numbers to</label>
                <span class="small">Pick a group</span>
                <select id="inpGroupId4" name="inpGroupId4"></select>
                
                <br/>
                                        
                <button id="GroupContactsButton" TYPE="button" class="ui-corner-all">Add Numbers to Group</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
                <div id="loadingDlgAddToGroup" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>        
        </form>
    
    </div>
</div>

</cfoutput>






















