<cfparam name="dataTable" default="0">
<style>
	#previewPhoneListGrid{
		border: 1px solid #3399CC;
	    border-radius: 15px 15px 15px 15px;
	    padding: 10px;
	    margin: 10px;
	    background-color: white;
	}
	
	#previewPhoneListGrid th{
		border:1px solid #8f8e7c;
		background-color: #a7fcf3;
		width: 80px;
	}
	
	#previewPhoneListGrid td{
		border:1px solid #8f8e7c;
		padding:3px;
		width: 80px;
	}
	#previewPhoneListGrid #number{
		width: 7%;
	}
</style>
<script type="text/javascript">
	var dataTable = <cfoutput>#dataTable#</cfoutput>
	$(function() {
		var dataArray = 0;
		var template = "";
		template = '<tr>';
		template += '<th>No.</th>';
		if(typeof(dataTable.TYPE) != 'undefined'){
			if(dataTable.TYPE == 'XLS'){
				var colNameIndex = 0;
				if(dataTable.SKIPLINE > 0)
				{
					colNameIndex = dataTable.SKIPLINE -1;
				}
				dataArray = dataTable.QUERY.DATA;
				for(index in dataArray[colNameIndex]){
					template += '<th>'+ dataArray[colNameIndex][index] +'</th>';
				}
				template += '</tr>';
			} else {
				dataArray = dataTable.DATA;
				for(index in dataTable.COLUMNNAMES){
					template += '<th>'+ dataTable.COLUMNNAMES[index] +'</th>';
				}
				template += '<th>Valid</th>';
				template += '</tr>';
			}	
		}
		var count = 1;
		for(index in dataArray){
			if(index >= dataTable.SKIPLINE){
				template += '<tr>';
				template += '<td id="number">'+ count +'</td>';
				count++;
				for(i in dataArray[index]){
					template += '<td>'+ dataArray[index][i] +'</td>';
				}
				template += '</tr>';
			}
		}
		$("#previewPhoneListGrid").append(template);
	});
</script>
<div style="max-height:500px;max-width:800px;overflow:auto">
<table id="previewPhoneListGrid" class="previewPhoneList"></table>
</div>

