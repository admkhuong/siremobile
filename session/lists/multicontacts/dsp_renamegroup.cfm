<script TYPE="text/javascript">

	OldGroupName = "";

	function RenameGroup()
	{					
		$("#loadingDlgRenameGroup").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=RenameGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpGroupId: $("#inpGroupId2").val(), INPGROUPDESC : $("#RenameGroupDiv #INPGROUPDESC").val() }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{													
					
						
																			
	
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Old Group " + OldGroupName + " has been renamed to " + d.DATA.INPGROUPDESC[0] + ".", "Success!", function(result) 
																			{ 
																				$("#loadingDlgRenameGroup").hide();
																				<!--- Populate Group list box from AJAX--->	
																				ReloadMCContactsGroupData("inpGroupId_MCContacts", 1); 
																				gridReload_MCContacts();
																				CreateRenameGroupDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Group has NOT been renamed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRenameGroup").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{					
		<!---	$("#RenameGroupDiv #RenameGroupButton").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$("#RenameGroupDiv #Cancel").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		--->	
			$("#RenameGroupDiv #RenameGroupButton").click( function() { RenameGroup(); return false;  }); 	
			
			<!--- Kill the new dialog --->
			$("#RenameGroupDiv #Cancel").click( function() 
				{
						$("#loadingDlgRenameGroup").hide();					
						CreateRenameGroupDialog.remove(); 
						return false;
				}); 		
			  
			  
			ReloadMCContactsGroupData("inpGroupId2");
		  
			$("#inpGroupId2").change(function() 
			{ 
				OldGroupName = $("#inpGroupId2 option:selected").text();
				LastGroupId2 = $("#inpGroupId2").val();
				$("#INPGROUPDESC").val($("#inpGroupId2 option:selected").text());
			});
			
			$("#loadingDlgRenameGroup").hide();	
	
		  
	} );
		
		
</script>


<style>

#RenameGroupDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 375px;
	height: 375px;
	font-size:12px;
}


#RenameGroupDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#RenameGroupDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#RenameGroupDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 

<cfoutput>
        
<div id='RenameGroupDiv' class="RXForm">
        
    <div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Rename a Group</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "EditGroup_Web_128x128" src="../../public/images/dock/blank.gif" /></div>

		<div style="margin: 10px 5px 10px 20px; line-height: 20px;">
         	<ul>
            	<li>Select an Existing <i>Group</i></li>
         		<li>Give your <i>Group</i> a title you can use to uniquely distiguish it.</li>                               
                <li>Press "Rename" button when ready.</li>
                <li>Press "Cancel" button to exit without changes.</li>
            </ul>
            
            <BR />
            <BR />
            <i>DEFINITION:</i> A Group is a logical container for managing lists of single or multi-channel contacts.
            
        </div>
            
	</div>
    
    <div id="RightStage">
    
        <form id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">
        
                <label>Select a Group to Rename</label>
                <span class="small">Only user added groups may be renamed.</span>
                <select id="inpGroupId2" name="inpGroupId2"></select>
                
                <br/>
                
                <label>New Group Name</label>
                <span class="small">Must be unique.</span>
                <input TYPE="text" name="INPGROUPDESC" id="INPGROUPDESC" size="255" /> 
                
                <br/>
                        
                <button id="RenameGroupButton" TYPE="button" class="ui-corner-all">Rename</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
                <div id="loadingDlgRenameGroup" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
                        
        </form>
	
    </div>
</div>


</cfoutput>





















