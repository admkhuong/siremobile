<cfparam name="inpTZContactString" default="0" >
<cfparam name="inpTimeZone" default="0" >
<cfparam name="INPCONTACTTYPEID" default="1" >

<!---

Time Zone — Mellissa Data is just the number of hours past Greenwich Mean Time. The time zones are:
RX is Mellissa plus 23
   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
--->

<script TYPE="text/javascript">

			  
var OldTimeZone = "##";

	function UpdateTZ()
	{					
		$("#loadingDlgUpdateTZ").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=UpdateContactTZData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpContactString: $("#UpdateTZForm #inpContactString").val(), inpTimeZone : $("#UpdateTZForm #inpTimeZone").val(), INPCONTACTTYPEID : <cfoutput>#INPCONTACTTYPEID#</cfoutput> }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								gridReload_MCContacts();
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Time Zone has been updated.", "Success!", function(result) 
																			{ 
																				$("#loadingDlgUpdateTZ").hide();
																				CreateUpdateTimeZoneDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Time Zone has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdateTZ").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{			
			$("#UpdateTZDiv #UpdateTZButton").click( function() { UpdateTZ(); return false;  }); 	
			
			<!--- Kill the new dialog --->
			$("#UpdateTZDiv #Cancel").click( function() 
				{
						$("#loadingDlgUpdateTZ").hide();					
						CreateUpdateTimeZoneDialog.remove(); 
						return false;
				}); 		
			  			  
			
			$("#loadingDlgUpdateTZ").hide();	
	
		  
	} );
		
		
</script>


<style>

#UpdateTZDiv{
width:450px;
border: none;
display:inline;
}


#UpdateTZDiv div{
display:inline;
border:none;
}

</style> 

<cfoutput>
        
<div id='UpdateTZDiv' class="RXForm">

<form id="UpdateTZForm" name="UpdateTZForm" action="" method="POST">

		<label>Contact to Change Time Zone for: #inpTZContactString#</label>
		<input TYPE="hidden" name="inpContactString" id="inpContactString" value="#inpTZContactString#" /> 
        
        <BR>        
        
        <select name="inpTimeZone" id="inpTimeZone" size="1" style="width:90px;" title="Must bne a valid time zone - not unknown - to use with Babble Blaster tool"> 
                    <option value="0" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 0>selected="selected"</cfif>>Unknown</option>
                    <option value="27" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 27>selected="selected"</cfif>>Atlantic</option>
                    <option value="28" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 28>selected="selected"</cfif>>Eastern</option>
                    <option value="29" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 29>selected="selected"</cfif>>Central</option>
                    <option value="30" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 30>selected="selected"</cfif>>Mountain</option>
                    <option value="31" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 31>selected="selected"</cfif>>Pacific</option>
                    <option value="32" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 32>selected="selected"</cfif>>Alaska</option>
                    <option value="33" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 33>selected="selected"</cfif>>Hawaii</option>
                    <option value="34" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 34>selected="selected"</cfif>>Samoa</option>
                    <option value="37" class="MinuteSelectOption" <cfif #inpTimeZone# EQ 37>selected="selected"</cfif>>Guam</option>
         </select>        
        
        <br/>
                
        <button id="UpdateTZButton" TYPE="button">Update</button>
        <button id="Cancel" TYPE="button">Cancel</button>
        
        <div id="loadingDlgUpdateTZ" style="display:inline;">
            <img class="loadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
                
</form>
</cfoutput>
</div>
























