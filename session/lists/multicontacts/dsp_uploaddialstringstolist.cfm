
<cfparam name="inpSocialmediaFlag" default="0">
<cfparam name="inpGroupOptions" default="1">


<!--- for uploading audio files --->
<cfoutput>
	<script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/ajaxfileupload.js"></script>
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.filestyle.js"></script>
</cfoutput>

        
<script TYPE="text/javascript">

	var OldGroupName7 = "";
	var UploadId = "0";
	var UploadedFileName = "error";
	var OrgFileName = "error";
	var inpFullFilePath = "";
	var UploadStausLevel = 0;

	function ajaxPhoneListFileUpload()
	{	
	<!---	$("#loading").show();--->

		inpFullFilePath = $("#inpFileName").val();
						
		$.ajaxFileUpload
		(
			{
				url:'<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/lists/act_AsyncWritePhoneListFile?inpSocialmediaFlag=' + <cfoutput>#inpSocialmediaFlag#</cfoutput> + '&inpGroupId=' + $("#UploadPhoneListDiv #inpGroupId7").val(),
				secureuri:false,
				fileElementId:'inpFileName',
				dataType: 'json',				
				complete:function()
				{					
					<!---$("#loading").hide();--->
					
				},				
				success: function (data, status)
				{						
				
					
							
							
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							UploadId = data.UploadId;
							UploadedFileName = data.UniqueFileName;	
							OrgFileName = data.OrgFileName;	
							
							$("#UploadMonitor").append("File Name: " + inpFullFilePath + "<br/>");
							$("#UploadMonitor").append("Step 1 of 10 - File posted to server OK." +  $("#loadingDlgUploadFile").clone().html() + "<br/>");
							
							$("#UploadPhoneListDiv").hide();
							
							$("#UploadMonitor").show();
						
							UploadStausLevel = 0;
							
							UploadStatusalertTimerId = setTimeout ( "GetCurrentUploadStatus()", 3000 );				
											
						}
					}															
				},
				error: function (data, status, e)
				{						
					alert('general Error ' + e);	
				}
			}
		);
		
		return false;

	}
	
	$(function()
	{	
		
		$("#UploadMonitor").hide();

		<!--- Hide the button so it doesnt get clicked twice --->
		$("#UploadPhoneListDiv #UploadPhoneListButton").click( function() { $("#UploadPhoneListDiv #UploadPhoneListButton").hide(); ajaxPhoneListFileUpload();	return false;  }); 		
		
		<cfif inpGroupOptions GT 0>
			ReloadGroupData("inpGroupId7", 1);
		  
			$("#inpGroupId7").change(function() 
			{ 
				OldGroupName = $("#inpGroupId7 option:selected").text();
				LastGroupId2 = $("#inpGroupId7").val();
				$("#inpGroupDesc").val($("#inpGroupId7 option:selected").text());
			});
		</cfif>	
		
		$("#loadingDlgUploadFile").hide();	
	} );
	
	
	function GetCurrentUploadStatus()
	{					
		<!---$("#loading").show();		--->
	
		<!--- Get latest upload by USERID --->
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=GetUploadStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  {  }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								if(d.DATA.ERRORCODE_INT < 0)
								{
									if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId);
									$("#UploadMonitor .loadingDlgUploadFileImg").remove();
									$("#UploadMonitor").append("Error During Upload Processing. (" + d.DATA.ErrorMsg_vch + "). <br/>");
									$("#UploadMonitor").append("Step 10 of 10 - Upload Stopped - Errors.<br/>");
								}
								else
								{<!--- Error checking OK--->
								
								
									if(UploadStausLevel == 0)
									{
										<!--- Check if file size data is available yet --->
										if(d.DATA.fileSize_int > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											
											$("#UploadMonitor").append("Step 2 of 10 - File size is (" + d.DATA.fileSize_int + ") bytes. " + $("#loadingDlgUploadFile").clone().html() + "<br/>");										
											UploadStausLevel = 1;										
										}
									}
																	
									if(UploadStausLevel == 1)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.FileStagedToDB_int > 0)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor").append("Step 3 of 10 - File has been staged to DB for processing. " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = 2;
										}
									}
									
									if(UploadStausLevel == 2)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.RecordsInFile_int > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor").append("Step 4 of 10 - Records in file (" + d.DATA.RecordsInFile_int + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = 3;
										}
									}
									
									if(UploadStausLevel == 3)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.DuplicatesInFile_int > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor").append("Step 5 of 10 - Duplicate Records in file count (" + d.DATA.DuplicatesInFile_int + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = 4;
										}
									}
																								
									
									if(UploadStausLevel < 4)
										UploadStatusalertTimerId = setTimeout ( "GetCurrentUploadStatus()", 1000 );
									else
									{
										$("#UploadMonitor .loadingDlgUploadFileImg").remove();
										$("#UploadMonitor").append("Step 10 of 10 - Upload Complete.<br/>");	
									}
								}<!--- Error checking OK--->
							}
							else
							{
								
								if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId);
								$("#UploadMonitor .loadingDlgUploadFileImg").remove();
								$("#UploadMonitor").append("Error During Upload Processing. (" + d.DATA.MESSAGE + " - " + d.DATA.ERRMESSAGE + "). <br/>");
							
							}							
						}
						else
						{<!--- Invalid structure returned --->	
							<!---$("#loading").hide();--->
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					<!--- show LOCALOUTPUT menu options - re-show regardless of success or failure --->
				<!--- 	$("#EditMCIDForm_" + inpQID + " #RXEditSubMenu_" + inpQID).show(); --->
					
					<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
					<!--- $("#EditMCIDForm_" + inpQID + " #RXEditSubMenuWait").html(''); --->
			} );		
	
		return false;

	}
	
			
</script>


<style>

#UploadPhoneListDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}

#UploadPhoneListDiv #UploadPhoneListButton{
margin:2px 0 20px 10px;
font-size:12px;
padding:4px 2px;
}

#UploadPhoneListDiv #UploadPhoneListButton:hover{
margin:2px 0 20px 10px;
font-size:12px;
padding:4px 2px;
cursor:pointer;
}

#UploadMonitor{
font-size:12px;
margin:0 5;
width:450px;
min-height:500px;
padding:5px;
border: none;
}

</style> 

<cfoutput>

<div id="loadingDlgUploadFile">
    <img class="loadingDlgUploadFileImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
</div>
        
<div id='UploadPhoneListDiv' class="RXForm">

<form name="form" action="" method="POST" enctype="multipart/form-data">

 		<label>File Path/Name To Upload </label> <span class="small">TYPE or browse for your file</span>
        
        <input TYPE="file" name="inpFileName" id="inpFileName" size="60" class="ui-corner-all" /> 
        <BR />
      
      	<cfif inpGroupOptions GT 0>
            <label>Optional - Select a Group to add to.</label>
            <span class="small">Add phone number to group.</span>
            <select id="inpGroupId7" name="inpGroupId7" class="ui-corner-all"></select>
             
             <BR />
             
        <cfelse>
      		<input TYPE="hidden" id="inpGroupId7" value="0" />           
        </cfif>
        
        <button id="UploadPhoneListButton" TYPE="button">Upload Phone List</button>        
</form>

</div>

<div id="UploadMonitor">




</div>

</cfoutput>
























