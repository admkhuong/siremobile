
<cfparam name="inpSocialmediaFlag" default="0">
<cfparam name="inpGroupOptions" default="1">

<!--- for uploading audio files --->
<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/fileuploader.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.filestyle.js"></script>
	<link href="#rootUrl#/#PublicPath#/css/fileuploader.css" rel="stylesheet" type="text/css">
</cfoutput>

        
<script TYPE="text/javascript">

	var OldGroupName7 = "";
	var UploadId = "0";
	var UploadedFileName = "error";
	var OrgFileName = "error";
	var inpFullFilePath = "";
	var UploadStausLevel = 0;

	function ajaxPhoneListFileUpload()
	{	
	<!---	$("#loading").show();--->

		inpFullFilePath = $("#inpFileName").val();
		var fileName = $("#fileNameId").val();
		var fileServerUploadName = $("#fileUploadId").val();
		var fileSize = $("#fileSizeId").val();
		$.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/act_AsyncWritePhoneListFile', 
			dataType: 'json',
			data:  { 
				inpSocialmediaFlag : '<cfoutput>#inpSocialmediaFlag#</cfoutput>',
				inpGroupId: $("#UploadPhoneListDiv #inpGroupId7").val(),
				inpETLDefID:$("#UploadPhoneListDiv #inpETLIDUploadSel").val(),
				fileUploadName:fileName,
				fileServerUploadName:fileServerUploadName,
				fileUploadSize:fileSize
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			 <!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					var d = eval('(' + xhr.responseText + ')');
					if(typeof(d.error) != 'undefined')
					{
						if(d.error != '')
						{
							alert(d.error);
						}else
						{
							UploadId = d.UploadId;
							UploadedFileName = d.UniqueFileName;	
							OrgFileName = d.OrgFileName;	
							
							$("#UploadMonitor #StatusArea").append("File Name: " + fileName + "<br/>");
							$("#UploadMonitor #StatusArea").append("Step 1 of 9 - File posted to server OK." +  $("#loadingDlgUploadFile").clone().html() + "<br/>");
							
							$("#UploadPhoneListDiv #RightStage").hide();
							
							$("#UploadPhoneListDiv #UploadMonitor").show();
						
							UploadStausLevel = 0;
							
							UploadStatusalertTimerId = setTimeout ( "GetCurrentUploadStatus()", 3000 );				
											
						}
					}
				} 		
			}); 
		
		return false;

	}
	
	function previewPhoneListFileUpload()
	{	
	<!---	$("#loading").show();--->

		inpFullFilePath = $("#inpFileName").val();
		var fileName = $("#fileUploadId").val();
		var fileSize = $("#fileSizeId").val();
		$.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/act_AsyncPreviewPhoneListFile', 
			dataType: 'json',
			data:  { 
				inpSocialmediaFlag : '<cfoutput>#inpSocialmediaFlag#</cfoutput>',
				inpGroupId: $("#UploadPhoneListDiv #inpGroupId7").val(),
				inpETLDefID:$("#UploadPhoneListDiv #inpETLIDUploadSel").val(),
				fileUploadName:fileName,
				fileUploadSize:fileSize
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				var d = eval('(' + xhr.responseText + ')');
				var $previewPhoneListDialog = $('<div id="ConversionScriptDialog" class="StageObjectEditDiv"></div>');
				$previewPhoneListDialog
					.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_PreviewPhoneList',
						{dataTable:JSON.stringify( d.dataTable ) }
					)
					.dialog({
						modal : true,
						title : 'Preview Phone List',
						width : 'auto',
						position:'top',
						height : 'auto',
						close : function() {
							$(this).dialog('destroy');
							$(this).remove();
							$previewPhoneListDialog = 0;
						}
					})
				  .bind("ajaxStop", function(){
				      $(this).dialog('option', 'position', 'top');
				  });

			} 		
		}); 
						
		return false;

	}
			
	$(function()
	{	
		
		var uploader = new qq.FileUploader({
              element: document.getElementById('uploadFileControl'),
              fileNameId:'fileNameId',
              fileUploadId:'fileUploadId',
              fileSizeId:'fileSizeId',
              action: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/ajaxUpload.cfc',
			  params: {method: 'Upload'}
         });  
		
		$("#UploadMonitor").hide();

		<!--- In MC List manager for reuse--->
		ReloadETLData('#UploadPhoneListDiv #RightStage #inpETLIDUploadSelDiv', 0);
			
			
		<!--- Hide the button so it doesnt get clicked twice --->
		$("#UploadPhoneListDiv #UploadPhoneListButton").click( function() {
				if($("#fileNameId").val() != ''){
					$("#UploadPhoneListDiv #UploadPhoneListButton").hide(); 
					ajaxPhoneListFileUpload();	
					return false;  
				} 
			}); 		
		<!--- Hide the button so it doesnt get clicked twice --->
		$("#UploadPhoneListDiv #PreviewPhoneListButton").click( function() {
				if($("#fileUploadId").val() != ''){
					previewPhoneListFileUpload();
					return false;  
				} 
			}); 
			
		<cfif inpGroupOptions GT 0>
			ReloadMCContactsGroupData("inpGroupId7", 1);
		  
			$("#inpGroupId7").change(function() 
			{ 
				OldGroupName = $("#inpGroupId7 option:selected").text();
				LastGroupId2 = $("#inpGroupId7").val();
				$("#inpGroupDesc").val($("#inpGroupId7 option:selected").text());
			});
		</cfif>	
		
		
		<!--- Kill the new dialog --->
		$("#UploadPhoneListDiv #Cancel").click( function() {$("#loading").hide(); if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId); CreateUploadPhoneToListDialog.remove(); return false;  }); 	
		$("#UploadPhoneListDiv #Cancel2").click( function() {$("#loading").hide(); if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId); CreateUploadPhoneToListDialog.remove(); return false;  }); 	
	
		$("#UploadPhoneListDiv #DefineImport").click(function() { EditETLDialog(); return false; });
	
	
		$("#loadingDlgUploadFile").hide();	
	} );
	
	
	function GetCurrentUploadStatus()
	{					
		<!---$("#loading").show();		--->
	
		<!--- Get latest upload by USERID --->
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists.cfc?method=GetUploadStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  {  }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								if(d.DATA.ERRORCODE_INT < 0)
								{
									if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId);
									$("#UploadMonitor .loadingDlgUploadFileImg").remove();
									$("#UploadMonitor #StatusArea").append("Error During Upload Processing. (" + d.DATA.ErrorMsg_vch + "). <br/>");
									$("#UploadMonitor #StatusArea").append("Upload Stopped - Errors.<br/>");
								}
								else
								{<!--- Error checking OK--->
								
									<!--- Define as variables to make it easier to add more steps later--->
									var CurrStep = 0;
									var TotalSteps = 9;
								
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if file size data is available yet --->
										if(d.DATA.FILESIZE_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - File size is (" + d.DATA.FILESIZE_INT + ") bytes. " + $("#loadingDlgUploadFile").clone().html() + "<br/>");										
											UploadStausLevel = CurrStep + 1;										
										}
									}
									CurrStep++;
																	
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.FILESTAGEDTODB_INT > 0)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - File has been staged to DB for processing. " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
									
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.RECORDSINFILE_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - Records in file (" + d.DATA.RECORDSINFILE_INT + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
									
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.DUPLICATESINFILE_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - Duplicate Records in file count (" + d.DATA.DUPLICATESINFILE_INT + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
									
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.COUNTELLIGIBLEPHONE_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - Elligible phone numbers in file count (" + d.DATA.COUNTELLIGIBLEPHONE_INT + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
									
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.COUNTELLIGIBLEEMAIL_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - Elligible eMail addresses in file count (" + d.DATA.COUNTELLIGIBLEEMAIL_INT + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
																		
									if(UploadStausLevel == CurrStep)
									{
										<!--- Check if data has been staged to DB yet--->
										if(d.DATA.COUNTELLIGIBLESMS_INT > -1)
										{
											$("#UploadMonitor .loadingDlgUploadFileImg").remove();
											$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + TotalSteps + " - Elligible SMS numbers in file count (" + d.DATA.COUNTELLIGIBLESMS_INT + "). " + $("#loadingDlgUploadFile").clone().html() + "<br/>");
											UploadStausLevel = CurrStep + 1;
										}
									}
									CurrStep++;
									
									if(UploadStausLevel < CurrStep)
										UploadStatusalertTimerId = setTimeout ( "GetCurrentUploadStatus()", 1000 );
									else
									{
										$("#UploadMonitor .loadingDlgUploadFileImg").remove();
										$("#UploadMonitor #StatusArea").append("Step " + (CurrStep + 2 ) + " of " + (CurrStep + 2 ) + " - Upload Complete.<br/>");	
									}
								}<!--- Error checking OK--->
							}
							else
							{
								
								if(UploadStatusalertTimerId > 0) clearTimeout(UploadStatusalertTimerId);
								$("#UploadMonitor .loadingDlgUploadFileImg").remove();
								$("#UploadMonitor #StatusArea").append("Error During Upload Processing. (" + d.DATA.MESSAGE + " - " + d.DATA.ERRMESSAGE + "). <br/>");
							
							}							
						}
						else
						{<!--- Invalid structure returned --->	
							<!---$("#loading").hide();--->
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					<!--- show LOCALOUTPUT menu options - re-show regardless of success or failure --->
				<!--- 	$("#EditMCIDForm_" + inpQID + " #RXEditSubMenu_" + inpQID).show(); --->
					
					<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
					<!--- $("#EditMCIDForm_" + inpQID + " #RXEditSubMenuWait").html(''); --->
			} );		
	
		return false;

	}
			
</script>


<style>

#UploadPhoneListDiv
{
	margin:0 5;
	width:450px;
	padding:5px;
	border: none;
	display:inline;
	min-height: 315px;
	height: 315px;
	font-size:12px;
}

#UploadMonitor{
	font-size:12px;
	margin:0 5;
	width:450px;
	min-height:500px;
	padding:5px;
	border: none;
}


#UploadPhoneListDiv #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#UploadPhoneListDiv #RightStage, #UploadPhoneListDiv #UploadMonitor
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#UploadPhoneListDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 

<cfoutput>

<div style="min-height:360px; height:360px;">
                
    <div id="loadingDlgUploadFile">
        <img class="loadingDlgUploadFileImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
                    
                    
    <div id='UploadPhoneListDiv' class="RXForm">
        
        <div id="LeftMenu">
    
            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Upload a File of Contacts to the List</h1></div>
    
            <div width="100%" align="center" style="margin-bottom:20px;"><img class = "Load-DB--icon_web" src="../../public/images/dock/blank.gif" /></div>
    
            <div style="margin: 10px 5px 10px 20px; line-height: 20px;">
                <ul>
                    <li>Choose a file to upload from your computer.</li>
                    <li>*For single contacts use the add Contact to list option.</li>
                    <li>Optionally specify a group to add new contacts to.</li>
                    <li>Press "Upload List" button when ready.</li>
                    <li>Press "Cancel" button to exit without uploading.</li>
                    <li>Press "Exit" button to exit when done.</li>
                </ul>
            </div>
                
        </div>
        
        
         <div id="RightStage">
                 
            <form name="form" action="" method="POST" enctype="multipart/form-data">
            
                    <label>Click button below to upload file.</label>
                    
                    <div id="uploadFileControl">
						
					</div>
                    <BR />
                  
                    <label>Select a Data Import Definition</label>
                    
                    <div id='inpETLIDUploadSelDiv'>
                    	<select id="inpETLIDUploadSel" name="inpETLIDUploadSel" class="ui-corner-all"></select>
                    </div>
                    
                    <BR />
                    
                    <cfif inpGroupOptions GT 0>
                        <label>Optional - Select a Group to add to.</label>
                        <span class="small">Add phone number to group.</span>
                        <select id="inpGroupId7" name="inpGroupId7" class="ui-corner-all"></select>
                         
                         <BR />
                         
                    <cfelse>
                        <input TYPE="hidden" id="inpGroupId7" value="0" />           
                    </cfif>
                    <input TYPE="hidden" id="fileNameId" value="" />
					<input TYPE="hidden" id="fileUploadId" value="" />
					<input TYPE="hidden" id="fileSizeId" value="" />          
                    <button id="UploadPhoneListButton" TYPE="button" class="ui-corner-all">Upload List</button>
					<button id="PreviewPhoneListButton" TYPE="button" class="ui-corner-all">Preview</button>
                    <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>        
                  
            </form>
                            
        </div>
   
        <div id="UploadMonitor">
            <div id="StatusArea">
            
            </div>
            
            <div>
            	<button id="Cancel2" TYPE="button" class="ui-corner-all ui-widget ">Exit</button>    
        	</div>
                
        </div>
    
     
    </div>
    
   

</div>

</cfoutput>