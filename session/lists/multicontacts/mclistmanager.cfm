
<cfparam name="inpSourceMask_MCContacts" default="0">
<cfparam name="inpShowSocialmediaOptions" default="0">

<cfif inpShowSocialmediaOptions GT 0>
	<cfset ListTitle = "Multi Channel Lists - Social media Version">
    <cfset ListPopupTitleText = "Contact">
<cfelse>
	<cfset ListTitle = "Multi Channel Lists">
    <cfset ListPopupTitleText = "Contact">
</cfif>
<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.18.custom.min.js"></script>
<style>

#panel_MCFilters {
	width: 300px;
	height: 440px;
	position: absolute;
	bottom: -465px;
	left: 480px;
	z-index: 200;
	background-color:#FFF;
	border: 1px solid #B6B6B6;
	
	-moz-border-radius-topleft: 20px;
	-webkit-border-top-left-radius: 20px;
	border-top-left-radius: 20px;
	
	-moz-border-radius-bottomleft: 20px;
	-webkit-border-bottom-left-radius: 20px;
	border-bottom-left-radius: 20px;
	
	-moz-border-radius-bottomright: 20px;
	-webkit-border-bottom-right-radius: 20px;
	border-bottom-right-radius: 20px;
	
	padding: 10px;
	 
}
#panel_tab_MCFilters {
	width: 140px;
	height: 16px;
	position: absolute;
	top: -29px;
	right: -1px;
	text-decoration: none;
	color: #313131;
	border: 1px solid #B6B6B6;
	border-bottom:1px solid #FFF;;
		
	-moz-border-radius-topleft: 20px;
	-webkit-border-top-left-radius: 20px;
	border-top-left-radius: 20px;
	
	-moz-border-radius-topright: 20px;
	-webkit-border-top-right-radius: 20px;
	border-top-right-radius: 20px;
	
	padding: .5em 1em; 
	text-decoration: none; 
	font-size: 12px; 
	font-weight: bold; 
	text-shadow: 0 1px 0 rgba(255,255,255,0.5);
	
	background: url(../../public/css/mb/images/bg_fallback_MBGreenWeb.png) 0 0 repeat-x;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
}

#panel_tab_MCFilters:focus {
	outline: none;
	
	
	
	
	
} 


#LaunchContainer
{
	
	width: 100px;
	height: 100px;
	position: absolute;
	top: 555px;
	right: 25px;
}

	
<!---	.dock-container_BabbleMCContacts { position: relative; top: -8px; height: 50px; padding-left: 20px; z-index:1; display:inline;  }
 ---> 		
</style>


<script>
		var $items = $('#vtab>ul>li.VoiceVTab');
		$items.addClass('selected');
		
var timeoutHnd_MCContacts;
var flAuto_MCContacts = true;
var lastsel_MCContacts;

var LastSource_MCContacts = -1;
var LastTypeId_MCContacts = 1;
var LastGroupId_MCContacts = 0;
var ShowRequest_MCContacts = 0;
var ShowTZ_MCContacts = 0;
var lastObj_MCContacts;
var sipPos_MCFilters = 0;

	
$(function() {

	$("#MCStringList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GetMCListData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=<cfoutput>#inpShowSocialmediaOptions#</cfoutput>',
		datatype: "json",
		height: "auto",
		colNames:['Contact','Type','Notes', 'Time&nbsp;Zone', 'Options &nbsp;&nbsp;&nbsp;'],
		colModel:[	
			{name:'ContactString_vch',index:'ContactString_vch', width:150, editable:false, sortable:true, resizable:false},
			{name:'ContactTypeId_int',index:'ContactTypeId_int', width:70, editable:false, sortable:true, resizable:false},
			{name:'UserSpecifiedData_vch',index:'UserSpecifiedData_vch', width:290, editable:true, sortable:true, resizable:false},
			{name:'TimeZone_int',index:'TimeZone_int', width:110, editable:false, sortable:true, resizable:false},
			{name:'Options',index:'Options', width:165, editable:false, align:'right', sortable:false, resizable:false}
		],
		rowNum:15,
	//   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Records Found.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'ContactString_vch',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: true,
<!---	ondblClickRow: function(ContactString_vch, RowId ){ if(!ShowRequest_MCContacts) $('#MCStringList').jqGrid('editRow',ContactString_vch,true, '', '', '', {inpContactTypeId:$('#MCStringList').getCell(2, 'ContactTypeId_int'), inpRowId: RowId}, rx_AfterEdit,'', ''); }, --->
		onSelectRow: function(ContactString_vch){
				if(ContactString_vch && ContactString_vch!==lastsel_MCContacts){
					$('#MCStringList').jqGrid('restoreRow',lastsel_MCContacts);
					//$('#MCStringList').jqGrid('editRow',ContactString_vch,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel_MCContacts=ContactString_vch;
				}
			},
		 loadComplete: function(data){ 	
		 		 				 		  
				$(".del_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_Row").click( function() { DeleteContacts($(this).attr('rel'), $(this).attr('rel3')); } );
				$(".edit_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_Row").click( function() { EditContacts($(this).attr('rel'), $(this).attr('rel3')); } );
				
				$(".reinvite_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".reinvite_Row").click( function() { ReinvitePhoneNumber($(this).attr('rel')); } );
				
				$(".add_RowToList").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".add_RowToList").click( function() {AddContactStringToListDialog($(this).attr('rel'), $(this).attr('rel2')); } );
				
				
				$(".currtz_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".currtz_Row").click( function() { UpdateTimeZoneDialog($(this).attr('rel'), $(this).attr('rel2'), $(this).attr('rel3')) ;  } );
				
			
				<!--- Add a filter row--->
				
				var MCStringListSearchFilterRow = ''+
				'<tr class="ui-widget-content jqgrow ui-row-ltr" role="row" id="MCStringListSearchFilterRow" aria-selected="true">'+
				'	<TD></TD><td aria-describedby="MCStringList_ContactString_vch" title=""  align="left" style="text-align:left;" role="gridcell">'+
				'	&nbsp;</td>'+
				'	<td aria-describedby="MCStringList_cb" style="" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="MCStringList_UserSpecifiedData_vch" title=""  align="left" style="text-align:left;" role="gridcell">'+
				'	&nbsp;</td>'+
				'	<td aria-describedby="MCStringList_cb" style="" role="gridcell"></td>'+
				'	<td aria-describedby="MCStringList_TimeZone_int" title="" style="display:none;" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="MCStringList_Options" title="" style="text-align:right;" role="gridcell">&nbsp;</td>'+
				'</tr>';
				

				<!--- Append to end of table body - nice and tight to the form - be sure structure is correct--->
				$("#MCStringList tbody").append(MCStringListSearchFilterRow);
				
				<!--- Bind all search functionality to new row --->
				$("#MCStringList #search_dialstring").unbind();
				$("#MCStringList #search_notes").unbind();
				
				$("#MCStringList #search_dialstring").keydown(function(event) { doSearch_MCContacts(arguments[0]||event,'search_dialstring') }); 
				$("#MCStringList #search_notes").keydown(function(event) { doSearch_MCContacts(arguments[0]||event,'search_notes') }); 
				
				<!--- Reset the focus to an inline filter box--->
				if(typeof(lastObj_MCContacts) != "undefined" && lastObj_MCContacts != "")
				{					
					
					setTimeout(function() 
					{ 
						 var LocalFocus = $("#MCStringList #" + lastObj_MCContacts);
						 if (LocalFocus.setSelectionRange) 
						 {         
							 <!--- ... then use it        
							   (Doesn't work in IE)
							   Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh. 
							  ---> 
							   var len = $(LocalFocus).val().length * 2;         
							   LocalFocus.setSelectionRange(len, len);        
						 }        
						 else
  			          	 {
						     <!---... otherwise replace the contents with itself         
							   (Doesn't work in Google Chrome)    --->     
							  $(LocalFocus).focus(); 
							  var str = $(LocalFocus).val();
							  $(LocalFocus).val("");    
							  $(LocalFocus).val(str);   
					     }         
						 
						 <!--- Scroll to the bottom, in case we're in a tall textarea
						  (Necessary for Firefox and Google Chrome)--->
						 LocalFocus.scrollTop = 999999; 
						lastObj_MCContacts = undefined;
					}, 10);
				}
				
   			},	
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=UpdateContactData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
		  }	
	});
	
	<!--- Add name of table gview first - so not to interfere with other tabs--->	
//	$('#gview_MCStringList .ui-jqgrid-hdiv').hide();
	$('#pagerDiv').hide();

	$("#MCStringList_ContactString_vch").css('text-align', 'left');
	$("#MCStringList_UserSpecifiedData_vch").css('text-align', 'left');
    $("#MCStringList_ContactTypeId_int").css('text-align', 'left');
	$("#MCStringList_TimeZone_int").css('text-align', 'left');
	$("#MCStringList_Options").css('text-align', 'right');
	
	$("#refresh_List").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#MCStringList_toppager_left").html("<b>Contacts</b><img id='refresh_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");

	$('#MCStringList').jqGrid('hideCol', 'TimeZone_int'); 


<!---alert($("#MCStringList").getCell("1111111112", "ContactString_vch"));  console.log($("#MCStringList").getCell("1111111112", "ContactString_vch"));'--->
<!---$("#grid_id").setGridParam({rowNum:10}).trigger("reloadGrid");
 $("#MCStringList").resetSelection();
 <b>My Babble MCContacts</b>
--->

<!---//	$("#t_MCStringList").height(30);
//	$("#t_MCStringList").html(<cfinclude template="../lists/dsp_PhoneListToolbar">);--->
	
	$("#inpSourceMask_MCContacts").change(function() 
    { 
		LastSource_MCContacts = $("#inpSourceMask_MCContacts").val();
		gridReload_MCContacts();

	});
		
	$("#inpTypeId_MCContacts").change(function() 
    { 
		LastTypeId_MCContacts = $("#inpTypeId_MCContacts").val();
		gridReload_MCContacts();

	});
	
	$("#inpGroupId_MCContacts").change(function() 
    { 
		LastGroupId_MCContacts = $("#inpGroupId_MCContacts").val();
		gridReload_MCContacts();

	});
	
		
	$("#edit_Notes").click(function() { $('#MCStringList').jqGrid('editRow',lastsel_MCContacts,true, '', '', '', '', rx_AfterEdit,'', ''); });
	
	$("#del_Phone").click(function() { DeleteContacts(); return false; });
	
		
	$("#refresh_List").click(function() { gridReload_MCContacts(); return false; });
	
	$("#MCStringList_toppager_left").click(function() { gridReload_MCContacts(); return false; });
	
	$("#Upload_Phone").click(function() { UploadPhoneToListDialog(); return false; });
			
	$("#OustandingRequestsIcon").click(function() { 
	
					
		return false; 
	
	});
	
	<!--- Populate Group list box from AJAX--->	
	ReloadMCContactsScorceKeyData("inpSourceMask_MCContacts", 1);
		
	$("#filter_MCContacts").change(
		function()
		{
			gridReload_MCContacts();				
		}
	);
	
	$("#filter_Outgoing").change(
		function()
		{
			gridReload_MCContacts();				
		}
	);
	
	$("#filter_Incoming").change(
		function()
		{
			gridReload_MCContacts();				
		}
	);
	
	$("#filter_Fresh").change(
		function()
		{
			gridReload_MCContacts();				
		}
	);

	<!--- Populate Group list box from AJAX--->	
	ReloadMCContactsGroupData("inpGroupId_MCContacts", 1);
	
 	LoadGroupCounts();
 
});

function ToggleTZLinks()
{
	if(ShowTZ_MCContacts)
	{	
		$('#MCStringList').jqGrid('hideCol', 'TimeZone_int');   
		$('#Show_TZ').html('Show Time Zone Links');
		ShowTZ_MCContacts = 0;
	}
	else
	{			
		$('#MCStringList').jqGrid('showCol', 'TimeZone_int');
		$('#Show_TZ').html('Hide Time Zone Links');
		ShowTZ_MCContacts = 1;
	}
}

var OldGroupName = "";




function ReloadMCContactsScorceKeyData(inpObjName, inpShowSystemGroups, inpCallBack)
{
	$("#loadingPhoneList").show();		
	
	if(typeof(inpShowSystemGroups) == 'undefined')
		  inpShowSystemGroups = 0;
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GetSourceData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpShowSystemGroups : inpShowSystemGroups},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var GroupOptions = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								
								if(CurrRXResultCode > 0)
								{								
									if(LastSource_MCContacts == d.DATA.GROUPID[x])
										GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" SELECTED class="">' + d.DATA.GROUPNAME[x] + '</option>';
									else
										GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" class="">' + d.DATA.GROUPNAME[x] + '</option>';
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								GroupOptions += '<option value="' + d.DATA.GROUPID[0] + '" SELECTED class="">' + d.DATA.GROUPNAME[0] + '</option>';
							}
							
								
							<!--- Allow differnet html elements to get filled up--->	
							$("#" + inpObjName).html(GroupOptions);
								 				
											
							if(typeof(inpCallBack) != 'undefined')
		  						inpCallBack();
								
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}



function ReloadMCContactsGroupData(inpObjName, inpShowSystemGroups, inpCallBack)
{
	$("#loadingPhoneList").show();		
	
	if(typeof(inpShowSystemGroups) == 'undefined')
		  inpShowSystemGroups = 0;
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=GetGroupData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  {inpShowSystemGroups : inpShowSystemGroups},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							var GroupOptions = "";
							
							for(x=0; x<d.ROWCOUNT; x++)
							{
								
								if(CurrRXResultCode > 0)
								{								
									if(LastGroupId_MCContacts == d.DATA.GROUPID[x])
										GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" SELECTED class="">' + d.DATA.GROUPNAME[x] + '</option>';
									else
										GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" class="">' + d.DATA.GROUPNAME[x] + '</option>';
								}							
							}
							
							if(CurrRXResultCode < 1)
							{
								GroupOptions += '<option value="' + d.DATA.GROUPID[0] + '" SELECTED class="">' + d.DATA.GROUPNAME[0] + '</option>';
							}
							
								
							<!--- Allow differnet html elements to get filled up--->	
							$("#" + inpObjName).html(GroupOptions);
								 				
							OldGroupName = $("#" + inpObjName + " option:selected").text();												
											
							if(typeof(inpCallBack) != 'undefined')
		  						inpCallBack();
								
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} 		
					
		});


	return false;
}

<!---
This function is called after an add/edit happens. We just take the MSG from the response and display it in the toolbar.
Note currently, since we reload the grid after the add/edit, the msg will only be visible for a short second or so
--->
	function commonSubmit(data,params)
	{
		return true;
	}	


<!--- Updates data to scrubbed values on the server side - removes formatting--->
function rx_AfterEdit(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#MCStringList").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{							
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{								
			
				if(typeof(d.DATA.INPCONTACTID[0]) != "undefined")								
				{					 	
						$("#MCStringList").jqGrid('setRowData',rowid,{ContactString_vch:d.DATA.INPCONTACTID[0]});
				}
				
				if(typeof(d.DATA.INPCONTACTID[0]) != "undefined")								
				{					 	
						$("#MCStringList").jqGrid('setRowData',rowid,{UserSpecifiedData_vch:d.DATA.INPUSERSPECIFIEDDATA[0]});
				}
				
			}
				
		}
		else
		{<!--- Invalid structure returned --->	
			
		}
	}
	else
	{<!--- No result returned --->
		
	}  
};

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DeleteContacts(inpContactList, inpContactType)
{	
	<!--- Get currently selected phone strings --->	
	<!---var inpContactList = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
	
	<!--- Dont go overboard on displaying too much info --->
	var displayList =  Left(inpContactList, 50);
	
	if(String(inpContactList).length > 50)
		displayList = displayList + " (...)";
		
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to delete\n(" + displayList + ") \n\nAre you absolutely sure?", "About to delete contacts from your <cfoutput>#ListPopupTitleText#</cfoutput> list.", function(result) { if(!result){$("#loadingPhoneList").hide();  $("#MCStringList").setSelection(lastsel_MCContacts);  return;}else{	
		
		  $("#loadingPhoneList").show();		
				
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=DeleteContactStrings&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpContactList: String(inpContactList), INPCONTACTTYPEID: inpContactType},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
							}
																										
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridReload_MCContacts();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;
}

<!--- CuongVM 2011.12.14 ADD : Contacts editor dialog --->

var EditContactsDialog = 0;
<!--- dsp_AbuseReport needs below parameters --->
var inpUserId = 0;
var inpScriptId = 0;

function EditContacts(ContactString, inpContactType)
{
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
<!---	if(AutoCheckConversationDetailTimer > 0) clearTimeout(AutoCheckConversationDetailTimer);--->
						
	var ParamStr = "?inpContactString=" + encodeURIComponent(ContactString) + "&inpContactType=" + encodeURIComponent(inpContactType);
						
	<!--- Erase any existing dialog data --->
	if(EditContactsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		EditContactsDialog.remove();
		EditContactsDialog = 0;
		
	}
	
	var div = $('<div></div>');
	EditContactsDialog = div.append($loading.clone());
	
	EditContactsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/dsp_editRXMuliListDialog' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Edit Contact',
			width: 700,
			height: 'auto',
			position: 'top'
		});

	EditContactsDialog.dialog('open');
	//bb_center_popup(div, -1000);

	return false;
}

function ReinvitePhoneNumber(inpContactList)
{	
	<!--- Get currently selected phone strings --->	
	<!---var inpContactList = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
	
	<!--- Dont go overboard on displaying too much info --->
	var displayList =  Left(inpContactList, 50);
	
	if(String(inpContactList).length > 50)
		displayList = displayList + " (...)";
		
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to re-invite\n(" + displayList + ") \n\nAre you absolutely sure?", "About to re-invite number(s) to your <cfoutput>#ListPopupTitleText#</cfoutput> list.", function(result) { if(!result){$("#loadingPhoneList").hide();  $("#MCStringList").setSelection(lastsel_MCContacts);  return;}else{	
		
		  $("#loadingPhoneList").show();		
				
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=ReinvitePhoneStrings&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { inpContactList: String(inpContactList), inpSocialmediaFlag : <cfoutput>#inpShowSocialmediaOptions#</cfoutput>},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
							}
																										
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridReload_MCContacts();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;
}


function UnGroupContacts()
{	
	if(jQuery("#MCStringList").getGridParam('selrow')==null || jQuery("#MCStringList").getGridParam('selrow')=='null'){
		//custom the alert status: 'error','highlight'
		$.jAlert('No contact was chosen !', 'error',function(){
			
		});
		return;
	}	
	<!--- Get currently selected phone strings --->	
	var inpContactList = $("#MCStringList").jqGrid('getGridParam','selarrrow');
	
	<!--- Dont go overboard on displaying too much info --->
	var displayList =  Left(inpContactList, 50);
	
	if(String(inpContactList).length > 50)
		displayList = displayList + " (...)";
		
	if($("#inpGroupId_MCContacts").val() == 0)
	{		
		jAlert("Selections are memebers of the 'All' group by default. Use delete to fully remove from 'All' lists. \n" , "Notice!", function(result) { } );					
		return false;
	}
		
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to remove from group these numbers:\n(" + displayList + ") \n\nAre you absolutely sure?", "About to ungroup Contact String(s) from the phone list.", function(result) { if(!result){$("#loadingPhoneList").hide(); return;}else{	
		
		  $("#loadingPhoneList").show();		
				
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=RemoveGroupContacts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  type: 'POST',
		  data:  { inpContactList: String(inpContactList), INPGROUPID: $("#inpGroupId_MCContacts").val()},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{		<!---Use grid reload instead - will maintain grouping info
													
								   var selectedRowsJq = new Array();
								   var selectedRows = new Array();
								   selectedRowsJq=$('#MCStringList').getGridParam('selarrrow');
								   for(var i = 0; i < selectedRowsJq.length; i++)selectedRows[i]=selectedRowsJq[i];
								   for(var x = 0; x < selectedRows.length; x++)
								   {  
									   	alert("Deleting Row" + selectedRows[x] );
								       $('#MCStringList').delRowData(selectedRows[x]);
								   }    --->
													
								gridReload_MCContacts();							     
							}
																										
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridReload_MCContacts();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;
}


function GroupContacts()
{	
	<!--- Get currently selected phone strings --->	
	var inpContactList = $("#MCStringList").jqGrid('getGridParam','selarrrow');
	
	<!--- Dont go overboard on displaying too much info --->
	var displayList =  Left(inpContactList, 50);
	
	if(String(inpContactList).length > 50)
		displayList = displayList + " (...)";
		
	return false;
}


<!--- Global so popup can refernece it to close it--->
var CreatePrintListDialog = 0;
	
function PrintListDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(CreatePrintListDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreatePrintListDialog.remove();
		CreatePrintListDialog = 0;
		
	}
					
	CreatePrintListDialog = $('<div></div>').append($loading.clone());
	
	var ParamStr = GetParamString_MCContacts();		
	
	CreatePrintListDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_ExportPrint' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Print Contact Strings',
			width: 500,
			height: 400
		});

	CreatePrintListDialog.dialog('open');

	return false;		
}

<!--- Global so popup can refernece it to close it--->
var CreateUploadPhoneToListDialog = 0;
var UploadStatusalertTimerId = 0;
		
function UploadPhoneToListDialog()
{					
	window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/uploadMCsToList?inpSocialmediaFlag=<cfoutput>#inpShowSocialmediaOptions#</cfoutput>&inpGroupOptions=1';
	return false;		
}
	
<!--- Global so popup can refernece it to close it--->
var $CreateGroupContactsDialog = 0;

function GroupContactsDialog()
{
	if(jQuery("#MCStringList").getGridParam('selrow')==null || jQuery("#MCStringList").getGridParam('selrow')=='null'){
		$.jAlert("No contact was chosen !","error");
		return;
	}
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
												
	<!--- Erase any existing dialog data --->
	if($CreateGroupContactsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		$CreateGroupContactsDialog.remove();
		$CreateGroupContactsDialog = 0;
		
	}
					
	$CreateGroupContactsDialog = $('<div></div>').append($loading.clone());
	
	$CreateGroupContactsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_GroupContacts')
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Add Contact Strings to a Group',
			width: 800,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff' 
		});

	$CreateGroupContactsDialog.dialog('open');

	return false;		
}


<!--- Global so popup can refernece it to close it--->
var CreateRenameGroupDialog = 0;

function RenameGroupDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
												
	<!--- Erase any existing dialog data --->
	if(CreateRenameGroupDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateRenameGroupDialog.remove();
		CreateRenameGroupDialog = 0;
		
	}
					
	CreateRenameGroupDialog = $('<div></div>').append($loading.clone());
	
	CreateRenameGroupDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_RenameGroup')
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Rename A Group',
			width: 800,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff' 
		});

	CreateRenameGroupDialog.dialog('open');

	return false;		
}


<!--- Global so popup can refernece it to close it--->
var $CreateDeleteGroupDialog = 0;

function DeleteGroupDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if($CreateDeleteGroupDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		$CreateDeleteGroupDialog.remove();
		$CreateDeleteGroupDialog = 0;
		
	}
					
	$CreateDeleteGroupDialog = $('<div></div>').append($loading.clone());
	
	$CreateDeleteGroupDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_DeleteGroup')
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Delete a User Defined Group',
			width: 800,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff' 
		});

	$CreateDeleteGroupDialog.dialog('open');

	return false;		
}

<!--- Global so popup can refernece it to close it--->
var CreateaddgroupDialog = 0;

function addgroupDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(CreateaddgroupDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateaddgroupDialog.remove();
		CreateaddgroupDialog = 0;
		
	}
					
	CreateaddgroupDialog = $('<div></div>').append($loading.clone());
	
	CreateaddgroupDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_addgroup')
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Add A Group',
			width: 800,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff' 
		});

	CreateaddgroupDialog.dialog('open');

	return false;		
}


<!--- Global so popup can refernece it to close it--->
var CreateAddContactStringToListDialog = 0;
	
function AddContactStringToListDialog(inpBRDialString, inpNotes)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(inpBRDialString) != "undefined" && inpBRDialString != "")
	{					
		ParamStr = '?inpBRDialString=' + encodeURIComponent(inpBRDialString);
		
		if(typeof(inpNotes) != "undefined" && inpNotes != "")		
			ParamStr = ParamStr + '&inpNotes=' + encodeURIComponent(inpNotes);
		
	}
	window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/addContactStringToList' + ParamStr;

	return false;		
}
	


<!--- Global so popup can refernece it to close it--->
var CreateUpdateTimeZoneDialog = 0;
	
function UpdateTimeZoneDialog(inpTZContactString, inpTimeZone, INPCONTACTTYPEID)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(inpTZContactString) != "undefined" && inpTZContactString != "")					
		ParamStr = '?inpTZContactString=' + encodeURIComponent(inpTZContactString) + '&inpTimeZone=' + encodeURIComponent(inpTimeZone) + '&INPCONTACTTYPEID=' + encodeURIComponent(INPCONTACTTYPEID);
	else
	{
		jAlert("Unable to get Contact String and Current Time Zone iormaation - Can not perform any updates against this link." , "Notice!", function(result) { } );					
		return false;		
	}
						
	<!--- Erase any existing dialog data --->
	if(CreateUpdateTimeZoneDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateUpdateTimeZoneDialog.remove();
		CreateUpdateTimeZoneDialog = 0;
		
	}
					
	CreateUpdateTimeZoneDialog = $('<div></div>').append($loading.clone());
	
	CreateUpdateTimeZoneDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_UpdateTZ' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Update Time Zone',
			width: 500,
			height: 'auto',
			position: 'top' 
		});

	CreateUpdateTimeZoneDialog.dialog('open');

	return false;		
}
	


<!--- Global so popup can refernece it to close it--->
var AddFilteredToQueueDialog = 0;
	
function AddFilteredToQueue()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = "";
								
	<!--- Erase any existing dialog data --->
	if(AddFilteredToQueueDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		AddFilteredToQueueDialog.remove();
		AddFilteredToQueueDialog = 0;
		
	}
					
	AddFilteredToQueueDialog = $('<div></div>').append($loading.clone());
	
	AddFilteredToQueueDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/distribution/dsp_AddBatchToQueue' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Add the Filtered List to the Queue',
			width: 800,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff' 
		});

	AddFilteredToQueueDialog.dialog('open');

	return false;		
}
	


<!--- Global so popup can refernece it to close it--->
var AddFilteredToQueueIIDialog = 0;
	
function AddFilteredToQueueII()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = "";
								
	<!--- Erase any existing dialog data --->
	if(AddFilteredToQueueIIDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		AddFilteredToQueueIIDialog.remove();
		AddFilteredToQueueIIDialog = 0;
		
	}
					
	AddFilteredToQueueIIDialog = $('<div></div>').append($loading.clone());
	
	AddFilteredToQueueIIDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/distribution/dsp_AddBatchToQueueConsole' + ParamStr)
		.dialog({
			close: function() { AddFilteredToQueueIIDialog.dialog('destroy'); AddFilteredToQueueIIDialog.remove(); AddFilteredToQueueIIDialog = 0;}, <!--- be sure to stop timed ajax calls--->
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Add the Filtered List to the Queue',
			width: 1005,
			height: 'auto',
			position: 'top', 
			closeOnEscape: false, 
			dialogClass: 'noTitleStuff' 
		});

	AddFilteredToQueueIIDialog.dialog('open');

	return false;		
}
	



function doSearch_MCContacts(ev, sourceObj){
	
	if(!flAuto_MCContacts)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd_MCContacts)
		clearTimeout(timeoutHnd_MCContacts)
	timeoutHnd_MCContacts = setTimeout(gridReload_MCContacts,500)
	
	lastObj_MCContacts = sourceObj;
	
}

function GetParamString_MCContacts()
{	
	var ParamStr = '';
	var MCCONTACT_MASK = $("#MCStringList #search_dialstring").val();
	var notes_mask = $("#MCStringList #search_notes").val();
	var CurrGroup = $("#inpGroupId_MCContacts").val(); 
	var CurrSource = $("#inpSourceMask_MCContacts").val(); 
	var CurrSORD =  $("#MCStringList").jqGrid('getGridParam','sord');
	var CurrSIDX =  $("#MCStringList").jqGrid('getGridParam','sidx');
	var CurrPage = $('#MCStringList').getGridParam('page'); 
	var CurrType = $("#inpTypeId_MCContacts").val(); 
	
	if(CurrGroup == '' ||  typeof(CurrGroup) == "undefined")
		CurrGroup = 0;
		
	if(CurrSource == '' ||  typeof(CurrSource) == "undefined")
		CurrSource = 0;

	ParamStr += '&inpSourceMask=' + encodeURIComponent(CurrSource);
							
	if(MCCONTACT_MASK) ParamStr += '&MCCONTACT_MASK=' + encodeURIComponent(MCCONTACT_MASK);	
	
	if(notes_mask) ParamStr += '&notes_mask=' + encodeURIComponent(notes_mask);			

	if(CurrGroup != '' && CurrGroup != null) ParamStr += '&inpGroupId=' + CurrGroup;
			
	if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
		
	if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
	
	if(CurrType != '' && CurrType != null && CurrType != 0) ParamStr += '&type_mask=' + CurrType;	
	
	return ParamStr;
}

function gridReload_MCContacts(){
	
	var ParamStr = GetParamString_MCContacts();

	$("#MCStringList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GetMCListData&returnformat=json&queryformat=column&inpSocialmediaFlag=<cfoutput>#inpShowSocialmediaOptions#</cfoutput>&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:1}).trigger("reloadGrid");

    LoadGroupCounts();

}

function enableAutosubmit(state){
	flAuto_MCContacts = state;
	gridReload_MCContacts();
	$("#submitButton").attr("disabled",state);
}



function LoadGroupCounts()
{
	$("#loadingPhoneList").show();				
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists.cfc?method=GetSourceCount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { inpSourceMask : LastSource_MCContacts },					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
								if(typeof(d.DATA.GROUPCOUNT[0]) != "undefined")								
									$("#tab-panel-MCContacts #GroupSelectLableBabbleMCContacts").html('Current Group ' + '(' + d.DATA.GROUPCOUNT[0] + ')');
								else
									$("#tab-panel-MCContacts #GroupSelectLableBabbleMCContacts").html('Current Group');
							}
													
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
					
		});


	return false;
}


	
	
<!--- Global so popup can refernece it to close it--->
	var CreateDefineImportDialog = 0;
			
	function DefineImportDialog(inpETLIDSel, inpDoCallBack)
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
							
		<!--- Erase any existing dialog data --->
		if(CreateDefineImportDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			CreateDefineImportDialog.remove();
			CreateDefineImportDialog = 0;
			
		}
						
		CreateDefineImportDialog = $('<div style="overflow-y: auto;height: 725px;"></div>').append($loading.clone());
		
		<!---CreateDefineImportDialog = $('<div></div>').append($loading.clone());--->
		
		if(inpDoCallBack)
		{
		
			CreateDefineImportDialog
				.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_DefineFileFormat?inpETLDefID=' + inpETLIDSel )
				.dialog({
					show: {effect: "fade", duration: 500},
					hide: {effect: "fold", duration: 500},
					modal : true,
					close: function() {  $("#UploadPhoneListDiv #RightStage #inpETLIDUploadSelDiv" ).html("<select id='inpETLIDUploadSel' name='inpETLIDUploadSel' class='ui-corner-all'></select>");    ReloadETLData('#UploadPhoneListDiv #RightStage #inpETLIDUploadSelDiv', 0);      }, <!--- be sure to stop timed ajax calls--->
					title: 'Define a file format for uploading data into the multi-list',
					width: 1200,
					height: 716,
					position: 'top',
					dialogClass: 'noTitleStuff' 
				});
	
		}
		else
		{
			
			CreateDefineImportDialog
				.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_DefineFileFormat?inpETLDefID=' + inpETLIDSel )
				.dialog({
					show: {effect: "fade", duration: 500},
					hide: {effect: "fold", duration: 500},
					modal : true,
					close: function() {                      }, <!--- be sure to stop timed ajax calls--->
					title: 'Define a file format for uploading data into the multi-list',
					width: 1200,
					height: 716,
					position: 'top',
					dialogClass: 'noTitleStuff' 
				});

			
		}
	
	
		CreateDefineImportDialog.dialog('open');
	
		return false;		
	}
	


<!--- Global so popup can refernece it to close it--->
var CreateEditETLDialog = 0;

function EditETLDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
												
	<!--- Erase any existing dialog data --->
	if(CreateEditETLDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateEditETLDialog.remove();
		CreateEditETLDialog = 0;
		
	}
					
	CreateEditETLDialog = $('<div></div>').append($loading.clone());
	
	
		CreateEditETLDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/lists/MultiContacts/dsp_EditETLDefinitions')
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
				close: function() {            }, <!--- be sure to stop timed ajax calls--->
				title: 'Choose Data Import Definition to edit',
				position: 'top',
				width: 700,
				height: 'auto',
				dialogClass: 'noTitleStuff' 
			});

	
	
	
	CreateEditETLDialog.dialog('open');

	return false;		
}

	
</script>
     
	    
<cfoutput>


    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)---> 
    <div id="tab-panel-MCContacts" class="ui-tabs-hide" style="position:relative; overflow: hidden;">
     
         <div style="table-layout:fixed; min-height:475px; height:475px;">
         
                <div id="pagerDiv"></div>
                               
                <div style="text-align:left;">
                <table id="MCStringList"></table>
                </div>
                    
          
        
        </div>        
        
        <br />
        <br />
                   
	</div>	          
    
   <div style="">                           

 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockMCCotactList" style="text-align:left;">
            <div class="dock-container_BabbleMCContacts">
                <a class="BBMainMenu" href="##" onclick="AddContactStringToListDialog(); return false;"><span>Add #ListPopupTitleText#</span></a> 
                <a class="BBMainMenu" href="##" onclick="UploadPhoneToListDialog(); return false;"><span>Upload a Phone List</span></a> 
                <a class="BBMainMenu" href="##" onclick="ToggleTZLinks();"><span id="Show_TZ">Show Time Zone Links</span></a> 
                <a class="BBMainMenu" href="##" onclick="return false;"><span></span></a>
                <a class="BBMainMenu" href="##" onclick="GroupContactsDialog(); return false;"><span>Group Selected</span></a>
                <a class="BBMainMenu" href="##" onclick="UnGroupContacts(); return false;"><span>Ungroup Selected</span></a> 
                <a class="BBMainMenu" href="##" onclick="addgroupDialog();"><span>Add a User Defined Group</span></a> 
                <a class="BBMainMenu" href="##" onclick="RenameGroupDialog(); return false;"><span>Rename a User Defined Group</span></a> 
                <a class="BBMainMenu" href="##" onclick="DeleteGroupDialog();"><span>Delete a User Defined Group</span></a> 
           </div>
           
        </div>
        
    </div>
        
	<div id="LaunchContainer">
  <!---  	<a href="##" class="MedallionImageLink" onclick="AddFilteredToQueue();"><img class = "LaunchIconWebII" src="../../public/images/dock/blank.gif" alt="Launch using current filters" title="Launch campaign using current filters" border="0" /></a> 
  --->     
  			<a href="##" class="MedallionImageLink" onclick="AddFilteredToQueueII();"><img class = "LaunchIconWebII" src="../../public/images/dock/blank.gif" alt="Launch using current filters" title="Launch campaign using current filters II" border="0" /></a> 
	</div>
                 
</cfoutput>    
     

