

<cfparam name="TransferTargets" default="">
<cfparam name="CURRLINKTARGET" default="0">
<cfparam name="AddTSC" default="1">

<!--- Preset to found transfer data--->
<cfset AddTSC = 1>
<cfset TrimToNull = 0>

<cfswitch expression="#CURRLINKTARGET#">
    
    <cfcase value="101"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField1_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="102"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField2_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="103"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField3_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="104"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField4_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="105"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField5_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="106"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField6_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="107"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField7_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="108"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField8_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="109"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField9_int">
        <cfset TrimToNull = 1>
    </cfcase>
    
    <cfcase value="110"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField10_int">
        <cfset TrimToNull = 1>
    </cfcase>
        
    <cfcase value="201"> 
      	<cfset TransferTargets = TransferTargets & "," & " CustomField1_vch">
    </cfcase>
    
    <cfcase value="202"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField2_vch">
    </cfcase>
    
    <cfcase value="203"> 
        <cfset TransferTargets = TransferTargets & "," & " CustomField3_vch">
    </cfcase>
    
    <cfcase value="204"> 
       <cfset TransferTargets = TransferTargets & "," & " CustomField4_vch">
    </cfcase>
    
    <cfcase value="205"> 
       <cfset TransferTargets = TransferTargets & "," & " CustomField5_vch">
    </cfcase>
    
    <cfcase value="206"> 
      <cfset TransferTargets = TransferTargets & "," & " CustomField6_vch">
    </cfcase>
    
    <cfcase value="207"> 
      <cfset TransferTargets = TransferTargets & "," & " CustomField7_vch">
    </cfcase>
    
    <cfcase value="208"> 
      <cfset TransferTargets = TransferTargets & "," & " CustomField8_vch">
    </cfcase>
    
    <cfcase value="209"> 
     	<cfset TransferTargets = TransferTargets & "," & " CustomField9_vch">
    </cfcase>
    
    <cfcase value="210"> 
      <cfset TransferTargets = TransferTargets & "," & " CustomField10_vch">
    </cfcase>
    
    
    <cfcase value="301"> 
      <cfset TransferTargets = TransferTargets & "," & " LocationKey1_vch">
    </cfcase>
    
    <cfcase value="302"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey2_vch">
    </cfcase>
    
    <cfcase value="303"> 
        <cfset TransferTargets = TransferTargets & "," & " LocationKey3_vch">
    </cfcase>
    
    <cfcase value="304"> 
        <cfset TransferTargets = TransferTargets & "," & " LocationKey4_vch">
    </cfcase>
    
    <cfcase value="305"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey5_vch">
    </cfcase>
    
    <cfcase value="306"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey6_vch">
    </cfcase>
    
    <cfcase value="307"> 
        <cfset TransferTargets = TransferTargets & "," & " LocationKey7_vch">
    </cfcase>
    
    <cfcase value="308"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey8_vch">
    </cfcase>
    
    <cfcase value="309"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey9_vch">
    </cfcase>
    
    <cfcase value="310"> 
       <cfset TransferTargets = TransferTargets & "," & " LocationKey10_vch">
    </cfcase>
    
    
  <!---  Hold these out for one at a time transfers
    <cfcase value="1"> 
        <cfif TransferTargets EQ "">
            <cfset TransferTargets = TransferTargets & "ContactString_vch">
        <cfelse>
            <cfset TransferTargets = TransferTargets & "," & " ContactString_vch">
        </cfif>
    </cfcase>
    
    <cfcase value="2"> 
        <cfif TransferTargets EQ "">
            <cfset TransferTargets = TransferTargets & "ContactString_vch">
        <cfelse>
            <cfset TransferTargets = TransferTargets & "," & " ContactString_vch">
        </cfif>
    </cfcase>
    
    <cfcase value="3"> 
        <cfif TransferTargets EQ "">
            <cfset TransferTargets = TransferTargets & "ContactString_vch">
        <cfelse>
            <cfset TransferTargets = TransferTargets & "," & " ContactString_vch">
        </cfif>
    </cfcase>
	--->
    
    <cfdefaultcase>
        <!--- Do nothing ?--->
        <cfset AddTSC = 0>        
    </cfdefaultcase>

	

</cfswitch>
        

<cfif AddTSC EQ 1>

	<cfif TrimToNull EQ 1>
        <cfset TransferSourceColumns = TransferSourceColumns & ", " & "CASE TRIM(#CurrColumnName#) WHEN '' THEN NULL ELSE TRIM(#CurrColumnName#) END">
    <cfelse>
         <cfset TransferSourceColumns = TransferSourceColumns & ", " & CurrColumnName>
    </cfif>
        
</cfif>    
                                
                                        
        
        
        
        
        
        