<!--- Transform to dedupe based on this field --->

<cfparam name="inpDBSource" default="#Session.DBSourceEBM#">
<cfparam name="inpUploadId" default="0">
<cfparam name="inpUserId" default="0">
<cfparam name="CurrColumnName" default="0">


<!--- Dedupe logic if you have a primary key assigned using self joined tables
	This is the proper format, allowing for easy debugging of errors…
	DELETE
	FROM table1
	USING table1, table1 AS vtable
	WHERE vtable.id > table1.id
	AND table1.field_name = vtable.field_name
	
	If you're nervous about deleteing everything, you can make sure it's working first:
	SELECT *
	FROM table1, table1 AS vtable
	WHERE vtable.id > table1.id
	AND vtable.field_name = table1.field_name

	Or just update a clean flag
	UPDATE
	table1, table1 AS vtable
	SET table1.CleanFlag_int = 1
	WHERE vtable.id > table1.id
	AND vtable.field_name = table1.field_name					
--->


<!--- Dedupe only agains phone,email,SMS ???? --->


<!--- Dedupe --->
<!---  Flag all but last entry as a duplicate  --->		
<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
	UPDATE
		simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#, simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# AS vtable
	SET 
		simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#.cleanflag_int = 1
	WHERE 
		vtable.UID > simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#.uid
	AND 
		vtable.#CurrColumnName# = simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#.#currcolumnname#				
</cfquery>
