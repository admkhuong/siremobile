<!--- Scrub invalid formated numbers that made it this far WARNING - mySQL does not support \d for 0-9 in reg exp--->    
 <cfquery name="CharacterScrubbingInvalidCharacters" datasource="#inpDBSource#">
     UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET CleanFlag_int = 3 WHERE NOT (#CurrColumnName# REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$')
 </cfquery>
