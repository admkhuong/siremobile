<!--- Transform to dialable phone number with IVR nav options allowed --->

<cfparam name="inpDBSource" default="#Session.DBSourceEBM#">
<cfparam name="inpUploadId" default="0">
<cfparam name="inpUserId" default="0">
<cfparam name="CurrColumnName" default="0">
<cfparam name="inpAllowIVR" default="0">

<!---North American Phone Number Transformation    --->                             
<!--- Regular expression scrub - clean up non numeric numbers  [^\d^\*^P^X^##] --->    
<!--- Run these scrubs first then "Clean Flag" what is left--->                                                 
<cfquery name="CharacterScrubbing1" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, '-', '') WHERE #CurrColumnName# REGEXP '[[.hyphen.]]' 				
</cfquery>

<cfquery name="CharacterScrubbing2" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, '.', '') WHERE #CurrColumnName# REGEXP '[[.period.]]' 					
</cfquery>

<cfquery name="CharacterScrubbing3" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, ' ', '') WHERE #CurrColumnName# REGEXP '[[.space.]]' 					
</cfquery>

<cfquery name="CharacterScrubbing4" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, ',', '') WHERE #CurrColumnName# REGEXP '[[.comma.]]' 					
</cfquery>

<cfquery name="CharacterScrubbing4" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, '(', '') WHERE #CurrColumnName# REGEXP '[[.left-parenthesis.]]' 					
</cfquery>

<cfquery name="CharacterScrubbing5" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, ')', '') WHERE #CurrColumnName# REGEXP '[[.right-parenthesis.]]' 					
</cfquery>

<cfquery name="CharacterScrubbing6" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, '"', '') WHERE #CurrColumnName# REGEXP '[[.quotation-mark.]]'                         					
</cfquery>
		  
<cfquery name="CharacterScrubbing5" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET #CurrColumnName# = Replace(#CurrColumnName#, '\'', '') WHERE #CurrColumnName# REGEXP '[[.apostrophe.]]' 					
</cfquery>    
                    
                  
<!---  If 10 digit scrub less than ten digit numbers  --->		
<cfquery name="NorthAmericaScrubbing" datasource="#inpDBSource#">
	UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET CleanFlag_int = 2 WHERE LENGTH(#CurrColumnName#) < 10				
</cfquery>

<cfif inpAllowIVR GT 0>
	<!--- Scrub invalid formated numbers that made it this far WARNING - mySQL does not support \d for 0-9 in reg exp--->    
    <!--- IVR option allows * # P and X--->
    <cfquery name="CharacterScrubbingInvalidCharacters" datasource="#inpDBSource#">
        UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET CleanFlag_int = 3 WHERE NOT(#CurrColumnName# REGEXP '^[0-9\*PX##]*$') 					
    </cfquery>
<cfelse>
	<!--- Scrub invalid formated numbers that made it this far WARNING - mySQL does not support \d for 0-9 in reg exp--->    
    <cfquery name="CharacterScrubbingInvalidCharacters" datasource="#inpDBSource#">
        UPDATE simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid# SET CleanFlag_int = 3 WHERE NOT(#CurrColumnName# REGEXP '^[0-9]*$')
    </cfquery>
</cfif>           
                   

                  
<!--- Get time zones, Localities, Cellular data --->
<cfquery name="GetTZInfo" datasource="#inpDBSource#">
    UPDATE MelissaData.FONE AS fx JOIN
     MelissaData.CNTY AS cx ON
     (fx.FIPS = cx.FIPS) INNER JOIN
     simplexuploadstage.simplephonelist_stage_#inpuploadid#_#inpuserid#  AS ded ON (LEFT(ded.#CurrColumnName#, 3) = fx.NPA AND MID(ded.#CurrColumnName#, 4, 3) = fx.NXX)
    SET 
      ded.TimeZone_int = (CASE cx.T_Z
      WHEN 14 THEN 16 
      WHEN 10 THEN 35 
      WHEN 9 THEN 33 
      WHEN 8 THEN 31 
      WHEN 7 THEN 30 
      WHEN 6 THEN 29 
      WHEN 5 THEN 28 
      WHEN 4 THEN 27  
     END),
     ded.State_vch = 
     (CASE 
      WHEN fx.STATE IS NOT NULL THEN fx.STATE
      ELSE '' 
      END),
      ded.City_vch = 
      (CASE 
      WHEN fx.CITY IS NOT NULL THEN fx.CITY
      ELSE '' 
      END),
      ded.CellFlag_int =  
      (CASE 
      WHEN fx.Cell IS NOT NULL THEN fx.Cell
      ELSE 0 
      END),
     ded.Country_vch = 
     (CASE 
      WHEN fx.CTRY IS NOT NULL THEN fx.CTRY
      ELSE '' 
      END)                                                  
     WHERE
        cx.T_Z IS NOT NULL                            
     AND 
        ded.CleanFlag_int = 0		
        
        <!--- Limit to US or Canada?--->
        <!---  AND fx.CTRY IN ('U', 'u')   Country_vch    --->
</cfquery>                  