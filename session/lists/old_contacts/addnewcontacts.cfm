
<cfif isDefined("url.lid")>
	<cfif isNumeric(url.lid)>
    	<cfset url.lid = url.lid>
    <cfelseif url.lid eq 'fc'>
    	<cfset url.lid = session.currentList>
    </cfif>
<div style="display:inline-block; margin-left:50px;">

    <div style="height:30px; width:625px; margin-top:15px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:#2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> ADD SINGLE CONTACT:<cfoutput>#session.currentList#</cfoutput>
        </div>
    </div>
    <div id="singlecontact" style="width:595px; border-left:1px inset #cccccc; border-right:1px inset #cccccc; border-bottom:1px inset #cccccc; display:inline-block; padding:15px; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px; ">
        <form id="SingleForm" name="SingleForm">
        <cfoutput>
        	<input type="hidden" name="ListID" id="ListID" value="#url.lid#" />
        </cfoutput>
        <div style="float:left;">
            <label for="FirstName" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">First Name:
            </label>
            <input type="text" class="mainLogin" name="FirstName" id="FirstName">
        </div>
        <div style="float:right; margin-left:15px;">
            <label for="LastName" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Last Name:
            </label>
            <input type="text" class="mainLogin" name="LastName" id="LastName">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="City" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> City:
            </label>
            <input type="text" class="mainLogin" name="City" id="City">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="State" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> State:
            </label>
            <input type="text" class="mainLogin" name="State" id="State">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="Postal" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Postal Code:
            </label>
            <input type="text" class="mainLogin" name="Postal" id="Postal">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="MobileNumber" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Mobile Number:
            </label>
            <input type="text" class="mainLogin" name="MobileNumber" id="MobileNumber">
        </div>
       <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="LandNumber" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Land Line:
            </label>
            <input type="text" class="mainLogin" name="LandNumber" id="LandNumber">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="Email" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Email:
            </label>
            <input type="text" class="mainLogin" name="Email" id="Email">
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="float:left;">
            <label for="userDefined" style="text-align:left; color:#2b83a7; font-weight:bold; width:150px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">User Defined Data:
            </label>
            <input type="text" class="mainLogin" name="userDefined" id="userDefined" style="width:250px;">
        </div>
        <div style="float:right; margin-left:15px; font-size:11px;">
            <input type="submit" name="addscontact" value="add contact" class="mainSubmit" style="padding:5px 10px 5px 10px;" onclick="return AddSingle();">
        </div>
       <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
		</form>
        <div id="uploadingloadsc" style="width:625px; text-align:center; display:none; padding:10px;">
            Please wait while we process your contact.<br />
            <img src="images/loading.gif" style="margin-top:20px;" />
        </div>
        <div id="uploadingdonesc" style="width:625px; text-align:center; display:none; padding:10px; font-weight:bold;">
            
        </div>

        
    </div>
    
    <div style="height:30px; width:625px; margin-top:45px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:#2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> UPLOAD CONTACT LIST:
        </div>
    </div>
    <div id="uploadcontact" style="width:595px; border-left:1px inset #cccccc; border-right:1px inset #cccccc; border-bottom:1px inset #cccccc; display:inline-block; padding:15px; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px; ">
        
        <div style="float:left;">
            <label for="FirstName" style="text-align:left; color:#2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">CSV/TEXT FILE:
            </label>
            <div style="float:right; margin-left:15px;">
                    <cffileupload
                    extensionfilter="csv,txt" 
                    name="portfoliofiles1" 
                    maxfileselect="1" 
                    title="Upload Contact List (MUST BE COMMA DELIMITED!)" 
                    progressbar="true"
                    onerror="fileUploadError"
                    onuploadcomplete="fileUploadComplete"
                    stoponerror="true"
                    url = "fileUpload?phoneId=#url.lid#"
                    height="200"
                    width="450"
                    wmode="transparent"
                    bgcolor="##FFFFFF"
                    >
            </div>
        </div>
        <div style="clear:both; border-bottom:1px dashed #cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        
        <div id="ProcessListLoad" style="float:left; width:100%; text-align:center; display:none; padding:10px; font-size:14px; font-weight:bold;">
            Please wait while your list is being processed.<br />
            <img src="images/loading.gif" style="margin-top:20px;" />
        </div>
        
        <div id="uploadchoose" style="float:left; margin-top:15px; display:none;" class="norm">
            <cfdiv id="chooselistcolumn" bind="url:cfdiv/cfdiv_listcolumnchoose" bindonload="no">
        </div>
        
        <div id="uploadlistresult" style="float:left; margin-top:15px; display:none;" class="norm">
            <cfdiv id="uplresults" bind="url:cfdiv/cfdiv_listuploadresults" bindonload="no">
        </div>
        
        
    </div>
</div>
<!---YES <cfoutput>#url.lid#</cfoutput>--->
<cfelse>
	<!---NO--->
</cfif>