<cfcomponent
	output="false"
	hint="I listen for CSV parser events and compile a array of arrays.">
 
 
	<cffunction
		name="init"
		access="public"
		returntype="any"
		output="false"
		hint="I initialize this component.">
 		
        <cftry>
        
		<!--- Set up the data. --->
		<cfset variables.csvData = [] />
 		
        	<cfcatch TYPE="any">
					
                <cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="handler init" type="html">
                    <cfdump var="#cfcatch#">
                </cfmail>
        	
            </cfcatch>
        	
        </cftry>
        
		<!--- Return this object reference. --->
		<cfreturn this />
	</cffunction>
 
 
	<cffunction
		name="getData"
		access="public"
		returntype="array"
		output="false"
		hint="I return the current data collection.">
 
		<cfreturn duplicate( variables.csvData ) />
	</cffunction>
 
 
	<cffunction
		name="handleEvent"
		access="public"
		returntype="any"
		output="false"
		hint="I listen for and then response to events published by a CSV parser.">
 
		<!--- Define arguments. --->
		<cfargument
			name="eventType"
			type="string"
			required="true"
			hint="I am the type of event being raised."
			/>
 
		<cfargument
			name="eventData"
			type="string"
			required="false"
			default=""
			hint="I am the (optional) data being published along with the CSV parsing event."
			/>
 		
        <cftry>
        
		<!---
		<cffile
			action="append"
			file="#expandPath( './log.txt' )#"
			output="#arguments.eventType# [#arguments.eventData#]"
			addnewline="true"
			/>
		--->
 
		<!--- Check to see what kind of event we have. --->
		<cfif (arguments.eventType eq "startRow")>
 
			<!--- Push a new row onto the data. --->
			<cfset arrayAppend(
				variables.csvData,
				arrayNew( 1 )
				) />
 
		<cfelseif (arguments.eventType eq "endField")>
 
			<!--- Push this field onto the latest row. --->
			<cfset arrayAppend(
				variables.csvData[ arrayLen( variables.csvData ) ],
				arguments.eventData
				) />
 
		</cfif>
        
        
        	<cfcatch TYPE="any">
					
                <cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="handler handleEvent" type="html">
                    <cfdump var="#cfcatch#">
                </cfmail>
        	
            </cfcatch>
        	
        </cftry>
 
		<!--- Return this object reference. --->
		<cfreturn this />
	</cffunction>
 
</cfcomponent>