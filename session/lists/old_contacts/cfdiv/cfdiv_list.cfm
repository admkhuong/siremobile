<cfif Session.UserId neq 0>
	<!--- This sets up the pagnation links --->
    
    <cfif IsDefined("url.max")>
        <cfset max = url.max>
    <cfelse>
        <cfset max = 20>
    </cfif>
    <cfset start = IIF(IsDefined('start'), 'start', DE("1"))>
    <cfif NOT IsNumeric(start)>
        <cfset start = 1>
    </cfif>
    
    <!---<cfinvoke component="#application.cfcpath#.smslist" method="GetGroupData" returnvariable="records" />--->
    
    <cfinvoke component="session.lists.contacts.cfc.newcontactlist" method="GetGroupData" returnvariable="records">
    	<cfinvokeargument name="GroupType" value="1">
    </cfinvoke>
    
    <!---<cfset records.recordcount = 541>--->
    
    
    
    <div style="display:inline-block; position:relative; width:805px;">
    	<div style="float:left; width:660px;">
			<cfif records.recordcount neq 0>
              <cfif (start + (max - 1)) gt records.recordcount>
                  <cfset endc = records.recordcount>
              <cfelse>
                  <cfset endc = start + (max - 1)>
              </cfif>
              <cfoutput>
              <div style="float:left; position:relative; margin-left:10px; margin-top:5px; width:220px; text-align:left;" class="norm">Displaying #start# to #endc# of #records.recordcount# records</div>
              </cfoutput>
              <div style="float:inherit; position:relative; margin-bottom:0px; margin-top:0px; width:410px; text-align:right;">
              <cfif records.recordcount GT max>
                  <cfset pagecount = #Ceiling((records.recordcount / max))#>
                  <cfset currentpage = #Ceiling((start/max))#>
                  
                  <cfif currentpage gt "1">
                          <cfset pstp = (currentpage * max) - ((max * 2) - 1)>
                          <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_smslist?start=#pstp#&max=#max#', 'list')" style="margin-right:5px;">&lt;</a></cfoutput>
                  </cfif> 
                  
                  <cfif currentpage gte 6>
                      <cfset pc = currentpage - 5>
                  <cfelse>
                      <cfset pc = 1>
                  </cfif>
                  
                  <cfif pc + 9 lt pagecount>
                      <cfset curpagect = pc + 9>
                  <cfelse>
                      <cfset curpagect = pagecount>
                  </cfif>
                  <cfif pagecount - pc gte 9>
                      <cfset pc = curpagect - 9>
                  <cfelseif pagecount - pc lt 9 and pagecount gte 10>
                      <cfset pc = pagecount - 9>
                  <cfelse>
                      <cfset pc = 1>
                  </cfif>
                  <cfset stp = 1>
                  <cfloop condition="#pc# LESS THAN OR EQUAL TO curpagect">
                      <cfset sc = (pc * max) - (max - 1)>
                          <cfoutput>
                          <cfif pc is not currentpage>
                              <a class="brsl" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_smslist?start=#sc#&max=#max#', 'list')" style="margin-left:5px;">#NumberFormat(pc,'00')#</a>
                          <cfelse>
                              <span class="norm" style="padding:3px;"><strong>#NumberFormat(pc,'00')#</strong></span>
                              <cfset stp = sc + max>
                          </cfif>
                          </cfoutput>
                      <cfset pc = #IncrementValue(pc)#>
                  </cfloop>
                  <cfif stp LTE records.recordcount>
                      <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_smslist?start=#stp#&max=#max#', 'list')" style="margin-left:5px;">&gt;</a></cfoutput>
                  </cfif>
              <cfelse>
              <cfset max = records.recordcount>
              </cfif>
              </div>
            </cfif>
        </div>
    	<div style="float:right; text-align:center; display:inline-block; margin-right:15px;">
            <a href="javascript:addnewlist();" style="width:100%; height:100%; font-size:12px; font-weight:bold; display:inline-block; cursor:pointer; padding:7px 5px 5px 5px;" class="blubut" title="Create a new distribution list"><img src="images/add-newlist.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;" />Create New List</a>
        </div>
    </div>
    <!--- Current Phone Lists --->
    <div style="position:relative; width:805px; margin-top:5px; display:inline-block;">
    <!---<cfoutput query="getplist">--->
    <!--- Phone List Header --->
    <div style="width:805px; position:relative; display:inline-block;" class="barheaders">
        <div style="float:left; margin:7px 0 0 10px; width:175px;">
            Current Lists:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:left; margin:7px 0 0 0; width:64px; text-align:center;">
            List Size:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:left; margin:7px 0 0 0; width:64px; text-align:center;">
             Mobile:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:left; margin:7px 0 0 0; width:64px; text-align:center;">
            Voice:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:left; margin:7px 0 0 0; width:64px; text-align:center;">
            Email:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:left; margin:7px 0 0 0; width:64px; text-align:center;">
            Fax:
        </div>
        <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
        <div style="float:right; margin:7px 0 0 0; width:290px; text-align:center;">
            Action:
        </div>
    </div>
    
    <div id="recordsDiv">
    	<cfif records.recordcount neq 0>
			<cfoutput query="records" startrow="#start#" maxrows="#max#">
                <div style="background:##f8f8f8; min-height:45px; border-bottom:1px solid ##ccc; position:relative;" class="norm" id="record#GroupID_bi#">
                    <div style="float:left; margin:15px 0 0 10px; width:175px;">
                        #GroupName_vch#
                    </div>
                    <div style="float:left; margin:15px 0 0 0; width:64px; text-align:center;">
                        #numberFormat(tccl,"999,999,999")#
                    </div>
                    <div style="float:left; margin:15px 0 0 0; width:64px; text-align:center;">
                         #numberFormat(mccl,"999,999,999")#
                    </div>
                    <div style="float:left; margin:15px 0 0 0; width:64px; text-align:center;">
                        #numberFormat(lccl,"999,999,999")#
                    </div>
                    <div style="float:left; margin:15px 0 0 0; width:64px; text-align:center;">
                        #numberFormat(eccl,"999,999,999")#
                    </div>
                    <div style="float:left; margin:15px 0 0 0; width:64px; text-align:center;">
                        #numberFormat(fccl,"999,999,999")#
                    </div>
                    <div style="float:right; position:relative; margin:15px 10px 0 0; font-size:10px; font-weight:bold;">
                        <div style="float:left;">
                            <a href="javascript:editContactList(#GroupID_bi#);" class="blu" title="Edit/View this distribution list"><img src="images/segment-list.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;" />View/Edit</a>
                        </div>
                        <div style="float:left; margin-left:10px;">
                            <a href="javascript:addnewcontacts(#GroupID_bi#);" class="blu" title="Add or Import contacts into this distribution list"><img src="images/add-list.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;" />Add/Import</a>
                        </div>
                        <div style="float:left; margin-left:10px;">
                            <a href="javascript:getExport(#GroupID_bi#);" class="blu" title="Export the contacts from this distribution list"><img src="images/export-list.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;" />Export</a>
                        </div>
                        <div style="float:left; margin-left:10px;">
                            <a href="" class="blu" title="Delete the contacts and list"><img src="images/clean-list.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;" />Delete</a>
                        </div>
                    </div>
                </div>
            </cfoutput>
        <cfelse>
        	<div style="width:100%; text-align:center;">
            	No lists have been created!
            </div>
        </cfif>
    </div>
    <!---</cfoutput>--->
    </div>

<cfelse>
	<cfset AjaxOnLoad("logoutuser")>
</cfif>