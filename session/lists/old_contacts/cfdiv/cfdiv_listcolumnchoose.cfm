<!---<cfif session.lipasp eq "in">--->

	<div id="uploadingload" style="width:100%; text-align:center; display:none; padding:10px;">
    	<img src="resources/images/loading.gif" />
    </div>
<!--- Output the result. --->
    <cfoutput>
   
   	<!--- pull first few values from uploaded file to display to customer to assign row names --->
   	<cfif arraylen(session.uploadlist) gt 5>
   		<cfset atlc = 5>
   	<cfelse>
   		<cfset atlc = arraylen(session.uploadlist)>
   	</cfif>     
        
   	<cfloop from="1" to="#atlc#" index="bb">
    
    	<cfloop from="1" to="#ArrayLen(session.uploadlist[bb])#" index="aa">
        	<cfset navd = session.uploadlist[#bb#][#aa#]>
        	<cfif isDefined("field#aa#")>
            	<cfset  #VARIABLES['field' & aa]# =  VARIABLES['field' & aa] & '<br>' & navd>
            <cfelse>
            	<cfset #VARIABLES['field' & aa]# = navd>
            </cfif>
    	</cfloop>
        
    </cfloop>
    
    <cfquery name="getudvars" datasource="#Session.DBSourceEBM#">
        select 
            Distinct(VariableName_vch)
        from 
            simplelists.contactvariable
        where
            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
    </cfquery>
<div style="position:relative; display:inline-block; width:98%; padding:5px; background:##eff6fe; border:1px solid ##ccc;">
    <!--- loop through columns and let user choose what fields they are --->
    <form id="columnlistupload" name="columnlistupload">
    <div style="position:relative; display:inline-block; width:100%;">
    	<div style="padding:5px 0 15px 5px;">
    		<b>Please tell us what the columns in your uploaded list are:</b> (select "Ignore" for columns not to be uploaded)
        </div>	
    <cfloop from="1" to="#session.ColumnCount#" index="cc">

		<div style="float:left; display:inline-block; border:1px solid ##0066ff; padding:5px; margin:5px;">
        	<div style="margin:0 0 5px 0; padding:3px 0 3px 0; text-align:center; font-weight:bold; background:##ccc;">
        		Column #cc#
        	</div>
        	<select id="column#cc#" name="column#cc#" onchange="showudv('#cc#')">
            <optgroup label="Standard Columns">	
                <option value="ignore">Ignore</option>
                <option value="UserDefined">UserDefined</option>
                <option value="Company">Company</option>
                <option value="First Name">First Name</option>
                <option value="Last Name">Last Name</option>
            	<option value="Mobile Number">SMS Number</option>
                <option value="Mobile Alt">Alt SMS Number</option>
                <option value="Land Number">Voice Number</option>
                <option value="Land Alt">Alt Voice Number</option>
                <option value="Email">Email Address</option>
                <option value="Email Alt">Alt Email Address</option>
                <option value="Fax">Fax Number</option>
                <option value="Fax Alt">Alt Fax Number</option>
                <option value="Address">Address</option>
                <option value="Suite">Suite</option>
                <option value="City">City</option>
                <option value="State">State</option>
                <option value="ZipCode">Zipcode</option>
                <option value="Country">Country</option>
                <option value="Identifier">Identifier</option>
            </optgroup>
            <optgroup label="User Defined Columns">
                <cfloop query="getudvars">
                <option value="#VariableName_vch#">#VariableName_vch#</option>
                </cfloop>
            </optgroup>
            </select><br />
            <div id="udvname#cc#" style="margin:5px 0 0 5px; display:none;">
        		<input type="text" name="udname#cc#" id="udname#cc#" placeholder="name user defined" />
        	</div><br />
            <div id="altvnum#cc#" style="margin:5px 0 0 5px; display:none;">
        		Alternate Hierarchy: <select name="altnum#cc#" id="altnum#cc#">
                	<option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
        	</div>
            <div style="margin:5px 0 0 5px;">
        		#VARIABLES['field' & cc]#<br />
        	</div>
        </div>
        
    </cfloop>
    </div>
    <div style="margin:25px 0 0 5px; display:inline-block;">
    	<input type="checkbox" id="Scrub" name="Scrub" style="padding:3px; margin-right:10px;" /> <b>Mobile Number Scrubbing:</b> Check this box only if you have explicit written consent from all mobile phone numbers on your list to receive messages from your company. Without explicit written consent, we will remove all mobile phone numbers from your list.
    </div>
    <div style="margin:25px 0 0 5px; display:inline-block;">
    	<input type="button" name="update" value="Complete Upload" style="padding:3px;" onclick="doubleCheckColumn('columnlistupload')" />
    </div>
    </form>
</div>   
    </cfoutput>
<!---<cfelse>
	<cfset AjaxOnLoad("logoutuser")>
</cfif>
--->