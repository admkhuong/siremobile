<cfif Session.UserId neq 0>
	
	<cfif isDefined("url.lid")>
	<!--- This sets up the pagnation links --->
    
		<cfif IsDefined("url.max")>
            <cfset max = url.max>
        <cfelse>
            <cfset max = 20>
        </cfif>
        <cfset start = IIF(IsDefined('start'), 'start', DE("1"))>
        <cfif NOT IsNumeric(start)>
            <cfset start = 1>
        </cfif>
        
        <!---<cfinvoke component="#application.cfcpath#.smslist" method="GetGroupData" returnvariable="records" />--->
        
        <cfinvoke component="session.lists.contacts.cfc.newcontactlist" method="ListDetail" returnvariable="records">
            <cfinvokeargument name="ListId" value="#url.lid#">
        </cfinvoke>
        
        <!---<cfset records.recordcount = 541>--->
        
        
        
        <div style="display:inline-block; position:relative; width:820px;">
        <cfif records.recordcount neq 0>
          <cfif (start + (max - 1)) gt records.recordcount>
              <cfset endc = records.recordcount>
          <cfelse>
              <cfset endc = start + (max - 1)>
          </cfif>
          <cfoutput>
          <div style="float:left; position:relative; margin-left:10px; margin-top:5px; width:220px; text-align:left;" class="norm">Displaying #start# to #endc# of #records.recordcount# records</div>
          </cfoutput>
          <div style="float:inherit; position:relative; margin-bottom:0px; margin-top:0px; width:810px; text-align:right;">
          <cfif records.recordcount GT max>
              <cfset pagecount = #Ceiling((records.recordcount / max))#>
              <cfset currentpage = #Ceiling((start/max))#>
              
              <cfif currentpage gt "1">
                      <cfset pstp = (currentpage * max) - ((max * 2) - 1)>
                      <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_listcontacts?start=#pstp#&max=#max#&lid=#url.lid#', 'contactdetdiv')" style="margin-right:5px;">&lt;</a></cfoutput>
              </cfif> 
              
              <cfif currentpage gte 6>
                  <cfset pc = currentpage - 5>
              <cfelse>
                  <cfset pc = 1>
              </cfif>
              
              <cfif pc + 9 lt pagecount>
                  <cfset curpagect = pc + 9>
              <cfelse>
                  <cfset curpagect = pagecount>
              </cfif>
              <cfif pagecount - pc gte 9>
                  <cfset pc = curpagect - 9>
              <cfelseif pagecount - pc lt 9 and pagecount gte 10>
                  <cfset pc = pagecount - 9>
              <cfelse>
                  <cfset pc = 1>
              </cfif>
              <cfset stp = 1>
              <cfloop condition="#pc# LESS THAN OR EQUAL TO curpagect">
                  <cfset sc = (pc * max) - (max - 1)>
                      <cfoutput>
                      <cfif pc is not currentpage>
                          <a class="brsl" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_listcontacts?start=#sc#&max=#max#&lid=#url.lid#', 'contactdetdiv')" style="margin-left:5px;">#NumberFormat(pc,'00')#</a>
                      <cfelse>
                          <span class="normm" style="padding:3px;"><b>#NumberFormat(pc,'00')#</b></span>
                          <cfset stp = sc + max>
                      </cfif>
                      </cfoutput>
                  <cfset pc = #IncrementValue(pc)#>
              </cfloop>
              <cfif stp LTE records.recordcount>
                  <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_listcontacts?start=#stp#&max=#max#&lid=#url.lid#', 'contactdetdiv')" style="margin-left:5px;">&gt;</a></cfoutput>
              </cfif>
          <cfelse>
          <cfset max = records.recordcount>
          </cfif>
          </div>
        </cfif>
        </div>
        <!--- Current Phone Lists --->
        <div style="position:relative; width:805px; margin-top:5px; display:inline-block;">
        	<div style="position:relative; margin:5px 0 5px 10px; display:inline-block; font-size:12px;">
            	<cfoutput>
            	<a href="javascript:DeleteContacts();" class="blu" style="margin-right:10px; padding-right:10px; border-right:1px solid ##ccc;">Delete</a>
                <a href="javascript:MoveContacts(#url.lid#);" class="blu" style="margin-right:10px; padding-right:10px; border-right:1px solid ##ccc;">Move To List</a>
                <a href="javascript:CopyContacts(#url.lid#);" class="blu" style="margin-right:10px; padding-right:10px; border-right:1px solid ##ccc;">Copy To List</a>
                <!---<a href="javascript:MessageContacts();" class="blu" style="margin-right:10px;">Message Selected Contacts</a>--->
                </cfoutput>
            </div>
        <!---<cfoutput query="getplist">--->
        <!--- Phone List Header --->
        <div style="width:805px; position:relative; display:inline-block;" class="barheaders">
            <div style="float:left; margin:7px 0 0 0; width:30px; text-align:center;">
                <input type="checkbox" name="all" onclick="toggleChecked(this.checked)" />
            </div>
            <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
            <div style="float:left; margin:7px 0 0 10px; width:100px;">
                First Name:
            </div>
            <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
            <div style="float:left; margin:7px 0 0 0; text-align:center; width:140px;">
                Last Name:
            </div>
            <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
            <div style="float:left; margin:7px 0 0 0; text-align:center; width:290px;">
                Contact:
            </div>
            <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
            <div style="float:left; margin:7px 0 0 0; text-align:center; width:120px;">
                Type:
            </div>
            <div style="float:left; height:23px; width:1px; margin-top:3px; background:#ccc;"></div>
            <div style="float:left; margin:7px 0 0 0; text-align:center; width:100px;">
                Date Added:
            </div>
        </div>
        
        <div id="recordsDiv">
            <cfif records.recordcount neq 0>
            	<form id="ContactList" name="ContactList">
                <cfoutput query="records" startrow="#start#" maxrows="#max#">
                    <div style="background:##f8f8f8; min-height:35px; border-bottom:1px solid ##ccc; position:relative;" class="norm" id="record#ContactAddressId_bi#">
                        <div style="float:left; margin:10px 0 0 10px; width:20px;">
                            <input type="checkbox" name="#ContactAddressId_bi#" class="pist" />
                        </div>
                        <div style="float:left; margin:10px 0 0 5px; width:108px;">
                            <a href="javascript:viewContactDet(#ContactId_bi#);" class="blu" title="View This Contacts Details">#FirstName_vch#</a>
                        </div>
                        <div style="float:left; margin:10px 0 0 5px; width:130px;">
                            <a href="javascript:viewContactDet(#ContactId_bi#);" class="blu" title="View This Contacts Details">#LastName_vch#</a>
                        </div>
                        <div style="float:left; margin:10px 0 0 5px; width:288px; text-align:center;">
                            <a href="javascript:viewContactDet(#ContactId_bi#);" class="blu" title="View This Contacts Details">#ContactString_vch#</a>
                        </div>
                        <div style="float:left; margin:10px 0 0 5px; width:120px; text-align:center;">
                            <cfif ContactType_int eq 1 and AltNum_int eq 0>
                            Voice
                            <cfelseif ContactType_int eq 1 and AltNum_int neq 0>
                            Alternate Voice
                            <cfelseif ContactType_int eq 2 and AltNum_int eq 0>
                            Email
                            <cfelseif ContactType_int eq 2 and AltNum_int neq 0>
                             Alternate Email
                            <cfelseif ContactType_int eq 3 and AltNum_int eq 0>
                            SMS
                            <cfelseif ContactType_int eq 3 and AltNum_int neq 0>
                             Alternate SMS
                            <cfelseif ContactType_int eq 4 and AltNum_int eq 0>
                            Fax
                            <cfelseif ContactType_int eq 4 and AltNum_int neq 0>
                             Alternate Fax
                            </cfif>
                        </div>
                        <div style="float:left; margin:10px 0 0 5px; width:100px; text-align:center;">
                            #DateFormat((Created_dt),'mm/dd/yyyy')#
                        </div>
                    </div>
                </cfoutput>
                </form>
            <cfelse>
                <div style="width:100%; text-align:center;">
                    No Contacts In This List!
                </div>
            </cfif>
        </div>
        <!---</cfoutput>--->
        </div>
	</cfif>
<cfelse>
	<cfset AjaxOnLoad("logoutuser")>
</cfif>