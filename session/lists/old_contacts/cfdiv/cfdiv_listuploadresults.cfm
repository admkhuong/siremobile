<cfif IsDefined("url.DCS")>
<div style="width:100%; display:inline-block; position:relative;">
	<div style="margin-left:300px; font-weight:bold; font-size:16px; padding:10px; width:400px;">
    	Contact Upload Results
    </div>
    <div style="margin-left:300px; padding:10px; width:400px;">
    <cfset databasename = url.DCS>
    
    <cfquery name="pullUpload" datasource="#Session.DBSourceEBM#">
    	select
        	*
        From
        	simplelists.contactuploadresults
        where
        	ContactResultsId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.DCS#">
    </cfquery>
    
    <!---
	
	Flag Results
	0 - Good Contact String
	1 - Mobile or Voice number not 10 numbers in length
	2 - Duplicate Contact String In ContactList table for user
	4 - Non Valid Email Address
	5 - Contact String Contained 0 characters in length
	
	DupFlag Results
	0 - Good non duplicate in upload list
	1 - Bad - Contact string is listed multiple times in uploaded list
	
	CellFlag Results
	0 - Non Mobile Number
	1 - Mobile Number
	
	--->
    
    <cfoutput query="pullUpload">
    
		<!--- check For Voice Number Uploads --->
        <cfif ListLen(VoiceColumns_vch,",") gt 0>
            
            <cfloop list="#VoiceColumns_vch#" index="va" delimiters=",">
            	
                
                <cfset ColNum = replaceNoCase(va,"C","")>
                <cfset L1 = "LFlag" & va>
				<cfset L2 = "LDupFlag" & va>
                <cfset L6 = "LCellFlag" & va>
                
                <cfmail from="#Application.SiteAdmin#" to="#Application.SiteAdmin#" type="html" subject="Voice Result">
					<cfoutput>
                    	L1 - #L1#<br />
						L2 - #L2#
					</cfoutput>
                </cfmail>
                
            	<cfquery name="GetVUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#L1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1# = 0) as GoodUploadCount,
                        (select 
                            Count(#L1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1# = 1) as FailedUploadCount,
                        (select 
                            Count(#L1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1# = 5) as EmptyUploadCount,
                        (select 
                            Count(#L1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1# = 2) as DupUploadCount,
                        (select 
                            Count(#L2#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L2# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
            	
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Voice Uploaded Column #ColNum#</li>
                    <li>Voice Duplicate Contacts in Upload List: #GetVUploadStatus.DupListUploadCount#</li>
                    <li>Voice Duplicate Contacts in Table: #GetVUploadStatus.DupUploadCount#</li>
                    <li>Voice Bad Contact Numbers: #GetVUploadStatus.FailedUploadCount#</li>
                    <li>Voice Empty Contact Numbers: #GetVUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact Voice Numbers Added: #GetVUploadStatus.GoodUploadCount#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
            
        </cfif>
        
        <!--- check For Alt Voice Number Uploads --->
        <cfif ListLen(VoiceAltColumns_vch,",") gt 0>
        
        	<cfloop list="#VoiceAltColumns_vch#" index="val" delimiters=",">
            	
                <cfset nAltNum = getToken(val,2,"_")>
                <cfset getCol = getToken(val,1,"_")>
                <cfset ColNum = replaceNoCase(getCol,"C","")>
				<cfset L1a = "LaltFlag" & val>
				<cfset L2a = "LaltDupFlag" & val>
                <cfset L6a = "LaltCellFlag" & val>
                
                <cfmail from="#Application.SiteAdmin#" to="#Application.SiteAdmin#" type="html" subject="Voice Alt Result">
					<cfoutput>
                    	L1a - #L1a#<br />
						L2a - #L2a#
					</cfoutput>
                </cfmail>
                
            	<cfquery name="GetVAUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#L1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1a# = 0) as GoodUploadCount,
                        (select 
                            Count(#L1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1a# = 1) as FailedUploadCount,
                        (select 
                            Count(#L1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1a# = 5) as EmptyUploadCount,
                        (select 
                            Count(#L1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L1a# = 2) as DupUploadCount,
                        (select 
                            Count(#L2a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #L2a# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
                
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Alternate Voice Uploaded Column #ColNum#</li>
                    <li>Alternate Voice Duplicate Contacts in Upload List: #GetVAUploadStatus.DupListUploadCount#</li>
                    <li>Alternate Voice Duplicate Contacts in Table: #GetVAUploadStatus.DupUploadCount#</li>
                    <li>Alternate Voice Bad Contact Numbers: #GetVAUploadStatus.FailedUploadCount#</li>
                    <li>Alternate Voice Empty Contact Numbers: #GetVAUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact Alternate Voice Numbers Added: #GetVAUploadStatus.GoodUploadCount#</li>
                    <li>Alternate Voice Number: #nAltNum#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For Email Addresses Uploads --->
        <cfif ListLen(EmailColumns_vch,",") gt 0>
        
        	<cfloop list="#EmailColumns_vch#" index="ea" delimiters=",">
            	
                <cfset ColNum = replaceNoCase(ea,"C","")>
                <cfset E1 = "EFlag" & ea>
				<cfset E2 = "EDupFlag" & ea>
                
            	<cfquery name="GetEUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#E1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1# = 0) as GoodUploadCount,
                        (select 
                            Count(#E1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1# = 1) as FailedUploadCount,
                        (select 
                            Count(#E1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1# = 4) as NonValidUploadCount,
                        (select 
                            Count(#E1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1# = 5) as EmptyUploadCount,
                        (select 
                            Count(#E1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1# = 2) as DupUploadCount,
                        (select 
                            Count(#E2#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E2# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
            	
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Email Uploaded Column #ColNum#</li>
                    <li>Duplicate Contact Emails in Upload List: #GetEUploadStatus.DupListUploadCount#</li>
                    <li>Duplicate Contact Emails in Table: #GetEUploadStatus.DupUploadCount#</li>
                    <li>Bad Contact Emails: #GetEUploadStatus.FailedUploadCount#</li>
                    <li>Non Valid Contact Emails: #GetEUploadStatus.NonValidUploadCount#</li>
                    <li>Total New Contact Emails Added: #GetEUploadStatus.GoodUploadCount#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For Alt Email Addresses Uploads --->
        <cfif ListLen(EmailAltColumns_vch,",") gt 0>
        
        	<cfloop list="#EmailAltColumns_vch#" index="eal" delimiters=",">
            	
                <cfset E1a = "EaltFlag" & eal>
				<cfset E2a = "EaltDupFlag" & eal>
                <cfset nAltNum = getToken(eal,2,"_")>
                <cfset getCol = getToken(eal,1,"_")>
                <cfset ColNum = replaceNoCase(getCol,"C","")>
                
                <cfset ColNum = replaceNoCase(ea,"C","")>
                
            	<cfquery name="GetEAUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#E1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1a# = 0) as GoodUploadCount,
                        (select 
                            Count(#E1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1a# = 1) as FailedUploadCount,
                        (select 
                            Count(#E1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1a# = 4) as NonValidUploadCount,
                        (select 
                            Count(#E1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1a# = 5) as EmptyUploadCount,
                        (select 
                            Count(#E1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E1a# = 2) as DupUploadCount,
                        (select 
                            Count(#E2a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #E2a# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
            	
                <cfoutput>
                <ul>
                    <li>Duplicate Contact Alternate Emails in Upload List: #GetEUploadStatus.DupListUploadCount#</li>
                    <li>Duplicate Contact Alternate Emails in Table: #GetEUploadStatus.DupUploadCount#</li>
                    <li>Bad Contact Alternate Emails: #GetEUploadStatus.FailedUploadCount#</li>
                    <li>Non Valid Contact Alternate Emails: #GetEUploadStatus.NonValidUploadCount#</li>
                    <li>Total New Contact Alternate Emails Added: #GetEUploadStatus.GoodUploadCount#</li>
                    <li>Alternate Email Number: #nAltNum#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For SMS Number Uploads --->
        <cfif ListLen(SMSColumns_vch,",") gt 0>
        
        	<cfloop list="#SMSColumns_vch#" index="sa" delimiters=",">
            	
                <cfset S1 = "MFlag" & sa>
				<cfset S2 = "MDupFlag" & sa>
                <cfset S6 = "MCellFlag" & sa>
                <cfset ColNum = replaceNoCase(sa,"C","")>
                
            	<cfquery name="GetSUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#S1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1# = 0) as GoodUploadCount,
                        (select 
                            Count(#S1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1# = 1) as FailedUploadCount,
                        (select 
                            Count(#S1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1# = 5) as EmptyUploadCount,
                        (select 
                            Count(#S1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1# = 2) as DupUploadCount,
                        (select 
                            Count(#S2#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S2# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
            	
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For SMS Uploaded Column #ColNum#</li>
                    <li>Duplicate Contact SMS Numbers in Upload List: #GetSUploadStatus.DupListUploadCount#</li>
                    <li>Duplicate Contact SMS Numbers in Table: #GetSUploadStatus.DupUploadCount#</li>
                    <li>Bad Contact SMS Numbers: #GetSUploadStatus.FailedUploadCount#</li>
                    <li>Empty Contact SMS Numbers: #GetSUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact SMS Numbers Added: #GetSUploadStatus.GoodUploadCount#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For Alt SMS Number Uploads --->
        <cfif ListLen(SMSAltColumns_vch,",") gt 0>
        
        	<cfloop list="#SMSAltColumns_vch#" index="sal" delimiters=",">
            	
                <cfset nAltNum = getToken(sal,2,"_")>
                <cfset getCol = getToken(sal,1,"_")>
                <cfset ColNum = replaceNoCase(getCol,"C","")>
                <cfset S1a = "MaltFlag" & sal>
				<cfset S2a = "MaltDupFlag" & sal>
                <cfset S6a = "MaltCellFlag" & sal>
                
            	<cfquery name="GetSAUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#S1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1a# = 0) as GoodUploadCount,
                        (select 
                            Count(#S1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1a# = 1) as FailedUploadCount,
                        (select 
                            Count(#S1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1a# = 5) as EmptyUploadCount,
                        (select 
                            Count(#S1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S1a# = 2) as DupUploadCount,
                        (select 
                            Count(#S2a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #S2a# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
                
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Alternate SMS Uploaded Column #ColNum#</li>
                    <li>Alternate SMS Duplicate Contacts in Upload List: #GetSAUploadStatus.DupListUploadCount#</li>
                    <li>Alternate SMS Duplicate Contacts in Table: #GetSAUploadStatus.DupUploadCount#</li>
                    <li>Alternate SMS Bad Contact Numbers: #GetSAUploadStatus.FailedUploadCount#</li>
                    <li>Alternate SMS Empty Contact Numbers: #GetSAUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact Alternate SMS Numbers Added: #GetSAUploadStatus.GoodUploadCount#</li>
                    <li>Alternate SMS Number: #nAltNum#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For Fax Number Uploads --->
        <cfif ListLen(FaxColumns_vch,",") gt 0>
        
        	<cfloop list="#FaxColumns_vch#" index="fa" delimiters=",">
            	
                <cfset F1 = "FFlag" & fa>
				<cfset F2 = "FDupFlag" & fa>
                <cfset F6 = "FCellFlag" & fa>
                <cfset ColNum = replaceNoCase(fa,"C","")>
                
            	<cfquery name="GetFUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#F1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1# = 0) as GoodUploadCount,
                        (select 
                            Count(#F1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1# = 1) as FailedUploadCount,
                        (select 
                            Count(#F1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1# = 5) as EmptyUploadCount,
                        (select 
                            Count(#F1#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1# = 2) as DupUploadCount,
                        (select 
                            Count(#F2#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F2# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
            
            	<cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Fax Uploaded Column #ColNum#</li>
                    <li>Duplicate Contact Fax Numbers in Upload List: #GetFUploadStatus.DupListUploadCount#</li>
                    <li>Duplicate Contact Fax Numbers in Table: #GetFUploadStatus.DupUploadCount#</li>
                    <li>Bad Contact Fax Numbers: #GetFUploadStatus.FailedUploadCount#</li>
                    <li>Empty Contact Fax Numbers: #GetFUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact Fax Numbers Added: #GetFUploadStatus.GoodUploadCount#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
        
        </cfif>
        
        <!--- check For Alt Fax Number Uploads --->
        <cfif ListLen(FaxAltColumns_vch,",") gt 0>
        	
            <cfloop list="#FaxAltColumns_vch#" index="fal" delimiters=",">
            	
                <cfset nAltNum = getToken(fal,2,"_")>
                <cfset getCol = getToken(fal,1,"_")>
                <cfset ColNum = replaceNoCase(getCol,"C","")>
                <cfset F1a = "FaltFlag" & fal>
				<cfset F2a = "FaltDupFlag" & fal>
                <cfset F6a = "FaltCellFlag" & fal>
                
            	<cfquery name="GetFAUploadStatus" datasource="#Session.DBSourceEBM#">
                SELECT 
                        *,
                        (select 
                            Count(#F1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1a# = 0) as GoodUploadCount,
                        (select 
                            Count(#F1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1a# = 1) as FailedUploadCount,
                        (select 
                            Count(#F1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1a# = 5) as EmptyUploadCount,
                        (select 
                            Count(#F1a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F1a# = 2) as DupUploadCount,
                        (select 
                            Count(#F2a#) 
                         from 
                            simplexuploadstage.#tablename_vch#
                         Where
                            #F2a# = 1) as DupListUploadCount
                FROM
                        simplexuploadstage.#tablename_vch#
             	</cfquery>
                
                <cfoutput>
                <ul>
                	<li style="font-size:14px; font-weight:bold; list-style:none;">Result For Alternate Fax Uploaded Column #ColNum#</li>
                    <li>Alternate Fax Duplicate Contacts in Upload List: #GetFAUploadStatus.DupListUploadCount#</li>
                    <li>Alternate Fax Duplicate Contacts in Table: #GetFAUploadStatus.DupUploadCount#</li>
                    <li>Alternate Fax Bad Contact Numbers: #GetFAUploadStatus.FailedUploadCount#</li>
                    <li>Alternate Fax Empty Contact Numbers: #GetFAUploadStatus.EmptyUploadCount#</li>
                    <li>Total New Contact Alternate Fax Numbers Added: #GetFAUploadStatus.GoodUploadCount#</li>
                    <li>Alternate Fax Number: #nAltNum#</li>
                </ul>
                </cfoutput>
                
            </cfloop>
            
        </cfif>
        
        <!--- check For Variables Uploads --->
        <cfif ListLen(VariableColumns_vch,",") gt 0>
        
        </cfif>
        
    </cfoutput>
    

    <!---<cfoutput>
    <cfset totaldup = GetUploadStatus.dupcount + GetUploadStatus.dupgcount>
    <cfset totalcon = GetUploadStatus.goodcount + GetUploadStatus.dupgcount>
    <ul>
    	<li>Total Contacts Uploaded: #GetUploadStatus.recordcount#</li>
        <li>Total Duplicate Contacts: #totaldup#</li>
        <li>Total Duplicate Contacts in List: #GetUploadStatus.dupcount#</li>
        <li>Total Bad Contact Numbers: #GetUploadStatus.badcount#</li>
        <li>Total New Contacts Added: #GetUploadStatus.goodcount#</li>
        <li>Total Contacts Added To List: #totalcon#</li>
    </ul>
    </cfoutput>--->
    </div>
</div>
</cfif>