
<!---<script>
var setUploadUrl = function(id) 
    { 
		var radioButtons = document.getElementsByName("phoneListId");
      	for (var x = 0; x < radioButtons.length; x ++) {
        	if (radioButtons[x].checked) {
          	var selectedListId = radioButtons[x].value;
        	}
      	}
        var selectedFiles = ColdFusion.FileUpload.getSelectedFiles(id);
        var uploadUrl = "fileUpload?phoneId="+selectedListId;
        if(selectedFiles.length){ 
            ColdFusion.FileUpload.setURL(id,uploadUrl); 
            ColdFusion.FileUpload.startUpload(id); 
			$("#uploadingload").fadeIn(200);
        } 
    } 
</script>--->
	<!--- This sets up the pagnation links --->
    
    <cfif IsDefined("url.max")>
        <cfset max = url.max>
    <cfelse>
        <cfset max = 20>
    </cfif>
    <cfset start = IIF(IsDefined('start'), 'start', DE("1"))>
    <cfif NOT IsNumeric(start)>
        <cfset start = 1>
    </cfif>
    
    <cfinvoke component="session.lists.contacts.cfc.phonelist" method="GetGroupData" returnvariable="records" />
    
    <!---<cfset records.recordcount = 541>--->
    
    <!---<div id="uploadlist" style="float:left; margin-top:15px; display:inline-block;" class="norm">
    
    <cfform onsubmit="return ListUpload('uploadListForm');" id="uploadListForm" enctype="multipart/form-data">

          <cffileupload 
              extensionfilter="csv,txt" 
              name="portfoliofiles" 
              maxfileselect="1" 
              title="Upload Phone Contact List (MUST BE COMMA DELIMITED!)" 
              progressbar="true"
              oncomplete="fileComplete"
              onerror="fileUploadError"
              onUploadComplete="fileUploadComplete"
              stoponerror="true"
              url = "fileUpload"
              height="200"
              width="500"
              wmode="transparent"
              bgcolor="##4c7897"
              hideuploadbutton="true"
              > 
              <cfinput type="button" name="submit" value="Click To Upload Files" onClick="setUploadUrl('uploader')" style="padding:3px;"> 
        
    </div>--->
    
    
    <div style="display:inline-block; position:relative; width:820px;">
    <cfif records.recordcount neq 0>
      <cfif (start + (max - 1)) gt records.recordcount>
          <cfset endc = records.recordcount>
      <cfelse>
          <cfset endc = start + (max - 1)>
      </cfif>
      <cfoutput>
      <div style="float:left; position:relative; margin-left:10px; margin-top:5px; width:220px; text-align:left;" class="norm">Displaying #start# to #endc# of #records.recordcount# records</div>
      </cfoutput>
      <div style="float:inherit; position:relative; margin-bottom:0px; margin-top:0px; width:810px; text-align:right;">
      <cfif records.recordcount GT max>
          <cfset pagecount = #Ceiling((records.recordcount / max))#>
          <cfset currentpage = #Ceiling((start/max))#>
          
          <cfif currentpage gt "1">
                  <cfset pstp = (currentpage * max) - ((max * 2) - 1)>
                  <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_phonelist?start=#pstp#&max=#max#', 'phonediv')" style="margin-right:5px;">&lt;</a></cfoutput>
          </cfif> 
          
          <cfif currentpage gte 6>
              <cfset pc = currentpage - 5>
          <cfelse>
              <cfset pc = 1>
          </cfif>
          
          <cfif pc + 9 lt pagecount>
              <cfset curpagect = pc + 9>
          <cfelse>
              <cfset curpagect = pagecount>
          </cfif>
          <cfif pagecount - pc gte 9>
              <cfset pc = curpagect - 9>
          <cfelseif pagecount - pc lt 9 and pagecount gte 10>
              <cfset pc = pagecount - 9>
          <cfelse>
              <cfset pc = 1>
          </cfif>
          <cfset stp = 1>
          <cfloop condition="#pc# LESS THAN OR EQUAL TO curpagect">
              <cfset sc = (pc * max) - (max - 1)>
                  <cfoutput>
                  <cfif pc is not currentpage>
                      <a class="brsl" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_phonelist?start=#sc#&max=#max#', 'phonediv')" style="margin-left:5px;">#NumberFormat(pc,'00')#</a>
                  <cfelse>
                      <span class="norm" style="padding:3px;"><strong>#NumberFormat(pc,'00')#</strong></span>
                      <cfset stp = sc + max>
                  </cfif>
                  </cfoutput>
              <cfset pc = #IncrementValue(pc)#>
          </cfloop>
          <cfif stp LTE records.recordcount>
              <cfoutput><a class="pagarrow" href="javascript:ColdFusion.navigate('cfdiv/cfdiv_phonelist?start=#stp#&max=#max#', 'phonediv')" style="margin-left:5px;">&gt;</a></cfoutput>
          </cfif>
      <cfelse>
      <cfset max = records.recordcount>
      </cfif>
      </div>
    </cfif>
    </div>
    <!--- Current Phone Lists --->
    <div style="position:relative; width:820px; margin-top:5px; display:inline-block;">
    <!---<cfoutput query="getplist">--->
    <!--- Phone List Header --->
    <div style="width:820px; position:relative; display:inline-block;" class="barheaders">
        <div style="float:left; margin:7px 0 0 10px; width:537px;">
            Current Lists:
        </div>
        <div style="float:left; height:27px; width:1px; margin-top:1px; background:#333;"></div>
        <div style="float:left; margin:7px 0 0 0; width:74px; text-align:center;">
            List Size:
        </div>
        <div style="float:left; height:27px; width:1px; margin-top:1px; background:#333;"></div>
        <div style="float:right; margin:7px 0 0 0; width:195px; text-align:center;">
            Action:
        </div>
    </div>
    
    <div id="recordsDiv">
    <form>
    <cfoutput query="records" startrow="#start#" maxrows="#max#">
        <div style="background:##f8f8f8; min-height:45px; border-bottom:1px solid ##234c68; position:relative;" class="norm" id="record#GROUPID_INT#">
        	<div style="float:left; margin:15px 0 0 10px; width:25px;">
                <input type="radio" name="phoneListId" value="#GroupId_int#" />
            </div>
            <div style="float:left; margin:15px 0 0 10px; width:502px;">
                #DESC_VCH#
            </div>
            <div style="float:left; margin:15px 0 0 0; width:74px; text-align:center;">
                #numberFormat(tccl,"999,999,999")#
            </div>
            <div style="float:right; margin:2px 0 0 0; width:195px; text-align:center; position:relative;">
                <div id="editdiv_#GROUPID_INT#" style="float:left; margin-left:20px;">
                <img src="images/asp-edit.png" width="40" height="40" title="Edit List" class="edticonhover" onclick="editPhoneList('#GROUPID_INT#')" id="edit_#GROUPID_INT#">
                </div>
                <div id="viewdiv_#GROUPID_INT#" style="float:left;">
                <img src="images/asp-view.png" style="margin-left:0px;" width="40" height="40" title="View List" class="vwiconhover" onclick="viewPhoneList('#GROUPID_INT#')" id="view_#GROUPID_INT#">
                </div>
                <div id="repdiv_#GROUPID_INT#" style="float:left;">
                <img src="images/asp-download.png" style="margin-left:0px;" width="40" height="40" title="Download Contact List" class="dwnconhover" onclick="getExport('#GROUPID_INT#')" id="report_#GROUPID_INT#">
                </div>
                <div id="deletediv_#GROUPID_INT#" style="float:left;">
                <img src="images/asp-delete.png" style="margin-left:0px;" width="40" height="40" title="Delete List" class="deliconhover" onclick="delPhoneList('#GROUPID_INT#')" id="delete_#GROUPID_INT#">
                </div>
            </div>
        </div>
    </cfoutput>
    </form>
    </div>
    <!---</cfoutput>--->
    </div> 