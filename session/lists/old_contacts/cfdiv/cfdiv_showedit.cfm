<cfif isDefined("url.lid")>
	
    <cfinvoke component="#Application.SessionCFCPath#.phonelist" method="GetListName" returnvariable="lisname">
        <cfinvokeargument name="ListID" value="#url.lid#">
    </cfinvoke>
    
    
    <cfif isDefined("lisname.recordcount") and lisname.recordcount neq 0>
    <!--- Current Phone Lists --->
    <div style="float:right; position:relative; margin-top:5px; margin-right:10px; text-align:center; display:inline-block; font-size:12px;">
        <cfoutput query="lisname">
            <div style="float:left; text-align:left; margin-top:5px; display:inline-block;">
                <span style="font-weight:800; margin-right:15px;">
                    <cfif url.tp eq "v">
                        Now Displaying:
                    <cfelseif url.tp eq "e">
                        Now Editing:
                    </cfif>
                </span>
                <br />
                [ #DESC_VCH# ]
            </div>
            
            <div style="float:right; margin-left:10px;">
                <cfif url.tp eq "v">
                    <img src="resources/images/asp-view-o.png" style="margin-left:5px; vertical-align:middle; margin-bottom:.25em;" width="40" height="40" title="View List">
                <cfelseif url.tp eq "e">
                    <img src="resources/images/asp-edit-o.png" style="margin-left:5px; vertical-align:middle; margin-bottom:.25em;" width="40" height="40" title="Edit List">
                </cfif>
            </div>
        </cfoutput>
    </div>
    </cfif>
    
</cfif>