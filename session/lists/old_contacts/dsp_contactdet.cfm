
<cfif isDefined("url.cid") and IsNumeric(url.cid)>

	<!---<cfquery name="contactInfo" datasource="#application.datasource#">
       SELECT 
            *
       FROM 
       		SimpleLists.GroupContactList 
                inner join SimpleLists.Contactstring on SimpleLists.GroupContactList.ContactAddressId_bi = SimpleLists.Contactstring.ContactAddressId_bi
                inner join SimpleLists.Contactlist on SimpleLists.Contactstring.ContactId_bi = SimpleLists.Contactlist.ContactId_bi
            EMAILS..[email_MultiList] left join EMAILS..[email_UserVariables] on EMAILS..[email_MultiList].ContactId_int = EMAILS..[email_UserVariables].ContactId_int
       WHERE 
            EMAILS..[email_MultiList].CompanyId_int = #session.UserId_int# and 
            EMAILS..[email_MultiList].ContactId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.cid#">
    </cfquery>--->
    
    <cfquery datasource="#session.DBSourceEBM#" name="contactInfo">
            SELECT 
            	SimpleLists.Contactstring.ContactId_bi,
                SimpleLists.Contactstring.ContactString_vch,
                SimpleLists.Contactstring.ContactType_int,
                SimpleLists.Contactstring.Altnum_int,
                SimpleLists.Contactstring.ContactAddressId_bi,
                SimpleLists.Contactstring.Optin_int,
                SimpleLists.Contactstring.OptOut_int,
                SimpleLists.Contactlist.FirstName_vch,
                SimpleLists.Contactlist.LastName_vch,
                SimpleLists.Contactlist.Address_vch,
                SimpleLists.Contactlist.Address1_vch,
                SimpleLists.Contactlist.City_vch,
                SimpleLists.Contactlist.State_vch,
                SimpleLists.Contactlist.ZipCode_vch,
                SimpleLists.Contactlist.Country_vch,
                SimpleLists.Contactlist.UserDefinedKey_vch,
                SimpleLists.Contactlist.Created_dt,
                SimpleLists.ContactVariable.VariableName_vch,
                SimpleLists.ContactVariable.VariableValue_vch,
                SimpleLists.ContactVariable.ContactVariableId_bi
            FROM 
            	SimpleLists.Contactlist 
                	left join SimpleLists.Contactstring on SimpleLists.Contactlist.ContactId_bi = SimpleLists.Contactstring.ContactId_bi
                    left join SimpleLists.ContactVariable on SimpleLists.Contactlist.ContactId_bi = SimpleLists.ContactVariable.ContactId_bi
            WHERE 
            	SimpleLists.Contactlist.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" VALUE="#Session.UserId#"> and
                SimpleLists.Contactlist.ContactId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.cid#">
		</cfquery>

<div style="display:inline-block; margin-left:50px;">

    <div style="height:30px; width:625px; margin-top:15px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:#2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> CONTACT DETAILS:
        </div>
    </div>
    <cfoutput query="contactInfo" group="ContactId_bi">
    <div id="singlecontact" style="width:595px; border:1px solid ##cccccc; display:inline-block; padding:15px; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px; ">
        <form id="UpdateContactForm" name="UpdateContactForm">
        	<input type="hidden" name="ContactId" id="ContactId" value="#ContactId_bi#" />
        <div style="float:left;">
            <label for="FirstName_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">First Name:
            </label>
            <input type="text" class="mainLogin" name="FirstName_vch" id="FirstName_vch" value="#FirstName_vch#">
        </div>
        <div style="float:right; margin-left:15px;">
            <label for="LastName_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Last Name:
            </label>
            <input type="text" class="mainLogin" name="LastName_vch" id="LastName_vch" value="#LastName_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="Address_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Address:
            </label>
            <input type="text" class="mainLogin" name="Address_vch" id="Address_vch" value="#Address_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="Address1_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Suite/Apt:
            </label>
            <input type="text" class="mainLogin" name="Address1_vch" id="Address1_vch" value="#Address1_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="City_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> City:
            </label>
            <input type="text" class="mainLogin" name="City_vch" id="City_vch" value="#City_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="State_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> State:
            </label>
            <input type="text" class="mainLogin" name="State_vch" id="State_vch" value="#State_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="ZipCode_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Postal Code:
            </label>
            <input type="text" class="mainLogin" name="ZipCode_vch" id="ZipCode_vch" value="#ZipCode_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="position:relative;">
            <label for="Country_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:120px;">
               <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;"> Postal Code:
            </label>
            <input type="text" class="mainLogin" name="Country_vch" id="Country_vch" value="#Country_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="float:left;">
            <label for="UserSpecifiedData_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:150px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">User Defined Data:
            </label>
            <input type="text" class="mainLogin" name="UserDefinedKey_vch" id="UserDefinedKey_vch" style="width:250px;" value="#UserDefinedKey_vch#">
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="float:left;">
            <label for="userDefined" style="text-align:left; color:##2b83a7; font-weight:bold; width:150px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Contact Created:
            </label>
            <span style="display:inline-block; margin-top:5px; font-weight:bold;">
            #DateFormat((Created_dt),'mm/dd/yyyy')#
            </span>
        </div>
        <div style="float:right; margin-right:15px; font-size:11px;">
            <label for="userDefined" style="text-align:left; color:##2b83a7; font-weight:bold; width:150px;">
                <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Last Updated:
            </label>
            <span style="display:inline-block; margin-top:5px; font-weight:bold;">
            <!---#DateFormat((LastUpdated_dt),'mm/dd/yyyy')#--->
            </span>
        </div>
        <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
        <div style="float:right; margin-left:15px; font-size:11px;">
            <input type="submit" name="updatecontact" value="update contact" class="mainSubmit" style="padding:5px 10px 5px 10px;" onclick="return upSingle();">
        </div>
		</form>
	
    </div>
<!--- Contact Numbers --->
	<div style="height:30px; width:625px; margin-top:15px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:##2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> CONTACT NUMBERS:
        </div>
    </div>
    <div id="Ccontact" style="width:595px; position:relative; display:inline-block; padding:15px; border:1px solid ##cccccc; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px; ">
    <form id="UpdateContactForm" name="UpdateContactForm">
    <input type="hidden" name="ContactId" id="ContactId" value="#ContactId_bi#" />        
	<cfoutput>
    <div style="position:relative;">
    	
        <div style="float:left;">
    	<input type="hidden" name="ContactAddressId_bi" id="ContactAddressId_bi" value="#ContactAddressId_bi#" />   
        <label for="ContactString_vch" style="text-align:left; color:##2b83a7; font-weight:bold; width:100px;">
            <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">Contact:
        </label>
        </div>
        <div style="float:left; margin:5px 0 0 5px; width:120px; font-size:11px; text-align:left;">
            <cfif ContactType_int eq 1 and AltNum_int eq 0>
            Voice
            <cfelseif ContactType_int eq 1 and AltNum_int neq 0>
            Alternate Voice
            <cfelseif ContactType_int eq 2 and AltNum_int eq 0>
            Email
            <cfelseif ContactType_int eq 2 and AltNum_int neq 0>
             Alternate Email
            <cfelseif ContactType_int eq 3 and AltNum_int eq 0>
            SMS
            <cfelseif ContactType_int eq 3 and AltNum_int neq 0>
             Alternate SMS
            <cfelseif ContactType_int eq 4 and AltNum_int eq 0>
            Fax
            <cfelseif ContactType_int eq 4 and AltNum_int neq 0>
             Alternate Fax
            </cfif>
        </div>
        <div style="float:left;">
        <input type="text" class="mainLogin" name="ContactString_vch" id="ContactString_vch" value="#ContactString_vch#" readonly="readonly">
        </div>
        <div style="float:left; display:inline-block; margin:5px 0 0 15px;">
            <cfif Optin_int eq 1 and OptOut_int eq 0>
                <span style="color:##3C3;">Active</span><span style="margin-left:15px; display:inline-block; width:230px; text-align:right;"><input type="checkbox" name="Unsub" style="margin-right:10px; margin-bottom:.25em; vertical-align:middle;" />Unsubscribe</span>
            <cfelseif Optin_int eq 1 and OptOut_int eq 1>
                <span style="color:##F00;">UnSubscribed</span>
            <cfelse>
                <span style="color:##F00;">Not Subscribed</span>
            </cfif>
        </div>
    </div>
    <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
    </cfoutput>

    </div>

<!--- User Defined Columns --->
	<div style="height:30px; width:625px; margin-top:15px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:##2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> USER DEFINED FIELDS:
        </div>
    </div>
    <div id="UDcontact" style="width:595px; display:inline-block; padding:15px; border:1px solid ##cccccc; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px; ">
    <cfoutput>
    	<cfif IsDefined("VariableId_bi") and len(trim(VariableId_bi)) neq 0>
    	<cfset nvarid = encrypt(VariableId_bi,application.enca,"AES","HEX")>
        <form id="UDV#nvarid#" name="UDV#nvarid#">
        	<input type="hidden" name="udvid" value="#nvarid#" />
            <input type="hidden" name="ctvid" value="#ContactId_int#" />
            <div style="float:left;">
                <label for="VariableName_vch" style="text-align:left; color:##2b83a7; font-weight:bold;">
                    <img src="images/tini-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:5px;">#VariableName_vch#:
                </label><br />
                <input type="text" class="mainLogin" name="VariableValue_vch" id="VariableValue_vch" style="width:375px;" value="#VariableValue_vch#">
            </div>
            <div style="float:right; margin-top:23px; font-size:11px;">
                <input type="button" name="updatecontact" value="update field" class="mainSubmit" style="padding:5px 10px 5px 10px;" onclick="return upUDVar('#nvarid#');">
            </div>
            <div style="clear:both; border-bottom:1px dashed ##cccccc; padding-bottom:5px; margin-bottom:5px;"></div>
		</form>
        </cfif>
  </cfoutput>
        <div id="uploadingloadsc" style="width:625px; text-align:center; display:none; padding:10px;">
            Please wait while we process your contact.<br />
            <img src="images/loading.gif" style="margin-top:20px;" />
        </div>
        <div id="uploadingdonesc" style="width:625px; text-align:center; display:none; padding:10px; font-weight:bold;">
            
        </div>

    </div>
	
    <div style="height:30px; width:625px; margin-top:15px;" class="mheader">
        <div style="padding-top:7px; margin-left:10px; font-size:14px; font-weight:bold; width:100%; color:##2b83a7;">
        <img src="images/header-rarrow.png" style="margin-bottom:.25em; vertical-align:middle; margin-right:10px;"> ADD USER DEFINED VALUE:
        </div>
    </div>
    <div id="UDcontact" style="width:595px; display:inline-block; padding:15px; border:1px solid ##cccccc;">
  		<cfquery name="getudvars" datasource="#session.DBSourceEBM#">
            select 
                Distinct(VariableName_vch)
            from 
                SimpleLists.ContactVariable
            where
               UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
        </cfquery>
  		<form id="addUDV" name="addUDV">
            <input type="hidden" name="ctvid" value="#ContactId_bi#" />
            <div style="float:left; line-height:30px;">
                    Field:  
                    <select name="VariableName" style="margin-left:10px; padding:3px;">
                    	<cfloop query="getudvars">
                    	<option value="#VariableName_vch#">#VariableName_vch#</option>
                        </cfloop>
                    </select>
                <br />
                Value: <input type="text" class="mainLogin" name="VariableValue" id="VariableValue" style="width:250px; margin-left:4px;">
            </div>
            <div style="float:right; margin-top:23px; font-size:11px;">
                <input type="button" name="updatecontact" value="add field" class="mainSubmit" style="padding:5px 10px 5px 10px;" onclick="return addUDVar();">
            </div>
            <div style="clear:both;"></div>
		</form>
        <div id="uploadingloadsc" style="width:625px; text-align:center; display:none; padding:10px;">
            Please wait while we process your contact.<br />
            <img src="images/loading.gif" style="margin-top:20px;" />
        </div>
        <div id="uploadingdonesc" style="width:625px; text-align:center; display:none; padding:10px; font-weight:bold;">
            
        </div>

    </div>
    
    
	</cfoutput>
</div>
<!---YES <cfoutput>#url.lid#</cfoutput>--->
<cfelse>
	<!---NO--->
</cfif>