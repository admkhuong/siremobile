<cftry>
<cfset session.ImportFileOnDB = "">
<cfset session.uploadlist = "">
<cfset session.ColumnCount = "">
<cfset fuldatetime = dateformat(now(),'mmddyy') & timeformat(now(),'HHmmss')>
<!---<cfmail from="#Application.SiteAdmin#" to="#Application.SiteAdmin#" type="html" subject="dump asp file upload userid">
            <cfoutput>#Session.UserId#</cfoutput>
        </cfmail>--->
<cfif !DirectoryExists("#PhoneListUploadLocalWritePath#\U#Session.UserId#")>
    <cfdirectory action="create" directory="#PhoneListUploadLocalWritePath#\U#Session.UserId#">
</cfif>

<cffile 
    action="upload" 
    destination="#PhoneListUploadLocalWritePath#\U#Session.UserId#\PhoneList_#Session.UserId#_#session.currentlist#_#fuldatetime#.csv" 
    nameconflict="overwrite" 
    result="CurrUpload"
    > 

<!---<cffile 
    action="copy" 
    file="#Application.uploaddirectory#\PhoneList_#Session.UserId#_#session.currentList#_#fuldatetime#.csv" 
    destination="#Application.PhoneListUploadLocalWritePath#\U#Session.USERID#\PhoneList_#Session.UserId#_#session.currentList#_#fuldatetime#.csv"
    >--->

<!---
	Get the file path to the CSV data file that we will be reading
	in with an Input Stream (so as not to have to read the whole
	file at one time).
--->
<cfset filePath = CurrUpload.serverDirectory & "\" & CurrUpload.serverFile >
<!--- 
	Set the file path and file name for bulk importing
--->
<cfset session.ImportFileOnDB = "#PhoneListDBLocalWritePath#\\U#Session.UserId#\\PhoneList_#Session.UserId#_#session.currentList#_#fuldatetime#.csv">
 
<!---
	Create our handler. This must have one method - handleEvent() -
	which can respond to events published by the CSV parser.
--->
<cfset handler = createObject( "component", "session.lists.contacts.cfc.Handler" ).init()>

 
<!--- Create our CSV data evented parser. --->
<cfset parser = createObject( "component", "session.lists.contacts.cfc.CSVParser" ).init(filePath,handler)>


<!--- Set session vairiable with handler results --->
<cfset session.uploadlist = handler.getData()>

<!--- set session variable with uploaded files column count --->
<cfset session.ColumnCount = ArrayLen(session.uploadlist[1])>

<script>
	fileUploadComplete();
</script>

    <cfcatch type="any">

    	<cfmail from="#Application.SiteAdmin#" to="jpeterson@messagebroadcast.com" type="html" subject="dump asp file upload catch">
            <cfdump var="#cfcatch#"/>
        </cfmail>
    
    </cfcatch>

</cftry>
