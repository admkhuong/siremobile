<cfif IsDefined("lid") and IsNumeric(lid)>

<cfinvoke component="session.lists.contacts.cfc.newcontactlist" method="ListExport" returnvariable="records">
    <cfinvokeargument name="ListId" value="#lid#">
</cfinvoke>

<!---<cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="ListExport Catch Error 1 " type="html">
            <cfdump var="#records#">
        </cfmail>--->

    <cftry>
    
    <cfset pfilename = replace(records.GroupList," ","_","all")>
    <cfset nfilename = replace(pfilename,":","_","all")>
    <cfspreadsheet  
        action="write" 
        filename = "#ExpandPath("upload")#\#nfilename#.xls"
        overwrite = "true"
        query = "records"
        >
        
        
        <!---<cfdump var="#records#">--->
        
        <cfheader name="content-disposition" value="attachment; filename=#nfilename#.xls" />
            
            
        <cfcontent type="application/vnd.ms-excel" file="#ExpandPath("upload")#\#nfilename#.xls">
        
        
        <cfcatch type="any">
             <cfmail to="#Application.SiteAdmin#" from="#Application.SiteAdmin#" subject="ListExport Catch Error" type="html">
                <cfdump var="#cfcatch#">
            </cfmail>
        </cfcatch>
        
        
    </cftry>
    
</cfif>