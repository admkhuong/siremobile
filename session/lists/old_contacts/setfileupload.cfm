<style>
.norm {
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;	
}
</style>
    
<!---<cfajaximport tags="cfform">  --->  
    
<!---<cfajaxproxy cfc="cfc/phonelist" jsclassname="AddFormValues" />--->
<cfajaxproxy cfc="/session/lists/contacts/cfc/newcontactlist" jsclassname="AddContactsList" />

<script type="text/javascript">
var NFCValue = new AddContactsList();

<!--- Form Submissions --->
var ListCreation = function(){
	NFCValue.setAsyncMode();
	NFCValue.setCallbackHandler(DataFieldsResult);
	NFCValue.setForm('cnlist');
	NFCValue.addgroup();
	return false;
}	
<!--- Set Columns --->
var SetColumns = function(clul){
	NFCValue.setAsyncMode();
	NFCValue.setCallbackHandler(uploadFileResult);
	NFCValue.setForm(clul);
	NFCValue.AddColumns();
	$("#uploadchoose").fadeOut(200);
	$("#ProcessListLoad").delay(200).fadeIn(200);
	return false;
}
<!--- check column duplicate --->
function doubleCheckColumn(clul){
		var col1 = 0;
		var col1a = 0;
		var col2 = 0;
		var col3 = 0;
		var col4 = 0;
		var cer = 0;
		if($('#Scrub').is(':checked')){
			alert('You have stated that you DO have explicit written consent from every mobile phone number on your list. We WILL NOT remove the mobile phone numbers on your list.');	
		}else{
			alert('You have stated that you DO NOT have explicit written consent and we WILL remove the mobile phone numbers on your list during the upload process.');	
		}
		$(':input','#columnlistupload').each(function(){
			
			if(this.value == 'Mobile Number' || this.value == 'Mobile Alt' || this.value == 'Land Number' || this.value == 'Land Alt' || this.value == 'Email' || this.value == 'Email Alt' || this.value == 'Fax' || this.value == 'Fax Alt'){
				col1++;
				col1a++;
			}
			if(this.value == 'Identifier'){
				col2++;
			}
			if(this.value == 'First Name'){
				col3++;
			}
			if(this.value == 'Last Name'){
				col4++;
			}
		});
		
		if(col1a == 0){
			alert("You MUST choose at least one Contact Number column");
			cer = 1;
		}
		if(col2 > 1){
			alert("You can only choose one Identifier column");
			cer = 1;
		}
		if(col3 > 1){
			alert("You can only choose one First Name column");
			cer = 1;
		}
		if(col4 > 1){
			alert("You can only choose one Last Name column");
			cer = 1;
		}
		
		if(cer == 0 && col1a != 0){
			SetColumns('columnlistupload');
		}else{
		return false;	
		}
}

<!--- Results --->
var NewNameResult = function(nError){
	if (nError.TEXT == 'No Error'){

		<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_showedit.cfm?tp='+nError.TYPE+'&lid='+nError.CAMP, 'showeditdiv');</cfoutput>
		<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp='+nError.TYPE+'&lid='+nError.CAMP, 'contactdetdiv');</cfoutput>
		$('#listcreationview').fadeOut(200);
		$('#listdetailview').delay(200).fadeIn(300);
		setTimeout(function(){
			$("#phoneupload").trigger('click');
			$("#crenew").fadeOut(200);
			$("#addtlist").fadeIn(500);
		},100);
		
		alert('List Name Changed');
		return false;
	}
}



var DataFieldsResult = function(nError){
	if (nError.TEXT == 'No Error'){
		<cfoutput>
		ColdFusion.navigate('cfdiv/cfdiv_list.cfm', 'contactdiv');
		</cfoutput>
		<!---<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_showedit.cfm?tp=v&lid='+nError.ID, 'showeditdiv');</cfoutput>
		<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=v&lid='+nError.ID, 'contactdetdiv');</cfoutput>--->
		$("#groupdesc").val("");
		makepliston = 1;
		return false;
	}
	else{
		alert(nError.TEXT);
	}
}	
var uploadFileResult = function(results){
	var DCF = results.TABLE;
	<!---<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+results.ID, 'contactdetdiv');</cfoutput>--->
	<!---Coldfusion.Grid.refresh('phoneListDetailGrid', false);--->
	$("#uploadingload").fadeOut(200);
	$("#uploadchoose").fadeOut(200);
	$("#ProcessListLoad").fadeOut(200);
	setTimeout(function(){
	$("#uploadlistresult").fadeIn(500);
	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listuploadresults.cfm?DCS='+DCF, 'uplresults');</cfoutput>
	}, 200);
}
function addnewcontactspop(){
	  $('#getaddlistc')
	  .load('addnewcontacts.cfm?lid=fc')
	  .dialog({
		  height: 900,
		  width:750,
		  modal: true,
		  title: 'Add/Import Contacts',
		  resizable: false,
		  hide: "explode",
		  show: "fade",
		  close: function(event, ui) { ColdFusion.navigate('cfdiv/cfdiv_list.cfm', 'contactdiv'); }
	  });
}
function addnewcontacts(id){
	var imageIDVal = id;
	$( "#getaddlist:ui-dialog" ).dialog( "destroy" );
	$( "#getaddlist" ).html( "" );
	$.ajax({
		  type:"POST",
		  url:"cfc/newcontactlist.cfc?method=setCurrentList",
		  data: "GroupListId="+imageIDVal,
		  cache:false,
		  success: function(msg) {
			  $('#uploadlist').show();
			  	<!---$('#getaddlist')
				.load('addnewcontacts.cfm?lid='+imageIDVal)
				.dialog({
					height: 1000,
					width:750,
					modal: true,
					title: 'Add/Import Contacts',
					resizable: false,
					hide: "explode",
					show: "fade",
					close: function(event, ui) { ColdFusion.navigate('cfdiv/cfdiv_list.cfm', 'contactdiv'); }
				});--->
		  }
	});
}
function getExport(id){
	var imageIDVal = id;
	$('#getaddlist3')
		.html('<div style="width:100%; margin-top:15px; text-align:center;"><a href="ListExport.cfm?lid=' + imageIDVal + '">Click To Download Contacts</a></div>')
		.dialog({
			height: 100,
			width:250,
			modal: true,
			title: 'Contact List Export',
			resizable: false,
			hide: "explode",
			show: "fade"
		});
}
function viewContactDet(id){
	var imageIDVal = id;
	$('#getaddlist4')
		.load('dsp_contactDet.cfm?cid='+imageIDVal)
		.dialog({
			scroll: true,
			height: 550,
			width:750,
			modal: true,
			title: 'View/Edit Contact Information',
			resizable: false,
			hide: "explode",
			show: "fade",
			// open:function(){$('#getaddlist4').scrolltop(1); }
			open: function (event, ui) {
   			 $('#getaddlist4').css('overflow', 'auto'); //this line does the actual hiding
  			}
			
		});
}
function editContactList(id)
{	
	var imageIDVal = id;
	$.ajax({
		  type:"POST",
		  url:"cfc/newcontactlist.cfc?method=setCurrentList",
		  data: "GroupListId="+imageIDVal,
		  cache:false,
		  success: function(msg) {
			  	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+imageIDVal, 'contactdetdiv');</cfoutput>
				$('#listcreationview').fadeOut(200);
				$('#listdetailview').delay(200).fadeIn(300);
				$('div#recordsDiv').children('div').each(function () 
				{
					if($(this).attr('id') == 'record'+id)
					{
						$(this).css({'color':'black','background-color':'#dddddd'});
						$('#phoneListName').html('<b>Phone list name: </b>' + $(this).children('div').html());
					}
					else
						$(this).css({'color':'#333333','background-color':'#F8F8F8'});
				});
				setTimeout(function(){
					$("#phoneupload").trigger('click');
					$("#crenew").fadeOut(200);
					$("#addtlist").fadeIn(500);
				},100);
		  }
	});
}
function DeleteContacts(){
	var oneselected = 0;
	$( "#getaddlist2:ui-dialog" ).dialog( "destroy" );
		$( "#getaddlist2" ).html( "" );
		$(".pist").each( function() {
			if($(this).is(':checked')) {
				$( "#getaddlist2" ).dialog({
					resizable: false,
					height:100,
					modal: true,
					title: 'Are you sure you want to delete contacts?',
					hide: "explode",
					show: "fade",
					buttons: {
						"Yes Delete": function() {
							contactdel();
							$( this ).dialog( "close" );
						},
						"No Do NOT Delete": function() {
							$( this ).dialog( "close" );
						}
					}
				});
				oneselected = 1;
				return false;
			}
		})
		if(oneselected == 0){
			alert('You must select at least one contact to delete.');	
		}
}
function contactdel(){
	NFCValue.setAsyncMode();
	NFCValue.setCallbackHandler(ContactDelResult);
	NFCValue.setForm('ContactList');
	NFCValue.contactDelete();
}

function MoveContacts(id){
	var imageIDVal = id;
	var oneselected = 0;
	$( "#getaddlist3:ui-dialog" ).dialog( "destroy" );
		$( "#getaddlist3" ).html( "" );
		$(".pist").each( function() {
			if($(this).is(':checked')) {
				$( "#getaddlist3" )
				.load('dsp_contactListMove.cfm?cid='+imageIDVal)
				.dialog({
					resizable: false,
					height:100,
					modal: true,
					title: 'Move contacts to new list?',
					hide: "explode",
					show: "fade"
				});
				oneselected = 1;
				return false;
			}
		})
		if(oneselected == 0){
			alert('You must select at least one contact to move.');	
		}
}
function contactsmove(nlid){
	var nListID = nlid;
	NFCValue.setAsyncMode();
	NFCValue.setCallbackHandler(ContactMovResult);
	NFCValue.setForm('ContactList');
	NFCValue.contactMove(nListID);
}

function CopyContacts(id){
	var imageIDVal = id;
	var oneselected = 0;
	$( "#getaddlist4:ui-dialog" ).dialog( "destroy" );
		$( "#getaddlist4" ).html( "" );
		$(".pist").each( function() {
			if($(this).is(':checked')) {
				$( "#getaddlist4" )
				.load('dsp_contactListCopy.cfm?cid='+imageIDVal)
				.dialog({
					resizable: false,
					height:100,
					modal: true,
					title: 'Copy contacts to new list?',
					hide: "explode",
					show: "fade"
				});
				oneselected = 1;
				return false;
			}
		})
		if(oneselected == 0){
			alert('You must select at least one contact to copy.');	
		}
}
function contactscopy(nlid){
	var nListID = nlid;
	NFCValue.setAsyncMode();
	NFCValue.setCallbackHandler(ContactCopResult);
	NFCValue.setForm('ContactList');
	NFCValue.contactCopy(nListID);
}
function ContactDelResult(id){
	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+id, 'contactdetdiv');</cfoutput>
}

function ContactMovResult(id){
	$('#getaddlist3').dialog("close");
	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+id, 'contactdetdiv');</cfoutput>
}

function ContactCopResult(id){
	$('#getaddlist4').dialog("close");
	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+id, 'contactdetdiv');</cfoutput>
}
function fileComplete(msg){}

function fileUploadError(err){alert('There is an error while uploading the file. Please check your file and upload again.')}

function fileUploadComplete(){
	
	$('#uploadlist').fadeOut(200);
	setTimeout(function(){
				$("#uploadchoose").fadeIn(500);
				<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcolumnchoose.cfm', 'chooselistcolumn');</cfoutput>
			}, 200);
	<!---var ggb = $('#phoneListId').val();
	<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid=' + ggb, 'contactdetdiv');</cfoutput>--->
	<!---Coldfusion.Grid.refresh('phoneListDetailGrid', false);--->
}//alert(msg.STATUS);alert(msg.MESSAGE)}
function backPhoneList(){
	$('#listdetailview').fadeOut(200);
	$('#listcreationview').delay(200).fadeIn(300);
	ColdFusion.navigate('cfdiv/cfdiv_showedit.cfm', 'showeditdiv');
	ColdFusion.navigate('cfdiv/cfdiv_list.cfm', 'contactdiv');
	$("#phoneview").trigger('click');
}
function editPhoneList(id)
{	
	var imageIDVal = id;
	makepliston = 1;
	$.ajax({
		  type:"POST",
		  url:"cfc/newcontactlist.cfc?method=setCurrentList",
		  data: "GroupListId="+imageIDVal,
		  cache:false,
		  success: function(msg) {
			  	
				$('#totalinfile').html('0');
				$('#totalinfilevalid').html('0');
				$('#totalinfileduplicate').html('0');
				$('#totalinfileinvalid').html('0');			
				$('#totalinlist').html('0');
				$('#phoneListUploadResultDiv').hide();
			  	
		  		var simageX = $("#editdiv_" + imageIDVal).offset().left;
				var simageY = $("#editdiv_" + imageIDVal).offset().top;
			
				var basketX = ($("#VImageItemWrap").offset().left) + 770;
				var basketY = $("#VImageItemWrap").offset().top;
			
				var gotoX = basketX - simageX;
				var gotoY = basketY - simageY;
			
				var newImageWidth = $("#editdiv_" + imageIDVal).width() / 3;
				var newImageHeight	= $("#editdiv_" + imageIDVal).height() / 3;
				
				$('#phoneListId').val(imageIDVal);
				$('input[id=ListID]').val(imageIDVal);
				
				$("#editdiv_" + imageIDVal)
				.clone()
				.prependTo("#editdiv_" + imageIDVal)
				.css({'position' : 'absolute'})
				.animate({opacity: 1.2} )
				.animate({opacity: 0.3, marginLeft: gotoX, marginTop: gotoY}, 400,function() {
					$(this).remove().delay(400,function(){
						<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_showedit.cfm?tp=e&lid='+imageIDVal, 'showeditdiv');</cfoutput>
						<cfoutput>ColdFusion.navigate('cfdiv/cfdiv_listcontacts.cfm?tp=e&lid='+imageIDVal, 'contactdetdiv');</cfoutput>
						$('#listcreationview').fadeOut(200);
						$('#listdetailview').delay(200).fadeIn(300);
						$('div#recordsDiv').children('div').each(function () 
						{
							if($(this).attr('id') == 'record'+id)
							{
								$(this).css({'color':'black','background-color':'#dddddd'});
								$('#phoneListName').html('<b>Phone list name: </b>' + $(this).children('div').html());
							}
							else
								$(this).css({'color':'#333333','background-color':'#F8F8F8'});
						});
						setTimeout(function(){
							$("#phoneupload").trigger('click');
							$("#crenew").fadeOut(200);
							$("#addtlist").fadeIn(500);
						},100);
					});
				});
			
		  }
	  });

	
	
}

function showudv(udv){
	var udvccn = $("#column"+udv).val();
	if(udvccn == "UserDefined"){
		$("#udvname"+udv).fadeIn(200);
	}
	else if (udvccn == "Mobile Alt"){
		$("#altvnum"+udv).fadeIn(200);
	}
	else if (udvccn == "Land Alt"){
		$("#altvnum"+udv).fadeIn(200);
	}
	else if (udvccn == "Email Alt"){
		$("#altvnum"+udv).fadeIn(200);
	}
	else if (udvccn == "Fax Alt"){
		$("#altvnum"+udv).fadeIn(200);
	}
	else{
		$("#udvname"+udv).fadeOut(200);
		$("#altvnum"+udv).fadeOut(200);
	}
}
function addnewlist(){
	$('#getaddlista')
		.load('dsp_addnewlist.cfm')
		.dialog({
			height: 100,
			width:450,
			modal: true,
			title: 'Create New Distribution List',
			resizable: false,
			hide: "explode",
			show: "fade",
			
		})
}

</script>

<cfif IsDefined("form.upfile")>
	<cfinclude template="fileUpload.cfm">
</cfif>

<div style="width:820px; margin-left:10px; position:relative; display:inline-block;">
	<div id="VimageWrap" style="margin-top:0px; height:50px;">
        <div id="VImageItemWrap">
            <cfdiv id="showeditdiv" bind="url:cfdiv/cfdiv_showedit.cfm">
        </div>
    </div>
	
        <div style="position:relative; width:100%; background-color:#9C6;">
            
            <div id="uploadlist" style="float:left; margin-top:15px; display:none;" class="norm">
               
                
                <!---<cffileupload
                    extensionfilter="csv,txt" 
                    name="portfoliofiles1" 
                    maxfileselect="1" 
                    title="Upload Contact List (MUST BE COMMA DELIMITED!)" 
                    progressbar="true"
                    onerror="fileUploadError"
                    onuploadcomplete="fileUploadComplete"
                    stoponerror="true"
                    url = "fileUpload.cfm"
                    height="200"
                    width="450"
                    wmode="transparent"
                    bgcolor="##FFFFFF"
                    >--->
                	
                   <form action="" method="post" name="portfoliofiles1" enctype="multipart/form-data">
                   		<input type="file" name="upfile" />
                        <input type="submit" name="submit" value="submit" />
                   </form>
                    
            </div>
            
            <div id="ProcessListLoad" style="float:left; width:100%; text-align:center; display:none; padding:10px; font-size:14px; font-weight:bold;">
                Please wait while your list is being processed.<br />
                <img src="images/loading.gif" style="margin-top:20px;" />
            </div>
            
            <div id="uploadchoose" style="float:left; margin-top:15px; display:none;" class="norm">
                <cfdiv id="chooselistcolumn" bind="url:cfdiv/cfdiv_listcolumnchoose.cfm" bindonload="no">
            </div>
            
            <div id="uploadlistresult" style="float:left; margin-top:15px; display:none;" class="norm">
                <cfdiv id="uplresults" bind="url:cfdiv/cfdiv_listuploadresults.cfm" bindonload="no">
            </div>

    </div>
    
    <div id="listcreationview" style="width:820px; margin-left:10px; margin-top:15px; position:relative; display:inline-block;">
		<!--- Display Current Phone List Results --->
        
        <cfdiv id="contactdiv" bind="url:cfdiv/cfdiv_list.cfm">
    </div>
    <div id="listdetailview" style="width:820px; margin-left:10px; margin-top:15px; position:relative; display:none;">
        <div style="float:right;">
            <input type="button" name="back" value="< Back to Phone Lists" onclick="backPhoneList()" style="padding:3px; margin-bottom:15px;" />
        </div>
        <!--- Display Current Phone List Results --->
        <cfdiv id="contactdetdiv" bind="url:cfdiv/cfdiv_listcontacts.cfm">
    </div>
	<div id="getaddlist" style="font-size:12px;"></div>
    <div id="getaddlista" style="font-size:12px;"></div>
    <div id="getaddlistc" style="font-size:12px;"></div>
    <div id="getaddlist2" style="font-size:12px;"></div>
    <div id="getaddlist3" style="font-size:12px;"></div>
    <div id="getaddlist4" style="font-size:12px;"></div>
    <div id="getaddlist5" style="font-size:12px;"></div>
</div>