﻿<cfset MO_NOT_TRANFORM = 0>
<cfset MO_TRANFORM = 1>
<cfset SMS_MT_TYPE = 1>

<cfinclude template="../../public/paths.cfm">

<cfscript>
	try
    {
    	// get all MO that not processed
    	moMessageQueryString = "
	    	SELECT
				`moInboundQueueId_bi`,
				`ContactString_vch`,
				`CarrierId_vch`,
				`ShortCode_vch`,
				`Keyword_vch`,
				`TransactionId_vch`,
				`Time_dt`,
				`Created_dt`,
				`Status_ti`
			FROM 
			    `simplequeue`.`moinboundqueue`
			WHERE 
			    Status_ti =:Status";
			    
		moMessageQuery = new query();
		moMessageQuery.setDataSource("#DBSourceEBM#");
		moMessageQuery.setSql(moMessageQueryString);
		moMessageQuery.addparam(
				name="Status",
				value="#MO_NOT_TRANFORM#",
				CFSQLTYPE="CF_SQL_INTEGER"
			);
		moMessageResult = moMessageQuery.execute().getResult();	
		for(currRow=1; currRow LTE moMessageResult.RecordCount; currRow++){
			response = "";
			operator = moMessageResult.CarrierId_vch[currRow];
			status = moMessageResult.Status_ti[currRow];
			time = moMessageResult.Time_dt[currRow];
			shortcode = moMessageResult.ShortCode_vch[currRow];
			keyword = moMessageResult.Keyword_vch[currRow];
			moInboundQueueId = moMessageResult.moInboundQueueId_bi[currRow];
			contactString = moMessageResult.ContactString_vch[currRow];
			
			// get response information
			responseMsgQueryString = "
				SELECT 
				    sc.ShortCodeId_int,
				    sc.ShortCode_vch,
				    k.Keyword_vch,
				    k.Response_vch,
				    k.BatchId_bi,
				    sc.OwnerId_int AS UserID
				FROM 
				    sms.shortcode sc
				LEFT JOIN
				    sms.Keyword k
				ON
				    sc.ShortCodeId_int=k.ShortCodeId_int
				WHERE
				    sc.ShortCode_vch=(:ShortCode)
				
				UNION
				
				SELECT 
				    sc.ShortCodeId_int,
				    sc.ShortCode_vch,
				    k.Keyword_vch,
				    k.Response_vch,
				    k.BatchId_bi,
				    scr.RequesterId_int AS UserID
				FROM 
				    sms.shortcode sc
				LEFT JOIN
				    sms.shortCodeRequest scr
				ON
				    scr.ShortCodeId_int=sc.ShortCodeId_int
				LEFT JOIN
				    sms.Keyword k
				ON
				    scr.ShortCodeRequestId_int=k.ShortCodeRequestId_int
				WHERE
				    sc.ShortCode_vch=(:ShortCode)
			";
			
			responseMsgQuery = new query();
			responseMsgQuery.setDataSource("#DBSourceEBM#");
			responseMsgQuery.setSql(responseMsgQueryString);
			responseMsgQuery.addparam(
				name="ShortCode",
				value="#shortcode#",
				CFSQLTYPE="CF_SQL_VARCHAR"
			);
			responseMsgResult = responseMsgQuery.execute().getResult();
			
			/*TODO: Validate billing*/
			
			/*TODO: Check active campaign*/
			
			helpResponse = "";
			stopResponse = "";
			mtResponse = "";
			userId = "";
			for(currResponseRow=1; currResponseRow LTE responseMsgResult.RecordCount; currResponseRow++){
				if(responseMsgResult.Keyword_vch[currResponseRow] EQ "HELP"){
					helpResponse = responseMsgResult.Response_vch[currResponseRow];
				}
				if(responseMsgResult.Keyword_vch[currResponseRow] EQ "STOP"){
					stopResponse = responseMsgResult.Response_vch[currResponseRow];
				}	
				if(responseMsgResult.Keyword_vch[currResponseRow] EQ keyword){
					mtResponse = responseMsgResult.Response_vch[currResponseRow];
				}
				userId = responseMsgResult.UserID[currResponseRow];
			}
			
			batchId = responseMsgResult.BatchId_bi;
			if(batchId == ''){
				batchId = 0;
			}
			
			// init transform MO message object
			transformMoMsgObject = CreateObject("component","#LocalSessionDotPath#.processing.TransformMOMessage").init(
				batchId = batchId
			);
			
			/*
				Get xml control string
			*/
			OutToDBXMLBuff = transformMoMsgObject.GetMOXmlControlString(
				mtResponse = mtResponse,
				helpResponse = helpResponse,
				stopResponse = stopResponse,
				keyword = keyword,
				shortcode = shortcode
			);
			
			// update batch with new control string
			writeBatchQueryString = "
				UPDATE
                    simpleobjects.batch
                SET
                    XMLControlString_vch = (:xmlControlString)
                WHERE
                    BatchId_bi = (:BatchId)
			";
			
			writeBatchQuery = new query();
			writeBatchQuery.setDataSource("#DBSourceEBM#");
			writeBatchQuery.setSql(writeBatchQueryString);
			
			writeBatchQuery.addParam(
				name="xmlControlString",
				value="#OutToDBXMLBuff#",
				CFSQLTYPE="CF_SQL_VARCHAR"
			);
			
			writeBatchQuery.addParam(
				name="BatchId",
				value="#batchId#",
				CFSQLTYPE="CF_SQL_INTEGER"
			);
			writeBatchQuery.execute();
			
			/*mark this MO message in queue that has delivered*/
			markQueueMoMessageQueryString = "
				UPDATE 
					`simplequeue`.`moinboundqueue`
				SET
					`Status_ti` = #MO_TRANFORM#
				WHERE 
					`moInboundQueueId_bi`=(:moInboundQueueId)	
			";
			
			markQueueMoMessageQuery = new query();
			markQueueMoMessageQuery.setDataSource("#DBSourceEBM#");
			markQueueMoMessageQuery.setSql("#markQueueMoMessageQueryString#");
			markQueueMoMessageQuery.addParam(
				name="moInboundQueueId",
				value="#moInboundQueueId#",
				CFSQLTYPE="CF_SQL_INTEGER"
			);
			
			markQueueMoMessageQuery.execute(); // execute, force mark MO message as delivered
			/*ADD CONTACT RESULT*/
			cscObject = CreateObject("component","#LocalSessionDotPath#.cfc.csc.csc");
			buildXmlResultString ='
				<Response value="#mtResponse#">
					<ShortCode>#shortcode#</ShortCode>
					<Keyword>#keyword#</Keyword>
					<Operator>#operator#</Operator>
				</Response>
			';
			cscObject.AddContactResult(
				INPBATCHID= "#batchId#",
				inpShortCode="#shortcode#",
				inpResultString="#buildXmlResultString#",
				inpSMSResult=0,
				inpSMSType=SMS_MT_TYPE,
				inpDeliveryTime = time,
				inpXmlControlString = OutToDBXMLBuff,
				inpContactString = "#contactString#",
				inpMessage = keyword
			);
			
			// notify to client in chatting session 
			
			httpService = new http(); 
		    httpService.setMethod("get"); 
		    httpService.setCharset("utf-8"); 
		    httpService.setUrl("#serverSocket#:#serverSocketPort#/NotifyNewMessage/#keyword#/#userId#/#shortcode#/#contactString#"); 	
		    result = httpService.send().getPrefix(); 
			
			/* TODO: SEND MT TO QUEUE*/
			
			
		}
		
    }
    catch(Any e)
    {
    	writeDump(e);
    }

</cfscript>
