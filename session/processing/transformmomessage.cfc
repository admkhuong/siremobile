﻿<cfcomponent hint="this component is for package all method related to MO, MT message">
	<cfinclude template="../../public/paths.cfm">
	
	<cffunction name="init" access="public" returntype="TransformMOMessage" output="false">
		<cfargument name="batchId" type="string" />
		
		<cfset variables.batchId = batchId />
		<cfset variables.xmlUtil = CreateObject("component","#LocalSessionDotPath#.lib.xml.XmlUtil") />
		<cfreturn this>
	</cffunction>
	
	<cffunction name="GetMOXmlControlString" returntype="string" hint="Get information and Return xml control string to save to contact result">
		<cfargument name="mtResponse" type="string" default="" />
		<cfargument name="helpResponse" type="string" default="" />
		<cfargument name="stopResponse" type="string" default="" />
		<cfargument name="keyword" type="string" default="" />
		<cfargument name="shortcode" type="string" default="" />
	
		<cfscript>
			batchQueryString = "
				SELECT
					XMLControlString_vch,
					UserId_int
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi=(:batchId)
			";
			
			batchQuery = new query();
			batchQuery.setDataSource("#DBSourceEBM#");
			batchQuery.setSql(batchQueryString);
			batchQuery.addparam(
				name = "batchId", 
				value= variables.batchId, 
				CFSQLTYPE="CF_SQL_INTEGER");
			batchResult = batchQuery.execute().getResult();
			
			xmlUtilObject = variables.xmlUtil;
			
			// Create new xml sms element
			smsTransformXmlString = "<DM BS='0' DESC='Description Not Specified' DSUID='{%UserId%}' LIB='0' MT='4' PT='12' X='250' Y='90'>";
			smsTransformXmlString = smsTransformXmlString & "<ELE BS='0' CK1='{%MainMessage%}' CK10='0' CK11='0' CK12='0' CK13='0' CK14='0' CK2='{%HelpResponse%}' CK3='{%StopResponse%}' CK4='0' CK5='{%KeywordMO%}' CK6='0' CK7='{%ShortCode%}' CK8='0' CK9='0' CP='0' DESC='Description Not Specified' DSUID='{%UserId%}' LINK='0' QID='1' RQ='0' RXT='20' X='0' Y='0'>0</ELE>";
			smsTransformXmlString = smsTransformXmlString & "</DM>";
			
			// Replace real data
			smsTransformXmlString = replace(smsTransformXmlString, "{%UserId%}","#batchResult.UserId_int#", "all"); // user id
			smsTransformXmlString = replace(smsTransformXmlString, "{%MainMessage%}","#mtResponse#", "all"); // Main message
			smsTransformXmlString = replace(smsTransformXmlString, "{%HelpResponse%}","#helpResponse#", "all"); // Help response
			smsTransformXmlString = replace(smsTransformXmlString, "{%StopResponse%}","#stopResponse#", "all"); // Stop response
			smsTransformXmlString = replace(smsTransformXmlString, "{%KeywordMO%}","#keyword#", "all"); // Keyword
			smsTransformXmlString = replace(smsTransformXmlString, "{%ShortCode%}","#shortcode#", "all"); // Short code	
			
			xmlUpdateSmsObject = XmlParse(smsTransformXmlString);
			xmlUpdateSmsEleArr = XmlSearch(xmlUpdateSmsObject, "/DM");
			xmlUpdateSmsEle = xmlUpdateSmsEleArr[1];
			
			xmlDoc = xmlParse('<xmlDoc>' & batchResult.XMLControlString_vch & '</xmlDoc>');
			smsXmlObject = XmlSearch(xmldoc,'//DM[@MT=4]');
			// delete old sms object
			xmlUtilObject.XmlDeleteNodes(xmlDoc, smsXmlObject);
			
			// insert new sms object
			xmlDocEleArr = XmlSearch(xmldoc,"/xmlDoc");
			xmlDocEle = xmlDocEleArr[1];
			xmlUtilObject.XmlAppend(xmlDocEle, xmlUpdateSmsEle);
			
			/*save xml control string*/
			OutToDBXMLBuff = ToString(xmldoc);
			OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "");
			OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<xmlDoc>', "", "ALL");
			OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</xmlDoc>', "", "ALL");
			OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL");
			OutToDBXMLBuff = TRIM(OutToDBXMLBuff);
			
			return OutToDBXMLBuff;
        </cfscript>
		
	</cffunction>
	
	<cffunction name="GetMbloxMtMessage" access="public" returntype="string" hint="build xml that mblox need to send MT message">
		<cfargument name="accountName" type="string" default="" />
		<cfargument name="password" type="string" default="" />
		<cfargument name="batchId" type="string" default="" />
		<cfargument name="messageBody" type="string" default="" />
		<cfargument name="profile" type="string" default="" />
		<cfargument name="shortCode" type="string" default="" />
		<cfargument name="exprireDate" type="string" default="" hint="format MMDDHHmm"/>
		<cfargument name="operator" type="string" default="xxxx" hint=""/>
		<cfargument name="contactString" type="string" default="" hint="contact string"/>
		<cfargument name="serviceId" type="string" default="" hint=""/>
		
		<cfscript>
			xmlMTRequest = '
				<?xml version="1.0" ?>
				<NotificationRequest Version="3.5">
					<NotificationHeader>
					    <PartnerName>{%AccountName%}</PartnerName>
					    <PartnerPassword>{%Password%}</PartnerPassword>
					</NotificationHeader>
					<NotificationList BatchID="{%BatchId%}">
					    <Notification SequenceNumber="1" MessageType="SMS" Format="Unicode">
					        <Message>{%MessageBody%}</Message>
					        <Profile>{%Profile%}</Profile>
					        <Udh>Udh</Udh>
					        <SenderID Type="Shortcode">{%ShortCode%}</SenderID>
					        <ExpireDate>{%ExpireDate%}</ExpireDate>
					        <Operator>{%Operator%}</Operator>
					        <Tariff>x</Tariff> 
					        <Subscriber>
					            <SubscriberNumber>{%ContactString%}</SubscriberNumber>
					        </Subscriber>
					        <Tags>
					            <Tag Name="Program">xxxx</Tag>
					            <Tag Name="SidTmo">xxx</Tag>
					            <Tag Name="SidVzW">xxxx</Tag>
					        </Tags>
					        <ContentType>x</ContentType>
					        <ServiceId>{%ServiceId%}</ServiceId>
					    </Notification>
					</NotificationList>
			    </NotificationRequest>';        
			
			// replace real data
			xmlMTRequest = replace(xmlMTRequest,"{%AccountName%}","#accountName#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%Password%}","#password#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%BatchId%}","#batchId#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%MessageBody%}","#messageBody#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%Profile%}","#profile#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%ShortCode%}","#shortCode#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%ExpireDate%}","#expireDate#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%Operator%}","#operator#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%ContactString%}","#contactString#", "all");
			xmlMTRequest = replace(xmlMTRequest,"{%ServiceId%}","#serviceId#", "all");
			
			return xmlMTRequest;
        </cfscript>
	</cffunction>
	
	<cffunction name="GetMTQueueXmlControlString" access="public" returntype="string" hint="build xmlcontrolstring to save to queue">
		<cfargument name="submitMethod" type="string" default="POST" />
		<cfargument name="contentType" type="string" default="text/xml" />
		<cfargument name="webPort" type="string" default="8180" />
		<cfargument name="webDomain" type="string" default="xml3.us.mblox.com" />
		<cfargument name="webPath" type="string" default="send" />
		<cfargument name="replaceUUID" type="string" default="REPLACETHISUUID" />
		<cfargument name="ESIID" type="string" default="" />
		
		<!---parametters for mblox request--->
		<cfargument name="accountName" type="string" default="" />
		<cfargument name="password" type="string" default="" />
		<cfargument name="batchId" type="string" default="" />
		<cfargument name="messageBody" type="string" default="" />
		<cfargument name="profile" type="string" default="" />
		<cfargument name="shortCode" type="string" default="" />
		<cfargument name="exprireDate" type="string" default="" hint="format MMDDHHmm"/>
		<cfargument name="operator" type="string" default="xxxx" hint=""/>
		<cfargument name="contactString" type="string" default="" hint="contact string"/>
		<cfargument name="serviceId" type="string" default="" hint=""/>
		
		<cfscript>
        	xmlQueueControlString = "
        		<DM LIB='0' MT='1' PT='13' EMPT='Dynamic'><ELE ID='0'>0</ELE></DM>
        		<RXSS>
        			<ELE QID='1' RXT='19' BS='0' DS='0' DSE='0' DI='0' 
        				CK1='{%SubmitMethod%}' 
        				CK2='{%ContentType%}' 
        				CK4='()' 
        				CK5='-1' 
        				CK3='{%WebPort%}' 
        				CK6='{%WebDomain%}' 
        				CK7='{%WebPath%}'
        				CK8='{%XmlServiceRequest%}'
        				CK14='{%REPLACETHISUUID%}'>0</ELE>
        		</RXSS>
        		<CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='0' RMin='0' PTL='1' PTM='1' ESI='{%ESIID%}'>0</CCD>	
        	";        

        	/*get Mblox MT message*/
        	mbloxMtMessage = GetMbloxMtMessage(
        		accountName = accountName,
        		password = password,
        		batchId = batchId,
        		messageBody = messageBody,
        		profile = profile,
        		shortCode = shortCode,
        		exprireDate = exprireDate,
        		operator = operator,
        		contactString = contactString,
        		serviceId = serviceId
        	);
        	
        	/*replace real data*/
			xmlQueueControlString = replace(xmlQueueControlString, "{%SubmitMethod%}", submitMethod,"all");        	
			xmlQueueControlString = replace(xmlQueueControlString, "{%ContentType%}", contentType,"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%WebPort%}", webPort,"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%WebDomain%}", webDomain,"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%WebPath%}", xmlFormat(webPath),"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%XmlServiceRequest%}", XmlFormat(mbloxMtMessage),"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%REPLACETHISUUID%}", replaceUUID,"all");
			xmlQueueControlString = replace(xmlQueueControlString, "{%ESIID%}", "#ESIID#","all");
        	
        	return xmlQueueControlString;
        </cfscript>
	</cffunction>
	
</cfcomponent>