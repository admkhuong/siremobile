<cfparam name="inprel1" default="">
<cfparam name="inprel2" default="CSV">
<cfparam name="inpBatchIdList" default="0000">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -1, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">
<cfparam name="inpcustomdata1" default="">
<cfparam name="inpcustomdata2" default="">
<cfparam name="inpcustomdata3" default="">
<cfparam name="inpcustomdata4" default="">
    
<cfset RetVarQueryToCSV = "">
    
<cftry>
	<!--- Add log --->
    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
        <cfinvokeargument name="userId" value="#session.userid#">
        <cfinvokeargument name="moduleName" value="Report Survey">
        <cfinvokeargument name="operator" value="Export report survey #inpBatchIdList# for #inprel1#">
    </cfinvoke>
    
    <!--- Get query specified --->
    <cfinvoke method="#inprel1#" component="#Session.SessionCFCPath#.reporting" returnvariable="RetVarinprel1">
        <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#">
        <cfinvokeargument name="inpStart" value="#inpStart#">
        <cfinvokeargument name="inpEnd" value="#inpEnd#">
        <cfinvokeargument name="OutQueryResults" value="1">
        <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
        <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
        <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
        <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
    </cfinvoke>
    
   <!--- <cfdump var="#RetVarinprel1#">
    <cfabort>--->
    
    <cfinvoke method="QueryToCSV" component="#Session.SessionCFCPath#.reporting" returnvariable="RetVarQueryToCSV">
        <cfinvokeargument name="Query" value="#RetVarinprel1#">
        <cfinvokeargument name="CreateHeaderRow" value="true">
        <cfinvokeargument name="Delimiter" value=",">
    </cfinvoke>

<cfcatch type="any">

  
    <!---<cfdump var="#cfcatch#">
    <cfabort>
    --->
</cfcatch>

</cftry>
    
    <cfif LEN(TRIM(inpBatchIdList)) EQ 0>
    	<cfset inpBatchIdList = "EXPORT" />
    </cfif>
    
    <cfswitch expression="#inprel2#">
    	
        <cfcase value="CSV">
            <cfcontent type="application/msexcel">
            <cfheader 
                name="Content-Disposition" 
                value="filename=DispositionExport_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
                charset="utf-8">  
                
                <cfoutput>#RetVarQueryToCSV#</cfoutput>
                    
        </cfcase>
        
        <cfcase value="WORD">
            <cfcontent type="application/msword">
            <cfheader 
                name="Content-Disposition" 
                value="filename=DispositionExport_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.doc" 
                charset="utf-8">   
                
                <cfoutput>#RetVarQueryToCSV#</cfoutput>
                   
        </cfcase>
        
        <cfcase value="PDF">
            <cfcontent type="application/pdf">
            <cfheader 
                name="Content-Disposition" 
                value="filename=DispositionExport_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.pdf" 
                charset="utf-8">   
                
                <cfdocument format="PDF"> 
                
                	<cfoutput>#RetVarQueryToCSV#</cfoutput>
                
                </cfdocument>   
        </cfcase>
        
        <cfdefaultcase>
            <cfcontent type="application/msexcel">
            <cfheader 
                name="Content-Disposition" 
                value="filename=DispositionExport_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
                charset="utf-8"> 
                
                <cfoutput>#RetVarQueryToCSV#</cfoutput>
                
        </cfdefaultcase>
        
    </cfswitch>
    
    

