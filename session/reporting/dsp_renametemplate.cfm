<cfparam name="inpBatchIdList" default="">


 	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Save the current dashboard layout as a template for future use</h4>     
    </div>
    
	
    <div class="modal-body">
        <div>
            <label class="qe-label-a">Please name your Dashboard Template</label>
        </div>
        <div>
            <input TYPE="hidden" name="inpDashboardTemplateId" id="inpDashboardTemplateId" value="0" />
            <input type="text" name="inpDashboardTemplateDesc" id="inpDashboardTemplateDesc" value="" style="height: 20px; width: 435px"/> 
            <br>
            <label id="lblCampaignNameRequired" style="color: red; display: none;">Dashboard Template description is required</label>
        </div>
    </div>
	   
    
     <div class="modal-footer">
	     <button type="button" class="btn" data-dismiss="modal">Cancel</button>
         <button type="button" class="btn btn-default" onclick="SaveCurrentAsTemplate();">Save</button>           
     </div>
        

<script TYPE="text/javascript">

	function SaveCurrentAsTemplate()
	{		
		var inpDashboardTemplateDesc = $("#inpDashboardTemplateDesc").val();
		if ($.trim($("#inpDashboardTemplateDesc").val()) == "") {
			$('#lblCampaignNameRequired').show();
			return;
		}
		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=SaveCurrentAsTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpDesc: inpDashboardTemplateDesc,
				inpTemplateId : $("#dialog_RenameDashboardTemplate #inpDashboardTemplateId").val()
			},					  
			success:function(data)
			{				
										
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(data.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = data.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0)
						{	
							$('#SaveTemplagteModal').modal('hide');
							return false;
						}
						else
						{							
							bootbox.alert("Error Template Save Request failed!.\n"  + data.MESSAGE[0] + "\n" + data.ERRMESSAGE[0], function(result) { $('#SaveTemplagteModal').modal('hide'); } );							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						bootbox.alert("Error Template Save Request failed!.\n"  + data.MESSAGE[0] + "\n" + data.ERRMESSAGE[0], function(result) { $('#SaveTemplagteModal').modal('hide'); } );											
					}															
					
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Template Request failed!", function(result) { $('#SaveTemplagteModal').modal('hide'); } );
			} 		
		
		});			
	}
	
	
	
	$(function() {	
		
		
			
		
	});
		
</script>
