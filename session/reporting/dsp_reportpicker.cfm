

<!--- Check for Reporting Permission - if not send to home page --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">


<!--- Used to specify default Dashboard Object Sizes --->
<cfset CounterTinyClassString = "DashObj DashLeft DashBorder DashSize_Height_Tiny DashSize_Width_2 col-xs-12 col-sm-12 col-md-3" />
<cfset CounterSmallClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_2 col-xs-12 col-sm-12 col-md-3" />
<cfset ListMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />
<cfset ChartMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />

<script type="text/javascript">
	
	$(document).ready(function()
	{
		LoadDashboardTemplateList();
    					
	});
	
	function LoadDashboardTemplateList()
	{			
	
		$('#DashboardTemplates').empty();
		
		$('#DashboardTemplates').html('<li><span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></span></li>');
	
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ReadTemplateList&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:true,
			data:  { 
				
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{																				
					for(i=0; i< data.TEMPLATELIST.length; i++)
					{											
						var $NewTemplate;
					
						if(parseInt(data.TEMPLATELIST[i][2]) > 0 )	
						{
							$NewTemplate =  $('<li><a href="##" class="menuItem" data-dismiss="modal" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +   data.TEMPLATELIST[i][1] + '</a></li>').appendTo("#DashboardTemplates");
						}
						else
						{
							$NewTemplate =  $('<li><a href="##" class="menuItem" data-dismiss="modal" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +  '(sys)&nbsp;&nbsp;' +  data.TEMPLATELIST[i][1] + '</a></li>').appendTo("#DashboardTemplates");
						}
							
						$NewTemplate.children().draggable({
								revert: "invalid",
								containment: "",
								opacity: 0.7,
								helper: "clone",
								cursor: "move",
								start: function(event, ui){
									$('.droppable').css({
										'border': 'solid 2px red'
									})
									
								},
								stop: function(event, ui){
									$('.droppable').css({
										'border': 'solid 1px #999999'
									})
								}
							});
							
						
					}
					
				}
				
				
				
				$("#TemplateTrash").droppable(
				{<!--- Droppable config --->
				
					<!--- Visual feedback cues --->	
					over: function(event, ui)
					{
						$(this).css({
							'border': 'solid 2px #15436C',
							'box-shadow': '0 0px 10px #777',
							'-webkit-box-shadow': '0 0px 10px #777',
							'-moz-box-shadow': '0 0px 10px #777',
							'margin': '0px 15px 0px 0px'
						});
					},
					<!--- Visual feedback cues --->
					out: function(event, ui)
					{
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
					},
					<!--- Process when somthing is dropped here --->
					drop: function(event, ui)
					{<!--- drop event --->
						
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
						
						if(ui.draggable.attr('rel2') == "TEMPLATE")
						{	
							
							if(ui.draggable.attr('rel5') == 0)
							{								
								bootbox.alert("You can remove user defined templates by dragging them to the trash can." + "\n" + "System Templates are not removeable!", function(result) { } );	
								return false;
							}
											
							RemoveUserTemplate(ui.draggable.attr('rel4'), ui.draggable);
														
							return;	
						}
					}
										
				});
				
			
				<!--- DO this after all templates are loaded --->
				$(".menuItem" ).click(function() {
	 
					<!---console.log($(this).attr("rel1"));--->
					AddChartItem($(this));
					
					<!---$("div.droppableContentDashboard").trigger("dropthis", [{},{draggable : $(this)}]);--->
					
					
				});	
				
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				<!---
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Read Dashboard Request failed!", function(result) { } );	
				--->
			} 		
		
		});			
	}
	
	
	function RemoveUserTemplate(inpId, inpObj)
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=RemoveTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  
			{ 
				inpId: parseInt(inpId)
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{
					inpObj.remove();
				}
										
			}, 		
			error:function(jqXHR, textStatus)
			{	
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Template Request failed!", function(result) { } );	
			} 		
		
		});			
	}
	
	
</script>
    

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Select the report you wish to add to the dashboard</h4>     
</div>


<div class="no-print">
       
        <cfoutput>    
           	          
                <ul id="ReportMenu">
                
                 <li class="content-EM"S>
                            <h3 class="head-EMS"><a href="##">Combo Results</a></h3>
                        
                            <ul>
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="ComboResults" rel2="CHART">
                                        Combo Results
                                    </a>
                                </li>
                                </cfif>
                                
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_BatchSummary" rel2="TABLE">
                                        Campaign Summary
                                    </a>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_results" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Results Counter
                                    </a>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_queue" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - In Queue
                                    </a>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_queue_total" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - Processed
                                    </a>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_QueueSummary" rel2="TABLE" rel3="#ListMediumClassString#">
                                        Queue List
                                    </a>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_ResultSummary" rel2="TABLE" rel3="#ListMediumClassString#">
                                        Results List
                                    </a>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_CallResultsPerHour_Title).havePermission>
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="ComboResultsByHour" rel2="CHART"> 
                                        Combo Results per Hour
                                    </a>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_DialHistoryLookup_Title).havePermission>
                                 <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_ContactStringDialHistory" rel2="FORM" title="List Call Results for Contact String for Date Range">
                                        Contact History Lookup
                                    </a>
                                </li>
                                </cfif>                            
                                
                            </ul>
                   </li>    
                                                
                     <cfif permissionObject.havePermission(Reporting_CallResults_Title).havePermission>  
                    <li>
                        <h3><a href="##">Call Results</a></h3>
                    
                        <ul>
                                                   
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTime_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallAvgCallLength" rel2="CHART">
                                    Avg Call Time
                                </a>
                            </li>
                            </cfif>
                           
                                                     
                            <cfif permissionObject.havePermission(Reporting_LiveCallsPerHour_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallLivePerHour" rel2="CHART"> 
                                    Live Calls per Hour
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_MachineCallsPerHour_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallMachinePerHour" rel2="CHART"> 
                                    Machine Calls per Hour
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTimeBy_CallResult_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallAvgLengthByResult" rel2="CHART">
                                    Avg Call Time By Call Result
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_FinalCallResults_Basic_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_FinalDialResults" rel2="TABLE">
                                    Final Call Results Basic
                                </a>
                            </li>  
                            </cfif>
                           
                            
                        
                        </ul>
                        
                    </li>    
                    </cfif>
                       
                    <cfif permissionObject.havePermission(Reporting_States_Title).havePermission>                
                    <li>
                        <h3><a href="##">Location</a></h3>
                    
                        <ul>
                            <cfif permissionObject.havePermission(Reporting_CallResults_ByState_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallResultsByState" rel2="CHART">
                                    Call Results by State
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTime_ByState_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CallAvgCallLengthByState" rel2="CHART">
                                    Avg Call Time By State
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_TotalCalls_ByState_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="TotalCallsByState" rel2="CHART">
                                    Total Calls By State
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_TotalCalls_ByState_Title).havePermission>
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="MTCountByState" rel2="CHART" title="SMS MT Count by State">
                                        SMS MT Counts By State
                                    </a>
                                </li>
                               
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="MTCountByZip" rel2="CHART" title="SMS MT Count by Top 125 Zip codes">
                                        SMS MT Counts By Zip Code
                                    </a>
                                </li>
                               
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="MTCountByMSA" rel2="CHART" title="SMS MT Count by The Metropolitan Statistical Area (MSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                        SMS MT Counts By MSA
                                    </a>
                                </li>
                           
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="MTCountByPMSA" rel2="CHART" title="SMS MT Count by The Primary Metropolitan Statistical Area (PMSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                        SMS MT Counts By PMSA
                                    </a>
                                </li>
                               
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="MTCountByFIPS" rel2="CHART" title="SMS MT Count by The FIPS (Federal Information Processing Standard) number assigned to each county in the U.S. by the Census Bureau. The first two digits are the state number, and the last three digits are the county number.">
                                        SMS MT Counts By FIPS
                                    </a>
                                </li>
                            </cfif>                            
                             
                        </ul>
                    
                    </li>
                    </cfif>
                         
                     
                    <cfif permissionObject.havePermission(Reporting_SMS_Title).havePermission>    
                    <li>
                        <h3><a href="##">SMS</a></h3>
                    
                        <ul>			
                        
                        	<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsmt" rel2="FORM" rel3="#CounterTinyClassString#">
                                    SMS MT Counter
                                </a>
                            </li>
                                
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsmo" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS MO Counter
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSSurveyComboResults" rel2="CHART" rel3="#ChartMediumClassString#" title="Status of Surveys Started in Date Range Specified">
                                    SMS Survey Status
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsSurvey" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS Surveys Requested
                                </a>
                            </li>
                            
                            <cfif permissionObject.havePermission(Reporting_optin_Title).havePermission>   
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_optin" rel2="FORM" title="number of users optin campaigns.">
	                                    SMS Optin
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_optout_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_optout" rel2="FORM" title="number of users optout campaigns.">
	                                    SMS Optout
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Activeusers_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_activeusers" rel2="FORM" title="number of active users.">
	                                    Active Users
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Campaigndropout_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_dropout" rel2="FORM" title="number of drop out users.">
	                                    Campaign Dropouts 
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Userinformation_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information 
	                                </a>
	                            </li>
	                            
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="legacy_Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information (Legacy)
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Userhistory_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cshistory" rel2="FORM" title="History of Contact String">
	                                    User History
	                                </a>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Comparequestionresponses_Title).havePermission>
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cpcompare" rel2="FORM" title="Compare control points in keyword">
	                                    Compare question responses 
	                                </a>
	                            </li>
                            </cfif>
                            
                        	<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_chartMO" rel2="FORM" title="Time chart MO">
                                    MO Charts 
                                </a>
                            </li>
                            
                            <cfif permissionObject.havePermission(Reporting_SMSSessionstate_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSSessionStates" rel2="CHART">
                                        SMS Session States
                                    </a>
                                </li>
                           </cfif>
                        <!---    <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel2="CHART">
                                    SMS Delivered
                                </a>
                            </li>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel2="CHART">
                                    SMS Undelivered
                                </a>
                            </li>--->
                            
                         <!---   <cfif permissionObject.havePermission(Reporting_ActiveICBy_Keyword_Title).havePermission>
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="ICByKeyword" rel2="CHART" title="Active Interactive Campaign (IC) by Keyword">
                                    Active IC By Keyword
                                </a>
                            </li>
                            </cfif>
						
                    
                             <cfif permissionObject.havePermission(Reporting_ActiveIC_Details_Title).havePermission>
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_ICByKeywordDetails" rel2="TABLE" title="Active Interactive Campaign (IC) Details">
                                    Active IC Details
                                </a>
                            </li>
                            </cfif>
                    
                            <cfif permissionObject.havePermission(Reporting_ActiveIC_Counts_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="ICActivated" rel2="CHART" title="Active Interactive Campaign (IC) for Date Range">
                                    Actived IC Counts
                                </a>
                            </li>
                            </cfif>
                            
							--->
                            
                            
                            <cfif permissionObject.havePermission(Reporting_MTDelivery_Results_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_ContactStringMTStatusUpdates" rel2="FORM" title="MT Delivery Results for Contact String for Date Range">
                                    MT Delivery Results
                                </a>
                            </li>
                            </cfif>
                    		
                    		<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_campaigndetails" rel2="FORM" >
                                  Keyword Response Details
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_CSdetails" rel2="FORM" >
                                  Contact String Details
                                </a>
                            </li>
                            
                          <!---  <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_moresponses" rel2="FORM" title="MO Keyword Response Counts by Short Code">
                                    MO Responses
                                </a>
                            </li>
                            </cfif>
							--->
                            
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cpresponses" rel2="FORM" title="MO IC Response Counts by Control Point">
                                    CP Responses
                                </a>
                            </li>
                            </cfif>
                            
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="display_smsfinaldisposition" rel2="TABLE" title="All user history for a batch">
                                    SMS Final Disposition
                                </a>
                            </li>
                            
                            
                        <!---  These are commented out until they are working within format and indexed properly   
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CompareKeywords" rel2="FORM" title="User's Response">
                                    Compare Keywords
                                </a>
                            </li>
                            </cfif>
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_getRegUsers" rel2="FORM" title="User's Response">
                                    Registered Users
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_filteredresponses" rel2="FORM" title="Survey Results Filtered">
                                    Survey Status on Campaign Filter
                                </a>
                            </li>
                            </cfif>
							
						--->
                                                    
                            
                        </ul>
                    
                    </li>
                    </cfif>
                        
                    
                    <cfif permissionObject.havePermission(Reporting_Email_Title).havePermission> 
                    <li>
                        <h3><a href="##">eMail</a></h3>
                    
                        <ul>
                           
                            <cfif permissionObject.havePermission(Reporting_Email_Pie_Chart_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "EmailResults" rel2="CHART" >
                                    Events Percentage Distribution                            
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Events_Bar_Graph_Title).havePermission>
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "GraphResult" rel2="CHART" >
                                    Events Bar Graph
                                </a>
                            </li> 
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_Events_Results_Table_Title).havePermission>
                             <!--- <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Display_EmailLog" rel2="TABLE" >
                                    Events Results Table
                                </a>
                            </li>  --->
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_FinalEmailResultsBasic" rel2="TABLE">
                                    Final Email Results 
                                </a>
                            </li>  
                            </cfif>   
                            
                            <!--- <cfif permissionObject.havePermission(Reporting_Email_History_Lookup_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_EmailStringHistory" rel2="FORM" title="List Email Results for specific Date Range">
                                    Email History Lookup
                                </a>
                            </li> 
                            </cfif>  --->  
                            
                            <cfif permissionObject.havePermission(Reporting_Opens_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_opens" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Opens
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Delivered_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_delievered" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Delivered
                                 </a>
                            </li>
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_Spamreport_Title).havePermission>
                             <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_spamreport" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Spamreport
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Bounces_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_bounce" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Bounces
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Unsubscribes_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_unsubscribe" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Unsubscribes
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Deferred_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_deferred" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Deferred
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Dropped_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_dropped" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Dropped
                                 </a>
                            </li>
                            </cfif>
                            
                       </ul>
                    
                    </li>
                    </cfif> 
                   
                   
                   <!---	<cfif 1 EQ 1>
                    <li>
                        <h3><a href="##">Group (Contact List) Reports</a></h3>
                        
                        
                         <ul>
	                        <cfif 1 EQ 1>
                             <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsmo_noq" rel2="FORM" rel3="#CounterTinyClassString#" title="Distribution of CDF Choice">
                                    CDF Distribution Counts
                                </a>
                            </li> 
                            
                            </cfif>  
                            
                         </ul>   
                        
                    </li>       
                    
                    </cfif>--->
                                                       
                    <!--- System Reports--->
                    <cfif permissionObject.havePermission(Reporting_System_Group_Title).havePermission> 
                    <li>
                        <h3><a href="##">System Reports</a></h3>
                    
                        <ul>
	                        <cfif permissionObject.havePermission(Reporting_System_MO).havePermission>
                             <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsmo_noq" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's for billing special - No Queue counts">
                                    SMS MO No Q Counter
                                </a>
                            </li> 
                            
                            </cfif>  
                            
                            
                           	<cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_results" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Results Counter
                                    </a>
                                </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_queue_system" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - System In Queue
                                    </a>
                                </li>
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_queue_system_total" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - System Processed
                                    </a>
                                </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_System_MO).havePermission>
                             <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsmo_sys" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's regardless of user or campaign">
                                    SMS MO Counter
                                </a>
                            </li> 
                            
                            </cfif>  
                            
                            <cfif permissionObject.havePermission(Reporting_System_MT).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_EmailStringHistory" rel2="FORM" title="List Email Results for Email Address for Date Range">
                                    MT Summary
                                </a>
                            </li> 
                            </cfif>   
                            
                            <cfif permissionObject.havePermission(Reporting_SMSMO_Log_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_MOLog" rel2="TABLE">
                                    SMS MO Log
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_System_Avg_Queue_Processing_Time).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Display_rs_c_avgqueueproctime" rel2="FORM" rel3="#CounterTinyClassString#" title="Get Avg Porcessing Time to Insert to Queue">
                                    AVG Process Time - Insert Queue
                                </a>
                            </li> 
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Opens_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_opensGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Opens
                                 </a>
                            </li>
                            </cfif>
                            
                            
                            <cfif permissionObject.havePermission(Reporting_Delivered_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_delieveredGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Delivered 
                                 </a>
                            </li>
                            </cfif>
                            
                            
                            
                            <cfif permissionObject.havePermission(Reporting_Spamreport_Overall_Title).havePermission>
                             <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_spamreportGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Spamreport 
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Bounces_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_bounceGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Bounces 
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Unsubscribes_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_unsubscribeGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Unsubscribes 
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Deferred_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_deferredGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Deferred 
                                 </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Dropped_Overall_Title).havePermission>
                            <li>
                                 <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_droppedGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Dropped 
                                 </a>
                            </li>  
                            </cfif>
                             <cfif permissionObject.havePermission(Reporting_Client_Stats_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "ClientStats" rel2="CHART" >
                                    Clients Stats
                                </a>
                            </li> 
                            </cfif>  
                             <cfif permissionObject.havePermission(Reporting_Unique_Client_Stats_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "UniqueClientsStats" rel2="CHART" >
                                   Unique Clients Stats
                                </a>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_ClientStats_Piechart_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "ClientsStatsPieChart" rel2="CHART" >
                                   Clients Stats Pie Chart
                                </a>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Open_Devices_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "DeviceStats" rel2="CHART" >
                                  Open Device Types
                                </a>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Unique_Devices_Title).havePermission> 
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "UniqueDeviceStats" rel2="CHART" >
                                  Unique Device Types
                                </a>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Global_Stats_Title).havePermission> 
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "GlobalStats" rel2="CHART" >
                                  Advanced Global Stats
                                </a>
                            </li>
                            </cfif>  
                            
                            <cfif permissionObject.havePermission(Reporting_ISPS_Results_Title).havePermission> 
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "ISPStats" rel2="CHART" >
                                  ISPS Results
                                </a>
                            </li>
                            </cfif>  
                                        
                            <!--- <cfif permissionObject.havePermission(Reporting_ISPS_Results_Title).havePermission> --->
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "rs_avgsmsposttime" rel2="CHART" >
                                  Avg SMS Post Time
                                </a>
                            </li> 
                           <!---  </cfif>  --->    
                           
                           <cfif permissionObject.havePermission(Reporting_SMPPError_Title).havePermission>      
                           <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_smssmpperror" rel2="FORM" title="SMPP SMS Error">
                                    SMPP-SMS Error 
                                </a>
                            </li> 
                            </cfif>            
                                                         
                        </ul>
                    
                    </li>
                    </cfif> 
                    
                    
                    <!--- Templates --->
                    <cfif permissionObject.havePermission(Reporting_Dashboard_Template_Group_Title).havePermission> 
                    <li>
                        <h3><a href="##">Dashboard Templates</a></h3>
                   		
                        
                        <ul id="DashboardTemplates">
                       
                       		<li><span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;"><img src="#rootUrl#/#publicPath#/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></a></li>
                                                                                    
                        </ul>
                    
                    </li>
                    </cfif> 
                    
                    
                   <!--- <cfif permissionObject.havePermission(Reporting_Test_Title).havePermission>                     
                         <li>
                            <h3><a href="##">Special</a></h3>
                        
                            <ul>
                            
                         
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSKeywordSummary" rel2="CHART" >
                                        TestNew
                                    </a>
                                </li>
                            </ul>
                            
                        </li>        
                    </cfif>--->
                    
                </ul>
           
        </cfoutput>
         
             
        </div>     
        
        
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>           
        </div>
                
                