<cfinclude template="../sire/configs/paths.cfm">

<cfparam name="INPBATCHID" default="0">

<!--- Presumes dialog is opened assigned to a javascript var of CreateXMLContorlStringMasterTemplateDialogVoiceTools--->


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	
	#MC_Stage_XMLControlString_Master_Template .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}
	
	
	#MC_Stage_XMLControlString_Master_Template .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;   
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 100%;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
		position:absolute;
		top: -25px;
		right: 37px;
	}
	
	#MC_Stage_XMLControlString_Master_Template .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#MC_Stage_XMLControlString_Master_Template .form-right-portal {
    z-index: 1000;
}

.dd-option-text,.dd-selected-text{
	line-height: 30px !important;
}
.dd-option-image, .dd-selected-image{
	max-height: 50px;
}

</style>
<script type="text/javascript">


	$(function() 
	{		
							

		$("#inpSubmit").click(function() { 


			try{
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateRecurringLog&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:$('#form-create-template').serialize(),
		            dataType: "json", 
		            success: function(d) {
		            	
         				if(d.RXRESULTCODE == 1){
							
							bootbox.alert('Save Log Success', function() { 
								$('#RecurringLogModal').modal('hide');
								window.location.href = '/session/reporting/recurring_report'
							});
						}
						else
						{
							<!--- No result returned --->							
							bootbox.alert(d.MESSAGE);
						}
		         	}
		   		});
			}catch(ex){
				bootbox.alert('Update Fail', function() {});
			}
		});

	});



	

</script>	


<cfoutput>

<!--- GET MAX ORDER IN ORDER --->

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Update Status Log</h4>        
</div>


<cfquery name="getLog" datasource="#Session.DBSourceEBM#">
					SELECT 
	        			l.*,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
</cfquery>

<div class="modal-body">


    <div id="MC_Stage_XMLControlString_Master_Template" class="EBMDialog">
                          
        <form method="POST" id="form-create-template">
                                      
            <div class="inner-txt-box">
   
            
                <div class="form-left-portal">
            
                    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
	
					<label for="INPSTATUS">Status</label>
                	<select id="" name="INPSTATUS">
                		<option <cfif getLog.Status_ti EQ 0>selected</cfif> value="0">Open</option>
                		<option <cfif getLog.Status_ti EQ -1>selected</cfif> value="-1">In Progess</option>
                		<option <cfif getLog.Status_ti EQ -2>selected</cfif> value="-2">Tech Review</option>
                		<option <cfif getLog.Status_ti EQ -3>selected</cfif> value="-3">QA Review</option>
                		<option <cfif getLog.Status_ti EQ 1>selected</cfif> value="1">Close</option>
				    </select>

				                                          
                </div>                            
                                       
          
          
            </div>
                       
        </form>
           
    </div>     
    
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
     
	<button type="button" class="btn btn-primary" id="inpSubmit">Save</button>
   
</div>	

    
</cfoutput>
