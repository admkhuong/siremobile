<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_Campaign_Error_Title#">
</cfinvoke>

<script>
	$().ready(function ()
	{
		
		$('#subTitleText').text('<cfoutput>#Reporting_Campaigns_Text# >> #Reporting_Error_Text#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
		
	    document.title = "Campaign Errors Reports";
	});
	
</script>