<cfparam name="UUID" default="-1">
<cfparam name="INPSIGNEDDATEFROM" default="">
<cfparam name="INPSIGNEDDATETO" default="">
<cfparam name="isIncLang" default="1"> <!-----Check cpp include language or not------->
<cfparam name="isIncIdentity" default="1">

<cfset INPCPPUUID = UUID />
<cfsetting showdebugoutput="false" />


<!--- Import the POI tag library. --->
<cfimport taglib="../lib/excel/poi" prefix="poi" />	

<cfinvoke 
	      component="#SessionPath#.cfc.reporting"
	      method="GetCppContacts"
	      returnvariable="dbResult">     
    <cfinvokeargument name="page" value="1"/>  
   	<cfinvokeargument name="INPCPPUUID" value="#INPCPPUUID#">
	<cfinvokeargument name="INPSIGNEDDATEFROM" value="#INPSIGNEDDATEFROM#">
	<cfinvokeargument name="INPSIGNEDDATETO" value="#INPSIGNEDDATETO#">
   <cfinvokeargument name="inpIsExport" value="true">
 </cfinvoke>

<!--- 
	Create an excel document and store binary data into 
	REQUEST variable. 
--->
<poi:document 
	name="REQUEST.ExcelData"
	style="font-size: 10pt ; color: black ; white-space: nowrap;">
	
	<!--- Define style classes. --->
	<poi:classes>
		<poi:class
			name="title"
			style="font-size: 18pt ; text-align: left ;"
			/>
		
		<poi:class 
			name="header" 
			style="font-size: 12pt ;  border-top: 2px solid white ;" 
			/>
			
	</poi:classes>
		
	<!--- Define Sheets. --->
	<poi:sheets>
	
		<poi:sheet 
			name="Cpp's contact"
			freezerow="2"
			orientation="landscape"
			zoom="100%">
		
			<!--- Define global column styles. --->
			<poi:columns>
				<poi:column style="width: 150px; text-align: left;" />
				<poi:column style="width: 150px;" />
				<poi:column style="width: 130px;" />
				<poi:column style="width: 200px; text-align: center;" />
				<cfif isIncLang EQ 1>
					<poi:column style="width: 200px; text-align: center;" />
				</cfif>
				<cfif isIncIdentity EQ 1>
					<poi:column style="width: 200px; text-align: center;" />
				</cfif>
			</poi:columns>
			
			<!--- Title row. --->
			<poi:row class="title">
				<poi:cell value="Customer Preference Portal Contacts" colspan="4" />
			</poi:row>
			
			<!--- Header row. --->
			<poi:row class="header">
				<poi:cell value="Added" />
				<poi:cell value="Contact Type" />
				<poi:cell value="Contact Detail" />
				<poi:cell value="Preferences" />
				<cfif isIncIdentity EQ 1>
					<poi:cell value="Identity" />
				</cfif>
				<cfif isIncLang EQ 1>
					<poi:cell value="Language" />
				</cfif>
				
			</poi:row>
			
			<!--- Output the people. --->
			<cfloop array="#dbResult.ROWS#" index="index">
			
				<poi:row>
					<poi:cell value="#index.added#" />
					<poi:cell value="#index.contact_type#" />
					<poi:cell value="#index.contact_detail#" />
					<poi:cell value="#index.contact_preference#" />
					<cfif isIncIdentity EQ 1>
						<poi:cell value="#index.identity#" />
					</cfif>
					<cfif isIncLang EQ 1>
						<poi:cell value="#index.language#" />
					</cfif>
				</poi:row>
			
			</cfloop>
			
		</poi:sheet>
		
	</poi:sheets>
		
</poi:document>

<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Customer Preference Portal">
	<cfinvokeargument name="operator" value="Export Excel cppuuid=#UUID#">
</cfinvoke>

<!--- Tell the browser to expect an Excel file attachment. --->
<cfheader
	name="content-disposition"
	value="filename=CPP_Contacts_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.xls"
	/>
	
<!--- Stream the binary data to the user. --->
<cfcontent
	type="application/excel"
	variable="#REQUEST.ExcelData.ToByteArray()#"
	/>
