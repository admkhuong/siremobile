<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_CPP_Errors_Title#">
</cfinvoke>

<script type="text/javascript">
	
	$('#subTitleText').text('<cfoutput>#Reporting_CPP_Text# >> #Reporting_Error_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');

	$().ready(function ()
	{
	    document.title = "CPP Errors Reports";
	});
	
</script>