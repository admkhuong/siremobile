<cfparam name="UUID" default="-1">
<cfparam name="page" default="1">
<cfparam name="INPSIGNEDDATEFROM" default="">
<cfparam name="INPSIGNEDDATETO" default="">
<cfparam name="isIncLang" default="1"> <!-----Check cpp include language or not------->
<cfparam name="isIncIdentity" default="1">

<cfsetting enablecfoutputonly="yes"> <!--- Required for CSV export to function properly --->

<CFIF FindNoCase('Firefox','#CGI.HTTP_USER_AGENT#') GREATER THAN 0>
	<cfset delim = 9> <!--- Use a comma for a field delimitter, Excel will open CSV files --->
<cfelse>
	<cfset delim = 44>
</CFIF>
<!--- check permission --->
<cfinvoke method="checkCppPermissionByCppId" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="checkCppPermissionByCppId">
	<cfinvokeargument name="CPPID" value="#UUID#">
	<cfinvokeargument name="operator" value="#Reporting_CPP_Detail_Title#">
</cfinvoke>

<cfif NOT checkCppPermissionByCppId.havePermission>
	<cfset session.permissionError = cppPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Customer Preference Portal">
	<cfinvokeargument name="operator" value="Export CSV cppuuid=#UUID#">
</cfinvoke>
			
<cfinvoke component="#SessionPath#.cfc.reporting" method="GetCppContacts"  returnvariable="GetCppContacts">
	<cfinvokeargument name="inpCPPUUID" value="#UUID#">
	<cfinvokeargument name="INPSIGNEDDATEFROM" value="#INPSIGNEDDATEFROM#">
	<cfinvokeargument name="INPSIGNEDDATETO" value="#INPSIGNEDDATETO#">
	<cfinvokeargument name="inpIsExport" value="true">
	<cfinvokeargument name="page" value="#page#">
</cfinvoke>

<cfcontent type="application/msexcel">
<cfheader 
	name="Content-Disposition" 
	value="filename=ReportOfCPP_#UUID#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
	charset="utf-8">

<cfoutput>Added #chr(delim)# Contact Type #chr(delim)# Contact Detail #chr(delim)# Preferences #chr(delim)# <cfif isIncIdentity EQ 1>Identity #chr(delim)# </cfif> <cfif isIncLang EQ 1> Language </cfif></cfoutput>

<cfoutput>#chr(13)#</cfoutput> <!--- line break after column header --->
 
<cfloop array="#GetCppContacts.rows#" index="exportRow">
	<cfoutput>
		#exportRow.added# #chr(delim)#
		#exportRow.contact_type# #chr(delim)#
		#exportRow.contact_detail# #chr(delim)#
		#exportRow.contact_preference# #chr(delim)#
		<cfif isIncIdentity EQ 1>
			#exportRow.identity# #chr(delim)#
		</cfif>
		<cfif isIncLang EQ 1>
			#exportRow.language# 
		</cfif>
		#chr(13)#
	</cfoutput>
</cfloop>