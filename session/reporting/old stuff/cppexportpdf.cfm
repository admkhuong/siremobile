<cfparam name="UUID" default="-1">
<cfset INPCPPUUID = UUID />
<cfparam name="INPSIGNEDDATEFROM" default="">
<cfparam name="INPSIGNEDDATETO" default="">
<cfparam name="isIncIdentity" default="1">
<cfparam name="isIncLang" default="1"> <!-----Check cpp include language or not------->


<!--- Import the POI tag library. --->
<cfimport taglib="../lib/excel/poi" prefix="poi" />	

<cfinvoke 
       component="#SessionPath#.cfc.reporting"
       method="GetCppContacts"
       returnvariable="dbResult">     
	     <cfinvokeargument name="page" value="1"/>  
	   <cfinvokeargument name="INPCPPUUID" value="#INPCPPUUID#">
	   <cfinvokeargument name="inpIsExport" value="true">
	   <cfinvokeargument name="INPSIGNEDDATEFROM" value="#INPSIGNEDDATEFROM#">
	   <cfinvokeargument name="INPSIGNEDDATETO" value="#INPSIGNEDDATETO#">
  </cfinvoke>
<!--- Export cpp contact content to pdf --->
<cfscript>
	totalColumn = 4;
	if(isIncIdentity){
		totalColumn = totalColumn + 1;
	}
	
	if(isIncLang){
		totalColumn = totalColumn + 1;
	}
	// create document
	document = CreateObject("java", "com.lowagie.text.Document");
	document.init();
	
	//create directory if not exist 
	if(!DirectoryExists(CppPdfExportWritePath & '/u' & Session.UserId )){
		DirectoryCreate(CppPdfExportWritePath  & '/u' & Session.UserId );
	}
	// writer
	fileIO = CreateObject("java", "java.io.FileOutputStream");
	fileIO.init(CppPdfExportWritePath  & '/u' & Session.UserId & '/ExportPdf.pdf');
	writer = CreateObject("java", "com.lowagie.text.pdf.PdfWriter");
	writer.getInstance(document, fileIO);
	document.open();

	table = CreateObject("java","com.lowagie.text.pdf.PdfPTable").init(totalColumn);
	table.getDefaultCell().setBorderWidth(0.1);
	table.getDefaultCell().setPadding(5.0);
	
	// Create java int array :( 
	arrObj = createobject("java", "java.lang.reflect.Array");
	jClass = createobject("java", "java.lang.Integer").TYPE;
	jArr = arrObj.newInstance(jClass, totalColumn);
	
	arrObj.setInt(jArr, 0, 18);
	arrObj.setInt(jArr, 1, 14);
	arrObj.setInt(jArr, 2, 25);
	arrObj.setInt(jArr, 3, 25);
	if(isIncIdentity){
		arrObj.setInt(jArr, 4, 30);
		if(isIncLang){
			arrObj.setInt(jArr, 5, 16);
		}
	}else{
		if(isIncLang){
			arrObj.setInt(jArr, 4, 16);
		}
	}
	
	
	
	table.setWidths(jArr);
	Font = createObject("java", "com.lowagie.text.Font");
	
	FONT_BOLD = Font.init(Font.TIMES_ROMAN, 10.0, Font.BOLD);
	FONT_NORMAL = Font.init(Font.TIMES_ROMAN, 9.0);
	
	phrase = CreateObject("java", "com.lowagie.text.Phrase");
	phrase.init("Added",FONT_BOLD);
	cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
	cell.setPadding(3.0);
	cell.setVerticalAlignment(5);
	cell.addElement(phrase);
	table.addCell(cell);
	
	phrase = CreateObject("java", "com.lowagie.text.Phrase");
	phrase.init("Contact Type",FONT_BOLD);
	cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
	cell.setPadding(3.0);
	cell.setVerticalAlignment(5);
	cell.addElement(phrase);
	table.addCell(cell);
	
	phrase = CreateObject("java", "com.lowagie.text.Phrase");
	phrase.init("Contact Detail",FONT_BOLD);
	cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
	cell.setPadding(3.0);
	cell.setVerticalAlignment(5);
	cell.addElement(phrase);
	table.addCell(cell);
	
	phrase = CreateObject("java", "com.lowagie.text.Phrase");
	phrase.init("Preferences",FONT_BOLD);
	cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
	cell.setPadding(3.0);
	cell.setVerticalAlignment(5);
	cell.addElement(phrase);
	table.addCell(cell);
	
	if(isIncIdentity EQ 1){
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init("Identity",FONT_BOLD);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.setVerticalAlignment(5);
		cell.addElement(phrase);
		table.addCell(cell);
	}
	
	if(isIncLang EQ 1){
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init("Language",FONT_BOLD);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.setVerticalAlignment(5);
		cell.addElement(phrase);
		table.addCell(cell);
	}
	
	for(i=1; i <= arraylen(dbResult.ROWS); i++){
		
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init(dbResult.ROWS[i]["ADDED"],FONT_NORMAL);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.addElement(phrase);
		table.addCell(cell);
		
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init(dbResult.ROWS[i]["CONTACT_TYPE"],FONT_NORMAL);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.addElement(phrase);
		table.addCell(cell);
		
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init(dbResult.ROWS[i]["CONTACT_DETAIL"],FONT_NORMAL);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.addElement(phrase);
		table.addCell(cell);
		
		phrase = CreateObject("java", "com.lowagie.text.Phrase");
		phrase.init(dbResult.ROWS[i]["CONTACT_PREFERENCE"],FONT_NORMAL);
		cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
		cell.setPadding(3.0);
		cell.addElement(phrase);
		table.addCell(cell);
		
		if(isIncIdentity EQ 1){
			phrase = CreateObject("java", "com.lowagie.text.Phrase");
			phrase.init(dbResult.ROWS[i]["IDENTITY"],FONT_NORMAL);
			cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
			cell.setPadding(3.0);
			cell.addElement(phrase);
			table.addCell(cell);
		}
		
		if(isIncLang EQ 1){
			phrase = CreateObject("java", "com.lowagie.text.Phrase");
			phrase.init(dbResult.ROWS[i]["LANGUAGE"],FONT_NORMAL);
			cell = CreateObject("java","com.lowagie.text.pdf.PdfPCell");
			cell.setPadding(3.0);
			cell.addElement(phrase);
			table.addCell(cell);
		}	
	}
	
	// Code 5
	document.add(table);
	
	document.close();
	fileIO.close();
</cfscript>

<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Customer Preference Portal">
	<cfinvokeargument name="operator" value="Export PDF cppuuid=#UUID#">
</cfinvoke>

<!--- Get file pdf and return to user --->
<cffile action="READBINARY" file="#CppPdfExportWritePath#/u#Session.UserId#/ExportPdf.pdf" variable="objPdf" />

<cfheader name="content-length" value="#ArrayLen( objPdf )#"/>
<cfheader name="Content-Disposition" value="inline; filename=CPP_Contacts_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.pdf">
<cfcontent type="application/pdf" variable="#objPdf#" />
