<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_CPP_Title#">
</cfinvoke>

<cfif NOT cppPermission.havePermission>
	<cfset session.permissionError = cppPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfimport taglib="../lib/grid" prefix="mb" />
 <cfoutput>
	<cfscript>
		htmlOption = '<a href="cPPReportDetails?UUID={%CPP_UUID_vch%}"><img title="Details" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks campaign_detail_report_16_16">';
		htmlOption = htmlOption & '<a href="cPPResponses?UUID={%CPP_UUID_vch%}"><img title="Responses" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks response_16_16">';
		htmlOption = htmlOption & '<a href="cPPErrors?UUID={%CPP_UUID_vch%}"><img title="Errors"  src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks campaign_error_report_16_16">';
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>
	<!--- Prepare column data---->
	<cfset arrNames = ['ID', 'Description', 'Created', 'Actions']>	
	<cfset arrColModel = [			
			{name='CPP_UUID_vch', width='10%'},
			{name='Desc_vch', width='50%'},	
			{name='Created_dt', width='25%'},
			{name='FORMAT', width='15%', format = [htmlFormat]}] >		
 	<mb:table 
		component="#SessionPath#.cfc.ire.marketingCPP" 
		method="GetCPPListReport" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		FilterSetId="DefaulFSListCpp"
	>
	</mb:table>
</cfoutput>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Reporting_CPP_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
</script>

<!--- <cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">
<a href="campaigns"></a>
<cfset param = '&_search=#_search#&nd=#nd#&rows=#rows#&page=#page#&sidx=#sidx#&sord=#sord#'>
<cfif _search EQ "">
	<cfset param = ''>
</cfif>

<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/campaign.css');
	</style>
</cfoutput>
<script>   
	var timeoutHndBatchesCPPList;
	var fAutoBatchesCPPList = true;
	var LastSelBatchesCPPList;
	
	$(function() {
		<cfoutput>
		<cfif _search NEQ "">
			param = '&_search=#_search#&nd=#nd#&rows=#rows#&page=#page#&sidx=#sidx#&sord=#sord#';
		<cfelse>
			param = '';	
		</cfif>
		</cfoutput>
		$("#ListCPPList").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/CPP.cfc?method=GetCPPListReport&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1' + param,
			datatype: "json",
			height: 385,
			colNames:['CPP UUID', 'Desc', 'Create', 'Option'],
			colModel:[			
				{name:'CPP_UUID_vch',index:'CPP_UUID_vch', width:230, editable:false, resizable:false},
				<!--- {name:'UserId_int',index:'UserId_int', width:80, editable:false, resizable:false}, --->
				{name:'Desc_vch',index:'Desc_vch', width:320, editable:true, resizable:false},	
				{
					name:'Created_dt', 
					index:'Created_dt', 
					width:100, 
					editable:false, 
					resizable:false,
					searchoptions:{
							<!--- sopt: [
								"eq", "ne", "gt", "lt"
							], --->
							dataInit:function(el){
	                        	$(el).datepicker({dateFormat:'yy-mm-dd',onSelect: function(dateText, inst){ 
	                        		if (this.id.substr(0, 3) === "gs_") {
				                        setTimeout(function () {
				                            $("#ListCPPList")[0].triggerToolbar();
				                        }, 100);
	                    			} 
	                        	}
	                        })
	                        }
						}
				},
				{name:'Options',index:'Options', width:140, editable:false, align:'right', sortable:false, resizable:false, search: false}
			],
			autowidth: true,
			rowNum:10,
		   	rowList:[10,20,30,40],
			mtype: "POST",
			pager: $('#pagerDiv'),
			physicalPaging:true,
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current CPP Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			sortname: 'Created_dt',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",
			multiselect: false,
			loadComplete: function(data){ 		
				$(".report_details").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".report_details").click( function() { ReportCPPList($(this).attr('rel')); } );			
		  
		  	    $(".report_responses").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".report_responses").click( function() { 
		 			ViewResponsesReport($(this).attr('rel'));
	 			});
			
				$(".report_errors").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".report_errors").click( function() { 
		 			ViewErrorsReport($(this).attr('rel'));
		 		});
	
			},	
			editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/CPP.cfc?method=EditDescCPP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
			jsonReader: {
				root: "ROWS", //our data
				page: "PAGE", //current page
				total: "TOTAL", //total pages
				records:"RECORDS", //total records
				cell: "", //not used
				id: "0" //will default second column as ID
			}	
		});	
		
		$("#ListCPPList_toppager_left").html("<b>Customer Preference Portal</b><img id='refresh_List_BatchCPPList' class='ListIconLinks' src='../../public/images/icons16x16/Refresh_16x16.png' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
		$("#refresh_List_BatchCPPList").click(function() { gridReloadCPPList(); return false; });
		$("#tab-panel-CPPList").toggleClass("ui-tabs-hide");	
	});

	function ReportCPPList(CPP_UUID_vch){
		var ParamStr = '?UUID=' + CPP_UUID_vch;
		window.location = 'cPPReportDetails' + ParamStr;
	}
	
	function ViewResponsesReport(CPP_UUID_vch){
		var ParamStr = '?UUID=' + CPP_UUID_vch;
		window.location = 'cPPResponses' + ParamStr;
	}
	
	function ViewErrorsReport(CPP_UUID_vch){
		var ParamStr = '?UUID=' + CPP_UUID_vch;
		window.location = 'cPPErrors' + ParamStr;
	}
	
	function gridReloadCPPList() {
		var ParamStr = '';
		var CurrSORD =  $("#ListCPPList").jqGrid('getGridParam','sord');
		var CurrSIDX =  $("#ListCPPList").jqGrid('getGridParam','sidx');
		var CurrPage = $('#ListCPPList').getGridParam('page'); 
		<!--- var notes_mask = $("#ListCPPList #search_notes").val();
		Created_dt
		if(notes_mask) ParamStr += '&notes_mask=' + notes_mask; --->
		var CurrCreated =  $("#ListCPPList").jqGrid('getGridParam','Created_dt');

		if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
			
		if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
			
		$("#ListCPPList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/CPP.cfc?method=GetCPPListReport&returnformat=json&queryformat=column&inpSocialmediaFlag=0&_cf_nodebug=true&_cf_nocache=true" + param, page:CurrPage}).trigger("reloadGrid");
	
	}
	
</script>
<div id="tab-panel-CPPList" class="ui-tabs-hide" style="position:relative; overflow: hidden;">   
      
        <div style="table-layout:fixed; min-height:475px; height:475px;">
                
            <!--- <div id="pagerDiv"></div> --->
                           
            <div style="text-align:left;">
            <table id="ListCPPList"></table>
            </div>
        
        </div>
	</div>	    
</div> --->