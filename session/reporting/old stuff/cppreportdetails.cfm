<cfparam name="UUID" default="-1">
<cfparam name="page" default="1">
<cfparam name="INPSIGNEDDATEFROM" default="">
<cfparam name="INPSIGNEDDATETO" default="">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_CPP_Detail_Title#">
</cfinvoke>

<cfset INPCPPUUID = UUID />
<cfimport taglib="../lib/grid" prefix="mb" />

<cfset contactParams = [
	{
		name = "INPCPPUUID",
		value = "#INPCPPUUID#"
	},
	{
		name = 'INPSIGNEDDATEFROM',
		value = '#INPSIGNEDDATEFROM#'	
	},
	{
		name = 'INPSIGNEDDATETO',
		value = '#INPSIGNEDDATETO#'	
	}
 ]>

<cfform name="frmDateFilter" method="get">
	<span style="float:left;"> From: </span> 
	<cfinput 
		type="datefield"
		name="inpSignedDateFrom" 
		id="inpSignedDateFrom" 
		validate="date" 
		mask="mm/dd/yyyy"
		maxlength="10"
		message="inpSignedDateFrom"
		readOnly="true"
		value="#DateFormat(INPSIGNEDDATEFROM,"yyyy-mm-dd")#"
 	/> 
	<cfinput type="hidden" name="page" value="#page#">
	<cfinput type="hidden" name="UUID" value="#UUID#">
	
			 
	<span style="float:left;"> To: </span> 
	<cfinput 
		type="datefield"
		name="inpSignedDateTo" 
		id="inpSignedDateTo" 
		validate="date" 
		mask="mm/dd/yyyy"
		maxlength="10"
		message="inpSignedDateTo"
		readOnly="true"
	    value="#DateFormat(INPSIGNEDDATETO,"yyyy-mm-dd")#"
 	/> 
	<br /><br />		 
	<div style="display:block; padding-left:15px;">
		<cfinput 
				type="submit" 
				name="submit" 
				value="Apply"
				class="somadesign"
			>
	</div>	 
</cfform>		

<cfinvoke component="#LocalSessionDotPath#.cfc.reporting" method="GetTotalCppContacts" returnvariable="retCppContactTotal">
	<cfinvokeargument name="inpCppUUID" value="#INPCPPUUID#">
	<cfinvokeargument name="inpSignedDateFrom" value="#INPSIGNEDDATEFROM#">
	<cfinvokeargument name="inpSignedDateTo" value="#INPSIGNEDDATETO#">
</cfinvoke>
<cfoutput>
	<div> 
		Total: #retCppContactTotal.TOTAL# 
		<cfif retCppContactTotal.TOTAL_EMAIL GT 0>
			Email: #retCppContactTotal.TOTAL_EMAIL#
		</cfif>
		<cfif retCppContactTotal.TOTAL_SMS GT 0>
			SMS: #retCppContactTotal.TOTAL_SMS#
		</cfif>
		<cfif retCppContactTotal.TOTAL_VOICE GT 0>
			Voice: #retCppContactTotal.TOTAL_VOICE#
		</cfif>
	</div>
</cfoutput>

<cfinvoke component="#LocalSessionDotPath#.cfc.reporting" method="GetCppOptionalData" returnvariable="retOptionalData">
	<cfinvokeargument name="inpCppUUID" value="#INPCPPUUID#">
</cfinvoke>

<div class="exportCpp">
	<a href = "<cfoutput>#rootUrl#/#sessionPath#/reporting/cPPContactExcelExport?UUID=#UUID#&INPSIGNEDDATEFROM=#INPSIGNEDDATEFROM#&INPSIGNEDDATETO=#INPSIGNEDDATETO#&page=#page#&isIncLang=#retOptionalData.INCLUDE_LANGUAGE#&isIncIdentity=#retOptionalData.INCLUDE_IDENTITY#</cfoutput>" title="Export Excel">
		<img src ="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/page_excel.png">
	</a>
	<a href = "<cfoutput>#rootUrl#/#sessionPath#/reporting/cppExportPdf?UUID=#UUID#&INPSIGNEDDATEFROM=#INPSIGNEDDATEFROM#&INPSIGNEDDATETO=#INPSIGNEDDATETO#&page=#page#&isIncLang=#retOptionalData.INCLUDE_LANGUAGE#&isIncIdentity=#retOptionalData.INCLUDE_IDENTITY#</cfoutput>" title="Export PDF">
		<img src ="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/page_white_acrobat.png">
	</a>
	<a href = "<cfoutput>#rootUrl#/#sessionPath#/reporting/cppExportCsv?UUID=#UUID#&INPSIGNEDDATEFROM=#INPSIGNEDDATEFROM#&INPSIGNEDDATETO=#INPSIGNEDDATETO#&page=#page#&isIncLang=#retOptionalData.INCLUDE_LANGUAGE#&isIncIdentity=#retOptionalData.INCLUDE_IDENTITY#</cfoutput>" title="Export CSV">
		<img src ="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/page_white_excel.png">
	</a>
</div>
	 
<cfscript>
	contactArrNames = ['Added', 'Contact Type', 'Contact Detail', 'Preferences'];
	contactArrColModel = [			
		{name='added', width='80px'},
		{name='contact_type', width='60px', isHtmlEncode=false},
		{name='contact_detail', width='250px', isHtmlEncode=false},	
		{name='contact_preference', width='250px'}];
		
	if(retOptionalData.INCLUDE_IDENTITY EQ 1){
		contactArrNames[5] = 'Identity';
		contactArrColModel[5] = {name='identity',width='250px'};
		method = "GetCppContactsByIdenty";
		
		if(retOptionalData.INCLUDE_LANGUAGE EQ 1){
			contactArrNames[6] = 'Language';
			contactArrColModel[6] = {name='language',width='50px'};
		}
	} else {
		// for cpp with none include identity
		method = "GetCppContacts";
		
		if(retOptionalData.INCLUDE_LANGUAGE EQ 1){
			contactArrNames[5] = 'Language';
			contactArrColModel[5] = {name='language',width='50px'};
		}
	}
	
	
</cfscript>		
				
<mb:table component="#SessionPath#.cfc.reporting" 
		  method="#method#" 
		  width="80%" 
		  colNames= #contactArrNames# 
		  colModels= #contactArrColModel# 
		  params = #contactParams#>
</mb:table>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Reporting_CPP_Text# >> #Reporting_Detail_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
	
	$(function(){
		$('#submit').click(function(){
		if($('#inpSignedDateFrom').val() > $('#inpSignedDateTo').val()){
			jAlert('Date from can not greater than Date To!', 'Error');
			return false;
		}
	});
	});
	
</script>

<cfoutput>
	<style type="text/css">
		@import url('#rootUrl#/#PublicPath#/css/reporting/cpp.css');
	</style>
</cfoutput>
