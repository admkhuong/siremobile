<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_CPP_Responses_Title#">
</cfinvoke>

<script type="text/javascript">
	
	$('#subTitleText').text('<cfoutput>#Reporting_CPP_Text# >> #Reporting_Response_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');

	$().ready(function ()
	{
	    document.title = "CPP Responses Reports";
	});
	
</script>