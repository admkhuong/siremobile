<cfsetting enablecfoutputonly="yes"> <!--- Required for CSV export to function properly --->

<CFIF FindNoCase('Firefox','#CGI.HTTP_USER_AGENT#') GREATER THAN 0>
	<cfset delim = 9> <!--- Use a comma for a field delimitter, Excel will open CSV files --->
<cfelse>
	<cfset delim = 44>
</CFIF>
<cfif StructKeyExists(URL, 'INPBATCHID')>
	<cfset INPBATCHID = URL.INPBATCHID>
<cfelse>
	<cfoutput><h4 class="err">You don't have permission to export report!</h4></cfoutput>
	<cfabort>
	<cfexit>	
</cfif>
<!--- Create survey object and get export data --->
<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
<cfset answersData = surveyObj.ReadSurveyAnswers(INPBATCHID)>
<cfif answersData.RXRESULTCODE NEQ 1 OR ArrayIsEmpty(answersData.ARRAYQUESTION)>
	<cfoutput><h4 class="err">Cannot access database server or There're no data to export.</h4></cfoutput>
</cfif>
<cfset surveyData = surveyObj.ReadXMLEMAIL(INPBATCHID)>
<cfif surveyData.RXRESULTCODE NEQ 1 OR ArrayIsEmpty(answersData.ARRAYQUESTION)>
	<cfoutput><h4 class="err">Cannot access database server or There're no data to export.</h4></cfoutput>
</cfif>
<cfset QArray = surveyData.ARRAYQUESTION>
<cfset QAnswerArray = answersData.ARRAYQUESTION>

<cfset exportData = ArrayNew(1)>
<cfset i = 1>
<cfloop array="#QArray#" index="quest">
	<!--- Count answers --->
	<cfset ansStruct = StructNew()>
	<cfset QTitle = quest.TEXT>
	<cfif quest.TYPE EQ "SHORTANSWER">
		<cfset QID = quest.ID>
		<cfset QType = 3>
		<cfset ansStruct.Qtype = 3>
		<cfset ANSWERS = ArrayNew(1)>
	<cfelseif quest.TYPE EQ "ONESELECTION">
		<cfset QID = quest.ID>
		<cfset QType = 1>
		<cfset ansStruct.Qtype = 1>
		<cfset ANSWERS = quest.ANSWERS>
	</cfif>
	
	<cfset ansStruct.TotalResponse = 0>
	<cfset ansStruct.TotalSkipped = 0>
	<cfset ansStruct.answerArray = ArrayNew(1)>	
	
	<cfif QType EQ 1>
		<cfloop array="#ANSWERS#" index="ans">
			<cfset ansStruct.answerArray[ans.ID] = StructNew()>
			<cfset ansStruct.answerArray[ans.ID].TEXT = ans.TEXT>
			<cfset ansStruct.answerArray[ans.ID].TotalResponse = 0>
		</cfloop>
	</cfif>
	
	<cfset ansIndex = 1>	
	<cfloop array="#QAnswerArray#" index="answer">
		<cfif QType EQ answer.CP AND QID EQ answer.QUESTIONID>
			<cfif answer.ANSWER NEQ '-1'>
				<cfset ansStruct.TotalResponse = ansStruct.TotalResponse + 1>
				<!--- Short answer --->
				<cfif QType EQ 3>
					<cfset ansStruct.answerArray[ansIndex] = answer.ANSWER>
					<cfset ansIndex = ansIndex + 1>
				<!--- single answer --->
				<cfelseif QType EQ 1>
					<cfset ansStruct.answerArray[#answer.ANSWER#].TotalResponse = ansStruct.answerArray[#answer.ANSWER#].TotalResponse + 1>
				</cfif>
			<cfelse>
				<cfset ansStruct.TotalSkipped = ansStruct.TotalSkipped + 1>
			</cfif>
		</cfif>
	</cfloop>
	
	<cfset ansStruct.Title = QTitle>
	
	<cfset exportData[i] = ansStruct>
	<cfset i = i + 1>
</cfloop>
<!--- Add log --->
<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Survey">
	<cfinvokeargument name="operator" value="Export report survey #INPBATCHID#">
</cfinvoke>
<!--- 
	***** generate data ******
	***** Format Data   ******
	CP1: Single selection
	CP3: Short Answer
	Question ---------- Response Count
	1. Question Title 
	Answer 1 ---------- 10
	Answer 2 ---------- 11
	--------------------------
	Total Response ---- 21
	Total Time Skip --- 100
	---------------------------------
	
	2. Short Answer Q
	Answer 1
	Answer 2
	....
	Total Response --- 100
	Total Time Skip -- 0
--->
<cfcontent type="application/msexcel">
<cfheader 
	name="Content-Disposition" 
	value="filename=ReportOfSurvey_#INPBATCHID#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
	charset="utf-8">

<!--- <cfoutput><table><tr>
	<td>
		Nguyen Tien Dung
	</td>
	<td>
		Nguyen Tien Dung 1
	</td>
</tr></table></cfoutput> --->
<!--- Output Column Headers --->

<cfoutput>Question#chr(delim)#Response Count</cfoutput>

<cfoutput>#chr(13)#</cfoutput> <!--- line break after column header --->
 
<cfset QIndex = 1>
<cfloop array="#exportData#" index="exportRow">
	<cfoutput>#QIndex#. #exportRow.Title#</cfoutput>
	<cfset QIndex = QIndex + 1>
	<cfoutput>#chr(13)#</cfoutput>
	<cfif exportRow.QType EQ 1>
		<cfloop array="#exportRow.ANSWERARRAY#" index="ans">
			<cfoutput>#ans.TEXT##chr(delim)##ans.TOTALRESPONSE#</cfoutput>
			<cfoutput>#chr(13)#</cfoutput>
		</cfloop>
	<cfelseif exportRow.QType EQ 3>
		<cfloop array="#exportRow.ANSWERARRAY#" index="ans">
			<cfoutput>#ans#</cfoutput>
			<cfoutput>#chr(13)#</cfoutput>
		</cfloop>
	</cfif>
	<cfoutput>#chr(13)#</cfoutput>
	<cfoutput>Total Responses#chr(delim)##exportRow.TOTALRESPONSE#</cfoutput>
	<cfoutput>#chr(13)#</cfoutput>
	<cfoutput>Total Times Skipped#chr(delim)##exportRow.TOTALSKIPPED#</cfoutput>
	<cfoutput>#chr(13)#</cfoutput>
	<cfoutput>#chr(13)#</cfoutput>
</cfloop>
