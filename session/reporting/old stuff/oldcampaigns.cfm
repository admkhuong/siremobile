<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Reporting_Campaigns_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>



<cfset isDialog="1">
<cfset isReportPicker="1">

<div id="coverfilterload" style="padding:10px; display:none;">
	<cfinclude template="../campaign/listcampaigns.cfm">
</div>


<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Reporting_Campaigns_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
</script>