<cfparam name="page" default="1">
<cfparam name="query" default="">
<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfoutput>
	<style TYPE="text/css">
		<!---@import url('#rootUrl#/#PublicPath#/css/survey.css');--->
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		<!---@import url('#rootUrl#/#PublicPath#/css/utility.css');--->
		<!---@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css');--->
        ##DefaulFS{
            float: left;
            width: 100%;
        }
    </style>

<!---<cfinclude template="../../campaign/dsp_renameBatch.cfm">--->

</cfoutput>

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>


<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>

<cfif NOT campaignPermission.havePermission>
	<cfset session.permissionError = campaignPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Campaign_Title#">
</cfinvoke>

<cfimport taglib="../../lib/grid" prefix="mb" />
		
<cfoutput>
	<cfscript>
		htmlOption = "";
		
		
		htmlOption = htmlOption & '<a href="##" onclick="ReviewResponses({%BATCHID_BI%}, {%MasterRXCallDetailId_int%}, {%TFLId_int%})"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Responses" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';
		htmlOption = htmlOption & '<a href="##" onclick="return showRenameCampaignDialog({%BATCHID_BI%});"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';		
		
		controlsColumn = '';
				
				
	</cfscript>

	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>
				
	<cfset controlsColumn = {
			name ='normal',
			html = '#controlsColumn#'	
		}>	
	
	<!--- Prepare column data---->
	<cfset arrNames = ['Batch Id', 'CDL Id', 'TFL Id', 'ReviewNotes_vch', 'Options']>	
	<cfset arrColModel = [		
			{name='BatchId_bi',index='BatchId_bi', width='10%', class='BatchId_bi'},
			{name='MasterRXCallDetailId_int',index='MasterRXCallDetailId_int', width='10%', class='MasterRXCallDetailId_int'},
			{name='TFLId_int',index='TFLId_int', width='10%', class='TFLId_int'},	
			{name='ReviewNotes_vch',index='ReviewNotes_vch', width='25%', class='CampaignDescription'},	
			{name='Options',index='Options', width='24%',class='CampaignOptions', format = [htmlFormat]}
		   ] >		
   <cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayinsertAt(arrNames, 4, "Owner ID")>
		<cfset ArrayinsertAt(arrColModel, 4, {name='UserId_int',index='UserId_int',width='10%', class="CampaignOwner"})>
		<cfset ArrayinsertAt(arrNames, 5, "Company ID")>
		<cfset ArrayinsertAt(arrColModel, 5, {name='CompanyName_vch',index='CompanyName_vch',width='10%', class="CampaignCompany"})>
	</cfif>
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Campaign', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}
		]
	>
	<cfset 
		filterFields = [
			'b.Desc_vch'
		]	
	>
	
  	<cfset inpParams = [{NAME='INPBATCHID', VALUE='#INPBATCHID#'}]	>
    
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(filterFields, "b.UserId_int")>
		<cfset ArrayAppend(filterKeys, {VALUE='2', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"})>
		<cfset ArrayAppend(filterFields, "c.CompanyName_vch")>
		<cfset ArrayAppend(filterKeys, {VALUE='3', DISPLAY='Company ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"})>
	</cfif>
	
	<mb:table 
		name="campaigns" 
		component="#LocalSessionDotPath#.cfc.recordedresponses" 
		method="GetBatchesLBR"
		colNames= #arrNames# 
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
        params="#inpParams#"
		width = "100%"
		>
	</mb:table>		

</cfoutput>		
		
<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/iconslist.css');
	</style>
</cfoutput>



<script>
	
	$('#subTitleText').hide();
	$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
	
	$(function() {
		$(".del_RowMCContent").click(function() {
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var currPage =  $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					delBatch(CurrREL,currPage);
				}
				return false;																	
			});
		});
		
		$('.Cancel_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
			
			jConfirm( "Are you sure you want to cancel this Campaign?", "Cancel Campaign", function(result) { 
				if(result)
				{	
					cancelBatch(batchId,currPage);
				}
				return false;																	
			});
		});
		
		$('.Stop_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
				
			jConfirm( "Are you sure you want to stop this Campaign?", "Stop Campaign", function(result) { 
				if(result)
				{	
					stopBatch(batchId,currPage);
				}
				return false;																	
			});
		});
	});
	
	
	
	function ReviewResponses(INPBATCHID, INPCDLID, INPTFLID)
	{
		<cfoutput>
			var params = {'INPBATCHID': INPBATCHID, 'INPCDLID': INPCDLID, 'INPTFLID': INPTFLID};				
			post_to_url('#rootUrl#/#SessionPath#/reporting/recordedresponses/managerecordings', params, 'POST');				
		</cfoutput>
	}
		
	function reloadPage(){
					
		<cfif StructKeyExists(URL, 'page')>
			<cfset pageUrl = "?page=" & URL.page>
		<cfelse>
			<cfset pageUrl = "">
		</cfif>
		var submitLink = "<cfoutput>#rootUrl#/#SessionPath#/reporting/recordedresponses/listrecordings#pageUrl#</cfoutput>";
		reIndexFilterID();
		$("#filters_content").attr("action", submitLink);
		// Check if dont user filter
		if($("#isClickFilter").val() == 0){
			$("#totalFilter").val(0);
		}
		$("#filters_content").submit();
		
	}
	
    setInterval(function() {
    	$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				page : '<cfoutput>#page#</cfoutput>',
				query: '<cfoutput>#query#</cfoutput>',
				INPBATCHID: '<cfoutput>#INPBATCHID#</cfoutput>'
			},
			success: function(d) 
			{
				// update all status of camapain
				$('.View_status').each(function(index){
					$(this).removeClass();
					$(this).addClass('View_status img16_16 ' + d.ROWS[index].IMAGESTATUS);
					$(this).attr('title', d.ROWS[index].STATUSNAME);
												
					// update campaign description
					var campaignDescObj = $('.CampaignDescription').get(index);
					$(campaignDescObj).html(d.ROWS[index].DESC_VCH);
					
					// update recipients
					var campaignRecipient = $('.CampaignRecipients').get(index);
					$(campaignRecipient).empty();
					if(d.ROWS[index].RECIPIENTVIEWFORMAT == 'none' || d.ROWS[index].RECIPIENTCOUNT == -1){
						$(campaignRecipient).append('None ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignRecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">Add</a> )');
					}else{
						$(campaignRecipient).append(d.ROWS[index].RECIPIENTCOUNT + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/ViewRecipientListDetails?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&APPLYRULE=' + d.ROWS[index].CONTACTISAPPLYFILTER + '&CONTACTTYPES='+ d.ROWS[index].CONTACTTYPES_VCH + '&SELECTEDGROUP='+ d.ROWS[index].CONTACTGROUPID_INT + '&NOTE='+ d.ROWS[index].CONTACTNOTE_VCH +'">view</a> | <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignRecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">edit</a> )');
					}
							
									
					<!--- Show campaign user id and company column only for super user --->
					<cfif session.userrole EQ 'SuperUser'>
					var campaignOwner = $('.CampaignOwner').get(index);
					$(campaignOwner).empty();
					$(campaignOwner).html(d.ROWS[index].USERID_INT);
					
					var campaignCompany = $('.CampaignCompany').get(index);
					$(campaignCompany).empty();
					$(campaignCompany).html(d.ROWS[index].COMPANYNAME_VCH);
					</cfif>
					
					<!--- for campaign controls column --->
					var campaignControls = $('.CampaignControls').get(index);
					$(campaignControls).empty();
					
					<!--- for campaign options column --->
					var campaignOptions = $('.CampaignOptions').get(index);
					$(campaignOptions).empty();
				<!---	<cfif 1 EQ 1> <!--- campaignEditPermission.havePermission --->
						$(campaignOptions).append('<a href="##" onclick="ReviewResponses('+ d.ROWS[index].BATCHID_BI +')"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Responses" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
					</cfif>--->
					
					$(campaignOptions).append('<a href="##" onclick="ReviewResponses('+ d.ROWS[index].BATCHID_BI +')"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Responses" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
					
					$(campaignOptions).append('<a href="##" onclick="return showRenameCampaignDialog('+ d.ROWS[index].BATCHID_BI +');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
					
				});
				
			}
		});	
    }, 60000);
	
	function showRenameCampaignDialog(INPBATCHID) {
		OpenDialog(
				"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/dsp_renameBatch?INPBATCHID=" + encodeURIComponent(INPBATCHID), 
				"Rename Campaign", 
				'auto', 
				500,
				false);
	}
</script>
