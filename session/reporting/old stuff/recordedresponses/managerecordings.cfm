
<cfoutput>
	<script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.jplayer.min.js"></script>
	<script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.jplayer.inspector.js"></script> 	
    
	<link rel="stylesheet" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/recordedresponsesreview.player/jplayer.recordedresponsesreview.css">
</cfoutput>



<cfparam name="inpBatchId" default="0">
<cfparam name="INPCDLID" default="0">
<cfparam name="INPTFLID" default="0">

<cftry>
                    	
	<cfset GroupOptions = "">
    <cfset ProgramDescription = "">
    
    <cfinvoke 
     component="#Session.SessionCFCPath#.RecordedResponses"
     method="GetRecordingDetails"
     returnvariable="RetValRRDetailData">                         
        <cfinvokeargument name="INPBATCHID" value="#inpBatchId#"/>
        <cfinvokeargument name="inpCDLId" value="#INPCDLID#"/>
    </cfinvoke>  

    <!--- Check for success --->
    <cfif RetValRRDetailData.RXRESULTCODE LT 1>
        <cfthrow MESSAGE="Failed to get data for this response." TYPE="Any" detail="" errorcode="-1">               
    </cfif>
        
    <cfif RetValRRDetailData.RecordCount GT 0>
                      
               
    <cfelse>
        <cfthrow MESSAGE="Failed to get data for this response." TYPE="Any" detail="" errorcode="-1"> 
    </cfif>
    
<cfcatch type="any">
	
	<cfset RetValRRDetailData.REVIEWNOTES_VCH = "Error loading response data - The system administrator has been notified. Please try again later. " >
        
</cfcatch>                
</cftry>         
					
                    
                    
                    

<style>
	<!---#RRSidebar
	{
		width: 20%;
		min-width: 20%;
		background:#000;
		background: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(1, rgb(200,216,143)),
		color-stop(0, rgb(224, 234, 194))
		);
		background: -moz-linear-gradient(
			center top,
			rgb(200,216,143),
			rgb(224, 234, 194)
		);
			
		position:absolute;
		margin:0px;	
		border: 0;
		border-right: 1px solid #CCC;
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		min-height: 100%;
		height: 110%;
		/*z-index:2300;*/
	}--->
	
	#RRManageRecording
	{
		position:relative;
		
	
	}
	
	#RRPlayContainer
	{		
		width: 1000px;
		mid-width: 1000px;
	<!---	height: 200px;
		min-height:: 200px;	--->	
		left: 200px;
		top:150px;
		position:absolute;
		
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		
		-moz-border-radius: 5px 5px 0px 0px;
		-webkit-border-radius: 5px 5px 0px 0px;
		border-radius: 5px 5px 0px 0px;
		background-color:#FFF;	
	}
	
	#RRAudioPlayer
	{
		height: 34%;
		min-height: 34%;
		width: 36%;
		min-width: 36%;
		
		left: 21%;
		top: 2%;
		position: absolute;
	}
	
	#Cancel
	{
		margin-left: 30px;
		margin-top: 20px;
	}
	
	#AudioSpectrogramContainer
	{
		width: 300px;
		mid-width: 300px;
	<!---	height: 200px;
		min-height:: 200px;	--->	
		left: 200px;
		top:150px;
		position:absolute;
		
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		
		-moz-border-radius: 5px 5px 0px 0px;
		-webkit-border-radius: 5px 5px 0px 0px;
		border-radius: 5px 5px 0px 0px;
		background-color:#FFF;	
	}
	
	#AudioSpectrogramImg
	{				
		width: 100%;
		min-width: 100%;
		height: 80%;
		min-height: 90%;
		
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	}
	
	audio
	{
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	}
	
	#RRNotesContainer
	{
		width: 40%;
		min-width: 40%;
		min-height: 22.5%;
		height: 22.5%;
		
		position:absolute;
		top: 70%;
		left: 21%;
	}
	
	#RRTranscriptionContainer
	{
		width: 40%;
		min-width: 40%;
		min-height: 22.5%;
		height: 22.5%;
		
		position:absolute;
		top: 40%;
		left: 21%;
	}
	
	#RRHeatIndexContainer
	{
		height: 56%;
		left: 63%;
		min-height: 45%;
		min-width: 35%;

		position: absolute;
		top: 40%;
		width: 30%;
	}
	
	#RRNotes, #RRTranscription
	{
		position:relative;
		top:5px;
		left:0px;
		padding:10px;
		margin:0px;	
		
		background:#bbb;
		border: 0;
		border-right: 1px solid #CCC;
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		
		width: 95%;
		min-width: 95%;
		min-height: 95%;
		height: 95%;
		z-index:2300;
		
	}

	audio {
		z-index: 2300;
		position:relative;
	}
	
</style>

<script type="text/javascript">
		
	$(function() {
			
			<!--- Kill the new dialog --->
			$("#RRManageRecording #Cancel").click( function() 
			{
				$("#loadingDlgManageRecording").hide();
				CreateViewBatchDetailsDialogLBR3.remove();
				return false;
			}); 	
			  
			$("#RRManageRecording #UpdateReviewNotesButton").click( function() 
			{
				UpdateReviewNotes();
				return false;
			});
			
			$("#RRManageRecording #UpdateTranscriptionButton").click( function() 
			{
				UpdateTranscription();					
				return false;
			});
			
			$("#RRManageRecording #UpdateASRTranscriptionButton").click( function() 
			{
				UpdateASRTranscription();					
				return false;
			});
			
		
		
		
		
		<!--- initialize jplayers based on relX values --->
		<!---$(inpNewObj + ".cp-jplayer").each(function(index, input)--->
		$(".jp-jplayer").each(function(index, input)
		{
			<!---console.log(this);--->
			
			if(typeof($(this).attr("rel1")) == "undefined")
			{					
				return;
			}
				
			if(typeof($(this).attr("rel2")) == "undefined")
			{					
				return;
			}	
			
			var CurrIndex = parseInt($(this).attr("rel1"))
		  
			$("#jquery_jplayer_ebm_" + CurrIndex).jPlayer({
				ready: function () {
					$(this).jPlayer("setmedia", {
						mp3:$(this).attr("rel2")
					});
				},
				swfPath: "<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/share</cfoutput>",
				supplied: "mp3",
				cssSelectorAncestor: "#jp_container_ebm_" + CurrIndex,
				wmode: "window"
			});
			
			$("#jquery_jplayer_ebm_" + CurrIndex).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
							
				var NextIndex = parseInt($(this).attr("rel1")) + 1;
							
				if ($(".jp-jplayer[rel1='" + NextIndex + "']").length )
				{
					if(ContinuousPlay == 1)
						$(".jp-jplayer[rel1='" + NextIndex + "']").jPlayer("play");
				}
				else
				{
						if(keep_playing > 0 && keep_playing < 5)
						{
							<!--- No paging just one long list--->
							<!---<cfoutput>
								window.location = '#LocalProtocol#://#CGI.SERVER_NAME##CGI.SCRIPT_NAME#?inpbxi=#inpbxi#&inpNumberRowsPerPage=#inpNumberRowsPerPage#&inpSortIndex=#inpSortIndex#&inpSortOrder=#inpSortOrder#&inpFilterAbuse=#inpFilterAbuse#&inpCurrentPage=#inpCurrentPage + 1#&keep_playing=' + (keep_playing+1) + '';
							</cfoutput>--->
						}
					
					
				}
			
			});
										
		});		
		
			LoadSpectrogram();
		
			<!---CreateSpectrogram();--->
			$("#loadingDlgManageRecording").hide();	
			
	});


	<!--- Feature --->	
	function UpdateReviewNotes()
	{	
		<!--- Get currently selected phone strings --->	
		<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
		
					
			  $("#loadingDlgManageRecording").show();		
					
			  $.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=UpdateReviewNotes&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  { INPTFLID : <cfoutput>#INPTFLID#</cfoutput>,  REVIEWNOTES_VCH : $("#ManageRecording #RRNotes").val()},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:'Review Notes Updated OK' });											
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Review Notes has NOT been renamed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgManageRecording").hide();	
									
				} 		
						
			});
			
		return false;
	} 
	
	function UpdateTranscription()
	{	
		<!--- Get currently selected phone strings --->	
		<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
		
					
			  $("#loadingDlgManageRecording").show();		
					
			  $.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=UpdateTranscription&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  {  INPTFLID : <cfoutput>#INPTFLID#</cfoutput>,  TRANSCRIPTION_VCH : $("#RRManageRecording #RRTranscription").val()},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:'Transcription Updated OK' });											
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Transcription has NOT been renamed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgManageRecording").hide();	
									
				} 		
						
			});
			
		return false;
	}


	function UpdateASRTranscription()
	{	
		<!--- Get currently selected phone strings --->	
		<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
		
					
			  $("#loadingDlgManageRecording").show();		
					
			  $.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=UpdateASRTranscription&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  { INPTFLID : <cfoutput>#INPTFLID#</cfoutput>},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{																
								$("#RRManageRecording #RRASRTranscription").val(d.DATA.OUTASRTRANSCRIPTION[0])
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:'ASR Transcription Updated OK' });											
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Transcription has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgManageRecording").hide();	
									
				} 		
						
			});
			
		return false;
	}
	
	<!--- Feature --->	
	function LoadSpectrogram()
	{	
		<!--- Get currently selected phone strings --->	
		<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
			  $.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=GetSpectrogramImagePath&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  { INPTFLID : <cfoutput>#INPTFLID#</cfoutput> },
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!---$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:'Spectrogram created.' });--->
								$("#AudioSpectrogramImg").attr('src', d.DATA.SPECTROGRAMPATH[0]);
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Spectrogram has NOT been created.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgManageRecording").hide();										
				} 		
						
			});
			
		return false;
	}	
</script>
     
	    
<cfoutput>


<cfdump var="#RetValRRDetailData#">
    
    
    <div id="RRManageRecording"> 
    
        <div id="RRPlayContainer">
            <cfset PlayMyMP3_BD = "#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/share/act_getrecordedresponse?scrId=#Session.USERID#_#inpBatchId#_#INPCDLID#_#INPTFLID#">       
            <cfset iFlashCount = 0>                         
                              
            <!--- The jPlayer div must not be hidden. Keep it at the root of the body element to avoid any such problems. --->
            <div id="jquery_jplayer_ebm_#iFlashCount#" rel1="#iFlashCount#" rel2="#PlayMyMP3_BD#" class="jp-jplayer"></div>  
            <div  id="jp_container_ebm_#iFlashCount#"  class="jp-audio">  
               <div class="jp-type-single">  
                    <div id="jp_interface_1" class="jp-interface">  
                    <ul class="jp-controls">  
                        <li><a style="display: block;" href="javascript:;" class="jp-play" tabindex="1">play</a></li>  
                        <li><a style="display: none;" href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>  
                        <li><a style="display: block;" href="javascript:;" class="jp-mute" tabindex="1">mute</a></li>  
                        <li><a style="display: none;" href="javascript:;" class="jp-unmute" tabindex="1">unmute</a></li>  
                    </ul>  
                    <div class="jp-progress-container">  
                        <div class="jp-progress">  
                            <div style="width: 100%;" class="jp-seek-bar">  
                                <div style="width: 3.24158%;" class="jp-play-bar"></div>  
                            </div>  
                        </div>  
                    </div>  
                    <div class="jp-volume-bar-container">  
                        <div class="jp-volume-bar">  
                            <div style="width: 95%;" class="jp-volume-bar-value"></div>  
                        </div>  
                    </div>  
                </div>  
                <div class="jp-no-solution">  
                   <span>Update Required</span>  
                   To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.  
                 </div>  
                </div>  
             </div> 
                    
        
        </div>
    
    
    
	    <div id="RRASRTranscriptionContainer">
				<button id="UpdateASRTranscriptionButton" TYPE="button" class="ui-corner-all">ASR Transcribe</button>
				<textarea id="RRASRTranscription" class="ui-corner-all">#RetValRRDetailData.ASRTRANSCRIPTION_VCH#</textarea>
		</div>
        
        
        <div id="AudioSpectrogramContainer">
	        Yo!<img id="AudioSpectrogramImg" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/share/act_getrecordedresponsegraph?scrId=#Session.USERID#_#inpBatchId#_#INPCDLID#_#INPTFLID#" />        
        </div>
                    
            
    <!---
    
        <div id="AudioSpectrogramContainer">
                                   
            <!---<img id="AudioSpectrogramImg" src="#rootUrl#/#SessionPath#/RecordedResponses/act_GetMySpectrogram?x=#inpBatchId#&y=#INPCDLID#&z=#DateFormat(RetValRRDetailData.RXCDLStartTime_dt,'yyyy_mm_dd')#" />
				<audio controls="control" preload="auto" id="player1" width="100%">
					<source src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_getMyRecordedResponse?inpUserId=#Session.USERID#&amp;inpBatchId=#inpBatchID#&amp;inpDate=#DateFormat(RetValRRDetailData.RXCDLStartTime_dt,'yyyy_mm_dd')#&amp;INPCDLID=#INPCDLID#" type="audio/mpeg">
					<object width="100%" type="application/x-shockwave-flash" data="#rootUrl#/#PublicPath#/js/mediaelement/flashmediaelement.swf">
						<param name="movie" value="flashmediaelement.swf" />
						<param name="flashvars" value="controls=true&file=#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_getMyRecordedResponse?inpUserId=#Session.USERID#&amp;inpBatchId=#inpBatchID#&amp;inpDate=#DateFormat(RetValRRDetailData.RXCDLStartTime_dt,'yyyy_mm_dd')#&amp;INPCDLID=#INPCDLID#" />
					</object>
				</audio>--->
				<!--- http://html5multimedia.com/  --->
		
			<div id="RRNotesContainer">
				<button id="UpdateReviewNotesButton" TYPE="button" class="ui-corner-all">Save Notes</button>
				<textarea id="RRNotes" class="ui-corner-all">#RetValRRDetailData.REVIEWNOTES_VCH#</textarea>
			</div>
		
			<div id="RRTranscriptionContainer">
				<button id="UpdateTranscriptionButton" TYPE="button" class="ui-corner-all">Save Transcription</button>
				<textarea id="RRTranscription" class="ui-corner-all">#RetValRRDetailData.Transcription_vch#</textarea>
			</div>
            
            
		
			<div id="RRHeatIndexContainer">
				Heat Index Stuff
			</div>
		
			<div id="loadingDlgManageRecording" style="display:inline;">
				<img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
			</div>
		</div>--->


</div> <!--- RRManageRecording --->

</cfoutput>

