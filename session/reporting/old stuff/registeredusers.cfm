<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Registered users in keyword</title>
		<link rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/style.css</cfoutput>"	type="text/css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

		<script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>" type="text/javascript"></script>
        <script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>" type="text/javascript"></script>
        <script src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amstock.js</cfoutput>" type="text/javascript"></script>
		<!---<script src="amcharts/serial.js" type="text/javascript"></script>
		<script src="amcharts/amstock.js" type="text/javascript"></script>--->

		<script type="text/javascript">
			AmCharts.ready(function () {
				generateChartData();
				
				$( "#from" ).datepicker({
				  changeMonth: true,
				  changeYear: true,
				  dateFormat: 'mm-dd-yy',
				  onClose: function( selectedDate ) {
					$( "#to" ).datepicker( "option", "minDate", selectedDate );
					var date2 = $( "#from" ).datepicker('getDate');
					/*date2.setDate(date2.getDate()+7); 
					$( "#to" ).datepicker( "option", "maxDate", date2 );*/
				  }
				});
				$( "#to" ).datepicker({
				  changeMonth: true,
				  changeYear: true,
				  dateFormat: 'mm-dd-yy',
				  onClose: function( selectedDate ) {
					  var date2 = $( "#to" ).datepicker('getDate');
					/*date2.setDate(date2.getDate()-7);
					$( "#from" ).datepicker( "option", "minDate", date2 ); */ 
					$( "#from" ).datepicker( "option", "maxDate", selectedDate );
				  }
				});
				//createStockChart();
			});

			var chartData1 = [];
			var chartData2 = [];
			var chartData3 = [];
			var chartData4 = [];

			function generateChartData() {
				$.ajax({
					  url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=getRegesteredUsers&returnformat=json&queryformat=column",
					  context: document.body
					}).done(function(data) {
						var arr = $.parseJSON(data);
					  	
						var chart = new AmCharts.AmStockChart();
						chart.pathToImages = "<cfoutput>#rootUrl#/#publicPath#/jsamcharts/images/</cfoutput>";
						var dataSet = [];
						
						for(var index = 0;
								index < arr.length;
								index++)
						{
							dataSet[index] = new AmCharts.DataSet();
							dataSet[index].title = arr[index].KEYWORD;
							dataSet[index].fieldMappings = [{
								fromField: "VALUE",
								toField: "VALUE"
							}];
							dataSet[index].dataProvider = arr[index].VALARRAY;
							dataSet[index].categoryField = "DATE";	
						}
						
						chart.dataSets = dataSet;
						
						var stockPanel1 = new AmCharts.StockPanel();
						stockPanel1.showCategoryAxis = false;
						stockPanel1.title = "Value";
						stockPanel1.percentHeight = 70;
		
						// graph of first stock panel
						var graph1 = new AmCharts.StockGraph();
						graph1.valueField = "VALUE";
						graph1.comparable = true;
						graph1.compareField = "VALUE";
						graph1.bullet = "round";
						graph1.bulletBorderColor = "#FFFFFF";
						graph1.bulletBorderAlpha = 1;
						graph1.balloonText = "[[title]]:<b>[[VALUE]]</b>";
						graph1.compareGraphBalloonText = "[[title]]:<b>[[VALUE]]</b>";
						graph1.compareGraphBullet = "round";
						graph1.compareGraphBulletBorderColor = "#FFFFFF";
						graph1.compareGraphBulletBorderAlpha = 1;
						stockPanel1.addStockGraph(graph1);
		
						// create stock legend
						var stockLegend1 = new AmCharts.StockLegend();
						stockLegend1.periodValueTextComparing = "[[percents.value.close]]%";
						stockLegend1.periodValueTextRegular = "[[values.close]]";
						stockPanel1.stockLegend = stockLegend1;
						
						chart.panels = [stockPanel1];
						
						// OTHER SETTINGS ////////////////////////////////////
						var sbsettings = new AmCharts.ChartScrollbarSettings();
						sbsettings.graph = graph1;
						chart.chartScrollbarSettings = sbsettings;
		
						// CURSOR
						var cursorSettings = new AmCharts.ChartCursorSettings();
						cursorSettings.valueBalloonsEnabled = true;
						chart.chartCursorSettings = cursorSettings;
		
		
						// PERIOD SELECTOR ///////////////////////////////////
						var periodSelector = new AmCharts.PeriodSelector();
						periodSelector.position = "left";
						periodSelector.periods = [{
							period: "DD",
							count: 10,
							label: "10 days"
						}, {
							period: "MM",
							selected: true,
							count: 1,
							label: "1 month"
						}, {
							period: "YYYY",
							count: 1,
							label: "1 year"
						}, {
							period: "YTD",
							label: "YTD"
						}, {
							period: "MAX",
							label: "MAX"
						}];
						chart.periodSelector = periodSelector;
		
		
						// DATA SET SELECTOR
						var dataSetSelector = new AmCharts.DataSetSelector();
						dataSetSelector.position = "left";
						chart.dataSetSelector = dataSetSelector;
		
						chart.write('chartdiv');
					});
					
					
				
			}

			function createStockChart() {
				// PANELS ///////////////////////////////////////////
				// first stock panel
				var stockPanel1 = new AmCharts.StockPanel();
				stockPanel1.showCategoryAxis = false;
				stockPanel1.title = "Value";
				stockPanel1.percentHeight = 70;

				// graph of first stock panel
				var graph1 = new AmCharts.StockGraph();
				graph1.valueField = "value";
				graph1.comparable = true;
				graph1.compareField = "value";
				graph1.bullet = "round";
				graph1.bulletBorderColor = "#FFFFFF";
				graph1.bulletBorderAlpha = 1;
				graph1.balloonText = "[[title]]:<b>[[value]]</b>";
				graph1.compareGraphBalloonText = "[[title]]:<b>[[value]]</b>";
				graph1.compareGraphBullet = "round";
				graph1.compareGraphBulletBorderColor = "#FFFFFF";
				graph1.compareGraphBulletBorderAlpha = 1;
				stockPanel1.addStockGraph(graph1);

				// create stock legend
				var stockLegend1 = new AmCharts.StockLegend();
				stockLegend1.periodValueTextComparing = "[[value.close]]%";
				stockLegend1.periodValueTextRegular = "[[value.close]]";
				stockPanel1.stockLegend = stockLegend1;
				

				// set panels to the chart
				chart.panels = [stockPanel1, stockPanel2];


				// OTHER SETTINGS ////////////////////////////////////
				var sbsettings = new AmCharts.ChartScrollbarSettings();
				sbsettings.graph = graph1;
				chart.chartScrollbarSettings = sbsettings;

				// CURSOR
				var cursorSettings = new AmCharts.ChartCursorSettings();
				cursorSettings.valueBalloonsEnabled = true;
				chart.chartCursorSettings = cursorSettings;


				// PERIOD SELECTOR ///////////////////////////////////
				var periodSelector = new AmCharts.PeriodSelector();
				periodSelector.position = "left";
				periodSelector.periods = [{
					period: "DD",
					count: 10,
					label: "10 days"
				}, {
					period: "MM",
					selected: true,
					count: 1,
					label: "1 month"
				}, {
					period: "YYYY",
					count: 1,
					label: "1 year"
				}, {
					period: "YTD",
					label: "YTD"
				}, {
					period: "MAX",
					label: "MAX"
				}];
				chart.periodSelector = periodSelector;


				// DATA SET SELECTOR
				var dataSetSelector = new AmCharts.DataSetSelector();
				dataSetSelector.position = "left";
				chart.dataSetSelector = dataSetSelector;

				chart.write('chartdiv');
			}
		</script>
	</head>
	<body style="background-color:#FFFFFF">
    	<cfform name="Filter" id="Filter" action="reg">
            <input type="hidden" name="isxls" id="isxls" value="1"/>
            <table align="center">
                <tr>
                    <td>
                        From
                    </td>
                    <td>
                        <input type="text" name="from" id="from" />
                    </td>
                    <td>
                        To
                    </td>
                    <td>
                        <input type="text" name="to" id="to" />
                    </td>
                    <td><input type="submit" name="submit1" id="submit1" value="Import xls" /></td>
                </tr>
            </table>
        </cfform>
		<div id="chartdiv" style="width:1600px; height:700px;"></div>
	</body>

</html>