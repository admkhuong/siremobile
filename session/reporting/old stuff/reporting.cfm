<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset reportingPermission = permissionObject.havePermission(Reporting_Title)>
<cfset reportingCampaignPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset reportingSurveyPermission = permissionObject.havePermission(Reporting_Surveys_Title)>

<cfif NOT reportingPermission.havePermission>
	<cfset session.permissionError = reportingPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_Title#">
</cfinvoke>

<cfoutput>
	<cfif reportingCampaignPermission.havePermission>
		<a href='#rootUrl#/#SessionPath#/reporting/campaigns'>Campaigns</a>
	</cfif>
	<cfif reportingSurveyPermission.havePermission>
		<a href='#rootUrl#/#SessionPath#/reporting/surveyList'>Surveys</a>
	</cfif>
	<a href='#rootUrl#/#SessionPath#/reporting/cPPList'>CPP</a>
</cfoutput>

<script type="text/javascript">
	$('#subTitleText').hide();
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
</script>