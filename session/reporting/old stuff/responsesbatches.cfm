<cfparam name="inpBatchId" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_Campaign_Response_Title#">
</cfinvoke>

<style>


#panel_LBRFilters {
	width: 300px;
	height: 440px;
	position: absolute;
	bottom: -465px;
	left: 480px;
	z-index: 200;
	background-color:#FFF;
	border: 1px solid #B6B6B6;
	
	-moz-border-radius-topleft: 20px;
	-webkit-border-top-left-radius: 20px;
	border-top-left-radius: 20px;
	
	-moz-border-radius-bottomleft: 20px;
	-webkit-border-bottom-left-radius: 20px;
	border-bottom-left-radius: 20px;
	
	-moz-border-radius-bottomright: 20px;
	-webkit-border-bottom-right-radius: 20px;
	border-bottom-right-radius: 20px;
	
	padding: 10px;
	 
}

#panel_tab_LBRFilters {
	width: 140px;
	height: 16px;
	position: absolute;
	top: -29px;
	right: -1px;
	text-decoration: none;
	color: #313131;
	border: 1px solid #B6B6B6;
	border-bottom:1px solid #FFF;;
		
	-moz-border-radius-topleft: 20px;
	-webkit-border-top-left-radius: 20px;
	border-top-left-radius: 20px;
	
	-moz-border-radius-topright: 20px;
	-webkit-border-top-right-radius: 20px;
	border-top-right-radius: 20px;
	
	padding: .5em 1em; 
	text-decoration: none; 
	font-size: 12px; 
	font-weight: bold; 
	text-shadow: 0 1px 0 rgba(255,255,255,0.5);
	
	background: url(mb/images/bg_fallback_MBGreenWeb.png) 0 0 repeat-x;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
}

#panel_tab_LBRFilters:focus {
	outline: none;
	
} 

	
</style>
<!---
	MasterRXCallDetailId_int,
                RedialNumber_int,
                InitialDate_dt,
                Reviewed_dt
				
--->

<script>

var timeoutHndBatchesLBR;
var fAutoBatchesLBR = true;
var LastSelBatchesLBR;
var lastObj_MCContent;
var sipPos_LBRFilters = 0;
	
$(function() {
	
	$('#subTitleText').text('<cfoutput>#Reporting_Campaigns_Text# >> #Reporting_Response_Text#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
	
	document.title = "Campaign Responses Reports";
	$("#BatchListLBR").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=GetBatchesLBR&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&INPBATCHID=<cfoutput>#inpBatchId#</cfoutput>&inpLightDisplay=1',
		datatype: "json",
		height: 385,
		colNames:['ID','Description','Created Date', 'Last Updated Date', 'Options'],
		colModel:[			
<!---			{name:'BatchId_bi',index:'BatchId_bi', width:100, editable:false, resizable:false},--->
			{name:'MasterRXCallDetailId_int',index:'MasterRXCallDetailId_int', width:100, editable:true, resizable:false},			
			{name:'ReviewNotes_vch',index:'ReviewNotes_vch', width:320, editable:true, resizable:false},			
			{name:'InitialDate_dt',index:'InitialDate_dt', width:125, editable:false, resizable:false},
			{name:'Reviewed_dt',index:'Reviewed_dt', width:125, editable:false, resizable:false},			
			{name:'Options',index:'Options', width:105, editable:false, align:'right', sortable:false, resizable:false}
		],
		rowNum:15,
	//   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Results Found For Batch ID (<cfoutput>#inpBatchId#</cfoutput>).",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'InitialDate_dt',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
		ondblClickRow: function(BatchId_bi){ $('#BatchListLBR').jqGrid('editRow',BatchId_bi,true, '', '', '', '', rx_AfterEditMCContent,'', ''); },
		onSelectRow: function(BatchId_bi){
				if(BatchId_bi && BatchId_bi!==LastSelBatchesLBR){
					$('#BatchListLBR').jqGrid('restoreRow',LastSelBatchesLBR);
					//$('#BatchListLBR').jqGrid('editRow',InitialDate_dt,true, '', '', '', '', rx_AfterEditMCContent,'', '');
					LastSelBatchesLBR=BatchId_bi;
				}
			},
		 loadComplete: function(data){ 		
		 	 
		 <!--- $(this).attr({"title":'Reports Viewer'}); --->
				$(".view_ReportTool").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_ReportTool").click( function() { ViewReportToolDialogLBR($(this).attr('rel')); } );			
	<!---	  
			//	$(".view_RowBatchesLBR").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	//	$(".view_RowBatchesLBR").click( function() { ViewBatchDetailsDialogLBR($(this).attr('rel')); } );
			
			//	$(".view_RowBatchesLBR2").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	//	$(".view_RowBatchesLBR2").click( function() { ViewBatchDetailsDialogLBR2($(this).attr('rel'), $(this).attr('rel2') ); } );
	--->	
				$(".view_RowBatchesLBR3").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_RowBatchesLBR3").click( function() { ViewBatchDetailsDialogLBR3($(this).attr('rel'), $(this).attr('rel2') ); } );				
				$("#tab-panel-BatchesLBR .view_RowBatchesMCContent3").attr("title", "Review Recordings");


		<!---	
			//	$(".view_RowBatchesMCRules").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	//	$(".view_RowBatchesMCRules").click( function() { ViewRulesEditorDialogLBR($(this).attr('rel')); } );
		--->						
		
		<!---		$(".del_RowMCContent").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_RowMCContent").click( function() { 
				
												$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
												$.alerts.cancelButton = '&nbsp;No&nbsp;';		
												
												var CurrREL = $(this).attr('rel');
												
												jConfirm( "Are you sure you want to delete this Campaign?", "About to delete Campaign.", function(result) { 
													if(result)
													{	
														DelBatch(CurrREL);
													}
													return false;																	
												});
																 
											} );
		--->
			
				<!--- Add a filter row--->
				
				var BatchListLBRSearchFilterRow = ''+
				'<tr class="ui-widget-content jqgrow ui-row-ltr" role="row" id="BatchListLBRSearchFilterRow" aria-selected="true">'+
				'	<td aria-describedby="" style="" role="gridcell"></td>'+
				'	<td aria-describedby="BatchListLBR_ReviewNotes_vch" title=""  align="left" style="text-align:left;" role="gridcell">'+
				'Descritpion Filter<BR><input type="text" class="ui-corner-all" style="width:240px; display:inline; margin-bottom:3px;" value="' + data.NOTES_MASK + '" id="search_notes">'+
				'	</td>'+
				'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="" title="" style="display:none;" role="gridcell">&nbsp;</td>'+
				'</tr>'


				<!--- Append to end of table body - nice and tight to the form - be sure structure is correct--->
				$("#BatchListLBR tbody").append(BatchListLBRSearchFilterRow);
				
				<!--- Bind all search functionality to new row --->
				$("#BatchListLBR #search_notes").unbind();
				$("#BatchListLBR #search_notes").keydown(function(event) { doSearchLBR(arguments[0]||event,'search_notes') }); 
				
				<!--- Reset the focus to an inline filter box--->
				if(typeof(lastObj_MCContent) != "undefined" && lastObj_MCContent != "")
				{					
					<!--- Stupid IE not rendering yet so call again in 10 ms--->
					setTimeout(function() 
					{ 
					
						 var LocalFocus = $("#BatchListLBR #" + lastObj_MCContent);
						 if (LocalFocus.setSelectionRange) 
						 {         
							 <!--- ... then use it        
							   (Doesn't work in IE)
							   Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh. 
							  ---> 
							   var len = $(LocalFocus).val().length * 2;         
							   LocalFocus.setSelectionRange(len, len);         
						 }        
						 else
  			          	 {
						     <!---... otherwise replace the contents with itself         
							   (Doesn't work in Google Chrome)    --->     
							  $(LocalFocus).focus(); 
							  var str = $(LocalFocus).val();
							  $(LocalFocus).val("");    
							  $(LocalFocus).val(str);        
					     }         
						 
						 <!--- Scroll to the bottom, in case we're in a tall textarea
						  Necessary for Firefox and Google Chrome)--->
						 LocalFocus.scrollTop = 999999; 
						 
						
						lastObj_MCContent = undefined;
						$("#BatchListLBR #" + lastObj_MCContent).focus(); 
					}, 10);
				}
			
				
   			},	
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default second column as ID
			  }	
	});
	
	$('#pagerDiv').hide();

	$("#BatchListLBR_BatchId_bi").css('text-align', 'left');
	$("#BatchListLBR_ReviewNotes_vch").css('text-align', 'left');
	$("#BatchListLBR_InitialDate_dt").css('text-align', 'left');
	$("#BatchListLBR_Reviewed_dt").css('text-align', 'left');
	$("#BatchListLBR_Options").css('text-align', 'right');

	$("#refresh_List_BatchMCContent").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#BatchListLBR_toppager_left").html("<b>Results</b><img id='refresh_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
	$("#refresh_List").click(function() { gridReloadBatchesLBR(); return false; });
		
	$("#tab-panel-BatchesLBR").toggleClass("ui-tabs-hide");	
	
	$('#dockBatchesLBR').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_MCContent',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);	
			
			
	$('#LBRFilter_STOP_DT').datepicker({
		numberOfMonths: 2,
		showButtonPanel: true,
		dateFormat: 'yy-mm-dd'
	});
	
	$('#LBRFilter_START_DT').datepicker({
		numberOfMonths: 2,
		showButtonPanel: true,
		dateFormat: 'yy-mm-dd'
	});
		
	<!--- Animated filters tab--->
	$("#panel_tab_LBRFilters").click(function(e) {
				
		e.preventDefault();
		$("#panel_LBRFilters").animate({ bottom: sipPos_LBRFilters }, 565, 'linear', function()
		 {
			if(sipPos_LBRFilters == 0) 
			{ 
				sipPos_LBRFilters = -465; 
				$("#panel_tab_LBRFilters").html('Close Filters');
			}
			else
			{
				 sipPos_LBRFilters = 0; 
				 $("#panel_tab_LBRFilters").html('Open Filters');
			}
		});
	});		
	
//	tinyMCE.init({
 //   	mode : "none",
//    	theme : "simple"
//	});
		
	<!---	<!--- Setup TinyMCE --->
		jQuery('textarea.tinymce').tinymce({
			script_url : '../../public/js/tiny_mce_JQuery/tiny_mce.js',
			width: "800px",
			height: "500px",
			mode: "none",
			theme : "advanced",
			theme_advanced_toolbar_location : "bottom"
			
			});--->
			
});


<!--- Updates data to scrubbed values on the server side - removes formatting--->
function rx_AfterEditMCContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchListLBR").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{							
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{								
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESC[0]) != "undefined")								
				{					 	
						$("#BatchListLBR").jqGrid('setRowData',rowid,{ReviewNotes_vch:d.DATA.INPDESC[0]});
				}
				
			}
				
		}
		else
		{<!--- Invalid structure returned --->	
			
		}
	}
	else
	{<!--- No result returned --->
		
	}  
};




function doSearchLBR(ev, sourceObj){
	
	if(!fAutoBatchesLBR)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHndBatchesLBR)
		clearTimeout(timeoutHndBatchesLBR)
	timeoutHndBatchesLBR = setTimeout(gridReloadBatchesLBR,500)
	
	lastObj_MCContent = sourceObj;
}

function gridReloadBatchesLBR(){
			
		var ParamStr = '';
		var CurrSORD =  $("#BatchListLBR").jqGrid('getGridParam','sord');
		var CurrSIDX =  $("#BatchListLBR").jqGrid('getGridParam','sidx');
		var CurrPage = $('#BatchListLBR').getGridParam('page'); 
		var notes_mask = $("#BatchListLBR #search_notes").val();
		
		if(notes_mask) ParamStr += '&notes_mask=' + notes_mask;
				
		if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
			
		if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
		
	$("#BatchListLBR").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RecordedResponses.cfc?method=GetBatchesLBR&returnformat=json&queryformat=column&INPBATCHID=<cfoutput>#inpBatchId#</cfoutput>&_cf_nodebug=true&_cf_nocache=true&inpLightDisplay=1" + ParamStr,page:CurrPage}).trigger("reloadGrid");

}

function enableAutosubmitMCContent(state){
	fAutoBatchesLBR = state;
	gridReloadBatchesLBR();
	$("#submitButton").attr("disabled",state);
}




<!--- Global so popup can refernece it to close it--->
var CreateViewBatchDetailsDialogLBR3 = 0;

function ViewBatchDetailsDialogLBR3(INPBATCHID, inpMasterRXCallDetailId_int, inpDesc)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(INPBATCHID) == "undefined" || INPBATCHID == "")					
		INPBATCHID = 0;
		
	if(typeof(inpMasterRXCallDetailId_int) == "undefined" || inpMasterRXCallDetailId_int == "")					
		inpMasterRXCallDetailId_int = 0;
		
	
	ParamStr = '?inpbatchid=' + encodeURIComponent(INPBATCHID) + '&MASTERRXCALLDETAILID_INT=' + encodeURIComponent(inpMasterRXCallDetailId_int);
		
	
	<!--- Erase any existing dialog data --->
	if(CreateViewBatchDetailsDialogLBR3 != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateViewBatchDetailsDialogLBR3.remove();
		CreateViewBatchDetailsDialogLBR3 = 0;
		
	}
					
	CreateViewBatchDetailsDialogLBR3 = $('<div></div>').append($loading.clone());
	RRWidth = $(window).width() * 0.95;
	RRHeight = $(window).height() * 0.95;

	CreateViewBatchDetailsDialogLBR3
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/RecordedResponses/dsp_ManageRecording' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Manage Recording',
			width: RRWidth,
			height: RRHeight,
			closeOnEscape: false,
// 		    open: function() { },
// 		    focus: function(event, ui) { },
			close: function() { $(this).dialog('destroy');$(this).remove(); }, 
			beforeClose:  function() { 
				return true;
			 }, 			
			position: 'top', 
			zIndex: 9 
		});

	CreateViewBatchDetailsDialogLBR3.dialog('open');

	return false;		
}


</script>
     
	    
<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-BatchesLBR" class="ui-tabs-hide" style="position:relative; overflow: hidden;">   
      
        <div style="table-layout:fixed; min-height:475px; height:475px;">
                
            <div id="pagerDiv"></div>
                           
            <div style="text-align:left;">
            <table id="BatchListLBR"></table>
            </div>
        
        </div>

        <br />
        <br />
        
          
      
        
        <div id="panel_LBRFilters">
          <a href="##" id="panel_tab_LBRFilters">Open Filters</a>
          <p>
          	      <div style="text-align:left; margin: 5px 0 5px 0; position:relative;" class="RXForm">

             		   
                    
                        <label title="Calander date...">Start Date</label>
                        <input TYPE="text" name="LBRFilter_START_DT" id="LBRFilter_START_DT" value="" class="ui-corner-all" style="width:85px;" title="Calander date..."/> 
                    
                        <label title="Calander date...">Stop Date</label>
                        <input TYPE="text" name="LBRFilter_STOP_DT" id="LBRFilter_STOP_DT" value="" class="ui-corner-all" style="width:85px;" title="Calander date..."/> 
                   
                        
                </div>
                <div>        
                        <p>
              <!--- Add additional filters and then report accross filtered batchids        	
			  
			  	<a class="MedallionImageLink" href="##" onclick="ViewReportToolDialogLBR(); return false;"><img src="../../public/images/dock/Apps-kchart-icon64x64.png" alt="Activity Console" title="Activity Console"/></a> 
            --->
                        </p>
               </div>
          </p>
     </div>
     
	</div>        


  	<div style="position:relative; bottom:-80px;">                           

             
	</div>

  
    
    
	
</cfoutput>    
     
