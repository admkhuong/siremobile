﻿<cfparam name="page" default="1">
<cfparam name="query" default="">

<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/utility.css');
	</style>

</cfoutput>

<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset SmsMessagingsPermission = permissionObject.havePermission(Reporting_SmsMessagings_Title)>

<cfif NOT SmsMessagingsPermission.havePermission>
	<cfset session.permissionError = SmsMessagingsPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>
<!---end check permission--->

<!---import grid lib--->
<cfimport taglib="../../lib/grid" prefix="mb" />


<!--- Prepare column data---->
<cfset arrNames = ['Contact Number', 'Type', 'Message', 'Time']>	
<cfset arrColModel = [			
		{name = 'ContactNumber', index = 'ContactNumber', width = '25%'},
		{name = 'Type', index = 'Type', width = '10%'},	
		{name = 'Message', index = 'Message', width = '40%'},
		{name = 'Time', index = 'Time', width = '25%'}
	   ] >	
<cfoutput>
	<mb:table 
		name="smsMessagings" 
		component="#LocalSessionDotPath#.cfc.smsmessaging" 
		method="GetSmsMessagings"
		colNames= #arrNames# 
		colModels= #arrColModel#
		width = "100%"
		FilterSetId="DefaulFSListSmsMessaging"
		>
	</mb:table>		

</cfoutput>		
<script>
	$('#subTitleText').hide();
	$('#mainTitleText').text('Sms Messaging');
</script>