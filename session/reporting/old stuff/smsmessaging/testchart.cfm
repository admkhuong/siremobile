﻿<!---<cfset groupBarchartObj = createObject("component", "#LocalSessionDotPath#.lib.report.GroupBarchart").init() />
<cfset groupBarchartObj.setTitle('group bar chart') />
<cfset data = [
				[45.0,12.0],<!--- Firefox data --->
				[26.8,12.0],<!--- IE data --->
				[26.8,12.0],<!--- Safari data --->
				[26.8,12.0],<!--- Opera data --->
				[26.8,12.0]<!--- Others data --->
			]>
<cfset series = ['Firefox','IE','Safari','Opera','Others']>
<cfset category = ["category 1","category 2"]>
<cfset groupBarchartObj.setseries(series)>            
<cfset groupBarchartObj.setData(data)>
<cfset groupBarchartObj.setCategory(category)>
<cfset groupBarchartObj.setChartType(1)/><!---1 for draw fusion chart, 2 for highchart--->
<div id="container"></div>
<cfoutput>
	 <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery-1.9.1.min.js"></script>
	 <script type="text/javascript" src="#rootUrl#/#publicPath#/js/Highcharts-3.0.2/js/highcharts.js"></script>
	 <script type="text/javascript" src="#rootUrl#/#publicPath#/js/Highcharts-3.0.2/js/modules/exporting.js"></script>
</cfoutput>
<script type="text/javascript">
 $(document).ready(function(){
  var options = <cfoutput>#groupBarchartObj.drawHighChart()#</cfoutput>;
  $('#container').highcharts(options);
 }); 
</script>  
<cfoutput>
 #groupBarchartObj.drawFusionChart()#
</cfoutput>--->

<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_2.10.8/amcharts/amcharts.js</cfoutput>"></script>
<cfset barchartObj = createObject("component", "#LocalSessionDotPath#.lib.report.groupbarchart").init() />
<cfset barchartObj.setTitle('bar chart') />
<!---<cfset data = [45.0,26.8,8.5,6.2,0.7]>
<cfset category = ["Firefox","IE","Safari","Opera","Others"]>--->
<cfset category = ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']>
<cfset data = [{
                "name"= 'John',
                "data"= [5, 3, 4, 7, 2]
            }, {
                "name"= 'Jane',
                "data"= [2, 2, 3, 2, 1]
            }, {
                "name"= 'Joe',
                "data"= [3, 4, 4, 2, 5]
            }]>
<cfset barchartObj.setData(data) />
<cfset barchartObj.setChartType(2) />
<cfset barchartObj.setCategory(category) />
<cfoutput>
 <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery-1.9.1.min.js"></script>
 <script type="text/javascript" src="#rootUrl#/#publicPath#/js/Highcharts-3.0.2/js/highcharts.js"></script>
</cfoutput>
<cfoutput>
	<cfset temp = #barchartObj.drawAmChart()#>
	#temp#
</cfoutput>
<div id="chartdiv" style="height:400px"></div>

<script type="text/javascript">
//	$(document).ready(function(){
		<cfoutput>
			#temp#
		</cfoutput>
//	});
	
	
 	chart.write("chartdiv");
	
</script> 
<!---var chart;
	
	var chartData = [
	{
	    category: "John",
	    Apples: 5,
	    Oranges:3,
		Pears:4,
		Grapes:7,
		Bananas:2
    },
	{
	    category: "Jane",
	    Apples: 2,
	    Oranges:2,
		Pears:3,
		Grapes:2,
		Bananas:1
	},
	{
	    category: "Joe",
	    Apples: 3,
	    Oranges:4,
		Pears:4,
		Grapes:2,
		Bananas:5
	}
    ];
	
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "category";
    chart.startDuration = 1;

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.labelRotation = 90;
    categoryAxis.gridPosition = "start";
    
    // value
    // in case you don't want to change default settings of value axis,
    // you don't need to create it, as one value axis is created automatically.
    // GRAPH
    var graph = new AmCharts.AmGraph();
	graph.valueField = "Apples";
	graph.balloonText = "Apples: [[value]]";
	graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);
	
	var graph = new AmCharts.AmGraph();
	graph.valueField = "Oranges";
	graph.balloonText = "Oranges: [[value]]";
	graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);
	
	var graph = new AmCharts.AmGraph();
	graph.valueField = "Pears";
	graph.balloonText = "Pears: [[value]]";
	graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);
	
	var graph = new AmCharts.AmGraph();
	graph.valueField = "Grapes";
	graph.balloonText = "Grapes: [[value]]";
	graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);
	
	var graph = new AmCharts.AmGraph();
	graph.valueField = "Bananas";
	graph.balloonText = "Bananas: [[value]]";
	graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);--->
<!---var chart;
	
	var chartData = [{
	    category: "USA",
	    Apples: 4025,
	    visit1:1222
	    },
	{
	    category: "China",
	    Apples: 1882,
	    visit1:4222},
	{
	    category: "Japan",
	    Apples: 1809,
	    visit1:4222}];
	
	
	AmCharts.ready(function() {
	    // SERIAL CHART
	    chart = new AmCharts.AmSerialChart();
	    chart.dataProvider = chartData;
	    chart.categoryField = "category";
	    chart.startDuration = 1;
	
	    // AXES
	    // category
	    var categoryAxis = chart.categoryAxis;
	    categoryAxis.labelRotation = 90;
	    categoryAxis.gridPosition = "start";
	    
	    // value
	    // in case you don't want to change default settings of value axis,
	    // you don't need to create it, as one value axis is created automatically.
	    // GRAPH

	    var graph = new AmCharts.AmGraph();
	    graph.valueField = "Apples";
	    graph.balloonText = "[apples]: [[value]]";
	    graph.type = "column";
	    graph.lineAlpha = 0;
	    graph.fillAlphas = 0.8;
	    chart.addGraph(graph);
	    
	    var graph = new AmCharts.AmGraph();
	    graph.valueField = "visit1";
	    graph.balloonText = "[[category]]: [[value]]";
	    graph.type = "column";
	    graph.lineAlpha = 0;
	    graph.fillAlphas = 0.8;
	    chart.addGraph(graph);
	    
	    var graph = new AmCharts.AmGraph();
	    graph.valueField = "visits";
	    graph.balloonText = "[[category]]: [[value]]";
	    graph.type = "column";
	    graph.lineAlpha = 0;
	    graph.fillAlphas = 0.8;
	    chart.addGraph(graph);
	    
	    
	    chart.write("chartdiv");});--->

