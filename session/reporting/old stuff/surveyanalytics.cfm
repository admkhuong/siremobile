<cfoutput>
  	<link href='#rootUrl#/#PublicPath#/css/survey.css' rel='stylesheet' type='text/css'>
</cfoutput>
<cfparam name="collectionMethod" type="string" default="">

<!--- Add log --->
<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Survey">
	<cfinvokeargument name="operator" value="View report survey #INPBATCHID#">
</cfinvoke>

<script type="text/javascript">
	$().ready(function (){
		$('#subTitleText').text('<cfoutput>#Reporting_Surveys_Text# >> #Reporting_Response_Text#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
	});
</script>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_Survey_Response_Title#">
</cfinvoke>

<cfif StructKeyExists(URL, 'INPBATCHID')>
	<cfset INPBATCHID = URL.INPBATCHID>
<cfelse>
	<cfoutput><h4 class="err">You don't have permission to view report!</h4></cfoutput>
	<cfabort>
	<cfexit>	
</cfif>
<cfif StructKeyExists(URL, 'collectionMethod')>
	<cfset collectionMethod = URL.collectionMethod>
<cfelse>
	<cfset collectionMethod = 'Web Link'>
</cfif>
<!--- Create survey object and get export data --->
<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />

<cfset exportData = ArrayNew(1)>
<cfif collectionMethod EQ "Web Link" OR collectionMethod EQ "">
	<cfset answersData = surveyObj.ReadSurveyAnswers(INPBATCHID)>

	<cfif answersData.RXRESULTCODE NEQ 1>
		<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
		<cfabort>
		<cfexit>
	</cfif>
	<cfset surveyData = surveyObj.ReadXMLEMAIL(INPBATCHID)>
	<cfif surveyData.RXRESULTCODE NEQ 1>
		<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
		<cfabort>
		<cfexit>
	</cfif>
	<cfset QArray = surveyData.ARRAYQUESTION>
	<cfset QAnswerArray = answersData.ARRAYQUESTION>
	
	
	<cfset i = 1>
	<cfloop array="#QArray#" index="quest">
		<!--- Count answers --->
		<cfset ansStruct = StructNew()>
		<cfset QTitle = quest.TEXT>
		<cfif quest.TYPE EQ "SHORTANSWER">
			<cfset QID = quest.ID>
			<cfset QType = 3>
			<cfset ansStruct.Qtype = 3>
			<cfset ANSWERS = ArrayNew(1)>
		<cfelseif quest.TYPE EQ "ONESELECTION">
			<cfset QID = quest.ID>
			<cfset QType = 1>
			<cfset ansStruct.Qtype = 1>
			<cfset ANSWERS = quest.ANSWERS>
		</cfif>
		<cfset ansStruct.QID = 0>
		<cfset ansStruct.TotalResponse = 0>
		<cfset ansStruct.TotalSkipped = 0>
		<cfset ansStruct.answerArray = ArrayNew(1)>	
		
		<cfif QType EQ 1>
			<cfloop array="#ANSWERS#" index="ans">
				<cfset ansStruct.answerArray[ans.ID] = StructNew()>
				<cfset ansStruct.answerArray[ans.ID].TEXT = ans.TEXT>
				<cfset ansStruct.answerArray[ans.ID].TotalResponse = 0>
			</cfloop>
		</cfif>
		
		<cfset ansIndex = 1>	
		<cfloop array="#QAnswerArray#" index="answer">
			<cfif QType EQ answer.CP AND QID EQ answer.QUESTIONID>
				<cfset ansStruct.QID = answer.QUESTIONID>
				<cfif answer.ANSWER NEQ '-1'>
					<cfset ansStruct.TotalResponse = ansStruct.TotalResponse + 1>
					<!--- Short answer --->
					<cfif QType EQ 3>
						<cfset ansStruct.answerArray[ansIndex] = answer.ANSWER>
						<cfset ansIndex = ansIndex + 1>
					<!--- single answer --->
					<cfelseif QType EQ 1>
						<cfset ansStruct.answerArray[#answer.ANSWER#].TotalResponse = ansStruct.answerArray[#answer.ANSWER#].TotalResponse + 1>
					</cfif>
				<cfelse>
					<cfset ansStruct.TotalSkipped = ansStruct.TotalSkipped + 1>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfset ansStruct.Title = QTitle>
		
		<cfset exportData[i] = ansStruct>
		<cfset i = i + 1>
	</cfloop>
<cfelseif collectionMethod EQ "Voice">
	<cfset answersData = surveyObj.ReadSurveyVoiceAnswers(INPBATCHID)>

	<cfif answersData.RXRESULTCODE NEQ 1>
		<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
		<cfabort>
		<cfexit>
	</cfif>
	<cfset exportData = ArrayNew(1)>
	<cfset surveyData = surveyObj.ReadXMLVOICE(INPBATCHID)>

	<cfif surveyData.RXRESULTCODE NEQ 1>
		<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
		<cfabort>
		<cfexit>
	</cfif>
	<cfset QArray = surveyData.ARRAYQUESTION>
	<cfset QAnswerArray = answersData.ARRAYQUESTION>
	
	
	<cfset i = 1>
	<cfloop array="#QArray#" index="quest">
		<!--- Count answers --->
		<cfset ansStruct = StructNew()>
		<cfset QTitle = quest.QUESTION_TEXT>
		<cfif quest.QUESTION_TYPE EQ "rxt3">
			<cfset QID = quest.QID>
			<cfset QType = 3>
			<cfset ansStruct.Qtype = 3>
			<cfset ANSWERS = ArrayNew(1)>
		<cfelse>
			<cfset QID = quest.QID>
			<cfset QType = 1>
			<cfset ansStruct.Qtype = 1>
			<cfset ANSWERS = quest.QUESTION_ANS_LIST>
		</cfif>
		<cfset ansStruct.QID = 0>
		<cfset ansStruct.TotalResponse = 0>
		<cfset ansStruct.TotalSkipped = 0>
		<cfset ansStruct.answerArray = ArrayNew(1)>	

		<cfif QType EQ 1>
			<cfset index = 1>
			<cfloop array="#ANSWERS#" index="ans">
				<cfset ansStruct.answerArray[index] = StructNew()>
				<cfset ansStruct.answerArray[index].TEXT = ans.TEXT>
				<cfset ansStruct.answerArray[index].TotalResponse = 0>
				<cfset index = index + 1>
			</cfloop>
		</cfif>
		
		<cfset ansIndex = 1>	

		<cfloop array="#QAnswerArray#" index="answer">
			<cfif QType EQ answer.CP AND QID EQ answer.QUESTIONID>
				<cfset ansStruct.QID = answer.QUESTIONID>
				<cfif answer.ANSWER NEQ '-1'>
					<cfset ansStruct.TotalResponse = ansStruct.TotalResponse + 1>
					<!--- Short answer --->
					<cfif QType EQ 3>
						<cfset ansStruct.answerArray[ansIndex] = answer.ANSWER>
						<cfset ansIndex = ansIndex + 1>
					<!--- single answer --->
					<cfelseif QType EQ 1>
						<cfset AnswerIndex = GetIndexByAnswer(answer.ANSWER, ANSWERS)>
						<cfset ansStruct.answerArray[#AnswerIndex#].TotalResponse = ansStruct.answerArray[#AnswerIndex#].TotalResponse + 1>
					</cfif>
				<cfelse>
					<cfset ansStruct.TotalSkipped = ansStruct.TotalSkipped + 1>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfset ansStruct.Title = QTitle>
		
		<cfset exportData[i] = ansStruct>
		<cfset i = i + 1>
	</cfloop>
	
	<cffunction name="GetIndexByAnswer" hint="Get Index By Answer">
		<cfargument name="INPANSWER" TYPE="string" />
		<cfargument name="ANSWERLIST"/>
		<cfloop from="1" to="#arrayLen(ANSWERLIST)#" index="index">
			<cfif ANSWERLIST[index].TEXT EQ INPANSWER>
				<cfreturn index>
			</cfif>
		</cfloop>
		<cfreturn 1>
	</cffunction>
</cfif>

<cfoutput>
	
	<h2>Survey Response Details</h2><br />
	<div style="clear:both;">
		Collection Method: <select id="cboCollectionMethod" onchange="SelectCollectionMethod()">
			<option selected="true" value="Web Link">Web Link</option>
			<option value="Email">Email</option>
			<option value="Website">Website</option>
			<option value="Social media">Social media</option>
			<option value="Voice">Voice</option>		
		</select>
	</div>
	<cfset statisticSur = surveyObj.GetSurveyResponseStatistic(INPBATCHID, collectionMethod)>
	<div class="report_summary_header">
		<div class="response_summary">
			<span class="reponse_summary_label">Response Summary</span>
		</div>
		<div style="float: right; color: blue;">
			<div style="float: right; clear: both;">
				<span>Total Start: 
					<cfif collectionMethod EQ "Web Link" OR collectionMethod EQ "">
						<cfoutput>#statisticSur.TotalStart#</cfoutput>
					<cfelse>0
					</cfif></span>
			</div>
			<div style="float: right; clear: both;">
				<span>Total Finish: 
					<cfif collectionMethod EQ "Web Link" OR collectionMethod EQ "">
						<cfoutput>#statisticSur.TotalFinish#</cfoutput>
					<cfelse>0
					</cfif></span>
			</div>
		</div>
	</div>
	
	<div style="width: 500px;">
		<div style="width: 200px; text-align: center; float: right">
			Response Count
		</div>
	</div>
	<cfset QIndex = 1>
	<cfloop array="#exportData#" index="exportRow">
		<div class="q_box">
			<div class="question_title">
				#QIndex#. #exportRow.Title#
			</div>
			<cfset QIndex = QIndex + 1>
			<cfif exportRow.QType EQ 1>
				<div class="answer_details">
				<cfloop array="#exportRow.ANSWERARRAY#" index="ans">
					<div style="width: 290px; float: left; margin-left:10px;">
						<span class="q_txt">#ans.TEXT# : </span>
					</div>
					<div style="width: 200px; text-align: center; float: right">
						<span class="q_res">#ans.TOTALRESPONSE#</span>
					</div>

				</cfloop>
				</div>
			<cfelseif exportRow.QType EQ 3>
				<!---
				<cfloop array="#exportRow.ANSWERARRAY#" index="ans">
					<span class="q_txt">#ans#</span><br />
				</cfloop>
				--->
				<cfoutput>
					<cfif collectionMethod EQ "Web Link" OR collectionMethod EQ "">
						<div style="width: 500px; clear: both;margin-left:10px;">
							<a href="surveyResponseDetail?inpbatchid=#INPBATCHID#&QID=#exportRow.QID#">View Response</a>
						</div>
					<cfelse>
						<div class="answer_details">
							<a href="##">View Response</a>
						</div>
					</cfif>
				</cfoutput>
			</cfif>
			<div class="question_summary">
				<div class="summary_details">
					<span class="total_count_label">Total Responses : </span><span class="q_res">#exportRow.TOTALRESPONSE#</span><br />
					<span class="total_count_label">Total Times Skipped : </span><span class="q_res">#exportRow.TOTALSKIPPED#</span>
				</div>
			</div>
			<cfif QIndex - 1 LT ArrayLen(exportData)>
				<div class="question_gap"></div>
			</cfif>
		</div>
	</cfloop>
</cfoutput>

<script>
	function SelectCollectionMethod() {
		window.location = "surveyAnalytics?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>&collectionMethod=" + $('#cboCollectionMethod').val();
	}
	$(function () {
		$('#cboCollectionMethod').val('<cfoutput>#collectionMethod#</cfoutput>')
	});
	$().ready(function (){
		$('#subTitleText').text('<cfoutput>#Reporting_Surveys_Title# >> #Reporting_Survey_Response_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
	});
</script>