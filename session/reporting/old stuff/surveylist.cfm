<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Reporting_Surveys_Title#">
</cfinvoke>

<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Reporting_Surveys_Title#">
</cfinvoke>

<cfsilent>
	<cfimport taglib="../lib/grid" prefix="mb" />
</cfsilent>
<cfoutput>
	<cfscript>
		htmlOption = '<a href="surveyAnalytics?inpbatchid={%BATCHID_BI%}"><img title="Responses" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks response_16_16"></a>';
/*		htmlOption = htmlOption & '<a href="surveyVoiceResponse?inpbatchid={%BATCHID_BI%}"><img title="Voice Responses" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks response_16_16"></a>';*/
		htmlOption = htmlOption & '<a href="exportCsv?inpbatchid={%BATCHID_BI%}"><img title="Save" src="../../public/images/dock/blank.gif" class="img16_16 ListIconLinks save_16_16"></a>';
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Survey ID','Survey Name','Survey Type','Total Responses ','Total Voice', 'Total Online','Expiration Date', 'Actions']>	
	<cfset arrColModel = [			
			{name='BATCHID_BI', width='8%'},
			{name='Description', width='25%'},
			{name='SurveyType', width='12%'},
			<!--- Dont have data for total voice - hard code this value to total online --->
			{name='TotalOnlineAnswer', width='10%'}, <!--- Total Responses --->
			{name='TotalVoiceAnswer', width='8%'}, <!--- Total Voice --->
			{name='TotalOnlineAnswer', width='8%'},	
			{name='ExpDateFormat', width='20%'},
			{name='FORMAT', width='20%', format = [htmlFormat]}] >	
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Survey ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='Survey Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Survey Type', TYPE='CF_SQL_VARCHAR', FILTERTYPE="LIST", LISTOFVALUES = 
														[{VALUE = 'ONLINE', DISPLAY="ONLINE"},
														{VALUE = 'VOICE', DISPLAY="VOICE"}, 
														{VALUE = 'VOICE & ONLINE', DISPLAY="VOICE & ONLINE"}] },
			{VALUE='4', DISPLAY='Total Responses', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='5', DISPLAY='Total Voice', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='6', DISPLAY='Total Online', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='7', DISPLAY='Expiration Date', TYPE='CF_SQL_DATE', FILTERTYPE="DATE"}
		]
	>
	<cfset 
		filterFields = [
			'bas.BatchId_bi',
			'bas.DESC_VCH',
			'SurveyType',
			'CallResult.TotalOnlineAnswer',
			'CallResult.TotalVoiceAnswer',
			'TotalOnlineAnswer',
			"EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')"
		]	
	>	

	<mb:table 
		component="#SessionPath#.cfc.ire.marketingSurveyTools" 
		method="GetReportingSurveys" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	  	FilterSetId="DefaulFSListSurvey"
		>
	</mb:table>
</cfoutput>

<script>
	$().ready(function (){
		$('#subTitleText').text('<cfoutput>#Reporting_Surveys_Text# </cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Reporting_Title#</cfoutput>');
	});
</script>

<!--- 

<cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">

<cfoutput>
<style>
@import url('#rootUrl#/#PublicPath#/css/campaign.css');
</style>
</cfoutput>
<script>
	<cfoutput>
		<cfif _search NEQ "">
			var param = '&_search=#_search#&nd=#nd#&rows=#rows#&page=#page#&sidx=#sidx#&sord=#sord#';
		<cfelse>
			var param = '';	
		</cfif>
	</cfoutput>	
	$(function() {
		$("#ListSurveyList").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc?method=GetReportingSurveys&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1' + param,
			datatype: "json",
			height: 475,
			colNames:['Batch Id','Survey Description','Expire Date', 'Options'],
			colModel:[			
				{name:'SurveyListId_int',index:'SurveyListId_int', width:100, editable:false, resizable:false},
				{name:'Desc_vch',index:'Desc_vch', width:320, editable:true, resizable:false},
				{name:'Expire_Dt',index:'Created_dt', width:125, editable:false, resizable:false},	
				{name:'Options',index:'Options', width:250, editable:false, align:'center', sortable:false, resizable:false}
			],
			rowNum:15,
		//   	rowList:[10,20,30,40],
			mtype: "POST",
			pager: $('#pagerDiv'),
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current Survey Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			physicalPaging:true,
			sortname: 'Created_dt',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",	
			multiselect: false,
			loadComplete: function(data){ 		
					$(".report_details").hover(function() { 
						$(this).addClass("ui-state-hover"); 
					}, function() { 
						$(this).removeClass("ui-state-hover"); 
					});
			 		
			 		$(".report_details").click(function() { 
			 			ViewReportToolDialogSurveyList($(this).attr('rel'),$(this).attr('rel2')); 
		 			});
			 		
					$(".report_responses").hover(function() { 
						$(this).addClass("ui-state-hover"); 
					}, function() { 
						$(this).removeClass("ui-state-hover"); 
					});
			 		
			 		$(".report_responses").click(function() { 
			 			ViewAnalyticsToolDialogSurveyList($(this).attr('rel'),$(this).attr('rel2')); 
		 			});
			  
					$(".report_errors").hover(function() { 
						$(this).addClass("ui-state-hover"); 
					}, 
					function() { 
						$(this).removeClass("ui-state-hover"); 
					});
			 		
			 		$(".report_errors").click( function() { 
			 			ViewErrorsResport($(this).attr('rel')); 
		 			});
			},	
			editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc?method=UpdateSurveyListDescExpDate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		
			<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
			  jsonReader: {
				  root: "ROWS", //our data
				  page: "PAGE", //current page
				  total: "TOTAL", //total pages
				  records:"RECORDS", //total records
				  cell: "", //not used
				  id: "0" //will default second column as ID
				  }	
		});
		
		$('#pagerDiv').hide();
	
		$("#refresh_List_BatchSurveyList").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#ListSurveyList_toppager_left").html("<b>Survey</b><img id='refresh_List_BatchSurveyList' class='ListIconLinks' src='../../public/images/icons16x16/Refresh_16x16.png' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
		$("#refresh_List_BatchSurveyList").click(function() { gridReloadSurveyList(); return false; });
	});

	function gridReloadSurveyList(){
			var ParamStr = '';
			var CurrSORD =  $("#ListSurveyList").jqGrid('getGridParam','sord');
			var CurrSIDX =  $("#ListSurveyList").jqGrid('getGridParam','sidx');
			var CurrPage = $('#ListSurveyList').getGridParam('page'); 
			var notes_mask = $("#ListSurveyList #search_notes").val();
			
			if(notes_mask) ParamStr += '&notes_mask=' + notes_mask;
					
			if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
				
			if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
			
		$("#ListSurveyList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc?method=GetSurveyMCContent&returnformat=json&queryformat=column&inpSocialmediaFlag=0&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:CurrPage}).trigger("reloadGrid");
	}
	
	<!--- view reports tools --->
	function ViewReportToolDialogSurveyList(BATCHID, surveyDesc) {
		var ParamStr = '';
		if(typeof(BATCHID) != "undefined" && BATCHID != "")	{				
			ParamStr = '?inpbatchid=' + encodeURIComponent(BATCHID);
		} 
		
		window.location = 'surveyReports' + ParamStr;
	}
	
	<!--- view Analytics tools --->
	function ViewAnalyticsToolDialogSurveyList(BATCHID, surveyDesc) {			
		var ParamStr = '';
		if(typeof(BATCHID) != "undefined" && BATCHID != "")	{				
			ParamStr = '?inpbatchid=' + encodeURIComponent(BATCHID);
		} 
		
		window.location = 'surveyAnalytics' + ParamStr;	
	}
	
	function ViewErrorsResport(BATCHID) {			
		var ParamStr = '';
		if(typeof(BATCHID) != "undefined" && BATCHID != "")	{				
			ParamStr = '?inpbatchid=' + encodeURIComponent(BATCHID);
		} 
		
		window.location = 'surveyErrors' + ParamStr;	
	}
</script>
     
	    
<cfoutput>
    <div id="tab-panel-BatchesSurveyList" style="position:relative; overflow: hidden;">   
        <div style="table-layout:fixed; min-height:475px; height:475px;">
            <div id="pagerDiv"></div>
            <div style="text-align:left;">
	            <table id="ListSurveyList"></table>
            </div>    
        </div>
	</div>        
</cfoutput>    
 --->

