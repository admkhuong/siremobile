

<cfparam name="INPBATCHID" default="-1">
<cfparam name="INPUUID" default="-1">
<script src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/highcharts/highcharts.js"></script>
<script src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/highcharts/modules/exporting.js"></script>
<!---
<script type="text/javascript">
	define(
			"friends.data",
			[ /** no dependencies. **/ ],
			[
				{
					"id": 1,
					"qtitle": "Question 1",
					"age": 35
				},
				{
					"id": 2,
					"qtitle": "Question 2",
					"age": 38
				},
				{
					"id": 3,
					"qtitle": "Question 3",
					"age": 29
				},
				{
					"id": 4,
					"qtitle": "Question 4",
					"age": 37
				}
			]
		);
		require(
			[
				"friends.data",
				"./text!<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Survey/dsp_result_oneselect"
			],
			function( friendsData, friendHtml ){
 
				// Now that we've loaded the template and data, let's
				// make sure we're waiting for the DOM-ready event.
				// Since we loaded the jQuery/RequireJS library, we
				// can use jQuery to get the DOM-ready.
				$(function(){
 
					// Get the friends list.
					var friendsList = $( "ul.friends" );
 
					// Get the template node (which we will use to
					// create the friends.
					var template = $( friendHtml );
 
					// Map the array of friends' data into an array
					// of template nodes to be added to the DOM.
					var buffer = $.map(
						friendsData,
						function( friendData, index ){
 
							// Close the template.
							var friend = template.clone();
 
							// Set the name.
							friend.find( "span.qtitle" ).text(
								friendData.qtitle
							);
 
							// Set the age.
							friend.find( "span.age span.value" ).text(
								friendData.age
							);
 
							// Return the raw node.
							return( friend.get() );
 
						}
					);
 
					// Insert the friend template DOM node buffer
					// into the page.
					friendsList.append( buffer );
 
				});
 
			}
		);
</script>
--->
<script>
	$(function () {
		document.title = "Survey Details Reports";
		$("#loadingDlg").show();
		var INPUUID = <cfoutput>#INPUUID#</cfoutput>
		var INPBATCHID = <cfoutput>#INPBATCHID#</cfoutput>
		<!--- var arrCallResult = new Array(1);
		arrCallResult = getCallResult(); --->
    	var data = '';
    	data = [['Email',   45.0],['SMS',   22.8], ['VOICE',    15.4], ['web',     16.8]];
    	data = loadDataAverage(INPUUID,INPBATCHID);
		$("#qtitle0").click(function () {
			alert('a1234');
		});
		
		<!--- createChart('surveyChart','pie',data); --->

	});
	function reportEachQuestion(LOCALOUTPUT) {

		html = '';
		html += '<div class="survey-report">';
		for(i=0;i<LOCALOUTPUT.length;i++) {
			html += '<div class="qListContent">';
			html += '<div class="qtitle" id="qtitle'+i+'">'+ (i+1) + '.  ' +LOCALOUTPUT[i].NAME +'</div>';
			
			switch(LOCALOUTPUT[i].TYPE) {
				case 'MATRIXONE':
					html += '<div id="qChart'+i+'" class="QuestionChart"></div>';
					break;
				default:
					html += '<div id="qChart'+i+'" class="QuestionChart"></div>';
					break;
			}
			html += '</div>';
		}
		html += '</div>';
		return html;
	}
	<!--- open report each quesion --->
	var CreateReportEachQuestionDialog = 0;
	function reportEachQuestion1(INPBATCHID,INPUUID,QUESTIONID) {
		if(CreateReportEachQuestionDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateReportEachQuestionDialog.remove();
			CreateReportEachQuestionDialog = 0;
			
		}
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
		var ParamStr = '';
		if(typeof(INPBATCHID) != "undefined" && INPBATCHID != "")	{				
			ParamStr = '?inpbatchid=' + encodeURIComponent(INPBATCHID);
		} else {
			INPBATCHID = 0;
		}
		if(typeof(QUESTIONID) != "undefined" && QUESTIONID != "")	{				
			ParamStr += '&QUESTIONID=' + encodeURIComponent(QUESTIONID);
		} else {
			QUESTIONID = 0;
		}
		if(typeof(INPUUID) != "undefined" && INPUUID != "")	{				
			ParamStr += '&INPUUID=' + encodeURIComponent(INPUUID);
		} else {
			INPUUID = 0;
		}
		
		CreateReportEachQuestionDialog = $('<div></div>').append($loading.clone());
		CreateReportEachQuestionDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Survey/dsp_SurveyReportEachQuestion' + ParamStr)
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
			    title: 'Reports Each Question ',
				close: function() {   $(this).dialog('destroy'); $(this).remove(); },
				width: 1000,
				height: 'auto',
				position: 'top',
			    closeOnEscape: false, 
			    buttons:
				[
					{
						text: "Close",
						click: function(event)
						{
							 CreateReportEachQuestionDialog.remove();	
							 return false; 
						}
					}
				]

			});
		
		CreateReportEachQuestionDialog.dialog('open');	
		return false;
		
	}
	
	function loadDataAverage(INPUUID,INPBATCHID) {
		data = ''
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc?method=getDataAverageSurvey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		async: false,
		data:  { 
			INPBATCHID : INPBATCHID,
			INPUUID : INPUUID
		},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{

				<!--- Alert if failure --->
					var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');																		
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							var createPreviewResult = '';
							createPreviewResult = $('<div></div>').append($loading.clone());
							if(CurrRXResultCode > 0)
							{				 								
								<!--- data = d.DATA.AVERAGE[0]; --->
								createChart('surveyChart','pie',d.DATA.AVERAGE[0]);
								$("#loadingDlg").hide();
								data = d.DATA.AVERAGE[0];	
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Survey has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
					$("#loadingDlg").hide();
								
			} 		
			
		});
		if (data != '') {
			return data;
		}
		return false;
	}
	function getCallResult() {
		var arrCallResult = new Array(1);
		INPBATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
		INPUUID = <cfoutput>#INPUUID#</cfoutput>;
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/SurveyTools.cfc?method=getCallResult&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
			INPUUID : <cfoutput>#INPUUID#</cfoutput>
		},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');																		
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							var createPreviewResult = '';
							createPreviewResult = $('<div></div>').append($loading.clone());
							if(CurrRXResultCode > 0)
							{				 
								var ParamStr = '';
								<!--- createPreviewResult.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Survey/dsp_previewresult' + ParamStr);

								
								$(".survey-report").append(createPreviewResult);--->
								if (d.DATA.LOCALOUTPUT[0].length >= 1) {
									//html = reportEachQuestion(d.DATA.LOCALOUTPUT[0]);
									html = '';
									html += '<div class="survey-report">';
									LOCALOUTPUT = d.DATA.LOCALOUTPUT[0];
									for(i=0;i<LOCALOUTPUT.length;i++) {
										html += '<div class="qListContent">';
										html += '<div class="qtitle" id="qtitle'+i+'" rel="'+i+'">'+ (i+1) + '.  ' +LOCALOUTPUT[i].NAME +'</div>';
										switch(LOCALOUTPUT[i].TYPE) {
											case 'MATRIXONE':
												html += '<div id="qChart'+i+'" class="QuestionChart"></div>';
												break;
											default:
												html += '<div id="qChart'+i+'" class="QuestionChart"></div>';
												break;
										}
										html += '</div>';
									}
									html += '</div>';
								} else {
									html = 'No data display';
								}
								
								$("#surveyChart").html('');
								$("#surveyChart").append(html);
								$("div.qtitle").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
								$("div.qtitle").click(function() {
									reportEachQuestion1(INPBATCHID,INPUUID,$(this).attr("rel"));
								});
								
								$("#loadingDlg").hide();
								return d.DATA.LOCALOUTPUT;	
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Survey has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
					$("#loadingDlg").hide();
								
			} 		
			
		});
		return false;
	}

		function reportType(type) {
			
			if (type == 'all') {
				createChart('surveyChart','pie',data);
			} else {
				html = 'Hello kity';
				
				arrCallResult = getCallResult();
			}
			return ;
		}
		
		function chartType(type) {
			createChart('surveyChart',type,data);
			return false;
		}
		
		function createChart(areaRender,type,data) {
			
			if (type == null) {
				type = 'pie';
			}
			if (type == '') {
				type = 'pie';
			}
		//data = [['Email',   45.0],['SMS',   22.8], ['VOICE',    15.4], ['web',     16.8]];
		if (data == null) {
			data = [['Email',   45.0],['SMS',   22.8], ['VOICE',    15.4], ['web',     16.8]];
		}
        chart = new Highcharts.Chart({
            chart: {
                renderTo: areaRender,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Voice Survey Tools'
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
            series: [{
                type: type,
                name: 'Report Survey',
                data: data
            }]
        });
		}
		</script>
<cfoutput>
	
</cfoutput>
	<ul class="friends">
		<!-- To be populated dynamically. -->
	</ul>
<div class="survey-report">

</div>
<div class="charttype">
	Type type : 
	<select name="reporttype" onchange="reportType($(this).val())">
		<option value="all">All</option>
		<option value="eachquestion">each question</option>
	</select>
	Chart type : 
	<select name="charttype" onchange="chartType($(this).val())">
		<option value="pie">Pie chart</option>
		<option value="bar">Bar chart</option>
		<option value="column">Column chart</option>
		<option value="line">Line chart</option>
		<option value="area">Area chart</option>
	</select>

</div>
<div id="surveyChart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

<div id="loadingDlg" style="display:inline;">
     <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
 </div>
<!--- <p>Display report how many user use for SMS,VOICE,EMAIL,.. </p> --->
<!--- 
7. You wrote - �Analyse the responses % for each response to each question, Partial completed, % complete, and other reports��
Will we analyse reponses for each Survey or for group of Survey? - Both
What about other reports? Can you clarify this task? � Google is your friend. What do other survey sites offer for reports? Beyond the most basic and obvious reporting examples I am looking for you guys to give me sample
 --->
