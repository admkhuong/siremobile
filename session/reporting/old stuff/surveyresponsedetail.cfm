<cfif StructKeyExists(URL, 'INPBATCHID')>
	<cfset INPBATCHID = URL.INPBATCHID>
<cfelse>
	<cfoutput><h4 class="err">You don't have permission to view detail response!</h4></cfoutput>
	<cfabort>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">	
</cfif>
<cfif StructKeyExists(URL, 'QID')>
	<cfset CurrentQID = URL.QID>
<cfelse>
	<cfoutput><h4 class="err">You don't have permission to view detail response!</h4></cfoutput>
	<cfabort>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>
<!--- Create survey object and get export data --->
<cfset surveyObj = CreateObject("component", "#LocalSessionDotPath#.cfc.ire.marketingSurveyTools") />
<cfset answersData = surveyObj.ReadSurveyAnswers(INPBATCHID)>
<cfif answersData.RXRESULTCODE NEQ 1 OR ArrayIsEmpty(answersData.ARRAYQUESTION)>
	<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
	<cfabort>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>
<cfset surveyData = surveyObj.ReadXMLEMAIL(INPBATCHID)>
<cfif surveyData.RXRESULTCODE NEQ 1 OR ArrayIsEmpty(answersData.ARRAYQUESTION)>
	<cfoutput><h4 class="err">Cannot access database server or There're no data to view.</h4></cfoutput>
	<cfabort>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>
<cfset QAnswerArray = answersData.ARRAYQUESTION>

<cfset answerArray = ArrayNew(1)>	
<cfset ansIndex = 1>	
<cfloop array="#QAnswerArray#" index="answer">
	<cfif CurrentQID EQ answer.QUESTIONID>
		<cfif answer.ANSWER NEQ '-1'>
			<!--- Short answer --->
			<cfset answerArray[ansIndex] = answer.ANSWER>
			<cfset ansIndex = ansIndex + 1>
		</cfif>
	</cfif>
</cfloop>
<!--- Add log --->
<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
	<cfinvokeargument name="userId" value="#session.userid#">
	<cfinvokeargument name="moduleName" value="Report Survey">
	<cfinvokeargument name="operator" value="View detail response survey #INPBATCHID# Question #CurrentQID#">
</cfinvoke>
<cfoutput>
	
	<h2>Detailed response listing for Question #CurrentQID#</h2><br />
		<div class="q_box">
		<h3><strong>Responses</strong></h3><br />
		<ul>
		<cfloop array="#answerArray#" index="ans">
			<li><span class="q_txt">#ans#</span></li>
		</cfloop>
		</ul>
		</div>
</cfoutput>