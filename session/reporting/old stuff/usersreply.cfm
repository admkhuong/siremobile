<cfsetting showdebugoutput="no">

<cflock scope="session" timeout="20">
    <cfset VARIABLES.userId = session.userid />
</cflock>

<cfquery name="keywords" datasource="10.11.0.130">
    select t1.batchid_bi,
    	   t2.keyword_vch 
      FROM simpleobjects.batch t1
      join sms.keyword t2
        on t1.batchId_bi = t2.batchId_bi
     where userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#VARIABLES.userId#" />
     order by t2.keyword_vch
</cfquery>

<div class="filterDiv">
<cfform name="filter">
	<div>
    <table border="0" align="center">
        <tr>
            <td style="font-weight:bold"><label>KeyWord</label></td>
            <td><label><cfselect query="keywords" name="batchId" display="keyword_vch" value="batchid_bi" id="batchId" style="width:150px">
            </cfselect></label></td>
            
        </tr>
        <tr>
            <td style="font-weight:bold">
                From
            </td>
            <td>
                <input type="text" name="from" id="from" />
            </td>
            <td style="font-weight:bold">
                To
            </td>
            <td>
                <input type="text" name="to" id="to" />
            </td>
        </tr>
        <tr>
            <td></td><td></td>
            <td>
                
            </td>
            <td align="right">
                <cfinput name="submitchart" id="submitchart" type="button" value="Get Chart">
                <cfinput name="submitxls" id="submitxls" type="submit" value="Import xls">
            </td>    
        </tr>
    </table>
    </div>
    <div style="width:100%" id="chartContent">
    
    </div>
</cfform>
</div>
 <cfoutput>  
 <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatablepage.css">
<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatable.css">    
<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datatables/js/jquery.datatables.min.js"></script> 
</cfoutput>

<script type="text/javascript">
	$(document).ready(function()  
	{
			
		$( "#from" ).datepicker({
			  changeMonth: true,
			  changeYear: true,
			  dateFormat: 'mm-dd-yy',
			  onClose: function( selectedDate ) {
				$( "#to" ).datepicker( "option", "minDate", selectedDate );
				var date2 = $( "#from" ).datepicker('getDate');
			  }
			});
			$( "#to" ).datepicker({
			  changeMonth: true,
			  changeYear: true,
			  dateFormat: 'mm-dd-yy',
			  onClose: function( selectedDate ) {
				  var date2 = $( "#to" ).datepicker('getDate');
				$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			  }
			});
			
			GetChart();
	});
	
	var GetChart = function(){
		$('#submitchart').click(function(){
			$.ajax({
					  url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetUsersReply&returnformat=json&queryformat=column&batchId="+$('#batchId').val()+'&from='+$('#from').val()+'&to='+$('#to').val(),
					  context: document.body
					}).done(function(data) {
						if(data.trim() == 'Sorry No question in batch'){
							$('#chartContent').html('No question in the batch');
						}else{
							var parsedData = $.parseJSON(data)
							
							if(parsedData == "No Subscriber intraction in this time frame"){
								$('#chartContent').html(parsedData);
							}else{
								var html = '';
								if(parsedData.oneSelection != ""){
									var html = '<div><h2>One Selection</h2>'+parsedData.oneSelection+'</div>';
								}
								
								if(parsedData.shortAnswer != ""){
									html = html + '<div style="margin-top:5%"><h2>Short Answers</h2>'+parsedData.shortAnswer+'</div>';	
								}
								
								$('#chartContent').html(html);
								
								if(parsedData.shortAnswer != ""){
									$('.shortAnswer').dataTable();
								}
							}
							
						}
						//console.log(data);
					});
		});
	};
</script>


