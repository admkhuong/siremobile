<cfparam name="inpBatchIdList" default="">
<cfparam name="inpReportingSectionType" default="1">



<!--- Check for Reporting Permission - if not send to home page --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<!---
<cfif NOT permissionObject.havePermission(Reporting_Title).havePermission>
	
    <cfoutput>#Permission_Fail#</cfoutput>
    <cfabort />
</cfif>
--->

<cfoutput>
	
	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>--->
	
    <!--- Latest bootstrap styled version of datatables --->
 	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
     
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
    
	<!--- Include Required Prerequisites for date range picker http://www.daterangepicker.com/ or https://github.com/dangrossman/bootstrap-daterangepicker --->
    <script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/daterangepicker/moment.min.js"></script>
     
    <!--- Include Date Range Picker --->
    <script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/daterangepicker/daterangepicker.css" />

    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    
	<!--- After version 3.1.0 you need to include each chart type as separate js file--->
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/pie.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>"></script>
    <script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amstock.js</cfoutput>"></script>
	
    <script src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
                       
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>        
		  
       
	<style>
	
					
	</style>
</cfoutput>

<cfset BATCHTITLE = ""> 

<!--- Validate user has access to all Batches in list--->
<cfloop list="#inpBatchIdList#" index="INPBATCHID" delimiters=",">

	<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarGetBatch">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
    </cfinvoke> 
    
    <!---<cfdump var="#RetVarGetBatch#"><cfabort>--->
    
    <cfif RetVarGetBatch.RXRESULTCODE LT 0>
        
        <div>
            No campaign data found for <cfoutput>#INPBATCHID#</cfoutput> 
        </div>
    
        <cfabort/>
    
    </cfif>
                
    <cfset BATCHTITLE = RetVarGetBatch.DESC> 
    
</cfloop>



<!--- REPORT FORM STYLES --->
<style>

	.ResetChartOption
	{
		position:relative;
		top:-20px;
		right:10px;
		z-index:10000;	
		cursor:pointer;	
		text-decoration:none;
		font-size: 12px;
	    text-align: right;
	}

	.ResetChartOption:hover
	{
		text-decoration:underline;
	}
	
	.messages-lbl{
		color: #0888D1 !important;
		font-family: "Verdana" ;
		font-weight: bold ;
		line-height: 14px ;
		margin-left: 20px ;
		padding-bottom: 20px ;
		padding-top: 30px ;
		font-size: 16px;
	}
	
	.caller_id{
		padding-top:0;
	}
	
	#content-AAU-Admin{
		background-color:#fff;
		border:1px solid #ccc;
		border-radius:4px;
	}
	
	#divAgent{
		margin-bottom:90px;
	}
	
	#content-AAU-Admin img{
		width:16px; 
		float:left;
		cursor:pointer;
	}
	
	#tblActiveAgent tbody{
		border-left:1px solid #ccc;
		border-right:1px solid #ccc;
	}
	
	#tblActiveAgent_paginate {
		border-left: 1px solid #CCCCCC;
		margin-top: -21px;
	}
	
	#em-name-block{
		width:70%;
		margin-left:21px;
		font-family:"Verdana" !important;
		font-size:12px !important;
	}
	
					
	.message-block .right-input{
		width:100%;
		float: left;
		border:1px solid #ccc;
		height:28px;
		border-radius: 0 4px 4px 0;
		color: #666666;
	}
	
	.message-block .right-input>span.selectmenucustom{
		height:28px!important;
		border: medium none;
		border-radius: 0;
		overflow:hidden;
	}
	
	.message-block .right-input .ui-selectmenu-status {
		line-height: 1.4em;
	}
	
	.message-block .right-input .ui-selectmenu-icon{
		margin-top: -13px;
	} 
	
	.right-input>input{
		border:none !important;
		height: 21px;
		line-height: 30px;
		width: 100%;
		background-color:#fbfbfb;
		font-family:"Verdana" !important;
		font-size:12px !important;
		color:#000 !important;
		outline: 0 none !important;
	}
	
		
	.message-block{
		margin-left:21px;
		font-family:"Verdana";
		font-size:12px;
	}
	
	
	.left-input{
		width:130px;
		float:left;
		border-color: #ccc;
		border-radius: 4px;
		border-style: solid;
		border-width: 1px;
		height:28px;
		border-radius: 4px 0 0 4px;
		color: #666666;
		line-height: 25px;
		padding-left: 10px;
	}
	
	.left-input>span.em-lbl{
		width: 106px;
		float:left;
	}
	
	.left-input>div.hide-info{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
	}
	
	.left-input>div.hide-info:hover{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
	}
	
	.info-block{
		background-color: #FFFFFF;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		left: 100px;
		padding: 6px;
		position: relative;
		top: -10px;
	}
	
	.EM-btn{
		height:30px;
		border:1px solid #ccc;
		width:128px;
		float:left;
		line-height: 28px;
		margin-bottom: 10px;
		margin-left: 21px;
		text-align: center;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1) inset, 0 1px 5px rgba(0, 0, 0, 0.25);
		border-radius:4px;
		font-family:"Verdana";
		font-size:14px;
		color:#fff;
	} 
	

	.top-input{
		border: 1px solid #CCCCCC;
		border-radius: 4px 4px 0 0;
		color: #666666;
		margin-top: 10px;
		min-height: 40px;
		padding: 10px;
		width: 505px;
	}
	
	.bottom-input{
		-moz-border-bottom-colors: #CCCCCC;
		-moz-border-left-colors: #CCCCCC;
		-moz-border-right-colors: #CCCCCC;
		-moz-border-top-colors: none;
		border-image: none;
		border-radius: 0 0 4px 4px;
		border-style: none solid solid;
		border-width: medium 1px 1px;
		color: #666666;
		width: 505px;
		float:left;
		height: 14px;
		padding: 8px 10px;
	}
	
	.bottom-input input{
		float:left;
	}
	
	.bottom-input label{
		float:left;
		margin-left:10px;
	}
	
	.bottom-input .hide-info{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	
	.bottom-input .hide-info:hover{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	

	
	.method-btn {
		background-color: #F5F5F5;
		background-image: linear-gradient(to bottom, #FFFFFF, #E6E6E6);
		background-repeat: repeat-x;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
		cursor: pointer;
		display: inline-block;
		font-size: 14px;
		line-height: 20px;
		margin-bottom: 0;
		padding: 4px 12px;
		text-align: center;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
		vertical-align: middle;
	}
	
	div.warning-block{
		border: 1px solid #FFB400;
		border-radius: 4px;
		margin-left: 20px;
		width: 93%;
		padding:16px;
		font-family:"Verdana";
		font-size:13px;
		font-style:normal;
		color:#666666;
		background-color:#fff7e5;
	}
	
	span.warning-text{
		font-weight:bolder;
	}
	
	div.warning-checkbox{
		color: #737373;
		margin-top:25px;
	}
	
	div#warning-content{
		margin-bottom:25px;
	}
	
	div#head-AAU-Admin{
		font-weight:bolder;
	}
	
	div#sent-messages-lbl{
		padding:30px 20px 24px;
		font-family:"Verdana";
		font-size:14px;
		font-weight:bolder;
		color:#0888d1;
		float:left;
	}
	
	div#btnCreateNew{
		float:right;
		margin:20px 25px 0 20px;
	}
	
	#buttonBar{
		width:100%;
	}
	
	table#tblListEMS{
		width:96%!important;
		border: 1px solid #CCCCCC;
	}	
	
		
	.tool-tip1{
		background: url("../images/info.png") no-repeat scroll 0px 0px rgba(0, 0, 0, 0);
		display: block;
		opacity: 0.4;
		width: 20px;
		height: 20px;
	}
	.tool-tip1:hover {
		opacity: 1;
	}
	
	#inpAvailableShortCode-menu{
		margin-left:-6px;
		width:381px!important;
	}
	
	.ui-selectmenu-menu ul, ui-selectmenu-item-selected ul li, .ui-state-hover ul li {
    border: 1px solid #AAAAAA !important;

	}
	
	.btn
	{   
    	padding: 0px 5px !important;
	}

	#inpAvailableShortCode-button
	{		
		outline:none;		
	}



</style>




<style type="text/css">
	
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
			
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;		
	}
			
	#ReportMenu .ui-state-active, #ReportMenu .ui-widget-content #ReportMenu .ui-state-active, #ReportMenu .ui-widget-header #ReportMenu .ui-state-active 
	{
  		background-color: #118ACF; 
		border: none;
		color: #FFFFFF !important;
		outline: medium none;	
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;	
	}

.ui-widget-content {
    background: none repeat scroll 0 0 #FFFFFF;
    border: none;
		
}

..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
  
    border: none;
    color: #313131;
    font-weight: normal;			
}

	.SideSummary
	{ 
		width:350px; 
		height:250px; 
		margin: 0 0 0 0; 
		float:left; 
		overflow-y:auto;
	}
			
	.reportingContent {
		float: none;
		width: 2000px;
		min-height: 1200px;
		background:none;
		border-radius: 8px 8px 8px 8px;	
		position:static;	
	}
	
	#tblListBatch_1_info
	{
	   width: 300px !important;
	   padding-left: 10px;
	   margin-left: 13px;
	   display: none;
	}


	#chartContent
	{
		background: #fff;
		float:left;
		width: 1200px;		
	}
		
</style>


<style>

	hr.style-five 
	{
		border: 0;
		height: 0; 
		box-shadow: 0 0 3px 1px #CCCCCC;
	}
	
	hr.style-five:after 
	{  
		content: "\00a0";  
	}

	#reportSummaryOverlay {
		background-color: #FFFFFF;
		display: none;
		left: 0;
		opacity: 0.8;
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 90;		
		background: "url(../../public/images/loading.gif) center no-repeat");		
	}
	
	.summaryAnalytics 
	{
		display: inline-block;
		margin: 15px 10px 5px 20px;
		vertical-align: top;
		width: 100%;
	}

	.reportSummaryTitle {
	    font-weight: bold;
	    text-align: right;		
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
					
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;
	}
	
		
	.ui-widget-content {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
			
	}
	
	..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
	  
		border: none;
		color: #313131;
		font-weight: normal;			
	}

	
		
	

		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 16px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
			font-size:12px;
		}
		
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 2px 4px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 2px 4px !important;
		}
		
		.paging_full_numbers {
    		line-height: 25px;
    		text-align:right;
		}

		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}
		
		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}
		
			
		.EBMDataTableWrapper
		{
			height:85%;
			min-height:260px;
			margin-top: 15px;
			overflow:auto;
		}

		.dataTables_wrapper {
			position: relative;
			clear: both;
			zoom: 1; /* Feeling sorry for IE */	
			/*overflow:auto !important;*/
			max-height:100%;						
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
			padding: 3px 10px;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
				
		.dataTable tbody > tr 
		{
		    min-height: 22px;
			height: 22px;  
			width:625px;
			max-width:625px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 22px; 
			height: 22px; 
		}
		
		.dataTable tbody > tr > td
		{ 
			white-space: nowrap; 
			overflow:hidden !important;
		}

		#tblListQueue 
		{
			<!--- width: 625px; --->			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
		#tblListResults 
		{
			<!--- width: 625px;	 --->		
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
	
	
		.tblListBatch
		{
			width: 300px !Important;			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
			border-bottom: 1px solid #CCCCCC;
		}
		
		#tblListBatch_1_info
		{
		   width: 300px !important;
		   padding-left: 10px;
		   margin-left: 13px;
		   display: none;
		}
	
	
		.dataTables_filter 
		{
			float: right;
			margin-right: 15px;
			text-align: right;
			font-size:10px;
		}

		.datatable td 
		{
		  overflow: hidden; 
		  text-overflow: ellipsis; 
		  white-space: nowrap;
		}
		
		.rsheader
		{	
			width:1345px; 
			float:left;
		}
		
		.SingleWide
		{
		 	width:650px; 
		 	height:365px;
		}
		
		.DoubleWide
		{
		 	width:1320px; 
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 18px;
			height: 18px;  
			width:417px;
			max-width:417px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 18px; 
			height: 18px; 
		}
		
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:10px;
			color:#666;
			padding: 3px 10px;
		}
				
		.dataTables_length 
		{
		    margin-left: 10px;
		}
		
		
		
table.dataTable
{
    table-layout:fixed;
	border-collapse:collapse;
}

td.dataTable
{
    overflow:hidden;
    text-overflow: elipsis;
	padding: 0 8px 0 8px;
}

th.dataTable
{
	text-align:left;
	padding: 0 8px 0 8px;	
}

thead.dataTable tr
{
	background-color: #E9E9CE;	
	
}

.alignRight { text-align: right; }
.alignLeft { text-align: left; }
.alignCenter { text-align: center; }


.DownloadLinkText {
	color: #0063DC !important;
	text-decoration: none !important;
}

.DownloadLink {
	color: #0063DC !important;
	text-decoration: none !important;
}

.excelIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.excelIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.wordIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.wordIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.pdfIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.pdfIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.dataTables_info {
    background-color: #EEEEEE;
    border-top: 2px solid #46A6DD;
    box-shadow: 3px 3px 5px #888888;
    line-height: 39px;
    margin-left: 0;
    /*padding-left: 10px;*/
    padding-left:0;
    position: relative;
    top: 0 !important;
    width: 100%;
	font-size:12px;
	text-align:left;
}


select.ebmReport
	{
		width:100%;		
	}
			

</style>

<!--- Dashboard object styles --->
<style>

	.DashboardLeftMenu 
	{
		float: left;
		min-height: 870px;
		width: 350px;
		background-color: #FFFFFF;
    	border: 1px solid #CCCCCC;
	    border-radius: 4px;
	}

	.droppableContentDashboard
	{
		box-shadow: 3px 3px 5px #888888 !important;
		<!---margin: 0 15px 15px 15px;--->
		padding: 10px 10px 300px 10px;
		width:100%;		
	<!---	width: 1210px;
		max-width: 1210px;--->
		min-height:800px;
		border:none !important;
		float:left;
	}
	
	.DashObjHeader 
	{
		background-color: #118ACF;
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;
		color: #FFFFFF;
		font-family: "Verdana";
		font-size: 14px;
		height: 28px;
		min-height: 28px;
		line-height: 28px;
		padding-left: 10px;
		position:relative;	
		cursor: move; 		
	}
	
	.DashObjHeader:hover
	{
		<!---color:#15436C;	--->	
	}
	
<!---	.DashObjHeader:after 
	{  
		content: "\f0c9";  
		display: inline-block;  
		font-family: "FontAwesome";  
		position: absolute;  
		right: 18px;  
		top: 3px;  
		text-align: center;  
		line-height: 28px;  
		color: rgba(255,255,255,.2);  
		text-shadow: 0px 0px 0px rgba(0,0,0,0);  
		cursor: move;  
	}  
	--->

	.DashBoardHeaderMenu
	{
		float: right;
		margin-right:10px;			
	}
	
	.DashBoardHeaderMenuSlim
	{
		float: right;
		margin-right:0px;			
	}
		
	.DashBoardHeaderMenu a, .DashBoardHeaderMenuSlim a
	{		
		margin-left:3px;
		color:#15436C !important; 	
	}
	
	.DashObjHeader:hover a
	{				
		color:#15436C !important; 	
	}
				
	<!--- Class used to distiguish between drop and sort objects --->	
	.DashObj
	{
		
	}
	
	.DashObjPlaceHolder
	{
		width: 575px;
		min-width: 575px;
		height:300px;
		min-height:300px;		
	}
	
	.DashBorder
	{		
    	<!---box-shadow: 3px 3px 5px #888888;--->
		margin-bottom:15px;
	}
	
	.DashObjChart
	{
		width:100%;
		height:100%;	
		position:relative;	
		box-shadow: 3px 3px 5px #888888;
	}
	
	.DashLeft
	{
		<!---float:left;--->
	}
	
	.DashRight
	{
		float:right;
	}
	
	.DashSize_Width_2
	{		
		<!---width: 272px;
		min-width: 272px;--->		
	}
	
	.DashSize_Width_3
	{		
		<!---width: 575px;
		min-width: 575px;	--->	
	}
	
	.DashSize_Width_4
	{
		<!---width: 1180px;
		min-width: 1180px;--->
	}
	
	.DashSize_Height_Tiny
	{
		height:142px;
		min-height:142px;
	}
	
	.DashSize_Height_Small
	{
		height:299px;
		min-height:299px;
	}
	
	.DashSize_Height_Med
	{
		height:615px;
		min-height:615px;
	}
	
	.DashSize_Height_Large
	{
		height:1180px;
		min-height:1180px;
	}	
	
	
	.Trans90
	{
		display:block;	
		transform: rotate(90deg);	 
		-o-transform: rotate(90deg);
		-khtml-transform: rotate(90deg);
		-webkit-transform: rotate(90deg); 
		-moz-transform: rotate(90deg);				
	}
	
	.Trans180
	{
		display:block;
		transform: rotate(180deg);	 
		-o-transform: rotate(180deg);
		-khtml-transform: rotate(180deg);
		-webkit-transform: rotate(180deg); 
		-moz-transform: rotate(180deg);		
	}
	
	.Trans270
	{
		display:block;	
		transform: rotate(270deg);	 
		-o-transform: rotate(270deg);
		-khtml-transform: rotate(270deg);
		-webkit-transform: rotate(270deg); 
		-moz-transform: rotate(270deg);		
	}

	.DashObjMenuRight
	{	
		width:40%; 
		display:inline-block; 
		float:right;	
	}
	
	<!--- text based table for sizer --->
	.DashObjSizer
	{	
	 	border:none;	
		table-layout:fixed;
		border-collapse:collapse;	
		font-size:12px;
	}
	
	.DashObjSizer td
	{	
	 	padding: 0 3px 0 3px;		
		margin: 0;
		line-height:9px;
		text-align:center;
	}
	
	.DashObjSizer td:hover
	{	
	 	background:#666666;	
	}
	
	.DashObjSizer tr
	{		 
		line-height:9px;
	}
	
	.DashObjHeaderText
	{
		width:60%; 
		overflow:hidden; 
		display:inline-block;	
		text-align:left;
		height:	100%;
	}
	
	.FormContainerX
	{		
		box-shadow: 3px 3px 5px #888888;
		text-align:left;
		height:100%;
	}
	
	.FormContainerX div
	{
		text-align:left;
		margin-bottom:.8em;
	}
	
	.FormContainerX div.row
	{
		padding-left:.8em;
		padding-right:.8em;
	}
	
	.FormContainerX label
	{
		
		margin:0;
		font-size:.8em;	
		
	}
	
	.DataTableContainerX
	{
		box-shadow: 3px 3px 5px #888888;
		height:100%;
	}
	
	.EBMDashCounterWrapper
	{		
		background-color: #8099b1; 
		text-align:center;	
		box-shadow: 3px 3px 5px #888888;	
	}

	.Absolute-Center {
	  width: 90%;
	  /*height: 60%;*/
	  overflow: auto;
	  margin: auto;
	  position: absolute;
	  top: 10; left: 0; /*bottom: 0;*/ right: 0;
	}

	.EBMDashCounterWrapper h1
	{
		color:#FFF;
		font-size:36px;		
	}
	
	.RSHighAlert
	{
		background-color: rgba(255,0,0,0.7); 
		font-size:36px;		
	}
	
	.RSMedAlert
	{
		background-color: rgba(255,255,0,0.7); 
		font-size:36px;		
	}
	
	.EBMDashCounterWrapper h2
	{
		color:#CCC;
		font-size:24px;	
	}

	.ReportLink{
		cursor: pointer;
	}

.droppableContentDashboard li
{
	list-style:none;	
}



</style>




<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Reporting_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #Reporting_Campaigns_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput>#BATCHTITLE#</cfoutput>');	
</script>

<!--- Used to specify default Dashboard Object Sizes --->
<cfset CounterTinyClassString = "DashObj DashLeft DashBorder DashSize_Height_Tiny DashSize_Width_2 col-xs-12 col-sm-12 col-md-3" />
<cfset CounterSmallClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_2 col-xs-12 col-sm-12 col-md-3" />
<cfset ListMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />
<cfset ChartMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />

<script language="javascript">
<!--- Create javascript Dashboard Templates--->

	var DT_VoiceObj = new Object();
	DT_VoiceObj.Length = 3;
	
	
	DT_VoiceObj.Pos = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.Name = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.Type = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.ClassString = new Array(DT_VoiceObj.Length);
	
	DT_VoiceObj.Pos[0] = 1;
	DT_VoiceObj.Name[0] = "ComboResults";
	DT_VoiceObj.Type[0] = "CHART";
	DT_VoiceObj.ClassString[0] = "<cfoutput>#ChartMediumClassString#</cfoutput>"
	
	DT_VoiceObj.Pos[1] = 2;
	DT_VoiceObj.Name[1] = "Display_c_queue";
	DT_VoiceObj.Type[1] = "FORM";
	DT_VoiceObj.ClassString[1] = "<cfoutput>#CounterTinyClassString#</cfoutput>"
	
	DT_VoiceObj.Pos[2] = 3;
	DT_VoiceObj.Name[2] = "Display_c_results";
	DT_VoiceObj.Type[2] = "FORM";
	DT_VoiceObj.ClassString[2] = "<cfoutput>#CounterTinyClassString#</cfoutput>"
	
    <!--- DT_VoiceObj.Pos[3] = 4;
	DT_VoiceObj.Name[3] = "Display_c_open";
	DT_VoiceObj.Type[3] = "FORM";
	DT_VoiceObj.ClassString[3] = "<cfoutput>#CounterTinyClassString#</cfoutput>" --->
	 
	
</script>

<!---wrap the page content do not style this--->
<div id="page-content">
   
   	<div id="reportSummaryOverlay" class="no-print"></div>
   
  	<div class="container" style="width:100%;" >
  		
        <div class="row">
        	
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<h4 class="no-margin-top">Reporting and Analytics Dashboard</h4>            
            </div>
            
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="reportSummaryTitle">
                        Selected Date Range
                </div>
            
            	<input id="inpStartDate" type="hidden" name="inpStartDate" value="01/01/2015" /> 
                <input id="inpEndDate" type="hidden" name="inpEndDate" value="01/01/2015" /> 
                
                <div id="daterange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                </div>

                <!---<input id="daterange" type="text" name="daterange" value="01/01/2015 - 01/31/2015" style="width:100%;" /> 
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>    --->       
            </div>
        </div>
                    
        <div class="clear"></div>
        <BR/>
            
	<div>
    	
    </div>
    
    <!--- Template Maker --->
    <cfsavecontent variable="DashBoardOptions"></cfsavecontent>       
   
    <div class="content-EMS" style="width: 100%;">
       	<div class="head-EMS DashObjHeader no-print">
        	
            <row>
        	<div class="col-md-3 col-sm-3 col-xs-3">Dashboard</div>
			<cfoutput>
            	<div class='col-md-9 col-sm-9 col-xs-9'>
                	<span class='DashBoardHeaderMenu'><a data-toggle='modal' href='dsp_reportpicker' data-target='##ReportPickerModal' title="Add a report object to the dashboard">Add</a> | 
                    <a href="##" onclick='drawChart();' title='Reload Dashboard Data'>Reload</a> | 
                    <a data-toggle='modal' href="dsp_renametemplate?inpBatchIdList=#inpBatchIdList#" data-target='##SaveTemplagteModal' title='Make a template of current Dashboard Objects'>Template</a> | 
                    <a href="##" onclick='ClearDashboard()' title='Clear all dashboard objects'>Clear</a></span>
                </div>
			</cfoutput>
            
            </row>
      	</div>
    </div>
	
    <!--- No newlines so can include in jquery .append--->    
    <cfsavecontent variable="DashBoardPrintMenu"><div class="reportingHeader print-display-only"><h2><cfoutput>#BATCHTITLE#</cfoutput></h2><div class="summaryAnalytics" style="margin-top:40px;"> <span class="reportSummaryTitle">         Report Selected Range   </span>    <BR /> <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span><span id="SelectedMinBoundaryDatePrint" rel1=""></span>      <BR />   <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span><span id="SelectedMaxBoundaryDatePrint" rel1=""></span> </div></div> </cfsavecontent>       
  
  	<row>

    	<ul class="droppableContentDashboard" id="DashboardContent">
      	
        <!---<div>Drop stuff here....</div>--->
        
   		</ul>
    
	</row>

  		<p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->

    
	<!--- 1=up 2=right 3=left 4=down--->
    <cfsavecontent variable="DashObjSizer">
    	<div class='DashObjMenuRight'>
        <span class='DashBoardHeaderMenu'><a onclick='RemoveDashboardObject(this)'>X</a></span>
        <span class='DashBoardHeaderMenuSlim'>
            <table class='DashObjSizer'>
                <tr>
                    <td colspan='2'><a onclick='SizeDashboardObject(this,3)' title='Shrink Veritcal'>^</a></td>
                </tr>
                <tr>
                    <td><a onclick='SizeDashboardObject(this,4)' class='Trans270' title='Shrink Horizontal'>^</a></td>
                    <td><a onclick='SizeDashboardObject(this,2)' class='Trans90' title='Grow Horizontal'>^</a></td>
                </tr>
                <tr>
                    <td colspan='2'><a onclick='SizeDashboardObject(this,1)' class='Trans180' title='Grow Veritcal'>^</a></td>
                </tr>
        </table>
        </span>
        </div>
        
    </cfsavecontent>    
    
    <!--- inline string - edit above and recreate string as needed --->
    <cfsavecontent variable="DashObjSizer"><div class='DashObjMenuRight'><span class='DashBoardHeaderMenu'><a onclick='RemoveDashboardObject(this)'>X</a></span><span class='DashBoardHeaderMenuSlim'><table class='DashObjSizer'><tr><td colspan='2'><a onclick='SizeDashboardObject(this,3)' title='Shrink Veritcal'>^</a></td></tr><tr><td><a onclick='SizeDashboardObject(this,4)' class='Trans270' title='Shrink Horizontal'>^</a></td><td><a onclick='SizeDashboardObject(this,2)' class='Trans90' title='Grow Horizontal'>^</a></td></tr><tr><td colspan='2'><a onclick='SizeDashboardObject(this,1)' class='Trans180' title='Grow Veritcal'>^</a></td></tr></table></span></div></cfsavecontent>    
    

<script type="text/javascript">

	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var PriorStartDate = null;
	var PriorEndDate = null;
	
	var _tblListBatch_1 = null;
	
	var chartQuantity = 0;
	var CountDroppedThisSession = 0;
	
	<cfset DashObjMenu = "#DashObjSizer#" />
	
	<!--- Declared here but called in header as optional if exists--->
	function AdditionalHideTasks()
	{
		$('.DashboardReportingLeftMenu').hide("fast", function(){
			
		});
	}
	
	<!--- Declared here but called in header as optional if exists--->
	function AdditionalShowTasks()
	{
		$('.DashboardReportingLeftMenu').show("fast");
	}
		
	$(function() {	
		
		<!--- Force all page local modals to reload with new parameters each tine or it will just re-SHOW the old data --->
		$('body').on('hidden.bs.modal', '.modal', function () {			
			$(this).removeData('bs.modal');
		});
		 	
		<!--- Make Reports Menu Accordion Style --->
		 var icons = {
		  header: "ui-icon-circle-arrow-e",
		  activeHeader: "ui-icon-circle-arrow-s"
		};
		$( "#ReportMenu" ).accordion({
		  icons: icons,
		  heightStyle: "content"
		});
		$( "#toggle" ).button().click(function() {
		  if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
			$( "#accordion" ).accordion( "option", "icons", null );
		  } else {
			$( "#accordion" ).accordion( "option", "icons", icons );
		  }
		});
	
		<!--- Init componets, read date boundaries, load existing content--->
		ReadDateBoundaries();
					
	});
	
	function initDataTable(inpObj, inpReportName, inpcustomdata1, inpcustomdata2, inpcustomdata3, inpcustomdata4, inpcustomdata5, inpCustomConfigDataTablesSort)
	{			
		<!--- Set default sorting if not defined in display method --->	
		if(typeof(inpCustomConfigDataTablesSort) == "undefined" || inpCustomConfigDataTablesSort == "")
			inpCustomConfigDataTablesSort = '[[ 0, "desc" ]]';	
					
		inpObj.dataTable( {		
		   
			"bProcessing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
			"bLengthChange": true,
			"aaSorting": eval(inpCustomConfigDataTablesSort),
			"bServerSide": true,
			"pagingType": "full",
			"sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=" + inpReportName + "&returnformat=plain&queryformat=column",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
								
					var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
					var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": encodeURI("<cfoutput>#inpBatchIdList#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
					
					if(String(inpcustomdata1).length > 0)
						aoData.push({"name": "inpcustomdata1", "value": encodeURI(inpcustomdata1)});
					
					if(String(inpcustomdata2).length > 0)
						aoData.push({"name": "inpcustomdata2", "value": encodeURI(inpcustomdata2)});
					
					if(String(inpcustomdata3).length > 0)
						aoData.push({"name": "inpcustomdata3", "value": encodeURI(inpcustomdata3)});
					
					if(String(inpcustomdata4).length > 0)
						aoData.push({"name": "inpcustomdata4", "value": encodeURI(inpcustomdata4)});
						
					if(String(inpcustomdata5).length > 0)
						aoData.push({"name": "inpcustomdata5", "value": encodeURI(inpcustomdata5)});		
						
   								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    $('td', nRow).attr('nowrap','nowrap');
                    return nRow;
               },
			   "fnInitComplete":function(oSettings, json){
										
					inpObj.find('thead tr th').each(function(index){
															
						$this = $(this);
						if($this.hasClass("nosort"))
						{				
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}
						
					  });					
					
				},
			   "fnDrawCallback": function( oSettings ) {
					
					<!--- For tables that are hidden overflow - add full text to title --->								
					inpObj.find('tbody tr td').each(function(index){
																
						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });
					  			  
					<!--- Init Download Link(s) They are contained in the parent two levels up - .parents().eq(1) is the same as .parent().parent()--->
					inpObj.parents().eq(2).find($(".DownloadLink")).unbind();
					inpObj.parents().eq(2).find($(".DownloadLink")).click(function() {
		  
			  	    var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
					var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
					
					var CustomParams = "";
					
					if(typeof($(this).attr("inpcustomdata1")) != "undefined")
						CustomParams += "&inpcustomdata1=" + encodeURIComponent($(this).attr("inpcustomdata1")) ;
						
					if(typeof($(this).attr("inpcustomdata2")) != "undefined")
						CustomParams += "&inpcustomdata2=" + encodeURIComponent($(this).attr("inpcustomdata2")) ;
						
					if(typeof($(this).attr("inpcustomdata3")) != "undefined")
						CustomParams += "&inpcustomdata3=" + encodeURIComponent($(this).attr("inpcustomdata3")) ;		
					
					if(typeof($(this).attr("inpcustomdata4")) != "undefined")
						CustomParams += "&inpcustomdata4=" + encodeURIComponent($(this).attr("inpcustomdata4")) ;	
						
					if(typeof($(this).attr("inpcustomdata5")) != "undefined")
						CustomParams += "&inpcustomdata5=" + encodeURIComponent($(this).attr("inpcustomdata5")) ;	
							
					      <!---console.log("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>"))--->
						window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>");
										  
				<!---	  	OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + '',
						"Export Table Data", 
						280, 
						500,
						"ExportTableDataForm",
						false);			--->  
					  
				  });
									
				
				
		
				}
	
			
    	} );
		
				
	}
	
	function initComponents(){
		
		<!---/* Special date widget */--->
		var to = new Date();
		var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
			
		
		<!---$('input[name="daterange"]').daterangepicker();--->
		
		$('#daterange').daterangepicker({
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			startDate: "<cfoutput>#dateformat(NOW(),'mm/dd/yyyy')#</cfoutput>", <!---07/01/2015--->
			endDate: "<cfoutput>#dateformat(NOW(),'mm/dd/yyyy')#</cfoutput>",
			opens: "left",
			drops: "down",
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default"
		}, DateRangePickerCallback
			
		);


		if($("#inpEndDate").val() != "")
		{
			from = new Date($("#inpStartDate").val());
			to = new Date($("#inpEndDate").val());
			
	<!---		$('#daterange span').html('<span class="hidden-xs">' + from.format('dddd, ') + '</span>' + from.format('MMMM D, YYYY') + ' - ' + '<span class="hidden-xs">' + to.format('dddd, ') + '</span>' + to.format('MMMM D, YYYY'));
	--->	
		}
		
		<!---// initialize the special date dropdown field--->
		$('#daterange span').html('<span class="hidden-xs">' + days[from.getDay()] + ', ' + '</span>' + months[from.getMonth(true)] + ' ' + from.getDate() + ', ' + from.getFullYear() + ' - ' 
		+ '<span class="hidden-xs">' + days[to.getDay()] + ', ' + '</span>' + months[to.getMonth(true)] + ' ' + to.getDate() + ', ' + to.getFullYear());
			
	   		
		<!---/* add draggable for menu item */--->
		$(".menuItem").draggable({
			revert: "invalid",
			containment: "",
			opacity: 0.7,
			helper: "clone",
			cursor: "move",
			start: function(event, ui){
				$('.droppable').css({
					'border': 'solid 2px red'
				})
				
			},
			stop: function(event, ui){
				$('.droppable').css({
					'border': 'solid 1px #999999'
				})
			}
		});
		
	  		
		InitDashboardDroppable();
					
		drawChart();			
	}
	
	<!--- This function is called everytime the date range has been modified--->
	<!--- This function is defined here as part of the date range picker js class --->
	function DateRangePickerCallback(start, end, label)
	{		
		<!---
		console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		--->
		
		<!--- Update hidden inputs with new values so reports can access them easily --->
		$("#inpStartDate").val(start.format('YYYY-MM-DD'));
		$("#inpEndDate").val(end.format('YYYY-MM-DD'));
		
		$('#daterange span').html('<span class="hidden-xs">' + start.format('dddd, ') + '</span>' + start.format('MMMM D, YYYY') + ' - ' + '<span class="hidden-xs">' + end.format('dddd, ') + '</span>' + end.format('MMMM D, YYYY'));
			
		<!--- Redraw all chart objects with new date range --->
		drawChart();		
	}
	
	function loadingSingleChartData(id){
		$('#' + id).html('');
		$('#' + id).css("background", "url(../../public/images/loading.gif) center no-repeat");
	}
	
	function finishedLoadingSingleChartData(id){
		$('#' + id).css("background", "none");
	}
	
	function loadingData(){
		<!---$('#DashboardContent').css("background", "url(../../public/images/loading.gif) center no-repeat");
		$('.droppableContent').css('visibility', 'hidden');	--->	
		<!---$('#reportSummaryOverlay').toggle(); --->
	}
	
	function finishedLoadingData(){
		<!---$('#DashboardContent').css("background", "#FFF");
		$('.droppableContent').css('visibility', 'visible');--->
		<!--- $('#reportSummaryOverlay').toggle(); --->
	}
	
	function convertDateToTimestamp(date){
		return date.getFullYear() + '-' + (date.getMonth() +1) + '-' + date.getDate();
	}
	
	function resizeChart()
	{		
		var realWidth = $('.DashObjChart').width()-1;
		var realHeight = $('.DashObjChart').height()-1;
				
		$('.DashObj').each(function(index)
		{															
			$this = $(this);
		
			<!--- Chart size is dynamic based on obj and header sizes - Reset here --->				
			var heightNew = $this.height() - $this.children('.DashObjHeader').height();
			$this.children('.DashObjChart').height(heightNew);
		});
						
		for(index in AmCharts.charts )
		{			
			AmCharts.charts[index].invalidateSize();
		}				
	}
	
	function ReadDateBoundaries()
	{
		$('.reportingTimeSummary').css("background", "url(../../public/images/loading.gif) center no-repeat");
		
		var data = 
		{ 
			inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
			LimitDays: 1
		};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc','ReadDateBoundaries', data,
		'Get boundary data for reporting fail',		
		function(results)
		{
			$("#inpEndDate").val(results.MAXDATE);
			$("#inpStartDate").val(results.MINDATE);
			
			<!---$('#daterange span').html(results.MINDATE + ' - ' + results.MAXDATE);--->
									
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();			
			
		},
		function() <!--- Error function --->
		{
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();
		});
		
		
	}
	
	<!--- Method to draw chart after variable data has been defined "FORM" type--->
	function RedrawChartWithNewData(inpDataURP, inpDataGC, inpChartPosition)
	{
		$('#droppable_BatchList_'+ inpChartPosition).empty();
		loadingSingleChartData('droppable_BatchList_'+ inpChartPosition);
		
		$.ajax({
	
				type: "POST", 
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
				dataType: 'json',
				async:false,
				data: inpDataURP,					  
				success:function(res){
					if(parseInt(res.RXRESULTCODE) == 1){
						var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
						var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
						
						ServerProxy.PostToServerStruct(
						'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc',
						'GetChartByName',
						inpDataGC,
						'Get data for reporting fail',
						function(data){
							var reportArr = data.REPORT_ARRAY;
							var displayData = (reportArr[0]).DISPLAYDATA;
							var reportType = (reportArr[0]).REPORTTYPE;
							
							<!--- Create new object for chart container --->
							<!--- Add the container to the display --->
							var $NewObj =  $("#droppable_BatchList_" + inpChartPosition);
							
							<!--- Size and style the container --->				
							$NewObj.empty();
							$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_3 DashSize_Height_Small col-md-3 col-sm-6 col-xs-12");
							$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
												
							if(reportArr[0].REPORTTYPE == "TABLE")						
							{							
								$NewObj.html(displayData);

								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");					
					
								initDataTable($('#droppable_BatchList_'+ inpChartPosition + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + inpChartPosition).val());
							}
							else if(reportArr[0].REPORTTYPE == "FORM")						
							{							
								$NewObj.html(reportArr[0].DISPLAYDATA);							
								
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");
							}
							else
							{
								eval(displayData);
								if(typeof (chart) != 'undefined')
								{	
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + inpChartPosition) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ inpChartPosition + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var height = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(height);
													
									chart.write('DashObjChart_'+ inpChartPosition);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + inpChartPosition)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + inpChartPosition) + '">Change</div>');
								}	
				
							}							
						
							finishedLoadingSingleChartData('droppable_BatchList_'+ inpChartPosition);
						}
					);
				}
			} 		
		});
				
	}
			
	<!--- Setup dropable area for chart containers --->	
	function InitDashboardDroppable()
	{<!--- InitDashboardDroppable() --->
		
		$( "ul.droppableContentDashboard" ).sortable({
	<!---//	  	connectWith: ".DashBorder"  //,--->
		  	handle: ".DashObjHeader",
	<!---//	  	cancel: ".portlet-toggle",
	//	  	placeholder: "DashObjPlaceHolder ui-state-highlight"--->
	
			start: function(e, ui) 
			{
				<!--- creates a temporary attribute on the element with the old index--->
				$(this).attr('data-previndex', ui.item.index());
			},
			update: function(e, ui) 
			{
				
				loadingData();
				
				var newIndex = ui.item.index();
				var oldIndex = $(this).attr('data-previndex');
				$(this).removeAttr('data-previndex');
								
				<!--- Update DB Position --->
				$.ajax({
					type: "POST", 
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateDashboardObjectPosition&returnformat=json&queryformat=column',   
					dataType: 'json',
					async:false,
					data:  { 
						inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
						inpOriginalPos: parseInt(oldIndex),
						inpNewPos: parseInt(newIndex)
					},					  
					success:function(data)
					{				
						<!--- Update display--->
						
						var position = 1;
								
						$("#DashboardContent").children('.DashObj').each( function(index, value) {
							
							<!---Dont update unless you update all data and links...--->
							<!---$(this).attr('id', 'droppable_BatchList_' + position);--->
							$(this).attr('pos', position);
							position = position + 1;						
							
						});
				
				
						finishedLoadingData();
								
					}, 		
					error:function(jqXHR, textStatus)
					{				
						
						bootbox.alert(textStatus + "- Move Request failed!", function(result) { finishedLoadingData(); } );	
								
					} 		
				
				});		
		
		
			}


		});
	
		<!--- Disable showing the drop area as default selection --->
		<!---$( "ul.droppableContentDashboard" ).disableSelection();--->
	
		$("ul.droppableContentDashboard").droppable(
		{<!--- Droppable config --->
		
			<!--- Visual feedback cues --->	
			over: function(event, ui)
			{
				$(this).css({
					'border': 'solid 2px green',
					'box-shadow': '0 0px 10px #777',
					'-webkit-box-shadow': '0 0px 10px #777',
					'-moz-box-shadow': '0 0px 10px #777'
				});
			},
			<!--- Visual feedback cues --->
			out: function(event, ui)
			{
				$(this).css({
					'border': 'solid 1px #999999',
					'box-shadow': '0 0px 0px #fff',
					'-webkit-box-shadow': '0 0px 0px #fff',
					'-moz-box-shadow': '0 0px 0px #fff'
				});
			},
			<!--- Process when somthing is dropped here --->
			drop: function(event, ui)
			{<!--- drop event --->
				
				if(ui.draggable.hasClass("DashObj"))
				{<!--- Update order in DB for all DashObj --->
					
					
					<!--- Pos is updated in the Update method of the droppable object. --->
					
					
				}<!--- Update order in DB for all DashObj --->
				else
				{
					
				}
						
			}<!--- drop event --->
			
		});<!--- Droppable config --->
								
	}<!--- InitDashboardDroppable() --->
	
	<!--- Allow user to pick a report to add to the dashboard - replaces old drag and drop version which did not work well on mobile devices --->
	function AddChartItem(inpObj)
	{<!--- Default Drop Chart Obj--->
				
				
		if(inpObj.attr('rel2') == "TEMPLATE")
		{						
			LoadTemplate(inpObj.attr('rel4'));
			
			<!--- Add list of objects to the display --->	
			<!---
			if(inpObj.attr('rel4') == "DT_VoiceObj")
									
			var arrayLength = DT_VoiceObj.Length;
			for (var i = 0; i < arrayLength; i++) 
			{
				AddSingleDashObj(DT_VoiceObj.Pos[i], DT_VoiceObj.Name[i], DT_VoiceObj.Type[i], DT_VoiceObj.ClassString[i]);	
			}
		--->
			return;	
		}
		
	
		<!--- New chart added --->				
		chartQuantity++;
		
		<!--- ID names must be unique while position and total count can change--->
		CountDroppedThisSession++;
		
		var reportType = new Array();
		var quadarant = new Array();
					
		reportName = inpObj.attr('rel1');
		reportDisplayType = inpObj.attr('rel2');
		
		var ClassInfo = "";
		
		if(inpObj.attr('rel3') != "" && typeof(inpObj.attr('rel3')) != "undefined")
			ClassInfo = inpObj.attr('rel3');
		else
			ClassInfo = "DashObj DashLeft DashBorder DashSize_Width_3 DashSize_Height_Small col-md-3 col-sm-6 col-xs-12";	
												
		loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
		
		<!--- Create new object for chart container --->
		<!--- Add the container to the display --->
		var $NewObj =  $("<li id='droppable_BatchList_" + CountDroppedThisSession + "'></li>").appendTo("ul.droppableContentDashboard");
		
		<!--- Size and style the container --->				
		$NewObj.html('');
		$NewObj.addClass(ClassInfo);
		$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
		$NewObj.attr('pos', chartQuantity);
		
		<!--- Get current date range of page --->
		var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
		var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
		
		<!--- Write selection to user preferences --->
		
		$.ajax({

			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpReportName: reportName,
				inpReportType: reportDisplayType,
				inpClassInfo: ClassInfo,
				inpChartPostion: chartQuantity 
			},		
			error:function(jqXHR, textStatus)
			{	
				chartQuantity--;
			},
			success:function(res){
				if(parseInt(res.RXRESULTCODE) == 1)
				{
					
					
					$.ajax({
						type: "POST", 
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetChartByName&returnformat=json&queryformat=column',   
						dataType: 'json',
						async:false,
						data:  
						{ 
							inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
							inpStart: startDate,
							inpEnd: endDate,
							inpChartName: reportName,
							inpReportType: reportDisplayType,
							inpChartPostion: chartQuantity 
						},					  
						success:function(data)
						{<!--- ajax success --->	
										
							if(typeof(data.RXRESULTCODE != "undefined"))
							{				
							
								if(typeof(data.ROWCOUNT != "undefined"))
									if (data.ROWCOUNT > 0) 
										CurrRXResultCode = parseInt(data.DATA.RXRESULTCODE[0]);
									else	
										CurrRXResultCode = parseInt(data.RXRESULTCODE);			
								else
									CurrRXResultCode = parseInt(data.RXRESULTCODE);	
								
								if(CurrRXResultCode > 0)
								{
									var reportArr = data.REPORT_ARRAY;
									var displayData = (reportArr[0]).DISPLAYDATA;
									var reportType = (reportArr[0]).REPORTTYPE;
									
									<!--- Remove any data that may already be here --->
									$NewObj.empty();
									
									if(reportArr[0].REPORTTYPE == "TABLE")						
									{							
										$NewObj.html(displayData);
				
										<!--- Add DashObj options to header with class head-EMS --->
										$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
										$NewObj.find('.head-EMS').addClass("DashObjHeader");
				
										 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
									}
									else if(reportArr[0].REPORTTYPE == "FORM")						
									{			
										$NewObj.html(reportArr[0].DISPLAYDATA);			
										
										$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
										$NewObj.find('.head-EMS').addClass("DashObjHeader");			
									}
									else
									{
										eval(displayData);
										if(typeof (chart) != 'undefined')
										{																										
											<!--- Add a header for info as well as an anchor for drag and sort --->												
											if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
												$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
											else
												$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
						
											
											<!--- Add a new area for the chart --->	
											$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
												
											<!--- Chart size is dynamic based on obj and header sizes --->				
											var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
											$NewObj.children('.DashObjChart').height(heightNew);
																									
											chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
											
											<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
											if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
												$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
										}											
									}
									
									$NewObj.css("background", "none");
																		
									return false;
										
								}
								else
								{
									
									bootbox.alert("Error" + "\n"  + data.MESSAGE + "\n" + data.ERRMESSAGE + " - Failure!", function(result) 
									{ 
										$NewObj.css("background", "none");
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report Load Error" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
										$NewObj.append("Error" + "<BR/>"  + data.MESSAGE + "<BR/>" + data.ERRMESSAGE, "Failure!");	
									});							
								}
							}
							
						}<!--- ajax success --->	
					});
				
				}
				
			}
		});
		
	}<!--- Default Drop Chart Obj--->

	
	<!--- Load charts from DB preferences --->	
	function drawChart()
	{
		<!--- Read current date data--->
		var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
		var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
		
		<!--- Clear current dashboard--->
		$("#DashboardContent").empty();
		
		$("#DashboardContent").append('<cfoutput>#DashBoardPrintMenu#</cfoutput>');
		
		
		var from = new Date(startDate);
		var to = new Date(endDate);
			
						
		$("#SelectedMinBoundaryDatePrint").html(days[from.getDay()] + ', ' + from.getMonth(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
		$("#SelectedMinBoundaryDatePrint").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
		$("#SelectedMaxBoundaryDatePrint").html(days[to.getDay()] + ', ' + to.getMonth(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
		$("#SelectedMaxBoundaryDatePrint").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
		
		<!--- Feedback to user while loading --->
		loadingData();
			
		<!--- GetDashBoardCount --->
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetDashBoardCount&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>'
			},					  
			success:function(data)
			{				
				chartQuantity = data.TOTALCOUNT;				
			} 		
		});
		
		<!--- Reset count as all are reloaded here --->		
		CountDroppedThisSession = 0;
					
		<!--- Hide dashboard until first object is loaded --->
		$('#reportSummaryOverlay').show();	
		$('#DashboardContent').css("background", "url(../../public/images/loading.gif) center no-repeat");	
					
		for(icq=0; icq< chartQuantity; icq++)
		{
			<!--- Get the report data based on previously stored preferences - do this for each report postion --->
			var displayData = "";	
									
			<!--- ID names must be unique while position and total count can change--->
			CountDroppedThisSession++;
			
			loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
					
			<!--- Create new object for chart container --->
			<!--- Add the container to the display --->
			var $NewObj =  $("<li id='droppable_BatchList_" + CountDroppedThisSession + "'></li>").appendTo("ul.droppableContentDashboard");
			
			<!--- Size and style the container --->				
			$NewObj.html('');
			$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_3 DashSize_Height_Small col-md-3 col-sm-6 col-xs-12");
			$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
			$NewObj.attr('pos', CountDroppedThisSession);
			
			
			$.ajax({
				type: "POST", 
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetChartFromPreferences&returnformat=json&queryformat=column',   
				dataType: 'json',
				async:false,
				data:  
				{ 
					inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
					inpStart: startDate,
					inpEnd: endDate,
					inpChartPostion: CountDroppedThisSession
				},					  
				success:function(data)
				{<!--- ajax success --->	
					
					<!--- Show dashboard while individual obejcts are still loading --->			
					$('#reportSummaryOverlay').hide();	
					$('#DashboardContent').css("background", "#FFF");
								
					if(typeof(data.RXRESULTCODE != "undefined"))
					{				
					
						if(typeof(data.ROWCOUNT != "undefined"))
							if (data.ROWCOUNT > 0) 
								CurrRXResultCode = parseInt(data.DATA.RXRESULTCODE[0]);
							else	
								CurrRXResultCode = parseInt(data.RXRESULTCODE);			
						else
							CurrRXResultCode = parseInt(data.RXRESULTCODE);	
						
						if(CurrRXResultCode > 0)
						{
							var reportArr = data.REPORT_ARRAY;
					
							<!--- Remove any data that may already be here --->
							$NewObj.empty();
							
							$NewObj.removeClass();
							
							if(reportArr.CLASSINFO != '')
								$NewObj.addClass(reportArr.CLASSINFO);
							else
								$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_3 DashSize_Height_Small col-md-3 col-sm-6 col-xs-12");
							
							if(reportArr.REPORTTYPE == "TABLE")						
							{							
								 $NewObj.html(reportArr.DISPLAYDATA);
								 
								 $NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");
								 $NewObj.find('.head-EMS').addClass("DashObjHeader");
													
								 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr.TABLEREPORTNAME, reportArr.INPCUSTOMDATA1, reportArr.INPCUSTOMDATA2, reportArr.INPCUSTOMDATA3, reportArr.INPCUSTOMDATA4, reportArr.INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
							}
							else if(reportArr.REPORTTYPE == "FORM")						
							{			
								$NewObj.html(reportArr.DISPLAYDATA);
								
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");						
							}
							else
							{
								eval(reportArr.DISPLAYDATA);
								if(typeof (chart) != 'undefined')
								{	
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(heightNew);
																	
									chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
								}
								
								resizeChart();
							}
							
							$NewObj.css("background", "none");
																	
							<!---console.log($NewObj);
							console.log($NewObj.html());--->
														
							return false;
								
						}
						else
						{
							
							bootbox.alert("Error" + "\n"  + data.MESSAGE + "\n" + data.ERRMESSAGE + " - Failure!", function(result) 
							{ 
								$NewObj.css("background", "none");
								$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report Load Error" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
								$NewObj.append("Error" + "<BR/>"  + data.MESSAGE + "<BR/>" + data.ERRMESSAGE, "Failure!");									
							});							
						}
					}
					
				}<!--- ajax success --->	
			});
							
		}
	
		<!--- Show dashboard even if no objects loaded --->
		$('#reportSummaryOverlay').hide();	
		$('#DashboardContent').css("background", "#FFF");
	
		finishedLoadingData();
	}	
	
	function RemoveDashboardObject(inpObj)
	{	
		loadingData();
	
		<!--- Remove from DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=DeleteDashboardObj&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpChartPostion: $(inpObj).parents(".DashObj").attr('pos')
			},					  
			success:function(data)
			{				
				<!--- Update Remaining position / ids  --->
				chartQuantity--;
				<!---//CountDroppedThisSession--;  Add this here breaks next drop - two objects with same name becomes possible - may not need this?--->
				
				var position = 1;
								
				$("#DashboardContent").children('.DashObj').each( function(index, value) {
					
					<!---Dont update unless you update all data and links...--->
					<!---$(this).attr('id', 'droppable_BatchList_' + position);--->
					
					if($(inpObj).parents(".DashObj").attr('pos') != index+1)
					{
						$(this).attr('pos', position);
						position = position + 1;						
					}
				});
				
				<!--- Remove from display--->	
				$(inpObj).parents(".DashObj").remove();	
				
				finishedLoadingData();		
			}, 		
			error:function(jqXHR, textStatus)
			{				
				
				bootbox.alert(textStatus + "\n" + " - Remove Request failed!", function(result) { finishedLoadingData(); } );									
			} 		
		
		});			
	}
	
	<!--- 1=up 2=right 3=left 4=down--->
	function SizeDashboardObject(inpObj, inpDir)
	{		
		var $CurrObj = $(inpObj).parents(".DashObj");
		
		switch(inpDir)
		{		
			<!--- Make bigger vertically --->
			case 1:
			
				if($CurrObj.hasClass('DashSize_Height_Tiny'))
				{
					$CurrObj.addClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Tiny');	
					$CurrObj.removeClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Large');
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Height_Small'))
				{
					$CurrObj.addClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Small');	
					$CurrObj.removeClass('DashSize_Height_Tiny');	
					$CurrObj.removeClass('DashSize_Height_Large');
					break;		
				}
				
				
				if($CurrObj.hasClass('DashSize_Height_Med'))
				{
					$CurrObj.addClass('DashSize_Height_Large');
					$CurrObj.removeClass('DashSize_Height_Tiny');	
					$CurrObj.removeClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Med');
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Height_Large'))
				{
					<!--- Large is as large as it gets --->	
					$CurrObj.removeClass('DashSize_Height_Tiny');	
					$CurrObj.removeClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Med');
					break;		
				}		
				
				break;		
			
			<!--- Make bigger horizontally --->
			case 2:
			
			<!--- 
				1	col-xs-6 col-sm-6 col-md-1  
				2   col-xs-12 col-sm-12 col-md-2
				3	col-xs-12 col-sm-12 col-md-3
				4	col-xs-12 col-sm-12 col-md-6
				5	col-xs-12 col-sm-12 col-md-9
				6	col-xs-12 col-sm-12 col-md-12
						
			--->
			
				if($CurrObj.hasClass('DashSize_Width_1'))
				{
					$CurrObj.addClass('DashSize_Width_2');
					$CurrObj.removeClass('DashSize_Width_1');
					
					$CurrObj.addClass('col-md-2');
					$CurrObj.removeClass('col-md-1');
					
					$CurrObj.addClass('col-sm-12');
					$CurrObj.removeClass('col-sm-6');
					
					$CurrObj.addClass('col-xs-12');
					$CurrObj.removeClass('col-xs-6');
					
					
					<!---$CurrObj.find('.DashObjHeaderText').show();--->
										
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_2'))
				{
					$CurrObj.addClass('DashSize_Width_3');
					$CurrObj.removeClass('DashSize_Width_2');
					
					$CurrObj.addClass('col-md-3');
					$CurrObj.removeClass('col-md-2');					
									
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_3'))
				{
					$CurrObj.addClass('DashSize_Width_4');
					$CurrObj.removeClass('DashSize_Width_3');
					
					$CurrObj.addClass('col-md-6');
					$CurrObj.removeClass('col-md-3');					
									
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_4'))
				{
					$CurrObj.addClass('DashSize_Width_5');
					$CurrObj.removeClass('DashSize_Width_4');
					
					$CurrObj.addClass('col-md-9');
					$CurrObj.removeClass('col-md-6');					
									
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_5'))
				{
					$CurrObj.addClass('DashSize_Width_6');
					$CurrObj.removeClass('DashSize_Width_5');
					
					$CurrObj.addClass('col-md-12');
					$CurrObj.removeClass('col-md-9');					
									
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_6'))
				{
					<!--- Large is as large as it gets --->	
					break;		
				}	
				
				break;		
				
			<!--- Make smaller vertically --->
			case 3:
			
				if($CurrObj.hasClass('DashSize_Height_Tiny'))
				{
					<!--- Small is as small as it gets --->	
					$CurrObj.removeClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Large');	
					break;	
				}
				
				if($CurrObj.hasClass('DashSize_Height_Small'))
				{
					$CurrObj.addClass('DashSize_Height_Tiny');
					$CurrObj.removeClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Large');	
					break;	
				}
				
				if($CurrObj.hasClass('DashSize_Height_Med'))
				{
					$CurrObj.addClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Tiny');
					$CurrObj.removeClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Large');	
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Height_Large'))
				{
					$CurrObj.addClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Tiny');
					$CurrObj.removeClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Large');	
					break;								
				}	
				
				break;		
				
			<!--- Make smaller horizontally --->
			case 4:
			
			<!--- 
			
				1	col-xs-6 col-sm-6 col-md-1  
				2   col-xs-12 col-sm-12 col-md-2
				3	col-xs-12 col-sm-12 col-md-3
				4	col-xs-12 col-sm-12 col-md-6
				5	col-xs-12 col-sm-12 col-md-9
				6	col-xs-12 col-sm-12 col-md-12
			
				Small = col-md-2 col-sm-3 
				Med = col-md-3 col-sm-6
				Large = col-md-12 col-sm-12			
			--->
			
				if($CurrObj.hasClass('DashSize_Width_1'))
				{
					<!--- Small is as small as it gets --->	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_2'))
				{
					$CurrObj.addClass('DashSize_Width_1');
					$CurrObj.removeClass('DashSize_Width_2');	
					
					$CurrObj.addClass('col-md-1');
					$CurrObj.removeClass('col-md-2');
					
					$CurrObj.addClass('col-sm-6');
					$CurrObj.removeClass('col-sm-12');
					
					$CurrObj.addClass('col-xs-6');
					$CurrObj.removeClass('col-xs-12');
					
					<!---$CurrObj.find('.DashObjHeaderText').hide();--->
					
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_3'))
				{
					$CurrObj.addClass('DashSize_Width_2');
					$CurrObj.removeClass('DashSize_Width_3');	
					
					$CurrObj.addClass('col-md-2');
					$CurrObj.removeClass('col-md-3');					
					
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_4'))
				{
					$CurrObj.addClass('DashSize_Width_3');
					$CurrObj.removeClass('DashSize_Width_4');	
					
					$CurrObj.addClass('col-md-3');
					$CurrObj.removeClass('col-md-6');					
					
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_5'))
				{
					$CurrObj.addClass('DashSize_Width_4');
					$CurrObj.removeClass('DashSize_Width_5');	
					
					$CurrObj.addClass('col-md-6');
					$CurrObj.removeClass('col-md-9');					
					
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_6'))
				{
					$CurrObj.addClass('DashSize_Width_5');
					$CurrObj.removeClass('DashSize_Width_6');	
					
					$CurrObj.addClass('col-md-9');
					$CurrObj.removeClass('col-md-12');					
					
					break;		
				}
				
				
				
				break;		
		}
		
		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateDashboardObjectClassInfo&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpChartPostion: $(inpObj).parents(".DashObj").attr('pos'),
				inpClassInfo: $CurrObj.attr('class')
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				resizeChart();	
						
			}, 		
			error:function(jqXHR, textStatus)
			{				
				
				bootbox.alert(textStatus + "\n" + " - Resize Request failed!", function(result) { } );	
						
			} 		
		
		});			
			
	}
	
	
	function AddSingleDashObj(inpDashObj)
	{
		<!--- New chart added --->				
		chartQuantity++;
		
		<!--- ID names must be unique while position and total count can change--->
		CountDroppedThisSession++;
		
		var reportType = new Array();
		var quadarant = new Array();
					
		reportName = inpDashObj.NAME;
		reportDisplayType = inpDashObj.TYPE;
		
		var ClassInfo = "";
		
		if(inpDashObj.CLASSINFO != "")
			ClassInfo = inpDashObj.CLASSINFO;
		else
			ClassInfo = "DashObj DashLeft DashBorder DashSize_Width_3 DashSize_Height_Small col-md-3 col-sm-6 col-xs-12";
												
		loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
		
		<!--- Create new object for chart container --->
		<!--- Add the container to the display --->
		var $NewObj =  $("<li id='droppable_BatchList_" + CountDroppedThisSession + "'></li>").appendTo("ul.droppableContentDashboard");
		
		<!--- Size and style the container --->				
		$NewObj.html('');
		$NewObj.addClass(ClassInfo);
		$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
		$NewObj.attr('pos', chartQuantity);
		
		<!--- Get current date range of page --->
		var startDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').startDate));
		var endDate = convertDateToTimestamp(new Date($('#daterange').data('daterangepicker').endDate) );
		
		<!--- Write selection to user preferences --->
		
		$.ajax({

			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpReportName: reportName,
				inpReportType: reportDisplayType,
				inpClassInfo: ClassInfo,
				inpChartPostion: chartQuantity,
				inpcustomdata1: inpDashObj.CUSTOMDATA1,
				inpcustomdata2: inpDashObj.CUSTOMDATA2,
				inpcustomdata3: inpDashObj.CUSTOMDATA3,
				inpcustomdata4: inpDashObj.CUSTOMDATA4,
				inpcustomdata5: inpDashObj.CUSTOMDATA5
				 
			},		
			error:function(jqXHR, textStatus)
			{	
				chartQuantity--;
			},
			success:function(res){
				if(parseInt(res.RXRESULTCODE) == 1)
				{
					<!--- Read chart data --->
					ServerProxy.PostToServerStruct
					(<!--- ServerProxy.PostToServerStruct --->
						'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc',
						'GetChartByName',
						{ 
							inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
							inpStart: startDate,
							inpEnd: endDate,
							inpChartName: reportName,
							inpReportType: reportDisplayType,
							inpChartPostion: chartQuantity 
						},
						'Get data for reporting fail',
						function(data)
						{
							var reportArr = data.REPORT_ARRAY;
							var displayData = (reportArr[0]).DISPLAYDATA;
							var reportType = (reportArr[0]).REPORTTYPE;
							
							<!--- Remove any data that may already be here --->
							$NewObj.empty();
							
							if(reportArr[0].REPORTTYPE == "TABLE")						
							{							
								$NewObj.html(displayData);
		
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");
		
								 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
							}
							else if(reportArr[0].REPORTTYPE == "FORM")						
							{			
								$NewObj.html(reportArr[0].DISPLAYDATA);			
								
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");			
							}
							else
							{
								eval(displayData);
								if(typeof (chart) != 'undefined')
								{																										
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
				
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(heightNew);
																							
									chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
								}											
							}
							
							$NewObj.css("background", "none");
						}
							
					);<!--- ServerProxy.PostToServerStruct --->
					
				}
			}
		});

		
	}
	
	function SaveCurrentAsTemplateDialog()
	{		
		<!--- See naming template at bottom of page--->
		showRenameDashboardTemplateDialog('0', '');
	}
	
	function LoadTemplate(inpId)
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ReadTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  
			{ 
				inpId: parseInt(inpId)
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{
					var DT_TemplateObj = JSON.parse( data.TEMPLATE);
					
					var arrayLength = DT_TemplateObj.length;
					for (var i = 0; i < arrayLength; i++) 
					{						
						AddSingleDashObj(DT_TemplateObj[i]);	
					}	
				}
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				
				bootbox.alert("Connection Failure!" + textStatus + "\n" + " - Template Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
	function ClearDashboard()
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ClearDashboard&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>'
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				drawChart();
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Clear Dashboard Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
	
	
</script>

<!-- Modal -->
<div class="modal fade" id="ReportPickerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="SaveTemplagteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



