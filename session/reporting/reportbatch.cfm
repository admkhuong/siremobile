<cfparam name="inpBatchIdList" default="">
<cfparam name="inpReportingSectionType" default="1">



<!--- Check for Reporting Permission - if not send to home page --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfif NOT permissionObject.havePermission(Reporting_Title).havePermission>
	
    <cfoutput>#Permission_Fail#</cfoutput>
    <cfabort />
</cfif>

<cfoutput>
	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	
   <!--- <script type="text/javascript" src="#rootUrl#/#publicPath#/js/datatables/js/jquery.datatables.min.js"></script> ---> 
    <script type="text/javascript" src="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	
        
  <!---  <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatablepage.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables/css/datatable.css">--->
    
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
             
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datepicker/datepicker.js"></script>    
       
    <script type='text/javascript' src='#rootUrl#/#publicPath#/js/jquery.mousewheel.min.js'></script>
    
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    
	<!---	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_2.11.2/amcharts/amcharts.js</cfoutput>"></script>--->	
	<!--- After version 3.1.0 you need to include each chart type as separate js file--->
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amcharts.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/pie.js</cfoutput>"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/serial.js</cfoutput>"></script>
    <script type="text/javascript" src="<cfoutput>#rootUrl#/#publicPath#/js/amcharts_3.4.3/amcharts/amstock.js</cfoutput>"></script>
	
	<style>
		@import url('#rootUrl#/#publicPath#/css/datepicker/base.css');
		@import url('#rootUrl#/#publicPath#/css/datepicker/clean.css');
		@import url('#rootUrl#/#publicPath#/css/reporting/campaign.css');
		@import url('#rootUrl#/#publicPath#/css/style.css');
	</style>
</cfoutput>

<cfset BATCHTITLE = ""> 

<!--- Validate user has access to all Batches in list--->
<cfloop list="#inpBatchIdList#" index="INPBATCHID" delimiters=",">

	<!--- This will also Validate URL passed Batch Id is owned accessible by current session user id --->  
    <!--- Load inpXMLControlString once so we dont have to keep loading while loading page - Do this in csc.CFC also --->
    <cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarGetBatch">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">                      
    </cfinvoke> 
    
    <!---<cfdump var="#RetVarGetBatch#"><cfabort>--->
    
    <cfif RetVarGetBatch.RXRESULTCODE LT 0>
        
        <div>
            No campaign data found for <cfoutput>#INPBATCHID#</cfoutput> 
        </div>
    
        <cfabort/>
    
    </cfif>
                
    <cfset BATCHTITLE = RetVarGetBatch.DESC> 
    
</cfloop>



<!--- REPORT FORM STYLES --->
<style>

	#datepicker-calendar {
		
		left: 0px;
		z-index: 100;
	}


	.ResetChartOption
	{
		position:relative;
		top:-20px;
		right:10px;
		z-index:10000;	
		cursor:pointer;	
		text-decoration:none;
		font-size: 12px;
	    text-align: right;
	}

	.ResetChartOption:hover
	{
		text-decoration:underline;
	}
	
	.messages-lbl{
		color: #0888D1 !important;
		font-family: "Verdana" ;
		font-weight: bold ;
		line-height: 14px ;
		margin-left: 20px ;
		padding-bottom: 20px ;
		padding-top: 30px ;
		font-size: 16px;
	}
	
	.caller_id{
		padding-top:0;
	}
	
	#content-AAU-Admin{
		background-color:#fff;
		border:1px solid #ccc;
		border-radius:4px;
	}
	
	#divAgent{
		margin-bottom:90px;
	}
	
	#content-AAU-Admin img{
		width:16px; 
		float:left;
		cursor:pointer;
	}
	
	#tblActiveAgent tbody{
		border-left:1px solid #ccc;
		border-right:1px solid #ccc;
	}
	
	#tblActiveAgent_paginate {
		border-left: 1px solid #CCCCCC;
		margin-top: -21px;
	}
	
	#em-name-block{
		width:70%;
		margin-left:21px;
		font-family:"Verdana" !important;
		font-size:12px !important;
	}
	
	.left-input{
		width:95px;
		float:left;
		border-left:1px solid #ccc;
		border-top:1px solid #ccc;
		border-bottom:1px solid #ccc;
		border-right:none;
		height:28px;
		border-radius: 4px 0 0 4px;
		color: #666666;
		line-height: 25px;
		padding-left: 15px;
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#f4f4f4));
		background-image: -webkit-linear-gradient(top, #fff, #f4f4f4);
		background-image: -moz-linear-gradient(to bottom, #fff, #f4f4f4);
		background-image: -o-linear-gradient(top, #fff, #f4f4f4);
		background-image: linear-gradient(top, #fff, #f4f4f4);
	}
	
	.left-input>span.em-lbl{
		width: 72px;
		float:left;
	}
	
	.left-input>div.hide-info{
		width:18px;
		float:left;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
	}
	
	.left-input>div.hide-info:hover{
		width:18px;
		float:left;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
	}
	
	.message-block .right-input{
		width:387px;
		float: left;
		border:1px solid #ccc;
		height:28px;
		border-radius: 0 4px 4px 0;
		color: #666666;
	}
	
			
	.message-block .right-input{
		width:381px;
		float: left;
		border:1px solid #ccc;
		height:28px;
		border-radius: 0 4px 4px 0;
		color: #666666;
	}
	
	.message-block .right-input>span.selectmenucustom{
		height:28px!important;
		border: medium none;
		border-radius: 0;
		overflow:hidden;
	}
	
	.message-block .right-input .ui-selectmenu-status {
		line-height: 1.4em;
	}
	
	.message-block .right-input .ui-selectmenu-icon{
		margin-top: -13px;
	} 
	
	.right-input>input{
		border:none !important;
		height: 21px;
		line-height: 30px;
		width: 321px;
		background-color:#fbfbfb;
		font-family:"Verdana" !important;
		font-size:12px !important;
		color:#000 !important;
		outline: 0 none !important;
	}
	
		
	.message-block{
		margin-left:21px;
		font-family:"Verdana";
		font-size:12px;
	}
	
	.left-input{
		width:130px;
		float:left;
		border-left:1px solid #ccc;
		border-top:1px solid #ccc;
		border-bottom:1px solid #ccc;
		border-right:none;
		height:28px;
		border-radius: 4px 0 0 4px;
		color: #666666;
		line-height: 25px;
		padding-left: 10px;
	}
	
	.left-input>span.em-lbl{
		width: 106px;
		float:left;
	}
	
	.left-input>div.hide-info{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
	}
	
	.left-input>div.hide-info:hover{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
	}
	
	.info-block{
		background-color: #FFFFFF;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		left: 100px;
		padding: 6px;
		position: relative;
		top: -10px;
	}
	
	.EM-btn{
		height:30px;
		border:1px solid #ccc;
		width:128px;
		float:left;
		line-height: 28px;
		margin-bottom: 10px;
		margin-left: 21px;
		text-align: center;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1) inset, 0 1px 5px rgba(0, 0, 0, 0.25);
		border-radius:4px;
		font-family:"Verdana";
		font-size:14px;
		color:#fff;
	} 
	

	.top-input{
		border: 1px solid #CCCCCC;
		border-radius: 4px 4px 0 0;
		color: #666666;
		margin-top: 10px;
		min-height: 40px;
		padding: 10px;
		width: 505px;
	}
	
	.bottom-input{
		-moz-border-bottom-colors: #CCCCCC;
		-moz-border-left-colors: #CCCCCC;
		-moz-border-right-colors: #CCCCCC;
		-moz-border-top-colors: none;
		border-image: none;
		border-radius: 0 0 4px 4px;
		border-style: none solid solid;
		border-width: medium 1px 1px;
		color: #666666;
		width: 505px;
		float:left;
		height: 14px;
		padding: 8px 10px;
	}
	
	.bottom-input input{
		float:left;
	}
	
	.bottom-input label{
		float:left;
		margin-left:10px;
	}
	
	.bottom-input .hide-info{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	
	.bottom-input .hide-info:hover{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	

	
	.method-btn {
		background-color: #F5F5F5;
		background-image: linear-gradient(to bottom, #FFFFFF, #E6E6E6);
		background-repeat: repeat-x;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
		cursor: pointer;
		display: inline-block;
		font-size: 14px;
		line-height: 20px;
		margin-bottom: 0;
		padding: 4px 12px;
		text-align: center;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
		vertical-align: middle;
	}
	
	div.warning-block{
		border: 1px solid #FFB400;
		border-radius: 4px;
		margin-left: 20px;
		width: 93%;
		padding:16px;
		font-family:"Verdana";
		font-size:13px;
		font-style:normal;
		color:#666666;
		background-color:#fff7e5;
	}
	
	span.warning-text{
		font-weight:bolder;
	}
	
	div.warning-checkbox{
		color: #737373;
		margin-top:25px;
	}
	
	div#warning-content{
		margin-bottom:25px;
	}
	
	div#head-AAU-Admin{
		font-weight:bolder;
	}
	
	div#sent-messages-lbl{
		padding:30px 20px 24px;
		font-family:"Verdana";
		font-size:14px;
		font-weight:bolder;
		color:#0888d1;
		float:left;
	}
	
	div#btnCreateNew{
		float:right;
		margin:20px 25px 0 20px;
	}
	
	#buttonBar{
		width:100%;
	}
	
	table#tblListEMS{
		width:96%!important;
		border: 1px solid #CCCCCC;
	}	
	
		
	.tool-tip1{
		background: url("../images/info.png") no-repeat scroll 0px 0px rgba(0, 0, 0, 0);
		display: block;
		opacity: 0.4;
		width: 20px;
		height: 20px;
	}
	.tool-tip1:hover {
		opacity: 1;
	}
	
	#inpAvailableShortCode-menu{
		margin-left:-6px;
		width:381px!important;
	}
	
	.ui-selectmenu-menu ul, ui-selectmenu-item-selected ul li, .ui-state-hover ul li {
    border: 1px solid #AAAAAA !important;

	}
	
	.btn
	{   
    	padding: 0px 5px !important;
	}

	#inpAvailableShortCode-button
	{		
		outline:none;		
	}



</style>




<style type="text/css">
	.reportSummaryTitle {
	    font-weight: bold;
	    align: left
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
			
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;		
	}
			
	#ReportMenu .ui-state-active, #ReportMenu .ui-widget-content #ReportMenu .ui-state-active, #ReportMenu .ui-widget-header #ReportMenu .ui-state-active 
	{
  		background-color: #118ACF; 
		border: none;
		color: #FFFFFF !important;
		outline: medium none;	
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;	
	}

.ui-widget-content {
    background: none repeat scroll 0 0 #FFFFFF;
    border: none;
		
}

..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
  
    border: none;
    color: #313131;
    font-weight: normal;			
}

	.SideSummary
	{ 
		width:350px; 
		height:250px; 
		margin: 0 0 0 0; 
		float:left; 
		overflow-y:auto;
	}
			
	.reportingContent {
		float: none;
		width: 2000px;
		min-height: 1200px;
		background:none;
		border-radius: 8px 8px 8px 8px;	
		position:static;	
	}
	
	#tblListBatch_1_info
	{
	   width: 300px !important;
	   padding-left: 10px;
	   margin-left: 13px;
	   display: none;
	}


	#chartContent
	{
		background: #fff;
		float:left;
		width: 1200px;		
	}
		
</style>


<style>

	hr.style-five 
	{
		border: 0;
		height: 0; 
		box-shadow: 0 0 3px 1px #CCCCCC;
	}
	
	hr.style-five:after 
	{  
		content: "\00a0";  
	}

	#reportSummaryOverlay {
		background-color: #FFFFFF;
		display: none;
		left: 0;
		opacity: 0.8;
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 90;		
		background: "url(../../public/images/loading.gif) center no-repeat");		
	}
	
	.summaryAnalytics 
	{
		display: inline-block;
		margin: 15px 10px 5px 20px;
		vertical-align: top;
		width: 100%;
	}

	#date-range {
		float: right;
		position: absolute;
		margin-right:15px; 
	}
	
	
	.reportSummaryTitle {
	    font-weight: bold;
	    align: left
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
					
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;
	}
	
		
	.ui-widget-content {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
			
	}
	
	..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
	  
		border: none;
		color: #313131;
		font-weight: normal;			
	}

	
		
	

		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 16px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
			font-size:12px;
		}
		
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 2px 4px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 2px 4px !important;
		}
		
		.paging_full_numbers {
    		line-height: 16px;
		}

		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}
		
		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}
		
			
		.EBMDataTableWrapper
		{
			height:85%;
			min-height:260px;
			margin-top: 15px;
			overflow:auto;
		}

		.dataTables_wrapper {
			position: relative;
			clear: both;
			zoom: 1; /* Feeling sorry for IE */	
			overflow:auto !important;
			max-height:100%;						
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		
		
		.dashboard2x 
		{
			background-position: center center;
			background-repeat: no-repeat;
			border: nonr;
			<!---float: right;--->
			<!---height: 500px;
			min-height: 415px;
			margin: 5px;
		   <!--- min-width: 560px;--->
			position: static;
			width: 1120px;--->
			
				
			margin: 5px;
		   <!--- min-width: 560px;--->
			position: static;
			
		<!---	
			height: 400px;
			min-height: 400px;
			width: 750px;--->
			
			height: 500px;
			min-height: 500px;
			width: 1120px;
			
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 22px;
			height: 22px;  
			width:625px;
			max-width:625px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 22px; 
			height: 22px; 
		}
		
		.dataTable tbody > tr > td
		{ 
			white-space: nowrap; 
			overflow:hidden !important;
		}

		#tblListQueue 
		{
			<!--- width: 625px; --->			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
		#tblListResults 
		{
			<!--- width: 625px;	 --->		
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
	
	
		.tblListBatch
		{
			width: 300px !Important;			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
			border-bottom: 1px solid #CCCCCC;
		}
		
		#tblListBatch_1_info
		{
		   width: 300px !important;
		   padding-left: 10px;
		   margin-left: 13px;
		   display: none;
		}
	
	
		.dataTables_filter 
		{
			float: right;
			margin-right: 15px;
			text-align: right;
		}

		.datatable td 
		{
		  overflow: hidden; 
		  text-overflow: ellipsis; 
		  white-space: nowrap;
		}
		
		.rsheader
		{	
			width:1345px; 
			float:left;
		}
		
		.SingleWide
		{
		 	width:650px; 
		 	height:365px;
		}
		
		.DoubleWide
		{
		 	width:1320px; 
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 18px;
			height: 18px;  
			width:417px;
			max-width:417px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 18px; 
			height: 18px; 
		}
		
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:10px;
			color:#666;
		}
				
		.dataTables_length 
		{
		    margin-left: 10px;
		}
		
		
		
table 
{
    table-layout:fixed;
	border-collapse:collapse;
}

td
{
    overflow:hidden;
    text-overflow: elipsis;
	padding: 0 8px 0 8px;
}

th
{
	text-align:left;
	padding: 0 8px 0 8px;	
}

thead tr
{
	background-color: #E9E9CE;	
	
}

.alignRight { text-align: right; }
.alignLeft { text-align: left; }
.alignCenter { text-align: center; }


.DownloadLinkText {
	color: #0063DC !important;
	text-decoration: none !important;
}

.DownloadLink {
	color: #0063DC !important;
	text-decoration: none !important;
}

.excelIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.excelIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.wordIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.wordIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.pdfIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.pdfIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.dataTables_info {
    background-color: #EEEEEE;
    border-top: 2px solid #46A6DD;
    box-shadow: 3px 3px 5px #888888;
    line-height: 39px;
    margin-left: 0;
    /*padding-left: 10px;*/
    padding-left:0;
    position: relative;
    top: 0 !important;
    width: 100%;
	font-size:12px;
	text-align:left;
}

			

</style>

<!--- Dashboard object styles --->
<style>

	.DashboardLeftMenu 
	{
		float: left;
		min-height: 870px;
		width: 350px;
		background-color: #FFFFFF;
    	border: 1px solid #CCCCCC;
	    border-radius: 4px;
	}

	.droppableContentDashboard
	{
		box-shadow: 3px 3px 5px #888888 !important;
		margin: 0 15px 15px 15px;
		padding: 10px 10px 300px 10px;		
		width: 1210px;
		max-width: 1210px;
		min-height:800px;
		border:none !important;
		float:left;
	}
	
	.DashObjHeader 
	{
		background-color: #118ACF;
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;
		color: #FFFFFF;
		font-family: "Verdana";
		font-size: 14px;
		height: 28px;
		min-height: 28px;
		line-height: 28px;
		padding-left: 10px;
		position:relative;	
		cursor: move; 		
	}
	
	.DashObjHeader:hover
	{
		<!---color:#15436C;	--->	
	}
	
<!---	.DashObjHeader:after 
	{  
		content: "\f0c9";  
		display: inline-block;  
		font-family: "FontAwesome";  
		position: absolute;  
		right: 18px;  
		top: 3px;  
		text-align: center;  
		line-height: 28px;  
		color: rgba(255,255,255,.2);  
		text-shadow: 0px 0px 0px rgba(0,0,0,0);  
		cursor: move;  
	}  
	--->

	.DashBoardHeaderMenu
	{
		float: right;
		margin-right:10px;			
	}
		
	.DashBoardHeaderMenu a
	{		
		margin-left:3px;
		color:#118ACF !important; 	
	}
	
	.DashObjHeader:hover a
	{				
		color:#15436C !important; 	
	}
				
	<!--- Class used to distiguish between drop and sort objects --->	
	.DashObj
	{
		
	}
	
	.DashObjPlaceHolder
	{
		width: 575px;
		min-width: 575px;
		height:300px;
		min-height:300px;		
	}
	
	.DashBorder
	{		
    	box-shadow: 3px 3px 5px #888888;
		margin:15px;
	}
	
	.DashObjChart
	{
		width:100%;
		height:100%;	
		position:relative;	
	}
	
	.DashLeft
	{
		float:left;
	}
	
	.DashRight
	{
		float:right;
	}
	
	.DashSize_Width_Small
	{		
		width: 272px;
		min-width: 272px;		
	}
	
	.DashSize_Width_Med
	{		
		width: 575px;
		min-width: 575px;		
	}
	
	.DashSize_Width_Large
	{
		width: 1180px;
		min-width: 1180px;
	}
	
	.DashSize_Height_Tiny
	{
		height:135px;
		min-height:135px;
	}
	
	.DashSize_Height_Small
	{
		height:300px;
		min-height:300px;
	}
	
	.DashSize_Height_Med
	{
		height:575px;
		min-height:575px;
	}
	
	.DashSize_Height_Large
	{
		height:1180px;
		min-height:1180px;
	}	
	
	
	.Trans90
	{
		display:block;	
		transform: rotate(90deg);	 
		-o-transform: rotate(90deg);
		-khtml-transform: rotate(90deg);
		-webkit-transform: rotate(90deg); 
		-moz-transform: rotate(90deg);				
	}
	
	.Trans180
	{
		display:block;
		transform: rotate(180deg);	 
		-o-transform: rotate(180deg);
		-khtml-transform: rotate(180deg);
		-webkit-transform: rotate(180deg); 
		-moz-transform: rotate(180deg);		
	}
	
	.Trans270
	{
		display:block;	
		transform: rotate(270deg);	 
		-o-transform: rotate(270deg);
		-khtml-transform: rotate(270deg);
		-webkit-transform: rotate(270deg); 
		-moz-transform: rotate(270deg);		
	}

	<!--- text based table for sizer --->
	.DashObjSizer
	{	
	 	border:none;	
		table-layout:fixed;
		border-collapse:collapse;	
		font-size:12px;
	}
	
	.DashObjSizer td
	{	
	 	padding: 0 3px 0 3px;		
		margin: 0;
		line-height:9px;
		text-align:center;
	}
	
	.DashObjSizer td:hover
	{	
	 	background:#666666;	
	}
	
	.DashObjSizer tr
	{		 
		line-height:9px;
	}
	
	.DashObjHeaderText
	{
		width:70%; 
		overflow:hidden; 
		display:inline-block;	
		text-align:left;	
	}
	
	.EBMDashCounterWrapper
	{
		
		background-color: #8099b1; 
		text-align:center;		
	}

	.Absolute-Center {
	  width: 90%;
	  height: 60%;
	  overflow: auto;
	  margin: auto;
	  position: absolute;
	  top: 10; left: 0; bottom: 0; right: 0;
	}

	.EBMDashCounterWrapper h1
	{
		color:#FFF;
		font-size:36px;		
	}
	
	.RSHighAlert
	{
		background-color: rgba(255,0,0,0.7); 
		font-size:36px;		
	}
	
	.RSMedAlert
	{
		background-color: rgba(255,255,0,0.7); 
		font-size:36px;		
	}
	
	.EBMDashCounterWrapper h2
	{
		color:#CCC;
		font-size:24px;	
	}

</style>




<script language="javascript">
	$('#mainTitleText').html('<cfoutput>#Reporting_Title# <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> #Reporting_Campaigns_Title#</cfoutput>');
	$('#subTitleText').html('<cfoutput>#BATCHTITLE#</cfoutput>');	
</script>

<!--- Used to specify default Dashboard Object Sizes --->
<cfset CounterTinyClassString = "DashObj DashLeft DashBorder DashSize_Height_Tiny DashSize_Width_Small" />
<cfset CounterSmallClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_Small" />
<cfset ListMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_Med" />
<cfset ChartMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_Med" />

<script language="javascript">
<!--- Create javascript Dashboard Templates--->


	var DT_VoiceObj = new Object();
	DT_VoiceObj.Length = 3;
	
	
	DT_VoiceObj.Pos = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.Name = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.Type = new Array(DT_VoiceObj.Length);
	DT_VoiceObj.ClassString = new Array(DT_VoiceObj.Length);
	
	DT_VoiceObj.Pos[0] = 1;
	DT_VoiceObj.Name[0] = "ComboResults";
	DT_VoiceObj.Type[0] = "CHART";
	DT_VoiceObj.ClassString[0] = "<cfoutput>#ChartMediumClassString#</cfoutput>"
	
	DT_VoiceObj.Pos[1] = 2;
	DT_VoiceObj.Name[1] = "Display_c_queue";
	DT_VoiceObj.Type[1] = "FORM";
	DT_VoiceObj.ClassString[1] = "<cfoutput>#CounterTinyClassString#</cfoutput>"
	
	DT_VoiceObj.Pos[2] = 3;
	DT_VoiceObj.Name[2] = "Display_c_results";
	DT_VoiceObj.Type[2] = "FORM";
	DT_VoiceObj.ClassString[2] = "<cfoutput>#CounterTinyClassString#</cfoutput>"
	
    <!--- DT_VoiceObj.Pos[3] = 4;
	DT_VoiceObj.Name[3] = "Display_c_open";
	DT_VoiceObj.Type[3] = "FORM";
	DT_VoiceObj.ClassString[3] = "<cfoutput>#CounterTinyClassString#</cfoutput>" --->
	 
	
</script>


<div class="reportingContent">

<div id="reportSummaryOverlay" class="no-print"></div>


	<div style="float:left; width:350px;" class="DashboardReportingLeftMenu">
        <div class="content-EMS SideSummary no-print">
           
            <div class="head-EMS">Campaign Range</div>
            
            <div class="summaryAnalytics"> 
                <div id="date-range">
                    <div id="date-range-field">
                        <span>
                        </span>
                        <a href="javascript:void(0)">
                            &#9660;
                        </a>
                    </div>
                    <!--- This widget was manually customized for EBM with preset date range options in the raw #rootUrl#/#publicPath#/js/datepicker/datepicker.js --->
                    <div id="datepicker-calendar">
                    </div>
                </div>
            </div>
           
            <div class="summaryAnalytics" style="margin-top:40px;"> 
                <span class="reportSummaryTitle">
                    Report Selected Range
                </span>
                
                <BR />
                 
                <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
                <span id="SelectedMinBoundaryDate" rel1=""></span>   
               
                <BR />
                
                <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
                <span id="SelectedMaxBoundaryDate" rel1=""></span>
                                 
            </div>
            
            
            <div class="summaryAnalytics" style="visibility:hidden;"> 
                <span class="reportSummaryTitle">
                    Overall Campaign Range
                </span>
                
                <BR />
                
                <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
                <span id="MinBoundaryDate" rel1=""></span>   
                
                <BR />
                
                <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
                <span id="MaxBoundaryDate" rel1=""></span>
                     
            </div>
               
        </div>
        
    
        <div class="clear"></div>
        <BR/>
        
        <div class="DashboardLeftMenu no-print">
        <!--- 	    
        
        
        <div style="margin: 20px 0 0 20px;">
            Filters
        </div>
        
        <div class="filterBox">
            Type 
            <select>
                <option>
                    SMS
                </option>
                <option>
                    eMail
                </option>
                <option>
                    Voice
                </option>
                <option>
                    Survey
                </option>
            </select>
        </div>--->
        
        <cfoutput>    
           	<div class="content-EMS">
            	<div class="head-EMS">Report Options</div>
            </div>
                            
            <div class="reportList">
                <ul id="ReportMenu">
                
                 <li class="content-EM"S>
                            <h3 class="head-EMS"><a href="##">Combo Results</a></h3>
                        
                            <ul>
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="ComboResults" rel2="CHART">
                                        Combo Results
                                    </span>
                                </li>
                                </cfif>
                                
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_BatchSummary" rel2="TABLE">
                                        Campaign Summary
                                    </span>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_results" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Results Counter
                                    </span>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_queue" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - In Queue
                                    </span>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_queue_total" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - Processed
                                    </span>
                                </li>
                                </cfif>
                                
                                <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_QueueSummary" rel2="TABLE" rel3="#ListMediumClassString#">
                                        Queue List
                                    </span>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_ResultSummary" rel2="TABLE" rel3="#ListMediumClassString#">
                                        Results List
                                    </span>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_CallResultsPerHour_Title).havePermission>
                                <li>
                                    <span class="menuItem" rel1="ComboResultsByHour" rel2="CHART"> 
                                        Combo Results per Hour
                                    </span>
                                </li>
                                </cfif>
                                
                                 <cfif permissionObject.havePermission(Reporting_DialHistoryLookup_Title).havePermission>
                                 <li>
                                    <span class="menuItem" rel1="Form_ContactStringDialHistory" rel2="FORM" title="List Call Results for Contact String for Date Range">
                                        Contact History Lookup
                                    </span>
                                </li>
                                </cfif>                            
                                
                            </ul>
                   </li>    
                                                
                     <cfif permissionObject.havePermission(Reporting_CallResults_Title).havePermission>  
                    <li>
                        <h3><a href="##">Call Results</a></h3>
                    
                        <ul>
                                                   
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTime_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1="CallAvgCallLength" rel2="CHART">
                                    Avg Call Time
                                </span>
                            </li>
                            </cfif>
                           
                                                     
                            <cfif permissionObject.havePermission(Reporting_LiveCallsPerHour_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CallLivePerHour" rel2="CHART"> 
                                    Live Calls per Hour
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_MachineCallsPerHour_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CallMachinePerHour" rel2="CHART"> 
                                    Machine Calls per Hour
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTimeBy_CallResult_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CallAvgLengthByResult" rel2="CHART">
                                    Avg Call Time By Call Result
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_FinalCallResults_Basic_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Display_FinalDialResults" rel2="TABLE">
                                    Final Call Results Basic
                                </span>
                            </li>  
                            </cfif>
                           
                            
                        
                        </ul>
                        
                    </li>    
                    </cfif>
                       
                    <cfif permissionObject.havePermission(Reporting_States_Title).havePermission>                
                    <li>
                        <h3><a href="##">Location</a></h3>
                    
                        <ul>
                            <cfif permissionObject.havePermission(Reporting_CallResults_ByState_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CallResultsByState" rel2="CHART">
                                    Call Results by State
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_AvgCallTime_ByState_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CallAvgCallLengthByState" rel2="CHART">
                                    Avg Call Time By State
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_TotalCalls_ByState_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="TotalCallsByState" rel2="CHART">
                                    Total Calls By State
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_TotalCalls_ByState_Title).havePermission>
                                <li>
                                    <span class="menuItem" rel1="MTCountByState" rel2="CHART" title="SMS MT Count by State">
                                        SMS MT Counts By State
                                    </span>
                                </li>
                               
                                <li>
                                    <span class="menuItem" rel1="MTCountByZip" rel2="CHART" title="SMS MT Count by Top 125 Zip codes">
                                        SMS MT Counts By Zip Code
                                    </span>
                                </li>
                               
                                <li>
                                    <span class="menuItem" rel1="MTCountByMSA" rel2="CHART" title="SMS MT Count by The Metropolitan Statistical Area (MSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                        SMS MT Counts By MSA
                                    </span>
                                </li>
                           
                                <li>
                                    <span class="menuItem" rel1="MTCountByPMSA" rel2="CHART" title="SMS MT Count by The Primary Metropolitan Statistical Area (PMSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                        SMS MT Counts By PMSA
                                    </span>
                                </li>
                               
                                <li>
                                    <span class="menuItem" rel1="MTCountByFIPS" rel2="CHART" title="SMS MT Count by The FIPS (Federal Information Processing Standard) number assigned to each county in the U.S. by the Census Bureau. The first two digits are the state number, and the last three digits are the county number.">
                                        SMS MT Counts By FIPS
                                    </span>
                                </li>
                            </cfif>                            
                             
                        </ul>
                    
                    </li>
                    </cfif>
                         
                     
                    <cfif permissionObject.havePermission(Reporting_SMS_Title).havePermission>    
                    <li>
                        <h3><a href="##">SMS</a></h3>
                    
                        <ul>			
                        
                        	<li>
                                <span class="menuItem" rel1="Display_c_smsmt" rel2="FORM" rel3="#CounterTinyClassString#">
                                    SMS MT Counter
                                </span>
                            </li>
                                
                            <li >
                                <span class="menuItem" rel1="Display_c_smsmo" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS MO Counter
                                </span>
                            </li>
                            
                            <li >
                                <span class="menuItem" rel1="SMSSurveyComboResults" rel2="CHART" rel3="#ChartMediumClassString#" title="Status of Surveys Started in Date Range Specified">
                                    SMS Survey Status
                                </span>
                            </li>
                            
                            <li >
                                <span class="menuItem" rel1="Display_c_smsSurvey" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS Surveys Requested
                                </span>
                            </li>
                            
                            <cfif permissionObject.havePermission(Reporting_optin_Title).havePermission>   
	                            <li >
	                                <span class="menuItem" rel1="Form_optin" rel2="FORM" title="number of users optin campaigns.">
	                                    SMS Optin
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_optout_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_optout" rel2="FORM" title="number of users optout campaigns.">
	                                    SMS Optout
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Activeusers_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_activeusers" rel2="FORM" title="number of active users.">
	                                    Active Users
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Campaigndropout_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_dropout" rel2="FORM" title="number of drop out users.">
	                                    Campaign Dropouts 
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Userinformation_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information 
	                                </span>
	                            </li>
	                            
	                            <li>
	                                <span class="menuItem" rel1="legacy_Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information (Legacy)
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Userhistory_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_cshistory" rel2="FORM" title="History of Contact String">
	                                    User History
	                                </span>
	                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Comparequestionresponses_Title).havePermission>
	                            <li>
	                                <span class="menuItem" rel1="Form_cpcompare" rel2="FORM" title="Compare control points in keyword">
	                                    Compare question responses 
	                                </span>
	                            </li>
                            </cfif>
                            
                        	<li>
                                <span class="menuItem" rel1="Form_chartMO" rel2="FORM" title="Time chart MO">
                                    MO Charts 
                                </span>
                            </li>
                            
                            <cfif permissionObject.havePermission(Reporting_SMSSessionstate_Title).havePermission> 
                                <li>
                                    <span class="menuItem" rel1="SMSSessionStates" rel2="CHART">
                                        SMS Session States
                                    </span>
                                </li>
                           </cfif>
                        <!---    <li>
                                <span class="menuItem" rel2="CHART">
                                    SMS Delivered
                                </span>
                            </li>
                            <li>
                                <span class="menuItem" rel2="CHART">
                                    SMS Undelivered
                                </span>
                            </li>--->
                            
                         <!---   <cfif permissionObject.havePermission(Reporting_ActiveICBy_Keyword_Title).havePermission>
                             <li>
                                <span class="menuItem" rel1="ICByKeyword" rel2="CHART" title="Active Interactive Campaign (IC) by Keyword">
                                    Active IC By Keyword
                                </span>
                            </li>
                            </cfif>
						
                    
                             <cfif permissionObject.havePermission(Reporting_ActiveIC_Details_Title).havePermission>
                             <li>
                                <span class="menuItem" rel1="Display_ICByKeywordDetails" rel2="TABLE" title="Active Interactive Campaign (IC) Details">
                                    Active IC Details
                                </span>
                            </li>
                            </cfif>
                    
                            <cfif permissionObject.havePermission(Reporting_ActiveIC_Counts_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="ICActivated" rel2="CHART" title="Active Interactive Campaign (IC) for Date Range">
                                    Actived IC Counts
                                </span>
                            </li>
                            </cfif>
                            
							--->
                            
                            
                            <cfif permissionObject.havePermission(Reporting_MTDelivery_Results_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Form_ContactStringMTStatusUpdates" rel2="FORM" title="MT Delivery Results for Contact String for Date Range">
                                    MT Delivery Results
                                </span>
                            </li>
                            </cfif>
                    		
                    		<li>
                                <span class="menuItem" rel1= "Form_campaigndetails" rel2="FORM" >
                                  Keyword Response Details
                                </span>
                            </li>
                            
                            <li>
                                <span class="menuItem" rel1= "Form_CSdetails" rel2="FORM" >
                                  Contact String Details
                                </span>
                            </li>
                            
                          <!---  <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Form_moresponses" rel2="FORM" title="MO Keyword Response Counts by Short Code">
                                    MO Responses
                                </span>
                            </li>
                            </cfif>
							--->
                            
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Form_cpresponses" rel2="FORM" title="MO IC Response Counts by Control Point">
                                    CP Responses
                                </span>
                            </li>
                            </cfif>
                            
                            
                        <!---  These are commented out until they are working within format and indexed properly   
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="CompareKeywords" rel2="FORM" title="User's Response">
                                    Compare Keywords
                                </span>
                            </li>
                            </cfif>
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Form_getRegUsers" rel2="FORM" title="User's Response">
                                    Registered Users
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1="Form_filteredresponses" rel2="FORM" title="Survey Results Filtered">
                                    Survey Status on Campaign Filter
                                </span>
                            </li>
                            </cfif>
							
						--->
                                                    
                            
                        </ul>
                    
                    </li>
                    </cfif>
                        
                    
                    <cfif permissionObject.havePermission(Reporting_Email_Title).havePermission> 
                    <li>
                        <h3><a href="##">eMail</a></h3>
                    
                        <ul>
                           
                            <cfif permissionObject.havePermission(Reporting_Email_Pie_Chart_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "EmailResults" rel2="CHART" >
                                    Events Percentage Distribution                            
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Events_Bar_Graph_Title).havePermission>
                             <li>
                                <span class="menuItem" rel1= "GraphResult" rel2="CHART" >
                                    Events Bar Graph
                                </span>
                            </li> 
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_Events_Results_Table_Title).havePermission>
                             <!--- <li>
                                <span class="menuItem" rel1= "Display_EmailLog" rel2="TABLE" >
                                    Events Results Table
                                </span>
                            </li>  --->
                            <li>
                                <span class="menuItem" rel1="Display_FinalEmailResultsBasic" rel2="TABLE">
                                    Final Email Results 
                                </span>
                            </li>  
                            </cfif>   
                            
                            <!--- <cfif permissionObject.havePermission(Reporting_Email_History_Lookup_Title).havePermission>
                            <li>
                                <span class="menuItem" rel1= "Form_EmailStringHistory" rel2="FORM" title="List Email Results for specific Date Range">
                                    Email History Lookup
                                </span>
                            </li> 
                            </cfif>  --->  
                            
                            <cfif permissionObject.havePermission(Reporting_Opens_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_opens" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Opens
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Delivered_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_delievered" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Delivered
                                 </span>
                            </li>
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_Spamreport_Title).havePermission>
                             <li>
                                 <span class="menuItem" rel1="Display_c_spamreport" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Spamreport
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Bounces_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_bounce" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Bounces
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Unsubscribes_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_unsubscribe" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Unsubscribes
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Deferred_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_deferred" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Deferred
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Dropped_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_dropped" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Dropped
                                 </span>
                            </li>
                            </cfif>
                            
                       </ul>
                    
                    </li>
                    </cfif> 
                   
                   
                   <!---	<cfif 1 EQ 1>
                    <li>
                        <h3><a href="##">Group (Contact List) Reports</a></h3>
                        
                        
                         <ul>
	                        <cfif 1 EQ 1>
                             <li>
                                 <span class="menuItem" rel1="Display_c_smsmo_noq" rel2="FORM" rel3="#CounterTinyClassString#" title="Distribution of CDF Choice">
                                    CDF Distribution Counts
                                </span>
                            </li> 
                            
                            </cfif>  
                            
                         </ul>   
                        
                    </li>       
                    
                    </cfif>--->
                                                       
                    <!--- System Reports--->
                    <cfif permissionObject.havePermission(Reporting_System_Group_Title).havePermission> 
                    <li>
                        <h3><a href="##">System Reports</a></h3>
                    
                        <ul>
	                        <cfif permissionObject.havePermission(Reporting_System_MO).havePermission>
                             <li>
                                 <span class="menuItem" rel1="Display_c_smsmo_noq" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's for billing special - No Queue counts">
                                    SMS MO No Q Counter
                                </span>
                            </li> 
                            
                            </cfif>  
                            
                            
                           	<cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_results" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Results Counter
                                    </span>
                                </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_queue_system" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - System In Queue
                                    </span>
                                </li>
                            </cfif>
                            
                             <cfif permissionObject.havePermission(Reporting_CallResultsChild_Title).havePermission> 
                                <li >
                                    <span class="menuItem" rel1="Display_c_queue_system_total" rel2="FORM" rel3="#CounterTinyClassString#">
                                        Queue Counter - System Processed
                                    </span>
                                </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_System_MO).havePermission>
                             <li>
                                 <span class="menuItem" rel1="Display_c_smsmo_sys" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's regardless of user or campaign">
                                    SMS MO Counter
                                </span>
                            </li> 
                            
                            </cfif>  
                            
                            <cfif permissionObject.havePermission(Reporting_System_MT).havePermission>
                            <li>
                                <span class="menuItem" rel1= "Form_EmailStringHistory" rel2="FORM" title="List Email Results for Email Address for Date Range">
                                    MT Summary
                                </span>
                            </li> 
                            </cfif>   
                            
                            <cfif permissionObject.havePermission(Reporting_SMSMO_Log_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1="Display_MOLog" rel2="TABLE">
                                    SMS MO Log
                                </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_System_Avg_Queue_Processing_Time).havePermission>
                            <li>
                                <span class="menuItem" rel1= "Display_rs_c_avgqueueproctime" rel2="FORM" rel3="#CounterTinyClassString#" title="Get Avg Porcessing Time to Insert to Queue">
                                    AVG Process Time - Insert Queue
                                </span>
                            </li> 
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Opens_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_opensGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Opens
                                 </span>
                            </li>
                            </cfif>
                            
                            
                            <cfif permissionObject.havePermission(Reporting_Delivered_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_delieveredGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Delivered 
                                 </span>
                            </li>
                            </cfif>
                            
                            
                            
                            <cfif permissionObject.havePermission(Reporting_Spamreport_Overall_Title).havePermission>
                             <li>
                                 <span class="menuItem" rel1="Display_c_spamreportGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Spamreport 
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Bounces_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_bounceGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Bounces 
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Unsubscribes_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_unsubscribeGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Unsubscribes 
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Deferred_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_deferredGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Deferred 
                                 </span>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_Dropped_Overall_Title).havePermission>
                            <li>
                                 <span class="menuItem" rel1="Display_c_droppedGeneral" rel2="FORM" rel3="#CounterTinyClassString#">
                                  Overall Dropped 
                                 </span>
                            </li>  
                            </cfif>
                             <cfif permissionObject.havePermission(Reporting_Client_Stats_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "ClientStats" rel2="CHART" >
                                    Clients Stats
                                </span>
                            </li> 
                            </cfif>  
                             <cfif permissionObject.havePermission(Reporting_Unique_Client_Stats_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "UniqueClientsStats" rel2="CHART" >
                                   Unique Clients Stats
                                </span>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_ClientStats_Piechart_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "ClientsStatsPieChart" rel2="CHART" >
                                   Clients Stats Pie Chart
                                </span>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Open_Devices_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "DeviceStats" rel2="CHART" >
                                  Open Device Types
                                </span>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Unique_Devices_Title).havePermission> 
                             <li>
                                <span class="menuItem" rel1= "UniqueDeviceStats" rel2="CHART" >
                                  Unique Device Types
                                </span>
                            </li> 
                            </cfif> 
                            
                            <cfif permissionObject.havePermission(Reporting_Global_Stats_Title).havePermission> 
                             <li>
                                <span class="menuItem" rel1= "GlobalStats" rel2="CHART" >
                                  Advanced Global Stats
                                </span>
                            </li>
                            </cfif>  
                            
                            <cfif permissionObject.havePermission(Reporting_ISPS_Results_Title).havePermission> 
                            <li>
                                <span class="menuItem" rel1= "ISPStats" rel2="CHART" >
                                  ISPS Results
                                </span>
                            </li>
                            </cfif>  
                                        
                            <!--- <cfif permissionObject.havePermission(Reporting_ISPS_Results_Title).havePermission> --->
                            <li>
                                <span class="menuItem" rel1= "rs_avgsmsposttime" rel2="CHART" >
                                  Avg SMS Post Time
                                </span>
                            </li> 
                           <!---  </cfif>  --->    
                           
                           <cfif permissionObject.havePermission(Reporting_SMPPError_Title).havePermission>      
                           <li>
                                <span class="menuItem" rel1="Form_smssmpperror" rel2="FORM" title="SMPP SMS Error">
                                    SMPP-SMS Error 
                                </span>
                            </li> 
                            </cfif>            
                                                         
                        </ul>
                    
                    </li>
                    </cfif> 
                    
                    
                    <!--- Templates --->
                    <cfif permissionObject.havePermission(Reporting_Dashboard_Template_Group_Title).havePermission> 
                    <li>
                        <h3><a href="##">Dashboard Templates</a></h3>
                   		
                        
                        <ul id="DashboardTemplates">
                       
                       		<li><span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;"><img src="#rootUrl#/#publicPath#/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></span></li>
                                                                                    
                        </ul>
                    
                    </li>
                    </cfif> 
                    
                    
                   <!--- <cfif permissionObject.havePermission(Reporting_Test_Title).havePermission>                     
                         <li>
                            <h3><a href="##">Special</a></h3>
                        
                            <ul>
                            
                         
                                <li>
                                    <span class="menuItem" rel1="SMSKeywordSummary" rel2="CHART" >
                                        TestNew
                                    </span>
                                </li>
                            </ul>
                            
                        </li>        
                    </cfif>--->
                    
                </ul>
            </div>
        </cfoutput>
         
             
        </div>     
	
    </div> <!--- Left Menu area--->
    
    
    <!--- Template Maker --->
    <cfsavecontent variable="DashBoardOptions"><div style='width:30%; display:inline-block; float:right;'><span class='DashBoardHeaderMenu'><a onclick='drawChart();' title='Reload Dashboard Data'>Reload</a> | <a onclick='SaveCurrentAsTemplateDialog()' title='Make a template of current Dashboard Objects'>Template</a> | <a onclick='ClearDashboard()' title='Clear all dashboard objects'>Clear</a></span></div></cfsavecontent>       
   
    <div class="content-EMS" style="width: 1230px; float:left; margin-left: 15px;">
       	<div class="head-EMS DashObjHeader no-print">Dashboard <cfoutput>#DashBoardOptions#</cfoutput></div>
    </div>
	
    <!--- No newlines so can include in jquery .append--->    
    <cfsavecontent variable="DashBoardPrintMenu"><div class="reportingHeader print-display-only"><h2><cfoutput>#BATCHTITLE#</cfoutput></h2><div class="summaryAnalytics" style="margin-top:40px;"> <span class="reportSummaryTitle">         Report Selected Range   </span>    <BR /> <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span><span id="SelectedMinBoundaryDatePrint" rel1=""></span>      <BR />   <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span><span id="SelectedMaxBoundaryDatePrint" rel1=""></span> </div></div> </cfsavecontent>       
  
  

    <div class="droppableContentDashboard" id="DashboardContent">
      
        <!---<div>Drop stuff here....</div>--->
        
    </div>
    
</div>



    
	<!--- 1=up 2=right 3=left 4=down--->
    <cfsavecontent variable="DashObjSizer">
    	<div style='width:30%; display:inline-block; float:right;'>
        <span class='DashBoardHeaderMenu'><a onclick='RemoveDashboardObject(this)'>X</a></span>
        <span class='DashBoardHeaderMenu'>
            <table class='DashObjSizer'>
                <tr>
                    <td colspan='2'><a onclick='SizeDashboardObject(this,3)' title='Shrink Veritcal'>^</a></td>
                </tr>
                <tr>
                    <td><a onclick='SizeDashboardObject(this,4)' class='Trans270' title='Shrink Horizontal'>^</a></td>
                    <td><a onclick='SizeDashboardObject(this,2)' class='Trans90' title='Grow Horizontal'>^</a></td>
                </tr>
                <tr>
                    <td colspan='2'><a onclick='SizeDashboardObject(this,1)' class='Trans180' title='Grow Veritcal'>^</a></td>
                </tr>
        </table>
        </span>
        </div>
        
    </cfsavecontent>    
    
    <!--- inline string - edit above and recreate string as needed --->
    <cfsavecontent variable="DashObjSizer"><div style='width:30%; display:inline-block; float:right;'><span class='DashBoardHeaderMenu'><a onclick='RemoveDashboardObject(this)'>X</a></span><span class='DashBoardHeaderMenu'><table class='DashObjSizer'><tr><td colspan='2'><a onclick='SizeDashboardObject(this,3)' title='Shrink Veritcal'>^</a></td></tr><tr><td><a onclick='SizeDashboardObject(this,4)' class='Trans270' title='Shrink Horizontal'>^</a></td><td><a onclick='SizeDashboardObject(this,2)' class='Trans90' title='Grow Horizontal'>^</a></td></tr><tr><td colspan='2'><a onclick='SizeDashboardObject(this,1)' class='Trans180' title='Grow Veritcal'>^</a></td></tr></table></span></div></cfsavecontent>    
    

<script type="text/javascript">

	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var PriorStartDate = null;
	var PriorEndDate = null;
	
	var _tblListBatch_1 = null;
	
	var chartQuantity = 0;
	var CountDroppedThisSession = 0;
	
	<cfset DashObjMenu = "#DashObjSizer#" />
	
	<!--- Declared here but called in header as optional if exists--->
	function AdditionalHideTasks()
	{
		$('.DashboardReportingLeftMenu').hide("fast", function(){
			
		});
	}
	
	<!--- Declared here but called in header as optional if exists--->
	function AdditionalShowTasks()
	{
		$('.DashboardReportingLeftMenu').show("fast");
	}
		
	$(document).ready(function()
	{
	
		<!--- Make Reports Menu Accordion Style --->
		 var icons = {
		  header: "ui-icon-circle-arrow-e",
		  activeHeader: "ui-icon-circle-arrow-s"
		};
		$( "#ReportMenu" ).accordion({
		  icons: icons,
		  heightStyle: "content"
		});
		$( "#toggle" ).button().click(function() {
		  if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
			$( "#accordion" ).accordion( "option", "icons", null );
		  } else {
			$( "#accordion" ).accordion( "option", "icons", icons );
		  }
		});
	
	
		ReadDateBoundaries();
					
	});
	
	function initDataTable(inpObj, inpReportName, inpcustomdata1, inpcustomdata2, inpcustomdata3, inpcustomdata4, inpcustomdata5, inpCustomConfigDataTablesSort)
	{				
		<!---	"bAutoWidth": false,
			"aoColumns" : [
            { sWidth: '25%' },
            { sWidth: '25%' },
            { sWidth: '25%' },
            { sWidth: '25%' } ],
			
			 "sScrollX": "100%",
		    "sScrollY": "100%",	
			
			
			--->
		
		<!--- Set default sorting if not defined in display method --->	
		if(typeof(inpCustomConfigDataTablesSort) == "undefined" || inpCustomConfigDataTablesSort == "")
			inpCustomConfigDataTablesSort = '[[ 0, "desc" ]]';	
					
		inpObj.dataTable( {		
		   
			"bProcessing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
			"bLengthChange": true,
			"aaSorting": eval(inpCustomConfigDataTablesSort),
			"bServerSide": true,
			"sPaginationType": "full_numbers",
			"sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=" + inpReportName + "&returnformat=plain&queryformat=column",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
								
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": encodeURI("<cfoutput>#inpBatchIdList#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
					
					if(String(inpcustomdata1).length > 0)
						aoData.push({"name": "inpcustomdata1", "value": encodeURI(inpcustomdata1)});
					
					if(String(inpcustomdata2).length > 0)
						aoData.push({"name": "inpcustomdata2", "value": encodeURI(inpcustomdata2)});
					
					if(String(inpcustomdata3).length > 0)
						aoData.push({"name": "inpcustomdata3", "value": encodeURI(inpcustomdata3)});
					
					if(String(inpcustomdata4).length > 0)
						aoData.push({"name": "inpcustomdata4", "value": encodeURI(inpcustomdata4)});
						
					if(String(inpcustomdata5).length > 0)
						aoData.push({"name": "inpcustomdata5", "value": encodeURI(inpcustomdata5)});		
						
   								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    $('td', nRow).attr('nowrap','nowrap');
                    return nRow;
               },
			   "fnInitComplete":function(oSettings, json){
										
					inpObj.find('thead tr th').each(function(index){
															
						$this = $(this);
						if($this.hasClass("nosort"))
						{				
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}
						
					  });
				  
												
					
				},
			   "fnDrawCallback": function( oSettings ) {
					
					<!--- For tables that are hidden overflow - add full text to title --->								
					inpObj.find('tbody tr td').each(function(index){
																
						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });
					  			  
					<!--- Init Download Link(s) They are contained in the parent two levels up - .parents().eq(1) is the same as .parent().parent()--->
					inpObj.parents().eq(2).find($(".DownloadLink")).unbind();
					inpObj.parents().eq(2).find($(".DownloadLink")).click(function() {
		  
			  	    var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
					
					var CustomParams = "";
					
					if(typeof($(this).attr("inpcustomdata1")) != "undefined")
						CustomParams += "&inpcustomdata1=" + encodeURIComponent($(this).attr("inpcustomdata1")) ;
						
					if(typeof($(this).attr("inpcustomdata2")) != "undefined")
						CustomParams += "&inpcustomdata2=" + encodeURIComponent($(this).attr("inpcustomdata2")) ;
						
					if(typeof($(this).attr("inpcustomdata3")) != "undefined")
						CustomParams += "&inpcustomdata3=" + encodeURIComponent($(this).attr("inpcustomdata3")) ;		
					
					if(typeof($(this).attr("inpcustomdata4")) != "undefined")
						CustomParams += "&inpcustomdata4=" + encodeURIComponent($(this).attr("inpcustomdata4")) ;	
						
					if(typeof($(this).attr("inpcustomdata5")) != "undefined")
						CustomParams += "&inpcustomdata5=" + encodeURIComponent($(this).attr("inpcustomdata5")) ;	
							
					      <!---console.log("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>"))--->
						window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>");
										  
				<!---	  	OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + '',
						"Export Table Data", 
						280, 
						500,
						"ExportTableDataForm",
						false);			--->  
					  
				  });
									
				
				  inpObj.find($("td")).mousewheel(function(event, delta) {
				
					<!---console.log('MouseWheel=' + delta);--->
					
					  this.scrollLeft -= (delta * 30);
					
					  event.preventDefault();
				
				   });
		
				}
	
			
    	} );
		
		
		
		<!--- Bind mouse wheel to scroll hidden overflow on td
		
		$(".td").bind("mousewheel",function(ev, delta) {
			var scrollTop = $(this).scrollTop();
			$(this).scrollTop(scrollTop-Math.round(delta));
		});

		
		--->
		
	}
	
	function initComponents(){
		
		<!---/* Special date widget */--->
		var to = new Date();
		var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
		if($("#MaxBoundaryDate").html() != "")
		{
			from = new Date($("#MinBoundaryDate").html());
			to = new Date($("#MaxBoundaryDate").html());
		}
		
   	    <!--- This widget was manually customized for EBM with preset date range options in the raw #rootUrl#/#publicPath#/js/datepicker/datepicker.js --->
       $('#datepicker-calendar').DatePicker({
			inline: true,
			date: [from, to],
			calendars: 3,
			mode: 'range',
			extraWidth: 100,
			current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
			onChange: function(dates, el){					
				
				// update the range display
				$('#date-range-field span').text(dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ', ' + dates[0].getFullYear() + ' - ' +
				dates[1].getDate() +
				' ' +
				dates[1].getMonthName(true) +
				', ' +
				dates[1].getFullYear());
									
				$("#SelectedMinBoundaryDate").html(days[dates[1].getDay()] + ', ' + dates[1].getMonthName(true) + ' ' + dates[1].getDate() + ', ' + dates[1].getFullYear());
				$("#SelectedMinBoundaryDate").attr("rel1", dates[1].getFullYear() + '-' + dates[1].getMonth() + '-' + dates[1].getDate());
				$("#SelectedMaxBoundaryDate").html(days[dates[0].getDay()] + ', ' + dates[0].getMonthName(true) + ' ' + dates[0].getDate() + ', ' + dates[0].getFullYear());
				$("#SelectedMaxBoundaryDate").attr("rel1", dates[0].getFullYear() + '-' + dates[0].getMonth() + '-' + dates[0].getDate());
			}
		});
		
		<!---// initialize the special date dropdown field--->
		$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
		to.getDate() +
		' ' +
		to.getMonthName(true) +
		', ' +
		to.getFullYear());
		
		<!---// bind a click handler to the date display field, which when clicked
		// toggles the date picker calendar, flips the up/down indicator arrow,
		// and keeps the borders looking pretty--->
		$('#date-range-field').bind('click', function()
		{
			var dateRange = $('#datepicker-calendar').DatePickerGetDate();
								
			<!--- Check if opening or closing range picker --->				
			if($('#datepicker-calendar').is(":visible"))
			{
				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
			}
			else
			{	
							
				PriorStartDate = days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear();
				PriorEndDate = days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear();										

				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
			}
				
			if ($('#date-range-field a').text().charCodeAt(0) == 9650) 
			{
				// switch to up-arrow
				$('#date-range-field a').html('&#9660;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 0,
					borderBottomRightRadius: 0
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 0
				});
			}
			else 
			{
				// switch to down-arrow
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			}
			return false;
		});
		
		
		<!--- Special easy to select preset values --->
		$('#EBMPBFull').bind('click', function()
		{							
			var from = new Date($("#MinBoundaryDate").html());
			var to = new Date($("#MaxBoundaryDate").html());
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());

		});
					
		$('#EBMPBLastSeven').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 7); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
		
		});
		
		$('#EBMPBLastThirty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastSixty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 60); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastNinety').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 90); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
				
		});
		
		<!--- Allow user to cancel and revert changes in date picker --->
		$('#EBMPresetsCancel').bind('click', function()
		{		
			var to = new Date(PriorEndDate);
			var from = new Date(PriorStartDate);
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
			
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
			$('#datepicker-calendar').hide();
			$('#reportSummaryOverlay').hide();
		
		});
		
					 
	   <!--- // global click handler to hide the widget calendar when it's open, and
		// some other part of the document is clicked.  Note that this works best
		// defined out here rather than built in to the datepicker core because this
		// particular example is actually an 'inline' datepicker which is displayed
		// by an external event, unlike a non-inline datepicker which is automatically
		// displayed/hidden by clicks within/without the datepicker element and datepicker respectively--->
		$('html').click(function(){
							
			if ($('#datepicker-calendar').is(":visible")) 
			{
				
				$('#datepicker-calendar').hide();
				$('#reportSummaryOverlay').hide();
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			
				var dateRange = $('#datepicker-calendar').DatePickerGetDate();
			
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
										
			}
		});
		
	   <!--- // stop the click propagation when clicking on the calendar element
		// so that we don't close it--->
		$('#datepicker-calendar').click(function(event)
		{
			event.stopPropagation();
		});	
		<!--- /* End special page widget */--->
		
		<!---/* add draggable for menu item */--->
		$(".menuItem").draggable({
			revert: "invalid",
			containment: "",
			opacity: 0.7,
			helper: "clone",
			cursor: "move",
			start: function(event, ui){
				$('.droppable').css({
					'border': 'solid 2px red'
				})
				
			},
			stop: function(event, ui){
				$('.droppable').css({
					'border': 'solid 1px #999999'
				})
			}
		});
		
	   <!--- /* End draggable event*/--->
		
		<!---/* Add droppable event*/--->
		 
		<!---drawChart();--->
		
		InitDashboardDroppable();
		
		LoadDashboardTemplateList();
				
		drawChart();
		
	}
	
	
	
	function loadingSingleChartData(id){
		$('#' + id).html('');
		$('#' + id).css("background", "url(../../public/images/loading.gif) center no-repeat");
	}
	
	function finishedLoadingSingleChartData(id){
		$('#' + id).css("background", "none");
	}
	
	function loadingData(){
		<!---$('#DashboardContent').css("background", "url(../../public/images/loading.gif) center no-repeat");
		$('.droppableContent').css('visibility', 'hidden');	--->	
		<!---$('#reportSummaryOverlay').toggle(); --->
	}
	
	function finishedLoadingData(){
		<!---$('#DashboardContent').css("background", "#FFF");
		$('.droppableContent').css('visibility', 'visible');--->
		<!--- $('#reportSummaryOverlay').toggle(); --->
	}
	
	function convertDateToTimestamp(date){
		return date.getFullYear() + '-' + (date.getMonth() +1) + '-' +date.getDate();
	}
	
	function resizeChart()
	{		
		var realWidth = $('.DashObjChart').width()-1;
		var realHeight = $('.DashObjChart').height()-1;
				
		$('.DashObj').each(function(index)
		{															
			$this = $(this);
		
			<!--- Chart size is dynamic based on obj and header sizes - Reset here --->				
			var heightNew = $this.height() - $this.children('.DashObjHeader').height();
			$this.children('.DashObjChart').height(heightNew);
		});
						
		for(index in AmCharts.charts )
		{			
			AmCharts.charts[index].invalidateSize();
		}				
	}
	
	function ReadDateBoundaries()
	{
		$('.reportingTimeSummary').css("background", "url(../../public/images/loading.gif) center no-repeat");
		
		var data = 
		{ 
			inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
			LimitDays: 1
		};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc','ReadDateBoundaries', data,
		'Get boundary data for reporting fail',		
		function(results)
		{
			$("#MaxBoundaryDate").html(results.MAXDATE);
			$("#MinBoundaryDate").html(results.MINDATE);
			
			$("#SelectedMaxBoundaryDate").html(results.MAXDATE);
			$("#SelectedMinBoundaryDate").html(results.MINDATE);
						
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();
			
			
		},
		function() <!--- Error function --->
		{
			$('.reportingTimeSummary').css("background", "none");
			
			initComponents();
		});
		
		
	}
	
	<!--- Method to draw chart after variable data has been defined "FORM" type--->
	function RedrawChartWithNewData(inpDataURP, inpDataGC, inpChartPosition)
	{
		$('#droppable_BatchList_'+ inpChartPosition).empty();
		loadingSingleChartData('droppable_BatchList_'+ inpChartPosition);
		
		$.ajax({
	
				type: "POST", 
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
				dataType: 'json',
				async:false,
				data: inpDataURP,					  
				success:function(res){
					if(parseInt(res.RXRESULTCODE) == 1){
						var dateRange = $('#datepicker-calendar').DatePickerGetDate();
						var startDate = convertDateToTimestamp(dateRange[0][0]);
						var endDate = convertDateToTimestamp(dateRange[0][1]);
						
						ServerProxy.PostToServerStruct(
						'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc',
						'GetChartByName',
						inpDataGC,
						'Get data for reporting fail',
						function(data){
							var reportArr = data.REPORT_ARRAY;
							var displayData = (reportArr[0]).DISPLAYDATA;
							var reportType = (reportArr[0]).REPORTTYPE;
							
							<!--- Create new object for chart container --->
							<!--- Add the container to the display --->
							var $NewObj =  $("#droppable_BatchList_" + inpChartPosition);
							
							<!--- Size and style the container --->				
							$NewObj.empty();
							$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_Med DashSize_Height_Small");
							$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
												
							if(reportArr[0].REPORTTYPE == "TABLE")						
							{							
								$NewObj.html(displayData);

								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");					
					
								initDataTable($('#droppable_BatchList_'+ inpChartPosition + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + inpChartPosition).val());
							}
							else if(reportArr[0].REPORTTYPE == "FORM")						
							{							
								$NewObj.html(reportArr[0].DISPLAYDATA);							
								
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");
							}
							else
							{
								eval(displayData);
								if(typeof (chart) != 'undefined')
								{	
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + inpChartPosition) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ inpChartPosition + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var height = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(height);
													
									chart.write('DashObjChart_'+ inpChartPosition);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + inpChartPosition)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + inpChartPosition) + '">Change</div>');
								}	
				
							}							
						
							finishedLoadingSingleChartData('droppable_BatchList_'+ inpChartPosition);
						}
					);
				}
			} 		
		});
				
	}
			
	<!--- Setup dropable area for chart containers --->	
	function InitDashboardDroppable()
	{<!--- InitDashboardDroppable() --->
		
		$( "div.droppableContentDashboard" ).sortable({
	//	  	connectWith: ".DashBorder"  //,
		  	handle: ".DashObjHeader",
	//	  	cancel: ".portlet-toggle",
	//	  	placeholder: "DashObjPlaceHolder ui-state-highlight"
	
			start: function(e, ui) 
			{
				<!--- creates a temporary attribute on the element with the old index--->
				$(this).attr('data-previndex', ui.item.index());
			},
			update: function(e, ui) 
			{
				
				loadingData();
				
				var newIndex = ui.item.index();
				var oldIndex = $(this).attr('data-previndex');
				$(this).removeAttr('data-previndex');
								
				<!--- Update DB Position --->
				$.ajax({
					type: "POST", 
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateDashboardObjectPosition&returnformat=json&queryformat=column',   
					dataType: 'json',
					async:false,
					data:  { 
						inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
						inpOriginalPos: parseInt(oldIndex),
						inpNewPos: parseInt(newIndex)
					},					  
					success:function(data)
					{				
						<!--- Update display--->
						
						var position = 1;
								
						$("#DashboardContent").children('.DashObj').each( function(index, value) {
							
							<!---Dont update unless you update all data and links...--->
							<!---$(this).attr('id', 'droppable_BatchList_' + position);--->
							$(this).attr('pos', position);
							position = position + 1;						
							
						});
				
				
						finishedLoadingData();
								
					}, 		
					error:function(jqXHR, textStatus)
					{				
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert(textStatus + "\n", "Move Request failed!", function(result) { finishedLoadingData(); } );	
								
					} 		
				
				});		
		
		
			}


		});
	
		<!--- Disable showing the drop area as default selection --->
		<!---$( "div.droppableContentDashboard" ).disableSelection();--->
	
		$("div.droppableContentDashboard").droppable(
		{<!--- Droppable config --->
		
			<!--- Visual feedback cues --->	
			over: function(event, ui)
			{
				$(this).css({
					'border': 'solid 2px green',
					'box-shadow': '0 0px 10px #777',
					'-webkit-box-shadow': '0 0px 10px #777',
					'-moz-box-shadow': '0 0px 10px #777'
				});
			},
			<!--- Visual feedback cues --->
			out: function(event, ui)
			{
				$(this).css({
					'border': 'solid 1px #999999',
					'box-shadow': '0 0px 0px #fff',
					'-webkit-box-shadow': '0 0px 0px #fff',
					'-moz-box-shadow': '0 0px 0px #fff'
				});
			},
			<!--- Process when somthing is dropped here --->
			drop: function(event, ui)
			{<!--- drop event --->
				
				if(ui.draggable.hasClass("DashObj"))
				{<!--- Update order in DB for all DashObj --->
					
					
					<!--- Pos is updated in the Update method of the droppab le object. --->
					
					
				}<!--- Update order in DB for all DashObj --->
				else
				{<!--- Default Drop Chart Obj--->
				
				
					if(ui.draggable.attr('rel2') == "TEMPLATE")
					{						
						LoadTemplate(ui.draggable.attr('rel4'));
						
						<!--- Add list of objects to the display --->	
						<!---
						if(ui.draggable.attr('rel4') == "DT_VoiceObj")
												
						var arrayLength = DT_VoiceObj.Length;
						for (var i = 0; i < arrayLength; i++) 
						{
							AddSingleDashObj(DT_VoiceObj.Pos[i], DT_VoiceObj.Name[i], DT_VoiceObj.Type[i], DT_VoiceObj.ClassString[i]);	
						}
					--->
						return;	
					}
					
				
					<!--- New chart added --->				
					chartQuantity++;
					
					<!--- ID names must be unique while position and total count can change--->
					CountDroppedThisSession++;
					
					var reportType = new Array();
					var quadarant = new Array();
								
					reportName = ui.draggable.attr('rel1');
					reportDisplayType = ui.draggable.attr('rel2');
					
					var ClassInfo = "";
					
					if(ui.draggable.attr('rel3') != "" && typeof(ui.draggable.attr('rel3')) != "undefined")
						ClassInfo = ui.draggable.attr('rel3');
					else
						ClassInfo = "DashObj DashLeft DashBorder DashSize_Width_Med DashSize_Height_Small";
															
					loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
					
					<!--- Create new object for chart container --->
					<!--- Add the container to the display --->
					var $NewObj =  $("<div id='droppable_BatchList_" + CountDroppedThisSession + "'></div>").appendTo("div.droppableContentDashboard");
					
					<!--- Size and style the container --->				
					$NewObj.html('');
					$NewObj.addClass(ClassInfo);
					$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
					$NewObj.attr('pos', chartQuantity);
					
					<!--- Get current date range of page --->
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
					
					<!--- Write selection to user preferences --->
					
					$.ajax({
			
						type: "POST", 
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
						dataType: 'json',
						async:false,
						data:  { 
							inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
							inpReportName: reportName,
							inpReportType: reportDisplayType,
							inpClassInfo: ClassInfo,
							inpChartPostion: chartQuantity 
						},		
						error:function(jqXHR, textStatus)
						{	
							chartQuantity--;
						},
						success:function(res){
							if(parseInt(res.RXRESULTCODE) == 1)
							{
								
								
								$.ajax({
									type: "POST", 
									url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetChartByName&returnformat=json&queryformat=column',   
									dataType: 'json',
									async:false,
									data:  
									{ 
										inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
										inpStart: startDate,
										inpEnd: endDate,
										inpChartName: reportName,
										inpReportType: reportDisplayType,
										inpChartPostion: chartQuantity 
									},					  
									success:function(data)
									{<!--- ajax success --->	
													
										if(typeof(data.RXRESULTCODE != "undefined"))
										{				
										
											if(typeof(data.ROWCOUNT != "undefined"))
												if (data.ROWCOUNT > 0) 
													CurrRXResultCode = parseInt(data.DATA.RXRESULTCODE[0]);
												else	
													CurrRXResultCode = parseInt(data.RXRESULTCODE);			
											else
												CurrRXResultCode = parseInt(data.RXRESULTCODE);	
											
											if(CurrRXResultCode > 0)
											{
												var reportArr = data.REPORT_ARRAY;
												var displayData = (reportArr[0]).DISPLAYDATA;
												var reportType = (reportArr[0]).REPORTTYPE;
												
												<!--- Remove any data that may already be here --->
												$NewObj.empty();
												
												if(reportArr[0].REPORTTYPE == "TABLE")						
												{							
													$NewObj.html(displayData);
							
													<!--- Add DashObj options to header with class head-EMS --->
													$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
													$NewObj.find('.head-EMS').addClass("DashObjHeader");
							
													 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
												}
												else if(reportArr[0].REPORTTYPE == "FORM")						
												{			
													$NewObj.html(reportArr[0].DISPLAYDATA);			
													
													$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
													$NewObj.find('.head-EMS').addClass("DashObjHeader");			
												}
												else
												{
													eval(displayData);
													if(typeof (chart) != 'undefined')
													{																										
														<!--- Add a header for info as well as an anchor for drag and sort --->												
														if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
															$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
														else
															$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
									
														
														<!--- Add a new area for the chart --->	
														$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
															
														<!--- Chart size is dynamic based on obj and header sizes --->				
														var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
														$NewObj.children('.DashObjChart').height(heightNew);
																												
														chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
														
														<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
														if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
															$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
													}											
												}
												
												$NewObj.css("background", "none");
																					
												return false;
													
											}
											else
											{
												$.alerts.okButton = '&nbsp;OK&nbsp;';
												jAlert("Error" + "\n"  + data.MESSAGE + "\n" + data.ERRMESSAGE, "Failure!", function(result) 
												{ 
													$NewObj.css("background", "none");
													$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report Load Error" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
													$NewObj.append("Error" + "<BR/>"  + data.MESSAGE + "<BR/>" + data.ERRMESSAGE, "Failure!");	
												});							
											}
										}
										
									}<!--- ajax success --->	
								});
							
							}
							
						}
					});
					
				}<!--- Default Drop Chart Obj--->
						
			}<!--- drop event --->
			
		});<!--- Droppable config --->
								
	}<!--- InitDashboardDroppable() --->
		
	
	<!--- Load charts from DB preferences --->	
	function drawChart()
	{
		<!--- Read current date data--->
		var dateRange = $('#datepicker-calendar').DatePickerGetDate();
		var startDate = convertDateToTimestamp(dateRange[0][0]);
		var endDate = convertDateToTimestamp(dateRange[0][1]);
		
		<!--- Clear current dashboard--->
		$("#DashboardContent").empty();
		
		$("#DashboardContent").append('<cfoutput>#DashBoardPrintMenu#</cfoutput>');
		
		
		var from = new Date(dateRange[0][0]);
		var to = new Date(dateRange[0][1]);
			
						
		$("#SelectedMinBoundaryDatePrint").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
		$("#SelectedMinBoundaryDatePrint").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
		$("#SelectedMaxBoundaryDatePrint").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
		$("#SelectedMaxBoundaryDatePrint").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
		
		<!--- Feedback to user while loading --->
		loadingData();
			
		<!--- GetDashBoardCount --->
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetDashBoardCount&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>'
			},					  
			success:function(data)
			{				
				chartQuantity = data.TOTALCOUNT;				
			} 		
		});
		
		<!--- Reset count as all are reloaded here --->		
		CountDroppedThisSession = 0;
					
		<!--- Hide dashboard until first object is loaded --->
		$('#reportSummaryOverlay').show();	
		$('#DashboardContent').css("background", "url(../../public/images/loading.gif) center no-repeat");	
					
		for(icq=0; icq< chartQuantity; icq++)
		{
			<!--- Get the report data based on previously stored preferences - do this for each report postion --->
			var displayData = "";	
									
			<!--- ID names must be unique while position and total count can change--->
			CountDroppedThisSession++;
			
			loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
					
			<!--- Create new object for chart container --->
			<!--- Add the container to the display --->
			var $NewObj =  $("<div id='droppable_BatchList_" + CountDroppedThisSession + "'></div>").appendTo("div.droppableContentDashboard");
			
			<!--- Size and style the container --->				
			$NewObj.html('');
			$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_Med DashSize_Height_Small");
			$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
			$NewObj.attr('pos', CountDroppedThisSession);
			
			
			$.ajax({
				type: "POST", 
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetChartFromPreferences&returnformat=json&queryformat=column',   
				dataType: 'json',
				async:false,
				data:  
				{ 
					inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
					inpStart: startDate,
					inpEnd: endDate,
					inpChartPostion: CountDroppedThisSession
				},					  
				success:function(data)
				{<!--- ajax success --->	
					
					<!--- Show dashboard while individual obejcts are still loading --->			
					$('#reportSummaryOverlay').hide();	
					$('#DashboardContent').css("background", "#FFF");
								
					if(typeof(data.RXRESULTCODE != "undefined"))
					{				
					
						if(typeof(data.ROWCOUNT != "undefined"))
							if (data.ROWCOUNT > 0) 
								CurrRXResultCode = parseInt(data.DATA.RXRESULTCODE[0]);
							else	
								CurrRXResultCode = parseInt(data.RXRESULTCODE);			
						else
							CurrRXResultCode = parseInt(data.RXRESULTCODE);	
						
						if(CurrRXResultCode > 0)
						{
							var reportArr = data.REPORT_ARRAY;
					
							<!--- Remove any data that may already be here --->
							$NewObj.empty();
							
							$NewObj.removeClass();
							
							if(reportArr.CLASSINFO != '')
								$NewObj.addClass(reportArr.CLASSINFO);
							else
								$NewObj.addClass("DashObj DashLeft DashBorder DashSize_Width_Med DashSize_Height_Small");
							
							if(reportArr.REPORTTYPE == "TABLE")						
							{							
								 $NewObj.html(reportArr.DISPLAYDATA);
								 
								 $NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");
								 $NewObj.find('.head-EMS').addClass("DashObjHeader");
													
								 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr.TABLEREPORTNAME, reportArr.INPCUSTOMDATA1, reportArr.INPCUSTOMDATA2, reportArr.INPCUSTOMDATA3, reportArr.INPCUSTOMDATA4, reportArr.INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
							}
							else if(reportArr.REPORTTYPE == "FORM")						
							{			
								$NewObj.html(reportArr.DISPLAYDATA);
								
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");						
							}
							else
							{
								eval(reportArr.DISPLAYDATA);
								if(typeof (chart) != 'undefined')
								{	
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(heightNew);
																	
									chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
								}
								
								resizeChart();
							}
							
							$NewObj.css("background", "none");
									
							
														
							return false;
								
						}
						else
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error" + "\n"  + data.MESSAGE + "\n" + data.ERRMESSAGE, "Failure!", function(result) 
							{ 
								$NewObj.css("background", "none");
								$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report Load Error" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
								$NewObj.append("Error" + "<BR/>"  + data.MESSAGE + "<BR/>" + data.ERRMESSAGE, "Failure!");									
							});							
						}
					}
					
				}<!--- ajax success --->	
			});
							
		}
	
		<!--- Show dashboard even if no objects loaded --->
		$('#reportSummaryOverlay').hide();	
		$('#DashboardContent').css("background", "#FFF");
	
		finishedLoadingData();
	}	
	
	function RemoveDashboardObject(inpObj)
	{	
		loadingData();
	
		<!--- Remove from DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=DeleteDashboardObj&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpChartPostion: $(inpObj).parents(".DashObj").attr('pos')
			},					  
			success:function(data)
			{				
				<!--- Update Remaining position / ids  --->
				chartQuantity--;
				<!---//CountDroppedThisSession--;  Add this here breaks next drop - two objects with same name becomes possible - may not need this?--->
				
				var position = 1;
								
				$("#DashboardContent").children('.DashObj').each( function(index, value) {
					
					<!---Dont update unless you update all data and links...--->
					<!---$(this).attr('id', 'droppable_BatchList_' + position);--->
					
					if($(inpObj).parents(".DashObj").attr('pos') != index+1)
					{
						$(this).attr('pos', position);
						position = position + 1;						
					}
				});
				
				<!--- Remove from display--->	
				$(inpObj).parents(".DashObj").remove();	
				
				finishedLoadingData();		
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert(textStatus + "\n", "Remove Request failed!", function(result) { finishedLoadingData(); } );									
			} 		
		
		});			
	}
	
	<!--- 1=up 2=right 3=left 4=down--->
	function SizeDashboardObject(inpObj, inpDir)
	{		
		var $CurrObj = $(inpObj).parents(".DashObj");
		
		switch(inpDir)
		{		
			<!--- Make bigger vertically --->
			case 1:
			
				if($CurrObj.hasClass('DashSize_Height_Tiny'))
				{
					$CurrObj.addClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Tiny');	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Height_Small'))
				{
					$CurrObj.addClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Small');	
					break;		
				}
				
				
				if($CurrObj.hasClass('DashSize_Height_Med'))
				{
					$CurrObj.addClass('DashSize_Height_Large');
					$CurrObj.removeClass('DashSize_Height_Med');	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Height_Large'))
				{
					<!--- Large is as large as it gets --->	
					break;		
				}		
				
				break;		
			
			<!--- Make bigger horizontally --->
			case 2:
			
				if($CurrObj.hasClass('DashSize_Width_Small'))
				{
					$CurrObj.addClass('DashSize_Width_Med');
					$CurrObj.removeClass('DashSize_Width_Small');
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Width_Med'))
				{
					$CurrObj.addClass('DashSize_Width_Large');
					$CurrObj.removeClass('DashSize_Width_Med');	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_Large'))
				{
					<!--- Large is as large as it gets --->	
					break;		
				}	
				
				break;		
				
			<!--- Make smaller vertically --->
			case 3:
			
				if($CurrObj.hasClass('DashSize_Height_Tiny'))
				{
					<!--- Small is as small as it gets --->	
					break;	
				}
				
				if($CurrObj.hasClass('DashSize_Height_Small'))
				{
					$CurrObj.addClass('DashSize_Height_Tiny');
					$CurrObj.removeClass('DashSize_Height_Small');
					break;	
				}
				
				if($CurrObj.hasClass('DashSize_Height_Med'))
				{
					$CurrObj.addClass('DashSize_Height_Small');
					$CurrObj.removeClass('DashSize_Height_Med');
					break;			
				}
				
				if($CurrObj.hasClass('DashSize_Height_Large'))
				{
					$CurrObj.addClass('DashSize_Height_Med');
					$CurrObj.removeClass('DashSize_Height_Large');	
					break;								
				}	
				
				break;		
				
			<!--- Make smaller horizontally --->
			case 4:
			
				if($CurrObj.hasClass('DashSize_Width_Small'))
				{
					<!--- Small is as small as it gets --->	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_Med'))
				{
					$CurrObj.addClass('DashSize_Width_Small');
					$CurrObj.removeClass('DashSize_Width_Med');	
					break;		
				}
				
				if($CurrObj.hasClass('DashSize_Width_Large'))
				{
					$CurrObj.addClass('DashSize_Width_Med');
					$CurrObj.removeClass('DashSize_Width_Large');
					break;						
				}	
				
				break;		
		}
		
		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateDashboardObjectClassInfo&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpChartPostion: $(inpObj).parents(".DashObj").attr('pos'),
				inpClassInfo: $CurrObj.attr('class')
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				resizeChart();	
						
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert(textStatus + "\n", "Resize Request failed!", function(result) { } );	
						
			} 		
		
		});			
			
	}
	
	
	function AddSingleDashObj(inpDashObj)
	{
		<!--- New chart added --->				
		chartQuantity++;
		
		<!--- ID names must be unique while position and total count can change--->
		CountDroppedThisSession++;
		
		var reportType = new Array();
		var quadarant = new Array();
					
		reportName = inpDashObj.NAME;
		reportDisplayType = inpDashObj.TYPE;
		
		var ClassInfo = "";
		
		if(inpDashObj.CLASSINFO != "")
			ClassInfo = inpDashObj.CLASSINFO;
		else
			ClassInfo = "DashObj DashLeft DashBorder DashSize_Width_Med DashSize_Height_Small";
												
		loadingSingleChartData('droppable_BatchList_'+ CountDroppedThisSession);
		
		<!--- Create new object for chart container --->
		<!--- Add the container to the display --->
		var $NewObj =  $("<div id='droppable_BatchList_" + CountDroppedThisSession + "'></div>").appendTo("div.droppableContentDashboard");
		
		<!--- Size and style the container --->				
		$NewObj.html('');
		$NewObj.addClass(ClassInfo);
		$NewObj.css("background", "url(../../public/images/loading.gif) center no-repeat");
		$NewObj.attr('pos', chartQuantity);
		
		<!--- Get current date range of page --->
		var dateRange = $('#datepicker-calendar').DatePickerGetDate();
		var startDate = convertDateToTimestamp(dateRange[0][0]);
		var endDate = convertDateToTimestamp(dateRange[0][1]);
		
		<!--- Write selection to user preferences --->
		
		$.ajax({

			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=UpdateReportPreference&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
				inpReportName: reportName,
				inpReportType: reportDisplayType,
				inpClassInfo: ClassInfo,
				inpChartPostion: chartQuantity,
				inpcustomdata1: inpDashObj.CUSTOMDATA1,
				inpcustomdata2: inpDashObj.CUSTOMDATA2,
				inpcustomdata3: inpDashObj.CUSTOMDATA3,
				inpcustomdata4: inpDashObj.CUSTOMDATA4,
				inpcustomdata5: inpDashObj.CUSTOMDATA5
				 
			},		
			error:function(jqXHR, textStatus)
			{	
				chartQuantity--;
			},
			success:function(res){
				if(parseInt(res.RXRESULTCODE) == 1)
				{
					<!--- Read chart data --->
					ServerProxy.PostToServerStruct
					(<!--- ServerProxy.PostToServerStruct --->
						'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc',
						'GetChartByName',
						{ 
							inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>',
							inpStart: startDate,
							inpEnd: endDate,
							inpChartName: reportName,
							inpReportType: reportDisplayType,
							inpChartPostion: chartQuantity 
						},
						'Get data for reporting fail',
						function(data)
						{
							var reportArr = data.REPORT_ARRAY;
							var displayData = (reportArr[0]).DISPLAYDATA;
							var reportType = (reportArr[0]).REPORTTYPE;
							
							<!--- Remove any data that may already be here --->
							$NewObj.empty();
							
							if(reportArr[0].REPORTTYPE == "TABLE")						
							{							
								$NewObj.html(displayData);
		
								<!--- Add DashObj options to header with class head-EMS --->
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");
		
								 initDataTable($('#droppable_BatchList_'+ CountDroppedThisSession + ' #ReportDataTable'), reportArr[0].TABLEREPORTNAME, reportArr[0].INPCUSTOMDATA1, reportArr[0].INPCUSTOMDATA2, reportArr[0].INPCUSTOMDATA3, reportArr[0].INPCUSTOMDATA4, reportArr[0].INPCUSTOMDATA5, $("#ExtraDTInitSort_" + CountDroppedThisSession).val());
							}
							else if(reportArr[0].REPORTTYPE == "FORM")						
							{			
								$NewObj.html(reportArr[0].DISPLAYDATA);			
								
								$NewObj.find('.head-EMS').append("<cfoutput>#DashObjMenu#</cfoutput>");	
								$NewObj.find('.head-EMS').addClass("DashObjHeader");			
							}
							else
							{
								eval(displayData);
								if(typeof (chart) != 'undefined')
								{																										
									<!--- Add a header for info as well as an anchor for drag and sort --->												
									if(typeof($('body').data('DashBoardHeaer' + CountDroppedThisSession)) != 'undefined')					
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + $('body').data('DashBoardHeaer' + CountDroppedThisSession) + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");		
									else
										$NewObj.append("<div class='DashObjHeader'><div class='DashObjHeaderText'>" + "Report" + "</div><cfoutput>#DashObjMenu#</cfoutput></div>");	
				
									
									<!--- Add a new area for the chart --->	
									$NewObj.append("<div id='DashObjChart_"+ CountDroppedThisSession + "' class='DashObjChart'></div>");
										
									<!--- Chart size is dynamic based on obj and header sizes --->				
									var heightNew = $NewObj.height() - $NewObj.children('.DashObjHeader').height();
									$NewObj.children('.DashObjChart').height(heightNew);
																							
									chart.write('DashObjChart_'+ CountDroppedThisSession);<!---//chart is variable created for amChart;--->
									
									<!--- If there is a reset link defined in both the chart build logic and the chart object then show it --->							
									if(typeof($('body').data('ResetLink' + CountDroppedThisSession)) != 'undefined')
										$NewObj.append('<div class="ResetChartOption no-print" onclick="' + $('body').data('ResetLink' + CountDroppedThisSession) + '">Change</div>');
								}											
							}
							
							$NewObj.css("background", "none");
						}
							
					);<!--- ServerProxy.PostToServerStruct --->
					
				}
			}
		});

		
	}
	
	function SaveCurrentAsTemplateDialog()
	{		
		<!--- See naming template at bottom of page--->
		showRenameDashboardTemplateDialog('0', '');
	}
	
	function LoadTemplate(inpId)
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ReadTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  
			{ 
				inpId: parseInt(inpId)
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{
					var DT_TemplateObj = JSON.parse( data.TEMPLATE);
					
					var arrayLength = DT_TemplateObj.length;
					for (var i = 0; i < arrayLength; i++) 
					{						
						AddSingleDashObj(DT_TemplateObj[i]);	
					}	
				}
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Connection Failure!" + textStatus + "\n", "Template Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
	function ClearDashboard()
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ClearDashboard&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  { 
				inpBatchIdList: '<cfoutput>#inpBatchIdList#</cfoutput>'
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				drawChart();
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Connection Failure!" + textStatus + "\n", "Clear Dashboard Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
	function LoadDashboardTemplateList()
	{			
	
		$('#DashboardTemplates').empty();
		
		$('#DashboardTemplates').html('<li><span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></span></li>');
	
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ReadTemplateList&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:true,
			data:  { 
				
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{																				
					for(i=0; i< data.TEMPLATELIST.length; i++)
					{											
						var $NewTemplate;
					
						if(parseInt(data.TEMPLATELIST[i][2]) > 0 )	
						{
							$NewTemplate =  $('<li><span class="menuItem" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +   data.TEMPLATELIST[i][1] + '</span></li>').appendTo("#DashboardTemplates");
						}
						else
						{
							$NewTemplate =  $('<li><span class="menuItem" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +  '(sys)&nbsp;&nbsp;' +  data.TEMPLATELIST[i][1] + '</span></li>').appendTo("#DashboardTemplates");
						}
							
						$NewTemplate.children().draggable({
								revert: "invalid",
								containment: "",
								opacity: 0.7,
								helper: "clone",
								cursor: "move",
								start: function(event, ui){
									$('.droppable').css({
										'border': 'solid 2px red'
									})
									
								},
								stop: function(event, ui){
									$('.droppable').css({
										'border': 'solid 1px #999999'
									})
								}
							});
							
						
					}
					
				}
				
				
				
				$("#TemplateTrash").droppable(
				{<!--- Droppable config --->
				
					<!--- Visual feedback cues --->	
					over: function(event, ui)
					{
						$(this).css({
							'border': 'solid 2px #15436C',
							'box-shadow': '0 0px 10px #777',
							'-webkit-box-shadow': '0 0px 10px #777',
							'-moz-box-shadow': '0 0px 10px #777',
							'margin': '0px 15px 0px 0px'
						});
					},
					<!--- Visual feedback cues --->
					out: function(event, ui)
					{
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
					},
					<!--- Process when somthing is dropped here --->
					drop: function(event, ui)
					{<!--- drop event --->
						
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
						
						if(ui.draggable.attr('rel2') == "TEMPLATE")
						{	
							
							if(ui.draggable.attr('rel5') == 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("You can remove user defined templates by dragging them to the trash can." + "\n", "System Templates are not removeable!", function(result) { } );	
								return false;
							}
											
							RemoveUserTemplate(ui.draggable.attr('rel4'), ui.draggable);
														
							return;	
						}
					}
					
				});
				
			
				
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				<!---
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Connection Failure!" + textStatus + "\n", "Read Dashboard Request failed!", function(result) { } );	
				--->
			} 		
		
		});			
	}
	
	
	function RemoveUserTemplate(inpId, inpObj)
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=RemoveTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  
			{ 
				inpId: parseInt(inpId)
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{
					inpObj.remove();
				}
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Connection Failure!" + textStatus + "\n", "Template Request failed!", function(result) { } );	
						
			} 		
		
		});			
	}
	
	
</script>


<cfinclude template="dsp_renametemplate.cfm">




