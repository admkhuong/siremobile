<cfcomponent>
	
    <cffunction name="AddPRJCTRow" access="remote" returntype="string">
    	<cfargument name="voicetem" type="struct">
        
        
         <!--- Check If Table Data Has Been Already Created and Stored --->
            <cfquery name="CheckProjectData" datasource="#Session.DBSourceEBM#">
                SELECT 
                	BatchId_bi 
                FROM 
                	simpleobjects.pm_projectinformation
                WHERE 
                	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">
            </cfquery>
            
            <!--- If Table Data Has Not Been Entered Use INSERT To Add Table Data --->
			
            
            
            
            
			<cfif CheckProjectData.recordcount eq 0>
            	<cfset results = InsertPRJCTRow(argumentCollection)>
            <cfelse>
            	<cfset results = UpdatePRJCTRow(argumentCollection)>
            </cfif>
			
            
            <!--- After checking for changes, update changes table and set all not settled items to settled --->
            <cfquery name="checkchanges" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_change SET
                Settled_int = 1
                WHERE Settled_int = 0 and BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#"> and ChangedCUser_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">
            </cfquery>
            
       <cfreturn results>
	</cffunction> 
        
            
    <cffunction name="InsertPRJCTRow" access="remote" returntype="string">
    	
        <cfset NewCollection = DeserializeJSON(argumentCollection)>
        
         		<cfif IsDefined("NewCollection.PRECALL_TO_EMAIL_INT") and NewCollection.PRECALL_TO_EMAIL_INT eq "on"><cfset NewCollection.PRECALL_TO_EMAIL_INT = 1><cfelse><cfset NewCollection.PRECALL_TO_EMAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.STANDALONE_CALL_INT") and NewCollection.STANDALONE_CALL_INT eq "on"><cfset NewCollection.STANDALONE_CALL_INT = 1><cfelse><cfset NewCollection.STANDALONE_CALL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PRECALL_TO_DIRECT_MAIL_INT") and NewCollection.PRECALL_TO_DIRECT_MAIL_INT eq "on"><cfset NewCollection.PRECALL_TO_DIRECT_MAIL_INT = 1><cfelse><cfset NewCollection.PRECALL_TO_DIRECT_MAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.POSTCALL_TO_DIRECT_MAIL_INT") and NewCollection.POSTCALL_TO_DIRECT_MAIL_INT eq "on"><cfset NewCollection.POSTCALL_TO_DIRECT_MAIL_INT = 1><cfelse><cfset NewCollection.POSTCALL_TO_DIRECT_MAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.DRIVE_TO_WEB_INT") and NewCollection.DRIVE_TO_WEB_INT eq "on"><cfset NewCollection.DRIVE_TO_WEB_INT = 1><cfelse><cfset NewCollection.DRIVE_TO_WEB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.INFORMATIONAL_INT") and NewCollection.INFORMATIONAL_INT eq "on"><cfset NewCollection.INFORMATIONAL_INT = 1><cfelse><cfset NewCollection.INFORMATIONAL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.OTHER_INT") and NewCollection.OTHER_INT eq "on"><cfset NewCollection.OTHER_INT = 1><cfelse><cfset NewCollection.OTHER_INT = 0></cfif>
                
				<cfif IsDefined("NewCollection.PLATFORM_ATT_INT") and NewCollection.PLATFORM_ATT_INT eq "on"><cfset NewCollection.PLATFORM_ATT_INT = 1><cfelse><cfset NewCollection.PLATFORM_ATT_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PLATFORM_MB_INT") and NewCollection.PLATFORM_MB_INT eq "on"><cfset NewCollection.PLATFORM_MB_INT = 1><cfelse><cfset NewCollection.PLATFORM_MB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PLATFORM_EGS_INT") and NewCollection.PLATFORM_EGS_INT eq "on"><cfset NewCollection.PLATFORM_EGS_INT = 1><cfelse><cfset NewCollection.PLATFORM_EGS_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.CUSTOMER_BASE_CON_INT") and NewCollection.CUSTOMER_BASE_CON_INT eq "on"><cfset NewCollection.CUSTOMER_BASE_CON_INT = 1><cfelse><cfset NewCollection.CUSTOMER_BASE_CON_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CUSTOMER_BASE_BUS_INT") and NewCollection.CUSTOMER_BASE_BUS_INT eq "on"><cfset NewCollection.CUSTOMER_BASE_BUS_INT = 1><cfelse><cfset NewCollection.CUSTOMER_BASE_BUS_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.DO_NOT_SCRUB_INT") and NewCollection.DO_NOT_SCRUB_INT eq "on"><cfset NewCollection.DO_NOT_SCRUB_INT = 1><cfelse><cfset NewCollection.DO_NOT_SCRUB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.STATE_DNC_LIST_INT") and NewCollection.STATE_DNC_LIST_INT eq "on"><cfset NewCollection.STATE_DNC_LIST_INT = 1><cfelse><cfset NewCollection.STATE_DNC_LIST_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CLIENT_DNC_INT") and NewCollection.CLIENT_DNC_INT eq "on"><cfset NewCollection.CLIENT_DNC_INT = 1><cfelse><cfset NewCollection.CLIENT_DNC_INT = 0></cfif>
                <cfif IsDefined("NewCollection.DO_NOT_DIAL_STATES_INT") and NewCollection.DO_NOT_DIAL_STATES_INT eq "on"><cfset NewCollection.DO_NOT_DIAL_STATES_INT = 1><cfelse><cfset NewCollection.DO_NOT_DIAL_STATES_INT = 0></cfif>
                <cfif IsDefined("NewCollection.NATIONAL_DNC_INT") and NewCollection.NATIONAL_DNC_INT eq "on"><cfset NewCollection.NATIONAL_DNC_INT = 1><cfelse><cfset NewCollection.NATIONAL_DNC_INT = 0></cfif>
                <cfif IsDefined("NewCollection.COMPANY_INTERNAL_LIST_INT") and NewCollection.COMPANY_INTERNAL_LIST_INT eq "on"><cfset NewCollection.COMPANY_INTERNAL_LIST_INT = 1><cfelse><cfset NewCollection.COMPANY_INTERNAL_LIST_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CELL_PHONE_LIST_INT") and NewCollection.CELL_PHONE_LIST_INT eq "on"><cfset NewCollection.CELL_PHONE_LIST_INT = 1><cfelse><cfset NewCollection.CELL_PHONE_LIST_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.FILTER_WITHIN_FILE_INT") and NewCollection.FILTER_WITHIN_FILE_INT eq "on"><cfset NewCollection.FILTER_WITHIN_FILE_INT = 1><cfelse><cfset NewCollection.FILTER_WITHIN_FILE_INT = 0></cfif>
                <cfif IsDefined("NewCollection.FILTER_SAME_DAY_INT") and NewCollection.FILTER_SAME_DAY_INT eq "on"><cfset NewCollection.FILTER_SAME_DAY_INT = 1><cfelse><cfset NewCollection.FILTER_SAME_DAY_INT = 0></cfif>
                <cfif IsDefined("NewCollection.FILTER_X_AMOUNT_DAYS_INT") and NewCollection.FILTER_X_AMOUNT_DAYS_INT eq "on"><cfset NewCollection.FILTER_X_AMOUNT_DAYS_INT = 1><cfelse><cfset NewCollection.FILTER_X_AMOUNT_DAYS_INT = 0></cfif>
        
        		<cfparam name="NewCollection.TYPE_OF_CALL_VCH" default="0">
                
                <cfparam name="NewCollection.MONITORING_INT" default="0">
                <cfparam name="NewCollection.MAXATTEMPTS_INT" default="0">
                <cfparam name="NewCollection.RINGCOUNT_INT" default="0">
                

                <cfparam name="NewCollection.FILTER_DUPLICATE_RECORDS_INT" default="0">

        <cfif IsDefined("NewCollection.SUBMISSION_DATE_DT") and len(trim(NewCollection.SUBMISSION_DATE_DT)) neq 0>
        	<cfset NewCollection.SUBMISSION_DATE_DT = DateFormat((NewCollection.SUBMISSION_DATE_DT),'yyyy-mm-dd')>
        <cfelse>
        	<cfset NewCollection.SUBMISSION_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
        </cfif>
        
        <cfif IsDefined("NewCollection.PROJECT_START_DATE_DT") and len(trim(NewCollection.PROJECT_START_DATE_DT)) neq 0>
        	<cfset NewCollection.PROJECT_START_DATE_DT = DateFormat((NewCollection.PROJECT_START_DATE_DT),'yyyy-mm-dd')>
        <cfelse>
        	<cfset NewCollection.PROJECT_START_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
        </cfif>
        
        <cfif IsDefined("NewCollection.PROJECT_END_DATE_DT") and len(trim(NewCollection.PROJECT_END_DATE_DT)) neq 0>
        	<cfset NewCollection.PROJECT_END_DATE_DT = DateFormat((NewCollection.PROJECT_END_DATE_DT),'yyyy-mm-dd')>
        <cfelse>
        	<cfset NewCollection.PROJECT_END_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
        </cfif>
        
        
        <cfif IsDefined("NewCollection.AL") and NewCollection.AL eq "on"><cfset nAL = 1><cfelse><cfset nAL = 0></cfif>
        <cfif IsDefined("NewCollection.AK") and NewCollection.AK eq "on"><cfset nAK = 1><cfelse><cfset nAK = 0></cfif>
        <cfif IsDefined("NewCollection.AZ") and NewCollection.AZ eq "on"><cfset nAZ = 1><cfelse><cfset nAZ = 0></cfif>
        <cfif IsDefined("NewCollection.AR") and NewCollection.AR eq "on"><cfset nAR = 1><cfelse><cfset nAR = 0></cfif>
        <cfif IsDefined("NewCollection.CA") and NewCollection.CA eq "on"><cfset nCA = 1><cfelse><cfset nCA = 0></cfif>
        <cfif IsDefined("NewCollection.CO") and NewCollection.CO eq "on"><cfset nCO = 1><cfelse><cfset nCO = 0></cfif>
        <cfif IsDefined("NewCollection.CT") and NewCollection.CT eq "on"><cfset nCT = 1><cfelse><cfset nCT = 0></cfif>
        <cfif IsDefined("NewCollection.DE") and NewCollection.DE eq "on"><cfset nDE = 1><cfelse><cfset nDE = 0></cfif>
        <cfif IsDefined("NewCollection.FL") and NewCollection.FL eq "on"><cfset nFL = 1><cfelse><cfset nFL = 0></cfif>
        <cfif IsDefined("NewCollection.GA") and NewCollection.GA eq "on"><cfset nGA = 1><cfelse><cfset nGA = 0></cfif>
        <cfif IsDefined("NewCollection.HI") and NewCollection.HI eq "on"><cfset nHI = 1><cfelse><cfset nHI = 0></cfif>
        <cfif IsDefined("NewCollection.ID") and NewCollection.ID eq "on"><cfset nID = 1><cfelse><cfset nID = 0></cfif>
        <cfif IsDefined("NewCollection.IL") and NewCollection.IL eq "on"><cfset nIL = 1><cfelse><cfset nIL = 0></cfif>
        <cfif IsDefined("NewCollection.IN") and NewCollection.IN eq "on"><cfset nIN = 1><cfelse><cfset nIN = 0></cfif>
        <cfif IsDefined("NewCollection.IA") and NewCollection.IA eq "on"><cfset nIA = 1><cfelse><cfset nIA = 0></cfif>
        <cfif IsDefined("NewCollection.KS") and NewCollection.KS eq "on"><cfset nKS = 1><cfelse><cfset nKS = 0></cfif>
        <cfif IsDefined("NewCollection.KY") and NewCollection.KY eq "on"><cfset nKY = 1><cfelse><cfset nKY = 0></cfif>
        <cfif IsDefined("NewCollection.LA") and NewCollection.LA eq "on"><cfset nLA = 1><cfelse><cfset nLA = 0></cfif>
        <cfif IsDefined("NewCollection.MA") and NewCollection.MA eq "on"><cfset nMA = 1><cfelse><cfset nMA = 0></cfif>
        <cfif IsDefined("NewCollection.ME") and NewCollection.ME eq "on"><cfset nME = 1><cfelse><cfset nME = 0></cfif>
        <cfif IsDefined("NewCollection.MD") and NewCollection.MD eq "on"><cfset nMD = 1><cfelse><cfset nMD = 0></cfif>
		<cfif IsDefined("NewCollection.MI") and NewCollection.MI eq "on"><cfset nMI = 1><cfelse><cfset nMI = 0></cfif>
        <cfif IsDefined("NewCollection.MN") and NewCollection.MN eq "on"><cfset nMN = 1><cfelse><cfset nMN = 0></cfif>
        <cfif IsDefined("NewCollection.MS") and NewCollection.MS eq "on"><cfset nMS = 1><cfelse><cfset nMS = 0></cfif>
        <cfif IsDefined("NewCollection.MO") and NewCollection.MO eq "on"><cfset nMO = 1><cfelse><cfset nMO = 0></cfif>
        <cfif IsDefined("NewCollection.MT") and NewCollection.MT eq "on"><cfset nMT = 1><cfelse><cfset nMT = 0></cfif>
        <cfif IsDefined("NewCollection.NE") and NewCollection.NE eq "on"><cfset nNE = 1><cfelse><cfset nNE = 0></cfif>
        <cfif IsDefined("NewCollection.NV") and NewCollection.NV eq "on"><cfset nNV = 1><cfelse><cfset nNV = 0></cfif>
        <cfif IsDefined("NewCollection.NH") and NewCollection.NH eq "on"><cfset nNH = 1><cfelse><cfset nNH = 0></cfif>
        <cfif IsDefined("NewCollection.NJ") and NewCollection.NJ eq "on"><cfset nNJ = 1><cfelse><cfset nNJ = 0></cfif>
        <cfif IsDefined("NewCollection.NM") and NewCollection.NM eq "on"><cfset nNM = 1><cfelse><cfset nNM = 0></cfif>
        <cfif IsDefined("NewCollection.NY") and NewCollection.NY eq "on"><cfset nNY = 1><cfelse><cfset nNY = 0></cfif>
        <cfif IsDefined("NewCollection.NC") and NewCollection.NC eq "on"><cfset nNC = 1><cfelse><cfset nNC = 0></cfif>
        <cfif IsDefined("NewCollection.ND") and NewCollection.ND eq "on"><cfset nND = 1><cfelse><cfset nND = 0></cfif>
        <cfif IsDefined("NewCollection.OH") and NewCollection.OH eq "on"><cfset nOH = 1><cfelse><cfset nOH = 0></cfif>
        <cfif IsDefined("NewCollection.OK") and NewCollection.OK eq "on"><cfset nOK = 1><cfelse><cfset nOK = 0></cfif>
        <cfif IsDefined("NewCollection.zOR")><cfset nOR = 1><cfelse><cfset nOR = 0></cfif>
        <cfif IsDefined("NewCollection.PA") and NewCollection.PA eq "on"><cfset nPA = 1><cfelse><cfset nPA = 0></cfif>
        <cfif IsDefined("NewCollection.RI") and NewCollection.RI eq "on"><cfset nRI = 1><cfelse><cfset nRI = 0></cfif>
        <cfif IsDefined("NewCollection.SD") and NewCollection.SD eq "on"><cfset nSD = 1><cfelse><cfset nSD = 0></cfif>
        <cfif IsDefined("NewCollection.SC") and NewCollection.SC eq "on"><cfset nSC = 1><cfelse><cfset nSC = 0></cfif>
        <cfif IsDefined("NewCollection.TN") and NewCollection.TN eq "on"><cfset nTN = 1><cfelse><cfset nTN = 0></cfif>
        <cfif IsDefined("NewCollection.TX") and NewCollection.TX eq "on"><cfset nTX = 1><cfelse><cfset nTX = 0></cfif>
        <cfif IsDefined("NewCollection.UT") and NewCollection.UT eq "on"><cfset nUT = 1><cfelse><cfset nUT = 0></cfif>
        <cfif IsDefined("NewCollection.VT") and NewCollection.VT eq "on"><cfset nVT = 1><cfelse><cfset nVT = 0></cfif>
        <cfif IsDefined("NewCollection.VA") and NewCollection.VA eq "on"><cfset nVA = 1><cfelse><cfset nVA = 0></cfif>
        <cfif IsDefined("NewCollection.WA") and NewCollection.WA eq "on"><cfset nWA = 1><cfelse><cfset nWA = 0></cfif>
        <cfif IsDefined("NewCollection.WV") and NewCollection.WV eq "on"><cfset nWV = 1><cfelse><cfset nWV = 0></cfif>
        <cfif IsDefined("NewCollection.WI") and NewCollection.WI eq "on"><cfset nWI = 1><cfelse><cfset nWI = 0></cfif>
        <cfif IsDefined("NewCollection.WY") and NewCollection.WY eq "on"><cfset nWY = 1><cfelse><cfset nWY = 0></cfif>
        
        <cfif IsDefined("NewCollection.TFAL") and NewCollection.TFAL eq "on"><cfset nTFAL = 1><cfelse><cfset nTFAL = 0></cfif>
        <cfif IsDefined("NewCollection.TFAK") and NewCollection.TFAK eq "on"><cfset nTFAK = 1><cfelse><cfset nTFAK = 0></cfif>
        <cfif IsDefined("NewCollection.TFAZ") and NewCollection.TFAZ eq "on"><cfset nTFAZ = 1><cfelse><cfset nTFAZ = 0></cfif>
        <cfif IsDefined("NewCollection.TFAR") and NewCollection.TFAR eq "on"><cfset nTFAR = 1><cfelse><cfset nTFAR = 0></cfif>
        <cfif IsDefined("NewCollection.TFCA") and NewCollection.TFCA eq "on"><cfset nTFCA = 1><cfelse><cfset nTFCA = 0></cfif>
        <cfif IsDefined("NewCollection.TFCO") and NewCollection.TFCO eq "on"><cfset nTFCO = 1><cfelse><cfset nTFCO = 0></cfif>
        <cfif IsDefined("NewCollection.TFCT") and NewCollection.TFCT eq "on"><cfset nTFCT = 1><cfelse><cfset nTFCT = 0></cfif>
        <cfif IsDefined("NewCollection.TFDE") and NewCollection.TFDE eq "on"><cfset nTFDE = 1><cfelse><cfset nTFDE = 0></cfif>
        <cfif IsDefined("NewCollection.TFFL") and NewCollection.TFFL eq "on"><cfset nTFFL = 1><cfelse><cfset nTFFL = 0></cfif>
        <cfif IsDefined("NewCollection.TFGA") and NewCollection.TFGA eq "on"><cfset nTFGA = 1><cfelse><cfset nTFGA = 0></cfif>
        <cfif IsDefined("NewCollection.TFHI") and NewCollection.TFHI eq "on"><cfset nTFHI = 1><cfelse><cfset nTFHI = 0></cfif>
        <cfif IsDefined("NewCollection.TFID") and NewCollection.TFID eq "on"><cfset nTFID = 1><cfelse><cfset nTFID = 0></cfif>
        <cfif IsDefined("NewCollection.TFIL") and NewCollection.TFIL eq "on"><cfset nTFIL = 1><cfelse><cfset nTFIL = 0></cfif>
        <cfif IsDefined("NewCollection.TFIN") and NewCollection.TFIN eq "on"><cfset nTFIN = 1><cfelse><cfset nTFIN = 0></cfif>
        <cfif IsDefined("NewCollection.TFIA") and NewCollection.TFIA eq "on"><cfset nTFIA = 1><cfelse><cfset nTFIA = 0></cfif>
        <cfif IsDefined("NewCollection.TFKS") and NewCollection.TFKS eq "on"><cfset nTFKS = 1><cfelse><cfset nTFKS = 0></cfif>
        <cfif IsDefined("NewCollection.TFKY") and NewCollection.TFKY eq "on"><cfset nTFKY = 1><cfelse><cfset nTFKY = 0></cfif>
        <cfif IsDefined("NewCollection.TFLA") and NewCollection.TFLA eq "on"><cfset nTFLA = 1><cfelse><cfset nTFLA = 0></cfif>
        <cfif IsDefined("NewCollection.TFMA") and NewCollection.TFMA eq "on"><cfset nTFMA = 1><cfelse><cfset nTFMA = 0></cfif>
        <cfif IsDefined("NewCollection.TFME") and NewCollection.TFME eq "on"><cfset nTFME = 1><cfelse><cfset nTFME = 0></cfif>
        <cfif IsDefined("NewCollection.TFMD") and NewCollection.TFMD eq "on"><cfset nTFMD = 1><cfelse><cfset nTFMD = 0></cfif>
		<cfif IsDefined("NewCollection.TFMI") and NewCollection.TFMI eq "on"><cfset nTFMI = 1><cfelse><cfset nTFMI = 0></cfif>
        <cfif IsDefined("NewCollection.TFMN") and NewCollection.TFMN eq "on"><cfset nTFMN = 1><cfelse><cfset nTFMN = 0></cfif>
        <cfif IsDefined("NewCollection.TFMS") and NewCollection.TFMS eq "on"><cfset nTFMS = 1><cfelse><cfset nTFMS = 0></cfif>
        <cfif IsDefined("NewCollection.TFMO") and NewCollection.TFMO eq "on"><cfset nTFMO = 1><cfelse><cfset nTFMO = 0></cfif>
        <cfif IsDefined("NewCollection.TFMT") and NewCollection.TFMT eq "on"><cfset nTFMT = 1><cfelse><cfset nTFMT = 0></cfif>
        <cfif IsDefined("NewCollection.TFNE") and NewCollection.TFNE eq "on"><cfset nTFNE = 1><cfelse><cfset nTFNE = 0></cfif>
        <cfif IsDefined("NewCollection.TFNV") and NewCollection.TFNV eq "on"><cfset nTFNV = 1><cfelse><cfset nTFNV = 0></cfif>
        <cfif IsDefined("NewCollection.TFNH") and NewCollection.TFNH eq "on"><cfset nTFNH = 1><cfelse><cfset nTFNH = 0></cfif>
        <cfif IsDefined("NewCollection.TFNJ") and NewCollection.TFNJ eq "on"><cfset nTFNJ = 1><cfelse><cfset nTFNJ = 0></cfif>
        <cfif IsDefined("NewCollection.TFNM") and NewCollection.TFNM eq "on"><cfset nTFNM = 1><cfelse><cfset nTFNM = 0></cfif>
        <cfif IsDefined("NewCollection.TFNY") and NewCollection.TFNY eq "on"><cfset nTFNY = 1><cfelse><cfset nTFNY = 0></cfif>
        <cfif IsDefined("NewCollection.TFNC") and NewCollection.TFNC eq "on"><cfset nTFNC = 1><cfelse><cfset nTFNC = 0></cfif>
        <cfif IsDefined("NewCollection.TFND") and NewCollection.TFND eq "on"><cfset nTFND = 1><cfelse><cfset nTFND = 0></cfif>
        <cfif IsDefined("NewCollection.TFOH") and NewCollection.TFOH eq "on"><cfset nTFOH = 1><cfelse><cfset nTFOH = 0></cfif>
        <cfif IsDefined("NewCollection.TFOK") and NewCollection.TFOK eq "on"><cfset nTFOK = 1><cfelse><cfset nTFOK = 0></cfif>
        <cfif IsDefined("NewCollection.TFOR") and NewCollection.TFOR eq "on"><cfset nTFOR = 1><cfelse><cfset nTFOR = 0></cfif>
        <cfif IsDefined("NewCollection.TFPA") and NewCollection.TFPA eq "on"><cfset nTFPA = 1><cfelse><cfset nTFPA = 0></cfif>
        <cfif IsDefined("NewCollection.TFRI") and NewCollection.TFRI eq "on"><cfset nTFRI = 1><cfelse><cfset nTFRI = 0></cfif>
        <cfif IsDefined("NewCollection.TFSD") and NewCollection.TFSD eq "on"><cfset nTFSD = 1><cfelse><cfset nTFSD = 0></cfif>
        <cfif IsDefined("NewCollection.TFSC") and NewCollection.TFSC eq "on"><cfset nTFSC = 1><cfelse><cfset nTFSC = 0></cfif>
        <cfif IsDefined("NewCollection.TFTN") and NewCollection.TFTN eq "on"><cfset nTFTN = 1><cfelse><cfset nTFTN = 0></cfif>
        <cfif IsDefined("NewCollection.TFTX") and NewCollection.TFTX eq "on"><cfset nTFTX = 1><cfelse><cfset nTFTX = 0></cfif>
        <cfif IsDefined("NewCollection.TFUT") and NewCollection.TFUT eq "on"><cfset nTFUT = 1><cfelse><cfset nTFUT = 0></cfif>
        <cfif IsDefined("NewCollection.TFVT") and NewCollection.TFVT eq "on"><cfset nTFVT = 1><cfelse><cfset nTFVT = 0></cfif>
        <cfif IsDefined("NewCollection.TFVA") and NewCollection.TFVA eq "on"><cfset nTFVA = 1><cfelse><cfset nTFVA = 0></cfif>
        <cfif IsDefined("NewCollection.TFWA") and NewCollection.TFWA eq "on"><cfset nTFWA = 1><cfelse><cfset nTFWA = 0></cfif>
        <cfif IsDefined("NewCollection.TFWV") and NewCollection.TFWV eq "on"><cfset nTFWV = 1><cfelse><cfset nTFWV = 0></cfif>
        <cfif IsDefined("NewCollection.TFWI") and NewCollection.TFWI eq "on"><cfset nTFWI = 1><cfelse><cfset nTFWI = 0></cfif>
        <cfif IsDefined("NewCollection.TFWY") and NewCollection.TFWY eq "on"><cfset nTFWY = 1><cfelse><cfset nTFWY = 0></cfif>
        
        <cfif IsDefined("NewCollection.TNAL") and NewCollection.TNAL eq "on"><cfset nTNAL = 1><cfelse><cfset nTNAL = 0></cfif>
        <cfif IsDefined("NewCollection.TNAK") and NewCollection.TNAK eq "on"><cfset nTNAK = 1><cfelse><cfset nTNAK = 0></cfif>
        <cfif IsDefined("NewCollection.TNAZ") and NewCollection.TNAZ eq "on"><cfset nTNAZ = 1><cfelse><cfset nTNAZ = 0></cfif>
        <cfif IsDefined("NewCollection.TNAR") and NewCollection.TNAR eq "on"><cfset nTNAR = 1><cfelse><cfset nTNAR = 0></cfif>
        <cfif IsDefined("NewCollection.TNCA") and NewCollection.TNCA eq "on"><cfset nTNCA = 1><cfelse><cfset nTNCA = 0></cfif>
        <cfif IsDefined("NewCollection.TNCO") and NewCollection.TNCO eq "on"><cfset nTNCO = 1><cfelse><cfset nTNCO = 0></cfif>
        <cfif IsDefined("NewCollection.TNCT") and NewCollection.TNCT eq "on"><cfset nTNCT = 1><cfelse><cfset nTNCT = 0></cfif>
        <cfif IsDefined("NewCollection.TNDE") and NewCollection.TNDE eq "on"><cfset nTNDE = 1><cfelse><cfset nTNDE = 0></cfif>
        <cfif IsDefined("NewCollection.TNFL") and NewCollection.TNFL eq "on"><cfset nTNFL = 1><cfelse><cfset nTNFL = 0></cfif>
        <cfif IsDefined("NewCollection.TNGA") and NewCollection.TNGA eq "on"><cfset nTNGA = 1><cfelse><cfset nTNGA = 0></cfif>
        <cfif IsDefined("NewCollection.TNHI") and NewCollection.TNHI eq "on"><cfset nTNHI = 1><cfelse><cfset nTNHI = 0></cfif>
        <cfif IsDefined("NewCollection.TNID") and NewCollection.TNID eq "on"><cfset nTNID = 1><cfelse><cfset nTNID = 0></cfif>
        <cfif IsDefined("NewCollection.TNIL") and NewCollection.TNIL eq "on"><cfset nTNIL = 1><cfelse><cfset nTNIL = 0></cfif>
        <cfif IsDefined("NewCollection.TNIN") and NewCollection.TNIN eq "on"><cfset nTNIN = 1><cfelse><cfset nTNIN = 0></cfif>
        <cfif IsDefined("NewCollection.TNIA") and NewCollection.TNIA eq "on"><cfset nTNIA = 1><cfelse><cfset nTNIA = 0></cfif>
        <cfif IsDefined("NewCollection.TNKS") and NewCollection.TNKS eq "on"><cfset nTNKS = 1><cfelse><cfset nTNKS = 0></cfif>
        <cfif IsDefined("NewCollection.TNKY") and NewCollection.TNKY eq "on"><cfset nTNKY = 1><cfelse><cfset nTNKY = 0></cfif>
        <cfif IsDefined("NewCollection.TNLA") and NewCollection.TNLA eq "on"><cfset nTNLA = 1><cfelse><cfset nTNLA = 0></cfif>
        <cfif IsDefined("NewCollection.TNMA") and NewCollection.TNMA eq "on"><cfset nTNMA = 1><cfelse><cfset nTNMA = 0></cfif>
        <cfif IsDefined("NewCollection.TNME") and NewCollection.TNME eq "on"><cfset nTNME = 1><cfelse><cfset nTNME = 0></cfif>
        <cfif IsDefined("NewCollection.TNMD") and NewCollection.TNMD eq "on"><cfset nTNMD = 1><cfelse><cfset nTNMD = 0></cfif>
		<cfif IsDefined("NewCollection.TNMI") and NewCollection.TNMI eq "on"><cfset nTNMI = 1><cfelse><cfset nTNMI = 0></cfif>
        <cfif IsDefined("NewCollection.TNMN") and NewCollection.TNMN eq "on"><cfset nTNMN = 1><cfelse><cfset nTNMN = 0></cfif>
        <cfif IsDefined("NewCollection.TNMS") and NewCollection.TNMS eq "on"><cfset nTNMS = 1><cfelse><cfset nTNMS = 0></cfif>
        <cfif IsDefined("NewCollection.TNMO") and NewCollection.TNMO eq "on"><cfset nTNMO = 1><cfelse><cfset nTNMO = 0></cfif>
        <cfif IsDefined("NewCollection.TNMT") and NewCollection.TNMT eq "on"><cfset nTNMT = 1><cfelse><cfset nTNMT = 0></cfif>
        <cfif IsDefined("NewCollection.TNNE") and NewCollection.TNNE eq "on"><cfset nTNNE = 1><cfelse><cfset nTNNE = 0></cfif>
        <cfif IsDefined("NewCollection.TNNV") and NewCollection.TNNV eq "on"><cfset nTNNV = 1><cfelse><cfset nTNNV = 0></cfif>
        <cfif IsDefined("NewCollection.TNNH") and NewCollection.TNNH eq "on"><cfset nTNNH = 1><cfelse><cfset nTNNH = 0></cfif>
        <cfif IsDefined("NewCollection.TNNJ") and NewCollection.TNNJ eq "on"><cfset nTNNJ = 1><cfelse><cfset nTNNJ = 0></cfif>
        <cfif IsDefined("NewCollection.TNNM") and NewCollection.TNNM eq "on"><cfset nTNNM = 1><cfelse><cfset nTNNM = 0></cfif>
        <cfif IsDefined("NewCollection.TNNY") and NewCollection.TNNY eq "on"><cfset nTNNY = 1><cfelse><cfset nTNNY = 0></cfif>
        <cfif IsDefined("NewCollection.TNNC") and NewCollection.TNNC eq "on"><cfset nTNNC = 1><cfelse><cfset nTNNC = 0></cfif>
        <cfif IsDefined("NewCollection.TNND") and NewCollection.TNND eq "on"><cfset nTNND = 1><cfelse><cfset nTNND = 0></cfif>
        <cfif IsDefined("NewCollection.TNOH") and NewCollection.TNOH eq "on"><cfset nTNOH = 1><cfelse><cfset nTNOH = 0></cfif>
        <cfif IsDefined("NewCollection.TNOK") and NewCollection.TNOK eq "on"><cfset nTNOK = 1><cfelse><cfset nTNOK = 0></cfif>
        <cfif IsDefined("NewCollection.TNOR") and NewCollection.TNOR eq "on"><cfset nTNOR = 1><cfelse><cfset nTNOR = 0></cfif>
        <cfif IsDefined("NewCollection.TNPA") and NewCollection.TNPA eq "on"><cfset nTNPA = 1><cfelse><cfset nTNPA = 0></cfif>
        <cfif IsDefined("NewCollection.TNRI") and NewCollection.TNRI eq "on"><cfset nTNRI = 1><cfelse><cfset nTNRI = 0></cfif>
        <cfif IsDefined("NewCollection.TNSD") and NewCollection.TNSD eq "on"><cfset nTNSD = 1><cfelse><cfset nTNSD = 0></cfif>
        <cfif IsDefined("NewCollection.TNSC") and NewCollection.TNSC eq "on"><cfset nTNSC = 1><cfelse><cfset nTNSC = 0></cfif>
        <cfif IsDefined("NewCollection.TNTN") and NewCollection.TNTN eq "on"><cfset nTNTN = 1><cfelse><cfset nTNTN = 0></cfif>
        <cfif IsDefined("NewCollection.TNTX") and NewCollection.TNTX eq "on"><cfset nTNTX = 1><cfelse><cfset nTNTX = 0></cfif>
        <cfif IsDefined("NewCollection.TNUT") and NewCollection.TNUT eq "on"><cfset nTNUT = 1><cfelse><cfset nTNUT = 0></cfif>
        <cfif IsDefined("NewCollection.TNVT") and NewCollection.TNVT eq "on"><cfset nTNVT = 1><cfelse><cfset nTNVT = 0></cfif>
        <cfif IsDefined("NewCollection.TNVA") and NewCollection.TNVA eq "on"><cfset nTNVA = 1><cfelse><cfset nTNVA = 0></cfif>
        <cfif IsDefined("NewCollection.TNWA") and NewCollection.TNWA eq "on"><cfset nTNWA = 1><cfelse><cfset nTNWA = 0></cfif>
        <cfif IsDefined("NewCollection.TNWV") and NewCollection.TNWV eq "on"><cfset nTNWV = 1><cfelse><cfset nTNWV = 0></cfif>
        <cfif IsDefined("NewCollection.TNWI") and NewCollection.TNWI eq "on"><cfset nTNWI = 1><cfelse><cfset nTNWI = 0></cfif>
        <cfif IsDefined("NewCollection.TNWY") and NewCollection.TNWY eq "on"><cfset nTNWY = 1><cfelse><cfset nTNWY = 0></cfif>
        
        	<cftransaction>
           		<cftry>
				<cfset colsNameCollection=ArrayNew(1)>
				<cfset args=StructNew()/>      	
                <!--- ProjectInformation Table --->
				<cfset args.inpTableName="pm_projectinformation"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfif REFindNoCase("_int$","#varName#")>
							<cfparam name="#varName#" default=0>
						<cfelse>
							<cfparam name="#varName#" default="">
						</cfif>
					</cfif>   
				</cfloop>
                <cfquery name="ProjectInformation" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectinformation
                    (
                    BatchId_bi,
                    PROJECTID_VCH,
                    VERSION_VCH,
                    PLATFORM_TYPE_VCH,
                    PROJECT_DURATION_VCH,
                    SUBMISSION_DATE_DT,
                    PROJECT_START_DATE_DT,
                    PROJECT_END_DATE_DT,
                    PREPARED_BY_VCH,
                    EMAIL_PHONE_VCH,
                    PROJECTLEADCONTACT_VCH,
                    SCRIPT_CONTENT_CONTACT_VCH,
                    DATA_FEED_CONTACT_VCH,
                    MANDP_CONTACT_VCH,
                    PROJECT_TEAM_VCH,
                    COMMUNICATIONS_ALERT_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTID_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VERSION_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PLATFORM_TYPE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_DURATION_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUBMISSION_DATE_DT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_START_DATE_DT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_END_DATE_DT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PREPARED_BY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAIL_PHONE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTLEADCONTACT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPT_CONTENT_CONTACT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_FEED_CONTACT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MANDP_CONTACT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_TEAM_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COMMUNICATIONS_ALERT_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="ProjectInformation" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProgramOverview Table --->
				<cfset args.inpTableName="pm_ProgramOverview"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProgramOverview" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_programoverview
                    (
                    BatchId_bi,
                    PROJECTDESC_VCH,
                    TYPE_OF_CALL_VCH,
                    PRECALL_TO_EMAIL_INT,
                    STANDALONE_CALL_INT,
                    PRECALL_TO_DIRECT_MAIL_INT,
                    POSTCALL_TO_DIRECT_MAIL_INT,
                    DRIVE_TO_WEB_INT,
                    INFORMATIONAL_INT,
                    OTHER_INT,
                    OTHER_VCH,
                    PLATFORM_ATT_INT,
                    PLATFORM_MB_INT,
                    PLATFORM_EGS_INT,
                    CUSTOMER_BASE_CON_INT,
                    CUSTOMER_BASE_BUS_INT,
                    AL,
                    AK,
                    AZ,
                    AR,
                    CA,
                    CO,
                    CT,
                    DE,
                    FL,
                    GA,
                    HI,
                    ID,
                    IL,
                    nIN,
                    IA,
                    KS,
                    KY,
                    LA,
                    MA,
                    MD,
                    ME,
                    MI,
                    MN,
                    MS,
                    MO,
                    MT,
                    NE,
                    NV,
                    NH,
                    NJ,
                    NM,
                    NY,
                    NC,
                    ND,
                    OH,
                    OK,
                    zOR,
                    PA,
                    RI,
                    SD,
                    SC,
                    TN,
                    TX,
                    UT,
                    VT,
                    VA,
                    WA,
                    WV,
                    WI,
                    WY
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTDESC_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TYPE_OF_CALL_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PRECALL_TO_EMAIL_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.STANDALONE_CALL_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PRECALL_TO_DIRECT_MAIL_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.POSTCALL_TO_DIRECT_MAIL_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DRIVE_TO_WEB_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.INFORMATIONAL_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.OTHER_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OTHER_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_ATT_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_MB_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_EGS_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CUSTOMER_BASE_CON_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CUSTOMER_BASE_BUS_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nAL#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nAK#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nAZ#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nAR#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nCA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nCO#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nCT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nDE#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nFL#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nGA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nHI#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nID#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nIL#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nIN#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nIA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nKS#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nKY#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nLA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMD#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nME#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMI#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMN#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMS#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMO#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nMT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNE#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNV#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNJ#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNM#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNY#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nNC#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nND#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nOH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nOK#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nOR#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nPA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nRI#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nSD#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nSC#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nTN#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nTX#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nUT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nVT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nVA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nWA#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nWV#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nWI#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#nWY#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="ProjectOverview" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProgramComplexity Table --->
				<cfset args.inpTableName="pm_ProgramComplexity"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>					
                <cfquery name="AddProgramComplexity" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_programcomplexity
                    (
                    BatchId_bi,
                    PROGRAM_COMPLEXITY_VCH,
                    MONITORING_THRESHOLD_INT,
                    MONDAY_VCH,
                    TUESDAY_VCH,
                    WEDNESDAY_VCH,
                    THURSDAY_VCH,
                    FRIDAY_VCH,
                    SATURDAY_VCH,
                    SUNDAY_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROGRAM_COMPLEXITY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.MONITORING_THRESHOLD_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MONDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TUESDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WEDNESDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.THURSDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FRIDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SATURDAY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUNDAY_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="ProjectComplexity" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectSettings Table --->
				<cfset args.inpTableName="pm_ProjectSettings"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProjectSettings" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectsettings
                    (
                    BatchId_bi,
                    MFDIALINGHOURS_VCH,
                    SATURDAYDIALINGHOURS_VCH,
                    SUNDAYDIALINGHOURS_VCH,
                    HOLIDAYDIALINGHOURS_VCH,
                    DAILYCALLVOLUMES_VCH,
                    CONSUMERCALLERID_VCH,
                    BUSINESSCALLERID_VCH,
                    SPANISHOPTION_VCH,
                    ALTERNATENUMBERDIALING_VCH,
                    PHONENUMTOBEDIALED_VCH,
                    REDIALLOGIC_VCH,
                    REDIALATTEMPTS_VCH,
                    ROLLOVERDIALING_VCH,
                    ANSWERMACHINESTRATEGY_VCH,
                    MAXATTEMPTS_INT,
                    RINGCOUNT_INT,
                    ANSWERMACHINEWAIT_VCH,
                    BUSYCALLBACKTIME_VCH,
                    BCBTIMEREDIALTIME_VCH,
                    NOANSWERCALLBACKTIME_VCH,
                    NACBTIMEREDIALTIME_VCH,
                    OTHERFAX_VCH,
                    OFTIMEREDIALTIME_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MFDIALINGHOURS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SATURDAYDIALINGHOURS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUNDAYDIALINGHOURS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HOLIDAYDIALINGHOURS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DAILYCALLVOLUMES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CONSUMERCALLERID_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BUSINESSCALLERID_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SPANISHOPTION_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALTERNATENUMBERDIALING_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PHONENUMTOBEDIALED_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.REDIALLOGIC_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.REDIALATTEMPTS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ROLLOVERDIALING_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ANSWERMACHINESTRATEGY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MAXATTEMPTS_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RINGCOUNT_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ANSWERMACHINEWAIT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BUSYCALLBACKTIME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BCBTIMEREDIALTIME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NOANSWERCALLBACKTIME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NACBTIMEREDIALTIME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OTHERFAX_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OFTIMEREDIALTIME_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="ProjectSettings" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectScriptInfo Table  - Contains Voice Scripts --->
				<cfset args.inpTableName="pm_ProjectScriptInfo"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProjectScriptInfo" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectscriptinfo
                    (
                    BatchId_bi,
                    SCRIPT_INFORMATION_VCH,
                    VOICE_TALENT_VCH,
                    BENEFITS_VCH,
                    PHRASES_VCH,
                    TONALITY_VCH,
                    NOTES_VCH,
                    CALLCENTERSALES_VCH,
                    INBOUNDCALLSMETHOD_VCH,
                    SCRIPTTOAPPROVE_VCH,
                    SCRIPTAPPROVEDBY_VCH,
                    SCRIPTAPPROVEDDATE_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPT_INFORMATION_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VOICE_TALENT_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BENEFITS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PHRASES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TONALITY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NOTES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALLCENTERSALES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBOUNDCALLSMETHOD_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTTOAPPROVE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTAPPROVEDBY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTAPPROVEDDATE_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Script Info" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectEmailScripts Table  - Contains Email Scripts --->
				<cfset args.inpTableName="pm_ProjectEmailScripts"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProjectEmailScripts" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectemailscripts
                    (
                    BatchId_bi,
                    EMAILPROJECTNAME_VCH,
                    EMAILFROMLINE_VCH,
                    EMAILSUBJECTLINE_VCH,
                    EMAILTAGCODE_VCH,
                    EMAILDELIVERYATTEMPTS_VCH,
                    BOUNCEBACKSTRATEGY_VCH,
                    ORIGINATINGFILENAME_VCH,
                    EXPORTDATEFEED_VCH,
                    DATAFEEDNOTES_VCH,
                    EMAILHTMLTEMPLATE_VCH,
                    EMAILTEXTTEMPLATE_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILPROJECTNAME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILFROMLINE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILSUBJECTLINE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILTAGCODE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILDELIVERYATTEMPTS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BOUNCEBACKSTRATEGY_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORIGINATINGFILENAME_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EXPORTDATEFEED_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATAFEEDNOTES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILHTMLTEMPLATE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILTEXTTEMPLATE_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Email Scripts" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectDataFeeds Table --->
				<cfset args.inpTableName="pm_ProjectDataFeeds"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProjectDataFeeds" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectdatafeeds
                    (
                    BatchId_bi,
                    DATA_REQUIREMENTS_VCH,
                    IT_INTERFACE_SIGNOFF_VCH,
                    CALL_RESULTS_PROCESSING_VCH,
                    CALL_YES_VCH,
                    DATA_EXCHANGE_DEVELOP_VCH,
                    DATA_SOURCE_VCH,
                    DO_NOT_SCRUB_INT,
                    STATE_DNC_LIST_INT,
                    CLIENT_DNC_INT,
                    DO_NOT_DIAL_STATES_INT,
                    NATIONAL_DNC_INT,
                    COMPANY_INTERNAL_LIST_INT,
                    CELL_PHONE_LIST_INT,
                    DATA_FILE_TRANSMISSION_VCH,
                    FILTER_DUPLICATE_RECORDS_INT,
                    FILTER_WITHIN_FILE_INT,
                    FILTER_SAME_DAY_INT,
                    FILTER_X_AMOUNT_DAYS_INT,
                    X_DAYS_NUMBER_INT,
                    PDFNOTES_VCH
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_REQUIREMENTS_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IT_INTERFACE_SIGNOFF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALL_RESULTS_PROCESSING_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALL_YES_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_EXCHANGE_DEVELOP_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_SOURCE_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DO_NOT_SCRUB_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.STATE_DNC_LIST_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CLIENT_DNC_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DO_NOT_DIAL_STATES_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.NATIONAL_DNC_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.COMPANY_INTERNAL_LIST_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CELL_PHONE_LIST_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_FILE_TRANSMISSION_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_DUPLICATE_RECORDS_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_WITHIN_FILE_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_SAME_DAY_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_X_AMOUNT_DAYS_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.X_DAYS_NUMBER_INT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PDFNOTES_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Data Feeds" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectReportInfo Table --->
				<cfset args.inpTableName="pm_ProjectReportInfo"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>
                <cfquery name="AddProjectReportInfo" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectreportinfo
                    (
                    BatchId_bi,
                    Project_Reporting_vch
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.Project_Reporting_vch#">
                    )
                </cfquery>
                <cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="ProjectReport" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectDisclosures Table --->
				<cfset args.inpTableName="pm_ProjectDisclosures"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddProjectDisclosures" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_projectdisclosures
                    (
                    BatchId_bi,
                    Project_Disclosures_vch
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.Project_Disclosures_vch#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Disclosure" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                
                <!--- ScriptsTollFree Table --->
				<cfset args.inpTableName="pm_ScriptsTollFree"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>				
                <cfquery name="AddScriptsTollFree" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_scriptstollfree
                    (
                    BatchId_bi,
                    ALTF_vch,
                    AKTF_vch,
                    AZTF_vch,
                    ARTF_vch,
                    CATF_vch,
                    COTF_vch,
                    CTTF_vch,
                    DETF_vch,
                    FLTF_vch,
                    GATF_vch,
                    HITF_vch,
                    IDTF_vch,
                    ILTF_vch,
                    INTF_vch,
                    IATF_vch,
                    KSTF_vch,
                    KYTF_vch,
                    LATF_vch,
                    MATF_vch,
                    METF_vch,
                    MDTF_vch,
                    MITF_vch,
                    MNTF_vch,
                    MSTF_vch,
                    MOTF_vch,
                    MTTF_vch,
                    NETF_vch,
                    NVTF_vch,
                    NHTF_vch,
                    NJTF_vch,
                    NMTF_vch,
                    NYTF_vch,
                    NCTF_vch,
                    NDTF_vch,
                    OHTF_vch,
                    OKTF_vch,
                    ORTF_vch,
                    PATF_vch,
                    RITF_vch,
                    SDTF_vch,
                    SCTF_vch,
                    TNTF_vch,
                    TXTF_vch,
                    UTTF_vch,
                    VTTF_vch,
                    VATF_vch,
                    WATF_vch,
                    WVTF_vch,
                    WITF_vch,
                    WYTF_vch,
                    ALCTF_vch,
                    AKCTF_vch,
                    AZCTF_vch,
                    ARCTF_vch,
                    CACTF_vch,
                    COCTF_vch,
                    CTCTF_vch,
                    DECTF_vch,
                    FLCTF_vch,
                    GACTF_vch,
                    HICTF_vch,
                    IDCTF_vch,
                    ILCTF_vch,
                    INCTF_vch,
                    IACTF_vch,
                    KSCTF_vch,
                    KYCTF_vch,
                    LACTF_vch,
                    MACTF_vch,
                    MECTF_vch,
                    MDCTF_vch,
                    MICTF_vch,
                    MNCTF_vch,
                    MSCTF_vch,
                    MOCTF_vch,
                    MTCTF_vch,
                    NECTF_vch,
                    NVCTF_vch,
                    NHCTF_vch,
                    NJCTF_vch,
                    NMCTF_vch,
                    NYCTF_vch,
                    NCCTF_vch,
                    NDCTF_vch,
                    OHCTF_vch,
                    OKCTF_vch,
                    ORCTF_vch,
                    PACTF_vch,
                    RICTF_vch,
                    SDCTF_vch,
                    SCCTF_vch,
                    TNCTF_vch,
                    TXCTF_vch,
                    UTCTF_vch,
                    VTCTF_vch,
                    VACTF_vch,
                    WACTF_vch,
                    WVCTF_vch,
                    WICTF_vch,
                    WYCTF_vch,
                    ALBTF_vch,
                    AKBTF_vch,
                    AZBTF_vch,
                    ARBTF_vch,
                    CABTF_vch,
                    COBTF_vch,
                    CTBTF_vch,
                    DEBTF_vch,
                    FLBTF_vch,
                    GABTF_vch,
                    HIBTF_vch,
                    IDBTF_vch,
                    ILBTF_vch,
                    INBTF_vch,
                    IABTF_vch,
                    KSBTF_vch,
                    KYBTF_vch,
                    LABTF_vch,
                    MABTF_vch,
                    MEBTF_vch,
                    MDBTF_vch,
                    MIBTF_vch,
                    MNBTF_vch,
                    MSBTF_vch,
                    MOBTF_vch,
                    MTBTF_vch,
                    NEBTF_vch,
                    NVBTF_vch,
                    NHBTF_vch,
                    NJBTF_vch,
                    NMBTF_vch,
                    NYBTF_vch,
                    NCBTF_vch,
                    NDBTF_vch,
                    OHBTF_vch,
                    OKBTF_vch,
                    ORBTF_vch,
                    PABTF_vch,
                    RIBTF_vch,
                    SDBTF_vch,
                    SCBTF_vch,
                    TNBTF_vch,
                    TXBTF_vch,
                    UTBTF_vch,
                    VTBTF_vch,
                    VABTF_vch,
                    WABTF_vch,
                    WVBTF_vch,
                    WIBTF_vch,
                    WYBTF_vch
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFAL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFAK#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFAZ#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFAR#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFCA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFCO#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFCT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFDE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFFL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFGA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFHI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFIL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFIN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFIA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFKS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFKY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFLA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFME#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMD#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMO#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFMT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNV#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNJ#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNM#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFNC#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFND#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFOH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFOK#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFOR#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFPA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFRI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFSD#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFSC#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFTN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFTX#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFUT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFVT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFVA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFWA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFWV#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFWI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTFWY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DECTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HICTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MECTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MICTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NECTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RICTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WACTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WICTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYCTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DEBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HIBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MEBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MIBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NEBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RIBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WABTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WIBTF_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYBTF_VCH#">
                    )
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Toll Free" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
				<!--- ScriptsTransfer Table --->
				<cfset args.inpTableName="pm_ScriptsTransfer"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>					
                <cfquery name="AddScriptsTransfer" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.pm_scriptstransfer
                    (
                    BatchId_bi,
                    AL_vch,
                    AK_vch,
                    AZ_vch,
                    AR_vch,
                    CA_vch,
                    CO_vch,
                    CT_vch,
                    DE_vch,
                    FL_vch,
                    GA_vch,
                    HI_vch,
                    ID_vch,
                    IL_vch,
                    IN_vch,
                    IA_vch,
                    KS_vch,
                    KY_vch,
                    LA_vch,
                    MA_vch,
                    ME_vch,
                    MD_vch,
                    MI_vch,
                    MN_vch,
                    MS_vch,
                    MO_vch,
                    MT_vch,
                    NE_vch,
                    NV_vch,
                    NH_vch,
                    NJ_vch,
                    NM_vch,
                    NY_vch,
                    NC_vch,
                    ND_vch,
                    OH_vch,
                    OK_vch,
                    OR_vch,
                    PA_vch,
                    RI_vch,
                    SD_vch,
                    SC_vch,
                    TN_vch,
                    TX_vch,
                    UT_vch,
                    VT_vch,
                    VA_vch,
                    WA_vch,
                    WV_vch,
                    WI_vch,
                    WY_vch,
                    ALCTN_vch,
                    AKCTN_vch,
                    AZCTN_vch,
                    ARCTN_vch,
                    CACTN_vch,
                    COCTN_vch,
                    CTCTN_vch,
                    DECTN_vch,
                    FLCTN_vch,
                    GACTN_vch,
                    HICTN_vch,
                    IDCTN_vch,
                    ILCTN_vch,
                    INCTN_vch,
                    IACTN_vch,
                    KSCTN_vch,
                    KYCTN_vch,
                    LACTN_vch,
                    MACTN_vch,
                    MECTN_vch,
                    MDCTN_vch,
                    MICTN_vch,
                    MNCTN_vch,
                    MSCTN_vch,
                    MOCTN_vch,
                    MTCTN_vch,
                    NECTN_vch,
                    NVCTN_vch,
                    NHCTN_vch,
                    NJCTN_vch,
                    NMCTN_vch,
                    NYCTN_vch,
                    NCCTN_vch,
                    NDCTN_vch,
                    OHCTN_vch,
                    OKCTN_vch,
                    ORCTN_vch,
                    PACTN_vch,
                    RICTN_vch,
                    SDCTN_vch,
                    SCCTN_vch,
                    TNCTN_vch,
                    TXCTN_vch,
                    UTCTN_vch,
                    VTCTN_vch,
                    VACTN_vch,
                    WACTN_vch,
                    WVCTN_vch,
                    WICTN_vch,
                    WYCTN_vch,
                    ALBTN_vch,
                    AKBTN_vch,
                    AZBTN_vch,
                    ARBTN_vch,
                    CABTN_vch,
                    COBTN_vch,
                    CTBTN_vch,
                    DEBTN_vch,
                    FLBTN_vch,
                    GABTN_vch,
                    HIBTN_vch,
                    IDBTN_vch,
                    ILBTN_vch,
                    INBTN_vch,
                    IABTN_vch,
                    KSBTN_vch,
                    KYBTN_vch,
                    LABTN_vch,
                    MABTN_vch,
                    MEBTN_vch,
                    MDBTN_vch,
                    MIBTN_vch,
                    MNBTN_vch,
                    MSBTN_vch,
                    MOBTN_vch,
                    MTBTN_vch,
                    NEBTN_vch,
                    NVBTN_vch,
                    NHBTN_vch,
                    NJBTN_vch,
                    NMBTN_vch,
                    NYBTN_vch,
                    NCBTN_vch,
                    NDBTN_vch,
                    OHBTN_vch,
                    OKBTN_vch,
                    ORBTN_vch,
                    PABTN_vch,
                    RIBTN_vch,
                    SDBTN_vch,
                    SCBTN_vch,
                    TNBTN_vch,
                    TXBTN_vch,
                    UTBTN_vch,
                    VTBTN_vch,
                    VABTN_vch,
                    WABTN_vch,
                    WVBTN_vch,
                    WIBTN_vch,
                    WYBTN_vch
                    )
                    values
                    (
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNAL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNAK#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNAZ#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNAR#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNCA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNCO#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNCT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNDE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNFL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNGA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNHI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNIL#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNIN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNIA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNKS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNKY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNLA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNME#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMD#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMO#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNMT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNV#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNJ#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNM#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNNC#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNND#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNOH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNOK#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNOR#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNPA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNRI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNSD#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNSC#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNTN#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNTX#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNUT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNVT#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNVA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNWA#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNWV#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNWI#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#nTNWY#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DECTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HICTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MECTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MICTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NECTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RICTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WACTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WICTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYCTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DEBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HIBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MEBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MIBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NEBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RIBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WABTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WIBTN_VCH#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYBTN_VCH#">
                    )
                </cfquery>
                <cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Transfer" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
           </cftransaction>     
                
                <cfset results = "Project Data Inserted">
                

		<cfreturn results>
	</cffunction>
    
    
    <cffunction name="UpdatePRJCTRow" access="remote" returntype="string">
    	
        	<cfset NewCollection = DeserializeJSON(argumentCollection)>
                
                <cfif IsDefined("NewCollection.PRECALL_TO_EMAIL_INT") and NewCollection.PRECALL_TO_EMAIL_INT eq "on"><cfset NewCollection.PRECALL_TO_EMAIL_INT = 1><cfelse><cfset NewCollection.PRECALL_TO_EMAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.STANDALONE_CALL_INT") and NewCollection.STANDALONE_CALL_INT eq "on"><cfset NewCollection.STANDALONE_CALL_INT = 1><cfelse><cfset NewCollection.STANDALONE_CALL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PRECALL_TO_DIRECT_MAIL_INT") and NewCollection.PRECALL_TO_DIRECT_MAIL_INT eq "on"><cfset NewCollection.PRECALL_TO_DIRECT_MAIL_INT = 1><cfelse><cfset NewCollection.PRECALL_TO_DIRECT_MAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.POSTCALL_TO_DIRECT_MAIL_INT") and NewCollection.POSTCALL_TO_DIRECT_MAIL_INT eq "on"><cfset NewCollection.POSTCALL_TO_DIRECT_MAIL_INT = 1><cfelse><cfset NewCollection.POSTCALL_TO_DIRECT_MAIL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.DRIVE_TO_WEB_INT") and NewCollection.DRIVE_TO_WEB_INT eq "on"><cfset NewCollection.DRIVE_TO_WEB_INT = 1><cfelse><cfset NewCollection.DRIVE_TO_WEB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.INFORMATIONAL_INT") and NewCollection.INFORMATIONAL_INT eq "on"><cfset NewCollection.INFORMATIONAL_INT = 1><cfelse><cfset NewCollection.INFORMATIONAL_INT = 0></cfif>
                <cfif IsDefined("NewCollection.OTHER_INT") and NewCollection.OTHER_INT eq "on"><cfset NewCollection.OTHER_INT = 1><cfelse><cfset NewCollection.OTHER_INT = 0></cfif>
                
				<cfif IsDefined("NewCollection.PLATFORM_ATT_INT") and NewCollection.PLATFORM_ATT_INT eq "on"><cfset NewCollection.PLATFORM_ATT_INT = 1><cfelse><cfset NewCollection.PLATFORM_ATT_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PLATFORM_MB_INT") and NewCollection.PLATFORM_MB_INT eq "on"><cfset NewCollection.PLATFORM_MB_INT = 1><cfelse><cfset NewCollection.PLATFORM_MB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.PLATFORM_EGS_INT") and NewCollection.PLATFORM_EGS_INT eq "on"><cfset NewCollection.PLATFORM_EGS_INT = 1><cfelse><cfset NewCollection.PLATFORM_EGS_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.CUSTOMER_BASE_CON_INT") and NewCollection.CUSTOMER_BASE_CON_INT eq "on"><cfset NewCollection.CUSTOMER_BASE_CON_INT = 1><cfelse><cfset NewCollection.CUSTOMER_BASE_CON_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CUSTOMER_BASE_BUS_INT") and NewCollection.CUSTOMER_BASE_BUS_INT eq "on"><cfset NewCollection.CUSTOMER_BASE_BUS_INT = 1><cfelse><cfset NewCollection.CUSTOMER_BASE_BUS_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.DO_NOT_SCRUB_INT") and NewCollection.DO_NOT_SCRUB_INT eq "on"><cfset NewCollection.DO_NOT_SCRUB_INT = 1><cfelse><cfset NewCollection.DO_NOT_SCRUB_INT = 0></cfif>
                <cfif IsDefined("NewCollection.STATE_DNC_LIST_INT") and NewCollection.STATE_DNC_LIST_INT eq "on"><cfset NewCollection.STATE_DNC_LIST_INT = 1><cfelse><cfset NewCollection.STATE_DNC_LIST_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CLIENT_DNC_INT") and NewCollection.CLIENT_DNC_INT eq "on"><cfset NewCollection.CLIENT_DNC_INT = 1><cfelse><cfset NewCollection.CLIENT_DNC_INT = 0></cfif>
                <cfif IsDefined("NewCollection.DO_NOT_DIAL_STATES_INT") and NewCollection.DO_NOT_DIAL_STATES_INT eq "on"><cfset NewCollection.DO_NOT_DIAL_STATES_INT = 1><cfelse><cfset NewCollection.DO_NOT_DIAL_STATES_INT = 0></cfif>
                <cfif IsDefined("NewCollection.NATIONAL_DNC_INT") and NewCollection.NATIONAL_DNC_INT eq "on"><cfset NewCollection.NATIONAL_DNC_INT = 1><cfelse><cfset NewCollection.NATIONAL_DNC_INT = 0></cfif>
                <cfif IsDefined("NewCollection.COMPANY_INTERNAL_LIST_INT") and NewCollection.COMPANY_INTERNAL_LIST_INT eq "on"><cfset NewCollection.COMPANY_INTERNAL_LIST_INT = 1><cfelse><cfset NewCollection.COMPANY_INTERNAL_LIST_INT = 0></cfif>
                <cfif IsDefined("NewCollection.CELL_PHONE_LIST_INT") and NewCollection.CELL_PHONE_LIST_INT eq "on"><cfset NewCollection.CELL_PHONE_LIST_INT = 1><cfelse><cfset NewCollection.CELL_PHONE_LIST_INT = 0></cfif>
                
                <cfif IsDefined("NewCollection.FILTER_WITHIN_FILE_INT") and NewCollection.FILTER_WITHIN_FILE_INT eq "on"><cfset NewCollection.FILTER_WITHIN_FILE_INT = 1><cfelse><cfset NewCollection.FILTER_WITHIN_FILE_INT = 0></cfif>
                <cfif IsDefined("NewCollection.FILTER_SAME_DAY_INT") and NewCollection.FILTER_SAME_DAY_INT eq "on"><cfset NewCollection.FILTER_SAME_DAY_INT = 1><cfelse><cfset NewCollection.FILTER_SAME_DAY_INT = 0></cfif>
                <cfif IsDefined("NewCollection.FILTER_X_AMOUNT_DAYS_INT") and NewCollection.FILTER_X_AMOUNT_DAYS_INT eq "on"><cfset NewCollection.FILTER_X_AMOUNT_DAYS_INT = 1><cfelse><cfset NewCollection.FILTER_X_AMOUNT_DAYS_INT = 0></cfif>

                
                
                <cfparam name="NewCollection.X_DAYS_NUMBER_INT" default="0">
                <cfif IsDefined("NewCollection.SUBMISSION_DATE_DT") and len(trim(NewCollection.SUBMISSION_DATE_DT)) neq 0>
                    <cfset NewCollection.SUBMISSION_DATE_DT = DateFormat((NewCollection.SUBMISSION_DATE_DT),'yyyy-mm-dd')>
                <cfelse>
                    <cfset NewCollection.SUBMISSION_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
                </cfif>
                
                <cfif IsDefined("NewCollection.PROJECT_START_DATE_DT") and len(trim(NewCollection.PROJECT_START_DATE_DT)) neq 0>
                    <cfset NewCollection.PROJECT_START_DATE_DT = DateFormat((NewCollection.PROJECT_START_DATE_DT),'yyyy-mm-dd')>
                <cfelse>
                    <cfset NewCollection.PROJECT_START_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
                </cfif>
                
                <cfif IsDefined("NewCollection.PROJECT_END_DATE_DT") and len(trim(NewCollection.PROJECT_END_DATE_DT)) neq 0>
                    <cfset NewCollection.PROJECT_END_DATE_DT = DateFormat((NewCollection.PROJECT_END_DATE_DT),'yyyy-mm-dd')>
                <cfelse>
                    <cfset NewCollection.PROJECT_END_DATE_DT = DateFormat(Now(),'yyyy-mm-dd')>
                </cfif>
                
                <cfif IsDefined("NewCollection.AL") and NewCollection.AL eq "on"><cfset nAL = 1><cfelse><cfset nAL = 0></cfif>
                <cfif IsDefined("NewCollection.AK") and NewCollection.AK eq "on"><cfset nAK = 1><cfelse><cfset nAK = 0></cfif>
                <cfif IsDefined("NewCollection.AZ") and NewCollection.AZ eq "on"><cfset nAZ = 1><cfelse><cfset nAZ = 0></cfif>
                <cfif IsDefined("NewCollection.AR") and NewCollection.AR eq "on"><cfset nAR = 1><cfelse><cfset nAR = 0></cfif>
                <cfif IsDefined("NewCollection.CA") and NewCollection.CA eq "on"><cfset nCA = 1><cfelse><cfset nCA = 0></cfif>
                <cfif IsDefined("NewCollection.CO") and NewCollection.CO eq "on"><cfset nCO = 1><cfelse><cfset nCO = 0></cfif>
                <cfif IsDefined("NewCollection.CT") and NewCollection.CT eq "on"><cfset nCT = 1><cfelse><cfset nCT = 0></cfif>
                <cfif IsDefined("NewCollection.DE") and NewCollection.DE eq "on"><cfset nDE = 1><cfelse><cfset nDE = 0></cfif>
                <cfif IsDefined("NewCollection.FL") and NewCollection.FL eq "on"><cfset nFL = 1><cfelse><cfset nFL = 0></cfif>
                <cfif IsDefined("NewCollection.GA") and NewCollection.GA eq "on"><cfset nGA = 1><cfelse><cfset nGA = 0></cfif>
                <cfif IsDefined("NewCollection.HI") and NewCollection.HI eq "on"><cfset nHI = 1><cfelse><cfset nHI = 0></cfif>
                <cfif IsDefined("NewCollection.ID") and NewCollection.ID eq "on"><cfset nID = 1><cfelse><cfset nID = 0></cfif>
                <cfif IsDefined("NewCollection.IL") and NewCollection.IL eq "on"><cfset nIL = 1><cfelse><cfset nIL = 0></cfif>
                <cfif IsDefined("NewCollection.IN") and NewCollection.IN eq "on"><cfset nIN = 1><cfelse><cfset nIN = 0></cfif>
                <cfif IsDefined("NewCollection.IA") and NewCollection.IA eq "on"><cfset nIA = 1><cfelse><cfset nIA = 0></cfif>
                <cfif IsDefined("NewCollection.KS") and NewCollection.KS eq "on"><cfset nKS = 1><cfelse><cfset nKS = 0></cfif>
                <cfif IsDefined("NewCollection.KY") and NewCollection.KY eq "on"><cfset nKY = 1><cfelse><cfset nKY = 0></cfif>
                <cfif IsDefined("NewCollection.LA") and NewCollection.LA eq "on"><cfset nLA = 1><cfelse><cfset nLA = 0></cfif>
                <cfif IsDefined("NewCollection.MA") and NewCollection.MA eq "on"><cfset nMA = 1><cfelse><cfset nMA = 0></cfif>
                <cfif IsDefined("NewCollection.ME") and NewCollection.ME eq "on"><cfset nME = 1><cfelse><cfset nME = 0></cfif>
                <cfif IsDefined("NewCollection.MD") and NewCollection.MD eq "on"><cfset nMD = 1><cfelse><cfset nMD = 0></cfif>
                <cfif IsDefined("NewCollection.MI") and NewCollection.MI eq "on"><cfset nMI = 1><cfelse><cfset nMI = 0></cfif>
                <cfif IsDefined("NewCollection.MN") and NewCollection.MN eq "on"><cfset nMN = 1><cfelse><cfset nMN = 0></cfif>
                <cfif IsDefined("NewCollection.MS") and NewCollection.MS eq "on"><cfset nMS = 1><cfelse><cfset nMS = 0></cfif>
                <cfif IsDefined("NewCollection.MO") and NewCollection.MO eq "on"><cfset nMO = 1><cfelse><cfset nMO = 0></cfif>
                <cfif IsDefined("NewCollection.MT") and NewCollection.MT eq "on"><cfset nMT = 1><cfelse><cfset nMT = 0></cfif>
                <cfif IsDefined("NewCollection.NE") and NewCollection.NE eq "on"><cfset nNE = 1><cfelse><cfset nNE = 0></cfif>
                <cfif IsDefined("NewCollection.NV") and NewCollection.NV eq "on"><cfset nNV = 1><cfelse><cfset nNV = 0></cfif>
                <cfif IsDefined("NewCollection.NH") and NewCollection.NH eq "on"><cfset nNH = 1><cfelse><cfset nNH = 0></cfif>
                <cfif IsDefined("NewCollection.NJ") and NewCollection.NJ eq "on"><cfset nNJ = 1><cfelse><cfset nNJ = 0></cfif>
                <cfif IsDefined("NewCollection.NM") and NewCollection.NM eq "on"><cfset nNM = 1><cfelse><cfset nNM = 0></cfif>
                <cfif IsDefined("NewCollection.NY") and NewCollection.NY eq "on"><cfset nNY = 1><cfelse><cfset nNY = 0></cfif>
                <cfif IsDefined("NewCollection.NC") and NewCollection.NC eq "on"><cfset nNC = 1><cfelse><cfset nNC = 0></cfif>
                <cfif IsDefined("NewCollection.ND") and NewCollection.ND eq "on"><cfset nND = 1><cfelse><cfset nND = 0></cfif>
                <cfif IsDefined("NewCollection.OH") and NewCollection.OH eq "on"><cfset nOH = 1><cfelse><cfset nOH = 0></cfif>
                <cfif IsDefined("NewCollection.OK") and NewCollection.OK eq "on"><cfset nOK = 1><cfelse><cfset nOK = 0></cfif>
                <cfif IsDefined("NewCollection.zOR")><cfset nOR = 1><cfelse><cfset nOR = 0></cfif>
                <cfif IsDefined("NewCollection.PA") and NewCollection.PA eq "on"><cfset nPA = 1><cfelse><cfset nPA = 0></cfif>
                <cfif IsDefined("NewCollection.RI") and NewCollection.RI eq "on"><cfset nRI = 1><cfelse><cfset nRI = 0></cfif>
                <cfif IsDefined("NewCollection.SD") and NewCollection.SD eq "on"><cfset nSD = 1><cfelse><cfset nSD = 0></cfif>
                <cfif IsDefined("NewCollection.SC") and NewCollection.SC eq "on"><cfset nSC = 1><cfelse><cfset nSC = 0></cfif>
                <cfif IsDefined("NewCollection.TN") and NewCollection.TN eq "on"><cfset nTN = 1><cfelse><cfset nTN = 0></cfif>
                <cfif IsDefined("NewCollection.TX") and NewCollection.TX eq "on"><cfset nTX = 1><cfelse><cfset nTX = 0></cfif>
                <cfif IsDefined("NewCollection.UT") and NewCollection.UT eq "on"><cfset nUT = 1><cfelse><cfset nUT = 0></cfif>
                <cfif IsDefined("NewCollection.VT") and NewCollection.VT eq "on"><cfset nVT = 1><cfelse><cfset nVT = 0></cfif>
                <cfif IsDefined("NewCollection.VA") and NewCollection.VA eq "on"><cfset nVA = 1><cfelse><cfset nVA = 0></cfif>
                <cfif IsDefined("NewCollection.WA") and NewCollection.WA eq "on"><cfset nWA = 1><cfelse><cfset nWA = 0></cfif>
                <cfif IsDefined("NewCollection.WV") and NewCollection.WV eq "on"><cfset nWV = 1><cfelse><cfset nWV = 0></cfif>
                <cfif IsDefined("NewCollection.WI") and NewCollection.WI eq "on"><cfset nWI = 1><cfelse><cfset nWI = 0></cfif>
                <cfif IsDefined("NewCollection.WY") and NewCollection.WY eq "on"><cfset nWY = 1><cfelse><cfset nWY = 0></cfif>
                
                <cfif IsDefined("NewCollection.TFAL") and NewCollection.TFAL eq "on"><cfset nTFAL = 1><cfelse><cfset nTFAL = 0></cfif>
                <cfif IsDefined("NewCollection.TFAK") and NewCollection.TFAK eq "on"><cfset nTFAK = 1><cfelse><cfset nTFAK = 0></cfif>
                <cfif IsDefined("NewCollection.TFAZ") and NewCollection.TFAZ eq "on"><cfset nTFAZ = 1><cfelse><cfset nTFAZ = 0></cfif>
                <cfif IsDefined("NewCollection.TFAR") and NewCollection.TFAR eq "on"><cfset nTFAR = 1><cfelse><cfset nTFAR = 0></cfif>
                <cfif IsDefined("NewCollection.TFCA") and NewCollection.TFCA eq "on"><cfset nTFCA = 1><cfelse><cfset nTFCA = 0></cfif>
                <cfif IsDefined("NewCollection.TFCO") and NewCollection.TFCO eq "on"><cfset nTFCO = 1><cfelse><cfset nTFCO = 0></cfif>
                <cfif IsDefined("NewCollection.TFCT") and NewCollection.TFCT eq "on"><cfset nTFCT = 1><cfelse><cfset nTFCT = 0></cfif>
                <cfif IsDefined("NewCollection.TFDE") and NewCollection.TFDE eq "on"><cfset nTFDE = 1><cfelse><cfset nTFDE = 0></cfif>
                <cfif IsDefined("NewCollection.TFFL") and NewCollection.TFFL eq "on"><cfset nTFFL = 1><cfelse><cfset nTFFL = 0></cfif>
                <cfif IsDefined("NewCollection.TFGA") and NewCollection.TFGA eq "on"><cfset nTFGA = 1><cfelse><cfset nTFGA = 0></cfif>
                <cfif IsDefined("NewCollection.TFHI") and NewCollection.TFHI eq "on"><cfset nTFHI = 1><cfelse><cfset nTFHI = 0></cfif>
                <cfif IsDefined("NewCollection.TFID") and NewCollection.TFID eq "on"><cfset nTFID = 1><cfelse><cfset nTFID = 0></cfif>
                <cfif IsDefined("NewCollection.TFIL") and NewCollection.TFIL eq "on"><cfset nTFIL = 1><cfelse><cfset nTFIL = 0></cfif>
                <cfif IsDefined("NewCollection.TFIN") and NewCollection.TFIN eq "on"><cfset nTFIN = 1><cfelse><cfset nTFIN = 0></cfif>
                <cfif IsDefined("NewCollection.TFIA") and NewCollection.TFIA eq "on"><cfset nTFIA = 1><cfelse><cfset nTFIA = 0></cfif>
                <cfif IsDefined("NewCollection.TFKS") and NewCollection.TFKS eq "on"><cfset nTFKS = 1><cfelse><cfset nTFKS = 0></cfif>
                <cfif IsDefined("NewCollection.TFKY") and NewCollection.TFKY eq "on"><cfset nTFKY = 1><cfelse><cfset nTFKY = 0></cfif>
                <cfif IsDefined("NewCollection.TFLA") and NewCollection.TFLA eq "on"><cfset nTFLA = 1><cfelse><cfset nTFLA = 0></cfif>
                <cfif IsDefined("NewCollection.TFMA") and NewCollection.TFMA eq "on"><cfset nTFMA = 1><cfelse><cfset nTFMA = 0></cfif>
                <cfif IsDefined("NewCollection.TFME") and NewCollection.TFME eq "on"><cfset nTFME = 1><cfelse><cfset nTFME = 0></cfif>
                <cfif IsDefined("NewCollection.TFMD") and NewCollection.TFMD eq "on"><cfset nTFMD = 1><cfelse><cfset nTFMD = 0></cfif>
                <cfif IsDefined("NewCollection.TFMI") and NewCollection.TFMI eq "on"><cfset nTFMI = 1><cfelse><cfset nTFMI = 0></cfif>
                <cfif IsDefined("NewCollection.TFMN") and NewCollection.TFMN eq "on"><cfset nTFMN = 1><cfelse><cfset nTFMN = 0></cfif>
                <cfif IsDefined("NewCollection.TFMS") and NewCollection.TFMS eq "on"><cfset nTFMS = 1><cfelse><cfset nTFMS = 0></cfif>
                <cfif IsDefined("NewCollection.TFMO") and NewCollection.TFMO eq "on"><cfset nTFMO = 1><cfelse><cfset nTFMO = 0></cfif>
                <cfif IsDefined("NewCollection.TFMT") and NewCollection.TFMT eq "on"><cfset nTFMT = 1><cfelse><cfset nTFMT = 0></cfif>
                <cfif IsDefined("NewCollection.TFNE") and NewCollection.TFNE eq "on"><cfset nTFNE = 1><cfelse><cfset nTFNE = 0></cfif>
                <cfif IsDefined("NewCollection.TFNV") and NewCollection.TFNV eq "on"><cfset nTFNV = 1><cfelse><cfset nTFNV = 0></cfif>
                <cfif IsDefined("NewCollection.TFNH") and NewCollection.TFNH eq "on"><cfset nTFNH = 1><cfelse><cfset nTFNH = 0></cfif>
                <cfif IsDefined("NewCollection.TFNJ") and NewCollection.TFNJ eq "on"><cfset nTFNJ = 1><cfelse><cfset nTFNJ = 0></cfif>
                <cfif IsDefined("NewCollection.TFNM") and NewCollection.TFNM eq "on"><cfset nTFNM = 1><cfelse><cfset nTFNM = 0></cfif>
                <cfif IsDefined("NewCollection.TFNY") and NewCollection.TFNY eq "on"><cfset nTFNY = 1><cfelse><cfset nTFNY = 0></cfif>
                <cfif IsDefined("NewCollection.TFNC") and NewCollection.TFNC eq "on"><cfset nTFNC = 1><cfelse><cfset nTFNC = 0></cfif>
                <cfif IsDefined("NewCollection.TFND") and NewCollection.TFND eq "on"><cfset nTFND = 1><cfelse><cfset nTFND = 0></cfif>
                <cfif IsDefined("NewCollection.TFOH") and NewCollection.TFOH eq "on"><cfset nTFOH = 1><cfelse><cfset nTFOH = 0></cfif>
                <cfif IsDefined("NewCollection.TFOK") and NewCollection.TFOK eq "on"><cfset nTFOK = 1><cfelse><cfset nTFOK = 0></cfif>
                <cfif IsDefined("NewCollection.TFOR") and NewCollection.TFOR eq "on"><cfset nTFOR = 1><cfelse><cfset nTFOR = 0></cfif>
                <cfif IsDefined("NewCollection.TFPA") and NewCollection.TFPA eq "on"><cfset nTFPA = 1><cfelse><cfset nTFPA = 0></cfif>
                <cfif IsDefined("NewCollection.TFRI") and NewCollection.TFRI eq "on"><cfset nTFRI = 1><cfelse><cfset nTFRI = 0></cfif>
                <cfif IsDefined("NewCollection.TFSD") and NewCollection.TFSD eq "on"><cfset nTFSD = 1><cfelse><cfset nTFSD = 0></cfif>
                <cfif IsDefined("NewCollection.TFSC") and NewCollection.TFSC eq "on"><cfset nTFSC = 1><cfelse><cfset nTFSC = 0></cfif>
                <cfif IsDefined("NewCollection.TFTN") and NewCollection.TFTN eq "on"><cfset nTFTN = 1><cfelse><cfset nTFTN = 0></cfif>
                <cfif IsDefined("NewCollection.TFTX") and NewCollection.TFTX eq "on"><cfset nTFTX = 1><cfelse><cfset nTFTX = 0></cfif>
                <cfif IsDefined("NewCollection.TFUT") and NewCollection.TFUT eq "on"><cfset nTFUT = 1><cfelse><cfset nTFUT = 0></cfif>
                <cfif IsDefined("NewCollection.TFVT") and NewCollection.TFVT eq "on"><cfset nTFVT = 1><cfelse><cfset nTFVT = 0></cfif>
                <cfif IsDefined("NewCollection.TFVA") and NewCollection.TFVA eq "on"><cfset nTFVA = 1><cfelse><cfset nTFVA = 0></cfif>
                <cfif IsDefined("NewCollection.TFWA") and NewCollection.TFWA eq "on"><cfset nTFWA = 1><cfelse><cfset nTFWA = 0></cfif>
                <cfif IsDefined("NewCollection.TFWV") and NewCollection.TFWV eq "on"><cfset nTFWV = 1><cfelse><cfset nTFWV = 0></cfif>
                <cfif IsDefined("NewCollection.TFWI") and NewCollection.TFWI eq "on"><cfset nTFWI = 1><cfelse><cfset nTFWI = 0></cfif>
                <cfif IsDefined("NewCollection.TFWY") and NewCollection.TFWY eq "on"><cfset nTFWY = 1><cfelse><cfset nTFWY = 0></cfif>
                
                <cfif IsDefined("NewCollection.TNAL") and NewCollection.TNAL eq "on"><cfset nTNAL = 1><cfelse><cfset nTNAL = 0></cfif>
                <cfif IsDefined("NewCollection.TNAK") and NewCollection.TNAK eq "on"><cfset nTNAK = 1><cfelse><cfset nTNAK = 0></cfif>
                <cfif IsDefined("NewCollection.TNAZ") and NewCollection.TNAZ eq "on"><cfset nTNAZ = 1><cfelse><cfset nTNAZ = 0></cfif>
                <cfif IsDefined("NewCollection.TNAR") and NewCollection.TNAR eq "on"><cfset nTNAR = 1><cfelse><cfset nTNAR = 0></cfif>
                <cfif IsDefined("NewCollection.TNCA") and NewCollection.TNCA eq "on"><cfset nTNCA = 1><cfelse><cfset nTNCA = 0></cfif>
                <cfif IsDefined("NewCollection.TNCO") and NewCollection.TNCO eq "on"><cfset nTNCO = 1><cfelse><cfset nTNCO = 0></cfif>
                <cfif IsDefined("NewCollection.TNCT") and NewCollection.TNCT eq "on"><cfset nTNCT = 1><cfelse><cfset nTNCT = 0></cfif>
                <cfif IsDefined("NewCollection.TNDE") and NewCollection.TNDE eq "on"><cfset nTNDE = 1><cfelse><cfset nTNDE = 0></cfif>
                <cfif IsDefined("NewCollection.TNFL") and NewCollection.TNFL eq "on"><cfset nTNFL = 1><cfelse><cfset nTNFL = 0></cfif>
                <cfif IsDefined("NewCollection.TNGA") and NewCollection.TNGA eq "on"><cfset nTNGA = 1><cfelse><cfset nTNGA = 0></cfif>
                <cfif IsDefined("NewCollection.TNHI") and NewCollection.TNHI eq "on"><cfset nTNHI = 1><cfelse><cfset nTNHI = 0></cfif>
                <cfif IsDefined("NewCollection.TNID") and NewCollection.TNID eq "on"><cfset nTNID = 1><cfelse><cfset nTNID = 0></cfif>
                <cfif IsDefined("NewCollection.TNIL") and NewCollection.TNIL eq "on"><cfset nTNIL = 1><cfelse><cfset nTNIL = 0></cfif>
                <cfif IsDefined("NewCollection.TNIN") and NewCollection.TNIN eq "on"><cfset nTNIN = 1><cfelse><cfset nTNIN = 0></cfif>
                <cfif IsDefined("NewCollection.TNIA") and NewCollection.TNIA eq "on"><cfset nTNIA = 1><cfelse><cfset nTNIA = 0></cfif>
                <cfif IsDefined("NewCollection.TNKS") and NewCollection.TNKS eq "on"><cfset nTNKS = 1><cfelse><cfset nTNKS = 0></cfif>
                <cfif IsDefined("NewCollection.TNKY") and NewCollection.TNKY eq "on"><cfset nTNKY = 1><cfelse><cfset nTNKY = 0></cfif>
                <cfif IsDefined("NewCollection.TNLA") and NewCollection.TNLA eq "on"><cfset nTNLA = 1><cfelse><cfset nTNLA = 0></cfif>
                <cfif IsDefined("NewCollection.TNMA") and NewCollection.TNMA eq "on"><cfset nTNMA = 1><cfelse><cfset nTNMA = 0></cfif>
                <cfif IsDefined("NewCollection.TNME") and NewCollection.TNME eq "on"><cfset nTNME = 1><cfelse><cfset nTNME = 0></cfif>
                <cfif IsDefined("NewCollection.TNMD") and NewCollection.TNMD eq "on"><cfset nTNMD = 1><cfelse><cfset nTNMD = 0></cfif>
                <cfif IsDefined("NewCollection.TNMI") and NewCollection.TNMI eq "on"><cfset nTNMI = 1><cfelse><cfset nTNMI = 0></cfif>
                <cfif IsDefined("NewCollection.TNMN") and NewCollection.TNMN eq "on"><cfset nTNMN = 1><cfelse><cfset nTNMN = 0></cfif>
                <cfif IsDefined("NewCollection.TNMS") and NewCollection.TNMS eq "on"><cfset nTNMS = 1><cfelse><cfset nTNMS = 0></cfif>
                <cfif IsDefined("NewCollection.TNMO") and NewCollection.TNMO eq "on"><cfset nTNMO = 1><cfelse><cfset nTNMO = 0></cfif>
                <cfif IsDefined("NewCollection.TNMT") and NewCollection.TNMT eq "on"><cfset nTNMT = 1><cfelse><cfset nTNMT = 0></cfif>
                <cfif IsDefined("NewCollection.TNNE") and NewCollection.TNNE eq "on"><cfset nTNNE = 1><cfelse><cfset nTNNE = 0></cfif>
                <cfif IsDefined("NewCollection.TNNV") and NewCollection.TNNV eq "on"><cfset nTNNV = 1><cfelse><cfset nTNNV = 0></cfif>
                <cfif IsDefined("NewCollection.TNNH") and NewCollection.TNNH eq "on"><cfset nTNNH = 1><cfelse><cfset nTNNH = 0></cfif>
                <cfif IsDefined("NewCollection.TNNJ") and NewCollection.TNNJ eq "on"><cfset nTNNJ = 1><cfelse><cfset nTNNJ = 0></cfif>
                <cfif IsDefined("NewCollection.TNNM") and NewCollection.TNNM eq "on"><cfset nTNNM = 1><cfelse><cfset nTNNM = 0></cfif>
                <cfif IsDefined("NewCollection.TNNY") and NewCollection.TNNY eq "on"><cfset nTNNY = 1><cfelse><cfset nTNNY = 0></cfif>
                <cfif IsDefined("NewCollection.TNNC") and NewCollection.TNNC eq "on"><cfset nTNNC = 1><cfelse><cfset nTNNC = 0></cfif>
                <cfif IsDefined("NewCollection.TNND") and NewCollection.TNND eq "on"><cfset nTNND = 1><cfelse><cfset nTNND = 0></cfif>
                <cfif IsDefined("NewCollection.TNOH") and NewCollection.TNOH eq "on"><cfset nTNOH = 1><cfelse><cfset nTNOH = 0></cfif>
                <cfif IsDefined("NewCollection.TNOK") and NewCollection.TNOK eq "on"><cfset nTNOK = 1><cfelse><cfset nTNOK = 0></cfif>
                <cfif IsDefined("NewCollection.TNOR") and NewCollection.TNOR eq "on"><cfset nTNOR = 1><cfelse><cfset nTNOR = 0></cfif>
                <cfif IsDefined("NewCollection.TNPA") and NewCollection.TNPA eq "on"><cfset nTNPA = 1><cfelse><cfset nTNPA = 0></cfif>
                <cfif IsDefined("NewCollection.TNRI") and NewCollection.TNRI eq "on"><cfset nTNRI = 1><cfelse><cfset nTNRI = 0></cfif>
                <cfif IsDefined("NewCollection.TNSD") and NewCollection.TNSD eq "on"><cfset nTNSD = 1><cfelse><cfset nTNSD = 0></cfif>
                <cfif IsDefined("NewCollection.TNSC") and NewCollection.TNSC eq "on"><cfset nTNSC = 1><cfelse><cfset nTNSC = 0></cfif>
                <cfif IsDefined("NewCollection.TNTN") and NewCollection.TNTN eq "on"><cfset nTNTN = 1><cfelse><cfset nTNTN = 0></cfif>
                <cfif IsDefined("NewCollection.TNTX") and NewCollection.TNTX eq "on"><cfset nTNTX = 1><cfelse><cfset nTNTX = 0></cfif>
                <cfif IsDefined("NewCollection.TNUT") and NewCollection.TNUT eq "on"><cfset nTNUT = 1><cfelse><cfset nTNUT = 0></cfif>
                <cfif IsDefined("NewCollection.TNVT") and NewCollection.TNVT eq "on"><cfset nTNVT = 1><cfelse><cfset nTNVT = 0></cfif>
                <cfif IsDefined("NewCollection.TNVA") and NewCollection.TNVA eq "on"><cfset nTNVA = 1><cfelse><cfset nTNVA = 0></cfif>
                <cfif IsDefined("NewCollection.TNWA") and NewCollection.TNWA eq "on"><cfset nTNWA = 1><cfelse><cfset nTNWA = 0></cfif>
                <cfif IsDefined("NewCollection.TNWV") and NewCollection.TNWV eq "on"><cfset nTNWV = 1><cfelse><cfset nTNWV = 0></cfif>
                <cfif IsDefined("NewCollection.TNWI") and NewCollection.TNWI eq "on"><cfset nTNWI = 1><cfelse><cfset nTNWI = 0></cfif>
                <cfif IsDefined("NewCollection.TNWY") and NewCollection.TNWY eq "on"><cfset nTNWY = 1><cfelse><cfset nTNWY = 0></cfif>
        
        <cftransaction>
           		<cftry>
				<cfset colsNameCollection=ArrayNew(1)>
				<cfset args=StructNew()/>
                <!--- ProjectInformation Table --->
				<cfset args.inpTableName="pm_projectinformation"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="projectInfoCols">	
  				<cfloop array="#projectInfoCols#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop>  
                <cfquery name="ProjectInformation" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectinformation SET
                        PROJECTID_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTID_VCH#">,
                        VERSION_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VERSION_VCH#">,
                        PLATFORM_TYPE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PLATFORM_TYPE_VCH#">,
                        PROJECT_DURATION_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_DURATION_VCH#">,
                        SUBMISSION_DATE_DT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUBMISSION_DATE_DT#">,
                        PROJECT_START_DATE_DT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_START_DATE_DT#">,
                        PROJECT_END_DATE_DT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_END_DATE_DT#">,
                        PREPARED_BY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PREPARED_BY_VCH#">,
                        EMAIL_PHONE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAIL_PHONE_VCH#">,
                        PROJECTLEADCONTACT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTLEADCONTACT_VCH#">,
                        SCRIPT_CONTENT_CONTACT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPT_CONTENT_CONTACT_VCH#">,
                        DATA_FEED_CONTACT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_FEED_CONTACT_VCH#">,
                        MANDP_CONTACT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MANDP_CONTACT_VCH#">,
                        PROJECT_TEAM_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECT_TEAM_VCH#">,
                        COMMUNICATIONS_ALERT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COMMUNICATIONS_ALERT_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Information update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProgramOverview Table --->
				<cfset args.inpTableName="pm_ProgramOverview"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddProgramOverview" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_programoverview SET
                        PROJECTDESC_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROJECTDESC_VCH#">,
                        TYPE_OF_CALL_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TYPE_OF_CALL_VCH#">,
                        PRECALL_TO_EMAIL_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PRECALL_TO_EMAIL_INT#">,
                        STANDALONE_CALL_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.STANDALONE_CALL_INT#">,
                        PRECALL_TO_DIRECT_MAIL_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PRECALL_TO_DIRECT_MAIL_INT#">,
                        POSTCALL_TO_DIRECT_MAIL_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.POSTCALL_TO_DIRECT_MAIL_INT#">,
                        DRIVE_TO_WEB_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DRIVE_TO_WEB_INT#">,
                        INFORMATIONAL_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.INFORMATIONAL_INT#">,
                        OTHER_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.OTHER_INT#">,
                        OTHER_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OTHER_VCH#">,
                        PLATFORM_ATT_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_ATT_INT#">,
                        PLATFORM_MB_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_MB_INT#">,
                        PLATFORM_EGS_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.PLATFORM_EGS_INT#">,
                        CUSTOMER_BASE_CON_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CUSTOMER_BASE_CON_INT#">,
                        CUSTOMER_BASE_BUS_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CUSTOMER_BASE_BUS_INT#">,
                        AL = <cfqueryparam cfsqltype="cf_sql_integer" value="#nAL#">,
                        AK = <cfqueryparam cfsqltype="cf_sql_integer" value="#nAK#">,
                        AZ = <cfqueryparam cfsqltype="cf_sql_integer" value="#nAZ#">,
                        AR = <cfqueryparam cfsqltype="cf_sql_integer" value="#nAR#">,
                        CA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nCA#">,
                        CO = <cfqueryparam cfsqltype="cf_sql_integer" value="#nCO#">,
                        CT = <cfqueryparam cfsqltype="cf_sql_integer" value="#nCT#">,
                        DE = <cfqueryparam cfsqltype="cf_sql_integer" value="#nDE#">,
                        FL = <cfqueryparam cfsqltype="cf_sql_integer" value="#nFL#">,
                        GA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nGA#">,
                        HI = <cfqueryparam cfsqltype="cf_sql_integer" value="#nHI#">,
                        ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#nID#">,
                        IL = <cfqueryparam cfsqltype="cf_sql_integer" value="#nIL#">,
                        nIN = <cfqueryparam cfsqltype="cf_sql_integer" value="#nIN#">,
                        IA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nIA#">,
                        KS = <cfqueryparam cfsqltype="cf_sql_integer" value="#nKS#">,
                        KY = <cfqueryparam cfsqltype="cf_sql_integer" value="#nKY#">,
                        LA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nLA#">,
                        MA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMA#">,
                        MD = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMD#">,
                        ME = <cfqueryparam cfsqltype="cf_sql_integer" value="#nME#">,
                        MI = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMI#">,
                        MN = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMN#">,
                        MS = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMS#">,
                        MO = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMO#">,
                        MT = <cfqueryparam cfsqltype="cf_sql_integer" value="#nMT#">,
                        NE = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNE#">,
                        NV = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNV#">,
                        NH = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNH#">,
                        NJ = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNJ#">,
                        NM = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNM#">,
                        NY = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNY#">,
                        NC = <cfqueryparam cfsqltype="cf_sql_integer" value="#nNC#">,
                        ND = <cfqueryparam cfsqltype="cf_sql_integer" value="#nND#">,
                        OH = <cfqueryparam cfsqltype="cf_sql_integer" value="#nOH#">,
                        OK = <cfqueryparam cfsqltype="cf_sql_integer" value="#nOK#">,
                        zOR = <cfqueryparam cfsqltype="cf_sql_integer" value="#nOR#">,
                        PA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nPA#">,
                        RI = <cfqueryparam cfsqltype="cf_sql_integer" value="#nRI#">,
                        SD = <cfqueryparam cfsqltype="cf_sql_integer" value="#nSD#">,
                        SC = <cfqueryparam cfsqltype="cf_sql_integer" value="#nSC#">,
                        TN = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTN#">,
                        TX = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTX#">,
                        UT = <cfqueryparam cfsqltype="cf_sql_integer" value="#nUT#">,
                        VT = <cfqueryparam cfsqltype="cf_sql_integer" value="#nVT#">,
                        VA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nVA#">,
                        WA = <cfqueryparam cfsqltype="cf_sql_integer" value="#nWA#">,
                        WV = <cfqueryparam cfsqltype="cf_sql_integer" value="#nWV#">,
                        WI = <cfqueryparam cfsqltype="cf_sql_integer" value="#nWI#">,
                        WY = <cfqueryparam cfsqltype="cf_sql_integer" value="#nWY#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Overview Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProgramComplexity Table --->
				<cfset args.inpTableName="pm_ProgramComplexity"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    					<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 					
                <cfquery name="AddProgramComplexity" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_programcomplexity SET
                        PROGRAM_COMPLEXITY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PROGRAM_COMPLEXITY_VCH#">,
                        MONITORING_THRESHOLD_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.MONITORING_THRESHOLD_INT#">,
                        MONDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MONDAY_VCH#">,
                        TUESDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TUESDAY_VCH#">,
                        WEDNESDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WEDNESDAY_VCH#">,
                        THURSDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.THURSDAY_VCH#">,
                        FRIDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FRIDAY_VCH#">,
                        SATURDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SATURDAY_VCH#">,
                        SUNDAY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUNDAY_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Complexity Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectSettings Table --->
				<cfset args.inpTableName="pm_ProjectSettings"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddProjectSettings" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectsettings SET
                        MFDIALINGHOURS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MFDIALINGHOURS_VCH#">,
                        SATURDAYDIALINGHOURS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SATURDAYDIALINGHOURS_VCH#">,
                        SUNDAYDIALINGHOURS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SUNDAYDIALINGHOURS_VCH#">,
                        HOLIDAYDIALINGHOURS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HOLIDAYDIALINGHOURS_VCH#">,
                        DAILYCALLVOLUMES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DAILYCALLVOLUMES_VCH#">,
                        CONSUMERCALLERID_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CONSUMERCALLERID_VCH#">,
                        BUSINESSCALLERID_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BUSINESSCALLERID_VCH#">,
                        SPANISHOPTION_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SPANISHOPTION_VCH#">,
                        ALTERNATENUMBERDIALING_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALTERNATENUMBERDIALING_VCH#">,
                        PHONENUMTOBEDIALED_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PHONENUMTOBEDIALED_VCH#">,
                        REDIALLOGIC_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.REDIALLOGIC_VCH#">,
                        REDIALATTEMPTS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.REDIALATTEMPTS_VCH#">,
                        ROLLOVERDIALING_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ROLLOVERDIALING_VCH#">,
                        ANSWERMACHINESTRATEGY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ANSWERMACHINESTRATEGY_VCH#">,
                        MAXATTEMPTS_INT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MAXATTEMPTS_INT#">,
                        RINGCOUNT_INT = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RINGCOUNT_INT#">,
                        ANSWERMACHINEWAIT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ANSWERMACHINEWAIT_VCH#">,
                        BUSYCALLBACKTIME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BUSYCALLBACKTIME_VCH#">,
                        BCBTIMEREDIALTIME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BCBTIMEREDIALTIME_VCH#">,
                        NOANSWERCALLBACKTIME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NOANSWERCALLBACKTIME_VCH#">,
                        NACBTIMEREDIALTIME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NACBTIMEREDIALTIME_VCH#">,
                        OTHERFAX_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OTHERFAX_VCH#">,
                        OFTIMEREDIALTIME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OFTIMEREDIALTIME_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Settings Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectScriptInfo Table  - Contains Voice Scripts --->
				<cfset args.inpTableName="pm_ProjectScriptInfo"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 					
                <cfquery name="AddProjectScriptInfo" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectscriptinfo SET
                        SCRIPT_INFORMATION_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPT_INFORMATION_VCH#">,
                        VOICE_TALENT_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VOICE_TALENT_VCH#">,
                        BENEFITS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BENEFITS_VCH#">,
                        PHRASES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PHRASES_VCH#">,
                        TONALITY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TONALITY_VCH#">,
                        NOTES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NOTES_VCH#">,
                        CALLCENTERSALES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALLCENTERSALES_VCH#">,
                        INBOUNDCALLSMETHOD_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBOUNDCALLSMETHOD_VCH#">,
                        SCRIPTTOAPPROVE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTTOAPPROVE_VCH#">,
                        SCRIPTAPPROVEDBY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTAPPROVEDBY_VCH#">,
                        SCRIPTAPPROVEDDATE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCRIPTAPPROVEDDATE_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Script Info Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectEmailScripts Table  - Contains Email Scripts --->
				<cfset args.inpTableName="pm_ProjectEmailScripts"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddProjectEmailScripts" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectemailscripts SET
                        EMAILPROJECTNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILPROJECTNAME_VCH#">,
                        EMAILFROMLINE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILFROMLINE_VCH#">,
                        EMAILSUBJECTLINE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILSUBJECTLINE_VCH#">,
                        EMAILTAGCODE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILTAGCODE_VCH#">,
                        EMAILDELIVERYATTEMPTS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILDELIVERYATTEMPTS_VCH#">,
                        BOUNCEBACKSTRATEGY_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.BOUNCEBACKSTRATEGY_VCH#">,
                        ORIGINATINGFILENAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORIGINATINGFILENAME_VCH#">,
                        EXPORTDATEFEED_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EXPORTDATEFEED_VCH#">,
                        DATAFEEDNOTES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATAFEEDNOTES_VCH#">,
                        EMAILHTMLTEMPLATE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILHTMLTEMPLATE_VCH#">,
                        EMAILTEXTTEMPLATE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.EMAILTEXTTEMPLATE_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Email Scripts Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectDataFeeds Table --->
				<cfset args.inpTableName="pm_ProjectDataFeeds"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 					
                <cfquery name="AddProjectDataFeeds" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectdatafeeds SET
                        DATA_REQUIREMENTS_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_REQUIREMENTS_VCH#">,
                        IT_INTERFACE_SIGNOFF_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IT_INTERFACE_SIGNOFF_VCH#">,
                        CALL_RESULTS_PROCESSING_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALL_RESULTS_PROCESSING_VCH#">,
                        CALL_YES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CALL_YES_VCH#">,
                        DATA_EXCHANGE_DEVELOP_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_EXCHANGE_DEVELOP_VCH#">,
                        DATA_SOURCE_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_SOURCE_VCH#">,
                        DO_NOT_SCRUB_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DO_NOT_SCRUB_INT#">,
                        STATE_DNC_LIST_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.STATE_DNC_LIST_INT#">,
                        CLIENT_DNC_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CLIENT_DNC_INT#">,
                        DO_NOT_DIAL_STATES_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.DO_NOT_DIAL_STATES_INT#">,
                        NATIONAL_DNC_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.NATIONAL_DNC_INT#">,
                        COMPANY_INTERNAL_LIST_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.COMPANY_INTERNAL_LIST_INT#">,
                        CELL_PHONE_LIST_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.CELL_PHONE_LIST_INT#">,
                        DATA_FILE_TRANSMISSION_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DATA_FILE_TRANSMISSION_VCH#">,
                        FILTER_DUPLICATE_RECORDS_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_DUPLICATE_RECORDS_INT#">,
                        FILTER_WITHIN_FILE_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_WITHIN_FILE_INT#">,
                        FILTER_SAME_DAY_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_SAME_DAY_INT#">,
                        FILTER_X_AMOUNT_DAYS_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.FILTER_X_AMOUNT_DAYS_INT#">,
                        X_DAYS_NUMBER_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#NewCollection.X_DAYS_NUMBER_INT#">,
                        PDFNOTES_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PDFNOTES_VCH#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Data Feeds Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectReportInfo Table --->
				<cfset args.inpTableName="pm_ProjectReportInfo"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddProjectReportInfo" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectreportinfo SET
                    	Project_Reporting_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.Project_Reporting_vch#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                <cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Report Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                <!--- ProjectDisclosures Table --->
				<cfset args.inpTableName="pm_ProjectDisclosures"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddProjectDisclosures" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_projectdisclosures SET
                    Project_Disclosures_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.Project_Disclosures_vch#">
                    WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Disclosure Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                
                <!--- ScriptsTollFree Table --->
				<cfset args.inpTableName="pm_ScriptsTollFree"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 				
                <cfquery name="AddScriptsTollFree" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_scriptstollfree SET
                        
                        ALTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFAL#">,
                        AKTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFAK#">,
                        AZTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFAZ#">,
                        ARTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFAR#">,
                        CATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFCA#">,
                        COTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFCO#">,
                        CTTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFCT#">,
                        DETF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFDE#">,
                        FLTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFFL#">,
                        GATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFGA#">,
                        HITF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFHI#">,
                        IDTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFID#">,
                        ILTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFIL#">,
                        INTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFIN#">,
                        IATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFIA#">,
                        KSTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFKS#">,
                        KYTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFKY#">,
                        LATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFLA#">,
                        MATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMA#">,
                        MDTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMD#">,
                        METF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFME#">,
                        MITF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMI#">,
                        MNTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMN#">,
                        MSTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMS#">,
                        MOTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMO#">,
                        MTTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFMT#">,
                        NETF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNE#">,
                        NVTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNV#">,
                        NHTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNH#">,
                        NJTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNJ#">,
                        NMTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNM#">,
                        NYTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNY#">,
                        NCTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFNC#">,
                        NDTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFND#">,
                        OHTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFOH#">,
                        OKTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFOK#">,
                        ORTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFOR#">,
                        PATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFPA#">,
                        RITF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFRI#">,
                        SDTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFSD#">,
                        SCTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFSC#">,
                        TNTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFTN#">,
                        TXTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFTX#">,
                        UTTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFUT#">,
                        VTTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFVT#">,
                        VATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFVA#">,
                        WATF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFWA#">,
                        WVTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFWV#">,
                        WITF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFWI#">,
                        WYTF_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTFWY#">,
                        ALCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALCTF_VCH#">,
                        AKCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKCTF_VCH#">,
                        AZCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZCTF_VCH#">,
                        ARCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARCTF_VCH#">,
                        CACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CACTF_VCH#">,
                        COCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COCTF_VCH#">,
                        CTCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTCTF_VCH#">,
                        DECTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DECTF_VCH#">,
                        FLCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLCTF_VCH#">,
                        GACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GACTF_VCH#">,
                        HICTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HICTF_VCH#">,
                        IDCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDCTF_VCH#">,
                        ILCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILCTF_VCH#">,
                        INCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INCTF_VCH#">,
                        IACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IACTF_VCH#">,
                        KSCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSCTF_VCH#">,
                        KYCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYCTF_VCH#">,
                        LACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LACTF_VCH#">,
                        MACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MACTF_VCH#">,
                        MDCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDCTF_VCH#">,
                        MECTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MECTF_VCH#">,
                        MICTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MICTF_VCH#">,
                        MNCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNCTF_VCH#">,
                        MSCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSCTF_VCH#">,
                        MOCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOCTF_VCH#">,
                        MTCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTCTF_VCH#">,
                        NECTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NECTF_VCH#">,
                        NVCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVCTF_VCH#">,
                        NHCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHCTF_VCH#">,
                        NJCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJCTF_VCH#">,
                        NMCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMCTF_VCH#">,
                        NYCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYCTF_VCH#">,
                        NCCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCCTF_VCH#">,
                        NDCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDCTF_VCH#">,
                        OHCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHCTF_VCH#">,
                        OKCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKCTF_VCH#">,
                        ORCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORCTF_VCH#">,
                        PACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PACTF_VCH#">,
                        RICTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RICTF_VCH#">,
                        SDCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDCTF_VCH#">,
                        SCCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCCTF_VCH#">,
                        TNCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNCTF_VCH#">,
                        TXCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXCTF_VCH#">,
                        UTCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTCTF_VCH#">,
                        VTCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTCTF_VCH#">,
                        VACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VACTF_VCH#">,
                        WACTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WACTF_VCH#">,
                        WVCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVCTF_VCH#">,
                        WICTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WICTF_VCH#">,
                        WYCTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYCTF_VCH#">,
                        ALBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALBTF_VCH#">,
                        AKBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKBTF_VCH#">,
                        AZBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZBTF_VCH#">,
                        ARBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARBTF_VCH#">,
                        CABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CABTF_VCH#">,
                        COBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COBTF_VCH#">,
                        CTBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTBTF_VCH#">,
                        DEBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DEBTF_VCH#">,
                        FLBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLBTF_VCH#">,
                        GABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GABTF_VCH#">,
                        HIBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HIBTF_VCH#">,
                        IDBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDBTF_VCH#">,
                        ILBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILBTF_VCH#">,
                        INBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBTF_VCH#">,
                        IABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IABTF_VCH#">,
                        KSBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSBTF_VCH#">,
                        KYBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYBTF_VCH#">,
                        LABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LABTF_VCH#">,
                        MABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MABTF_VCH#">,
                        MDBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDBTF_VCH#">,
                        MEBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MEBTF_VCH#">,
                        MIBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MIBTF_VCH#">,
                        MNBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNBTF_VCH#">,
                        MSBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSBTF_VCH#">,
                        MOBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOBTF_VCH#">,
                        MTBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTBTF_VCH#">,
                        NEBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NEBTF_VCH#">,
                        NVBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVBTF_VCH#">,
                        NHBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHBTF_VCH#">,
                        NJBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJBTF_VCH#">,
                        NMBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMBTF_VCH#">,
                        NYBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYBTF_VCH#">,
                        NCBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCBTF_VCH#">,
                        NDBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDBTF_VCH#">,
                        OHBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHBTF_VCH#">,
                        OKBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKBTF_VCH#">,
                        ORBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORBTF_VCH#">,
                        PABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PABTF_VCH#">,
                        RIBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RIBTF_VCH#">,
                        SDBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDBTF_VCH#">,
                        SCBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCBTF_VCH#">,
                        TNBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNBTF_VCH#">,
                        TXBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXBTF_VCH#">,
                        UTBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTBTF_VCH#">,
                        VTBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTBTF_VCH#">,
                        VABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VABTF_VCH#">,
                        WABTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WABTF_VCH#">,
                        WVBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVBTF_VCH#">,
                        WIBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WIBTF_VCH#">,
                        WYBTF_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYBTF_VCH#">
 					WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Toll Free Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>
                <cftry>
                
                
                
                
                
                
                
                
                <!--- ScriptsTransfer Table --->
				<cfset args.inpTableName="pm_ScriptsTransfer"/>
				<cfinvoke method="getColumnNameCollection" argumentcollection="#args#" returnvariable="colsNameCollection">	
  				<cfloop array="#colsNameCollection#" index="colName">
    				<cfif NOT StructKeyExists(NewCollection,"#colName#")>
						<cfset varName="NewCollection." & "#colName#">
						<cfparam name="#varName#" default="0">
					</cfif>   
				</cfloop> 					
                <cfquery name="AddScriptsTransfer" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.pm_scriptstransfer SET
                        
                        AL_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNAL#">,
                        AK_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNAK#">,
                        AZ_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNAZ#">,
                        AR_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNAR#">,
                        CA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNCA#">,
                        CO_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNCO#">,
                        CT_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNCT#">,
                        DE_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNDE#">,
                        FL_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNFL#">,
                        GA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNGA#">,
                        HI_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNHI#">,
                        ID_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNID#">,
                        IL_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNIL#">,
                        IN_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNIN#">,
                        IA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNIA#">,
                        KS_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNKS#">,
                        KY_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNKY#">,
                        LA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNLA#">,
                        MA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMA#">,
                        MD_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMD#">,
                        ME_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNME#">,
                        MI_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMI#">,
                        MN_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMN#">,
                        MS_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMS#">,
                        MO_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMO#">,
                        MT_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNMT#">,
                        NE_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNE#">,
                        NV_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNV#">,
                        NH_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNH#">,
                        NJ_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNJ#">,
                        NM_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNM#">,
                        NY_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNY#">,
                        NC_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNNC#">,
                        ND_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNND#">,
                        OH_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNOH#">,
                        OK_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNOK#">,
                        OR_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNOR#">,
                        PA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNPA#">,
                        RI_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNRI#">,
                        SD_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNSD#">,
                        SC_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNSC#">,
                        TN_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNTN#">,
                        TX_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNTX#">,
                        UT_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNUT#">,
                        VT_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNVT#">,
                        VA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNVA#">,
                        WA_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNWA#">,
                        WV_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNWV#">,
                        WI_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNWI#">,
                        WY_vch = <cfqueryparam cfsqltype="cf_sql_integer" value="#nTNWY#">,
                        ALCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALCTN_VCH#">,
                        AKCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKCTN_VCH#">,
                        AZCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZCTN_VCH#">,
                        ARCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARCTN_VCH#">,
                        CACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CACTN_VCH#">,
                        COCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COCTN_VCH#">,
                        CTCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTCTN_VCH#">,
                        DECTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DECTN_VCH#">,
                        FLCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLCTN_VCH#">,
                        GACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GACTN_VCH#">,
                        HICTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HICTN_VCH#">,
                        IDCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDCTN_VCH#">,
                        ILCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILCTN_VCH#">,
                        INCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INCTN_VCH#">,
                        IACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IACTN_VCH#">,
                        KSCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSCTN_VCH#">,
                        KYCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYCTN_VCH#">,
                        LACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LACTN_VCH#">,
                        MACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MACTN_VCH#">,
                        MDCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDCTN_VCH#">,
                        MECTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MECTN_VCH#">,
                        MICTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MICTN_VCH#">,
                        MNCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNCTN_VCH#">,
                        MSCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSCTN_VCH#">,
                        MOCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOCTN_VCH#">,
                        MTCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTCTN_VCH#">,
                        NECTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NECTN_VCH#">,
                        NVCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVCTN_VCH#">,
                        NHCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHCTN_VCH#">,
                        NJCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJCTN_VCH#">,
                        NMCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMCTN_VCH#">,
                        NYCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYCTN_VCH#">,
                        NCCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCCTN_VCH#">,
                        NDCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDCTN_VCH#">,
                        OHCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHCTN_VCH#">,
                        OKCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKCTN_VCH#">,
                        ORCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORCTN_VCH#">,
                        PACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PACTN_VCH#">,
                        RICTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RICTN_VCH#">,
                        SDCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDCTN_VCH#">,
                        SCCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCCTN_VCH#">,
                        TNCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNCTN_VCH#">,
                        TXCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXCTN_VCH#">,
                        UTCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTCTN_VCH#">,
                        VTCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTCTN_VCH#">,
                        VACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VACTN_VCH#">,
                        WACTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WACTN_VCH#">,
                        WVCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVCTN_VCH#">,
                        WICTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WICTN_VCH#">,
                        WYCTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYCTN_VCH#">,
                        ALBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ALBTN_VCH#">,
                        AKBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AKBTN_VCH#">,
                        AZBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.AZBTN_VCH#">,
                        ARBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ARBTN_VCH#">,
                        CABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CABTN_VCH#">,
                        COBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.COBTN_VCH#">,
                        CTBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.CTBTN_VCH#">,
                        DEBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.DEBTN_VCH#">,
                        FLBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.FLBTN_VCH#">,
                        GABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.GABTN_VCH#">,
                        HIBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.HIBTN_VCH#">,
                        IDBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IDBTN_VCH#">,
                        ILBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ILBTN_VCH#">,
                        INBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.INBTN_VCH#">,
                        IABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.IABTN_VCH#">,
                        KSBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KSBTN_VCH#">,
                        KYBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.KYBTN_VCH#">,
                        LABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.LABTN_VCH#">,
                        MABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MABTN_VCH#">,
                        MDBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MDBTN_VCH#">,
                        MEBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MEBTN_VCH#">,
                        MIBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MIBTN_VCH#">,
                        MNBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MNBTN_VCH#">,
                        MSBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MSBTN_VCH#">,
                        MOBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MOBTN_VCH#">,
                        MTBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.MTBTN_VCH#">,
                        NEBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NEBTN_VCH#">,
                        NVBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NVBTN_VCH#">,
                        NHBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NHBTN_VCH#">,
                        NJBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NJBTN_VCH#">,
                        NMBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NMBTN_VCH#">,
                        NYBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NYBTN_VCH#">,
                        NCBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NCBTN_VCH#">,
                        NDBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.NDBTN_VCH#">,
                        OHBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OHBTN_VCH#">,
                        OKBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.OKBTN_VCH#">,
                        ORBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.ORBTN_VCH#">,
                        PABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.PABTN_VCH#">,
                        RIBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.RIBTN_VCH#">,
                        SDBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SDBTN_VCH#">,
                        SCBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.SCBTN_VCH#">,
                        TNBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TNBTN_VCH#">,
                        TXBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.TXBTN_VCH#">,
                        UTBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.UTBTN_VCH#">,
                        VTBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VTBTN_VCH#">,
                        VABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.VABTN_VCH#">,
                        WABTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WABTN_VCH#">,
                        WVBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WVBTN_VCH#">,
                        WIBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WIBTN_VCH#">,
                        WYBTN_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NewCollection.WYBTN_VCH#">
 					WHERE
                    	BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#NewCollection.BATCHID#">
                </cfquery>
                	<cfcatch type="any">
                		<cfdump var="#cfcatch#"><!---<cfmail to="jpeterson@messagebroadcast.com" from="it@messagebroadcast.com" subject="Project Transfer Update" type="html">
							<cfdump var="#cfcatch#">
                        </cfmail>--->
                    </cfcatch>
                </cftry>

           </cftransaction> 
        
        
        
    	<cfset results = "Project Data Updated">
    
    	<cfreturn results>
	</cffunction>

	<cffunction name="getColumnNameCollection" access="private" hint="Get all column names of a table">
		<cfargument name="inpTableName" type="string" required="yes">
		<cfset var colsName=ArrayNew(1)/>
		<cftry>
			<cfquery name="getColumnNames" datasource="#Session.DBSourceEBM#">
				SELECT
					UCASE(column_name) as column_name
	            FROM
	                information_schema.columns
	            WHERE
	                table_name=<CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#inpTableName#">
				AND
					column_name != <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="BATCHID_BI">
			</cfquery>
			
			<cfloop query="getColumnNames" >
				<cfset ArrayAppend(colsName,"#getColumnNames.column_name#")/>
			</cfloop>
			<cfcatch type="any">
				<cfdump var="#cfcatch#">
			</cfcatch>
		</cftry>
		<cfreturn colsName>
	</cffunction>
    
    <cffunction name="AddMCDERow" access="remote" returntype="string">
    	<cfargument name="BatchId_bi" type="string" required="yes">
		<cfargument name="element" type="string" required="yes">
        <cfargument name="elementdesc" type="string" required="yes">
        <cftry>
        	<cfquery name="AddMainDataElement" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_maindataelements
                (
                CompanyId_int,
                BatchId_bi,
                Element_vch,
                ElementDesc_vch
                )
                Values
                (
                #session.userid#,
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BatchId_bi#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.element#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.elementdesc#" maxlength="500">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "New Data Element Added">
        </cfif>

		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpMCDERow" access="remote" returntype="string">
    	<cfargument name="maindataelementsid_bi" type="string" required="yes">
		<cfargument name="element" type="string" required="yes">
        <cfargument name="elementdesc" type="string" required="yes">
        <cfargument name="upelement" type="string" required="no">
        
        
        	<cfquery name="UpMainDataElement" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_maindataelements SET
                Element_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.element#" maxlength="500">,
                ElementDesc_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.elementdesc#" maxlength="500">
                WHERE maindataelementsid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.maindataelementsid_bi#">
            </cfquery>
        
		<cfset results = "Element Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddMCCRCRow" access="remote" returntype="string">
    	<cfargument name="BatchId_bi" type="string" required="yes">
		<cfargument name="code_vch" type="string" required="yes">
        <cfargument name="crc_vch" type="string" required="yes">
        <cfargument name="definition_vch" type="string" required="yes">
        
        
        <cftry>
        	<cfquery name="AddMainCRC" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_maincrc
                (
                CompanyId_int,
                BatchId_bi,
                Code_vch,
                CRC_vch,
                Definition_vch
                )
                Values
                (
                #session.userid#,
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BatchId_bi#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.code_vch#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.crc_vch#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.definition_vch#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "New Data CRC Added">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpMCCRCRow" access="remote" returntype="string">
    	<cfargument name="maincrcid_bi" type="string" required="yes">
		<cfargument name="code_vch" type="string" required="yes">
        <cfargument name="crc_vch" type="string" required="yes">
        <cfargument name="definition_vch" type="string" required="yes">
        
        
        <cftry>
        	<cfquery name="UpMainCRC" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_maincrc SET
                Code_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.code_vch#" maxlength="500">,
                CRC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.crc_vch#" maxlength="500">,
                Definition_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.definition_vch#" maxlength="2000">
                WHERE maincrcid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.maincrcid_bi#">
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Data CRC Updated">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddPARow" access="remote" returntype="string">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="approvename" type="string" required="yes" default="defaultName">
        <cfargument name="approvegroup" type="string" required="yes" default="defaultGtoup">
        <cfargument name="dapprove" type="string" required="yes">
		
		<cftry>
        	<cfquery name="AddApproval" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_approval
                (
                BatchId_bi,
                ApprovalName_vch,
                ApprovalGroup_vch,
                ApprovalDate_dt,
                ActualApproved_dt
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.approvename#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.approvegroup#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.dapprove#">,
                NOW()
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Project Approval Submitted">
        </cfif>

		<cfreturn results>
	</cffunction>

	<cffunction name="AddDFRow" access="remote" returntype="string" description="add data field row">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="DATAFIELDNAME_VCH" type="string" required="yes">
        <cfargument name="DATAFIELDDESC_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_datafields
                (
                BatchId_bi,
                DATAFIELDNAME_VCH,
                DATAFIELDDESC_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.DATAFIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.DATAFIELDDESC_VCH#" maxlength="1000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Data Fields">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpDFRow" access="remote" returntype="string" description="update data fields row">
    	<cfargument name="DATAFIELDID_BI" type="string" required="yes">
		<cfargument name="DATAFIELDNAME_VCH" type="string" required="yes">
        <cfargument name="DATAFIELDDESC_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_datafields SET
                DATAFIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.DATAFIELDNAME_VCH#" maxlength="500">,
                DATAFIELDDESC_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.DATAFIELDDESC_VCH#" maxlength="1000">
                WHERE DataFieldID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.DATAFIELDID_BI#">
            </cfquery>
        
		<cfset results = "Data Field Updated">
		<cfreturn results>
	</cffunction>
    
    
    <cffunction name="AddEHRow" access="remote" returntype="string" description="Email Headers">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_emailheader
                (
                BatchId_bi,
                FIELDNAME_VCH,
                ROUTESTO_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Email Header">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddEBRow" access="remote" returntype="string" description="Email Body">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_emailbody
                (
                 BatchId_bi,
                FIELDNAME_VCH,
                ROUTESTO_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Email Body">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddEFRow" access="remote" returntype="string" description="Email Footer">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_emailfooter
                (
                 BatchId_bi,
                FIELDNAME_VCH,
                ROUTESTO_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Email Footer">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddEORow" access="remote" returntype="string" description="Email Online Support">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_emailonlinesupport
                (
                 BatchId_bi,
                FIELDNAME_VCH,
                ROUTESTO_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Email Online">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="AddERRow" access="remote" returntype="string" description="Email Right Side">
    	<cfargument name="BATCHID" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        <cftry>
        	<cfquery name="AddFDRowN" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                simpleobjects.pm_emailrightside
                (
                 BatchId_bi,
                FIELDNAME_VCH,
                ROUTESTO_VCH
                )
                Values
                (
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.BATCHID#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                )
            </cfquery>
            <cfcatch type="any">
            	<cfset results = cfcatch.detail>
            </cfcatch>
        </cftry>
        <cfif IsDefined("results")>
			<cfset results = results>
        <cfelse>
        	<cfset results = "Email Right">
        </cfif>
        
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpEHDRow" access="remote" returntype="string" description="update Email Header Data row">
    	<cfargument name="EMAILHEADERID_BI" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_emailheader SET
                FIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                ROUTESTO_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                WHERE EmailHeaderID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.EMAILHEADERID_BI#">
            </cfquery>
        
		<cfset results = "Email Header Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpEFDRow" access="remote" returntype="string" description="update Email Footer Data row">
    	<cfargument name="EMAILFOOTERID_BI" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_emailfooter SET
                FIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                ROUTESTO_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                WHERE EmailFooterID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.EMAILFOOTERID_BI#">
            </cfquery>
        
		<cfset results = "Email Footer Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpEBDRow" access="remote" returntype="string" description="update Email Body Data row">
    	<cfargument name="EMAILBODYID_BI" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_emailbody SET
                FIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                ROUTESTO_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                WHERE EmailBodyID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.EMAILBODYID_BI#">
            </cfquery>
        
		<cfset results = "Email Body Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpEOSDRow" access="remote" returntype="string" description="update Email Online Support Data row">
    	<cfargument name="EMAILONLINESUPPORTID_BI" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_emailonlinesupport SET
                FIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                ROUTESTO_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                WHERE EmailOnlineSupportID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.EMAILONLINESUPPORTID_BI#">
            </cfquery>
        
		<cfset results = "Email Online Support Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="UpERDRow" access="remote" returntype="string" description="update Email Right Side Data row">
    	<cfargument name="EMAILRIGHTSIDEID_BI" type="string" required="yes">
		<cfargument name="FIELDNAME_VCH" type="string" required="yes">
        <cfargument name="ROUTESTO_VCH" type="string" required="yes">
        
        	<cfquery name="UpDF" datasource="#Session.DBSourceEBM#">
                UPDATE simpleobjects.pm_emailrightside SET
                FIELDNAME_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.FIELDNAME_VCH#" maxlength="500">,
                ROUTESTO_VCH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ROUTESTO_VCH#" maxlength="2000">
                WHERE EmailRightSideID_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.EMAILRIGHTSIDEID_BI#">
            </cfquery>
        
		<cfset results = "Email Right Side Updated">
		<cfreturn results>
	</cffunction>
    
    <cffunction name="getApprovalInfo" access="remote" returntype="any" description="Get Approval information for campaigns">
    	<cfargument name="BATCHID" type="string" required="yes">
    	
        <cfquery name="getapprovals" datasource="#Session.DBSourceEBM#">
        SELECT *
        FROM simpleobjects.pm_approval
        WHERE BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#BATCHID#">
        </cfquery>
        
    	<cfreturn getapprovals>
    </cffunction>
</cfcomponent>