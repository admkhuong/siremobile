<cfcomponent output="false">
    <cffunction name="getTaskList" access="remote" hint="Check the user right" output="true">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">
		<cfargument name="inpTableName" TYPE="string" required="yes">
		<cfset var taskListArray=ArrayNew(1)>
		<cfset var taskListRequiredArray=ArrayNew(1)>
		<cfset var dataout = '0' /> 
		<cfset result=StructNew()>
		
		
		<cftry>
		<cfquery name = "getTaskListQuery" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		FieldName_vch,required_int
		    FROM 
		    		simpleobjects.tasklist
		    WHERE
		    		UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		    AND
		    		tableName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">
		</cfquery>		
		
		<cfloop query="getTaskListQuery">
			<cfset ArrayAppend(taskListArray,#getTaskListQuery.FieldName_vch#)>
			<cfset ArrayAppend(taskListRequiredArray,#getTaskListQuery.required_int#)>
		</cfloop>
		<cfset requiredList=ArrayToList(taskListRequiredArray,",")>
		<cfset taskListList = ArrayToList(taskListArray,",")>
			<cfloop list="#taskListList#" delimiters="," index="idx">
				<cfset requiredValue=taskListRequiredArray.elementAt(taskListArray.indexOf("#idx#"))>
				<cfset StructInsert(result,#idx#,#requiredValue#)> 
			</cfloop>
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />           
				<cfreturn dataout/>
			</cfcatch>
		</cftry>

			<cfreturn result/>
		
	</cffunction>

	<cffunction name="getTableData" access="remote" output="false" hint="get all data from a table">
		<cfargument name="inpTableName" required="true" default="pm_projectinformation">
		<cfargument name="inpBatchId" required="true" default="0">
		<cfargument name="inpFieldList" require="false" default="">
		<cfset var LOCALOUTPUT = {} />
		<cfset dataout="">
		<cfset selectString= "">

		<cfloop list="#inpFieldList#" delimiters="," index="fieldName">
			<cfset selectString=selectString & fieldName & ",">
		</cfloop>
		<cfset selectString=Left(selectString,len(selectString)-1)>
		<cfquery name = "getData" datasource="#Session.DBSourceEBM#"> 
		    SELECT 
		    		<cfoutput>#selectString#</cfoutput>
		    FROM 
		    		simpleobjects.<cfoutput>#inptablename#</cfoutput>
		    WHERE
		    		BatchId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
		</cfquery>
		<cfif getData.recordCount GT 0>
				<cfset LOCALOUTPUT.rows = ArrayNew(1) />
	            <cfset i = 1>
	        <cfloop query="getData" >
				<cfset LOCALOUTPUT.rows[i]=getData.getRow(i-1).getRowData()>
<!--- 				<cfset LOCALOUTPUT.rows[i] = [#getData.getRow(i-1).getColumn(0)#, #GetUsers.UserName_vch#,#GetUsers.CompanyName_vch#,#GetUsers.Active_int#,#GetUsers.created_dt#,#GetUsers.UserLevel_int#,#GetUsers.RoleName_vch#]> --->
				<cfset i = i + 1>
			</cfloop>
		</cfif>
		<cfreturn LOCALOUTPUT />		
	</cffunction>
	
	<cffunction name="createTableListSessionVar" access="remote" output="true" hint="convert js table list to session.list">
		<cfargument name="inpTableList" required="true" default="">
		<cfset Session.printableTableList=ArrayNew(1)>
		<cfset dataout=''>
		
		<cfloop list="#inpTableList#" index="tableName" delimiters=",">
			<cfset ArrayAppend(Session.printableTableList,#tableName#)>
		</cfloop>
		<cfif isDEfined("Session.printableTableList")>
		</cfif>
		<cfset dataout =  QueryNew("RXRESULTCODE, inpTableList,TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		<cfset QuerySetCell(dataout, "inpTableList", "#inpTableList#") />     
		<cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		<cfreturn dataout>		
	</cffunction>
	
</cfcomponent>