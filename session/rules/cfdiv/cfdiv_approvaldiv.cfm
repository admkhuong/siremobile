<!---<cfinvoke component="Session/Rules/cfc/emailproject" method="getApprovalInfo" returnvariable="getapprovals">
	<cfinvokeargument name="BATCHID" value="#url.BID#">
</cfinvoke>--->

<cfquery name="getapprovals" datasource="#Session.DBSourceEBM#">
SELECT *
FROM simpleobjects.pm_approval
WHERE BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.BID#">
</cfquery>

<cfoutput query="getapprovals">
<div style="position:relative; display:inline-block; border-top:1px solid ##ccc; padding:5px;">
	<div style="margin-bottom:10px; float:left; width:240px;">
    	<span style="margin-right:10px;color:##333;">Approved By:</span> #ApprovalName_vch#
    </div>
    <div style="margin-bottom:10px; float:left; margin-left:5px; width:100px;">
    	#ApprovalGroup_vch#
    </div>
    <div style="margin:0 0 10px 5px; float:left; width:80px;">
    	<span style="color:##333;">on:</span> #DateFormat((ApprovalDate_dt),'mm/dd/yyyy')#
    </div>
</div>
</cfoutput>
<!---<cfoutput>#cgi.PATH_info# v#rootUrl#/#SessionPath#/Rules</cfoutput>--->