<cfif IsDefined("url.cpi") and IsDefined("url.cpih")>
<cftry>
	<!--- Check if changed item has already been entered into changed table --->
    <cfquery name="checkchanges" datasource="#Session.DBSourceEBM#">
    	SELECT ItemChanged_vch
        FROM simpleobjects.pm_change
        WHERE ItemChanged_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.cpi#" maxlength="200"> and Settled_int = 0 and BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.bid#">
    </cfquery>
    
    <cfif checkchanges.recordcount eq 0>
    	<!--- If item is not in table, insert it --->
        <cfquery name="setchanges" datasource="#Session.DBSourceEBM#">
        INSERT INTO simpleobjects.pm_change
        (
        ItemChanged_vch,
        HeaderChanged_vch,
        SectionChanged_vch,
        DateChanged_dt,
        BatchId_bi,
        ChangedIP_vch,
        ChangedUser_int,
        ChangedCUser_int,
        Settled_int
        )
        values
        (
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.cpi#" maxlength="200">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.cpih#" maxlength="200">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.cpidh#" maxlength="250">,
        NOW(),
        <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.bid#">,
        '#cgi.REMOTE_ADDR#',
        <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
        <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.CompanyID#">,
        0
        )
        </cfquery>
    
    </cfif>
	<cfcatch type="any">
		<cfdump var="#cfcatch#">
<!---     	<cfmail to="jpeterson@messagebroadcast.com" from="jpeterson@messagebroadcast.com" subject="dvtest" type="html">
        	
        </cfmail> --->
    </cfcatch>
</cftry>
</cfif>