
<cfquery name="getchanges" datasource="#Session.DBSourceEBM#">
    SELECT *
    FROM
        simpleobjects.pm_change inner join simpleobjects.useraccount on simpleobjects.pm_change.changedcuser_int = simpleobjects.useraccount.userid_int
    WHERE
        BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.batchid#">
        Order by DateChanged_dt desc
</cfquery>


<cfoutput query="getchanges">
<div style="position:relative; display:inline-block; border-top:1px solid ##ccc; padding:2px; font-size:.9em;">
	<div style="position:relative; display:inline-block;">
        <div style="margin:0 0 5px 5px; float:left; width:150px;">
            <span style="margin-right:5px;color:##333;">Date:</span><br />#DateFormat((DateChanged_dt),'mm/dd/yyyy')# #TimeFormat((DateChanged_dt),'hh:mm tt')#
        </div>
        <div style="margin:0 0 5px 5px; float:left; width:150px;">
            <span style="margin-right:5px;color:##333;">Field:</span><br />#ItemChanged_vch#
        </div>
        <div style="margin:0 0 5px 5px; float:left; width:130px;">
            <span style="margin-right:5px;color:##333;">Section:</span><br />#HeaderChanged_vch#<br />&gt;#SectionChanged_vch#
        </div>
    </div>
    <div style="position:relative; display:inline-block;">
        <div style="margin:0 0 5px 5px; float:left; width:150px;">
            <span style="margin-right:5px;color:##333;">User:</span><br />#Left(FirstName_vch,1)#. #LastName_vch#
        </div>
        <div style="margin:0 0 5px 5px; float:left; width:150px;">
            <span style="margin-right:5px;color:##333;">IP:</span><br />#ChangedIP_vch#
        </div>
        <div style="margin:0 0 5px 5px; float:left; width:130px;">
            <span style="margin-right:5px;color:##333;">Changes Saved:</span><br /><cfif Settled_int eq 1>yes<cfelse><span style="color:##FF0000; font-weight:bold">no</span></cfif>
        </div>
    </div>
</div>
</cfoutput>