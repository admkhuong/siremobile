<cfquery name="getmde" datasource="#Session.DBSourceEBM#">
	SELECT * FROM simpleobjects.pm_maindataelements WHERE BatchId_bi = 
	<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.BID#">
</cfquery>
<cfoutput query="getmde">
	<div style="margin-bottom:10px; position:relative; display:inline-block; width:100%; border-top:1px solid ##cfe7fe;">
		<form action="" method="post" name="uelement#getmde.maindataelementsid_bi#" id="uelement#getmde.maindataelementsid_bi#" onsubmit="return UDataElement('uelement#getmde.maindataelementsid_bi#');">
			<input type="hidden" name="maindataelementsid_bi" value="#getmde.maindataelementsid_bi#" />
			<div style="margin-bottom:5px; float:left;">
				Element:
				<br />
				<input type="text" name="element" value="#getmde.Element_vch#" style="width:100px;" />
			</div>
			<div style="margin:0 0 5px 10px; float:left;">
				Description:
				<br />
				<textarea name="elementdesc" style="width:200px;">#getmde.ElementDesc_vch#</textarea>
			</div>
			<cfif Session.CollEditable eq 1>
			<div style="margin:0 0 5px 10px; float:right;">
				Action
				<br />
				<input type="submit" name="upelement" value="update" style="width:64px;"/>
			</div>
			</cfif>
		</form>
	</div>
</cfoutput>
