<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>

<cfparam name="INPBATCHID" default="0">


<style>

	.LCEMapRight
	{ 
		display:inline;
		border:none;
		font-size:10px;
	}
	
	.LCEMapRight th
	{ 
		background: -moz-linear-gradient(center top , #B9E0F5, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#0CF;	
		padding: 2px;			
	}
	
	.LCEMapRight th.LCEMapRightRowHighlight 
	{ 
		background: -moz-linear-gradient(center top , #B9E0F5, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#0CF;	
		padding: 2px;			
	}
	
	.LCEOdd
	{ 
		background: -moz-linear-gradient(center top , #6FF, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#FFF;				
	}
	
	.LCEOdd:hover {
		background: -moz-linear-gradient(center top , #0CF, #92BDD6) repeat scroll 0 0 transparent; 
		background-color:#0CF;		
	}
	
	.LCEEven
	{
		background: -moz-linear-gradient(center top , #0CF, #92BDD6) repeat scroll 0 0 transparent; 
		background-color:#0CF;				
	}
	
	.LCEEven:hover {
		background: -moz-linear-gradient(center top , #6FF, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#FFF;	
	}

	
	.LCEMapLeft
	{
		display:inline;
		border:none;
		font-size:10px;				
	}
	
	.LCEMapLeft th
	{ 
		background: -moz-linear-gradient(center top , #0CF, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#92BDD6;	
		padding: 2px;				
	}
	
	.LCEMapLeft td
	{
		background: -moz-linear-gradient(center top , #B9E0F5, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#0CF;					
	}

	.LCEMapLeft tr.LCEMapRightRowHighlight td
	{		
		background: -moz-linear-gradient(center top , #6FF, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#FFF;	
	}
	
	.LCEMapLeft tr:hover td
	{		
		background: -moz-linear-gradient(center top , #6FF, #92BDD6) repeat scroll 0 0 transparent;
		background-color:#FFF;	
	}


<!---	.noOverlayStuff .ui-widget-overlay { background: ##262b33; opacity: .1;filter:Alpha(Opacity=10); }--->

	#LCEMap1 .ui-widget-overlay { background: ##262b33; opacity: .1;filter:Alpha(Opacity=10); }

</style>





<script TYPE="text/javascript">
	
	$(function() 
	{
		<!---$("#dialog-test2").dialog({ 
		
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true
			
		
		 });--->
		
		LoadETLDefs();
		
	});

	var CreateViewLCEDescriptionDialog = 0;
	var CreateViewLCEMessageDialog = 0;
	var CreateViewLCEMEssTableHeaderDescriptionDialog = 0;
	var LCEMessCurrTableHeader = 0;
	var LCEDescriptionDialogCurrRow = 0;
	var LCEMessageDialogCurrCol = 0;
		
	function LoadETLDefs()
	{
		$("#loadingETLList").show();		
				
		$.ajax({
		url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/analytics.cfc?method=ReadLCEMap&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		dataType: 'json',
		type: 'POST',
		data:  { INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput> },					  
		error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				<!---console.log(textStatus, errorThrown);--->
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("LCE Map Definitions have NOT been loaded properly. .\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { $("#loadingETLList").hide(); } );													
			},				
		success:			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!---<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{																									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{						
									
						<!---	if(typeof(d.DATA.FILETYPEID[0]) != "undefined")								
							{									
								SetSeparator(d.DATA.FILETYPEID[0], d.DATA.FIELDSEPARATOR[0]);											
							}					--->									
																							 
						}
																									
						$("#loadingETLList").hide();
										
					}
					else
					{<!--- Invalid structure returned --->	
						$("#loadingETLList").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					$("#loadingETLList").hide();
				}		
				--->
				
				
				if(d.VARS.ROWCOUNT > 0)
				{
					if(typeof(d.VARS.COLUMNS) != "undefined")
					{						
						
						var AppendStringBuffA = "";
						
						
						AppendStringBuffA += '<TABLE class="LCEMapContainer" border="0">';	
						
						AppendStringBuffA += "	<TR>";	
						AppendStringBuffA += "		<TD valign='top'>";	
						
						AppendStringBuffA += '<TABLE class="LCEMapLeft" cellpadding="3" cellspacing="3">';	
						
						AppendStringBuffA += "	<TR>";	
						
						<!--- Get column names as headers--->						
						for(i=0; i< d.DCS.length; i++)	
						{	
							AppendStringBuffA +=  "		<TH>" + d.DCS[i] + "<BR/>&nbsp;</TH>";	
						}
						
						AppendStringBuffA += "	</TR>";
					
						<!--- (0 based) column data --->
						for(i=0; i< d.VARS.ROWCOUNT; i++)	
						{
						
							AppendStringBuffA += '	<TR class="LCEComboRow" rel1="' + i + '">';	
						
							for(x=0; x< d.VARS.COLUMNS.length; x++)	
							{	
								AppendStringBuffA +=  "		<TD align='center'>" + eval("d.VARS.DATA." + d.VARS.COLUMNS[x] + "[" + i + "]" ) + "</TD>";	
							}
						
							AppendStringBuffA += "	</TR>";
	
						//	console.log( eval("d.VARS.DATA." + d.VARS.COLUMNS[0] + "[" + x + "]" ) );
							
						}
					
						AppendStringBuffA += "	</TABLE>";
					
						
						AppendStringBuffA += "		</TD>";	
						AppendStringBuffA += "		<TD valign='top'>";
						
					
						AppendStringBuffA += '<TABLE class="LCEMapRight" cellpadding="3" cellspacing="3">';	
						
						AppendStringBuffA += "	<TR>";	
						
						<!--- Message headers --->
						for(i=0; i< d.MS.ROWCOUNT; i++)	
						{	
							AppendStringBuffA +=  "		<TH class='LCEMessHeader' title='" +  d.MS.DATA.DESCRIPTION[i]  + "'>" + "M" + (i+1) + "<BR/>" + "POS-" + d.MS.DATA.POS[i] + "</TH>";	
						}						
						
						<!--- (0 based) column data --->
						for(i=0; i< d.MSFLAGGED.ROWCOUNT; i++)	
						{
						
														
							var CurrRowBuff = "";
							var BuffDesc= "";
	
							<!--- Alternate row coloring using modulo--->
							if(i%2)
							{
								for(x=0; x< d.MSFLAGGED.COLUMNS.length; x++)									
								{	
									if(eval("d.MSFLAGGED.DATA." + d.MSFLAGGED.COLUMNS[x] + "[" + i + "]" ))
									{
										CurrRowBuff +=  "		<TD align='center' class='LCEOdd' LIBID='" + d.MS.DATA.LIBID[x] + "' ELEID='" + d.MS.DATA.ELEID[x] + "' SCRIPTID='" + d.MS.DATA.SCRIPTID[x] + "'>" + "<b>X</b>" + "</TD>";
										BuffDesc += " " + d.MS.DATA.DESCRIPTION[x];
									}
									else
									{
										CurrRowBuff +=  "		<TD align='center' class='LCEOdd' LIBID='" + d.MS.DATA.LIBID[x] + "' ELEID='" + d.MS.DATA.ELEID[x] + "' SCRIPTID='" + d.MS.DATA.SCRIPTID[x] + "'>" + "&nbsp;" + "</TD>";		
									}
								}
							}
							else
							{	
								for(x=0; x< d.MSFLAGGED.COLUMNS.length; x++)	
								{	
								
									if(eval("d.MSFLAGGED.DATA." + d.MSFLAGGED.COLUMNS[x] + "[" + i + "]" ))
									{
										CurrRowBuff +=  "		<TD align='center' class='LCEEven' LIBID='" + d.MS.DATA.LIBID[x] + "' ELEID='" + d.MS.DATA.ELEID[x] + "' SCRIPTID='" + d.MS.DATA.SCRIPTID[x] + "'>" + "<b>X</b>" + "</TD>";
										BuffDesc += " " + d.MS.DATA.DESCRIPTION[x];
									}
									else
									{	
										CurrRowBuff +=  "		<TD align='center' class='LCEEven' LIBID='" + d.MS.DATA.LIBID[x] + "' ELEID='" + d.MS.DATA.ELEID[x] + "' SCRIPTID='" + d.MS.DATA.SCRIPTID[x] + "'>" + "&nbsp;" + "</TD>";	
									}
										
								}
							}
						
							AppendStringBuffA += '	<TR class="LCEMessagesRow" rel1="' + i + '" rel2="' + BuffDesc + '">';

							AppendStringBuffA += CurrRowBuff;
						
							AppendStringBuffA += "	</TR>";
	
																					
						}
									
						
						AppendStringBuffA += "	</TABLE>";
						
						AppendStringBuffA += "		</TD>";	
						AppendStringBuffA += "	</TR>";
						AppendStringBuffA += '</TABLE>';	
									
						
						$("#LCEMap1").append(AppendStringBuffA);						
						
						$(".LCEComboRow").click(function() { 
										
							$(this).addClass("LCEMapRightRowHighlight");
							
							LCEDescriptionDialogCurrRow = $(this);
						
							CreateViewLCEDescriptionDialog = $('<div style="overflow: auto;">' + $(".LCEMessagesRow[rel1='" + $(this).attr("rel1") + "']").attr("rel2") + '</div>');

							CreateViewLCEDescriptionDialog
									.dialog({
										autoOpen: false,
										modal : true,
										title: 'Content',
										close: function() { LCEDescriptionDialogCurrRow.removeClass("LCEMapRightRowHighlight"); CreateViewLCEDescriptionDialog.dialog('destroy'); CreateViewLCEDescriptionDialog.remove(); CreateViewLCEDescriptionDialog = 0;},
										width: 500,
										height: 200,
										position: [LCEDescriptionDialogCurrRow.position().left + CreateEditLCEMDialog.offset().left, LCEDescriptionDialogCurrRow.position().top + CreateEditLCEMDialog.offset().top + 15]
									});
							
							CreateViewLCEDescriptionDialog.dialog('open');
	
						});
						
						
						$(".LCEMessHeader").click(function() { 
										
							$(this).addClass("LCEMapRightRowHighlight");
							
							LCEMessCurrTableHeader = $(this);
						
							CreateViewLCEMEssTableHeaderDescriptionDialog = $('<div style="overflow: auto;">' + $(this).attr("title") + '</div>');

							CreateViewLCEMEssTableHeaderDescriptionDialog
									.dialog({
										autoOpen: false,
										modal : true,
										title: 'Message Component Content',
										close: function() { LCEMessCurrTableHeader.removeClass("LCEMapRightRowHighlight"); CreateViewLCEMEssTableHeaderDescriptionDialog.dialog('destroy'); CreateViewLCEMEssTableHeaderDescriptionDialog.remove(); CreateViewLCEMEssTableHeaderDescriptionDialog = 0;},
										width: 'auto',
										height: 'auto',
										position: [LCEMessCurrTableHeader.position().left + CreateEditLCEMDialog.offset().left, LCEMessCurrTableHeader.position().top + CreateEditLCEMDialog.offset().top + 35]
									});
							
							CreateViewLCEMEssTableHeaderDescriptionDialog.dialog('open');
	
						});
						
												
						$(".LCEOdd, .LCEEven").click(function() { 
						
						
							var LCEMessageDialogCurrCol = $(this);
						
							var ParamStr = '?inpLibId=' + encodeURIComponent($(this).attr("LIBID"));
							ParamStr += '&inpEleId=' +  encodeURIComponent($(this).attr("ELEID"));
							ParamStr += '&inpScriptId=' +  encodeURIComponent($(this).attr("SCRIPTID"));
				
							CreateViewLCEMessageDialog = $('<div style="overflow: auto;">' + $(".LCEMessagesRow[rel1='" + $(this).attr("rel1") + "']").attr("rel2") + '</div>');

							CreateViewLCEMessageDialog
									.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_PreviewPopup' + ParamStr )
									.dialog({
										autoOpen: false,
										modal : true,
										title: 'Script Preview',
										close: function() { CreateViewLCEMessageDialog.dialog('destroy'); CreateViewLCEMessageDialog.remove(); CreateViewLCEMessageDialog = 0;},
										width: 240,
										height: 80,
										position: [LCEMessageDialogCurrCol.position().left + CreateEditLCEMDialog.offset().left, LCEMessageDialogCurrCol.position().top + CreateEditLCEMDialog.offset().top + 15]
									});
							
							CreateViewLCEMessageDialog.dialog('open');
	
						});
										
					}
					
				}
				
				
				$("#loadingETLList").hide();		
			} 		
					
		});
	
		return false;
		
	}
	
</script>
<!---
<div id="dialog-test2" title="Test Description">
This is a test
</div>--->


<div id="LCEMap1">




</div>


<div id="loadingETLList" style="display:inline;" align="center">
Analyzing Data... <img class="loadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
<BR>
This may take a few moments depending on scripting complexity.
</div>