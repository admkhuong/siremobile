<cfparam name="INPBATCHID" default="0">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>

<cfparam name="Session.CollEditable" default= 0>
<cfinclude template="ProjectManagement/html/inc_ProjectParams.cfm">
<cfajaxproxy cfc="cfc/emailproject" jsclassname="AddFormValues" />

<script type="text/javascript">
var NFValue = new AddFormValues();
var HasChanged_pm = false;
var dpid=0;
<!---  --->
<cfoutput>
<cfset roleData=structNew()/>
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfinvoke argumentcollection="#args#" method="getUserRole" component="#Session.SessionCFCPath#.administrator.permission" returnvariable="roleData"/>
<cfif isDefined("roleData.role") AND roleData.role NEQ 'User'>
	<cfset Session.CollEditable = 1>
	<cfelse>
	<cfset Session.CollEditable = 0>
</cfif>
</cfoutput>

<!--- --->

var NProjectData = function(){
	$("#waitsave").fadeIn(600);
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataFieldsResult);
	NFValue.setHTTPMethod("POST");
	NFValue.setForm('voicetem');
	NFValue.AddPRJCTRow();
	return false;
}	

var NDataElement = function(){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataElementResult);
	NFValue.setForm('aelement');
	NFValue.AddMCDERow();
	return false;
}	

var UDataElement = function(ffg){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataElementResult);
	NFValue.setForm(ffg);
	NFValue.UpMCDERow();
	return false;
}	

var NDataCRC = function(){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(CRCResult);
	NFValue.setForm('acrc');
	NFValue.AddMCCRCRow();
	return false;
}	

var UDataCRC = function(ffg){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(CRCResult);
	NFValue.setForm(ffg);
	NFValue.UpMCCRCRow();
	return false;
}	

var NProjectApproval = function(){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(ApprovalResult);
	NFValue.setForm('papprovals');
	NFValue.AddPARow();
	return false;
}	

var NDataFields = function(fndf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataFieldsDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');	
	var BatchID = <cfoutput>#INPBATCHID#</cfoutput>;
	var DATAFIELDNAME_VCH = document.getElementById('DATAFIELDNAME_VCH').value;
	var DATAFIELDDESC_VCH = document.getElementById('DATAFIELDDESC_VCH').value;
	NFValue.AddDFRow(BatchID,DATAFIELDNAME_VCH,DATAFIELDDESC_VCH);
	return false;
}	
var UDataElement = function(ffg){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataElementResult);
	NFValue.setForm(ffg);
	NFValue.UpMCDERow();
	return false;
}	
var UDataFields = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(DataFieldsDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(7, PriNum);

	var DATAFIELDID_BI = nnumc;
	var DATAFIELDNAME_VCH = document.getElementById('DATAFIELDNAME_VCH' + nnumc).value;
	var DATAFIELDDESC_VCH = document.getElementById('DATAFIELDDESC_VCH' + nnumc).value;
	NFValue.UpDFRow(DATAFIELDID_BI,DATAFIELDNAME_VCH,DATAFIELDDESC_VCH);
	return false;
}

<!--- Insert Email Hyperlinks --->	

var NEmailHeader = function(nehd){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailHeaderDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var BATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
	var FIELDNAME_VCH = document.getElementById('EMAILHEADERLINK_VCH').value;
	var ROUTESTO_VCH = document.getElementById('EMAILHEADERROUTE_VCH').value;
	
	NFValue.AddEHRow(BATCHID,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	

var NEmailBody = function(nebd){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailBodyDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var BATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
	var FIELDNAME_VCH = document.getElementById('EMAILBODYLINK_VCH').value;
	var ROUTESTO_VCH = document.getElementById('EMAILBODYROUTE_VCH').value;
	NFValue.AddEBRow(BATCHID,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}

var NEmailOnline = function(neod){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailOnlineSupportDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var BATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
	var FIELDNAME_VCH = document.getElementById('EMAILONLINESUPPORTLINK_VCH').value;
	var ROUTESTO_VCH = document.getElementById('EMAILONLINESUPPORTROUTE_VCH').value;
	NFValue.AddEORow(BATCHID,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	

var NEmailFooter = function(nefd){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailFooterDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var BATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
	var FIELDNAME_VCH = document.getElementById('EMAILFOOTERLINK_VCH').value;
	var ROUTESTO_VCH = document.getElementById('EMAILFOOTERROUTE_VCH').value;
	NFValue.AddEFRow(BATCHID,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}

var NEmailRight = function(nerd){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailRightSideDVResult);
	//NFValue.setForm('DATAFIELDNAME_VCH','DATAFIELDDESC_VCH');
	var BATCHID = <cfoutput>#INPBATCHID#</cfoutput>;
	var FIELDNAME_VCH = document.getElementById('EMAILRIGHTLINK_VCH').value;
	var ROUTESTO_VCH = document.getElementById('EMAILRIGHTROUTE_VCH').value;
	NFValue.AddERRow(BATCHID,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}

<!--- Update Email Hyperlinks --->

var UEmailHeader = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailHeaderDVResult);
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(3, PriNum);
	var EMAILHEADERID_BI = nnumc;
	var FIELDNAME_VCH = document.getElementById('EHFIELDNAME_VCH' + nnumc).value;
	var ROUTESTO_VCH = document.getElementById('EHROUTESTO_VCH' + nnumc).value;
	
	NFValue.UpEHDRow(EMAILHEADERID_BI,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	

var UEmailBody = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailBodyDVResult);
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(3, PriNum);
	var EMAILBODYID_BI = nnumc;
	var FIELDNAME_VCH = document.getElementById('EBFIELDNAME_VCH' + nnumc).value;
	var ROUTESTO_VCH = document.getElementById('EBROUTESTO_VCH' + nnumc).value;
	
	NFValue.UpEBDRow(EMAILBODYID_BI,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	

var UEmailFooter = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailFooterDVResult);
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(3, PriNum);
	var EMAILFOOTERID_BI = nnumc;
	var FIELDNAME_VCH = document.getElementById('EFFIELDNAME_VCH' + nnumc).value;
	var ROUTESTO_VCH = document.getElementById('EFROUTESTO_VCH' + nnumc).value;
	
	NFValue.UpEFDRow(EMAILFOOTERID_BI,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	
var UEmailOnlineSupport = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailOnlineSupportDVResult);
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(4, PriNum);
	var EMAILONLINESUPPORTID_BI = nnumc;
	var FIELDNAME_VCH = document.getElementById('EOSFIELDNAME_VCH' + nnumc).value;
	var ROUTESTO_VCH = document.getElementById('EOSROUTESTO_VCH' + nnumc).value;
	
	NFValue.UpEOSDRow(EMAILONLINESUPPORTID_BI,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	
var UEmailRightSide = function(ffdf){
	NFValue.setAsyncMode();
	NFValue.setCallbackHandler(EmailRightSideDVResult);
	var PriValue = ffdf;
	var PriNum = PriValue.length;
    var nnumc = PriValue.substring(3, PriNum);
	var EMAILRIGHTSIDEID_BI = nnumc;
	var FIELDNAME_VCH = document.getElementById('ERFIELDNAME_VCH' + nnumc).value;
	var ROUTESTO_VCH = document.getElementById('ERROUTESTO_VCH' + nnumc).value;
	
	NFValue.UpERDRow(EMAILRIGHTSIDEID_BI,FIELDNAME_VCH,ROUTESTO_VCH);
	return false;
}	

<!--- Begin Callback Handlers --->

var DataFieldsResult = function(results){
	$("#waitsave").fadeOut(600);
	HasChanged_pm = false;
<!--- 	$("#donesave").delay(1000).fadeIn(600);
	setTimeout(function(){
		$("#donesave").fadeOut(600);
	},4000); --->
	$.jGrowl("Update sucessfully", { life:2000, position:"center", header:' Message' });
	return false;
}	

var DataElementResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_maindataelement?BID=#INPBATCHID#', 'mainde');</cfoutput>
	return false;
}	
var CRCResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_maincrc?BID=#INPBATCHID#', 'maincrc');</cfoutput>
	return false;
}	
var ApprovalResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_approvaldiv?BID=#INPBATCHID#', 'approvaldv');</cfoutput>
	return false;
}	
var DataFieldsDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_datafields?BID=#INPBATCHID#', 'datafieldsdv');</cfoutput>
	return false;
}	
var EmailHeaderDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_emailheaders?BID=#INPBATCHID#', 'emailheaderdv');</cfoutput>
	return false;
}	
var EmailFooterDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_emailfooter?BID=#INPBATCHID#', 'emailfooterdv');</cfoutput>
	return false;
}	
var EmailBodyDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_emailbody?BID=#INPBATCHID#', 'emailbodydv');</cfoutput>
	return false;
}	
var EmailOnlineSupportDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_emailonlinesupport?BID=#INPBATCHID#', 'emailonlinesupportdv');</cfoutput>
	return false;
}	
var EmailRightSideDVResult = function(results){
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_emailrightside?BID=#INPBATCHID#', 'emailrightsidedv');</cfoutput>
	return false;
}	
</script>
<!--- Define a separate page for the javascript for each form in the document--->
<!---<cfinclude template="ProjectManagement/js/js_ProjectMainCallElements.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectMainCallDiposition.cfm">--->

<!---<cfinclude template="ProjectManagement/js/js_ProjectInformation.cfm">---> 
<!---<cfinclude template="ProjectManagement/js/js_ProgramOverview.cfm"> 
<cfinclude template="ProjectManagement/js/js_ProgramComplexity.cfm"> 
<cfinclude template="ProjectManagement/js/js_ProjectSettings.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectVoiceScripts.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectEmailScripts.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectDataFeeds.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectReporting.cfm">
<cfinclude template="ProjectManagement/js/js_ProjectApprovals.cfm">--->

<!---<cfinclude template="ProjectManagement/html/inc_ProjectParams.cfm">---> 

<script>
$('#voicetem :input').keyup(function(){
	if(!HasChanged_pm){
		HasChanged_pm = true;
	}
});
$('#voicetem :input').change(function(){
	if(!HasChanged_pm){
		HasChanged_pm = true;
	}
	fcgb = this.name;
	zzxx = $(this).parent().parent().parent().parent().attr('id');
	zzxx1 = $(this).parent().parent().parent().parent().parent().attr('id');
	zzxx2 = $(this).parent().parent().parent().attr('id');
		if ($('#'+zzxx).hasClass('info-col')){
			zzxx5 = zzxx;
		}
		else if ($('#'+zzxx1).hasClass('info-col')){
			zzxx5 = zzxx1;
		}
		else if ($('#'+zzxx2).hasClass('info-col')){
			zzxx5 = zzxx2;
		}
	
	fftt = $('#'+zzxx5).find('.colhdr').html();
	var fhdn = $('div').hasClass('dt');
	dpid = $(this).parent().attr('id');
		if (!dpid) {
			dpid = $(this).parent().parent().attr('id');
		}
		if(!dpid){
			dpid = $(this).parent().parent().parent().attr('id');
		}
	var fhdnc = dpid.substring(1, dpid.length);
	var dhdv = $('#'+zzxx5).find('#t'+fhdnc).html();
		if (!$('#'+zzxx5).find('#t'+fhdnc).hasClass('dtde')){
			$('#'+zzxx5).find('#t'+fhdnc).addClass('dtde');
		}

	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_changes?bid=#INPBATCHID#&cpi=' + fcgb + '&cpih=' + fftt + '&cpidh=' + dhdv, 'changesdv');</cfoutput>
	<cfoutput>ColdFusion.navigate('../Rules/cfdiv/cfdiv_changesview.cfm?batchid=#INPBATCHID#', 'changesviewdv');</cfoutput>
});

function toggleChecked(status) {
	$(".pist").each( function() {
	$(this).attr("checked",status);
	})
}
function toggleChecked1(status) {
	$(".pist1").each( function() {
	$(this).attr("checked",status);
	})
}
function toggleChecked2(status) {
	$(".pist2").each( function() {
	$(this).attr("checked",status);
	})
}
$(function() {

function inputFilledValue(){
	$('#voicetem :input').each(function(i){
		var dpidz = '';
		var vvdd = $(this).attr('value');
		if (vvdd){
			if (vvdd != 'on'){
				aazz5 = '';
				aazz = $(this).parent().parent().parent().parent().attr('id');
				aazz1 = $(this).parent().parent().parent().parent().parent().attr('id');
				aazz2 = $(this).parent().parent().parent().attr('id');
				
				if ($('#'+aazz).hasClass('info-col')){
					aazz5 = aazz;
				}
				else if ($('#'+aazz1).hasClass('info-col')){
					aazz5 = aazz1;
				}
				else if ($('#'+aazz2).hasClass('info-col')){
					aazz5 = aazz2;
				}
				
				if (aazz5){
					
						dpidz = $(this).parent().attr('id');
						if (!dpidz) {
							dpidz = $(this).parent().parent().attr('id');
						}
						if (dpidz){
							var fhdncz = dpidz.substring(1, dpidz.length);
							$('#'+aazz5).find('#t'+fhdncz).addClass('dtde');
						}
					
					if ($(this).parent().attr('id') == 'undefined') {
						dpidz = $(this).parent().attr('id');
					} else {
						dpidz = $(this).parent().parent().attr('id');
					}
	
					if (dpidz == 'undefined') {
						var fhdncz = dpidz.substring(1, dpidz.length);
						$('#'+aazz5).find('#t'+fhdncz).addClass('dtde');	
					}
	
				}
			}
		}
	});
}
function checkBoxFilledValue(){	
	$('#voicetem :checkbox').each(function(i){
		var dpidz = '';
		var vvdd = $(this).is(':checked');
		if (vvdd){
			aazz5 = '';
			aazz = $(this).parent().parent().parent().parent().attr('id');
			aazz1 = $(this).parent().parent().parent().parent().parent().attr('id');
			aazz2 = $(this).parent().parent().parent().attr('id');
			
			if ($('#'+aazz).hasClass('info-col')){
				aazz5 = aazz;
			}
			else if ($('#'+aazz1).hasClass('info-col')){
				aazz5 = aazz1;
			}
			else if ($('#'+aazz2).hasClass('info-col')){
				aazz5 = aazz2;
			}
			
			if (aazz5){
				
					dpidz = $(this).parent().attr('id');
					if (!dpidz) {
						dpidz = $(this).parent().parent().attr('id');
					}
					if (dpidz){
						var fhdncz = dpidz.substring(1, dpidz.length);
						$('#'+aazz5).find('#t'+fhdncz).addClass('dtde');
					}
				
				if ($(this).parent().attr('id') == 'undefined') {
					dpidz = $(this).parent().attr('id');
				} else {
					dpidz = $(this).parent().parent().attr('id');
				}

				if (dpidz == 'undefined') {
					var fhdncz = dpidz.substring(1, dpidz.length);
					$('#'+aazz5).find('#t'+fhdncz).addClass('dtde');	
				}

			}
		}
	});
}


	$("#LifeCycleEventMap").click(function() { EditLCEMDialog(); return false; });

 	SetupAccordianTables();
		
	<!--- Load each forms initalization method as defined in rel="" of the .info-col object --->
	$(".info-col").each(function (i) { 
		if( typeof( $(this).attr("rel") ) != "undefined" )
		{	
			eval("'" +$(this).attr("rel") + "();'" );
		}
	 });
	 
	 
	 $("#PMArchive").click(function() { PMArchiveBatchInfo(); return false; });
	 
	 checkBoxFilledValue();
	 inputFilledValue()
});




function PMArchiveBatchInfo()
{
		$("#loadingDlgBatchRulesEditor").show();		
					
		$.ajax({
		url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RulesForm.cfc?method=SaveNewArchiveVersionProgramOverviewForm&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		dataType: 'json',
		type: 'POST',
		data:  { INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput>, PROJECTLEADCONTACT_VCH : $("#PM_ContactInfo #PROJECTLEADCONTACT_VCH").val() },					  
		error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				<!---console.log(textStatus, errorThrown);--->
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Form Data has NOT been updated properly. .\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { $("#loadingDlgBatchRulesEditor").hide(); } );													
			},				
		success:			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{																									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{						
									
						
																							 
						}
																									
						$("#loadingDlgBatchRulesEditor").hide();
										
					}
					else
					{<!--- Invalid structure returned --->	
						$("#loadingDlgBatchRulesEditor").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					$("#loadingDlgBatchRulesEditor").hide();
				}				
			} 		
					
		});
	
		return false;
	
}

	

<!--- http://css-tricks.com/5994-grid-accordion-with-jquery/ --->
function SetupAccordianTables() {

    // Set up variables
    var $el, $parentWrap, $otherWrap, 
        $allTitles = $(".dt").css({
            padding: 5, // setting the padding here prevents a weird situation, where it would start animating at 0 padding instead of 5
            "cursor": "pointer" // make it seem clickable
        }),
		$allTitleHdr = $(".colhdr").css({
            "cursor": "pointer" // make it seem clickable
        }),
		$allChangeHdr = $(".chcolhdr").css({
            "cursor": "pointer" // make it seem clickable
        }),
        $allCells = $(".dd").css({
            position: "relative",
            top: -1,
            left: 0,
            display: "none" // info cells are just kicked off the page with CSS (for accessibility)
        });
    
    // clicking image of inactive column just opens column, doesn't go to link   
    $("#page-wrap, #page-wrap2, #hdr-wrap").delegate(".colhdr","click", function(e) { 

        if ( !$(this).parent().hasClass("curCol") ) {         
            e.preventDefault(); 
            $(this).next().find('.dt:first').click(); 
            //$(this).find('.print-checkbox:first').click(); 
        } 
        
    });
	
	$("#page-wrap, #page-wrap2, #hdr-wrap").delegate(".chcolhdr","click", function(e) { 
        
        if ( !$(this).parent().hasClass("curCol") ) {         
            e.preventDefault(); 
            $(this).next().find('.dt:first').click();
        } 
        
    });
    
    // clicking on titles does stuff
    $("#page-wrap, #page-wrap2, #hdr-wrap").delegate(".dt", "click", function() {
        						
        // cache this, as always, is good form
        $el = $(this);
        
        // if this is already the active cell, don't do anything
        if (!$el.hasClass("dtsel")) {
        		
            $parentWrap = $el.parent().parent();
            $otherWraps = $(".info-col").not($parentWrap);
            
            // remove current cell from selection of all cells
            $allTitles = $(".dt").not(this);
        
		
			<!--- Get old --->
	//		$ClickedAwayItem = $("dt.current");
		
	//		if( typeof( $ClickedAwayItem.attr("rel") ) != "undefined" )
	//		{
	//			eval( $ClickedAwayItem.attr("rel") + "($ClickedAwayItem.first() );");				
	//		}
	
			if( typeof( $parentWrap.data("updatef") ) != "undefined" )
			{
				eval( $parentWrap.data("updatef") + "();");				
			}
	
			    
            // close all info cells
            $allCells.slideUp();
            
            // return all titles (except current one) to normal size
            $allTitles.animate({
                fontSize: "12px",
                paddingTop: 5,
                paddingRight: 5,
                paddingBottom: 5,
                paddingLeft: 5
            }, 200);

            
            // animate current title to larger size            
            $el.animate({
                "font-size": "16px",
                paddingTop: 10,
                paddingRight: 5,
                paddingBottom: 0,
                paddingLeft: 10
            }, 200).next().slideDown();
            
            // make the current column the large size
            $parentWrap.animate({
                width: 470
            }, 200).addClass("curCol");
            
            // make other columns the small size
            $otherWraps.animate({
                width: 160
            }, 200).removeClass("curCol");
            
            // make sure the correct column is current
            $allTitles.removeClass("dtsel");
			$allTitles.addClass("dt");  
            $el.addClass("dtsel");  
        
        }
        
    });
	
    
    $("#starter").trigger("click");
    
}

<!--- Global so popup can refernece it to close it--->
var CreateEditLCEMDialog = 0;

function EditLCEMDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
												
	<!--- Erase any existing dialog data --->
	if(CreateEditLCEMDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateEditLCEMDialog.remove();
		CreateEditLCEMDialog = 0;
		
	}
					
	CreateEditLCEMDialog = $('<div style="overflow: auto;"></div>').append($loading.clone());
	
	CreateEditLCEMDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rules/dsp_LifeCycleEventMap')
		.dialog({
			modal : true,
			title: 'Life Cycle Event Map',
			close: function() { CreateEditLCEMDialog.dialog('destroy'); CreateEditLCEMDialog.remove(); CreateEditLCEMDialog = 0;},
			width: 1100,
			height: 'auto'
		});

	CreateEditLCEMDialog.dialog('open');

	return false;		
}



	var UpdateCollaborationCheckListDialog =0;
	function UpdateCollaborationCheckListDialogCall(){
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		<!--- Erase any existing dialog data --->
		if(UpdateCollaborationCheckListDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateCollaborationCheckListDialog.remove();
			UpdateCollaborationCheckListDialog = 0;
		}
		UpdateCollaborationCheckListDialog = $('<div></div>').append($loading.clone());
		UpdateCollaborationCheckListDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#session</cfoutput>/Rules/dsp_SelectPrintData?BID=<cfoutput>#INPBATCHID#</cfoutput>')
			.dialog({
				modal : true,
				title: 'Printable table list',
				close: function() {
					 UpdateCollaborationCheckListDialog.remove(); UpdateCollaborationCheckListDialog = 0;
					 var grid = $("#PrintableTableList");
					 grid.trigger("reloadGrid");
				},
				width: 751,
				position:'top',
				height: 'auto',
				buttons:[
					{
						text:'Print preview',
						click:function(){
							PrintableDataDialogCall();
							UpdateCollaborationCheckListDialog.remove();
							UpdateCollaborationCheckListDialog = 0;
						}
					},
					{
						text: 'Cancel',
						click:function(){
							UpdateCollaborationCheckListDialog.remove(); 
							UpdateCollaborationCheckListDialog = 0;
						}
					}
				]
			});
	
		UpdateCollaborationCheckListDialog.dialog('open');			
		return false;	
	}		
</script>

<!--- http://www.personal.psu.edu/cab38/ColorBrewer/ColorBrewer.html --->


<style>
	<cfif find('Firefox',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 .35em 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;disabled:true;}
	<cfelseif find('MSIE 9.0',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 .5em 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('MSIE',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('Chrome',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('Safari',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	</cfif>
	.pist1 {
		vertical-align:middle;
		width:60px;
	}
	input[type="checkbox"] { height:20px;width:21px; border:1px solid #ccc; margin:0; padding:0; vertical-align:middle; margin-bottom:-.75em;float:left;}
	input[type="radio"] { height:20px;width:21px; border:1px solid #ccc; margin:0; padding:0; vertical-align:middle; margin-bottom:-0.75;float:left;}
	.colhdr {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0; background:url(../Rules/ProjectManagement/images/hdr-blu-40.png); border: 1px solid #666; text-align: center;}
	.colhdr:hover {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0;  -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blu-over-40.png); border: 1px solid #666; text-align: center;}
	.curCol .colhdr {  border:1px solid #fff; font-size:18px; color:#e7fbff; text-shadow: 0px 2px 2px #000; -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blu-over-40.png); filter:progid:DXImageTransform.Microsoft.Shadow(color='black', Direction=180, Strength=5)}
	
	.chcolhdr {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0; background:url(../Rules/ProjectManagement/images/hdr-blk-40.png); border: 1px solid #666; text-align: center;}
	.chcolhdr:hover {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0;  -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blk-over-40.png); border: 1px solid #666; text-align: center;}
	.curCol .chcolhdr {  border:1px solid #fff; font-size:18px; color:#e7fbff; text-shadow: 0px 2px 2px #000; -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blk-over-40.png); filter:progid:DXImageTransform.Microsoft.Shadow(color='black', Direction=180, Strength=5)}
	
	
	#hdr-wrap { width: 1160px; padding: 0 0 20px 15px; margin: 0 auto; overflow: hidden; }
	
	#page-wrap { width: 1160px; padding: 0 0 20px 15px; margin: 0 auto; overflow: hidden; }
	
	#page-wrap2 { width: 1160px; padding: 0 0 0 15px; margin: 0 auto; overflow: hidden;}
		
	.info-col { float: left; width: 160px; height: 100%; padding: 0 0 0 0; line-height:18px;}
	/*.info-col h2 { text-align: center; font-weight: normal; padding: 10px 0; background:#FFF; border: 1px solid #666; }
	.curCol h2 { text-align: center; font-weight: normal; padding: 10px 0; background:#FFF; border: 1px solid #666; border-top:none; border-left:none; border-right:none; }*/
	
	.image { height: 100px; text-indent: -9999px; display: block; border-right: 1px solid white; }
	
	/*.dth { padding: 5px; background: #900; color: white; border-bottom: 1px solid white; border-right: 1px solid white; font-size:14px; }
	.ddh { position: absolute; left: -9999px; top: -9999px; width: 449px; background: #900; padding: 10px; color: white; border-right: 1px solid white; font-size:11px; display:inline-block; }
	*/
	
	.dt { padding: 5px; color: #666; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:#ddd;}
	.dt:hover { padding: 5px; color: #333; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:#ddd;}
	
	.dtde { padding: 5px; color: #666; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png);}
	.dtde:hover { padding: 5px; color: #333; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top-over.png);}
	
	.dtsel { padding: 5px; color: #000; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png); cursor:none; text-shadow: 0px 1px 0px white;}
	.dtsel:hover { padding: 5px; color: #000; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png); cursor:none; text-shadow: 0px 1px 0px white;}
	
	.dd { position: absolute; left: -9999px; top: -9999px; width: 449px; padding: 10px; color: #666; border-right: 1px solid #ccc;; font-size:12px; display:inline-block; background:url(../Rules/ProjectManagement/images/wht-grd.png);overflow:scroll; }
		
	
	.curCol { position: relative; border: 1px solid #333; }

	.FullWidth
	{
		width: 100%;
		min-width:: 100%;
		max-height:225px;
	}
	
/* the input field */
.date {
	border:1px solid #ccc;
	font-size:18px;
	padding:4px;
	text-align:center;
	width:194px;
	
	-moz-box-shadow:0 0 10px #eee inset;
	-webkit-box-shadow:0 0 10px #eee inset;
}

/* calendar root element */
#calroot {
	/* place on top of other elements. set a higher value if nessessary */
	z-index:10000;
	
	margin-top:-1px;
	width:198px;
	padding:2px;
	background-color:#fff;
	font-size:11px;
	border:1px solid #ccc;
	
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	
	-moz-box-shadow: 0 0 15px #666;
	-webkit-box-shadow: 0 0 15px #666;	
}

/* head. contains title, prev/next month controls and possible month/year selectors */
#calhead {	
	padding:2px 0;
	height:22px;
} 

.savebtn {height:36px; width:135px; background:url(../Rules/ProjectManagement/images/savebtn.png); border:none;}
.savebtn:hover {height:36px; width:135px; background:url(../Rules/ProjectManagement/images/savebtn-o.png); border:none;}

#caltitle {
	font-size:14px;
	color:#0150D1;	
	float:left;
	text-align:center;
	width:155px;
	line-height:20px;
	text-shadow:0 1px 0 #ddd;
}

#calnext, #calprev {
	display:block;
	width:20px;
	height:20px;
	background:transparent url(../Rules/ProjectManagement/prev.gif) no-repeat scroll center center;
	float:left;
	cursor:pointer;
}

#calnext {
	background-image:url(../Rules/ProjectManagement/next.gif);
	float:right;
}

#calprev.caldisabled, #calnext.caldisabled {
	visibility:hidden;	
}

/* year/month selector */
#caltitle select {
	font-size:10px;	
}

/* names of the days */
#caldays {
	height:14px;
	border-bottom:1px solid #ddd;
}

#caldays span {
	display:block;
	float:left;
	width:28px;
	text-align:center;
}

/* container for weeks */
#calweeks {
	background-color:#fff;
	margin-top:4px;
}

/* single week */
.calweek {
	clear:left;
	height:22px;
}

/* single day */
.calweek a {
	display:block;
	float:left;
	width:27px;
	height:20px;
	text-decoration:none;
	font-size:11px;
	margin-left:1px;
	text-align:center;
	line-height:20px;
	color:#666;
	-moz-border-radius:3px;
	-webkit-border-radius:3px; 		
} 

/* different states */
.calweek a:hover, .calfocus {
	background-color:#ddd;
}

/* sunday */
a.calsun {
	color:red;		
}

/* offmonth day */
a.caloff {
	color:#ccc;		
}

a.caloff:hover {
	background-color:rgb(245, 245, 250);		
}


/* unselecteble day */
a.caldisabled {
	background-color:#efefef !important;
	color:#ccc	!important;
	cursor:default;
}

/* current day */
#calcurrent {
	background-color:#498CE2;
	color:#fff;
}

/* today */
#caltoday {
	background-color:#333;
	color:#fff;
}
	
</style>

<!---

SEC I - Contact Information
SEC II -  Program Overview
SEC III – Program Complexity
SEC IV – Project Settings
SEC V – Dialer Settings
SEC VI – Script Information
SEC VII – Call Data Elements
SEC VIII – UAT
SEC IX – Data Feed Information
SEC X – CRC/Last Call Disp.
SEC XI – Project Benefits
SEC XII – Reporting/Website
SECXIII- AT&T AO Disclosures
SEC XIV - Approvals

SEC XV Overrides - if this message goes out who gets Blocked - Internal DNC - External DNC 
SEC XVI Branding - Jingle - Voice Talent - Headers - Greetings - Tag Lines



--->
<!---<div id="resultdiv-display" title="INSERT/UPDATE NOTIFICATION!" style="display:none; text-align:center;">
	<span id="resultshow" style="line-height:18px; display:inline-block; margin:10px; font-size:.9em; text-align:justify; "></span>
</div>

<BR>

<!---<a id="LifeCycleEventMap">Life Cycle Event Map</a>--->

<BR>--->
<!---<div>
	<!---<button id="PMArchive">Archive</button>--->

</div>--->
<!---<div style="margin:0 0 10px 55px; width:1200px; font-weight:bold; font-size:18px; position:relative; display:inline-block;">
Collaboration
</div>--->
      
       
<style>
	
	#CollaborationSBContent
	{
		margin:0 0;
		width:450px;
		padding:0px;
		border: none;
		min-height: 330px;
		height: 1000px;
		font-size:12px;
	}
	
	
	#CollaborationSBContent #LeftMenu
	{
		width:270px;
		min-width:270px;		
		background: #B6C29A;
		background: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(1, rgb(237,237,237)),
		color-stop(0, rgb(200,216,143))
		);
		background: -moz-linear-gradient(
			center top,
			rgb(237,237,237),
			rgb(200,216,143)
		);
		
		position:absolute;
		top:-8px;
		left:-17px;
		#left:150px;
		padding:15px;
		margin:0px;	
		border: 0;
		border-right: 1px solid #CCC;
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		min-height: 100%;
		height: 100%;
		z-index:2300;
	}
	
	
	#CollaborationSBContent #RightStage
	{
		position:absolute;
		top:0;
		left:287px;
		#left:420px;
		padding:15px;
		margin:0px;	
		border: 0;
	}
	
	
	#CollaborationSBContent h1
	{
		font-size:12px;
		font-weight:bold;
		display:inline;
		padding-right:10px;	
		min-width: 100px;
		width: 100px;	
	}

</style> 

<div id='CollaborationSBContent' class="RXForm" >

	<div id="LeftMenu" >

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Edit Collaboration Documentation</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img src="../../public/images/trk_collaboration.png" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul>
         		<li>Areas with no content will be in grey.</li>
                <li>Areas with information filled out will be highlighted in blue.</li>
				<cfif Session.CollEditable eq 0>
				<li>You don't have permission to edit fields.</li>
				</cfif>
                <li>Press "Save Data" button when finished.</li>
                <li>Press "Cancel" button to exit without creating a new one.</li>               
            </ul>   
            
            <BR />
            <BR />
            
            <i>DEFINITION:</i> The Collaboration documentation is used to help multiple parties mangage all aspects of the current campaign.
            
            <BR />
            <BR />
            <cfif Session.CollEditable eq 1>
            <div style="float:left; margin:5px 0 0 0; display:inline-block;">
                <img src="../Rules/ProjectManagement/images/savebtn.png" class="savebtn" name="UpdateTable" onclick="return NProjectData('voicetem');" style="float:left; width:135px; height:36px; border:none;" />
				<img src="../Rules/ProjectManagement/images/printButton.png" class="savebtn" name="PrintTable" onclick="UpdateCollaborationCheckListDialogCall(); return false" style="float:left; width:135px; height:36px; border:none;" />
				
            </div>
			</cfif>
            
            <BR />
             <div style="margin: 30px 5px 3px 20px;">
                <ul> 
                    <li>Press "Exit" button to return to stage.</li>               
                </ul>                                              
            </div>            
            <button id="CancelrxdsDialog" class="CancelrxdsDialog">Exit</button>
            
            
            
                   
        </div>
        
	</div>
    
    <div id="RightStage">
                       
        <div style="margin:0 0 15px 55px; width:1200px; position:relative; display:inline-block;">
            <div id="waitsave" style="float:left; margin:5px 0 0 20px; color:#F00; display:none; position:relative;">
                Please wait while we save your changes. <img src="../../public/images/loading-small.gif" style="vertical-align:middle; margin-bottom:.25em; margin-left:10px;" />
            </div>
            <div id="donesave" style="float:left; margin:10px 0 0 20px; color:#F00; display:none; position:relative;">
                Changes Saved!
            </div>
        </div>
        <div id="hdr-wrap">
                
                <!--- Define a separate page for the html for each form in the document--->
                <cfinclude template="ProjectManagement/html/inc_ProjectMainCallElements.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectMainCallDiposition.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectApprovals.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectChanges.cfm">
                
        </div>	
                <cfoutput>
                <cfform name="voicetem" id="voicetem">
                <input type="hidden" name="BatchID" value="#INPBATCHID#" />
                
        <div id="page-wrap">
                
                <!--- Define a separate page for the html for each form in the document--->
                <cfinclude template="ProjectManagement/html/inc_ProjectInformation.cfm">         
                <cfinclude template="ProjectManagement/html/inc_ProgramOverview.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProgramComplexity.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectSettings.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectVoiceScripts.cfm">
                
        </div>
            
        
        <div id="page-wrap2">
        
                <cfinclude template="ProjectManagement/html/inc_ProjectEmailScripts.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectDataFeeds.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectReporting.cfm">
                <cfinclude template="ProjectManagement/html/inc_ProjectDisclosures.cfm">
                        
        </div>		
                </cfform>
                </cfoutput>
      
    
    </div>
</div>
             
<script>
$(document).ready(function(){
	$("#CancelrxdsDialog").click(function(){
		CreateCollaborationSBDialog.remove();
		CreateCollaborationSBDialog = 0;		
	});
	$(":date").dateinput({format: 'mm/dd/yyyy'});
});
</script>