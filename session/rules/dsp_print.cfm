<cfparam name="Session.projectApprovalsList" default="#StructNew()#"/> 
<cfparam name="Session.projectApprovalsList" default="#StructNew()#"/> 


<SCRIPT LANGUAGE="JavaScript">  
  
var ob;  
var obLeft;  
var obRight; 
var obRightWidth; 
var over = false;  
var iEdgeThreshold = 10;  
   
function findPos(obj) {  
  var curleft = curtop = 0;  
  if (obj.offsetParent) {  
      curleft = obj.offsetLeft;  
      curtop = obj.offsetTop;  
      while (obj = obj.offsetParent) {  
         curleft += obj.offsetLeft;  
         curtop += obj.offsetTop;  
      }  
   }  
   return [curleft,curtop];  
}  
   
/* Function that tells me if on the border or not */  
function isOnBorderRight(objTable,objTh,event){  
  var width = objTh.offsetWidth;  
  var left = objTh.offsetLeft;  
  var pos = findPos(objTable);  
  var absRight = pos[0] + left + width;  
   
  if( event.clientX > (absRight - iEdgeThreshold) ){  
      return true;  
  }  
   
  return false;  
}  
   
function getNodeName(objReference,nodeName){  
   var oElement = objReference;  
   while (oElement != null && oElement.tagName != null && oElement.tagName != "BODY") {  
      if (oElement.tagName.toUpperCase() == nodeName) {  
         return oElement;  
      }  
      oElement = oElement.parentNode;  
   }  
   return null;  
}  
   
function doResize(objTh,event){  
    if(!event) event = window.event;  
    var objTable = getNodeName(objTh,"TABLE");  
    if( isOnBorderRight(objTable,objTh,event)){   
       over=true;  
       objTh.style.cursor="e-resize";  
    }  
    else{  
       over=false;  
       objTh.style.cursor="";  
    }  
    return over;  
}  
   
function doneResizing(){  
   over=false;  
}  
   
function MD(event) {  
   if(!event) event = window.event;  
   
   MOUSTSTART_X=event.clientX;  
   MOUSTSTART_Y=event.clientY;  
   
   if (over){        
       if(event.srcElement)ob = event.srcElement;  
       else if(event.target)ob = event.target;  
       else return;  
   
       ob = getNodeName(ob,"TH");  
       if(ob == null) return;  
       //ob2 = getNodeName(ob,"TABLE");  
       //obLeft = ob.previousSibling;  
       obRight = ob.nextSibling;  
       obLeft = ob.previousElementSibling;   
       obRight = ob.nextElementSibling;  // Uncomment For FF  
       obwidth=parseInt(ob.style.width);  
       if (obLeft != null)  
       obLeftWidth=parseInt(obLeft.style.width);  
       if (obRight != null)  
       obRightWidth=parseInt(obRight.style.width);  
       //obwidth2=parseInt(ob2.offsetWidth);  
   }          
}  
   
function MM(event) {  
    if(!event) event = window.event;  
   
    if (ob) {  
        st=event.clientX-MOUSTSTART_X+obwidth;  
        //st2=event.clientX-MOUSTSTART_X+obwidth2;  
        document.getElementById("infoDiv").innerHTML = "st=" + st + " clientX=" + event.clientX + " moustart_x=" + MOUSTSTART_X + " obwidth=" + obwidth;  
        //document.getElementById("infoDiv").innerHTML += ;  
        //document.getElementById("infoDiv").innerHTML += ;  
        //document.getElementById("infoDiv").innerHTML += obwidth;  
   
        if(st>=10){  
            ob.style.width=st;  
            //ob2.style.width=st2;  
            //obLeft.style.width=st-obLeftWidth;  
            obRight.style.width=(parseInt(obwidth - st + obRightWidth) > 10 ? (obwidth - st + obRightWidth) : iEdgeThreshold + "px") ;  
        }  
        if(document.selection) document.selection.empty();  
        else if(window.getSelection)window.getSelection().removeAllRanges();  
    }  
}  
   
function MU(event) {  
    if(!event) event = window.event;  
    if(ob){  
        if(document.selection) document.selection.empty();  
        else if(window.getSelection)window.getSelection().removeAllRanges();  
        ob = null;  
    }  
}  
   
document.onmousedown = MD;  
document.onmousemove = MM;  
document.onmouseup = MU;  
</script>  
<style>
	    table {
    table-layout:fixed;
    width:auto;
    overflow:hidden;
    word-wrap:break-word;
    }

         /* if mozilla */  
         html>body div.scrollable tbody {  
            overflow: auto;  
         }  
   
         table.resizable th{  
            text-align:center;  
            overflow: hidden;  
         }  
   
         /* if mozilla, add 10 for the scrollbar */  
         html>body th.scrollbarCol {  
            width:10px;  
         }  
   
         table.resizable td{  
            overflow: hidden;  
         }  
   
         table.resizable{  
            table-layout:fixed;  
         }  
   
         table.resizable input{  
            width:100%;  
         }  
   
         table.resizable textarea{  
            width:100%;  
         }  
   
         .nowrap {  
             white-space:nowrap;  
         }  
   
         /* needed for IE */  
         table.tabular th.scrollbarCol {  
            background-color:transparent;   
         }  
   
         table.tabular {  
            FONT-SIZE: 13px;  
            FONT-FAMILY: 'Verdana, Arial, Helvetica, sans-serif';  
            COLOR: #336699;  
         }  
   
         table.tabular thead {  
             COLOR: #ffffff;  
             FONT-WEIGHT: bold;  
         }  
   
         table.tabular th{  
            background-color:#c0c0c0;   
            padding: 4px;  
         }  
   
         table.tabular td {  
            background-color:#EAF4F3;  
            padding: 2px;  
         }  	
<!--- --->	
.example{
background-color: #E5EECC;
    background-image: url("/images/bgfadegreen.gif");
    background-repeat: repeat-x;
    border: 1px solid #D4D4D4;
    clear: both;
    color: #000000;
    margin: 0;
    padding: 8px;
    width: auto;
}
#rounded-corner
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 20px;
	width: 1200px;
	text-align: left;
	border-collapse: collapse;
}
#rounded-corner thead th.rounded-company
{
	background: #b9c9fe url('../Rules/ProjectManagement/images/left.png') left -1px no-repeat;
}
#rounded-corner thead th.rounded-q4
{
	background: #b9c9fe url('../Rules/ProjectManagement/images/right.png') right -1px no-repeat;
}
#rounded-corner th
{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #b9c9fe;
}
#rounded-corner td
{
	padding: 8px;
	background: #e8edff;
	border-top: 1px solid #fff;
	color: #669;
}
#rounded-corner tfoot td.rounded-foot-left
{
	background: #e8edff url('../Rules/ProjectManagement/images/botleft.png') left bottom no-repeat;
}
#rounded-corner tfoot td.rounded-foot-right
{
	background: #e8edff url('../Rules/ProjectManagement/images/botright.png') right bottom no-repeat;
}
#rounded-corner tbody tr:hover td
{
	background: #d0dafd;
}
</style>
<cfset fieldList=StructNew()>
<cfoutput>
	


<!--- <cfdump var="#Session.printableTableList.elementAt(0)#"> --->
<cfloop array="#Session.printableTableList#" index="tableName">

	<cfif tableName EQ 'PM_APPROVAL'>
		<cfset fieldList=#Session.projectApprovalsList#/>
	<cfelseif tableName EQ 'PM_PROJECTINFORMATION'>
		<cfset fieldList=#Session.projectInfoList#/>
	<cfelseif tableName EQ 'PM_PROGRAMCOMPLEXITY'>
		<cfset fieldList=#Session.programComplexityList#/>
	<cfelseif tableName EQ 'PM_ProgramOverview'>
		<cfset fieldList=#Session.programOverviewList#/>
	<cfelseif tableName EQ 'pm_change'>
		<cfset fieldList=#Session.changeList#/>		
			<cfelseif tableName EQ 'pm_projectdatafeeds'>
		<cfset fieldList=#Session.projectDataFeedsList#/>
			<cfelseif tableName EQ 'pm_projectdisclosures'>
		<cfset fieldList=#Session.projectDisclosuresList#/>		
			<cfelseif tableName EQ 'pm_projectemailscripts'>
		<cfset fieldList=#Session.projectEmailScriptsList#/>		
					<cfelseif tableName EQ 'pm_maincrc'>
		<cfset fieldList=#Session.projectMainCRCList#/>
					<cfelseif tableName EQ 'pm_maindataelements'>
		<cfset fieldList=#Session.projectMainCallElementList#/>
					<cfelseif tableName EQ 'pm_projectreportinfo'>
		<cfset fieldList=#Session.projectReportInfoList#/>
					<cfelseif tableName EQ 'pm_projectsettings'>
		<cfset fieldList=#Session.projectSettingsList#/>		
					<cfelseif tableName EQ 'pm_scriptstollfree'>
		<cfset fieldList=#Session.ScriptTollFreeList#/>			
					<cfelseif tableName EQ 'pm_scriptstransfer'>
		<cfset fieldList=#Session.ScriptTransferList#/>
					<cfelseif tableName EQ 'pm_projectscriptinfo'>
		<cfset fieldList=#Session.projectVoiceScriptList#/>		
					<cfelse>		
		<cfset fieldList=StructNew()/>			
	</cfif>
<cfif not StructIsEmpty(fieldList)>
	<cfset dataStruct=Structnew()>
	<cfset args=StructNew()>
	<cfset args.inpBatchId=#url['BID']#>
	<cfset args.inpTableName="#tableName#">
	<cfset args.inpFieldList="#StructKeyList(fieldList,',')#">
	<cfinvoke argumentcollection="#args#" method="getTableData" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="dataStruct"/>
	
	<div class="scrollable" >  
	<table id="rounded-corner" >
	<thead>
		<cfif listLen(args.inpFieldList) EQ 1>
			<tr><th colspan="1" class="rounded-company rounded-q4"><font size="2"><b>Table: #tableName#</b></font></th>
		<cfelse>
			<tr><th colspan="#(listLen(args.inpFieldList)-1)#" class="rounded-company"><font size="2"><b>Table: #tableName#</b></font></th>
			<th colspan="1" class="rounded-q4"></th>		
		</cfif>

		</tr>
	</thead>
	    <thead>
	    	<tr>
				<cfloop list=#args.inpFieldList# delimiters="," index="colName">
					<th scope="col" >#Replace(colName,"_"," ","all")#</th>
				</cfloop>
	        </tr>
	    </thead>
	    <cfif not StructIsEmpty(dataStruct) and ArrayLen(#dataStruct.rows#) GT 0>
	    <tbody>
			<cfloop array="#dataStruct.rows#" index="row" >
	    	<tr style="width:100%;">
		    	<cfif isDefined("row")>
 				<cfloop array="#row#" index="rowValue">
					<cfif isDefined("rowValue") AND #rowValue# NEQ "">
					<td><cfoutput>#rowValue#</cfoutput></td>
					<cfelse>
					<td><cfoutput>N/A</cfoutput></td>
					</cfif>
					<!--- <cfdump var="#dataArray[i][j]#"> --->
				</cfloop>
				</cfif>
	        </tr>
			</cfloop>
	    </tbody>
	    </cfif>
	</table>
	</div>

</cfif>
</cfloop>

</cfoutput>


