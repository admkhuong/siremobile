<cfparam name="INPBATCHID" default="0">
<cfparam name="SESSION.USERID" default="0">
<cfoutput>
    	<script src="#rootUrl#/#SessionPath#/MCID/js/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>  
</cfoutput>
<script>

var taskItemIds=0;
var localSelectedValues=new Array();
var lastsel;
var selectedTableName;
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#PrintableTableList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=getPrintableTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		width:700,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['Table Name','Description'],
		colModel:[
			{name:'Tablename_vch',index:'Tablename_vch', width:150, editable:false},
			{name:'table_comment',index:'table_comment', width:150, editable:true},
		],
		rowNum:20,
	   	rowList:[5, 10, 20, 30],
		mtype: "POST",
		pager: jQuery('#pagerTaskList'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'Tablename_vch',
		viewrecords: true,
		sortorder: "asc",
		formatter: checkboxFormatter,
		multiselect: true,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		 loadComplete: function(data){
				//reloadUserTaskList();
				$(".edit_CheckList").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_CheckList").click( function() { EditCollaborationDialogCall($(this).attr('rel')); } );		
				return false;	 
   			},		
			onSelectRow: function(tableName,status){
				//alert(ItemId_int + '-' + status);
<!--- 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				} --->
				lastsel=tableName;
			},
			
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=UpdatePhoneData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_PrintableTableList").height(55);
	$("#loading").hide();
		
});
	//remote existing element in array1 from array2]
	
	function getNeedInsertList(originalList,selectedList){
		var result=new Array();
		if(originalList.length > 0 && selectedList.length > 0){
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]*1,originalList) < 0){
					result.push(selectedList[i]*1);
				}
			}
		}else
		{
			return selectedList;
		}
		return result;
	}
	function getRequiredValueList(list){
		var result=new Array();
		if(list.length>0){
			for(i=0;i<list.length;i++){
				//var rowData = jQuery('#TaskList').getRowData(list[i]);
				//var temp= rowData['required'];
				var temp=getCellValue(jQuery('#PrintableTableList').getCell(list[i],'required'));
				result.push(temp);
				//result.push($('#' + list[i] + '_required').val());
			}
		}
		//alert(result);
		return result;
	}
	
	
	//get the not existing element from array 1 from array2
	function getNeedRemoveList(selectedList,originalList){
		var res=new Array();
		if(selectedList.length>0 && originalList.length>0){
			for(i=0;i<originalList.length;i++){
				if($.inArray(originalList[i]+"",selectedList) < 0){
					res.push(originalList[i]*1);
				}
			}
		}else{
			return originalList;
		}
		return res;
	}
	

	function reloadUserTaskList(){
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getTaskListOfUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : lastsel }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							taskItemIds=d.DATA.TASKITEMS[0];
							requireds=d.DATA.REQUIRED[0];
							//set local selected values to array of original task list
							localSelectedValues=taskItemIds;
							//jQuery("#TaskList").resetSelection();
							if(taskItemIds.length > 0)
							{
								for(var i=0;i<=taskItemIds.length;i++){
									jQuery("#PrintableTableList").setSelection(taskItemIds[i],false);
									//$(#ur_checkbox_id).attr('disabled', true);
									jQuery("#PrintableTableList").setCell(taskItemIds[i],'required',requireds[i]);
								}
							}
	<!--- 									else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("User has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										} --->
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgLinkUserToCompany").hide();
			} );
	}
	
	
	var EditCollaborationDialog = 0;
	function EditCollaborationDialogCall(tableName){
		var selectingRow=jQuery("#PrintableTableList").getGridParam('selrow');
		
 		 if(selectingRow=='null'){
		 	jQuery("#PrintableTableList").setSelection(tableName,false);
		 	
		 }else{
		 	jQuery("#PrintableTableList").setSelection(selectingRow,false);
		 } 
		 
		selectedTableName=tableName;
		var $loadingLevel = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		if(EditCollaborationDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			EditCollaborationDialog.remove();
			EditCollaborationDialog = 0;
		}
		EditCollaborationDialog = $('<div></div>').append($loadingLevel.clone());
		EditCollaborationDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_EditCollaboration')
			.dialog({
				modal : true,
				title: 'Associate Permissions to Level',
				close: function() {
					 EditCollaborationDialog.remove(); 
					 EditCollaborationDialog = 0; 
					 var grid = $("#FieldList");
					 grid.trigger("reloadGrid");
				},
				width: 700,
				position:'top',
				height: 400,
			});
	
		EditCollaborationDialog.dialog('open');
	
		return false;			
	}
	
		function diffArrays (A, B) {
		
		  var strA = ":" + A.join("::") + ":";
		  var strB = ":" +  B.join(":|:") + ":";
		
		  var reg = new RegExp("(" + strB + ")","gi");
		
		  var strDiff = strA.replace(reg,"").replace(/^:/,"").replace(/:$/,"");
		
		  var arrDiff = strDiff.split("::");
		
		  return arrDiff;
		}
		function checkboxFormatter(cellvalue, options, rowObject) {
		    cellvalue = cellvalue + "";
		    cellvalue = cellvalue.toLowerCase();
		    var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked='checked'" : "";
		    return '<input type="checkbox" onclick="clientSave(' + options.rowId + ','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';
		}
		function clientSave(rowId,value){

			if(value == 0 || value == 'undefined' || value === undefined){
				jQuery("#PrintableTableList").setCell(rowId,'required',1);
			}else{
				jQuery("#PrintableTableList").setCell(rowId,'required',0);
			}
		}
		function getCellValue(content) {
		  var k1, val = 0;
		  if(content==false) return 0;
		  k1 = content.indexOf(' value=') + 7;
		  val = content.substr(k1,3);
		  val = val.replace(/[^0-1]/g, '');
		  if(val=='undefined' || val=='') return 0;
		  return val;
		}

function createSessionVar(){
	var selectedTableList=jQuery("#PrintableTableList").getGridParam('selarrrow');
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/Rules/cfc/rulechecklist.cfc?method=createTableListSessionVar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpTableList : selectedTableList.join(',')},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								//console.log('abc');
							}
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPermission").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
					
		});	
}

	
function PrintableDataDialogCall()
{
	createSessionVar();
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	$.ajax({
	  url: '<cfoutput>#rootUrl#/#LocalServerDirPath#session/Rules/</cfoutput>dsp_print?BID=<cfoutput>#url['BID']#</cfoutput>',
	  success: function(data) {
	  	var template = '<div id="printColloboration">'+ data +'</div>';
	    $(document.body).append(template);
	    $.printPreview({
        	printDiv : 'printColloboration'
        });
        $("#printColloboration").remove();
	    return false;
	  }
	});
	
	
	return false;		
}	

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
</script>

<table id="PrintableTableList" align="center"></table>
<div id="pagerTaskList"></div>
	   