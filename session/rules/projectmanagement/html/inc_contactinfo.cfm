

<style>


</style>

<cfset tableName="PM_projectinformation">
<cfparam name="projectInfoList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.CompanyId#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="projectInfoList"/>
<cfoutput>
<div class="info-col" id="PM_ContactInfo" rel="InitContactInfoview">
    <div class="colhdr">Project Information</div>
    <div id="dl">
    	<div class="dt" id="t1">Project ID</div>
        <div class="dd" id="d1"><textarea id="PROJECTID_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t2">Version</div>
        <div class="dd" id="d2"><textarea id="VERSION_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t3">Platform Type</div>
        <div class="dd" id="d3"><textarea id="PLATFORM_TYPE_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t4">Project Duration</div>
        <div class="dd" id="d4"><textarea id="PROJECT_DURATION_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t5">Submission Date</div>
        <div class="dd" id="d5"><textarea id="SUBMISSION_DATE_DT" class="FullWidth"></textarea></div>
        <div class="dt" id="t6">Project Start Date</div>
        <div class="dd" id="d6"><textarea id="PROJECT_START_DATE_DT" class="FullWidth"></textarea></div> 		
        <div class="dt" id="t7">Project End Date</div>
        <div class="dd" id="d7"><textarea id="PROJECT_END_DATE_DT" class="FullWidth"></textarea></div> 
        <div class="dt" id="t8">Prepared By</div>
        <div class="dd" id="d8"><textarea id="PREPARED_BY_VCH" class="FullWidth"></textarea></div> 
        <div class="dt" id="t9">Email/Phone Information</div>
        <div class="dd" id="d9"><textarea id="EMAIL_PHONE_VCH" class="FullWidth"></textarea></div> 
        
        <div class="dt" id="t10">Project Lead Contact</div>
        <div class="dd" id="d10"><textarea id="PROJECTLEADCONTACT_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t11">Script Content Contact</div>
        <div class="dd" id="d11"><textarea id="SCRIPT_CONTENT_CONTACT_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t12">Data Feed Contact</div>
        <div class="dd" id="d12"><textarea id="DATA_FEED_CONTACT_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t13">Project Team Members</div>
        <div class="dd" id="d13"><textarea id="PROJECT_TEAM_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t14">Communications Alerts</div>
        <div class="dd" id="d14"><textarea id="COMMUNICATIONS_ALERT_VCH" class="FullWidth"></textarea></div>
        <div class="dt" id="t15">M&P Contact</div>
        <div class="dd" id="d15"><textarea id="MANDP_CONTACT_VCH" class="FullWidth"></textarea></div> 			
    </div>
</div>
</cfoutput>