<cfset tableName="PM_ProgramComplexity">
<cfparam name="Session.programComplexityList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.programComplexityList"/>
<cfif not StructIsEmpty(Session.programComplexityList)>
<cfoutput>
<div class="info-col" id="PM_ProgramComplexity" rel="InitProgramComplexity">

    <div class="colhdr">Program Complexity</div>
    
    <!---<a class="image batman" href="http://jprart.deviantart.com/">View Image</a>--->
    
    <div id="dl">
		<cfif StructKeyExists(Session.programComplexityList,'PROGRAM_COMPLEXITY_VCH')>
	      <div class="dt" id="t1">Max Connect Time
					    	<cfif structFind(Session.programComplexityList,"PROGRAM_COMPLEXITY_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
</div>
	      <div class="dd" id="d1"><textarea name="PROGRAM_COMPLEXITY_VCH" class="FullWidth">#PROGRAM_COMPLEXITY_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.programComplexityList,'MONITORING_THRESHOLD_INT')>
	      
	      <div class="dt" id="t2">Monitoring<cfif structFind(Session.programComplexityList,"MONITORING_THRESHOLD_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
	      <div class="dd" id="d2">
	      	<div style="margin-bottom:15px;">
	        Do we need threshold monitoring?<br />
				<input type="radio" name="MONITORING_THRESHOLD_INT" value="1" style="vertical-align:middle; margin-bottom:.25em; margin-right:5px;"<cfif MONITORING_THRESHOLD_INT eq 1> checked="checked"</cfif> /><label>Yes</label></br>
				<input type="radio" name="MONITORING_THRESHOLD_INT" value="0"<cfif MONITORING_THRESHOLD_INT eq 0> checked="checked"</cfif> style="vertical-align:middle; margin-bottom:.25em; margin-right:5px; margin-left:-26px;" /><label>No</label>
	      	</div>

		
			<cfif StructKeyExists(Session.programComplexityList,'MONDAY_VCH')>
		        <div style="margin-bottom:15px;">
		     	If Yes, How many records per day?:<br />
		      	</div>
		      	<div style="margin-bottom:15px;">
		        Monday:
		      	<textarea name="MONDAY_VCH" class="FullWidth">#MONDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Tuesday:
		      	<textarea name="TUESDAY_VCH" class="FullWidth">#TUESDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Wednesday:
		      	<textarea name="WEDNESDAY_VCH" class="FullWidth">#WEDNESDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Thursday:
		      	<textarea name="THURSDAY_VCH" class="FullWidth">#THURSDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Friday:
		      	<textarea name="FRIDAY_VCH" class="FullWidth">#FRIDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Saturday:
		      	<textarea name="SATURDAY_VCH" class="FullWidth">#SATURDAY_VCH#</textarea>
		        </div>
		        <div style="margin-bottom:15px;">
		        Sunday:
		      	<textarea name="SUNDAY_VCH" class="FullWidth">#SUNDAY_VCH#</textarea>
		        </div>
			</cfif>
    	</div>
		</cfif>

    </div>

</div>
</cfoutput>
</cfif>