<cfset tableName="PM_ProgramOverview">
<cfparam name="Session.programOverviewList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.programOverviewList"/>

<cfif not StructIsEmpty(Session.programOverviewList)>
<cfoutput>
<div class="info-col" id="PM_ProgramOverview" rel="InitProgramOverview">

    <div class="colhdr">Program Overview</div>
    
    <!---<a class="image batman" href="http://jprart.deviantart.com/">View Image</a>--->
    
    <div id="dl">
		<cfif StructKeyExists(Session.programOverviewList,'PROJECTDESC_VCH')>
      <div class="dt" id="t1">Program Description<cfif structFind(Session.programOverviewList,"PROJECTDESC_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d1"><textarea name="PROJECTDESC_VCH" class="FullWidth" style="min-height:225px;"></textarea></div>
	</cfif>
	
	<cfif StructKeyExists(Session.programOverviewList,'TYPE_OF_CALL_VCH')>
      <div class="dt" id="t2">Type Of Call<cfif structFind(Session.programOverviewList,"TYPE_OF_CALL_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d2">
      <input type="radio" name="TYPE_OF_CALL_VCH" value="0" style="margin-right:10px;"<cfif TYPE_OF_CALL_VCH eq 0> checked="checked"</cfif>>Informational<br />
      <input type="radio" name="TYPE_OF_CALL_VCH" value="1" style="margin-right:10px;"<cfif TYPE_OF_CALL_VCH eq 1> checked="checked"</cfif>>Solicitation
      	 
      
      </div>
	</cfif>
      <cfif StructKeyExists(Session.programOverviewList,'PRECALL_TO_EMAIL_INT')>
      <div class="dt" id="t3">Call Type<cfif structFind(Session.programOverviewList,"PRECALL_TO_EMAIL_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d3">
      <input type="checkbox" name="PRECALL_TO_EMAIL_INT" style="margin-right:10px;"<cfif PRECALL_TO_EMAIL_INT eq 1> checked="checked"</cfif>>Support Deflection<br />
      <input type="checkbox" name="STANDALONE_CALL_INT" style="margin-right:10px;"<cfif STANDALONE_CALL_INT eq 1> checked="checked"</cfif>>Standalone<br />
      <input type="checkbox" name="PRECALL_TO_DIRECT_MAIL_INT" style="margin-right:10px;"<cfif PRECALL_TO_DIRECT_MAIL_INT eq 1> checked="checked"</cfif>>Pre Service Call<br />
      <input type="checkbox" name="POSTCALL_TO_DIRECT_MAIL_INT" style="margin-right:10px;"<cfif POSTCALL_TO_DIRECT_MAIL_INT eq 1> checked="checked"</cfif>>Post Service Call<br />
      <input type="checkbox" name="DRIVE_TO_WEB_INT" style="margin-right:10px;"<cfif DRIVE_TO_WEB_INT eq 1> checked="checked"</cfif>>Drive To Web<br />
      <input type="checkbox" name="INFORMATIONAL_INT" style="margin-right:10px;"<cfif INFORMATIONAL_INT eq 1> checked="checked"</cfif>>Survey<br />
      <input type="checkbox" name="OTHER_INT" style="margin-right:10px;"<cfif OTHER_INT eq 1> checked="checked"</cfif>>Other<br />
      Other Description:<br />
      <textarea name="OTHER_VCH" class="FullWidth"></textarea>
      	 
      
      </div>
	</cfif>
	
	<cfif StructKeyExists(Session.programOverviewList,'PLATFORM_ATT_INT')>
      <div class="dt" id="t4">Platform<cfif structFind(Session.programOverviewList,"PLATFORM_ATT_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d4">
      <input type="checkbox" name="PLATFORM_ATT_INT" style="margin-right:10px;"<cfif PLATFORM_ATT_INT eq 1> checked="checked"</cfif>>Infrastructure<br />
      <input type="checkbox" name="PLATFORM_MB_INT" style="margin-right:10px;"<cfif PLATFORM_MB_INT eq 1> checked="checked"</cfif>>MessageBroadcast (Turnkey)<br />
      <input type="checkbox" name="PLATFORM_EGS_INT" style="margin-right:10px;"<cfif PLATFORM_EGS_INT eq 1> checked="checked"</cfif>>EGS - Email
      </div>
	</cfif>
	<cfif StructKeyExists(Session.programOverviewList,'AL')>
      <div class="dt" id="t5">Geographic Impacts<cfif structFind(Session.programOverviewList,"AL") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d5" style="position:relative; display:inline-block;">
      	<div style="width:100%; margin-bottom:15px;">
        	<input type="checkbox" onclick="toggleChecked(this.checked)"> Select / De-Select All States
        </div>

      	<div style="float:left; width:100px;">
            <input type="checkbox" name="AL" class="pist" <cfif AL eq 1> checked="checked"</cfif>>AL<br />
            <input type="checkbox" name="AK" class="pist" <cfif AK eq 1> checked="checked"</cfif>>AK<br />
            <input type="checkbox" name="AZ" class="pist" <cfif AZ eq 1> checked="checked"</cfif>>AZ<br />
            <input type="checkbox" name="AR" class="pist" <cfif AR eq 1> checked="checked"</cfif>>AR<br />
            <input type="checkbox" name="CA" class="pist" <cfif CA eq 1> checked="checked"</cfif>>CA<br />
            <input type="checkbox" name="CO" class="pist" <cfif CO eq 1> checked="checked"</cfif>>CO<br />
            <input type="checkbox" name="CT" class="pist" <cfif CT eq 1> checked="checked"</cfif>>CT<br />
            <input type="checkbox" name="DE" class="pist" <cfif DE eq 1> checked="checked"</cfif>>DE<br />
            <input type="checkbox" name="FL" class="pist" <cfif FL eq 1> checked="checked"</cfif>>FL<br />
            <input type="checkbox" name="GA" class="pist" <cfif GA eq 1> checked="checked"</cfif>>GA<br />
            <input type="checkbox" name="HI" class="pist" <cfif HI eq 1> checked="checked"</cfif>>HI<br />
            <input type="checkbox" name="ID" class="pist" <cfif ID eq 1> checked="checked"</cfif>>ID<br />
            <input type="checkbox" name="IL" class="pist" <cfif IL eq 1> checked="checked"</cfif>>IL<br />
            <input type="checkbox" name="IN" class="pist" <cfif IN eq 1> checked="checked"</cfif>>IN<br />
            <input type="checkbox" name="IA" class="pist" <cfif IA eq 1> checked="checked"</cfif>>IA<br />
            <input type="checkbox" name="KS" class="pist" <cfif KS eq 1> checked="checked"</cfif>>KS<br />
            <input type="checkbox" name="KY" class="pist" <cfif KY eq 1> checked="checked"</cfif>>KY
        </div>
        <div style="float:left; width:100px; margin-left:15px;">
            <input type="checkbox" name="LA" class="pist" style="margin-right:10px;"<cfif LA eq 1> checked="checked"</cfif>>LA<br />
            <input type="checkbox" name="MD" class="pist" style="margin-right:10px;"<cfif MD eq 1> checked="checked"</cfif>>MD<br />
            <input type="checkbox" name="ME" class="pist" style="margin-right:10px;"<cfif ME eq 1> checked="checked"</cfif>>ME<br />
            <input type="checkbox" name="MI" class="pist" style="margin-right:10px;"<cfif MI eq 1> checked="checked"</cfif>>MI<br />
            <input type="checkbox" name="MN" class="pist" style="margin-right:10px;"<cfif MN eq 1> checked="checked"</cfif>>MN<br />
            <input type="checkbox" name="MS" class="pist" style="margin-right:10px;"<cfif MS eq 1> checked="checked"</cfif>>MS<br />
            <input type="checkbox" name="MO" class="pist" style="margin-right:10px;"<cfif MO eq 1> checked="checked"</cfif>>MO<br />
            <input type="checkbox" name="MT" class="pist" style="margin-right:10px;"<cfif MT eq 1> checked="checked"</cfif>>MT<br />
            <input type="checkbox" name="NE" class="pist" style="margin-right:10px;"<cfif NE eq 1> checked="checked"</cfif>>NE<br />
            <input type="checkbox" name="NV" class="pist" style="margin-right:10px;"<cfif NV eq 1> checked="checked"</cfif>>NV<br />
            <input type="checkbox" name="NH" class="pist" style="margin-right:10px;"<cfif NH eq 1> checked="checked"</cfif>>NH<br />
            <input type="checkbox" name="NJ" class="pist" style="margin-right:10px;"<cfif NJ eq 1> checked="checked"</cfif>>NJ<br />
            <input type="checkbox" name="NM" class="pist" style="margin-right:10px;"<cfif NM eq 1> checked="checked"</cfif>>NM<br />
            <input type="checkbox" name="NY" class="pist" style="margin-right:10px;"<cfif NY eq 1> checked="checked"</cfif>>NY<br />
            <input type="checkbox" name="NC" class="pist" style="margin-right:10px;"<cfif NC eq 1> checked="checked"</cfif>>NC<br />
            <input type="checkbox" name="ND" class="pist" style="margin-right:10px;"<cfif ND eq 1> checked="checked"</cfif>>ND
        </div>
        <div style="float:left; width:100px; margin-left:15px;">
            <input type="checkbox" name="OH" class="pist" style="margin-right:10px;"<cfif OH eq 1> checked="checked"</cfif>>OH<br />
            <input type="checkbox" name="OK" class="pist" style="margin-right:10px;"<cfif OK eq 1> checked="checked"</cfif>>OK<br />
            <input type="checkbox" name="zOR" class="pist" style="margin-right:10px;"<cfif zOR eq 1> checked="checked"</cfif>>OR<br />
            <input type="checkbox" name="PA" class="pist" style="margin-right:10px;"<cfif PA eq 1> checked="checked"</cfif>>PA<br />
            <input type="checkbox" name="RI" class="pist" style="margin-right:10px;"<cfif RI eq 1> checked="checked"</cfif>>RI<br />
            <input type="checkbox" name="SD" class="pist" style="margin-right:10px;"<cfif SD eq 1> checked="checked"</cfif>>SD<br />
            <input type="checkbox" name="SC" class="pist" style="margin-right:10px;"<cfif SC eq 1> checked="checked"</cfif>>SC<br />
            <input type="checkbox" name="TN" class="pist" style="margin-right:10px;"<cfif TN eq 1> checked="checked"</cfif>>TN<br />
            <input type="checkbox" name="TX" class="pist" style="margin-right:10px;"<cfif TX eq 1> checked="checked"</cfif>>TX<br />
            <input type="checkbox" name="UT" class="pist" style="margin-right:10px;"<cfif UT eq 1> checked="checked"</cfif>>UT<br />
            <input type="checkbox" name="VT" class="pist" style="margin-right:10px;"<cfif VT eq 1> checked="checked"</cfif>>VT<br />
            <input type="checkbox" name="VA" class="pist" style="margin-right:10px;"<cfif VA eq 1> checked="checked"</cfif>>VA<br />
            <input type="checkbox" name="WA" class="pist" style="margin-right:10px;"<cfif WA eq 1> checked="checked"</cfif>>WA<br />
            <input type="checkbox" name="WV" class="pist" style="margin-right:10px;"<cfif WV eq 1> checked="checked"</cfif>>WV<br />
            <input type="checkbox" name="WI" class="pist" style="margin-right:10px;"<cfif WI eq 1> checked="checked"</cfif>>WI<br />
            <input type="checkbox" name="WY" class="pist" style="margin-right:10px;"<cfif WY eq 1> checked="checked"</cfif>>WY
        </div>
      </div>
	</cfif>
	
	<cfif StructKeyExists(Session.programOverviewList,'CUSTOMER_BASE_CON_INT')>
      <div class="dt" id="t6">Customer Base<cfif structFind(Session.programOverviewList,"CUSTOMER_BASE_CON_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
      <div class="dd" id="d6">
      <input type="checkbox" name="CUSTOMER_BASE_CON_INT" style="margin-right:10px;"<cfif CUSTOMER_BASE_CON_INT eq 1> checked="checked"</cfif>>Consumer<br />
      <input type="checkbox" name="CUSTOMER_BASE_BUS_INT" style="margin-right:10px;"<cfif CUSTOMER_BASE_BUS_INT eq 1> checked="checked"</cfif>>Small Business
      </div>
	</cfif>
      

    </div>

</div>
</cfoutput>
</cfif>