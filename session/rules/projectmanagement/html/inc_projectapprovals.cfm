<cfquery name="getapprovals" datasource="#Session.DBSourceEBM#">
SELECT *
FROM simpleobjects.pm_approval
WHERE BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPBATCHID#">
</cfquery>
<cfset tableName="pm_approval">
<cfparam name="Session.projectApprovalsList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectApprovalsList"/>
<cfif not StructIsEmpty(Session.projectApprovalsList)>

<div class="info-col" id="PM_ProjectApprovals" rel="InitProjectApprovals">
    <div class="colhdr">Campaign Approvals</div>
    <div id="dl">
	<cfif Session.CollEditable eq 1>
    	<div class="dt" id="t1">Add New Approval</div>
        <div class="dd" id="d1" style="position:relative;">
        	<form action="" method="post" name="papprovals" id="papprovals" onsubmit="return NProjectApproval();">
            <input type="hidden" name="BatchID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
			<cfif StructKeyExists(Session.projectApprovalsList,'ApprovalName_vch')>
        	<div style="margin-bottom:10px; float:left;">
            Participants<br />Name:	<cfif structFind(Session.projectApprovalsList,"ApprovalName_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
				    	<br />
        	<input type="text" name="approvename" style="width:100px;" />
        	</div>
			</cfif>
			
			<cfif StructKeyExists(Session.projectApprovalsList,'ApprovalGroup_vch')>
            <div style="margin-bottom:10px; float:left; margin-left:5px;">
            Participants<br />Group:<cfif structFind(Session.projectApprovalsList,"ApprovalGroup_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif><br />
        	<input type="text" name="approvegroup" style="width:100px;" />
        	</div>
			</cfif>
            <!--- Create Automatically by combining the results code and the CRC together with a dash
			<div style="margin-bottom:10px; float:left; margin-left:5px;">
            Last call<br />disposition:<br />
        	<input type="text" name="element" style="width:70px;" />
        	</div>--->
			<cfif StructKeyExists(Session.projectApprovalsList,'ApprovalDate_dt')>
            <div style="margin:0 0 10px 5px; float:left;">
            Approval<br />Date:<cfif structFind(Session.projectApprovalsList,"ApprovalDate_dt") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif><br />
			<input name="dapprove" style="width:100px;" type="date" class="date" value="<cfoutput>#DateFormat(Now(),'mm/dd/yyyy')#</cfoutput>" />
        	</div>
			</cfif>
		
            <div style="margin:0 0 10px 5px; float:left;">
            <br />Action<br />
        	<input type="submit" name="addelement" value="Add Approval" />
        	</div>
		
            </form>
            
        </div>
			</cfif>
		<div class="dt" id="t2">Current Approvals</div>
		<div class="dd" id="d2">

			<cfdiv bind="url:../Rules/cfdiv/cfdiv_approvaldiv?BID=#INPBATCHID#" id="approvaldv" >


		</div>		
    </div>

</div>

</cfif>
