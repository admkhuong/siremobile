<cfset tableName="pm_change">
<cfparam name="Session.changeList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.changeList"/>

<cfoutput>
<div class="info-col" id="#tableName#" rel="InitProjectApprovals">
    <div class="chcolhdr">Project Update List</div>
    <div id="dl">
    	<div class="dt" id="t1">Changes by Date</div>
        <div class="dd" id="d1" style="position:relative;height:400px">
        	<cfdiv bind="url:../Rules/cfdiv/cfdiv_changes" id="changesdv" >
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_changesview?batchid=#INPBATCHID#" id="changesviewdv" >
        </div>
    </div>
</div>
</cfoutput>
