<cfset tableName="pm_projectdatafeeds">
<cfparam name="Session.projectDataFeedsList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectDataFeedsList"/>

<cfif not StructIsEmpty(Session.projectDataFeedsList)>
<cfoutput>
<div class="info-col" id="#tableName#" rel="Initprojectsettingsview">
    <div class="colhdr">Project Data Feeds</div>
    <div id="dl">
		<cfif StructKeyExists(Session.projectDataFeedsList,'DATA_REQUIREMENTS_VCH')>
    	<div class="dt" id="t1">Data Requirements<cfif structFind(Session.projectDataFeedsList,"DATA_REQUIREMENTS_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d1"><textarea name="DATA_REQUIREMENTS_VCH" class="FullWidth" style="min-height:225px;">#DATA_REQUIREMENTS_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectDataFeedsList,'IT_INTERFACE_SIGNOFF_VCH')>
        <div class="dt" id="t2">IT Interface Agreement<cfif structFind(Session.projectDataFeedsList,"IT_INTERFACE_SIGNOFF_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d2">
        	<div style="margin-bottom:10px;">
        	Will your company need to sign off on an IT Interface Agreement due to an IT effort to provide your company the datafeed?<br />
            <textarea name="IT_INTERFACE_SIGNOFF_VCH" class="FullWidth">#IT_INTERFACE_SIGNOFF_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectDataFeedsList,'CALL_RESULTS_PROCESSING_VCH')>
        <div class="dt" id="t3">Call Results Processing<cfif structFind(Session.projectDataFeedsList,"CALL_RESULTS_PROCESSING_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d3">
        	<div style="margin-bottom:10px;">
            Will your company be required to send call results back for processing on the client side?<br />
        	<textarea name="CALL_RESULTS_PROCESSING_VCH" class="FullWidth">#CALL_RESULTS_PROCESSING_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            If YES, Please Explain:<br />
        	<textarea name="CALL_YES_VCH" class="FullWidth" style="min-height:200px;">#CALL_YES_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'DATA_EXCHANGE_DEVELOP_VCH')>
        <div class="dt" id="t4">Data Exchange<cfif structFind(Session.projectDataFeedsList,"DATA_EXCHANGE_DEVELOP_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d4">
        	<div style="margin-bottom:10px;">
            Who should your company work with to develop the data exchange?<br />
        	<textarea name="DATA_EXCHANGE_DEVELOP_VCH" class="FullWidth">#DATA_EXCHANGE_DEVELOP_VCH#</textarea>
            </div>
        </div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'DATA_SOURCE_VCH')>
        <div class="dt" id="t5">Data Source<cfif structFind(Session.projectDataFeedsList,"DATA_SOURCE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d5">
        	<div style="margin-bottom:10px;">
            Will the Data Source be from Existing Customer Database or Number of Files to Receive?<br />
        	<textarea name="DATA_SOURCE_VCH" class="FullWidth">#DATA_SOURCE_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'DO_NOT_SCRUB_INT')>
        <div class="dt" id="t6">Scrubbing Parameters<cfif structFind(Session.projectDataFeedsList,"DO_NOT_SCRUB_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d6">
        	For Telemarketing campaigns please advise which States we are not allowed to dial.
        	<div style="margin-bottom:10px; margin-top:10px;">
             <input type="checkbox" name="DO_NOT_SCRUB_INT" style="margin-right:10px;"<cfif DO_NOT_SCRUB_INT eq 1> checked="checked"</cfif>>DO NOT SCRUB
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="STATE_DNC_LIST_INT" style="margin-right:10px;"<cfif STATE_DNC_LIST_INT eq 1> checked="checked"</cfif>>State/DMA DNC List
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="CLIENT_DNC_INT" style="margin-right:10px;"<cfif CLIENT_DNC_INT eq 1> checked="checked"</cfif>>Client DNC
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="DO_NOT_DIAL_STATES_INT" style="margin-right:10px;"<cfif DO_NOT_DIAL_STATES_INT eq 1> checked="checked"</cfif>>Do Not Dial States
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="NATIONAL_DNC_INT" style="margin-right:10px;"<cfif NATIONAL_DNC_INT eq 1> checked="checked"</cfif>>National DNC
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="COMPANY_INTERNAL_LIST_INT" style="margin-right:10px;"<cfif COMPANY_INTERNAL_LIST_INT eq 1> checked="checked"</cfif>>Company Internal List
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="CELL_PHONE_LIST_INT" style="margin-right:10px;"<cfif CELL_PHONE_LIST_INT eq 1> checked="checked"</cfif>>Cell Phone List
        	</div>
        </div> 
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'DATA_FILE_TRANSMISSION_VCH')>
        <div class="dt" id="t7">Data File Transmission<cfif structFind(Session.projectDataFeedsList,"DATA_FILE_TRANSMISSION_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d7"><textarea name="DATA_FILE_TRANSMISSION_VCH" class="FullWidth" style="min-height:225px;">#DATA_FILE_TRANSMISSION_VCH#</textarea></div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'FILTER_DUPLICATE_RECORDS_INT')>
        <div class="dt" id="t8">Duplicate Records<cfif structFind(Session.projectDataFeedsList,"FILTER_DUPLICATE_RECORDS_INT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d8">
        	Should your company filter duplicate records?
        	<div style="margin-bottom:10px; margin-top:10px;">
             Yes<input type="radio" name="FILTER_DUPLICATE_RECORDS_INT" value="1" style="margin-left:5px; margin-right:10px;"<cfif FILTER_DUPLICATE_RECORDS_INT eq 1> checked="checked"</cfif>></br>
			 No<input type="radio" name="FILTER_DUPLICATE_RECORDS_INT" value="0" style="margin-left:-31px;"<cfif FILTER_DUPLICATE_RECORDS_INT eq 0> checked="checked"</cfif>>
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="FILTER_WITHIN_FILE_INT" style="margin-right:10px;"<cfif FILTER_WITHIN_FILE_INT eq 1> checked="checked"</cfif>>Within File
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="FILTER_SAME_DAY_INT" style="margin-right:10px;"<cfif FILTER_SAME_DAY_INT eq 1> checked="checked"</cfif>>Same Day
        	</div>
            <div style="margin-bottom:10px;">
            <input type="checkbox" name="FILTER_X_AMOUNT_DAYS_INT" style="margin-right:10px;"<cfif FILTER_X_AMOUNT_DAYS_INT eq 1> checked="checked"</cfif>>X Amount Of Days</br>
			<input type="text" name="X_DAYS_NUMBER_INT" value="#X_DAYS_NUMBER_INT#" style="margin-right:10px; margin-left:3px;width:50px;float:left;"> Number Of Days
        	</div>
        </div> 
		</cfif>
		
		<cfif StructKeyExists(Session.projectDataFeedsList,'NOTES_VCH')>
        <div class="dt" id="t9">Notes<cfif structFind(Session.projectDataFeedsList,"NOTES_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d9"><textarea name="PDFNOTES_VCH" class="FullWidth" style="min-height:225px;">#PDFNOTES_VCH#</textarea></div>
		</cfif>
    </div>
</div>
</cfoutput>
</cfif>