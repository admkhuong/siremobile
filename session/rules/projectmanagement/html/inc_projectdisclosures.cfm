
<cfset tableName="pm_projectdisclosures">
<cfparam name="Session.projectDisclosuresList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectDisclosuresList"/>

<cfif not StructIsEmpty(Session.projectDisclosuresList)>
<cfoutput>
<div class="info-col" id="PM_ProjectDisclosures" rel="InitProjectDisclosures">
    <div class="colhdr">Project Disclosures<cfif structFind(Session.projectDisclosuresList,"Project_Disclosures_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
    <div id="dl">
    	<div class="dt" id="t1">Disclosure Information</div>
        <div class="dd" id="d1" style="position:relative;">
        	<textarea name="Project_Disclosures_vch" class="FullWidth" style="height:100px;">#Project_Disclosures_vch#</textarea>
        </div>
    </div>
</div>
</cfoutput>
</cfif>