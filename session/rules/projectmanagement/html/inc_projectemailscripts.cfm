<cfset tableName="pm_projectemailscripts">
<cfparam name="Session.projectEmailScriptsList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectEmailScriptsList"/>

<cfif not StructIsEmpty(Session.projectEmailScriptsList)>
<cfoutput>
<div class="info-col" id="#tableName#" rel="Initprojectsettingsview">
    <div class="colhdr">Project Email Scripts</div>
    <div id="dl">
		<cfif StructKeyExists(Session.projectEmailScriptsList,'EMAILPROJECTNAME_VCH')>
    	<div class="dt" id="t1">Email Information<cfif structFind(Session.projectEmailScriptsList,"EMAILPROJECTNAME_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d1">`
        	<div style="margin-bottom:10px;">
            Project Name:<br />
        	<textarea name="EMAILPROJECTNAME_VCH" class="FullWidth">#EMAILPROJECTNAME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            From Line:<br />
        	<textarea name="EMAILFROMLINE_VCH" class="FullWidth">#EMAILFROMLINE_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Subject Line:<br />
        	<textarea name="EMAILSUBJECTLINE_VCH" class="FullWidth">#EMAILSUBJECTLINE_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            TAG CODE:<br />
        	<textarea name="EMAILTAGCODE_VCH" class="FullWidth">#EMAILTAGCODE_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectEmailScriptsList,'EMAILDELIVERYATTEMPTS_VCH')>
        <div class="dt" id="t2">Delivery Requirements<cfif structFind(Session.projectEmailScriptsList,"EMAILDELIVERYATTEMPTS_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d2">
        	<div style="margin-bottom:10px;">
            Number Of Attempts:<br />
        	<textarea name="EMAILDELIVERYATTEMPTS_VCH" class="FullWidth">#EMAILDELIVERYATTEMPTS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Bounce Back Strategy:<br />
        	<textarea name="BOUNCEBACKSTRATEGY_VCH" class="FullWidth">#BOUNCEBACKSTRATEGY_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectEmailScriptsList,'ORIGINATINGFILENAME_VCH')>
        <div class="dt" id="t3">Data Feed Requirements<cfif structFind(Session.projectEmailScriptsList,"ORIGINATINGFILENAME_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d3">
        	<div style="margin-bottom:10px;">
            Originating File Name:<br />
        	<textarea name="ORIGINATINGFILENAME_VCH" class="FullWidth">#ORIGINATINGFILENAME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Export Data Feed:<br />
        	<textarea name="EXPORTDATEFEED_VCH" class="FullWidth">#EXPORTDATEFEED_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Data Feed Notes:<br />
        	<textarea name="DATAFEEDNOTES_VCH" class="FullWidth">#DATAFEEDNOTES_VCH#</textarea>
        	</div>
            <div style="margin-bottom:15px; position:relative; display:inline-block;">
            Interchangeable Data Fields:<br />
            	<form id="dfnew">
                <div style="float:left; width:100px;">
                	Fieldname:<br />
                	<input type="text" id="DATAFIELDNAME_VCH" style="width:100px;" />
                </div>
                <div style="float:left; width:200px; margin-left:10px;">
                	Field Description<br />
                	<textarea id="DATAFIELDDESC_VCH" style="width:200px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="datafieldbutton" value="add new" style="padding:3px; margin-top:20px;width: 80px;" onclick="return NDataFields('dfnew');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_datafields?BID=#INPBATCHID#" id="datafieldsdv" >
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectEmailScriptsList,'EMAILHEADERLINK_VCH')>
        <div class="dt" id="t4">Hyperlink Details<cfif structFind(Session.projectEmailScriptsList,"EMAILHEADERLINK_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d4">
        	<div style="margin-bottom:10px; position:relative; width:100%; display:inline-block;">
            Email Header:<br />
            	<form id="nehd">
            	<div style="float:left; width:175px;">
                	Link:<br />
                	<textarea id="EMAILHEADERLINK_VCH" style="width:175px;" ></textarea>
                </div>
                <div style="float:left; width:175px; margin-left:10px;">
                	Routes To:<br />
                	<textarea id="EMAILHEADERROUTE_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="addnew" value="add new" style="padding:2px; margin-top:15px; width: 80px;" onclick="return NEmailHeader('nehd');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_emailheaders?BID=#INPBATCHID#" id="emailheaderdv" style="border-bottom:3px solid ##666;" >
            <div style="margin-bottom:10px; position:relative; width:100%; display:inline-block;">
            Email Body:<br />
            	<form id="nebd">
            	<div style="float:left; width:175px;">
                	Link:<br />
                	<textarea id="EMAILBODYLINK_VCH" style="width:175px;" ></textarea>
                </div>
                <div style="float:left; width:175px; margin-left:10px;">
                	Routes To:<br />
                	<textarea id="EMAILBODYROUTE_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="addnew" value="add new" style="padding:2px; margin-top:15px;width: 80px;" onclick="return NEmailBody('nebd');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_emailbody?BID=#INPBATCHID#" id="emailbodydv" style="border-bottom:3px solid ##666;">
            <div style="margin-bottom:10px; position:relative; width:100%; display:inline-block;">
            Email Online Support:<br />
            	<form id="neod">
            	<div style="float:left; width:175px;">
                	Link:<br />
                	<textarea id="EMAILONLINESUPPORTLINK_VCH" style="width:175px;" ></textarea>
                </div>
                <div style="float:left; width:175px; margin-left:10px;">
                	Routes To:<br />
                	<textarea id="EMAILONLINESUPPORTROUTE_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="addnew" value="add new" style="padding:2px; margin-top:15px; width: 80px;" onclick="return NEmailOnline('neod');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_emailonlinesupport?BID=#INPBATCHID#" id="emailonlinesupportdv" style="border-bottom:3px solid ##666;" >
            <div style="margin-bottom:10px; position:relative; width:100%; display:inline-block;">
            Email Footer:<br />
            	<form id="nefd">
            	<div style="float:left; width:175px;">
                	Link:<br />
                	<textarea id="EMAILFOOTERLINK_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:175px; margin-left:10px;">
                	Routes To:<br />
                	<textarea id="EMAILFOOTERROUTE_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="addnew" value="add new" style="padding:2px; margin-top:15px;" onclick="return NEmailFooter('nefd');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_emailfooter?BID=#INPBATCHID#" id="emailfooterdv" style="border-bottom:3px solid ##666;">
            <div style="margin-bottom:10px; position:relative; width:100%; display:inline-block;">
            Email Right Side:<br />
            	<form id="nerd">
            	<div style="float:left; width:175px;">
                	Link:<br />
                	<textarea id="EMAILRIGHTLINK_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:175px; margin-left:10px;">
                	Routes To:<br />
                	<textarea id="EMAILRIGHTROUTE_VCH" style="width:175px;"></textarea>
                </div>
                <div style="float:left; width:40px; margin-left:10px;">
                	<input type="button" name="addnew" value="add new" style="padding:2px; margin-top:15px;" onclick="return NEmailRight('nerd');" />
                </div>
                </form>
        	</div>
            <cfdiv bind="url:../Rules/cfdiv/cfdiv_emailrightside?BID=#INPBATCHID#" id="emailrightsidedv" >
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectEmailScriptsList,'EMAILHTMLTEMPLATE_VCH')>
        <div class="dt" id="t5">Email HTML Template<cfif structFind(Session.projectEmailScriptsList,"EMAILHTMLTEMPLATE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d5"><textarea name="EMAILHTMLTEMPLATE_VCH" class="FullWidth" style="min-height:275px;">#EMAILHTMLTEMPLATE_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectEmailScriptsList,'EMAILTEXTTEMPLATE_VCH')>
        <div class="dt" id="t6">Email TEXT Template<cfif structFind(Session.projectEmailScriptsList,"EMAILTEXTTEMPLATE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif></div>
        <div class="dd" id="d6"><textarea name="EMAILTEXTTEMPLATE_VCH" class="FullWidth" style="min-height:275px;">#EMAILTEXTTEMPLATE_VCH#</textarea></div>
        </cfif>
        
    </div>
</div>
</cfoutput>
</cfif>