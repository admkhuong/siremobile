<style>
.print-checkbox
{
	width: 19px;
	height: 25px;
	padding: 0 5px 0 0;
	background: url(../Rules/ProjectManagement/html/checkbox.png) no-repeat;
	display: block;
	clear: left;
	float: right;
}
</style>


<cfset tableName="PM_projectinformation">
<cfparam name="Session.projectInfoList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectInfoList"/>
<cfif not StructIsEmpty(Session.projectInfoList)>
<cfoutput>

<div class="info-col" id="#tableName#" rel="InitContactInfoview">
    <div class="colhdr">Project Information
	</div>
    <div id="dl">
		<cfif StructKeyExists(Session.projectInfoList,'PROJECTID_VCH')>
	    	<div class="dt" id="t1">Project ID
		    	<cfif structFind(Session.projectInfoList,"PROJECTID_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
			</div>
	        <div class="dd" id="d1"><textarea name="PROJECTID_VCH" class="FullWidth">#PROJECTID_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'VERSION_VCH')>
	        <div class="dt" id="t2">Version
		    	<cfif structFind(Session.projectInfoList,"VERSION_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
			</div>
	        <div class="dd" id="d2"><textarea name="VERSION_VCH" class="FullWidth">#VERSION_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'PLATFORM_TYPE_VCH')>
	        <div class="dt" id="t3">Platform Type
					    	<cfif structFind(Session.projectInfoList,"PLATFORM_TYPE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
	        <div class="dd" id="d3"><textarea name="PLATFORM_TYPE_VCH" class="FullWidth">#PLATFORM_TYPE_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'PROJECT_DURATION_VCH')>
	        <div class="dt" id="t4">Project Duration
					    	<cfif structFind(Session.projectInfoList,"PROJECT_DURATION_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
	        <div class="dd" id="d4"><textarea name="PROJECT_DURATION_VCH" class="FullWidth">#PROJECT_DURATION_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'SUBMISSION_DATE_DT')>
	        <div class="dt" id="t5">Project Dates
					    	<cfif structFind(Session.projectInfoList,"SUBMISSION_DATE_DT") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
	        <div class="dd" id="d5">
	        	<cfoutput>
	            <div style="margin-bottom:10px;">
	            Submission Date:<br />
	        	<input name="SUBMISSION_DATE_DT" type="date" class="date" value="#DateFormat((SUBMISSION_DATE_DT),'mm/dd/yyyy')#">
	        	</div>


	            <div style="margin-bottom:10px;">
	            Project Start Date:<br />
	        	<input name="PROJECT_START_DATE_DT" type="date" class="date" value="#DateFormat((PROJECT_START_DATE_DT),'mm/dd/yyyy')#">
	        	</div>


	            <div style="margin-bottom:10px;">
	            Project End Date:<br />
	        	<input name="PROJECT_END_DATE_DT" type="date" class="date" value="#DateFormat((PROJECT_END_DATE_DT),'mm/dd/yyyy')#">
	        	</div>

	        	</cfoutput>
	        </div>
	    </cfif>
		<cfif StructKeyExists(Session.projectInfoList,'PREPARED_BY_VCH')>
        <div class="dt" id="t6">Prepared By
				    	<cfif structFind(Session.projectInfoList,"PREPARED_BY_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
        <div class="dd" id="d6"><textarea name="PREPARED_BY_VCH" class="FullWidth">#PREPARED_BY_VCH#</textarea></div> 
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'EMAIL_PHONE_VCH')>
        <div class="dt" id="t7">Email/Phone Information
				    	<cfif structFind(Session.projectInfoList,"EMAIL_PHONE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
        <div class="dd" id="d7"><textarea name="EMAIL_PHONE_VCH" class="FullWidth">#EMAIL_PHONE_VCH#</textarea></div> 
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'PROJECTLEADCONTACT_VCH')>
        <div class="dt" id="t8">Project Contacts
				    	<cfif structFind(Session.projectInfoList,"PROJECTLEADCONTACT_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
		</cfif>
		
        <div class="dd" id="d8">
			<cfif StructKeyExists(Session.projectInfoList,'PROJECTLEADCONTACT_VCH')>
        	<div style="margin-bottom:10px;">
            Project Lead Contact:<br />
        	<textarea name="PROJECTLEADCONTACT_VCH" class="FullWidth">#PROJECTLEADCONTACT_VCH#</textarea>
        	</div>
			</cfif>
			<cfif StructKeyExists(Session.projectInfoList,'SCRIPT_CONTENT_CONTACT_VCH')>
            <div style="margin-bottom:10px;">
            Script Content Contact:<br />
        	<textarea name="SCRIPT_CONTENT_CONTACT_VCH" class="FullWidth">#SCRIPT_CONTENT_CONTACT_VCH#</textarea>
        	</div>
			</cfif>
			<cfif StructKeyExists(Session.projectInfoList,'DATA_FEED_CONTACT_VCH')>
            <div style="margin-bottom:10px;">
            Data Feed Contact:<br />
        	<textarea name="DATA_FEED_CONTACT_VCH" class="FullWidth">#DATA_FEED_CONTACT_VCH#</textarea>
        	</div>
			</cfif>
			<cfif StructKeyExists(Session.projectInfoList,'MANDP_CONTACT_VCH')>
        	<div style="margin-bottom:10px;">
            M&P Contact:<br />
        	<textarea name="MANDP_CONTACT_VCH" class="FullWidth">#MANDP_CONTACT_VCH#</textarea>
        	</div>
			</cfif>
        </div>
		<cfif StructKeyExists(Session.projectInfoList,'PROJECT_TEAM_VCH')>
        <div class="dt" id="t9">Project Team Members
				    	<cfif structFind(Session.projectInfoList,"PROJECT_TEAM_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
        <div class="dd" id="d9"><textarea name="PROJECT_TEAM_VCH" class="FullWidth">#PROJECT_TEAM_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectInfoList,'COMMUNICATIONS_ALERT_VCH')>
        <div class="dt" id="t10">Communications Alerts
				    	<cfif structFind(Session.projectInfoList,"COMMUNICATIONS_ALERT_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>	</div>
        <div class="dd" id="d10"><textarea name="COMMUNICATIONS_ALERT_VCH" class="FullWidth">#COMMUNICATIONS_ALERT_VCH#</textarea></div>
		</cfif>
    </div>
</div>
</cfoutput>
</cfif>