<cfquery name="getmcrc" datasource="#Session.DBSourceEBM#">
SELECT *
FROM simpleobjects.pm_maincrc
WHERE BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#INPBATCHID#">
</cfquery>
<cfset tableName="pm_maincrc" />
<cfparam name="Session.projectMainCRCList" default="#StructNew()#"/>
<cfset args=StructNew() />
<cfset args.inpUserId="#Session.USERID#" />
<cfset args.inpTableName="#tableName#" />
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectMainCRCList"/>
<cfif not StructIsEmpty(Session.projectMainCRCList)>
	<div class="info-col" id="PM_ProjectMainCallDiposition" rel="InitProjectMainCallDiposition">
		<div class="colhdr">Main Call CRC's List</div>
		<div id="dl">
			<cfif Session.CollEditable eq 1>
			<div class="dt" id="t1">Add New Call Element</div>
			<div class="dd" id="d1" style="position:relative;">
				<form action="" method="post" name="acrc" id="acrc" onsubmit="return NDataCRC();">
					<input type="hidden" name="BatchID_bi" value="<cfoutput>#INPBATCHID#</cfoutput>" />
					<cfif StructKeyExists(Session.projectMainCRCList,'Code_vch')>
					<div style="margin-bottom:10px; float:left;">
						Results
						<br />
						Code:<cfif structFind(Session.projectMainCRCList,"Code_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif>
						<br />
						<input type="text" name="Code_vch" style="width:70px;" />
					</div>
					</cfif>
					<cfif StructKeyExists(Session.projectMainCRCList,'CRC_vch')>
					<div style="margin-bottom:10px; float:left; margin-left:5px;">
						<br />
						CRC:<cfif structFind(Session.projectMainCRCList,"CRC_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
						<br />
						<input type="text" name="CRC_vch" style="width:70px;" />
					</div>
					</cfif>
					<!--- Create Automatically by combining the results code and the CRC together with a dash
						<div style="margin-bottom:10px; float:left; margin-left:5px;">
						Last call<br />disposition:<br />
						<input type="text" name="element" style="width:70px;" />
						</div>--->
					<cfif StructKeyExists(Session.projectMainCRCList,'Definition_vch')>	
					<div style="margin:0 0 10px 5px; float:left;">
						<br />
						Definition:<cfif structFind(Session.projectMainCRCList,"Definition_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    </cfif>
						<br />
						<textarea name="Definition_vch" style="width:200px;"></textarea>
					</div>
					</cfif>
					
					<div style="margin:0 0 10px 5px; float:left;">
						<br />
						Action
						<br />
						<input type="submit" name="addelement" value="Add New" />
					</div>
					
				</form>
			</div>
			</cfif>
			<div class="dt" id="t2">Current Call Elements</div>
			<div class="dd" id="d2">
				<div id="maincrc">
					<cfdiv bind="url:../Rules/cfdiv/cfdiv_maincrc?BID=#INPBATCHID#" id="maincrc" >
				</div>	
			</div>
		</div>
	</div>
</cfif>
