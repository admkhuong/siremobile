<cfajaximport tags="cfdiv">
<cfquery name="getmde" datasource="#Session.DBSourceEBM#">
	SELECT * FROM simpleobjects.pm_maindataelements WHERE BatchId_bi = 
	<cfqueryparam cfsqltype="cf_sql_integer" value="#INPBATCHID#">
</cfquery>
<cfset tableName="pm_maindataelements" />
<cfparam name="Session.projectMainCallElementList" default="#StructNew()#"/>
<cfset args=StructNew() />
<cfset args.inpUserId="#Session.USERID#" />
<cfset args.inpTableName="#tableName#" />
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectMainCallElementList"/>

<script>
		function updateTask(inpElementId){
			var elementId='element'+inpElementId;
			var inpElement=document.getElementById(elementId).value;
			var elementDescId='elementDesc'+inpElementId;
			var inpElementDesc=document.getElementById(elementDescId).value;
		if(inpElement == "" || inpElement==undefined)
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid value\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							
			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Rules/cfc/emailproject.cfc?method=UpdMainCallElementsRow&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpElementId : inpElementId,inpElement : inpElement,inpElementDesc:inpElementDesc}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Element has been updated.", "Success!", function(result) {} );
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Element has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;			
			
	}	
</script>
<cfif not StructIsEmpty(Session.projectMainCallElementList)>
<div class="info-col" id="PM_ProjectMainDataElements" rel="InitProjectMainDataElements">
    <div class="colhdr">Main Call Data Elements</div>
    <div id="dl">
	<cfif Session.CollEditable eq 1>
    	<div class="dt" id="t1">Add New Data Element</div>
        <div class="dd" id="d1" style="position:relative;">
        	<div style="position:relative; display:inline-block;">
        		<form action="" method="post" name="aelement" id="aelement" onsubmit="return NDataElement();">
            		<input type="hidden" name="BatchID_bi" value="<cfoutput>#INPBATCHID#</cfoutput>" />
        			<div style="margin-bottom:10px; float:left;">
            		Element:<br />
        			<input type="text" name="element" style="width:100px;" />
        			</div>
            		<div style="margin:0 0 10px 10px; float:left;">
            		Description:<br />
        			<textarea name="elementdesc" style="width:200px;"></textarea>
        			</div>
		
           			<div style="margin:0 0 10px 10px; float:left;">
            		Action<br/>
        			<input type="submit" name="addelement" value="Add Element" />
        			</div>

            	</form>
            </div>
        </div>
				</cfif>
        <div class="dt" id="t2">Current Data Elements</div>
        <div class="dd" id="d2">

			<cfdiv bind="url:../Rules/cfdiv/cfdiv_maindataelement?BID=#INPBATCHID#" id="mainde" >


        </div>
       
    </div>
</div>
</cfif>