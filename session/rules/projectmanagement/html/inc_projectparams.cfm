<cfquery name="GetCampaignInfo" datasource="#Session.DBSourceEBM#">
    SELECT * 
    FROM
    	simpleobjects.pm_projectinformation left join simpleobjects.pm_programoverview on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_programoverview.batchid_bi
        									left join simpleobjects.pm_programcomplexity on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_programcomplexity.batchid_bi
        									left join simpleobjects.pm_projectsettings on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectsettings.batchid_bi
                                            left join simpleobjects.pm_projectscriptinfo on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectscriptinfo.batchid_bi
                                            left join simpleobjects.pm_projectemailscripts on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectemailscripts.batchid_bi
                                            left join simpleobjects.pm_projectdatafeeds on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectdatafeeds.batchid_bi
                                            left join simpleobjects.pm_projectreportinfo on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectreportinfo.batchid_bi
                                            left join simpleobjects.pm_projectdisclosures on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_projectdisclosures.batchid_bi
                                            left join simpleobjects.pm_scriptstollfree on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_scriptstollfree.batchid_bi
                                            left join simpleobjects.pm_scriptstransfer on simpleobjects.pm_projectinformation.batchid_bi = simpleobjects.pm_scriptstransfer.batchid_bi
    WHERE
        simpleobjects.pm_projectinformation.batchid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPBATCHID#">
    </cfquery>
	
    <!---<cfoutput>RECORDCOUNT = #GetCampaignInfo.Recordcount#</cfoutput>--->
    
    <cfif GetCampaignInfo.Recordcount neq 0>
    	<cfoutput query="GetCampaignInfo">
    
			<!--- Project Info --->
            <cfset ProjectID_vch = ProjectID_vch>
            <cfset VERSION_VCH = VERSION_VCH>
            <cfset PLATFORM_TYPE_VCH = PLATFORM_TYPE_VCH>
            <cfset PROJECT_DURATION_VCH = PROJECT_DURATION_VCH>
            <cfset SUBMISSION_DATE_DT = SUBMISSION_DATE_DT>
            <cfset PROJECT_START_DATE_DT = PROJECT_START_DATE_DT>
            <cfset PROJECT_END_DATE_DT = PROJECT_END_DATE_DT>
            <cfset PREPARED_BY_VCH = PREPARED_BY_VCH>
            <cfset EMAIL_PHONE_VCH = EMAIL_PHONE_VCH>
            <cfset PROJECTLEADCONTACT_VCH = PROJECTLEADCONTACT_VCH>
            <cfset SCRIPT_CONTENT_CONTACT_VCH = SCRIPT_CONTENT_CONTACT_VCH>
            <cfset DATA_FEED_CONTACT_VCH = DATA_FEED_CONTACT_VCH>
            <cfset MANDP_CONTACT_VCH = MANDP_CONTACT_VCH>
            <cfset PROJECT_TEAM_VCH = PROJECT_TEAM_VCH>
            <cfset COMMUNICATIONS_ALERT_VCH = COMMUNICATIONS_ALERT_VCH>
            
            <!--- Project Overview --->
            <cfset PROJECTDESC_VCH = PROJECTDESC_VCH>
            <cfset TYPE_OF_CALL_VCH = TYPE_OF_CALL_VCH>
            <cfset PRECALL_TO_EMAIL_INT = PRECALL_TO_EMAIL_INT>
            <cfset STANDALONE_CALL_INT = STANDALONE_CALL_INT>
            <cfset PRECALL_TO_DIRECT_MAIL_INT = PRECALL_TO_DIRECT_MAIL_INT>
            <cfset POSTCALL_TO_DIRECT_MAIL_INT = POSTCALL_TO_DIRECT_MAIL_INT>
            <cfset DRIVE_TO_WEB_INT = DRIVE_TO_WEB_INT>
            <cfset INFORMATIONAL_INT = INFORMATIONAL_INT>
            <cfset OTHER_INT = OTHER_INT>
            <cfset OTHER_VCH = OTHER_VCH>
            <cfset PLATFORM_ATT_INT = PLATFORM_ATT_INT>
            <cfset PLATFORM_MB_INT = PLATFORM_MB_INT>
            <cfset PLATFORM_EGS_INT = PLATFORM_EGS_INT>
            <cfset CUSTOMER_BASE_CON_INT = CUSTOMER_BASE_CON_INT>
            <cfset CUSTOMER_BASE_BUS_INT = CUSTOMER_BASE_BUS_INT>
            
            <cfset AL = AL>
            <cfset AK = AK>
            <cfset AZ = AZ>
            <cfset AR = AR>
            <cfset CA = CA>
            <cfset CO = CO>
            <cfset CT = CT>
            <cfset DE = DE>
            <cfset FL = FL>
            <cfset GA = GA>
            <cfset HI = HI>
            <cfset ID = ID>
            <cfset IL = IL>
            <cfset IN = nIN>
            <cfset IA = IA>
            <cfset KS = KS>
            <cfset KY = KY>
            <cfset LA = LA>
            <cfset MA = MA>
            <cfset MD = MD>
            <cfset ME = ME>
            <cfset MI = MI>
            <cfset MN = MN>
            <cfset MS = MS>
            <cfset MO = MO>
            <cfset MT = MT>
            <cfset NE = NE>
            <cfset NV = NV>
            <cfset NH = NH>
            <cfset NJ = NJ>
            <cfset NM = NM>
            <cfset NY = NY>
            <cfset NC = NC>
            <cfset ND = ND>
            <cfset OH = OH>
            <cfset OK = OK>
            <cfset zOR = zOR>
            <cfset PA = PA>
            <cfset RI = RI>
            <cfset SD = SD>
            <cfset SC = SC>
            <cfset TN = TN>
            <cfset TX = TX>
            <cfset UT = UT>
            <cfset VT = VT>
            <cfset VA = VA>
            <cfset WA = WA>
            <cfset WV = WV>
            <cfset WI = WI>
            <cfset WY = WY>
            
            <!--- Project Complexity --->
            <cfset PROGRAM_COMPLEXITY_VCH = PROGRAM_COMPLEXITY_VCH>
            <cfset MONITORING_THRESHOLD_INT = MONITORING_THRESHOLD_INT>
            <cfset MONDAY_VCH = MONDAY_VCH>
            <cfset TUESDAY_VCH = TUESDAY_VCH>
            <cfset WEDNESDAY_VCH = WEDNESDAY_VCH>
            <cfset THURSDAY_VCH = THURSDAY_VCH>
            <cfset FRIDAY_VCH = FRIDAY_VCH>
            <cfset SATURDAY_VCH = SATURDAY_VCH >
            <cfset SUNDAY_VCH = SUNDAY_VCH>
            
            <!--- Project Settings --->
            <cfset MFDIALINGHOURS_VCH = MFDIALINGHOURS_VCH>
            <cfset SATURDAYDIALINGHOURS_VCH = SATURDAYDIALINGHOURS_VCH>
            <cfset SUNDAYDIALINGHOURS_VCH = SUNDAYDIALINGHOURS_VCH>
            <cfset HOLIDAYDIALINGHOURS_VCH = HOLIDAYDIALINGHOURS_VCH>
            <cfset DAILYCALLVOLUMES_VCH = DAILYCALLVOLUMES_VCH>
            <cfset CONSUMERCALLERID_VCH = CONSUMERCALLERID_VCH>
            <cfset BUSINESSCALLERID_VCH = BUSINESSCALLERID_VCH>
            <cfset SPANISHOPTION_VCH = SPANISHOPTION_VCH>
            <cfset ALTERNATENUMBERDIALING_VCH = ALTERNATENUMBERDIALING_VCH>
            <cfset PHONENUMTOBEDIALED_VCH = PHONENUMTOBEDIALED_VCH>
            <cfset REDIALLOGIC_VCH = REDIALLOGIC_VCH>
            <cfset REDIALATTEMPTS_VCH = REDIALATTEMPTS_VCH>
            <cfset ROLLOVERDIALING_VCH = ROLLOVERDIALING_VCH>
            <cfset ANSWERMACHINESTRATEGY_VCH = ANSWERMACHINESTRATEGY_VCH>
            <cfset MAXATTEMPTS_INT = MAXATTEMPTS_INT>
            <cfset RINGCOUNT_INT = RINGCOUNT_INT>
            <cfset ANSWERMACHINEWAIT_VCH = ANSWERMACHINEWAIT_VCH>
            <cfset BUSYCALLBACKTIME_VCH = BUSYCALLBACKTIME_VCH>
            <cfset BCBTIMEREDIALTIME_VCH = BCBTIMEREDIALTIME_VCH>
            <cfset NOANSWERCALLBACKTIME_VCH = NOANSWERCALLBACKTIME_VCH>
            <cfset NACBTIMEREDIALTIME_VCH = NACBTIMEREDIALTIME_VCH>
            <cfset OTHERFAX_VCH = OTHERFAX_VCH>
            <cfset OFTIMEREDIALTIME_VCH = OFTIMEREDIALTIME_VCH>
        
            <!--- Project Voice Scripts --->
            <cfset SCRIPT_INFORMATION_VCH = SCRIPT_INFORMATION_VCH>
            <cfset VOICE_TALENT_VCH = VOICE_TALENT_VCH>
            <cfset BENEFITS_VCH = BENEFITS_VCH>
            <cfset PHRASES_VCH = PHRASES_VCH>
            <cfset TONALITY_VCH = TONALITY_VCH>
            <cfset NOTES_VCH = NOTES_VCH>
            <cfset CALLCENTERSALES_VCH = CALLCENTERSALES_VCH>
            <cfset INBOUNDCALLSMETHOD_VCH = INBOUNDCALLSMETHOD_VCH>
            <cfset SCRIPTTOAPPROVE_VCH = SCRIPTTOAPPROVE_VCH>
            <cfset SCRIPTAPPROVEDBY_VCH = SCRIPTAPPROVEDBY_VCH>
            <cfset SCRIPTAPPROVEDDATE_VCH = SCRIPTAPPROVEDDATE_VCH>
            
            <cfset ALTF = ALTF_vch>
            <cfset AKTF = AKTF_vch>
            <cfset AZTF = AZTF_vch>
            <cfset ARTF = ARTF_vch>
            <cfset CATF = CATF_vch>
            <cfset COTF = COTF_vch>
            <cfset CTTF = CTTF_vch>
            <cfset DETF = DETF_vch>
            <cfset FLTF = FLTF_vch>
            <cfset GATF = GATF_vch>
            <cfset HITF = HITF_vch>
            <cfset IDTF = IDTF_vch>
            <cfset ILTF = ILTF_vch>
            <cfset INTF = INTF_vch>
            <cfset IATF = IATF_vch>
            <cfset KSTF = KSTF_vch>
            <cfset KYTF = KYTF_vch>
            <cfset LATF = LATF_vch>
            <cfset MATF = MATF_vch>
            <cfset MDTF = MDTF_vch>
            <cfset METF = METF_vch>
            <cfset MITF = MITF_vch>
            <cfset MNTF = MNTF_vch>
            <cfset MSTF = MSTF_vch>
            <cfset MOTF = MOTF_vch>
            <cfset MTTF = MTTF_vch>
            <cfset NETF = NETF_vch>
            <cfset NVTF = NVTF_vch>
            <cfset NHTF = NHTF_vch>
            <cfset NJTF = NJTF_vch>
            <cfset NMTF = NMTF_vch>
            <cfset NYTF = NYTF_vch>
            <cfset NCTF = NCTF_vch>
            <cfset NDTF = NDTF_vch>
            <cfset OHTF = OHTF_vch>
            <cfset OKTF = OKTF_vch>
            <cfset ORTF = ORTF_vch>
            <cfset PATF = PATF_vch>
            <cfset RITF = RITF_vch>
            <cfset SDTF = SDTF_vch>
            <cfset SCTF = SCTF_vch>
            <cfset TNTF = TNTF_vch>
            <cfset TXTF = TXTF_vch>
            <cfset UTTF = UTTF_vch>
            <cfset VTTF = VTTF_vch>
            <cfset VATF = VATF_vch>
            <cfset WATF = WATF_vch>
            <cfset WVTF = WVTF_vch>
            <cfset WITF = WITF_vch>
            <cfset WYTF = WYTF_vch>
            
            
            
            <cfset ALCTF_VCH = ALCTF_VCH>
            <cfset AKCTF_VCH = AKCTF_VCH>
            <cfset AZCTF_VCH = AZCTF_VCH>
            <cfset ARCTF_VCH = ARCTF_VCH>
            <cfset CACTF_VCH = CACTF_VCH>
            <cfset COCTF_VCH = COCTF_VCH>
            <cfset CTCTF_VCH = CTCTF_VCH>
            <cfset DECTF_VCH = DECTF_VCH>
            <cfset FLCTF_VCH = FLCTF_VCH>
            <cfset GACTF_VCH = GACTF_VCH>
            <cfset HICTF_VCH = HICTF_VCH>
            <cfset IDCTF_VCH = IDCTF_VCH>
            <cfset ILCTF_VCH = ILCTF_VCH>
            <cfset INCTF_VCH = INCTF_VCH>
            <cfset IACTF_VCH = IACTF_VCH>
            <cfset KSCTF_VCH = KSCTF_VCH>
            <cfset KYCTF_VCH = KYCTF_VCH>
            <cfset LACTF_VCH = LACTF_VCH>
            <cfset MACTF_VCH = MACTF_VCH>
            <cfset MDCTF_VCH = MDCTF_VCH>
            <cfset MECTF_VCH = MECTF_VCH>
            <cfset MICTF_VCH = MICTF_VCH>
            <cfset MNCTF_VCH = MNCTF_VCH>
            <cfset MSCTF_VCH = MSCTF_VCH>
            <cfset MOCTF_VCH = MOCTF_VCH>
            <cfset MTCTF_VCH = MTCTF_VCH>
            <cfset NECTF_VCH = NECTF_VCH>
            <cfset NVCTF_VCH = NVCTF_VCH>
            <cfset NHCTF_VCH = NHCTF_VCH>
            <cfset NJCTF_VCH = NJCTF_VCH>
            <cfset NMCTF_VCH = NMCTF_VCH>
            <cfset NYCTF_VCH = NYCTF_VCH>
            <cfset NCCTF_VCH = NCCTF_VCH>
            <cfset NDCTF_VCH = NDCTF_VCH>
            <cfset OHCTF_VCH = OHCTF_VCH>
            <cfset OKCTF_VCH = OKCTF_VCH>
            <cfset ORCTF_VCH = ORCTF_VCH>
            <cfset PACTF_VCH = PACTF_VCH>
            <cfset RICTF_VCH = RICTF_VCH>
            <cfset SDCTF_VCH = SDCTF_VCH>
            <cfset SCCTF_VCH = SCCTF_VCH>
            <cfset TNCTF_VCH = TNCTF_VCH>
            <cfset TXCTF_VCH = TXCTF_VCH>
            <cfset UTCTF_VCH = UTCTF_VCH>
            <cfset VTCTF_VCH = VTCTF_VCH>
            <cfset VACTF_VCH = VACTF_VCH>
            <cfset WACTF_VCH = WACTF_VCH>
            <cfset WVCTF_VCH = WVCTF_VCH>
            <cfset WICTF_VCH = WICTF_VCH>
            <cfset WYCTF_VCH = WYCTF_VCH>
            
            <cfset ALBTF_VCH = ALBTF_VCH>
            <cfset AKBTF_VCH = AKBTF_VCH>
            <cfset AZBTF_VCH = AZBTF_VCH>
            <cfset ARBTF_VCH = ARBTF_VCH>
            <cfset CABTF_VCH = CABTF_VCH>
            <cfset COBTF_VCH = COBTF_VCH>
            <cfset CTBTF_VCH = CTBTF_VCH>
            <cfset DEBTF_VCH = DEBTF_VCH>
            <cfset FLBTF_VCH = FLBTF_VCH>
            <cfset GABTF_VCH = GABTF_VCH>
            <cfset HIBTF_VCH = HIBTF_VCH>
            <cfset IDBTF_VCH = IDBTF_VCH>
            <cfset ILBTF_VCH = ILBTF_VCH>
            <cfset INBTF_VCH = INBTF_VCH>
            <cfset IABTF_VCH = IABTF_VCH>
            <cfset KSBTF_VCH = KSBTF_VCH>
            <cfset KYBTF_VCH = KYBTF_VCH>
            <cfset LABTF_VCH = LABTF_VCH>
            <cfset MABTF_VCH = MABTF_VCH>
            <cfset MDBTF_VCH = MDBTF_VCH>
            <cfset MEBTF_VCH = MEBTF_VCH>
            <cfset MIBTF_VCH = MIBTF_VCH>
            <cfset MNBTF_VCH = MNBTF_VCH>
            <cfset MSBTF_VCH = MSBTF_VCH>
            <cfset MOBTF_VCH = MOBTF_VCH>
            <cfset MTBTF_VCH = MTBTF_VCH>
            <cfset NEBTF_VCH = NEBTF_VCH>
            <cfset NVBTF_VCH = NVBTF_VCH>
            <cfset NHBTF_VCH = NHBTF_VCH>
            <cfset NJBTF_VCH = NJBTF_VCH>
            <cfset NMBTF_VCH = NMBTF_VCH>
            <cfset NYBTF_VCH = NYBTF_VCH>
            <cfset NCBTF_VCH = NCBTF_VCH>
            <cfset NDBTF_VCH = NDBTF_VCH>
            <cfset OHBTF_VCH = OHBTF_VCH>
            <cfset OKBTF_VCH = OKBTF_VCH>
            <cfset ORBTF_VCH = ORBTF_VCH>
            <cfset PABTF_VCH = PABTF_VCH>
            <cfset RIBTF_VCH = RIBTF_VCH>
            <cfset SDBTF_VCH = SDBTF_VCH>
            <cfset SCBTF_VCH = SCBTF_VCH>
            <cfset TNBTF_VCH = TNBTF_VCH>
            <cfset TXBTF_VCH = TXBTF_VCH>
            <cfset UTBTF_VCH = UTBTF_VCH>
            <cfset VTBTF_VCH = VTBTF_VCH>
            <cfset VABTF_VCH = VABTF_VCH>
            <cfset WABTF_VCH = WABTF_VCH>
            <cfset WVBTF_VCH = WVBTF_VCH>
            <cfset WIBTF_VCH = WIBTF_VCH>
            <cfset WYBTF_VCH = WYBTF_VCH>
            
            
            
            <cfset TNAL = AL_vch>
            <cfset TNAK = AK_vch>
            <cfset TNAZ = AZ_vch>
            <cfset TNAR = AR_vch>
            <cfset TNCA = CA_vch>
            <cfset TNCO = CO_vch>
            <cfset TNCT = CT_vch>
            <cfset TNDE = DE_vch>
            <cfset TNFL = FL_vch>
            <cfset TNGA = GA_vch>
            <cfset TNHI = HI_vch>
            <cfset TNID = ID_vch>
            <cfset TNIL = IL_vch>
            <cfset TNIN = IN_vch>
            <cfset TNIA = IA_vch>
            <cfset TNKS = KS_vch>
            <cfset TNKY = KY_vch>
            <cfset TNLA = LA_vch>
            <cfset TNMA = MA_vch>
            <cfset TNMD = MD_vch>
            <cfset TNME = ME_vch>
            <cfset TNMI = MI_vch>
            <cfset TNMN = MN_vch>
            <cfset TNMS = MS_vch>
            <cfset TNMO = MO_vch>
            <cfset TNMT = MT_vch>
            <cfset TNNE = NE_vch>
            <cfset TNNV = NV_vch>
            <cfset TNNH = NH_vch>
            <cfset TNNJ = NJ_vch>
            <cfset TNNM = NM_vch>
            <cfset TNNY = NY_vch>
            <cfset TNNC = NC_vch>
            <cfset TNND = ND_vch>
            <cfset TNOH = OH_vch>
            <cfset TNOK = OK_vch>
            <cfset TNOR = OR_vch>
            <cfset TNPA = PA_vch>
            <cfset TNRI = RI_vch>
            <cfset TNSD = SD_vch>
            <cfset TNSC = SC_vch>
            <cfset TNTN = TN_vch>
            <cfset TNTX = TX_vch>
            <cfset TNUT = UT_vch>
            <cfset TNVT = VT_vch>
            <cfset TNVA = VA_vch>
            <cfset TNWA = WA_vch>
            <cfset TNWV = WV_vch>
            <cfset TNWI = WI_vch>
            <cfset TNWY = WY_vch>
            
            
            <cfset ALCTN_VCH = ALCTN_VCH>
            <cfset AKCTN_VCH = AKCTN_VCH>
            <cfset AZCTN_VCH = AZCTN_VCH>
            <cfset ARCTN_VCH = ARCTN_VCH>
            <cfset CACTN_VCH = CACTN_VCH>
            <cfset COCTN_VCH = COCTN_VCH>
            <cfset CTCTN_VCH = CTCTN_VCH>
            <cfset DECTN_VCH = DECTN_VCH>
            <cfset FLCTN_VCH = FLCTN_VCH>
            <cfset GACTN_VCH = GACTN_VCH>
            <cfset HICTN_VCH = HICTN_VCH>
            <cfset IDCTN_VCH = IDCTN_VCH>
            <cfset ILCTN_VCH = ILCTN_VCH>
            <cfset INCTN_VCH = INCTN_VCH>
            <cfset IACTN_VCH = IACTN_VCH>
            <cfset KSCTN_VCH = KSCTN_VCH>
            <cfset KYCTN_VCH = KYCTN_VCH>
            <cfset LACTN_VCH = LACTN_VCH>
            <cfset MACTN_VCH = MACTN_VCH>
            <cfset MDCTN_VCH = MDCTN_VCH>
            <cfset MECTN_VCH = MECTN_VCH>
            <cfset MICTN_VCH = MICTN_VCH>
            <cfset MNCTN_VCH = MNCTN_VCH>
            <cfset MSCTN_VCH = MSCTN_VCH>
            <cfset MOCTN_VCH = MOCTN_VCH>
            <cfset MTCTN_VCH = MTCTN_VCH>
            <cfset NECTN_VCH = NECTN_VCH>
            <cfset NVCTN_VCH = NVCTN_VCH>
            <cfset NHCTN_VCH = NHCTN_VCH>
            <cfset NJCTN_VCH = NJCTN_VCH>
            <cfset NMCTN_VCH = NMCTN_VCH>
            <cfset NYCTN_VCH = NYCTN_VCH>
            <cfset NCCTN_VCH = NCCTN_VCH>
            <cfset NDCTN_VCH = NDCTN_VCH>
            <cfset OHCTN_VCH = OHCTN_VCH>
            <cfset OKCTN_VCH = OKCTN_VCH>
            <cfset ORCTN_VCH = ORCTN_VCH>
            <cfset PACTN_VCH = PACTN_VCH>
            <cfset RICTN_VCH = RICTN_VCH>
            <cfset SDCTN_VCH = SDCTN_VCH>
            <cfset SCCTN_VCH = SCCTN_VCH>
            <cfset TNCTN_VCH = TNCTN_VCH>
            <cfset TXCTN_VCH = TXCTN_VCH>
            <cfset UTCTN_VCH = UTCTN_VCH>
            <cfset VTCTN_VCH = VTCTN_VCH>
            <cfset VACTN_VCH = VACTN_VCH>
            <cfset WACTN_VCH = WACTN_VCH>
            <cfset WVCTN_VCH = WVCTN_VCH>
            <cfset WICTN_VCH = WICTN_VCH>
            <cfset WYCTN_VCH = WYCTN_VCH>
            
            <cfset ALBTN_VCH = ALBTN_VCH>
            <cfset AKBTN_VCH = AKBTN_VCH>
            <cfset AZBTN_VCH = AZBTN_VCH>
            <cfset ARBTN_VCH = ARBTN_VCH>
            <cfset CABTN_VCH = CABTN_VCH>
            <cfset COBTN_VCH = COBTN_VCH>
            <cfset CTBTN_VCH = CTBTN_VCH>
            <cfset DEBTN_VCH = DEBTN_VCH>
            <cfset FLBTN_VCH = FLBTN_VCH>
            <cfset GABTN_VCH = GABTN_VCH>
            <cfset HIBTN_VCH = HIBTN_VCH>
            <cfset IDBTN_VCH = IDBTN_VCH>
            <cfset ILBTN_VCH = ILBTN_VCH>
            <cfset INBTN_VCH = INBTN_VCH>
            <cfset IABTN_VCH = IABTN_VCH>
            <cfset KSBTN_VCH = KSBTN_VCH>
            <cfset KYBTN_VCH = KYBTN_VCH>
            <cfset LABTN_VCH = LABTN_VCH>
            <cfset MABTN_VCH = MABTN_VCH>
            <cfset MDBTN_VCH = MDBTN_VCH>
            <cfset MEBTN_VCH = MEBTN_VCH>
            <cfset MIBTN_VCH = MIBTN_VCH>
            <cfset MNBTN_VCH = MNBTN_VCH>
            <cfset MSBTN_VCH = MSBTN_VCH>
            <cfset MOBTN_VCH = MOBTN_VCH>
            <cfset MTBTN_VCH = MTBTN_VCH>
            <cfset NEBTN_VCH = NEBTN_VCH>
            <cfset NVBTN_VCH = NVBTN_VCH>
            <cfset NHBTN_VCH = NHBTN_VCH>
            <cfset NJBTN_VCH = NJBTN_VCH>
            <cfset NMBTN_VCH = NMBTN_VCH>
            <cfset NYBTN_VCH = NYBTN_VCH>
            <cfset NCBTN_VCH = NCBTN_VCH>
            <cfset NDBTN_VCH = NDBTN_VCH>
            <cfset OHBTN_VCH = OHBTN_VCH>
            <cfset OKBTN_VCH = OKBTN_VCH>
            <cfset ORBTN_VCH = ORBTN_VCH>
            <cfset PABTN_VCH = PABTN_VCH>
            <cfset RIBTN_VCH = RIBTN_VCH>
            <cfset SDBTN_VCH = SDBTN_VCH>
            <cfset SCBTN_VCH = SCBTN_VCH>
            <cfset TNBTN_VCH = TNBTN_VCH>
            <cfset TXBTN_VCH = TXBTN_VCH>
            <cfset UTBTN_VCH = UTBTN_VCH>
            <cfset VTBTN_VCH = VTBTN_VCH>
            <cfset VABTN_VCH = VABTN_VCH>
            <cfset WABTN_VCH = WABTN_VCH>
            <cfset WVBTN_VCH = WVBTN_VCH>
            <cfset WIBTN_VCH = WIBTN_VCH>
            <cfset WYBTN_VCH = WYBTN_VCH>
            
            
            <!--- Project Email Scripts --->
            <cfset EMAILPROJECTNAME_VCH = EMAILPROJECTNAME_VCH>
            <cfset EMAILFROMLINE_VCH = EMAILFROMLINE_VCH>
            <cfset EMAILSUBJECTLINE_VCH = EMAILSUBJECTLINE_VCH>
            <cfset EMAILTAGCODE_VCH = EMAILTAGCODE_VCH>
            <cfset EMAILDELIVERYATTEMPTS_VCH = EMAILDELIVERYATTEMPTS_VCH>
            <cfset BOUNCEBACKSTRATEGY_VCH = BOUNCEBACKSTRATEGY_VCH>
            <cfset ORIGINATINGFILENAME_VCH = ORIGINATINGFILENAME_VCH>
            <cfset EXPORTDATEFEED_VCH = EXPORTDATEFEED_VCH>
            <cfset DATAFEEDNOTES_VCH = DATAFEEDNOTES_VCH>
            <cfset EMAILHTMLTEMPLATE_VCH = EMAILHTMLTEMPLATE_VCH>
            <cfset EMAILTEXTTEMPLATE_VCH = EMAILTEXTTEMPLATE_VCH>
            
            <!--- Project Data Feeds --->
            <cfset DATA_REQUIREMENTS_VCH = DATA_REQUIREMENTS_VCH>
            <cfset IT_INTERFACE_SIGNOFF_VCH = IT_INTERFACE_SIGNOFF_VCH>
            <cfset CALL_RESULTS_PROCESSING_VCH = CALL_RESULTS_PROCESSING_VCH>
            <cfset CALL_YES_VCH = CALL_YES_VCH>
            <cfset DATA_EXCHANGE_DEVELOP_VCH = DATA_EXCHANGE_DEVELOP_VCH>
            <cfset DATA_SOURCE_VCH = DATA_SOURCE_VCH>
            <cfset DO_NOT_SCRUB_INT = DO_NOT_SCRUB_INT>
            <cfset STATE_DNC_LIST_INT = STATE_DNC_LIST_INT>
            <cfset CLIENT_DNC_INT = CLIENT_DNC_INT>
            <cfset DO_NOT_DIAL_STATES_INT = DO_NOT_DIAL_STATES_INT>
            <cfset NATIONAL_DNC_INT = NATIONAL_DNC_INT>
            <cfset COMPANY_INTERNAL_LIST_INT = COMPANY_INTERNAL_LIST_INT>
            <cfset CELL_PHONE_LIST_INT = CELL_PHONE_LIST_INT>
            <cfset DATA_FILE_TRANSMISSION_VCH = DATA_FILE_TRANSMISSION_VCH>
            <cfset FILTER_DUPLICATE_RECORDS_INT = FILTER_DUPLICATE_RECORDS_INT>
            <cfset FILTER_WITHIN_FILE_INT = FILTER_WITHIN_FILE_INT>
            <cfset FILTER_SAME_DAY_INT = FILTER_SAME_DAY_INT>
            <cfset FILTER_X_AMOUNT_DAYS_INT = FILTER_X_AMOUNT_DAYS_INT>
            <cfset X_DAYS_NUMBER_INT = X_DAYS_NUMBER_INT>
            <cfset PDFNOTES_VCH = PDFNOTES_VCH>
            
            <!--- Project Reporting Info --->
            <cfset Project_Reporting_vch = Project_Reporting_vch>
            
            <!--- Project Disclosures --->
            <cfset Project_Disclosures_vch = Project_Disclosures_vch>
    	
		</cfoutput>
    <cfelse>
    
    	<!--- Project Info --->
        <cfparam name="ProjectID_vch" default="">
        <cfparam name="VERSION_VCH" default="">
        <cfparam name="PLATFORM_TYPE_VCH" default="">
        <cfparam name="PROJECT_DURATION_VCH" default="">
        <cfparam name="SUBMISSION_DATE_DT" default="#Now()#">
        <cfparam name="PROJECT_START_DATE_DT" default="#Now()#">
        <cfparam name="PROJECT_END_DATE_DT" default="#Now()#">
        <cfparam name="PREPARED_BY_VCH" default="">
        <cfparam name="EMAIL_PHONE_VCH" default="">
        <cfparam name="PROJECTLEADCONTACT_VCH" default="">
        <cfparam name="SCRIPT_CONTENT_CONTACT_VCH" default="">
        <cfparam name="DATA_FEED_CONTACT_VCH" default="">
        <cfparam name="MANDP_CONTACT_VCH" default="">
        <cfparam name="PROJECT_TEAM_VCH" default="">
        <cfparam name="COMMUNICATIONS_ALERT_VCH" default="">
        
        <!--- Project Overview --->
        <cfparam name="PROJECTDESC_VCH" default="">
        <cfparam name="TYPE_OF_CALL_VCH" default="0">
        <cfparam name="PRECALL_TO_EMAIL_INT" default="0">
        <cfparam name="STANDALONE_CALL_INT" default="0">
        <cfparam name="PRECALL_TO_DIRECT_MAIL_INT" default="0">
        <cfparam name="POSTCALL_TO_DIRECT_MAIL_INT" default="0">
        <cfparam name="DRIVE_TO_WEB_INT" default="0">
        <cfparam name="INFORMATIONAL_INT" default="0">
        <cfparam name="OTHER_INT" default="0">
        <cfparam name="OTHER_VCH" default="">
        <cfparam name="PLATFORM_ATT_INT" default="0">
        <cfparam name="PLATFORM_MB_INT" default="0">
        <cfparam name="PLATFORM_EGS_INT" default="0">
        <cfparam name="CUSTOMER_BASE_CON_INT" default="0">
        <cfparam name="CUSTOMER_BASE_BUS_INT" default="0">
        
        <cfparam name="AL" default="0">
        <cfparam name="AK" default="0">
        <cfparam name="AZ" default="0">
        <cfparam name="AR" default="0">
        <cfparam name="CA" default="0">
        <cfparam name="CO" default="0">
        <cfparam name="CT" default="0">
        <cfparam name="DE" default="0">
        <cfparam name="FL" default="0">
        <cfparam name="GA" default="0">
        <cfparam name="HI" default="0">
        <cfparam name="ID" default="0">
        <cfparam name="IL" default="0">
        <cfparam name="IN" default="0">
        <cfparam name="IA" default="0">
        <cfparam name="KS" default="0">
        <cfparam name="KY" default="0">
        <cfparam name="LA" default="0">
        <cfparam name="MA" default="0">
        <cfparam name="MD" default="0">
        <cfparam name="ME" default="0">
        <cfparam name="MI" default="0">
        <cfparam name="MN" default="0">
        <cfparam name="MS" default="0">
        <cfparam name="MO" default="0">
        <cfparam name="MT" default="0">
        <cfparam name="NE" default="0">
        <cfparam name="NV" default="0">
        <cfparam name="NH" default="0">
        <cfparam name="NJ" default="0">
        <cfparam name="NM" default="0">
        <cfparam name="NY" default="0">
        <cfparam name="NC" default="0">
        <cfparam name="ND" default="0">
        <cfparam name="OH" default="0">
        <cfparam name="OK" default="0">
        <cfparam name="zOR" default="0">
        <cfparam name="PA" default="0">
        <cfparam name="RI" default="0">
        <cfparam name="SD" default="0">
        <cfparam name="SC" default="0">
        <cfparam name="TN" default="0">
        <cfparam name="TX" default="0">
        <cfparam name="UT" default="0">
        <cfparam name="VT" default="0">
        <cfparam name="VA" default="0">
        <cfparam name="WA" default="0">
        <cfparam name="WV" default="0">
        <cfparam name="WI" default="0">
        <cfparam name="WY" default="0">
        
        <!--- Project Complexity --->
        <cfparam name="PROGRAM_COMPLEXITY_VCH" default="">
        <cfparam name="MONITORING_THRESHOLD_INT" default="0">
        <cfparam name="MONDAY_VCH" default="">
        <cfparam name="TUESDAY_VCH" default="">
        <cfparam name="WEDNESDAY_VCH" default="">
        <cfparam name="THURSDAY_VCH" default="">
        <cfparam name="FRIDAY_VCH" default="">
        <cfparam name="SATURDAY_VCH" default="">
        <cfparam name="SUNDAY_VCH" default="">
        
        <!--- Project Settings --->
        <cfparam name="MFDIALINGHOURS_VCH" default="">
        <cfparam name="SATURDAYDIALINGHOURS_VCH" default="">
        <cfparam name="SUNDAYDIALINGHOURS_VCH" default="">
        <cfparam name="HOLIDAYDIALINGHOURS_VCH" default="">
        <cfparam name="DAILYCALLVOLUMES_VCH" default="">
        <cfparam name="CONSUMERCALLERID_VCH" default="">
        <cfparam name="BUSINESSCALLERID_VCH" default="">
        <cfparam name="SPANISHOPTION_VCH" default="">
        <cfparam name="ALTERNATENUMBERDIALING_VCH" default="">
        <cfparam name="PHONENUMTOBEDIALED_VCH" default="">
        <cfparam name="REDIALLOGIC_VCH" default="">
        <cfparam name="REDIALATTEMPTS_VCH" default="">
        <cfparam name="ROLLOVERDIALING_VCH" default="">
        <cfparam name="ANSWERMACHINESTRATEGY_VCH" default="">
        <cfparam name="MAXATTEMPTS_INT" default="0">
        <cfparam name="RINGCOUNT_INT" default="0">
        <cfparam name="ANSWERMACHINEWAIT_VCH" default="">
        <cfparam name="BUSYCALLBACKTIME_VCH" default="">
        <cfparam name="BCBTIMEREDIALTIME_VCH" default="">
        <cfparam name="NOANSWERCALLBACKTIME_VCH" default="">
        <cfparam name="NACBTIMEREDIALTIME_VCH" default="">
        <cfparam name="OTHERFAX_VCH" default="">
        <cfparam name="OFTIMEREDIALTIME_VCH" default="">
    
        <!--- Project Voice Scripts --->
        <cfparam name="SCRIPT_INFORMATION_VCH" default="">
        <cfparam name="VOICE_TALENT_VCH" default="">
        <cfparam name="BENEFITS_VCH" default="">
        <cfparam name="PHRASES_VCH" default="">
        <cfparam name="TONALITY_VCH" default="">
        <cfparam name="NOTES_VCH" default="">
        <cfparam name="CALLCENTERSALES_VCH" default="">
        <cfparam name="INBOUNDCALLSMETHOD_VCH" default="">
        <cfparam name="SCRIPTTOAPPROVE_VCH" default="">
        <cfparam name="SCRIPTAPPROVEDBY_VCH" default="">
        <cfparam name="SCRIPTAPPROVEDDATE_VCH" default="">
        
        <cfparam name="ALTF" default="0">
        <cfparam name="AKTF" default="0">
        <cfparam name="AZTF" default="0">
        <cfparam name="ARTF" default="0">
        <cfparam name="CATF" default="0">
        <cfparam name="COTF" default="0">
        <cfparam name="CTTF" default="0">
        <cfparam name="DETF" default="0">
        <cfparam name="FLTF" default="0">
        <cfparam name="GATF" default="0">
        <cfparam name="HITF" default="0">
        <cfparam name="IDTF" default="0">
        <cfparam name="ILTF" default="0">
        <cfparam name="INTF" default="0">
        <cfparam name="IATF" default="0">
        <cfparam name="KSTF" default="0">
        <cfparam name="KYTF" default="0">
        <cfparam name="LATF" default="0">
        <cfparam name="MATF" default="0">
        <cfparam name="MDTF" default="0">
        <cfparam name="METF" default="0">
        <cfparam name="MITF" default="0">
        <cfparam name="MNTF" default="0">
        <cfparam name="MSTF" default="0">
        <cfparam name="MOTF" default="0">
        <cfparam name="MTTF" default="0">
        <cfparam name="NETF" default="0">
        <cfparam name="NVTF" default="0">
        <cfparam name="NHTF" default="0">
        <cfparam name="NJTF" default="0">
        <cfparam name="NMTF" default="0">
        <cfparam name="NYTF" default="0">
        <cfparam name="NCTF" default="0">
        <cfparam name="NDTF" default="0">
        <cfparam name="OHTF" default="0">
        <cfparam name="OKTF" default="0">
        <cfparam name="ORTF" default="0">
        <cfparam name="PATF" default="0">
        <cfparam name="RITF" default="0">
        <cfparam name="SDTF" default="0">
        <cfparam name="SCTF" default="0">
        <cfparam name="TNTF" default="0">
        <cfparam name="TXTF" default="0">
        <cfparam name="UTTF" default="0">
        <cfparam name="VTTF" default="0">
        <cfparam name="VATF" default="0">
        <cfparam name="WATF" default="0">
        <cfparam name="WVTF" default="0">
        <cfparam name="WITF" default="0">
        <cfparam name="WYTF" default="0">
        
        <cfparam name="ALCTF_VCH" default="">
        <cfparam name="AKCTF_VCH" default="">
        <cfparam name="AZCTF_VCH" default="">
        <cfparam name="ARCTF_VCH" default="">
        <cfparam name="CACTF_VCH" default="">
        <cfparam name="COCTF_VCH" default="">
        <cfparam name="CTCTF_VCH" default="">
        <cfparam name="DECTF_VCH" default="">
        <cfparam name="FLCTF_VCH" default="">
        <cfparam name="GACTF_VCH" default="">
        <cfparam name="HICTF_VCH" default="">
        <cfparam name="IDCTF_VCH" default="">
        <cfparam name="ILCTF_VCH" default="">
        <cfparam name="INCTF_VCH" default="">
        <cfparam name="IACTF_VCH" default="">
        <cfparam name="KSCTF_VCH" default="">
        <cfparam name="KYCTF_VCH" default="">
        <cfparam name="LACTF_VCH" default="">
        <cfparam name="MACTF_VCH" default="">
        <cfparam name="MDCTF_VCH" default="">
        <cfparam name="MECTF_VCH" default="">
        <cfparam name="MICTF_VCH" default="">
        <cfparam name="MNCTF_VCH" default="">
        <cfparam name="MSCTF_VCH" default="">
        <cfparam name="MOCTF_VCH" default="">
        <cfparam name="MTCTF_VCH" default="">
        <cfparam name="NECTF_VCH" default="">
        <cfparam name="NVCTF_VCH" default="">
        <cfparam name="NHCTF_VCH" default="">
        <cfparam name="NJCTF_VCH" default="">
        <cfparam name="NMCTF_VCH" default="">
        <cfparam name="NYCTF_VCH" default="">
        <cfparam name="NCCTF_VCH" default="">
        <cfparam name="NDCTF_VCH" default="">
        <cfparam name="OHCTF_VCH" default="">
        <cfparam name="OKCTF_VCH" default="">
        <cfparam name="ORCTF_VCH" default="">
        <cfparam name="PACTF_VCH" default="">
        <cfparam name="RICTF_VCH" default="">
        <cfparam name="SDCTF_VCH" default="">
        <cfparam name="SCCTF_VCH" default="">
        <cfparam name="TNCTF_VCH" default="">
        <cfparam name="TXCTF_VCH" default="">
        <cfparam name="UTCTF_VCH" default="">
        <cfparam name="VTCTF_VCH" default="">
        <cfparam name="VACTF_VCH" default="">
        <cfparam name="WACTF_VCH" default="">
        <cfparam name="WVCTF_VCH" default="">
        <cfparam name="WICTF_VCH" default="">
        <cfparam name="WYCTF_VCH" default="">
        
        <cfparam name="ALBTF_VCH" default="">
        <cfparam name="AKBTF_VCH" default="">
        <cfparam name="AZBTF_VCH" default="">
        <cfparam name="ARBTF_VCH" default="">
        <cfparam name="CABTF_VCH" default="">
        <cfparam name="COBTF_VCH" default="">
        <cfparam name="CTBTF_VCH" default="">
        <cfparam name="DEBTF_VCH" default="">
        <cfparam name="FLBTF_VCH" default="">
        <cfparam name="GABTF_VCH" default="">
        <cfparam name="HIBTF_VCH" default="">
        <cfparam name="IDBTF_VCH" default="">
        <cfparam name="ILBTF_VCH" default="">
        <cfparam name="INBTF_VCH" default="">
        <cfparam name="IABTF_VCH" default="">
        <cfparam name="KSBTF_VCH" default="">
        <cfparam name="KYBTF_VCH" default="">
        <cfparam name="LABTF_VCH" default="">
        <cfparam name="MABTF_VCH" default="">
        <cfparam name="MDBTF_VCH" default="">
        <cfparam name="MEBTF_VCH" default="">
        <cfparam name="MIBTF_VCH" default="">
        <cfparam name="MNBTF_VCH" default="">
        <cfparam name="MSBTF_VCH" default="">
        <cfparam name="MOBTF_VCH" default="">
        <cfparam name="MTBTF_VCH" default="">
        <cfparam name="NEBTF_VCH" default="">
        <cfparam name="NVBTF_VCH" default="">
        <cfparam name="NHBTF_VCH" default="">
        <cfparam name="NJBTF_VCH" default="">
        <cfparam name="NMBTF_VCH" default="">
        <cfparam name="NYBTF_VCH" default="">
        <cfparam name="NCBTF_VCH" default="">
        <cfparam name="NDBTF_VCH" default="">
        <cfparam name="OHBTF_VCH" default="">
        <cfparam name="OKBTF_VCH" default="">
        <cfparam name="ORBTF_VCH" default="">
        <cfparam name="PABTF_VCH" default="">
        <cfparam name="RIBTF_VCH" default="">
        <cfparam name="SDBTF_VCH" default="">
        <cfparam name="SCBTF_VCH" default="">
        <cfparam name="TNBTF_VCH" default="">
        <cfparam name="TXBTF_VCH" default="">
        <cfparam name="UTBTF_VCH" default="">
        <cfparam name="VTBTF_VCH" default="">
        <cfparam name="VABTF_VCH" default="">
        <cfparam name="WABTF_VCH" default="">
        <cfparam name="WVBTF_VCH" default="">
        <cfparam name="WIBTF_VCH" default="">
        <cfparam name="WYBTF_VCH" default="">
        
        <cfparam name="TNAL" default="0">
        <cfparam name="TNAK" default="0">
        <cfparam name="TNAZ" default="0">
        <cfparam name="TNAR" default="0">
        <cfparam name="TNCA" default="0">
        <cfparam name="TNCO" default="0">
        <cfparam name="TNCT" default="0">
        <cfparam name="TNDE" default="0">
        <cfparam name="TNFL" default="0">
        <cfparam name="TNGA" default="0">
        <cfparam name="TNHI" default="0">
        <cfparam name="TNID" default="0">
        <cfparam name="TNIL" default="0">
        <cfparam name="TNIN" default="0">
        <cfparam name="TNIA" default="0">
        <cfparam name="TNKS" default="0">
        <cfparam name="TNKY" default="0">
        <cfparam name="TNLA" default="0">
        <cfparam name="TNMA" default="0">
        <cfparam name="TNMD" default="0">
        <cfparam name="TNME" default="0">
        <cfparam name="TNMI" default="0">
        <cfparam name="TNMN" default="0">
        <cfparam name="TNMS" default="0">
        <cfparam name="TNMO" default="0">
        <cfparam name="TNMT" default="0">
        <cfparam name="TNNE" default="0">
        <cfparam name="TNNV" default="0">
        <cfparam name="TNNH" default="0">
        <cfparam name="TNNJ" default="0">
        <cfparam name="TNNM" default="0">
        <cfparam name="TNNY" default="0">
        <cfparam name="TNNC" default="0">
        <cfparam name="TNND" default="0">
        <cfparam name="TNOH" default="0">
        <cfparam name="TNOK" default="0">
        <cfparam name="TNOR" default="0">
        <cfparam name="TNPA" default="0">
        <cfparam name="TNRI" default="0">
        <cfparam name="TNSD" default="0">
        <cfparam name="TNSC" default="0">
        <cfparam name="TNTN" default="0">
        <cfparam name="TNTX" default="0">
        <cfparam name="TNUT" default="0">
        <cfparam name="TNVT" default="0">
        <cfparam name="TNVA" default="0">
        <cfparam name="TNWA" default="0">
        <cfparam name="TNWV" default="0">
        <cfparam name="TNWI" default="0">
        <cfparam name="TNWY" default="0">
        
        <cfparam name="ALCTN_VCH" default="">
        <cfparam name="AKCTN_VCH" default="">
        <cfparam name="AZCTN_VCH" default="">
        <cfparam name="ARCTN_VCH" default="">
        <cfparam name="CACTN_VCH" default="">
        <cfparam name="COCTN_VCH" default="">
        <cfparam name="CTCTN_VCH" default="">
        <cfparam name="DECTN_VCH" default="">
        <cfparam name="FLCTN_VCH" default="">
        <cfparam name="GACTN_VCH" default="">
        <cfparam name="HICTN_VCH" default="">
        <cfparam name="IDCTN_VCH" default="">
        <cfparam name="ILCTN_VCH" default="">
        <cfparam name="INCTN_VCH" default="">
        <cfparam name="IACTN_VCH" default="">
        <cfparam name="KSCTN_VCH" default="">
        <cfparam name="KYCTN_VCH" default="">
        <cfparam name="LACTN_VCH" default="">
        <cfparam name="MACTN_VCH" default="">
        <cfparam name="MDCTN_VCH" default="">
        <cfparam name="MECTN_VCH" default="">
        <cfparam name="MICTN_VCH" default="">
        <cfparam name="MNCTN_VCH" default="">
        <cfparam name="MSCTN_VCH" default="">
        <cfparam name="MOCTN_VCH" default="">
        <cfparam name="MTCTN_VCH" default="">
        <cfparam name="NECTN_VCH" default="">
        <cfparam name="NVCTN_VCH" default="">
        <cfparam name="NHCTN_VCH" default="">
        <cfparam name="NJCTN_VCH" default="">
        <cfparam name="NMCTN_VCH" default="">
        <cfparam name="NYCTN_VCH" default="">
        <cfparam name="NCCTN_VCH" default="">
        <cfparam name="NDCTN_VCH" default="">
        <cfparam name="OHCTN_VCH" default="">
        <cfparam name="OKCTN_VCH" default="">
        <cfparam name="ORCTN_VCH" default="">
        <cfparam name="PACTN_VCH" default="">
        <cfparam name="RICTN_VCH" default="">
        <cfparam name="SDCTN_VCH" default="">
        <cfparam name="SCCTN_VCH" default="">
        <cfparam name="TNCTN_VCH" default="">
        <cfparam name="TXCTN_VCH" default="">
        <cfparam name="UTCTN_VCH" default="">
        <cfparam name="VTCTN_VCH" default="">
        <cfparam name="VACTN_VCH" default="">
        <cfparam name="WACTN_VCH" default="">
        <cfparam name="WVCTN_VCH" default="">
        <cfparam name="WICTN_VCH" default="">
        <cfparam name="WYCTN_VCH" default="">
        
        <cfparam name="ALBTN_VCH" default="">
        <cfparam name="AKBTN_VCH" default="">
        <cfparam name="AZBTN_VCH" default="">
        <cfparam name="ARBTN_VCH" default="">
        <cfparam name="CABTN_VCH" default="">
        <cfparam name="COBTN_VCH" default="">
        <cfparam name="CTBTN_VCH" default="">
        <cfparam name="DEBTN_VCH" default="">
        <cfparam name="FLBTN_VCH" default="">
        <cfparam name="GABTN_VCH" default="">
        <cfparam name="HIBTN_VCH" default="">
        <cfparam name="IDBTN_VCH" default="">
        <cfparam name="ILBTN_VCH" default="">
        <cfparam name="INBTN_VCH" default="">
        <cfparam name="IABTN_VCH" default="">
        <cfparam name="KSBTN_VCH" default="">
        <cfparam name="KYBTN_VCH" default="">
        <cfparam name="LABTN_VCH" default="">
        <cfparam name="MABTN_VCH" default="">
        <cfparam name="MDBTN_VCH" default="">
        <cfparam name="MEBTN_VCH" default="">
        <cfparam name="MIBTN_VCH" default="">
        <cfparam name="MNBTN_VCH" default="">
        <cfparam name="MSBTN_VCH" default="">
        <cfparam name="MOBTN_VCH" default="">
        <cfparam name="MTBTN_VCH" default="">
        <cfparam name="NEBTN_VCH" default="">
        <cfparam name="NVBTN_VCH" default="">
        <cfparam name="NHBTN_VCH" default="">
        <cfparam name="NJBTN_VCH" default="">
        <cfparam name="NMBTN_VCH" default="">
        <cfparam name="NYBTN_VCH" default="">
        <cfparam name="NCBTN_VCH" default="">
        <cfparam name="NDBTN_VCH" default="">
        <cfparam name="OHBTN_VCH" default="">
        <cfparam name="OKBTN_VCH" default="">
        <cfparam name="ORBTN_VCH" default="">
        <cfparam name="PABTN_VCH" default="">
        <cfparam name="RIBTN_VCH" default="">
        <cfparam name="SDBTN_VCH" default="">
        <cfparam name="SCBTN_VCH" default="">
        <cfparam name="TNBTN_VCH" default="">
        <cfparam name="TXBTN_VCH" default="">
        <cfparam name="UTBTN_VCH" default="">
        <cfparam name="VTBTN_VCH" default="">
        <cfparam name="VABTN_VCH" default="">
        <cfparam name="WABTN_VCH" default="">
        <cfparam name="WVBTN_VCH" default="">
        <cfparam name="WIBTN_VCH" default="">
        <cfparam name="WYBTN_VCH" default="">
        
        <!--- Project Email Scripts --->
        <cfparam name="EMAILPROJECTNAME_VCH" default="">
        <cfparam name="EMAILFROMLINE_VCH" default="">
        <cfparam name="EMAILSUBJECTLINE_VCH" default="">
        <cfparam name="EMAILTAGCODE_VCH" default="">
        <cfparam name="EMAILDELIVERYATTEMPTS_VCH" default="">
        <cfparam name="BOUNCEBACKSTRATEGY_VCH" default="">
        <cfparam name="ORIGINATINGFILENAME_VCH" default="">
        <cfparam name="EXPORTDATEFEED_VCH" default="">
        <cfparam name="DATAFEEDNOTES_VCH" default="">
        <cfparam name="EMAILHTMLTEMPLATE_VCH" default="">
        <cfparam name="EMAILTEXTTEMPLATE_VCH" default="">
        
        <!--- Project Data Feeds --->
        <cfparam name="DATA_REQUIREMENTS_VCH" default="">
        <cfparam name="IT_INTERFACE_SIGNOFF_VCH" default="">
        <cfparam name="CALL_RESULTS_PROCESSING_VCH" default="">
        <cfparam name="CALL_YES_VCH" default="">
        <cfparam name="DATA_EXCHANGE_DEVELOP_VCH" default="">
        <cfparam name="DATA_SOURCE_VCH" default="">
        <cfparam name="DO_NOT_SCRUB_INT" default="0">
        <cfparam name="STATE_DNC_LIST_INT" default="0">
        <cfparam name="CLIENT_DNC_INT" default="0">
        <cfparam name="DO_NOT_DIAL_STATES_INT" default="0">
        <cfparam name="NATIONAL_DNC_INT" default="0">
        <cfparam name="COMPANY_INTERNAL_LIST_INT" default="0">
        <cfparam name="CELL_PHONE_LIST_INT" default="0">
        <cfparam name="DATA_FILE_TRANSMISSION_VCH" default="">
        <cfparam name="FILTER_DUPLICATE_RECORDS_INT" default="0">
        <cfparam name="FILTER_WITHIN_FILE_INT" default="0">
        <cfparam name="FILTER_SAME_DAY_INT" default="0">
        <cfparam name="FILTER_X_AMOUNT_DAYS_INT" default="0">
        <cfparam name="X_DAYS_NUMBER_INT" default="0">
        <cfparam name="PDFNOTES_VCH" default="0">
        
        <!--- Project Reporting Info --->
        <cfparam name="PROJECT_REPORTING_VCH" default="">
        
        <!--- Project Disclosures --->
        <cfparam name="PROJECT_DISCLOSURES_VCH" default="">
    
    </cfif>
    