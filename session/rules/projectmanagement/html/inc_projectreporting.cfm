<cfset tableName="pm_projectreportinfo">
<cfparam name="Session.projectReportInfoList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectReportInfoList"/>

<cfif not StructIsEmpty(Session.projectReportInfoList)>
<cfoutput>
	<cfif StructKeyExists(Session.projectReportInfoList,'Project_Reporting_vch')>
		<div class="info-col" id="PM_ProjectReporting" rel="InitProjectReporting">
		    <div class="colhdr">Project Reporting Info
			</div>
		    <div id="dl">
		    	<div class="dt" id="t1">Report/Web Information
					<cfif structFind(Session.projectReportInfoList,"Project_Reporting_vch") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif>			    	
				</div>
		        <div class="dd" id="d1" style="position:relative;">
		        	<textarea name="Project_Reporting_vch" class="FullWidth" style="height:100px;">#Project_Reporting_vch#</textarea>
		        </div>
		    </div>
		</div>
	</cfif>
</cfoutput>
</cfif>