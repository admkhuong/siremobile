<cfset tableName="pm_projectsettings">
<cfparam name="Session.projectSettingsList" default="#StructNew()#"/> 
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfset args.inpTableName="#tableName#">
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectSettingsList"/>

<cfif not StructIsEmpty(Session.projectSettingsList)>
<cfoutput>
<div class="info-col" id="#tableName#" rel="Initprojectsettingsview">
    <div class="colhdr">Project Settings</div>
    <div id="dl">
		<cfif StructKeyExists(Session.projectSettingsList,'MFDIALINGHOURS_VCH')>
    	<div class="dt" id="t1">Dialing Hours
<cfif structFind(Session.projectSettingsList,"MFDIALINGHOURS_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif>		
		</div>
        <div class="dd" id="d1">
        	<div style="margin-bottom:10px;">
            Monday through Friday: <br />
        	<textarea name="MFDIALINGHOURS_VCH" class="FullWidth">#MFDIALINGHOURS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Saturday:<br />
        	<textarea name="SATURDAYDIALINGHOURS_VCH" class="FullWidth">#SATURDAYDIALINGHOURS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Sunday:<br />
        	<textarea name="SUNDAYDIALINGHOURS_VCH" class="FullWidth">#SUNDAYDIALINGHOURS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Holiday:<br />
        	<textarea name="HOLIDAYDIALINGHOURS_VCH" class="FullWidth">#HOLIDAYDIALINGHOURS_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectSettingsList,'DAILYCALLVOLUMES_VCH')>
        <div class="dt" id="t2">Daily Call Volumes<cfif structFind(Session.projectSettingsList,"DAILYCALLVOLUMES_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d2"><textarea name="DAILYCALLVOLUMES_VCH" class="FullWidth">#DAILYCALLVOLUMES_VCH#</textarea></div>
		</cfif>
		<cfif StructKeyExists(Session.projectSettingsList,'CONSUMERCALLERID_VCH')>
        <div class="dt" id="t3">Caller ID Numbers<cfif structFind(Session.projectSettingsList,"CONSUMERCALLERID_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d3">
        	<div style="margin-bottom:10px;">
            Consumer Caller ID:<br />
        	<textarea name="CONSUMERCALLERID_VCH" class="FullWidth">#CONSUMERCALLERID_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Business Caller ID:<br />
        	<textarea name="BUSINESSCALLERID_VCH" class="FullWidth">#BUSINESSCALLERID_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectSettingsList,'SPANISHOPTION_VCH')>
        <div class="dt" id="t4">Spanish Option<cfif structFind(Session.projectSettingsList,"SPANISHOPTION_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d4"><textarea name="SPANISHOPTION_VCH" class="FullWidth">#SPANISHOPTION_VCH#</textarea></div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectSettingsList,'ALTERNATENUMBERDIALING_VCH')>
        <div class="dt" id="t5">Call Settings<cfif structFind(Session.projectSettingsList,"ALTERNATENUMBERDIALING_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d5">
        	<div style="margin-bottom:10px;">
            Use Alternate Number Dialing:<br />
        	<textarea name="ALTERNATENUMBERDIALING_VCH" class="FullWidth">#ALTERNATENUMBERDIALING_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Phone Numbers To Be Dialed:<br />
        	<textarea name="PHONENUMTOBEDIALED_VCH" class="FullWidth">#PHONENUMTOBEDIALED_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Redial Logic:<br />
        	<textarea name="REDIALLOGIC_VCH" class="FullWidth">#REDIALLOGIC_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Redial Attempts:<br />
        	<textarea name="REDIALATTEMPTS_VCH" class="FullWidth">#REDIALATTEMPTS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Rollover Dialing:<br />
        	<textarea name="ROLLOVERDIALING_VCH" class="FullWidth">#ROLLOVERDIALING_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Answering Machine Strategy:<br />
        	<textarea name="ANSWERMACHINESTRATEGY_VCH" class="FullWidth">#ANSWERMACHINESTRATEGY_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Max Attempts Per Record:<br />
        	<textarea name="MAXATTEMPTS_INT" class="FullWidth">#MAXATTEMPTS_INT#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Ring Count:<br />
        	<textarea name="RINGCOUNT_INT" class="FullWidth">#RINGCOUNT_INT#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Answering Machine Wait Time:<br />
        	<textarea name="ANSWERMACHINEWAIT_VCH" class="FullWidth">#ANSWERMACHINEWAIT_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		
		<cfif StructKeyExists(Session.projectSettingsList,'BUSYCALLBACKTIME_VCH')>
        <div class="dt" id="t6">Redial Settings<cfif structFind(Session.projectSettingsList,"BUSYCALLBACKTIME_VCH") EQ 1>
				<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d6">
        	<div style="margin-bottom:10px;">
            Busy Callback Time:<br />
        	<textarea name="BUSYCALLBACKTIME_VCH" class="FullWidth">#BUSYCALLBACKTIME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Busy Callback Time To Redial:<br />
	        	<textarea name="BCBTIMEREDIALTIME_VCH" class="FullWidth">#BCBTIMEREDIALTIME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            No Answer Callback Time:<br />
        	<textarea name="NOANSWERCALLBACKTIME_VCH" class="FullWidth">#NOANSWERCALLBACKTIME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            No Answer Callback Time To Redial:<br />
        	<textarea name="NACBTIMEREDIALTIME_VCH" class="FullWidth">#NACBTIMEREDIALTIME_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Other, Fax, Operator Intercept:<br />
        	<textarea name="OTHERFAX_VCH" class="FullWidth">#OTHERFAX_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Other Time To Redial:<br />
        	<textarea name="OFTIMEREDIALTIME_VCH" class="FullWidth">#OFTIMEREDIALTIME_VCH#</textarea>
        	</div>
        </div> 
        </cfif>
    </div>

</div>
</cfoutput>
</cfif>