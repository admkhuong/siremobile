

<cfparam name="Session.projectVoiceScriptList" default="#StructNew()#"/>
<cfparam name="Session.ScriptTollFreeList" default="#StructNew()#"/>
<cfparam name="Session.ScriptTransferList" default="#StructNew()#"/>
<cfset args=StructNew() />
<cfset args.inpUserId="#Session.USERID#" />
<cfset tableName="pm_projectscriptinfo" />
<cfset args.inpTableName="#tableName#" />
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.projectVoiceScriptList"/>
<cfset tableName="pm_scriptstollfree" />
<cfset args.inpTableName="#tableName#" />
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.ScriptTollFreeList"/>
<cfset tableName="pm_scriptstransfer" />
<cfset args.inpTableName="#tableName#" />
<cfinvoke argumentcollection="#args#" method="getTaskList" component="#Session.RulesCFCPath#.rulechecklist" returnvariable="Session.ScriptTransferList"/>
<cfif not StructIsEmpty(Session.projectVoiceScriptList)>
<cfoutput>
<div class="info-col" id="PM_ProjectVoiceScripts" rel="InitProjectVoiceScriptsview">
    <div class="colhdr">Project Voice Scripts</div>
    <div id="dl">
		<cfif StructKeyExists(Session.projectVoiceScriptList,'SCRIPT_INFORMATION_VCH')>
    	<div class="dt" id="t1">Voice Script<cfif structFind(Session.projectVoiceScriptList,"SCRIPT_INFORMATION_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d1">
        	<textarea name="SCRIPT_INFORMATION_VCH" class="FullWidth" style="min-height:225px;">#SCRIPT_INFORMATION_VCH#</textarea>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectVoiceScriptList,'VOICE_TALENT_VCH')>
        <div class="dt" id="t2">Voice Talent Info<cfif structFind(Session.projectVoiceScriptList,"VOICE_TALENT_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d2">
        	<div style="margin-bottom:10px;">
            Voice Talent By:<br />
        	<textarea name="VOICE_TALENT_VCH" class="FullWidth">#VOICE_TALENT_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Benefits:<br />
        	<textarea name="BENEFITS_VCH" class="FullWidth">#BENEFITS_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Suggested/Mandatory Phrases:<br />
        	<textarea name="PHRASES_VCH" class="FullWidth">#PHRASES_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Tonality:<br />
        	<textarea name="TONALITY_VCH" class="FullWidth">#TONALITY_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Notes:<br />
        	<textarea name="NOTES_VCH" class="FullWidth" style="min-height:150px;">#NOTES_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectVoiceScriptList,'CALLCENTERSALES_VCH') OR StructKeyExists(Session.projectVoiceScriptList,'INBOUNDCALLSMETHOD_VCH')>
        <div class="dt" id="t3">Additional Info<cfif structFind(Session.projectVoiceScriptList,"CALLCENTERSALES_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d3">
        	<div style="margin-bottom:10px;">
            Do the call centers perform sales presentations?:<br />
        	<textarea name="CALLCENTERSALES_VCH" class="FullWidth">#CALLCENTERSALES_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Method used to inform reps/techs of inbound calls:<br />
        	<textarea name="INBOUNDCALLSMETHOD_VCH" class="FullWidth">#INBOUNDCALLSMETHOD_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		<cfif StructKeyExists(Session.projectVoiceScriptList,'SCRIPTTOAPPROVE_VCH')>
        <div class="dt" id="t4">Voice Script Approval<cfif structFind(Session.projectVoiceScriptList,"SCRIPTTOAPPROVE_VCH") EQ 1>
			    	<label style="color:red;">*</label>
			    	</cfif></div>
        <div class="dd" id="d4">
        	<div style="margin-bottom:10px;">
            Scripts To Approve:<br />
        	<textarea name="SCRIPTTOAPPROVE_VCH" class="FullWidth">#SCRIPTTOAPPROVE_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Approved By:<br />
        	<textarea name="SCRIPTAPPROVEDBY_VCH" class="FullWidth">#SCRIPTAPPROVEDBY_VCH#</textarea>
        	</div>
            <div style="margin-bottom:10px;">
            Date Approved:<br />
        	<textarea name="SCRIPTAPPROVEDDATE_VCH" class="FullWidth">#SCRIPTAPPROVEDDATE_VCH#</textarea>
        	</div>
        </div>
		</cfif>
		
        <div class="dt" id="t5">Toll Free Numbers</div>
      	<div class="dd" id="d5" style="position:relative; display:inline-block;height:500px;">
      	<div style="width:100%; margin-bottom:15px;">
        	Place a check next to all the states you will include a Toll Free Number on your voice message and the consumer and/or business Toll Free number you wish to display.
            <div style="margin-top:15px;">
        		<input type="checkbox" onclick="toggleChecked1(this.checked)"> Select / De-Select All States
            </div>
        </div>
      	<div style="float:left; width:60px; display:inline-block;height:500px;">
        	State<br />
            <input type="checkbox" name="TFAL" class="pist1" style="margin-right:10px;"<cfif ALTF eq 1> checked="checked"</cfif>>AL<br />
            <input type="checkbox" name="TFAK" class="pist1" style="margin-right:10px;"<cfif AKTF eq 1> checked="checked"</cfif>>AK<br />
            <input type="checkbox" name="TFAZ" class="pist1" style="margin-right:10px;"<cfif AZTF eq 1> checked="checked"</cfif>>AZ<br />
            <input type="checkbox" name="TFAR" class="pist1" style="margin-right:10px;"<cfif ARTF eq 1> checked="checked"</cfif>>AR<br />
            <input type="checkbox" name="TFCA" class="pist1" style="margin-right:10px;"<cfif CATF eq 1> checked="checked"</cfif>>CA<br />
            <input type="checkbox" name="TFCO" class="pist1" style="margin-right:10px;"<cfif COTF eq 1> checked="checked"</cfif>>CO<br />
            <input type="checkbox" name="TFCT" class="pist1" style="margin-right:10px;"<cfif CTTF eq 1> checked="checked"</cfif>>CT<br />
            <input type="checkbox" name="TFDE" class="pist1" style="margin-right:10px;"<cfif DETF eq 1> checked="checked"</cfif>>DE<br />
            <input type="checkbox" name="TFFL" class="pist1" style="margin-right:10px;"<cfif FLTF eq 1> checked="checked"</cfif>>FL<br />
            <input type="checkbox" name="TFGA" class="pist1" style="margin-right:10px;"<cfif GATF eq 1> checked="checked"</cfif>>GA<br />
            <input type="checkbox" name="TFHI" class="pist1" style="margin-right:10px;"<cfif HITF eq 1> checked="checked"</cfif>>HI<br />
            <input type="checkbox" name="TFID" class="pist1" style="margin-right:10px;"<cfif IDTF eq 1> checked="checked"</cfif>>ID<br />
            <input type="checkbox" name="TFIL" class="pist1" style="margin-right:10px;"<cfif ILTF eq 1> checked="checked"</cfif>>IL<br />
            <input type="checkbox" name="TFIN" class="pist1" style="margin-right:10px;"<cfif INTF eq 1> checked="checked"</cfif>>IN<br />
            <input type="checkbox" name="TFIA" class="pist1" style="margin-right:10px;"<cfif IATF eq 1> checked="checked"</cfif>>IA<br />
            <input type="checkbox" name="TFKS" class="pist1" style="margin-right:10px;"<cfif KSTF eq 1> checked="checked"</cfif>>KS<br />
            <input type="checkbox" name="TFKY" class="pist1" style="margin-right:10px;"<cfif KYTF eq 1> checked="checked"</cfif>>KY<br />
            <input type="checkbox" name="TFLA" class="pist1" style="margin-right:10px;"<cfif LATF eq 1> checked="checked"</cfif>>LA<br />
            <input type="checkbox" name="TFME" class="pist1" style="margin-right:10px;"<cfif METF eq 1> checked="checked"</cfif>>ME<br />
            <input type="checkbox" name="TFMD" class="pist1" style="margin-right:10px;"<cfif MDTF eq 1> checked="checked"</cfif>>MD<br />
            <input type="checkbox" name="TFME" class="pist1" style="margin-right:10px;"<cfif METF eq 1> checked="checked"</cfif>>ME<br />
            <input type="checkbox" name="TFMI" class="pist1" style="margin-right:10px;"<cfif MITF eq 1> checked="checked"</cfif>>MI<br />
            <input type="checkbox" name="TFMN" class="pist1" style="margin-right:10px;"<cfif MNTF eq 1> checked="checked"</cfif>>MN<br />
            <input type="checkbox" name="TFMS" class="pist1" style="margin-right:10px;"<cfif MSTF eq 1> checked="checked"</cfif>>MS<br />
            <input type="checkbox" name="TFMO" class="pist1" style="margin-right:10px;"<cfif MOTF eq 1> checked="checked"</cfif>>MO<br />
            <input type="checkbox" name="TFMT" class="pist1" style="margin-right:10px;"<cfif MTTF eq 1> checked="checked"</cfif>>MT<br />
            <input type="checkbox" name="TFNE" class="pist1" style="margin-right:10px;"<cfif NETF eq 1> checked="checked"</cfif>>NE<br />
            <input type="checkbox" name="TFNV" class="pist1" style="margin-right:10px;"<cfif NVTF eq 1> checked="checked"</cfif>>NV<br />
            <input type="checkbox" name="TFNH" class="pist1" style="margin-right:10px;"<cfif NHTF eq 1> checked="checked"</cfif>>NH<br />
            <input type="checkbox" name="TFNJ" class="pist1" style="margin-right:10px;"<cfif NJTF eq 1> checked="checked"</cfif>>NJ<br />
            <input type="checkbox" name="TFNM" class="pist1" style="margin-right:10px;"<cfif NMTF eq 1> checked="checked"</cfif>>NM<br />
            <input type="checkbox" name="TFNY" class="pist1" style="margin-right:10px;"<cfif NYTF eq 1> checked="checked"</cfif>>NY<br />
            <input type="checkbox" name="TFNC" class="pist1" style="margin-right:10px;"<cfif NCTF eq 1> checked="checked"</cfif>>NC<br />
            <input type="checkbox" name="TFND" class="pist1" style="margin-right:10px;"<cfif NDTF eq 1> checked="checked"</cfif>>ND<br />
            <input type="checkbox" name="TFOH" class="pist1" style="margin-right:10px;"<cfif OHTF eq 1> checked="checked"</cfif>>OH<br />
            <input type="checkbox" name="TFOK" class="pist1" style="margin-right:10px;"<cfif OKTF eq 1> checked="checked"</cfif>>OK<br />
            <input type="checkbox" name="TFOR" class="pist1" style="margin-right:10px;"<cfif ORTF eq 1> checked="checked"</cfif>>OR<br />
            <input type="checkbox" name="TFPA" class="pist1" style="margin-right:10px;"<cfif PATF eq 1> checked="checked"</cfif>>PA<br />
            <input type="checkbox" name="TFRI" class="pist1" style="margin-right:10px;"<cfif RITF eq 1> checked="checked"</cfif>>RI<br />
            <input type="checkbox" name="TFSD" class="pist1" style="margin-right:10px;"<cfif SDTF eq 1> checked="checked"</cfif>>SD<br />
            <input type="checkbox" name="TFSC" class="pist1" style="margin-right:10px;"<cfif SCTF eq 1> checked="checked"</cfif>>SC<br />
            <input type="checkbox" name="TFTN" class="pist1" style="margin-right:10px;"<cfif TNTF eq 1> checked="checked"</cfif>>TN<br />
            <input type="checkbox" name="TFTX" class="pist1" style="margin-right:10px;"<cfif TXTF eq 1> checked="checked"</cfif>>TX<br />
            <input type="checkbox" name="TFUT" class="pist1" style="margin-right:10px;"<cfif UTTF eq 1> checked="checked"</cfif>>UT<br />
            <input type="checkbox" name="TFVT" class="pist1" style="margin-right:10px;"<cfif VTTF eq 1> checked="checked"</cfif>>VT<br />
            <input type="checkbox" name="TFVA" class="pist1" style="margin-right:10px;"<cfif VATF eq 1> checked="checked"</cfif>>VA<br />
            <input type="checkbox" name="TFWA" class="pist1" style="margin-right:10px;"<cfif WATF eq 1> checked="checked"</cfif>>WA<br />
            <input type="checkbox" name="TFWV" class="pist1" style="margin-right:10px;"<cfif WVTF eq 1> checked="checked"</cfif>>WV<br />
            <input type="checkbox" name="TFWI" class="pist1" style="margin-right:10px;"<cfif WITF eq 1> checked="checked"</cfif>>WI<br />
            <input type="checkbox" name="TFWY" class="pist1" style="margin-right:10px;"<cfif WYTF eq 1> checked="checked"</cfif>>WY
        </div>
        <div style="float:left; width:150px; display:inline-block;">
        	Consumer<br />
            <input type="input" name="ALCTF_VCH" style="width:150px;" value="#ALCTF_VCH#"><br />
            <input type="input" name="AKCTF_VCH" style="width:150px;" value="#AKCTF_VCH#"><br />
            <input type="input" name="AZCTF_VCH" style="width:150px;" value="#AZCTF_VCH#"><br />
            <input type="input" name="ARCTF_VCH" style="width:150px;" value="#ARCTF_VCH#"><br />
            <input type="input" name="CACTF_VCH" style="width:150px;" value="#CACTF_VCH#"><br />
            <input type="input" name="COCTF_VCH" style="width:150px;" value="#COCTF_VCH#"><br />
            <input type="input" name="CTCTF_VCH" style="width:150px;" value="#CTCTF_VCH#"><br />
            <input type="input" name="DECTF_VCH" style="width:150px;" value="#DECTF_VCH#"><br />
            <input type="input" name="FLCTF_VCH" style="width:150px;" value="#FLCTF_VCH#"><br />
            <input type="input" name="GACTF_VCH" style="width:150px;" value="#GACTF_VCH#"><br />
            <input type="input" name="HICTF_VCH" style="width:150px;" value="#HICTF_VCH#"><br />
            <input type="input" name="IDCTF_VCH" style="width:150px;" value="#IDCTF_VCH#"><br />
            <input type="input" name="ILCTF_VCH" style="width:150px;" value="#ILCTF_VCH#"><br />
            <input type="input" name="INCTF_VCH" style="width:150px;" value="#INCTF_VCH#"><br />
            <input type="input" name="IACTF_VCH" style="width:150px;" value="#IACTF_VCH#"><br />
            <input type="input" name="KSCTF_VCH" style="width:150px;" value="#KSCTF_VCH#"><br />
            <input type="input" name="KYCTF_VCH" style="width:150px;" value="#KYCTF_VCH#"><br />
            <input type="input" name="LACTF_VCH" style="width:150px;" value="#LACTF_VCH#"><br />
            <input type="input" name="MACTF_VCH" style="width:150px;" value="#MACTF_VCH#"><br />
            <input type="input" name="MDCTF_VCH" style="width:150px;" value="#MDCTF_VCH#"><br />
            <input type="input" name="MECTF_VCH" style="width:150px;" value="#MECTF_VCH#"><br />
            <input type="input" name="MICTF_VCH" style="width:150px;" value="#MICTF_VCH#"><br />
            <input type="input" name="MNCTF_VCH" style="width:150px;" value="#MNCTF_VCH#"><br />
            <input type="input" name="MSCTF_VCH" style="width:150px;" value="#MSCTF_VCH#"><br />
            <input type="input" name="MOCTF_VCH" style="width:150px;" value="#MOCTF_VCH#"><br />
            <input type="input" name="MTCTF_VCH" style="width:150px;" value="#MTCTF_VCH#"><br />
            <input type="input" name="NECTF_VCH" style="width:150px;" value="#NECTF_VCH#"><br />
            <input type="input" name="NVCTF_VCH" style="width:150px;" value="#NVCTF_VCH#"><br />
            <input type="input" name="NHCTF_VCH" style="width:150px;" value="#NHCTF_VCH#"><br />
            <input type="input" name="NJCTF_VCH" style="width:150px;" value="#NJCTF_VCH#"><br />
            <input type="input" name="NMCTF_VCH" style="width:150px;" value="#NMCTF_VCH#"><br />
            <input type="input" name="NYCTF_VCH" style="width:150px;" value="#NYCTF_VCH#"><br />
            <input type="input" name="NCCTF_VCH" style="width:150px;" value="#NCCTF_VCH#"><br />
            <input type="input" name="NDCTF_VCH" style="width:150px;" value="#NDCTF_VCH#"><br />
            <input type="input" name="OHCTF_VCH" style="width:150px;" value="#OHCTF_VCH#"><br />
            <input type="input" name="OKCTF_VCH" style="width:150px;" value="#OKCTF_VCH#"><br />
            <input type="input" name="ORCTF_VCH" style="width:150px;" value="#ORCTF_VCH#"><br />
            <input type="input" name="PACTF_VCH" style="width:150px;" value="#PACTF_VCH#"><br />
            <input type="input" name="RICTF_VCH" style="width:150px;" value="#RICTF_VCH#"><br />
            <input type="input" name="SDCTF_VCH" style="width:150px;" value="#SDCTF_VCH#"><br />
            <input type="input" name="SCCTF_VCH" style="width:150px;" value="#SCCTF_VCH#"><br />
            <input type="input" name="TNCTF_VCH" style="width:150px;" value="#TNCTF_VCH#"><br />
            <input type="input" name="TXCTF_VCH" style="width:150px;" value="#TXCTF_VCH#"><br />
            <input type="input" name="UTCTF_VCH" style="width:150px;" value="#UTCTF_VCH#"><br />
            <input type="input" name="VTCTF_VCH" style="width:150px;" value="#VTCTF_VCH#"><br />
            <input type="input" name="VACTF_VCH" style="width:150px;" value="#VACTF_VCH#"><br />
            <input type="input" name="WACTF_VCH" style="width:150px;" value="#WACTF_VCH#"><br />
            <input type="input" name="WVCTF_VCH" style="width:150px;" value="#WVCTF_VCH#"><br />
            <input type="input" name="WICTF_VCH" style="width:150px;" value="#WICTF_VCH#"><br />
            <input type="input" name="WYCTF_VCH" style="width:150px;" value="#WYCTF_VCH#">
        </div>
        <div style="float:left; width:150px; margin-left:10px; display:inline-block;">
        	Business<br />
            <input type="input" name="ALBTF_VCH" style="width:150px;" value="#ALBTF_VCH#"><br />
            <input type="input" name="AKBTF_VCH" style="width:150px;" value="#AKBTF_VCH#"><br />
            <input type="input" name="AZBTF_VCH" style="width:150px;" value="#AZBTF_VCH#"><br />
            <input type="input" name="ARBTF_VCH" style="width:150px;" value="#ARBTF_VCH#"><br />
            <input type="input" name="CABTF_VCH" style="width:150px;" value="#CABTF_VCH#"><br />
            <input type="input" name="COBTF_VCH" style="width:150px;" value="#COBTF_VCH#"><br />
            <input type="input" name="CTBTF_VCH" style="width:150px;" value="#CTBTF_VCH#"><br />
            <input type="input" name="DEBTF_VCH" style="width:150px;" value="#DEBTF_VCH#"><br />
            <input type="input" name="FLBTF_VCH" style="width:150px;" value="#FLBTF_VCH#"><br />
            <input type="input" name="GABTF_VCH" style="width:150px;" value="#GABTF_VCH#"><br />
            <input type="input" name="HIBTF_VCH" style="width:150px;" value="#HIBTF_VCH#"><br />
            <input type="input" name="IDBTF_VCH" style="width:150px;" value="#IDBTF_VCH#"><br />
            <input type="input" name="ILBTF_VCH" style="width:150px;" value="#ILBTF_VCH#"><br />
            <input type="input" name="INBTF_VCH" style="width:150px;" value="#INBTF_VCH#"><br />
            <input type="input" name="IABTF_VCH" style="width:150px;" value="#IABTF_VCH#"><br />
            <input type="input" name="KSBTF_VCH" style="width:150px;" value="#KSBTF_VCH#"><br />
            <input type="input" name="KYBTF_VCH" style="width:150px;" value="#KYBTF_VCH#"><br />
            <input type="input" name="LABTF_VCH" style="width:150px;" value="#LABTF_VCH#"><br />
            <input type="input" name="MABTF_VCH" style="width:150px;" value="#MABTF_VCH#"><br />
            <input type="input" name="MDBTF_VCH" style="width:150px;" value="#MDBTF_VCH#"><br />
            <input type="input" name="MEBTF_VCH" style="width:150px;" value="#MEBTF_VCH#"><br />
            <input type="input" name="MIBTF_VCH" style="width:150px;" value="#MIBTF_VCH#"><br />
            <input type="input" name="MNBTF_VCH" style="width:150px;" value="#MNBTF_VCH#"><br />
            <input type="input" name="MSBTF_VCH" style="width:150px;" value="#MSBTF_VCH#"><br />
            <input type="input" name="MOBTF_VCH" style="width:150px;" value="#MOBTF_VCH#"><br />
            <input type="input" name="MTBTF_VCH" style="width:150px;" value="#MTBTF_VCH#"><br />
            <input type="input" name="NEBTF_VCH" style="width:150px;" value="#NEBTF_VCH#"><br />
            <input type="input" name="NVBTF_VCH" style="width:150px;" value="#NVBTF_VCH#"><br />
            <input type="input" name="NHBTF_VCH" style="width:150px;" value="#NHBTF_VCH#"><br />
            <input type="input" name="NJBTF_VCH" style="width:150px;" value="#NJBTF_VCH#"><br />
            <input type="input" name="NMBTF_VCH" style="width:150px;" value="#NMBTF_VCH#"><br />
            <input type="input" name="NYBTF_VCH" style="width:150px;" value="#NYBTF_VCH#"><br />
            <input type="input" name="NCBTF_VCH" style="width:150px;" value="#NCBTF_VCH#"><br />
            <input type="input" name="NDBTF_VCH" style="width:150px;" value="#NDBTF_VCH#"><br />
            <input type="input" name="OHBTF_VCH" style="width:150px;" value="#OHBTF_VCH#"><br />
            <input type="input" name="OKBTF_VCH" style="width:150px;" value="#OKBTF_VCH#"><br />
            <input type="input" name="ORBTF_VCH" style="width:150px;" value="#ORBTF_VCH#"><br />
            <input type="input" name="PABTF_VCH" style="width:150px;" value="#PABTF_VCH#"><br />
            <input type="input" name="RIBTF_VCH" style="width:150px;" value="#RIBTF_VCH#"><br />
            <input type="input" name="SDBTF_VCH" style="width:150px;" value="#SDBTF_VCH#"><br />
            <input type="input" name="SCBTF_VCH" style="width:150px;" value="#SCBTF_VCH#"><br />
            <input type="input" name="TNBTF_VCH" style="width:150px;" value="#TNBTF_VCH#"><br />
            <input type="input" name="TXBTF_VCH" style="width:150px;" value="#TXBTF_VCH#"><br />
            <input type="input" name="UTBTF_VCH" style="width:150px;" value="#UTBTF_VCH#"><br />
            <input type="input" name="VTBTF_VCH" style="width:150px;" value="#VTBTF_VCH#"><br />
            <input type="input" name="VABTF_VCH" style="width:150px;" value="#VABTF_VCH#"><br />
            <input type="input" name="WABTF_VCH" style="width:150px;" value="#WABTF_VCH#"><br />
            <input type="input" name="WVBTF_VCH" style="width:150px;" value="#WVBTF_VCH#"><br />
            <input type="input" name="WIBTF_VCH" style="width:150px;" value="#WIBTF_VCH#"><br />
            <input type="input" name="WYBTF_VCH" style="width:150px;" value="#WYBTF_VCH#">

        </div>
      </div>
      <div class="dt" id="t6">Transfer Numbers</div>
      	<div class="dd" id="d6" style="position:relative; display:inline-block;height:500px;">
      	<div style="width:100%; margin-bottom:15px;">
        	Place a check next to all the states you will include a Transfer Number and the consumer and/or business Transfer number you wish to use.
            <div style="margin-top:15px;">
        		<input type="checkbox" onclick="toggleChecked2(this.checked)"> Select / De-Select All States
            </div>
        </div>
      	<div style="float:left; width:60px; display:inline-block;">
        	State<br />
            <input type="checkbox" name="TNAL" class="pist2" style="margin-right:10px;"<cfif TNAL eq 1> checked="checked"</cfif>>AL<br />
            <input type="checkbox" name="TNAK" class="pist2" style="margin-right:10px;"<cfif TNAK eq 1> checked="checked"</cfif>>AK<br />
            <input type="checkbox" name="TNAZ" class="pist2" style="margin-right:10px;"<cfif TNAZ eq 1> checked="checked"</cfif>>AZ<br />
            <input type="checkbox" name="TNAR" class="pist2" style="margin-right:10px;"<cfif TNAR eq 1> checked="checked"</cfif>>AR<br />
            <input type="checkbox" name="TNCA" class="pist2" style="margin-right:10px;"<cfif TNCA eq 1> checked="checked"</cfif>>CA<br />
            <input type="checkbox" name="TNCO" class="pist2" style="margin-right:10px;"<cfif TNCO eq 1> checked="checked"</cfif>>CO<br />
            <input type="checkbox" name="TNCT" class="pist2" style="margin-right:10px;"<cfif TNCT eq 1> checked="checked"</cfif>>CT<br />
            <input type="checkbox" name="TNDE" class="pist2" style="margin-right:10px;"<cfif TNDE eq 1> checked="checked"</cfif>>DE<br />
            <input type="checkbox" name="TNFL" class="pist2" style="margin-right:10px;"<cfif TNFL eq 1> checked="checked"</cfif>>FL<br />
            <input type="checkbox" name="TNGA" class="pist2" style="margin-right:10px;"<cfif TNGA eq 1> checked="checked"</cfif>>GA<br />
            <input type="checkbox" name="TNHI" class="pist2" style="margin-right:10px;"<cfif TNHI eq 1> checked="checked"</cfif>>HI<br />
            <input type="checkbox" name="TNID" class="pist2" style="margin-right:10px;"<cfif TNID eq 1> checked="checked"</cfif>>ID<br />
            <input type="checkbox" name="TNIL" class="pist2" style="margin-right:10px;"<cfif TNIL eq 1> checked="checked"</cfif>>IL<br />
            <input type="checkbox" name="TNIN" class="pist2" style="margin-right:10px;"<cfif TNIN eq 1> checked="checked"</cfif>>IN<br />
            <input type="checkbox" name="TNIA" class="pist2" style="margin-right:10px;"<cfif TNIA eq 1> checked="checked"</cfif>>IA<br />
            <input type="checkbox" name="TNKS" class="pist2" style="margin-right:10px;"<cfif TNKS eq 1> checked="checked"</cfif>>KS<br />
            <input type="checkbox" name="TNKY" class="pist2" style="margin-right:10px;"<cfif TNKY eq 1> checked="checked"</cfif>>KY<br />
            <input type="checkbox" name="TNLA" class="pist2" style="margin-right:10px;"<cfif TNLA eq 1> checked="checked"</cfif>>LA<br />
            <input type="checkbox" name="TNME" class="pist2" style="margin-right:10px;"<cfif TNME eq 1> checked="checked"</cfif>>ME<br />
            <input type="checkbox" name="TNMD" class="pist2" style="margin-right:10px;"<cfif TNMD eq 1> checked="checked"</cfif>>MD<br />
            <input type="checkbox" name="TNME" class="pist2" style="margin-right:10px;"<cfif TNME eq 1> checked="checked"</cfif>>ME<br />
            <input type="checkbox" name="TNMI" class="pist2" style="margin-right:10px;"<cfif TNMI eq 1> checked="checked"</cfif>>MI<br />
            <input type="checkbox" name="TNMN" class="pist2" style="margin-right:10px;"<cfif TNMN eq 1> checked="checked"</cfif>>MN<br />
            <input type="checkbox" name="TNMS" class="pist2" style="margin-right:10px;"<cfif TNMS eq 1> checked="checked"</cfif>>MS<br />
            <input type="checkbox" name="TNMO" class="pist2" style="margin-right:10px;"<cfif TNMO eq 1> checked="checked"</cfif>>MO<br />
            <input type="checkbox" name="TNMT" class="pist2" style="margin-right:10px;"<cfif TNMT eq 1> checked="checked"</cfif>>MT<br />
            <input type="checkbox" name="TNNE" class="pist2" style="margin-right:10px;"<cfif TNNE eq 1> checked="checked"</cfif>>NE<br />
            <input type="checkbox" name="TNNV" class="pist2" style="margin-right:10px;"<cfif TNNV eq 1> checked="checked"</cfif>>NV<br />
            <input type="checkbox" name="TNNH" class="pist2" style="margin-right:10px;"<cfif TNNH eq 1> checked="checked"</cfif>>NH<br />
            <input type="checkbox" name="TNNJ" class="pist2" style="margin-right:10px;"<cfif TNNJ eq 1> checked="checked"</cfif>>NJ<br />
            <input type="checkbox" name="TNNM" class="pist2" style="margin-right:10px;"<cfif TNNM eq 1> checked="checked"</cfif>>NM<br />
            <input type="checkbox" name="TNNY" class="pist2" style="margin-right:10px;"<cfif TNNY eq 1> checked="checked"</cfif>>NY<br />
            <input type="checkbox" name="TNNC" class="pist2" style="margin-right:10px;"<cfif TNNC eq 1> checked="checked"</cfif>>NC<br />
            <input type="checkbox" name="TNND" class="pist2" style="margin-right:10px;"<cfif TNND eq 1> checked="checked"</cfif>>ND<br />
            <input type="checkbox" name="TNOH" class="pist2" style="margin-right:10px;"<cfif TNOH eq 1> checked="checked"</cfif>>OH<br />
            <input type="checkbox" name="TNOK" class="pist2" style="margin-right:10px;"<cfif TNOK eq 1> checked="checked"</cfif>>OK<br />
            <input type="checkbox" name="TNOR" class="pist2" style="margin-right:10px;"<cfif TNOR eq 1> checked="checked"</cfif>>OR<br />
            <input type="checkbox" name="TNPA" class="pist2" style="margin-right:10px;"<cfif TNPA eq 1> checked="checked"</cfif>>PA<br />
            <input type="checkbox" name="TNRI" class="pist2" style="margin-right:10px;"<cfif TNRI eq 1> checked="checked"</cfif>>RI<br />
            <input type="checkbox" name="TNSD" class="pist2" style="margin-right:10px;"<cfif TNSD eq 1> checked="checked"</cfif>>SD<br />
            <input type="checkbox" name="TNSC" class="pist2" style="margin-right:10px;"<cfif TNSC eq 1> checked="checked"</cfif>>SC<br />
            <input type="checkbox" name="TNTN" class="pist2" style="margin-right:10px;"<cfif TNTN eq 1> checked="checked"</cfif>>TN<br />
            <input type="checkbox" name="TNTX" class="pist2" style="margin-right:10px;"<cfif TNTX eq 1> checked="checked"</cfif>>TX<br />
            <input type="checkbox" name="TNUT" class="pist2" style="margin-right:10px;"<cfif TNUT eq 1> checked="checked"</cfif>>UT<br />
            <input type="checkbox" name="TNVT" class="pist2" style="margin-right:10px;"<cfif TNVT eq 1> checked="checked"</cfif>>VT<br />
            <input type="checkbox" name="TNVA" class="pist2" style="margin-right:10px;"<cfif TNVA eq 1> checked="checked"</cfif>>VA<br />
            <input type="checkbox" name="TNWA" class="pist2" style="margin-right:10px;"<cfif TNWA eq 1> checked="checked"</cfif>>WA<br />
            <input type="checkbox" name="TNWV" class="pist2" style="margin-right:10px;"<cfif TNWV eq 1> checked="checked"</cfif>>WV<br />
            <input type="checkbox" name="TNWI" class="pist2" style="margin-right:10px;"<cfif TNWI eq 1> checked="checked"</cfif>>WI<br />
            <input type="checkbox" name="TNWY" class="pist2" style="margin-right:10px;"<cfif TNWY eq 1> checked="checked"</cfif>>WY
        </div>
        <div style="float:left; width:150px; display:inline-block;">
        	Consumer<br />
            <input type="input" name="ALCTN_VCH" style="width:150px;" value="#ALCTN_VCH#"><br />
            <input type="input" name="AKCTN_VCH" style="width:150px;" value="#AKCTN_VCH#"><br />
            <input type="input" name="AZCTN_VCH" style="width:150px;" value="#AZCTN_VCH#"><br />
            <input type="input" name="ARCTN_VCH" style="width:150px;" value="#ARCTN_VCH#"><br />
            <input type="input" name="CACTN_VCH" style="width:150px;" value="#CACTN_VCH#"><br />
            <input type="input" name="COCTN_VCH" style="width:150px;" value="#COCTN_VCH#"><br />
            <input type="input" name="CTCTN_VCH" style="width:150px;" value="#CTCTN_VCH#"><br />
            <input type="input" name="DECTN_VCH" style="width:150px;" value="#DECTN_VCH#"><br />
            <input type="input" name="FLCTN_VCH" style="width:150px;" value="#FLCTN_VCH#"><br />
            <input type="input" name="GACTN_VCH" style="width:150px;" value="#GACTN_VCH#"><br />
            <input type="input" name="HICTN_VCH" style="width:150px;" value="#HICTN_VCH#"><br />
            <input type="input" name="IDCTN_VCH" style="width:150px;" value="#IDCTN_VCH#"><br />
            <input type="input" name="ILCTN_VCH" style="width:150px;" value="#ILCTN_VCH#"><br />
            <input type="input" name="INCTN_VCH" style="width:150px;" value="#INCTN_VCH#"><br />
            <input type="input" name="IACTN_VCH" style="width:150px;" value="#IACTN_VCH#"><br />
            <input type="input" name="KSCTN_VCH" style="width:150px;" value="#KSCTN_VCH#"><br />
            <input type="input" name="KYCTN_VCH" style="width:150px;" value="#KYCTN_VCH#"><br />
            <input type="input" name="LACTN_VCH" style="width:150px;" value="#LACTN_VCH#"><br />
            <input type="input" name="MACTN_VCH" style="width:150px;" value="#MACTN_VCH#"><br />
            <input type="input" name="MDCTN_VCH" style="width:150px;" value="#MDCTN_VCH#"><br />
            <input type="input" name="MECTN_VCH" style="width:150px;" value="#MECTN_VCH#"><br />
            <input type="input" name="MICTN_VCH" style="width:150px;" value="#MICTN_VCH#"><br />
            <input type="input" name="MNCTN_VCH" style="width:150px;" value="#MNCTN_VCH#"><br />
            <input type="input" name="MSCTN_VCH" style="width:150px;" value="#MSCTN_VCH#"><br />
            <input type="input" name="MOCTN_VCH" style="width:150px;" value="#MOCTN_VCH#"><br />
            <input type="input" name="MTCTN_VCH" style="width:150px;" value="#MTCTN_VCH#"><br />
            <input type="input" name="NECTN_VCH" style="width:150px;" value="#NECTN_VCH#"><br />
            <input type="input" name="NVCTN_VCH" style="width:150px;" value="#NVCTN_VCH#"><br />
            <input type="input" name="NHCTN_VCH" style="width:150px;" value="#NHCTN_VCH#"><br />
            <input type="input" name="NJCTN_VCH" style="width:150px;" value="#NJCTN_VCH#"><br />
            <input type="input" name="NMCTN_VCH" style="width:150px;" value="#NMCTN_VCH#"><br />
            <input type="input" name="NYCTN_VCH" style="width:150px;" value="#NYCTN_VCH#"><br />
            <input type="input" name="NCCTN_VCH" style="width:150px;" value="#NCCTN_VCH#"><br />
            <input type="input" name="NDCTN_VCH" style="width:150px;" value="#NDCTN_VCH#"><br />
            <input type="input" name="OHCTN_VCH" style="width:150px;" value="#OHCTN_VCH#"><br />
            <input type="input" name="OKCTN_VCH" style="width:150px;" value="#OKCTN_VCH#"><br />
            <input type="input" name="ORCTN_VCH" style="width:150px;" value="#ORCTN_VCH#"><br />
            <input type="input" name="PACTN_VCH" style="width:150px;" value="#PACTN_VCH#"><br />
            <input type="input" name="RICTN_VCH" style="width:150px;" value="#RICTN_VCH#"><br />
            <input type="input" name="SDCTN_VCH" style="width:150px;" value="#SDCTN_VCH#"><br />
            <input type="input" name="SCCTN_VCH" style="width:150px;" value="#SCCTN_VCH#"><br />
            <input type="input" name="TNCTN_VCH" style="width:150px;" value="#TNCTN_VCH#"><br />
            <input type="input" name="TXCTN_VCH" style="width:150px;" value="#TXCTN_VCH#"><br />
            <input type="input" name="UTCTN_VCH" style="width:150px;" value="#UTCTN_VCH#"><br />
            <input type="input" name="VTCTN_VCH" style="width:150px;" value="#VTCTN_VCH#"><br />
            <input type="input" name="VACTN_VCH" style="width:150px;" value="#VACTN_VCH#"><br />
            <input type="input" name="WACTN_VCH" style="width:150px;" value="#WACTN_VCH#"><br />
            <input type="input" name="WVCTN_VCH" style="width:150px;" value="#WVCTN_VCH#"><br />
            <input type="input" name="WICTN_VCH" style="width:150px;" value="#WICTN_VCH#"><br />
            <input type="input" name="WYCTN_VCH" style="width:150px;" value="#WYCTN_VCH#">
            
        </div>
        <div style="float:left; width:150px; margin-left:10px; display:inline-block;">
        	Business<br />
            <input type="input" name="ALBTN_VCH" style="width:150px;" value="#ALBTN_VCH#"><br />
            <input type="input" name="AKBTN_VCH" style="width:150px;" value="#AKBTN_VCH#"><br />
            <input type="input" name="AZBTN_VCH" style="width:150px;" value="#AZBTN_VCH#"><br />
            <input type="input" name="ARBTN_VCH" style="width:150px;" value="#ARBTN_VCH#"><br />
            <input type="input" name="CABTN_VCH" style="width:150px;" value="#CABTN_VCH#"><br />
            <input type="input" name="COBTN_VCH" style="width:150px;" value="#COBTN_VCH#"><br />
            <input type="input" name="CTBTN_VCH" style="width:150px;" value="#CTBTN_VCH#"><br />
            <input type="input" name="DEBTN_VCH" style="width:150px;" value="#DEBTN_VCH#"><br />
            <input type="input" name="FLBTN_VCH" style="width:150px;" value="#FLBTN_VCH#"><br />
            <input type="input" name="GABTN_VCH" style="width:150px;" value="#GABTN_VCH#"><br />
            <input type="input" name="HIBTN_VCH" style="width:150px;" value="#HIBTN_VCH#"><br />
            <input type="input" name="IDBTN_VCH" style="width:150px;" value="#IDBTN_VCH#"><br />
            <input type="input" name="ILBTN_VCH" style="width:150px;" value="#ILBTN_VCH#"><br />
            <input type="input" name="INBTN_VCH" style="width:150px;" value="#INBTN_VCH#"><br />
            <input type="input" name="IABTN_VCH" style="width:150px;" value="#IABTN_VCH#"><br />
            <input type="input" name="KSBTN_VCH" style="width:150px;" value="#KSBTN_VCH#"><br />
            <input type="input" name="KYBTN_VCH" style="width:150px;" value="#KYBTN_VCH#"><br />
            <input type="input" name="LABTN_VCH" style="width:150px;" value="#LABTN_VCH#"><br />
            <input type="input" name="MABTN_VCH" style="width:150px;" value="#MABTN_VCH#"><br />
            <input type="input" name="MDBTN_VCH" style="width:150px;" value="#MDBTN_VCH#"><br />
            <input type="input" name="MEBTN_VCH" style="width:150px;" value="#MEBTN_VCH#"><br />
            <input type="input" name="MIBTN_VCH" style="width:150px;" value="#MIBTN_VCH#"><br />
            <input type="input" name="MNBTN_VCH" style="width:150px;" value="#MNBTN_VCH#"><br />
            <input type="input" name="MSBTN_VCH" style="width:150px;" value="#MSBTN_VCH#"><br />
            <input type="input" name="MOBTN_VCH" style="width:150px;" value="#MOBTN_VCH#"><br />
            <input type="input" name="MTBTN_VCH" style="width:150px;" value="#MTBTN_VCH#"><br />
            <input type="input" name="NEBTN_VCH" style="width:150px;" value="#NEBTN_VCH#"><br />
            <input type="input" name="NVBTN_VCH" style="width:150px;" value="#NVBTN_VCH#"><br />
            <input type="input" name="NHBTN_VCH" style="width:150px;" value="#NHBTN_VCH#"><br />
            <input type="input" name="NJBTN_VCH" style="width:150px;" value="#NJBTN_VCH#"><br />
            <input type="input" name="NMBTN_VCH" style="width:150px;" value="#NMBTN_VCH#"><br />
            <input type="input" name="NYBTN_VCH" style="width:150px;" value="#NYBTN_VCH#"><br />
            <input type="input" name="NCBTN_VCH" style="width:150px;" value="#NCBTN_VCH#"><br />
            <input type="input" name="NDBTN_VCH" style="width:150px;" value="#NDBTN_VCH#"><br />
            <input type="input" name="OHBTN_VCH" style="width:150px;" value="#OHBTN_VCH#"><br />
            <input type="input" name="OKBTN_VCH" style="width:150px;" value="#OKBTN_VCH#"><br />
            <input type="input" name="ORBTN_VCH" style="width:150px;" value="#ORBTN_VCH#"><br />
            <input type="input" name="PABTN_VCH" style="width:150px;" value="#PABTN_VCH#"><br />
            <input type="input" name="RIBTN_VCH" style="width:150px;" value="#RIBTN_VCH#"><br />
            <input type="input" name="SDBTN_VCH" style="width:150px;" value="#SDBTN_VCH#"><br />
            <input type="input" name="SCBTN_VCH" style="width:150px;" value="#SCBTN_VCH#"><br />
            <input type="input" name="TNBTN_VCH" style="width:150px;" value="#TNBTN_VCH#"><br />
            <input type="input" name="TXBTN_VCH" style="width:150px;" value="#TXBTN_VCH#"><br />
            <input type="input" name="UTBTN_VCH" style="width:150px;" value="#UTBTN_VCH#"><br />
            <input type="input" name="VTBTN_VCH" style="width:150px;" value="#VTBTN_VCH#"><br />
            <input type="input" name="VABTN_VCH" style="width:150px;" value="#VABTN_VCH#"><br />
            <input type="input" name="WABTN_VCH" style="width:150px;" value="#WABTN_VCH#"><br />
            <input type="input" name="WVBTN_VCH" style="width:150px;" value="#WVBTN_VCH#"><br />
            <input type="input" name="WIBTN_VCH" style="width:150px;" value="#WIBTN_VCH#"><br />
            <input type="input" name="WYBTN_VCH" style="width:150px;" value="#WYBTN_VCH#">
        </div>
      </div>  
    </div>
</div>
</cfoutput>
</cfif>