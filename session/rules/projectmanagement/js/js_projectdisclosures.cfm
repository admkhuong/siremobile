<script>

function InitProjectDisclosuresview()
{
	$("#PM_ProjectDisclosures").data("loadf", "LoadBatchProjectDisclosuresItem");
	$("#PM_ProjectDisclosures").data("updatef", "UpdateBatchProjectDisclosuresItem");	
	
	 LoadBatchProjectDisclosuresItem();
}

function UpdateBatchProjectDisclosuresItem()
{
		$("#loadingDlgBatchRulesEditor").show();		
					
		$.ajax({
		url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RulesForm.cfc?method=UpdateProjectDisclosuresForm&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		dataType: 'json',
		type: 'POST',
		data:  { INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput>, PROJECTLEADCONTACT_VCH : $("#PM_ProjectDisclosures #PROJECTLEADCONTACT_VCH").val() },					  
		error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				<!---console.log(textStatus, errorThrown);--->
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Form Data has NOT been updated properly. .\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { $("#loadingDlgBatchRulesEditor").hide(); } );													
			},				
		success:			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{						
									
						
																							 
						}
																									
						$("#loadingDlgBatchRulesEditor").hide();
										
					}
					else
					{<!--- Invalid structure returned --->	
						$("#loadingDlgBatchRulesEditor").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					$("#loadingDlgBatchRulesEditor").hide();
				}				
			} 		
					
		});
	
		return false;
	
	
}

function LoadBatchProjectDisclosuresItem()
{
		$("#loadingDlgBatchRulesEditor").show();		
					
		$.ajax({
		url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/RulesForm.cfc?method=LoadProjectDisclosuresForm&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		dataType: 'json',
		type: 'POST',
		data:  { INPBATCHID: <cfoutput>#INPBATCHID#</cfoutput> },					  
		error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				<!---console.log(textStatus, errorThrown);--->
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Form Data has NOT been loaded properly. .\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { $("#loadingDlgBatchRulesEditor").hide(); } );													
			},				
		success:			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{																									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{							
							if(typeof(d.DATA.PROJECTLEADCONTACT_VCH[0]) != "undefined")
							{								
								$("#PM_ProjectDisclosures #PROJECTLEADCONTACT_VCH").val(d.DATA.PROJECTLEADCONTACT_VCH[0]);								
							}																							 
						}
																									
						$("#loadingDlgBatchRulesEditor").hide();
										
					}
					else
					{<!--- Invalid structure returned --->	
						$("#loadingDlgBatchRulesEditor").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					$("#loadingDlgBatchRulesEditor").hide();
				}				
			} 		
					
		});
	
		return false;
}

</script>
