<!---<!---<cfinclude template="config">--->--->
<cfset SESSION.userId = 1 />
<cfinclude template="lib/jsonencode.cfm">
<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "Add element success";
</cfscript>
	<cfset desc = label /> 
	<cfquery name="getScript" datasource="#application.datasource#">
		SELECT DSEId_int, Desc_vch
		FROM #application.database#.dselement
		WHERE UserId_int='#SESSION.userId#'
	</cfquery>
		<cfif getScript.RecordCount>
			<cfscript>
				retVal.success = false;
				retVal.message = "Element is existed. Please choose other element label.";
			</cfscript>
		<cfelse>
			<cfquery name="elInsert" datasource="#application.datasource#">  
				INSERT INTO #application.database#.dselement (UserId_int, Active_int, Desc_vch)
				VALUES('#SESSION.userId#', '1', '#desc#')
			</cfquery>
		</cfif>
<cfoutput>#jsonencode(retVal)#</cfoutput>