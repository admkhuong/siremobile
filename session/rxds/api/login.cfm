<!---<!---<cfinclude template="../config.cfm">--->--->
<cfinclude template="../lib/jsonencode.cfm">
<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "";
</cfscript>
<cfif getPageContext().getRequest().getMethod() EQ "GET">
	<cfscript>
		retVal.success = false;
		retVal.message = "Access denied.";
	</cfscript>
<cfelse>
	<cfif isDefined("username")>
		<cfquery name="loginCheck" datasource="#application.datasource#">
		  SELECT UserId_int, UserName_vch, Password_vch FROM #application.database#.useraccount
		  WHERE UserName_vch = '#username#'
			AND Password_vch = MD5('#password#')
			LIMIT 1
		</cfquery>
	 
		<cfif loginCheck.RecordCount>
			<cfscript>
				chars = "0123456789abcdefghiklmnopqrstuvwxyz";
				token = "";
				for (i=1; i LTE 64; i=i+1) {
					rnum = ceiling(rand() * len(chars));
					if (rnum EQ 0) {
						rnum = 1;
					}
					token = token & mid(chars, rnum, 1);
				}
			</cfscript>
			<cfquery name="updScript" datasource="#application.datasource#">
				UPDATE useraccount
				SET Token_vch = '#token#'
				WHERE UserId_int = '#loginCheck.UserId_int#'
				LIMIT 1
			</cfquery>
			<cfset retVal.token = #token# />
		<cfelse>
			<cfscript>
				retVal.success = false;
				retVal.message = "Username or Password incorrect";
			</cfscript>
		</cfif>
	</cfif>
</cfif>
<cfoutput>#jsonencode(retVal)#</cfoutput>