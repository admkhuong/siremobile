<cfapplication sessionmanagement="yes" clientmanagement="yes" name="Rxdu">
<cfscript>
	application.datasource = "rxdu";
	application.database = "rxdu";
	application.baseDir = GetDirectoryFromPath(GetCurrentTemplatePath());
	application.baseUrl = "http://" & CGI.HTTP_HOST & GetDirectoryFromPath(CGI.SCRIPT_NAME);
	application.soxPath = "C:\sox\sox.exe";
	application.soxMaxExeTime = 90;
</cfscript>
