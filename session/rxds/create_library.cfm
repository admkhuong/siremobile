<!---<cfinclude template="config.cfm">--->
<cfinclude template="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/rxds/lib/rxdu_user.cfc">



<cfif (NOT isDefined("SESSION.loggedIn")) OR (SESSION.loggedIn NEQ "1")>
	<cfset StructClear(SESSION) />
 	<cflocation url="#application.baseUrl#login" addtoken="no">
</cfif>

<cfif isDefined("form.name")>
	<cfset libId = GetNextLibraryId(SESSION.userId) />
	<cfquery name="libInsert" datasource="#application.datasource#">  
		INSERT INTO #application.database#.dynamicscript (UserId_int, DSId_int, Desc_vch)
		VALUES('#SESSION.userId#', '#libId#', '#form.name#')
	</cfquery>
	<cflocation url="#application.baseUrl#library" addtoken="no">
<cfelse>
	<cfset errorMessage = "Error!">
	<cfset form.name = "">
</cfif>

<html>
<head>
<title>Register</title>
</head>
<body>
	<cfif isDefined("errorMessage")>
	<p class="error">
		<cfoutput>
			#errorMessage#
		</cfoutput>
	</p>
	</cfif>
	<h1>Create Library</h1>
	<form action="create_library" method="post">
		x Name: <input type="text" name="name" value="<cfoutput>#form.name#</cfoutput>" /><br />
		<input type="submit" value="Submit" />
	</form>
</body>
</html>
