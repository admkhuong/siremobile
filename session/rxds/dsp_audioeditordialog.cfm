    
<cfparam name="INPBATCHID" default="0">


<script type="text/javascript"> 

$(function() 
{
	
	$("#CancelRXDSDialog").click(function(){
		$('#voiceAudio').remove(); 
	});	 
	openflashContent();	
					
});

	function closeRXDSPopup(){
		CreateNewScriptEditorDialog.remove();		
	}
	function openflashContent() {
		var flashvars = {
			urlHost : "<cfoutput>#application.flashApiBaseUrl#</cfoutput>",
			showLibrary: "true",
			urlLibrary: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/audioeditortree.swf"
		};
		var params = {
		 	quality:      'high',
		 	<!---bgcolor:	'#E8E6DC#',--->
			<!---bgcolor:	'#FFFFFF#',--->
			<!---bgcolor:	'#EBEBEB#',--->
			bgcolor:	'#FFFFFF#',
		 	play:		'true',
		 	loop:		'true',
		 	wmode:		'wmode',
		 	scale:		'showall',
		 	menu:		'true',
		 	devicefont:	'false',
		 	salign:		'',
		 	allowscriptaccess:    'sameDomain'
		 	
		};
		var attributes = {
			 id:                   'main', 
			 name:                 'main'
		};
<!---  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/RXDS/main.swf', 'flashContent', '100%', '100%', '9.0.124', false, flashvars, params, attributes);
--->	
			swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/main.swf', 'flashContent', '1050px;', '725px;', '9.0.124', false, flashvars, params, attributes);
	}
    

    
       
</script> 

<style>
#RXDSDialogMainx
	{
		margin:0 0;
		width:450px;
		padding:0px;
		border: none;
		min-height: 330px;
		height: 330px;
		font-size:12px;
	}
	
	
	#RXDSDialogMainx #LeftMenu
	{
		width:270px;
		min-width:270px;		
		background: #B6C29A;
		background: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(1, rgb(237,237,237)),
		color-stop(0, rgb(200,216,143))
		);
		background: -moz-linear-gradient(
			center top,
			rgb(237,237,237),
			rgb(200,216,143)
		);
		
		position:absolute;
		top:-8px;
		left:-17px;
		padding:15px;
		margin:0px;	
		border: 0;
		border-right: 1px solid #CCC;
		box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
		min-height: 765px;
		height: 765px;
		z-index:2300;
	}
	
	
	#RXDSDialogMainx #RightStage
	{
		position:absolute;
		top:0;
		left:287px;
		padding:15px;
		margin:0px;	
		border: 0;
		min-height:800px;
		min-width:800px;
		height:600px;
		width:1100px;
	}
			
	#RXDSDialogContentHTML
	{
		position:absolute;
		top:120px;
	}
	
	#SMSAdvancedContentHTML
	{
		position:absolute;
		top:120px;
		background-color:#FFF;	
		border-top-left-radius:20px; 
		border-top-right-radius:20px; 
<!---		border-bottom-left-radius:20px; --->
  	    padding: 10px 15px 10px 10px;	
		border: 1px solid #B6B6B6;
	}
	
<!---	.SaveRXDSDialog
	{
		position:absolute;
		top:20px;
		left: 10px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	.CancelRXDSDialog
	{
		position:absolute;
		top:85px;
		left: 10px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}--->
	

	
	
	.SubjectRXDSDialog
	{
		position:absolute;
		top:85px;
		left: 95px;
		min-width: 80px;
		min-height:20px;	
		font-size:12px;
		font-weight:normal;
	}
	
	#RXDSDialogButtonOptions
	{
	
	}

		
	
	
</style>
    
    
    
        
    
    
        
<div id='RXDSDialogMainx' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Voice Content Library</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img src="../../../../public/images/ScriptLibraryWeb.png" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul> 
                <li>The <i>Voice</i> Content Library is where all of your pre-recorded audio message components are stored.</li> 
                <li>Pre-recorded audio message components can be used stand alone or combined to generate dynamic data driven audio messaging</li>                            
            </ul>                                              
        </div>
        
                 
            <div style="width 250px; margin:0px 0 5px 0;" class="RXForm">
             
               <BR />
               <BR />    
             
                <div style="margin: 10px 5px 10px 20px;">
            		<i>DEFINITION:</i> Voice is a term used for all of your audio only portion of your multi-channel communications.
            	</div>
            
	           <BR />
               <BR />  
             
                <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
                           
                
                <div style="margin: 30px 5px 3px 20px;">
                    <ul> 
                        <li>Press "Exit" button to return to stage.</li>               
                    </ul>                                              
                </div>            
                <button id="CancelRXDSDialog" class="CancelRXDSDialog">Exit</button>
            
                
                <BR />
                <BR />
            
          
              
            </div>	    
            
        
	</div>
    
    <div id="RightStage">
    
            <div id="flashContent" style="margin-top:-5px;">
            </div>
    
    </div>

</div>



