<!---<cfinclude template="../config.cfm">--->
<cfset rnd = RAND() * 1000000000000 />
<cfset unixTime = DateDiff("s", CreateDate(1970, 1, 1), Now()) />
<cfif IsArray(getHTTPRequestData().content) and ArrayLen(getHTTPRequestData().content)>
	<cfset fileName = "#unixTime#_#rnd#" />
	<cfset srcFile = "#application.baseDir#/tmp/tmp_#fileName#.#type#" />
	<cffile action="write" file = "#srcFile#" output = "#getHTTPRequestData().content#" nameconflict="overwrite">
	<cfset dstFile = "#application.baseDir#/tmp/#fileName#.mp3" />
	<cfif fileExists("#dstFile#")>
		<cffile action="delete"	file="#dstFile#" />
	</cfif>
	<cfset cType = getPageContext().getServletContext().getMimeType("#srcFile#") />
		
	<!--- <cfdump var="#contentType#" /> --->
	<cfoutput>#cType#</cfoutput>
	<cfexecute
		name = "#application.soxPath#"
		arguments = '"#srcFile#" #application.soxDefaultArguments# "#dstFile#" #application.soxDefaultEffects#' 
		outputFile = "#application.baseDir#soxlog.txt"
		timeout = "#application.soxMaxExeTime#">
	</cfexecute>
	<cffile action="delete"	file="#srcFile#" />

	<!--- <cfheader name="Content-Disposition" value="attachment;filename=#fileName#.mp3" /> --->
	<cfcontent type="audio/mpeg" file="#dstFile#" />
	<!--- <cfoutput>#srcFile#</cfoutput> --->
<cfelse>
	<cfoutput>No Record Data Posted</cfoutput>
</cfif>

