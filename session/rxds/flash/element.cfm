<!---<cfinclude template="../config.cfm">--->
<cfinclude template="../lib/jsonencode.cfm">
<cfinclude template="../lib/rxdu_library.cfc">
<cfinclude template="../lib/rxdu_element.cfc">
<cfif (NOT isDefined("SESSION.USERID"))>
	<cfset StructClear(SESSION) />
 	<cflocation url="../login" addtoken="no">
</cfif>
<!---<cfset SESSION.loggedIn = "1">
<cfset SESSION.userId = "1">
<cfset SESSION.userName = "votong">
<cfset SESSION.isAdmin = true />--->

<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "";
</cfscript>
<cfswitch expression="#lCase(a)#">
	<cfcase value="i">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			ids = ListToArray(libraryId, "_");
			userId = ids[1];
			libraryId = ids[2];
		</cfscript>
		<cfset eleId = GetNextElementId(userId, libraryId) />
		<cfquery name="eleInsert" datasource="#application.datasource#">
			INSERT INTO 
				#application.database#.dselement (UserId_int, DSId_int, DSEId_int, Desc_vch)
			VALUES
			(
				'#userId#', 
				'#libraryId#', 
				'#eleId#', 
				'#name#'
			)
		</cfquery>
		<cfscript>
			retVal.userId = userId;
			retVal.libraryId = libraryId;
			retVal.elementId = eleId;
			retVal.elementName = name;
		</cfscript>
	</cfcase>
	
	<cfcase value="u">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
		</cfscript>
		<cfif NOT isDefined("elementId")>
			<cfscript>
				retVal.success = false;
				retVal.message = "ElementId is incorect";
			</cfscript>
		<cfelse>
			<cfset ids = ListToArray(elementId, "_") />
			<cfset userId = ids[1] /> 
			<cfset libId = ids[2] /> 
			<cfset eleId = ids[3] />
			
			<cfquery name="eleUpdate" datasource="#application.datasource#">
				UPDATE 
					#application.database#.dselement 
				SET 
					Desc_vch = '#name#'
				WHERE 
					UserId_int = '#userId#' 
				AND 
					DSId_int = '#libId#' 
				AND 
					DSEId_int = '#eleId#'
				LIMIT 1
			</cfquery>
			<cfscript>
				retVal.userId = userId;
				retVal.libraryId = libId;
				retVal.elementId = eleId;
				retVal.elementName = name;
			</cfscript>
		</cfif>
	</cfcase>
	
	<!--- Paste script --->
	<cfcase value="p">
		<cfif NOT isDefined("elementId")>
			<cfscript>
				retVal.success = false;
				retVal.message = "ElementId is incorect";
			</cfscript>
		<cfelse>
			<cfset ids = ListToArray(elementId, "_") />
			<cfset userId = ids[1] /> 
			<cfset libId = ids[2] /> 
			<cfset eleId = ids[3] />
			
			<cfquery name="eleUpdate" datasource="#application.datasource#">
				UPDATE #application.database#.dselement 
				SET Desc_vch = '#name#'
				WHERE UserId_int = '#userId#' AND DSId_int = '#libId#' AND DSEId_int = '#eleId#'
				LIMIT 1
			</cfquery>
			<cfscript>
				retVal.userId = userId;
				retVal.libraryId = libId;
				retVal.elementId = eleId;
				retVal.elementName = name;
			</cfscript>
		</cfif>
	</cfcase>
	
	<cfcase value="abs">
		<cfscript>
			if (NOT isDefined("elementId")) {
				retVal.success = false;
				retVal.message = "ElementId is incorect.";
			} else {
				if (NOT isDefined("name")) {
					retVal.success = false;
					retVal.message = "Script Name is required.";
				} else {
					retVal = AddBlankScript(elementId, name);
				}
			}
		</cfscript>
	</cfcase>
	
	
</cfswitch>

<cfoutput>#jsonencode(retVal)#</cfoutput>