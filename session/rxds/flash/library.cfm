<!---<cfinclude template="../config.cfm">--->
<cfinclude template="../lib/jsonencode.cfm">
<cfinclude template="../lib/rxdu_user.cfc">
<cfinclude template="../lib/rxdu_library.cfc">
<cfinclude template="../lib/rxdu_element.cfc">
<cfif (NOT isDefined("SESSION.USERID"))>
	<cfset StructClear(SESSION) />
 	<cflocation url="../login" addtoken="no">
</cfif>


<cfswitch expression="#lCase(a)#">
	<cfcase value="i">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
		</cfscript>
		<cfset libId = GetNextLibraryId(userId) />
		<cfquery name="libInsert" datasource="#application.datasource#">
			INSERT INTO #application.database#.dynamicscript (UserId_int, DSId_int, Desc_vch)
			VALUES('#userId#', '#libId#', '#name#')
		</cfquery>
		<cfscript>
			retVal.userId = userId;
			retVal.libraryId = libId;
			retVal.libraryName = name;
		</cfscript>
	</cfcase>
	
	<cfcase value="u">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
		</cfscript>
		<cfif NOT isDefined("libraryId")>
			<cfscript>
				retVal.success = false;
				retVal.message = "LibraryId is incorect";
			</cfscript>
		<cfelse>
			<cfset ids = ListToArray(libraryId, "_") />
			<cfset userId = ids[1] /> 
			<cfset libId = ids[2] />
			
			<cfquery name="libUpdate" datasource="#application.datasource#">
				UPDATE #application.database#.dynamicscript 
				SET Desc_vch = '#name#'
				WHERE UserId_int = '#userId#' AND DSId_int = '#libId#'
				LIMIT 1
			</cfquery>
			<cfscript>
				retVal.userId = userId;
				retVal.libraryId = libId;
				retVal.libraryName = name;
			</cfscript> 
		</cfif>
	</cfcase>
	
	<!--- Paste element --->
	<cfcase value="p">
		<cfset retVal = CopyElement(elementId, libraryId) />
	</cfcase>
</cfswitch>

<cfoutput>#jsonencode(retVal)#</cfoutput>