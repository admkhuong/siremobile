<cfinclude template="../lib/jsonencode.cfm">
<cfinclude template="../lib/rxdu_script.cfc">
<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "";
</cfscript> 
<cftry>
	<cfif isDefined("eleId")>
		<!--- Record and add to a element --->
		<cfset ids = ListToArray(eleId, "_") />
		<cfset userId = ids[1] />
		<cfset libId = ids[2] />
		<cfset eleId = ids[3] />
		<cfquery name="getEle" datasource="#application.datasource#">
			SELECT 
				DSEId_int, DSId_int, UserId_int, Desc_vch 
			FROM 
				#application.database#.dselement 
			WHERE 
				UserId_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#userId#"> 
			AND 
				DSId_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#libId#"> 
			AND 
				DSEId_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#eleId#"> 
			AND 
				Active_int='1' 
			LIMIT 1 
		</cfquery>
		<cfif getEle.RecordCount>
			<cfif IsArray(getHTTPRequestData().content) and ArrayLen(getHTTPRequestData().content)>
				<cfquery name="getNextScriptId" datasource="#application.datasource#">
					SELECT 
						CASE WHEN MAX(DataId_int) IS NULL 
						THEN 1 ELSE MAX(DataId_int) + 1 
						END AS NextScriptId 
					FROM 
						#application.database#.scriptdata 
					WHERE 
						UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#Session.UserID#"> 
					AND 
						DSID_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getEle.DSId_int#"> 
					AND 
						DSEId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getEle.DSEId_int#"> 
				</cfquery>
				<cfset scrId = getNextScriptId.NextScriptId />
				<cfif Not DirectoryExists("#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/")>
					<cfdirectory action = "create" directory = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/" recurse="yes" mode="0775" />
				</cfif>
				<cffile action="write" file = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/RXDS_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3" output = "#getHTTPRequestData().content#">
				<cfscript>
					if (!isDefined("fname")) {
						fname = "Record_#userId#_#getEle.DSId_int#_#getEle.DSEId_int#_#scrId#.mp3";
					}
				</cfscript> 
				<cfquery name="scrInsert" datasource="#application.datasource#">
					INSERT INTO 
						#application.database#.scriptdata (DataId_int, DSEId_int, DSId_int, UserId_int, Desc_vch, Created_dt) 
					VALUES(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scrId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getEle.DSEId_int#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getEle.DSId_int#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fname#">, 
						NOW()) 
				</cfquery>
				<cfscript>
					retVal.node = StructNew();
					retVal.node.userId = userId;
					retVal.node.libId = getEle.DSId_int;
					retVal.node.eleId = getEle.DSEId_int;
					retVal.node.id = scrId;
					if (isDefined("fname")) {
						retVal.node.label = "#fname#";
					} else {
						retVal.node.label = "Record_#userId#_#getEle.DSId_int#_#getEle.DSEId_int#_#scrId#.mp3";
					}
					retVal.node.url = "#application.baseUrl#get-script.cfm?id=#userId#_#getEle.DSId_int#_#getEle.DSEId_int#_#scrId#";
				</cfscript> 
			<cfelse>
				<cfscript>
					retVal.success = false;
					retVal.message = "No Record Data Posted";
				</cfscript> 
			</cfif>
		<cfelse>
			<cfscript>retVal.success = false;
				retVal.message = "EleId is incorect";</cfscript> 
		</cfif>
	<cfelseif isDefined("scrId")>
		<!--- Record and overwrite a existed script --->
		<cfset ids = ListToArray(scrId, "_") />
		<cfset userId = ids[1] />
		<cfset libId = ids[2] />
		<cfset eleId = ids[3] />
		<cfset scrId = ids[4] />
		<cfset script = IsExistedScript(userId, libId, eleId, scrId) />
		<cfif isBoolean(script)>
			<cfscript>retVal.success = false;
				retVal.message = "Script is not existed!";</cfscript> 
		<cfelse>
			<cfif IsArray(getHTTPRequestData().content) and ArrayLen(getHTTPRequestData().content)>				
				<cfif Not DirectoryExists("#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/")>
					<cfdirectory action = "create" directory = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/" recurse="yes" mode="0775" />
				</cfif>
				<cffile action="write" file = "#application.scriptPath#/U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#/RXDS_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3" output = "#getHTTPRequestData().content#">
				<cfscript>
					retVal.node = StructNew();
					retVal.node.userId = userId;
					retVal.node.libId = libId;
					retVal.node.eleId = eleId;
					retVal.node.id = scrId;
					retVal.node.label = script.Desc_vch;
					retVal.node.url = "#application.baseUrl#get-script.cfm?id=#userId#_#libId#_#eleId#_#scrId#";
				</cfscript> 
			<cfelse>
				<cfscript>
					retVal.success = false;
					retVal.message = "No Record Data Posted";
				</cfscript> 
			</cfif>
		</cfif>
	<cfelse>
		<cfscript>retVal.success = false;
			retVal.message = "Id is incorect";</cfscript> 
	</cfif>
	<cfcatch type="any">
		<cfscript>
			retVal.success = false;
			retVal.message = cfcatch.message & ":" & cfcatch.detail;
		</cfscript> 
	</cfcatch>
</cftry>
<cfoutput>#jsonencode(retVal)#</cfoutput>
