<cfparam name="filename" default="">
<cfparam name="scrId" default="">
<cfparam name="eleId" default="">
<cfparam name="clientId" default="">
<cfparam name="callbackFunction" default="SaveSystemPromptVoice">
<cfparam name="QID" default="">
<cfinclude template="/#sessionPath#/ire/constants/surveyConstants.cfm">

<style type="text/css" media="screen">
html, body { height:100%; background-color: #ffffff;}
body { margin:20; padding:0; }
</style>
<link href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/survey/previewVoice.css" rel="stylesheet">
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/jPlayer/js/jquery.jplayer.min.js"></script>
<link href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/jPlayer/skin/blue.monday/jplayer.blue.monday.css" rel="stylesheet">	

<cfif scrId NEQ "">
	<cfset eleKey = "scrId">
	<cfset eleValue = scrId>
<cfelse>	
	<cfset eleKey = "eleId">
	<cfset eleValue = eleId>
</cfif>
<cfif QID EQ 5 OR QID EQ 6>
	<cfset isErrorOrTryAgain = true>
<cfelse>
	<cfset isErrorOrTryAgain = false>
</cfif>
<script type="text/javascript">
	$(document).ready(function(){
		var scriptId = $('#<cfoutput>#clientId#</cfoutput>').val();
		var recordType = "";
		if (scriptId != "") {
			$("#jquery_jplayer_1").jPlayer({
				ready: function () {
					$(this).jPlayer("setmedia", {
						mp3: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/act_getPreviewAudio?recordedAudioId=' + scriptId
					});
				},
				ended: function() { 
				},
				swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
				supplied: "mp3"
			});
			
			$('#jPlayer').show();
			
			recordType = "1";
		}
		else {
			$("#jquery_jplayer_1").jPlayer({
				ready: function () {
				},
				ended: function() { 
				},
				swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
				supplied: "mp3"
			});
			recordType = "2";
		}
		
		$('#ddlVoiceType').val(recordType);
		RecordType_OnChange();	
		
		$('#btnSave').click(function() {
			var scriptId = $('#hidScriptId').val();
			var recordType = $('#ddlVoiceType').val();
			
			if (recordType == "1") {
				if (scriptId == "") {
					jAlert("Please record a voice");
					return;
				}
				$('#<cfoutput>#clientId#</cfoutput>').val(scriptId)
				<cfif isErrorOrTryAgain>
					$('#<cfoutput>#clientId#</cfoutput>').attr('desc', "");
				</cfif>
			}
			else {
				$('#<cfoutput>#clientId#</cfoutput>').val("")
				<cfif isErrorOrTryAgain>
					var description = $('#txtDescription').val();
					if (description == '<cfoutput>#VOICEPROMPT#</cfoutput>') {
						description = '';
					}
					$('#<cfoutput>#clientId#</cfoutput>').attr('desc', description);
				</cfif>
			}
			
			<cfif NOT isErrorOrTryAgain>
				var func = "<cfoutput>#callbackFunction#('#QID#</cfoutput>', '" + scriptId + "')";
			<cfelse>
				var description = $('#txtDescription').val();
				var func = "<cfoutput>#callbackFunction#('#QID#</cfoutput>', '" + scriptId + "', '" + description + "')";
			</cfif>
			eval(func);
			CloseVoiceRecordDialog();
		});
		
		<cfif isErrorOrTryAgain>
			var description = $('#<cfoutput>#clientId#</cfoutput>').attr('desc');
			if (description != "") {
				$('#txtDescription').val(description);
			}
		</cfif>
	});
	
	function CloseVoiceRecordDialog() {
		dialog.dialog('close');
	}
	function flash_callback(info){
		if (info.code=="RESPONSE_SAVE_RECORD_ERROR"){
			alert("save record failure");
		}
		if (info.code=="RESPONSE_SAVE_RECORD_SUCCESSFUL"){
			var response = JSON.parse(info.response);
			
			$('#jPlayer').show();
			var scriptId = response.NODE.USERID + "_" + response.NODE.LIBID + "_" + response.NODE.ELEID + "_" + response.NODE.ID

			var url = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/survey/act_getPreviewAudio?recordedAudioId=' + scriptId;

			$("#jquery_jplayer_1").jPlayer("setmedia", {
				mp3 : url
			}).jPlayer("play");
			
			$('#hidScriptId').val(scriptId);
		}
	}
	
	function RecordType_OnChange() {
		var recordType = $('#ddlVoiceType').val();
		if (recordType == "1") {
			$('#RECORDVOICE').show();
			$('#TEXTTOSPEECH').hide();
		}
		else {
			$('#RECORDVOICE').hide();
			$('#TEXTTOSPEECH').show();
		}
		
	}
	
</script>
	<form>
		<div class="voice_type_body">
		    <div class="left voice_type_label">
		    	<label >Voice Type:</label>
			</div>
			<div class="left padding_left_5px">
	  			<select id="ddlVoiceType" onchange="RecordType_OnChange()">	
	  				<option value="1">Record New</option>
					<option value="2">Text to Speak</option>
		   		</select>
  			</div>
		</div>

		<div id="RECORDVOICE">
			<object type="application/x-shockwave-flash" data="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/flash/standalonerecorder.swf" width="480" height="400" id="dewplayer" name="dewplayer">
		        <param name="wmode" value="transparent" />
		        <param name="movie" value="standalonerecorder.swf" />
				<param name="allowScriptAccess" value="sameDomain" />
		        <param name="flashvars" value="<cfoutput>#eleKey#=#eleValue#</cfoutput>&urlSaveFile=<cfoutput>#application.flashApiBaseUrl#</cfoutput>&useBtInit=true&postService=<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/flash/record&callbackFunction=flash_callback&buttonColor=0x0099CC&backgroundColor=0xFFFFFF&backgroundShadow=0xCCCCCC&textLabelColor=0x0099CC&textInputColor=0x000000" />
			</object>
			
			<div id="jquery_jplayer_1" class="jp-jplayer"></div>
	
			<div class="jplayer_body" id="jPlayer">
				<div id="jp_container_1" class="jp-audio">
					<div class="jp-type-single">
						<div class="jp-gui jp-interface">
							<ul class="jp-controls">
								<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
								<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
								<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
								<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
								<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
								<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
							</ul>
							<div class="jp-progress">
								<div class="jp-seek-bar">
									<div class="jp-play-bar"></div>
								</div>
							</div>
							<div class="jp-volume-bar">
								<div class ="jp-volume-bar-value "></div>
							</div>
							<div class="jp-time-holder">
								<div class="jp-current-time"></div>
								<div class="jp-duration"></div>
					
								<ul class="jp-toggles">
									<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
									<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
								</ul>
							</div>
						</div>
						<div class="jp-no-solution">
							<span>Update Required</span>
							To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="TEXTTOSPEECH" class="text_to_speech">
			<cfif isErrorOrTryAgain>
				<textarea
					id="txtDescription"
					name="errMsgText"
					class=""
					cols="60"
					rows="6"
					style="width: 95%"
				><cfoutput>#VOICEPROMPT#</cfoutput></textarea>
			</cfif>
		</div>
		
		<input type="hidden" id="hidScriptId">
		<div class='button_area' style="clear: both; padding-top: 10px;">
			<button 
				id="btnSave" 
				type="button" 
				class="ui-corner-all survey_builder_button"
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="ui-corner-all survey_builder_button"
				onclick="CloseVoiceRecordDialog()"
			>Cancel</button>
		</div>
	</form>


