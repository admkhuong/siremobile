<cfsetting requesttimeout="900" />
<!---<cfinclude template="../config.cfm">--->
<cfinclude template="../lib/jsonencode.cfm">
<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "Save file success";
</cfscript>
<cfif isDefined("scrId")>
	<cfset ids = ListToArray(scrId, "_") />
	<cfset userId = ids[1] /> 
	<cfset libId = ids[2] /> 
	<cfset eleId = ids[3] /> 
	<cfset scrId = ids[4] />

	<cfquery name="getScript" datasource="#application.datasource#">
		SELECT 
			UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
		FROM 
			#application.database#.scriptdata
		WHERE 
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
			AND DSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libId#">
			AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
			AND DataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scrId#">
			AND Active_int = '1'
		LIMIT 1
	</cfquery>
	<cfif getScript.RecordCount>
		<cfif IsArray(getHTTPRequestData().content) and ArrayLen(getHTTPRequestData().content)>
			
			<cffile action="write" file = "#application.baseDir#/tmp/Save_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.wav" output = "#getHTTPRequestData().content#">
			<cfset fileName = "RXDS_#userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3" />
			<cfset dstDirPath = "U#userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#" />
			<cfset srcFile = "#application.baseDir#/tmp/Save_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.wav" />
			<cfset dstFile = "#application.scriptPath#/#dstDirPath#/#fileName#" />
			<cfset retVal.message = "#dstFile#" />
			<cfif Not DirectoryExists("#application.scriptPath#/#dstDirPath#")>
				<cfdirectory action = "create" directory = "#application.scriptPath#/#dstDirPath#" recurse="yes" mode="0775" />
			</cfif>
			<cfif fileExists("#dstFile#")>
				<cffile action="delete"	file="#dstFile#" />
			</cfif>
			<cfexecute
				name = "#application.soxPath#"
		    arguments = '"#srcFile#" #application.soxDefaultArguments# "#dstFile#" #application.soxDefaultEffects#' 
		    outputFile = "#application.baseDir#soxlog.txt"
		    timeout = "#application.soxMaxExeTime#">
			</cfexecute>
			<cffile action="delete"	file="#srcFile#" />
		<cfelse>
			<cfscript>
				retVal.success = false;
				retVal.message = "No Record Data Posted";
			</cfscript>	
		</cfif>
	<cfelse>
 		<cfscript>
			retVal.success = false;
			retVal.message = "ScriptId is incorect";
		</cfscript>
 	</cfif>
<cfelse>
	<cfscript>
		retVal.success = false;
		retVal.message = "ScriptId is incorect";
	</cfscript>
</cfif>

<cfoutput>#jsonencode(retVal)#</cfoutput>