<!---<!---<cfinclude template="../config.cfm" />--->--->
<cfinclude template="../lib/jsonencode.cfm" />
<cfinclude template="../lib/rxdu_script.cfc" />
<cfscript>retVal = StructNew();
	retVal.success = true;
	retVal.message = "";</cfscript> 
<cfswitch expression="#lCase(a)#">
	<cfcase value="abs">
		<cfscript>
			if (NOT isDefined("id")) {
				retVal.success = false;
				retVal.message = "ScriptId is incorrect.";
			} else {
				ids = ListToArray(id, "_");
				userId = ids[1];
				libId = ids[2];
				eleId = ids[3];
				scrId = ids[4];
				script = IsExistedScript(userId, libId, eleId, scrId);
				if (IsBoolean(script)) {
					// Script not existed
					retVal.success = false;
					retVal.message = "ScriptId is incorrect.";
				} else {
					// Script existed
					// Validate authorized current session user can listen to input users request
					if (!(SESSION.isAdmin OR SESSION.userId == userId)) {
						retVal.success = false;
						retVal.message = "Access denied!";
					} else {
						filePath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#\RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3";
						if (fileExists(filePath)) {
							retVal.success = false;
							retVal.message = "Script audio file is existed.";
						} else {
							retVal.node = StructNew();
							retVal.node.id = id;
							retVal.node.label = script.Desc_vch;
							retVal.node.url = "#application.baseUrl#get-script.cfm?id=#id#";
						}	
					}
				}
			}
		</cfscript>
		<cfif isDefined("retVal.node")>
        
        
			<!--- Create directory if none exists --->
			<cfset CurrDir = "#application.scriptPath#/U#userId#">
        
            <cfif !DirectoryExists("#CurrDir#")>                                    
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrDir#">            
            </cfif>
    
		    <!--- Create directory if none exists --->
			<cfset CurrDir = "#application.scriptPath#/U#userId#/L#libId#">
        
            <cfif !DirectoryExists("#CurrDir#")>                                    
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrDir#">            
            </cfif>
    
    
    		<!--- Create directory if none exists --->
			<cfset CurrDir = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#">
        
            <cfif !DirectoryExists("#CurrDir#")>                                    
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrDir#">            
            </cfif>
        
			<!--- Copy file --->
			<cfset fileName = "RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3" />
			<!---<cfset dstDirPath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#" />--->
            <cfset dstDirPath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#" />
			<!---<cfset srcFile = "#application.baseDir#/data/blank.mp3" />--->
            <cfset srcFile = "#application.baseDir#/data/blank.mp3" />
			<!---<cfset dstFile = "#dstDirPath#/#fileName#" />--->
            <cfset dstFile = "#dstDirPath#/#fileName#" />
			
			<cfset retVal.message = "Yo A! --- #application.baseDir#/data\blank.mp3  --- #application.baseDir#  ---  #dstFile#">
            
			<cfif Not DirectoryExists("#dstDirPath#")>
				<cfdirectory action = "create" directory = "#dstDirPath#" recurse="yes" mode="0775" />
                
                <cfset retVal.message = "Yo B! #application.baseDir#/data\blank.mp3  --- #application.baseDir#  ---  #dstFile#">
                
			</cfif>
			<cfif FileExists("#srcFile#")>
				<cffile action = "copy" source="#srcFile#" destination="#dstFile#" />
			
				<cfset retVal.message = "Yo C! #application.baseDir#/data\blank.mp3  --- #application.baseDir#  ---  #dstFile#">
            
            </cfif>
            
           
            
		</cfif>
		<cfoutput>#jsonencode(retVal)#</cfoutput> 
	</cfcase>
	<cfcase value="rbn">
		<cfscript>
			if (NOT isDefined("id")) {
				retVal.success = false;
				retVal.message = "ScriptId is incorrect.";
			} else {
				ids = ListToArray(id, "_");
				userId = ids[1];
				libId = ids[2];
				eleId = ids[3];
				scrId = ids[4];
				script = IsExistedScript(userId, libId, eleId, scrId);
				if (IsBoolean(script)) {
					// Script not existed
					retVal.success = false;
					retVal.message = "ScriptId is incorrect.";
				} else {
					// Script existed
					// Validate authorized current session user can listen to input users request
					if (!(SESSION.isAdmin OR SESSION.userId == userId)) {
						retVal.success = false;
						retVal.message = "Access denied!";
					} else {
						filePath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#\RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3";
						if (!fileExists(filePath)) {
							retVal.success = false;
							retVal.message = "Script audio file is not existed.";
						} else {
							if (NOT isDefined("start") OR NOT isDefined("length")) {
								retVal.success = false;
								retVal.message = "Not define noise profile.";
							} else {
								retVal.node = StructNew();
								retVal.node.id = id;
								retVal.node.label = script.Desc_vch;
								retVal.node.url = "#application.baseUrl#get-script.cfm?id=#id#";
							}
						}	
					}
				}
			}
		</cfscript>
		<cfif isDefined("retVal.node")>
			<!--- Get noise profile --->
			<cfset fileName = "RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3" />
			<cfset dstDirPath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#" />
			<cfset srcFile = "#dstDirPath#/#fileName#" />
			<cfset dstFile = "#dstDirPath#/EDIT_#userId#_#libId#_#eleId#_#scrId#.mp3" />
			<cfset noiseFile = "#dstDirPath#/NOISE_#userId#_#libId#_#eleId#_#scrId#" />
			
			<!--- Copy source file --->
			<cffile action = "copy" source="#srcFile#" destination="#dstFile#" />

			<!--- Get noise profile --->
			<cfexecute
				name = "#application.soxPath#"
		    arguments = '"#dstFile#" -n trim #start# #length# noiseprof #noiseFile#"' 
		    outputFile = "#application.baseDir#soxlog.txt"
		    timeout = "#application.soxMaxExeTime#">
			</cfexecute>
			
			<!--- Remove background noise --->
			<cfexecute
				name = "#application.soxPath#"
		    arguments = '"#dstFile#" #application.soxDefaultArguments# "#srcFile#" noisered "#noiseFile#" 0.3' 
		    outputFile = "#application.baseDir#soxlog.txt"
		    timeout = "#application.soxMaxExeTime#">
			</cfexecute>
			
			<!--- Delete temp file & noise profile file --->
			<cffile action="delete"	file="#dstFile#" />
			<cffile action="delete"	file="#noiseFile#" />
			<!--- <cfcontent type="audio/mpeg" file="#srcFile#" /> --->
		<!--- <cfelse>
			<cfoutput>#jsonencode(retVal)#</cfoutput> --->
		</cfif>
		<cfoutput>#jsonencode(retVal)#</cfoutput>
	</cfcase>
</cfswitch>
