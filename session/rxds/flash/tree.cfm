<!---<cfinclude template="../config.cfm">--->
<cfinclude template="../lib/jsonencode.cfm">
<cfinclude template="../lib/rxdu_user.cfc">
<cfinclude template="../lib/rxdu_library.cfc">
<cfinclude template="../lib/rxdu_element.cfc">
<cfinclude template="../lib/rxdu_script.cfc">
<cfif (NOT isDefined("SESSION.USERID"))>
	<cfset StructClear(SESSION) />
 	<cflocation url="../login" addtoken="no">
</cfif>
<!---<cfset SESSION.loggedIn = "1">
<cfset SESSION.userId = "1">
<cfset SESSION.userName = "votong">
<cfset SESSION.isAdmin = true />--->

<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "";
</cfscript>

<cfswitch expression="#lCase(a)#">
	<!--- rename --->
	<cfcase value="r">
		<cfscript>
			if (NOT isDefined("id")) {
				retVal.success = false;
				retVal.message = "";
			} else {
				_ids = ListToArray(id, "_");
				if (arraylen(_ids) GTE 2) {
					if (arraylen(_ids) GTE 3) {
						if (arraylen(_ids) GTE 4) {
							// Rename Script
							retVal = RenameScript(id, name);
						} else {
							// Rename Element
							retVal = RenameElement(id, name);
						}
					} else {
						// Rename Library
						retVal = RenameLibrary(id, name);
					}
				}
			}
		</cfscript>
	</cfcase>
	
	<!--- delete --->
	<cfcase value="d">
		<cfif NOT isDefined("id")>
			<cfscript>
				retVal.success = false;
				retVal.message = "Id is invalid!";
			</cfscript>
		<cfelse>
			<cfset _ids = ListToArray(id, "_") />
			<cfif arraylen(_ids) GTE 2> 
				<cfset _libId = _ids[2] />
				<cfif arraylen(_ids) GTE 3> 
					<cfset _eleId = _ids[3] />
					<cfif arraylen(_ids) GTE 4>
						<!--- Delete Script --->
						<cfset retVal = DeleteScript(id) />
					<cfelse>
						<!--- Delete Element --->
						<cfset retVal = DeleteElement(id) />
					</cfif>
				<cfelse>
					<!--- Delete Library --->
					<cfset retVal = DeleteLibrary(id) />
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	<!--- paste --->
	<cfcase value="p">
		<cfif NOT isDefined("srcId")>
		<cfelse>
			<cfif NOT isDefined("dstId")>
			<cfelse>
				<cfset _ids = ListToArray(srcId, "_") />
				<cfif arraylen(_ids) GTE 2> 
					<cfif arraylen(_ids) GTE 3> 
						<cfif arraylen(_ids) GTE 4>
							<!--- Copy/Paste Script --->
							<cfset retVal = CopyScript(srcId, dstId) />
						<cfelse>
							<!--- Copy/Paste Element --->
							<cfset retVal = CopyElement(srcId, dstId) />
						</cfif>
					<cfelse>
						<!--- Copy/Paste Library --->
						<cfset retVal = CopyLibrary(srcId, dstId) />
					</cfif>
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	<!--- move --->
	<cfcase value="m">
		<cfif NOT isDefined("srcId")>
		<cfelse>
			<cfif NOT isDefined("dstId")>
			<cfelse>
				<cfset _ids = ListToArray(srcId, "_") />
				<cfif arraylen(_ids) GTE 2> 
					<cfif arraylen(_ids) GTE 3> 
						<cfif arraylen(_ids) GTE 4>
							<!--- Cut/Paste Script --->
							<cfset retVal = MoveScript(srcId, dstId) />
						<cfelse>
							<!--- Cut/Paste Element --->
							<cfset retVal = MoveElement(srcId, dstId) />
						</cfif>
					<cfelse>
						<!--- Cut/Paste Library --->
						<cfset retVal = MoveLibrary(srcId, dstId) />
					</cfif>
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	<!--- export --->
	<cfcase value="e">
		<cfscript>
			options = StructNew();
			options.isNoConversion = isNoConversion;
			options.is16Bit = is16Bit;
			options.prefix = prefix;
			options.userDSSD = userDSSD;
			options.userDesfault = userDesfault;
			options.isNormalize = isNormalize;
			options.valueNormalize = valueNormalize;
		</cfscript>
		<cfif NOT isDefined("id")>
		<cfelse>
			<cfset _ids = ListToArray(id, "_") />
			<cfif arraylen(_ids) GTE 2> 
				<cfif arraylen(_ids) GTE 3> 
					<cfif arraylen(_ids) GTE 4>
						<!--- Export Script --->
						<cfset retVal = ExportScript(id) />
					<cfelse>
						<!--- Export Element --->
						<cfset retVal = ExportElement(id) />
					</cfif>
				<cfelse>
					<!--- Export Library --->
					<cfset retVal = ExportLibrary(id, options) />
				</cfif>
			</cfif>
		</cfif>
	</cfcase>
	
	<!--- Clear --->
	<cfcase value="clear">
		<cfscript>
			if (NOT isDefined("id")) {
				retVal.success = false;
				retVal.message = "";
			} else {
				//_ids = ListToArray(id, "_");
				retVal = ClearScript(id);
			}
		</cfscript>
	</cfcase>
</cfswitch>

<cfoutput>#jsonencode(retVal)#</cfoutput>
