<!---<cfinclude template="../config.cfm">--->
<cfinclude template="../lib/jsonencode.cfm">
<cfinclude template="../lib/rxdu.cfc">
<cfinclude template="../lib/rxdu_user.cfc">
<cfinclude template="../lib/rxdu_library.cfc">
<cfinclude template="../lib/rxdu_element.cfc">
<cfinclude template="../lib/rxdu_script.cfc">

<cfparam name="sortname" default="UserName_vch">
<cfparam name="sorttype" default="DESC">
<!---<cfif (NOT isDefined("SESSION.loggedIn")) OR (SESSION.loggedIn NEQ "1")>
 	<cflocation url="../login" addtoken="no">
 	<cfset StructClear(SESSION)>
</cfif>--->
<!---<cfset SESSION.loggedIn = "1">
<cfset SESSION.userId = "11">
<cfset SESSION.userName = "votong">
<cfset SESSION.isAdmin = true />
--->
<?xml version="1.0" encoding="utf-8" ?>
<cfscript>
			if (sortname != 'UserId') {
				sortname = 'UserName_vch';
			}
			if (sorttype == 'ASC') {
				sorttype = "ASC";
			} else {
				sorttype = "DESC";
			}
	if (NOT isDefined("id")) {
		
			writeOutput('<node id="' & SESSION.userId & '" label="' & SESSION.userName & '">');
			
			librarys = GetLibraryList(SESSION.userId,"Desc_vch",sorttype);
			for (i = 1; i LTE librarys.RecordCount; i = i + 1) {
				writeOutput('<node id="' & SESSION.userId & '_' & librarys["DSId_int"][i] & '" label="' & librarys["Desc_vch"][i] & '" />');
			}
	
		writeOutput('</node>');
	} else {
		_ids = ListToArray(id, "_");
		if (NOT isDefined("recursive")) {
			if (arraylen(_ids) GT 1) {
				if (arraylen(_ids) GT 2) {
					element = IsExistedElement(_ids[1], _ids[2], _ids[3]);
					writeOutput('<node id="' & _ids[1] & '_' & _ids[2] & '_' & _ids[3] & '" label="' & element.Desc_vch & '">');
					scripts = GetScriptList(_ids[1], _ids[2], _ids[3],"Desc_vch",sorttype);
					for (i = 1; i LTE scripts.RecordCount; i = i + 1) {
						writeOutput(
							'<node id="' & _ids[1] & '_' & _ids[2] & '_' & _ids[3] & '_' & scripts["DataId_int"][i] &
							'" label="' & scripts["Desc_vch"][i] &
							'" url="' & application.baseUrl & "get-script.cfm?id=" & scripts["UserId_int"][i] & "_" & scripts["DSId_int"][i] & "_" & scripts["DSEId_int"][i] & "_" & scripts["DataId_int"][i] & '" />'
						);
						
					}
					writeOutput('</node>');
				} else {
					library = IsExistedLibrary(_ids[1], _ids[2]);
					writeOutput('<node id="' & _ids[1] & '_' & _ids[2] & '" label="' & library.Desc_vch & '">');
					elements = GetElementList(_ids[1], _ids[2],"Desc_vch",sorttype);
					for (i = 1; i LTE elements.RecordCount; i = i + 1) {
						writeOutput('<node id="' & _ids[1] & '_' & _ids[2] & '_' & elements["DSEId_int"][i] & '" label="' & elements["Desc_vch"][i] & '" />');
					}
					writeOutput('</node>');
				}
			} else {
				user = IsExistedUser(_ids[1]);
				if (IsQuery(user)) {
					writeOutput('<node id="' & _ids[1] & '" label="' & user.UserName_vch & '">');
					librarys = GetLibraryList(_ids[1],"Desc_vch",sorttype);
					for (i = 1; i LTE librarys.RecordCount; i = i + 1) {
						writeOutput('<node id="' & _ids[1] & '_' & librarys["DSId_int"][i] & '" label="' & librarys["Desc_vch"][i] & '" />');
					}
					writeOutput('</node>');
				}	
			}
		}
		else {
			xml = ScriptGetTreeXML(id);
			//alert(xml);
			writeOutput(xml);
		}	
	}
</cfscript>