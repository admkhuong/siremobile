<!---<cfinclude template="../config.cfm">--->
<!---<cfset SESSION.userId = 1 />
<cfif (NOT isDefined("SESSION.loggedIn")) OR (SESSION.loggedIn NEQ "1")>
 	<cflocation url="login" addtoken="no">
 	<cfset StructClear(SESSION)>
</cfif>--->

<cfinclude template="../lib/jsonencode.cfm">
<cfscript>
	retVal = StructNew();
	retVal.success = true;
	retVal.message = "";
</cfscript>
<cfif isDefined("eleId")>
	<cfset ids = ListToArray(eleId, "_") />
	<cfset userId = ids[1] /> 
	<cfset libId = ids[2] /> 
	<cfset eleId = ids[3] /> 
	<cfquery name="getEle" datasource="#application.datasource#">
		SELECT 
			DSEId_int, DSId_int, UserId_int, Desc_vch
		FROM 
			#application.database#.dselement
		WHERE UserId_int='#userId#'
			AND DSId_int='#libId#'
			AND DSEId_int='#eleId#'
			AND Active_int='1'
		LIMIT 1
	</cfquery>
	<cfif getEle.RecordCount>
		<cfif isDefined("Filedata")>
			<cfquery name="getNextScriptId" datasource="#application.datasource#">
				SELECT
					CASE 
						WHEN MAX(DataId_int) IS NULL THEN 1
					ELSE
						MAX(DataId_int) + 1 
					END AS NextScriptId  
				FROM
					#application.database#.scriptdata
				WHERE
					UserId_int = #userId#
					AND DSID_int = #getEle.DSId_int#
          			AND DSEId_int = #getEle.DSEId_int#
			</cfquery>
			<cfset scrId = getNextScriptId.NextScriptId />
			
			<cffile	action="upload" fileField="Filedata" destination="#application.baseDir#/tmp/" nameConflict="makeUnique" result="upload" />
			<cfset fileName = "RXDS_#userId#_#getEle.DSId_int#_#getEle.DSEId_int#_#scrId#.mp3" />
			<cfset dstDirPath = "U#userId#/L#getEle.DSId_int#/E#getEle.DSEId_int#" />
			<cfset srcFile = "#upload.SERVERDIRECTORY#/#upload.SERVERFILE#" />
			<cfset dstFile = "#application.scriptPath#/#dstDirPath#/#fileName#" />
			
			<cfif Not DirectoryExists("#application.scriptPath#/#dstDirPath#")>
				<cfdirectory action = "create" directory = "#application.scriptPath#/#dstDirPath#" recurse="yes" mode="0775" />
			</cfif>
			<cfexecute
				name = "#application.soxPath#"
		    arguments = '"#srcFile#" #application.soxDefaultArguments# "#dstFile#" #application.soxDefaultEffects#' 
		    outputFile = "#application.baseDir#soxlog.txt"
		    timeout = "#application.soxMaxExeTime#">
			</cfexecute>
			<cffile action="delete"	file="#srcFile#" />
			
			<cfif NOT isDefined("name")>
				<cfset name = "#upload.CLIENTFILE#" />
			</cfif>
			<cfquery name="scrInsert" datasource="#application.datasource#">  
				INSERT INTO #application.database#.scriptdata (DataId_int, DSEId_int, DSId_int, UserId_int, Desc_vch, Created_dt)
				VALUES('#scrId#', '#getEle.DSEId_int#', '#getEle.DSId_int#', '#userId#', '#name#', NOW())
			</cfquery>
			<cfscript>
				retVal.node = StructNew();
				retVal.node.userId = userId;
				retVal.node.libId = getEle.DSId_int;
				retVal.node.eleId = getEle.DSEId_int;
				retVal.node.id = scrId;
				retVal.node.label = name;
				retVal.node.url = "#application.baseUrl#get-script.cfm?id=#userId#_#getEle.DSId_int#_#getEle.DSEId_int#_#scrId#";
			</cfscript>
		<cfelse>
			<cfscript>
				retVal.success = false;
				retVal.message = "No File Uploaded";
			</cfscript>	
		</cfif>
	<cfelse>
 		<cfscript>
			retVal.success = false;
			retVal.message = "EleId is incorect";
		</cfscript>
 	</cfif>
<cfelse>
	<cfscript>
		retVal.success = false;
		retVal.message = "EleId is incorect";
	</cfscript>
</cfif>

<cfoutput>#jsonencode(retVal)#</cfoutput>