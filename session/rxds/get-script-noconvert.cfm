<cfparam name="SESSION.isAdmin" default="0">

<!---<!---<cfinclude template="config.cfm" />--->--->
<!--- Check token for mobile --->
<cfif isDefined("token")>
	<cfquery name="loginCheck" datasource="#application.datasource#">
		SELECT UserId_int, UserLevel_int FROM #application.database#.useraccount WHERE Token_vch = '#token#' LIMIT 1 
	</cfquery>
	<cfif loginCheck.RecordCount>
		<cfscript>
			SESSION.loggedIn = 1;
			SESSION.userId = loginCheck.UserId_int;
			if (loginCheck.UserLevel_int) {
				SESSION.isAdmin = true;
			} else {
				SESSION.isAdmin = false;
			}
		</cfscript> 
	</cfif>
</cfif>
<!--- Check session for web --->
<cfif (NOT isDefined("SESSION.USERID")) OR (SESSION.USERID LT 1)>
	<cfoutput>
		Session expired!
	</cfoutput>
	<cfabort />
</cfif>

<!--- Why would this have even worked in ADF ?!? need to include function not component --->
<!--- <cfinclude template="lib/rxdu_script.cfc"> --->

<cffunction name="IsExistedScript" access="remote" output="false" hint="Check script is existed">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfquery name="getScript" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
			FROM 
				rxds.scriptdata
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#elementId#">
				AND DataId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scriptId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif getScript.RecordCount>
			<cfreturn getScript />
		</cfif>
		<cfreturn false />
	</cffunction>

<cffunction name="fileSize" returntype="Numeric" access="public" output="false">
	<cfargument name="pathToFile" type="String" required="true" />
	<cfscript>
		var fileInstance = createObject("java","java.io.File").init(toString(arguments.pathToFile));
		var fileList = "";
		var ii = 0;
		var totalSize = 0;
		
		//if this is a simple file, just return it's length
		if(fileInstance.isFile()){
			return fileInstance.length();
		}
		else if(fileInstance.isDirectory()) {
			fileList = fileInstance.listFiles();
			for(ii = 1; ii LTE arrayLen(fileList); ii = ii + 1){
				totalSize = totalSize + fileSize(fileList[ii]);
			}
			return totalSize;
		}
		else {
			return 0;
		}
	</cfscript> 
</cffunction>


<cffunction name="getLastModifiedStamp">
	<cfargument name="filename" required="true" type="String" />
	<cfscript>
		var _File = createObject("java","java.io.File");
		// Calculate adjustments fot timezone and daylightsavindtime
		var _Offset = ((GetTimeZoneInfo().utcHourOffset)+1)*-3600;
		_File.init(JavaCast("string", filename));
		// Date is returned as number of seconds since 1-1-1970
		return DateAdd('s', (Round(_File.lastModified()/1000))+_Offset, CreateDateTime(1970, 1, 1, 0, 0, 0));
	</cfscript> 
</cffunction>

<cfscript>
	ok = false;
	if (NOT isDefined("id")) {
		writeOutput('Script does not exist!');
	} else {
		ids = ListToArray(id, "_");
		userId = ids[1];
		libId = ids[2];
		eleId = ids[3];
		scrId = ids[4];
		script = IsExistedScript(userId, libId, eleId, scrId);
		if (IsBoolean(script)) {
			// Script not existed
			writeOutput('Script does not exist!');
		} else {
			// Script existed
			// Validate authorized current session user can listen to input users request
			if (!(SESSION.isAdmin OR SESSION.userId == userId)) {
				writeOutput('Access denied!');
			} else {
				filePath = "#application.scriptPath#/U#userId#/L#libId#/E#eleId#/RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3";
				if (!fileExists(filePath)) {
					writeOutput('File not found! get script:' & filePath);
				} else {
					try{
						ok = true;
						
						fileName = Replace(Replace(script.Desc_vch, " ", "-"), ".wav", "") & ".mp3";
						contentType = getPageContext().getServletContext().getMimeType(filePath);
						//writeOutput(filePath);
						//contentType = "audio/mpeg";
					}catch(any excpt){
						writeOutput('Invalid mp3 file! get script:' & filePath);
						exit;
					}
				}	
			}
		}
	}
</cfscript>
<!--- <cfif isDefined("err404") AND err404>
	<cfheader statuscode="404" statustext="File Not Found">
	<html>
	<head><title>File not found</title></head>
	<body><h1>File Not found</h1></body>
	</html>
	<cfabort>
</cfif> --->
<cfif ok>
	<!--- <cfheader name="Last-Modified" value="#getLastModifiedStamp(filePath)#" />
	<cfheader name="Accept-Ranges" value="bytes" /> --->
	<cfheader name="Content-Disposition" value="attachment;filename=#fileName#" />
	<cfcontent type="#contentType#" file="#filePath#" />
	<!--- <cfheader name="Content-Length" value="#fileSize(filePath)#" /> --->
</cfif>