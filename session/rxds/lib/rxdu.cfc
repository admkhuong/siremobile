<!--- No display --->
<cfcomponent output="false">
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
    <cfinclude template="../../../public/paths.cfm" >
	
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	<cfparam name="Session.UserID" default="0"/> 
	
	<!--- ************************************************************************************************************************* --->
	<!--- Read an XML string from DB and parse CCD Values --->
	<!--- ************************************************************************************************************************* --->       
	
	<cffunction name="AddLibrary" access="remote" output="false" hint="Add a new library to the current user">
		<cfargument name="inpDesc" type="string" default=""/>
		<cfset var dataout = '0' />    
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 
		--->
		
		<cfoutput>
			<!--- move to just Lib number
			<cfif inpDesc EQ "">
			<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
			</cfif>
			--->
			
			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout = QueryNew("RXRESULTCODE, NextLibId, message")>  
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			<cfset QuerySetCell(dataout, "NextLibId", 0) />     
			<cfset QuerySetCell(dataout, "message", "") />   
			
			<cftry>
				<!--- Cleanup SQL injection --->
				<!--- Replace ' with '' --->
				<!--- Verify all numbers are actual numbers --->                    
				
				<!--- Replace ' with '' --->
				<cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
				
				<cfset CreationCount = 0> 
				<cfset ExistsCount = 0> 
				
				<!--- Validate session still in play - handle gracefully if not --->
				<cfif Session.UserID GT 0>
				
				<!--- Get next Lib ID for current user --->           
				<cfquery name="GetNextLibId" datasource="rxds">  

					SELECT
						CASE 
							WHEN MAX(DSID_int) IS NULL THEN 1
						ELSE
							MAX(DSID_int) + 1 
						END AS NextLibId  
					FROM
						rxds.DynamicScript
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserID#">
				</cfquery>  
				
				<cfif inpDesc EQ "">
				<cfset inpDesc = "Lib #GetNextLibId.NextLibId#">
				</cfif>
				
				<!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
				<!--- Add record --->
				<cfquery name="GetLibraries" datasource="rxds">

				INSERT INTO 
					rxds.DynamicScript 	(DSID_int, UserID_int, Active_int, Desc_vch )
				VALUES	(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetNextLibId.NextLibId#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserID#">, 
					1, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">
				)                                        
				</cfquery>       
				
				
				<!--- Create Library Path --->
				<!--- Does User Directory Exist --->
				<cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#">
				
				<cfif !DirectoryExists("#CurrRemoteUserPath#")>
				
				<!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
				<cfdirectory action="create" directory="#CurrRemoteUserPath#">
				
				<cfset CreationCount = CreationCount + 1> 
				
				<!--- Still doesn't exist - check your access settings --->
				<cfif !DirectoryExists("#CurrRemoteUserPath#")>
				<cfthrow message="Unable to create remote user directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
				</cfif>
				
				<cfelse>
				
				<cfset ExistsCount = ExistsCount + 1> 
				
				</cfif>
				
				<!--- Does Lib Directory Exist --->
				<cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#/L#GetNextLibId.NextLibId#">
				
				<cfif !DirectoryExists("#CurrRemoteUserPath#")>
				
				<!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
				<cfdirectory action="create" directory="#CurrRemoteUserPath#">
				
				<cfset CreationCount = CreationCount + 1> 
				
				<!--- Still doesn't exist - check your access settings --->
				<cfif !DirectoryExists("#CurrRemoteUserPath#")>
				<cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
				</cfif>
				
				<cfelse>
				
				<cfset ExistsCount = ExistsCount + 1> 
				
				</cfif>
				
				
				<!--- All good --->
				<cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, message")>  
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "NextLibId", "#GetNextLibId.NextLibId#") />
				<cfset QuerySetCell(dataout, "message", "New Library created OK") />    
				
				
				<cfelse>
				
				<cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, message")>  
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
				<cfset QuerySetCell(dataout, "NextLibId", 0) />     
				<cfset QuerySetCell(dataout, "message", "User Session is expired") />     
				
				</cfif>          
				
				<cfcatch type="any">
					<cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, type, message, ErrMessage")>  
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					<cfset QuerySetCell(dataout, "NextLibId", 0) />     
					<cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
					<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
					<cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
				</cfcatch>
			</cftry>     
		
		</cfoutput>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetUserList" access="remote" output="false" hint="Get all user of system">
		<cfargument name="inpSortName" type="string" default="UserId_int"/>
		<cfargument name="inpSortType" type="string" default="desc"/>
		<cfset var local = StructNew() />
		<cfoutput>
		<cfquery name="local.getUsers" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, UserName_vch
			FROM 
				simpleobjects.useraccount
			WHERE 
				Active_int='1'
			ORDER BY
				#inpSortName# #inpSortType#
		</cfquery>
		</cfoutput>
		<cfreturn local.getUsers />
	</cffunction>
</cfcomponent>
