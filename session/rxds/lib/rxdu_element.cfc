<!--- No display --->
<cfcomponent output="true">
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="IsExistedElement" access="remote" output="false" hint="Check element is existed">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libId" type="string" required="Yes" />
		<cfargument name="eleId" type="string" required="Yes" />
		<cfquery name="getEle" datasource="#Session.DBSourceEBM#">
			SELECT 
				DSEId_int, DSId_int, UserId_int, Desc_vch
			FROM 
				rxds.dselement
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif getEle.RecordCount>
			<cfreturn getEle />
		</cfif>
		<cfreturn false />
	</cffunction>
	
	<cffunction name="GetNextScriptId" access="remote" output="false" hint="Get next script id of element">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libId" type="string" required="Yes" />
		<cfargument name="eleId" type="string" required="Yes" />
		<cfquery name="qGetNextScriptId" datasource="#Session.DBSourceEBM#">
			SELECT
				CASE 
					WHEN MAX(DataId_int) IS NULL THEN 1
				ELSE
					MAX(DataId_int) + 1 
				END AS NextScriptId  
			FROM
				rxds.scriptdata
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libId#">
        		AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
		</cfquery>
		<cfreturn qGetNextScriptId.NextScriptId />
	</cffunction>
	
	<cffunction name="GetScriptList" access="remote" output="false" hint="Get all script of element">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="sortname" type="string" default="Desc_vch" />
		<cfargument name="sorttype" type="string" default="ASC" />
		<cfoutput>
			<cfquery name="getScript" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
				FROM 
					rxds.scriptdata
				WHERE 
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
					AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
					AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#elementId#">
				ORDER BY
					#sortname# #sorttype#
			</cfquery>
		</cfoutput>
		<cfreturn getScript />
	</cffunction>
	
	<cffunction name="RenameElement" access="remote" output="false" hint="Rename a element">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="name" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			local.ids = ListToArray(elementId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
		</cfscript>
		<cfquery name="updateElement" datasource="#Session.DBSourceEBM#">
			UPDATE 
				rxds.dselement
			SET 
				Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#name#">
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.eleId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="DeleteElement" access="remote" output="false" hint="Delete element">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			local.ids = ListToArray(elementId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
			local.scripts = GetScriptList(local.userId, local.libId, local.eleId);
		</cfscript>
		<cfloop query = "local.scripts">
			<cfscript>
				DeleteScript("#elementId#_#local.scripts.DataId_int#");
			</cfscript>
		</cfloop>
		<cfquery name="qDeleteElement" datasource="#Session.DBSourceEBM#">
			DELETE 
			FROM 
				rxds.dselement
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.eleId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		
		<cfreturn local.retVal />
	</cffunction>
	
	<cffunction name="CopyScript" access="remote" output="false" hint="Copy/Paste a script to a element">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfargument name="elementId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			if (NOT isDefined("scriptId")) {
				retVal.success = false;
				retVal.message = "ScriptId is incorect";
				return retVal;
			}
			
			if (NOT isDefined("elementId")) {
				retVal.success = false;
				retVal.message = "ElementId is incorect";
				return retVal;
			}
			
			local.ids = ListToArray(scriptId, "_");
			local.srcUserId = local.ids[1];
			local.srcLibId = local.ids[2];
			local.srcEleId = local.ids[3];
			local.srcScrId = local.ids[4];
			local.script = IsExistedScript(local.srcUserId, local.srcLibId, local.srcEleId, local.srcScrId);
			if (IsBoolean(local.script)) {
				retVal.success = false;
				retVal.message = "ScriptId is incorect";
				return retVal;
			}
			
			local.ids = ListToArray(elementId, "_");
			local.dstUserId = local.ids[1];
			local.dstLibId = local.ids[2];
			local.dstEleId = local.ids[3];
			local.element = IsExistedElement(local.dstUserId, local.dstLibId, local.dstEleId);
			if (IsBoolean(local.element)) {
				retVal.success = false;
				retVal.message = "ElementId is incorect";
				return retVal;
			}
		</cfscript>
		<!--- Create new local.script for destination local.element --->				
		<cfset dstScrId = GetNextScriptId(local.dstUserId, local.dstLibId, local.dstEleId) />
		<cfquery name="scrInsert" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				rxds.scriptdata (UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch, Created_dt)
			VALUES(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstUserId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstLibId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstEleId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dstScrId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.script.Desc_vch#">, 
				NOW()
			)
		</cfquery>
		<cfscript>
			retVal.node = StructNew();
			retVal.node.id = "#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#";
			retVal.node.label = local.script.Desc_vch;
			//retVal.node.url = "#application.baseUrl#local.script/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#/RXDS_#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#.mp3";
			retVal.node.url = "#application.baseUrl#get-script.cfm?id=#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#";
		</cfscript>

		<!--- Copy file --->
		<cfset fileName = "RXDS_#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#.mp3" />
		<cfset dstDirPath = "#application.scriptPath#/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#" />
		<cfset srcFile = "#application.scriptPath#/U#local.srcUserId#/L#local.srcLibId#/E#local.srcEleId#/RXDS_#local.srcUserId#_#local.srcLibId#_#local.srcEleId#_#local.srcScrId#.mp3" />
		<cfset dstFile = "#dstDirPath#/#fileName#" />
		<cfif Not DirectoryExists("#dstDirPath#")>
			<cfdirectory action = "create" directory = "#dstDirPath#" recurse="yes" mode="0775" />
		</cfif>
		<cfif FileExists("#srcFile#")>
			<cffile action = "copy" source="#srcFile#" destination="#dstFile#" />
		</cfif>
		<cfscript>
			retVal.srcId = scriptId;
			retVal.dstId = elementId;
		</cfscript>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="MoveScript" access="remote" output="false" hint="Cut/Paste a script to a element">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfargument name="elementId" type="string" required="Yes" />
		<cfscript>
			var retVal = CopyScript(scriptId, elementId);
			if (retVal.success) {
				DeleteScript(scriptId);
			}
		</cfscript>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="ExportElement" access="remote" output="false" hint="Export a element">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="filePath" type="string" required="Yes" />
		<cfargument name="options" type="object" required="No" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			if (NOT isDefined("elementId")) {
				retVal.success = false;
				retVal.message = "ElementId is incorect";
				return retVal;
			}
			
			local.ids = ListToArray(elementId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
			local.element = IsExistedElement(local.userId, local.libId, local.eleId);
			if (IsBoolean(local.element)) {
				retVal.success = false;
				retVal.message = "ElementId is incorect";
				return retVal;
			}
			local.scrList = GetScriptList(local.userId, local.libId, local.eleId);
		</cfscript>
		
		<!--- Copy scripts from source local.element to destination local.element --->				
		<cfloop query = "local.scrList">
			<cfscript>
				ExportScript("#local.userId#_#local.libId#_#local.eleId#_#local.scrList.DataId_int#", "#filePath#", options);
			</cfscript>
		</cfloop>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="AddBlankScript" access="remote" output="false" hint="Add a blank script to a element">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="scriptName" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			if (NOT isDefined("elementId")) {
				retVal.success = false;
				retVal.message = "ElementId is incorect.";
				return retVal;
			}
			
			if (NOT isDefined("scriptName")) {
				retVal.success = false;
				retVal.message = "Script Name is required.";
				return retVal;
			}
			
			local.ids = ListToArray(elementId, "_");
			local.dstUserId = local.ids[1];
			local.dstLibId = local.ids[2];
			local.dstEleId = local.ids[3];
			local.element = IsExistedElement(local.dstUserId, local.dstLibId, local.dstEleId);
			if (IsBoolean(local.element)) {
				retVal.success = false;
				retVal.message = "ElementId is incorect";
				return retVal;
			}
			dstScrId = GetNextScriptId(local.dstUserId, local.dstLibId, local.dstEleId);
		</cfscript>
		<!--- Create new local.script for destination local.element --->				
		<cfset dstScrId = GetNextScriptId(local.dstUserId, local.dstLibId, local.dstEleId) />
		<cfquery name="scrInsert" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				rxds.scriptdata (UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch, Created_dt, Length_int)
			VALUES(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstUserId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstLibId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstEleId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dstScrId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scriptName#">, 
				NOW(),
				0
				)
		</cfquery>
		<cfscript>
			retVal.node = StructNew();
			retVal.node.id = "#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#";
			retVal.node.label = scriptName;
			retVal.node.url = "#application.baseUrl#get-script.cfm?id=#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#";
		</cfscript>

		<!--- Copy file --->
		<cfset fileName = "RXDS_#local.dstUserId#_#local.dstLibId#_#local.dstEleId#_#dstScrId#.mp3" />
		<cfset dstDirPath = "#application.scriptPath#/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#" />
		<cfset srcFile = "#application.baseDir#/data/blank.mp3" />
		<cfset dstFile = "#dstDirPath#/#fileName#" />
		<cfif Not DirectoryExists("#dstDirPath#")>
			<cfdirectory action = "create" directory = "#dstDirPath#" recurse="yes" mode="0775" />
		</cfif>
		<cfif FileExists("#srcFile#")>
			<cffile action = "copy" source="#srcFile#" destination="#dstFile#" />
		</cfif>
		<cfscript>
			//retVal.scrId = scriptId;
			retVal.dstId = elementId;
		</cfscript>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="ElementGetTreeXML" access="remote" output="false" hint="Get element's xml string for tree">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.ids = ListToArray(elementId, "_");
			
			local.element = IsExistedElement(local.ids[1], local.ids[2], local.ids[3]);
			local.retVal = '<node id="' & elementId & '" label="' & local.element.Desc_vch & '">';
			local.scripts = GetScriptList(local.ids[1], local.ids[2], local.ids[3]);
			for (i = 1; i LTE local.scripts.RecordCount; i = i + 1) {
				local.retVal = local.retVal &
					'<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.ids[3] & '_' & local.scripts["DataId_int"][i] &
					'" label="' & local.scripts["Desc_vch"][i] &
					'" url="' & application.baseUrl & "get-script.cfm?id=" & local.scripts["UserId_int"][i] & "_" & local.scripts["DSId_int"][i] & "_" & local.scripts["DSEId_int"][i] & "_" & local.scripts["DataId_int"][i] & '" />'
				;
			}
			local.retVal = local.retVal & '</node>';
			return retVal;
		</cfscript>
	</cffunction>
	
	<cffunction name="ClearScript" access="remote" output="true" hint="Overright script with blank script">
		<cfargument name="elementId" type="string" required="Yes" />
		
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			local.ids = ListToArray(elementId, "_");
			local.dstUserId = local.ids[1];
			local.dstLibId = local.ids[2];
			local.dstEleId = local.ids[3];
			local.dstScrId = local.ids[4];
		</cfscript>
		<cfset srcFile = "#application.baseDir#/data/blank.mp3" />
		
		<cfset fileName = "RXDS_#elementId#.mp3" />
		<cfset dstDirPath = "#application.scriptPath#/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#" />
		

		<cfset dstFile = "#dstDirPath#/#fileName#" />
		<cfif FileExists("#application.scriptPath#/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#/RXDS_#elementId#.mp3")>
			<cffile action="delete"	file="#application.scriptPath#/U#local.dstUserId#/L#local.dstLibId#/E#local.dstEleId#/RXDS_#elementId#.mp3" />
		</cfif>
		
		<cfif FileExists("#srcFile#")>
			<cffile action = "copy" source="#srcFile#" destination="#dstFile#" />
		</cfif>
		
		<cfreturn retVal />
	</cffunction>
</cfcomponent>
