<!--- No display --->
<cfcomponent output="false">
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
    <cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="IsExistedLibrary" access="remote" output="false" hint="Check library is existed">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfquery name="getLib" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, DSId_int, Desc_vch
			FROM 
				rxds.dynamicscript
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif getLib.RecordCount>
			<cfreturn getLib />
		</cfif>
		<cfreturn false />
	</cffunction>
	
	<cffunction name="GetNextElementId" access="remote" output="false" hint="Get next element id of local.library">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfquery name="qGetNextElementId" datasource="#Session.DBSourceEBM#">
			SELECT
				CASE 
					WHEN MAX(DSEId_int) IS NULL THEN 1
				ELSE
					MAX(DSEId_int) + 1 
				END AS NextElementId  
			FROM
				rxds.dselement
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
		</cfquery>
		<cfreturn qGetNextElementId.NextElementId />
	</cffunction>
	
	<cffunction name="GetElementList" access="remote" output="false" hint="Get all element of local.library">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="sortname" type="string" default="Desc_vch" />
		<cfargument name="sorttype" type="string" default="ASC" />
		<cfoutput>
			<cfquery name="getElement" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, DSId_int, DSEId_int, Desc_vch
				FROM 
					rxds.dselement
				WHERE 
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
					AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
				ORDER BY
					#sortname# #sorttype#
			</cfquery>
		</cfoutput>
		<cfreturn getElement />
	</cffunction>
	
	<cffunction name="RenameLibrary" access="remote" output="false" hint="Rename a library">
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="name" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			local.ids = ListToArray(libraryId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
		</cfscript>
		<cfquery name="updateLibrary" datasource="#Session.DBSourceEBM#">
			UPDATE 
				rxds.dynamicscript
			SET 
				Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#name#">
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="DeleteLibrary" access="remote" output="false" hint="Delete library">
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			local.ids = ListToArray(libraryId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.libraries = GetElementList(local.userId, local.libId);
		</cfscript>
		<cfloop query = "local.libraries">
			<cfscript>
				DeleteElement("#libraryId#_#local.libraries.DSEId_int#");
			</cfscript>
		</cfloop>
		<cfquery name="deleteLibrary" datasource="#Session.DBSourceEBM#">
			DELETE 
			FROM 
				rxds.dynamicscript
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="CopyElement" access="remote" output="false" hint="Copy/Paste a element to a library">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			if (NOT isDefined("elementId")) {
				local.retVal.success = false;
				local.retVal.message = "ElementId is incorect";
				return local.retVal;
			}
			
			if (NOT isDefined("libraryId")) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			
			local.ids = ListToArray(elementId, "_");
			local.srcUserId = local.ids[1];
			local.srcLibId = local.ids[2];
			local.srcEleId = local.ids[3];
			local.element = IsExistedElement(local.srcUserId, local.srcLibId, local.srcEleId);
			if (IsBoolean(local.element)) {
				local.retVal.success = false;
				local.retVal.message = "ElementId is incorect";
				return local.retVal;
			}
			
			local.ids = ListToArray(libraryId, "_");
			local.dstUserId = local.ids[1];
			local.dstLibId = local.ids[2];
			local.library = IsExistedLibrary(local.dstUserId, local.dstLibId);
			if (IsBoolean(local.library)) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			// Create new element for destination library
			local.dstEleId = GetNextElementId(local.dstUserId, local.dstLibId);
		</cfscript>
		<!--- Create new element for destination library --->
		<cfquery name="eleInsert" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				rxds.dselement (UserId_int, DSId_int, DSEId_int, Desc_vch)
			VALUES(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstUserId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstLibId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.dstEleId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.element.Desc_vch#">
			)
		</cfquery>
		<cfscript>
			local.retVal.node = StructNew();
			local.retVal.node.id = "#local.dstUserId#_#local.dstLibId#_#local.dstEleId#";
			local.retVal.node.label = getEle.Desc_vch;
			local.retVal.node.childs = ArrayNew(1);
			local.scrList = GetScriptList(local.srcUserId, local.srcLibId, local.srcEleId);
		</cfscript>
		
		<!--- Copy scripts from source local.element to destination local.element --->				
		<cfloop query = "local.scrList">
			<cfscript>
				local.ps = CopyScript("#local.srcUserId#_#local.srcLibId#_#local.srcEleId#_#local.scrList.DataId_int#", "#local.dstUserId#_#local.dstLibId#_#local.dstEleId#");
				if (local.ps.success) {
					ArrayAppend(local.retVal.node.childs, local.ps.node);
				}
			</cfscript>
		</cfloop>
		
		<cfscript>
			local.retVal.srcId = elementId;
			local.retVal.dstId = libraryId;
		</cfscript>
		
		<cfreturn local.retVal />
	</cffunction>
	
	<cffunction name="MoveElement" access="remote" output="false" hint="Cut/Paste a element to a library">
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfscript>
			var retVal = CopyElement(elementId, libraryId);
			if (retVal.success) {
				if (retVal.success) {
					DeleteElement(elementId);
				}
			}
		</cfscript>
		
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="ExportLibrary" access="remote" output="false" hint="Export a local.library">
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="options" type="struct" required="No" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			if (NOT isDefined("libraryId")) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			
			local.ids = ListToArray(libraryId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.library = IsExistedLibrary(local.userId, local.libId);
			if (IsBoolean(local.library)) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			local.eleList = GetElementList(local.userId, local.libId);
			local.params = "";
			
			if (options.isNoConversion) {
				local.params = "#local.params# -b 16 -r 44100";
			} else {
				if (options.is16Bit) {
					local.params = "#local.params# -b 16 -r 11025";
				} else {
					local.params = "#local.params# -b 8 -r 11025";
				}
			}
			if (options.isNormalize) {
				local.params = "#local.params# -v #options.valueNormalize#";
			}
		</cfscript>
		
		<cfif Not DirectoryExists("#application.baseDir#/export/#local.library.Desc_vch#")>
			<cfdirectory action = "create" directory = "#application.baseDir#/export/#local.library.Desc_vch#" recurse="yes" mode="0775" />
		</cfif>
		<cfloop query = "local.eleList">
			<cfif Not DirectoryExists("#application.baseDir#/export/#local.library.Desc_vch#/#local.eleList.Desc_vch#")>
				<cfdirectory action = "create" directory = "#application.baseDir#/export/#local.library.Desc_vch#/#local.eleList.Desc_vch#" recurse="yes" mode="0775" />
			</cfif>
			<cfset local.scrList = GetScriptList(local.userId, local.libId, local.eleList.DSEId_int) />
			<cfloop query = "local.scrList">
				<cfscript>
					local.srcFile = "#application.scriptPath#/U#local.userId#/L#local.libId#/E#local.eleList.DSEId_int#/RXDS_#local.userId#_#local.libId#_#local.eleList.DSEId_int#_#local.scrList.DataId_int#.mp3";
					local.dstFile = "#application.baseDir#/export/#local.library.Desc_vch#/#local.eleList.Desc_vch#/#options.prefix#";
					if (options.userDesfault) {
						local.dstFile = "#local.dstFile##local.scrList.Desc_vch#.wav";
					} else {
						if (options.userDSSD) {
						} else {
							local.dstFile = "#local.dstFile#DSSD_#local.scrList.DataId_int#_#local.libId#_#local.eleList.DSEId_int#.wav";
						}
					}
					local.args = '"#local.srcFile#" #local.params# "#local.dstFile#"';
				</cfscript>
				<cfif FileExists(local.srcFile)>
					<cfexecute
						name = "#application.soxPath#"
						arguments = "#local.args#" 
						outputFile = "#application.baseDir#soxlog.txt"
						timeout = "#application.soxMaxExeTime#">
					</cfexecute>
				</cfif>
			</cfloop>
		</cfloop>
		
		<cfreturn local.retVal />
	</cffunction>
</cfcomponent>
