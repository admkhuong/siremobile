<!--- No display --->
<cfcomponent output="true">
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
    <cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="IsExistedScript" access="remote" output="false" hint="Check script is existed">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="elementId" type="string" required="Yes" />
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfquery name="getScript" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
			FROM 
				rxds.scriptdata
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libraryId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#elementId#">
				AND DataId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scriptId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif getScript.RecordCount>
			<cfreturn getScript />
		</cfif>
		<cfreturn false />
	</cffunction>
	
	<cffunction name="GetScriptInfo" access="remote" output="false" hint="Get script info">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="libId" type="string" required="Yes" />
		<cfargument name="eleId" type="string" required="Yes" />
		<cfargument name="scrId" type="string" required="Yes" />
		
		<cfscript>
			script = StructNew();
			script.existed = false;
		</cfscript>
		<cfquery name="getScript" datasource="#Session.DBSourceEBM#">
			SELECT UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
			FROM 
				rxds.scriptdata
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#eleId#">
				AND DataId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#scrId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfscript>
			if (getScript.RecordCount) {
				script.existed = true;
				script.userId = getScript.UserId_int;
				script.libId = getScript.DSId_int;
				script.eleId = getScript.DSEId_int;
				script.scrId = getScript.DataId_int;
				script.label = getScript.Desc_vch;
				//script.url = "#application.baseUrl#script/U#dstUserId#/L#dstLibId#/E#dstEleId#/RXDS_#userId#_#dstLibId#_#dstEleId#_#dstScrId#.mp3";
			}
				
			return script;
		</cfscript>
	</cffunction>
	
	<cffunction name="RenameScript" access="remote" output="false" hint="Rename a script">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfargument name="name" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			local.ids = ListToArray(scriptId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
			local.scrId = local.ids[4];
		</cfscript>
		<cfquery name="updateScript" datasource="#Session.DBSourceEBM#">
			UPDATE 
				rxds.scriptdata
			SET 
				Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#name#">
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.eleId#">
				AND DataId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.scrId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		
		<cfreturn local.retVal />
	</cffunction>
	
	<cffunction name="DeleteScript" access="remote" output="false" hint="Delete a script">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			local.ids = ListToArray(scriptId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
			local.scrId = local.ids[4];
		</cfscript>
		<cfquery name="qDeleteScript" datasource="#Session.DBSourceEBM#">
			DELETE FROM 
				rxds.scriptdata
			WHERE 
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.userId#">
				AND DSId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.libId#">
				AND DSEId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.eleId#">
				AND DataId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.scrId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif FileExists("#application.scriptPath#/U#local.userId#/L#local.libId#/E#local.eleId#/RXDS_#local.userId#_#local.libId#_#local.eleId#_#local.scrId#.mp3")>
			<cffile action="delete"	file="#application.scriptPath#/U#local.userId#/L#local.libId#/E#local.eleId#/RXDS_#local.userId#_#local.libId#_#local.eleId#_#local.scrId#.mp3" />
		</cfif>
		
		<cfreturn local.retVal />
	</cffunction>
	
	<cffunction name="ExportScript" access="remote" output="false" hint="Export a script">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfargument name="filePath" type="string" required="Yes" />
		<cfargument name="options" type="object" required="No" />
		<cfscript>
			var local = StructNew();
			var userId = StructNew();
			var libId = StructNew();
			var eleId = StructNew();
			var scrId = StructNew();
			var srcFile = "";
			var dstFile = "";
			local.retVal = StructNew();
			local.ids = StructNew();
			retVal.success = true;
			retVal.message = "";
			
			local.ids = ListToArray(scriptId, "_");
			local.userId = local.ids[1];
			local.libId = local.ids[2];
			local.eleId = local.ids[3];
			local.scrId = local.ids[4];
			local.srcFile = "#application.scriptPath#/U#local.userId#/L#local.libId#/E#local.eleId#/RXDS_#local.userId#_#local.libId#_#local.eleId#_#local.scrId#.mp3";
			local.dstFile = "#application.baseDir#/export/script/U#local.userId#/L#local.libId#/E#local.eleId#/RXDS_#local.userId#_#local.libId#_#local.eleId#_#local.scrId#.mp3";
		</cfscript>
		<cfif Not DirectoryExists("#application.scriptPath#/#dstDirPath#")>
			<cfdirectory action = "create" directory = "#application.scriptPath#/#dstDirPath#" recurse="yes" mode="0775" />
		</cfif>
			
		<cfif FileExists("#application.scriptPath#/U#userId#/L#libId#/E#eleId#/RXDS_#userId#_#libId#_#eleId#_#scrId#.mp3")>
			<cfexecute
				name = "#application.soxPath#"
				arguments = '"#srcFile#" "#dstFile#"' 
				outputFile = "#application.baseDir#soxlog.txt"
				timeout = "#application.soxMaxExeTime#">
			</cfexecute>
		</cfif>
		
		<cfreturn retVal />
	</cffunction>

	<cffunction name="ScriptGetTreeXML" access="remote" output="true" hint="Get script's xml string for tree">
		<cfargument name="scriptId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.ids = ListToArray(scriptId, "_");
			local.retVal = '';
			if (isDefined("SESSION.isAdmin") AND SESSION.isAdmin) {
				local.retVal = local.retVal & '<node id="0" label="Root">';
				local.users = GetUserList();
				for (i = 1; i LTE local.users.RecordCount; i = i + 1) {
					if (local.users["UserId_int"][i] != local.ids[1]) {
						local.retVal = local.retVal & '<node id="' & local.users["UserId_int"][i] & '" label="' & local.users["UserName_vch"][i] & '" />';
					} else {
						local.retVal = local.retVal & '<node id="' & local.users["UserId_int"][i] & '" label="' & local.users["UserName_vch"][i] & '">';
						local.librarys = GetLibraryList(SESSION.userId);
						for (j = 1; j LTE local.librarys.RecordCount; j = j + 1) {
							if (local.librarys["DSId_int"][j] != local.ids[2]) {
								local.retVal = local.retVal & '<node id="' & SESSION.userId & '_' & local.librarys["DSId_int"][j] & '" label="' & local.librarys["Desc_vch"][j] & '" />';
							} else {
								local.retVal = local.retVal & '<node id="' & SESSION.userId & '_' & local.librarys["DSId_int"][j] & '" label="' & local.librarys["Desc_vch"][j] & '">';
								local.elements = GetElementList(local.ids[1], local.ids[2]);
								for (k = 1; k LTE local.elements.RecordCount; k = k + 1) {
									if (local.elements["DSEId_int"][k] != local.ids[3]) {
										local.retVal = local.retVal & '<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.elements["DSEId_int"][k] & '" label="' & local.elements["Desc_vch"][k] & '" />';
									} else {
										local.retVal = local.retVal & '<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.elements["DSEId_int"][k] & '" label="' & local.elements["Desc_vch"][k] & '">';
										local.scripts = GetScriptList(local.ids[1], local.ids[2], local.ids[3]);
										for (l = 1; l LTE local.scripts.RecordCount; l = l + 1) {
											local.retVal = local.retVal &
												'<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.ids[3] & '_' & local.scripts["DataId_int"][l] &
												'" label="' & local.scripts["Desc_vch"][l] &
												'" url="' & application.baseUrl & "get-script.cfm?id=" & local.scripts["UserId_int"][l] & "_" & local.scripts["DSId_int"][l] & "_" & local.scripts["DSEId_int"][l] & "_" & local.scripts["DataId_int"][l] & '" />'
											;
										}
										local.retVal = local.retVal & '</node>';
									}
								}
								local.retVal = local.retVal & '</node>';
							}
						}
						local.retVal = local.retVal & '</node>';
					}
				}
			} else {
				local.retVal = local.retVal & '<node id="' & SESSION.userId & '" label="' & SESSION.userName & '">';
				local.librarys = GetLibraryList(SESSION.userId);
				for (j = 1; j LTE local.librarys.RecordCount; j = j + 1) {
					if (local.librarys["DSId_int"][j] != local.ids[2]) {
						local.retVal = local.retVal & '<node id="' & SESSION.userId & '_' & local.librarys["DSId_int"][j] & '" label="' & local.librarys["Desc_vch"][j] & '" />';
					} else {
						local.retVal = local.retVal & '<node id="' & SESSION.userId & '_' & local.librarys["DSId_int"][j] & '" label="' & local.librarys["Desc_vch"][j] & '">';
						local.elements = GetElementList(local.ids[1], local.ids[2]);
						for (k = 1; k LTE local.elements.RecordCount; k = k + 1) {
							if (local.elements["DSEId_int"][k] != local.ids[3]) {
								local.retVal = local.retVal & '<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.elements["DSEId_int"][k] & '" label="' & local.elements["Desc_vch"][k] & '" />';
							} else {
								local.retVal = local.retVal & '<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.elements["DSEId_int"][k] & '" label="' & local.elements["Desc_vch"][k] & '">';
								local.scripts = GetScriptList(local.ids[1], local.ids[2], local.ids[3]);
								for (l = 1; l LTE local.scripts.RecordCount; l = l + 1) {
									local.retVal = local.retVal &
										'<node id="' & local.ids[1] & '_' & local.ids[2] & '_' & local.ids[3] & '_' & local.scripts["DataId_int"][l] &
										'" label="' & local.scripts["Desc_vch"][l] &
										'" url="' & application.baseUrl & "get-script.cfm?id=" & local.scripts["UserId_int"][l] & "_" & local.scripts["DSId_int"][l] & "_" & local.scripts["DSEId_int"][l] & "_" & local.scripts["DataId_int"][l] & '" />'
									;
								}
								local.retVal = local.retVal & '</node>';
							}
						}
						local.retVal = local.retVal & '</node>';
					}
				}
			}
			local.retVal = local.retVal & '</node>';
			return local.retVal;
		</cfscript>
	</cffunction>
</cfcomponent>
