<!--- No display --->
<cfcomponent output="false">
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
    <cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="IsExistedUser" access="remote" output="false" hint="Check local.user is existed">
		<cfargument name="userId" type="string" required="Yes" />
		<cfset var getUser = 0>
		<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
			SELECT UserId_int, UserName_vch, Desc_vch
			FROM 
				simpleobjects.useraccount
			WHERE 
				UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				AND Active_int='1'
			LIMIT 1
		</cfquery>
		<cfif getUser.RecordCount>
			<cfreturn getUser />
		</cfif>
		<cfreturn false />
	</cffunction>
	
	<cffunction name="GetNextLibraryId" access="remote" output="false" hint="Get next local.library id of local.user">
		<cfargument name="userId" type="string" required="Yes" />
		<cfset var getNextLibraryId = 0>
		
		<cfquery name="getNextLibraryId" datasource="#Session.DBSourceEBM#">
			SELECT
				CASE 
					WHEN MAX(DSId_int) IS NULL THEN 1
				ELSE
					MAX(DSId_int) + 1 
				END AS NextLibraryId  
			FROM
				rxds.dynamicscript
			WHERE
				UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
		</cfquery>
		<cfreturn getNextLibraryId.NextLibraryId />
	</cffunction>
	
	<cffunction name="GetLibraryList" access="remote" output="false" hint="Get all local.library of local.user">
		<cfargument name="userId" type="string" required="Yes" />
		<cfargument name="sortname" type="string" default="Desc_vch" />
		<cfargument name="sorttype" type="string" default="ASC" />
		<cfset var getLibrary = 0>
		<cfoutput>
			<cfquery name="getLibrary" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, DSId_int, Desc_vch
				FROM 
					rxds.dynamicscript
				WHERE 
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">
				ORDER BY
					#sortname# #sorttype#
			</cfquery>
		</cfoutput>
		<cfreturn getLibrary />
	</cffunction>
	
	<cffunction name="GetLibraryRoot" access="remote" output="false" returnformat="JSON">
		<cfset var getLibrary = 0>
		<cfset var libraryArr = ArrayNew(1)>
		
		<cftry>
			<cfquery name="getLibrary" datasource="#Session.DBSourceEBM#">
					SELECT 
						ds.UserId_int, 
						ds.DSId_int, 
						ds.Desc_vch,
						Ele.TotalELE
					FROM 
						rxds.dynamicscript as ds
					JOIN
						(
							Select
								COUNT(*) AS TotalELE,
								DSId_int
							FROM
								rxds.dselement
							WHERE
								UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
							GROUP BY DSId_int
						) as Ele
					ON
						Ele.DSId_int = ds.DSId_int
					WHERE 
						ds.UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.UserId#">
			</cfquery>
			<cfset libraryArr = ArrayNew(1)>
			
			<cfloop query="getLibrary">
				<cfset var hasChildren = false>
				<cfif getLibrary.TotalELE GT 0 >
					<cfset hasChildren = true>	
				</cfif>
				
				<cfset var libItem = {
					"id" = 'ds_' & getLibrary.DSId_int,
					"text" = getLibrary.Desc_vch,
					"children" = hasChildren
				}>
				<cfset ArrayAppend(libraryArr, libItem)>
			</cfloop>
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

		<cfreturn libraryArr />
	</cffunction>
	
	<cffunction name="CopyLibrary" access="remote" output="false" hint="Copy/Paste a local.library to a local.user">
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="userId" type="string" required="Yes" />
                
    <cfscript>
			var local = StructNew();
			local.retVal = StructNew();
			local.retVal.success = true;
			local.retVal.message = "";
			
			if (NOT isDefined("libraryId")) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			
			if (NOT isDefined("userId")) {
				local.retVal.success = false;
				local.retVal.message = "UserId is incorect";
				return local.retVal;
			}
			
			local.ids = ListToArray(libraryId, "_");
			local.srcUserId = local.ids[1];
			local.srcLibId = local.ids[2];
			local.library = IsExistedLibrary(local.srcUserId, local.srcLibId);
			if (IsBoolean(local.library)) {
				local.retVal.success = false;
				local.retVal.message = "LibraryId is incorect";
				return local.retVal;
			}
			
			local.user = IsExistedUser(userId);
			if (IsBoolean(local.user)) {
				local.retVal.success = false;
				local.retVal.message = "UserId is incorect";
				return local.retVal;
			}
		</cfscript>
		
		<!--- Create new local.library for destination local.user --->				
		<cfset var dstLibId = GetNextLibraryId(userId) />
		<cfset var libInsert = 0>
		<cfquery name="libInsert" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				rxds.dynamicscript (UserId_int, DSId_int, Desc_vch)
			VALUES(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dstLibId#">, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.library.Desc_vch#">
			)
		</cfquery>
		<cfscript>
			local.retVal.node = StructNew();
			local.retVal.node.id = "#userId#_#dstLibId#";
			local.retVal.node.label = local.library.Desc_vch;
			local.retVal.node.childs = ArrayNew(1);
			local.eleList = GetElementList(local.srcUserId, local.srcLibId);
		</cfscript>
		
		<!--- Copy scripts from source element to destination element --->				
		<cfloop query = "local.eleList">
			<cfscript>
				local.pe = CopyElement("#local.srcUserId#_#local.srcLibId#_#local.eleList.DSEId_int#", "#userId#_#dstLibId#");
				if (local.pe.success) {
					ArrayAppend(local.retVal.node.childs, local.pe.node);
				}
			</cfscript>
		</cfloop>
		
		<cfscript>
			local.retVal.srcId = libraryId;
			local.retVal.dstId = userId;
		</cfscript>
		
		<cfreturn local.retVal />
	</cffunction>
	
	<cffunction name="MoveLibrary" access="remote" output="false" hint="Cut/Paste a local.library to a local.user">
		<cfargument name="libraryId" type="string" required="Yes" />
		<cfargument name="userId" type="string" required="Yes" />
		<cfscript>
			var local = StructNew();
			local.retVal = CopyLibrary(libraryId, userId);
			if (local.retVal.success) {
				if (local.retVal.success) {
					DeleteLibrary(libraryId);
				}
			}
		</cfscript>
		
		<cfreturn local.retVal />
	</cffunction>
</cfcomponent>
