<!---<cfinclude template="config.cfm">--->
<cfif isDefined("SESSION.loggedIn") AND (SESSION.loggedIn EQ "1")>
	<cflocation url="index" addtoken="no">
</cfif>
<cfif isDefined("form.username")>
	<cfquery name="loginCheck" datasource="#application.datasource#">
	SELECT UserId_int, UserName_vch, Password_vch, UserLevel_int
	FROM #application.User_database#
	WHERE UserName_vch = '#form.username#'
	AND Password_vch = MD5('#form.password#')
	AND Active_int = 1
	LIMIT 1
	</cfquery>
	<cfif loginCheck.RecordCount>
		<cfset SESSION.loggedIn = "1" />
		<cfset SESSION.userId = "#loginCheck.UserId_int#" />
		<cfset SESSION.userName = "#loginCheck.UserName_vch#" />
		<cfif loginCheck.UserLevel_int>
			<cfset SESSION.isAdmin = true>
		<cfelse>
			<cfset SESSION.isAdmin = false>
		</cfif>
		<cflocation url="index" addtoken="no">
		<cfelse>
		<cfset errorMessage = "Username or Password incorrect">
	</cfif>
	<cfelse>
	<cfset loginCheck.RecordCount = "0">
	<cfset form.userName = "">
</cfif>
<html>
<head>
<title>Login</title>
</head>
<body>
<cfif isDefined("errorMessage")>
	<p class="error"> <cfoutput> #errorMessage# </cfoutput> </p>
</cfif>
<form action="login" method="post">
	Username:
	<input type="text" name="username" value="<cfoutput>#FORM.username#</cfoutput>" />
	<br />
	Password:
	<input type="password" name="password" />
	<br />
	<input type="submit" value="Login" />
	<a href="<cfoutput>#application.baseUrl#</cfoutput>register">Register</a>
</form>
</body>
</html>
