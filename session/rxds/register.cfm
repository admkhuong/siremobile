<!---<cfinclude template="config.cfm">--->

<cfif isDefined("form.username")>
	<cfquery name="loginCheck" datasource="#application.datasource#">
	  SELECT UserId_int, UserName_vch, Password_vch FROM #application.User_database#
	  WHERE UserName_vch='#form.username#'
		LIMIT 1
 	</cfquery>
 
	<cfif loginCheck.RecordCount>
	 	<cfset errorMessage = "Username is existed. Please choose other username.">
	<cfelse>
		<cfif form.password NEQ form.password2>
			<cfset errorMessage = "2 passwords not match. Please check your password again.">
		<cfelse>
			<cfquery name="userInsert" datasource="#application.datasource#" result="r">  
				INSERT INTO #application.User_database# (UserName_vch, Password_vch, Created_dt)
				VALUES('#form.username#', MD5('#form.password#'), NOW())
			</cfquery>
			<cfdump var="#r#" />
			<cfset SESSION.loggedIn = "1">
			<cfset SESSION.userId = "#r.generatedkey#">
		  <cfset SESSION.userName = "#form.username#">
		  <cflocation url="index" addtoken="no">
		</cfif>
 	</cfif>
<cfelse>
	<cfset loginCheck.RecordCount = "0">
	<cfset form.userName = "">
</cfif>

<html>
<head>
<title>Register</title>
</head>
<body>
	<cfif isDefined("errorMessage")>
	<p class="error">
		<cfoutput>
			#errorMessage#
		</cfoutput>
	</p>
	</cfif>
	<form action="register" method="post">
		Username: <input type="text" name="username" value="<cfoutput>#FORM.username#</cfoutput>" /><br />
		Password: <input type="password" name="password" /><br />
		Confirm Password: <input type="password2" name="password2" /><br />
		<input type="submit" value="Submit" />
	</form>
</body>
</html>
