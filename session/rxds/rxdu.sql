# SQL Manager 2010 for MySQL 4.5.0.9
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : rxdu


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `rxdu`;

CREATE DATABASE `rxdu`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `rxdu`;

SET sql_mode = '';

#
# Structure for the `dselement` table : 
#

DROP TABLE IF EXISTS `dselement`;

CREATE TABLE `dselement` (
  `DSEId_int` int(11) NOT NULL DEFAULT '0',
  `DSId_int` int(11) NOT NULL DEFAULT '0',
  `UserId_int` int(11) NOT NULL DEFAULT '0',
  `Active_int` int(11) DEFAULT '1',
  `Desc_vch` text,
  PRIMARY KEY (`DSEId_int`,`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `dynamicscript` table : 
#

DROP TABLE IF EXISTS `dynamicscript`;

CREATE TABLE `dynamicscript` (
  `DSId_int` int(11) NOT NULL DEFAULT '0',
  `UserId_int` int(11) NOT NULL DEFAULT '0',
  `Active_int` int(11) DEFAULT '1',
  `Desc_vch` text,
  PRIMARY KEY (`DSId_int`,`UserId_int`),
  UNIQUE KEY `UC_DSId_int` (`DSId_int`,`UserId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `UserId_int` (`UserId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=8192;

#
# Structure for the `scriptdata` table : 
#

DROP TABLE IF EXISTS `scriptdata`;

CREATE TABLE `scriptdata` (
  `DataId_int` int(11) NOT NULL DEFAULT '0',
  `DSEId_int` int(11) NOT NULL DEFAULT '0',
  `DSId_int` int(11) NOT NULL DEFAULT '0',
  `UserId_int` int(11) NOT NULL DEFAULT '0',
  `StatusId_int` int(11) DEFAULT NULL,
  `AltId_vch` varchar(255) NOT NULL DEFAULT '',
  `Length_int` int(11) DEFAULT '0',
  `Format_int` int(11) DEFAULT '0',
  `Active_int` int(11) DEFAULT '1',
  `Desc_vch` text,
  `TTS_vch` text,
  `AccessLevel_int` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DataId_int`,`DSEId_int`,`DSId_int`,`UserId_int`),
  KEY `DataId_int` (`DataId_int`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `UserId_int` (`UserId_int`),
  KEY `StatusId_int` (`StatusId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `IDX_AccessLevel_int` (`AccessLevel_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `useraccount` table : 
#

DROP TABLE IF EXISTS `useraccount`;

CREATE TABLE `useraccount` (
  `UserId_int` int(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` varchar(255) NOT NULL DEFAULT '',
  `Password_vch` varchar(255) NOT NULL,
  `AltUserName_vch` varchar(255) DEFAULT NULL,
  `AltPassword_vch` varchar(255) DEFAULT NULL,
  `UserLevel_int` int(11) NOT NULL DEFAULT '0',
  `Desc_vch` varchar(255) DEFAULT NULL,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `CompanyName_vch` varchar(255) DEFAULT NULL,
  `HomePhoneId_int` int(11) DEFAULT NULL,
  `WorkPhoneId_int` int(11) DEFAULT NULL,
  `AlternatePhoneId_int` int(11) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Token_vch` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for the `dselement` table  (LIMIT 0,500)
#

INSERT INTO `dselement` (`DSEId_int`, `DSId_int`, `UserId_int`, `Active_int`, `Desc_vch`) VALUES 
  (1,1,1,1,'Element 1_1_1'),
  (1,1,2,1,'Element 2_1_1'),
  (1,1,3,1,'Element 3_1_1'),
  (1,2,1,1,'Element 1_2_1'),
  (1,3,1,1,'Element 1_3_1'),
  (2,1,1,1,'Element 1_1_2'),
  (2,1,3,1,'Element 3_1_2'),
  (2,2,1,1,'Element 1_2_2');
COMMIT;

#
# Data for the `dynamicscript` table  (LIMIT 0,500)
#

INSERT INTO `dynamicscript` (`DSId_int`, `UserId_int`, `Active_int`, `Desc_vch`) VALUES 
  (1,1,1,'Lib 1_1'),
  (2,1,1,'Lib 1_2'),
  (3,1,1,'Lib 1_3'),
  (4,1,1,'Lib 1_4');
COMMIT;

#
# Data for the `scriptdata` table  (LIMIT 0,500)
#

INSERT INTO `scriptdata` (`DataId_int`, `DSEId_int`, `DSId_int`, `UserId_int`, `StatusId_int`, `AltId_vch`, `Length_int`, `Format_int`, `Active_int`, `Desc_vch`, `TTS_vch`, `AccessLevel_int`) VALUES 
  (2,1,3,1,NULL,'',0,0,1,'6_10100.mp3',NULL,0),
  (2,2,1,1,NULL,'',0,0,1,'3.mp3',NULL,0),
  (3,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (4,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_11.mp3',NULL,0),
  (4,2,1,1,NULL,'',0,0,1,'news_2.wav',NULL,0),
  (5,1,1,1,NULL,'',0,0,1,'Record_1_1_1_5.wav',NULL,0),
  (5,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (5,2,1,1,NULL,'',0,0,1,'news.wav',NULL,0),
  (6,1,1,1,NULL,'',0,0,1,'Record_1_1_1_6.wav',NULL,0),
  (6,1,2,1,NULL,'',0,0,1,'3.WAV',NULL,0),
  (6,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_12.mp3',NULL,0),
  (6,2,1,1,NULL,'',0,0,1,'New name 11',NULL,0),
  (7,1,1,1,NULL,'',0,0,1,'Record_1_1_1_7.wav',NULL,0),
  (7,1,2,1,NULL,'',0,0,1,'3.WAV',NULL,0),
  (7,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (8,1,1,1,NULL,'',0,0,1,'Record_1_1_1_8.wav',NULL,0),
  (8,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (8,2,1,1,NULL,'',0,0,1,'test new name',NULL,0),
  (9,1,1,1,NULL,'',0,0,1,'xincongoitennhau.mp3',NULL,0),
  (9,1,3,1,NULL,'',0,0,1,'rxds_1_2_2_11.mp3',NULL,0),
  (10,1,1,1,NULL,'',0,0,1,'The phantom of the opera.mp3',NULL,0),
  (10,1,2,1,NULL,'',0,0,1,'3.WAV',NULL,0),
  (10,2,1,1,NULL,'',0,0,1,'6.mp3',NULL,0),
  (11,1,1,1,NULL,'',0,0,1,'Castles and Dreams.mp3',NULL,0),
  (11,1,2,1,NULL,'',0,0,1,'testrename',NULL,0),
  (11,2,2,1,NULL,'',0,0,1,'new_name',NULL,0),
  (12,1,1,1,NULL,'',0,0,1,'news_2.wav',NULL,0),
  (12,1,2,1,NULL,'',0,0,1,'New name 1234',NULL,0),
  (12,2,2,1,NULL,'',0,0,1,'New name 1_1_1_4',NULL,0),
  (13,1,1,1,NULL,'',0,0,1,'7.mp3',NULL,0),
  (13,1,2,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (13,2,2,1,NULL,'',0,0,1,'Record_1_2_2_5.wav',NULL,0),
  (14,1,1,1,NULL,'',0,0,1,'xincongoitennhau.mp3',NULL,0),
  (14,1,2,1,NULL,'',0,0,1,'rxds_1_2_2_11.mp3',NULL,0),
  (14,2,2,1,NULL,'',0,0,1,'rxds_1_2_1_7.mp3',NULL,0),
  (15,1,1,1,NULL,'',0,0,1,'rxds_1_1_2_1.mp3',NULL,0),
  (16,1,1,1,NULL,'',0,0,1,'Chiec-La-Mua-Dong.mp3',NULL,0),
  (17,1,1,1,NULL,'',0,0,1,'Castles and Dreams.mp3',NULL,0),
  (18,1,1,1,NULL,'',0,0,1,'BlackMore s Night  Now And Then.mp3',NULL,0),
  (19,1,1,1,NULL,'',0,0,1,'3.WAV',NULL,0),
  (20,1,1,1,NULL,'',0,0,1,'6.mp3',NULL,0),
  (21,1,1,1,NULL,'',0,0,1,'5.wav',NULL,0),
  (22,1,1,1,NULL,'',0,0,1,'Taunt021.wav',NULL,0),
  (23,1,1,1,NULL,'',0,0,1,'A-sombre-dance-Chapter-6.mp3',NULL,0),
  (24,1,1,1,NULL,'',0,0,1,'Castles and Dreams.mp3',NULL,0),
  (25,1,1,1,NULL,'',0,0,1,'CastlesandDreams.mp3',NULL,0),
  (26,1,1,1,NULL,'',0,0,1,'a b.mp3',NULL,0),
  (27,1,1,1,NULL,'',0,0,1,'BlackMore s Night  Now And Then.mp3',NULL,0),
  (28,1,1,1,NULL,'',0,0,1,'Midnight (1).mp3',NULL,0),
  (29,1,1,1,NULL,'',0,0,1,'Record_1_1_1_29.wav',NULL,0),
  (30,1,1,1,NULL,'',0,0,1,'a b.mp3',NULL,0),
  (31,1,1,1,NULL,'',0,0,1,'Record_1_1_1_31.wav',NULL,0),
  (32,1,1,1,NULL,'',0,0,1,'Record_1_1_1_32.wav',NULL,0),
  (33,1,1,1,NULL,'',0,0,1,'Record_1_1_1_33.wav',NULL,0),
  (34,1,1,1,NULL,'',0,0,1,'android setup guide.txt',NULL,0),
  (35,1,1,1,NULL,'',0,0,1,'rxds_1_2_2_13.mp3',NULL,0),
  (36,1,1,1,NULL,'',0,0,1,'rxds_1_2_2_12.mp3',NULL,0);
COMMIT;

#
# Data for the `useraccount` table  (LIMIT 0,500)
#

INSERT INTO `useraccount` (`UserId_int`, `UserName_vch`, `Password_vch`, `AltUserName_vch`, `AltPassword_vch`, `UserLevel_int`, `Desc_vch`, `ClientServicesReps_vch`, `PrimaryRep_vch`, `CSRNotes_vch`, `FirstName_vch`, `LastName_vch`, `Address1_vch`, `Address2_vch`, `City_vch`, `State_vch`, `Country_vch`, `PostalCode_vch`, `EmailAddress_vch`, `CompanyName_vch`, `HomePhoneId_int`, `WorkPhoneId_int`, `AlternatePhoneId_int`, `FaxPhoneId_int`, `PrimaryPhoneStr_vch`, `HomePhoneStr_vch`, `WorkPhoneStr_vch`, `AlternatePhoneStr_vch`, `FaxPhoneStr_vch`, `CBPId_int`, `Created_dt`, `LastLogIn_dt`, `Public_int`, `Active_int`, `Token_vch`) VALUES 
  (1,'votong','ba2a6b3f8ff7a6ef793b1531e65f7111',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-03-21 15:24:28',NULL,NULL,NULL,'uy8w6emgnf96q9aoemrfi23a2bvpbp9d3xggpd5w57xo0vrdfl8udf1vktp0igt9'),
  (2,'tuyennguyen','tuyennguyen',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-03-31 11:18:55',NULL,NULL,NULL,NULL),
  (3,'jeff','jeff',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-03-31 11:19:20',NULL,NULL,NULL,NULL),
  (4,'a','a',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-04-01 10:32:29',NULL,NULL,NULL,NULL),
  (5,'b','b',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-04-01 10:33:22',NULL,NULL,NULL,NULL),
  (6,'c','c',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-04-01 10:39:46',NULL,NULL,NULL,NULL),
  (7,'quynh','quynh',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'2011-04-01 16:19:32',NULL,NULL,NULL,NULL);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;