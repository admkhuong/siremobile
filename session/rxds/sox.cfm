﻿<!---<cfinclude template="config.cfm">--->
<cfset SESSION.userId = 1 />
<!---<cfif (NOT isDefined("SESSION.loggedIn")) OR (SESSION.loggedIn NEQ "1")>
 	<cflocation url="login" addtoken="no">
 	<cfset StructClear(SESSION)>
</cfif>--->

<cfif isDefined("scrId")>
	<cfif not isDefined("step")>
		<cfset step = 1 />
	</cfif>
	<cfset ids = ListToArray(scrId, "_") />
	<cfset userId = ids[1] /> 
	<cfset libId = ids[2] /> 
	<cfset eleId = ids[3] /> 
	<cfset scrId = ids[4] /> 
	<cfquery name="getScript" datasource="#application.datasource#">
		SELECT UserId_int, DSId_int, DSEId_int, DataId_int, Desc_vch
		FROM #application.database#.scriptdata
		WHERE UserId_int='#SESSION.userId#'
			AND DSId_int='#libId#'
			AND DSEId_int='#eleId#'
			AND DataId_int='#scrId#'
			AND Active_int='1'
		LIMIT 1
	</cfquery>
	<cfif getScript.RecordCount>
		<cfif step EQ 1>
			<!--- If step = 0 then load from base script --->
			<cfset fileName = "RXDS_#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#.mp3" />
			<cfset dstDirPath = "U#SESSION.userId#/L#getScript.DSId_int#/E#getScript.DSEId_int#" />
			<cfset baseFile="#application.baseDir#script/#dstDirPath#/#fileName#" />
			<cfset srcFile="#application.baseDir#tmp/#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#_0.mp3" />
			<cffile action="copy"	source="#baseFile#" destination="#srcFile#">
		<cfelse>
			<!--- Else then load from previous step --->
			<cfset preStep = step - 1 />
			<cfset srcFile = "#application.baseDir#tmp/#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#_#preStep#.mp3" />
		</cfif>
		
		<cfset dstPath = "#SESSION.userId#_#getScript.DSId_int#_#getScript.DSEId_int#_#scrId#_#step#.mp3" />
		<cfset dstFile="#application.baseDir#tmp/#dstPath#" />
		
		<cfswitch expression="#lCase(URL.task)#">
			<cfcase value="speed">
				<cfset args="#srcFile# #dstFile# #URL.task# " />
				<cfif isDefined("URL.factor") AND URL.factor NEQ 1 >
					<cfset args="#args# #URL.factor#" />
					<cfexecute
						name = "#application.soxPath#"
				    arguments = "#args#" 
				    outputFile = "#application.baseDir#soxlog.txt"
				    timeout = "#application.soxMaxExeTime#">
					</cfexecute>
				</cfif>
			</cfcase>
			
			<cfcase value="trim">
				<cfset args="#srcFile# #dstFile# #URL.task# " />
				
				<cfif isDefined("URL.start") >
					<cfset args="#args# #URL.start#" />
				</cfif>
				<cfif isDefined("URL.length") >
					<cfset args="#args# #URL.length#" />
				</cfif>
				
				<cfexecute
					name = "#application.soxPath#"
			    arguments = "#args#" 
			    outputFile = "#application.baseDir#soxlog.txt"
			    timeout = "#application.soxMaxExeTime#">
				</cfexecute>
			</cfcase>
			
			<cfcase value="copypaste">
				<cfif isDefined("URL.start") >
					<!--- Copy (part 2)) --->
					<cfset tmpCopyFile2="#application.baseDir#tmp/tmp2_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
					<cfset tmpArgs="#srcFile# #tmpCopyFile2#" />
					<cfset tmpArgs="#tmpArgs# trim #URL.start#" />
					<cfif isDefined("URL.length") >
						<cfset tmpArgs="#tmpArgs# #URL.length#" />
					</cfif>
					<cfexecute
						name = "#application.soxPath#"
				    arguments = "#tmpArgs#" 
				    outputFile = "#application.baseDir#soxlog.txt"
				    timeout = "#application.soxMaxExeTime#">
					</cfexecute>
					
					<cfif isDefined("URL.at") >
						<!--- Paste --->
						<cfif URL.at GT 0 >
							<!--- Copy part 1 --->
							<cfset tmpCopyFile1="#application.baseDir#tmp/tmp1_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
							<cfset tmpArgs="#srcFile# #tmpCopyFile1#" />
							<cfset tmpArgs="#tmpArgs# trim 0 #URL.at#" />
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpArgs#" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
						</cfif>
						<cfset tmpCopyFile3="#application.baseDir#tmp/tmp3_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile3#" />
						<cfset tmpArgs="#tmpArgs# trim #URL.at#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
						
						<cfif isDefined("tmpCopyFile1") >
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile1# #tmpCopyFile2# #tmpCopyFile3# #dstFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
							
							<cffile action="delete"	file="#tmpCopyFile1#" />
						<cfelse>
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile2# #tmpCopyFile3# #dstFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
						</cfif>
						<cffile action="delete"	file="#tmpCopyFile2#" />
						<cffile action="delete"	file="#tmpCopyFile3#" />
					</cfif>
				</cfif>
			</cfcase>
			
			<cfcase value="cut">
				<cfif isDefined("URL.start")>
					<cfif URL.start GT 0>
						<!--- Copy part 1 --->
						<cfset tmpCopyFile1="#application.baseDir#tmp/tmp1_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile1#" />
						<cfset tmpArgs="#tmpArgs# trim 0 #URL.start#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
					</cfif>
					<cfif isDefined("URL.length")>
						<!--- Copy part 2 --->
						<cfset end = URL.start + URL.length />
						<cfset tmpCopyFile2="#application.baseDir#tmp/tmp2_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile2#" />
						<cfset tmpArgs="#tmpArgs# trim #end#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
						
						<!--- Complete cut: dstFile = tmpCopyFile1 + tmpCopyFile2 --->
						<cfif isDefined("tmpCopyFile1")>
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile1# #tmpCopyFile2# #dstFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
							
							<cffile action="delete"	file="#tmpCopyFile1#" />
							<cffile action="delete"	file="#tmpCopyFile2#" />
						<cfelse>
							<cffile action="rename"	source="#tmpCopyFile2#" destination="#dstFile#" />
						</cfif>
					<cfelse>
						<cfif isDefined("tmpCopyFile1")>
							<cffile action="rename"	source="#tmpCopyFile1#" destination="#dstFile#" />
						</cfif>
					</cfif>
				</cfif>
			</cfcase>
			
			<cfcase value="cutpaste">
				<cfif isDefined("URL.start")>
					<!--- Cut --->
					<cfif URL.start GT 0>
						<!--- Copy part 1 --->
						<cfset tmpCopyFile1="#application.baseDir#tmp/tmp1_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile1#" />
						<cfset tmpArgs="#tmpArgs# trim 0 #URL.start#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
					</cfif>
					<cfif isDefined("URL.length") >
						<!--- Copy part 2 --->
						<cfset tmpCopyFile2="#application.baseDir#tmp/tmp2_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile2#" />
						<cfset tmpArgs="#tmpArgs# trim #URL.start# #URL.length#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
						
						<!--- Copy part 3 --->
						<cfset end = URL.start + URL.length />
						<cfset tmpCopyFile3 = "#application.baseDir#tmp/tmp3_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs = "#srcFile# #tmpCopyFile3#" />
						<cfset tmpArgs = "#tmpArgs# trim #end#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
						
						<!--- Complete cut: srcFile = tmpCopyFile1 + tmpCopyFile3 --->
						<cfif isDefined("tmpCopyFile1")>
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile1# #tmpCopyFile3# #srcFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
							
							<cffile action="delete"	file="#tmpCopyFile1#" />
							<cffile action="delete"	file="#tmpCopyFile3#" />
						<cfelse>
							<cffile action="rename"	source="#tmpCopyFile3#" destination="#srcFile#" />
						</cfif>
					<cfelse>
						<cfif isDefined("tmpCopyFile1")>
							<cffile action="rename"	source="#tmpCopyFile1#" destination="#srcFile#" />
						</cfif>
					</cfif>
					
					<!--- Paste --->
					<cfif isDefined("URL.at") >
						<cfif URL.at GT 0 >
							<!--- Copy part 1 --->
							<cfset tmpCopyFile1="#application.baseDir#tmp/tmp1_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
							<cfset tmpArgs="#srcFile# #tmpCopyFile1#" />
							<cfset tmpArgs="#tmpArgs# trim 0 #URL.at#" />
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpArgs#" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
						</cfif>
						<cfset tmpCopyFile3="#application.baseDir#tmp/tmp3_#SESSION.userId#_6_#DateDiff("s", CreateDate(1970,1,1), Now())#.mp3" />
						<cfset tmpArgs="#srcFile# #tmpCopyFile3#" />
						<cfset tmpArgs="#tmpArgs# trim #URL.at#" />
						<cfexecute
							name = "#application.soxPath#"
					    arguments = "#tmpArgs#" 
					    outputFile = "#application.baseDir#soxlog.txt"
					    timeout = "#application.soxMaxExeTime#">
						</cfexecute>
						
						<cfif isDefined("tmpCopyFile1") >
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile1# #tmpCopyFile2# #tmpCopyFile3# #dstFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
							
							<cffile action="delete"	file="#tmpCopyFile1#" />
						<cfelse>
							<cfexecute
								name = "#application.soxPath#"
						    arguments = "#tmpCopyFile2# #tmpCopyFile3# #dstFile# splice" 
						    outputFile = "#application.baseDir#soxlog.txt"
						    timeout = "#application.soxMaxExeTime#">
							</cfexecute>
						</cfif>
						<cffile action="delete"	file="#tmpCopyFile2#" />
						<cffile action="delete"	file="#tmpCopyFile3#" />
					</cfif>
				</cfif>
			</cfcase>
			
		</cfswitch>
					
		<cfoutput>#application.baseUrl#tmp/#dstPath#</cfoutput>
	</cfif>
</cfif>