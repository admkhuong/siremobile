<!---<cfinclude template="config.cfm">--->
<cfinclude template="lib/jsonencode.cfm">
<!---<cfset SESSION.loggedIn = "1">
<cfset SESSION.userId = "1">
<cfset SESSION.userName = "votong">
<cfset SESSION.isAdmin = true />--->

<cfswitch expression="#lCase(a)#">
	<cfcase value="gcu">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
			retVal.userId = SESSION.userId;
			retVal.userName = SESSION.userName;
			retVal.isAdmin = SESSION.isAdmin;
		</cfscript>
	</cfcase>
	
	<cfcase value="i">
		<cfscript>
			retVal = StructNew();
			retVal.success = true;
			retVal.message = "";
		</cfscript>
		<cfquery name="loginCheck" datasource="#application.datasource#">
			SELECT UserId_int, UserName_vch, Password_vch FROM #application.User_database#
			WHERE UserName_vch='#username#'
			LIMIT 1
		</cfquery>
		<cfif loginCheck.RecordCount>
			<cfset retVal.success = false />
			<cfset retVal.message = "Username is existed. Please choose other username." />
			<cfelse>
			<cfif password NEQ password2>
				<cfset retVal.success = false />
				<cfset retVal.message = "2 passwords not match. Please check your password again." />
			<cfelse>
				<cfquery name="userInsert" datasource="#application.datasource#" result="r">
					INSERT INTO #application.User_database# (UserName_vch, Password_vch, Active_int, Created_dt)
					VALUES('#username#', MD5('#password#'), 1, NOW())
				</cfquery>
				<cfscript>
					retVal.userId = r.generatedkey;
					retVal.userName = userName;
					retVal.isAdmin = false;
				</cfscript>
			</cfif>
		</cfif>
	</cfcase>
</cfswitch>

<cfoutput>#jsonencode(retVal)#</cfoutput>