<cfparam name="inpShowPauseOption" default="1">    

<cfparam name="STARTMINUTE_TI" TYPE="string" default="0">
<cfparam name="ENDMINUTE_TI" TYPE="string" default="0">
<cfparam name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0">
<cfparam name="BLACKOUTENDHOUR_TI" TYPE="string" default="0">
<cfparam name="ENABLED_TI" TYPE="string" default="1"> 

<cfoutput>
	<input TYPE="hidden" name="LOOPLIMIT_INT" id="LOOPLIMIT_INT" value="100" /> 
          
        <div style="display:block; clear:both; width:500px; padding:10px; height: 165px; min-height:165px;">

            <div style="float:left; min-width:245px;">
		        <label>Blackout Start Hour</label>
                <div id="BLACKOUTSTARTHOUR_TI" class="" type="text" name="BLACKOUTSTARTHOUR_TI"></div>
                <!---<select name="BLACKOUTSTARTHOUR_TI" id="BLACKOUTSTARTHOUR_TI" size="1" style="width:90px;" title="Calls in queue will stop going out when this hour starts and will either carry over until next day or restart after Blackout End hour is over. Can be the same as Blackout End Hour."> 
                    <option value="0" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# LTE 0>selected="selected"</cfif>>NONE</option>
					<cfif Session.AfterHours GT 0>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #BLACKOUTSTARTHOUR_TI# EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTSTARTHOUR_TI# EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
                </select> --->
            </div>
     
            <div style="float:right; min-width:245px;">
                <label>Blackout End Hour</label>
                <div id="BLACKOUTENDHOUR_TI" class="" type="text" name="BLACKOUTENDHOUR_TI"></div>
               <!--- <select name="BLACKOUTENDHOUR_TI" id="BLACKOUTENDHOUR_TI" size="1" style="width:90px;" title="Calls in queue will continue to go out after this hour is over. Can be the same as Blackout Start Hour."> 
                   <option value="0" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# LTE 0>selected="selected"</cfif>>NONE</option>
				   <cfif Session.AfterHours GT 0>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #BLACKOUTENDHOUR_TI# EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #BLACKOUTENDHOUR_TI# EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
                </select>  --->
            </div>                          
        
        </div>                
       
       
       <div style="display:block; clear:both; width:550px; padding:5px; height: 65px; min-height:65px;">
           
           	<label>Select Days of Weeks to Run</label>
            <div id="DayOfWeekSelection" style="display:block; float:left; width:550px; margin: 5px 0 5px 0; padding:5px;" >
                      
                    <input TYPE="checkbox" name="SUNDAY_TI" id="SUNDAY_TI" class="checkboxInline" value="1" style="display:inline;" <cfif #SUNDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Sunday</label>
                              
                    <input TYPE="checkbox" name="MONDAY_TI" id="MONDAY_TI" class="checkboxInline" style="display:inline;" value="1" <cfif #MONDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Monday</label>
                                  
                    <input TYPE="checkbox" name="TUESDAY_TI" id="TUESDAY_TI" class="checkboxInline" style="display:inline;" value="1" <cfif #TUESDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Tuesday</label>
                             
                    <input TYPE="checkbox" name="WEDNESDAY_TI" id="WEDNESDAY_TI" class="checkboxInline"  style="display:inline;" value="1" <cfif #WEDNESDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked."  style="display:inline; padding-right:8px;">Wednesday</label>
                   
                    <input TYPE="checkbox" name="THURSDAY_TI" id="THURSDAY_TI" class="checkboxInline" style="display:inline;" value="1" <cfif #THURSDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Thursday</label>
                    
                    <input TYPE="checkbox" name="FRIDAY_TI" id="FRIDAY_TI" class="checkboxInline" style="display:inline;" value="1" <cfif #FRIDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Friday</label>
                                 
                    <input TYPE="checkbox" name="SATURDAY_TI" id="SATURDAY_TI" class="checkboxInline" style="display:inline;" value="1" <cfif #SATURDAY_TI# gt 0>checked</cfif> title="Queued calls will be dialed on any day of the week that is checked."/> 
                    <label title="Queued calls will be dialed on any day of the week that is checked." style="display:inline; padding-right:8px;">Saturday</label>
                         
            </div>

        </div>     
               
        <cfif inpShowPauseOption GT 0>
        	<!--- Display as a pause option - value is inverted = check needs to by set to off and unchecked to on--->
            <div style="display:block; clear:both; margin: 5px 0 5px 5px; width:300px; text-align:left;" >                   
                <input TYPE="checkbox" name="ENABLED_TI" id="ENABLED_TI" value="1" class="checkboxInline" style="display:inline;" <cfif #ENABLED_TI# gt 0>checked</cfif> /> 
                <label style="display:inline;">Pause</label><span class="small" style="display:inline;">Pauses a running batch of calls still left in the queue. Calls will remain in queue and may be restarted until call end date.</span>
            </div>
        <cfelse>
        	<!--- On by default --->
            <div style="display:block; clear:both; margin: 5px 0 5px 5px; width:300px; text-align:left;" >  
	        	<input TYPE="hidden" name="ENABLED_TI" id="ENABLED_TI" value="1" />
            </div>    
        </cfif>
        
       
	
    
</cfoutput>    