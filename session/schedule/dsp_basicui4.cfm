
<cfparam name="SUNDAY_TI" TYPE="string" default="1">
<cfparam name="MONDAY_TI" TYPE="string" default="1">
<cfparam name="TUESDAY_TI" TYPE="string" default="1">
<cfparam name="WEDNESDAY_TI" TYPE="string" default="1">
<cfparam name="THURSDAY_TI" TYPE="string" default="1">
<cfparam name="FRIDAY_TI" TYPE="string" default="1">
<cfparam name="SATURDAY_TI" TYPE="string" default="1">

<cfparam name="STARTHOUR_TI" TYPE="string" default="9">
<cfparam name="ENDHOUR_TI" TYPE="string" default="17">
<cfparam name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#">
<cfparam name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#">
<cfparam name="LOOPLIMIT_INT" TYPE="string" default="200">

<cfoutput>

	        
    <div style="clear:both; width:500px; padding:5px; height: 195px; min-height:195px;">
           
        <div style="float:left; min-width:245px; height: 195px; min-height:195px;">
            <label title="Calander date queued calls are eligible to start going out.">Start Date</label>
            <div TYPE="text" name="START_DT" id="START_DT" value="#LSDateFormat(START_DT, 'yyyy-mm-dd')#" class="ui-corner-all" style="width:145px;" title="Calander date queued calls are eligible to start going out."/>
        </div>
        
        <div style="float:left; min-width:245px; height: 195px; min-height:195px;"> 
            <label title="Calander date queued calls are stopped from going out. Calls not completed by this date will be removed from the call queue.">Stop Date</label>
            <div TYPE="text" name="STOP_DT" id="STOP_DT" value="#LSDateFormat(STOP_DT, 'yyyy-mm-dd')#" class="ui-corner-all" style="width:145px;" title="Calander date queued calls are stopped from going out. Calls not completed by this date will be removed from the call queue."/> 
        </div>    
   
    </div>
    
    <div  id="HourOptions" style="clear:both; width:500px; padding:5px; height: 185px; min-height:185px;" >		
        <div style="float:left; min-width:245px;">

		 <label style="display:block; width:130px; text-align:left; margin:0px 0 2px 5px;" title="Calls will not start until the time LOCALOUTPUT to the phone number is the start time or later.">Start Time</label>         
         <div id="STARTHOUR_TI" class="" type="text" name="STARTHOUR_TI"></div>
           
        </div>
                    
        <div style="float:right; min-width:245px;"> 
            <!---<label title="Calls still in queue after this time will carry over until tommorow's start hour.">End Hour</label>--->
                        
            <label title="Calls still in queue after this time will carry over until tommorow's start time.">End Time</label>            
            <div id="ENDHOUR_TI" class="" type="text" name="ENDHOUR_TI"></div>
                      
        </div>    
    </div>
    
</cfoutput>    