
<cfparam name="INPBATCHID" default="0">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>
	
<script TYPE="text/javascript">


	$(function() {	
	
	   $("#loadingDlgUpdateSchedule").hide();			
		
		<!--- Kill the new dialog --->
		$("#SetScheduleSBDiv #Cancel").click( function() 
		{
			$("#loadingDlgUpdateSchedule").hide();					
			CreateScheduleSBDialog.remove(); 
			return false;
		}); 	
		
		
		$("input[name='inpScheduleType']").change(function(){
			
			// console.log($("input[name='LineTerminator']:checked").val());
			
			if ($("input[name='inpScheduleType']:checked").val() == '0')
			{
				
				
			<!---	$("#htabs_DynamicSchedules").tabs( "remove", 0 );
				$("#htabs_DynamicSchedules").tabs( "remove", 1 );
				$("#htabs_DynamicSchedules").tabs( "remove", 2 );
				$("#htabs_DynamicSchedules").tabs( "remove", 3 );
				$("#htabs_DynamicSchedules").tabs( "remove", 4 );
				$("#htabs_DynamicSchedules").tabs( "remove", 5 );
				$("#htabs_DynamicSchedules").tabs( "remove", 6 );--->
							
							
				$("#htabs_DynamicSchedules .DSSunday").hide();
				$("#htabs_DynamicSchedules .DSMonday").hide();
				$("#htabs_DynamicSchedules .DSTuesday").hide();
				$("#htabs_DynamicSchedules .DSWednesday").hide();
				$("#htabs_DynamicSchedules .DSThursday").hide();
				$("#htabs_DynamicSchedules .DSFriday").hide();
				$("#htabs_DynamicSchedules .DSSaturday").hide();
							
							
				$("#htabs_DynamicSchedules .DSSingle").show();
				$("#htabs_DynamicSchedules").tabs( "select", 0);
				
		
			
			}
			else
			{							
				$("#htabs_DynamicSchedules .DSSingle").hide();
				
				$("#htabs_DynamicSchedules .DSSunday").show();
				$("#htabs_DynamicSchedules").tabs( "select", 1);
				$("#htabs_DynamicSchedules .DSMonday").show();
				$("#htabs_DynamicSchedules .DSTuesday").show();
				$("#htabs_DynamicSchedules .DSWednesday").show();
				$("#htabs_DynamicSchedules .DSThursday").show();
				$("#htabs_DynamicSchedules .DSFriday").show();
				$("#htabs_DynamicSchedules .DSSaturday").show();
							
				
			<!---	$("#htabs_DynamicSchedules").tabs( "remove", 0 );
				
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=1&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Sunday", 0 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=2&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Monday", 1 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=3&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Tuesday", 2 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=4&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Wednesday", 3 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=5&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Thursday", 4 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=6&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Friday", 5 );
				$("#htabs_DynamicSchedules").tabs( "add", "../Schedule/dsp_Schedule5?INPDSDOW=7&INPBATCHID=<cfoutput>#INPBATCHID#</cfoutput>", "Saturday", 6 );
			--->
			
			
			}
		});
			
		
		<!--- Open based on current schedule type --->
		if(1==1)
		{
			$('[name="inpScheduleType"]').filter('[value="0"]').attr("checked","checked");
			
			$("#htabs_DynamicSchedules .DSSunday").hide();
				$("#htabs_DynamicSchedules .DSMonday").hide();
				$("#htabs_DynamicSchedules .DSTuesday").hide();
				$("#htabs_DynamicSchedules .DSWednesday").hide();
				$("#htabs_DynamicSchedules .DSThursday").hide();
				$("#htabs_DynamicSchedules .DSFriday").hide();
				$("#htabs_DynamicSchedules .DSSaturday").hide();
							
							
				$("#htabs_DynamicSchedules .DSSingle").show();
								
		
		}
		
			
		<!--- Kill the new dialog --->
		$("#SetScheduleSBDiv #Cancel").click( function() 
		{				
			$("#loadingDlgUpdateSchedule").hide();					
			CreateScheduleSBDialog.remove(); 
			return false;
		}); 	
		
		
	});
		
	
</script>

<style>

#SetScheduleSBDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 475px;
	height: 475px;
	font-size:12px;
}


#SetScheduleSBDiv #LeftMenu
{
	width:280px;
	min-width:280px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:30px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#SetScheduleSBDiv #RightStage

{
	position:absolute;
	top:0;
	left:320px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#SetScheduleSBDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}


.ui-datepicker {
	border-radius: 20px 20px 0px 0px;		
}


.ui-timepicker-table {
	border-radius: 20px 20px 0px 0px;	
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	background-size: 100% 24px;
	background-repeat:no-repeat;
	border: none;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.5);
	padding:5px;
}

.ui-timepicker-table .ui-timepicker-hour-cell{
	margin:3px;
}
.ui-timepicker-table .ui-timepicker-title {
	border-radius: 20px 20px 0px 0px;
	width:100%;
	border: none;
	background:none;
}
</style> 


<!--- .ui-timepicker-div .ui-datepicker .ui-state-default --->

<div id='SetScheduleSBDiv' class="RXFormXXX" style="width:300px;">

 		<div id="LeftMenu">
        
            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Campaign Schedule Options</h1></div>
    
            <div width="100%" align="center" style="margin-bottom:20px;"><img class = "batch-file-icon_web" src="../../public/images/dock/blank.gif" /></div>
    
            <div style="margin: 10px 5px 10px 20px;">
                <ul>
                    <li>Choose your schedule options.</li>
                    <li>Press "Save" button when finished.</li>
                    <li>Press "Cancel" button to exit without saving changes.</li>               
                </ul>   
                
                <BR />
                <BR />
                <i>DEFINITION:</i> A Campaign is a logical content container for managing single or multi-channel communications.
                <BR />
                <BR />
                <i>DEFINITION:</i> Schedule options provides boundaries for when fulfillment of a campaign is OK to run.
                       
            </div> 
 
 		</div>
        
        <div id="RightStage">
                
            <div style="min-height:25px; height:25px;">
                <div style="float:left;">
                    <input id="inpScheduleType" class="ui-corner-all" type="radio" style="display:inline; float:left;" value="0" name="inpScheduleType">
                    <label style="width:150px; text-align:left; display:inline; padding-left:5px;">Single</label>
                </div>
                
                <div style="float:left; margin-left:30px;">
                    <input id="inpScheduleType" class="ui-corner-all" type="radio" style="display:inline; float:left;" value="1" name="inpScheduleType" checked="checked">
                    <label style="width:150px; text-align:left; display:inline; padding-left:5px;">Dynamic</label>
                </div>
			</div>
            
			<div>
    			<cfinclude template="..\account\HorizontalTabs\dsp_DynamicSchedules.cfm">
    		</div>
    <!---    	
            <div>
        		<button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>    
            </div>
                        --->
     	</div>       
        
        
</div>



                        