
<cfparam name="INPBATCHID" default="0">
	
<script TYPE="text/javascript">


	$(function() {
		$('#STOP_DT').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'yy-mm-dd'
		});
				
	
		$('#STARTHOUR_TI').timepicker({	
			showPeriod: false,   
	 	    hours: {
        				starts: 0,               
						ends: 23                 
					},
			minutes: {
						starts: 0,                
						ends: 55,   
						interval: 5 
					},
			rows: 6
		});		
					
		$('#ENDHOUR_TI').timepicker({			
			showPeriod: false,   
	 	    hours: {
        				starts: 0,               
						ends: 23                 
					},
			minutes: {
						starts: 0,                
						ends: 55,   
						interval: 5 
					},
			rows: 6

		});		
			
			
		$('#BLACKOUTSTARTHOUR_TI').timepicker({	
				showPeriod: false, 
				showMinutes: false,  
				hours: {
							starts: 0,               
							ends: 23                 
						},
				
				rows: 4
		});		
					
		$('#BLACKOUTENDHOUR_TI').timepicker({			
			  showPeriod: false,   
			  showMinutes: false,
			  hours: {
							starts: 0,               
							ends: 23                 
						},
			  rows: 4
		});		
			
		
		$('#START_DT').hide();
		$('#STOP_DT').hide();	
		$('#STARTHOUR_TI').hide();
		$('#ENDHOUR_TI').hide();
		$('#BLACKOUTSTARTHOUR_TI').hide();
		$('#BLACKOUTENDHOUR_TI').hide();
		
				
		$("#UpdateScheduleButton").click( function() { UpdateSchedule(); return false;  }); 
	
		$("#loadingDlgUpdateSchedule").hide();	
				
		GetCurrentSchedule();
		
		
		<!--- Kill the new dialog --->
		$("#SetScheduleSBDiv #Cancel").click( function() 
		{
			$("#loadingDlgUpdateSchedule").hide();					
			CreateScheduleSBDialog.remove(); 
			return false;
		}); 	
		  
		
	});
			
		
	function GetCurrentSchedule()
	{	
		
		$("#SetScheduleSBDiv #AJAXInfoOut").html("Retrieving Schedule");
		
		$("#loadingDlgUpdateSchedule").show();		
	
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#SetScheduleSBDiv #INPBATCHID").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								var CurrHour = 0;
								var CurrMinute = 0;
								
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#SetScheduleSBDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
								if(typeof(d.DATA.STARTHOUR_TI[0]) != "undefined")
									CurrHour = d.DATA.STARTHOUR_TI[0];									
									<!---$("#SetScheduleSBDiv #STARTHOUR_TI").val(d.DATA.STARTHOUR_TI[0]);--->
																	
								if(typeof(d.DATA.STARTMINUTE_TI[0]) != "undefined")
									CurrMinute = d.DATA.STARTMINUTE_TI[0];	
									<!---$("#SetScheduleSBDiv #STARTMINUTE_TI").val(d.DATA.STARTMINUTE_TI[0]);--->
																									
								$("#SetScheduleSBDiv #STARTHOUR_TI").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
												
								$('#STARTHOUR_TI').show();
								
								CurrHour = 0;
								CurrMinute = 0;
								
								if(typeof(d.DATA.ENDHOUR_TI[0]) != "undefined")
									CurrHour = d.DATA.ENDHOUR_TI[0];	
									<!---$("#SetScheduleSBDiv #ENDHOUR_TI").val(d.DATA.ENDHOUR_TI[0]);--->
								
								if(typeof(d.DATA.ENDMINUTE_TI[0]) != "undefined")
									CurrMinute = d.DATA.ENDMINUTE_TI[0];	
									<!---$("#SetScheduleSBDiv #ENDMINUTE_TI").val(d.DATA.ENDMINUTE_TI[0]);--->
								
								$("#SetScheduleSBDiv #ENDHOUR_TI").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
									
								$('#ENDHOUR_TI').show();
							
								if(typeof(d.DATA.ENABLED_TI[0]) != "undefined")
									if(d.DATA.ENABLED_TI[0])								
										$("#SetScheduleSBDiv #PAUSE_TI").attr('checked', false);  
									else
										$("#SetScheduleSBDiv #PAUSE_TI").attr('checked', true); 
									
								if(typeof(d.DATA.SUNDAY_TI[0]) != "undefined")
									if(d.DATA.SUNDAY_TI[0])								
										$("#SetScheduleSBDiv #SUNDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #SUNDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.MONDAY_TI[0]) != "undefined")
									if(d.DATA.MONDAY_TI[0])								
										$("#SetScheduleSBDiv #MONDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #MONDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.TUESDAY_TI[0]) != "undefined")
									if(d.DATA.TUESDAY_TI[0])								
										$("#SetScheduleSBDiv #TUESDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #TUESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.WEDNESDAY_TI[0]) != "undefined")
									if(d.DATA.WEDNESDAY_TI[0])								
										$("#SetScheduleSBDiv #WEDNESDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #WEDNESDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.THURSDAY_TI[0]) != "undefined")
									if(d.DATA.THURSDAY_TI[0])								
										$("#SetScheduleSBDiv #THURSDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #THURSDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.FRIDAY_TI[0]) != "undefined")
									if(d.DATA.FRIDAY_TI[0])								
										$("#SetScheduleSBDiv #FRIDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #FRIDAY_TI").attr('checked', false); 
										
								if(typeof(d.DATA.SATURDAY_TI[0]) != "undefined")
									if(d.DATA.SATURDAY_TI[0])								
										$("#SetScheduleSBDiv #SATURDAY_TI").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #SATURDAY_TI").attr('checked', false); 
																
								if(typeof(d.DATA.LOOPLIMIT_INT[0]) != "undefined")
									$("#SetScheduleSBDiv #LOOPLIMIT_INT").val(d.DATA.LOOPLIMIT_INT[0]);
								
								if(typeof(d.DATA.STOP_DT[0]) != "undefined")
								{
									// $("#SetScheduleSBDiv #STOP_DT").val(d.DATA.STOP_DT[0]);								
									$('#STOP_DT').datepicker("setDate" , d.DATA.STOP_DT[0]);								
								}
								$('#STOP_DT').show();
								
								if(typeof(d.DATA.START_DT[0]) != "undefined")									
								{
									$('#START_DT').datepicker("setDate" , d.DATA.START_DT[0]);
								}
								$('#START_DT').show();
								
									
								CurrHour = 0;
								CurrMinute = 0;
									
								if(typeof(d.DATA.BLACKOUTSTARTHOUR_TI[0]) != "undefined")
									CurrHour = d.DATA.BLACKOUTSTARTHOUR_TI[0];
									<!---$("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI").val(d.DATA.BLACKOUTSTARTHOUR_TI[0]);--->
								
								$("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
								$('#BLACKOUTSTARTHOUR_TI').show();
								
								CurrHour = 0;
								CurrMinute = 0;
								
								if(typeof(d.DATA.BLACKOUTENDHOUR_TI[0]) != "undefined")
									CurrHour = d.DATA.BLACKOUTENDHOUR_TI[0];
									<!---$("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI").val(d.DATA.BLACKOUTENDHOUR_TI[0]);--->
								
								$("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
								$('#BLACKOUTENDHOUR_TI').show();
								
								if(typeof(d.DATA.LASTUPDATED_DT[0]) != "undefined")
									$("#SetScheduleSBDiv #LASTUPDATED_DT").val(d.DATA.LASTUPDATED_DT[0]);				
											
								
								
													
			
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	
	
	function UpdateSchedule()
	{	
		var ENABLED_TI = 1;
				
		$("#loadingDlgUpdateSchedule").show();		
	
		if($("#SetScheduleSBDiv #ENABLED_TI").val() > 0)
			ENABLED_TI = 0
	
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#SetScheduleSBDiv #INPBATCHID").val(), SUNDAY_TI : $("#SetScheduleSBDiv #SUNDAY_TI").val(), MONDAY_TI : $("#SetScheduleSBDiv #MONDAY_TI").val(), TUESDAY_TI : $("#SetScheduleSBDiv #TUESDAY_TI").val(), WEDNESDAY_TI : $("#SetScheduleSBDiv #WEDNESDAY_TI").val(), THURSDAY_TI : $("#SetScheduleSBDiv #THURSDAY_TI").val(), FRIDAY_TI : $("#SetScheduleSBDiv #FRIDAY_TI").val(), SATURDAY_TI : $("#SetScheduleSBDiv #SATURDAY_TI").val(), LOOPLIMIT_INT : $("#SetScheduleSBDiv #LOOPLIMIT_INT").val(), START_DT : $("#SetScheduleSBDiv #START_DT").val(), STOP_DT : $("#SetScheduleSBDiv #STOP_DT").val(), STARTHOUR_TI : $("#SetScheduleSBDiv #STARTHOUR_TI").timepicker('getHour'), ENDHOUR_TI : $("#SetScheduleSBDiv #ENDHOUR_TI").timepicker('getHour'), STARTMINUTE_TI : $("#SetScheduleSBDiv #STARTHOUR_TI").timepicker('getMinute'), ENDMINUTE_TI : $("#SetScheduleSBDiv #ENDHOUR_TI").timepicker('getMinute'), BLACKOUTSTARTHOUR_TI : $("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI").timepicker('getHour'), BLACKOUTENDHOUR_TI : $("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI").timepicker('getHour'), ENABLED_TI : ENABLED_TI}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#SetScheduleSBDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
							}
							
							$("#loadingDlgUpdateSchedule").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule").hide();			
			
			} );		
	
		return false;
	}
	
</script>

<style>

#SetScheduleSBDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 475px;
	height: 475px;
	font-size:12px;
}


#SetScheduleSBDiv #LeftMenu
{
	width:280px;
	min-width:280px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:30px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#SetScheduleSBDiv #RightStage
{
	position:absolute;
	top:0;
	left:320px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#SetScheduleSBDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

<!---
.ui-datepicker-header .ui-widget-header { border-radius: 20px 20px 0px 0px;}
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; border-radius: 20px 20px 0px 0px;}
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }



.ui-slider-horizontal .ui-slider-handle
{
   background: none repeat scroll 0 0 #5F83B9;
    border-color: #5F83B9;
    color: #FFFFFF;
    font-weight: bold;
    text-shadow: 0 1px 1px #234386;
}--->




.ui-datepicker {
	border-radius: 20px 20px 0px 0px;		
}


.ui-timepicker-table {
	border-radius: 20px 20px 0px 0px;	
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	background-size: 100% 24px;
	background-repeat:no-repeat;
	border: none;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.5);

}

.ui-timepicker-table .ui-timepicker-title {
	border-radius: 20px 20px 0px 0px;
	width:100%;
	border: none;
	background:none;
}




<!---



.ui-timepicker-table td {
	 padding: 0em;
}

.ui-timepicker-table td .ui-timepicker-hours {
	 padding: 0em;
}


.ui-timepicker-table td {
	 padding: 0;
}


.ui-timepicker td {
	 padding: 0.1em;
}

.ui-timepicker-hours {
	 padding: 0;
}

.ui-timepicker-table .ui-timepicker-hours {
	 padding: 0;
}

--->




</style> 


<!--- .ui-timepicker-div .ui-datepicker .ui-state-default --->

<div id='SetScheduleSBDiv' class="RXFormXXX" style="width:300px;">

 		<div id="LeftMenu">
        
            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Campaign Schedule Options</h1></div>
    
            <div width="100%" align="center" style="margin-bottom:20px;"><img class = "batch-file-icon_web" src="../../public/images/dock/blank.gif" /></div>
    
            <div style="margin: 10px 5px 10px 20px;">
                <ul>
                    <li>Choose your schedule options.</li>
                    <li>Press "Update" button when finished.</li>
                    <li>Press "Cancel" button to exit without saving changes.</li>               
                </ul>   
                
                <BR />
                <BR />
                <i>DEFINITION:</i> A Campaign is a logical content container for managing single or multi-channel communications.
                <BR />
                <BR />
                <i>DEFINITION:</i> Schedule options provides boundaries for when fulfillment of a campaign is OK to run.
                       
            </div> 
 
 		</div>
        
        <div id="RightStage">

			<form name="ScheduleForm" id="ScheduleForm">

                <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
                       
                <div id="SetSchedule" style="width: 1200px; margin:0px 0 5px 0;">
                               
                    <cfinclude template="..\schedule\dsp_basicUI4.cfm">    
                             
                    <cfset inpShowPauseOption = 0>   
                    <div id="AdvancedOptions">  
                    <cfinclude template="..\schedule\dsp_advancedUI4.cfm">
                    </div> 
                </div>	    
                
                <button id="UpdateScheduleButton" TYPE="button" class="ui-corner-all">Update Schedule</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Exit</button>
            
            </form>
            
     	</div>       
            
</div>



            