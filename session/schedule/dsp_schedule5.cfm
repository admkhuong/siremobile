
<cfparam name="INPBATCHID" default="0">

<!--- DSDOW = DynamicScheduleDayOfWeek --->
<cfparam name="INPDSDOW" default="0">

	
<script TYPE="text/javascript">


	$(function() {
		$('#STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'yy-mm-dd'
		});
				
	
		$('#STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').timepicker({	
			showPeriod: false,   
	 	    hours: {
        				starts: 0,               
						ends: 23                 
					},
			minutes: {
						starts: 0,                
						ends: 55,   
						interval: 5 
					},
			rows: 6
		});		
					
		$('#ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').timepicker({			
			showPeriod: false,   
	 	    hours: {
        				starts: 0,               
						ends: 23                 
					},
			minutes: {
						starts: 0,                
						ends: 55,   
						interval: 5 
					},
			rows: 6

		});		
			
			
		$('#BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').timepicker({	
				showPeriod: false, 
				showMinutes: false,  
				hours: {
							starts: 0,               
							ends: 23                 
						},
				
				rows: 4
		});		
					
		$('#BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').timepicker({			
			  showPeriod: false,   
			  showMinutes: false,
			  hours: {
							starts: 0,               
							ends: 23                 
						},
			  rows: 4
		});		
			
		
		$('#START_DT_<cfoutput>#INPDSDOW#</cfoutput>').hide();
		$('#STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>').hide();	
		$('#STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').hide();
		$('#ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').hide();
		$('#BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').hide();
		$('#BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').hide();
		
				
		$("#UpdateScheduleButton_<cfoutput>#INPDSDOW#</cfoutput>").click( function() { UpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>(); return false;  }); 
	
		$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();	
				
		GetCurrentSchedule_<cfoutput>#INPDSDOW#</cfoutput>();
		
		
		<!--- Kill the new dialog --->
		$("#SetScheduleSBDiv #Cancel").click( function() 
		{
			$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();					
			CreateScheduleSBDialog.remove(); 
			return false;
		}); 	
		  
		
	});
			
		
	function GetCurrentSchedule_<cfoutput>#INPDSDOW#</cfoutput>()
	{	
		
		$("#SetScheduleSBDiv #AJAXInfoOut").html("Retrieving Schedule");
		
		$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#SetScheduleSBDiv #INPBATCHID").val(), INPDSDOW : <cfoutput>#INPDSDOW#</cfoutput>}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								
								var CurrHour = 9;
								var CurrMinute = 0;
								
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#SetScheduleSBDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
								if(typeof(d.DATA.STARTHOUR_TI[0]) != "undefined" && d.DATA.STARTHOUR_TI[0] != null)
									CurrHour = d.DATA.STARTHOUR_TI[0];									
																	
								if(typeof(d.DATA.STARTMINUTE_TI[0]) != "undefined" && d.DATA.STARTMINUTE_TI[0] != null)
									CurrMinute = d.DATA.STARTMINUTE_TI[0];	
																									
								$("#SetScheduleSBDiv #STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
												
								$('#STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').show();
								
								CurrHour = 20;
								CurrMinute = 0;
								
								if(typeof(d.DATA.ENDHOUR_TI[0]) != "undefined" && d.DATA.ENDHOUR_TI[0] != null)
									CurrHour = d.DATA.ENDHOUR_TI[0];	
								
								if(typeof(d.DATA.ENDMINUTE_TI[0]) != "undefined" && d.DATA.ENDMINUTE_TI[0] != null)
									CurrMinute = d.DATA.ENDMINUTE_TI[0];	
								
								$("#SetScheduleSBDiv #ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
									
								$('#ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').show();
							
								if(typeof(d.DATA.ENABLED_TI) != "undefined")
									if(d.DATA.ENABLED_TI[0])								
										$("#SetScheduleSBDiv #PAUSE_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false);  
									else
										$("#SetScheduleSBDiv #PAUSE_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true); 
									
								if(typeof(d.DATA.SUNDAY_TI[0]) != "undefined")
									if(d.DATA.SUNDAY_TI[0])								
										$("#SetScheduleSBDiv #SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.MONDAY_TI[0]) != "undefined")
									if(d.DATA.MONDAY_TI[0])								
										$("#SetScheduleSBDiv #MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.TUESDAY_TI[0]) != "undefined")
									if(d.DATA.TUESDAY_TI[0])								
										$("#SetScheduleSBDiv #TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.WEDNESDAY_TI[0]) != "undefined")
									if(d.DATA.WEDNESDAY_TI[0])								
										$("#SetScheduleSBDiv #WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.THURSDAY_TI[0]) != "undefined")
									if(d.DATA.THURSDAY_TI[0])								
										$("#SetScheduleSBDiv #THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.FRIDAY_TI[0]) != "undefined")
									if(d.DATA.FRIDAY_TI[0])								
										$("#SetScheduleSBDiv #FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
										
								if(typeof(d.DATA.SATURDAY_TI[0]) != "undefined")
									if(d.DATA.SATURDAY_TI[0])								
										$("#SetScheduleSBDiv #SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', true);  
									else
										$("#SetScheduleSBDiv #SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").attr('checked', false); 
																
								if(typeof(d.DATA.LOOPLIMIT_INT[0]) != "undefined" && d.DATA.LOOPLIMIT_INT[0] != null)
									$("#SetScheduleSBDiv #LOOPLIMIT_INT_<cfoutput>#INPDSDOW#</cfoutput>").val(d.DATA.LOOPLIMIT_INT[0]);
								else
									$("#SetScheduleSBDiv #LOOPLIMIT_INT_<cfoutput>#INPDSDOW#</cfoutput>").val(200);	
								
								if(typeof(d.DATA.STOP_DT[0]) != "undefined" && d.DATA.STOP_DT[0] != null)
								{																	
									$('#STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker("setDate" , d.DATA.STOP_DT[0]);								
								}
								else
								{
									$('#STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker("setDate" , "<cfoutput>#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#</cfoutput>");																		
								}
								
								$('#STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>').show();
								
								if(typeof(d.DATA.START_DT) != "undefined" && d.DATA.START_DT[0] != null)									
								{
									$('#START_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker("setDate" , d.DATA.START_DT[0]);
								}
								else
								{
									$('#START_DT_<cfoutput>#INPDSDOW#</cfoutput>').datepicker("setDate" ,"<cfoutput>#LSDateFormat(NOW(), 'yyyy-mm-dd')#</cfoutput>");
									
								}
								
								$('#START_DT_<cfoutput>#INPDSDOW#</cfoutput>').show();
								
									
								CurrHour = 0;
								CurrMinute = 0;
									
								if(typeof(d.DATA.BLACKOUTSTARTHOUR_TI[0]) != "undefined" && d.DATA.BLACKOUTSTARTHOUR_TI[0] != null)
									CurrHour = d.DATA.BLACKOUTSTARTHOUR_TI[0];
								
								$("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
								$('#BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').show();
								
								CurrHour = 0;
								CurrMinute = 0;
								
								if(typeof(d.DATA.BLACKOUTENDHOUR_TI) != "undefined" && d.DATA.BLACKOUTENDHOUR_TI[0] != null)
									CurrHour = d.DATA.BLACKOUTENDHOUR_TI[0];
								
								$("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('setTime','' + CurrHour + ':' + CurrMinute);
								$('#BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>').show();
								
								if(typeof(d.DATA.LASTUPDATED_DT[0]) != "undefined")
									$("#SetScheduleSBDiv #LASTUPDATED_DT_<cfoutput>#INPDSDOW#</cfoutput>").val(d.DATA.LASTUPDATED_DT[0]);				
											
							}
							
							$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();			
			
			} );		
	
		return false;
	}
	
	
	function UpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>()
	{	
	
		<!--- Enable is only applicable to single schedule now--->
		var ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput> = 1;
				
		$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").show();		
	
		if($("#SetScheduleSBDiv #ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>").val() > 0)
			ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput> = 1;
			
		if($("#SetScheduleSBDiv #ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked'))
			ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput> = 1;	
			

			
			STARTHOUR = $("#SetScheduleSBDiv #STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(0,2);
			ENDHOUR = $("#SetScheduleSBDiv #ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(0,2);
			STARTMINUTE = $("#SetScheduleSBDiv #STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(3, 5);
			ENDMINUTE = $("#SetScheduleSBDiv #ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(3,5);
			
			BLACKOUTSTARTHOUR = $("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(0,2);
			BLACKOUTENDHOUR = $("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").val().substring(0,2);

			
			
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
		{ 
			INPBATCHID : $("#SetScheduleSBDiv #INPBATCHID").val(), 
			SUNDAY_TI : $("#SetScheduleSBDiv #SUNDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			MONDAY_TI : $("#SetScheduleSBDiv #MONDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			TUESDAY_TI : $("#SetScheduleSBDiv #TUESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			WEDNESDAY_TI : $("#SetScheduleSBDiv #WEDNESDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			THURSDAY_TI : $("#SetScheduleSBDiv #THURSDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			FRIDAY_TI : $("#SetScheduleSBDiv #FRIDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0 , 
			SATURDAY_TI : $("#SetScheduleSBDiv #SATURDAY_TI_<cfoutput>#INPDSDOW#</cfoutput>").is(':checked')?1:0, 
			LOOPLIMIT_INT : $("#SetScheduleSBDiv #LOOPLIMIT_INT_<cfoutput>#INPDSDOW#</cfoutput>").val(), 
			START_DT : $("#SetScheduleSBDiv #START_DT_<cfoutput>#INPDSDOW#</cfoutput>").val(), 
			STOP_DT : $("#SetScheduleSBDiv #STOP_DT_<cfoutput>#INPDSDOW#</cfoutput>").val(), 
			<!--- STARTHOUR_TI : $("#SetScheduleSBDiv #STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getHour'), 
			ENDHOUR_TI : $("#SetScheduleSBDiv #ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getHour'),
			STARTMINUTE_TI : $("#SetScheduleSBDiv #STARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getMinute'), 
			ENDMINUTE_TI : $("#SetScheduleSBDiv #ENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getMinute'), 
			BLACKOUTSTARTHOUR_TI : $("#SetScheduleSBDiv #BLACKOUTSTARTHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getHour'), 
			BLACKOUTENDHOUR_TI : $("#SetScheduleSBDiv #BLACKOUTENDHOUR_TI_<cfoutput>#INPDSDOW#</cfoutput>").timepicker('getHour'), 
			--->
			STARTHOUR_TI : STARTHOUR,
			ENDHOUR_TI : ENDHOUR,
			STARTMINUTE_TI : STARTMINUTE,
			ENDMINUTE_TI : ENDMINUTE,
			BLACKOUTSTARTHOUR_TI : BLACKOUTSTARTHOUR,
			BLACKOUTENDHOUR_TI : BLACKOUTENDHOUR,
			ENABLED_TI : ENABLED_TI_<cfoutput>#INPDSDOW#</cfoutput>, 
			INPDSDOW : <cfoutput>#INPDSDOW#</cfoutput>
		}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->												
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#SetScheduleSBDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
									
									
									<!--- jAlert("Ok.", "Ok Ok"); --->
								$.jGrowl("Schedule has been updated successfully .", 
						            { 
						             life:3000, 
						             position:"center",
						             header:'Success Message'<!--- ,
						             beforeClose:function(e,m,o){
						                alert("1234");
						             } --->
						            });	
									
							}
							
							$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgUpdateSchedule_<cfoutput>#INPDSDOW#</cfoutput>").hide();			
			
			} );		
	
		return false;
	}
	
</script>

<style>

</style> 


<!--- .ui-timepicker-div .ui-datepicker .ui-state-default --->


        
        <div id="XXXXXX">

			<form name="ScheduleForm" id="ScheduleForm">

                <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
                       
                <div id="SetSchedule" style="width: 1200px; margin:0px 0 5px 0;">
                               
                    <cfinclude template="..\schedule\dsp_basicUI5.cfm">    
                             
                    <cfset inpShowPauseOption = 0>   
                    <div id="AdvancedOptions">  
                    <cfinclude template="..\schedule\dsp_advancedUI5.cfm">
                    </div> 
                </div>	    
                
                <button id="UpdateScheduleButton_<cfoutput>#INPDSDOW#</cfoutput>" TYPE="button" class="ui-corner-all">Save</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel/Exit</button> 
                            
            </form>
            
     	</div>       
            



            