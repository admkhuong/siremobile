<cfset PageTitle = "Schedule Detail">

<script language="javascript">
	document.title = "<cfoutput>#PageTitle#</cfoutput>";
</script> 


<cfparam name="INPBATCHID" default="101694">

<cfif Session.USERID EQ "">Session Expired! Refresh page after logging back in.<cfabort></cfif>

<!--- validate current user has rights to modify current batch ID - no hacks --->
<cfquery name="ValidateBatchId" datasource="#Session.DBSourceEBM#">
    SELECT
        Count(*) AS TOTALCOUNT          
    FROM
        RX..RX_Batch (NOLOCK)
    WHERE
        UserId_int = #Session.USERID#    
        AND BatchId_num = #INPBATCHID#    
</cfquery>

<cfif ValidateBatchId.TOTALCOUNT EQ 0>
	BatchId does not belong to currently logged in User! Aborting now...<cfabort>
</cfif>

<cfquery name="GetScheduleOptions" datasource="#Session.DBSourceEBM#">
    SELECT
        ENABLED_TI,
        STARTHOUR_TI,
        ENDHOUR_TI, 
        SUNDAY_TI,
        MONDAY_TI,
        TUESDAY_TI,
        WEDNESDAY_TI,
        THURSDAY_TI,
        FRIDAY_TI,
        SATURDAY_TI,
        LOOPLIMIT_INT,
        STOP_DT,
        START_DT,
        STARTMINUTE_TI,
        ENDMINUTE_TI,
        BLACKOUTSTARTHOUR_TI,
        BLACKOUTENDHOUR_TI,
        LASTUPDATED_DT           
    FROM 
    	CallControl..ScheduleOptions (NOLOCK) 
    WHERE 
        BatchId_bi = #INPBATCHID#   
</cfquery>


<!---
<cfparam name="GetScheduleOptions.SUNDAY_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.MONDAY_TI" TYPE="string" default="1">
<cfparam name="GetScheduleOptions.TUESDAY_TI" TYPE="string" default="1">
<cfparam name="GetScheduleOptions.WEDNESDAY_TI" TYPE="string" default="1">
<cfparam name="GetScheduleOptions.THURSDAY_TI" TYPE="string" default="1">
<cfparam name="GetScheduleOptions.FRIDAY_TI" TYPE="string" default="1">
<cfparam name="GetScheduleOptions.SATURDAY_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.LOOPLIMIT_INT" TYPE="string" default="200">
<cfparam name="GetScheduleOptions.START_DT" TYPE="string" default="2000-01-01">
<cfparam name="GetScheduleOptions.STOP_DT" TYPE="string" default="2001-01-01">
<cfparam name="GetScheduleOptions.STARTHOUR_TI" TYPE="string" default="9">
<cfparam name="GetScheduleOptions.ENDHOUR_TI" TYPE="string" default="17">
<cfparam name="GetScheduleOptions.STARTMINUTE_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.ENDMINUTE_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.BLACKOUTSTARTHOUR_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.BLACKOUTENDHOUR_TI" TYPE="string" default="0">
<cfparam name="GetScheduleOptions.ENABLED_TI" TYPE="string" default="0"> 
--->





<!--- 
  `BatchId_bi` BIGINT(20) NOT NULL DEFAULT '0',
  `ENABLED_TI` TINYINT(4) DEFAULT '0',
  `STARTHOUR_TI` TINYINT(3) UNSIGNED DEFAULT '9',
  `ENDHOUR_TI` TINYINT(3) UNSIGNED DEFAULT '17',
  `STARTMINUTE_TI` TINYINT(3) NOT NULL DEFAULT '0',
  `ENDMINUTE_TI` TINYINT(3) NOT NULL DEFAULT '0',
  `BLACKOUTSTARTHOUR_TI` TINYINT(4) DEFAULT '0',
  `BLACKOUTENDHOUR_TI` TINYINT(4) DEFAULT '0',
  `SUNDAY_TI` TINYINT(4) DEFAULT '0',
  `MONDAY_TI` TINYINT(4) DEFAULT '0',
  `TUESDAY_TI` TINYINT(4) DEFAULT '0',
  `WEDNESDAY_TI` TINYINT(4) DEFAULT '0',
  `THURSDAY_TI` TINYINT(4) DEFAULT '0',
  `FRIDAY_TI` TINYINT(4) DEFAULT '0',
  `SATURDAY_TI` TINYINT(4) DEFAULT '0',
  `LOOPLIMIT_INT` INTEGER(10) UNSIGNED DEFAULT '100',
  `LastSent_dt` DATETIME DEFAULT NULL,
  `STOP_DT` DATETIME DEFAULT NULL,
  `START_DT` DATETIME DEFAULT NULL,
  `LASTUPDATED_DT` DATETIME DEFAULT NULL,
 --->

	
<script TYPE="text/javascript">


	$(function() {
		$('#STOP_DT').datepicker({
			numberOfMonths: 3,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT').datepicker({
			numberOfMonths: 3,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
	
		$("#UpdateScheduleButton").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } ); 
			
		$("#UpdateScheduleButton").click( function() { UpdateSchedule(); return false;  }); 
		
	});
	
	
	
		
	function UpdateSchedule()
	{	
		
		$("#loading").show();		
	
		$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : $("#ScheduleFormDiv #INPBATCHID").val(), SUNDAY_TI : $("#ScheduleFormDiv #SUNDAY_TI").val(), MONDAY_TI : $("#ScheduleFormDiv #MONDAY_TI").val(), TUESDAY_TI : $("#ScheduleFormDiv #TUESDAY_TI").val(), WEDNESDAY_TI : $("#ScheduleFormDiv #WEDNESDAY_TI").val(), THURSDAY_TI : $("#ScheduleFormDiv #THURSDAY_TI").val(), FRIDAY_TI : $("#ScheduleFormDiv #FRIDAY_TI").val(), SATURDAY_TI : $("#ScheduleFormDiv #SATURDAY_TI").val(), LOOPLIMIT_INT : $("#ScheduleFormDiv #LOOPLIMIT_INT").val(), START_DT : $("#ScheduleFormDiv #START_DT").val(), STOP_DT : $("#ScheduleFormDiv #STOP_DT").val(), STARTHOUR_TI : $("#ScheduleFormDiv #STARTHOUR_TI").val(), ENDHOUR_TI : $("#ScheduleFormDiv #ENDHOUR_TI").val(), STARTMINUTE_TI : $("#ScheduleFormDiv #STARTMINUTE_TI").val(), ENDMINUTE_TI : $("#ScheduleFormDiv #ENDMINUTE_TI").val(), BLACKOUTSTARTHOUR_TI : $("#ScheduleFormDiv #BLACKOUTSTARTHOUR_TI").val(), BLACKOUTENDHOUR_TI : $("#ScheduleFormDiv #BLACKOUTENDHOUR_TI").val(), ENABLED_TI : $("#ScheduleFormDiv #ENABLED_TI").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#ScheduleFormDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
							}
							
							$("#loading").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loading").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					<!--- show LOCALOUTPUT menu options - re-show regardless of success or failure --->
				<!--- 	$("#EditMCIDForm_" + inpQID + " #RXEditSubMenu_" + inpQID).show(); --->
					
					<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
					<!--- $("#EditMCIDForm_" + inpQID + " #RXEditSubMenuWait").html(''); --->
			} );		
	
		return false;
	}
	
</script>



<style>

<!--- 

#CCDSummaryDiv{
margin:0 5;
width:950px;
padding:5px;
border:solid 2px #aaaaaa;
border-top:dashed 1px #aaaaaa;
background-image: url(../css/pepper-grinder/images/ui-bg_fine-grain_15_eceadf_60x60.png)
} --->


/* ----------- stylized ----------- */
#ScheduleFormDiv{

margin:0 5;
width:950px;
padding:5px;
height:800px;
}


#ScheduleFormDiv p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #aaaaaa;
padding-bottom:10px;
}

#ScheduleFormDiv label{
display:block;
font-weight:bold;
text-align:right;
width:400px;
float:left;
}

#ScheduleFormDiv .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:400px;
}

#ScheduleFormDiv input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
width:200px;
margin:2px 0 20px 10px;
display:block;
}

<!--- #ScheduleFormDiv button{
border-style:hidden;
padding:5; 
}
--->

</style>

<cfoutput>
  
    <cfquery name="GetBatchInfo" datasource="#Session.DBSourceEBM#">
        SELECT 
        	LEFT(b.DESC_VCH, 255) AS BatchDesc, 
            b.BatchId_num              		 	 
        FROM            
        	RX..RX_Batch b (NOLOCK)       
        WHERE 
        	b.BatchId_num = #INPBATCHID# 
    </cfquery>



    <div id="BatchDescDiv" class="BatchDescDiv">	           
        <span class="small">Schedule Description Document</span> 
        <h1 title="#INPBATCHID#">Campaign Name: <span id="MainBatchDesc" style="text-decoration:underline; min-height:25px;">#GetBatchInfo.BatchDesc#</span></h1>      
    </div>
    
    
<div id="ScheduleFormDiv" class="ScheduleFormDiv">
<form name="ScheduleForm" id="ScheduleForm">

<!--- Dont use any single quotes so can be included in javascript --->
  
    <p id="rxtDesc">Statement</p>
                
    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
      
    <label>Enabled
    <span class="small">Allows calls to go out</span>
    </label>
	<input TYPE="checkbox" name="ENABLED_TI" id="ENABLED_TI" value="1" class="ui-corner-all" checked="checked"/> 
    
         
    <!--- Validate all numerics 0-9 --->
    <label>STARTHOUR_TI
    <span class="small">STARTHOUR_TI</span>
    </label>
	<input TYPE="text" name="STARTHOUR_TI" id="STARTHOUR_TI" value="#GetScheduleOptions.STARTHOUR_TI#" class="ui-corner-all" /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>ENDHOUR_TI
    <span class="small">ENDHOUR_TI</span>
    </label>
	<input TYPE="text" name="ENDHOUR_TI" id="ENDHOUR_TI" value="#GetScheduleOptions.ENDHOUR_TI#" class="ui-corner-all" /> 
    
     <!--- Validate all numerics 0-9 --->
    <label>STARTMINUTE_TI
    <span class="small">STARTMINUTE_TI</span>
    </label>
	<input TYPE="text" name="STARTMINUTE_TI" id="STARTMINUTE_TI" value="#GetScheduleOptions.STARTMINUTE_TI#" class="ui-corner-all" /> 
    
     <!--- Validate all numerics 0-9 --->
    <label>ENDMINUTE_TI
    <span class="small">ENDMINUTE_TI</span>
    </label>
	<input TYPE="text" name="ENDMINUTE_TI" id="ENDMINUTE_TI" value="#GetScheduleOptions.ENDMINUTE_TI#" class="ui-corner-all" /> 
    
     <!--- Validate all numerics 0-9 --->
    <label>BLACKOUTSTARTHOUR_TI
    <span class="small">BLACKOUTSTARTHOUR_TI</span>
    </label>
	<input TYPE="text" name="BLACKOUTSTARTHOUR_TI" id="BLACKOUTSTARTHOUR_TI" value="#GetScheduleOptions.BLACKOUTSTARTHOUR_TI#" class="ui-corner-all" /> 
    
     <!--- Validate all numerics 0-9 --->
    <label>BLACKOUTENDHOUR_TI
    <span class="small">BLACKOUTENDHOUR_TI</span>
    </label>
	<input TYPE="text" name="BLACKOUTENDHOUR_TI" id="BLACKOUTENDHOUR_TI" value="#GetScheduleOptions.BLACKOUTENDHOUR_TI#" class="ui-corner-all" /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>SUNDAY_TI
    <span class="small">SUNDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="SUNDAY_TI" id="SUNDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.SUNDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>MONDAY_TI
    <span class="small">MONDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="MONDAY_TI" id="MONDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.MONDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>TUESDAY_TI
    <span class="small">TUESDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="TUESDAY_TI" id="TUESDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.TUESDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>WEDNESDAY_TI
    <span class="small">WEDNESDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="WEDNESDAY_TI" id="WEDNESDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.WEDNESDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>THURSDAY_TI
    <span class="small">THURSDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="THURSDAY_TI" id="THURSDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.THURSDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>FRIDAY_TI
    <span class="small">FRIDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="FRIDAY_TI" id="FRIDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.FRIDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>SATURDAY_TI
    <span class="small">SATURDAY_TI</span>
    </label>
	<input TYPE="checkbox" name="SATURDAY_TI" id="SATURDAY_TI" class="ui-corner-all" value="1"  <cfif #GetScheduleOptions.SATURDAY_TI# gt 0>checked</cfif> /> 
    
    <!--- Validate all numerics 0-9 --->
    <label>LOOPLIMIT_INT
    <span class="small">LOOPLIMIT_INT</span>
    </label>
	<input TYPE="text" name="LOOPLIMIT_INT" id="LOOPLIMIT_INT" value="#GetScheduleOptions.LOOPLIMIT_INT#" class="ui-corner-all" /> 
   
    <label>START_DT
    <span class="small">START_DT</span>
    </label>
	<input TYPE="text" name="START_DT" id="START_DT" value="#LSDateFormat(GetScheduleOptions.START_DT, 'yyyy-mm-dd')#" class="ui-corner-all" /> 
     
     <!--- Validate all numerics 0-9 --->
    <label>STOP_DT
    <span class="small">STOP_DT</span>
    </label>
	<input TYPE="text" name="STOP_DT" id="STOP_DT" value="#LSDateFormat(GetScheduleOptions.STOP_DT, 'yyyy-mm-dd')#" class="ui-corner-all" /> 
    
   
          
    
    <div class="spacer"></div>
            
    <!--- Used for debugging --->        
    <div id="AJAXInfoOut" style="font-size:10px;"></div>
    
       
     
    </form> 
       
</div>

<div>

  <button id="UpdateScheduleButton" TYPE="button" class="ui-state-default ui-corner-all">Update Schedule</button><span class="Desc">Save Changes</span><BR>
 </div> 
  
</cfoutput>































