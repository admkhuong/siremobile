

<cfparam name="inpLibId" default="0">
<cfparam name="inpEleId" default="0">
<cfparam name="inpDataId" default="0">

<!--- Set here only so if changes can be quickly updated --->
<!---<!---<cfinclude template="data_ScriptPaths.cfm">--->--->

<cfset r = structNew()>
<cfset r["msg"] = "">
<cfset r["error"] = "">

<!--- Is there a file --->
<cfif structKeyExists(form, "inpFileName") and len(form.inpFileName)>
	<cffile action="upload" 
    	filefield="inpFileName" 
        destination="#rxdsWebProcessingPath#\CurrentUpload.dat" 
        nameconflict="makeunique"
        result = "CurrUpload"> 
	
	
	
	<cfoutput>
    
    	<cftry> 
		
        	<cfset r["error"] = "Unhandled Error in File Upload">
        
        	<!--- Create\Validate path in DB --->
            
            
            <!--- Does user exist? Auto add/repair system directory structure--->
            <!--- Only works with current session user --->
            <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#")><cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#"></cfif>
            
            <!--- Does Lib Exist? --->
            <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                SELECT
                    DSID_int,
                    DESC_VCH                    
                FROM
                    rxds.DynamicScript
                WHERE
                    Active_int = 1
                    AND DSID_int = #inpLibId#
                    AND UserId_int = #Session.USERID#
                ORDER BY
                    DESC_VCH ASC            
            </cfquery>      
                            
            
            <cfif GetLibraries.RecordCount EQ 0>
               <cfthrow MESSAGE="Can't find the Library specified" TYPE="Any" extendedinfo="" errorcode="-2">               
            <cfelse>
    	        <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#")><cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#"></cfif>    
            </cfif>               
            
            
            <!--- Does Ele Exist? --->
            <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                SELECT
                    DSID_int,
                    DSEId_int,
                    DESC_VCH                    
                FROM
                    rxds.dselement
                WHERE
                    Active_int = 1
                    AND DSID_int = #inpLibId#
                    AND DSEId_int = #inpEleId#
                    AND UserId_int = #Session.USERID#
                ORDER BY
                    DESC_VCH ASC            
            </cfquery>       
                            
            <cfif GetElements.RecordCount EQ 0>
               <cfthrow MESSAGE="Can't find the Element specified" TYPE="Any" extendedinfo="" errorcode="-2">                 
            <cfelse>
                <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#")><cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#"></cfif>    
            </cfif>               
                            
            
            
            <!--- Does Script Exist? --->
            <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                SELECT
                    DATAID_INT,
                    DSEId_int,
                    DSId_int,
                    UserId_int,
                    StatusId_int,
                    AltId_vch,
                    Length_int,
                    Format_int,
                    DESC_VCH
                FROM
                    rxds.scriptdata
                WHERE
                    Active_int = 1
                    AND DSID_int = #inpLibId#
                    AND DSEId_int = #inpEleId#
                    AND DATAID_INT = #inpDataId#
                    AND UserId_int = #Session.USERID#
                ORDER BY
                    DESC_VCH ASC            
            </cfquery>       
                     
            <cfif GetScripts.RecordCount EQ 0>
               <cfthrow MESSAGE="Can't find the Script specified" TYPE="Any" extendedinfo="" errorcode="-2">      
            <cfelse>
            
                <!--- todo: Do file conversion based on exention --->
            
                <!--- Preprocess file - conversion - data length--->
                
                <!--- todo: Update applicable data --->
                
                <!--- todo: Archive old file if exists --->
				
				<!--- todo: add cflock at the file level to prevent simultaneous. --->
                
                <!--- Move over file --->     
                <cffile action="move" source="#rxdsWebProcessingPath#/#CurrUpload.serverFile#" destination="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#/RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav"  nameconflict="overwrite">
                
                <cfset r["msg"] = "File uploaded. #rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#/RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav">
                <cfset r["error"] = "">
                                
            </cfif>     
            
		<cfcatch TYPE="any">
                      
    		<cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#">            
        
        </cfcatch>
        
        </cftry>
           
	</cfoutput>


<cfelse>
	<cfset r["error"] = "No file was uploaded.">
</cfif>

<!--- Output results in JSON format --->
<cfoutput>#serializeJSON(r)#</cfoutput>
