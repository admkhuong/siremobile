<cfsetting requesttimeout="360">

<cffunction name="getFileLength" access="public" returntype="numeric">
    <cfargument name="FilePath" type="string" required="yes">
    
    <cfset oAudioFile = createObject("java","java.io.File").init(FilePath)>
    <cfset oAudioSystem = createObject("java","javax.sound.sampled.AudioSystem")>
    <cfset oAudioFileFormat = oAudioSystem.getAudioFileFormat(oAudioFile)>
    <cfset oAudioInputStream = oAudioSystem.getAudioInputStream(oAudioFile)>
    <cfset oAudioFormat = oAudioInputStream.getFormat()>   
    <cfset nFrameLength = oAudioFileFormat.getFrameLength()>
    <cfset nFrameRate = oAudioFormat.getFrameRate()>
    
    <cfreturn round(nFrameLength / nFrameRate)>
    
</cffunction>

<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">
<cfparam name="notes" default="">
<cfparam name="catIds" default="0">
<cfparam name="access_type" default="2">

<!--- Validate session still in play - handle gracefully if not --->
<cfif Session.UserID LT "1"  >
	 <cfthrow message="User Session Has Expired - Please relog in." type="Any" extendedinfo="" errorcode="-2">
</cfif>  


<!--- Set here only so if changes can be quickly updated --->
<!---<cfinclude template="data_ScriptPaths.cfm">--->

<cfset r = structNew()>
<cfset r["id"] = "">
<cfset r["msg"] = "">
<cfset r["error"] = "">

<cfset DebugMsg1 = "">
<cfset DebugMsg2 = "">
<cfset DebugMsg3 = "">

<cfset inpDataIdIsNew = 0>

        
<!--- Is there a file --->
<cfif structKeyExists(form, "inpFileName") and len(form.inpFileName)>
	<cfoutput>
        <cftry> 
            <cfset r["error"] = "Unhandled Error in File Upload">
            <!--- create temp directory if not exists:::Should be there by DEFAULT --->
            <cfif not directoryExists("#rxdsWebProcessingPath#")><cfdirectory action="create" directory="#rxdsWebProcessingPath#"></cfif>
            
            <!---- create user specific directory if not exists:::Shouldn't be there by DEFAULT --->
            <cfif not directoryExists("#rxdsWebProcessingPath#/U#Session.UserID#")>
                <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.UserID#">
                <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.UserID#/tmpconvert">
            </cfif>
        	
            <!--- calculate the size of uploaded file --->    
            <cfset f = createObject("java","java.io.File").init(form.inpFileName)>
            
            <cfif f.length() lT (fileSizeLimitInMB * 1024 * 1024)>
                <cffile action="upload" 
                    filefield="inpFileName" 
                    destination="#rxdsWebProcessingPath#/U#Session.UserID#\" 
                    nameconflict="makeunique"
                    result = "CurrUpload">
            
            		
                <cfif ListContainsNoCase('#validExtensions#','#CurrUpload.CLIENTFILEEXT#')>    
            		
                    <!--- Create\Validate path in DB --->
                	
					<!--- Does user exist? Auto add/repair system directory structure--->
                    <!--- Only works with current session user --->
                    <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.UserID#")>
                    	<cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.UserID#">
                    </cfif>
                    
                    <!--- Does Lib Exist? --->
                    <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DSID_int,
                            Desc_vch                    
                        FROM
                            rxds.DynamicScript
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> 
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        ORDER BY
                            Desc_vch ASC            
                    </cfquery>      
                                    
                    <cfif GetLibraries.RecordCount EQ 0>
                    	<cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        	INSERT INTO rxds.DynamicScript 
                            	(DSId_int,Userid_int,Active_int,Desc_vch)
                        	VALUES
                            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> ,1,'First Library - upload script file')
                        </cfquery>
                    <cfelse>
                        <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#")>
                            <cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#">
                        </cfif>    
                    </cfif>               
                    
                    <!--- Does Ele Exist? --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DSID_int,
                            DSEId_int,
                            Desc_vch                    
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> 
                            AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">  
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        ORDER BY
                            Desc_vch ASC            
                    </cfquery>       
                                    
                    <cfif GetElements.RecordCount EQ 0>
                    	<cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        	INSERT INTO rxds.dselement 
                            	(DSEId_int,DSId_int,Userid_int,Active_int,Desc_vch)
                        	VALUES
                            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">  ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> ,1,'First Element - upload script file')
                        </cfquery>
                    <cfelse>
                        <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#")>
                            <cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#">
                        </cfif>    
                    </cfif>               
                                    
                    
                    
                   <cfif NOT(inpDataId GT 0)>
                   
                   		<cfset inpDataIdIsNew = 1>
                        
				  		<!--- Does Script Exist? --->
                       	<cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                            SELECT
                                max(DataId_int) as DataId_int
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> 
                                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">  
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        </cfquery>       
                                 
                        <cfif GetScripts.DataId_int eq '' OR GetScripts.DataId_int eq 0>
                            <cfset inpDataId = 1><!--- First time data upload --->
                        <cfelse>
                            <cfset inpDataId = GetScripts.DataId_int + 1 >
                        </cfif>
					
                    </cfif>
                    
                                        
                    <cffile action = "rename" 
                		source = "#rxdsWebProcessingPath#/U#Session.UserID#\#CurrUpload.SERVERFILE#" 
                        destination = "#rxdsWebProcessingPath#/U#Session.UserID#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.#CurrUpload.CLIENTFILEEXT#" attributes="normal">
                   	
                    <!---<cfexecute 	name="#SOXAudiopath#" 
                			arguments='#rxdsWebProcessingPath#/U#Session.UserID#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.#CurrUpload.CLIENTFILEEXT# -b 16 -r 11025 -c 1 #rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav' 
                            timeout="60"> 
                    </cfexecute>--->
                    
                    <!---- converting to mp3 format 
						channels[mp3c]: mono(1)
						frequency[mp3f]: 44.1 Khz(1)
						bitrate mode[mp3m]:Constant Bitrate (CBR) (0)
						bitrate[mp3b]: NOT USED
					--->
                    
                 	<cfset DebugMsg1 = "#rxdsWebProcessingPath#/U#Session.UserID#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.#CurrUpload.CLIENTFILEEXT# -C 128.2 #rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3 compand 0.3,1 6:-70,-60,-20 -5 -90 0.2">
                 
                    <cfexecute 	name="#SOXAudiopath#" 
                			arguments='#rxdsWebProcessingPath#/U#Session.UserID#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.#CurrUpload.CLIENTFILEEXT# -C 128.2 #rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3 compand 0.3,1 6:-60,-30,-20 -3 -90 0.2' 
                            timeout="60"> 
                    </cfexecute>
                    
                    <!--- delete file from temp storage --->
                    <cffile action="delete" file="#rxdsWebProcessingPath#/U#Session.UserID#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.#CurrUpload.CLIENTFILEEXT#" >
                    <!---<cfset fileLenInSec = getFileLength("#rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav") >--->
                    <!---<cffile action="delete" file="#rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" >--->
                    
                    <!---<cffile action = "move" 
                            source = "#rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" 
                            destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" 
                            nameconflict="overwrite"/>
                    --->
                    <cffile action = "move" 
                            source = "#rxdsWebProcessingPath#/U#Session.UserID#\tmpconvert\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3" 
                            destination = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3" 
                            nameconflict="overwrite"/>
                    
            <!---        <cfdirectory action="delete" directory="#rxdsWebProcessingPath#/U#Session.UserID#" recurse="yes">
            --->        
                    <CFOBJECT type="JAVA" action="CREATE" name="MP3File" class="helliker.id3.MP3File">
					<cfset fileLenInSec = MP3File.init("#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3").getPlayingTime() >
                    
                    <cfif inpDataIdIsNew GT 0>
                    
                        <cfquery name="InsertRecordingDetail" datasource="#Session.DBSourceEBM#">
                            INSERT INTO rxds.scriptdata
                                (DataId_int,DSEId_int,DSId_int,UserId_int,Length_int,Desc_vch,Created_dt,categories_vch,AccessLevel_int)
                            VALUES
                                (#inpDataId#,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">  ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> ,#fileLenInSec#,'#notes#',now(),'#catIds#,',#access_type#)
                         </cfquery>
                    
                    <cfelse>
                        <cfquery name="InsertRecordingDetail" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                rxds.scriptdata
                            SET
                                Length_int = #fileLenInSec#,
                                Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#notes#">,
                                LASTUPDATED_DT = NOW()
                            WHERE 
                                DataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDataId#">  
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLibId#"> 
                                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEleId#">  
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        </cfquery>
                                             
                    </cfif>
                
                        <!--- todo: Do file conversion based on exention --->
                    
                        <!--- Preprocess file - conversion - data length--->
                        
                        <!--- todo: Update applicable data --->
                        
                        <!--- todo: Archive old file if exists --->
                        
                        <!--- todo: add cflock at the file level to prevent simultaneous. --->
                        
                        <!--- Move over file --->     
                        <!---<cffile action="move" source="#rxdsWebProcessingPath#/#CurrUpload.serverFile#" destination="#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpEleId#/RXDS_#Session.UserID#_#inpLibId#_#inpEleId#_#inpDataId#.wav"  nameconflict="overwrite">
                        --->
                        <cfset r["id"] = #inpDataId#>
                        <cfset r["msg"] = "#notes#">
                        <cfset r["error"] = "">
                    
                    <cfelse>
                    	<cfset r["error"] = "Invalid File Type. Following are the file types allowed:"&#validExtensions#>       	
                    </cfif>    
                        
                <cfelse>
                     <cfset r["error"] = "File size (" & #decimalformat(f.length()/(1024*1024))# &" MB) greater than allowed ( "&fileSizeLimitInMB&" MB)">       
                </cfif>
    
	
		
        		
            <cfcatch type="any">
                <cfset r["error"] = "#cfcatch.type# - #cfcatch.message# - #cfcatch.detail# - #DebugMsg1#">            
            </cfcatch>
        
        </cftry>         
	</cfoutput>
<cfelse>
	<cfset r["error"] = "No file was uploaded.">
</cfif>

<!--- Output results in JSON format --->
<cfoutput>#serializeJSON(r)#</cfoutput>



