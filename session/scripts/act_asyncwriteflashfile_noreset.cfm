<cfsetting requesttimeout="360">

<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="1">

<cfparam name="description" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH-mm-ss')#">
<cfparam name="notes" default="#description#">
<cfparam name="catIds" default="0">

<cfparam name="categoryId" default="2">
<cfparam name="access_type" default="#categoryId#">

<!--- Flash file doesnt read cf json but will read the following (NOTE flash is CASE sesitive for the return variables)--->
<cfinclude template="act_jsonencode.cfm">




<!--- Validate session still in play - handle gracefully if not --->
<cfif Session.USERID LT "1"  >
	 <cfthrow MESSAGE="User Session Has Expired - Please relog in." TYPE="Any" extendedinfo="" errorcode="-2">
</cfif>  


<!--- Set here only so if changes can be quickly updated --->
<!---<cfinclude template="data_ScriptPaths.cfm">--->

<cfset r = structNew()>
<cfset r["msg"] = "">
<cfset r["error"] = "">



        
<!--- Is there a file --->
<cfif IsArray(getHTTPRequestData().content) AND ArrayLen(getHTTPRequestData().content)>
	<cfoutput>
        <cftry> 
            <cfset r["error"] = "Unhandled Error in File Upload">
            <!--- create temp directory if not exists:::Should be there by DEFAULT --->
            <cfif not directoryExists("#rxdsWebProcessingPath#")><cfdirectory action="create" directory="#rxdsWebProcessingPath#"></cfif>
            
            <!---- create user specific directory if not exists:::Shouldn't be there by DEFAULT --->
            <cfif not directoryExists("#rxdsWebProcessingPath#/U#Session.USERID#")>
                <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.USERID#">
                <cfdirectory action="create" directory="#rxdsWebProcessingPath#/U#Session.USERID#/tmpconvert">
            </cfif>
        	
            <!--- calculate the size of uploaded file --->    
            
            
            <cfif ArrayLen(getHTTPRequestData().content) lT (fileSizeLimitInMB * 1024 * 1024)>
            
            	<cfset sFullFilePath = "#rxdsWebProcessingPath#/U#Session.USERID#\FlashUpload_#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH-mm-ss')#.wav" />
            
	            <cffile action="write" file="#sFullFilePath#"  output="#getHTTPRequestData().content#" nameconflict="makeunique" result = "CurrUpload">
            		
                <cfif ListContainsNoCase('#validExtensions#','wav')>    
            		
                    <!--- Create\Validate path in DB --->
                	
					<!--- Does user exist? Auto add/repair system directory structure--->
                    <!--- Only works with current session user --->
                    <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#")>
                    	<cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#">
                    </cfif>
                    
                    <!--- Does Lib Exist? --->
                    <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DSID_int,
                            DESC_VCH                    
                        FROM
                            rxds.DynamicScript
                        WHERE
                            Active_int = 1
                            AND DSID_int = #inpLibId#
                            AND UserId_int = #Session.USERID#
                        ORDER BY
                            DESC_VCH ASC            
                    </cfquery>      
                                    
                    <cfif GetLibraries.RecordCount EQ 0>
                    	<cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        	INSERT INTO rxds.DynamicScript 
                            	(DSId_int,Userid_int,Active_int,DESC_VCH)
                        	VALUES
                            	(#inpLibId#,#session.USERID#,1,'First Library - upload script file')
                        </cfquery>
                    <cfelse>
                        <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#")>
                            <cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#">
                        </cfif>    
                    </cfif>               
                    
                    <!--- Does Ele Exist? --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DSID_int,
                            DSEId_int,
                            DESC_VCH                    
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = #inpLibId#
                            AND DSEId_int = #inpEleId#
                            AND UserId_int = #Session.USERID#
                        ORDER BY
                            DESC_VCH ASC            
                    </cfquery>       
                                    
                    <cfif GetElements.RecordCount EQ 0>
                    	<cfquery name="insertLibrary" datasource="#Session.DBSourceEBM#">
                        	INSERT INTO rxds.dselement 
                            	(DSEId_int,DSId_int,Userid_int,Active_int,DESC_VCH)
                        	VALUES
                            	(#inpEleId#,#inpLibId#,#session.USERID#,1,'First Element - upload script file')
                        </cfquery>
                    <cfelse>
                        <cfif !DirectoryExists("#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#")>
                            <cfdirectory action="create" directory="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#">
                        </cfif>    
                    </cfif>               
                                    
                    
                    
                   <!--- Does Script Exist? --->
                   <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                        SELECT
                            max(DATAID_INT) as DATAID_INT
                        FROM
                            rxds.scriptdata
                        WHERE
                            Active_int = 1
                            AND DSID_int = #inpLibId#
                            AND DSEId_int = #inpEleId#
                            AND UserId_int = #Session.USERID#
                    </cfquery>       
                             
                    <cfif GetScripts.DATAID_INT eq '' OR GetScripts.DATAID_INT eq 0>
                    	<cfset inpDataId = 1><!--- First time data upload --->
                    <cfelse>
                    	<cfset inpDataId = GetScripts.DATAID_INT + 1 >
                    </cfif>
                    
                    <cffile action = "rename" 
                		source = "#sFullFilePath#" 
                        destination = "#rxdsWebProcessingPath#/U#Session.USERID#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" attributes="normal">
                   	
                    <!---<cfexecute 	name="#SOXAudiopath#" 
                			arguments='#rxdsWebProcessingPath#/U#Session.USERID#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav -b 16 -r 11025 -c 1 #rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav' 
                            timeout="60"> 
                    </cfexecute>--->
                    
                    <!---- converting to mp3 format 
						channels[mp3c]: mono(1)
						frequency[mp3f]: 44.1 Khz(1)
						bitrate mode[mp3m]:Constant Bitrate (CBR) (0)
						bitrate[mp3b]: NOT USED
					--->
                 
                    <cfexecute 	name="#SOXAudiopath#" 
                			arguments='#rxdsWebProcessingPath#/U#Session.USERID#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav -C 128.3 #rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3 loudness' 
                            timeout="60"> 
                    </cfexecute>
                    
                    <!--- delete file from temp storage --->
                    <cffile action="delete" file="#rxdsWebProcessingPath#/U#Session.USERID#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" >
                    <!---<cfset fileLenInSec = getFileLength("#rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav") >--->
                    <!---<cffile action="delete" file="#rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" >--->
                    
                    <!---<cffile action = "move" 
                            source = "#rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" 
                            destination = "#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav" 
                            nameconflict="overwrite"/>
                    --->
                    <cffile action = "move" 
                            source = "#rxdsWebProcessingPath#/U#Session.USERID#\tmpconvert\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3" 
                            destination = "#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3" 
                            nameconflict="overwrite"/>
                    
                    <cfdirectory action="delete" directory="#rxdsWebProcessingPath#/U#Session.USERID#" recurse="yes">
                    
                    <CFOBJECT TYPE="JAVA" action="CREATE" name="MP3File" class="helliker.id3.MP3File">
					<cfset fileLenInSec = MP3File.init("#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3").getPlayingTime() >
                    
                    <cfif notes EQ ""><cfset notes = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#"></cfif>

                    <cfquery name="InsertRecordingDetail" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.scriptdata
                            (DATAID_INT,DSEId_int,DSId_int,UserId_int,Length_int,DESC_VCH,Created_dt,categories_vch,AccessLevel_int)
                        VALUES
                            (#inpDataId#,#inpEleId#,#inpLibId#,#Session.USERID#,#fileLenInSec#,'#notes#',now(),'#catIds#,',#access_type#)
                     </cfquery>
                    
    
                    
                    
                        <!--- todo: Do file conversion based on exention --->
                    
                        <!--- Preprocess file - conversion - data length--->
                        
                        <!--- todo: Update applicable data --->
                        
                        <!--- todo: Archive old file if exists --->
                        
                        <!--- todo: add cflock at the file level to prevent simultaneous. --->
                        
                        <!--- Move over file --->     
                        <!---<cffile action="move" source="#rxdsWebProcessingPath#/#sFullFilePath#" destination="#rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#/RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav"  nameconflict="overwrite">
                        --->
                        <cfset r["msg"] = "File uploaded. #rxdsLocalWritePath#/U#Session.USERID#/L#inpLibId#/E#inpEleId#/RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.mp3">
                        <cfset r["error"] = "">
                                                
                        <cfset r = structNew()>
                        <cfset r["SUCCESS"] = "true">
                        <cfset r["MESSAGE"] = "Save file success">
                        <cfset r["URL"] = "#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_GetMyMP3?inpUserId=#Session.USERID#&amp;inpLibId=#inpLibId#&amp;inpEleId=#inpEleId#&amp;inpScriptId=#inpDataId#">
		                <cfset r["NEWSCRIPTID"] = "#inpDataId#">
                        <cfset r["URL"] = "#inpDataId#">
					 
                    <cfelse>
                    	<cfset r["error"] = "Invalid File TYPE. Following are the file types allowed:<br>"&#validExtensions#>       	
                    </cfif>    
                        
                <cfelse>
                     <cfset r["error"] = "File size (" & #decimalformat(ArrayLen(getHTTPRequestData().content)/(1024*1024))# &" MB) greater than allowed ( "&fileSizeLimitInMB&" MB)">       
                </cfif>
    
	
		
        		
            <cfcatch TYPE="any">
                <cfset r["error"] = "#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#">            
            </cfcatch>
        
        </cftry>         
	</cfoutput>
<cfelse>
	<cfset r["error"] = "No file was uploaded.">
</cfif>

<!--- Output results in JSON format --->
<!---<cfoutput>#serializeJSON(r)#</cfoutput>--->
<cfoutput>#jsonencode(r)#</cfoutput>



