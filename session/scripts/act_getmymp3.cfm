
<cfparam name="inpUserId" default="0">
<cfparam name="inpLibId" default="0">
<cfparam name="inpEleId" default="0">
<cfparam name="inpScriptId" default="0">

<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  

<!--- Validate authorized current session user can listen to input users request --->

<cfset MainLibFile = "#rxdsLocalWritePath#/U#inpUserId#/L#inpLibId#/E#inpEleId#\RXDS_#inpUserId#_#inpLibId#_#inpEleId#_#inpScriptId#.mp3">       
<!---<cfset MainLibFile = "#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/#rxdsWebReadPath#/U#inpUserId#/L#inpLibId#/E#inpEleId#\RXDS_#inpUserId#_#inpLibId#_#inpEleId#_#inpScriptId#.mp3">       
<cfheader name="Content-Disposition" value="attachment;filename=TestFile.mp3">
<cfcontent TYPE="application/mp3" file="#MainLibFile#"></cfcontent>  
--->         

<cfif session.userid neq inpUserId>            
    <cfquery name="updateView" datasource="#Session.DBSourceEBM#">
        Update rxds.scriptData
        set viewcount_int = viewcount_int+1
        where userid_int = #inpUserId# and DATAID_INT=#inpScriptId#    
    </cfquery>
</cfif>

<cfheader name="Content-Disposition" value="inline; filename=TestFile.mp3">
<cfcontent file="#MainLibFile#"></cfcontent>    

