<cfapplication sessionmanagement="yes" clientmanagement="yes" name="Record">
<cfinclude template="act_jsonencode.cfm">
<cfscript>
	application.baseDir = GetDirectoryFromPath(GetCurrentTemplatePath());
	application.baseUrl = "http://" & CGI.HTTP_HOST & GetDirectoryFromPath(CGI.SCRIPT_NAME);
	application.soxPath = "C:\cds\sox\sox.exe";
	application.soxMaxExeTime = 90;
	
	retVal = StructNew();
	retVal.success = true;
	retVal.MESSAGE = "Save file success";
</cfscript>
<cfif IsArray(getHTTPRequestData().content) AND ArrayLen(getHTTPRequestData().content)>
	<cfif Not DirectoryExists("#application.baseDir#/audio/#categoryId#")>
		<cfdirectory action = "create" directory = "#application.baseDir#/audio/#categoryId#" recurse="yes" mode="0775" />
	</cfif>
	<cfset todayDate = now() />
	<cfset sFileName = dateFormat(todayDate, "yyyy-mm-dd-") & timeFormat(todayDate, "HH-mm-ss") />
	<cfif isDefined("filename")>
		<cfset sFileName = sFileName & "-" & filename />
	</cfif>
	<cffile action="write" file = "#application.baseDir#/audio/#categoryId#/#sFileName#.wav" output = "#getHTTPRequestData().content#">
	<cfexecute
		name = "#application.soxPath#"
    arguments = '"#application.baseDir#/audio/#categoryId#/#sFileName#.wav" "#application.baseDir#/audio/#categoryId#/#sFileName#.mp3"' 
    outputFile = "#application.baseDir#soxlog.txt"
    timeout = "#application.soxMaxExeTime#">
	</cfexecute>
	<cffile action="delete"	file="#application.baseDir#/audio/#categoryId#/#sFileName#.wav" />
	<cfset retVal.url = "#application.baseUrl#/audio/#categoryId#/#sFileName#.mp3" />
<cfelse>
	<cfscript>
		retVal.success = false;
		retVal.MESSAGE = "No Record Data Posted";
	</cfscript>	
</cfif>
<cfoutput>#jsonencode(retVal)#</cfoutput>