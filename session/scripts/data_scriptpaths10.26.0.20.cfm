<!--- Set here only so if changes can be quickly updated --->

<!--- Path Structure to files 
rxdsLocalWritePath/U#Session.USERID#/L#inpLibId#/E#inpEleId#\RXDS_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
--->

<!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
<cfset rxdsLocalWritePath = "\\10.26.0.60\SimpleXScripts">
<!---<cfset rxdsWebReadPath = "SimpleXScripts">--->
<cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
<cfset rxdsWebProcessingPath = "C:\Temp_script">

<!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
<!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
<cfset rxdsRemoteRXDialerPath = "\DynamicLibraries">

<!---Path to Audio conversion executable location --->
<cfset SOXAudiopath = "C:\cds\sox2\sox.exe"> 

<!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
<cfset validExtensions = "WAV,MP3">
<cfset fileSizeLimitInMB = 3 >


<!--- Legacy Storage Format \\10.0.1.10\DynaScript/LibId\DSSD_%s_%s_%s.wav - CurrScriptDataId, Lib, Element --->







