


<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">



<!--- for uploading audio files --->
<cfoutput>
	<script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.filestyle.js"></script>
</cfoutput>


<cfoutput>
<script type="text/javascript">
	var CurrentProcess = 0;
	
	call2RecordID.bind("dialogbeforeclose",function(event, ui) {
			if(CurrentProcess==0)
				return true;
			else{
				$.alerts.okButton = '&nbsp;Yes&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				
				jConfirm( "Are you sure to cancel this Call?", "About to cancel Call-2-record.", function(result) { 
					if(result)
					{	
						call2RecordID.dialog('destroy'); 
					}
					return result;
				});
				return false;
			}
					
		});
		
		
	$("##formCall2Record ##Cancel").click( function() {$("##loadingDlgcall2Record").hide(); call2RecordID.dialog('destroy'); return false;  }); 		
	
	var dtsid = 0;
	var msgNotes = '';
	var catIds = '';
	var phoneUsed = parseInt($('##Call2Record_phoneNumber').html());
	$(document).ready(function() {
		$("##formCall2Record ##callNow").click( function() {	
				callNowFirstStep();	
				CurrentProcess = 1;
				return false;  
			});
	});
	
	function callNowFirstStep()
	{
		msgNotes = $('##formCall2Record ##inpNotes').val();
		catIds = $('##formCall2Record ##category').val();
		$('##Call2Record_step1').css({'font-weight':'bold', 'color':'red'});
		document.getElementById('formCall2Record').style.display = 'none';
		document.getElementById('Call2RecordDiv').style.display = '';
				
		$.getJSON("../cfc/scripts.cfc?method=checkDirectory&returnformat=json&queryformat=column", {}, function(res,code) 		
		{
			if(parseInt(res) == 1)
			{
				$('##Call2Record_step1').css({'font-weight':'normal', 'color':'green'});
				makeCall();
			}
		});
	}
	
	function makeCall()
	{
		$('##Call2Record_step2').css({'font-weight':'bold', 'color':'red'});
		$.getJSON("../cfc/scripts.cfc?method=Call2Record&phoneUsed="+parseInt(phoneUsed)+"&returnformat=json&queryformat=column", {}, function(res,code) {
			if(parseInt(res)!=0)
			{
				dtsid = parseInt(res);
				updateCall();
			}
		});
	}
	
	function updateCall()
	{
		var d = new Date();
		$.getJSON("../cfc/scripts.cfc?method=getStatus&dtsid="+dtsid+"&t="+d.getTime()+"&returnformat=json&queryformat=column", {}, function(res,code) {
			if(parseInt(res)==1)
			{
				setTimeout(updateCall,1000);	
			}
			else if(parseInt(res)==2)
			{
				$('##Call2Record_step2').css({'font-weight':'normal', 'color':'green'});
				$('##Call2Record_step3').css({'font-weight':'bold', 'color':'red'});
				setTimeout(updateCall,1000);
			}
			else if(parseInt(res)==3)
			{
				$('##Call2Record_step3').css({'font-weight':'normal', 'color':'green'});
				$('##Call2Record_step4').css({'font-weight':'bold', 'color':'red'});
				setTimeout(updateCall,1000);
			}
			else if(parseInt(res)==4)
			{
				$('##Call2Record_step4').css({'font-weight':'normal', 'color':'green'});
				saveWavFile();
			}
			else
			{
				alert('Sorry something went wrong');
			}
		});
	}
	
	function saveWavFile()
	{
		$('##Call2Record_step5').css({'font-weight':'bold', 'color':'red'});
		$.getJSON("../cfc/scripts.cfc?method=saveWaveFile&phoneUsed="+parseInt(phoneUsed)+'&access_type='+$('input:radio[name=access_type]:checked').val() + "&msg="+encodeURIComponent(msgNotes)+"&category="+catIds+"&returnformat=json&queryformat=column", {}, function(res,code) {
			
			if (res.ROWCOUNT > 0) 
			{		
				//alert(d.ROWCOUNT)																							
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(res.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = parseInt(res.DATA.RXRESULTCODE[0]);	
					
					if(CurrRXResultCode > 0)
					{
						$('##Call2Record_step5').css({'font-weight':'normal', 'color':'green'});
						$('##Call2Record_step6').css({'display':''});
						<cfif session.facebook OR session.fbuserid>
							stream_publish(CurrRXResultCode,res.DATA.MSG[0]);
						</cfif>
					}
					else
					{
						<!--- Unsuccessful Login --->
						<!--- Check if variable is part of JSON result string   res.DATA.CCDXMLString[0]  --->								
						if(typeof(res.DATA.MSG[0]) != "undefined")
						{		
							alert(res.DATA.MSG[0]);
							//top.window.location = 'http://apps.facebook.com/babblesphere/';	
							//jAlertOK('Sorry a problem occured with Facebook authorization!!','Failure!',function(result) {top.window.location = 'http://apps.facebook.com/babblesphere/'; return false; });
							//top.window.location = 'http://apps.facebook.com/babblesphere/';
						}		
					}
				}
				else
				{<!--- Invalid structure returned --->	
					// Do something									
				}
			}
			else
			{<!--- No result returned --->
				// Do something									
			}
			
		});
	}
	
	function stream_publish(dataId,msg)
	{
		FB.ui({
		  method: "stream.publish",
		  display: "iframe",
		  user_message_prompt: "Publish This!",
		  message: "My new recorded babble",
		  attachment: {
			 name: "New Babble",
			 caption: "This is my newly published babble !!",
			 description: "Listen it here",
			 href: "http://example.com/",
			 media:[{
				 	"type":"mp3",
					"src":"#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_GetMyMP3?inpUserId=#session.userid#&inpLibId=1&inpEleId=1&inpScriptId="+dataId,
					<!---"src":"http://dev.telespeech.com/simplexscripts/u8/l1/e1/RXDS_8_1_1_7.mp3",--->
					"title":msg,
					"artist":"Deepak yadav"
					}],
			<!---properties:{
			   "1)":{"text":"Reading","href":"http://example.com/skill.php?reading"},
			   "2)":{"text":"Math","href":"http://example.com/skill.php?math"},
			   "3)":{"text":"Farmville","href":"http://example.com/skill.php?farmville"}
			 }--->
		  },
		  action_links: [{ text: 'Test yourself', href: 'http://example.com/test.php' }]
		 },
		 function(response) {
		   if (response && response.post_id) {
			 alert('Post was published.'+response.post_id);
		   } else {
			 //alert('Post was not published.');
		   }
		 }
		);
	}
	
	
	function continue_recording()
	{
		$('##Call2Record_step1').css({'font-weight':'normal', 'color':''});
		$('##Call2Record_step2').css({'font-weight':'normal', 'color':''});
		$('##Call2Record_step3').css({'font-weight':'normal', 'color':''});
		$('##Call2Record_step4').css({'font-weight':'normal', 'color':''});
		$('##Call2Record_step5').css({'font-weight':'normal', 'color':''});
		$('##Call2Record_step6').css({'display':'none'});
		$('##Call2RecordDiv').css({'display':'none'});
		$('##formCall2Record').css({'display':''});
		
	}
	
	function exit_recording()
	{
		$("##loadingDlgcall2Record").hide();
		call2RecordID.dialog('destroy');
		gridScriptListReload();	
	}
	
	 $("##formCall2Record ##category").click(function(){
					if(document.getElementById('category_none_rec').selected)
					{
						$("select option:selected").each(function () {$(this).attr("selected", false)});
						document.getElementById('category_none_rec').selected = true;
					}
				}
			)	
	
	
</script>

</cfoutput>

<style>

#Call2RecordDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#Call2RecordDiv div{
display:inline;
border:none;
}

</style> 



<cfquery name="getNumber" datasource="#Session.DBSourceEBM#">
	select 	primaryphonestr_vch as pn
    from 	simpleobjects.useraccount
    where 	userid_int = #session.userid#
</cfquery>
 
<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>

<cfoutput>#session.userid#:#session.fbuserid#
	<div id='formCall2Record' class="RXForm" style="font-size:12px">
    	<form>
        	<label>Recording Notes
            <span class="small">Notes or Data</span>
            </label>
            <input type="text" name="inpNotes" id="inpNotes"/> 
            
        	<label>Choose Category
            <span class="small">Type of Babble</span>
            </label>
            
            <select name="category" id="category" multiple="multiple" size="4">
                <option value="0" id="category_none_rec" selected="selected"> -- all --</option>
                <cfloop query="getCategories">
                    <option value="#categoryid_int#" id="cat#categoryid_int#">#categoryDesc_vch#</option>
                </cfloop>
            </select>
            
            <label>Access Type</label>
            <input type="radio" name="access_type" id="access_public" value="2" checked="checked" style="display:inline;width:10px" /><span class="small">Public</span>
            <input type="radio" name="access_type" id="access_friend" value="1"  style="display:inline;width:10px"  /><span class="small">Friend</span>
            <input type="radio" name="access_type" id="access_private" value="0" style="display:inline;width:10px"  /><span class="small">Private</span>
            <br /><Br />
        
            <button id="callNow" type="button" class="ui-corner-all">Call Now</button>
            <button id="Cancel" type="button" class="ui-corner-all">Cancel</button>
        </form>
    </div>
    <div id='Call2RecordDiv' class="RXForm" style="font-size:12px; display:none">
        Please wait while we perform following steps:<Br /><Br />
        <span id="Call2Record_step1">Step 1) Creating required directory.</span><Br /><br />
        <span id="Call2Record_step2">Step 2) Dialing <span id="Call2Record_phoneNumber">#getNumber.pn#</span>...</span><Br /><br />
        <span id="Call2Record_step3">Step 3) Ringing...</span><Br /><br />
        <span id="Call2Record_step4">Step 4) Call in progress...</span><Br /><br />
        <span id="Call2Record_step5">Step 5) Call end, saving audio script.</span><Br /><br />
        <span id="Call2Record_step6" style="display:none">
            Script saved successfully!!!. Want to record another one?<br />
            <button id="Call2Record_continue" value="Yes" onclick="continue_recording();">Yes</button> [Yes record new script.]<br />
            <button id="Call2Record_stop" value="No" onclick="exit_recording();">No</button> [No Exit to main screen.]<br />
        </span>
        <div id="loadingDlgcall2Record" style="display:none;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
    </div>
</cfoutput>









