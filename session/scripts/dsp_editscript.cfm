<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">

<cfset inpDataId = #url.inpScriptId#>

<!--- for uploading audio files --->
<cfoutput>
	<script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/ajaxfileupload.js"></script>
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.filestyle.js"></script>
</cfoutput>


<cfoutput>
<script TYPE="text/javascript">
	var dtsid = 0;
	var CurrentProcess = 0;
	var totalLen = 1000;
	
	editID.bind("dialogbeforeclose",function(event, ui) {
				if(CurrentProcess==0)
					return true;
				else{
					$.alerts.okButton = '&nbsp;Yes&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure to cancel process and exit?", "About to cancel Text-2-Speech.", function(result) { 
									if(result)
									{	
										editID.dialog('destroy'); 
									}
									return result;
								});
					return false;
				}
					
		});
	
	function updateCall()
	{
		CurrentProcess = 1;
		$('##loadingDlgtts').css({'display':''});
		
		$.getJSON("../cfc/scripts.cfc?method=updateScript&inpDataId=#inpDataId#&category="+$('##category').val()+"&access_type="+$('input:radio[name=access_type]:checked').val()+"&notes="+encodeURIComponent($('##inpNotes').val())+"&returnformat=json&queryformat=column", {}, function(res,code) {
			if(res.DATA.RXRESULTCODE[0] == 1 )
			{
				CurrentProcess = 0;
				gridScriptListReload();
				editID.dialog('destroy'); 
			}
			else if(res.DATA.RXRESULTCODE[0] == -2)
			{
				jAlert(res.DATA.MESSAGE[0]);
				$('##loadingDlgtts').css({'display':'none'});
			}
			else
			{
				jAlert('Error updating script!!');	
				$('##loadingDlgtts').css({'display':'none'});					
			}
		});
	}
	
	
	function countCharacter()
	{
		var curLen = $('##inpNotes').val().length;
		
		if(curLen >= 500)
		{
			$('##charLimDsp_edit').html('<span style="color:red">500 character limit reached.</span>');
			$('##inpNotes').val($('##inpNotes').val().substring(0,500));
		}
		else if(curLen > 250)
		{
			var rem = 500 - curLen;
			$('##charLimDsp_edit').html('<span style="color:##FFAF00">'+ rem + ' characters remaining only.</span>');	
		}
		else if(curLen < 500)
			$('##charLimDsp_edit').html(500- $('##inpNotes').val().length + ' characters remaining.');
	}
	
	$("##ttsDiv ##category").click(function(){
				
				if(document.getElementById('category_none_edit').selected)
				{
					$("select option:selected").each(function () {$(this).attr("selected", false)});
					document.getElementById('category_none_edit').selected = true;
				}
			}
		)
	
	$("##ttsDiv ##Cancel").click( function() {$("##loadingDlgtts").hide(); editID.dialog('destroy'); return false;  }); 		
</script>

</cfoutput>

<style>

#ttsDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#ttsDiv div{
display:inline;
border:none;
}

</style> 


<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>

<cfquery name="getDetails" datasource="#Session.DBSourceEBM#">
	select 	Categories_vch,DESC_VCH,accesslevel_int
    from 	rxds.scriptData
    where 	DATAID_INT=#inpDataId# and userid_int=#session.USERID#
</cfquery>


<cfoutput>
    <div id='ttsDiv' class="RXForm" style="font-size:12px">
        <cfform name="ttsForm" id="ttsForm" action="">
        	<label><span class="small">Preview</span></label>
            <object TYPE="application/x-shockwave-flash" data="dewplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> 
            	<param name="wmode" value="transparent" />
                <param name="movie" value="dewplayer.swf" /> 
                <param name="flashvars" value="mp3=http://dev.telespeech.com/simplexscripts/u#session.userid#/l1/e1/RXDS_#session.userid#_1_1_#inpdataid#.mp3" /> 
            </object>
            <br><br>
            <label>Script Notes <span class="small">Notes or Data <span class="small" id="charLimDsp_edit">500 characters limit</span> </span></label>
            <input TYPE="text" name="inpNotes" id="inpNotes" value="#getDetails.DESC_VCH#" onKeyUp="countCharacter()" />
            
             <br>
                      
            <label>Choose Category
            <span class="small">TYPE of Babble</span>
            </label>
            <select name="category" id="category" multiple="multiple" size="4">
                <option value="0" id="category_none_edit" <cfif ListContains(getDetails.Categories_vch,0)> selected</cfif>> -- all --</option>
                <cfloop query="getCategories">
                    <option <cfif ListContains(getDetails.Categories_vch,categoryid_int)> selected</cfif> value="#categoryid_int#" id="cat#categoryid_int#" >#categoryDesc_vch#</option>
                </cfloop>
            </select>
            
            <br>
            
            <label>Access TYPE</label>
            <input TYPE="radio" name="access_type" id="access_public" value="2" <cfif getDetails.accesslevel_int eq 2> checked="checked" </cfif> style="display:inline;width:10px" /><span class="small">Public</span>
            <input TYPE="radio" name="access_type" id="access_friend" value="1" <cfif getDetails.accesslevel_int eq 1> checked="checked" </cfif>  style="display:inline;width:10px"  /><span class="small">Friend</span>
            <input TYPE="radio" name="access_type" id="access_private" value="0" <cfif getDetails.accesslevel_int eq 0> checked="checked" </cfif> style="display:inline;width:10px"  /><span class="small">Private</span>
            <br /><Br />
        	
            <button id="updateButton" TYPE="button" class="ui-corner-all" onClick="updateCall();">Update</button>
            <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
            
            <span id="loadingDlgtts" style="display:none;">
            	<img class="loadingDlgDeleteGroupImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        	</span>
            
            <br>
        </cfform>    
            <span id="tts_success" style="display:none">
                Script saved successfully!!!. Want to upload new text-2-speech?<br />
                <button id="Call2Record_continue"  onclick="continue_tts();">Yes</button>
                <button id="Call2Record_stop" onclick="exit_tts();">No</button><br />
            </span>
        
       
        <div id="loadingDlgtts" style="display:none;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
    </div>
</cfoutput>


