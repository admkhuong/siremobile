<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">



<!--- for uploading audio files --->
<cfoutput>
	<script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/ajaxfileupload.js"></script>
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.filestyle.js"></script>
</cfoutput>


<cfoutput>
<script TYPE="text/javascript">
	var dtsid = 0;
	var CurrentProcess = 0;
	var totalLen = 1000;
	
	ttsID.bind("dialogbeforeclose",function(event, ui) {
				if(CurrentProcess==0)
					return true;
				else{
					$.alerts.okButton = '&nbsp;Yes&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure to cancel process and exit?", "About to cancel Text-2-Speech.", function(result) { 
									if(result)
									{	
										ttsID.dialog('destroy'); 
									}
									return result;
								});
					return false;
				}
					
		});
	
	function ttsCall()
	{
		CurrentProcess = 1;
		$('##loadingDlgtts').css({'display':''});
		
		$.getJSON("../cfc/scripts.cfc?method=checkDirectory&returnformat=json&queryformat=column", {}, function(res,code) {
			if(parseInt(res) == 1)
			{
				var uText = document.getElementById('inpuserText').value;
				var vt = document.getElementById('voiceTalent').selectedIndex + 1;
				$.getJSON("../cfc/scripts.cfc?method=tts&text="+encodeURIComponent(uText)+"&notes="+encodeURIComponent($('##tts_inpNotes').val())+"&voice="+vt+"&returnformat=json&queryformat=column", {}, function(res,code) {
					
					if(parseInt(res)!=0)
					{
						CurrentProcess = 0;
						gridScriptListReload();
						$('##loadingDlgtts').css({'display':'none'});
						$('##tts_success').css({'display':''});
					}
					else
					{
						alert('Sorry! there is some problem with TTS at present moment');
					}
				});
			}
			else
			{
				jAlert('Error creating directory!!');	
			}
		});
	}
	
	
	function continue_tts()
	{
		$('##inpuserText').val('');
		$('##tts_inpNotes').val('');
		$('##tts_success').css({'display':'none'});
	}
	
	function exit_tts()
	{
		$("##loadingDlgcall2Record").hide();
		ttsID.dialog('destroy');
		gridScriptListReload();	
	}
	
	function countCharacter()
	{
		var curLen = $('##inpuserText').val().length;
		
		if(curLen >= 1000)
		{
			$('##charLimDsp').html('<span style="color:red">1000 character limit reached.</span>');
			$('##inpuserText').val($('##inpuserText').val().substring(0,1000));
		}
		else if(curLen > 750)
		{
			var rem = 1000 - curLen;
			$('##charLimDsp').html('<span style="color:##FFAF00">'+ rem + ' characters remaining only.</span>');	
		}
		else if(curLen < 1000)
			$('##charLimDsp').html(1000- $('##inpuserText').val().length + ' characters remaining.');
	}
</script>

</cfoutput>

<style>

#Call2RecordDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#Call2RecordDiv div{
display:inline;
border:none;
}

</style> 

<!---<cfinclude template="data_ScriptPaths.cfm">--->


<cfoutput>

    <!---<cfquery name="getTitle" datasource="#Session.DBSourceEBM#">
    	Select 	DESC_VCH
        from 	rxds.scriptdata
        where 	userid_int IN (#url.ids#)
    </cfquery>
    <cfabort>--->
    <CFXML variable="xmlDoc">
    	<?xml version="1.0" encoding="UTF-8"?>
        <playlist version="1" xmlns="http://xspf.org/ns/0/">
            <title>Deepak test</title>
            <creator>Deepak</creator>
            <link>http://dev.telespeech.com/devjlp/SimpleFramedX/session/account/home</link>
            <info>The Best Playlist for Testing</info>
            <image>covers/tracklist.jpg</image>
            <trackList>
            <cfloop list="#url.ids#" index="i">
            	<track>
                  <location>http://dev.telespeech.com/simplexscripts/u8/l1/e1/RXDS_#session.userid#_1_1_#i#.mp3</location>
                  <creator>Deepak</creator>
                  <album>RXDS_album</album>
                  <title>RXDS_title</title>
                  <annotation>RXDS_title</annotation>
                  <duration>32000</duration>
                  <image>covers/0.jpg</image>
                  <info>info here</info>
                  <link>link here</link>
                </track>
            </cfloop>
            </trackList>
        </playlist>

    </CFXML>
    
    <cffile action="write" file="#rxdsLocalWritePath#/U8/L1/E1\test.xml"  output="#toString(xmlDoc)#" nameconflict="overwrite">
    
    <object TYPE="application/x-shockwave-flash" data="dewplayer-playlist.swf" width="240" height="200" id="dewplayer" name="dewplayer">
        <param name="wmode" value="transparent" />
        <param name="movie" value="dewplayer-playlist.swf" />
        <param name="flashvars" value="showtime=true&autoreplay=true&xml=http://dev.telespeech.com/simplexscripts/u8/l1/e1/test.xml" />
	</object>
    
</cfoutput>


