

<cfparam name="VerboseDebug" default="0" TYPE="numeric">
<cfparam name="inpDialerIPAddr" default="#Session.QARXDialer#" TYPE="string">
<cfparam name="inpLibId" default="0" TYPE="numeric">
<cfparam name="inpForceUpdate" default="0">
<cfparam name="INPBATCHID" default="0" TYPE="string">

<cfparam name="inpDoSingleShot" default="0" TYPE="string">
<cfparam name="inpDialString" default="0">

<!--- For one RXDialer --->


<!--- For each Ele in Lib --->
<!--- Get newest last changed date in Ele  --->

<!--- Verify directory structure --->
	<!--- if not exists add --->


<!--- Get newest Last Change date on RXDialer --->

<script TYPE="text/javascript">
	
	<!--- Main function called after page is loaded --->
	$(function()
	{		
		$("#PublishScriptsMain #DistributionLog").append('<BR><font style="font-weight: bold; color: #F00">Starting</font>' + '<br>');
		
		$("#PublishScriptsMain #DistributionLog").append('<font style="font-weight: bold; color: #000">Started Publish/Validate Remote Script Libraries</font>' + '<br>');
		GetAllSCRIPTLIBs(0);
//		ValidateRemoteScriptPaths();
		
	});
	
	function GetAllSCRIPTLIBs(inpLastProcessedLibId)
	{			
	
		if(typeof(inpLastProcessedLibId) == 'undefined')
			inpLastProcessedLibId= 0;
			
			
		$("#PublishScriptsMain #loading").show();		
			
			$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/campaign/MCID/cfc/mcidtools.cfc?method=GetSCRIPTLIBs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpStartLibId : inpLastProcessedLibId }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								<!--- All good --->
								<!--- if 3 then distribute all and over write --->
								<!--- Add option to force over write --->
								$("#PublishScriptsMain #DistributionLog").append(d.DATA.MESSAGE[0] + '<br>');
														
								
								<!--- Kick off next AJAX call --->
								if(d.DATA.SCRIPTLIB[0] > 0)
								{
									<!--- Publish the library --->
									ValidateRemoteScriptPaths(d.DATA.SCRIPTLIB[0]);
								}
								else
								{
									if(<cfoutput>#inpDoSingleShot#</cfoutput> > 0)
									{
										$("#PublishScriptsMain #DistributionLog").append('<font style="font-weight: bold; color: #000">Starting Remote Schedule Updates</font>' + '<br>');
										UpdateRemoteSchedule();
										
									}
									else
									{									
										<!--- All done --->
										$("#PublishScriptsMain #DistributionLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
										$("#PublishScriptsMain #loading").hide();	
									}
								}
								
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#PublishScriptsMain #loading").hide();} );
									$("#PublishScriptsMain #DistributionLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server. Unable to get list of script librarys", function(result) {$("#PublishScriptsMain #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to get list of script librarys",function(result) {$("#PublishScriptsMain #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.",function(result) {$("#PublishScriptsMain #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#PublishScriptsMain #loading").hide(); --->
			
			} );	
		
			
		
		return false;
	}
	
	
		
	function ValidateRemoteScriptPaths(inpLibId)
	{			
	
		<!--- Either use input values from context menu or try to get from LOCALOUTPUT form --->
		if(typeof(inpLibId) == 'undefined')
			inpLibId= <cfoutput>#inpLibId#</cfoutput>
		
		$("#PublishScriptsMain #loading").show();		
			
			$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/Scripts.cfc?method=ValidateRemotePaths&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpLibId : inpLibId,  inpDialerIPAddr : <cfoutput>'#inpDialerIPAddr#'</cfoutput>}, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								<!--- All good --->
								<!--- if 3 then distribute all and over write --->
								<!--- Add option to force over write --->
								$("#PublishScriptsMain #DistributionLog").append(d.DATA.MESSAGE[0] + '<br>');
			
								inpForceUpdate = <cfoutput>#inpForceUpdate#</cfoutput>;
								
								
								
								<!--- Kick off next AJAX call --->
								ValidateRemoteScriptDataCall(inpLibId, 0, 0, <cfoutput>'#inpDialerIPAddr#'</cfoutput>, 0); 
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#PublishScriptsMain #loading").hide();} );
									$("#PublishScriptsMain #DistributionLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server. Unable to verify script library", function(result) {$("#PublishScriptsMain #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to verify script library",function(result) {$("#PublishScriptsMain #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.",function(result) {$("#PublishScriptsMain #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#PublishScriptsMain #loading").hide(); --->
			
			} );	
		
			
		
		return false;
	}
	
	
	function ValidateRemoteScriptDataCall(inpLibId, INPNEXTELEID, INPNEXTDATAID, inpDialerIPAddr, inpForceUpdate)
	{	
		$("#PublishScriptsMain #loading").show();		
			
			$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/Scripts.cfc?method=ValidateRemoteScriptData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpLibId : inpLibId, INPNEXTELEID : INPNEXTELEID, INPNEXTDATAID : INPNEXTDATAID, inpDialerIPAddr : inpDialerIPAddr, inpForceUpdate : inpForceUpdate}, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{	
								<!--- All good --->
								<!--- if 3 then distribute all and over write --->
								<!--- Add option to force over write --->
								$("#PublishScriptsMain #DistributionLog").append(d.DATA.MESSAGE[0] + '<br>');
								
								<!--- Kick off next AJAX call --->
								<!--- Kick off next AJAX call --->
								if(d.DATA.INPNEXTELEID[0] >= 0)
								{
									ValidateRemoteScriptDataCall(inpLibId, d.DATA.INPNEXTELEID[0], d.DATA.INPNEXTDATAID[0], inpDialerIPAddr, inpForceUpdate);
								}
								else
								{	
									GetAllSCRIPTLIBs(inpLibId);																
								}
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#PublishScriptsMain #loading").hide();} );
									$("#PublishScriptsMain #DistributionLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server. Unable to publish script library", function(result) {$("#PublishScriptsMain #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to publish script library",function(result) {$("#PublishScriptsMain #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.",function(result) {$("#PublishScriptsMain #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#PublishScriptsMain #loading").hide(); --->
			
			} );	
		
			
		
		return false;
	}
	
	function DoSingleShotDistribution()
	{
			$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/Distribution.cfc?method=SingleShot&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpDialerIPAddr : <cfoutput>'#inpDialerIPAddr#'</cfoutput>, inpDialString : <cfoutput>'#inpDialString#'</cfoutput>, TimeZone_ti : <cfoutput>'31'</cfoutput>, RedialCount_int : <cfoutput>'0'</cfoutput>}, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								$("#PublishScriptsMain #DistributionLog").append('Remote Distribution OK' + '<br>');
							
								<!--- All done --->
								$("#PublishScriptsMain #DistributionLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
								$("#PublishScriptsMain #loading").hide();	
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#PublishScriptsMain #loading").hide();} );
									$("#PublishScriptsMain #DistributionLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server.  Unable to send single shot.", function(result) {$("#PublishScriptsMain #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to send single shot.",function(result) {$("#PublishScriptsMain #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.  Unable to send single shot.",function(result) {$("#PublishScriptsMain #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#PublishScriptsMain #loading").hide(); --->
			
			} );	
				
		return false;		
	}
		
	
	
		
	function UpdateRemoteSchedule()
	{		
			$.getJSON( '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateScheduleOptionsRemoteRXDialer&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpDialerIPAddr : <cfoutput>'#inpDialerIPAddr#'</cfoutput> }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								$("#PublishScriptsMain #DistributionLog").append('Remote Schedule Updated OK' + '<br>');
							
								$("#PublishScriptsMain #DistributionLog").append('<font style="font-weight: bold; color: #000">Starting Remote Distribution</font>' + '<br>');
								DoSingleShotDistribution();
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#PublishScriptsMain #loading").hide();} );
									$("#PublishScriptsMain #DistributionLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server.   Unable to update remote RXDialer schedule", function(result) {$("#PublishScriptsMain #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to update remote RXDialer schedule.",function(result) {$("#PublishScriptsMain #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.  Unable to send single shot.",function(result) {$("#PublishScriptsMain #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#PublishScriptsMain #loading").hide(); --->
			
			} );	
				
		return false;	
	
	}
		
		
</script>


<div id="PublishScriptsMain">


<div id="loading">
<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">Processing...
</div>

<div id="DistributionLog" style=" border:solid; border-bottom-width:2px; height:500px; width:750px; overflow:auto; white-space:nowrap;">

</div>


</div>


<!--- 

<script TYPE="text/javascript">

		
		alert('Yo!')
		$("#PublishScriptsMain #DistributionLog").append('Starting...' + '<br>');
		ValidateRemoteScriptPaths();
		
</script>

 --->