<!---<cfdump var="#session#">--->
<!---
plugInCheck[0] : flash
plugInCheck[1] : shockwave
plugInCheck[2] : quicktime
plugInCheck[3] : windows-media-player
plugInCheck[4] : html 5
--->
<cfparam name="inpGroupId" default="0">
<cfscript>
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}

</cfscript>

<!--- :#CGI.SERVER_PORT# --->
<cfif Application.socialNetwork>
	<cfset sname = "Babble">
    <cfset pname = "Babbles">
<cfelse>
	<cfset sname = "Recording">
    <cfset pname = "Recordings">
</cfif>

<cfoutput>

<!---	<script src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.jqGrid.min.js" TYPE="text/javascript"></script>--->
    <!---<script src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/pluginDetect.js" TYPE="text/javascript"></script>--->

<!---	<link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/css/ui.jqgrid.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/css/bb.ui.jqgrid.css" />--->
        
 <!---   <script TYPE='text/javascript' src='#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/css/tipsy.css" TYPE="text/css" />--->
    
<!------>




</cfoutput>

<style TYPE="text/css">
	.shadow {
		padding-top:10px;
		<!----moz-box-shadow: 3px 3px 4px #000;
		-webkit-box-shadow: 3px 3px 4px #000;
		box-shadow: 3px 3px 4px #000;
		/* For IE 8 */
		-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')";
		/* For IE 5.5 - 7 */
		filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000');--->
	}
	

</style>


<script>

var firstTimeLoad=1;

var plugInCheck = new Array(5);

//plugInCheck[0] : flash
//plugInCheck[1] : shockwave
//plugInCheck[2] : quicktime
//plugInCheck[3] : windows-media-player
//plugInCheck[4] : html 5

plugInCheck[0] = true;

<!--- audio-plugin check for browser --->
if(!plugInCheck[0])
	$('#listen_babbles').css({'display':'none'});

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;


var b_sort_date = '';
var b_sort_views = '';
var b_sort_votesup = '';
var b_sort_votesdown = '';
var b_sort_notes = '';
var b_sort_length = '';
	
$(function() {
	
	jQuery("#ScriptList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/scripts.cfc?method=GetSimplePhoneListRecording&flash='+plugInCheck[0]+'&socialNetwork='+socialNetwork+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: "auto",
		//colNames:['SNo','Script Notes','Play Length','Options'],
		colModel:[			
			{name:'DESC_VCH',index:'DESC_VCH', width:340, editable:false},
			{name:'decimalformat(GetRecording.Length_int/1024)',index:'Length_int', width:40, editable:false,align:'center'},
			{name:'option_col',index:'', width:210, editable:false,align:'center'},
			{name:'del_col',index:'', width:50, editable:false,align:'right'}
		],
		rowNum:7,
	   	//rowList:[5,10,15,20,25],
		mtype: "POST",
		pager: jQuery('#pagerb'),
		toppager: true,
		emptyrecords: "Nothing to display or login session has expired.",
		pgbuttons: true,
		altRows: true,
		altClass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'DATAID_INT',
		toolbar:[true,"top"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
		
		<!---ondblClickRow: function(ContactString_vch){ jQuery('#ScriptList').jqGrid('editRow',ContactString_vch,true, '', '', '', '', rx_AfterEdit,'', ''); },--->
		onSelectRow: function(ContactString_vch){
				if(ContactString_vch && ContactString_vch!==lastsel){
					jQuery('#ScriptList').jqGrid('restoreRow',lastsel);
					//jQuery('#ScriptList').jqGrid('editRow',ContactString_vch,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=ContactString_vch;
				}
			},
		loadComplete: function(data){ 		 
				
				$(".del_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_Row").click( function() { DelScripts($(this).attr('rel')); } );
				
				$(".edit_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_Row").click( function() { EditScripts($(this).attr('rel')); } );
				
				
				if(firstTimeLoad==1 && socialNetwork==1)
				{
					firstTimeLoad=0;
					$("#inpCategoryId").dropdownReplacement({optionsDisplayNum: 5,onSelect : function(value, text, selectIndex){
						CategoryFilterId = value;
						gridScriptListReload('filter');
					}});
					
					var $o = $("#babbleTypeDiv");
					$("#babbleTypeInput").dropdownReplacement({options: $o});
				}
				
				var babbleTypeChoice = $(".dd-all").eq(1).val();
				
				if(babbleTypeChoice  == '-- all --')
				{
					$("#babbleTypeFilter_img").attr("src","../images/icons16x16/all-icon.gif");
					$("#babbleTypeFilter_img").attr("title","All Babbles");
				}
				else if(babbleTypeChoice  == 'Public')
				{
					$("#babbleTypeFilter_img").attr("src","../images/icons16x16/public-icon.gif");
					$("#babbleTypeFilter_img").attr("title","Public Babbles");
				}
				else if(babbleTypeChoice  == 'Private')
				{
					$("#babbleTypeFilter_img").attr("src","../images/icons16x16/lock-icon.gif");
					$("#babbleTypeFilter_img").attr("title","Private Babbles");
				}
				else if(babbleTypeChoice  == 'Friend')
				{
					$("#babbleTypeFilter_img").attr("src","../images/icons16x16/key-icon.gif");
					$("#babbleTypeFilter_img").attr("title","Friend Babbles");
				}
								
				var titleChange = 0;
				$('#ScriptList').mouseover(function() {
					
					if(titleChange < 1)
					{
						var loopC = $('#ScriptList > tbody > tr').length;
						for(var i = 1 ; i < loopC ; i++)
						{
							$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0)").attr({"title":''});
							$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0) > span.babbleDesc").tipsy({gravity: "sw"});
							$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0) > p > span.category").tipsy({gravity: "sw"});
						}
						titleChange++;
					}
						
				});
				
   			},	
		
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/recording.cfc?method=UpdateRecordingData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "CELL",
			  id: "ID" //will default first column as ID
			  }	
			  
			  
	});
	 
	 
	$('#gview_ScriptList .ui-jqgrid-hdiv').hide();
	$('#t_ScriptList').hide();
	$('#pagerb').hide();
	$("#ScriptList").jqGrid('hideCol', 'cb');

	$("#ScriptList_toppager_left").html("<cfinclude template='dsp_topLeft' />");

	$("#inpGroupId").change(function() 
    { 
		LastGroupId = $("#inpGroupId").val();
		gridScriptListReload();

	});
	
	$("#RecordBabble").unbind();	
	$("#RecordBabble").click(function() { MicRecordDialog(); return false; });
	
	$("#upload_record").unbind();	
	$("#upload_record").click(function() { UploadRecordingDialog(); return false; });
	
	$("#call_record").unbind();	
	$("#call_record").click(function() { Call2RecordDialog(); return false; });
	
	$("#text_record").unbind();
	$("#text_record").click(function() { Text2SpeechDialog(); return false; });
	
	$("#del_Script").unbind();
	$("#del_Script").click(function() { DelScripts(); return false; });
	
	$("#listen_babbles").unbind();
	$("#listen_babbles").click(function() { listenScripts(); return false; });

	$("#reloadBabble_all").unbind();
	$("#reloadBabble_all").click(function(){gridScriptListReload()});
	
	$("#reloadBabble_private").unbind();
	$("#reloadBabble_private").click(function(){gridScriptListReload('private')});
	
	$("#reloadBabble_friend").unbind();
	$("#reloadBabble_friend").click(function(){gridScriptListReload('friend')});
	
	$("#reloadBabble_public").unbind();
	$("#reloadBabble_public").click(function(){gridScriptListReload('public')});
	
	$("#refresh_BabbleList").unbind();
	$("#refresh_BabbleList").click(function(){gridScriptListReload()});
	
	$(".ListIconLinksSmall").unbind();
	$(".ListIconLinksSmall").click(function(){
				if($('#search_script').val()!='' && $('#search_script').val()!='TYPE here to search...' ){
					$('#search_script').val('TYPE here to search...'); 
					$('#search_script').css({'color':'#999'});
					gridScriptListReload();
				}
		});
	
	$(".sort_class").unbind();
	$(".sort_class").hover(function(){$(".nav>li>ul").css({'top':'21px'});});
	$(".sort_class").mouseleave(function(){$(".nav>li>ul").css({'top':'-999em'});});
	
	$(".sort_submenu").unbind();
	$(".sort_submenu").hover(function(){$(".nav>li>ul").css({'top':'21px'});});
	$(".sort_submenu").mouseleave(function(){$(".nav>li>ul").css({'top':'-999em'});});
	
	$("#search_script").unbind();
	$("#search_script").keyup(function(){
		var isVal = $("#search_script").val();
		if(isVal != '')
		{
			$("#search_script").addClass("searchbox");
		}
	});
	
	
	$("#babble_sort_date").unbind();
	$("#babble_sort_date").click(function()
									{
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_views=''; b_sort_votesup=''; b_sort_votesdown=''; b_sort_notes='';b_sort_length;
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_date_spanimg').css({'display':''});
										
										if(b_sort_date === '' || b_sort_date === 'ASC')
										{	$(".sort_class").html('Date Inserted &nbsp;&nbsp;<img src="../images/icons/down-icon.png" style="vertical-align:middle;" />'); b_sort_date = 'DESC';  $('#babble_sort_date_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_date_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('Date Inserted &nbsp;&nbsp;<img src="../images/icons/up-icon.png" style="vertical-align:middle;" />'); b_sort_date = 'ASC';  $('#babble_sort_date_spanimg_up').css({'opacity':'1'});  $('#babble_sort_date_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);
	
	$("#babble_sort_views").unbind();
	$("#babble_sort_views").click(function() 
									{ 
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_date=''; b_sort_votesup=''; b_sort_votesdown=''; b_sort_notes='';b_sort_length;
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_views_spanimg').css({'display':''});
										
										if(b_sort_views === '' || b_sort_views === 'ASC')
										{	$(".sort_class").html('Views &nbsp;&nbsp;<img src="../images/icons/down-icon.png"  style="vertical-align:middle;" />'); b_sort_views = 'DESC';  $('#babble_sort_views_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_views_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('Views &nbsp;&nbsp;<img src="../images/icons/up-icon.png"  style="vertical-align:middle;" />'); b_sort_views = 'ASC';  $('#babble_sort_views_spanimg_up').css({'opacity':'1'});  $('#babble_sort_views_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);
	
	$("#babble_sort_voteUp").unbind();
	$("#babble_sort_voteUp").click(function()
									{
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_date=''; b_sort_views=''; b_sort_votesdown='';  b_sort_notes='';b_sort_length;
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_voteUp_spanimg').css({'display':''});
										
										if(b_sort_votesup === '' || b_sort_votesup === 'ASC')
										{	$(".sort_class").html('Votes Up &nbsp;&nbsp;<img src="../images/icons/down-icon.png" style="vertical-align:middle;"  />'); b_sort_votesup = 'DESC';  $('#babble_sort_voteUp_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_voteUp_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('Votes Up &nbsp;&nbsp;<img src="../images/icons/up-icon.png"  style="vertical-align:middle;" />'); b_sort_votesup = 'ASC';  $('#babble_sort_voteUp_spanimg_up').css({'opacity':'1'});  $('#babble_sort_voteUp_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);
	
	$("#babble_sort_voteDown").unbind();
	$("#babble_sort_voteDown").click(function()
									{
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_date=''; b_sort_views=''; b_sort_votesup =''; b_sort_notes='' ;b_sort_length;
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_voteDown_spanimg').css({'display':''});
										
										if(b_sort_votesdown === '' || b_sort_votesdown === 'ASC')
										{	$(".sort_class").html('Votes Down &nbsp;&nbsp;<img src="../images/icons/down-icon.png" style="vertical-align:middle;"  />'); b_sort_votesdown = 'DESC';  $('#babble_sort_voteDown_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_voteDown_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('Votes Down &nbsp;&nbsp;<img src="../images/icons/up-icon.png" style="vertical-align:middle;"  />'); b_sort_votesdown= 'ASC';  $('#babble_sort_voteDown_spanimg_up').css({'opacity':'1'});  $('#babble_sort_voteDown_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);
	
	$("#babble_sort_notes").unbind();
	$("#babble_sort_notes").click(function()
									{
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_date=''; b_sort_views=''; b_sort_votesup =''; b_sort_votesdown='' ;b_sort_length;
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_notes_spanimg').css({'display':''});
										
										if(b_sort_notes == 0 || b_sort_notes === 'ASC')
										{	$(".sort_class").html('<cfoutput>#sname#</cfoutput> Notes &nbsp;&nbsp;<img src="../images/icons/down-icon.png" style="vertical-align:middle;"  />'); b_sort_notes = 'DESC';  $('#babble_sort_notes_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_notes_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('<cfoutput>#sname#</cfoutput> Notes &nbsp;&nbsp;<img src="../images/icons/up-icon.png" style="vertical-align:middle;"  />'); b_sort_notes= 'ASC';  $('#babble_sort_notes_spanimg_up').css({'opacity':'1'});  $('#babble_sort_notes_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);
	$("#babble_sort_length").unbind();
	$("#babble_sort_length").click(function()
									{
										$(".nav>li>ul").css({'top':'-999em'});
										b_sort_date=''; b_sort_views=''; b_sort_votesup =''; b_sort_votesdown='';b_sort_notes='';
										$('.sort_submenu > li > a > span').css({'display':'none'});
										$('#babble_sort_length_spanimg').css({'display':''});
										
										if(b_sort_length == 0 || b_sort_length === 'ASC')
										{	$(".sort_class").html('<cfoutput>#sname#</cfoutput> length &nbsp;&nbsp;<img src="../images/icons/down-icon.png" style="vertical-align:middle;"  />'); b_sort_length = 'DESC';  $('#babble_sort_length_spanimg_up').css({'opacity':'0.3'});  $('#babble_sort_length_spanimg_down').css({'opacity':'1'});  }
										else
										{	$(".sort_class").html('<cfoutput>#sname#</cfoutput> length &nbsp;&nbsp;<img src="../images/icons/up-icon.png" style="vertical-align:middle;"  />'); b_sort_length= 'ASC';  $('#babble_sort_length_spanimg_up').css({'opacity':'1'});  $('#babble_sort_length_spanimg_down').css({'opacity':'0.3'});  }
											
										gridScriptListReload();
									}
								);						
								
								
	
	
	<!---$("#inpCategoryId").change(function() 
    { 
		alert('hello');
		CategoryFilterId = $("#inpCategoryId").val();
		gridScriptListReload('filter');

	});--->
	
	$("#tab-panel-Scripts").toggleClass("ui-tabs-hide");
	
	$('#dock_Scripts').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_Scripts',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);	
	
	$("#loading").hide();
	
		
});





function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}



<!--- Global so popup can refernece it to close it--->
var uploadRecordID = 0;
var call2RecordID = 0;
var ttsID = 0;
var delScriptID = 0;
var listenScriptID = 0;
var editID=0;
var CategoryFilterId= -1;

function UploadRecordingDialog()
{				
	var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	if(uploadRecordID != 0)
	{
		uploadRecordID.remove();
		uploadRecordID = 0;
		
	}
					
	uploadRecordID = $('<div></div>').append(loading.clone());
	
	uploadRecordID
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_UploadScript')
		.dialog({
			modal : true,
			title: 'Upload Recorded Audio File',
			width: 400,
			height: 350
		});

	uploadRecordID.dialog('open');
	
	
	return false;		
}
	
	
function Call2RecordDialog()
{				
	var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	if(call2RecordID != 0)
	{
		call2RecordID.remove();
		call2RecordID = 0;
	}
					
	call2RecordID = $('<div></div>').append(loading.clone());
	
	call2RecordID
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_Call2Record')
		.dialog({
			modal : true,
			title: 'We will call and record your response.',
			draggable: true,
			disabled: true,
			width: 500,
			height: 350,
			
			
		});
	
	call2RecordID.dialog('open');
	
		
	return false;
}

function Text2SpeechDialog()
{
	var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	if(ttsID != 0)
	{
		ttsID.remove();
		ttsID = 0;
		
	}
					
	ttsID = $('<div></div>').append(loading.clone());
	
	ttsID
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_tts')
		.dialog({
			modal : true,
			title: 'Text-2-Speech',
			width: 450,
			height: 500
		});

	ttsID.dialog('open');
	
			
	return false;			
}


function DelScripts(inpScriptList)
{
	var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	//var inpScriptList = $("#ScriptList").jqGrid('getGridParam','selarrrow');
	
	<!--- Dont go overboard on displaying too much info --->
	var displayScripts =  Left(inpScriptList, 50);
	
	if(String(inpScriptList).length > 50)
		displayScripts = displayScripts + " (...)";
		<!---inpScriptList.length--->
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to remove this babbles \n\nAre you absolutely sure?", "About to delete babble(s) from the babble list.", function(result) { if(!result){$("#loading").hide(); return;}else{	
		
		  $("#loading").show();		
				
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/scripts.cfc?method=RemoveScripts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { inpScriptList: String(inpScriptList)},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{						
								gridScriptListReload();						     
							}
																										
							$("#loading").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loading").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridScriptListReload();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;							
	
	
}


function EditScripts(inpScriptId)
{
	var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	if(editID != 0)
	{
		editID.remove();
		editID = 0;
		
	}
	 				
	editID = $('<div></div>').append(loading.clone());
	
	editID
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_editScript?inpScriptId='+inpScriptId)
		.dialog({
			modal : true,
			title: 'Edit Babble',
			width: 450,
			height: 370
		});

	editID.dialog('open');
			
	return false;
}


var titleChange = 0;

var flAuto_script = true
var timeoutHnd;

function doSearch(ev){
	
	if(!flAuto_script)
		return;

	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridScriptListReload,500)
}

function gridScriptListReload(access_type){
	
	var at=-1;
	var search_scriptNum = jQuery("#search_scriptNum").val();
	var search_scriptNotes = $("#search_script").val();
	
	if(search_scriptNotes==='TYPE here to search...')
		search_scriptNotes='';
	
	var sidx = 'DATAID_INT';
	var sord = 'ASC';
	
	if(access_type === undefined)
		at=-1;
	else if(access_type==='public')
		at=2;
	else if(access_type==='friend')
		at=1;
	else if(access_type==='private')
		at=0;
	//else if(access_type==='filter')
		//CategoryFilterId = access_type;
	
	if(b_sort_date != '')
	{
		sidx = 'created_dt';
		sord = b_sort_date;
	}
	else if(b_sort_views != '')
	{
		sidx = 'viewCount_int';
		sord = b_sort_views;	
	}
	else if(b_sort_votesup != '')
	{
		sidx = 'votesUp_int';
		sord = b_sort_votesup;	
	}
	else if(b_sort_votesdown != '')
	{
		sidx = 'votesDown_int';
		sord = b_sort_votesdown;	
	}
	else if(b_sort_notes != '')
	{
		sidx = 'DESC_VCH';
		sord = b_sort_notes;	
	}
	else if(b_sort_length != '')
	{
		sidx = 'Length_int';
		sord = b_sort_length;	
	}
		
	jQuery("#ScriptList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/scripts.cfc?method=GetSimplePhoneListRecording&returnformat=json&flash="+plugInCheck[0]+"&socialNetwork="+socialNetwork+"&queryformat=column&_cf_nodebug=true&_cf_nocache=true&search_scriptNum="+search_scriptNum+"&access_type="+at+"&category="+CategoryFilterId+"&sidx="+sidx+"&sord="+sord+"&search_scriptNotes="+search_scriptNotes,page:1}).trigger("reloadGrid");
	
	
	titleChange = 0;
	
	$('#ScriptList').mouseover(function() {
					
			if(titleChange < 1)
			{
				var loopC = $('#ScriptList > tbody > tr').length;
				for(var i = 1 ; i < loopC ; i++)
				{
					$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0)").attr({"title":''});
					$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0) > span.babbleDesc").tipsy({gravity: "sw"});
					$("#ScriptList > tbody > tr:eq("+i+") > td:eq(0) > p > span.category").tipsy({gravity: "sw"});
				}
				titleChange++;
			}
				
		});
	
}


<!--- Global so popup can refernece it to close it--->
var CreateMicRecordDialog = 0;
	
function MicRecordDialog(inpBRDialString)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(inpBRDialString) != "undefined" && inpBRDialString != "")					
		ParamStr = '?inpBRDialString=' + encodeURIComponent(inpBRDialString);
						
	<!--- Erase any existing dialog data --->
	if(CreateMicRecordDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateMicRecordDialog.remove();
		CreateMicRecordDialog = 0;
		
	}
					
	CreateMicRecordDialog = $('<div></div>').append($loading.clone());
	
	CreateMicRecordDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_MicRecord' + ParamStr)
		.dialog({
			modal : true,
			title: 'Record from Microphone',
			width: 510,
			height: 'auto'
		});

	CreateMicRecordDialog.dialog('open');

	return false;		
}
	
	


</script>





<style TYPE="text/css">
	#ScriptList {
		width:100% !important;
		border:0px !important;
	}
	#ScriptList tr td {
		border:0px !important;
		padding:5px 2px 5px 2px !important;
	}
	#ScriptList tr td span.timeDisplay {
		font-size:10px !important;
		color:#999 !important;
		margin-top:5px !important;
	}
	#ScriptList tr td p.metaBabble {
		margin-top: 8px !important;
		font-size:10px !important;
		color:#999 !important;
	}
	.ui-tabs .ui-tabs-panel { display: block; border-width: 0 1px 1px 1px; padding: 0.5em; background: none; }
	
	<!---span.publicBabble {
		border:solid 1px #904B74;
		background-color:#ffffff;
		padding:1px;
		-moz-border-radius: 100%;
	}
	span.friendBabble {
		border:solid 1px #904B74;
		background-color:#A13D69;
		color:white;
		padding:1px;
		-moz-border-radius: 100%;
	}
	span.privateBabble {
		border:solid 1px #11111;
		background-color:#444444;
		color:white;
		padding:1px;
		-moz-border-radius: 100%;
	}--->
	
	span.babbleDesc {
		font-size:12px;
		color:#000;
	}
	
	.public-icon{
		background:url('../images/icons16x16/public-icon.gif') no-repeat; 
		padding: 6px 0px 0px 20px !important;
	}
	.private-icon{
		background:url('../images/icons16x16/lock-icon.gif') no-repeat;
		padding: 6px 0px 0px 20px !important;
	}
	.friend-icon{
		background:url('../images/icons16x16/key-icon.gif') no-repeat;
		padding: 6px 0px 0px 20px !important;
	}
	.all-icon{
		background:url('../images/icons16x16/all-icon.gif') no-repeat;
		padding: 6px 0px 0px 20px !important;
	}
	
</style>
<script TYPE="text/javascript">
	if(socialNetwork == 1)
	{
		$(document).ready(function(){
		 
				$("#nav-one li").hover(
					function(){ $("ul", this).fadeIn("fast"); }, 
					function() { } 
				);
			if (document.all) {
					$("#nav-one li").hoverClass ("sfHover");
				}
		  });
	  
			$.fn.hoverClass = function(c) {
				return this.each(function(){
					$(this).hover( 
						function() { $(this).addClass(c);  },
						function() { $(this).removeClass(c); }
					);
				});
			};	  
  }
</script>

<div style="clear:both;"></div>
<!---<div>
	<cfinclude template="../scripts/dsp_scriptsToolbarII.cfm">
</div>--->

<style TYPE="text/css">

.printable {
	border: 1px dotted #CCCCCC ;
	padding: 10px 10px 10px 10px ;
	font:Verdana, geneva, sans-serif;
}
.dock-container { 
	position: relative; top: -8px; height: 50px; padding-left: 20px; z-index:1; display:inline;  
}
</style>

<!---<style>
      
      select {
        background: rgba(255,255,255,0.1) url('http://www.google.com/images/srpr/nav_logo6g.png') repeat-x 0 0; 
        padding:4px; 
        line-height: 21px;
        border: 1px solid #fff;
      }
    </style>--->

		
<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>

<!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)---> 
    <div id="tab-panel-Scripts" class="ui-tabs-hide">
    
		<!---<table id="ScriptList"></table>--->
        <div style="table-layout:fixed; min-height:475px; height:475px;">
        	<div id="pagerb"></div>
            <div style="text-align:left;">
                <table id="ScriptList"></table>
            </div>
            <div id='BabbleListSubMenu' style="text-align:left; margin 2px 0 2px 0;">
                <table style="table-layout:fixed; width:100%">
                	<cfif Application.socialNetwork>
                        <td nowrap="nowrap">
                            <p style="display:inline;">
                                <label style="margin: 0px 0 1px 0; text-align:left; font-size:10px; display:block;">Category filter</label>
                                <select name="inpCategoryId" id="inpCategoryId" style="width:130px; display:inline; margin-bottom:3px;" class="ui-corner-all" >
                                    <option value="-1" id="category_none_script" selected="selected"> -- all --</option>
                                    <cfoutput query="getCategories">
                                        <option value="#categoryid_int#" id="cat#categoryid_int#">#categoryDesc_vch#</option>
                                    </cfoutput>
                                </select>
                            </p>
                        </td>
                        <td nowrap="nowrap">
                            <p style="display:inline;">
                                <label style="margin: 0px 0 1px 0; text-align:left; font-size:10px; display:block;">
                                	Babble TYPE filter <img src="../images/icons16x16/all-icon.gif" id="babbleTypeFilter_img" title="All Babbles" width="10px" />
                                </label>
                                <input value="" name="babbleTypeInput" id="babbleTypeInput" style="display: none;">
                                <div style="display:none;" id="babbleTypeDiv" align="center">
                                    <a id="reloadBabble_all" name="name1" class="all-icon">-- all --</a>
                                    <a id="reloadBabble_public" name="name2" class="public-icon" >Public</a>
                                    <a id="reloadBabble_friend" name="name3" class="friend-icon">Friend</a>
                                    <a id="reloadBabble_private" name="name4" class="private-icon">Private</a>
                                </div>
                                
                            </p>
                        </td>
                    </cfif>
                        <td nowrap="nowrap">
                        	<p style="display:inline;">
                                <label style="margin-left:0px; margin-bottom:1px; text-align:left; font-size:10px; display:block;">Notes Filter</label>
                                <span class="notesFilterHolder">
                                    <input TYPE="text" id="search_script" class="notesFilter ui-corner-all" onkeydown="doSearch(arguments[0]||event)" value="TYPE here to search..." onfocus="focusedTextBox('search_script','TYPE here to search...')" onblur="bluredTextBox('search_script','TYPE here to search...')"/>
                                    <img src="../images/icons16x16/delete_10x10.png" class="ListIconLinksSmall" />
                                </span>
                            </p>
                        </td>
                        <td nowrap="nowrap" align="center" valign="middle">
                        	<div style="float:left; padding-top:0px" align="left">
                                <ul id='nav-one' class='nav'>
                                    <li class="shadow">
                                        <a href='#item1' class="sort_class" >Sorting &nbsp;&nbsp;<img src="../images/icons/up-icon.png" style="vertical-align:middle;"  /> <img src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></a>
                                        <ul class="sort_submenu shadow">
                                            <li>
                                                <a id='babble_sort_date'>Date Inserted 
                                                    <span id="babble_sort_date_spanimg" style="display:none;"><img id="babble_sort_date_spanimg_up" src="../images/icons/up-icon.png" style="vertical-align:middle;"  /> <img id="babble_sort_date_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a id='babble_sort_notes'><cfoutput>#sname#</cfoutput> Notes
                                                    <span id="babble_sort_notes_spanimg" style="display:none;"><img id="babble_sort_notes_spanimg_up" src="../images/icons/up-icon.png" style="vertical-align:middle;"  /> <img id="babble_sort_notes_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a id='babble_sort_length'><cfoutput>#sname#</cfoutput> Length 
                                                    <span id="babble_sort_length_spanimg" style="display:none;"><img id="babble_sort_length_spanimg_up" src="../images/icons/up-icon.png" style="vertical-align:middle;"  /> <img id="babble_sort_length_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            <cfif Application.socialNetwork>
                                            <li>
                                                <a id='babble_sort_views'>Views 
                                                    <span id="babble_sort_views_spanimg" style="display:none;"><img id="babble_sort_views_spanimg_up" src="../images/icons/up-icon.png" style="vertical-align:middle;"  /> <img id="babble_sort_views_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a id='babble_sort_voteUp'>Votes Up 
                                                    <span id="babble_sort_voteUp_spanimg" style="display:none;"><img id="babble_sort_voteUp_spanimg_up" src="../images/icons/up-icon.png"  style="vertical-align:middle;" /> <img id="babble_sort_voteUp_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a id='babble_sort_voteDown'>Votes Down 
                                                    <span id="babble_sort_voteDown_spanimg" style="display:none;"><img id="babble_sort_voteDown_spanimg_up" src="../images/icons/up-icon.png"  style="vertical-align:middle;" /> <img id="babble_sort_voteDown_spanimg_down" src="../images/icons/down-icon.png" style="vertical-align:middle;"  /></span>
                                                </a>
                                            </li>
                                            </cfif>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </td>
                </table> 
            </div>
                
		</div>


 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dock_Scripts" style="text-align:left;">
        	<div class="dock-container_Scripts">
            	<a class="dock-item" id="RecordBabble" ><span>Record Babble</span><img src="../images/dock/Add_Web64x64.png" alt="Record Babble by Microphone" /></a>
       		    <a class="dock-item"  id="upload_record"><span>Upload Babble</span><img src="../images/dock/upload_babble64x64.png" alt="Upload Babble" /></a> 
                <a class="dock-item"  id="call_record"><span>Call-2-Record Babble</span><img src="../images/dock/callToRecord_babble64x64.png" alt="Call-2-Record Babble" /></a> 
                <a class="dock-item"  id="text_record" style="width:400px"><span>Text-2-Speech Babble</span><img src="../images/dock/tts_babble64x64.png" alt="Text-2-Speech Babble" /></a>  
            </div>
        </div>

	</div>

