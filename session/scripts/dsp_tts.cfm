<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">



<!--- for uploading audio files --->
<cfoutput>
	<script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/js/jquery.filestyle.js"></script>
</cfoutput>


<cfoutput>
<script type="text/javascript">
	var dtsid = 0;
	var CurrentProcess = 0;
	var totalLen = 1000;
	
	ttsID.bind("dialogbeforeclose",function(event, ui) {
				if(CurrentProcess==0)
					return true;
				else{
					$.alerts.okButton = '&nbsp;Yes&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure to cancel process and exit?", "About to cancel Text-2-Speech.", function(result) { 
									if(result)
									{	
										ttsID.dialog('destroy'); 
									}
									return result;
								});
					return false;
				}
					
		});
	
	function ttsCall()
	{
		CurrentProcess = 1;
		$('##loadingDlgtts').css({'display':''});
		
		$.getJSON("../cfc/scripts.cfc?method=checkDirectory&returnformat=json&queryformat=column", {}, function(res,code) {
			if(parseInt(res) == 1)
			{
				var uText = document.getElementById('inpuserText').value;
				var vt = document.getElementById('voiceTalent').selectedIndex + 1;
				$.getJSON("../cfc/scripts.cfc?method=tts&text="+encodeURIComponent(uText)+"&category="+$('##ttsDiv ##category').val()+"&access_type="+$('input:radio[name=access_type]:checked').val()+"&notes="+encodeURIComponent($('##tts_inpNotes').val())+"&voice="+vt+"&returnformat=json&queryformat=column", {}, function(res,code) {
					
					<!---if(parseInt(res)!=0)
					{
						CurrentProcess = 0;
						gridScriptListReload();
						$('##loadingDlgtts').css({'display':'none'});
						$('##tts_success').css({'display':''});
						stream_publish(parseInt(res));
					}
					else
					{
						alert('Sorry! there is some problem with TTS at present moment');
					}--->
					if (res.ROWCOUNT > 0) 
							{		
								//alert(d.ROWCOUNT)																							
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(res.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = parseInt(res.DATA.RXRESULTCODE[0]);	
									
									if(CurrRXResultCode > 0)
									{
										CurrentProcess = 0;
										gridScriptListReload();
										$('##loadingDlgtts').css({'display':'none'});
										$('##tts_success').css({'display':''});
										<cfif session.facebook OR session.fbuserid>
											stream_publish(CurrRXResultCode,res.DATA.MSG[0]);
										</cfif>
									}
									else
									{
										<!--- Unsuccessful Login --->
										<!--- Check if variable is part of JSON result string   res.DATA.CCDXMLString[0]  --->								
										if(typeof(res.DATA.MSG[0]) != "undefined")
										{		
											alert(res.DATA.MSG[0]);
											//top.window.location = 'http://apps.facebook.com/babblesphere/';	
											//jAlertOK('Sorry a problem occured with Facebook authorization!!','Failure!',function(result) {top.window.location = 'http://apps.facebook.com/babblesphere/'; return false; });
											//top.window.location = 'http://apps.facebook.com/babblesphere/';
										}		
									}
								}
								else
								{<!--- Invalid structure returned --->	
									// Do something									
								}
							}
							else
							{<!--- No result returned --->
								// Do something									
							}
				});
				
			
				
			}
			else
			{
				jAlert('Error creating directory!!');	
			}
		});
	}
	
	function stream_publish(dataId,msg)
	{
		FB.ui({
		  method: "stream.publish",
		  display: "iframe",
		  user_message_prompt: "Publish This!",
		  auto_publish:true,
		  message: "My new recorded babble",
		  attachment: {
			 name: "New Babble",
			 caption: "This is my newly published babble !!",
			 description: "Listen it here",
			 href: "http://example.com/",
			 media:[{
				 	"type":"mp3",
					"src":"#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/scripts/act_GetMyMP3?inpUserId=#session.userid#&inpLibId=1&inpEleId=1&inpScriptId="+dataId,
					<!---"src":"http://dev.telespeech.com/simplexscripts/u8/l1/e1/RXDS_8_1_1_7.mp3",--->
					"title":msg,
					"artist":"Deepak yadav"
					}],
			<!---properties:{
			   "1)":{"text":"Reading","href":"http://example.com/skill.php?reading"},
			   "2)":{"text":"Math","href":"http://example.com/skill.php?math"},
			   "3)":{"text":"Farmville","href":"http://example.com/skill.php?farmville"}
			 }--->
		  },
		  action_links: [{ text: 'Test yourself', href: 'http://example.com/test.php' }]
		 },
		 function(response) {
		   if (response && response.post_id) {
			 //alert('Post was published.'+response.post_id);
			 $.getJSON("../cfc/scripts.cfc?method=updateFacebookScriptId&id="+response.post_id+"&scriptid="+dataId+"&returnformat=json&queryformat=column", {}, function(res,code) {
				if(parseInt(res) == 1)
				{
					
				}
			 });
			 
		   } else {
			 //alert('Post was not published.');
		   }
		 }
		);
			
	}
	
	
	function continue_tts()
	{
		$('##inpuserText').val('');
		$('##tts_inpNotes').val('');
		$('##tts_success').css({'display':'none'});
	}
	
	function exit_tts()
	{
		$("##loadingDlgcall2Record").hide();
		ttsID.dialog('destroy');
		gridScriptListReload();	
	}
	
	function countCharacter()
	{
		var curLen = $('##inpuserText').val().length;
		
		if(curLen >= 1000)
		{
			$('##charLimDsp').html('<span style="color:red">1000 character limit reached.</span>');
			$('##inpuserText').val($('##inpuserText').val().substring(0,1000));
		}
		else if(curLen > 750)
		{
			var rem = 1000 - curLen;
			$('##charLimDsp').html('<span style="color:##FFAF00">'+ rem + ' characters remaining only.</span>');	
		}
		else if(curLen < 1000)
			$('##charLimDsp').html(1000- $('##inpuserText').val().length + ' characters remaining.');
	}
	
	$("##ttsDiv ##category").click(function(){
				
				if(document.getElementById('category_none_tts').selected)
				{
					$("select option:selected").each(function () {$(this).attr("selected", false)});
					document.getElementById('category_none_tts').selected = true;
				}
			}
		)
	
	$("##ttsDiv ##Cancel").click( function() {$("##loadingDlgtts").hide(); ttsID.dialog('destroy'); return false;  }); 		
</script>

</cfoutput>

<style>

#ttsDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#ttsDiv div{
display:inline;
border:none;
}

</style> 



<cfquery name="getNumber" datasource="#Session.DBSourceEBM#">
	select 	workphonestr_vch as pn
    from 	rx..rx_user
    where 	userid_int = #session.userid#
</cfquery>
<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>

<cfoutput>
    <div id='ttsDiv' class="RXForm" style="font-size:12px">
        <cfform name="ttsForm" id="ttsForm" action="">
            <label>Please enter text below <span class="small" id="charLimDsp">1000 characters limit</span></label><br>
            <cftextarea id="inpuserText" name="inpuserText" cols="65" rows="4" onKeyUp="countCharacter()"></cftextarea><br /><br />
            <label>Voice Type <span class="small">select the voice talent</span></label>
            <select name="voiceTalent" id="voiceTalent">
                <option id="1" value="1">Joe</option>
                <option id="2" value="2">Samantha</option>
            </select>
            
            <label>Script Notes <span class="small">Notes or Data</span></label>
            <input type="text" name="tts_inpNotes" id="tts_inpNotes"/>
            
                      
            <label>Choose Category
            <span class="small">Type of Babble</span>
            </label>
            <select name="category" id="category" multiple="multiple" size="4">
                <option value="0" id="category_none_tts" selected="selected"> -- all --</option>
                <cfloop query="getCategories">
                    <option value="#categoryid_int#" id="cat#categoryid_int#">#categoryDesc_vch#</option>
                </cfloop>
            </select>
            
            <label>Access Type</label>
            <input type="radio" name="access_type" id="access_public" value="2" checked="checked" style="display:inline;width:10px" /><span class="small">Public</span>
            <input type="radio" name="access_type" id="access_friend" value="1"  style="display:inline;width:10px"  /><span class="small">Friend</span>
            <input type="radio" name="access_type" id="access_private" value="0" style="display:inline;width:10px"  /><span class="small">Private</span>
            <br /><Br />
        	
            <button id="ttsButton" type="button" class="ui-corner-all" onClick="ttsCall();">Text-To-Speech</button>
            <button id="Cancel" type="button" class="ui-corner-all">Cancel</button>
            
            <span id="loadingDlgtts" style="display:none;">
            	<img class="loadingDlgDeleteGroupImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        	</span>
            
            <br>
        </cfform>    
            <span id="tts_success" style="display:none">
                Script saved successfully!!!. Want to upload new text-2-speech?<br />
                <button id="Call2Record_continue"  onclick="continue_tts();">Yes</button>
                <button id="Call2Record_stop" onclick="exit_tts();">No</button><br />
            </span>
        
       
        <div id="loadingDlgtts" style="display:none;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
    </div>
</cfoutput>


