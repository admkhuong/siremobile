<cfscript>
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}

</cfscript>

<!--- :#CGI.SERVER_PORT# --->


<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">



<!--- for uploading audio files --->
<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.filestyle.js"></script>
</cfoutput>

        
<script TYPE="text/javascript">
var CurrentProcess = 0;

	uploadRecordID.bind("dialogbeforeclose",function(event, ui) {
				if(CurrentProcess==0)
					return true;
				else{
					$.alerts.okButton = '&nbsp;Yes&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure to cancel process and exit?", "About to cancel Upload", function(result) { 
							if(result)
							{	
								uploadRecordID.dialog('destroy'); 
							}
							return result;
						});
					return false;
				}
					
		});

	function ajaxFileUpload()
	{	
		CurrentProcess = 1;
		$("#loadingDlgUploadScript").show();
		
		$.ajaxFileUpload
		(
			{
				url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/act_AsyncWriteFile?inpLibId=' + $("#UpLoadScriptDiv #inpLibId").val() + '&inpEleId=' + $("#UpLoadScriptDiv #inpEleId").val() + '&inpDataId=' + $("#UpLoadScriptDiv #inpDataId").val()+'&catIds=' + $('#UpLoadScriptDiv #category').val() + '&access_type='+$('input:radio[name=access_type]:checked').val() + '&notes=' + encodeURIComponent($("#UpLoadScriptDiv #inpNotes").val()),
				secureuri:false,
				fileElementId:'inpFileName',
				dataType: 'json',				
				complete:function()
				{					
					$("#loadingDlgUploadScript").hide();
					
					if(typeof($UploadScriptpopDialog) != 'undefined')
					{
						$UploadScriptpopDialog.dialog('destroy');	
						
					}
					
				},				
				success: function (data, status)
				{	
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							CurrentProcess = 0;
							<cfif session.facebook OR session.fbuserid>
								stream_publish(parseInt(data.id),data.msg);
							<cfelse>
								$("#loadingDlgUploadScript").hide();
							
									$.alerts.okButton = '&nbsp;Yes&nbsp;';
									$.alerts.cancelButton = '&nbsp;No&nbsp;';
									
									jConfirm( "File has been uploaded. Upload more?", "File has been uploaded. Upload more?", function(result) { 
										if(!result)
										{	
											$("#loadingDlgUploadScript").hide();
											uploadRecordID.dialog('destroy');
											<!--- Populate Group list box from AJAX--->	
											gridScriptListReload();
										}
										return result;
									});
							</cfif>
							
						}
					}															
				},
				error: function (data, status, e)
				{					
					alert(e);	
				}
			}
		);
		
		return false;

	}
	
		$("#UpLoadScriptDiv #UploadScript").click( function() {	ajaxFileUpload();	return false;  }); 		
		
		$("#loadingDlgUploadScript").hide();	
		<!--- Kill the new dialog --->
		$("#UpLoadScriptDiv #Cancel").click( function() {$("#loadingDlgUploadScript").hide(); uploadRecordID.dialog('destroy'); return false;  }); 		
		$("#UpLoadScriptDiv #category").click(function(){
				
					if(document.getElementById('category_none_upload').selected)
					{
						$("select option:selected").each(function () {$(this).attr("selected", false)});
						document.getElementById('category_none_upload').selected = true;
					}
				}
			)
	
	
	function stream_publish(dataId,msg)
	{
		FB.ui({
		  method: "stream.publish",
		  display: "iframe",
		  user_message_prompt: "Publish This!",
		  message: "My new recorded babble",
		  attachment: {
			 name: "New Babble",
			 caption: "This is my newly published babble !!",
			 description: "Listen it here",
			 href: "http://example.com/",
			 media:[{
				 	"type":"mp3",
					"src":"<cfoutput>#rootUrl#/#SessionPath#/scripts/act_GetMyMP3?inpUserId=#session.userid#&inpLibId=1&inpEleId=1&inpScriptId="+dataId</cfoutput>,
					<!---"src":"http://dev.telespeech.com/simplexscripts/u8/l1/e1/RXDS_8_1_1_7.mp3",--->
					"title":msg,
					"artist":"Deepak yadav"
					}],
			<!---properties:{
			   "1)":{"text":"Reading","href":"http://example.com/skill.php?reading"},
			   "2)":{"text":"Math","href":"http://example.com/skill.php?math"},
			   "3)":{"text":"Farmville","href":"http://example.com/skill.php?farmville"}
			 }--->
		  },
		  action_links: [{ text: 'Test yourself', href: 'http://example.com/test.php' }]
		 },
		 function(response) {
		   if (response && response.post_id) {
			 alert('Post was published.'+response.post_id);
		   } else {
			 //alert('Post was not published.');
		   }
		    $("#loadingDlgUploadScript").hide();
							
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			
			jConfirm( "File has been uploaded. Upload more?", "File has been uploaded. Upload more?", function(result) { 
				if(!result)
				{	
					$("#loadingDlgUploadScript").hide();
					uploadRecordID.dialog('destroy');
					<!--- Populate Group list box from AJAX--->	
					gridScriptListReload();
				}
				return result;
			});
		 }
		);
	}	
		
</script>


<style>

#UpLoadScript{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#UpLoadScript div{
display:inline;
border:none;
}

</style> 

<cfoutput>
 
<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>
<div id='UpLoadScriptDiv' class="RXForm">

<form name="form" action="" method="POST" enctype="multipart/form-data">

     <input TYPE="hidden" name="inpLibId" id="inpLibId" value="#inpLibId#" />
     <input TYPE="hidden" name="inpEleId" id="inpEleId" value="#inpEleId#" />
     <input TYPE="hidden" name="inpDataId" id="inpDataId" value="#inpDataId#" />
    
     	<label>File Path/Name To Upload 
        <span class="small">TYPE or browse for your file</span>
        </label>
        <input TYPE="file" name="inpFileName" id="inpFileName" size="40" class="ui-corner-all" /> 
       
        <label>Recording Notes
        <span class="small">Notes or Data</span>
        </label>
        <input TYPE="text" name="inpNotes" id="inpNotes"/> 
       
        <label>Choose Category
        <span class="small">TYPE of Babble</span>
        </label>
        <select name="category" id="category" multiple="multiple" size="4">
        	<option value="0" id="category_none_upload" selected="selected"> -- all --</option>
            <cfloop query="getCategories">
            	<option value="#categoryid_int#" id="cat#categoryid_int#">#categoryDesc_vch#</option>
            </cfloop>
        </select>
       
        <label>Access TYPE</label>
        <input TYPE="radio" name="access_type" id="access_public" value="2" checked="checked" style="display:inline;width:10px" /><span class="small">Public</span>
        <input TYPE="radio" name="access_type" id="access_friend" value="1"  style="display:inline;width:10px"  /><span class="small">Friend</span>
        <input TYPE="radio" name="access_type" id="access_private" value="0" style="display:inline;width:10px"  /><span class="small">Private</span>
        <br /><Br />
        <button id="UploadScript" TYPE="button" class="ui-corner-all">Upload</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
        <div id="loadingDlgUploadScript" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
        
</form>
</cfoutput>
</div>
