<cfscript>
	// Fix root url for any host
	rootUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
	if (CGI.SERVER_PORT NEQ 80) {
		rootUrl = "#rootUrl#";
	}
</cfscript>

<!--- :#CGI.SERVER_PORT# --->


<cfparam name="inpLibId" default="1">
<cfparam name="inpEleId" default="1">
<cfparam name="inpDataId" default="0">



<!--- for uploading audio files --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.filestyle.js"></script>
</cfoutput>


        
<script TYPE="text/javascript">
var CurrentProcess = 0;

	$(function() 
	{
		
		
		$("#UpLoadScriptDiv #UploadScript").click( function() {	ajaxFileUpload();	return false;  }); 		
		
		$("#UpLoadScriptDiv #loadingDlgUploadScript").hide();	
		<!--- Kill the new dialog --->
		$("#UpLoadScriptDiv #Cancel").click( function() {$("#UpLoadScriptDiv #loadingDlgUploadScript").hide(); UploadScriptpopDialog.remove(); return false;  }); 		
	
	<!---
		$("#UpLoadScriptDiv #category").click(function(){
				
					if(document.getElementById('category_none_upload').selected)
					{
						$("select option:selected").each(function () {$(this).attr("selected", false)});
						document.getElementById('category_none_upload').selected = true;
					}
				}
			)		
			
	--->
		
	});


	UploadScriptpopDialog.bind("dialogbeforeclose",function(event, ui) {
				if(CurrentProcess==0)
					return true;
				else{
					$.alerts.okButton = '&nbsp;Yes&nbsp;';
					$.alerts.cancelButton = '&nbsp;No&nbsp;';		
					
					jConfirm( "Are you sure to cancel process and exit?", "About to cancel Upload", function(result) { 
							if(result)
							{	
								UploadScriptpopDialog.remove(); 
							}
							return result;
						});
					return false;
				}
					
		});

	function ajaxFileUpload()
	{	
		CurrentProcess = 1;
		$("#UpLoadScriptDiv #loadingDlgUploadScript").show();
				
		$.ajaxFileUpload
		(
			{
				url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/act_AsyncWriteFile?inpLibId=' + $("#UpLoadScriptDiv #inpLibId").val() + '&inpEleId=' + $("#UpLoadScriptDiv #inpEleId").val() + '&inpDataId=' + $("#UpLoadScriptDiv #inpDataId").val()+'&catIds=' + $('#UpLoadScriptDiv #catIds').val() + '&access_type=0&notes=' + encodeURIComponent($("#UpLoadScriptDiv #inpNotes").val()),
				secureuri:false,
				fileElementId:'inpFileName',
				dataType: 'json',				
				complete:function()
				{					
					$("#UpLoadScriptDiv #loadingDlgUploadScript").hide();
					
					if(typeof($UploadScriptpopDialog) != 'undefined')
					{
						$UploadScriptpopDialog.remove();	
						
					}
					
				},				
				success: function (data, status)
				{	
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							CurrentProcess = 0;
							
								$("#UpLoadScriptDiv #loadingDlgUploadScript").hide();
							
									$.alerts.okButton = '&nbsp;Yes&nbsp;';
									$.alerts.cancelButton = '&nbsp;No&nbsp;';
									
									jConfirm( "File has been uploaded. Upload more?", "File has been uploaded. Upload more?", function(result) { 
										if(!result)
										{	
											$("#UpLoadScriptDiv #loadingDlgUploadScript").hide();
											UploadScriptpopDialog.remove();
											<!--- Populate Group list box from AJAX--->	
											<!---gridScriptListReload();--->
										}
										return result;
									});														
						}
					}															
				},
				error: function (data, status, e)
				{					
					alert(e);	
				}
			}
		);
		
		return false;

	}
		
	
		
</script>


<style>

#UpLoadScript{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#UpLoadScript div{
display:inline;
border:none;
}

</style> 

<cfoutput>
 <!---
<cfquery name="getCategories" datasource="#Session.DBSourceEBM#">
	select 	CategoryId_int,CategoryDesc_vch
    from 	simpleobjects.category
</cfquery>
--->
<div id='UpLoadScriptDiv' class="RXForm">

<form name="form" action="" method="POST" enctype="multipart/form-data">

     <input TYPE="hidden" name="inpLibId" id="inpLibId" value="#inpLibId#" />
     <input TYPE="hidden" name="inpEleId" id="inpEleId" value="#inpEleId#" />
     <input TYPE="hidden" name="inpDataId" id="inpDataId" value="#inpDataId#" />
    
     	<label>File Path/Name To Upload 
        <span class="small">TYPE or browse for your file</span>
        </label>
        <input TYPE="file" name="inpFileName" id="inpFileName" size="40" class="ui-corner-all" /> 
       
        <label>Recording Notes
        <span class="small">Notes or Data</span>
        </label>
        <input TYPE="text" name="inpNotes" id="inpNotes"/> 
       
        <input TYPE="hidden" name="access_type" id="access_type" value="0" />
        <input TYPE="hidden" name="catIds" id="catIds" value="0" />
 <!--- 
        <label>Choose Category
        <span class="small">TYPE of Babble</span>
        </label>
        <select name="category" id="category" multiple="multiple" size="4">
        	<option value="0" id="category_none_upload" selected="selected"> -- all --</option>
            <cfloop query="getCategories">
            	<option value="#categoryid_int#" id="cat#categoryid_int#">#categoryDesc_vch#</option>
            </cfloop>
        </select>
       
        <label>Access TYPE</label>
        <input TYPE="radio" name="access_type" id="access_public" value="2" checked="checked" style="display:inline;width:10px" /><span class="small">Public</span>
        <input TYPE="radio" name="access_type" id="access_friend" value="1"  style="display:inline;width:10px"  /><span class="small">Friend</span>
        <input TYPE="radio" name="access_type" id="access_private" value="0" style="display:inline;width:10px"  /><span class="small">Private</span>
        <br /><Br />
---> 
        <button id="UploadScript" TYPE="button" class="ui-corner-all">Upload</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
        <div id="loadingDlgUploadScript" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
        
</form>
</cfoutput>
</div>
























