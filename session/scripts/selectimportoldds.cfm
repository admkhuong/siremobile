<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Copy Old RXDynascript Libaries into EBM System <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Setup Data</cfoutput>');
	$('#subTitleText').html('');	
</script>

<cfif session.userrole NEQ 'SuperUser'>
	You do not permission to use this feature!
	<cfabort>
</cfif>

<cfparam name="INPBATCHID" default="0" type="string">


<script type="text/javascript">


	$(function() {
		
		$("#CopyScriptLibraryButton").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } ); 
			
		$("#CopyScriptLibraryButton").click( function() { PublishScriptLibrary(); return false;  }); 
				
	});

	var $DistributionDialog = 0;

	function PublishScriptLibrary()
	{			
		inpLibId= $("#inpLibId").val();
		inpOldLibId= $("#inpOldLibId").val();
		inpDialerIPAddr= $("#inpDialerIPAddr").val();
		inpUserId= $("#inpUserId").val();
		
		
	
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof($("#inpLibId").val()) == 'undefined')
				inpLibId = 0;
			else
				inpLibId= $("#inpLibId").val();
				
		if(typeof(inpUserId) == 'undefined')
			if(typeof($("#inpUserId").val()) == 'undefined')
				inpUserId = 0;
			else
				inpUserId= $("#inpUserId").val();
				
		if(typeof(inpDialerIPAddr) == 'undefined')
			if(typeof($("#inpDialerIPAddr").val()) == 'undefined')
				inpDialerIPAddr = '0.0.0.0';
			else
				inpDialerIPAddr= $("#inpDialerIPAddr").val();
												
			var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
					

			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/DoImportOldDS?inpLibId=' + inpLibId + '&inpUserId=' + inpUserId + '&inpOldLibId=' + inpOldLibId;		
			return false;						
			
	}

</script>


<style>
/* ----------- stylized ----------- */
#PublishScriptFormDiv{

margin:0 5;
width:950px;
padding:5px;
height:800px;
}


#PublishScriptFormDiv p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #aaaaaa;
padding-bottom:10px;
}

#PublishScriptFormDiv label{
display:block;
font-weight:bold;
text-align:right;
width:400px;
float:left;
}

#PublishScriptFormDiv .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:400px;
}

#PublishScriptFormDiv input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
width:200px;
margin:2px 0 20px 10px;
display:block;
}



#PublishScriptFormDiv select{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
width:200px;
margin:2px 0 20px 10px;
display:block;
}

#PublishScriptFormDiv button{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
margin:2px 0 20px 10px;
display:block;
}

</style>




<!--- Select Library --->

<!--- Select RXDialer --->


<!--- Go button --->
<cfoutput>



	<div id="CopyOldrxds" class="stand-alone-content" style="width:850px;">
 		<div class="EBMDialog">
                              
            <form method="POST">
                            
                <div class="header">
                    <div class="header-text">Copy Old RXDynaScript Library</div>
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
               
                <div class="inner-txt-box">
       
                    <div class="inner-txt-hd">Add a new contact to the selected group</div>
                    <div class="inner-txt"></div>
                                        
                    <div class="form-left-portal">
                                            
                     
                        <div class="inputbox-container">
                            <label for="inpUserId">Target User ID <span class="small">Required</span></label>
                            <input id="inpUserId" name="inpUserId" placeholder="Enter Target User Id Here" size="20" autofocus="autofocus" />
                        </div>
                                                                               
                        <div style="clear:both"></div>
    
                        <div class="inputbox-container">
                            <label for="INPDESC">Target LibID <span class="small">Required - Specify 0 to Auto-Create new Library by default</span></label>
                            <input id="inpLibId" name="inpLibId" placeholder="Enter Target Lib Id Here" size="20" />
                        </div>
                        
                        <div style="clear:both"></div>
                        
                        <div class="inputbox-container">
                            <label for="inpUserId">Old RXDynascript LibID <span class="small">Required</span></label>
                            <input id="inpOldLibId" name="inpOldLibId" placeholder="Enter Target OLD RXDynascript Lib Id Here" size="20" autofocus="autofocus" />
                        </div>
                                               
                       <div style="clear:both"></div>
                       <div style="clear:both"></div>
                                                                                                                              
                    </div>                            
                                           
              
              
                </div>
                
                <div style="clear:both"></div>
                <div style="clear:both"></div>
                
                <div class="submit">                         
                    <a href="##" class="button filterButton small" id="CancelFormButton" >Back</a>
                    <a href="##" class="button filterButton small" id="CopyScriptLibraryButton" >Copy Script Library</a>
                </div>
    
            </form>
               
                                          
                
        </div>     

	</div>

</cfoutput>









