
<cfparam name="inpSFTPURL" default="">
<cfparam name="inpSFTPUID" default="">
<cfparam name="inpSFTPPAssword" default="">
<cfparam name="inpSFTPInDir" default="/">
<cfparam name="inpSFTPFingerPrint" default="">




<cfparam name="inpMSG" default="">

<cfparam name="inpList" default="0">
<cfparam name="inpDownload" default="0">
<cfparam name="inpDelete" default="0">
<cfparam name="inpUpload" default="0">

<cfparam name="inpRemoteFileName" default="">

<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Tools <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> SFTP Tools</cfoutput>');
	$('#subTitleText').html('SFTP SSL Web Tunnel');
</script>


<!--- List - Download - Upload - Directory - Delete File --->


<!--- SFTP Software events to trigger API calls ? --->


<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;

	$(function()
	{				
		 

		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
 				
				
		<!--- List files from SFTP --->		
		$('#SFTPTunnelContainer #inpListSFTP').click(function(){
			
									
			$('#SFTPTunnelContainer .MSGAlert').hide();
			$('#SFTPTunnelContainer #inpSubmit').hide();
			$("#loadingUpdatingStatus").css('visibility', 'visible');
				
			$('#SFTPTunnelContainer #UpdateStatus').html('<span style="float: right;">Connecting to SFTP ...</span>');			
			<!---$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "deductible_a_proc");--->
			$('#SFTPTunnelContainer form#SFTPTunnelForm').submit();
		});
		
		
		<!--- Navigate Directory on SFTP --->		
		$('#SFTPTunnelContainer .inpSFTPChangeDir').click(function(){
			
									
			$('#SFTPTunnelContainer .MSGAlert').hide();
			$('#SFTPTunnelContainer #inpSubmit').hide();
			$("#loadingUpdatingStatus").css('visibility', 'visible');
						
			$("#SFTPTunnelContainer #inpSFTPInDir").val($(this).attr('rel1'))
			
			$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "httpstunnel");
				
			$('#SFTPTunnelContainer #UpdateStatus').html('<span style="float: right;">Connecting to SFTP ...</span>');			
			<!---$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "deductible_a_proc");--->
			$('#SFTPTunnelContainer form#SFTPTunnelForm').submit();
		});
		
		
		<!--- Download file from SFTP --->		
		$('#SFTPTunnelContainer .inpSFTPDownloadFile').click(function(){
			
									
			$('#SFTPTunnelContainer .MSGAlert').hide();
			$('#SFTPTunnelContainer #inpSubmit').hide();
			$("#loadingUpdatingStatus").css('visibility', 'visible');
						
			$("#SFTPTunnelContainer #inpRemoteFileName").val($(this).attr('rel1'))
			
			$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "procdownload");
				
			$('#SFTPTunnelContainer #UpdateStatus').html('<span style="float: right;">Connecting to SFTP ...</span>');			
			<!---$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "deductible_a_proc");--->
			$('#SFTPTunnelContainer form#SFTPTunnelForm').submit();
			
			$("#loadingUpdatingStatus").css('visibility', 'hidden');
		});
		
		
		<!--- Upload file from SFTP --->		
		$('#SFTPTunnelContainer #inpUploadToSFTP').click(function(){
			
									
			$('#SFTPTunnelContainer .MSGAlert').hide();
			$('#SFTPTunnelContainer #inpSubmit').hide();
			$("#loadingUpdatingStatus").css('visibility', 'visible');
			
			$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "procupload");
				
			$('#SFTPTunnelContainer #UpdateStatus').html('<span style="float: right;">Connecting to SFTP ...</span>');			
			<!---$('#SFTPTunnelContainer form#SFTPTunnelForm').attr("action", "deductible_a_proc");--->
			$('#SFTPTunnelContainer form#SFTPTunnelForm').submit();
		});
		
		
		
		
		$('#SFTPTunnelContainer #inpReset').click(function(){
			<!--- Form Validation --->
			window.location = "httpstunnel";
		});
				
	});

	
</script>


<cfoutput>

	<div id="SFTPTunnelContainer" class="stand-alone-content" style="width:1250px;">
        <div class="EBMDialog">

            <form method="POST" id="SFTPTunnelForm" enctype="multipart/form-data">
            
            	<input type="hidden" id="inpSFTPInDir" name="inpSFTPInDir" value="#inpSFTPInDir#" />
                <input type="hidden" id="inpRemoteFileName" name="inpRemoteFileName" value="#inpRemoteFileName#" />
            
            	<div class="header">
                	<div class="header-text" style="padding-top:1px !important;">SFTP SSL Web Tunnel<span style="padding-bottom:5px; margin-right:10px; float:right;"><a href="##" class="button filterButton small" id="inpReset" >Start Over</a></span></div>                         
                </div>
                    
                <div class="inner-txt-box">
                    
                    <div class="" style="float:left; height:auto; width:650px; margin-top:8px;">
                       
                       <div class="inputbox-container" style="margin-right:25px;">
                            <label for="inpSFTPURL">SFTP URL<span class="small">Required</span></label>
                            <input id="inpSFTPURL" name="inpSFTPURL" placeholder="Enter SFTP URL Here" size="20" autofocus style="padding-left:5px;" value="#inpSFTPURL#" />
                       </div>
                      
                       <div style="clear:both"></div>
                        
                       <div class="inputbox-container" style="margin-right:25px;">
                            <label for="inpSFTPUID">SFTP User Id<span class="small">Required</span></label>
                            <input id="inpSFTPUID" name="inpSFTPUID" placeholder="Enter SFTP User ID Here" size="20" autofocus style="padding-left:5px;" value="#inpSFTPUID#" />
                       </div>
                        
                       <div style="clear:both"></div>
                        
                       <div class="inputbox-container" style="margin-right:25px;">
                            <label for="inpSFTPPassword">SFTP Password<span class="small">Required</span></label>
                            <input type="Password" id="inpSFTPPassword" name="inpSFTPPassword" placeholder="Enter SFTP Password Here" size="20" autofocus style="padding-left:5px;" value="#inpSFTPPassword#" />
                       </div>
                       
                       <div style="clear:both"></div>
                        
                       <div class="inputbox-container" style="margin-right:25px;">
                            <p>
                            
	                            <div class="hide-info showToolTip">
    	                            <label for="inpSFTPFingerPrint">SFTP Server Fingerprint<span class="small">Optional</span></label>
        	                        <input id="inpSFTPFingerPrint" name="inpSFTPFingerPrint" placeholder="Enter SFTP Server Fingerprint Here" size="20" autofocus style="padding-left:5px;" value="#inpSFTPFingerPrint#" />
                             	</div>
                                <div class="tooltiptext">
                                	Before you can connect to a sftp site with this tool you first need to get the remote SFTP server fingerprint. The fingerprint is a 16 pair colon separated list of hex numbers which the server returns to identify itself. Sample-> 12:2a:3e:c3:de:56:38:2b:e3:f6:0c:49:9a:d8:de:3d
                                </div>		                	
                            
                            </p>
                                                     
                       </div>
                       
                            
                        <div style="clear:both"></div>
                             
                    	<div class="submit">
                            <span id="inpListSFTPContainer"><a id="inpListSFTP" class="button filterButton small" href="##">List Files</a></span>   
                            <div id="loadingUpdatingStatus" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                        </div>
                        
                                               
                      <!---  <div style="clear:both"></div>
                        
                        <div class="submit">
                            <span id="inpResetContainer"><a href="##" class="button filterButton small" id="inpReset" >Start Over</a></span>   
                            <div id="loadingUpdatingStatus" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                        </div>--->
                        
                        
                         				<div class="message-block">
                                        
                                            <div class="left-input">
                                                <span class="em-lbl">File To Upload</span>
                                                <div class="hide-info showToolTip">&nbsp;</div>
                                                <div class="tooltiptext">
                                                   Choose a CSV file for which to upload Appointments
                                                </div>		
                                                <div class="info-block" style="display:none">content info block</div>
                                            </div>
                                        
                                           <div class="right-input">
                                                <input type="file" name="upfile" style="height:20px;" />                                               
                                            </div>
                                        </div>
                                        
                                        <div style="clear:both"></div>
                                         <div class="submit">
                                            <span id="inpUploadToSFTPContainer"><a id="inpUploadToSFTP" class="button filterButton small" href="##">Upload File</a></span>   
                                            <div id="loadingUpdatingStatus" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                                        </div>
                        
                                    <!---     
                                        
                                        <div class='button_area' style="padding-bottom: 10px;">
                                             <input type="submit" name="submit" value="submit" class="ui-corner-all survey_builder_button" style="float:right;" />
                                        </div>--->
                                        
                    </div>    
                        
                </div>
                           
                        
                        <div class="MSGAlert">#inpMSG#</div>
                                                     
                                              
                    
                              

            </form>



 							
                                    
	                                                                   
                                    
                                    <!---<cffileupload
                                        extensionfilter="csv,txt" 
                                        name="portfoliofiles1" 
                                        maxfileselect="1" 
                                        title="Upload Contact List (MUST BE COMMA DELIMITED!)" 
                                        progressbar="true"
                                        onerror="fileUploadError"
                                        onuploadcomplete="fileUploadComplete"
                                        stoponerror="true"
                                        url = "fileUpload"
                                        height="200"
                                        width="450"
                                        wmode="transparent"
                                        bgcolor="##FFFFFF"
                                        >--->
                                        
                                        
                                        
            
                                 
                                    
                                    
                                    
				<div style="clear:both"></div>

				 <div >
        
       					 <div style="clear:both"></div>
        
        
						<cfif TRIM(inpSFTPUID) NEQ "">
                                       
                                   
                              <!--- You need to connect with some other tool at least once to get the remote server's finger print and put it here in call to open--->
                             <!--- from a mc/linux box you just go to command terminal and type: ssh SFTPHostName --->
                             
                             <cfif TRIM(inpSFTPFingerPrint) NEQ "">
                             
                                 <cfftp action = "open"
                                  username = "#inpSFTPUID#"
                                  connection = "CurrSFTPConn"
                                  password = "#inpSFTPPAssword#"
                                  fingerprint = "#TRIM(inpSFTPFingerPrint)#"
                                  <!---fingerprint = "0f:6b:f2:23:56:34:39:07:63:a4:82:fb:09:a2:ae:90"--->
                                  server = "#inpSFTPURL#"
                                  secure = "yes"
                                  stoponerror="Yes">
                              
                              <cfelse>
                                  <cfftp action = "open"
                                  username = "#inpSFTPUID#"
                                  connection = "CurrSFTPConn"
                                  password = "#inpSFTPPAssword#"
                                  <!---fingerprint = "0f:6b:f2:23:56:34:39:07:63:a4:82:fb:09:a2:ae:90"--->
                                  server = "#inpSFTPURL#"
                                  secure = "yes"
                                  stoponerror="Yes">
                                 
                              </cfif>
                              
                             <!--- <cfdump var="#CurrSFTPConn#">--->
                              
                            <cfif cfftp.succeeded>
                            
                                <cftry>
                            
                                    <cfftp action="listDir"
                                        name="RemoteDirectoryContents"
                                        directory="#inpSFTPInDir#"
                                        timeout="300"
                                        connection = "CurrSFTPConn"
                                        stoponerror="Yes">
                            
                                    <!---<cfdump var="#RemoteDirectoryContents#">--->
                                    
                                        <div class="" style="height:auto; width:1250px; margin-top:8px;">
                                     
                                            <!--- List Directories  only --->
                                            <div class="inputbox-container" style="margin-right:25px; width:1200px;">                                	
                                                <label>Current Directory</label>  
												<label>#inpSFTPInDir#</label>                                            
                                            </div>
                                            
                                            <div style="clear:both"></div>
                                            
                                            <div class="inputbox-container" style="margin-right:25px; width:1200px;">                                	
                                                <label>Directory(ies)</label>                                                
                                                <BR/>
                                                
                                                <cfset DirUpPath = "/" />
                                                <cfset inpSFTPInDirBuffer = inpSFTPInDir />
                                            
                                                <cfif ListLen(inpSFTPInDir, '/') GT 1>
                                                    
                                                    <cfset DirUpPath = ListDeleteAt(inpSFTPInDirBuffer, ListLen(inpSFTPInDir, '/'), '/' ) />
                                                                                                      
                                                    <a class="inpSFTPChangeDir" rel1="#DirUpPath#" href="##">.. &##x25B2;</a>  <BR/>
                                                <cfelse>
                                                    
                                                    <a class="inpSFTPChangeDir" rel1="/" href="##">.. &##x25B2;</a>  <BR/>
                                                        
                                                </cfif>
                                            
                                            </div>
                                            
                                            <div style="clear:both"></div>
                                            
                                            <cfloop query="RemoteDirectoryContents" >
                                            
                                                <cfif RemoteDirectoryContents.ISDIRECTORY EQ TRUE> 
                                                    
                                                    <div class="inputbox-container" style="margin-right:25px; width:1200px;">
                                                        <!--- Directory Found --->
                                                        <a class="inpSFTPChangeDir" rel1="#RemoteDirectoryContents.PATH#" href="##">#RemoteDirectoryContents.PATH#</a>  <BR/>
                                                    </div>
                                                    
                                                    <div style="clear:both"></div>
                                                    
                                                </cfif>
                                                
                                            </cfloop>
                                            
                                            <!--- List files only --->
                                            <div class="inputbox-container" style="margin-right:25px; width:1200px;">                                	
                                                <label>File(s)</label>
                                            </div>
                                            
                                            <div style="clear:both"></div>
                                            <cfloop query="RemoteDirectoryContents" >
                                            
                                                <cfif RemoteDirectoryContents.ISDIRECTORY EQ FALSE> 
                                                    
                                                    <div class="inputbox-container" style="margin-right:25px; width:1200px;">
                                                        <!--- Directory Found --->
                                                        <a class="inpSFTPDownloadFile" rel1="#RemoteDirectoryContents.PATH#" href="##">#RemoteDirectoryContents.PATH#</a>  <BR/>
                                                    </div>
                                                    
                                                    <div style="clear:both"></div>
                                                   
                                               
                                                </cfif>
                                                
                                            </cfloop>
                                    
                                        </div>
                                        
                                    <cfftp action="close" connection="CurrSFTPConn"/>
                        
                                <cfcatch type="any">
                                    <cfdump var="#cfcatch#">
                                    <cfftp action="close" connection="CurrSFTPConn"/>
                                </cfcatch>
                                
                                </cftry>
                        
                            </cfif>
            
                                  
                        <cfelse>
            
                        
                        </cfif>	
            	  	       
        	</div>              
                 
		</div>
                      
	</div>
</cfoutput>


<cfset inpMSG="" />


