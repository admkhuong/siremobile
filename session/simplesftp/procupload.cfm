
<cfparam name="inpSFTPURL" default="">
<cfparam name="inpSFTPUID" default="">
<cfparam name="inpSFTPPAssword" default="">
<cfparam name="inpSFTPInDir" default="/">
<cfparam name="inpSFTPFingerPrint" default="">

<cfparam name="inpMSG" default="">

<cfparam name="inpList" default="0">
<cfparam name="inpDownload" default="0">
<cfparam name="inpDelete" default="0">
<cfparam name="inpUpload" default="0">

<cfparam name="inpRemoteFileName" default="">
    

<cfset inpRemoteFileNameBuff =  Replace(inpRemoteFileName, "/", "_", "ALL") >

<cftry>

	<!---<cffile action="delete" file="ram:///#Session.UserId#_RamFile_#inpRemoteFileNameBuff#">--->

<cfcatch type="any">

    
 
</cfcatch>

</cftry>


<!--- 			
<cfset getPageContext().Forward('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home.cfm') />
<cfabort />
--->
      
<cftry>


<cfset fuldatetime = dateformat(now(),'mmddyy') & timeformat(now(),'HHmmss')>

<!--- First write file to web server temp directory - make sure there is a nightly clean up process cleaning this out --->
<cffile 
    action="upload" 
    destination="#PhoneListUploadLocalWritePath#/SFTP_#Session.UserId#_#fuldatetime#.csv" 
    nameconflict="overwrite" 
    result="CurrUpload"
    > 
    
	<cfif TRIM(inpSFTPFingerPrint) NEQ "">
                
	                                                     
        <cfftp action = "putFile"
            username = "#inpSFTPUID#"
            password = "#inpSFTPPAssword#"
            fingerprint = "#TRIM(inpSFTPFingerPrint)#"
            server = "#inpSFTPURL#"
            secure = "yes"
            stoponerror="Yes"
            localFile = "#CurrUpload.serverDirectory#/#CurrUpload.serverFile#" 
            remoteFile = "/#inpSFTPInDir#/#CurrUpload.clientFile#">
            
    
    <cfelse>
         
        <cfftp action = "putFile"
            username = "#inpSFTPUID#"
            password = "#inpSFTPPAssword#"
            server = "#inpSFTPURL#"
            secure = "yes"
            stoponerror="Yes"
            localFile = "#CurrUpload.serverDirectory#/#CurrUpload.serverFile#" 
            remoteFile = "/#inpSFTPInDir#/#CurrUpload.clientFile#">
     
    </cfif>
   
    <cffile action="delete" file="#CurrUpload.serverDirectory#/#CurrUpload.serverFile#">    

	<cfset inpMSG = "#CurrUpload.clientFile# uploaded to SFTP server." />
    
<cfcatch type="any">

	<!--- Remove error in oroduction --->
    <cfdump var="#cfcatch#">
    
    <cfset inpMSG = "Error uploading to SFTP server." />
    
   <!--- <cfabort/>--->
 
</cfcatch>

</cftry>

<cfif TRIM(inpMSG) NEQ "">
    <cflocation addtoken="no" url="httpstunnel?inpSFTPURL=#inpSFTPURL#&inpSFTPUID=#inpSFTPUID#&inpSFTPPAssword=#inpSFTPPAssword#&inpSFTPInDir=#inpSFTPInDir#&inpSFTPFingerPrint=#inpSFTPFingerPrint#&inpMSG=#inpMSG#" />
<cfelse>
    <cflocation addtoken="no" url="httpstunnel?inpSFTPURL=#inpSFTPURL#&inpSFTPUID=#inpSFTPUID#&inpSFTPPAssword=#inpSFTPPAssword#&inpSFTPInDir=#inpSFTPInDir#&inpSFTPFingerPrint=#inpSFTPFingerPrint#" />
</cfif>
