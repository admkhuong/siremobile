module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			development: {
				options: {
					compress: false,
					yuicompress: true,
					optimization: 2
				},
				files: {
					// target.css file : source.less file
					"assets/layouts/layout4/css/custom.css": "assets/less/custom.less"
				}
			}
		},
		watch: {
			styles: {
				files: ['assets/less/**/*.less','desktop/less/**/*.less'],
				tasks: ['less'],
				options: {
					spawn: false
				}	
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['watch']);
};