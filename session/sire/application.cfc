<cfcomponent>
     <cfinclude template="/public/sire/configs/paths.cfm">
     <cfinclude template="/public/sire/configs/version.cfm" />

     <cfset this.name = "SireMobileApplication" />  <!--- RXMBEBMDemo ->  RXSireApplication --->

     <cfset this.sessionStorage  = "sessionstorage">

     <cfset this.SessionTimeout = CreateTimeSpan( 0, 2, 0, 0 ) />

     <!--- What is this?!? --->
     <cfset this.sessionCluster = true>

     <cfset this.mappings["/cfpayment"] = "/public/sire/models/cfc/cfpayment-master/" />

     <cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first CREATED.">
          <cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
          <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
          <cfset APPLICATION.sessions = 0>

         <cfreturn true />
     </cffunction>


     <cffunction name="onRequestStart" returnType="boolean">
          <cfargument type="String" name="targetPage" required=true/>
          <cfparam name="Session.DBSourceEBM" default="bishop">
          <cfparam name="Session.DBSourceREAD" default="bishop_read">

          <cfset var RetVarLookForRememberMeCookie = '' />
          <cfset var getUser =''/>
          <cfset var UpdateLastLoggedIn =''/>
          <cfset var checkLogin =''/>
          <cfset var getShortCode =''/>

          <cfset var currRememberMe = structNew() />

          <cfset var checkActive = '' />
<!---
QQQ<BR/>TEST ZZZ
<cfinvoke component="session.sire.republic.cfc.userstools" method="LookForRememberMeCookie" returnvariable="RetVarLookForRememberMeCookie"></cfinvoke>

<cfdump var="#RetVarLookForRememberMeCookie#" >
--->

          <cfset APPLICATION.Build_Version ="#Config_BuildVersionString#">

          <cfset var headers = getHttpRequestData().headers />

          <!--- What is this for ?!? --->
          <cfif CGI.REQUEST_METHOD EQ 'GET' AND !(structKeyExists(headers, "X-Requested-With") and (headers["X-Requested-With"] eq "XMLHttpRequest"))>
               <cfset Session._rememberPath = '' />
          </cfif>

          <!--- _SiteMaintenance is set in public/sire/configs/paths.cfm 0 is on no issues --->
          <cfif _SiteMaintenance EQ 1 >
          	<cfif structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/coming-soon.cfm')>
          		<cflocation url= "/coming-soon" addToken = "false"/>
          	</cfif>
          <cfelseif _SiteMaintenance EQ 2>
               <cfif structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/scheduled-maintenance.cfm')>
               	<cflocation url= "/scheduled-maintenance" addToken = "false"/>
               </cfif>
          </cfif>

          <!--- Dont check session if in the RE-pub;ic section - otherwise look for logged in --->
          <cfif Findnocase("/session/sire/republic", GetTemplatePath() ) EQ 0> <!--- Not Republic --->

               <!--- Verify if user is logged in --->
               <cfinvoke component="session.sire.republic.cfc.userstools" method="checkLogin" returnvariable="checkLogin"></cfinvoke>

               <cfif checkLogin>
                    <!--- Make sure logged in user has not had their account deactivated --->
                    <cfinvoke component="session.sire.republic.cfc.userstools" method="CheckUserActive" returnvariable="checkActive"></cfinvoke>

                    <cfif checkActive.ISACTIVE LT 1>
                   		<cfset structClear(session)/>
                   		<cflocation url="/signout" addtoken="false">
                   	</cfif>

          		<cfreturn true>
               <cfelse>

                    <cfinvoke component="session.sire.republic.cfc.userstools" method="LookForRememberMeCookie" returnvariable="RetVarLookForRememberMeCookie"></cfinvoke>

     <!--- <cfdump var="#RetVarLookForRememberMeCookie#" > --->

                    <!--- cookie check here --->
                    <cfif RetVarLookForRememberMeCookie.RXRESULTCODE NEQ 1 >

                         <cfif CGI.REQUEST_METHOD EQ 'GET' AND !(structKeyExists(headers, "X-Requested-With") and (headers["X-Requested-With"] eq "XMLHttpRequest"))>
                         	<cfset Session._rememberPath = replace(cgi.request_url, '.cfm', '') />
                         </cfif>

                         <!--- Debugging Session Issues Logic
                          Oh no!!!!
                         <cfdump var="#checkLogin#" />
                         <cfdump var="#session#" />
                          --->

                          <cflocation url='/' addtoken="false">

                         <cfreturn false>

                    </cfif>

               </cfif>

          </cfif> <!--- Not Republic --->

          <cfreturn true>

     </cffunction>

     <cffunction name="onRequestEnd" returnType="void">
     	<cfset var GetUserLevel=''>
     	<cfset var RetCustomerInfo=''>
     	<cfset var SireHigherLevelUserID=''>
     	<!--- for integrate user--->
     	<cfquery name="GetUserLevel" datasource="#Session.DBSourceEBM#">
     		SELECT
     			UserLevel_int,
     			IntegrateHigherUserID,
     			HigherUserID_int
     		FROM
     			simpleobjects.useraccount
     		WHERE
     			UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
     		AND
     			UserType_int=2
     	</cfquery>


     	<cfif GetUserLevel.UserLevel_int EQ 2>

     		<cfif getPageContext().getRequest().getRequestURI() NEQ "/session/sire/pages/billing.cfm"
     			AND getPageContext().getRequest().getRequestURI() NEQ "/session/sire/models/cfc/payment/vault.cfc"
     			AND getPageContext().getRequest().getRequestURI() NEQ "/session/sire/models/cfc/users.cfc"
     			AND getPageContext().getRequest().getRequestURI() NEQ "/session/sire/models/cfc/billing.cfc"

     			>
     			<cfinvoke component="session.sire.models.cfc.billing" method="CheckExistsPaymentMethod" returnvariable="RetCustomerInfo"></cfinvoke>

     			<cfif RetCustomerInfo.RXRESULTCODE EQ -1>
     				<cflocation url="/session/sire/pages/billing" addtoken="false">
     			</cfif>
     		</cfif>
     	<cfelseif GetUserLevel.UserLevel_int EQ 3>
     		<cfset SireHigherLevelUserID=GetUserLevel.HigherUserID_int>

     		<cfinvoke component="session.sire.models.cfc.billing" method="CheckExistsPaymentMethod" returnvariable="RetCustomerInfo">
     			<cfinvokeargument name="userId" value="#SireHigherLevelUserID#">
     		</cfinvoke>
     		<cfif getPageContext().getRequest().getRequestURI() NEQ "/session/sire/pages/dashboard.cfm">
     			<cfif RetCustomerInfo.RXRESULTCODE EQ -1>
     				<cflocation url="/session/sire/pages/dashboard" addtoken="false">
     			</cfif>
     		</cfif>
     	</cfif>


         <cfargument type="String" name="targetPage" required=true/>

         <cfif !isDefined('variables._Response')>
     		<cfset variables._PageContext = GetPageContext()>
     		<!--- Coldfusion --->
     		<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
     		<cfset variables._Response = variables._PageContext.getOut()>
     	</cfif>
     	<cfset variables._page = variables._Response.getString()>
     	<cfif Find('<link', variables._page) GT 0>
     		<cfset variables._av = '#Config_BuildVersionNumber#'>
     		<cfset variables._page = REReplace(variables._page, '(<(link|script)[^>]+(href|src)=["''][^\?>"'']+)(["''])', "\1?_=#variables._av#\4", "all")/>
     		<cfif structKeyExists(Session, "webAlertMessages") AND isStruct(Session.webAlertMessages) AND StructCount(Session.webAlertMessages) GT 0>
     			<cfset variables._page = REReplace(variables._page, '</body>', "<script src=""/session/sire/assets/pages/scripts/alert-system.js?_=#variables._av#""></script>\0", "one")/>
     		</cfif>
     		<cfset variables._Response.clear()/>
     		<cfoutput>#variables._page#</cfoutput>
     	</cfif>
     </cffunction>

     <cffunction name="onError">
       <cfargument name="Exception" required=true/>
       <cfargument name="EventName" type="String" required=true/>

        <!--- Log all errors. --->
        <cfset var message = ''>
        <cfset var request_url = ''>
        <cfset var messageDetail = ''>
        <cfset var request_url = ''>
        <cfset var http_referer = ''>
        <cfset var http_user_agent = ''>

        <cfif isStruct(Exception)>
            <cfset messageDetail = serializejson(Exception)>
        </cfif>

        <cfif structKeyExists(Exception, "message")>
            <cfset message = Exception.message>
        </cfif>

        <cfif structKeyExists(Exception, "Detail")>
            <cfset message &= ' '&Exception.Detail>
        </cfif>
        <!---
        <cfif structKeyExists(Exception, "queryError")>
            <cfset message &= ' '&Exception.queryError>
        </cfif>
        --->
        <cfif structKeyExists(cgi, "request_url")>
            <cfset request_url = cgi.request_url>
        </cfif>

        <cfif structKeyExists(cgi, "http_user_agent")>
            <cfset http_user_agent = cgi.http_user_agent>
        </cfif>

        <cfif structKeyExists(cgi, "http_referer")>
            <cfset http_referer = cgi.http_referer>
        </cfif>

        <cfinvoke component="session.sire.republic.cfc.error_logs" method="LogErrorAction">
             <cfinvokeargument name="INPERRORMESSAGE" value="#message#">
             <cfinvokeargument name="INPREQUESTURL" value="#request_url#">
             <cfinvokeargument name="INPERRORDETAIL" value="#messageDetail#">
             <cfinvokeargument name="INPHttpUSERAGENT" value="#http_user_agent#">
             <cfinvokeargument name="INPHTTPREFERER" value="#http_referer#">
        </cfinvoke>

        <!--- Display an error message if there is a page context. --->
        <cflocation url= "/error-page" addToken = "false"/>

    </cffunction>

</cfcomponent>
