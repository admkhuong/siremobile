(function($){
// Namr: sireFilterBar
// Author: NHH - v: 1.0
// Created: 02/20/2017
// Dependencies: jQueryUI, jQuery, Bootstrap Selectpicker

	$.fn.sireFilterBar = function(options, applyCallback){
		var self = this;
		// default options.

	    var settings = $.extend({
	        fields: [
	        	{DISPLAY_NAME: 'ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' Id_int '},
	        	{DISPLAY_NAME: 'Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' Name_vch '},
				{DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '}
	        ],
	        clearButton: false,
	        errorFocus: false,
	        rowLimited: null,
	        error: function(error){},//pass row error to handle
	        limited: function(msg){},
	        clearCallback: function(){}
	    }, options );

	    // operator list
	    const 	operatorKeys = [
			{value: 'LIKE', display: 'Similar To', validType: ['TEXT', 'KEYWORD', 'PHONE', 'WORDLIKE']},
			{value: '=', display: 'Is', validType: ['TEXT', 'INTEGER', 'STATUS', 'KEYWORD', 'LIST', 'PHONE','IS','SBOXUSERTYPE']},
			{value: '<>', display: 'Is Not', validType: ['TEXT', 'INTEGER', 'LIST', 'STATUS', 'PHONE']},
			{value: '>', display: 'Is Greater Than', validType: ['INTEGER']},
			{value: '<', display: 'Is Less Than', validType: ['INTEGER']},
			{value: 'LIKE', display: 'Is', validType: ['DATE']},
			{value: '>', display: 'Is After', validType: ['DATE']},
			{value: '<', display: 'Is Before', validType: ['DATE']},
			{value: 'NOT LIKE', display: 'NOT Similar To', validType: ['TEXT', 'PHONE']}
		];
	
		// accept sql types
	    const filedTypes = [
			'CF_SQL_BIGINT',
			'CF_SQL_BIT',
			'CF_SQL_CHAR',
			'CF_SQL_BLOB',
			'CF_SQL_CLOB',
			'CF_SQL_DATE',
			'CF_SQL_DECIMAL',
			'CF_SQL_DOUBLE',
			'CF_SQL_FLOAT',
			'CF_SQL_IDSTAMP',
			'CF_SQL_INTEGER',
			'CF_SQL_LONGVARCHAR',
			'CF_SQL_MONEY',
			'CF_SQL_MONEY4',
			'CF_SQL_NUMERIC',
			'CF_SQL_REAL',
			'CF_SQL_REFCURSOR',
			'CF_SQL_SMALLINT',
			'CF_SQL_TIME',
			'CF_SQL_TIMESTAMP',
			'CF_SQL_TINYINT',
			'CF_SQL_VARCHAR'
		];

	    // Add a new filter row
	    var addFilterRow = function(newRow){
	    	var item = generateRow(newRow);
	    	var def = $.Deferred().resolve(item).promise(self.append(item));
			$.when(def).done(function(item){
	    		item.find('select[name="field-name"]').selectpicker().trigger("change");
	    	});
	    }

	   	// remove a filter row
	    var removeFilterRow = function(item){
	    	item.remove();
	    }


	    var validateFilterRow = function(item){
	    	valueEle = $(item).find('input[name="value"]');

	    	if (valueEle.val() == "") {
	    		valueEle.addClass('has-error');
	    		return false;
	    	} else {
	    		valueEle.removeClass('has-error');
	    		return true;
	    	}
	    }

	    var isLimitedRow = function(){
	    	if (settings.rowLimited !== null && self.find('div.filter-item').length >= settings.rowLimited) return true ;
	    	return false;
	    }


	    var filterBarInit = function(){

	    	if (!self.hasClass('box-filter')) self.addClass('box-filter');



	    	self.on('change', 'select[name="field-name"]', function(event){

	    		var selectedOption = $(this).find('option[value="'+$(this).val()+'"]');
	    		var type = selectedOption.data('type'),
	    			index = selectedOption.data('index'),
	    			sqlType = selectedOption.data('sql-type'),
	    			thisItem = $(this).parents('div.filter-item'),
	    			inputBox = thisItem.find('div.input-box'),
	    			inputValue = $('<input type="text" class="filter-form-control form-control" name="value">')
	    			field = settings.fields[index];				
	    		thisItem.find('select[name="operator"]').html((function(){
	    			var strReturn = "";
	    			operatorKeys.forEach(function(item){
	    				if (item.validType.indexOf(type) >= 0) strReturn+= '<option value="'+item.value+'">'+item.display+'</option>';
	    			});
	    			return strReturn;
	    		})()).selectpicker('refresh');

	    		if (type == "DATE"){
	    				inputBox.html(inputValue)
	    				.promise().done(function(){
	    					inputValue.datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');
	    				});
	    		} else {
					switch(type){
						case "STATUS" :{
							inputBox.html((function(){
								if(field.CUSTOM_DATA) return $(field.CUSTOM_DATA()).attr('name', 'value');
								return $('<select name="value" class="filter-form-control form-control"></select>')
								//.append('<option value="-1">ALL</option>')
								.append('<option value="0">FALSE</option>')
								.append('<option value="1">TRUE</option>');
							})()).promise().done(function(inputBox){
								if (inputBox.find('select').length) inputBox.find('select').selectpicker();
							});
						} break;
	    				case "INTEGER":{
	    					inputBox.html(inputValue.attr('type', 'number'));
	    				} break;
	    				case "IS":{
	    					inputBox.html(inputValue.attr('type', 'number'));
	    				} break;
						case "PHONE":{
	    					inputBox.html(inputValue.addClass("phone-style").attr('type', 'text'));
	    				} break;
	    				case "TEXT": {							
	    					inputBox.html(inputValue.removeClass('phone-style'));
						} break;
						case "SBOXUSERTYPE":{
							inputBox.html((function(){								
								return $('<select name="value" class="filter-form-control form-control"></select>')								
								.append('<option value="1">COMMON USER</option>')
								.append('<option value="2">INTEGRATION USER</option>');
							})()).promise().done(function(inputBox){
								if (inputBox.find('select').length) inputBox.find('select').selectpicker();
							});
	    				} break;
	    			}
	    			if(field.CALLBACK && typeof(field.CALLBACK) == 'function'){
	    				field.CALLBACK(inputValue);
	    			}

	    		}

	    	})

	    	// click apply filter button
	    	.on('click', 'button.btn-apply-filter', function(event){
	    		
	    		var data = [];
	    		var isValid = true;
	    		var erroElem = self.find('div.filter-item').each(function(index){
	    			var fileName = $(this).find('select[name="field-name"]').val(),
	    				operator = $(this).find('select[name="operator"]').val(),
	    				type     = $(this).find('select[name="field-name"] option:selected').data('sql-type'),
	    				value    = $(this).find('[name="value"]').val();
	    			if (validateFilterRow(this)) {
	    				data.push({"NAME": fileName, "OPERATOR": operator, "VALUE": value, "TYPE": type});
	    			} else {
						isValid = false;
	    			}
	    		}).find('input.has-error').first().focus();

	    		if (isValid && typeof(applyCallback) == 'function') {
	    			applyCallback(data);
	    		} else if (!isValid)  {
	    			settings.error(erroElem.parents('div.filter-item'));
	    		}
			})
			//auto search as user type
			//.on('keyup', '[name="value"]', function(event){
	    		
	    		//var data = [];
	    		//var isValid = true;
	    		//var erroElem = self.find('div.filter-item').each(function(index){
	    		//	var fileName = $(this).find('select[name="field-name"]').val(),
	    		//		operator = $(this).find('select[name="operator"]').val(),
	    		//		type     = $(this).find('select[name="field-name"] option:selected').data('sql-type'),
				//		value    = $(this).find('[name="value"]').val();
				//		if(value.length < 3){
				//			return;
				//		}					
	    		//	if (validateFilterRow(this)) {
	    		//		data.push({"NAME": fileName, "OPERATOR": operator, "VALUE": value, "TYPE": type});
	    		//	} else {
				//		isValid = false;
	    		//	}
	    		//}).find('input.has-error').first().focus();

	    		//if (isValid && typeof(applyCallback) == 'function') {
	    		//	applyCallback(data);
	    		//} else if (!isValid)  {
	    		//	settings.error(erroElem.parents('div.filter-item'));
	    		//}
			//})

	    	// click add button 
	    	.on('click', 'button.btn-add-item', function(event){
	    		if (!isLimitedRow()) {
	    			addFilterRow(true);
	    		} else settings.limited('Number of row is limited! You can change "rowLimited" property in options to null to unlimit row number');
	    	})

	    	// click clear button
	    	.on('click', 'button.btn-clear-filter', function(event){
	    		var items = $('div.new-row');

	    		items.each(function(index){
	    			var time = 100;
	    			var item = $(this);
	    			setTimeout(function(){ item.remove(); }, (index+1)*time);
	    		}).promise().done(function(items){
	    			resetRow(self.find('div.filter-item'));
	    			if (typeof(settings.clearCallback) == 'function') settings.clearCallback(event);
	    		});	
	    	})

	    	// click remove button 
	    	.on('click', 'button.btn-remove-item', function(event){
	    		var item = $(this).parents('div.filter-item');
	    		removeFilterRow(item);
	    	})

	    	// press Enter Key on input value

	    	.on('keypress', 'input[name="value"]', function(event){
	    		if(event.keyCode == 13){
	    			self.find('button.btn-apply-filter').trigger('click');
	    		}
	    	});

	    	// add default filter row
	    	addFilterRow(false);

	    	return self;
	    }



	    var resetRow = function(item){
	    	console.log(item);
			item.find('select[name="field-name"] option').first().prop('selected', true).parent().trigger('change');
	    }




	    var generateRow = function(isNewRow){
	    	isNewRow = typeof(isNewRow) != "undefined" ? isNewRow : false;

	    	var fieldNameEle = $('<div class="col-sm-3 col-xs-6"><div class="form-group"><select name="field-name" class="form-control"></select></div></div>'),
	    		operatorEle  = $('<div class="col-sm-3 col-xs-6"><div class="form-group"><select name="operator" class="form-control"></select></div></div>'),
	    		valueEle     = $('<div class="col-sm-6 col-xs-12"><div class="form-group clearfix"><div style="width: calc(100% - '+(settings.clearButton ? 120 : 80)+'px)" class="input-box"><input name="value" type="text" class="filter-form-control form-control"></div><div style="width: '+(settings.clearButton ? 120 : 80)+'px" class="action-box pull-right"></div></div></div>'),
	    		applyButton  = $('<button class="btn-apply-filter btn-action"><i class="glyphicon glyphicon-search"></i></button>'),
	    		addButton    = $('<button class="btn-add-item btn-action"><i class="glyphicon glyphicon-plus"></i></button>'),
	    		removeButton = $('<button class="btn-remove-item btn-action"><i class="glyphicon glyphicon-minus"></i></button>'),
	    		clearButton  = $('<button class="btn-clear-filter btn-action"><i class="glyphicon glyphicon-remove"></i></button>');

	    	var box  =  $('<div class="row filter-item '+(isNewRow ? "new-row": "")+'"></div>')
				    	.append(fieldNameEle).append(operatorEle)
				    	.append(valueEle);
			var flexBox = valueEle.find('div.form-group div.action-box');				    	
    		if (isNewRow) flexBox.append(removeButton);
    		else flexBox.append(addButton).append(applyButton).append((function(){
				if (settings.clearButton) return clearButton[0].outerHTML;
			})());

    		fieldNameEle.find('select[name="field-name"]').append((function(){
    			var strReturn = "";
    			try{
    				if (settings.fields.length == 0) {
    					throw '"fields" property must be set and not empty!';
    				}
	    			settings.fields.forEach(function(item, index){
    					if (filedTypes.indexOf(item.CF_SQL_TYPE) >= 0) {
	    					strReturn+= '<option data-index="'+index+'" data-sql-type="'+item.CF_SQL_TYPE+'" data-type="'+item.TYPE+'" value="'+item.SQL_FIELD_NAME+'">'+item.DISPLAY_NAME+'</option>';
	    				} else {
	    					throw '"'+item.CF_SQL_TYPE + '" sql type is not valid!';
	    				}
	    			});
    			} catch(error){
					settings.error(error);
				}
    			return strReturn;
    		})());

	    	return box;
	    }

	    return filterBarInit(); 
	}

})(jQuery);