function confirmBox(message, title, callback){
    title  = typeof(title) != "undefined" ? title : "Confirmation";
    bootbox.dialog({
        message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
        title: '&nbsp;',
        className: "be-modal",
        buttons: {
            success: {
                label: "Yes",
                className: "btn btn-medium green-gd",
                callback: function(result) {
                   callback();
                }
            },
            cancel: {
                label: "NO",
                className: "green-cancel"
            },
        }
    });
}


function alertBox(message, title, callback) {
    title  = typeof(title) != "undefined" ? title : "Alert";
    bootbox.dialog({
        message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
        title: '&nbsp;',
        className: "be-modal",
        buttons: {
            success: {
                label: "OK",
                className: "green-gd",
                callback: function(){
                    typeof(callback) == "function" ? callback() : "";
                }
            },
        }
    });
}

function MenuHightlightMapper() {
    var url = window.location.pathname;
    var page = url.substr(url.lastIndexOf('/') + 1);
    var checkCFM = page.indexOf(".cfm");
    if(checkCFM >= 0)
        page = page.replace(".cfm", "");
	if('undefined' !== (typeof action)) {

		if (action == "CreateNew") {
			/* active campaign left menu for action create */
			$(".campaign-template-choice-li").addClass('active');
			$(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
		} else 
		if(action == "SelectTemplate"){
			$(".campaign-template-choice-li").addClass('active');
			$(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
		} else 
		if(action == "Edit"){
			$(".campaign-manage-li").addClass('active');
			$(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
		}
	}

	//if('campaign-edit' == page && !/&ADV=1/i.test(window.location.search)){
    //     $(".campaign-manage-li").addClass('active');
    //         $(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    // }

    if('campaign-edit' == page && !/&ADV=1/i.test(window.location.search)){
		page = 'campaign-template-picker';	
        $("."+page+"-li").addClass('active');

        if ($(".advanced-li").find(".sub-menu > li."+page+"-li").length > 0) {
            $(".advanced-li").addClass('active').addClass('active-without-highlight');
            if ($("."+page+"-li:visible").length <= 0) {
                $(".advanced-li").find(".sub-menu").toggle();
            }
        }
	}
    else if('campaign-manage' == page){
            $(".campaign-manage-li").addClass('active');
            $(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'keyword'){
        $(".keyword-li").addClass('active');
        $(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'subscriber'){
        $(".subscriber-li").addClass('active');
        $(".campaign-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }    
    else if(page == 'trouble-ticket-landing'){
        $(".trouble-ticket-landing-li").addClass('active');
        $(".trouble-ticket-app-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'ticket-type-management'){
        $(".ticket-type-management-li").addClass('active');
        $(".trouble-ticket-app-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }    
    else if(page == 'campaign-template-troubleticket'){
        $(".campaign-template-troubleticket-li").addClass('active');
        $(".trouble-ticket-app-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();     

        // remove incorrect highlight and active
        $(".campaign-manage-li").removeClass('active');
        $(".campaign-li").removeClass('active').removeClass('active-without-highlight');   
        
    }
    else if(page == 'mlp-list'){
        $(".mlp-list-li").addClass('active');
        $(".tools-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'short-url-manage'){
        $(".short-url-manage-li").addClass('active');
        $(".tools-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'user-import-contact-file'){
        $(".user-import-contact-file-li").addClass('active');
        $(".tools-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'user-chat-tools'){
        $(".user-chat-tools-li").addClass('active');
        $(".tools-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }
    else if(page == 'admin-crm-reports'){
        $(".advanced-li").addClass('active');
        $(".admin-home-li").addClass('active').addClass('active-without-highlight').find(".sub-menu").show();
    }   
    else if (page.indexOf("sms-chat") >= 0) {
        $(".sms-chat-li").addClass('active');
    }
    else {
       
        if('campaign-edit' == page && !/&customCampaign=1/i.test(window.location.search)){
            page = 'campaign-template-picker';
            $("."+page+"-li").addClass('active');
        }
        else
           $("."+page+"-li").addClass('active');

        if ($(".advanced-li").find(".sub-menu > li."+page+"-li").length > 0) {
            $(".advanced-li").addClass('active').addClass('active-without-highlight');
            if ($("."+page+"-li:visible").length <= 0) {
                $(".advanced-li").find(".sub-menu").toggle();
            }
    }

        // $(".advanced-li").addClass('active').addClass('active-without-highlight');

	    // if ($(".campaign-li").find(".sub-menu > li."+page+"-li").length > 0) {
	    //     $(".campaign-li").addClass('active').addClass('active-without-highlight');
	    //     if ($("."+page+"-li:visible").length <= 0) {
	    //         $(".campaign-li").find(".sub-menu").toggle();
	    //     }
	    // } else
	    // if ($(".advanced-li").find(".sub-menu > li."+page+"-li").length > 0) {
	    //     $(".advanced-li").addClass('active').addClass('active-without-highlight');
	    //     if ($("."+page+"-li:visible").length <= 0) {
	    //         $(".advanced-li").find(".sub-menu").toggle();
	    //     }
	    // } else
	    // if ($(".advanced-campaigns-li").find(".sub-menu > li."+page+"-li").length > 0) {
	    //     $(".advanced-campaigns-li").addClass('active').addClass('active-without-highlight');
	    //     if ($("."+page+"-li:visible").length <= 0) {
	    //         $(".advanced-campaigns-li").find(".sub-menu").toggle();
	    //     }
	    // }
    }
}


$(document).ready(function() {
    MenuHightlightMapper();
});

function escapeHtml(text) {

    // Preserve newlines      
    text = text.replace(/<br>/gi, 'nexlinex');

    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    text = text.replace(/[&<>"']/g, function(m) { return map[m]; });    
    text = text.replace(/nexlinex/gi, '<br>');

    return text;
}

function isLeapYear(year)
{
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}


//
$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

//Global default settings for datatable 
$.extend( true, $.fn.dataTable.defaults, {
    bStateSave : false,
    iStateDuration : -1,
    bProcessing: true,
    bFilter: false,
    bServerSide:true,
    bDestroy:true,
    sPaginationType: "input",
    iDisplayLength: 20,
    bAutoWidth: false,
    fnDrawCallback: function( oSettings ) {
        var paginateBar = this.siblings('div.dataTables_paginate');
        if (oSettings._iDisplayStart < 0){
            oSettings._iDisplayStart = 0;
            paginateBar.find('input.paginate_text').val("1");
        }
        if(oSettings._iRecordsTotal < 1) paginateBar.hide();
        else paginateBar.show();
    } 
});


//clear event queue by timeout is set 
(function($){
    $.fn.delayOn = function(eventType, time, callback){
        var eventType = (typeof(eventType) != "undefined" ? eventType : 'keypress' ),
            time = (typeof(time) != "undefined" ? time : 500 ),
            timeout;

        this.on(eventType, function(event){
            var ele = this;
            clearTimeout(timeout);
            if(typeof(time) == "function"){

                timeout = setTimeout(function(){
                     time(ele, event);
                }, 500);
            } else if(typeof(callback) == "function"){
                  timeout = setTimeout(function(){
                     callback(ele, event);
                }, time);
            }
        });
    }
})(jQuery);

// remove "space" characters begin of a string 
String.prototype.trimFirstSpace = function(){
    var charPattern = typeof(arguments[0]) != 'undefined' ? arguments[0] : " ";
    var str = this;
    if(str.indexOf(charPattern) == 0)
        str = str.substring(1);
        if(str.indexOf(charPattern) == 0)
            return str.trimFirstSpace(charPattern);
    else
        return str;
};

//converts the first character of each word in a string to uppercase.
String.prototype.ucWords = function(){
    return this.split(" ").map(function(word){
        return word.substr(0, 1).toUpperCase() + word.toLowerCase().substr(1);
    }).join(" ");
};

$(".phone-style").mask("(000)000-0000");

String.prototype.toPhoneFormat = function () {
    var phone = this;
    var prefix = "";
    if (phone.length == 11) {
        prefix = phone.substring(0,1);
        phone = phone.slice(1, phone.length);
    }
    var first = phone.substring(0,3);
    var mid = phone.substring(3,6);
    var last = phone.substring(6, 11);
    var formatedPhone = prefix + '(' + first + ') ' + mid + '-' + last;
    return formatedPhone;
}

function fnUpdateUserCurrentBalance () {
    $.ajax({
        url: '/session/sire/models/cfc/billing.cfc?method=GetBalanceCurrentUserRemote&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST'
    })
    .done(function(data) {
        if (parseInt(data.RXRESULTCODE) == 1) {
            $("#span-current-user-credits").text(data.BALANCE);
        } else {
            console.log(data.MESSAGE);
        }
    })
    .fail(function(e) {
        console.log(e);
    });
}