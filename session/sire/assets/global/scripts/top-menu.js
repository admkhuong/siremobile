ChangeShortCodeCallback = function () {
    return false;
};

(function ($) {

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    var redirectPagesList = [
        {
            origin: 'campaign-edit',
            redirect: 'campaign-manage',
            allowChange: 1,
            warning: 0,
            warningMessage: ''
        },
        {
            origin: 'campaign-template',
            redirect: 'campaign-manage',
            allowChange: 1,
            warning: 0,
            warningMessage: ''
        },
        {
            origin: 'sms-chat-new',
            redirect: 'sms-chat',
            allowChange: 1,
            warning: 0,
            warningMessage: ''
        },
        {
            origin: 'sms-response',
            redirect: 'sms-chat',
            allowChange: 1,
            warning: 1,
            warningMessage: 'Are you sure you want to change shortcode? <br>You will be redirected to sms chat keyword list.'
        },
        {
            origin: 'campaign-reports',
            redirect: 'campaign-manage',
            allowChange: 1,
            warning: 0,
            warningMessage: ''
        }
    ];

    var currShortcode = '';

    function getPathOfPage () {
        var url = location.pathname;
        url = url.substr(url.lastIndexOf('/')+1, url.length);
        return url;
    }

    function checkRedirectList() {
        for (var i = 0; i < redirectPagesList.length; i++) {
            if (redirectPagesList[i].origin === getPathOfPage()) {
                location.href = redirectPagesList[i].redirect;
                if (typeof action !== "undefined" && action === "CreateNew") {
                    return false;
                }
                if (redirectPagesList[i].allowChange === 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();

        $(".nav li.disabled a").click(function() {
            return false;
        });
    });

    function LoadSelectShortCode() {
        $.ajax({
            url: "/session/sire/models/cfc/users.cfc?method=getListShortCodes" + strAjaxQuery,
            type: "POST",
            dataType: "json",
            success: function (data) {
                var subSelectBox = $('select.short-code-list-select');
                if (data.RXRESULTCODE == 1) {
                    data.LIST_SHORT_CODES.forEach(function (item) {
                        subSelectBox.append($('<option></option>').attr('value', item.ID).text(item.SHORT_CODE));
                    });
                    currShortcode = data.LIST_SHORT_CODES[0].ID;
                } else {
                    // alertBox(data.MESSAGE, 'alert');
                }
            },
            complete: function (responseText) {

            }
        });

    };

    LoadSelectShortCode();

    $('body').on('change', '.short-code-list-select', function () {
        var select = $(this);
        for (var i = 0; i < redirectPagesList.length; i++) {
            if (redirectPagesList[i].origin === getPathOfPage()) {
                if (redirectPagesList[i].warning === 1) {
                    var message = typeof redirectPagesList[i].warningMessage !== "undefined"
                                    && redirectPagesList[i].warningMessage !== "" ?
                                        redirectPagesList[i].warningMessage : "Are you sure you want to change shortcode?";
                    bootbox.dialog({
                        message: '<h4 class="be-modal-title">Change Shortcode</h4><p>'+ message +'</p>',
                        title: '&nbsp;',
                        className: "be-modal",
                        buttons: {
                            success: {
                                label: "Yes",
                                className: "btn btn-medium green-gd",
                                callback: function(result) {
                                    MarkDefaultShortcode(select);
                                }
                            },
                            cancel: {
                                label: "NO",
                                className: "green-cancel",
                                callback: function() {
                                    ResetShortcodeList();
                                }
                            },
                        }
                    });
                    return false;
                } else {
                    MarkDefaultShortcode(select);
                    return false;
                }
            }
        }
        MarkDefaultShortcode(select);
        return false;
    });

    function MarkDefaultShortcode (select) {
        var check = checkRedirectList();
        if (check) {
            var shortCodeId = select.val();
            $.ajax({
                url: "/session/sire/models/cfc/users.cfc?method=markShortCodeDefault" + strAjaxQuery,
                type: "POST",
                data: {shortCodeId: shortCodeId},
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.RXRESULTCODE == 1) {
                        $('.short-code-list-select').children('option').remove();
                        LoadSelectShortCode();
                        if (typeof ChangeShortCodeCallback === "function") {
                            ChangeShortCodeCallback();
                        }
                    } else {
                        alertBox(data.MESSAGE, 'alert');
                    }
                },
                complete: function (responseText) {

                }
            });
        } else {
            ResetShortcodeList();
        }
    };

    function ResetShortcodeList () {
        var subSelectBox = $('select.short-code-list-select');
        subSelectBox.children('option[value="'+currShortcode+'"]').prop('selected', 'selected');
    };

})(jQuery);