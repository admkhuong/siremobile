    
    $("#my_account_form").validationEngine({promptPosition : "topLeft", scroll: false, focusFirstField : false});

    $('#my_account_form').on('submit',function(event){
        event.preventDefault();
        if($('#my_account_form').validationEngine('validate')){
            updatemyaccountinfo();
        }
    });

    function updatemyaccountinfo(){

        try{
            $.ajax({
            type: "POST",
            url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateUserSettingAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
            dataType: 'json',
            data: $('#my_account_form').serialize(),
            beforeSend: function( xhr ) {
                $('.btn').prop('disabled',true);
                $('#processingPayment').show();
            },                    
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //bootbox.alert('Update Fail', function() {});
                $('.btn').prop('disabled',false);
                $('#processingPayment').hide();


                bootbox.dialog({
                    message:'Unable to update your profile settings at this time. An error occurred.',
                    title: "My Profile",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                }); 
            },                    
            success:        
                function(d) {
                    //$('#btn_save_my_account').prop('disabled',false);
                    $('.btn').prop('disabled',false);
                    $('#processingPayment').hide();

                    if(d.RESULT == "SUCCESS"){
                        //goto success page
                        /*bootbox.alert(d.MESSAGE, function() {
                            window.location.href='/session/sire/pages/my-account';
                        });*/

                        alertBox(d.MESSAGE, "My Profile", function() {
                                        window.location.href='/session/sire/pages/account'; 
                        });

                        $(window).unbind('beforeunload');
                    }
                    else{
                        alertBox(d.MESSAGE, "My Profile", function() {
                        });
                    }
                }       
            });
        }catch(ex){
            //bootbox.alert('Update Fail', function() {});
            $('.btn').prop('disabled',false);
            $('#processingPayment').hide();
            alertBox('Unable to update your profile settings at this time. An error occurred.', "My Profile", function() {
            });
        }
}

    $(document).ready(function($) {
        $('.radio-sendtype').each(function(){
            if ($(this).is(':checked')){
                $('#sendType').val($(this).val());
            }
        });

        $("#sms_number").mask("(000) 000-0000");
    });

var ProfileComponents = function () {

    return {
        //#percent-complete

        //main function to initiate the module
        initDoughnutStep: function () {
            Chart.defaults.global.tooltips.enabled = false;

            var val1 = parseFloat($('#percent-complete').val()).toFixed(0);
            var val2 = 100 - val1;

            checkFinalCounter(val1);
            

            var config = {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: [val1, val2],
                            backgroundColor: [
                                '#74c37f',
                                '#5c5c5c'
                            ],
                            hoverBackgroundColor: ['#74c37f'],
                            borderWidth: 0,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 70
                }
            }
            
            window.onload = function () {
                $('.wrap-dial-step').find('.pie_value').text(val1);
                var ctx = document.getElementById('dialStep').getContext("2d");
                window.myDoughnut = new Chart(ctx, config);    
            }            

            function updatePercentStep (val1, val2) {
                if (config.data.datasets.length > 0) {
                    config.data.datasets.forEach(function(dataset) {
                        dataset.data[0] = val1;
                        dataset.data[1] = val2;
                    });

                    window.myDoughnut.update();

                    $('.wrap-dial-step').find('.pie_value').text(val1);

                    checkFinalCounter(val1);
                }
            }

            function checkFinalCounter (val1) {
                if (val1 == 100) {
                    $('.wrap-dial-step').find('.sub-stt-step').removeClass('hide');
                } else {
                    $('.wrap-dial-step').find('.sub-stt-step').addClass('hide');
                }
            }

            // event to Test when value change
            $(".click-test").bind('click', function(e) {
                e.preventDefault();
                updatePercentStep(75, 25);    
            });     
        },
        
        init: function () {
            this.initDoughnutStep();
        }

    };

}();

jQuery(document).ready(function() {    
   ProfileComponents.init(); 
});
