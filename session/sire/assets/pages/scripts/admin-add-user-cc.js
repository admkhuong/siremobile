(function($){
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";	
	var formCCInfo = $('form[name="frm-ccInfomationSession"]');
	

	// Select user form user-id or email
	$("select[name='user-id']").select2({
  		placeholder: "Search by email or id",
  		allowClear: true,
  		ajax: {
		    url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      	return {
			        inpEmailAddress: params.term, // search term
			        page: params.page
		      	};
		    },
		    processResults: function (data, params) {
		   		return {
					results: $.map(data, function (item) {
	                    return {
	                        text: item.EMAIL,
	                        id: item.ID
	                    }
	                })
		      	};
		    },
		    cache: true
	  	},
		minimumInputLength: 1,
		theme: "bootstrap",
		width: 'auto'
  	});  

  	if(formCCInfo.length){
  		formCCInfo.validationEngine('attach',  {promptPosition : "topLeft", showArrow: true});
  	}

  	// On change value when selected a user id
  	$("#user-id").change(function() {  		
	  var placeholderSesion = $('.select2-selection__placeholder').text();
	  if(placeholderSesion == ""){
	  	var renderSelectUser = $("#select2-user-id-container").attr("title");
	  	var res = renderSelectUser.split(" - ");	  	
	  	$("#selected-user-id").val(res[0]);	  	    	
	  }
	  else{
	  	$("#selected-user-id").val('');
	  }	 
	});	

  	// Merge Month and Year to Date
  	formCCInfo.find('#expiration_date_month_upgrade, #expiration_date_year_upgrade').change(function(){
        formCCInfo.find('#expiration_date_upgrade').val($('#expiration_date_month_upgrade').val() + '/' + $('#expiration_date_year_upgrade').val());						
    });
	// cookie    
    function setCookie(cname, cvalue, exminutes) {
        var d = new Date();
        d.setTime(d.getTime() + exminutes);
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + (expires*1000) + ";path=/";
    }
	function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
  	//token
    function GetAccessTokenKey()
    {

        return new Promise( function (resolve, reject) {            
            var cookieToken= getCookie("PayoutToken") ;

            if(cookieToken !="")
            {
                var data ={
                    MESSAGE:"Sucess",
                    RESULT: "Sucess",
                    RXRESULTCODE:1,
                    TOKENKEY: cookieToken
                };
                
                resolve(data);
            }
            else
            {
                $.ajax({
                    type:"POST",                    
                    url:'/session/sire/models/cfc/vault-paypal.cfc?method=GetAccessTokenKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                    dataType:'json',
                    data:
                        {     
                            
                        }
                    ,beforeSend:function(xhr)
                        {
                        $('#processingPayment').show()
                    }
                    ,error:function(XMLHttpRequest,textStatus,errorThrown){     
                        var data ={MESSAGE:"Unable to get token key at this time. An error occurred."};
                        reject(data);
                        $('#processingPayment').hide()
                    }
                    ,success:function(data)
                        {                        
                        $('#processingPayment').hide();
                        if(data.RXRESULTCODE==1){      
                            setCookie("PayoutToken",data.TOKENKEY,data.EXPIRES);
                            resolve(data);
                        }
                        else
                        {
                            reject(data);
                        }     
                    }
                }); 
            }
       
        });
    }

    $( "#btn_CancelAddCCInfo" ).click(function() {
	  	window.location.href = '/session/sire/pages/admin-management-user-cc';
	});

  	// Admin save CC information
  	formCCInfo.on('submit', function(event){
  		event.preventDefault();
  		if($(this).validationEngine('validate')){
  			$('#btn_SaveCCinfo').prop("disabled", false).promise().done(function(){  							
      			var cardObject = {
                  inpToken: '',
                  inpNumber: $('#card_number').val(),
                  inpType: 'visa',
                  inpExpireMonth: $('#expiration_date_month_upgrade').val(),
                  inpExpreYear: $('#expiration_date_year_upgrade').val(),
                  inpCvv2: $('#security_code').val(),
                  inpFirstName: $('#first_name').val(),
                  inpLastName: $('#last_name').val(),
                  inpLine1: $('#line1').val(),
                  inpCity: $('#city').val(),
                  inpCountryCode: $('#country').val(),
                  inpPostalCode: $('#zip').val(),
                  inpState: $('#state').val(),
                  inpEmail: $('#email').val()
            	}
   
              var selectedPaymentGateway = $("#paymentGatewayList :selected").val();	
			        var selectedUserId = $("#selected-user-id").val();											             						
          		$.ajax({
          					url: '/session/sire/models/cfc/payment/vault.cfc?method=storeCreditCardInVault&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
          					async: true,
          					type: 'POST',
          					dataType: 'json',
          					data: {
          						inpCardObject: JSON.stringify(cardObject),
          						inpPaymentGateway: selectedPaymentGateway,
          						inpUserId: selectedUserId
          					},
          					beforeSend: function()
                    {
          						$('#processingPayment').show();									
          					},
          					error: function(jqXHR, textStatus, errorThrown)
                    {									
          						$("#processingPayment").hide();
          						alertBox('Action Error!','Save CC information',''); 									
          					},
            				success: function(data) 
                    {									
            					if (data.RXRESULTCODE == 1) {									
            						$("#processingPayment").hide();																
            						//alertBox(data.MESSAGE,'Save CC information','');
            						window.location.href = '/session/sire/pages/admin-management-user-cc';

            					} else {
            						$("#processingPayment").hide();					
            						alertBox(data.MESSAGE,'Save CC information','');
            					}                       
            				}
			         });											
  			});
  		}
  	});


})(jQuery);