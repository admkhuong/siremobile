(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (ipaddress){
        $("#adm-block-ip-address-list").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a id='+object.ID+' class="delete-ip-link">Delete</a>';
            return strReturn;
        }

        var table = $('#adm-block-ip-address-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "IpAddress_vch", "sName": 'IP Address', "sTitle": 'IP Address', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "DateUpdate_dt", "sName": 'Update', "sTitle": 'Update date', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetListIpAddress'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpipaddress", "value": ipaddress}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                     fnCallback(data);
                    }
                });
            },
            "aaSorting": [[1,'desc']]
        });
    }

    function SaveIpAddress(inpIpAddress)
    {        
        if ($('#form-new-ip-address').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var Ipaddress = $('#form-new-ip-address').find('#ip_address').val();
            $.ajax({
                url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertIpAddress'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpipaddress:Ipaddress
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Add New IP Address Fail','Add New IP Address','');
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        $("#form-new-ip-address").modal('hide');
                        setTimeout(function(){
                            alertBox("Add new IP address successfully!.",'Add New IP Address','');
                        },10);
                        fnGetDataList(); 
                        $('#form-new-ip-address').find('#ip_address').val('');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Add New IP Address','');
                    }                       
                }
            });
        }
        
    }
    /* Init datatable */
    $(document).ready(function() {

        fnGetDataList();        
        $( "#searchbox" ).on( "input", function(e) { 
                fnGetDataList($( this ).val());            
        });            
        $("#form-new-ip-address").on( "submit", function(e) {   
            e.preventDefault();         
            SaveIpAddress(); 
            //return false;
        });  
        var table = $('#adm-block-ip-address-list').DataTable();     
        
        $("#adm-block-ip-address-list").on("click", ".delete-ip-link", function(event){
            event.preventDefault();
            var IpID = $(this).attr('id');
            confirmBox('Are you sure you want to delete this IP address?', 'Delete a IP address', function(){
                $.ajax({
                    url: '/session/sire/models/cfc/admin-tool.cfc?method=DeleteIpAddressById'+strAjaxQuery,
                    async: true,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        inpIpID:IpID
                    },
                    beforeSend: function(){
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alertBox('Delete IP Address Fail','Delete IP Address',''); 
                    },
                    success: function(data) {
                        if (data.RXRESULTCODE == 1) {
                            fnGetDataList(); 
                        } else {
                            /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                            alertBox(data.MESSAGE,'Delete IP Address Fail','');
                        }                       
                    }
                });
            });
        });

    });    

})();