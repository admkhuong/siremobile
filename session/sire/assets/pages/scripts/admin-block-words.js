(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (words){
        $("#adm-black-list-words").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a id='+object.ID+' class="delete-words-link">Delete</a>';
            return strReturn;
        }

        var table = $('#adm-black-list-words').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "Words_vch", "sName": 'Black Words', "sTitle": 'Black List Words', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "UpdateDate_dt", "sName": 'Update', "sTitle": 'Update date', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetListBlackWords'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpWords", "value": words}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                     fnCallback(data);
                    }
                });
            },
            "aaSorting": [[1,'desc']]
        });
    }

    function SaveWords(inpMessageID)
    {        
        if ($('#form-new-words').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var Words_vch = $('#bl_words').val();
            if(Words_vch == ''){
                alertBox('Invalid words.','Add New Words','');
                $('#bl_words').focus();
                return;
            }
            
            $.ajax({
                url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertWords'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpwords:Words_vch
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Add New Words Fail','Add New Words','');
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        $("#form-new-words").modal('hide');
                        setTimeout(function(){
                            alertBox("Add new words successfully!.",'Add New Words','');
                        },10);
                        fnGetDataList(); 
                        $('#form-new-words').find('#bl_words').val('');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Add New Words','');
                    }                       
                }
            });
        }
        
    }
    /* Init datatable */
    $(document).ready(function() {

        var wordscheck = $('#form-new-words').find('#bl_words').val();
        if (wordscheck == '') 
        {
            $('#invalidwords').show();
        }else{
            $('#invalidwords').hide();
        }

        fnGetDataList();        
        $( "#searchbox" ).on( "input", function(e) { 
                fnGetDataList($( this ).val());            
        });            
        $("#form-new-words").on( "submit", function(e) {   
            e.preventDefault();         
            SaveWords(); 
            //return false;
        });  
        var table = $('#adm-black-list-words').DataTable();     
        
        $("#adm-black-list-words").on("click", ".delete-words-link", function(event){
            event.preventDefault();
            var WordsId = $(this).attr('id');
            confirmBox('Are you sure you want to delete this words?', 'Delete Words', function(){
                $.ajax({
                    url: '/session/sire/models/cfc/admin-tool.cfc?method=DeleteWordsById'+strAjaxQuery,
                    async: true,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        inpWordsID:WordsId
                    },
                    beforeSend: function(){
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alertBox('Delete Words Fail','Delete Words Fail',''); 
                    },
                    success: function(data) {
                        if (data.RXRESULTCODE == 1) {
                            fnGetDataList(); 
                        } else {
                            /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                            alertBox(data.MESSAGE,'Delete Words Fail','');
                        }                       
                    }
                });
            });
        });

    });    

})();