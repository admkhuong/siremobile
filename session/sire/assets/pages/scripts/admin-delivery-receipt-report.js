var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";


function GetBlastList(customFilterObj){
    $("#tblListBlast").html('');    
    var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";  
    var searchByFilter=[]  ;
    var searchBy=$("#search-by").val(); 
    var searchValue=$("#search-value").val(); 
    searchByFilter.push(
        {"NAME":searchBy,"VALUE":searchValue}
    );
    searchByFilter=JSON.stringify(searchByFilter);
    //{"NAME":" u.EmailAddress_vch ","OPERATOR":"LIKE","VALUE":"5","TYPE":"CF_SQL_VARCHAR"}


    var actionBtn = function (object) {
        var strReturn = '<a  data-user-id="'+object.USERID+'" data-user-mail="'+object.EMAIL+'" data-campaign-id="'+object.BATCH_ID+'" data-campaign-name="'+object.BATCH_NAME+'" class="btn-re-xs campaign-report"  title="Report">Report</a>';
        return strReturn;
    }

    var table = $('#tblListBlast').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
        
            
            {"mData": "NAME", "sName": 'Account Info', "sTitle": 'Account Info', "sWidth": '150px',"bSortable": true},
            {"mData": "BATCH_ID", "sName": 'Campaign ID', "sTitle": 'Campaign ID', "sWidth": '80px',"bSortable": true},
            {"mData": "BATCH_NAME", "sName": 'Campaign Name', "sTitle": 'Campaign Name', "sWidth": '250px',"bSortable": true},
            {"mData": "CREATE_DATE", "sName": 'Create Date', "sTitle": 'Create Date', "sWidth": '100px',"bSortable": true},
            {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "sWidth": '100px',"bSortable": false}
        ],
        "sAjaxDataProp": "aoData",            
        "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=AdminBlastListReport'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "customFilter", "value": customFilterData}                
            );
            aoData.push(                
                { "name": "searchByFilter", "value": searchByFilter}                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);                                                                                        
                }
            });
        },
        "aaSorting": [[2,'desc']]
    });
}   
function GetReport(campaignId){
    if(campaignId >0 ){
        $.ajax({
            url: "/session/sire/models/cfc/campaign.cfc?method=GetReportBlast&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            type: 'POST',
            dataType: 'json',
            data: {
                inpBatchId: campaignId
            },
            beforeSend: function () {
                $('#processingPayment').show();
            },            
            success: function(data){                
                if(data.RXRESULTCODE == 1){                    
                    $("#mdCampaignReport").find("#number-subscribers").text(data.TOTALSUBSCRIBERS);
                    $("#mdCampaignReport").find("#mblox-number-sent").text(data.MBLOXSENT);
                    $("#mdCampaignReport").find("#number-progress").text(data.INPROGRESS);
                    $("#mdCampaignReport").find("#mblox-number-fail").text(data.MBLOXFAIL);

                    $("#mdCampaignReport").find("#delivered").text(data.DELIVERED);
                    $("#mdCampaignReport").find("#lost-noti").text(data.LOSTNOTI);
                    $("#mdCampaignReport").find("#non-delivered").text(data.NONDELIVERED);
                    
                    
                    $("#mdCampaignReport").find("#schedule-options-detail").html("");
                    var stringSchedule="";
                    for (let index = 0; index < data.SCHEDULEOPTIONS.length; index++) {
                        var item=data.SCHEDULEOPTIONS[index];
                        stringSchedule= stringSchedule 
                        + 
                        '<div class="form-group clearfix">'
                            +'<div class="col-md-6 col-sm-12">Start Date</div>'
                            +'<div class="col-md-6 col-sm-12">'+item.STARTDATE+'</div>'
                            +'<div class="col-md-6 col-sm-12">End Date</div>'
                            +'<div class="col-md-6 col-sm-12">'+item.ENDDATE+'</div>'
                            +'<div class="col-md-6 col-sm-12">Start Time</div>'
                            +'<div class="col-md-6 col-sm-12">'+item.STARTTIME+'</div>'
                            +'<div class="col-md-6 col-sm-12">End Time</div>'
                            +'<div class="col-md-6 col-sm-12">'+item.ENTTIME+'</div>'
                        +'</div>';                        
                    }                    
                    $("#mdCampaignReport").find("#schedule-options-detail").html(stringSchedule);
                }else{
                    $("#mdCampaignReport").modal("hide");
                }
            }
        })
        .done(function(data) {    		
    		$('#processingPayment').hide();
    	})
	   	.fail(function() {
            $('#processingPayment').hide();
    		alertBox("*Error. No Response from the remote server. Check your connection and try again.");
		});;
    }
}
$(document).ready(function(){
    $(".select2").select2(
        { 
            theme: "bootstrap", 
            width: 'auto'            
        }
    );
    
    
    //Filter bar initialize
    $('#box-filter').sireFilterBar({
    fields: [
        
        {DISPLAY_NAME: 'Batch Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.BatchId_bi '}
    ],
    clearButton: true,    
    error: function(ele){
        // console.log(ele);
    },    
    clearCallback: function(){
        GetBlastList();
    }
    }, function(filterData){
        //called if filter valid and click to apply button
        GetBlastList(filterData);
    });
    //
    $("#frSearch").on("submit",function(e){
        e.preventDefault();
        if ($("#frSearch").validationEngine('validate')) {
            $("#report-section").removeClass("hidden");
            GetBlastList();
        }        
    });
    //
    $(document).on("click",".campaign-report",function(){
        var campaignId= $(this).data("campaign-id");
        GetReport(campaignId);
        $("#mdCampaignReport").find("#campaign-id").text($(this).data("campaign-id"));
        $("#mdCampaignReport").find("#campaign-name").text($(this).data("campaign-name"));
        $("#mdCampaignReport").find("#user-id").text($(this).data("user-id"));
        $("#mdCampaignReport").find("#user-mail").text($(this).data("user-mail"));
        $("#mdCampaignReport").modal("show");
    });
});