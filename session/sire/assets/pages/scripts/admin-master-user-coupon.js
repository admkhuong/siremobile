
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
fnGetDataList = function (customFilterData){
    customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   
    $("#coupon-list").html('');    

    var actionBtn = function (object) {
        var strReturn = '';            
        return strReturn;
    }
   
    var table = $('#coupon-list').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,                
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "UserID", "sName": 'UserID', "sTitle": 'UserID', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": "CouponCode", "sName": 'Coupon Code', "sTitle": 'Coupon Code', "bSortable": true, "sClass": "", "sWidth": "300"},      
            {"mData": "CreateBy", "sName": 'Create By', "sTitle": 'Create By', "bSortable": true, "sClass": "", "sWidth": "200"},    
            {"mData": "CreateDate", "sName": 'Create Date', "sTitle": 'Create Date', "bSortable": true, "sClass": "", "sWidth": "200"},                           
                                      
            {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "", "sWidth": "200px"}
        ],
        "sAjaxDataProp": "aaData",
        "sAjaxSource": '/session/sire/models/cfc/master-user-coupon.cfc?method=GetMasterCouponList'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { 
                    "name": "customFilter", "value": customFilterData                                                                                   
                }
            );                   

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                }
            });
        },             
        "aaSorting": [[2,'desc']]
    });
}

function SaveNewCoupon()
{        
    if ($('#frNewCoupon').validationEngine('validate', {scroll: false, focusFirstField : false})) {          
        var saveForm=$("#frNewCoupon");
        var couponCode= saveForm.find("#couponCode").val();
        var masterUser= saveForm.find("#masterUser").val();
        var plan= saveForm.find("#plan").val();
        var id=0;

        $.ajax({
            url: '/session/sire/models/cfc/master-user-coupon.cfc?method=SaveCoupon'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpId: id,
                inpPlanId :plan,
                inpUserId:masterUser,
                inpCouponCode: couponCode
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();                
                alertBox(errorThrown); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    $("#mdNewCoupon").modal('hide');
                    fnGetDataList(); 
                } else {                    
                    alertBox(data.MESSAGE,'Opps!','');
                }                       
            }
        });
    }
    else
    {                           
        return false;
    }
}
$(document).ready(function(){
    $("#plan").select2({
        theme: "bootstrap",
        width: 'auto'
    });
    $("#masterUser").select2({
        placeholder: "Search by email or id",
        allowClear: true,
        ajax: {
          url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
          dataType: 'json',
          delay: 250,
          data: function (params) {
                return {
                  inpEmailAddress: params.term, // search term
                  page: params.page
                };
          },
          processResults: function (data, params) {
                 return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.EMAIL,
                          id: item.ID
                      }
                  })
                };
          },
          cache: true
        },
      minimumInputLength: 1,
      theme: "bootstrap",
      width: 'auto'
    });
    
    fnGetDataList();             
    //Filter bar initialize
    $('#box-filter').sireFilterBar({
        fields: [            
            {DISPLAY_NAME: 'User ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' UserId_int '},
            {DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' CreateDate_dt '},
            
        ],
        clearButton: true,
        // rowLimited: 5,
        error: function(ele){
            
        },
        // limited: function(msg){
        // 	alertBox(msg);
        // },
        clearCallback: function(){
            fnGetDataList();
        }
    }, function(filterData){
        //called if filter valid and click to apply button
        fnGetDataList(filterData);
    });    
    $("#frNewCoupon").on('submit',function(e){
        e.preventDefault();   
        SaveNewCoupon();
    });
});