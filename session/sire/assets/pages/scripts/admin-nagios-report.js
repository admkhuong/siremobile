(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*host list datatable*/
    fnGetDataList = function (){
        
         var actionBtn = function (object) {
            var strReturn = '<a data-servername='+object.SERVERNAME+' class="btn-nagios-report" title="Detail"><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i></a>';
            return strReturn;
        }

        var table = $('#tbl-nagios-report-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "SERVERNAME", "sName": '', "sTitle": 'Server Name', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "STATUS", "sName": '', "sTitle": 'Status', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "STATUSINFORMATION", "sName": '', "sTitle": 'Status Information', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "LASTCHECK", "sName": '', "sTitle": 'Last Check', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": actionBtn, "sName": '', "sTitle": 'Services Detail', "bSortable": false, "sClass": "", "sWidth": "auto"}
            ],
            "sAjaxDataProp": "ListServerStatus",
            "sAjaxSource": '/session/sire/models/cfc/nagios-report.cfc?method=GetNagiosStatus'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                aoData.push(
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                     fnCallback(data);
                    }
                });
            },
        });
    }

        /*host list datatable*/
    fnGetServiceDetail = function (serverName){

        var table = $('#tbl-service-detail').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "SERVERNAME", "sName": '', "sTitle": 'Server Name', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "STATUS", "sName": '', "sTitle": 'Status', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "STATUSINFORMATION", "sName": '', "sTitle": 'Status Information', "bSortable": false, "sClass": "", "sWidth": "auto"},
            ],
            "sAjaxDataProp": "ListServerSetail",
            "sAjaxSource": '/session/sire/models/cfc/nagios-report.cfc?method=GetNagiosDetailHost'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                aoData.push(
                    { "name": "inpServerName", "value": serverName}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                     fnCallback(data);
                     $('#processingPayment').hide();

                    }
                });
            },
        });
    }

    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();        
        $('body').on('click', '.btn-nagios-report', function(){      
            var serverName  = $(this).data('servername');
            if(serverName != '')
            {
                $('#processingPayment').show();
                $('#service-detail-name').html(serverName);
                $('#modal-service-detail').modal('show');
                fnGetServiceDetail(serverName);
            }
            
        });

    });    

})();