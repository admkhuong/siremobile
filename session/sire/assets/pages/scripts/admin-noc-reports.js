var _NOCList;
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var lastFilter = '';

$(document).ready(function(){
	// get NOC list
	GetNOCReportsList();

	// init filter bar 
	InitFilter();
});

// get NOC list function
function GetNOCReportsList(customFilterObj){
	//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	lastFilter = customFilterObj;

	var displayResult = function (object) {
		var strReturn = parseInt(object.Result_int) === 1 ? "OK" : "Error";
		return strReturn;
	}

	var extraReport = function (object) {
		var strReturn = '<a title="Details" class="extra-report" data-pkid="'+object.NOCId_int+'"><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i></a>';
		return strReturn;
	}

	//init datatable for active agent
	_NOCList = $('#NOCReportsList').dataTable( {
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 10,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumns": [
			{"mData": "PKId_int","sName": 'PKId_int', "sTitle": 'ID', "sWidth": '120px',"bSortable": true, "sClass": "col-center"},
			{"mData": "NOCName_vch","sName": 'NOCName_vch', "sTitle": 'NOC Name', "sWidth": '320px',"bSortable": true},
			{"mData": displayResult,"sName": 'Results', "sTitle": 'Results', "sWidth": '110px',"bSortable": true, "sClass": "col-center"},
			{"mData": "RunTimeS","sName": 'Rumtime', "sTitle": 'Runtime (s)', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
			{"mData": "Details_vch","sName": 'Details',"sTitle": 'Details', "sWidth": '400px',"bSortable": true, "sClass": "th-center"},
			{"mData": "Created_dt","sName": 'Created',"sTitle": 'Created', "sWidth": '220px',"bSortable": true, "sClass": "col-center"},
			// {"mData": extraReport,"sName": '',"sTitle": '', "sWidth": '100px',"bSortable": true, "sClass": "col-center"}
		],
		"sAjaxDataProp": "DATALIST",
		"sAjaxSource": '/session/sire/models/cfc/noc.cfc?method=GetNOCReportsList' + strAjaxQuery,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (parseInt(aData.Result_int) === 0) {
				$(nRow).css('background-color', '#f9e0e0');
			}
		},
		"fnDrawCallback": function( oSettings ) {

		},
		"fnHeaderCallback": function(nRow){
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
			);
			aoData.push(
				{ "name": "inpNOCId", "value": nocid}
			);
			$.ajax({dataType: 'json',
				type: "POST",
				url: sSource,
				data: aoData,
				success: function (data) {
					fnCallback(data);
				}
			});
		},
		"fnInitComplete":function(oSettings, json){
		}
	});
}

// init filter bar function
function InitFilter() {
	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'NOC Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' NOCName_vch '},
			{DISPLAY_NAME: 'Runtime (s)', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' RunTimeMS_int '},
			{DISPLAY_NAME: 'Check OK', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' Result_int '},
			{DISPLAY_NAME: 'Details', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' Details_vch '}
		],
		clearButton: true,
	error: function(ele){
	},
	clearCallback: function(){
		GetNOCReportsList();
	}
	}, function(filterData){
		GetNOCReportsList(filterData);
	});
}