var _NOCList;
var _CountChatSessionNOCList;
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var lastFilter = '';

$(document).ready(function(){
	// get NOC list
	GetNOCList();

	// init filter bar 
	InitFilter();

	// init date range
	InitDateRange();

	// init validation for create new and edit form input
	$('#create-new-noc-form').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topLeft"});
	$('#edit-noc-form').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topLeft"});

	// process create new noc
	$("body").on('click', '#create-new-noc-btn', function(event) {
		event.preventDefault();
		CreateNewNOC();
	});

	// clean create new noc modal when modal shown
	$('#CreateNewNOCModal').on('show.bs.modal', function () {
		CleanNewModal();
	});

	// get noc information for edit
	$("body").on('click', '.edit-noc', function(event) {
		event.preventDefault();
		var NOCId = $(this).data('pkid');
		GetOneNOC(NOCId);
	});

	// process edit noc
	$("body").on('click', '#edit-noc-btn', function(event) {
		event.preventDefault();
		EditNOC();
	});

	// process delete noc button
	$("body").on('click', '.delete-noc', function(event) {
		event.preventDefault();
		var NOCId = $(this).data('pkid');
		bootbox.dialog({
			message: '<h4 class="be-modal-title">Delete NOC</h4><p>Are you sure you want to delete this NOC?</p>',
			title: '&nbsp;',
			className: "be-modal",
			buttons: {
				success: {
					label: "Yes",
					className: "btn btn-medium green-gd",
					callback: function(result) {
						DeleteNOC(NOCId);
					}
				},
				cancel: {
					label: "NO",
					className: "green-cancel"
				},
			}
		});
	});

	// process check-all button
	$('body').on('click', 'table i.select-all', function(event){
		if(!$(this).hasClass('checked')){
			$('#NOCList').find('input.campaign-item[type="checkbox"]').prop('checked', true);

			$('#NOCList').find('div.tick-select > i').removeClass('fa-square-o');
			$('#NOCList').find('div.tick-select > i').addClass('fa-check-square-o');
		} else {
			$('#NOCList').find('input.campaign-item[type="checkbox"]').prop('checked', false);
			
			$('#NOCList').find('div.tick-select > i').removeClass('fa-check-square-o');
			$('#NOCList').find('div.tick-select > i').addClass('fa-square-o');
		}
		$(this).toggleClass('checked');
	});

	// process tick checkbox
	$('body').on('click', '.tick-select', function(event) {
		event.preventDefault();
		if( $(this).children('i').hasClass('fa-square-o') ){
			$(this).children('i').removeClass('fa-square-o');
			$(this).children('i').addClass('fa-check-square-o');
		} else if( $(this).children('i').hasClass('fa-check-square-o') ){
			$(this).children('i').removeClass('fa-check-square-o');
			$(this).children('i').addClass('fa-square-o');
		}
	});

	// process bulk noc button
	$('body').on('click', '.bulk-noc-function', function(event) {
		event.preventDefault();
		var action = $(this).data('action');

		var NOCIds = "";
		$('#NOCList').find('tbody i.fa-check-square-o').each(function(index){
			if (NOCIds !== "") {
				NOCIds += ",";
			}
			NOCIds += $(this).data('pkid');
		});

		if (NOCIds === "") {
			alertBox("Notthing's selected!", "Oops!");
			return false;
		}

		bootbox.dialog({
			message: '<h4 class="be-modal-title">Multiple NOC Processing</h4><p>Are you sure you want to '+action+' these NOCs?</p>',
			title: '&nbsp;',
			className: "be-modal",
			buttons: {
				success: {
					label: "Yes",
					className: "btn btn-medium green-gd",
					callback: function(result) {
						BulkNOCProcess(NOCIds, action);
					}
				},
				cancel: {
					label: "NO",
					className: "green-cancel"
				},
			}
		});
	});

	$("body").on('click', '.extra-report', function(event) {
		event.preventDefault();
		$("#ChatSessionReport").modal("show");
	});

	$("#ChatSessionReport").on("shown.bs.modal", function () {
		var start = moment().subtract(6, 'days');
		var end = moment();
		GetExtraReport(start, end);
	});
});

// get NOC list function
function GetNOCList(customFilterObj){
	//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	lastFilter = customFilterObj;

	var actionBtn = function (object) {
		var strReturn = '<a title="Edit" class="edit-noc" data-pkid="'+object.PKId_int+'"><i class="fa fa-cog" style="font-size:15px" aria-hidden="true"></i></a>';
		strReturn += '&nbsp;<a title="Delete" class="delete-noc" data-pkid="'+object.PKId_int+'"><i class="fa fa-times" style="font-size:15px" aria-hidden="true"></i></a>';
		strReturn += '&nbsp;<a title="View Reports" href="/session/sire/pages/admin-noc-reports?nocid='+object.PKId_int+'" target="_blank"><i class="fa fa-eye" style="font-size:15px" aria-hidden="true"></i></a>';
		if (object.PKId_int == _CCSS_NOC_ID) {
			strReturn += '<a title="Details" class="extra-report" data-pkid="'+object.NOCId_int+'"><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i></a>';
		}
		return strReturn;
	}

	var checkBtn = function (object) {
		var strReturn = '<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true" data-pkid="'+object.PKId_int+'"></i></div>';
		return strReturn;
	}

	var displayURL = function (object) {
		var strReturn = '<span title="'+object.URL_vch+'">'+object.URL_vch.substring(0,30)+'...</span>';
		return strReturn;
	}


	//init datatable for active agent
	_NOCList = $('#NOCList').dataTable( {
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 10,
		"aoColumns": [
		{"mData": checkBtn,"sName": '', "sTitle": '', "sWidth": '50px',"bSortable": false, "sClass": "selection"},
		{"mData": "Name_vch","sName": 'Name_vch', "sTitle": 'Name', "sWidth": '300px',"bSortable": true},
		{"mData": "Description_vch","sName": 'Description_vch', "sTitle": 'Description', "sWidth": '300px',"bSortable": true},
		{"mData": displayURL,"sName": 'URL_vch', "sTitle": 'URL', "sWidth": '300px',"bSortable": true},
		{"mData": "IntervalType_vch","sName": 'IntervalType_vch', "sTitle": 'Interval Type', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "Interval_tm","sName": 'Interval_tm', "sTitle": 'Interval', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "StartDate_dt","sName": 'StartDate_dt', "sTitle": 'Start Date', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "EndDate_dt","sName": 'EndDate_dt', "sTitle": 'End Date', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "StartTime_tm","sName": 'StartTime_tm', "sTitle": 'Start Time', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "EndTime_tm","sName": 'EndTime_tm', "sTitle": 'End Time', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
		{"mData": "Paused_ti","sName": 'Paused_ti', "sTitle": 'Paused', "sWidth": '100px',"bSortable": true, "sClass": "col-center"},
		{"mData": "Port_int","sName": 'Port_int', "sTitle": 'Port', "sWidth": '100px',"bSortable": true, "sClass": "col-center"},
		{"mData": "TimeOut_int","sName": 'TimeOut_int', "sTitle": 'Time Out', "sWidth": '120px',"bSortable": true, "sClass": "col-center"},
		{"mData": "LaunchedTime_bi","sName": 'LaunchedTime_bi', "sTitle": 'Launched', "sWidth": '130px',"bSortable": true, "sClass": "col-center"},
		{"mData": "Created_dt","sName": 'Created_dt', "sTitle": 'Created', "sWidth": '130px',"bSortable": true, "sClass": "col-center"},
		{"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
		],
		"sAjaxDataProp": "DATALIST",
		"sAjaxSource": '/session/sire/models/cfc/noc.cfc?method=GetNOCList' + strAjaxQuery,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (aData.Paused_ti === "Yes") {
				$(nRow).css('background-color', '#f9e0e0');
			}
		},
		"fnDrawCallback": function( oSettings ) {

		},
		"fnHeaderCallback": function(nRow){
			$(nRow).find('th.selection').html('<i class="select-all glyphicon glyphicon-ok"></i>');
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
				);
			$.ajax({dataType: 'json',
				type: "POST",
				url: sSource,
				data: aoData,
				success: function (data) {
					fnCallback(data);
				}
			});
		},
		"fnInitComplete":function(oSettings, json){
			$("#NOCList thead tr").find(':last-child').css("border-right","0px");
		}
	});
}

// init filter bar function
function InitFilter() {
	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' Name_vch '},
			{DISPLAY_NAME: 'Interval Type', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' IntervalType_vch '},
			{DISPLAY_NAME: 'Paused', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' Paused_ti '},
			{DISPLAY_NAME: 'Start Date', CF_SQL_TYPE: 'CF_SQL_DATE', TYPE: 'DATE', SQL_FIELD_NAME: ' StartDate_dt '},
			{DISPLAY_NAME: 'End Date', CF_SQL_TYPE: 'CF_SQL_DATE', TYPE: 'DATE', SQL_FIELD_NAME: ' EndDate_dt '}
		],
		clearButton: true,
	error: function(ele){
	},
	clearCallback: function(){
		GetNOCList();
	}
	}, function(filterData){
		GetNOCList(filterData);
	});
}

// ultility
function isNumber(n) 
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}

// create new noc function
function CreateNewNOC() {
	if ($("#create-new-noc-form").validationEngine('validate')) {
		var name = $("#new-name").val();
		var url = $("#new-url").val();
		var intervalType = $("#new-interval-type").val();
		var startDate = $("#new-start-date-year").val() + "-" +
						$("#new-start-date-month").val() + "-" +
						$("#new-start-date-day").val();
		var startTime = $("#new-start-time-hour").val() + ":" +
						$("#new-start-time-minute").val() + ":" +
						$("#new-start-time-second").val();
		var endDate = '';
		if ($("#new-end-date-day").val() !== "" & $("#new-end-date-month").val() !== "" && $("#new-end-date-year").val() !== "") {
			endDate = $("#new-end-date-year").val() + "-" +
						$("#new-end-date-month").val() + "-" +
						$("#new-end-date-day").val();
		}
		var endTime = '';
		if ($("#new-end-time-second").val() !== "" & $("#new-end-time-minute").val() !== "" && $("#new-end-time-hour").val() !== "") {
			endTime = $("#new-end-time-hour").val() + ":" +
						$("#new-end-time-minute").val() + ":" +
						$("#new-end-time-second").val();
		}
		var interval = '';
		if ($("#new-interval-second").val() !== "" & $("#new-interval-minute").val() !== "" && $("#new-interval-hour").val() !== "") {
			interval = $("#new-interval-hour").val() + ":" +
						$("#new-interval-minute").val() + ":" +
						$("#new-interval-second").val();
		}
		var paused = $("#new-paused").closest('span').hasClass('checked') ? 1 : 0;
		var port = $("#new-port").val();
		var timeout = $("#new-time-out").val();

		var totalfail = $("#new-total-fail").val();
		var avgconnecttime = $("#new-avg-connecttime").val();
		var description = $("#new-description").val();

		$.ajax({
			url: '/session/sire/models/cfc/noc.cfc?method=CreateNewNOC' + strAjaxQuery,
			type: 'POST',
			data: {
				inpName: name,
				inpURL: url,
				inpIntervalType: intervalType,
				inpStartDate: startDate,
				inpStartTime: startTime,
				inpEndDate: endDate,
				inpEndTime: endTime,
				inpPaused: paused,
				inpPort: port,
				inpTimeOut: timeout,
				inpInterval: interval,
				inptotalfail: totalfail,
				inpavgconnecttime: avgconnecttime,
				inpdescription: description

			},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) === 1) {
				GetNOCList(lastFilter);
				$("#CreateNewNOCModal").modal('hide');
				alertBox(data.MESSAGE, 'Success');
			} else {
				if (data.ERRMESSAGE.indexOf("Duplicate entry") >= 0) {
					alertBox("NOC Name must be unique!", "Oops!");
					return false;
				}
				alertBox("Can not create new NOC, please check your input", "Oops!");
			}
		})
		.fail(function(e,msg) {
			console.log("error: "+msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});
	}
}

// clean create new modal function
function CleanNewModal() {
	var today = new Date();

	$("#new-name").val('');
	$("#new-url").val('');

	$("#new-interval-type").prop('selectedIndex', 0);

	$("#new-start-date-year").val(today.getFullYear());
	$("#new-start-date-month").val((parseInt(today.getMonth())+1) < 10 ? "0"+(parseInt(today.getMonth())+1) : (parseInt(today.getMonth())+1));
	$("#new-start-date-day").val(today.getDate() < 10 ? "0"+today.getDate() : today.getDate());

	$("#new-start-time-hour").val('00');
	$("#new-start-time-minute").val('00');
	$("#new-start-time-second").val('00');
	
	$("#new-end-date-year").val('');
	$("#new-end-date-month").val('');
	$("#new-end-date-day").val('');

	$("#new-end-time-hour").val('');
	$("#new-end-time-minute").val('');
	$("#new-end-time-second").val('');

	$("#new-interval-hour").val('');
	$("#new-interval-minute").val('');
	$("#new-interval-second").val('');

	$("#new-paused").closest('span').removeClass('checked');
	$("#new-port").val('80');
	$("#new-time-out").val('3600');

	$("#create-new-noc-form").validationEngine('hide');
}

// get one noc information function
function GetOneNOC (NOCId = 0) {
	if (NOCId <= 0) {
		alertBox('NOC ID is invalid!', 'Oops!');
		return false;
	} else {
		$.ajax({
			url: '/session/sire/models/cfc/noc.cfc?method=GetOneNOCById' + strAjaxQuery,
			type: 'POST',
			data: {inpNOCId: NOCId},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) === 1) {
				$("#edit-noc-id").val(NOCId);
				$("#edit-name").val(data.NOC.NAME);
				$("#edit-url").val(data.NOC.URL);

				$("#edit-interval-type").prop('selectedIndex', (parseInt(data.NOC.INTERVALTYPE)-1));

				var startDate = data.NOC.STARTDATE.split('-');

				$("#edit-start-date-year").val(startDate[2]);
				$("#edit-start-date-month").val(startDate[1]);
				$("#edit-start-date-day").val(startDate[0]);

				var startTime = data.NOC.STARTTIME.split(':');

				$("#edit-start-time-hour").val(startTime[0]);
				$("#edit-start-time-minute").val(startTime[1]);
				$("#edit-start-time-second").val(startTime[2]);

				if (data.NOC.ENDDATE !== "") {
					var endDate = data.NOC.ENDDATE.split('-');
					$("#edit-end-date-year").val(endDate[2]);
					$("#edit-end-date-month").val(endDate[1]);
					$("#edit-end-date-day").val(endDate[0]);
				} else {
					$("#edit-end-date-year").val('');
					$("#edit-end-date-month").val('');
					$("#edit-end-date-day").val('');
				}

				if (data.NOC.ENDTIME !== "") {
					var endTime = data.NOC.ENDTIME.split(':');
					$("#edit-end-time-hour").val(endTime[0]);
					$("#edit-end-time-minute").val(endTime[1]);
					$("#edit-end-time-second").val(endTime[2]);
				} else {
					$("#edit-end-time-hour").val('');
					$("#edit-end-time-minute").val('');
					$("#edit-end-time-second").val('');
				}

				if (data.NOC.INTERVAL !== "") {
					var interval = data.NOC.INTERVAL.split(':');
					$("#edit-interval-hour").val(interval[0]);
					$("#edit-interval-minute").val(interval[1]);
					$("#edit-interval-second").val(interval[2]);
				} else {
					$("#edit-interval-hour").val('');
					$("#edit-interval-minute").val('');
					$("#edit-interval-second").val('');
				}

				if (data.NOC.TOTALFAIL !== ""){
					 $("#edit-total-fail").val(data.NOC.TOTALFAIL);
				}else{
					$("#edit-total-fail").val('');
				}

				if (data.NOC.AVGCONNECTTIME !== ""){
					 $("#edit-avg-connecttime").val(data.NOC.AVGCONNECTTIME);
				}else{
					$("#edit-avg-connecttime").val('');
				}

				if (data.NOC.DESCRIPTION !== ""){
					 $("#edit-description").val(data.NOC.DESCRIPTION);
				}else{
					 $("#edit-description").val('');
				}
				
				if (data.NOC.PAUSED) {
					$("#edit-paused").closest('span').addClass('checked');
				} else {
					$("#edit-paused").closest('span').removeClass('checked');
				}
				
				$("#edit-port").val(data.NOC.PORT);
				$("#edit-time-out").val(data.NOC.TIMEOUT);

				$("#EditNOCModal").modal('show');
			} else {
				alertBox("Can get NOC information.", "Oops!");
				return false;
			}
		})
		.fail(function(e,msg) {
			console.log("error: " + msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});
		
	}
}

// edit noc function
function EditNOC() {
	if ($("#edit-noc-form").validationEngine('validate')) {
		var NOCId = $("#edit-noc-id").val();
		var name = $("#edit-name").val();
		var url = $("#edit-url").val();
		var intervalType = $("#edit-interval-type").val();
		var startDate = $("#edit-start-date-year").val() + "-" +
						$("#edit-start-date-month").val() + "-" +
						$("#edit-start-date-day").val();
		var startTime = $("#edit-start-time-hour").val() + ":" +
						$("#edit-start-time-minute").val() + ":" +
						$("#edit-start-time-second").val();
		var endDate = '';
		if ($("#edit-end-date-day").val() !== "" & $("#edit-end-date-month").val() !== "" && $("#edit-end-date-year").val() !== "") {
			endDate = $("#edit-end-date-year").val() + "-" +
						$("#edit-end-date-month").val() + "-" +
						$("#edit-end-date-day").val();
		}
		var endTime = '';
		if ($("#edit-end-time-second").val() !== "" & $("#edit-end-time-minute").val() !== "" && $("#edit-end-time-hour").val() !== "") {
			endTime = $("#edit-end-time-hour").val() + ":" +
						$("#edit-end-time-minute").val() + ":" +
						$("#edit-end-time-second").val();
		}
		var interval = '';
		if ($("#edit-interval-second").val() !== "" & $("#edit-interval-minute").val() !== "" && $("#edit-interval-hour").val() !== "") {
			interval = $("#edit-interval-hour").val() + ":" +
						$("#edit-interval-minute").val() + ":" +
						$("#edit-interval-second").val();
		}
		var paused = $("#edit-paused").closest('span').hasClass('checked') ? 1 : 0;
		var port = $("#edit-port").val();
		var timeout = $("#edit-time-out").val();

		var totalfail = $("#edit-total-fail").val();
		var avgconnecttime = $("#edit-avg-connecttime").val();
		var description = $("#edit-description").val();

		$.ajax({
			url: '/session/sire/models/cfc/noc.cfc?method=EditNOCById' + strAjaxQuery,
			type: 'POST',
			data: {
				inpNOCId: NOCId,
				inpName: name,
				inpURL: url,
				inpIntervalType: intervalType,
				inpStartDate: startDate,
				inpStartTime: startTime,
				inpEndDate: endDate,
				inpEndTime: endTime,
				inpPaused: paused,
				inpPort: port,
				inpTimeOut: timeout,
				inpInterval: interval,
				inptotalfail: totalfail,
				inpavgconnecttime: avgconnecttime,
				inpdescription: description
			},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) === 1) {
				GetNOCList(lastFilter);
				$("#EditNOCModal").modal('hide');
				alertBox(data.MESSAGE, 'Success');
			} else {
				alertBox("Can not edit NOC, please check your input", "Oops!");
			}
		})
		.fail(function(e,msg) {
			console.log("error: "+msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});
	}
}

// delete noc function
function DeleteNOC (NOCId = 0) {
	if (NOCId <= 0) {
		alertBox("NOC Id is invalid!", "Oops!");
		return false;
	} else {
		$.ajax({
			url: '/session/sire/models/cfc/noc.cfc?method=DeleteNOCById'+strAjaxQuery,
			type: 'POST',
			data: {inpNOCId: NOCId},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) === 1) {
				GetNOCList(lastFilter);
				alertBox(data.MESSAGE, "Success");
			} else {
				alertBox("Can not delete NOC.", "Oops!");
			}
		})
		.fail(function(e,msg) {
			console.log("error: "+msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});
		
	}
}

// process a bulk of noc function
function BulkNOCProcess (NOCIds = "", action = "") {
	if (NOCIds === "" || action === "") {
		alertBox("Action is invalid!", "Oops!");
		return false;
	} else {
		$.ajax({
			url: '/session/sire/models/cfc/noc.cfc?method=BulkNOCProcess'+strAjaxQuery,
			type: 'POST',
			data: {inpNOCIds: NOCIds, inpAction: action},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) === 1) {
				GetNOCList(lastFilter);
				alertBox(data.MESSAGE, "Success");
			} else {
				alertBox("Can not process action.", "Oops!");
			}
		})
		.fail(function(e,msg) {
			console.log("error: "+msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});
	}
}

function GetExtraReport(start, end) {
	$('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

	var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
	var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

	$.ajax({
		url: "/session/sire/models/cfc/noc.cfc?method=ExtraReport"+strAjaxQuery,
		method: "POST",
		dataType: "json",
		data: {
			inpStartDate: dateStart,
			inpEndDate: dateEnd
		},
		success: function(data){
			var series = [];
			var months = [];

			data.ChartList.forEach(function(item){
				
				var dayData;
				switch(item.format){
					case "day": {
						months.push(moment(item.Time).format('MMM DD'));
						dayData =  moment(item.Time).format('MMMM DD, YYYY');
					}
					break;
					case "week": {
						months.push(moment(item.Time).format('MMM DD'));
						dayData =  moment(item.Time).format('MMMM DD, YYYY');
					}
					break;
					case "month": {
						months.push(moment(item.Time).format('MMM DD YYYY'));
						dayData =  moment(item.Time).format('MMMM DD, YYYY');
					}
					break;
				}

				series.push({meta: dayData, value: item.Value});
				
			});


			var chart_data = {labels: months,series:[ series]};
			/* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */
			var options = {
				//fullWidth: true,
				chartPadding: {
					right: 0
				},
				axisY: {
					showGrid: false,
					offset: 20,
					onlyInteger: true,
					position: 'start',
				},
				axisX: { 
					labelOffset: {
					  x: 1,
					  y: 1
					}
				},
				plugins: [
					Chartist.plugins.tooltip({
						currency: "Session: ",
						class: 'chart-tooltip-custom',
						appendToBody: false
					})
				],
				low: 0,
				height: '300px',
				lineSmooth: false,
				classNames: {
					chart: 'ct-chart-line',
					//label: 'ct-label-custom',
					labelGroup: 'ct-labels',
					series: 'ct-series',
					line: 'ct-line',
					point: 'ct-point',
					area: 'ct-area',
					grid: 'ct-grid-custom',
					gridGroup: 'ct-grids',
					gridBackground: 'ct-grid-background',
					vertical: 'ct-vertical',
					horizontal: 'ct-horizontal',
					// start: 'ct-start',
					// end: 'ct-end'
				 }
			};
			
			new Chartist.Line('#highchart', chart_data, options);

		}
	});

	GetCountChatSessionList(dateStart, dateEnd);
}

function InitDateRange() {
	// init date range
	var start = moment().subtract(6, 'days');
	var end = moment();

	$('.reportrange').daterangepicker({
		startDate: start,
		endDate: end,
		"dateLimit": {
			"years": 1
		},
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	}, GetExtraReport);

	$('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

// get NOC list function
function GetCountChatSessionList(start, end, customFilterObj){
	//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	lastFilter = customFilterObj;

	//init datatable for active agent
	_CountChatSessionNOCList = $('#CountChatSession').dataTable( {
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 10,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumns": [
			{"mData": "Created_dt","sName": 'Created',"sTitle": 'Created', "sWidth": '220px',"bSortable": true, "sClass": "col-center"},
			{"mData": "RunTimeS","sName": 'Rumtime', "sTitle": 'Runtime (s)', "sWidth": '150px',"bSortable": true, "sClass": "col-center"},
			{"mData": "CountResult_int","sName": 'Chat Session No.', "sTitle": 'Chat Session No.', "sWidth": '200px',"bSortable": true, "sClass": "col-center"},
		],
		"sAjaxDataProp": "DATALIST",
		"sAjaxSource": '/session/sire/models/cfc/noc.cfc?method=GetCountSessionNOCReport' + strAjaxQuery,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

		},
		"fnDrawCallback": function( oSettings ) {

		},
		"fnHeaderCallback": function(nRow){
			$(nRow).find('th.selection').html('<i class="select-all glyphicon glyphicon-ok"></i>');
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
			);
			aoData.push(
				{ "name": "inpStartDate", "value": start}
			);
			aoData.push(
				{ "name": "inpEndDate", "value": end}
			);
			$.ajax({dataType: 'json',
				type: "POST",
				url: sSource,
				data: aoData,
				success: function (data) {
					$("#total-session").text(data['TOTALSESSION'])
					fnCallback(data);
				}
			});
		},
		"fnInitComplete":function(oSettings, json){
			$("#CountChatSession thead tr").find(':last-child').css("border-right","0px");
		}
	});
}