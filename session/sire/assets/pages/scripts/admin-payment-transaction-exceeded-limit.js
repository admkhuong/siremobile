
(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Transaction Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ltel.CreateDate_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ltel.UserId_int '},
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
			
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			GetList();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		GetList(filterData);
	});



	GetList();

	function GetList(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        
		//init datatable for active agent
		tblList = $('#tblList').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "sWidth": '7%',"bSortable": true,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": true},
                {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "sWidth": '17%',"bSortable": true},
				{"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "sWidth": '11%',"bSortable": true},
				{"mData": "TRANSACTION", "sName": 'Transaction', "sTitle": 'Transaction', "sWidth": '12%',"bSortable": true,"sClass": ""},
				{"mData": "ITEM", "sName": 'Item', "sTitle": 'Item', "sWidth": '12%',"bSortable": false,"sClass": ""},
				{"mData": "TRANSACTION_DATE", "sName": 'Transaction Date', "sTitle": 'Transaction Date', "sWidth": '12%',"bSortable": true,"sClass": ""},				
				{"mData": "TRANSACTION_AMOUNT", "sName": 'Amount($)', "sTitle": 'Amount($)', "sWidth": '8%',"bSortable": true},
				{"mData": "PAYMENTGATEWAY", "sName": 'GateWay', "sTitle": 'GateWay', "sWidth": '8%',"bSortable": true},
				
				
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListData",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=GetListTransactionExceededLimitLogs'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			   },
			"aaSorting": [[6,'desc']],
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblList thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	

})(jQuery);