

var new_cc_status = 0;
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var cardDataApprovalForever="";
function alertBox(message, title, callback) {
    title  = typeof(title) != "undefined" ? title : "Alert";
    bootbox.dialog({
        message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
        title: '&nbsp;',
        className: "be-modal",
        buttons: {
            success: {
                label: "OK",
                className: "green-gd",
                callback: function(){
                    typeof(callback) == "function" ? callback() : "";
                }
            },
        }
    });
}

//Filter bar initialize
$('#box-filter').sireFilterBar({
	fields: [
		{DISPLAY_NAME: 'Transaction Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' lpr.TransactionDate_dt '},
		{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' lpr.UserId_int '},
		{DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
		{DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
		
	],
	clearButton: true,
	// rowLimited: 5,
	error: function(ele){
		console.log(ele);
	},
	// limited: function(msg){
	// 	alertBox(msg);
	// },
	clearCallback: function(){
		GetList();
	}
}, function(filterData){
	//called if filter valid and click to apply button
	GetList(filterData);
});



GetList();

function GetList(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	var colAction = function(object){
		var strReturn="";	
		if(object.TRANSACTIONSTATUS_CODE == 0){
			strReturn+="<a href='#' class='r-detail' data-id='"+object.ID+"' data-userid='"+object.USERID+"' data-vaultid= '"+object.VAULTID+"'>Review</a>";
		}
		else
		{
			strReturn+="<a href='#' class='r-detail' data-id='"+object.ID+"' data-userid='"+object.USERID+"' data-vaultid= '"+object.VAULTID+"'>Detail</a>";
		}
		
		return strReturn;
	}
	//init datatable for active agent
	tblList = $('#tblList').dataTable( {
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 20,
		"aoColumns": [
			{"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "sWidth": '100px',"bSortable": true,"sClass": "col-center"},
			{"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "sWidth": '150px',"bSortable": true},
			{"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "sWidth": '200px',"bSortable": true},
			{"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "sWidth": '150px',"bSortable": true},
			{"mData": "TRANSACTION", "sName": 'Transaction', "sTitle": 'Transaction', "sWidth": '150px',"bSortable": true,"sClass": ""},				
			{"mData": "TRANSACTION_DATE", "sName": 'Date', "sTitle": 'Date', "sWidth": '140px',"bSortable": true,"sClass": ""},				
			{"mData": "TRANSACTION_AMOUNT", "sName": 'Amount($)', "sTitle": 'Amount($)', "sWidth": '80px',"bSortable": true},				
			{"mData": "NOTES", "sName": 'Notes', "sTitle": 'Notes', "sWidth": '200px',"bSortable": false},				
			{"mData": "GATEWAY", "sName": 'GW', "sTitle": 'GW', "sWidth": '150px',"bSortable": false},				
			{"mData": "TRANSACTIONSTATUS", "sName": 'Status', "sTitle": 'Status', "sWidth": '150px',"bSortable": false},				
			{"mData": colAction, "sName": 'Action', "sTitle": 'Action', "sWidth": '100px',"bSortable": false},				
			
			
		],
		"bAutoWidth": false,
		"sAjaxDataProp": "ListData",
		"sAjaxSource": '/session/sire/models/cfc/payment/payment.cfc?method=GetListTransactionReview'+strAjaxQuery,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			},
		"aaSorting": [[5,'desc']],
		"fnDrawCallback": function( oSettings ) {
			var paginateBar = this.siblings('div.dataTables_paginate');
			if (oSettings._iDisplayStart < 0){
				oSettings._iDisplayStart = 0;
				paginateBar.find('input.paginate_text').val("1");
			}
			if(oSettings._iRecordsTotal < 1){
				paginateBar.hide();
			} else {
				paginateBar.show();
			}
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
			);
			$.ajax({dataType: 'json',
						type: "POST",
						url: sSource,
						data: aoData,
						success: fnCallback
			});
		},
		"fnInitComplete":function(oSettings, json){
			$("#tblList thead tr").find(':last-child').css("border-right","0px");
		}
	});
}	

function GetCCCheckByID(id){
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/session/sire/models/cfc/payment/payment.cfc?method=GetCCCheckByID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:{					
				inpId:id				
            }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data){
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
function CodeToStatus(code){
	if(code==1)
	{
		return '<i class="fa fa-check text-success" title="Match"></i>';
	}
	else if(code==-1){
		return '<i class="glyphicon glyphicon-remove text-danger" title="Not Match"></i>';
	}
	else
	{
		return '<i class="fa fa-minus text-primary" title="Not Check"></i>';
	}
}
function AdminActionSuspiciousTransaction(id,action,cardDataApprovalForever){
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/session/sire/models/cfc/payment/payment.cfc?method=AdminActionSuspiciousTransaction&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:{					
				inpId:id,
				inpAction:action,
				inpCardDataApprovalForever: JSON.stringify(cardDataApprovalForever)
            }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to update your infomation at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data){
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
$(document).ready(function(){
	$(document).on("click",".r-detail",function(){
		var id= $(this).data("id");		

		GetCCCheckByID(id).then(function(data){
			var modal= $("#mdCCMatch");
			var dataCheck= data.DATACHECK;
			var cardData= data.CARDDATA;
			var profileData= data.PROFILE;

			modal.find("#transaction-id").val(id);
			modal.find('.submit-action').select2( { 
				theme: "bootstrap", 
				width: 'auto' ,				                   				
			});
			
			$(".admin-action").html("<div class='col-xs-12'></div>");
			if( parseInt(dataCheck.TRANSACTIONSTATUS) != 0){					
				$(".status-rows").addClass("hidden");	
				$(".admin-action").removeClass("hidden");	
				if(data.CCSATUS ==1){
					$(".admin-action").html("<div class='col-xs-12'><strong>Submitted action: Verified this time only</strong></div>");
				}	
				else if (data.CCSATUS ==2)			
				{
					$(".admin-action").html("<div class='col-xs-12'><strong>Submitted action: Verified forever</strong></div>");
				}
				else if (data.CCSATUS ==3)			
				{
					$(".admin-action").html("<div class='col-xs-12'><strong>Submitted action: Reject this time only</strong></div>");
				}
				else if (data.CCSATUS ==4)			
				{
					$(".admin-action").html("<div class='col-xs-12'><strong>Submitted action: Blocked forever</strong></div>");
				}
			}
			else
			{
				$(".status-rows").removeClass("hidden");
				$(".admin-action").addClass("hidden");	
						
			}
			modal.find('.submit-action').val(0).trigger("change");
			if(dataCheck.LASTPROFILEUPDATEDAY_VCH != ""){
				if(dataCheck.LASTPROFILEUPDATEDAY_VCH > 30){
					modal.find('.profile-update-text').html(dataCheck.LASTPROFILEUPDATEDAY_VCH);
					modal.find('.profile-update-match').html('<i class="fa fa-check text-success" title="Valid"></i>');
				}				
				else
				{
					modal.find('.profile-update-text').html(dataCheck.LASTPROFILEUPDATEDAY_VCH);
					modal.find('.profile-update-match').html('<i class="glyphicon glyphicon-remove text-danger" title="Less than 30 days"></i>');
				}				
			}		
			else
			{
				modal.find('.profile-update-text').html("Over 30 days");
				modal.find('.profile-update-match').html('<i class="fa fa-check text-success" title="Valid"></i>');
			}				
			
			//CARD
			modal.find('.card-number-text').html(cardData.inpNumber);
			modal.find('.fname-text').html(cardData.inpFirstName);
			modal.find('.lname-text').html(cardData.inpLastName);
			modal.find('.addr-text').html(cardData.inpLine1);
			modal.find('.city-text').html(cardData.inpCity);
			modal.find('.state-text').html(cardData.inpState);
			modal.find('.country-text').html(cardData.inpCountryCode);
			modal.find('.zip-text').html(cardData.inpPostalCode);
			modal.find('.email-text').html(cardData.inpEmail);
			
			//
			if(dataCheck.PHONEMATCH_TI==1)
			{
				modal.find('.phone-text').html("Valid carrier");
			}
			else if(dataCheck.PHONEMATCH_TI==-1)
			{
				modal.find('.phone-text').html("InValid carrier");
			}
			else
			{
				modal.find('.phone-text').html("Not Check");
			}
			//
			modal.find('.ip-text').html(cardData.inpIp);
			modal.find('.avs-text').html("<a href='#' data-toggle='modal' data-target='#mdAVSExplanation'>"+dataCheck.AVS_RESULT_VCH+"</a>");
			//PROFILE
			modal.find('.fname-profile').html(profileData.FIRSTNAME);
			modal.find('.lname-profile').html(profileData.LASTNAME);
			modal.find('.addr-profile').html(profileData.ADDRESS);
			modal.find('.city-profile').html(profileData.CITY);
			modal.find('.state-profile').html(profileData.STATE);
			modal.find('.country-profile').html(profileData.COUNTRY);
			modal.find('.zip-profile').html(profileData.POSTALCODE);
			modal.find('.email-profile').html(profileData.EMAIL);
			modal.find('.phone-profile').html(profileData.MFAPHONE);			
			//MATCH
			modal.find('.fname-match').html(CodeToStatus(dataCheck.FIRSTNAMEMATCH_TI));
			modal.find('.lname-match').html(CodeToStatus(dataCheck.LASTNAMEMATCH_TI));
			modal.find('.addr-match').html(CodeToStatus(dataCheck.ADDRESSMATCH_TI));
			modal.find('.city-match').html(CodeToStatus(dataCheck.CITYMATCH_TI));
			modal.find('.state-match').html(CodeToStatus(dataCheck.STATEMATCH_TI));
			modal.find('.country-match').html(CodeToStatus(dataCheck.COUNTRYMATCH_TI));
			modal.find('.zip-match').html(CodeToStatus(dataCheck.POSTCODEMATCH_TI));
			modal.find('.email-match').html(CodeToStatus(dataCheck.EMAILMATCH_TI));
			
			if(dataCheck.AVS_RESULT_VCH=="00" || dataCheck.AVS_RESULT_VCH=="01" || dataCheck.AVS_RESULT_VCH=="02" || dataCheck.AVS_RESULT_VCH=="10" || dataCheck.AVS_RESULT_VCH=="11" || dataCheck.AVS_RESULT_VCH=="14" || dataCheck.AVS_RESULT_VCH=="30"
			|| dataCheck.AVS_RESULT_VCH=="D"
			|| dataCheck.AVS_RESULT_VCH=="M"
			|| dataCheck.AVS_RESULT_VCH=="Y"
			
			|| dataCheck.AVS_RESULT_VCH=="X"
			|| dataCheck.AVS_RESULT_VCH=="L"
			|| dataCheck.AVS_RESULT_VCH=="P"
			|| dataCheck.AVS_RESULT_VCH=="Z"
			|| dataCheck.AVS_RESULT_VCH=="W"
			|| dataCheck.AVS_RESULT_VCH=="G"
			|| dataCheck.AVS_RESULT_VCH=="I"
			|| dataCheck.AVS_RESULT_VCH=="S"
			){
				modal.find('.avs-match').html('<i class="fa fa-check text-success" title="Match"></i>');
			}
			else
			{
				modal.find('.avs-match').html('<i class="glyphicon glyphicon-remove text-danger" title="Not Match"></i>');
			}
			
			if(dataCheck.PHONEMATCH_TI==1)
			{
				modal.find('.phone-match').html('<i class="fa fa-check text-success" title="Valid Carrier"></i>');
			}
			else if(dataCheck.PHONEMATCH_TI==-1)
			{
				modal.find('.phone-match').html('<i class="glyphicon glyphicon-remove text-danger" title="Invalid Carrier"></i>');
			}
			else
			{
				modal.find('.phone-match').html('<i class="fa fa-minus text-primary" title="Not Check"></i>');			
			}
			if(dataCheck.IPMATCH_TI==1){
				modal.find('.ip-match').html('<i class="fa fa-check text-success" title="IP distance less than 100 miles"></i>');
				modal.find('.ip-text').html("IP distance less than 100 miles ["+dataCheck.IPTOIP_MILE_DEC+" miles]");
			}
			else if(dataCheck.IPMATCH_TI==-1)
			{
				modal.find('.ip-match').html('<i class="glyphicon glyphicon-remove text-danger" title="IP distance over 100 miles"></i>');
				modal.find('.ip-text').html("IP distance over 100 miles ["+dataCheck.IPTOIP_MILE_DEC+" miles]");
			}
			else
			{
				modal.find('.ip-match').html('<i class="fa fa-minus text-primary" title="Not Check"></i>');			
				modal.find('.ip-text').html("Not Check");	
			}			
			
			
			//
			cardDataApprovalForever={
				inpFirstName:cardData.inpFirstName.trim(),
				inpLastName:cardData.inpLastName.trim(),
				inpCity:cardData.inpCity.trim(),
				inpLine1:cardData.inpLine1.trim(),
				inpState:cardData.inpState.trim(),
				inpPostalCode:cardData.inpPostalCode.trim(),
				inpCountryCode:cardData.inpCountryCode.trim()
			}
						
			
			modal.modal('show');
		}).catch(function(data){
			alertBox(data.MESSAGE,"Oops!");
		})		
	});
	$(document).on("click","#btnSubmit",function(){
		if ($("#frCCMatch").validationEngine('validate')) {			
			var modal= $("#mdCCMatch");
			var action= modal.find("#submit-action").val();
			var id= modal.find("#transaction-id").val();
			if(id > 0 && action > 0)
			{
				AdminActionSuspiciousTransaction(id,action,cardDataApprovalForever).then(function(data){
					GetList();
					modal.modal("hide");
					alertBox(data.MESSAGE,"Success");
				}).catch(function(data){
					alertBox(data.MESSAGE,"Oops!");
				});
			}
		
		}
		
	});
	
});

