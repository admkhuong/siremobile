function sendMail(TID){
		
		bootbox.dialog({
	        message: 'Are you want to sendmail to client?',
	        title: 'Recurring Report',
	        className: "",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "btn btn-medium btn-success-custom",
	                callback: function(result) {
	                    if(result){
						  	try{
								$.ajax({
								type: "POST",
								url: '/public/sire/models/cfc/reportings.cfc?method=SendMailRecurringLogItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								dataType: 'json',
								data: { TID : TID},
								beforeSend: function( xhr ) {
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
									    message: "Delete Fail",
									    title: "Recurring Report",
									    buttons: {
									        success: {
									            label: "Ok",
									            className: "btn btn-medium btn-success-custom",
									            callback: function() {}
									        }
									    }
									});
								},					  
								success:		
									function(d) {
										$('#processingPayment').hide();
										if(d.RXRESULTCODE > 0)
										{
											// reload data
											var oTable = $('#tblListEMS').dataTable();
										  	oTable.fnDraw();
											return;
										}
										else
										{
				
											if (d.DATA.ERRMESSAGE[0] != '') {
												 bootbox.alert("Email don't send'.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
											}
										}
									} 		
								});
							}catch(ex){
								$('#processingPayment').hide();
								bootbox.dialog({
										        message: "Send Fail",
										        title: "Recurring Report",
										        buttons: {
										            success: {
										                label: "Ok",
										                className: "btn btn-medium btn-success-custom",
										                callback: function() {
										                }
										            }
										        }
								});
							}
						}
	                }
	            },
	            cancel: {
	                label: "Cancel",
	                className: "btn btn-medium btn-back-custom",
	                callback: function() {

	                }
	            },
	        }
	    });
		
	}

	// delete EMS
	function deleteEMS(TID){
		bootbox.dialog({
	        message: 'Are you sure you want to delete this item?',
	        title: 'Recurring Report',
	        className: "",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "btn btn-medium btn-success-custom",
	                callback: function(result) {
	                    if(result){
						  	try{
								$.ajax({
								type: "POST",
								url: '/public/sire/models/cfc/reportings.cfc?method=DeleteRecurringLogItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								dataType: 'json',
								data: { TID : TID},
								beforeSend: function( xhr ) {
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
									    message: "Delete Fail",
									    title: "Recurring Report",
									    buttons: {
									        success: {
									            label: "Ok",
									            className: "btn btn-medium btn-success-custom",
									            callback: function() {}
									        }
									    }
									});
								},					  
								success:		
									function(d) {
										$('#processingPayment').hide();
										if(d.RXRESULTCODE > 0)
										{
											// reload data
											var oTable = $('#tblListEMS').dataTable();
										  	oTable.fnDraw();
											return;
										}
										else
										{
				
											if (d.DATA.ERRMESSAGE[0] != '') {
												 bootbox.alert("This item has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
											}
										}
									} 		
								});
							}catch(ex){
								$('#processingPayment').hide();
								bootbox.dialog({
										        message: "Delete Fail",
										        title: "Recurring Report",
										        buttons: {
										            success: {
										                label: "Ok",
										                className: "btn btn-medium btn-success-custom",
										                callback: function() {
										                }
										            }
										        }
								});
							}
						}
	                }
	            },
	            cancel: {
	                label: "Cancel",
	                className: "btn btn-medium btn-back-custom",
	                callback: function() {

	                }
	            },
	        }
	    });
		
		
	}


	function ReRunBilling(TID){
		bootbox.dialog({
	        message: 'Are you want to Re-run Billing?',
	        title: 'Recurring Report',
	        className: "",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "btn btn-medium btn-success-custom",
	                callback: function(result) {
	                    if(result){
						  	try{
								$.ajax({
								type: "POST",
								url: '/public/sire/models/cfc/reportings.cfc?method=ReRunBilling&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								dataType: 'json',
								data: { TID : TID},
								beforeSend: function( xhr ) {
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
									    message: "Delete Fail",
									    title: "Recurring Report",
									    buttons: {
									        success: {
									            label: "Ok",
									            className: "btn btn-medium btn-success-custom",
									            callback: function() {}
									        }
									    }
									});
								},					  
								success:		
									function(d) {
										$('#processingPayment').hide();
										if(d.RXRESULTCODE > 0)
										{
											
											// reload data
											var oTable = $('#tblListEMS').dataTable();
										  	oTable.fnDraw();
											return;
										}
										else
										{
				
											if (d.ERRMESSAGE[0] != '') {
												 //bootbox.alert(d.MESSAGE + "\n" + d.ERRMESSAGE, function() { return; } );
												 bootbox.dialog({
												    message: d.MESSAGE + "\n" + d.ERRMESSAGE,
												    title: "Recurring Report",
												    buttons: {
												        success: {
												            label: "Ok",
												            className: "btn btn-medium btn-success-custom",
												            callback: function() {}
												        }
												    }
												});
											}
										}
									} 		
								});
							}catch(ex){
								$('#processingPayment').hide();
								bootbox.dialog({
										        message: "Re-run Billing Fail",
										        title: "Recurring Report",
										        buttons: {
										            success: {
										                label: "Ok",
										                className: "btn btn-medium btn-success-custom",
										                callback: function() {
										                }
										            }
										        }
								});
							}
						}
	                }
	            },
	            cancel: {
	                label: "Cancel",
	                className: "btn btn-medium btn-back-custom",
	                callback: function() {

	                }
	            },
	        }
	    });
		
	}
	function isNumber(n) 
	{
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}

(function($){

	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var StatusCustomData = function(){
	var options   = '<option value= "-100" selected>All</option>'+
					'<option value="0">Open</option>'+
					'<option value="-1">In Progess</option>'+
					'<option value="-2">Tech Review</option>'+
					'<option value="-3">QA Review</option>'+
					'<option value="1">Closed</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	var StatusAccRec = function(){
	var options   = '<option value= "ALL" selected>All</option>'+
					'<option value="RECURRING_PLAN">RECURRING PLAN</option>'+
					'<option value="RECURRING_KEYWORD">RECURRING KEYWORD</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Payment Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' l.PaymentDate_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' l.UserId_int '},
			{DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' l.EmailAddress_vch '},
			{DISPLAY_NAME: 'Paid Account Shut Down', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'STATUS', SQL_FIELD_NAME: 'Recurringfail', CUSTOM_DATA:StatusAccRec},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' l.Status_ti ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});



	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				// {"mData": "ID", "sName": 'Id', "sTitle": 'ID', "sWidth": '10%',"bSortable": false},
				{"mData": "ACCOUNT_ID", "sName": 'Id', "sTitle": 'Account ID', "sWidth": '6%',"bSortable": false},
				{"mData": "EMAIL", "sName": 'Id', "sTitle": 'Email', "sWidth": '10%',"bSortable": false},
				{"mData": "PLAN", "sName": 'Id', "sTitle": 'Plan', "sWidth": '8%',"bSortable": false},
				// {"mData": "PLAN_ID", "sName": 'Id', "sTitle": 'Plan ID', "sWidth": '4%',"bSortable": false},
				// {"mData": "PURCHASED_KEYWORD", "sName": 'Id', "sTitle": 'Purchased Keyword', "sWidth": '8%',"bSortable": false},
				{"mData": "CREDIT_CARD_NUMBER", "sName": 'Id', "sTitle": 'Card Number', "sWidth": '8%',"bSortable": false},
				// {"mData": "DUE_DATE", "sName": 'Id', "sTitle": 'Due Date', "sWidth": '8%',"bSortable": false},
				{"mData": "PAYMENT_DATE", "sName": 'Id', "sTitle": 'Payment Date', "sWidth": '10%',"bSortable": true},
				{"mData": "AMOUNT", "sName": 'Id', "sTitle": 'Amount($)', "sWidth": '8%',"bSortable": false},
				{"mData": "OF_PAYMENT_FAIL", "sName": 'Id', "sTitle": 'Payment Status', "sWidth": '8%',"bSortable": false},
				{"mData": "REASON", "sName": 'Id', "sTitle": 'Reason', "sWidth": '15%',"bSortable": false},
				{"mData": "SENT_EMAIL", "sName": 'Id', "sTitle": 'Sent email', "sWidth": '8%',"bSortable": false},
				{"mData": "STATUS", "sName": 'Id', "sTitle": 'Status', "sWidth": '8%',"bSortable": false},
				{"mData": "GW", "sName": 'GW', "sTitle": 'GW', "sWidth": '8%',"bSortable": false},
				{"mData": "RE_RUN_BILLING", "sName": 'link', "sTitle": 'Billing', "sWidth": '8%',"bSortable": false},
				{"mData": "OPTION_LINKS", "sName": 'link', "sTitle": 'Action', "sWidth": '8%',"bSortable": false}
				
				
			],
			"bAutoWidth": false,
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/public/sire/models/cfc/reportings.cfc?method=GetRecurringLog'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	


	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
	});

})(jQuery);