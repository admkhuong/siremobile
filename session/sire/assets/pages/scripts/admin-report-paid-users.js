function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var StatusCustomData = function(){
	var options   = '<option value= "-100" selected> </option>'+
					'<option value="0">Pending</option>'+
					'<option value="-1">Declined</option>'+
					'<option value="-2">Error</option>'+
					'<option value="1">Approved</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Payment Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' pkw.UpdateDate '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.MFAContactString_vch '},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' pkw.PaymentStatus ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});



	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {
			var strReturn = '<a href="" data-user-plan-id ="'+object.USERPLANID+'" data-user-id ="'+object.USERID+'" class="account-payment-detail" title="Account Payment Detail">Payment Detail</a>';
            return strReturn;
        }
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": false},
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '17%',"bSortable": false},
                {"mData": "PHONE", "sName": 'u.MFAContactString_vch', "sTitle": 'Phone', "sWidth": '11%',"bSortable": false},
				{"mData": "PAYMENT_DATE", "sName": 'pkw.UpdateDate', "sTitle": 'Recurring Date', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				{"mData": "BUYKEWORD", "sName": 'BUYKEWORD', "sTitle": 'Buy Keyword', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				{"mData": "PLANNAME", "sName": 'PLANNAME', "sTitle": 'Plan', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
                // {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "auto"},
                
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=GetAdminListPaidUsers'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	


	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    $("#tblListEMS").on("click", ".account-payment-detail", function(event){
        event.preventDefault();
        var userId = $(this).data('user-id');
        var userPlanId = $(this).data('user-plan-id');
        if( parseInt(userId) > 0 && parseInt(userPlanId) > 0)
            $.ajax({
                url: '/session/sire/models/cfc/billing.cfc?method=RetrieveCustomer'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
					userid:userId,
					inpUserPlanid: userPlanId
                },
                beforeSend: function(){
                	$('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Action Error!','Decline Payment Transaction',''); 
                    $('#processingPayment').hide();
                },
                success: function(data) {
                    if ( parseInt(data.RXRESULTCODE) == 1) {
	                    var expirationDate = data.CUSTOMERINFO.DATA.expirationDate;
	    				var maskedNumber = data.CUSTOMERINFO.DATA.maskedNumber;
	    				var line1 = data.CUSTOMERINFO.DATA.line1;
	    				var city = data.CUSTOMERINFO.DATA.city;
	    				var state = data.CUSTOMERINFO.DATA.state;
	    				var zip = data.CUSTOMERINFO.DATA.zip;
	    				var country = data.CUSTOMERINFO.DATA.country;
	    				var firstName = data.CUSTOMERINFO.DATA.firstName;
    					var lastName =data.CUSTOMERINFO.DATA.lastName;

	    				var html = "<b>Card Information </b>: " + maskedNumber + ' <br/> <b>Expires in</b>: ' + expirationDate;
	    				html+= "<br/> <b> Name </b>: "+firstName+" "+lastName;	
	    				html+=" <br/> <b>Billing Address</b>: <Br/>";
	    				html+= +line1+"<br/>"+city+"<Br/>"+state+", "+zip+"<Br/>"+country;
	                    alertBox(html,'Billing Info');
                    } else {
                         alertBox('Can not get user payment data');
                    }                       
                    $('#processingPayment').hide();
                }
            });
    });

})(jQuery);