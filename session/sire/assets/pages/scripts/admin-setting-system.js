(function($){

	var formAdminSet = $('#admin-sett-sys');
	// $("SELECT").select2( { theme: "bootstrap", width: 'auto'} );
    //var noti_method = <cfoutput>#RetValGetCalendarConfiguration.NOTIFIMETHOD#</cfoutput>;

  	if(formAdminSet.length){
  		formAdminSet.validationEngine('attach',  {promptPosition : "topLeft:0", showArrow: true});
    }
      
    

    // formAdminSet.on('submit', function(event){
    //     event.preventDefault();
    //     if (typeof(noti_method) != "undefined"){
           
    //     }else{
    //         alertBox("Save Payment Method Successfully.", "Payment Method Settings");
    //     }
      // });
    function UpdateTransactionExceededLimit(paymentType,value){
        if ($('#admin-sett-sys').validationEngine('validate', {scroll: false ,focusFirstField : false})) {
            if(paymentType !="")
            {
                $.ajax({
                    type:"POST",
                    url: '/session/sire/models/cfc/billing.cfc?method=UpdateTransactionExceededLimit&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
                    dataType:'json',
                    data:{					
                            inpPaymentMethod: paymentType,
                            inpValue:value
                    }
                    ,beforeSend:function(xhr){
                        $('#processingPayment').show()
                    }
                    ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                        $('#processingPayment').hide()
                        alertBox("Unable to update your infomation at this time. An error occurred.","Oops!");
                        
                    }
                    ,success:function(data){
                        $('#processingPayment').hide();
                        if(data.RXRESULTCODE==1){						
                            alertBox(data.MESSAGE,"Success");
                            GetAllTransactionExceededLimit();
                        }
                        else
                        {
                            alertBox(data.MESSAGE,"Oops!");
                        }					
                    }
                });                         
            }
        }
    }
    function GetAllTransactionExceededLimit(){
        $.ajax({
            type:"POST",
            url: '/session/sire/models/cfc/billing.cfc?method=GetAllTransactionExceededLimit&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            dataType:'json',
            data:{					
                    
            }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                $('#processingPayment').hide()
                alertBox("Unable to get your infomation at this time. An error occurred.","Oops!");
                
            }
            ,success:function(data){
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						                                        
                    data.DATALIST.map(function(obj){
                        $("#transaction-limit-"+obj.PAYMENTMETHOD).val(obj.VALUE);                        
                    })                   
                }
                else
                {
                    alertBox(data.MESSAGE,"Oops!");
                }					
            }
        }); 
    }
    $(document).ready(function(){
        $('input[type=radio][name=payment_method]').change(function() {
            $.ajax({
                method: 'POST',
                url: '/session/sire/models/cfc/billing.cfc?method=SavePaymentMethodSetting&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
                data:{
                    inppaymentmethod: this.value
                },
                dataType: 'json',
                success: function(data) {
                    if (data && data.RXRESULTCODE == 1) { 
                        alertBox(data.MESSAGE, "Payment Method Settings");
                    } else {
                        alertBox(data.MESSAGE, "Payment Method Settings");
                    }
                }
            });
        });
        // 
        GetAllTransactionExceededLimit();
        $(".save-transaction-limit").on("click",function(){
            
            var paymentType= $(this).data("payment-method");
            var value= $("#transaction-limit-"+paymentType).val() || '';            
            UpdateTransactionExceededLimit(paymentType,value);
            
        });
    });

})(jQuery);