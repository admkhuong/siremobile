var _ShortCodeList;
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var lastFilter = '';

$(document).ready(function(){
	// get shortcode list
	GetShortCodeList();

	// init filter bar 
	InitFilter();

	$("body").on('click', '.edit-status', function(event) {
		event.preventDefault();
		var id = $(this).data('pkid');
		var status = $(this).data('status');

		ChangeShortcodeStatus(id, status);
	});

	$("body").on('click', '.set-default', function(event) {
		event.preventDefault();
		var id = $(this).data('pkid');

		SetDefaultShortCode(id);
	});
});

// get NOC list function
function GetShortCodeList(customFilterObj){
	//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	lastFilter = customFilterObj;

	var showAction = function (object) {
		var strReturn = '';
		if (!object.IsDefault) {
			strReturn = '<a title="Set as default" class="set-default" data-pkid="'+object.ShortCodeId+'"><i class="fa fa-star-o" style="font-size:15px" aria-hidden="true"></i></a>&nbsp;';
		}
		return strReturn;
	}

	var showDefault = function (object) {
		var strReturn = object.IsDefault ? "True" : "";
		return strReturn;
	}

	//init datatable for active agent
	_ShortCodeList = $('#ShortCodeList').dataTable( {
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 10,
		"aaSorting": [[ 2, "desc" ]],
		"aoColumns": [
			{"mData": "ShortCodeId","sName": 'ShortCodeId', "sTitle": 'ID', "sWidth": '100px',"bSortable": true, "sClass": "col-center"},
			{"mData": "ShortCode","sName": 'ShortCode', "sTitle": 'ShortCode', "sWidth": '180px',"bSortable": true},
			{"mData": showDefault,"sName": 'Default', "sTitle": 'Is Default', "sWidth": '100px',"bSortable": false, "sClass": "col-center"},
			{"mData": showAction,"sName": 'Results', "sTitle": 'Action', "sWidth": '100px',"bSortable": false, "sClass": "col-center"},
		],
		"sAjaxDataProp": "DATALIST",
		"sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetShortCodeList' + strAjaxQuery,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (parseInt(aData.IsDefault) === 1) {
				$(nRow).css('font-weight', 'bold');
			}
		},
		"fnDrawCallback": function( oSettings ) {

		},
		"fnHeaderCallback": function(nRow){
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
			);
			$.ajax({dataType: 'json',
				type: "POST",
				url: sSource,
				data: aoData,
				success: function (data) {
					fnCallback(data);
				}
			});
		},
		"fnInitComplete":function(oSettings, json){
		}
	});
}

// init filter bar function
function InitFilter() {
	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Short Code', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ShortCode_vch '},
			{DISPLAY_NAME: 'Is Default', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' IsDefault_ti '}
		],
		clearButton: true,
	error: function(ele){
	},
	clearCallback: function(){
		GetShortCodeList();
	}
	}, function(filterData){
		GetShortCodeList(filterData);
	});
}

function ChangeShortcodeStatus (shortcodeid, status) {
	if ((shortcodeid <= 0) || (status !== 0 && status !== 1)) {
		alertBox("Input's invalid!");
		return false;
	}
	$.ajax({
		url: '/session/sire/models/cfc/admin.cfc?method=ChangeShortCodeStatus' + strAjaxQuery,
		type: 'POST',
		data: {
			inpShortCodeId: shortcodeid,
			inpChangeStatus: (1 - parseInt(status))
		},
		beforeSend: function () {
			$("#processingPayment").show();
		}
	})
	.done(function(data) {
		if (parseInt(data.RXRESULTCODE) === 1) {
			alertBox(data.MESSAGE, "Success");
			GetShortCodeList(lastFilter);
		} else {
			alertBox("Change status failed!");
		}
	})
	.fail(function(e, msg) {
		console.log("error: " + msg);
	})
	.always(function() {
		$("#processingPayment").hide();
	});
}

function SetDefaultShortCode (shortcodeid) {
	if (shortcodeid <= 0) {
		alertBox("Input's invalid!");
		return false;
	}
	$.ajax({
		url: '/session/sire/models/cfc/admin.cfc?method=SetDefaultShortCode' + strAjaxQuery,
		type: 'POST',
		data: {
			inpShortCodeId: shortcodeid
		},
		beforeSend: function () {
			$("#processingPayment").show();
		}
	})
	.done(function(data) {
		if (parseInt(data.RXRESULTCODE) === 1) {
			alertBox(data.MESSAGE, "Success");
			GetShortCodeList(lastFilter);
		} else {
			alertBox("Set default shortcode failed!");
		}
	})
	.fail(function(e, msg) {
		console.log("error: " + msg);
	})
	.always(function() {
		$("#processingPayment").hide();
	});
}