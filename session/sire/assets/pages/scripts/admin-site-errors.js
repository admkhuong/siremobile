(function($){
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var statusFieldControl= function(){
		return $('<select class="filter-form-control form-control"></select>').append('<option value="-1">All</option><option value="0">Open</option><option value="4">Closed</option>');
	};

	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Request URL', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' se.RequestUrl_txt '},
			{DISPLAY_NAME: 'Created date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' se.Created_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' se.UserId_int '},
        	{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' se.Status_int ', CUSTOM_DATA: statusFieldControl}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			GetList();
		}
	}, function(dataFilter){
		GetList(dataFilter);
	});


	GetList();

	function GetList(customFilterData){

		customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   

		var requestUrlCol = function(object){
			return $('<a></a>').attr('href', object.REQUEST_URL).attr('target', '_blank').text(object.REQUEST_URL)[0].outerHTML;
		},

		createdDateCol = function(object){
			return moment(object.CREATED_DT).tz("America/Los_Angeles").format('YYYY-MM-DD');
		},

		actionCol = function(object){
			return  $('<a></a>').attr("data-toggle", "modal").attr("data-target", "#AdminErrorModal")
								.attr("href", "admin-site-error-details?errid="+object.ID)
								.html("<i class='glyphicon glyphicon-edit'></i>")[0].outerHTML;
		};



		var table = $('#tblSiteErrorList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id","sWidth":"8%"},
				{"mData": "ERROR_MSG", "sName": 'error_msg', "sTitle": 'Error Message',"bSortable": true,"sClass":"error_msg","sWidth":"22%"},
				{"mData": requestUrlCol, "sName": 'request_url', "sTitle": 'Request Url',"bSortable": false,"sClass":"request_url","sWidth":"20%"},
				{"mData": "USER_ID", "sName": 'user_id', "sTitle": 'User ID',"bSortable": false,"sClass":"user_id","sWidth":"10%"},
				{"mData": "USER_IP", "sName": 'user_ip', "sTitle": 'IP',"bSortable": false,"sClass":"user_ip","sWidth":"10%"},
				{"mData": createdDateCol, "sName": 'created_date', "sTitle": 'Created',"bSortable": false,"sClass":"created_date", "sWidth":"10%"},
				{"mData": "STATUS", "sName": 'status', "sTitle": 'Status',"bSortable": false,"sClass":"created_date", "sWidth":"10%"},
				{"mData": actionCol, "sName": 'action', "sTitle": 'Actions',"bSortable": false,"sClass":"action", "sWidth":"10%"}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/public/sire/models/cfc/error_logs.cfc?method=GetErrorList'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				aoData.push(
	            	{ "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings){

			}
		});

		return function(){
			table.fnStandingRedraw();
		}

	};


	



})(jQuery);