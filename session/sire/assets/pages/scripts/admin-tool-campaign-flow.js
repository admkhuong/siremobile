(function($) {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    var fnGetDataList = function (searchName){

        var actionBtn = function (object) {
            var strReturn = '';

            switch(object.Status_int) {
                case 0:
                    strReturn = '<a href="/session/sire/pages/admin-tool-campaign-flowedit?campaignid='+object.ID+'" class="btn-re-xs"><i class="fa fa-pencil-square-o" style="color:#fff"></i>Edit</a> ';
                    break;
                case 1:
                    strReturn = '<a href="/session/sire/pages/admin-tool-campaign-flowpause?campaignid='+object.ID+'" class="btn-re-xs pause-alert-campaign" data-campaignid="'+object.ID+'"><i class="fa fa-pause" style="color:#fff"></i>Pause</a> ';
                    break;
                case 2:
                    strReturn = '<a href="/session/sire/pages/admin-tool-campaign-flowplay?campaignid='+object.ID+'" class="btn-re-xs play-alert-campaign" data-campaignid="'+object.ID+'"><i class="fa fa-play" style="color:#fff"></i>Continue</a> ';
            } 

            strReturn += '<a href="/session/sire/pages/admin-tool-campaign-report?campaignid='+object.ID+'" class="btn-re-xs">Report</a>';

            return strReturn;
        }

        var table = $('#admin-campaign-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "CampaignName_vch", "sName": 'CampaignName_vch', "sTitle": 'Campaign name', "bSortable": false, "sClass": "", "sWidth": ""},
                {"mData": "Subject_vch", "sName": 'Subject_vch', "sTitle": 'Message', "bSortable": false, "sClass": "", "sWidth": ""},
                {"mData": "SegmentName_vch", "sName": 'SegmentName_vch', "sTitle": 'Segment', "bSortable": false, "sClass": "", "sWidth": ""},
                {"mData": "ListCount_int", "sName": 'ListCount_int', "sTitle": 'List count', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "Created_dt", "sName": 'Created_dt', "sTitle": 'Created date', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "Trigger_bi", "sName": 'Trigger_bi', "sTitle": 'Trigger type', "bSortable": false, "sClass": "", "sWidth": ""},
                // {"mData": reportLink, "sName": 'Report', "sTitle": 'Report', "bSortable": false, "sClass": "", "sWidth": ""},
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "200px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=getCampaigns' + strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                aoData.push(
                    { "name": "inpSearchName", "value": searchName}
                );

                oSettings.jqXHR = $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "aaSorting": [[4,'desc']]
        });
    }

    function SaveMessage(inpMessageID)
    {        
        if ($('#form-edit-message').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var inpSubject=$("#message-name").val();
            var inpSMSAlert=$("#message-message").val();
            var inpWebAlert=$("#message-web").val();
            var inpMailAlert=$("#message-mail").val();
            $.ajax({
                url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertUpdateMessage'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpMessageID:inpMessageID,
                    inpSubject: inpSubject,
                    inpSMSAlert: inpSMSAlert,
                    inpWebAlert: inpWebAlert,
                    inpMailAlert: inpMailAlert
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    if(inpMessageID ==0)
                    {alertBox('Define Message Fail','Define Message Fail',''); }
                    else
                    {alertBox('Update Message Fail','Update Message Fail',''); }
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        $("#edit-message").modal('hide');
                        $(".modal-backdrop").fadeOut();
                        fnGetDataList(); 
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Define Message Fail','');
                    }
                }
            });
        }
        else
        {                           
            return false;
        }
    }
    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();        
        $( "#searchbox" ).on( "keypress", function(e) { 
            if (e.keyCode == 13)
            {
                fnGetDataList($( this ).val()); 
            }            
        });            
        $("#form-edit-message").on( "submit", function(e) {   
            e.preventDefault();          
            var messageId= $("#messageId").val();
            SaveMessage(messageId); 
            //return false;
        });  
        var table = $('#adm-message-list').DataTable();     
        $('#adm-message-list tbody').on('click', '.edit-message-link', function (e) {      
            //e.preventDefault();        
            $("#modal-tit").text("EDIT MESSAGE");
        });        
        //
        $('#edit-message').on('show.bs.modal', function (event) {            
            var button = $(event.relatedTarget) // Button that triggered the modal
            var modal = $(this)                        

            modal.find("#messageId").val(button.data('id'));    
            modal.find("#message-name").val(button.data('mes-subject'));            
            modal.find("#message-message").val(button.data('mes-sms'));
            modal.find("#message-web").val(button.data('mes-web'));
            modal.find("#message-mail").val(button.data('mes-mail'));
        })
        //
    });

    $('#admin-campaign-list').on('click', '.pause-alert-campaign', function(event){
        event.preventDefault();
        var self = $(this);
        var campaignid = self.data('campaignid');

        $.ajax({
            url: '/session/sire/models/cfc/admin-tool.cfc?method=pauseCampaign'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpCampaignId:campaignid
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();
                alertBox('Pause Campaign Fail','Pause Campaign','');
            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    self.html('<i class="fa fa-play" style="color:#fff"></i>Continue').removeClass('pause-alert-campaign').addClass('play-alert-campaign');
                } else {
                    if (data.MESSAGE)  {
                        alertBox(data.MESSAGE,'Pause Campaign','');
                    } else {
                        alertBox('Pause Campaign Fail','Pause Campaign','');
                    }
                }
            }
        });
    });

    $('#admin-campaign-list').on('click', '.play-alert-campaign', function(event){
        event.preventDefault();
        var self = $(this);
        var campaignid = self.data('campaignid');

        $.ajax({
            url: '/session/sire/models/cfc/admin-tool.cfc?method=playCampaign'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpCampaignId:campaignid
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();
                alertBox('Pause Campaign Fail','Pause Campaign','');
            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    self.html('<i class="fa fa-pause" style="color:#fff"></i>Pause').removeClass('play-alert-campaign').addClass('pause-alert-campaign');
                } else {
                    if (data.MESSAGE)  {
                        alertBox(data.MESSAGE,'Pause Campaign','');
                    } else {
                        alertBox('Pause Campaign Fail','Pause Campaign','');
                    }
                }
            }
        });
    });

})(jQuery);