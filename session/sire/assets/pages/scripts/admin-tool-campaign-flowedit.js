function getParamsPosition (rules, fnName) {
	for (var i = 0; i < rules.length; i++) {
		if (rules[i] == fnName) {
			return ++i;
		}
	}
}

function checkSegmentId_bi (field, rules, i, options) {

	/*console.log(field);
	console.log(rules[2]);
	console.log(i);
	console.log(options);*/

	var i = getParamsPosition(rules, 'checkSegmentId_bi');

	if ($(rules[i]).prop('checked') && field.val() == 0) {
		return options.allrules.required.alertText;
	}

}

function checkSentTo_int (field, rules, i, options) {

	/*console.log(field.val());
	console.log(rules);
	console.log(i);
	console.log(options);*/

	var form = $(field.closest("form, .validationEngineContainer"));
	var fieldName = field.attr("name");
	var fields = $(form.find("input[name='" + fieldName + "']"));
	var i = getParamsPosition(rules, 'checkSentTo_int');
	var messageId_bi = $(rules[i]).val();

	if (messageId_bi != '') {
		messageId_bi = parseInt(messageId_bi);
	}

	if (messageId_bi > 0) {
		for(var f = 0; f < fields.length; f++) {
			if (fields.eq(f).prop('checked')) {
				if ( (parseInt($(rules[i] + messageId_bi).data('messagesnotempty')) & parseInt(fields.eq(f).val())) == 0 ) {
					return fields.eq(f).data('errormessage-custom-error');
				}
			}
		}
	}
}

$.validationEngineLanguage.allRules.checkSentTo_int = {
	"func": function (field, rules, i, options) {
		var form = $(field.closest("form, .validationEngineContainer"));
		var fieldName = field.attr("name");
		var fields = $(form.find("input[name='" + fieldName + "']"));
		var i = getParamsPosition(rules, 'checkSentTo_int');
		var messageId_bi = $(rules[i]).val();

		if (messageId_bi != '') {
			messageId_bi = parseInt(messageId_bi);
		}

		if (messageId_bi > 0) {
			for(var f = 0; f < fields.length; f++) {
				if (fields.eq(f).prop('checked')) {
					if ( (parseInt($(rules[i] + messageId_bi).data('messagesnotempty')) & parseInt(fields.eq(f).val())) == 0 ) {
						return false;
					}
				}
			}
		}
		return true;
	},
	"alertText": "* message is empty"
};

var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

$.validationEngineLanguage.allRules.checkSentTo_int = {
	"func": function (field, rules, i, options) {
		if (field.prop('checked')) {

			var i = getParamsPosition(rules, 'checkSentTo_int');

			var messageId_bi = $(rules[i]).val();

			if (messageId_bi != '') {
				messageId_bi = parseInt(messageId_bi);
			}

			if (messageId_bi > 0) {

				if ( (parseInt($(rules[i] + messageId_bi).data('messagesnotempty')) & parseInt(field.val())) == 0 ) {
					return false;
				}

			}
		}
		return true;
	},
	"alertText": "* message is empty"
};

(function($) {

	$('#AlertCampaignForm input[name="SegmentId_bi_opt"]').click(function(event){
		switch($(this).val()) {
			case 0:
			case '0':
				$('#SegmentId_bi').val(0);
				$('#ReceiverId_int').select2('val', '0');
				break;
			case 1:
			case '1':
				$('#ReceiverId_int').select2('val', '0');
				break;
			case 2:
			case '2':
				$('#SegmentId_bi').val(0);
				break;
		}
	});

	$('#SegmentId_bi').change(function(event) {
		$('#ReceiverId_int').val(0);
		$('#AlertCampaignForm input[name="SegmentId_bi_opt"][value="1"]').prop('checked', true);
	});

	$('#ReceiverId_int').change(function(event) {
		$('#SegmentId_bi').val(0);
		if ($(this).val() > 0) {
			$('#AlertCampaignForm input[name="SegmentId_bi_opt"][value="2"]').prop('checked', true);
		}
	}).select2({
		placeholder: "Search by email",
		allowClear: true,
		ajax: {
			url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
			dataType: 'json',
			delay: 50,
			data: function (params) {
				return {
					inpEmailAddress: params.term, // search term
					page: params.page
				};
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.EMAIL,
							id: item.ID
						}
					})
				};
			},
			cache: true
		},
		minimumInputLength: 3
	});

	$('#AlertCampaignForm input[type="radio"][name="Trigger_bi"]').click(function(event){
		var self = $(this);
		var val = self.val();
		
		if (val == 1) {
			$('#auto-trigger').prop('checked', false);
		} else {
			$('#auto-trigger').prop('checked', true);
		}
		
		if (val < 3) {
			$('#AlertCampaignForm input[type="checkbox"][name="Trigger_bi"]').prop('checked', false);
		}

		if (val == 2) {
			$("#SegmentId_bi_opt0").click();
			$("#SegmentId_bi_opt1,#SegmentId_bi,#SegmentId_bi_opt2,#ReceiverId_int").prop("disabled", true);
		} else {
			$("#SegmentId_bi_opt1,#SegmentId_bi,#SegmentId_bi_opt2,#ReceiverId_int").prop("disabled", false);
		}
	});

	$('#AlertCampaignForm input[type="checkbox"][name="Trigger_bi"]').click(function(event){
		//var self = $(this);
		$('#auto-trigger').prop('checked', true);
		$('#AlertCampaignForm input[name="Trigger_bi"][value="4"]').click();//.prop('checked', true);
	});

	var alertCampaignForm = $('#AlertCampaignForm').submit(function(event){
		event.preventDefault();

		var self = $(this);

		if(self.validationEngine('validate')){
			var saveFn = function(){
				$("#processingPayment").show();
				var jqxhr = $.post("/session/sire/models/cfc/admin-tool.cfc?method=saveCampaign" + strAjaxQuery, self.serialize(), function(data) {
					if (data && data.RXRESULTCODE == 1) {
						bootbox.dialog({
							message: "Save Campaign Successfully",
							title: "Campaigns",
							buttons: {
								success: {
									label: "Ok",
									className: "btn btn-medium btn-success-custom",
									callback: function() {
										location = '/session/sire/pages/admin-tool-campaign-flow';
									}
								}
							}
						});
					} else {
						bootbox.dialog({
							message: "Save Campaign Fail",
							title: "Save Campaign",
							buttons: {
								success: {
									label: "Ok",
									className: "btn btn-medium btn-success-custom",
									callback: function() {
										
									}
								}
							}
						});
					}
				}, 'json')
				.fail(function() {
					$("#processingPayment").hide();
					alertBox('Error. No Response from the remote server. Check your connection and try again.', 'Save Campaign');
				})
				.always(function() {
					$("#processingPayment").hide();
				});
				/*.done(function() {
					alert( "second success" );
				});*/
			};

			if ($('#Trigger_bi1').prop('checked') && $('#Status_int1').prop('checked')) {
				confirmBox('Are you sure?', 'Send now!', saveFn);
			} else {
				saveFn();
			}
		}
	});

	alertCampaignForm.validationEngine('attach', { promptPosition : "topLeft", scroll: false, focusFirstField : false });

	$('#AlertCampaignForm input[type="radio"][name="Trigger_bi"][value="2"]:checked').click();
})(jQuery);