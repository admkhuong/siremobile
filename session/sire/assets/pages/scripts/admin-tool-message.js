(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (message){
        $("#adm-message-list").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a href="" data-id="'+object.ID+'" data-mes-subject="'+object.Subject_vch+'" data-mes-sms="'+object.SMS_Content+'" data-mes-web="'+object.Web_Alert_Content+'" data-mes-mail="'+object.Email_Content+'" class="edit-message-link" data-toggle="modal" data-message-action="edit" data-target="#edit-message"  title="Edit">Edit</a> | <a href="/session/sire/models/cfc/admin-tool.cfc?method=DeleteMessageById&inpMessageID='+object.ID+'" class="delete-message-link">Delete</a>';
            return strReturn;
        }

        var table = $('#adm-message-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "Subject_vch", "sName": 'Message Subject', "sTitle": 'Message Subject', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "Created_dt", "sName": 'Created', "sTitle": 'Created date', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetListMessage'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpMessage", "value": message}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "aaSorting": [[1,'desc']]
        });
    }
    function SaveMessage(inpMessageID)
    {        
        if ($('#form-edit-message').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var inpSubject=$("#message-name").val();
            var inpSMSAlert=$("#message-message").val();
            var inpWebAlert=$("#message-web").val();
            var inpMailAlert=$("#message-mail").val();

            if ($.trim(inpSMSAlert) == '' && $.trim(inpWebAlert) == '' && $.trim(inpMailAlert) == '') {
                $('#amw').show();
            } else {
                $('#amw').hide();

                $.ajax({
                    url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertUpdateMessage'+strAjaxQuery,
                    async: true,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        inpMessageID:inpMessageID,
                        inpSubject: inpSubject,
                        inpSMSAlert: inpSMSAlert,
                        inpWebAlert: inpWebAlert,
                        inpMailAlert: inpMailAlert
                    },
                    beforeSend: function(){
                        $('#processingPayment').show();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('#processingPayment').hide();
                        if(inpMessageID ==0)
                        {alertBox('Define Message Fail','Define Message Fail',''); }
                        else
                        {alertBox('Update Message Fail','Update Message Fail',''); }

                    },
                    success: function(data) {
                        $('#processingPayment').hide();
                        if (data.RXRESULTCODE == 1) {
                            $("#edit-message").modal('hide');
                            $(".modal-backdrop").fadeOut();
                            fnGetDataList(); 
                        } else {
                            /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                            alertBox(data.MESSAGE,'Define Message Fail','');
                        }                       
                    }
                });
            }
        }
        else
        {                           
            return false;
        }
    }
    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();        
        $( "#searchbox" ).on( "keypress", function(e) { 
            if (e.keyCode == 13)
            {
                fnGetDataList($( this ).val()); 
            }            
        });            
        $("#form-edit-message").on( "submit", function(e) {   
            e.preventDefault();          
            var messageId= $("#messageId").val();
            SaveMessage(messageId); 
            //return false;
        });  
        var table = $('#adm-message-list').DataTable();     
        $('#adm-message-list tbody').on('click', '.edit-message-link', function (e) {      
            //e.preventDefault();        
            $("#modal-tit").text("EDIT MESSAGE");
        });        
        //
        $('#edit-message').on('show.bs.modal', function (event) {            
            var button = $(event.relatedTarget) // Button that triggered the modal
            var modal = $(this)                        

            modal.find("#messageId").val(button.data('id'));    
            modal.find("#message-name").val(button.data('mes-subject'));            
            modal.find("#message-message").val(button.data('mes-sms'));
            modal.find("#message-web").val(button.data('mes-web'));
            modal.find("#message-mail").val(button.data('mes-mail'));
        })
        
        $("#adm-message-list").on("click", ".delete-message-link", function(event){
            event.preventDefault();
            var delUrl = $(this).attr('href');
            confirmBox('Are you sure you want to delete this message?', 'Delete a message', function(){
                $.ajax({
                    url: delUrl+strAjaxQuery,
                    dataType: "json",
                    success: function(data){
                        if(data && data.MESSAGE && data.MESSAGE != '') {
                            alertBox(data.MESSAGE, 'Delete a message');
                        }

                        if(data && data.RXRESULTCODE && data.RXRESULTCODE == 1) {
                            $("#adm-message-list_paginate .paginate_text").trigger(jQuery.Event('keyup', { keycode: 13 }));
                        }
                    }
                });
            });
        });

    });    

})();