(function($) {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    var segmentTable;

    /*Keyword list datatable*/
    fnGetDataList = function (segment){            
        $("#adm-segment-list").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a href="/session/sire/pages/admin-tool-segment?action=edit&segmentId='+object.ID+'" data-id="'+object.ID+'" class="edit-segment-link" title="Edit">Edit</a> | <a href="/session/sire/models/cfc/admin-tool.cfc?method=DeleteSegmentById&inpSegmentID='+object.ID+'" class="delete-segment-link">Delete</a>';
            return strReturn;
        }

        var table = $('#adm-segment-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "SegmentName", "sName": 'List Name', "sTitle": 'List Name', "bSortable": true, "sClass": "", "sWidth": "300"},
                {"mData": "Desc", "sName": 'Description', "sTitle": 'Description', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": "ListCount", "sName": 'List Count', "sTitle": 'List Count', "bSortable": true, "sClass": "", "sWidth": "150px"},  
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetListSegment'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpSegment", "value": segment}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            }            
        });

        segmentTable = table;
    }

    function GetListUserIdCheckedOnForm()
    {
        var stringReturn="";
        //$(".chkItem").each(function(){
        //    if($(this).is(":checked"))
        //    {
        //        stringReturn.push($(this).attr("data-id"));
        //    }
        //});
        stringReturn=$("#list-user-selected").val();
        return stringReturn.toString();
    }

    function SaveSegment(inpSegmentID)
    {        
        if ($('#form-edit-segment').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var inpSegmentName=$("#seg-list-name").val();
            var inpSegmentDesc=$("#seg-desc").val();  
            var inpListUserID=GetListUserIdCheckedOnForm();                                               
            //return false;
            $.ajax({                    
                url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertUpdateSegment'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpListUserID:inpListUserID, 
                    inpSegmentID:inpSegmentID,
                    inpSegmentName:inpSegmentName,
                    inpSegmentDesc: inpSegmentDesc                                    
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    if(inpSegmentID ==0)
                    {alertBox('Define Segment Fail','Define Segment Fail',''); }
                    else
                    {alertBox('Update Segment Fail','Update Segment Fail',''); }

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        location.href="/session/sire/pages/admin-tool-segment";
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Define Message Fail','');
                    }                       
                }
            });  
        }
        else
        {                                
            return false;
        }
    }

    //get list for user
    fnGetUserList = function (segment,strFilter){
        $("#adm-user-list").html('');    
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        var actionChk = function (object) {
            var strReturn = $('<input type="checkbox" class="chkItem" data-id="'+object.ID+'"/>')[0].outerHTML;
            return strReturn;
        }
        var status = function (object) {
            var strReturn = "Deactivated";
            if(object.STATUS==1){var strReturn = "Active"};
            return strReturn;
        }

        var table = $('#adm-user-list').dataTable({
            "bStateSave": false,            
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
            
                {"mData": actionChk, "sName": '', "sTitle": '<input type="checkbox" class="checkAll"/>', "bSortable": false, "sClass": "", "sWidth": "20"},
                {"mData": "ID", "sName": 'ID', "sTitle": 'ID', "bSortable": true, "sClass": "", "sWidth": "100"},
                {"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "300"},
                {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "bSortable": true, "sClass": "", "sWidth": "200"},                
                {"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "bSortable": true, "sClass": "", "sWidth": "150px"},  
                {"mData": status, "sName": 'Status', "sTitle": 'Status', "bSortable": true, "sClass": "", "sWidth": "100px"}
            ],
            "sAjaxDataProp": "UserList",            
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetUserList'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpSegment", "value": segment},
                    { "name": "customFilter", "value": customFilterData},
                    { "name": "inpListUserBySegment", "value": $("#list-user-selected").val()}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);  
                        var curSegmentId= $("#segmentId").val();   
                        if(curSegmentId != undefined && curSegmentId >0)
                        {            
                            GetSegmentListInfo(curSegmentId); 
                        }                                                                      
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            }
        });
    }

    function GetSegmentListInfo(segmentId)
    {        
        if(segmentId >0)
        {
            $.ajax({                    
                url: '/session/sire/models/cfc/admin-tool.cfc?method=GetSegmentListInfo'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpSegmentID:segmentId                                
                },
                beforeSend: function(){
                    
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Load Segment List Info Fail','Load Segment List Info Fail','');

                },
                success: function(data) {
                    //                     
                    $("#seg-list-name").val(data.SegmentName);
                    $("#seg-desc").val(data.SegmentDesc);
                     
                    // only rewrite at first load  
                    if($("#list-user-selected").val()!=="")
                    {
                        var listUserOfSegment=$("#list-user-selected").val().split(","); 
                    } 
                    else
                    {
                        var listUserOfSegment=data.ListUserBySegment.split(","); 
                    }
                    $("#list-user-selected").val(listUserOfSegment);                     
                    for (var i = 0; i < listUserOfSegment.length; i++) {                                            
                        $('.chkItem[data-id="'+listUserOfSegment[i]+'"]').prop("checked",true);
                    }
    
                    var chkItem = $('#adm-user-list .chkItem');
                    if (chkItem.length != 0 && chkItem.length == chkItem.filter(':checked').length) {
                        $('#adm-user-list .checkAll').prop("checked", true);
                    } else {
                        $('#adm-user-list .checkAll').prop("checked", false);
                    }
                }
            }); 
        }
    }

    /* Init datatable */
    $(document).ready(function() {   
        var segmentId= $("#segmentId").val();        
        // segment    
        if ($("#adm-segment-list").length ) {
            fnGetDataList();        
        }
        if ($("#adm-user-list").length ) {
            fnGetUserList();        
        }
        
        $( "#searchbox" ).on( "keypress", function(e) { 
            if (e.keyCode == 13)
            {
                fnGetDataList($( this ).val()); 
            }            
        });     
        $( "#searchbox-user" ).on( "keypress", function(e) { 
            if (e.keyCode == 13)
            {
                fnGetUserList(); 
            }            
        });         
        $("#form-edit-segment").on( "submit", function(e) {   

            e.preventDefault();               
            SaveSegment(segmentId);             
        });  
        // for usser list        
        $('#adm-user-list').on('click', '.checkAll', function () {            
            $("#adm-user-list tbody tr").find(".chkItem").prop("checked",this.checked);
            var listReturn="";
            var listChecked=[];
            $(".chkItem").each(function(){
                if($(this).is(":checked"))
                {
                    listChecked.push($(this).attr("data-id"));
                }
            });
            if(listChecked.toString()=="")
            {
                listReturn="";
            }
            else
            {
                var currentList=$("#list-user-selected").val().split(",");
                for (var i =0 ; i< listChecked.length; i++) {
                    var index=currentList.indexOf(listChecked[i]);     
                    if(index < 0)
                    {
                        currentList.push(listChecked[i]);                    
                    }
                }  
                listReturn=currentList.toString();              
            }
            $("#list-user-selected").val(listReturn);
        });       
               
        // for check each item
        var admUserListDatatable= $("#adm-user-list").dataTable();
        $(admUserListDatatable).on( "click",".chkItem", function(e) {   
            var id= $(this).data("id").toString();
            var listID= $("#list-user-selected").val().split(',');                                

            var index=listID.indexOf(id);           
            console.log(index);
            if(this.checked)
            {                
                if(index < 0)
                {
                    listID.push(id);                    
                }
            }
            else
            {
                 listID.splice(index, 1);
            }
            $("#list-user-selected").val(listID.toString()); 

            var chkItem = $('#adm-user-list .chkItem');
            if (chkItem.length != 0 && chkItem.length == chkItem.filter(':checked').length) {
                $('#adm-user-list .checkAll').prop("checked", true);
            } else {
                $('#adm-user-list .checkAll').prop("checked", false);
            }
        });  
        //  
        
        // filter
        $('#box-filter').sireFilterBar({
          fields: [
           {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' EmailAddress_vch '},
           {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' MFAContactString_vch '},
           {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' tmpTable.UserId_int '}
          ],
          clearButton: true,
          // rowLimited: 5,
          error: function(ele){
           // console.log(ele);
          },
          // limited: function(msg){
          //  alertBox(msg);
          // },
          clearCallback: function(){
           fnGetUserList();
          }
         }, function(filterData){
            //called if filter valid and click to apply button
            fnGetUserList("",filterData);
         });                

        $("#adm-segment-list").on("click", ".delete-segment-link", function(event){
            event.preventDefault();
            var delUrl = $(this).attr('href');
            confirmBox('Are you sure you want to delete this segment?', 'Delete a segment', function(){
                $.ajax({
                    url: delUrl+strAjaxQuery,
                    dataType: "json",
                    success: function(data){
                        if(data && data.MESSAGE && data.MESSAGE != '') {
                            alertBox(data.MESSAGE, 'Delete a segment');
                        }

                        if(data && data.RXRESULTCODE && data.RXRESULTCODE == 1) {
                            $("#adm-segment-list_paginate .paginate_text").trigger(jQuery.Event('keyup', { keycode: 13 }));
                        }
                    }
                });
            });
        });
    });    

})(jQuery);