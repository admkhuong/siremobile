(function() {
        //Define FileRecord statuses 
        const STARTLOADING = 2, PENDING = -1, DELETED = -2, JUSTCREATE = 0,  COMPLETED = 3, LOADING = 1, STOPPED = 4,OUTSTANDING=-4;       
        //Define contact status
        const CT_DUPLICATE = 10, CT_INVALID = 11, CT_VALID = 3, CT_DELETED = -2; CT_DNC=13;

        /*Ajax query string default param*/
        var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

        fnGetImportIndividualSumary=function(customFilterData)
        {
            customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   
            $("#individual-list ").html('');  
            var statusCol = function(object){ 
                var status = "";
                if (object.STATUS ==1) {            
                    status = "Ready" ;  
                } 
                else if (object.STATUS ==2){
                    status = "Processing" ;                  
                }
                else if (object.STATUS ==3){
                    status = "Completed" ;            
                }
                else if (object.STATUS ==4){                    
                    status = "Rejected" ;                   
                }
                else {
                    status ="<a class='admin-action-indi' data-action='approval' data-id='"+object.ID+"'>Approval</a> | <a class='admin-action-indi' data-action='reject' data-id='"+object.ID+"'>Reject</a> ";                                        
                }
                return status;
            };
            var colTotal=function(object){
                return "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='0' title='click to view data'>"+object.TOTALCONTACT+"</a>" ;                   
            };
            var colImported=function(object){
                return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='1' title='click to view data'>"+object.IMPORTED+"</a>" ;                   
            };
            var colFailed=function(object){
                return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='2' title='click to view data'>"+object.FAILED+"</a>" ;                   
            };
            var colDNC=function(object){
                return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='3' title='click to view data'>"+object.DNC+"</a>" ;                   
            };
            var table = $('#individual-list').dataTable({
                "bStateSave": false,                
                "iStateDuration": -1,
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "SUBSCRIBERLIST", "sName": 'Subscriber List', "sTitle": 'Subscriber List', "bSortable": true, "sClass": "", "sWidth": "200"},                   
                    {"mData": "IMPORTDATE", "sName": 'Create Date', "sTitle": 'Create Date', "bSortable": true, "sClass": "", "sWidth": "200"},   
                    {"mData": colTotal, "sName": 'Total Contact', "sTitle": 'Total Contact', "bSortable": true, "sClass": "", "sWidth": "140"},     
                    {"mData": colImported, "sName": 'Imported', "sTitle": 'Imported', "bSortable": false, "sClass": "", "sWidth": "80"}, 
                    {"mData": colFailed, "sName": 'Failed', "sTitle": 'Failed', "bSortable": false, "sClass": "", "sWidth": "80"}, 
                    {"mData": colDNC, "sName": 'DNC', "sTitle": 'DNC', "bSortable": false, "sClass": "", "sWidth": "80"},     
                    {"mData": "NAMEADMINACTIONBY", "sName": 'Admin Action By', "sTitle": 'Admin Action By', "bSortable": true, "sClass": "", "sWidth": "180"}, 
                    {"mData": "ADMINACTIONDATE", "sName": 'Admin Action Date', "sTitle": 'Admin Action Date', "bSortable": true, "sClass": "", "sWidth": "200"},         
                    {"mData": statusCol, "sName": 'Action', "sTitle": 'Action', "bSortable": true, "sClass": "", "sWidth": "160"}                
                ],
                "sAjaxDataProp": "aaData",
                "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=IndividualImprtSummary'+strAjaxQuery, 
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                    
                    aoData.push(
                        {                            
                            "name": "inpIsGetAll", "value": 1                                                                                   
                        }
                    );
                    aoData.push(
                        { 
                            "name": "customFilter", "value": customFilterData                                                                                   
                        }
                    );   

                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {
                            fnCallback(data);
                        }
                    });
                } ,              
                "aaSorting": [[1,'desc']]
            });  
        }
        fnGetDataList = function (customFilterData){
            customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   
            $("#upload-list").html('');    
    
            var actionBtn = function (object) {
                var strReturn = '';    
                if(object.STATUS==0)
                {
                    strReturn="<a class='admin-action' data-action='approval' data-id='"+object.ID+"'>Approval</a> | <a class='admin-action' data-action='reject' data-id='"+object.ID+"'>Reject</a> | <a class='admin-action' data-action='view' data-id='"+object.ID+"' title='click to view data'>View</a>";
                }            
                else{
                    if (object.STATUS in uploadStatus) {
                        if ((uploadStatus[object.STATUS] == "Completed" || uploadStatus[object.STATUS] == "Waiting") && object.SENDTOQUEUE == "0") {
                            strReturn= "<a class='admin-action' data-action='view' data-id='"+object.ID+"' title='click to view data'>Send Partial</a>" ;
                        }
                        strReturn= "<a class='admin-action' data-action='view' data-id='"+object.ID+"' title='click to view data'>"+uploadStatus[object.STATUS]+"</a>" ;
                        
                        if(object.STATUS==-3 && object.UNIQUE > 0)
                        {
                            strReturn = "<a class='admin-action' data-action='view' data-id='"+object.ID+"' title='click to view data'>Completed</a>" ;
                        }
                    } else {
                        strReturn = "<a class='admin-action' data-action='view' data-id='"+object.ID+"' title='click to view data'>Loading</a>" ;
                    }
                }
                return strReturn;
            }
           
            var table = $('#upload-list').dataTable({
                "bStateSave": false,
                "iStateDuration": -1,                
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "USERID", "sName": 'UserID', "sTitle": 'UserID', "bSortable": true, "sClass": "", "sWidth": "100"},
                    {"mData": "FILENAME", "sName": 'File Name', "sTitle": 'File Name', "bSortable": true, "sClass": "", "sWidth": "300"},      
                    {"mData": "CREATEDAT", "sName": 'Create Date', "sTitle": 'Create Date', "bSortable": true, "sClass": "", "sWidth": "200"},    
                    {"mData": "NAMEADMINACTIONBY", "sName": 'Admin Action By', "sTitle": 'Admin Action By', "bSortable": false, "sClass": "", "sWidth": "200"},    
                    {"mData": "ADMINACTIONAT", "sName": 'Admin Action Date', "sTitle": 'Admin Action Date', "bSortable": false, "sClass": "", "sWidth": "200"},                        
                                              
                    {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "", "sWidth": "200px"}
                ],
                "sAjaxDataProp": "aaData",
                "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=GetAllUploadList'+strAjaxQuery,
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                    
                    aoData.push(
                        { 
                            "name": "customFilter", "value": customFilterData                                                                                   
                        }
                    );                   
    
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {
                            fnCallback(data);
                        }
                    });
                },             
                "aaSorting": [[2,'desc']]
            });
        }
        function AdminActionForIndividual(uploadId,actionId)
        {
            //actionId : 10 approval, 20 reject
            $.ajax({
                url: '/session/sire/models/cfc/user-import-contact.cfc?method=UpdateAdminIndividualAction'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpUploadId:uploadId,
                    inpActionId: actionId
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('Action Fail','OOPS!','');

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        //
                        fnGetImportIndividualSumary(); 
                    } else {                        
                        alertBox(data.MESSAGE,'OOPS!','');
                    }                       
                }
            });           
        }
        function ViewIndividualImportData(id,type)
        {
            //aa
            $("#tbl-indi-view-data").html('');  
            
            var table = $('#tbl-indi-view-data').dataTable({
                "bStateSave": false,                
                "iStateDuration": -1,
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "PHONENUMBER", "sName": 'Contact Number', "sTitle": 'Contact Number', "bSortable": false, "sClass": "", "sWidth": "300"}                    
                ],
                "sAjaxDataProp": "aaData",
                "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=GetIndiDataByUploadId'+strAjaxQuery, 
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                    
                    aoData.push(
                        {                            
                            "name": "inpUploadId", "value": id                                                                               
                        }
                    );
                    aoData.push(
                        { "name": "inpType", "value": type}
                    );
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {
                            fnCallback(data);
                        }
                    });
                }                
            });  
            $("#md-indi-view-data").modal('show');
        }
        function AdminAction(uploadId,actionId)
        {
            //actionId : 10 approval, 20 reject
            $.ajax({
                url: '/session/sire/models/cfc/user-import-contact.cfc?method=UpdateAdminAction'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpUploadId:uploadId,
                    inpActionId: actionId
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('Action Fail','OOPS!','');

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        //
                        fnGetDataList(); 
                    } else {                        
                        alertBox(data.MESSAGE,'OOPS!','');
                    }                       
                }
            });           
        }
        
        function ViewImportData(uploadId)
        {
            
            //tbl-import-view-data
            $("#tbl-import-view-data").html('');    
            var table = $('#tbl-import-view-data').dataTable({
                "bStateSave": false,                
                "iStateDuration": -1,
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "PHONENUMBER", "sName": 'Contact Number', "sTitle": 'Contact Number', "bSortable": true, "sClass": "", "sWidth": "300"}                   
                ],
                "sAjaxDataProp": "aaData",
                "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=GetDataByUploadId'+strAjaxQuery,
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                    
                    aoData.push(
                        {
                             "name": "inpUploadId", "value": uploadId                                                     
                        }
                    );
    
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {
                            fnCallback(data);
                        }
                    });
                }                
            });
            //
            $("#md-import-view-data").modal('show');
        }
        /* Init datatable */
        $(document).ready(function() {            
            fnGetDataList();         
            fnGetImportIndividualSumary();          
            //Filter bar initialize
            $('#box-filter').sireFilterBar({
                fields: [
                    {DISPLAY_NAME: 'User Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' uimport.EmailAddress_vch '},
                    {DISPLAY_NAME: 'User ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
                    {DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' u.Created_dt '},
                    
                ],
                clearButton: true,
                // rowLimited: 5,
                error: function(ele){
                    
                },
                // limited: function(msg){
                // 	alertBox(msg);
                // },
                clearCallback: function(){
                    fnGetDataList();
                }
            }, function(filterData){
                //called if filter valid and click to apply button
                fnGetDataList(filterData);
            });
            $('#box-filter1').sireFilterBar({
                fields: [
                    {DISPLAY_NAME: 'User Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' uimport.EmailAddress_vch '},
                    {DISPLAY_NAME: 'User ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' data.UserId_int '},
                    {DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' data.ImportDate_dt '},
                    
                ],
                clearButton: true,
                // rowLimited: 5,
                error: function(ele){
                    
                },
                // limited: function(msg){
                // 	alertBox(msg);
                // },
                clearCallback: function(){
                    fnGetImportIndividualSumary();
                }
            }, function(filterData){
                //called if filter valid and click to apply button
                fnGetImportIndividualSumary(filterData);
            });
            //admin-action
            $(document).on('click','.admin-action',function(){
                var messconfirm="";
                var action=$(this).data('action');
                var id=$(this).data('id');
                if(action.length)
                {
                    var actionId=0;
                    if(action=='approval')
                    {
                        actionId=10;
                        messconfirm="Are you sure you want to approve this upload?";
                    }
                    else if(action=='reject')
                    {
                        actionId=20;
                        messconfirm="Are you sure you want to reject this upload?";
                    }                    
                    if(actionId == 10 || actionId==20)
                    {
                        bootbox.dialog({
                            message: '<h4 class="be-modal-title">Confirm!</h4><p>'+messconfirm+'</p>',
                            title: '&nbsp;',
                            className: "be-modal",
                            buttons: {
                                success: {
                                    label: "Yes",
                                    className: "btn btn-medium green-gd",
                                    callback: function(result) {
                                       AdminAction(id,actionId);
                                    }
                                },
                                cancel: {
                                    label: "NO",
                                    className: "green-cancel"
                                },
                            }
                        });
                    } 
                    else // view action
                    {
                        ViewImportData(id);
                    }                   
                }
            });
            $(document).on('click','.admin-action-indi',function(){
                var messconfirm="";
                var action=$(this).data('action');
                var id=$(this).data('id');
                var type=$(this).data('type');
                if(action.length)
                {
                    var actionId=0;
                    if(action=='approval')
                    {
                        actionId=10;
                        messconfirm="Are you sure you want to approve this upload?";
                    }
                    else if(action=='reject')
                    {
                        actionId=20;
                        messconfirm="Are you sure you want to reject this upload?";
                    }                    
                    if(actionId == 10 || actionId==20)
                    {
                        bootbox.dialog({
                            message: '<h4 class="be-modal-title">Confirm!</h4><p>'+messconfirm+'</p>',
                            title: '&nbsp;',
                            className: "be-modal",
                            buttons: {
                                success: {
                                    label: "Yes",
                                    className: "btn btn-medium green-gd",
                                    callback: function(result) {
                                        AdminActionForIndividual(id,actionId);
                                    }
                                },
                                cancel: {
                                    label: "NO",
                                    className: "green-cancel"
                                },
                            }
                        });
                    } 
                    else // view action
                    {
                        ViewIndividualImportData(id,type);
                    }                   
                }
            });
            //                
            var table = $('#upload-list').dataTable();				
            var table1 = $('#individual-list').dataTable();
            setInterval(function(){                 
                table.fnDraw(false);                    
                table1.fnDraw(false);  
            }, 10000);	
        });    
    
    })();