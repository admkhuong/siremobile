(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (phone){
        $("#adm-phone-white-list").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a id='+object.ID+' class="delete-phone-link">Delete</a>';
            return strReturn;
        }

        var table = $('#adm-phone-white-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "PhoneNumber_vch", "sName": 'White Phone Number', "sTitle": 'Phone Number', "bSortable": true, "sClass": "", "sWidth": ""},
                {"mData": "DateUpdate_dt", "sName": 'Update', "sTitle": 'Update date', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=GetListWhitePhone'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpPhone", "value": phone}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                     fnCallback(data);
                    }
                });
            },
            "aaSorting": [[1,'desc']]
        });
    }

    function SavePhoneNumber(inpMessageID)
    {        
        if ($('#form-new-wphone-number').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var Phonenumber = $('#form-new-wphone-number').find('#user_phone').val();
            $.ajax({
                url: '/session/sire/models/cfc/admin-tool.cfc?method=InsertWPhoneNumber'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpphone:Phonenumber
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Add New Phone Number Fail','Add New Phone Number','');
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        $("#form-new-wphone-number").modal('hide');
                        setTimeout(function(){
                            alertBox("Add new phone number successfully!.",'Add New Phone Number','');
                        },10);
                        fnGetDataList(); 
                        $('#form-new-wphone-number').find('#user_phone').val('');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Add New Phone Number','');
                    }                       
                }
            });
        }
        
    }
    /* Init datatable */
    $(document).ready(function() {

        $("#user_phone").mask("(000)000-0000");

        $("#user_phone").on('input', function(event){
        var reg = /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        var emailstr = $('#user_phone').val();
        if (reg.test(emailstr) == false) 
        {
            $('#invalidphone').show();
            statusphone = false;
            return false;
        }else{
            $('#invalidphone').hide();
            statusphone = true;
            return true;
        }
        
        
        });

        fnGetDataList();        
        $( "#searchbox" ).on( "input", function(e) { 
                fnGetDataList($( this ).val());            
        });            
        $("#form-new-wphone-number").on( "submit", function(e) {   
            e.preventDefault();         
            SavePhoneNumber(); 
            //return false;
        });  
        var table = $('#adm-phone-white-list').DataTable();     
        
        $("#adm-phone-white-list").on("click", ".delete-phone-link", function(event){
            event.preventDefault();
            var PhoneID = $(this).attr('id');
            confirmBox('Are you sure you want to delete this phone number?', 'Delete a phone number', function(){
                $.ajax({
                    url: '/session/sire/models/cfc/admin-tool.cfc?method=DeleteWPhoneNumberById'+strAjaxQuery,
                    async: true,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        inpPhoneID:PhoneID
                    },
                    beforeSend: function(){
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alertBox('Delete Phone Number Fail','Delete Phone Number Fail',''); 
                    },
                    success: function(data) {
                        if (data.RXRESULTCODE == 1) {
                            fnGetDataList(); 
                        } else {
                            /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                            alertBox(data.MESSAGE,'Delete Phone Number Fail','');
                        }                       
                    }
                });
            });
        });

    });    

})();