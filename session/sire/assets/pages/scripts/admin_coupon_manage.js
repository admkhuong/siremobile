(function() {
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var start = '';
    var end = '';
    var name = '';
    var isSearch = '';

    $("#promo-startdate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    $("#promo-enddate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    function GetPromotionList(code, start, end, isSearch){

    	var actionCol = function(object){
    		var strReturn = $('<a class="" title="Edit Promotion"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>')
								.attr('data-id', object.ID)
								.attr('href', 'admin-new-coupon?edit-coupon-id=' + object.ID)
								.attr('data-originid', object.ORIGINID)[0].outerHTML;
			return strReturn;
    	}

    	var couponCodeCol = function(object){
    		var strReturn = $('<a class="coupon-code-link" title="' + object.CODE + '">' + object.CODE + '</a>').attr('href', 'admin-new-coupon?coupon-details-id=' + object.ID)[0].outerHTML;
    		return strReturn;
    	}

    	var statusCol = function(object){
    		var strReturn = $('<h5 class="coupon-'+object.STATUS+'">' + object.STATUS + '</h5>')[0].outerHTML;
    		return strReturn;
    	}

    	var redeemCol = function(object){
    		var strReturn = $('<a class="coupon-code-link">' + object.REDEEMED + '</a>').attr('href', 'admin-coupon-report?couponid=' + object.ID)[0].outerHTML;
    		return strReturn;
    	}

    	var table = $('#coupon-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": couponCodeCol, "sName": '', "sTitle": 'Code',"bSortable": false,"sClass":"", "sWidth":"200px"},
				{"mData": "NAME", "sName": '', "sTitle": 'Name',"bSortable": false,"sClass":"", "sWidth":"150px"},
				// {"mData": "STARTDATE", "sName": '', "sTitle": "Activation Date","bSortable": false,"sClass":"", "sWidth":"115px"},
				// {"mData": "ENDDATE", "sName": '', "sTitle": "Expiration Date","bSortable": false,"sClass":"", "sWidth":"115px"},
				// {"mData": "DISCOUNTTYPE", "sName": '', "sTitle": "Discount Type","bSortable": false,"sClass":"", "sWidth":"120px"},
				// {"mData": "VALUETODISCOUNT", "sName": '', "sTitle": "Discount Value","bSortable": false,"sClass":"", "sWidth":"120px"},
				// {"mData": "RECURRING", "sName": '', "sTitle": "Recurring","bSortable": false,"sClass":"", "sWidth":"120px"},
				{"mData": "CREATED", "sName": '', "sTitle": "Created At","bSortable": false,"sClass":"", "sWidth":"100px"},
				{"mData": redeemCol, "sName": '', "sTitle": "Redeemed","bSortable": false,"sClass":"", "sWidth":"100px"},
				{"mData": statusCol, "sName": '', "sTitle": "Status","bSortable": false,"sClass":"", "sWidth":"100px"},
				// {"mData": detailsCol, "sName": '', "sTitle": "Actions","bSortable": false,"sClass":"coupon-manage-action", "sWidth":"80px"},
				// {"mData": actionCol, "sName": '', "sTitle": "Actions","bSortable": false,"sClass":"", "sWidth":"80px"}
			],
			"sAjaxDataProp": "LISTPROMOTION",
			"sAjaxSource": '/session/sire/models/cfc/promotion.cfc?method=GetPromoCodeList' + strAjaxQuery,

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "inpCouponCode", "value": code}
	            );

	            aoData.push(
		            { "name": "inpStartDate", "value": start}
	            );

	            aoData.push(
		            { "name": "inpEndDate", "value": end}
	            );

	            aoData.push(
		            { "name": "isSearchFeature", "value": isSearch}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.RXRESULTCODE != 1 && data.RXRESULTCODE != 0){
			        		alertBox(data.MESSAGE, 'SEARCH COUPON', function(){});
			        	}else{
			        		fnCallback(data);
			        	}
			        	
			        }
		      	});
	        }
		});
    }

    GetPromotionList(name, start, end, isSearch);

    $('#search-coupon-form').on('submit', function(event){
    	event.preventDefault();
    	inpcode = $('#promo-code').val();
    	inpstart = $('#promo-startdate').val();
    	inpend = $('#promo-enddate').val();
    	// if(inpname == '' && inpstart == '' && inpend == ''){
    	// 	alertBox('Please input data for filtering, at least one field is required!', 'SEARCH COUPON', function(){});
    	// 	return true;
    	// }

    	if(inpstart != '' && inpend != ''){
    		if(new Date(inpstart) > new Date(inpend)){
	    		alertBox('Start Date must be before End Date!', 'SEARCH COUPON');
	    		return true;
	    	}
    	}
    	
    	GetPromotionList(inpcode, inpstart, inpend, 1);
    });

	$('#promo-code').keypress(function( e ) {
	    if(e.which === 32) 
	        return false;
	});
})();