(function() {
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	$("#promo-startdate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    $("#promo-enddate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    var startTime = '';
	var endTime = '';  

	function GetPromotionList(couponId, startDate, endDate){

		var actionCol = function(object){
			var strReturn = $('<a class="" title="Edit Promotion"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>')
								.attr('data-id', object.ID)
								.attr('href', 'admin-new-coupon?edit-coupon-id=' + object.ID)
								.attr('data-originid', object.ORIGINID)[0].outerHTML;
			return strReturn;
		}

		var couponCodeCol = function(object){
			var strReturn = $('<a class="coupon-code-link" title="' + object.CODE + '">' + object.CODE + '</a>').attr('href', 'admin-new-coupon?coupon-details-id=' + object.ID)[0].outerHTML;
			return strReturn;
		}

		var statusCol = function(object){
			var strReturn = $('<h5 class="coupon-'+object.STATUS+'">' + object.STATUS + '</h5>')[0].outerHTML;
			return strReturn;
		}

		var table = $('#report-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
			"aaSorting": [[ 3, "desc" ]],
		    "aoColumns": [
				{"mData": "NAME", "sName": '', "sTitle": 'Customer\'s Name',"bSortable": true,"sClass":"", "sWidth":"120px"},
				{"mData": "PHONE", "sName": '', "sTitle": "Phone number","bSortable": false,"sClass":"", "sWidth":"130px"},
				{"mData": "EMAIL", "sName": '', "sTitle": "Email","bSortable": false,"sClass":"", "sWidth":"160px"},
				{"mData": "REDEEMED", "sName": '', "sTitle": "Redeemed","bSortable": false,"sClass":"", "sWidth":"100px"},
				{"mData": "TYPE", "sName": '', "sTitle": "Type","bSortable": true,"sClass":"", "sWidth":"100px"},
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/promotion.cfc?method=GetCouponReportById' + strAjaxQuery,

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "inpCouponId", "value": couponId}
	            );

	            aoData.push(
		            { "name": "inpStartDate", "value": startDate}
	            );

	            aoData.push(
		            { "name": "inpEndDate", "value": endDate}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.RXRESULTCODE != 1 && data.RXRESULTCODE != 0){
			        		alertBox(data.MESSAGE, 'COUPON REPORT', function(){});
			        	}else{
			        		fnCallback(data);
			        		$('#total-signup').text('Sign up: ' + data.SIGNUPCOUNT);
			        		$('#total-upgrade').text('Upgrade: ' + data.UPGRADECOUNT);
			        		$('#total-recurring').text('Recurring: ' + data.RECURRINGCOUNT);
			        	}
			        	
			        }
		      	});
	        }
		});
	}

	var couponId = $('#couponid').val();
	var couponCode = $('#couponcode').val();

	GetPromotionList(couponId);

	$('#search-coupon-form').on('submit', function(event){
    	event.preventDefault();
    	inpstart = $('#promo-startdate').val();
    	inpend = $('#promo-enddate').val();

    	if(inpstart != '' && inpend != ''){
    		if(new Date(inpstart) > new Date(inpend)){
	    		alertBox('Start Date must be before End Date!', 'REPORT FILTER');
	    		return true;
	    	}
    	}
    	
    	GetPromotionList(couponId, inpstart, inpend);
    	startTime = $('#promo-startdate').val();
		endTime = $('#promo-enddate').val();
    });

    $('body').on("click", "button.download-coupon-report", function(event){
    	
        window.location.href="/session/sire/models/cfm/admin/admin-coupon-report-list.cfm?couponid=" + couponId + "&startdate=" + startTime + "&enddate=" + endTime + "&couponcode=" + couponCode;
    });
})();