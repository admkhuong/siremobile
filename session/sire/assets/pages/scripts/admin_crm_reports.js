(function(w,d,s,g,js,fs){
    g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
    js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
    js.src='https://apis.google.com/js/platform.js';
    fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));

(function() {

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    $(".phone-style").mask("(000)000-0000");

    var filter = '';

    $(window).on('resize load', function(e){

        $('#crm-message-sent, #crm-message-received, #crm-opt-in, #crm-opt-out, #plan-free-div, #plan-individual-div, #plan-pro-div, #plan-smb-div, #signin-unique-div, #user-active-div, #total-user-div, #new-user-div, #signin-inactive-div, #support-keyword-div, #user-inactive-div, #upgrade-div, #downgrade-div, #total-enterprise-user, #total-enterprise-revenue, #user-paid-div').textfill({
            maxFontPixels: 64,
            minFontPixels: 4
        });

    });

    var dateStart_crm = '';
    var dateEnd_crm = '';
    var start = moment();
    var end = moment();
    var rfresh = 0;
    var startTime = '';
    var endTime = '';
    var planName = '';
    var method = '';
    var exportPath = '';
    var planAction = '';
    var planField = '';
    var exp_user_type=0;
    var exp_user_source ='';
    var exp_user_isaffiliatesource=0;


    $('.reportrange').daterangepicker({
        "dateLimit": {
            "years": 1
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, GetReport);

    var enterpriseRevenue = 0;
    var enterpriseRevenue1 = 0;
    var dotComRevenue = 0;
    var dotComRevenueCredit = 0;
    var dotComRevenueTotal = 0;
    var mtCount = 0;
    var predictedCount = 0;
    function GetReport (start, end, rfresh) {
        //$("#crm-revenue-numb").text("processing ...");
        dateStart_crm = start;
        dateEnd_crm = end;
        $("#crm-revenue-numb").text('');
        $("#txt_loading_revenue").show();
        $('.reportrange span.time').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

        startTime = dateStart;
        endTime = dateEnd;
        var ggtoken = $("#ggtoken").val() ;
        if($("#ggtoken").val()=='') ggtoken='ya29.EljABL_GwQ89TQC24TwyO4eED2TBkY6gyhyZr5zP-Fnk6wRUuQCH-EwZT3Cr7dB8YaL2nczhuXy_YXi5Ove2Sg8sPUMTTWonU79hbXARS5I3gA9kToczcvkk';

            gapi.analytics.ready(function() {

            if(gapi.analytics.auth.isAuthorized)
            {

                GetDataGoogle(startTime,endTime);
            }
            gapi.analytics.auth.authorize({
                serverAuth: {
                    access_token:  ggtoken //'ya29.EljABL_GwQ89TQC24TwyO4eED2TBkY6gyhyZr5zP-Fnk6wRUuQCH-EwZT3Cr7dB8YaL2nczhuXy_YXi5Ove2Sg8sPUMTTWonU79hbXARS5I3gA9kToczcvkk'
                }
            });
            gapi.analytics.auth.on('success', function(response) {
                $("#crm-acquistion-cost-numb").text('');
                $("#crm-acquistion-cost div").hide();
                GetDataGoogle(startTime,endTime);
            });
            function GetDataGoogle(startd, endd) {
                var start=  eval("(" + startd + ")");
                var end=  eval("(" + endd + ")");
                var startdate= start.year+"-"+start.month+"-"+start.day;
                var enddate  = end.year+"-"+end.month+"-"+end.day;


                var eventPages = query({
                    'ids': 'ga:127952803',
                    'dimensions': 'ga:adwordsCampaignID',
                    'metrics': 'ga:adCost',
                    'start-date':  startdate,
                    'end-date':enddate
                });

                Promise.all([ eventPages]).then(function(results) {

                    try {
                        $("#ggspend").val(0);
                        var d=0;
                        results[0].rows.map(function(row) {
                            d = d + parseFloat(row[1]);
                        });
                        $("#ggspend").val(d);
                        d = d + parseFloat($("#bingspend").val());
                        d = parseFloat(d).toFixed(2);
                        $("#crm-acquistion-cost-numb").text('$ '+d);
                    }
                    catch(err) {
                        try {
                            var d = parseFloat($("#bingspend").val()).toFixed(2);
                            $("#crm-acquistion-cost-numb").text('$ '+d);
                        }
                        catch(err) {
                            $("#crm-acquistion-cost-numb").text(0);
                        }
                    }

                }
            );
        }
        function query(params) {
            return new Promise(function(resolve, reject) {
                var data = new gapi.analytics.report.Data({query: params});
                data.once('success', function(response) { resolve(response); })
                .once('error', function(response) { reject(response); })
                .execute();
            });
        }
    });

    if(rfresh == 1){
        $.ajax({
            url: "/session/sire/models/cfc/admin.cfc?method=GetAllReportCount"+strAjaxQuery,
            type: "POST",
            dataType: "json",
            data: {
                inpStartDate: dateStart,
                inpEndDate: dateEnd
            },
            asynce: false,
            beforeSend: function(xhr){
                $("#processingPayment").show();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            	$('#processingPayment').hide();
                alertBox(errorThrown.message,'Oops!','');
            },
            success: function(data){
                $("#processingPayment").hide();

                data = jQuery.parseJSON(data);
                if(parseInt(data.RXRESULTCODE) === 1){
                    $("#plan-free").text(data.FREE);
                    $("#plan-individual").text(data.INDV);
                    $("#plan-pro").text(data.PRO);
                    $("#plan-smb").text(data.SMB);
                    $("#signin-unique").text(data.UNIQUESIGNIN);
                    $("#user-active").text(data.ACTIVEUSER);
                    $("#total-user").text(data.TOTALUSER);
                    $("#new-user").text(data.NEWUSER);
                    $("#signin-inactive").text(data.SIGNUPINACTIVE);
                    $("#support-keyword").text(data.KEYWORDSUPPORTREQUEST);
                    // $("#user-inactive").text(data.INACTIVEUSER);
                    $("#user-paid").text(data.NEWPAIDUSER);
                    $("#upgrade").text(data.UPGRAGES);
                    $("#downgrade").text(data.DOWNGRADES);
                    $("#crm-msg-sent-numb").text(data.MESSAGESENT);
                    $("#crm-msg-received-numb").text(data.MESSAGERECEIVED);
                    $("#crm-opt-in-numb").text(data.TOTALOPTIN);
                    $("#crm-opt-out-numb").text(data.TOTALOPTOUT);
                    $("#crm-buy-credit-numb").text(data.BUYCREDIT);
                    $("#crm-buy-keyword-numb").text(data.BUYKEYWORD);
                    $("#total-enterprise-user-numb").text(data.ENTERPRISECOUNT);
                    $("#total-enterprise-revenue-numb").text(data.ENTERPRISEREVENUE);
                    $("#total-failed-mfa-numb").text(data.FAILEDMFA);
                    $("#bingspend").val(data.GETBINGSPEND);
                    if(data.ENTERPRISEBILLING == 0){
                        $("#enterprise-billing-numb").text('$ 0.00');
                    }else{
                        $("#enterprise-billing-numb").text('$ ' + data.ENTERPRISEBILLING.toFixed(2));
                    }
                    $("#total-last30days-request-downgrade").text(data.REQUESTDOWNGRADES);
                    enterpriseRevenue = data.ENTERPRISEREVENUE;
                    enterpriseRevenue1 = data.ENTERPRISEREVENUE.toFixed(2);
                    if(parseInt(data.WORDPAYTOTAL)==0){
                        $("#crm-revenue-numb").text('$' + enterpriseRevenue1);
                    }else{
                        $("#crm-revenue-numb").text('$'+ parseFloat(data.WORDPAYTOTAL + enterpriseRevenue).toFixed(2));
                    }
                    dotComRevenue = parseFloat(data.WORDPAY).toFixed(2);
                    dotComRevenueCredit = parseFloat(data.WORDPAYREFUND).toFixed(2);
                    dotComRevenueTotal = parseFloat(data.WORDPAYTOTAL).toFixed(2);

                    var remainDay = GetRemainDayOfMonth(data.CURRENTDATE);
                    var passedDay = GetPassedDayOfMonth(data.CURRENTDATE);
                    predictedCount = (data.MTCOUNT + (remainDay / passedDay * data.MTCOUNT));
                    mtCount = data.MTCOUNT;
                    if(remainDay == 0){
                        $("#total-predicted").text('0');
                        predictedCount = 0;
                    }
                    else{
                        $("#total-predicted").text(predictedCount.toFixed(0));
                    }
                }

                $.ajax({
                    url: "/session/sire/models/cfc/admin.cfc?method=SetJobRefreshCrmData"+strAjaxQuery,
                    type: "POST",
                    dataType: "json",
                    asynce: false,
                    data: {
                        inpStartDate: dateStart,
                        inpEndDate: dateEnd
                    },
                    beforeSend: function(xhr){
                        //$("#processingPayment").show();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#processingPayment').hide();
                        alertBox(errorThrown.message,'Oops!','');
                    },
                    success: function(data){
                        //$("#processingPayment").hide();
                    }
                });

            },
            complete: function (data) {
                $("#processingPayment").hide();
            }
        });

        getEnterPriseBilling(dateStart,dateEnd)

    }else{

        Promise.all([getDataCrmBasic(dateStart,dateEnd),getEnterPriseBilling(dateStart,dateEnd)]).then(() => {
            //$("#processingPayment").hide();
          }).catch((response) => {
            //$("#processingPayment").hide();
            alertBox('Can not get crm data','Oops!','');
          })

    }


}

function getDataCrmBasic(dateStart,dateEnd){
    return $.ajax({
            url: "/session/sire/models/cfc/admin.cfc?method=GetDataCRMSummary"+strAjaxQuery,
            type: "POST",
            dataType: "json",
            data: {
                inpStartDate: dateStart,
                inpEndDate: dateEnd
            },
            asynce: false,
            beforeSend: function(xhr){
                $("#processingPayment").show();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alertBox(errorThrown.message,'Oops!','');
                $("#processingPayment").hide();
            },
            success: function(data){
                $("#processingPayment").hide();
                data = jQuery.parseJSON(data);
                if(parseInt(data.RXRESULTCODE) === 1){
                    $("#plan-free").text(data.FREE);
                    $("#plan-individual").text(data.INDV);
                    $("#plan-pro").text(data.PRO);
                    $("#plan-smb").text(data.SMB);
                    $("#signin-unique").text(data.UNIQUESIGNIN);
                    $("#user-active").text(data.ACTIVEUSER);
                    $("#total-user").text(data.TOTALUSER);
                    $("#new-user").text(data.NEWUSER);
                    $("#signin-inactive").text(data.SIGNUPINACTIVE);
                    $("#support-keyword").text(data.KEYWORDSUPPORTREQUEST);
                    // $("#user-inactive").text(data.INACTIVEUSER);
                    $("#user-paid").text(data.NEWPAIDUSER);
                    $("#upgrade").text(data.UPGRAGES);
                    $("#downgrade").text(data.DOWNGRADES);
                    $("#crm-msg-sent-numb").text(data.MESSAGESENT);
                    $("#crm-msg-received-numb").text(data.MESSAGERECEIVED);
                    $("#crm-opt-in-numb").text(data.TOTALOPTIN);
                    $("#crm-opt-out-numb").text(data.TOTALOPTOUT);
                    $("#crm-buy-credit-numb").text(data.BUYCREDIT);
                    $("#crm-buy-keyword-numb").text(data.BUYKEYWORD);
                    $("#total-enterprise-user-numb").text(data.ENTERPRISECOUNT);
                    $("#total-enterprise-revenue-numb").text(data.ENTERPRISEREVENUE);
                    $("#total-failed-mfa-numb").text(data.FAILEDMFA);

                    //Last 30 Days User Request Downgrade
                    $("#total-last30days-request-downgrade").text(data.REQUESTDOWNGRADES);


                    $("#bingspend").val(data.GETBINGSPEND);
                    $("#fbadsspend").val(data.GETFBADSSPEND);

                    // Total Revenue
                    enterpriseRevenue = data.ENTERPRISEREVENUE;
                    enterpriseRevenue1 = data.ENTERPRISEREVENUE.toFixed(2);
                    if(parseInt(data.WORDPAYTOTAL)==0){
                        $("#crm-revenue-numb").text('$' + enterpriseRevenue1);
                    }else{
                        $("#crm-revenue-numb").text('$'+ parseFloat(data.WORDPAYTOTAL + enterpriseRevenue).toFixed(2));
                        dotComRevenue = parseFloat(data.WORDPAYTOTAL).toFixed(2);
                    }
                    dotComRevenue = parseFloat(data.WORDPAY).toFixed(2);
                    dotComRevenueCredit = parseFloat(data.WORDPAYREFUND).toFixed(2);
                    dotComRevenueTotal = parseFloat(data.WORDPAYTOTAL).toFixed(2);
                    var d = parseFloat(data.GETBINGSPEND) + parseFloat(data.GETFBADSSPEND) + parseFloat($("#ggspend").val());
                    $("#crm-acquistion-cost-numb").text('$ '+ d.toFixed(2));

                    // PREDICTED MT
                    var remainDay = GetRemainDayOfMonth(data.CURRENTDATE);
                    var passedDay = GetPassedDayOfMonth(data.CURRENTDATE);

                    predictedCount = (data.MTCOUNT + (remainDay / passedDay * data.MTCOUNT));
                    mtCount = data.MTCOUNT;
                    if(remainDay == 0){
                        $("#total-predicted").text('0');
                        predictedCount = 0;
                    }
                    else{
                        $("#total-predicted").text(predictedCount.toFixed(0));
                    }
                    // // Enterprise Billing
                    // if(data.ENTERPRISEBILLING ==0){
                    //     $("#enterprise-billing-numb").text('$ 0.00');
                    // }else{
                    //     $("#enterprise-billing-numb").text('$ ' + data.ENTERPRISEBILLING.toFixed(2));
                    // }

                }
            }
        });
}

function getEnterPriseBilling(dateStart,dateEnd){
    return $.ajax({
            url: "/session/sire/models/cfc/admin.cfc?method=GetEnterpriseBillingTotal"+strAjaxQuery,
            type: "POST",
            dataType: "json",
            data: {
                inpStartDate: dateStart,
                inpEndDate: dateEnd
            },
            asynce: false,
            beforeSend: function(xhr){
                $("#enterprise-billing-numb").html('<img src="/public/sire/images/loading.gif" class="ajax-loader pt_30"/>');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alertBox(errorThrown.message,'Oops!','');
                $("#enterprise-billing-numb").html('Can not get crm data');
            },
            success: function(data){
                if(parseInt(data.RXRESULTCODE) === 1){
                    // Enterprise Billing
                    if(parseInt(data.ENTERPRISEBILLING) === 0){
                        $("#enterprise-billing-numb").text('$ 0.00');
                    }else{
                        $("#enterprise-billing-numb").text('$ ' + data.ENTERPRISEBILLING.toFixed(2));
                    }
                }
            }
        });
}

function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}

function GetRemainDayOfMonth(today)
{
    var today = new Date(today);
    var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
    var remainDay = datediff(today, lastDayOfMonth);
    if(remainDay == -0){
        remainDay = 0;
    }
    return remainDay;
}
function GetPassedDayOfMonth(today)
{
    var today = new Date(today);
    today.setDate(today.getDate() + 1);  // Add one day is today

    var firstDay = new Date(today.getFullYear(), today.getMonth(), 01);
    var passedDay =  datediff(firstDay, today);
    return passedDay;
}

$('body').on('click', '#total-predicted', function(){
    $('#mtCounttxt').text(mtCount);
    $('#predictedCounttxt').text(predictedCount.toFixed(0));
    $("#total-message-in-this-month").modal("show");
});

$('body').on('click', '#crm-revenue', function(){
    if(isNaN(parseFloat(dotComRevenue))){
        dotComRevenue = '0.00';
    }
    if(isNaN(parseFloat(dotComRevenueCredit))){
        dotComRevenueCredit = '0.00';
    }
    if(isNaN(parseFloat(dotComRevenueTotal))){
        dotComRevenueTotal = '0.00';
    }
    if(isNaN(parseFloat(enterpriseRevenue1))){
        enterpriseRevenue1 = '0.00';
    }
    $('#dotComRevenue').text('$ ' + dotComRevenue);
    $('#dotComRevenueCredit').text('$ ' + dotComRevenueCredit);
    $('#dotComRevenueTotal').text('$ ' + dotComRevenueTotal);

    $('#EnterpriseRevenue').text('$ ' + enterpriseRevenue1);
    $('#EnterpriseRevenueTotal').text('$ ' + enterpriseRevenue1);
    $("#total-revenue-modal").modal("show");
});

$('body').on('click', '#crm-acquistion-cost', function(){
    $('#gaac').text('$ ' + parseFloat($("#ggspend").val()).toFixed(2));
    $('#babs').text('$ ' + parseFloat($("#bingspend").val()).toFixed(2));
    $('#fbabs').text('$ ' + parseFloat($("#fbadsspend").val()).toFixed(2));
    $("#acquistion-cost-modal").modal("show");
});


/*User Plan Details Report***********************************************************************/
function GetUserPlanList(strFilter){
    $("#userPlanTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#userPlanTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "dt-body-norap", "sWidth": "200px"},
            {"mData": "ORGNAME", "sName": 'Org Name', "sTitle": 'Org Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "PLANSTARTDATE", "sName": 'Plan Start Date', "sTitle": 'Plan Start Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "PLANENDDATE", "sName": 'Plan End Date', "sTitle": 'Plan End Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "PLANSTATUS", "sName": 'Plan Status', "sTitle": 'Plan Status', "bSortable": false, "sClass": "", "sWidth": "165px"},
            {"mData": "PLANNAME", "sName": 'Plan Name', "sTitle": 'Plan Name', "bSortable": false, "sClass": "", "sWidth": "160px"}
        ],
        // No wrap data so easier to read        
          "fnDrawCallback": function( oSettings ) {
               $('#userPlanTable tbody tr').each(function(){
                   $(this).find('td').attr('nowrap', 'nowrap');
              });
         },
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetAllUserPlanReport'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "inpPlanName", "value": planName}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#userPlanTable_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterBarForUserPlanTable () {
    $("#userPlanFilterBox").html('');
    //Filter bar initialize
    $('#userPlanFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Plan Start Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' up.StartDate_dt '},
            {DISPLAY_NAME: 'Plan End Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' up.EndDate_dt '},
            //{DISPLAY_NAME: 'Plan Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' up.Status_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetUserPlanList();
        }
    }, function(filterData){
        GetUserPlanList(filterData);
    });
}

$(".plan-info").click(function(event) {
    planName = $(this).data('plan');
    $("#user-plan-modal-title").text(planName);
    GetUserPlanList();
    InitFilterBarForUserPlanTable();
    $("#user-plan-modal").modal("show");
});

$('body').on("click", "button.btn-re-dowload.user-plan", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-userplan-list.cfm?planname="+planName+"&startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});
/************************************************************************End User Plan Details Report*/



/*Report For User Table************************************************************************/
function GetUserList(strFilter){
    $("#userTable").html('');

    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    var source= function(object){
        var strReturn="";
        if(object.SOURCE != undefined){

            var strReturn= object.SOURCE;
        }
        else{
            var strReturn= null;
        }
        return strReturn;
    }

    var table = $('#userTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},

            {"mData":source, "sName": "Source", "sTitle": "Source", "bSortable": false, "sClass": "", "sWidth": "100px"},


            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method='+method+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#userTable_paginate').hide();
                    }
                    if(method=="getUsers"){
                        $("#user-modal .source-section").show();
                        GetUserSummarySource();
                    }
                    else
                    {
                        $("#user-modal .source-section").hide();
                    }
                }
            });
        }
    });
}

function InitFilterForUserTable () {
    $("#userFilterBox").html('');
    //Filter bar initialize
    $('#userFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetUserList();
        }
    }, function(filterData){
        GetUserList(filterData);
    });
}

$("#signin-unique").click(function(event) {
    method = 'GetUniqueLoginReport';
    exportPath = "admin-export-uniquesignin-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Unique Users Sign In');
    $("#user-modal").modal("show");
});

$("#signin-inactive").click(function(event) {
    method = 'GetUserSignUpInactive';
    exportPath = "admin-export-signupinactive-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Users Not Active Since Sign Up');
    $("#user-modal").modal("show");
});

$("#user-paid").click(function(event) {
    method = 'GetNewPaidUsers';
    exportPath = "admin-export-newpaidusers-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('New Paid Users');
    $("#user-modal").modal("show");
});

$("#user-active").click(function(event) {
    method = 'GetUserActiveReport';
    exportPath = "admin-export-active-user-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Total Active Users');
    $("#user-modal").modal("show");
});

$("#user-inactive").click(function(event) {
    method = 'GetUserInActiveReport';
    exportPath = "admin-export-inactive-user-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Total Inactive Users');
    $("#user-modal").modal("show");
});

$("#total-user").click(function(event) {
    method = 'getUsers';
    exportPath = "admin-export-totaluser-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Total Users');
    $("#user-modal").modal("show");
});

$("#new-user").click(function(event) {
    method = 'getNewUsers';
    exportPath = "admin-export-newuser-list.cfm";
    GetUserList();
    InitFilterForUserTable();
    $("#user-modal-title").text('Total of New Users');
    $("#user-modal").modal("show");
});

$('body').on("click", "button.btn-re-dowload.user-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/"+exportPath+"?startdate="+startTime+"&enddate="+endTime+"&source="+exp_user_source +"&isaffiliatesource="+exp_user_isaffiliatesource +"&user_type="+exp_user_type;
});

/************************************************************************End User Report*/


/*Keyword Support Details Report***********************************************************************/
function GetKeywordSupportReport(strFilter){
    $("#keywordSuportTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#keywordSuportTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "REQUESTID", "sName": 'ID', "sTitle": 'ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "BATCHID", "sName": 'Batch ID', "sTitle": 'Batch ID', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "STATUS", "sName": 'Status', "sTitle": 'Status', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "NOTIFIED", "sName": 'Notified', "sTitle": 'Notified', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "CREATED", "sName": 'Created at', "sTitle": 'Created at', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "UPDATED", "sName": 'Updated at', "sTitle": 'Updated at', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CUSTOMERMESSAGE", "sName": 'Customer Message', "sTitle": 'Customer Message', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "NOTE", "sName": 'Note', "sTitle": 'Note', "bSortable": false, "sClass": "", "sWidth": "250px"}
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetKeywordSupportRequestReports'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#keywordSuportTable_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterBarForKeywordSupportRequestTable () {
    $("#keywordSuportFilterBox").html('');
    //Filter bar initialize
    $('#keywordSuportFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Batch ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' sp.BatchId_bi '},
            {DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' sp.Status_int '},
            {DISPLAY_NAME: 'Notified', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' sp.Notified_int '},
            {DISPLAY_NAME: 'Created at', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' sp.Created_dt '},
            {DISPLAY_NAME: 'Updated at', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' sp.LastUpdated_dt '},
            {DISPLAY_NAME: 'Contact string', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' sp.ContactString_vch '},
            {DISPLAY_NAME: 'Customer message', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' sp.CustomerMessage_vch '},
            {DISPLAY_NAME: 'Note', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' sp.Note_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetKeywordSupportReport();
        }
    }, function(filterData){
        GetKeywordSupportReport(filterData);
    });
}

$("#support-keyword").click(function(event) {
    GetKeywordSupportReport();
    InitFilterBarForKeywordSupportRequestTable();
    $("#keyword-support-modal").modal("show");
});

$('body').on("click", "button.btn-re-dowload.keyword-support-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-keyword-support-list.cfm?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});
/************************************************************************End Keyword Support Details Report*/

// /*Report For User Table************************************************************************/
// // function GetUpDowngradeList(strFilter){
// //     $("#upDownTable").html('');
// //     var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

// //     var table = $('#upDownTable').dataTable({
// //         "bStateSave": false,
// //         "iStateDuration": -1,
// //         "bProcessing": true,
// //         "bFilter": false,
// //         "bServerSide":true,
// //         "bDestroy":true,
// //         "sPaginationType": "input",
// //         "bLengthChange": false,
// //         "iDisplayLength": 10,
// //         "bAutoWidth": false,
// //         "aoColumns": [
// //             {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
// //             {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
// //             {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
// //             {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
// //             {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "170px"},
// //             {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
// //             {"mData": planField, "sName": planAction + ' Date', "sTitle": planAction + ' Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
// //             //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
// //         ],
// //         "sAjaxDataProp": "datalist",
// //         "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method='+method+strAjaxQuery,
// //         "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

// //             aoData.push(
// //                 { "name": "inpStartDate", "value": startTime}
// //             );
// //             aoData.push(
// //                 { "name": "inpEndDate", "value": endTime}
// //             );
// //             aoData.push(
// //                 { "name": "customFilter", "value": customFilterData}
// //             );

// //             filter = customFilterData;

// //             oSettings.jqXHR = $.ajax( {
// //                 "dataType": 'json',
// //                 "type": "POST",
// //                 "url": sSource,
// //                 "data": aoData,
// //                 beforeSend: function(){
// //                     $('#processingPayment').show();
// //                 },
// //                 error: function(jqXHR, textStatus, errorThrown) {
// //                     $("#processingPayment").hide();
// //                     alertBox('Action Error!','Loading Data Error','');
// //                 },
// //                 "success": function(data) {
// //                     $("#processingPayment").hide();
// //                     fnCallback(data);
// //                     if(data.datalist.length == 0){
// //                         $('#upDownTable_paginate').hide();
// //                     }
// //                 }
// //             });
// //         }
// //     });
// // }

function GetUpgradeList(strFilter){
    $("#upTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#upTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "170px"},
            {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": planField, "sName": planAction + ' Date', "sTitle": planAction + ' Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method='+method+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#upTable_paginate').hide();
                    }
                }
            });
        }
    });
}
function GetDowngradeList(strFilter){
    $("#DownTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#DownTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": planField, "sName": planAction + ' Date', "sTitle": planAction + ' Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "FROMPLANNAME", "sName": 'From Plan', "sTitle": 'From Plan', "bSortable": false, "sClass": "", "sWidth": "170px"},
            {"mData": "TOPLANNAME", "sName": 'To Plan', "sTitle": 'To Plan', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "APPROVEDBY", "sName": 'Approved By', "sTitle": 'Approved By', "bSortable": false, "sClass": "", "sWidth": "200px"}
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method='+method+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#DownTable_paginate').hide();
                    }
                }
            });
        }
    });
}

function GetLast30DaysDowngradeList(strFilter){
    $("#Last30daysDownTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#Last30daysDownTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": planField, "sName": planAction + ' Date', "sTitle": planAction + ' Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "FROMPLANNAME", "sName": 'From Plan', "sTitle": 'From Plan', "bSortable": false, "sClass": "", "sWidth": "170px"},
            {"mData": "TOPLANNAME", "sName": 'To Plan', "sTitle": 'To Plan', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "DOWNGRADESDATE", "sName": 'Downgrades Date', "sTitle": 'Downgrades Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "APPROVEDBY", "sName": 'Approved By', "sTitle": 'Approved By', "bSortable": false, "sClass": "", "sWidth": "200px"}

        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method='+method+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#Last30daysDownTable_paginate').hide();
                    }
                }
            });
        }
    });
}

// function InitFilterUpDowngradeTable () {
//     $("#upDownFilterBox").html('');
//     //Filter bar initialize
//     $('#upDownFilterBox').sireFilterBar({
//         fields: [
//             {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
//             {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
//             {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
//             {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
//             {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
//             {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
//             {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
//             //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
//         ],
//         clearButton: true,
//         error: function(ele){

//         },
//         clearCallback: function(){
//             GetUpDowngradeList();
//         }
//     }, function(filterData){
//         GetUpDowngradeList(filterData);
//     });
// }
function InitFilterUpgradeTable () {
    $("#upFilterBox").html('');
    //Filter bar initialize
    $('#upFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetUpgradeList();
        }
    }, function(filterData){
        GetUpgradeList(filterData);
    });
}
function InitFilterDowngradeTable () {
    $("#DownFilterBox").html('');
    //Filter bar initialize
    $('#DownFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetDowngradeList();
        }
    }, function(filterData){
        GetDowngradeList(filterData);
    });
}
function InitFilterLast30DaysDowngradeTable () {
    $("#last30DaysDownFilterBox").html('');
    //Filter bar initialize
    $('#last30DaysDownFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetLast30DaysDowngradeList();
        }
    }, function(filterData){
        GetLast30DaysDowngradeList(filterData);
    });
}

$("#upgrade").click(function(event) {
    method = 'getUserUpgrades';
    exportPath = "admin-export-upgrade-list.cfm";
    planAction = "Upgrades";
    planField = "UPGRADES";
    GetUpgradeList();
    InitFilterUpgradeTable();
    $("#up-modal-title").text('Total Upgrades');
    $("#up-modal").modal("show");
});

$("#downgrade").click(function(event) {
    method = 'getUserDowngrades';
    exportPath = "admin-export-downgrade-list.cfm";
    planAction = "Downgrades";
    planField = "DOWNGRADES";
    GetDowngradeList();
    InitFilterDowngradeTable();
    $("#down-modal-title").text('Total Downgrades');
    $("#down-modal").modal("show");
});

$("#total-last30days-request-downgrade").click(function(event) {
    method = 'getUserRequestDowngrades';
    exportPath = "admin-export-request-downgrade-list.cfm";
    planAction = "Request Downgrades";
    planField = "REQUESTDOWNGRADES";
    GetLast30DaysDowngradeList();
    InitFilterLast30DaysDowngradeTable();

    //Inital From Date
    var fromDate = addDays(new Date(), -30);
    var fromMonths = fromDate.getMonth() + 1;
    var fromDates = fromDate.getDate();

    fromMonths = fromMonths < 10 ? '0'+fromMonths : fromMonths;
    fromDates = fromDates < 10 ? '0'+fromDates : fromDates;

    // Inital To Date
    var date = new Date();
    var months = date.getMonth() + 1;
    var dates =  date.getDate();
    months = months < 10 ? '0'+months : months;
    dates = dates < 10 ? '0'+dates : dates;
    var fromDate = fromDate.getFullYear() + "-" + fromMonths + "-" + fromDates;
    var toDate = date.getFullYear() + "-"  + months + "-" + dates;

    $("#last30days-down-modal-title").text('Total Last 30 Days User Request Downgrades from ' + fromDate + ' to ' + toDate);
    $("#last30days-down-modal").modal("show");
});

function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}

$('body').on("click", "button.btn-re-dowload.up-down-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/"+exportPath+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});
/************************************************************************End User Report*/

/*Report Total Sent Reveived Table************************************************************************/
function GetTotalSentReceivedMessageList(strFilter){
    $("#total-message-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var sentCol = function(object){
        if(object.USERID == ''){
            return object.SENT;
        }
        return $('<a class="sent-received-details" data-id="'+object.USERID+'">'+object.SENT+'</a>')[0].outerHTML;
    }

    var receivedCol = function(object){
        if(object.USERID == ''){
            return object.RECEIVED;
        }
        return $('<a class="sent-received-details" data-id="'+object.USERID+'">'+object.RECEIVED+'</a>')[0].outerHTML;
    }

    var table = $('#total-message-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
            {"mData": sentCol, "sName": '', "sTitle": 'Sent Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": receivedCol, "sName": '', "sTitle": 'Rec\'d Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetTotalSentReceivedMessageList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $('#processingPayment').hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-message-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterMessageTable () {
    $("#totalMessageFilterBox").html('');
    //Filter bar initialize
    $('#totalMessageFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalSentReceivedMessageList();
        }
    }, function(filterData){
        GetTotalSentReceivedMessageList(filterData);
    });
}

$('#crm-message-sent').click(function(event){
    InitFilterMessageTable();
    GetTotalSentReceivedMessageList();
    $("#total-message-modal").modal("show");
    $("#totalMessageFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalMessageFilterBox .btn-add-item").hide();
});

$('#crm-message-received').click(function(event){
    InitFilterMessageTable();
    GetTotalSentReceivedMessageList();
    $("#total-message-modal").modal("show");
    $("#totalMessageFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalMessageFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-message-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-sent-received-msg-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

/************************************************************************End Total message sent-received Report*/

/*Report Summary Sent Received Message Table********************************************************************/
function GetSummarySentReceivedMessage(strFilter){
    $("#total-message-summary-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var sentCol = function(object){
        // if(object.USERID == ''){
        //     return object.SENT;
        // }
        if(object.BATCHID >0 )
        {
            return $('<a class="preview-campaign" data-batchid="'+object.BATCHID +'" data-userid="'+object.USERID+'">'+object.SENT+'</a>')[0].outerHTML;
        }
        else
        {
            return $('<span class="" data-batchid="'+object.BATCHID +'" data-userid="'+object.USERID+'">'+object.SENT+'</span>')[0].outerHTML;
        }
    }

    var table = $('#total-message-summary-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "NAME", "sName": '', "sTitle": 'Campaign', "bSortable": false, "sClass": "", "sWidth": "270px"},
            {"mData": sentCol, "sName": '', "sTitle": 'Sent Count', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "RECEIVED", "sName": '', "sTitle": 'Rec\'d Count', "bSortable": false, "sClass": "", "sWidth": "150px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetSummarySentReceivedMessage'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpUserId", "value": $('#user-id').val()}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-message-summary-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterSummaryMessageTable () {
    $("#totalMessageSummaryFilterBox").html('');
    //Filter bar initialize
    $('#totalMessageSummaryFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ba.Desc_vch '},
            {DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.BatchId_bi '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetSummarySentReceivedMessage();
        }
    }, function(filterData){
        GetSummarySentReceivedMessage(filterData);
    });
}

$('body').on('click', '.sent-received-details', function(){
    var userId = $(this).data('id');
    $('#user-id').val(userId);
    InitFilterSummaryMessageTable();
    GetSummarySentReceivedMessage();
    $("#total-message-summary-modal").modal("show");
    $("#totalMessageSummaryFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalMessageSummaryFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-message-summary-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-summary-msg-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+"&userId="+$('#user-id').val();
});
/************************************************************************End Summary Report*/

/*Report Total Sent Received Table************************************************************************/
function GetOptInOutList(strFilter){
    $("#total-optin-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var optInCol = function(object){
        return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
    }

    var optOutCol = function(object){
        return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
    }
    var table = $('#total-optin-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
            {"mData": optInCol, "sName": '', "sTitle": 'Opt-In Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": optOutCol, "sName": '', "sTitle": 'Opt-Out Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetOptInOutList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-optin-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterOptTable () {
    $("#totalOptInFilterBox").html('');
    //Filter bar initialize
    $('#totalOptInFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetOptInOutList();
        }
    }, function(filterData){
        GetOptInOutList(filterData);
    });
}

//Split opt-in and opt-out
// function InitFilterOptOutTable () {
//     $("#totalOptInOutFilterBox").html('');
//     //Filter bar initialize
//     $('#totalOptInOutFilterBox').sireFilterBar({
//         fields: [
//             {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
//             {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.UserId_int '},
//             {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
//             {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '}
//         ],
//         clearButton: true,
//         error: function(ele){

//         },
//         clearCallback: function(){
//             GetOptOutList();
//         }
//     }, function(filterData){
//         GetOptOutList(filterData);
//     });
// }
//Split opt-in and opt-out
function InitFilterOptOutTable () {
    $("#totalOptOutFilterBox").html('');
    //Filter bar initialize
    $('#totalOptOutFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' oi.ContactString_vch '},
            {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_BIGINT', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.BatchId_bi '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetOptOutList();
        }
    }, function(filterData){
        GetOptOutList(filterData);
    });
}

$('#totalOptOutFilterBox').on('change', function(event){
    var selectedFilter = $("#totalOptOutFilterBox :selected").text().includes("Campaign Name");
    if(selectedFilter){
        $("#totalOptOutFilterBox .col-xs-12 .input-box").hide();
        var isExistFilter =  $("#totalOptOutFilterBox .filter-item .clearfix div").hasClass("btn-box-campaign");
        if(!isExistFilter){
            $("#totalOptOutFilterBox .filter-item .clearfix").append('<div class="btn-box-campaign" style="width: calc(100% - 120px);"><select class="form-control filter-option pull-left validate[required]" name="campaign-id" id="campaign-id" ></select><input type="hidden" name="cur-campaign-id" value="<cfoutput>#Session.USERID#</cfoutput>"/><input type="hidden" name="selected-campaign-id" id="selected-campaign-id" value=""/></div>');

            intialSelectCampaign();
        }
        else{
             $(".btn-box-campaign").show();
        }
        $("#totalOptOutFilterBox .col-xs-12 .input-box input.form-control").val($("#selected-campaign-id").val());
    }
    else{
        $("#totalOptOutFilterBox .col-xs-12 .input-box").show();
        $(".btn-box-campaign").hide();
    }
});

// Select user form user-id or email
function intialSelectCampaign(){
    var listIds = $("#listUserids").val();
    var listIds = listIds.slice(0, -1);

    $("select[name='campaign-id']").select2({
        placeholder: "Search by campaign name",
        allowClear: true,
        ajax: {
            url: "/session/sire/models/cfc/admin.cfc?method=GetCampainListByUserId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    inpCampaignName: params.term, // search term
                    inpUserIds: listIds, // search term
                    inpStartDate: startTime, // search term
                    inpEndDate: endTime, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.EMAIL,
                            id: item.ID
                        }
                    })
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        theme: "bootstrap",
        width: 'auto'
    });
    $("#campaign-id").change(function() {
      var placeholderSesion = $('.select2-selection__placeholder').text();
      if(placeholderSesion == ""){
        var renderSelectUser = $("#select2-campaign-id-container").attr("title");
        var res = renderSelectUser.split(" - ");
        $("#selected-campaign-id").val(res[0]);

        var optInCol = function(object){
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
        }

        var optOutCol = function(object){
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
        }
        $("#total-optout-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        $("#totalOptInOutFilterBox .col-xs-12 .input-box input.form-control").val(res[0]);

        var table = $('#total-optout-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "DATE", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CAMPAIGNNAME", "sName": '', "sTitle": 'Campaign Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": null, "sName": '', "sTitle": 'Inital userids', "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false}
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/campaign.cfc?method=GetOptOutListByBatch'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    {"name": "inpBatchIdList", "value": $("#selected-campaign-id").val()}
                );

                filter = '[{"NAME":" b.BatchId_bi ","OPERATOR":"LIKE","VALUE":"'+$("#selected-campaign-id").val()+'","TYPE":"CF_SQL_BIGINT"}]';

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
                            $('.dowload-optout-list').hide();
                        }
                        else
                        {
                            $('.dowload-optout-list').show();
                        }
                        fnCallback(data);
                        $("#mdOptOutList").modal("show");
                    }
                });
            }
        });
      }
      else{
        $("#selected-campaign-id").val('');
      }
});
}

function GetOptOutList(strFilter){
    var intialUserIds = function(object){
        var currentIds = $("#listUserids").val();
        if(currentIds != ''){
            var listCpIds = currentIds.split(",");
            var isAdd = false;
            for (a in listCpIds) {
                if( listCpIds[a] == object.USERID)
                {
                    isAdd = true;
                }
            }
            if(!isAdd){
                currentIds = currentIds +  object.USERID + ',';
                $("#listUserids").val(currentIds);
            }
        }
        else{
            $("#listUserids").val(object.USERID+ ',');
        }

        return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
    }

    $("#total-optout-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var optInCol = function(object){
        return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
    }

    var optOutCol = function(object){
        return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
    }

    var table = $('#total-optout-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "DATE", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},
            // {"mData": "USERID", "sName": '', "sTitle": 'User Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "CAMPAIGNNAME", "sName": '', "sTitle": 'Campaign Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": intialUserIds, "sName": '', "sTitle": 'Inital userids', "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false},
             // {"mData": null, "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px", "bVisible": false},
             //    {"mData": null, "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px", "bVisible": false},
             //    {"mData": optInCol, "sName": '', "sTitle": 'Opt-In Cnt',"sId": "bttttt", "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false},
             //    {"mData": optOutCol, "sName": '', "sTitle": 'Opt-Out Cnt', "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/reports/campaign.cfc?method=GetOptOutListByDateRange'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                {"name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                {"name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    if(data.DATALIST.length == 0) {
                        $('.dowload-optout-list').hide();
                    }
                    else
                    {
                        $('.dowload-optout-list').show();
                    }
                    fnCallback(data);
                    $("#mdOptOutList").modal("show");
                }
            });
        }
    });
}

// function GetOptOutList(strFilter){
//     $("#total-optinout-table").html('');
//     var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

//     var optInCol = function(object){
//         return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
//     }

//     var optOutCol = function(object){
//         var currentIds = $("#listUserids").val();
//         if(currentIds != ''){
//             var listCpIds = currentIds.split(",");
//             var isAdd = false;
//             for (a in listCpIds) {
//                 if( listCpIds[a] == object.USERID)
//                 {
//                     isAdd = true;
//                 }
//             }
//             if(!isAdd){
//                 currentIds = currentIds +  object.USERID + ',';
//                 $("#listUserids").val(currentIds);
//             }
//         }
//         else{
//             $("#listUserids").val(object.USERID+ ',');
//         }

//         return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
//     }
//     // No filter or start
//     var table = $('#total-optinout-table').dataTable({
//     "bStateSave": false,
//     "iStateDuration": -1,
//     "bProcessing": true,
//     "bFilter": false,
//     "bServerSide":true,
//     "bDestroy":true,
//     "sPaginationType": "input",
//     "bLengthChange": false,
//     "iDisplayLength": 10,
//     "bAutoWidth": false,
//     "aoColumns": [
//         {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
//         {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
//         {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
//         {"mData": optInCol, "sName": '', "sTitle": 'Opt-In Cnt',"sId": "bttttt", "bSortable": false, "sClass": "", "sWidth": "120px"},
//         {"mData": optOutCol, "sName": '', "sTitle": 'Opt-Out Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
//     ],
//     "sAjaxDataProp": "DATALIST",
//     "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetOptInOutList'+strAjaxQuery,
//     "fnDrawCallback": function( oSettings ) {
//         if (oSettings._iDisplayStart < 0){
//             oSettings._iDisplayStart = 0;
//             $('input.paginate_text').val("1");
//         }
//     },
//     "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

//         aoData.push(
//             { "name": "inpStartDate", "value": startTime}
//         );
//         aoData.push(
//             { "name": "inpEndDate", "value": endTime}
//         );
//         aoData.push(
//             { "name": "customFilter", "value": customFilterData}
//         );

//         filter = customFilterData;

//         oSettings.jqXHR = $.ajax( {
//             "dataType": 'json',
//             "type": "POST",
//             "url": sSource,
//             "data": aoData,
//             beforeSend: function(){
//                 $('#processingPayment').show();
//             },
//             error: function(jqXHR, textStatus, errorThrown) {
//                 $("#processingPayment").hide();
//                 alertBox('System is timeout!','Oops!','');
//             },
//             "success": function(data) {
//                 $("#processingPayment").hide();
//                 fnCallback(data);
//                 if(data.DATALIST.length == 0){
//                     $('#total-optinout-table_paginate').hide();
//                 }
//             }
//         });
//     }
//     });
// }


//End split opt-in and opt-out

$('#crm-opt-in').click(function(event){
    InitFilterOptTable();
    GetOptInOutList();
    $("#total-optin-modal").modal("show");
    $("#totalOptInFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalOptInFilterBox .btn-add-item").hide();
});

$('#crm-opt-out').click(function(event){
    InitFilterOptOutTable();
    GetOptOutList();
    $("#total-optout-modal").modal("show");
    $("#totalOptOutFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalOptOutFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-optout-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-opt-by-campaign.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});
$('body').on("click", "button.btn-re-dowload.total-optin-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-opt-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

/************************************************************************End Total message sent-received Report*/

/*Report Summary Opt In-out Table********************************************************************/
function GetOptInOutSummaryList(strFilter){
    $("#total-opt-summary-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#total-opt-summary-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "NAME", "sName": '', "sTitle": 'Campaign', "bSortable": false, "sClass": "", "sWidth": "270px"},
            {"mData": "OPTIN", "sName": '', "sTitle": 'Opt-In count', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "OPTOUT", "sName": '', "sTitle": 'Opt-Out count', "bSortable": false, "sClass": "", "sWidth": "150px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetOptInOutSummaryList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpUserId", "value": $('#user-id').val()}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-opt-summary-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterSummaryOptTable () {
    $("#totalOptSummaryFilterBox").html('');
    //Filter bar initialize
    $('#totalOptSummaryFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
            {DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.BatchId_bi '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetOptInOutSummaryList();
        }
    }, function(filterData){
        GetOptInOutSummaryList(filterData);
    });
}

$('body').on('click', '.opt-in-out-details', function(){
    var userId = $(this).data('id');
    $('#user-id').val(userId);
    InitFilterSummaryOptTable();
    GetOptInOutSummaryList();
    $("#total-opt-summary-modal").modal("show");
    $("#totalOptSummaryFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalOptSummaryFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-opt-summary-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-summary-opt-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+"&userId="+$('#user-id').val();
});
/************************************************************************End Summary Report*/

/*Report Total Addon Table************************************************************************/
function GetTotalBuyCreditList(strFilter){
    $("#total-credit-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var creditCol = function(object){
        return $('<a class="buy-credit-details" data-id="'+object.USERID+'">'+object.CREDIT+'</a>')[0].outerHTML;
    }

    var table = $('#total-credit-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
            {"mData": creditCol, "sName": '', "sTitle": 'Credits', "bSortable": false, "sClass": "", "sWidth": "120px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetTotalBuyCreditList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-credit-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function GetTotalBuyKeywordList(strFilter){
    $("#total-keyword-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var keywordCol = function(object){
        return $('<a class="buy-keyword-details" data-id="'+object.USERID+'">'+object.KEYWORD+'</a>')[0].outerHTML;
    }

    var table = $('#total-keyword-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
            {"mData": keywordCol, "sName": '', "sTitle": 'Keywords', "bSortable": false, "sClass": "", "sWidth": "120px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetTotalBuyKeywordList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-keyword-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterBuyCreditTable () {
    $("#totalCreditFilterBox").html('');
    //Filter bar initialize
    $('#totalCreditFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' p.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalBuyCreditList();
        }
    }, function(filterData){
        GetTotalBuyCreditList(filterData);
    });
}

function InitFilterBuyKeywordTable () {
    $("#totalKeywordFilterBox").html('');
    //Filter bar initialize
    $('#totalKeywordFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' p.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalBuyKeywordList();
        }
    }, function(filterData){
        GetTotalBuyKeywordList(filterData);
    });
}

$('#crm-buy-credit').click(function(event){
    InitFilterBuyCreditTable();
    GetTotalBuyCreditList();
    $("#total-credit-modal").modal("show");
    $("#totalCreditFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalCreditFilterBox .btn-add-item").hide();
});

$('#crm-buy-keyword').click(function(event){
    InitFilterBuyKeywordTable();
    GetTotalBuyKeywordList();
    $("#total-keyword-modal").modal("show");
    $("#totalKeywordFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalKeywordFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-credit-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-buy-credit-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

$('body').on("click", "button.btn-re-dowload.total-keyword-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-buy-keyword-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

/************************************************************************End Total message sent-received Report*/

/*Report Total Buy Credit Detail Table********************************************************************/
function GetTotalBuyCreditDetails(strFilter){
    $("#total-credit-details-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#total-credit-details-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "CREDIT", "sName": '', "sTitle": 'Credits', "bSortable": false, "sClass": "", "sWidth": "270px"},
            {"mData": "CREATED", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetTotalBuyCreditDetails'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpUserId", "value": $('#user-id').val()}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-credit-details-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function GetTotalBuyKeywordDetails(strFilter){
    $("#total-keyword-details-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#total-keyword-details-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "KEYWORD", "sName": '', "sTitle": 'Keywords', "bSortable": false, "sClass": "", "sWidth": "270px"},
            {"mData": "CREATED", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetTotalBuyKeywordDetails'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpUserId", "value": $('#user-id').val()}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-keyword-details-table_paginate').hide();
                    }
                }
            });
        }
    });
}


function InitFilterBuyCreditDetailsTable () {
    $("#totalCreditDetailsFilterBox").html('');
    //Filter bar initialize
    $('#totalCreditDetailsFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Date', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' created '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalBuyCreditDetails();
        }
    }, function(filterData){
        GetTotalBuyCreditDetails(filterData);
    });
}

function InitFilterBuyKeywordDetailsTable () {
    $("#totalKeywordDetailsFilterBox").html('');
    //Filter bar initialize
    $('#totalKeywordDetailsFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Date', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' created '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalBuyKeywordDetails();
        }
    }, function(filterData){
        GetTotalBuyKeywordDetails(filterData);
    });
}



$('body').on('click', '.buy-credit-details', function(){
    var userId = $(this).data('id');
    $('#user-id').val(userId);
    InitFilterBuyCreditDetailsTable();
    GetTotalBuyCreditDetails();
    $("#total-buy-credit-details-modal").modal("show");
    $("#totalCreditDetailsFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalCreditDetailsFilterBox .btn-add-item").hide();
});

$('body').on('click', '.buy-keyword-details', function(){
    var userId = $(this).data('id');
    $('#user-id').val(userId);
    InitFilterBuyKeywordDetailsTable();
    GetTotalBuyKeywordDetails();
    $("#total-buy-keyword-details-modal").modal("show");
    $("#totalKeywordDetailsFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalKeywordDetailsFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-credit-details-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-buy-credit-details-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+"&userId="+$('#user-id').val();
});

$('body').on("click", "button.btn-re-dowload.total-keyword-details-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-buy-keyword-details-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+"&userId="+$('#user-id').val();
});
/************************************************************************End Summary Report*/


/*Report Total Enterprise User Table************************************************************************/
function GetTotalEnterpriseUserList(strFilter){
    $("#total-enterprise-user-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#total-enterprise-user-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "280px"},
            {"mData": "THRESHOLD", "sName": '', "sTitle": 'Threshold', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "CREDIT", "sName": '', "sTitle": 'Credits', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "160px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetEnterpriseUsers'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-enterprise-user-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterEnterpriseUserTable () {
    $("#enterpriseUserFilterBox").html('');
    //Filter bar initialize
    $('#enterpriseUserFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' u.MFAContactString_vch '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalEnterpriseUserList();
        }
    }, function(filterData){
        GetTotalEnterpriseUserList(filterData);
    });
}

$('#total-enterprise-user').click(function(event){
    InitFilterEnterpriseUserTable();
    GetTotalEnterpriseUserList();
    $("#total-enterprise-users-modal").modal("show");
    $("#enterpriseUserFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#enterpriseUserFilterBox .btn-add-item").hide();
});

$('body').on("click", "button.btn-re-dowload.total-enterprise-user-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-enterprise-user-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

/*************Start report .COM paymenent details */
$('.show-Com-payment-revenue').click(function(event){
    InitFilterCOMPaymentDetailsTable();
    GetTotalCOMPaymentDetailList();
    $("#total-com-payment-detail-modal").modal("show");
    $("#totalComPaymentDetailFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalComPaymentDetailFilterBox .btn-add-item").hide();
});

function InitFilterCOMPaymentDetailsTable () {
    $("#totalComPaymentDetailFilterBox").html('');
    //Filter bar initialize
    $('#totalComPaymentDetailFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' us.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' us.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' us.MFAContactString_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalCOMPaymentDetailList();
        }
    }, function(filterData){
        GetTotalCOMPaymentDetailList(filterData);
    });
}
function GetTotalCOMPaymentDetailList(strFilter){
    $("#total-com-payment-detail-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    var transactionAmountCol = function(object){
        return ("$ " + parseFloat(object.TRANSACTIONAMOUNT).toFixed(2));
    }

    var table = $('#total-com-payment-detail-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "280px"},
            {"mData": transactionAmountCol, "sName": 'Transaction Amount', "sTitle": 'Transaction Amount', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "160px"}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetPaymentRevenueDetails'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-com-payment-detail-table_paginate').hide();
                    }
                }
            });
        }
    });
}

/*************End report .COM paymenent details */

/*************Start report enterprise user details */
$('.show-enterprise-revenue').click(function(event){
    InitFilterEnterpriseUserDetailsTable();
    GetTotalEnterpriseUserDetailList();
    $("#total-enterprise-user-detail-modal").modal("show");
    $("#totalEnterpriseUserDetailFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#totalEnterpriseUserDetailFilterBox .btn-add-item").hide();
});

function InitFilterEnterpriseUserDetailsTable () {
    $("#totalEnterpriseUserDetailFilterBox").html('');
    //Filter bar initialize
    $('#totalEnterpriseUserDetailFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' u.MFAContactString_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalEnterpriseUserDetailList();
        }
    }, function(filterData){
        GetTotalEnterpriseUserDetailList(filterData);
    });
}
function GetTotalEnterpriseUserDetailList(strFilter){
    $("#total-enterprise-user-detail-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var costPerMessagesCol = function(object){
        return ("$ " + parseFloat(object.CRMPERMESSAGE).toFixed(3));
    }
     var amountCol = function(object){
        return ("$ " + parseFloat(object.CRMPERMESSAGE * object.TOTALMESSAGE).toFixed(3));
    }

    var table = $('#total-enterprise-user-detail-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "280px"},
            {"mData": "TOTALMESSAGE", "sName": 'Total Message', "sTitle": 'Total Message', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": costPerMessagesCol, "sName": 'Cost Per Message', "sTitle": 'Cost Per Message', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": amountCol, "sName": '', "sTitle": 'Amount', "bSortable": false, "sClass": "", "sWidth": "120px"}
            // {"mData": "CREDIT", "sName": '', "sTitle": 'Credits', "bSortable": false, "sClass": "", "sWidth": "120px"},
            // {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "160px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetEnterpriseUserDetails'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-enterprise-user-detail-table_paginate').hide();
                    }
                }
            });
        }
    });
}

/*************End report enterprise user details */

/************************************************************************End Total Enterprise Users Report*/

/*Report Total Failed MFA Table************************************************************************/
function GetFailedMFA(strFilter){
    $("#total-failed-mfa-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#total-failed-mfa-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "280px"},
            {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "160px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetFailedMFA'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-failed-mfa-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterFailedMFATable () {
    $("#failedMFAFilterBox").html('');
    //Filter bar initialize
    $('#failedMFAFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' u.MFAContactString_vch '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetFailedMFA();
        }
    }, function(filterData){
        GetFailedMFA(filterData);
    });
}

$('#total-failed-mfa').click(function(event){
    InitFilterFailedMFATable();
    GetFailedMFA();
    $("#total-failed-mfa-modal").modal("show");
    $("#failedMFAFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#failedMFAFilterBox .btn-add-item").hide();
});

/*************Start report enterprise billing details */
$('#enterprise-billing-numb').click(function(event){
    InitFilterEnterpriseBillingDetailsTable();
    GetTotalEnterpriseBillingDetailList();
    $("#enterprise-billing-detail-modal").modal("show");
    $("#EnterpriseBillingDetailFilterBox > .filter-item > div:nth-child(2)").hide();
    $("#EnterpriseBillingDetailFilterBox .btn-add-item").hide();
});

function InitFilterEnterpriseBillingDetailsTable () {
    $("#EnterpriseBillingDetailFilterBox").html('');
    //Filter bar initialize
    $('#EnterpriseBillingDetailFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' u.MFAContactString_vch '}
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetTotalEnterpriseBillingDetailList();
        }
    }, function(filterData){
        GetTotalEnterpriseBillingDetailList(filterData);
    });
}
function GetTotalEnterpriseBillingDetailList(strFilter){
    $("#enterprise-billing-detail-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var amountCol = function(object){
        return ("$ " + parseFloat(object.TOTALAMOUNT).toFixed(2));
    }
    var amountCostPerMOCol = function(object){
        if(object.COSTPERMOS === ""){
            object.COSTPERMOS = 0;
        }
        return ("$ " + parseFloat(object.COSTPERMOS).toFixed(2));
    }
    var amountCostPerMTCol = function(object){
        if(object.COSTPERMTS === ""){
            object.COSTPERMTS = 0;
        }
        return ("$ " + parseFloat(object.COSTPERMTS).toFixed(2));
    }

    var table = $('#enterprise-billing-detail-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "280px"},
            {"mData": "TOTALMOS", "sName": 'Total MOs', "sTitle": 'Total MOs', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": "TOTALMTS", "sName": 'Total MTs', "sTitle": 'Total MTs', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": amountCostPerMOCol, "sName": 'Cost Per MO', "sTitle": 'Cost Per MO', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": amountCostPerMTCol, "sName": 'Cost Per MT', "sTitle": 'Cost Per MT', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": amountCol, "sName": '', "sTitle": 'Amount', "bSortable": false, "sClass": "", "sWidth": "120px"}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetEnterpriseBillingDetail'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('System is timeout!','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#enterprise-billing-detail-table_paginate').hide();
                    }
                }
            });
        }
    });
}

/*************End report enterprise billing details */

/************************************************************************End Total Enterprise billing Report*/


$('body').on("click", "button.btn-re-dowload.total-failed-mfa-list", function(event){
    window.location.href="/session/sire/models/cfm/admin/admin-export-total-failed-mfa-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter;
});

/************************************************************************End Total Enterprise Users Report*/

$('body').on('click', '.preview-campaign',function(e) {
    e.preventDefault();
    var sourceUserId = $(this).data('userid');
    var selectedBatchId = $(this).data('batchid');
    if(selectedBatchId > 0){
        try{
            $.ajax({
                type: "GET",
                url: '/session/sire/models/cfm/template_preview.cfm?templateid='+selectedBatchId+'&templateFlag=0&userId='+sourceUserId,
                beforeSend: function( xhr ) {
                    $('#processingPayment').show();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message: "Get preview Fail",
                        title: "Campaigns",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                },
                success:function(d){

                    if(d.indexOf('class="home_page"') > -1){
                        return false;
                    }

                    $('#processingPayment').hide();
                    $('#previewCampaignModal .modal-body').html(d);
                    $('#previewCampaignModal').modal('show');
                }
            });
        }catch(ex){
            $('#processingPayment').hide();
            bootbox.dialog({
                message: "Get preview Fail",
                title: "Campaigns",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
        }
    }
    else{
        bootbox.dialog({
            message: "Invalid batchId",
            title: 'Preview Campaign',
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        });
    }
});

function GetUserSummarySource(){
    $(".source-loop").html('');
    $.ajax({
        url: "/session/sire/models/cfc/admin.cfc?method=getUserSourceSummary"+strAjaxQuery,
        type: "POST",
        dataType: "json",
        asynce: false,
        data: {
            inpStartDate: startTime,
            inpEndDate: endTime
        },
        beforeSend: function(xhr){
            $("#processingPayment").show();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('#processingPayment').hide();
        },
        success: function(data){

            var stringHTML= "";
            if(data.RXRESULTCODE==1){


                $(".source-section .ttFree").text(data.TOTALFREE);
                $(".source-section .ttPaid").text(data.TOTALPAID);
                $(".source-section .ttFreeNoMFA").text(data.TOTALFREENOMFA);
                $(".source-section .ttFreeYesMFA").text(data.TOTALFREEYESMFA);
                $(".source-section .ttFreeBuyCredit").text(data.TOTALFREEBUYCREDITS);
                if(data.DATALIST.length > 0){
                    for (var i = 0; i < data.DATALIST.length ; i++) {
                        var temp= data.DATALIST[i];
                        stringHTML+='<div class="col-xs-12 source-item" data-source="'+temp.MARKETINGSOURCE+'" data-is-affiliate-source="'+temp.ISAFFILIATESOURCE+'"  >';
                            stringHTML+='<div class="portlet light bordered ">';
                                stringHTML+='<div class="form">';
                                    stringHTML+='<div class="admin-dashboard-block-title blue-text">';
                                    stringHTML+=temp.MARKETINGSOURCE+': <span >'+temp.TOTAL+'</span>';
                                    stringHTML+='</div>';
                                stringHTML+='</div>';
                            stringHTML+='</div>';
                        stringHTML+='</div>';


                    }
                }
                $(".source-loop").append(stringHTML);
            }
            $("#processingPayment").hide();
        }
    });
}
function GetUserListBySource(strFilter,source,isAffiliateSource){
    $("#userSourceTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#userSourceTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "SOURCE", "sName": 'Source', "sTitle": 'Source', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=getUserListBySource'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpSource", "value": source}
            );
            aoData.push(
                { "name": "inpIsAffiliateSource", "value": isAffiliateSource}
            );
            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('Unable to get your infomation at this time. An error occurred.','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#userSourceTable_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterForUserSourceTable (source,isAffiliateSource) {
    $("#userSourceFilterBox").html('');
    //Filter bar initialize
    $('#userSourceFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetUserListBySource([],source,isAffiliateSource);
        }
    }, function(filterData){
        GetUserListBySource(filterData,source,isAffiliateSource);
    });
}
$(document).on("click",".source-item",function(){


    var source= $(this).data("source");
    var isAffiliateSource= $(this) .data("is-affiliate-source");
    $("#user-source-modal").find("#user-source-modal-title").text(source);
    $("#user-source-modal").modal("show");
    InitFilterForUserSourceTable(source,isAffiliateSource);
    GetUserListBySource([],source,isAffiliateSource);

    exportPath = "admin-export-list-user-by-source.cfm";
    exp_user_source =source;
    exp_user_isaffiliatesource=isAffiliateSource;
});

function GetUserListByType(strFilter,type){
    $("#userTypeTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

    var table = $('#userTypeTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "SOURCE", "sName": 'Source', "sTitle": 'Source', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=getUserListByType'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

            aoData.push(
                { "name": "inpType", "value": type}
            );
            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );

            filter = customFilterData;

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#processingPayment").hide();
                    alertBox('Unable to get your infomation at this time. An error occurred.','Oops!','');
                },
                "success": function(data) {
                    $("#processingPayment").hide();
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#userTypeTable_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterForUserTypeTable (type) {
    $("#userTypeFilterBox").html('');
    //Filter bar initialize
    $('#userTypeFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){
            GetUserListByType([],type);
        }
    }, function(filterData){
        GetUserListByType(filterData,type);
    });
}


$(document).on("click",".type-item",function(){

    var utype= $(this).data("type");
    var mdTitle="";
    if(utype=="free")
    {
        mdTitle="Free accounts";
    }
    else if(utype=="paid"){
        mdTitle="Paid account";
    }
    else if(utype=="freebuycredits"){
        mdTitle="Free accounts that have bought credits";
    }
    else if(utype=="freenomfa"){
        mdTitle="Free account that have not MFA yet";
    }
    else if(utype=="freeyesmfa"){
        mdTitle="Free accounts that have MFA already";
    }

    $("#user-type-modal").find("#user-type-modal-title").text(mdTitle);
    $("#user-type-modal").modal("show");
    InitFilterForUserTypeTable(utype);
    GetUserListByType([],utype);

    exportPath = "admin-export-list-user-by-type.cfm";
    exp_user_type=utype;
});
$(document).ready(function() {
    GetReport(start, end);
    $('#refresh-data-crm').on('click', function(event){
        $('#processingPayment').show();
        GetReport(dateStart_crm, dateEnd_crm, 1);
    });
});

})();
