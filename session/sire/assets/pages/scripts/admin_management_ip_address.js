// A $( document ).ready() block.
$( document ).ready(function() {	
	// Process List IP Address by user id when open with param url
    var userid = getUrlParameter('userid');
    $('.input-box input').val(userid);    
    $('.glyphicon-search').click();	    
});

// Function get param from url
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [					
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},          
			{DISPLAY_NAME: 'IP Address', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' pkw.IP_vch '}			
		],
		clearButton: true,	
		error: function(ele){
			console.log(ele);
		},
		
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {              	        	
			var strReturn = '<a href="" IP ="'+object.IP+'" class="view-details-ip-address" title="Admin View Details IP Address">'+object.IP+'</a>';
            return strReturn;
        }     
         var actionUserId = function (object) {              	        	
			var strReturn = '<a href="" user-id ="'+object.USERID+'" class="view-top10-ip-address" title="View top 10 IP Address">'+object.USERID+'</a>';
            return strReturn;
        }     
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
		    	{"mData": actionBtn, "sName": 'pkw.IP_vch', "sTitle": 'IP Address', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				{"mData": actionUserId, "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": false},				
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '16%',"bSortable": false},              
				{"mData": "TIMESTAMP", "sName": 'pkw.Timestamp_dt', "sTitle": 'Date', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},				               
                {"mData": 'MODULENAME', "sName": 'pkw.ModuleName_vch', "sTitle": 'Module Name', "bSortable": false, "sClass": "col-center", "sWidth": "13%"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/public/sire/models/cfc/userstools.cfc?method=ManagementIPAddressList'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
	       	//ManagementIPAddressList
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data				
		       aoData.push(
		            {  
		            	"name": "customFilter", 
		            	"value": customFilterData
		        	}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	


	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    // View details IP Address
    $("#tblListEMS").on("click", ".view-details-ip-address", function(event){    	
        event.preventDefault();        
        // Clear on form first        
        $('#txtIP').text('');                    
	    $('#txtCountryCode').text('');
        $('#txtCountryName').text('');
        $('#txtRegionCode').text('');

        $('#txtRegionName').text('');
        $('#txtCity').text('');
        $('#txtZipCode').text('');

        $('#txtTimeZone').text('');        
        $('#txtLatitude').text('');
        $('#txtLongitude').text('');

		$('#txtMetroCode').text('');	
		
        //inital data on modal      
        var ipAddress = $(this).attr('IP');        
		//UIkit.modal('#modal-ip-details-sections')[0].toggle();
		$("#modal-ip-details-sections").modal("show");
			
		var url = 'https://json.geoiplookup.io/'+ipAddress;		
        // Inital Data on form
        $.getJSON(url, function( data ) {        							
			$('#txtIP').text(data.ip);                    
		    $('#txtCountryCode').text(data.country_code);
	        $('#txtCountryName').text(data.country_name);
	        $('#txtRegionCode').text(data.region);

	        $('#txtRegionName').text(data.region);
	        $('#txtCity').text(data.city);
	        $('#txtZipCode').text(data.postal_code);

	        $('#txtTimeZone').text(data.timezone_name);        
	        $('#txtLatitude').text(data.latitude);
	        $('#txtLongitude').text(data.longitude);

			$('#txtMetroCode').text(data.metro_code);	
		});		        
    });   


    // View Top 10 IP Address
    $("#tblListEMS").on("click", ".view-top10-ip-address", function(event){    	
        event.preventDefault();         
        //inital data on modal      
        var userId = $(this).attr('user-id');        
		//UIkit.modal('#modal-top10-ip-sections')[0].toggle();
		$("#modal-top10-ip-sections").modal("show");

		var actionBtn = function (object) {              	        	
			var strReturn = '<a href="" IP ="'+object.IP+'" class="view-details-ip-address" title="Admin View Details IP Address">'+object.IP+'</a>';
            return strReturn;
        }            
		//init datatable for active agent
		$('#tbTop10IPAddress').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			// "sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
		    	{"mData": actionBtn, "sName": 'pkw.IP_vch', "sTitle": 'IP Address', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": false},				
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '16%',"bSortable": false},              
				{"mData": "TIMESTAMP", "sName": 'pkw.Timestamp_dt', "sTitle": 'Date', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},				               
                {"mData": 'MODULENAME', "sName": 'pkw.ModuleName_vch', "sTitle": 'Module Name', "bSortable": false, "sClass": "col-center", "sWidth": "13%"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/public/sire/models/cfc/userstools.cfc?method=TopTenIPAddressList'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},	       	
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
				paginateBar.hide();
		  // 		if (oSettings._iDisplayStart < 0){
				// 	oSettings._iDisplayStart = 0;
				// 	paginateBar.find('input.paginate_text').val("1");
				// }
				// if(oSettings._iRecordsTotal < 1){
				// 	paginateBar.hide();
				// } else {
				// 	paginateBar.show();
				// }
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data				
		       aoData.push(
		            {  
		            	"name": "UserIdstatus", 
		            	"value": userId
		        	}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tbTop10IPAddress thead tr").find(':last-child').css("border-right","0px");
			}
	    });	

    });   
     // View details IP Address From Popup
    $("#tbTop10IPAddress").on("click", ".view-details-ip-address", function(event){    	
        event.preventDefault();        
        // Clear on form first        
        $('#txtIP').text('');                    
	    $('#txtCountryCode').text('');
        $('#txtCountryName').text('');
        $('#txtRegionCode').text('');

        $('#txtRegionName').text('');
        $('#txtCity').text('');
        $('#txtZipCode').text('');

        $('#txtTimeZone').text('');        
        $('#txtLatitude').text('');
        $('#txtLongitude').text('');

		$('#txtMetroCode').text('');	
		
		
        //inital data on modal      
        var ipAddress = $(this).attr('IP');        
		//UIkit.modal('#modal-ip-details-sections')[0].toggle();	
		
		$("#modal-ip-details-sections").modal("show");


		var url = 'https://json.geoiplookup.io/'+ipAddress;		
        // Inital Data on form
        $.getJSON(url, function( data ) {        
			
			$('#txtIP').text(data.ip);                    
		    $('#txtCountryCode').text(data.country_code);
	        $('#txtCountryName').text(data.country_name);
	        $('#txtRegionCode').text(data.region);

	        $('#txtRegionName').text(data.region);
	        $('#txtCity').text(data.city);
	        $('#txtZipCode').text(data.postal_code);

	        $('#txtTimeZone').text(data.timezone_name);        
	        $('#txtLatitude').text(data.latitude);
	        $('#txtLongitude').text(data.longitude);

			$('#txtMetroCode').text(data.metro_code);	
		});		        
    });   


})(jQuery);