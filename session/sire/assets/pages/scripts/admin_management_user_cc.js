function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var StatusCustomData = function(){
	var options   = '<option value= "-100" selected> </option>'+
					'<option value="1">WorldPay</option>'+
					'<option value="2">PayPal</option>'+
					'<option value="3">Mojo</option>'+
					'<option value="4">Total Apps</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [					
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},          
			{DISPLAY_NAME: 'Payment Gateway', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' pkw.paymentGateway_ti ',CUSTOM_DATA:StatusCustomData},
			{DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '}
		],
		clearButton: true,	
		error: function(ele){			
		},
		
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {              	        	
			var strReturn = '<a href="" id ="'+object.ID+'" email-data="'+object.EMAIL+'" user-id="'+object.USERID+'" gateway-id = "'+object.PAYMENTGATEWAYTI+'" data-action="update-cc" class="update-user-cc-info" title="Admin update user cc"><label id="extraNote-'+object.ID+'" style="display:none">'+object.EXTRANOTE+'</label><i class="fa fa-pencil-square-o subscriber-detail-icon" aria-hidden="true" ></i></a>';
			if(object.DEFAULT_GW==object.PAYMENTGATEWAYTI){
				strReturn += ' |&nbsp;&nbsp;<a href="#" id ="'+object.ID+'" user-id="'+object.USERID+'" gateway-id = "'+object.PAYMENTGATEWAYTI+'" data-action="charge-cc" class="update-user-cc-info" title="Admin charge user cc"><i class="fa fa-usd" aria-hidden="true"></i></a>';
			}		
			strReturn += ' |&nbsp;&nbsp;<a href="#" id ="'+object.ID+'" user-id="'+object.USERID+'" gateway-id = "'+object.PAYMENTGATEWAYTI+'" data-action="report-charge" class="report-charge" title="Report charge user cc"><i class="fa fa-bar-chart" aria-hidden="true"></i></a>';
					
			
			strReturn += ' |&nbsp;&nbsp;<a href="#" id ="'+object.ID+'" user-id="'+object.USERID+'" gateway-id = "'+object.PAYMENTGATEWAYTI+'" data-action="view-full-cc" class="view-full-cc" title="View full user CC information"><i class="fa fa-address-card-o" aria-hidden="true"></i></a>';	
			strReturn += ' |&nbsp;&nbsp;<a href="#" user-id="'+object.USERID+'" gateway-id = "'+object.PAYMENTGATEWAYTI+'" data-action="delete-user-cc" class="delete-user-cc" title="Delete user CC"><i class="fa fa-remove" aria-hidden="true"></i></a>';	

            return strReturn;
        }     
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": false},				
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '17%',"bSortable": false},              				
				{"mData": "STATUS", "sName": 'pkw.PaymentMethodID_vch', "sTitle": 'Payment Gateway', "sWidth": '12%',"bSortable": false,"sClass": "col-center"},
				//{"mData": "EXTRANOTE", "sName": 'extraNote', "sTitle": 'Extra Note', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
				{"mData": "UPDATEDDATE", "sName": 'pkw.Updated_dt', "sTitle": 'Updated Date', "sWidth": '12%',"bSortable": true,"sClass": "col-center"},				               
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "12%"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=ManagementUserCCList'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data				
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	


	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    $("#tblListEMS").on("click", ".update-user-cc-info", function(event){    	
		event.preventDefault();        		
		// Clear on form first		        

        $('#card_number').val('');                    
	    $('#expiration_date_month_upgrade').val('');
        $('#expiration_date_year_upgrade').val('');
        $('#security_code').val('');
        $('#first_name').val('');
        $('#last_name').val('');
        $('#line1').val('');
        $('#city').val('');
        //$('#country').val('');
        $('#zip').val('');
        $('#state').val('');
		$('#email').val('');
		//
		$("#txtMaskedNumber").text('');
		$("#txtExpirationDate").text('');
		$("#txtCardholderName").text('');
		$("#txtAddress").text('');
		$("#txtCity").text('');
		$("#txtState").text('');
		$("#txtZipCode").text('');
		$("#txtCountry").text('');		
		
        //inital data on modal
        var authorizeUserId = $(this).attr('id');
        var email = $(this).attr('email-data');
        var userId = $(this).attr('user-id');
        var extraNote = $("#extraNote-"+authorizeUserId).text(); 
		var gatewayId = $(this).attr('gateway-id');        
		var action= $(this).data("action");		
		$("#paymentGateway").val(gatewayId);	
		
		$("#action").val(action);

		var select_used_card= 1;		
		$(".select_used_card").each(function(){			
			if($(this).is(":checked")){
				select_used_card= $(this).val();				
			}			
		});
		if(select_used_card==1){
			$(".fset_cardholder_info").addClass("hidden");
		}
		else
		{
			$(".fset_cardholder_info").removeClass("hidden");
		}		
		
		$(".select_gw").each(function(){													
			if ($(this).val()==gatewayId) {
				$(this).prop('checked',true).trigger("click");
			}
		});
		
		if(action=="update-cc"){
			$(".fset_cardholder_info").removeClass("hidden");
			$("#buyaddon-modal-sections").find(".uk-new-modal-title").text("Admin Update User CC");	
			$("#buyaddon-modal-sections").find(".btn-update-user-cc").text("Save");	
			$("#buyaddon-modal-sections").find(".amount-row").hide();				
			$("#buyaddon-modal-sections").find(".select-user-card-row").hide();				
			$("#amount").removeClass("validate[required,min[1],custom[number]]");	
			$(".select-gateway-row").show();				
			$(".save-cc-row	").addClass("hidden");
					
		}
		else if(action=="charge-cc"){
			$("#buyaddon-modal-sections").find(".uk-new-modal-title").text("Admin Charge User CC");		
			$("#buyaddon-modal-sections").find(".btn-update-user-cc").text("Charge");		
			$("#buyaddon-modal-sections").find(".amount-row").show();	
			$("#buyaddon-modal-sections").find(".select-user-card-row").show();				
			$("#amount").addClass("validate[required,min[1],custom[number]]");
			$(".select-gateway-row").hide();
			$(".save-cc-row	").removeClass("hidden");
		}
		
		
		//update-cc
		UIkit.modal('#buyaddon-modal-sections')[0].toggle();		
        // Inital Data on form
		if(extraNote != ""){
			var obj = jQuery.parseJSON(extraNote);				
        	$("#txtMaskedNumber").text(obj.INPNUMBER);
			$("#txtExpirationDate").text(obj.INPEXPIREMONTH +'/'+ obj.INPEXPREYEAR);
			$("#txtCardholderName").text(obj.INPFIRSTNAME + ' ' + obj.INPLASTNAME);
			$("#txtAddress").text(obj.INPLINE1);
			$("#txtCity").text(obj.INPCITY);
			$("#txtState").text(obj.INPSTATE);
			$("#txtZipCode").text(obj.INPPOSTALCODE);
			$("#txtCountry").text(obj.INPCOUNTRYCODE);			
        }
        $("#txtEmail").text(email);   
        $("#authorizedUserId").val(authorizeUserId);
		$("#selected-user-id").val(userId);		       
    });   

	$("#addon-form").find('#expiration_date_month_upgrade, #expiration_date_year_upgrade').change(function(){
        $('#addon-form').find('#expiration_date_upgrade').val($('#expiration_date_month_upgrade').val() + '/' + $('#expiration_date_year_upgrade').val());						
    });
	// Run charge the payment after approve
	$("#addon-form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$(".select_used_card").on("click",function(){
		var select_used_card= 1;		
		$(".select_used_card").each(function(){
			if($(this).is(":checked")){
				select_used_card= $(this).val();				
			}			
		});
		if(select_used_card==1){
			$(".fset_cardholder_info").addClass("hidden");
		}
		else
		{
			$(".fset_cardholder_info").removeClass("hidden");
		}		
	});
    // Admin update user cc
	$('#addon-form').submit(function(event){		
		event.preventDefault();				
		if($("#addon-form").validationEngine('validate')){
			var self = $("#addon-form");	
			var selectedUserId =  self.find('#selected-user-id').val();		
			var select_used_card= 1;
			$(".select_used_card").each(function(){
				if($(this).is(":checked")){
					select_used_card= $(this).val();				
				}			
			});
			var gatewayId=$("#paymentGateway").val();
			var cardObject = {				
				inpNumber: $('#card_number').val(),
				inpType: 'visa',
				inpExpireMonth: $('#expiration_date_month_upgrade').val(),
				inpExpreYear: $('#expiration_date_year_upgrade').val(),
				expirationDate:$('#expiration_date_month_upgrade').val() +"/"+  $('#expiration_date_year_upgrade').val(),
				inpCvv2: $('#security_code').val(),
				inpFirstName: $('#first_name').val(),
				inpLastName: $('#last_name').val(),
				inpLine1: $('#line1').val(),
				inpCity: $('#city').val(),
				inpCountryCode: $('#country').val(),
				inpPostalCode: $('#zip').val(),
				inpState: $('#state').val(),
				inpEmail: $('#email').val(),
				phone: '',
				inpPhone:'',
				paymentGateway:$("#paymentGateway").val(),
				select_used_card:select_used_card
			}	               			
			
			var updateForGatewayId=0;
			$(".select_gw").each(function(){
				if($(this).is(":checked")){
					updateForGatewayId= $(this).val();				
				}			
			});
			
			var action= $("#action").val();
			if(action=="update-cc"){				
				// Call update user cc
				$.ajax({
					url: '/session/sire/models/cfc/payment/vault.cfc?method=storeCreditCardInVault&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					async: true,
					type: 'POST',
					dataType: 'json',
					data: {
						inpCardObject: JSON.stringify(cardObject),
						inpPaymentGateway: updateForGatewayId,
						inpUserId: selectedUserId
					},
					beforeSend: function(){
						$('#processingPayment').show();									
					},
					error: function(jqXHR, textStatus, errorThrown) {									
						$("#processingPayment").hide();
						alertBox('Action Error!','Admin Update User CC',''); 									
					},
					success: function(data) {									
						if (data.RXRESULTCODE == 1) {									
							$("#processingPayment").hide();																	
							//$("#buyaddon-modal-sections").hide();
							$(".btn-cancel-cc").click();
							alertBox("Update User CC Successfully",'Admin Update User CC','');
							InitControl();
						} else {
							$("#processingPayment").hide();					
							alertBox(data.MESSAGE,'Admin Update User CC','');
						}                       
					}
				});							
			}
			else if(action=="charge-cc"){
				var save_cc_info=0;
				if($("#save_cc_info").is(":checked")){
					save_cc_info=1;
				}
				// Call charge
				$.ajax({
					url: '/session/sire/models/cfc/billing.cfc?method=AdminMakePayment&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					async: true,
					type: 'POST',
					dataType: 'json',
					data: {
						inpPaymentdata: JSON.stringify(cardObject),
						inpPaymentGateway: gatewayId,
						inpUserId: selectedUserId,
						inpSelectUsedCard:select_used_card,
						inpOrderAmount:$("#amount").val() || 0,
						inpSaveCCInfo:save_cc_info
					},
					beforeSend: function(){
						$('#processingPayment').show();									
					},
					error: function(jqXHR, textStatus, errorThrown) {									
						$("#processingPayment").hide();
						alertBox('Action Error!','Admin Charge User CC',''); 									
					},
					success: function(data) {									
						if (data.RXRESULTCODE == 1) {									
							$("#processingPayment").hide();																	
							//$("#buyaddon-modal-sections").hide();
							$(".btn-cancel-cc").click();
							alertBox("Charge User CC Successfully",'Admin Charge User CC','');
							InitControl();
						} else {
							$("#processingPayment").hide();					
							alertBox(data.MESSAGE,'Admin Charge User CC','');
						}                       
					}
				});	
			}
			
		}   
	});	
	$(document).on("click",".report-charge",function(){
		var userId=$(this).attr("user-id");
		GetPaymentTransactionListByCC(userId);
		$("#mdTransaction").modal("show");
	});
	
	$(document).on("click", ".view-full-cc", function(event){    	
		event.preventDefault();        		
		// Clear on form first		       		
		var userId=$(this).attr("user-id");
		var gatewayId=$(this).attr("gateway-id");								
		GetFullUserCCData(gatewayId,userId);	      		
    });  
    $(document).on("click", ".delete-user-cc", function(event){    	
		event.preventDefault();        				    	
		var userId=$(this).attr("user-id");								
		var gatewayId=$(this).attr("gateway-id");	
		// action delete this payment				
		if(userId > 0){
				confirmBox('Are you sure you want to remove this user CC?', 'CONFIRM!', function(){
			$.ajax({
				url: '/session/sire/models/cfc/billing.cfc?method=AdminRemoveUserCC&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				async: true,
				type: 'POST',
				dataType: 'json',
				data: {
					inpUserID: userId,
					inpPaymentGateway: gatewayId			
				},
				beforeSend: function(){
					$('#processingPayment').show();									
				},
				error: function(jqXHR, textStatus, errorThrown) {									
					$("#processingPayment").hide();
					alertBox('Action Error!','Remove User CC',''); 									
				},
				success: function(data) {					
					if (data.RXRESULTCODE == 1) {									
						$("#processingPayment").hide();																						
						alertBox("Remove User CC Successfully",'Remove User CC','');
						InitControl();
					} else {
						$("#processingPayment").hide();					
						alertBox(data.MESSAGE,'Remove User CC','');
					}                       
				}
			});	
		});	
		}		
    });  

	var GetFullUserCCData = function(gatewayId, userId){
		// Clear form view first	
		$("#txtMaskedNumber1").text('');
		$("#txtExpirationDate1").text('');
		$("#txtCardholderName1").text('');
		$("#txtAddress1").text('');
		$("#txtCity1").text('');
		$("#txtState1").text('');
		$("#txtZipCode1").text('');
		$("#txtCountry1").text('');			
		$("#txtPhone1").text('');
		$.ajax({
			url: '/session/sire/models/cfc/users.cfc?method=getUserCCData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			async: true,
			type: 'POST',
			dataType: 'json',
			data: {				
				inpUserID: userId,	
				inpPaymentGateway: gatewayId				
			},
			beforeSend: function(){
				//$('#processingPayment').show();									
			},
			error: function(jqXHR, textStatus, errorThrown) {									
				//$("#processingPayment").hide();				
			},
			success: function(data) {		
				//$("#processingPayment").hide();						          						
				if (data.RXRESULTCODE == 1) {																									
					$("#txtMaskedNumber1").text(data.USERCCATA.cardNumber);
					$("#txtExpirationDate1").text(data.USERCCATA.cardExpirationMonth + "/" + data.USERCCATA.cardExpirationYear);
					$("#txtCardholderName1").text(data.USERCCATA.firstName + " " + data.USERCCATA.lastName); 
					$("#txtCardCVV1").text(data.USERCCATA.cardCVV);

					$("#txtAddress1").text(data.USERCCATA.addressLine1);
					$("#txtCity1").text(data.USERCCATA.city);
					$("#txtState1").text(data.USERCCATA.state);

					$("#txtZipCode1").text(data.USERCCATA.zipCode);
					$("#txtCountry1").text(data.USERCCATA.country);			
					$("#txtPhone1").text(data.USERCCATA.phone);
					$("#txtEmail1").text(data.USERCCATA.email);

				} else {					
					$("#txtMaskedNumber1").text('');
					$("#txtExpirationDate1").text('');
					$("#txtCardholderName1").text(''); 
					$("#txtCardCVV1").text('');

					$("#txtAddress1").text('');
					$("#txtCity1").text('');
					$("#txtState1").text('');

					$("#txtZipCode1").text('');
					$("#txtCountry1").text('');			
					$("#txtPhone1").text('');
					$("#txtEmail1").text('');
				} 
				UIkit.modal('#modal-full-cc-sections')[0].toggle();	        
			}
		});					
	};

	var GetPaymentTransactionListByCC = function(userId,customFilterData){
		customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   

		var viewJsonCol = function(object){
			var strReturn = "";
				strReturn = $("<div class='btn-re-edit' data-id='" +object.ID+ "' data-json='"+object.JSONCONTENT+"'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></div>")[0].outerHTML;
			return strReturn;
		}
		var colGwText = function(object){
			var strReturn = "";
			if(object.GATEWAY==1){
				strReturn="WorldPay";
			}
			else if(object.GATEWAY==2){
				strReturn="PayPal";
			}
			else if(object.GATEWAY==3){
				strReturn="Mojo";
			}
			else if(object.GATEWAY==4){
				strReturn="TotalApps";
			}
			return strReturn;
		}

		var viewTransIdCol = function(object){
			var strReturn = "";
			if(object.TRANSID != ''){
				strReturn = $("<a target='_blank' href='/session/sire/pages/admin-payment-transactions-detail.cfm?transId="+object.TRANSID+"'>"+object.TRANSID+"</a>")[0].outerHTML;
			}else{
				strReturn = $("<spam>"+object.TRANSID+"</span>")[0].outerHTML;
			}
			return strReturn;
		}

		$('#transactions-detail').html("").addClass('hidden');
		$('#transactions-list_wrapper').removeClass('hidden');

		var table = $('#transactions-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
		    "aoColumns": [
		    	// {"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id","sWidth":"100px"},
		    	{"mData": viewTransIdCol, "sName": '', "sTitle": 'Transaction ID',"bSortable": false,"sClass":"","sWidth":"210px"},				
				{"mData": "MODULE", "sName": '', "sTitle": 'Action',"bSortable": false,"sClass":"", "sWidth":"250px"},
				{"mData": "CREATED", "sName": '', "sTitle": 'Created',"bSortable": true,"sClass":"","sWidth":"130px"},
				{"mData": "VAUTID", "sName": '', "sTitle": 'VAUTID',"bSortable": false,"sClass":"","sWidth":"130px"},
				
				{"mData": "AMOUNT", "sName": '', "sTitle": 'Amount',"bSortable": false,"sClass":"","sWidth":"90px"},
				{"mData": "STATUS", "sName": '', "sTitle": 'Status',"bSortable": false,"sClass":"","sWidth": "130px"},				
				{"mData": colGwText, "sName": '', "sTitle": 'Gateway',"bSortable": true,"sClass":"","sWidth": "100px"},				
				{"mData": viewJsonCol, "sName": '', "sTitle": 'Response',"bSortable": false,"sClass":"json-content-td","sWidth":"100px"},
				
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetPaymentTransactionListByAdminAction'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
		     "fnFooterCallback": function(nRow){
		    },
		    "fnHeaderCallback": function(nRow){
		    	$(nRow).find('th.selection').html('<i class="select-all glyphicon glyphicon-ok"></i>');
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {
				
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
					{ 
						"name": "customFilter", "value": customFilterData,
						"name": "inpUserId", "value": userId
					}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
				$('#transactions-list_wrapper').removeClass('hidden');
			}
		});

		return function(){
			table.fnStandingRedraw();
		}

	};
	$('body').on('click', '.btn-re-edit', function(){
		var formatStatus = function(object){  
			object=JSON.stringify(object);
			result = String(object).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;'); 
			return result;
		}
		bootbox.dialog({
			message: '<h4 class="be-modal-title">'+ 'JSON Content' +'</h4><p style="word-wrap: break-word">'+ formatStatus($(this).data('json')) +'</p>',
			title: '&nbsp;',
			className: "be-modal",
			buttons: {
				success: {
					label: "OK",
					className: "green-gd",
					callback: function(){
					}
				},
			}
		});

		/* bootbox.dialog({
	        message: '<h4 class="be-modal-title">'+ 'JSON Content' +'</h4><p style="word-wrap: break-word">'+ JSON.stringify($(this).data('json')) +'</p>',
	        title: '&nbsp;',
	        className: "be-modal",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "green-gd",
	                callback: function(){
	                }
	            },
	        }
	    }); */
	});
})(jQuery);