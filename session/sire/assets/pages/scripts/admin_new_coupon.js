(function() {
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    $("#promo-new-startdate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    $("#promo-new-enddate").datepicker({
			    				inline: true,
								calendars: 4,
								extraWidth: 100,
								dateFormat: 'yy-mm-dd' 
			    			}).attr('type', 'text');

    jQuery(document).ready(function($) {
		$('.discount-option-detail2').prop('disabled', true);
		$('.discount-option-detail1').prop('disabled', true);
		$('#invitation-list').prop('disabled', true);
		$('#number-of-renewals').prop('disabled', true);
		$('#delete-coupon').hide();
    });
    
    $('#discount-no').on('click', function(){
    	$('.discount-option-detail1').val('');
    	$('.discount-option-detail2').val('');
		$('.discount-option-detail2').prop('disabled', true);
		$('.discount-option-detail1').prop('disabled', true);
		$('#discount-plan').parents('.plan-price-discount').find('.discount-option-checkbox').prop('checked', false);
		$('#discount-other').parents('.other-discount').find('.discount-option-checkbox').prop('checked', false);
	});

	$('#discount-plan').on('click', function(){
		$('.discount-option-detail2').val('');
		$('.discount-option-detail2').prop('disabled', true);
		$('.discount-option-detail1').prop('disabled', false);
		// $(this).parents('.plan-price-discount').find('.discount-option-checkbox').prop('disabled', false);
		$(this).parents('.plan-price-discount').find('.discount-option-value').prop('disabled', true);
		$('#discount-other').parents('.other-discount').find('.discount-option-checkbox').prop('checked', false);
	});

	$('#discount-other').on('click', function(){
		$('.discount-option-detail1').val('');
		$('.discount-option-detail1').prop('disabled', true);
		$('.discount-option-detail2').prop('disabled', false);
		// $(this).parents('.other-discount').find('.discount-option-checkbox').prop('disabled', false);
		$(this).parents('.other-discount').find('.discount-option-value').prop('disabled', true);
		$('#discount-plan').parents('.plan-price-discount').find('.discount-option-checkbox').prop('checked', false);
	});




	function EnableInputWhenCheckboxChecked(smth){
		$('#discount-plan-'+smth).on('click', function(){
			$('#add-new-coupon-form').validationEngine('hide');
			if($('#discount-plan-'+smth).is(':checked')){
				$('#discount-plan-'+smth+'-value').prop('disabled', false);
				$('#discount-plan-'+smth+'-value').addClass('validate[required]');
			}
			else{
				$('#discount-plan-'+smth+'-value').prop('disabled', true);
				if($('#discount-plan-'+smth+'-value').hasClass('validate[required]')){
					$('#discount-plan-'+smth+'-value').removeClass('validate[required]');
				}
			}
		});
	}


	$('#check-invitation').on('click', function(){
		$('#add-new-coupon-form').validationEngine('hide');
		if($('#check-invitation').is(':checked')){
			$('#invitation-list').prop('disabled', false);
			$('#invitation-list').addClass('validate[required,custom[noHTML]]');
		}
		else{
			$('#invitation-list').prop('disabled', true);
		}
	});

	$('#recurring-type-3').on('click', function(){
		if($('#recurring-type-3').is(':checked')){
			$('#number-of-renewals').prop('disabled', false);
			$('#number-of-renewals').addClass('validate[required,custom[onlyNumberNotZero]]');
		}
		else{
			$('#number-of-renewals').prop('disabled', true);
		}
	});

	$('#recurring-type-1,#recurring-type-2').on('click',function(){
		$('#number-of-renewals').prop('disabled', true);
	});

	$('#discount-plan-percentage').on('click', function(){
		$('#discount-plan-flat-rate-value').prop('disabled', true);
	});

	$('#discount-plan-flat-rate').on('click', function(){
		$('#discount-plan-percentage-value').prop('disabled', true);
	});
	
	EnableInputWhenCheckboxChecked('percentage');
	EnableInputWhenCheckboxChecked('flat-rate');
	EnableInputWhenCheckboxChecked('credit');
	EnableInputWhenCheckboxChecked('keyword');
	EnableInputWhenCheckboxChecked('mlp');
	EnableInputWhenCheckboxChecked('shorturl');

	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

	var couponToEdit = getUrlParameter('edit-coupon-id');

	var viewCouponDetails = getUrlParameter('coupon-details-id');

	$(document).ready(function($) {
		$('#coupon-id').val(viewCouponDetails);
	});

	var alertBoxTitle = typeof(viewCouponDetails) == "undefined" ? 'ADD NEW COUPON' : 'EDIT COUPON';

	var toDay = $.datepicker.formatDate('yy-mm-dd', new Date());

	function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

	function validateListEmail(emailString) {
	 	var listEmail = emailString.replace(/ /g,'').split(",");

	 	for (var i = 0; i < listEmail.length; i++) {
	  		if (!validateEmail(listEmail[i])) {
	   			return false;
	  		}
	 	}

	 	return true;
	}

    $('#add-new-coupon-form').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topLeft"});

    $('#add-new-coupon-form').on('submit', function(event){
    	event.preventDefault();
    	if($('#add-new-coupon-form').validationEngine('validate')){
    		var discountType;
			$('.discount-type').each(function(){
				if($(this).is(':checked')){
					discountType = $(this).val();
				}
			});

			var recurringType;
			var recurringTime;
			$('.recurring-type').each(function(){
				if($(this).is(':checked')){
					recurringType = $(this).val();
				}
			});

			if(recurringType == 1){
				recurringTime = 0;
			}
			else if(recurringType == 2){
				recurringTime = -1;
			}
			else{
				recurringTime = $('#number-of-renewals').val();
			}

    		var couponName = $('#coupon-name').val();
    		var couponCode = $('#coupon-code').val();
    		var couponDesc = $('#coupon-desc').val();
    		var couponStatus = $('#coupon-available').is(':checked') ? 1 : 0;
    		var limitcb1 = $('#check-combined').is(':checked') ? 1 : 0;
    		var limitcb2 = $('#check-individual').is(':checked') ? 1 : 0;
    		var invitationOnly = $('#check-invitation').is(':checked') ? 1 : 0;
    		var couponDiscountType = discountType;

    		var discountPricePercent = ($('#discount-plan-percentage').is(':checked') && discountType == 1) ? $('#discount-plan-percentage-value').val() : '';
    		var discountPriceFlatRate = ($('#discount-plan-flat-rate').is(':checked') && discountType == 1) ? $('#discount-plan-flat-rate-value').val() : '';
    		var couponCredit = ($('#discount-plan-credit').is(':checked') && discountType == 2) ? $('#discount-plan-credit-value').val() : '';
    		var couponKeyword = ($('#discount-plan-keyword').is(':checked') && discountType == 2) ? $('#discount-plan-keyword-value').val() : '';
    		var couponMLP = ($('#discount-plan-mlp').is(':checked') && discountType == 2) ? $('#discount-plan-mlp-value').val() : '';
    		var couponShortUrl = ($('#discount-plan-shorturl').is(':checked') && discountType == 2) ? $('#discount-plan-shorturl-value').val() : '';
    		var couponStartDate = $('#promo-new-startdate').val();
    		var couponEndDate = $('#promo-new-enddate').val();
    		var couponOriginId = $('#origin-coupon-id').val();
    		var invitationList = invitationOnly == 1 ? $('#invitation-list').val() : '';
    		var maxRedemption = $('#promo-new-redemptions').val();
    		var isEdit = typeof(viewCouponDetails)=='undefined' ? 0 : 1;

    		if(invitationOnly == 1 && !validateListEmail(invitationList)){
				alertBox('There\'s an invalid email in the invitation list, please check again!',alertBoxTitle);
				return false;
			}

    		if(couponStartDate != '' && alertBoxTitle === 'ADD NEW COUPON'){
    			if(new Date(couponStartDate) < new Date(toDay)){
    				alertBox('Activation Date of the coupon must be from or after today!',alertBoxTitle);
    				return false;
    			}
    		}

    		if(new Date(couponEndDate) <= new Date(toDay)){
				alertBox('Expiration Date of the coupon must be after today!',alertBoxTitle);
				return false;
			}

    		if(new Date(couponStartDate) > new Date(couponEndDate)){
    			alertBox('Activation Date of the coupon must be before Expiration Date!',alertBoxTitle);
    			return false;
    		}

    		if(couponDiscountType == 1 && $('#discount-plan-percentage').is(':checked') && $('#discount-plan-percentage-value').val() > 100){
    			alertBox('Plan price discount percentage cannot be bigger than 100!', alertBoxTitle);
    			return false;
    		}

    		$.ajax({
    			url: '/session/sire/models/cfc/promotion.cfc?method=AddNewPromoCode'+strAjaxQuery,
    			type: 'POST',
    			dataType: 'JSON',
    			data: {
    				inpCouponName: couponName,
    				inpCouponCode: couponCode,
    				inpCouponDescription: couponDesc,
    				inpCouponStatus: couponStatus,
    				inpCouponDiscountType: couponDiscountType,
    				inpDiscountPricePercent: discountPricePercent,
    				inpDiscountPriceFlatRate: discountPriceFlatRate,
    				inpCouponCredit: couponCredit,
    				inpCouponKeyword: couponKeyword,
    				inpCouponMLP: couponMLP,
    				inpCouponShortUrl: couponShortUrl,
    				inpCouponStartDate: couponStartDate,
    				inpCouponEndDate: couponEndDate,
    				inpCouponOriginId: couponOriginId,
    				inpLimitCb1: limitcb1,
    				inpLimitCb2: limitcb2,
    				inpInvitationOnly: invitationOnly,
    				inpInvitationList: invitationList,
    				inpRecurringTime: recurringTime,
    				inpMaxredemption: maxRedemption,
    				isEditCoupon: isEdit
    			},
    			beforeSend: function(){
					$("#processingPayment").show();
				},
				success: function(data){
					$("#processingPayment").hide();
					if(data.RXRESULTCODE == 1){
						alertBox(data.MESSAGE, alertBoxTitle, function(){
							document.getElementById("add-new-coupon-form").reset();
							window.location.href = '/session/sire/pages/admin-coupon-manage';
						});
						
					}
					else{
						alertBox(data.MESSAGE, alertBoxTitle, function(){});
					}
					
				}
    		});
    		
    	}
    });

	$('#delete-coupon').on('click', function(event){
		event.preventDefault();
		confirmBox('Are you sure you want to delete this coupon?', 'DELETE COUPON', function(){
			$.ajax({
				url: '/session/sire/models/cfc/promotion.cfc?method=DeleteCoupon'+strAjaxQuery,
				type: 'POST',
				dataType: 'JSON',
				data: {inpCouponId: $('#coupon-id').val()},
				beforeSend: function(){
					$("#processingPayment").show();
				},
				success: function(data){
					$("#processingPayment").hide();
					if(data.RXRESULTCODE == 1){
						alertBox(data.MESSAGE, 'DELETE COUPON', function(){
							document.getElementById("add-new-coupon-form").reset();
							window.location.href = '/session/sire/pages/admin-coupon-manage';
						});
						
					}
					else{
						alertBox(data.MESSAGE, 'DELETE COUPON', function(){});
					}
					
				}
			});
		});
		
	});

	// if(typeof(couponToEdit)!="undefined"){
	// 	$.ajax({
	// 		url: '/session/sire/models/cfc/promotion.cfc?method=GetCouponDetails'+strAjaxQuery,
	// 		type: 'POST',
	// 		dataType: 'JSON',
	// 		data: {inpCouponId: couponToEdit},
	// 		success: function(data){
	// 			if(data.RXRESULTCODE == 1){
	// 				$('#coupon-name').val(data.COUPONNAME);
	// 				$('#coupon-code').val(data.COUPONCODE);
	// 				$('#coupon-desc').val(data.COUPONDESC);
	// 				if(data.COUPONSTATUS == 1){
	// 					$('#coupon-available').prop('checked', true);
	// 				}
	// 				else{
	// 					$('#coupon-available').prop('checked', false);
	// 				}
	// 				$('#origin-coupon-id').val(data.ORIGINID);

	// 				if(data.COUPONDISCOUNTTYPE == 1){
	// 					$('#discount-plan').trigger('click');
	// 					if(data.COUPONDISCOUNTPRICEPERCENT > 0){
	// 						$('#discount-plan-percentage').trigger('click');
	// 						$('#discount-plan-percentage-value').val(data.COUPONDISCOUNTPRICEPERCENT);
	// 					}
	// 					else{
	// 						$('#discount-plan-percentage').prop('checked', false);
	// 					}

	// 					if(data.COUPONDISCOUNTPRICEFLATRATE > 0){
	// 						$('#discount-plan-flat-rate').trigger('click');
	// 						$('#discount-plan-flat-rate-value').val(data.COUPONDISCOUNTPRICEFLATRATE);
	// 					}
	// 					else{
	// 						$('#discount-plan-flat-rate').prop('checked', false);
	// 					}
	// 				}

	// 				if(data.COUPONDISCOUNTTYPE == 2){
	// 					$('#discount-other').trigger('click');
	// 					if(data.COUPONCREDIT > 0){
	// 						$('#discount-plan-credit').trigger('click');
	// 						$('#discount-plan-credit-value').val(data.COUPONCREDIT);
	// 					}
	// 					else{
	// 						$('#discount-plan-credit').prop('checked', false);
	// 					}

	// 					if(data.COUPONKEYWORD > 0){
	// 						$('#discount-plan-keyword').trigger('click');
	// 						$('#discount-plan-keyword-value').val(data.COUPONKEYWORD);
	// 					}
	// 					else{
	// 						$('#discount-plan-keyword').prop('checked', false);
	// 					}

	// 					if(data.COUPONMLP > 0){
	// 						$('#discount-plan-mlp').trigger('click');
	// 						$('#discount-plan-mlp-value').val(data.COUPONMLP);
	// 					}
	// 					else{
	// 						$('#discount-plan-mlp').prop('checked', false);
	// 					}

	// 					if(data.COUPONSHORTURL > 0){
	// 						$('#discount-plan-shorturl').trigger('click');
	// 						$('#discount-plan-shorturl-value').val(data.COUPONSHORTURL);
	// 					}
	// 					else{
	// 						$('#discount-plan-shorturl').prop('checked', false);
	// 					}
	// 				}

	// 				if(data.COUPONSTARTDATE != ''){
	// 					$('#promo-new-startdate').val(data.COUPONSTARTDATE);
	// 				}

	// 				if(data.COUPONENDDATE != ''){
	// 					$('#promo-new-enddate').val(data.COUPONENDDATE);
	// 				}
	// 			}
	// 			else{
	// 				alertBox(data.MESSAGE, 'EDIT COUPON', function(){
	// 					window.location.href = '/session/sire/pages/admin-coupon-manage';
	// 				});
	// 			}
	// 		}

	// 	});
		
	// }

	if(typeof(viewCouponDetails)!="undefined"){
		$.ajax({
			url: '/session/sire/models/cfc/promotion.cfc?method=GetCouponDetails'+strAjaxQuery,
			type: 'POST',
			dataType: 'JSON',
			data: {inpCouponId: viewCouponDetails},
			success: function(data){
				if(data.RXRESULTCODE == 1){
					$('#coupon-name').val(data.COUPONNAME);
					$('#coupon-code').val(data.COUPONCODE);
					$('#coupon-desc').val(data.COUPONDESC);
					if(data.COUPONSTATUS == 1){
						$('#coupon-available').prop('checked', true);
					}
					else{
						$('#coupon-available').prop('checked', false);
					}
					$('#origin-coupon-id').val(data.ORIGINID);

					if(data.COUPONDISCOUNTTYPE == 1){
						$('#discount-plan').trigger('click');
						if(data.COUPONDISCOUNTPRICEPERCENT > 0){
							$('#discount-plan-percentage').prop('checked', true);
							$('#discount-plan-percentage-value').val(data.COUPONDISCOUNTPRICEPERCENT);
							$('#discount-plan-percentage-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-percentage').prop('checked', false);
						}

						if(data.COUPONDISCOUNTPRICEFLATRATE > 0){
							$('#discount-plan-flat-rate').prop('checked', true);
							$('#discount-plan-flat-rate-value').val(data.COUPONDISCOUNTPRICEFLATRATE);
							$('#discount-plan-flat-rate-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-flat-rate').prop('checked', false);
						}
					}

					if(data.COUPONDISCOUNTTYPE == 2){
						$('#discount-other').trigger('click');
						if(data.COUPONCREDIT > 0){
							$('#discount-plan-credit').prop('checked', true);
							$('#discount-plan-credit-value').val(data.COUPONCREDIT);
							$('#discount-plan-credit-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-credit').prop('checked', false);
						}

						if(data.COUPONKEYWORD > 0){
							$('#discount-plan-keyword').prop('checked', true);
							$('#discount-plan-keyword-value').val(data.COUPONKEYWORD);
							$('#discount-plan-keyword-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-keyword').prop('checked', false);
						}

						if(data.COUPONMLP > 0){
							$('#discount-plan-mlp').prop('checked', true);
							$('#discount-plan-mlp-value').val(data.COUPONMLP);
							$('#discount-plan-mlp-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-mlp').prop('checked', false);
						}

						if(data.COUPONSHORTURL > 0){
							$('#discount-plan-shorturl').prop('checked', true);
							$('#discount-plan-shorturl-value').val(data.COUPONSHORTURL);
							$('#discount-plan-shorturl-value').prop('disabled', false);
						}
						else{
							$('#discount-plan-shorturl').prop('checked', false);
						}
					}

					if(data.COUPONSTARTDATE != ''){
						$('#promo-new-startdate').val(data.COUPONSTARTDATE);
					}

					if(data.COUPONENDDATE != ''){
						$('#promo-new-enddate').val(data.COUPONENDDATE);
					}

					if(data.LIMITCB1 == 1){
						$('#check-combined').prop('checked', true);
					}
					else{
						$('#check-combined').prop('checked', false);
					}

					if(data.LIMITCB2 == 1){
						$('#check-individual').prop('checked', true);
					}
					else{
						$('#check-individual').prop('checked', false);
					}

					if(data.INVITATIONONLY == 1){
						$('#check-invitation').prop('checked', true);
						$('#invitation-list').val(data.INVITATIONLIST);
						$('#invitation-list').prop('disabled', false);
					}

					if(data.COUPONREDEMPTION != ''){
						$('#promo-new-redemptions').val(data.COUPONREDEMPTION);
					}

					if(data.COUPONRECURRING == 0){
						$('#recurring-type-1').prop('checked', true);
					}
					else if(data.COUPONRECURRING == -1){
						$('#recurring-type-2').prop('checked', true);
					}
					else{
						$('#recurring-type-3').prop('checked', true);
						$('#number-of-renewals').val(data.COUPONRECURRING);
						$('#number-of-renewals').prop('disabled', false);
					}

					// $('#coupon-available').prop('disabled', true);
					// $('#coupon-code').prop('disabled', true);
					// $('#coupon-name').prop('disabled', true);
					// $('#coupon-desc').prop('disabled', true);
					// $('#discount-no').prop('disabled', true);
					// $('#discount-plan').prop('disabled', true);
					// $('#discount-other').prop('disabled', true);
					// $('#discount-plan-percentage').prop('disabled', true);
					// $('#discount-plan-percentage-value').prop('disabled', true);
					// $('#discount-plan-flat-rate').prop('disabled', true);
					// $('#discount-plan-flat-rate-value').prop('disabled', true);
					// $('#discount-plan-credit').prop('disabled', true);
					// $('#discount-plan-credit-value').prop('disabled', true);
					// $('#discount-plan-keyword').prop('disabled', true);
					// $('#discount-plan-keyword-value').prop('disabled', true);
					// $('#discount-plan-mlp').prop('disabled', true);
					// $('#discount-plan-mlp-value').prop('disabled', true);
					// $('#discount-plan-shorturl').prop('disabled', true);
					// $('#discount-plan-shorturl-value').prop('disabled', true);
					// $('#promo-new-startdate').prop('disabled', true);
					// $('#promo-new-enddate').prop('disabled', true);
					// $('#check-combined').prop('disabled', true);
					// $('#check-individual').prop('disabled', true);
					// $('#invitation-list').prop('disabled', true);
					// $('#check-invitation').prop('disabled', true);
					// $('#recurring-type-1').prop('disabled', true);
					// $('#recurring-type-2').prop('disabled', true);
					// $('#recurring-type-3').prop('disabled', true);
					// $('#number-of-renewals').prop('disabled', true);
					// $('#promo-new-redemptions').prop('disabled', true);
					// $('#create-new-coupon').hide();
					$('#delete-coupon').show();
				}
				else{
					alertBox(data.MESSAGE, 'EDIT COUPON', function(){
						window.location.href = '/session/sire/pages/admin-coupon-manage';
					});
				}
			}

		});
		
	}

	/* countdown function, display countdown number and prevent user to exceed a limit number of character */
	function count_down_character_by_id(countId, contentId, limit){
		$('#' + countId).text(limit-$('#'+ contentId).val().length);

		$('#' + contentId).on('keyup', function(){
			$('#' + countId).text(limit-$('#' + contentId).val().length);
		});

		var contentText = document.getElementById(contentId);
		if(contentText) {
		    contentText.addEventListener("input", function() {
		        if (contentText.value.length == limit) {
		        	return false;
		        } 
		        else if (contentText.value.length > limit) {
		        	contentText.value = contentText.value.substring(0, limit);
		        }
		    },  false);
		}

		$('#' + contentId).bind('paste', function(e) {
		    var elem = $(this);
		    setTimeout(function() {
		        var text = elem.val();
		        if (text.length > limit) {
					elem.val(text.substring(0,limit));
		        }
		        $('#' + countId).text(limit-parseInt(elem.val().length));
		    }, 100);
		});
	}

	count_down_character_by_id('coupon-name-countdown','coupon-name', 256);
	count_down_character_by_id('coupon-code-countdown','coupon-code', 16);
	count_down_character_by_id('coupon-desc-countdown','coupon-desc', 1000);

	$('#coupon-code').keypress(function( e ) {
	    if(e.which === 32) 
	        return false;
	});

	$('#promo-code').keypress(function( e ) {
	    if(e.which === 32) 
	        return false;
	});
})();