(function($){
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";	
	var options = '';	
	$(document).ready(function() {
	   	$.ajax({
			type:"GET",
			url:"/session/sire/models/cfc/reports.cfc?method=GetListStatusPayment" + strAjaxQuery,	 			
			success: function(data) {	
				var listPayment = data.DATALIST;								
				options = '<option value= "ALL" selected>All</option>';		
				var i;				
				for (i = 0; i < listPayment.length; i++) { 
				    options  = options + '<option value= "'+listPayment[i].VALUE+'">'+listPayment[i].TEXT+'</option>';
				}									
				$('<select class="filter-form-control form-control"></select>').append(options);				
				$('.filter-form-control').val('ALL').trigger('change');		
				$('.filter-form-control').prop('TEXT', 'ALL');								
			}
		});
	});
	

	var StatusPayment = function(){		
		
		$.ajax({
			type:"GET",
			url:"/session/sire/models/cfc/reports.cfc?method=GetListStatusPayment" + strAjaxQuery,	 			
			success: function(data) {	
				var listPayment = data.DATALIST;				
				options = '<option value= "ALL" selected>All</option>';				
				var i;				
				for (i = 0; i < listPayment.length; i++) { 
				    options  = options + '<option value= "'+listPayment[i].VALUE+'">'+listPayment[i].TEXT+'</option>';
				}					
				$('<select class="filter-form-control form-control"></select>').append(options);						
				$('.filter-form-control').val('ALL').trigger('change');			
			}
		 });								
		return $('<select class="filter-form-control form-control"></select>').append(options);
	};	
	

	var GetPaymentTransactionListByCC = function(customFilterData){
		customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   

		var viewJsonCol = function(object){
			var strReturn = "";
				strReturn = $("<div class='btn-re-edit' data-id='" +object.ID+ "' data-json='"+object.JSONCONTENT+"'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></div>")[0].outerHTML;
			return strReturn;
		}

		var viewTransIdCol = function(object){
			var strReturn = "";
			if(object.TRANSID != ''){
				strReturn = $("<a target='_blank' href='/session/sire/pages/admin-payment-transactions-detail.cfm?transId="+object.TRANSID+"'>"+object.TRANSID+"</a>")[0].outerHTML;
			}else{
				strReturn = $("<spam>"+object.TRANSID+"</span>")[0].outerHTML;
			}
			return strReturn;
		}

		$('#transactions-detail').html("").addClass('hidden');
		$('#transactions-list_wrapper').removeClass('hidden');

		var table = $('#transactions-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	// {"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id","sWidth":"100px"},
		    	{"mData": viewTransIdCol, "sName": '', "sTitle": 'Transaction ID',"bSortable": false,"sClass":"","sWidth":"210px"},
				{"mData": "USERID", "sName": '', "sTitle": 'UserID',"bSortable": true,"sClass":"", "sWidth": "80px"},
				{"mData": "FIRSTNAME", "sName": '', "sTitle": 'First Name',"bSortable": true,"sClass":"", "sWidth": "140px"},
				{"mData": "LASTNAME", "sName": '', "sTitle": 'Last Name',"bSortable": true,"sClass":"", "sWidth": "150px"},
				{"mData": "EMAIL", "sName": '', "sTitle": 'Email',"bSortable": false,"sClass":"", "sWidth": "250px"},
				{"mData": "MODULE", "sName": '', "sTitle": 'Action',"bSortable": false,"sClass":"", "sWidth":"250px"},
				{"mData": "CREATED", "sName": '', "sTitle": 'Created',"bSortable": true,"sClass":"","sWidth":"130px"},
				{"mData": "AMOUNT", "sName": '', "sTitle": 'Amount',"bSortable": false,"sClass":"","sWidth":"90px"},
				{"mData": "STATUS", "sName": '', "sTitle": 'Status',"bSortable": true,"sClass":"","sWidth": "140px"},
				{"mData": "CCNUMBER", "sName": '', "sTitle": 'CC No',"bSortable": false,"sClass":"","sWidth":"100px"},
				{"mData": "GATEWAY", "sName": '', "sTitle": 'GW',"bSortable": false,"sClass":"","sWidth":"120px"},
				{"mData": viewJsonCol, "sName": '', "sTitle": 'Response',"bSortable": false,"sClass":"json-content-td","sWidth":"100px"},
				
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetPaymentTransactionList'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
		     "fnFooterCallback": function(nRow){
		    },
		    "fnHeaderCallback": function(nRow){
		    	$(nRow).find('th.selection').html('<i class="select-all glyphicon glyphicon-ok"></i>');
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {
				
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
				$('#transactions-list_wrapper').removeClass('hidden');
			}
		});

		return function(){
			table.fnStandingRedraw();
		}

	};

	var GetPaymentTransactionListByTransId = function(transId){
		transId  = typeof(transId ) != "undefined" ? transId : "0";   

		 try{
			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfm/admin-payment-transactions-detail.cfm',   
			dataType: 'json',
			data: {transId:transId},
			beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//bootbox.alert('Update Fail', function() {});
				//$("#enable_security_question_form")[0].reset();
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: "Can not found this Transaction Id",
				    title: "Admin payment transaction",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			},					  
			success:		
				function(d) {
					$('#processingPayment').hide();
					$('#transactions-list_wrapper').addClass('hidden');
					$('#transactions-detail').html(d).removeClass('hidden');
				} 		
			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.dialog({
			    message:'Admin payment transaction',
			    title: "",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
		}
	};

	GetPaymentTransactionListByCC();

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Transaction ID', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: 'transid'},
			{DISPLAY_NAME: 'User ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' lpw.userId_int '},
			{DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
			{DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
			{DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
			{DISPLAY_NAME: 'Amount', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: 'amount'},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'STATUS', SQL_FIELD_NAME: 'status', CUSTOM_DATA:StatusPayment},
        	{DISPLAY_NAME: 'Last 4 card digit', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: 'cc'}
		],
		clearButton: true,
		error: function(ele){
			console.log(ele);
		},
		clearCallback: function(){
			GetPaymentTransactionListByCC();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		//console.log(filterData[0]);
		if(filterData[0].NAME == 'transid')
			GetPaymentTransactionListByTransId(filterData[0].VALUE);
		else 
			GetPaymentTransactionListByCC(filterData);
	});

	$('body').on('click', '.btn-re-edit', function(){
		var formatStatus = function(object){		
			object=JSON.stringify(object);
			result = String(object).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');	
			
			return result;
		}
		bootbox.dialog({
	        message: '<h4 class="be-modal-title">'+ 'JSON Content' +'</h4><p style="word-wrap: break-word">'+ formatStatus($(this).data('json')) +'</p>',
	        title: '&nbsp;',
	        className: "be-modal",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "green-gd",
	                callback: function(){
	                }
	            },
	        }
	    });		
	});

	$(".btn-add-item").hide();

	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		GetPaymentTransactionListByCC();
	});

})(jQuery);