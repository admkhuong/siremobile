(function($){
	var _form = $('#plan-form');
	_form.validationEngine({promptPosition: "topLeft",focusFirstField: false});

	$('#plan-form .save-plan').click(function(event){
		event.preventDefault();

		if(_form.validationEngine('validate')){
			$.ajax({
				url: '/session/sire/models/cfc/plan.cfc?method=SavePlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
				data: _form.serialize(),
				beforeSend: function(){
					$('#processingPayment').show();
				},
				complete: function(){
					$('#processingPayment').hide();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					bootbox.dialog({
						message: 'Save Plan Fail!',
						title: 'Save Plan',
						buttons: {
							success: {
								label: " Ok ",
								className: "btn btn-medium btn-success-custom",
								callback: function() {}
							}
						}
					});
				},
				success: function(data, textStatus, jqXHR) {
					if (data && data.RXRESULTCODE == 1) {
						location = '/session/sire/pages/admin-plan-management';
					} else {
						bootbox.dialog({
							message: data.MESSAGE,
							title: 'Save Plan',
							buttons: {
								success: {
									label: " Ok ",
									className: "btn btn-medium btn-success-custom",
									callback: function() {}
								}
							}
						});
					}
				}

			});
		}
	});	
	$(document).ready(function(){
		$("#Status_int").select2();				
	});
})(jQuery);

