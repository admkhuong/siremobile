(function($){

	function GetPlanList(strFilter, startFrom) {
		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"",
		statusCol = function(object){
			switch(object.STATUS_INT) {
				case 1:
				return ".com";
				break;
				case 8:
				return "Enterprise";
				break;
				case -8:
				return "Pending";
				break;
				case 0:
				return "Inactive";
				break;
				case 4:
				return "Enterprise PostPaid";
				break;				
				default:
				return "";
			}
		},
		amountCol = function(object) {
			if (object.AMOUNT_DEC == 0) {
				return 'Free';
			} else {
				return '$' + object.AMOUNT_DEC;
			}
		},
		actionCol = function(object) {
			if (Math.abs(object.STATUS_INT) == 1 || Math.abs(object.STATUS_INT) == 0) return '';
			
			var strReturn = $('<a title="Edit plan" class="simon-item '+(parseInt(object.STATUS_INT) > 0 ? "user-update-btn" : "")+'" '+
				(parseInt(object.STATUS_INT) > 0 ? ' ' : 'disabled ')+
				'href="/session/sire/pages/admin-plan-edit?inpPlanId='+ object.PLANID_INT +'"'+
				'><i class="fa fa-pencil-square-o" aria-hidden="true"></i>')[0].outerHTML ;

			//$('<a class="simon-item '+(parseInt(object.STATUS_INT) > 0 ? "deactive" : "active")+'-account-btn act-account-btn" title="'+(parseInt(object.STATUS_INT) > 0 ? "Deactive plan" : "Active plan")+'"><i class="fa fa-'+(parseInt(object.STATUS_INT) > 0 ? "lock" : "unlock")+'" aria-hidden="true"></i></a>')
			//.attr('data-id', object.PLANID_INT)[0].outerHTML;

			return strReturn;
		}

		;

		var table = $('#tblListPlan').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
			},
			"fnStateSaveParams": function (oSettings, oData) {
			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 20,
			"iDisplayStart": startFrom,
			"bAutoWidth": false,
			"aoColumns": [
			{"mData": "PLANID_INT", "sName": 'PlanId_int', "sTitle": 'ID',"bSortable": false,"sClass":"plan-id", "sWidth":"80px"},
			{"mData": "PLANNAME_VCH", "sName": 'PlanName_vch', "sTitle": 'Name',"bSortable": true,"sClass":"plan-name"},
			{"mData": amountCol, "sName": 'Amount_dec', "sTitle": "Amount","bSortable": true,"sClass":"","sWidth":"200px"},
			{"mData": statusCol, "sName": 'Status_int', "sTitle": "Status","bSortable": true,"sClass":"", "sWidth":"250px"},
			{"mData": "LASTUPDATEDATE_DT", "sName": 'Last Updated Date', "sTitle": "Last Updated Date","bSortable": false,"sClass":"", "sWidth":"220px"},
			{"mData": actionCol, "sName": 'Actions', "sTitle": "Actions","bSortable": false,"sClass":"user-total-used", "sWidth":"180px"}
			],
			"sAjaxDataProp": "PlanList",
			"sAjaxSource": '/session/sire/models/cfc/plan.cfc?method=GetPlanList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',

				"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
					// console.log(fnCallback);
					// if (aoData[3].value < 0){
					// 	aoData[3].value = 0;
					// }

					aoData.push(
						{ "name": "customFilter", "value": customFilterData}
						);
					
					oSettings.jqXHR = $.ajax( {
						"dataType": 'json',
						"type": "POST",
						"url": sSource,
						"data": aoData,
						"success": fnCallback
					});
				},
				"aaSorting": [[3,'asc']]
			});
	}

	GetPlanList();

	//Filter bar initialize
	var StatusCustomData = function(){
		var options   = '<option value= "-100" selected>All</option>'+
		'<option value="0">Deactivated</option>'+
		'<option value="1">Active</option>'+
		'<option value="8">Private</option>';
		return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	$('#box-filter').sireFilterBar({
		fields: [
		{DISPLAY_NAME: 'Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' PlanName_vch '},
		{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' Status_int ', CUSTOM_DATA:StatusCustomData},
		{DISPLAY_NAME: 'Plan Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' PlanId_int '}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			// console.log(ele);
		},
		  // limited: function(msg){
		  //  alertBox(msg);
		  // },
		  clearCallback: function(){
		  	GetPlanList();
		  }
		}, function(filterData){
		//called if filter valid and click to apply button
		GetPlanList(filterData);
	});

	/*$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		GetPlanList();
	});*/

	$('#tblListPlan').on('click', '.act-account-btn', function(){
		var self = $(this);
		var id = self.data('id');
		var act_2 = (self.hasClass('active-account-btn') ? 0 : 1);
		bootbox.dialog({
			message: 'Are you sure you want to '+ (act_2==1?'deactive':'active') +' this item?',
			title: (act_2==1?'Deactive':'Active') +' plan',
			className: "",
			buttons: {
				cancel: {
					label: "Cancel",
					className: "green-cancel"
				},
				success: {
					label: " OK ",
					className: "btn btn-medium btn-success-custom",
					callback: function(result) {
						if(result){
							$.ajax({
								type: "POST",
								url: '/session/sire/models/cfc/plan.cfc?method=ToggleStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								dataType: 'json',
								data: { inpPlanId : id},
								beforeSend: function( xhr ) {
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
										message: (act_2==1?'Deactive':'Active') +' Fail!',
										title: (act_2==1?'Deactive':'Active') +' plan',
										buttons: {
											success: {
												label: " Ok ",
												className: "btn btn-medium btn-success-custom",
												callback: function() {}
											}
										}
									});
								},					  
								success: function(data) {
									$('#processingPayment').hide();
									if(data.RXRESULTCODE > 0)
									{
										// reload data
										var oTable = $('#tblListPlan').dataTable();
										oTable.fnDraw();
										return;
									}
									else
									{
										if (data.ERRMESSAGE[0] != '') {
											bootbox.alert(data.MESSAGE + "\n" + data.ERRMESSAGE, function() { return; } );
										}
									}
								}
							});



						}
					}
				}
			}
		});
	});
})(jQuery);