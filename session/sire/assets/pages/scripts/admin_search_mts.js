(function() {

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    var dateStart_crm = '';
    var dateEnd_crm = '';
    var start = moment().subtract(6, 'days');
    var end = moment();
    var rfresh = 0;
    var startTime = '';
    var endTime = '';
    var planName = '';
    var method = '';
    var exportPath = '';
    var planAction = '';
    var planField = '';

    $("#search-mt-word-from").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});

    $('.reportrange').daterangepicker({
        "dateLimit": {
            "years": 1
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, GetList);

    function GetList(start, end, firstLoad, strFilter){
        $("#tableDataList").html('');

        if(typeof(strFilter)=='object'){
            customFilterData = JSON.stringify(strFilter);
        }
        else{
            customFilterData = '';
        }

        // console.log(customFilterData);
        dateStart_crm = start;
        dateEnd_crm = end;
        $('.reportrange span.time').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
        
        startTime = dateStart;
        endTime = dateEnd;

        if(firstLoad == 1){
            // do nothing
        }
        else{
            if($('#searchbox').val()!=''){
                if($("#search-mt-word-from").validationEngine('validate')){
                    var table = $('#tableDataList').dataTable({
                        "bStateSave": false,
                        "iStateDuration": -1,
                        "bProcessing": true,
                        "bFilter": false,
                        "bServerSide":true,
                        "bDestroy":true,
                        "sPaginationType": "input",
                        "bLengthChange": false,
                        "iDisplayLength": 10,
                        "bAutoWidth": false,
                        "aoColumns": [
                            {"mData": "USERID", "sName": '', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
                            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
                            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "200px"},
                            {"mData": "BATCHID", "sName": '', "sTitle": 'Campaign ID', "bSortable": false, "sClass": "", "sWidth": "200px"},
                            {"mData": "MESSAGE", "sName": '', "sTitle": 'Message', "bSortable": false, "sClass": "", "sWidth": "400px"},
                            {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "200px"},
                        ],
                        "sAjaxDataProp": "DATALIST",
                        "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=SearchMTMessageByWord'+strAjaxQuery,
                        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                            
                            aoData.push(
                                { "name": "inpStartDate", "value": startTime}
                            );
                            aoData.push(
                                { "name": "inpEndDate", "value": endTime}
                            );

                            aoData.push(
                                { "name": "customFilter", "value": $('#searchbox').val()}
                            );
                            filter = $('#searchbox').val();
                            
                            oSettings.jqXHR = $.ajax( {
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "beforeSend": function(xhr){
                                    $("#processingPayment").show();
                                },
                                "success": function(data) {
                                    fnCallback(data);
                                    if(data.DATALIST.length == 0){
                                        $('#tableDataList_paginate').hide();
                                    }
                                    $("#processingPayment").hide();
                                }
                            });
                        }
                    });
                }
            }
        }
    
    }

    // function InitFilter () {
    //     $("#box-filter").html('');
    //     //Filter bar initialize
    //     $('#box-filter').sireFilterBar({
    //         fields: [
    //             {DISPLAY_NAME: 'Word', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'WORDLIKE', SQL_FIELD_NAME: ' ire.Response_vch '},
    //         ],
    //         clearButton: true,
    //         error: function(ele){
                
    //         },
    //         clearCallback: function(){
    //             GetList(start,end);
    //         }
    //     }, function(filterData){
    //         GetList(start,end,filterData);
    //     });
    // }

    // InitFilter();

    $('#searchbox').keypress(function(event){
        if( (event.keyCode == 13) ) {
          event.preventDefault();
          GetList(dateStart_crm,dateEnd_crm);
        }
    });

    $('#search-word').click(function(event) {
        event.preventDefault();
        GetList(dateStart_crm,dateEnd_crm);
    });

    jQuery(document).ready(function($) {
        $('#search-mt-word-from').validationEngine('hide');
        GetList(start,end,1);
    });
})();