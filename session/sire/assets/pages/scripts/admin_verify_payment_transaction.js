function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var StatusCustomData = function(){
	var options   = '<option value= "-100" selected> </option>'+
					'<option value="0">Pending</option>'+
					'<option value="-1">Declined</option>'+
					'<option value="-2">Error</option>'+
					'<option value="1">Approved</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Payment Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' pkw.UpdateDate '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.MFAContactString_vch '},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' pkw.PaymentStatus ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});



	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {
			if(object.STATUS == 'Pending'){
				var strReturn = '<a href="" id ="'+object.PAYMENTID+'" class="decline-payment-transaction" title="Decline">Decline</a> | <a href="" id ="'+object.PAYMENTID+'" id-name ="'+object.USERID+'" id-email="'+object.EMAIL+'" payment-gateway="'+object.PAYMENTGATEWAY+'" data-target="#modal-sections" class="run-payment-transaction">Approve</a>';
           }else{
				var strReturn = '';
			}
            return strReturn;
        }
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '12%',"bSortable": false},
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '17%',"bSortable": false},
                {"mData": "PHONE", "sName": 'u.MFAContactString_vch', "sTitle": 'Phone', "sWidth": '11%',"bSortable": false},
				{"mData": "PAYMENT_DATE", "sName": 'pkw.UpdateDate', "sTitle": 'Purchased Date', "sWidth": '12%',"bSortable": true,"sClass": "col-center"},
				{"mData": "ITEM", "sName": 'item', "sTitle": 'Item', "sWidth": '12%',"bSortable": true,"sClass": "col-center"},
				{"mData": "PAYMENT_AMOUNT", "sName": 'pkw.PaymentAmount_dbl', "sTitle": 'Amount($)', "sWidth": '8%',"bSortable": true},
				{"mData": "STATUS", "sName": 'pkw.PaymentStatus', "sTitle": 'Status', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
               // {"mData": "VERIFYDATE", "sName": 'pkw.VerifyDate', "sTitle": 'Verify Date', "sWidth": '10%',"bSortable": false},
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "12%"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=PurchaseAmountLimitedList'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	


	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    $("#tblListEMS").on("click", ".decline-payment-transaction", function(event){
        event.preventDefault();
        var PAYMENTID = $(this).attr('id');
        confirmBox('Are you sure you want to decline this payment transaction?', 'Decline Payment Transaction', function(){
            $.ajax({
                url: '/session/sire/models/cfc/billing.cfc?method=UpdateStatusPaymentTransaction'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
					inpPaymentID:PAYMENTID,
					inpPaymentStatus: -1
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Action Error!','Decline Payment Transaction',''); 
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        InitControl(); 
                        alertBox(data.MESSAGE,'Decline Payment Transaction','');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Decline Payment Transaction','');
                    }                       
                }
            });
        });
    });

    $("#tblListEMS").on("click", ".run-payment-transaction", function(event){
		event.preventDefault();
		$("#processingPayment").show();
		upgradePlan = 0;		
		var UserID = $(this).attr('id-name');
		var h_email = $(this).attr('id-email');
		var PAYMENTID = $(this).attr('id');
		var paymentGateway = $(this).attr('payment-gateway');
	
		$.ajax({
			url: '/session/sire/models/cfc/payment/vault.cfc?method=getDetailCreditCardInVault'+strAjaxQuery,
			async: true,
			type: 'post',
			dataType: 'json',
			data: {
				inpUserId:UserID,
				inpPaymentGateway:paymentGateway
			},
			beforeSend: function(){
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$("#processingPayment").hide();
			},
			success: function(data) {
				if (data.RXRESULTCODE == 1) {
					
					// Get Keywordsnumber and SMSnumber
					$.ajax({
						url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentTransactionById'+strAjaxQuery,
						async: true,
						type: 'post',
						dataType: 'json',
						data: {
							inpPaymentID:PAYMENTID
						},
						beforeSend: function(){
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$("#processingPayment").hide();
							alertBox('Payment Transaction Error!','Purchase Payment Transaction',''); 
						},
						success: function(data1) {
							if (data1.RXRESULTCODE == 1) {
								 document.getElementById("addon-form").reset()
								var datapaymentinfor = jQuery.parseJSON(data1.PAYMENTINFOR);									
								$("#h_email").val(h_email);
								$('input[name="h_email_save"]').val(data.CUSTOMERINFO.EMAILADDRESS);								
								$('#addon-form').find('#numberSMSToSend').val(datapaymentinfor.numberSMSToSend);
								$('#addon-form').find('#numberKeywordToSend').val(datapaymentinfor.numberKeywordToSend);
								$('#addon-form').find('#amount').val(datapaymentinfor.amount);
								$('#addon-form').find('#vl_maskedNumber').text("Card Number : " + data.CUSTOMERINFO.MASKEDNUMBER);
								$('#addon-form').find('#vl_expirationDate').text("Expiration Date : " + data.CUSTOMERINFO.EXPIRATIONDATE);
								$('#addon-form').find('#vl_fullname').text("Cardholder Name : " + data.CUSTOMERINFO.FIRSTNAME + ' ' + data.CUSTOMERINFO.LASTNAME);
								$('#addon-form').find('#vl_line1').text("Address : " + data.CUSTOMERINFO.LINE1);
								$('#addon-form').find('#vl_city').text("City : " + data.CUSTOMERINFO.CITY);
								$('#addon-form').find('#vl_state').text("State : " + data.CUSTOMERINFO.STATE);
								$('#addon-form').find('#vl_zip').text("Zip code : " + data.CUSTOMERINFO.ZIP);
								$('#addon-form').find('#vl_country').text("Country : " + data.CUSTOMERINFO.COUNTRY);
								$('#addon-form').find('#vl_emailAddress').text("Email : " + data.CUSTOMERINFO.EMAILADDRESS);
								$('#addon-form').find('#paymentId').val(PAYMENTID);
								$('#addon-form').find('#expiration_date_upgrade').val("");
								$('#addon-form').find("#uniform-optionsRadios2 > span").removeClass ( 'checked' );
								$('#addon-form').find("#uniform-optionsRadios1 > span").addClass ( 'checked' );
								$('#addon-form').find('#paymentGateway').val(data1.PAYMENTGATEWAY);
								$('#addon-form').find('#userId').val(UserID);
								UIkit.modal('#buyaddon-modal-sections')[0].toggle();
								$("#addon-form").find('.fset_cardholder_info').hide();
								$("#processingPayment").hide();
																
							}
						}
					});
				} else {
					alertBox(data.MESSAGE,'Purchase Payment Transaction','');
				}
				$("#processingPayment").hide();                       
			}
		});
        //var PAYMENTID = $(this).attr('id');
	});

	$("#addon-form").find('.select_used_card').click(function(){
		if ($(this).val() == '2') {
			new_cc_status = 2;
			$("#addon-form").find('.fset_cardholder_info').show();
		} else {
			new_cc_status = 1;
			$("#addon-form").find('.fset_cardholder_info').hide();
		}
	});
    
	$("#addon-form").find('#expiration_date_month_upgrade, #expiration_date_year_upgrade').change(function(){
        $('#addon-form').find('#expiration_date_upgrade').val($('#expiration_date_month_upgrade').val() + '/' + $('#expiration_date_year_upgrade').val());						
    });
	// Run charge the payment after approve
	$("#addon-form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$('#addon-form').submit(function(event){
		event.preventDefault();
		if($("#addon-form").validationEngine('validate')){
			var self = $("#addon-form");
			//alert(self.serialize());
			var PAYMENTID = self.find('#paymentId').val();
			var paymentGateway = self.find('#paymentGateway').val();
			confirmBox('Are you sure you want to purchase this payment transaction?', 'Purchase Payment Transaction', function(){
				$("#processingPayment").show();
				$.ajax({
					url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentTransactionById'+strAjaxQuery,
					async: true,
					type: 'post',
					dataType: 'json',
					data: {
						inpPaymentID:PAYMENTID
					},
					beforeSend: function(){
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$("#processingPayment").hide();
						alertBox('Payment Transaction Error!','Purchase Payment Transaction',''); 
					},
					success: function(data) {
						var result = { };
			            $.each($('#addon-form').serializeArray(), function() {
			                result[this.name] = this.value;
			            });
			            var cc_saved = result.select_used_card;
			            var numberSMSToSend = result.numberSMSToSend;
			            var numberKeywordToSend = result.numberKeywordToSend;
			            var select_used_card = result.select_used_card;
			            var h_email = result.h_email;
			            var h_email_save = result.h_email_save;
			            var payment_method = result.payment_method;
			            var number = result.number;
			            var cvv =  result.cvv;
			            var expirationDate = result.expirationDate;
			            var firstName = result.firstName;
			            var lastName = result.lastName;
			            var city = result.city;
			            var line1 = result.line1;
			            var state = result.state;
			            var zip = result.zip;
			            var country = result.country;
			            var phone = result.phone;
			            var email = result.email;
			            var expiration_date_month_upgrade = result.expiration_date_month_upgrade;
			            var expiration_date_year_upgrade = result.expiration_date_year_upgrade;

			            var formData ={
			                'numberSMSToSend': numberSMSToSend,
			                'numberKeywordToSend': numberKeywordToSend,
			                'select_used_card': select_used_card,
			                'h_email': h_email,
			                'h_email_save': h_email_save,
			                'payment_method': payment_method,
			                'inpNumber': number,
			                'inpCvv2': cvv,
			                'expirationDate': expirationDate,
			                'inpFirstName': firstName,
			                'inpLastName': lastName,
			                'inpCity': city,
			                'inpLine1': line1,
			                'inpState': state,
			                'inpPostalCode': zip,
			                'inpCountryCode': country,
			                'phone': phone,
			                'inpEmail': email,
			                'paymentGateway': paymentGateway,
			                'inpExpireMonth': expiration_date_month_upgrade, 
			                'inpExpreYear' : expiration_date_year_upgrade
			            };
			            var jsondata = JSON.stringify(formData);
			            var selectusercard = result.select_used_card;
                        var saveccinfo = result.save_cc_info;
                        var amountpayment = result.amount;
                        var userId = result.userId;
						if (data.RXRESULTCODE == 1) {
							var payment_vch = jQuery.parseJSON(data.PAYMENTINFOR);																														
							$.ajax({
								url: '/session/sire/models/cfc/billing.cfc?method=DoAdminPurchasedCreditKeyword'+strAjaxQuery,
								async: true,
								type: 'post',
								dataType: 'json',
								data: {
									inpOrderAmount: amountpayment,
									inpPaymentdata: jsondata,
									inpPaymentGateway: paymentGateway,
									inpSelectUsedCard: selectusercard,
									inpSaveCCInfo: saveccinfo,
									inpUserId: userId
								},
								beforeSend: function(){
								},
								error: function(jqXHR, textStatus, errorThrown) {
									$("#processingPayment").hide();
									alertBox('Payment Transaction Error!','Purchase Payment Transaction',''); 
								},
								success: function(data) {											
									if(data.RXRESULTCODE == 1){
										updatePaymentTransaction(PAYMENTID,1);
									}
									else{
										$("#processingPayment").hide();
										$("#buyaddon-modal-sections").hide();
										alertBox(data.MESSAGE,'Purchase Payment Transaction','');                       
									}
								}
							});								
							
						} else {
							$("#processingPayment").hide();
							alertBox(data.MESSAGE,'Purchase Payment Transaction','');
						}                       
					}
				});
			});
		}
	});	

	function updatePaymentTransaction(PaymentId,Status){		
		$.ajax({
			url: '/session/sire/models/cfc/billing.cfc?method=UpdateStatusPaymentTransaction'+strAjaxQuery,
			async: true,
			type: 'post',
			dataType: 'json',
			data: {
				inpPaymentID:PaymentId,
				inpPaymentStatus: Status
			},
			beforeSend: function(){
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$("#processingPayment").hide();
				alertBox('Action Error!','Update Payment Transaction',''); 
			},
			success: function(data) {
				if (data.RXRESULTCODE == 1) {
					$("#processingPayment").hide();
					alertBox(data.MESSAGE,'Update Payment Transaction','');
					$("#buyaddon-modal-sections").hide();
					InitControl(); 
				} else {
					$("#processingPayment").hide();
					/*modalAlert('Add New Subscriber List', data.MESSAGE);*/
					alertBox(data.MESSAGE,'Update Payment Transaction','');
				}                       
			}
		});
	}

})(jQuery);