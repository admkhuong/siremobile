var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
fnGetReport = function (strFilter){
    $("#listReport").html('');    
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
    var detail = function (object) {
        var strReturn = "<a href='#' class='commission-detail' title='Detail' data-id='"+object.ID+"' data-rate='"+object.COMMISSION_RATE+"' data-name='"+object.AFFILIATE_NAME+"'><i class='fa fa-info-circle'></i></a>";        
        if(object.PAID_STATUS ==0){
            strReturn +='<a class="btn-payout" title="Pay out" data-id="'+object.ID+'" data-amount="'+object.COMMISSION_EARNED+'"  data-email="'+object.EMAIL+'" data-fromdate="'+object.FROMDATE+'" data-todate="'+object.TODATE+'" ><i class="fa fa-usd" style="font-size:15px" aria-hidden="true"></i></a>';                                    
        }
        else
        {
            strReturn +='<a class="btn-payment-detail" title="Payment detail" data-email="'+object.EMAIL+'" data-payout-batch-id="'+object.PAYOUT_BATCH_ID+'"><i class="fa fa-paypal" style="font-size:15px" aria-hidden="true"></i></a>';
        }
        return strReturn;
    }
    var paidStatus = function (object) {
        if(object.PAID_STATUS ==1){
            return "Close";
        }
        else
        {
            return "Open";
        }        
    }
    var colRevenue = function (object) {
        return "$ "+ object.REVENUE;       
    }
    var colCommissionRate = function (object) {
        return  object.COMMISSION_RATE +" %";       
    }
    var colCommissionEarn = function (object) {
        return "$ "+ object.COMMISSION_EARNED;       
    }
    
    

    var table = $('#listReport').dataTable({
        "bStateSave": false,            
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
                    
            {"mData": "AFFILIATE_NAME", "sName": 'Affiliate Name', "sTitle": 'Affiliate Name', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": "EMAIL", "sName": 'Paypal Email', "sTitle": 'Paypal Email', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": "AFFILIATE_CODE", "sName": 'Code', "sTitle": 'Code', "bSortable": true, "sClass": "", "sWidth": "180"},
            {"mData": "PAID_PERIOD", "sName": 'COM Period', "sTitle": 'COM Period', "bSortable": true, "sClass": "", "sWidth": "180"},
            
            {"mData": colRevenue, "sName": 'Revenue', "sTitle": 'Revenue', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": colCommissionRate, "sName": 'COM Rate', "sTitle": 'COM Rate', "bSortable": true, "sClass": "", "sWidth": "100"},
            {"mData": colCommissionEarn, "sName": 'COM Earned', "sTitle": 'COM Earned', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": paidStatus, "sName": 'Paid Status', "sTitle": 'Paid Status', "bSortable": true, "sClass": "", "sWidth": "100"},
            {"mData": "PAID_DATE", "sName": 'Paid Date', "sTitle": 'Paid Date', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": "PAID_CONFIRMATION", "sName": 'Paid Conf', "sTitle": 'Paid Conf', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": detail, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "", "sWidth": "120"},
        ],
        "sAjaxDataProp": "ReportList",            
        "sAjaxSource": '/public/sire/models/cfc/affiliate.cfc?method=GetCommissionReport'+strAjaxQuery,
        
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "customFilter", "value": customFilterData}                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);          
                    $(".div-commission-detail").addClass("hidden");                                                                                                                                                                        
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
        }
    });
}
fnGetDetail = function (commissionId, rate){
    $("#listDetail").html('');    
    
    var colCommission= function(object){
        return "$ "+(object.REVENUE * rate/100).toFixed(2);
    }
    var colRevenue = function (object) {
        return "$ "+ object.REVENUE;       
    }
    var table = $('#listDetail').dataTable({
        "bStateSave": false,            
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
        
            {"mData": "ID", "sName": 'ID', "sTitle": 'ID', "bSortable": true, "sClass": "", "sWidth": "100"},
            {"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": "PLAN", "sName": 'Plan', "sTitle": 'Plan', "bSortable": true, "sClass": "", "sWidth": "150"},
            {"mData": colRevenue, "sName": 'Revenue', "sTitle": 'Revenue', "bSortable": true, "sClass": "", "sWidth": "120"},
            {"mData": colCommission, "sName": 'Commission', "sTitle": 'Commission', "bSortable": true, "sClass": "", "sWidth": "120"},        
        ],
        "sAjaxDataProp": "ReportList",            
        "sAjaxSource": '/public/sire/models/cfc/affiliate.cfc?method=GetCommissionReportDetail'+strAjaxQuery,
        
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "inpCommissionId", "value": commissionId}                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    $(".div-commission-detail").removeClass("hidden");
                    fnCallback(data);                                                                                                                                                                                  
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
        }
    });
}
function GetAccessTokenKey()
{
    return new Promise( function (resolve, reject) {
        var cookieToken= getCookie("PayoutToken") ;
        if(cookieToken !="")
        {
            var data ={
                MESSAGE:"Sucess",
                RESULT: "Sucess",
                RXRESULTCODE:1,
                TOKENKEY: cookieToken
            };
            
            resolve(data);
        }
        else
        {
            $.ajax({
                type:"POST",
                url:'/session/sire/models/cfc/paypal-payout.cfc?method=GetAccessTokenKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType:'json',
                data:
                    {					
                        
                    }
                ,beforeSend:function(xhr)
                    {
                    $('#processingPayment').show()
                }
                ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                    var data ={MESSAGE:"Unable to get token key at this time. An error occurred."};
                    reject(data);
                    $('#processingPayment').hide()
                }
                ,success:function(data)
                    {
                    $('#processingPayment').hide();
                    if(data.RXRESULTCODE==1){						
                        setCookie("PayoutToken",data.TOKENKEY,data.EXPIRES);
                        resolve(data);
                    }
                    else
                    {
                        reject(data);
                    }					
                }
            }); 
        }
			
    });
}
function PaypalPayout(tokenKey,paymentData)
{
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/session/sire/models/cfc/paypal-payout.cfc?method=PaypalPayout&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpToken: tokenKey,
                    inpSenderBatchId:paymentData.inpSenderBatchId,
                    inpSubject:paymentData.inpSubject,
                    inpAmount:paymentData.inpAmount,
                    inpNotes:paymentData.inpNotes,
                    inpReceiver:paymentData.inpReceiver       
                }
            ,beforeSend:function(xhr)
                {
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to payment at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
function GetPaypalPaymentItemDetail(tokenKey,payoutBatchId)
{
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/session/sire/models/cfc/paypal-payout.cfc?method=GetPaypalPaymentItemDetail&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpToken: tokenKey,
                    inpPayoutBatchId:payoutBatchId
                }
            ,beforeSend:function(xhr)
                {
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to get payment detail at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}

function setCookie(cname, cvalue, exminutes) {
    var d = new Date();
    d.setTime(d.getTime() + exminutes);
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + (expires*1000) + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
$(document).ready(function(){    
    var StatusCustomData = function(){
		var options   = '<option value= "-100" selected>All</option>'+
		'<option value="0">Open</option>'+
		'<option value="1">Close</option>';
		return $('<select class="filter-form-control form-control"></select>').append(options);
	};
    $('#box-filter').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Affiliate Code', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' aca.Affiliate_code_vch	 '},

            {DISPLAY_NAME: 'Paid Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: 'acbp.PaymentStatus_ti', CUSTOM_DATA:StatusCustomData},
            {DISPLAY_NAME: 'Commission Month', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: 'acbp.FromDate_dt'},
            {DISPLAY_NAME: 'Commission Earned', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' acbp.TotalCommission_dec '},
        ],
        clearButton: true,
        // rowLimited: 5,
        error: function(ele){
                        
        },
        // limited: function(msg){
        //  alertBox(msg);
        // },
        clearCallback: function(){
            fnGetReport();
        }
        }, function(filterData){
            //called if filter valid and click to apply button
            fnGetReport(filterData);
    });  
    fnGetReport();
    $(document).on("click",".commission-detail",function(){
        var commissionId=  $(this).data("id");
        var rate=  $(this).data("rate");
        var name=  $(this).data("name");
        $('#listReport tr').removeClass("row-selected");
        $(this).closest("tr").addClass("row-selected");
        $("#commission-detail-tit").text("Affiliate Commission Detail for "+name);
        fnGetDetail(commissionId,rate);
    });
    $(document).on("click",".btn-payout",function(e){    
        e.preventDefault() ;        
        
        var receiverEmail= $(this).data("email");
        var amount= $(this).data("amount");
        var id= $(this).data("id");
        var fromDate= $(this).data("fromdate");
        var toDate= $(this).data("todate");
        title='Confirmation !';
        message='<b>Amount to be paid: $'+amount+'</br></br></b>';
        message+='Email Subject:</br><input type="text" maxlength="100" class="form-control validate[required,custom[noHTML]]" placeholder="" id="EmailSubject" value="Sire Affiliate Commission for [ '+fromDate+' - '+toDate+']"></br>';
        message+='Email Notes:</br><textarea maxlength="200" id="EmailNotes"  rows="5" class="text form-control validate[required]">Sire Affiliate Commission for [ '+fromDate+' - '+toDate+']</textarea>';

        bootbox.dialog({
            message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "Pay Now",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {  
                        var paymentData={
                            inpSenderBatchId:id,
                            inpSubject:$("#EmailSubject").val() || "Payment for commission [ "+fromDate+" - "+toDate+"]",
                            inpAmount:amount,
                            inpNotes:$("#EmailNotes").val().replace(/\n/ig, ". ") ||  "Payment for commission [ "+fromDate+" - "+toDate+"]",
                            inpReceiver:receiverEmail 
                        }                     
                        GetAccessTokenKey().then(function(data){    
                            
                            if(data.RXRESULTCODE==1){
                                                                                                
                                return PaypalPayout(data.TOKENKEY,paymentData).then(function(data){  
                                    fnGetReport();          
                                    alertBox(data.MESSAGE, 'Success');                              
                                }).catch(function(err){                                        
                                    alertBox(err.MESSAGE, 'Oops!');                                              
                                });               
                            }                                                      
                        }).catch(function(err){                                        
                            alertBox(err.MESSAGE, 'Oops!');                                   
                        });
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "green-cancel"
                },
            }
        });
        
        
    });
    $(document).on("click",".btn-payment-detail",function(e){                
                
        e.preventDefault() ;
        
        var payoutBatchId= $(this).data("payout-batch-id");      
        var email= $(this).data("email");      
        
        GetAccessTokenKey().then(function(data){      
            if(data.RXRESULTCODE==1){                
                return GetPaypalPaymentItemDetail(data.TOKENKEY,payoutBatchId);                
            }                                                      
        }).then(function(data){  
            var modalDetail=$("#mdPaymentDetail");
            modalDetail.find("#email-receive").text(email);
            modalDetail.find("#amount").text("$ "+data.PAYMENT_AMOUNT);
            modalDetail.find("#status").text(data.PAYMENT_STATUS);
            modalDetail.modal("show");
            return;
        }).catch(function(err){                                        
            alertBox(err.MESSAGE, 'Oops!');            
        });
    });
    
})