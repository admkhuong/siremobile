var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var start = moment().startOf('month');
var end = moment();

 

$('.reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    "dateLimit": {
        "years": 1
    },
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
    
}, GetReport);
function GetReport(start, end)
{
    MonthlyReferReport(start, end);
    AffiliateOverView(start, end);

}
function MonthlyReferReport(start, end) {
    
    $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
    dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
    $('input[name="dateStart"]').data('date-start', start);
    $('input[name="dateEnd"]').data('date-end', end);

    $.ajax({
        url: "/public/sire/models/cfc/affiliate.cfc?method=GetReferPeriod"+strAjaxQuery,
        method: "POST",
        dataType: "json",
        data: {                     
            inpStartDate: dateStart,
            inpEndDate: dateEnd,
            inpListUserId: $("#list-user-selected").val() || '',
            inpType: $("#dashboardType").val() || ''
        },
        success: function(data){
            var series_active = [];
            var series_new = [];
            var series_upgrade = [];
            var series_downgrade = [];
            var months = [];    
            //
            var active_acct_ref =0;
            var new_acct_ref =0;
            var upgrade_acct_ref =0;
            var downgrade_acct_ref =0;
            $(".set-chart").each(function(){
                if( !$(this).hasClass("col-lg-6"))
                {
                    $(this).addClass("col-lg-6");
                }
            });
            
            data.DataLabel.forEach(function(item){     
                if(data.DataPeriod =='year')                           
                {
                    months.push(moment(item).format('MMM/DD/YYYY'));     
                    $(".set-chart").removeClass("col-lg-6");
                }
                else if(data.DataPeriod=='month')
                {                    
                    $(".set-chart").removeClass("col-lg-6");
                    months.push(moment(item).format('MMM/DD/YYYY'));        
                }
                else if(data.DataPeriod=='week')
                {
                    months.push(moment(item).format('MMM/DD'));  
                }
                else
                {
                    months.push(moment(item).format('MM/DD'));                
                }
                
            });   

            data.DataListActive.forEach(function(item){                                                
                series_active.push({ value: item});    
                active_acct_ref+=    item;               
            });   
            data.DataListNew.forEach(function(item){                                
                series_new.push({value: item}); 
                new_acct_ref+=    item ;               
            });     
            
            data.DataListUpgrade.forEach(function(item){                                
                series_upgrade.push({value: item});      
                upgrade_acct_ref+=    item ;                         
            }); 

            data.DataListDowngrade.forEach(function(item){                                
                series_downgrade.push({value: item});    
                downgrade_acct_ref+=    item ;                           
            });

            $("#active-acct-ref").text(active_acct_ref);
            $("#new-acct-ref").text(new_acct_ref);
            $("#upgrade-acct-ref").text(upgrade_acct_ref);
            $("#downgrade-acct-ref").text(downgrade_acct_ref);

            var active_data = {labels: months,series:[ series_active]};
            var new_data = {labels: months,series:[ series_new]};
            var upgrade_data = {labels: months,series:[ series_upgrade]};
            var downgrade_data = {labels: months,series:[ series_downgrade]};
            /* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */
            var options = {
                //fullWidth: true,
                chartPadding: {
                    right: 0
                },
                axisY: {
                    showGrid: false,
                    offset: 20,
                    onlyInteger: true,
                    position: 'start',
                },
                axisX: { 
                    labelOffset: {
                      x: 1,
                      y: 1
                    }
                },
                plugins: [
                    Chartist.plugins.tooltip({
                        currency:"Referred: ",
                        class: 'chart-tooltip-custom',
                        appendToBody: true
                    })
                ],
                low: 0,
                height: '250px',
                lineSmooth: false,
                classNames: {
                    chart: 'ct-chart-line',
                    //label: 'ct-label-custom',
                    labelGroup: 'ct-labels',
                    series: 'ct-series',
                    line: 'ct-line',
                    point: 'ct-point',
                    area: 'ct-area',
                    grid: 'ct-grid-custom',
                    gridGroup: 'ct-grids',
                    gridBackground: 'ct-grid-background',
                    vertical: 'ct-vertical',
                    horizontal: 'ct-horizontal',
                    // start: 'ct-start',
                    // end: 'ct-end'
                 }
            };
                                    
            
            new Chartist.Line('.chart-active-referred', active_data, options);
            new Chartist.Line('.chart-new-referred', new_data, options);
            new Chartist.Line('.chart-upgrade-referred', upgrade_data, options);
            new Chartist.Line('.chart-upgrade-referred', upgrade_data, options);
            new Chartist.Line('.chart-downgrade-referred', downgrade_data, options);
        }
    });
}
function AffiliateOverView(start, end){
    var dashboardType= $("#dashboardType").val() || '';
    dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
    dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

    $.ajax({
        url: "/public/sire/models/cfc/affiliate.cfc?method=AffiliateOverView"+strAjaxQuery,
        type: 'post',
        dataType: 'json',
        data: {
            inpStartDate: dateStart,
            inpEndDate: dateEnd,
            inpListUserId: $("#list-user-selected").val() || '',
            inpType: dashboardType
        },
        beforeSend: function(){
            $('#processingPayment').show();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#processingPayment').hide();
            alertBox(errorThrown,'Oops');            
        },
        success: function(data) {
            $('#processingPayment').hide();
            if (data.RXRESULTCODE == 1) {     
                $("#earning-value").text("$" +data.TOTALEARNED_BYDATE);
                $("#total-revenue").text("$"+data.TOTALREVENUE_BYDATE);
                
                
                $("#total-affiliate-all").text(data.TOTALAFFILIATE).on("click",function(){
                    fnGetListAccountReferred1(data.LIST_TOTALAFFILIATE);                    
                    $("#referred-modal-title").text("Total Affiliates");
                    $("#mdListAccReferred").modal("show");                    
                });
                
                $("#new-affiliate").text(data.TOTALNEWAFFILIATE).on("click",function(){
                    fnGetListAccountReferred1(data.LIST_TOTALNEWAFFILIATE);                    
                    $("#referred-modal-title").text("Total New Affiliates");
                    $("#mdListAccReferred").modal("show");                    
                });

                $("#total-acct-all").text(data.TOTALREFERRED).on("click",function(){
                    fnGetListAccountReferred(data.LIST_TOTALREFERRED);
                    $("#referred-modal-title").text("Total Accounts Referred");                                        
                    $("#mdListAccReferred").modal("show");                    
                });
                $("#new-user").text(data.TOTALNEWREFERRED).on("click",function(){
                    fnGetListAccountReferred(data.LIST_TOTALNEWREFERRED);
                    $("#referred-modal-title").text("Total New Accounts Referred");                    
                    $("#mdListAccReferred").modal("show");                    
                });
                $("#total-acct-free").text(data.FREEREFERRED).on("click",function(){
                    fnGetListAccountReferred(data.LIST_FREEREFERRED);
                    $("#referred-modal-title").text("Free Accounts Referred");
                    $("#mdListAccReferred").modal("show");                    
                });
                $("#total-acct-paid").text(data.PAIDREFERRED).on("click",function(){
                    fnGetListAccountReferred(data.LIST_PAIDREFERRED);
                    $("#referred-modal-title").text("Paid Accounts Referred");
                    $("#mdListAccReferred").modal("show");
                });
            }            
        }
    });
}
//get list for user
fnGetUserList = function (strFilter){
    $("#adm-user-list").html('');    
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    var actionChk = function (object) {
        var strReturn = $('<input type="checkbox" class="chkItem" data-id="'+object.ID+'"/>')[0].outerHTML;
        return strReturn;
    }
    var status = function (object) {
        var strReturn = "Deactivated";
        if(object.STATUS==1){var strReturn = "Active"};
        return strReturn;
    }

    var table = $('#adm-user-list').dataTable({
        "bStateSave": false,            
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
        
            {"mData": actionChk, "sName": '', "sTitle": '<input type="checkbox" class="checkAll"/>', "bSortable": false, "sClass": "", "sWidth": "20"},
            {"mData": "ID", "sName": 'ID', "sTitle": 'ID', "bSortable": true, "sClass": "", "sWidth": "100"},
            {"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "300"},
            {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "bSortable": true, "sClass": "", "sWidth": "200"},                
            {"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "bSortable": true, "sClass": "", "sWidth": "150px"},  
            {"mData": "AMD", "sName": 'Registered Date', "sTitle": 'Registered Date', "bSortable": true, "sClass": "", "sWidth": "150px"},
            {"mData": "AFFILIATE_CODE", "sName": 'Affiliate Code', "sTitle": 'Affiliate Code', "bSortable": true, "sClass": "", "sWidth": "140px"}            
        ],
        "sAjaxDataProp": "UserList",            
        "sAjaxSource": '/public/sire/models/cfc/affiliate.cfc?method=GetUserList'+strAjaxQuery,
        
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "customFilter", "value": customFilterData}                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);        
                    // only rewrite at first load  
                    if($("#list-user-selected").val()!=="")
                    {
                        var listUserOfSegment=$("#list-user-selected").val().split(","); 
                        for (var i = 0; i < listUserOfSegment.length; i++) {                                            
                            $('.chkItem[data-id="'+listUserOfSegment[i]+'"]').prop("checked",true);
                        }
        
                        var chkItem = $('#adm-user-list .chkItem');
                        if (chkItem.length != 0 && chkItem.length == chkItem.filter(':checked').length) {
                            $('#adm-user-list .checkAll').prop("checked", true);
                        } else {
                            $('#adm-user-list .checkAll').prop("checked", false);
                        } 
                        $("#list-user-selected").trigger('change');
                    }                                                            
                                                                                                
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
        }
    });
}
fnGetListAccountReferred = function (listUser){    
    
    var dashboardType= $("#dashboardType").val() || '';
    $("#list-acc-referred").html('');        
    
    var status = function (object) {
        var strReturn = "Deactivated";
        if(object.STATUS==1){var strReturn = "Active"};
        return strReturn;
    }

    var table = $('#list-acc-referred').dataTable({
        "bStateSave": false,            
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
                    
            {"mData": "ID", "sName": 'ID', "sTitle": 'ID', "bSortable": true, "sClass": "", "sWidth": "110px"},
            {"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "300px"},
            {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "bSortable": true, "sClass": "", "sWidth": "200px"},                
            {"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "bSortable": true, "sClass": "", "sWidth": "150px"},  
            {"mData": "PLAN", "sName": 'Plan', "sTitle": 'Plan', "bSortable": true, "sClass": "", "sWidth": "150px"},  
            
            {"mData": "AFFILIATEDATEAPPLY", "sName": 'Referred Date', "sTitle": 'Referred Date', "bSortable": false, "sClass": "", "sWidth": "180px"},                
            
            {"mData": status, "sName": 'Status', "sTitle": 'Status', "bSortable": true, "sClass": "", "sWidth": "100px"}
            
        ],
        "sAjaxDataProp": "UserList",            
        "sAjaxSource": '/public/sire/models/cfc/affiliate.cfc?method=GetUserListByListId'+strAjaxQuery,
        
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "inpListUserId", "value": listUser}                
                                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {                    
                    fnCallback(data);                                                                                                                                                                                                         
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
            
        }
    });
}
fnGetListAccountReferred1 = function (listUser){    
    
    var dashboardType= $("#dashboardType").val() || '';
    $("#list-acc-referred").html('');        
    
    var status = function (object) {
        var strReturn = "Deactivated";
        if(object.STATUS==1){var strReturn = "Active"};
        return strReturn;
    }

    var table = $('#list-acc-referred').dataTable({
        "bStateSave": false,            
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
                    
            {"mData": "ID", "sName": 'ID', "sTitle": 'ID', "bSortable": true, "sClass": "", "sWidth": "110px"},
            {"mData": "NAME", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "300px"},
            {"mData": "EMAIL", "sName": 'Email', "sTitle": 'Email', "bSortable": true, "sClass": "", "sWidth": "200px"},                
            {"mData": "PHONE", "sName": 'Phone', "sTitle": 'Phone', "bSortable": true, "sClass": "", "sWidth": "150px"},  
            {"mData": "PLAN", "sName": 'Plan', "sTitle": 'Plan', "bSortable": true, "sClass": "", "sWidth": "150px"},                          

            {"mData": "AFFILIATEDATEREGISTER", "sName": 'Registered Date', "sTitle": 'Registered Date', "bSortable": true, "sClass": "", "sWidth": "200px"},
            {"mData": "AFFILIATECODEREGISTER", "sName": 'Affiliate Code', "sTitle": 'Affiliate Code', "bSortable": true, "sClass": "", "sWidth": "200px"},                        
            
        ],
        "sAjaxDataProp": "UserList",            
        "sAjaxSource": '/public/sire/models/cfc/affiliate.cfc?method=GetUserListByListId'+strAjaxQuery,
        
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(                
                { "name": "inpListUserId", "value": listUser}                
                                
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {                    
                    fnCallback(data);                                                                                                                                                                                                         
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
            
        }
    });
}
function UpdateAffiliateCode(affiliateId,affiliatecode){
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/public/sire/models/cfc/affiliate.cfc?method=SaveAffiliate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {			
                    inpId:affiliateId,		
                    inpAffiliateCode:affiliatecode						
                }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                $('#processingPayment').hide()
                var data ={MESSAGE:"Unable to update your Affiliate Code at this time. An error occurred."};
                reject(data);
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
function UpdateUserInfo(userInfo)
{
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/public/sire/models/cfc/userstools.cfc?method=UpdateUserInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpAddress:userInfo.address,
                    inpCity:userInfo.city,
                    inpState:userInfo.state,
                    inpZip:userInfo.zip,
                    inpSSNTAX:userInfo.taxid,
                    inpPaypalEmail:userInfo.paypalEmail
                }
            ,beforeSend:function(xhr)
                {
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to update your infomation at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
function CreateAffiliateCode(affiliatecode){
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/public/sire/models/cfc/affiliate.cfc?method=SaveAffiliate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpAffiliateCode:affiliatecode						
                }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                $('#processingPayment').hide()
                var data ={MESSAGE:"Unable to create your Affiliate Code at this time. An error occurred."};
                reject(data);
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}	
function validate_form()
{		
    $("#signup_form").validationEngine({promptPosition : "topLeft", scroll: false});	
    if($("#signup_form").validationEngine('validate'))
        {
        var actionType = $("#actionType").val() || 1; // <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
        var bbSignUpTitle="Sign Up";
        var accountType='personal';
        var fname=$("#fname").val();
        var lname=$("#lname").val();

        var address= $('#address').val() || "";
        var city= $('#city').val() || "";
        var state= $('#state').val() || "";
        var zip= $('#zip').val() || "";
        var taxid= $('#taxid').val() || "";
        var paypalEmail= $('#paypalemailadd').val() || "";			
        
        var affiliatetype=$('#affiliatetype').val() || "0"; 
        var affiliatecode= $('#affiliatecode').val() || "";            
        var ami=$('#AMI').val() || 0; // this is user id off user that we apply affiliate code (Affiliate Marketing Id)      

        try{					
                if(actionType==4){									
                    // have alr signup, only update some compulsory infor and request affiliate code
                    var userInfo = {							
                        fname:fname,
                        lname:lname,                       						
                        address:address,
                        city:city,
                        state:state,
                        zip:zip,
                        taxid:taxid,
                        paypalEmail:paypalEmail							
                    };
                                            
                    UpdateUserInfo(userInfo).then(function(data){
                        if(data.RXRESULTCODE ==1){
                            return CreateAffiliateCode(affiliatecode);
                        }							
                    }).then(function(data){
                        if(data.RXRESULTCODE ==1){
                            location.href="/session/sire/pages/affiliates-dashboard";
                        }	
                    }).catch(function(error){	
                                        
                        bootbox.dialog({
                            message:error.MESSAGE,title:"Oops!",
                            buttons:{
                                success:{
                                    label:"OK",className:"btn btn-success btn-success-custom",callback:function(){

                                    }
                                }
                            }
                        });
                    });
                }                					
        }
        catch(ex)
            {
            console.log(ex);
            $('#processingPayment').hide();
            bootbox.dialog(
                {
                message:"Sign Up Fail",title:bbSignUpTitle,buttons:
                    {
                    success:
                        {
                        label:"OK",className:"btn btn-success btn-success-custom",callback:function()
                            {
                        }
                    }
                }
            }
            );
            btnSignUp.prop('disabled',false);
            return false
        }
    }
}
$(document).ready(function(){    
    $(".div-affiliate-commission").height( $(".div-affiliate-link").height());
    $( window ).resize(function() {
        $(".div-affiliate-commission").height( $(".div-affiliate-link").height());
    });
    GetReport(start, end);            
    $(".affiliate-link-copy").on("click",function(e){
        e.preventDefault();
        $("#affiliate-link").select();                              
        document.execCommand("Copy");             
        $(this).html("Link was copied");
    });
    // for usser list 
    fnGetUserList();
    // filter
    $('#box-filter').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' MFAContactString_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' tmpTable.UserId_int '},
            {DISPLAY_NAME: 'Affiliate Code', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' amc_vch	 '}
        ],
        clearButton: true,
        // rowLimited: 5,
        error: function(ele){
            // console.log(ele);
        },
        // limited: function(msg){
        //  alertBox(msg);
        // },
        clearCallback: function(){
            fnGetUserList();
        }
        }, function(filterData){
            //called if filter valid and click to apply button
            fnGetUserList(filterData);
    });       
    $('#adm-user-list').on('click', '.checkAll', function () {            
        $("#adm-user-list tbody tr").find(".chkItem").prop("checked",this.checked);
        var listReturn="";
        var listChecked=[];
        $(".chkItem").each(function(){
            if($(this).is(":checked"))
            {
                listChecked.push($(this).attr("data-id"));
            }
        });
        if(listChecked.toString()=="")
        {
            listReturn="";
        }
        else
        {
            var currentList=$("#list-user-selected").val().split(",");
            for (var i =0 ; i< listChecked.length; i++) {
                var index=currentList.indexOf(listChecked[i]);     
                if(index < 0)
                {
                    currentList.push(listChecked[i]);                    
                }
            }  
            listReturn=currentList.toString();              
        }
        $("#list-user-selected").val(listReturn);
        $("#list-user-selected").trigger('change');
    });   
    // for check each item
    var admUserListDatatable= $("#adm-user-list").dataTable();
    $(admUserListDatatable).on( "click",".chkItem", function(e) {   
        var id= $(this).data("id").toString();
        var listID= $("#list-user-selected").val().split(',');                                

        var index=listID.indexOf(id);                   
        if(this.checked)
        {                
            if(index < 0)
            {
                listID.push(id);                    
            }
        }
        else
        {
             listID.splice(index, 1);
        }
        $("#list-user-selected").val(listID.toString()); 

        var chkItem = $('#adm-user-list .chkItem');
        if (chkItem.length != 0 && chkItem.length == chkItem.filter(':checked').length) {
            $('#adm-user-list .checkAll').prop("checked", true);
        } else {
            $('#adm-user-list .checkAll').prop("checked", false);
        }
        $("#list-user-selected").trigger('change');
    });  
    //
    $("#list-user-selected").on("change",function(){
        if($(this).val().length > 0)
        {
            $(".all-dashboard").removeClass("hidden");            
        }
        else
        {
            $(".all-dashboard").addClass("hidden");            
        }
        GetReport(start, end);            
    });
    // sign up
    $("#sms_number").mask("(000)000-0000");
	$("#signup_form_1").validationEngine(
		{
		promptPosition:"topLeft",scroll:false,focusFirstField:true
	}
	);
	$("#btn-new-signup").on('click',function(event)
		{
            event.preventDefault();
            validate_form()
	    }
    );
    $(".affiliate-code-update").on('click',function(){
        var affiliateId= $("#affiliateId").val();
        var affiliatecode= $("#affiliate-code").val();
        var newURL = window.location.protocol + "//" + window.location.host + "/signup-affiliates?ami="+ affiliatecode ;
                
        if(affiliatecode !==''){
            UpdateAffiliateCode(affiliateId,affiliatecode).then(function(data){
                if(data.RXRESULTCODE==1){
                    alertBox("Update Affiliate Code successfully","Success");
                    $("#affiliate-link").val(newURL);
                }
                else
                {
                    alertBox(data.MESSAGE,"Oops!");
                }
            }).catch(function(data){
                alertBox(data.MESSAGE,"Oops!");
            })
        }
        
    });
    $('.modal')
    .on('show.bs.modal', function (){
        var md = new MobileDetect(window.navigator.userAgent);
    
        if(md.phone() !== null || md.tablet() !== null){
            if(!$("body").hasClass('startFreeOpen1')){
                $("body").addClass('startFreeOpen1')
            }
        }
    })
    .on('hide.bs.modal', function (){
        var md = new MobileDetect(window.navigator.userAgent);
    
        if(md.phone() !== null || md.tablet() !== null){
            if($("body").hasClass('startFreeOpen1')){
                $("body").removeClass('startFreeOpen1')
            }
        }
    });
});