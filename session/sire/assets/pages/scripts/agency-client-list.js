$(function()
{
     Validation();
     ValidationFully();
     EditValidation();

     $('#phone-number').on('keyup',function(event)
     {
          $("#phone-number").mask("(000)000-0000")
     });

     $('.add-client-button-fully').on('click',function(event)
     {
          AddClientAjaxFully();
     });

     $('.add-client-button-semi').on('click',function(event)
     {
          AddClientAjaxSemi();
     });

     $('#phone-number-edit').on('keyup',function(event)
     {
          $("#phone-number-edit").mask("(000)000-0000")
     });

     $('.update-client-button').on('click',function(event)
     {
          EditDetailsAjax();
     });

     $('.send-reset-button').on('click',function(event)
     {
          SendResetEmail();
     })

     $('#new-user').on('click', function(event)
     {
          AddUserType();
     })

     $('#password-input-fully').on('change', function(event)
     {
          if($(this).is(':checked'))
          {
               $('#password-fully').val(GeneratePassword());
               $('#password-fully').attr('disabled', true);
               CheckFieldsFully();

          }
          else
          {
               $('#password-fully').val("");
               CheckFieldsFully();
               $('#password-fully').attr('disabled', false);
          }
     })

     $('#password-input-semi').on('change', function(event)
     {
          if($(this).is(':checked'))
          {
               $('#password-semi').val(GeneratePassword());
               $('#password-semi').attr('disabled', true);
          }
          else
          {
               CheckValidation("Password",0);
               $('#password-semi').val("");
               $('#password-semi').attr('disabled', false);
          }
     })


	var Table = $('#client-list-detail').dataTable(
          {
		"bStateSave": false,
		"iStateDuration": -1,
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"aaSorting": [[ 0, "desc" ]],
		"pagingType": "simple_numbers",
	     "bLengthChange": false,
		"iDisplayLength": 15,
		"bAutoWidth": false,
	     "aoColumns": [
                         { "sTitle": 'User ID',"bVisible": false, "bSortable": false, "sClass": ""},
                         { "sTitle": 'Company Name',"bVisible": false, "bSortable": false, "sClass": ""},
                         { "sTitle": 'First Name', "bSortable": true, "sClass": "", "sWidth": "15%"},
                         { "sTitle": 'Last Name', "bSortable": true, "sClass": "", "sWidth": "15%"},
                         { "sTitle": 'Phone Number', "bSortable": true, "sClass": "", "sWidth": "20%"},
                         { "sTitle": 'Email Address', "bSortable": true, "sClass": "", "sWidth": "20%"},
                         { "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "18%"},
                         { "sTitle": 'Status', "bSortable": true, "sClass": "", "sWidth": "12%",
                         "mRender": function (data, type, full)
                              {
                                   let ClientStatus = '';
                                   if (parseInt(full[7]) === 0)
                                   {
                                        ClientStatus = 'Inactive';
                                   }
                                   else
                                   {
                                        ClientStatus = 'Active';
                                   }
                                   return ClientStatus;
                              },
                         "createdCell": function (td, cellData, rowData, row, col)
                              {
                                   if (cellData === 0 )
                                   {
                                        $(td).css('color','red');
                                   }
                              }
                         },
                         { "mData": null, "sTitle": 'Actions', "bSortable": false, "sClass": "", "sWidth": "0%",
                         "mRender": function (data, type, full)
                              {
                                   let Status = null;
                                   let StatusButton = null;
                                   const ReportButton = '<li><a class="btn-report"><i class="fa fa-bar-chart"></i>Account Report</a></li>';
                                   const BillingButton = '<li><a class="btn-billing"><i class="fa fa-dollar"></i>Billing</a></li>';
                                   const ImpersonateButton = '<li><a class="btn-impersonate"><i class="fa fa-sign-in"></i>Login As</a></li>';
                                   const EditButton = '<li><a class="btn-edit" data-toggle="modal" data-target="#edit-modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></li>';
                                   const ResetPassword = '<li><a class="btn-reset-password" ><i class="fa fa-key"></i>Reset Password</a></li>';
                                   // Check the activation status for a user
                                   if (parseInt(full[7]) === 1)
                                   {
                                        Status = 'Deactivate';
                                        StatusButton = '<li><a class="btn-active-toggle"><i class="fa fa-lock" aria-hidden="true"></i>' + Status + '</a></li>';
                                   }
                                   else
                                   {
                                        Status = 'Activate';
                                        StatusButton = '<li><a class="btn-active-toggle"><i class="fa fa-unlock" aria-hidden="true"></i>' + Status + '</a></li>';
                                   }
                                   return '<div class="dropdown action-cp-dropdown-list"><button id="action-dropdown" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button><ul class="dropdown-menu">'+ ImpersonateButton + ReportButton + BillingButton + EditButton + ResetPassword + StatusButton +'</ul></div>';
                              }
                         }
		],
		"sAjaxDataProp": "aaData",
		"sAjaxSource": '/session/sire/models/cfc/agency.cfc?method=GetAgencyClients&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
          "fnCreatedRow": function( row, data, dataIndex )
          {
              // for each row look for the btn-active-toggle button
               $(row).find('.btn-active-toggle').each(function(id, el)
               {
               // Setup a click function call for each row
                 $(this).click(function()
                 {
                    if (parseInt(data[7]) === 1 )
                    {
                         DeactivateClientPrompt(data);
                    }
                    else
                    {
                         ToggleStatus(data, 'ActivateClient');
                    }
                 });
               });

               $(row).find('.btn-edit').each(function(id, el)
               {
               // Setup a click function call for each row
                 $(this).click(function()
                 {
                    EditValidInputCounter = 0;
                    EditDetails(data);
                    EditValidatorObj =
                    {
                         FirstName : 0,
                         LastName: 0,
                         PhoneNumber: 0,
                         EmailAddress: 0
                    };
                    $('.update-client-button').attr('disabled', true);
                 });
               });

               $(row).find('.btn-reset-password').each(function(id, el)
               {
                 $(this).click(function()
                 {
                      ResetPasswordPrompt(data);
                 });
               });

               $(row).find('.btn-impersonate').each(function(id, el)
               {
                 $(this).click(function()
                 {
                      let UserId = data[0];
                      ImpersonateAjax(UserId);
                 });
               });
          },
		"fnServerData": function (sSource, aoData, fnCallback, oSettings) {

             // this fn is used for filtering data
	        // aoData.push( { "name": "inpStart", "value": ""} );
		   // aoData.push( { "name": "inpEnd", "value": ""} );

	        oSettings.jqXHR = $.ajax({
		        "dataType": 'json',
		        "type": "POST",
		        "url": sSource,
		        "data": aoData,
                  "success": function(data)
                  {
                       fnCallback(data);
		        }
	      	});
        },
	});

});

function ToggleStatus(Data, Action, Type)
{
     $.ajax({
       type: 'POST',
       url: '/session/sire/models/cfc/agency.cfc?method=' + Action,
       dataType: "json",
       data:
       {
            UserId: Data[0],
            DeactivationType: Type
       },
       async: true,
       success: function(DataOut)
       {
            if ( DataOut.RXRESULTCODE == 1 )
            {
                 $('#client-list-detail').dataTable().fnDraw(false);
            }
            else
            {
                 bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                 {
                      message: " " + DataOut.MESSAGE,
                      title: "Failed to update client's status",
                      buttons:
                      {
                           success:
                           {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                           }
                      }
                 });
            }
       }
     })
}

function AddClientAjaxFully()
{
     $.ajax({
       type: 'POST',
       url: '/session/sire/models/cfc/agency.cfc?method=AddClientToAgencyFully&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
       dataType: "json",
       data: {
               inpFName: $('#first-name-fully').val(),
               inpLName: $('#last-name-fully').val(),
               inpCompanyName: $('#company-name-fully').val(),
               inpPassword: $('#password-fully').val()
             },
       async: true,
       error: function(XMLHttpRequest, textStatus, errorThrown)
       { /* No result returned */
            bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
            $(".add-client-button-fully").prop("disabled",false);
       },
       success: function(DataOut)
       {
            $('#client-list-detail').dataTable().fnDraw(false);
            $('#fully-signup-modal').modal('hide');
            $('input').val('');

            if (DataOut.RXRESULTCODE == 1)
            {
                 $('.add-client-button-fully').prop('disabled',false);
                 return;
            }
            else
            {
                 // bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                 {
                      message: " " + DataOut.MESSAGE,
                      title: "User didn't add successfully",
                      buttons:
                      {
                           success:
                           {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                           }
                      }
                 });

                 $(".add-client-button-fully").prop("disabled",false);
            }
       }
     });
}

function AddClientAjaxSemi()
{
     $.ajax({
       type: 'POST',
       url: '/session/sire/models/cfc/agency.cfc?method=AddClientToAgencySemi&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
       dataType: "json",
       data:
       {
            inpFName: $('#first-name-semi').val(),
            inpLName: $('#last-name-semi').val(),
            inpPhoneNumber: $('#phone-number').val(),
            inpEmailAddress: $('#email-address').val(),
            inpPassword: $('#password-semi').val()
       },
       async: true,
       error: function(XMLHttpRequest, textStatus, errorThrown)
       {
       /* No result returned */
       bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
       $(".add-client-button").prop("disabled",false);
       },
       success: function(DataOut)
       {
            $('#client-list-detail').dataTable().fnDraw(false);
            $('#semi-signup-modal').modal('hide');
            $('input').val('');

            if (DataOut.RXRESULTCODE == 1)
            {
                 $('.add-client-button').prop('disabled',false);
                 return;
            }
            else
            {
                 bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                 {
                      message: " " + DataOut.MESSAGE,
                      title: "User didn't add successfully",
                      buttons:
                      {
                           success:
                           {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                           }
                      }
                 });

                 $(".add-client-button").prop("disabled",false);
            }
       }
     });
}

function EditDetails(Data)
{
     $('#edit-modal').data("selected-user-id", Data[0]);
     $('#first-name-edit').val(Data[2]);
     $('#last-name-edit').val(Data[3]);
     $('#phone-number-edit').val(Data[4]);
     $('#company-name-edit').val(Data[1])
}

function EditDetailsAjax()
{
     $.ajax({
       type: 'POST',
       url: '/session/sire/models/cfc/agency.cfc?method=EditClientDetails&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
       dataType: "json",
       data:
       {
            inpUserId: $('#edit-modal').data("selected-user-id"),
            inpFName: $('#first-name-edit').val(),
            inpLName: $('#last-name-edit').val(),
            inpPhoneNumber: $('#phone-number-edit').val(),
            inpCompanyName: $('#company-name-edit').val()
            // inpEmailAddress: $('#email-address-edit').val()
       },
       async: true,
       error: function(XMLHttpRequest, textStatus, errorThrown)
       { /* No result returned */
            bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
            $(".edit-client-button").prop("disabled",false);
       },
       success: function(DataOut)
       {
            $('#edit-modal').modal('hide');
            $('input').val('');
            $('#client-list-detail').dataTable().fnDraw(false);

            if (DataOut.RXRESULTCODE == 1)
            {
                 $('.edit-client-button').prop('disabled',false);
                 return;
            }
            else
            {
                 bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                 {
                      message: " " + DataOut.MESSAGE,
                      title: "Edit Failed",
                      buttons:
                      {
                           success:
                           {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                           }
                      }
                 });

                 $(".edit-client-button").prop("disabled",false);
            }
       }
     });
}

const EmailRegex = /^([\w-\+.]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

// takes a password string, use val() when passing in password input field
function ValidatePassword( Password )
{
     let PasswordIsValid = false;
     let AlphaRegex = /[a-z]/i;
     let NumberRegex = /\d/;

     if ( Password.length > 7 && AlphaRegex.test(Password) && NumberRegex.test(Password) )
     {
          PasswordIsValid = true;
     }
     return PasswordIsValid;
}

var ValidationMessage =
{
     FirstName : "Please enter a first name",
     LastName: "Please enter a last name",
     PhoneNumber: "Please enter a valid phone number",
     EmailAddress: "Please enter a valid email address",
     Password: "Password must be at least 8 characters and contain at least 1 number and 1 letter",
     ConfirmPassword: "Passwords do not match"
}

function Validation()
{
     $('#first-name-semi').on('keyup',function(event)
     {
          if ( $(this).val().length != 0 )
          {
               CheckValidation("FirstName",1);
          }
          else
          {
               $('.validation-message').text(ValidationMessage.FirstName);
               CheckValidation("FirstName",0);
          }
     })

     $('#last-name-semi').on('keyup',function(event)
     {
          if ( $(this).val().length != 0 )
          {
               CheckValidation("LastName",1);
          }
          else
          {
               $('.validation-message').text(ValidationMessage.InpLastName);
               CheckValidation("LastName",0);
          }
     })

     $('#phone-number').on('keyup',function(event)
     {
          if ( $(this).val().length === 13 )
          {
               CheckValidation("PhoneNumber",1);
          }
          else
          {
               $('.validation-message').text(ValidationMessage.PhoneNumber);
               CheckValidation("PhoneNumber",0);
          }
     })

     $('#email-address').on('keyup',function(event)
     {
          if ( EmailRegex.test( $(this).val() ) )
          {
               CheckValidation("EmailAddress",1);
          }
          else
          {
               $('.validation-message').text(ValidationMessage.EmailAddress);
               CheckValidation("EmailAddress",0);
          }
     })

     $('#password-semi').on('keyup',function(event)
     {
          if (ValidatePassword( $(this).val() ))
          {
               CheckValidation("Password",1);
          }
          else
          {
               $('.validation-message').text(ValidationMessage.Password);
               CheckValidation("Password",0)
          }
     })

}


/* Validates the input fields first name, last name, password for the fully managed account creation form. Is not using validator structure because fewer fields are needed. */

function CheckFieldsFully()
{
     if ( $('#first-name-fully').val().length != 0  && $('#last-name-fully').val().length != 0 )
     {
          if( ValidatePassword($('#password-fully').val()) )
          {
               $('.add-client-button-fully').attr('disabled', false);
               $('.validation-message').text("");
          }
          else
          {
               $('.validation-message').text(ValidationMessage.Password);
               $('.add-client-button-fully').attr('disabled', true);
          }
     }
     else
     {
          $('.add-client-button-fully').attr('disabled', true);
     }
}

// Validation for fully managed accounts creation form
function ValidationFully()
{
     $('#first-name-fully').on('keyup',function(event)
     {
          CheckFieldsFully();
     })

     $('#last-name-fully').on('keyup',function(event)
     {
          CheckFieldsFully();
     })

     $('#password-fully').on('keyup',function(event)
     {
          CheckFieldsFully();
     })
}

var ValidInputCounter = 0;

var Validator =
{
     FirstName : 0,
     LastName: 0,
     PhoneNumber: 0,
     EmailAddress: 0,
     Password: 0
};

function CheckValidation(Label,Status)
{
     if (Status === 1 && Validator[Label] === 0)
     {
          Validator[Label] = 1;
          $('.validation-message').text("");
          ValidInputCounter ++;
     }
     else if (Status === 0 && Validator[Label] === 1)
     {
          Validator[Label] = 0;
          ValidInputCounter --;
     }

     if (ValidInputCounter === 5)
     {
          $('.add-client-button-semi').attr('disabled', false);
     }
     else
     {
          $('.add-client-button-semi').attr('disabled', true);
     }
}

function EditValidation()
{
     $('#first-name-edit').on('keyup', function(event)
     {
          if ( $(this).val().length != 0 )
          {
               CheckEditValidation("FirstName", 1);
          }
          else
          {
               CheckEditValidation("FirstName", 0);
          }
     })

     $('#last-name-edit').on('keyup',function(event)
     {
          if ( $(this).val().length != 0 )
          {
               CheckEditValidation("LastName", 1);
          }
          else
          {
               CheckEditValidation("LastName", 0);
          }
     })

     $('#phone-number-edit').on('keyup',function(event)
     {
          if ( $(this).val().length === 13 )
          {
               CheckEditValidation("PhoneNumber", 1);
          }
          else
          {
               CheckEditValidation("PhoneNumber", 0);
          }
     })

     $('#email-address-edit').on('keyup',function(event)
     {
          if ( EmailRegex.test( $(this).val() ) )
          {
               CheckEditValidation("EmailAddress", 1);
          }
          else
          {
               CheckEditValidation("EmailAddress", 0);
          }
     })
}

var EditValidInputCounter = 0;

var EditValidatorObj =
{
     FirstName : 0,
     LastName: 0,
     PhoneNumber: 0,
     EmailAddress: 0
}

function CheckEditValidation(Label, Status)
{
     if (Status === 1 && EditValidatorObj[Label] === 0)
     {
          EditValidatorObj[Label] = 1;
          ++EditValidInputCounter;
     }
     else if ( Status === 0 && EditValidatorObj[Label] === 1 )
     {
          EditValidatorObj[Label] = 0;
          --EditValidInputCounter;
     }

     if ( EditValidInputCounter > 0 )
     {
          $('.update-client-button').attr('disabled', false);
     }
     else
     {
          $('.update-client-button').attr('disabled', true);
     }
}

function DeactivateClientPrompt(Data)
{
     bootbox.dialog
     ({
          title: 'Select a deactivation option',
          message: "<p>To deactivate a client’s Sire mobile account in addition to their agency account, select all.<br> To deactivate only agency features while keeping a user’s Sire account, select Agency.</p>",
          buttons:
          {
               cancel:
               {
                    label: "Cancel",
                    className: 'btn-danger'
               },
               deactivateAgency:
               {
                    label: "Agency ",
                    className: 'btn-success',
                    callback: function()
                    {
                         ToggleStatus(Data, 'DeactivateClient', '0');
                    }
               },
               deactivateAll:
               {
                    label: "All",
                    className: 'btn-success',
                    callback: function()
                    {
                         ToggleStatus(Data, 'DeactivateClient', '1');
                    }
               }
          }
     });
}

//Prompts reset password request confirmation
function ResetPasswordPrompt(Data)
{
     let ClientName = Data[2] + ' ' + Data[3];
     bootbox.dialog
     ({
          message: "Are you sure you want to reset the password for" + " " + `<b>${ClientName}</b>` + "?",
          buttons:
          {
               cancel:
               {
                    label: "Cancel",
                    className: 'btn-danger'
               },
               confirm:
               {
                    label: "Confirm",
                    className: 'btn-success',
                    callback: function()
                    {
                         ResetPasswordEmailPrompt(Data[0], Data[5]);
                    }
               }
          }
     });
}

//prompts a target email in order to reset the password
function ResetPasswordEmailPrompt(UserId, ClientEmailAddress)
{
     $('#password-reset-modal').data("user-id", UserId);
     $('#password-reset-modal').modal('toggle');
     $('#reset-email').val(ClientEmailAddress);
     if ( EmailRegex.test( $('#reset-email').val() ) )
     {
          $('.send-reset-button').prop("disabled", false);
     }
     else
     {
          $('.send-reset-button').prop("disabled", true);
     }
     $('#reset-email').on('keyup',function(event)
     {
          if ( EmailRegex.test( $(this).val() ) )
          {
               $('.send-reset-button').prop("disabled", false);
          }
          else
          {
               $('.send-reset-button').prop("disabled", true);
          }
     });
}

function SendResetEmail()
{
     $.ajax({
       type: 'POST',
       url: '/session/sire/models/cfc/agency.cfc?method=ResetClientPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
       dataType: "json",
       data:
       {
            inpUserId: $('#password-reset-modal').data("user-id"),
            inpEmailAddress: $('#reset-email').val()
       },
       async: true,
       error: function(XMLHttpRequest, textStatus, errorThrown)
       { /* No result returned */
          bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
          $(".send-reset-button").prop("disabled",false);
       },
       success: function(DataOut)
       {
            $('#password-reset-modal').modal('hide');
            if (DataOut.RXRESULTCODE == 1)
            {
                 $('.send-reset-button').prop('disabled',false);
                 return;
            }
            else
            {
                 bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                 {
                      message: " " + DataOut.MESSAGE,
                      title: "Reset email didn't send successfully.",
                      buttons:
                      {
                           success:
                           {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                           }
                      }
                 });
                 $(".send-reset-button").prop("disabled",false);
            }
       }
     });
}

function ImpersonateAjax(UserId)
{
     $.ajax({
       type: 'POST',
       url: '/public/sire/models/cfc/userstools.cfc?method=ImpersonateAgencyClient&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
       dataType: "json",
       data:
       {
            inpUserId: UserId
       },
       async: true,
       error: function(XMLHttpRequest, textStatus, errorThrown)
       { /* No result returned */
            bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
       },
       success: function(DataOut)
       {
            if (DataOut.RXRESULTCODE == 1)
            {
                 location.reload(true);
                 return;
            }
            else
            {
                 bootbox.alert(DataOut.MESSAGE, function() {});
                 bootbox.dialog(
                      {
                           message: " " + DataOut.MESSAGE,
                           title: "Impersonation failed.",
                           buttons:
                           {
                                success:
                                {
                                     label: "Ok",
                                     className: "btn btn-medium btn-success-custom",
                                     callback: function() {}
                                }
                           }
                      });
            }
       }
     });
}

function AddUserType()
{
     bootbox.dialog
     ({
          title: 'Select an account type',
          message: "<p>If you expect your client to login to their Sire account, please select <b>Semi-managed</b>, otherwise select <b>Fully managed</b>.</p>",
          buttons:
          {
               cancel:
               {
                    label: "Cancel",
                    className: 'btn-danger'
               },
               semiManaged:
               {
                    label: "Semi-managed",
                    className: 'btn-success',
                    callback: function()
                    {
                         $('#password-input-semi').prop('checked', true);
                         $('#semi-signup-modal').modal('toggle');
                         $('#password-semi').val(GeneratePassword());
                         $('#password-semi').attr('disabled', true);
                    }
               },
               fullyManaged:
               {
                    label: "Fully managed",
                    className: 'btn-success',
                    callback: function()
                    {
                         $('#password-input-fully').prop('checked', true);
                         $('#fully-signup-modal').modal('toggle');
                         $('#password-fully').val(GeneratePassword());
                         $('#password-fully').attr('disabled', true);
                    }
               }
          }
     });
}

function GeneratePassword()
{
     let NumberRegex = /\d/;
     let Password = "";

     Password = Math.random().toString(36).substr(2,8);
     if (NumberRegex.test(Password) )
     {
          CheckValidation("Password", 1);
          return Password;
     }
     else
     {
        return GeneratePassword();
     }
}
