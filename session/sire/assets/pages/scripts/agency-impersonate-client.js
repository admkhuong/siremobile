(function($){

	var formImp = $('form[name="frm-impersonate"]');
	// $("SELECT").select2( { theme: "bootstrap", width: 'auto'} );
	$("select[name='user-id']").select2({
  		placeholder: "Search by email or id",
  		allowClear: true,
  		ajax: {
		    url: "/public/sire/models/cfc/userstools.cfc?method=GetAgencyClientList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      	return {
			        inpEmailAddress: params.term, // search term
			        page: params.page
		      	};
		    },
		    processResults: function (data, params) {
		   		return {
					results: $.map(data.USERLIST, function (item) {
	                    return {
	                        text: item.EMAIL,
	                        id: item.ID
	                    }
	                })
		      	};
		    },
		    cache: true
	  	},
		minimumInputLength: 1,
		theme: "bootstrap",
		width: 'auto'
  	});


  	if(formImp.length){
  		formImp.validationEngine('attach',  {promptPosition : "topRight:200", showArrow: true});
  	}


  	formImp.on('submit', function(event){
  		event.preventDefault();
  		if($(this).validationEngine('validate')){
  			$('#btn_SendNow').prop("disabled", true).promise().done(function(){
  				window.location = "agency-impersonate-client?IUserId=" + $("#user-id").val();
  			});
  		}
  	});


})(jQuery);
