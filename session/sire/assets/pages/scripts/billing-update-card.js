

(function($){

    function loadExpirationDate(){

        var expiration_date = $('#expiration_date').val();

        if(expiration_date != ''){
            var n = expiration_date.search("/");
            if( n > 0)
            {
                var res = expiration_date.split("/");
                var month = res[0];
                var year = res[1];

                $('#expiration_date_month').val(month);
                $('#expiration_date_year').val(year);
            }
        }
    }
    

    function update_expiration_date() {
        $('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
    }


    var my_plan = $("#my_plan_form");

    $('#expiration_date_month, #expiration_date_year').change(function(){
        update_expiration_date();
    });

        
    my_plan.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

    
    jQuery(document).ready(function($) {
        if($('#fset_cardholder_info').is(':visible')){
            $('.btn-update-profile').prop('disabled', false);
        }
        else
            $('.btn-update-profile').prop('disabled', true);
    });
    // cookie    
    function setCookie(cname, cvalue, exminutes) {
        var d = new Date();
        d.setTime(d.getTime() + exminutes);
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + (expires*1000) + ";path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    //token
    function GetAccessTokenKey()
    {
        return new Promise( function (resolve, reject) {
            var cookieToken= getCookie("PayoutToken") ;
            if(cookieToken !="")
            {
                var data ={
                    MESSAGE:"Sucess",
                    RESULT: "Sucess",
                    RXRESULTCODE:1,
                    TOKENKEY: cookieToken
                };
                
                resolve(data);
            }
            else
            {
                $.ajax({
                    type:"POST",                    
                    url:'/session/sire/models/cfc/vault-paypal.cfc?method=GetAccessTokenKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                    dataType:'json',
                    data:
                        {     
                            
                        }
                    ,beforeSend:function(xhr)
                        {
                        $('#processingPayment').show()
                    }
                    ,error:function(XMLHttpRequest,textStatus,errorThrown){     
                        var data ={MESSAGE:"Unable to get token key at this time. An error occurred."};
                        reject(data);
                        $('#processingPayment').hide()
                    }
                    ,success:function(data)
                        {
                        $('#processingPayment').hide();
                        if(data.RXRESULTCODE==1){      
                            setCookie("PayoutToken",data.TOKENKEY,data.EXPIRES);
                            resolve(data);
                        }
                        else
                        {
                            reject(data);
                        }     
                    }
                }); 
            }
       
        });
    }
   


    var isoCountries = {
        'AF' : 'Afghanistan',
        'AX' : 'Aland Islands',
        'AL' : 'Albania',
        'DZ' : 'Algeria',
        'AS' : 'American Samoa',
        'AD' : 'Andorra',
        'AO' : 'Angola',
        'AI' : 'Anguilla',
        'AQ' : 'Antarctica',
        'AG' : 'Antigua And Barbuda',
        'AR' : 'Argentina',
        'AM' : 'Armenia',
        'AW' : 'Aruba',
        'AU' : 'Australia',
        'AT' : 'Austria',
        'AZ' : 'Azerbaijan',
        'BS' : 'Bahamas',
        'BH' : 'Bahrain',
        'BD' : 'Bangladesh',
        'BB' : 'Barbados',
        'BY' : 'Belarus',
        'BE' : 'Belgium',
        'BZ' : 'Belize',
        'BJ' : 'Benin',
        'BM' : 'Bermuda',
        'BT' : 'Bhutan',
        'BO' : 'Bolivia',
        'BA' : 'Bosnia And Herzegovina',
        'BW' : 'Botswana',
        'BV' : 'Bouvet Island',
        'BR' : 'Brazil',
        'IO' : 'British Indian Ocean Territory',
        'BN' : 'Brunei Darussalam',
        'BG' : 'Bulgaria',
        'BF' : 'Burkina Faso',
        'BI' : 'Burundi',
        'KH' : 'Cambodia',
        'CM' : 'Cameroon',
        'CA' : 'Canada',
        'CV' : 'Cape Verde',
        'KY' : 'Cayman Islands',
        'CF' : 'Central African Republic',
        'TD' : 'Chad',
        'CL' : 'Chile',
        'CN' : 'China',
        'CX' : 'Christmas Island',
        'CC' : 'Cocos (Keeling) Islands',
        'CO' : 'Colombia',
        'KM' : 'Comoros',
        'CG' : 'Congo',
        'CD' : 'Congo, Democratic Republic',
        'CK' : 'Cook Islands',
        'CR' : 'Costa Rica',
        'CI' : 'Cote D\'Ivoire',
        'HR' : 'Croatia',
        'CU' : 'Cuba',
        'CY' : 'Cyprus',
        'CZ' : 'Czech Republic',
        'DK' : 'Denmark',
        'DJ' : 'Djibouti',
        'DM' : 'Dominica',
        'DO' : 'Dominican Republic',
        'EC' : 'Ecuador',
        'EG' : 'Egypt',
        'SV' : 'El Salvador',
        'GQ' : 'Equatorial Guinea',
        'ER' : 'Eritrea',
        'EE' : 'Estonia',
        'ET' : 'Ethiopia',
        'FK' : 'Falkland Islands (Malvinas)',
        'FO' : 'Faroe Islands',
        'FJ' : 'Fiji',
        'FI' : 'Finland',
        'FR' : 'France',
        'GF' : 'French Guiana',
        'PF' : 'French Polynesia',
        'TF' : 'French Southern Territories',
        'GA' : 'Gabon',
        'GM' : 'Gambia',
        'GE' : 'Georgia',
        'DE' : 'Germany',
        'GH' : 'Ghana',
        'GI' : 'Gibraltar',
        'GR' : 'Greece',
        'GL' : 'Greenland',
        'GD' : 'Grenada',
        'GP' : 'Guadeloupe',
        'GU' : 'Guam',
        'GT' : 'Guatemala',
        'GG' : 'Guernsey',
        'GN' : 'Guinea',
        'GW' : 'Guinea-Bissau',
        'GY' : 'Guyana',
        'HT' : 'Haiti',
        'HM' : 'Heard Island & Mcdonald Islands',
        'VA' : 'Holy See (Vatican City State)',
        'HN' : 'Honduras',
        'HK' : 'Hong Kong',
        'HU' : 'Hungary',
        'IS' : 'Iceland',
        'IN' : 'India',
        'ID' : 'Indonesia',
        'IR' : 'Iran, Islamic Republic Of',
        'IQ' : 'Iraq',
        'IE' : 'Ireland',
        'IM' : 'Isle Of Man',
        'IL' : 'Israel',
        'IT' : 'Italy',
        'JM' : 'Jamaica',
        'JP' : 'Japan',
        'JE' : 'Jersey',
        'JO' : 'Jordan',
        'KZ' : 'Kazakhstan',
        'KE' : 'Kenya',
        'KI' : 'Kiribati',
        'KR' : 'Korea',
        'KW' : 'Kuwait',
        'KG' : 'Kyrgyzstan',
        'LA' : 'Lao People\'s Democratic Republic',
        'LV' : 'Latvia',
        'LB' : 'Lebanon',
        'LS' : 'Lesotho',
        'LR' : 'Liberia',
        'LY' : 'Libyan Arab Jamahiriya',
        'LI' : 'Liechtenstein',
        'LT' : 'Lithuania',
        'LU' : 'Luxembourg',
        'MO' : 'Macao',
        'MK' : 'Macedonia',
        'MG' : 'Madagascar',
        'MW' : 'Malawi',
        'MY' : 'Malaysia',
        'MV' : 'Maldives',
        'ML' : 'Mali',
        'MT' : 'Malta',
        'MH' : 'Marshall Islands',
        'MQ' : 'Martinique',
        'MR' : 'Mauritania',
        'MU' : 'Mauritius',
        'YT' : 'Mayotte',
        'MX' : 'Mexico',
        'FM' : 'Micronesia, Federated States Of',
        'MD' : 'Moldova',
        'MC' : 'Monaco',
        'MN' : 'Mongolia',
        'ME' : 'Montenegro',
        'MS' : 'Montserrat',
        'MA' : 'Morocco',
        'MZ' : 'Mozambique',
        'MM' : 'Myanmar',
        'NA' : 'Namibia',
        'NR' : 'Nauru',
        'NP' : 'Nepal',
        'NL' : 'Netherlands',
        'AN' : 'Netherlands Antilles',
        'NC' : 'New Caledonia',
        'NZ' : 'New Zealand',
        'NI' : 'Nicaragua',
        'NE' : 'Niger',
        'NG' : 'Nigeria',
        'NU' : 'Niue',
        'NF' : 'Norfolk Island',
        'MP' : 'Northern Mariana Islands',
        'NO' : 'Norway',
        'OM' : 'Oman',
        'PK' : 'Pakistan',
        'PW' : 'Palau',
        'PS' : 'Palestinian Territory, Occupied',
        'PA' : 'Panama',
        'PG' : 'Papua New Guinea',
        'PY' : 'Paraguay',
        'PE' : 'Peru',
        'PH' : 'Philippines',
        'PN' : 'Pitcairn',
        'PL' : 'Poland',
        'PT' : 'Portugal',
        'PR' : 'Puerto Rico',
        'QA' : 'Qatar',
        'RE' : 'Reunion',
        'RO' : 'Romania',
        'RU' : 'Russian Federation',
        'RW' : 'Rwanda',
        'BL' : 'Saint Barthelemy',
        'SH' : 'Saint Helena',
        'KN' : 'Saint Kitts And Nevis',
        'LC' : 'Saint Lucia',
        'MF' : 'Saint Martin',
        'PM' : 'Saint Pierre And Miquelon',
        'VC' : 'Saint Vincent And Grenadines',
        'WS' : 'Samoa',
        'SM' : 'San Marino',
        'ST' : 'Sao Tome And Principe',
        'SA' : 'Saudi Arabia',
        'SN' : 'Senegal',
        'RS' : 'Serbia',
        'SC' : 'Seychelles',
        'SL' : 'Sierra Leone',
        'SG' : 'Singapore',
        'SK' : 'Slovakia',
        'SI' : 'Slovenia',
        'SB' : 'Solomon Islands',
        'SO' : 'Somalia',
        'ZA' : 'South Africa',
        'GS' : 'South Georgia And Sandwich Isl.',
        'ES' : 'Spain',
        'LK' : 'Sri Lanka',
        'SD' : 'Sudan',
        'SR' : 'Suriname',
        'SJ' : 'Svalbard And Jan Mayen',
        'SZ' : 'Swaziland',
        'SE' : 'Sweden',
        'CH' : 'Switzerland',
        'SY' : 'Syrian Arab Republic',
        'TW' : 'Taiwan',
        'TJ' : 'Tajikistan',
        'TZ' : 'Tanzania',
        'TH' : 'Thailand',
        'TL' : 'Timor-Leste',
        'TG' : 'Togo',
        'TK' : 'Tokelau',
        'TO' : 'Tonga',
        'TT' : 'Trinidad And Tobago',
        'TN' : 'Tunisia',
        'TR' : 'Turkey',
        'TM' : 'Turkmenistan',
        'TC' : 'Turks And Caicos Islands',
        'TV' : 'Tuvalu',
        'UG' : 'Uganda',
        'UA' : 'Ukraine',
        'AE' : 'United Arab Emirates',
        'GB' : 'United Kingdom',
        'US' : 'United States',
        'UM' : 'United States Outlying Islands',
        'UY' : 'Uruguay',
        'UZ' : 'Uzbekistan',
        'VU' : 'Vanuatu',
        'VE' : 'Venezuela',
        'VN' : 'Viet Nam',
        'VG' : 'Virgin Islands, British',
        'VI' : 'Virgin Islands, U.S.',
        'WF' : 'Wallis And Futuna',
        'EH' : 'Western Sahara',
        'YE' : 'Yemen',
        'ZM' : 'Zambia',
        'ZW' : 'Zimbabwe'
    };

    function getCountryName (countryCode) {
        if (isoCountries.hasOwnProperty(countryCode)) {
            return isoCountries[countryCode];
        } else {
            return countryCode;
        }
    }
    // User log update CC information
    function UserLogCCInfo(cardNumber){           
        var last4digit = cardNumber.substring(cardNumber.length - 4, cardNumber.length);
        var moduleName = 'UserUpdateCCInfo';
        var operator = 'Update CC Number ' + last4digit;
        $.ajax({
            method: 'POST',            
            url:'/session/sire/models/cfc/logs.cfc?method=createSireUserLog&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            data: {
                'moduleName': moduleName, 
                'operator' : operator
            },
            dataType: 'json',            
            beforeSend: function( xhr ) {               
            },                    
            error: function(XMLHttpRequest, textStatus, errorThrown) {                
                var message='Log update CC Failed!';
                var title= "User Log CC Info";
                alertBox(message, title);
            },                   
            success: function(data) {               
            }
        });        
    }

   var coco = $( "#country-code" ).data("country");
   var geco = getCountryName(coco);
   $("#country-code").text(geco);


    my_plan.submit(function(event){
        event.preventDefault();
        
        if (my_plan.validationEngine('validate', {focusFirstField : true, scroll: false, promptPosition: "topLeft" })) {
            var d = new Date();
            var expiredMonth = parseInt($('#expiration_date_month').val());
            var expiredYear = parseInt($('#expiration_date_year').val());

            if ( (expiredYear < d.getYear()) || (expiredMonth <= d.getMonth()+1 && expiredYear <= d.getFullYear())){
                var message = 'Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card.';
                var title= "Update Payment Method";
                alertBox(message, title);
            }
            else 
            {
                $('.btn').prop('disabled',true);
                $('.btn-update-profile').text('Processing');
                try{
                    $.ajax({
                        method: 'POST',                                                           
                        url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentMethodSettingByUserId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                        beforeSend: function( xhr ) {               
                        },                    
                        error: function(XMLHttpRequest, textStatus, errorThrown) {                
                            var message='Error';
                            var title= "Check payment method fail!.";
                            alertBox(message, title);                            
                        },                   
                        success: function(d) {                            
                            var cardObject = {
                                inpToken: '',
                                inpNumber: '',
                                inpType: '',
                                inpExpireMonth: '',
                                inpExpreYear: '',
                                inpCvv2: '',
                                inpFirstName: '',
                                inpLastName: '',
                                inpLine1: '',
                                inpCity: '',
                                inpCountryCode: '',
                                inpPostalCode: '',
                                inpState: ''
                            };                                                       
                            if(d.RESULT == 1){
                                // Worldpay
                                $.ajax({
                                    method: 'POST',
                                    url: '/session/sire/models/cfc/billing.cfc?method=UpdateCustomer&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                                    data: my_plan.serialize(),
                                    dataType: 'json',
                                    //timeout: 6000,
                                    beforeSend: function( xhr ) {
                                        $('#processingPayment').show();
                                        $('.btn').prop('disabled',true);
                                        $('.btn-update-profile').text('Processing');
                                    },                    
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        $('#processingPayment').hide();
        
                                        var message='Update Fail';
                                        var title= "Update Payment Method";
                                        alertBox(message, title);
        
                                        
                                        $('.btn').prop('disabled',false);
                                        // $('.btn-update-profile').text('PURCHASE');
                                        $('.btn-update-profile').text('UPDATE PAYMENT INFO');
                                    },                   
                                    success: function(data) {
                                        $('#processingPayment').hide();
                                        
                                        // log update cc info
                                        if(data.RXRESULTCODE == 1)               
                                        UserLogCCInfo(cardObject.inpNumber);
                                        
                                        var message = data.MESSAGE;
                                        var title= "Update Payment Method";
                                        alertBox(message, title, function () {
                                            if(data.RXRESULTCODE == 1) location.reload();
                                        });
                                    
                                        $('.btn').prop('disabled',false);
                                        // $('.btn-update-profile').text('PURCHASE');
                                        $('.btn-update-profile').text('UPDATE PAYMENT INFO');
                                    }
                                });
                            }else{
                                // Mojo, paypal, totalapps
                                cardObject = {
                                    inpNumber: $('#card_number').val(),
                                    inpType: '',
                                    inpExpireMonth: $('#expiration_date_month').val(),
                                    inpExpreYear: $('#expiration_date_year').val(),
                                    inpCvv2: $('#security_code').val(),
                                    inpFirstName: $('#first_name').val(),
                                    inpLastName: $('#last_name').val(),
                                    inpLine1: $('#line1').val(),
                                    inpCity: $('#city').val(),
                                    inpCountryCode: $('#country').val(),
                                    inpPostalCode: $('#zip').val(),
                                    inpState: $('#state').val(),
                                    inpEmail: $('#email').val()
                                }

                                $.ajax({
                                    method: 'POST',            
                                    url:'/session/sire/models/cfc/payment/vault.cfc?method=storeCreditCardInVault&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                                    data: {
                                        'inpCardObject':JSON.stringify(cardObject), 
                                        'inpPaymentGateway' : d.RESULT
                                    },
                                    dataType: 'json',
                                    //timeout: 6000,
                                    beforeSend: function( xhr ) {
                                        $('#processingPayment').show();
                                        $('.btn').prop('disabled',true);
                                        $('.btn-update-profile').text('Processing');
                                    },                    
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        $('#processingPayment').hide();
                                        var message='Update Fail';
                                        var title= "Update Payment Method";
                                        alertBox(message, title);
        
                                        
                                        $('.btn').prop('disabled',false);
                                        // $('.btn-update-profile').text('PURCHASE');
                                        $('.btn-update-profile').text('UPDATE PAYMENT INFO');
                                    },                   
                                    success: function(data) {
                                        $('#processingPayment').hide();
                                        
                                        // log update cc info
                                        if(data.RXRESULTCODE == 1)
                                        UserLogCCInfo(cardObject.inpNumber);

                                        var message = data.MESSAGE;
                                        var title= "Update Payment Method";
                                        alertBox(message, title, function () {
                                            if(data.RXRESULTCODE == 1) location.reload();
                                        });
                                    
                                        $('.btn').prop('disabled',false);
                                        // $('.btn-update-profile').text('PURCHASE');
                                        $('.btn-update-profile').text('UPDATE PAYMENT INFO');
                                    }
                                });
                            }
                        }
                    });                      
                }
                catch(ex){
                    $('#processingPayment').hide();

                    var message='Update Fail';
                    var title= "Update Payment Method";
                    alertBox(message, title);

                    $('.btn').prop('disabled',false);
                    // $('.btn-update-profile').text('PURCHASE');
                    $('.btn-update-profile').text('UPDATE PAYMENT INFO');
                }
            }
            
        }
    });

})(jQuery);
