(function() {
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	function GetBillingHistoryList(){

    	var reiceptCol = function(object){
    		var strReturn = $('<a class="btn newbtn green-gd download-billing-details" title="Download Billing History Details">Download</a>')
								.attr('data-id', object.ID)[0].outerHTML;
			return strReturn;
    	}

		var amountDollar = function(object){
			result = "$"+object.AMOUNT;
			return result;
		}

		var formatStatus = function(object){			
			result = String(object.STATUS).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');	
			return result;
		}

    	var table = $('#billing-history-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": "DATE", "sName": '', "sTitle": 'Date',"bSortable": false,"sClass":"", "sWidth":"130px"},
				{"mData": "BILLINGUSERID", "sName": '', "sTitle": 'Billing User ID',"bSortable": false,"sClass":"", "sWidth":"140px"},
				{"mData": amountDollar, "sName": '', "sTitle": 'Amount',"bSortable": false,"sClass":"", "sWidth":"120px"},
				{"mData": "PRODUCT", "sName": '', "sTitle": "Product","bSortable": false,"sClass":"", "sWidth":"250px"},
				// {"mData": "STATUS", "sName": '', "sTitle": "Status","bSortable": false,"sClass":"", "sWidth":"200px"},
				{"mData": formatStatus, "sName": '', "sTitle": "Status","bSortable": false,"sClass":"", "sWidth":"200px"},
				{"mData": reiceptCol, "sName": '', "sTitle": "Receipt","bSortable": false,"sClass":"", "sWidth":"150px"}
			],
			"sAjaxDataProp": "BILLINGHISTORYLIST",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=GetBillingHistoryList' + strAjaxQuery,

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.RXRESULTCODE != 1 && data.RXRESULTCODE != 0){
			        		console.log(data);
			        	}else{
			        		fnCallback(data);
			        	}
			        	
			        }
		      	});
	        }
		});
    }

    GetBillingHistoryList();

    function GetTransactionList(){

		var amountDollar = function(object){
			result = "$"+object.AMOUNT;
			return result;
		}
		var formatComment = function(object){			
			result = String(object.COMMENT).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');	
			return result;
		}


    	var table = $('#transaction-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": "DATE", "sName": '', "sTitle": 'Date',"bSortable": false,"sClass":"", "sWidth":"130px"},
				{"mData": amountDollar, "sName": '', "sTitle": 'Amount',"bSortable": false,"sClass":"", "sWidth":"120px"},
				{"mData": "DUE", "sName": '', "sTitle": "Due","bSortable": false,"sClass":"", "sWidth":"150px"},
				{"mData": formatComment, "sName": '', "sTitle": "Comments","bSortable": false,"sClass":"", "sWidth":"250px"}
			],
			"sAjaxDataProp": "TRANSACTIONLIST",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=GetTransactionList' + strAjaxQuery,

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.RXRESULTCODE != 1 && data.RXRESULTCODE != 0){
			        		//console.log(data);
			        	}else{
			        		fnCallback(data);
			        	}
			        	
			        }
		      	});
	        }
		});
    }

	GetTransactionList();
	
	function GetTransactionExceededList(){

		var amountDollar = function(object){
			result = "$"+object.AMOUNT;
			return result;
		}
		var actionBtn = function (object) {
			if(object.STATUS == 'Pending'){
				var strReturn = '<a href="" id ="'+object.PAYMENTID+'" class="delete-payment-transaction" title="Delete">Delete</a>';
			}else{
				var strReturn = '';
			}
            return strReturn;
        }
    	var table = $('#transaction-exceed-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
			},
			"fnStateSaveParams": function (oSettings, oData) {
			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Name', "sWidth": '23%',"bSortable": false},
                {"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '24%',"bSortable": false},
                {"mData": "PHONE", "sName": 'u.MFAContactString_vch', "sTitle": 'Phone', "sWidth": '11%',"bSortable": false},
				{"mData": "PAYMENT_DATE", "sName": 'pkw.UpdateDate', "sTitle": 'Payment Date', "sWidth": '13%',"bSortable": true,"sClass": "col-center"},
				{"mData": "PAYMENT_AMOUNT", "sName": 'pkw.PaymentAmount_dbl', "sTitle": 'Amount($)', "sWidth": '8%',"bSortable": true},
				{"mData": "STATUS", "sName": 'pkw.PaymentStatus', "sTitle": 'Status', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
               // {"mData": "VERIFYDATE", "sName": 'pkw.VerifyDate', "sTitle": 'Verify Date', "sWidth": '10%',"bSortable": false},
                //{"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "15%"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=PurchaseAmountLimitedList&UserIdstatus=1'+strAjaxQuery,

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.RXRESULTCODE != 1 && data.RXRESULTCODE != 0){
			        		console.log(data);
			        	}else{
			        		fnCallback(data);
			        	}
			        	
			        }
		      	});
	        }
		});
	}
	
	$("#transaction-exceed-list").on("click", ".delete-payment-transaction", function(event){
        event.preventDefault();
        var PAYMENTID = $(this).attr('id');
        confirmBox('Are you sure you want to delete this payment transaction?', 'Delete Payment Transaction', function(){
            $.ajax({
                url: '/session/sire/models/cfc/billing.cfc?method=UpdateStatusPaymentTransaction'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
					inpPaymentID:PAYMENTID,
					inpPaymentStatus: -3
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Action Error!','Deleted Payment Transaction',''); 
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        GetTransactionExceededList(); 
                        alertBox(data.MESSAGE,'Delete Payment Transaction','');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Delete Payment Transaction','');
                    }                       
                }
            });
        });
    });

    GetTransactionExceededList();

    $('body').on("click", ".download-billing-details", function(event){
    	var recordId = $(this).data('id');
        window.location.href="/session/sire/models/cfm/billing-history.cfm?id="+recordId;
    });
})();