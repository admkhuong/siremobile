function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var StatusCustomData = function(){
    var options   = '<option value= "-100" selected> </option>'+
                    '<option value="20">READY</option>'+
					'<option value="30">PROCESSING</option>'+
					'<option value="40">COMPLETED</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Batch Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' bll.BatchId_bi '},
			{DISPLAY_NAME: 'Batch Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
			{DISPLAY_NAME: 'Create Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' bll.Created_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' u.UserId_int '},
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.EmailAddress_vch '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' u.MFAContactString_vch '},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' bll.Status_int ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {
			if(object.STATUS == 'COMPLETED' || object.PRIORITY == 0){
				var strReturn = '';
           }else{
				var strReturn = '<a href="" id ="'+object.PKIDS+'" id-name ="'+object.USERID+'" class="run-blast-now">Run Now</a>';
			}
            return strReturn;
        }
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				//{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
				{"mData": "SORTORDERNUM", "sName": '', "sTitle": 'Priority', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
				{"mData": "BATCH_ID", "sName": 'bll.BatchId_bi', "sTitle": 'Batch Id', "sWidth": '8%',"bSortable": false},
				{"mData": "BATCH_NAME", "sName": 'b.Desc_vch', "sTitle": 'Batch Name', "sWidth": '22%',"bSortable": false},
				{"mData": "CREATE_DATE", "sName": 'bll.Created_dt', "sTitle": 'Create Date', "sWidth": '10%',"bSortable": true,"sClass": "col-center"},
				{"mData": "COMPLETED", "sName": 'bll.ActualLoaded_int', "sTitle": 'Completed', "sWidth": '8%',"bSortable": true,"sClass": "col-center"},
				{"mData": "TOTAL", "sName": 'bll.AmountLoaded_int', "sTitle": 'Total', "sWidth": '8%',"bSortable": true},
				{"mData": "STATUS", "sName": 'bll.Status_int', "sTitle": 'Status', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "8%"},
				{"mData": "NAME", "sName": 'Full_Name', "sTitle": 'Account Info', "sWidth": '18%',"bSortable": false},
                //{"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": 'Email', "sWidth": '17%',"bSortable": false},
				//{"mData": "PHONE", "sName": 'u.MFAContactString_vch', "sTitle": 'Phone', "sWidth": '11%',"bSortable": false}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=AdminBlastList'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}	

	var processInterval = setInterval(function () {
		InitControl();
	}, 10000);

	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    $("#tblListEMS").on("click", ".run-blast-now", function(event){
        event.preventDefault();
        var PKID = $(this).attr('id');
        confirmBox('Are you sure you want to setting highest priority this campaign?', 'Setting priority', function(){
            $.ajax({
                url: '/session/sire/models/cfc/campaign.cfc?method=UpdateStatusBlastNow'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
					inpPKIds:PKID
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Action Error!','Setting priority',''); 
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        InitControl(); 
                        alertBox(data.MESSAGE,'Setting priority','');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Setting priority','');
                    }                       
                }
            });
        });
    });
    
	// Run charge the payment after approve
	$("#addon-form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

})(jQuery);