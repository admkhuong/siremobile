function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
    var eventreportname = 'EventsUpcomingList';
	var StatusCustomData = function(){
    var options   = '<option value= "-100" selected> </option>'+
                    '<option value="0">Default</option>'+
                    '<option value="1">Reminder Sent</option>'+
                    '<option value="2">Reminder Accepted</option>'+
                    '<option value="3">Reminder Declined</option>'+
                    '<option value="4">Reminder Change Request</option>'+
                    '<option value="5">Reminder Error</option>'+
					'<option value="6">Do not send Reminder</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
    };
    
    $(document).on("change", "#select-event-report" , function(){
        eventreportname = $('#select-event-report').val();
        InitControl();        
        $(".btn-clear-filter").trigger('click');
    });

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Event Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ev.Title_vch '},
			{DISPLAY_NAME: 'Create Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ev.Created_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ev.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ev.SMSNumber_vch '},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ev.ConfirmationFlag_int ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	InitControl();

    function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionBtn = function (object) {
            var strReturn = '<a href="" id ="'+object.CONTACTQUEID+'" id-name ="'+object.EVENTID+'" class="re-reminder">Re Reminder</a>';
            return strReturn;
            }
        //init datatable for active agent
        if(eventreportname == 'EventsNoResponeList'){
            _tblListEMS = $('#tblListEMS').dataTable( {
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 20,
                "aoColumns": [
                    {"mData": "CREATE_DATE", "sName": 'ev.Created_dt', "sTitle": 'Created Date', "sWidth": '8%',"bSortable": false},
                    {"mData": "NAME", "sName": 'ev.Title_vch', "sTitle": 'Event Name', "sWidth": '20%',"bSortable": false},
                    {"mData": "PHONE", "sName": 'ev.SMSNumber_vch', "sTitle": 'Phone Number', "sWidth": '10%',"bSortable": false},
                    {"mData": "EVENT_START", "sName": 'ev.Start_dt', "sTitle": 'Event Start', "sWidth": '10%',"bSortable": false},
                    {"mData": "EVENT_END", "sName": 'ev.End_dt', "sTitle": 'Event End', "sWidth": '10%',"bSortable": false}
                   // {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sWidth": "10%"}
                ],
                "bAutoWidth": false,
                "sAjaxDataProp": "ListEMSData",
                "sAjaxSource": '/session/sire/models/cfc/calendar.cfc?method='+eventreportname+strAjaxQuery,
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                   },
                   "fnDrawCallback": function( oSettings ) {
                    var paginateBar = this.siblings('div.dataTables_paginate');
                      if (oSettings._iDisplayStart < 0){
                        oSettings._iDisplayStart = 0;
                        paginateBar.find('input.paginate_text').val("1");
                    }
                    
                    if(oSettings._iRecordsTotal < 1){
                        paginateBar.hide();
                    } else {
                        paginateBar.show();
                    }
                },
                "fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
                   aoData.push(
                        { "name": "customFilter", "value": customFilterData}
                    );
                    $.ajax({dataType: 'json',
                             type: "POST",
                             url: sSource,
                             data: aoData,
                             success: fnCallback
                     });
                },
                "fnInitComplete":function(oSettings, json){
                    $("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
                }
            });
        }else{
            _tblListEMS = $('#tblListEMS').dataTable( {
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 20,
                "aoColumns": [
                    {"mData": "NAME", "sName": 'ev.Title_vch', "sTitle": 'Event Name', "sWidth": '20%',"bSortable": false},
                    {"mData": "PHONE", "sName": 'ev.SMSNumber_vch', "sTitle": 'Phone Number', "sWidth": '10%',"bSortable": false},
                    {"mData": "EVENT_START", "sName": 'ev.Start_dt', "sTitle": 'Event Start Time', "sWidth": '10%',"bSortable": false},
                    {"mData": "EVENT_END", "sName": 'ev.End_dt', "sTitle": 'Event End Time', "sWidth": '10%',"bSortable": false},
                    {"mData": "STATUS", "sName": 'ev.ConfirmationFlag_int', "sTitle": 'Status', "sWidth": '10%',"bSortable": false},
                    {"mData": "CREATE_DATE", "sName": 'ev.Created_dt', "sTitle": 'Created Date', "sWidth": '8%',"bSortable": false}
                ],
                "bAutoWidth": false,
                "sAjaxDataProp": "ListEMSData",
                "sAjaxSource": '/session/sire/models/cfc/calendar.cfc?method='+eventreportname+strAjaxQuery,
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                   },
                   "fnDrawCallback": function( oSettings ) {
                    var paginateBar = this.siblings('div.dataTables_paginate');
                      if (oSettings._iDisplayStart < 0){
                        oSettings._iDisplayStart = 0;
                        paginateBar.find('input.paginate_text').val("1");
                    }
                    
                    if(oSettings._iRecordsTotal < 1){
                        paginateBar.hide();
                    } else {
                        paginateBar.show();
                    }
                },
                "fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
                   aoData.push(
                        { "name": "customFilter", "value": customFilterData}
                    );
                    $.ajax({dataType: 'json',
                             type: "POST",
                             url: sSource,
                             data: aoData,
                             success: fnCallback
                     });
                },
                "fnInitComplete":function(oSettings, json){
                    $("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
                }
            });
        }
		
	}	

	// var processInterval = setInterval(function () {
	// 	InitControl();
	// }, 10000);

	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });
    
    // $("#tblListEvent").on("click", ".run-blast-now", function(event){
    //     event.preventDefault();
    //     var PKID = $(this).attr('id');
    //     confirmBox('Are you sure you want to setting highest priority this campaign?', 'Setting priority', function(){
    //         $.ajax({
    //             url: '/session/sire/models/cfc/campaign.cfc?method=UpdateStatusBlastNow'+strAjaxQuery,
    //             async: true,
    //             type: 'post',
    //             dataType: 'json',
    //             data: {
	// 				inpPKIds:PKID
    //             },
    //             beforeSend: function(){
    //             },
    //             error: function(jqXHR, textStatus, errorThrown) {
    //                 alertBox('Action Error!','Setting priority',''); 
    //             },
    //             success: function(data) {
    //                 if (data.RXRESULTCODE == 1) {
    //                     InitControl(); 
    //                     alertBox(data.MESSAGE,'Setting priority','');
    //                 } else {
    //                     /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
    //                     alertBox(data.MESSAGE,'Setting priority','');
    //                 }                       
    //             }
    //         });
    //     });
    // });
    
	// Run charge the payment after approve
	$("#addon-form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

})(jQuery);