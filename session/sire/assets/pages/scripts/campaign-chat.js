var CampaignComponents = function () {
    
    return {

        initHandleSelectTemplate: function () {
            $('.campaign-template').bind('click', function (e) {
                $('.campaign-template').removeClass('active').find('.campaign-template-content').hide();
                $(this).addClass('active').find('.campaign-template-content').show();
                
                var heightBoxInner = $(this).find('.inner-content').outerHeight();
                $(this).find('.campaign-template-content').height(heightBoxInner);
            });

            $(window).bind('load resize', function () {
                var $box = $('.campaign-template.active').find('.campaign-template-content');
                var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
                calCulateHeightBox($box, $hBox);
            });

            function calCulateHeightBox ($box, $hBox) {
                $box.height($hBox);
            }
        },

        initPreviewScroll: function () {
            $("#SMSHistoryScreenArea").mCustomScrollbar({
                theme: 'dark-2',
                setHeight: 455
            });
        },

        handleBootstrapSelect: function() {
            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        },

        handleAddMoreAnswerText: function () {
            var $fieldStr = '';            

            $fieldStr += '<div class="row row-small">';
                $fieldStr += '<div class="col-xs-8 col-sm-10">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control text-center" value="" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<button class="form-control delete-answer">';
                            $fieldStr += '<i class="fa fa-minus"></i>';
                        $fieldStr += '</button>';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
            $fieldStr += '</div>';

            $('.wrapper-answer-text').on('click', '.add-more-answer', function(event) {
                event.preventDefault();
                $(this).parents('.wrapper-answer-text').find('.field-anser-holder').append($fieldStr);
            });

            $('.field-anser-holder').on('click', '.delete-answer', function(event) {
                event.preventDefault();
                $(this).parents('.row.row-small').remove();
            });
        },

        handleAddAnotherMessage: function () {
            var $rendered = new EJS({url:'assets/pages/scripts/another-message.ejs'}).render();
            
            $('.wrap-box-another-mesage').on('click', '.another-prepend', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').prepend($rendered);
            });

            $('.wrap-box-another-mesage').on('click', '.another-append', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').append($rendered);
            });
        },

        handleAddSmsNumber: function () {
            // var $rendered = new EJS({url:'assets/pages/scripts/sms-number-template.ejs'}).render();
            var fieldStr = '';

            fieldStr += '<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">';
                fieldStr += '<div class="uk-width-expand">';
                    fieldStr += '<input type="text" class="form-control phone-style" />';
                fieldStr += '</div>';
                fieldStr += '<div class="uk-width-auto col-for-more">';
                    fieldStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
                    fieldStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
                fieldStr += '</div>';
            fieldStr += '</div>';

            $('.sms-number-wrap').on('click', '.trigger-plus', function(event) {
                event.preventDefault();

                if ($(".sms-number-item").length >= 10) {
                    return false;
                }

                $(this).parents('.hold-more-content').append(fieldStr);

                var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

                if ($item.length > 1) {
                    $('.sms-number-wrap').addClass('hasMinus');
                }
            });

            $('.sms-number-wrap').on('click', '.trigger-minus', function(event) {
                event.preventDefault();

                $(this).parents('.sms-number-item').remove();

                var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

                if ($item.length === 1) {                    
                    $('.sms-number-wrap').removeClass('hasMinus');
                }
            });
        },

        handleEmailAddress: function () {
            // var $rendered = new EJS({url:'assets/pages/scripts/sms-number-template.ejs'}).render();
            var fieldsStr = '';

            fieldsStr += '<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">';
                fieldsStr += '<div class="uk-width-expand">';
                    fieldsStr += '<input type="text" class="form-control" />';
                fieldsStr += '</div>';
                fieldsStr += '<div class="uk-width-auto col-for-more">';
                    fieldsStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
                    fieldsStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
                fieldsStr += '</div>';
            fieldsStr += '</div>';

            $('.email-add-wrap').on('click', '.trigger-plus', function(event) {
                event.preventDefault();

                if ($(".email-add-item").length >= 10) {
                    return false;
                }

                $(this).parents('.hold-more-content').append(fieldsStr);

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length > 1) {
                    $('.email-add-wrap').addClass('hasMinus');
                }
            });

            $('.email-add-wrap').on('click', '.trigger-minus', function(event) {
                event.preventDefault();

                $(this).parents('.email-add-item').remove();

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length === 1) {                    
                    $('.email-add-wrap').removeClass('hasMinus');
                }
            });
        },
        
        init: function () {
            // this.initHandleSelectTemplate();
            // this.handleAddMoreAnswerText();
            this.initPreviewScroll();
            // this.handleBootstrapSelect();
            this.handleAddSmsNumber();
            this.handleEmailAddress();
        }

    };

}();

var BindCharacterCountAllPage = function () {
    var id = 1;
    $(".control-point-char").each(function(index, el) {
        $(el).attr('id', 'cp-char-count-'+(id++));
    });
    $(".control-point").each(function(index, el) {
        if ($(el).find('textarea').length > 0) {
            var counterId = $(el).find(".control-point-char").attr('id');;
            var text = $(el).find("textarea")[0];

            countText("#"+counterId, $(text).val());

            $(text).bind('input change', function(e) {
                countText("#"+counterId, $(text).val());
            });
        }
    });
}

jQuery(document).ready(function() {
    $(".sms-chat-li").addClass('active');

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    action = $("#action").val();

    keywordValid = action == "Edit" ? 1 : 0;

    inpBatchId = $("#Batchid").val();

    $(".campaign-template").off();

    CampaignComponents.init(); 

    BindCharacterCountAllPage();
    
    /* init page form validation */
    $("#frm-campaign").validationEngine({promptPosition : "topLeft", scroll: true,focusFirstField : false, scrollOffset: $(".page-header").height()});

    if(action == 'CreateNew'){
        $(".cpedit:not(:first)").addClass('hidden');
        $(".start-edit").addClass('hidden');
    }

    if (action == "Edit") {
        $(".cpedit:last").addClass('hidden');
    }

    $("#Keyword").keydown(function(event) {
        $(".keyword-next").hide();
    });

    // Validate keyword in real time - ajax with warnings
    $("#Keyword").delayOn("input", 800, function(element, event) {
        keywordValid = 0;
        $('#KeywordStatus').removeClass('has-error');       
        $('#KeywordStatus').removeClass('not-has-error');
        if(element.value.length == 0)
        {
            // Give blank to erase warning if one was previously set
            if( $('#KeywordDisplayValue').html.length > 0)
            {
                //$('#KeywordStatus').css('color', 'red');
                $('#KeywordStatus').addClass('has-error');
                $('#KeywordStatus').html("");
                keywordValid = 0;
                $(".keyword-next").hide();
            }
            else
            {
                //$('#KeywordStatus').css('color', 'red');
                $('#KeywordStatus').addClass('has-error');
                $('#KeywordStatus').html('');               
                keywordValid = 0;
                $(".keyword-next").hide();
            }
        }           
        else
        {
            $('#KeywordStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');
            
            /*<!--- Save Custom Responses - validation on server side - will return error message if not valid --->*/               
            $.ajax({
                type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
                url: '/session/sire/models/cfc/control-point.cfc?method=ValidateKeywordWithoutBatchId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                async: false,
                data:  
                {
                    inpKeyword : element.value,
                    inpShortCode : $("#ShortCode").val()                    
                    
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
                    alertBox("Error. No Response from the remote server. Check your connection and try again.");
                },                    
                success:        
                /*<!--- Default return function for Async call back --->*/
                function(d) 
                {
                    
                    /*<!--- RXRESULTCODE is 1 if everything is OK --->*/
                    if (parseInt(d.RXRESULTCODE) == 1) 
                    {   
                        //$('#KeywordStatus').css('color', 'green');
                        $('#KeywordStatus').addClass('not-has-error');
                        $('#KeywordStatus').html(d.MESSAGE);
                        keywordValid = 1;
                        $(".keyword-next").show();
                    }
                    else
                    {
                        /*<!--- No result returned ---> */
                        if(d.ERRMESSAGE != "")
                        {   
                            //$('#KeywordStatus').css('color', 'red');
                            $('#KeywordStatus').addClass('has-error');
                            $('#KeywordStatus').html(d.ERRMESSAGE);
                        }
                        keywordValid = 0;
                        $(".keyword-next").hide();
                    }
                    $('#KeywordStatus').show();                                     
                }
            });
        }
    });

    $("#tellmgr-sms-checkbox").change(function(event) {
        /* Act on the event */
        if (!this.checked) {
            var firstSMSInput = $('.sms-number-item').first();
            firstSMSInput.siblings('.sms-number-item').remove();
            firstSMSInput.find('input').val('');
            firstSMSInput.find('input').prop('disabled', 'true');
            // firstSMSInput.find('.trigger-plus').hide();
            $(".sms-number-wrap").addClass('noMinus');
        } else {
            var firstSMSInput = $('.sms-number-item').first();
            firstSMSInput.find('input').prop('disabled', false);
            $(".sms-number-wrap").removeClass('noMinus');
            $(".sms-number-wrap").removeClass('hasMinus');
        }
    });

    $("#tellmgr-email-checkbox").change(function(event) {
        /* Act on the event */
        if (!this.checked) {
            var firstEmailInput = $('.email-add-item').first();
            firstEmailInput.siblings('.email-add-item').remove();
            firstEmailInput.find('input').val('');
            firstEmailInput.find('input').prop('disabled', 'true');
            $(".email-add-wrap").addClass('noMinus');
        } else {
            var firstEmailInput = $('.email-add-item').first();
            firstEmailInput.find('input').prop('disabled', false);
            $(".email-add-wrap").removeClass('noMinus');
            $(".email-add-wrap").removeClass('hasMinus');
        }
    });

    var fnSaveKeyword = function (preview) {
        preview = typeof preview === "undefined" ? 0 : preview;
        if (keywordValid) {
            var welcomeMsg = $("#welcomeMsg").val();
            var endMsg = $("#endMsg").val();
            var inpListOfEmails = [];
            var inpListOfPhones = [];
            var method = '';

            /* Tell the manager options */
            if ($("#tellmgr-sms-checkbox").length > 0) {
                if ($("#tellmgr-sms-checkbox").is(':checked')) {
                    $(".sms-number-item").each(function(index, el) {
                        var phone = $(el).find("input").val();
                        if (phone !== "") {
                            inpListOfPhones.push(phone);
                        }
                    });
                }
            }
            if ($("#tellmgr-email-checkbox").length > 0) {
                if ($("#tellmgr-email-checkbox").is(':checked')) {
                    $(".email-add-item").each(function(index, el) {
                        var email = $(el).find("input").val();
                        if (email !== "") {
                            inpListOfEmails.push(email);
                        }
                    });
                }
            }

            if (action == "CreateNew") {
                method = "createKeyword";
            } else {
                method = "saveAPISettings";
            }

            $.ajax({
                url: '/session/sire/models/cfc/smschat.cfc?method='+method+strAjaxQuery,
                type: 'POST',
                data: {
                    inpBatchId: $("#Batchid").val(),
                    inpKeyword: $("#Keyword").val(),
                    inpSendEmailStart: $("#tellmgr-email-checkbox").is(':checked'),
                    inpSendEmailReceived: $("#tellmgr-email-checkbox").is(':checked'),
                    inpEmailList: inpListOfEmails.join(","),
                    inpSendSMSStart: $("#tellmgr-sms-checkbox").is(':checked'),
                    inpSendSMSReceived: $("#tellmgr-sms-checkbox").is(':checked'),
                    inpSMSList: inpListOfPhones.join(","),
                    inpWelcomeMsg: welcomeMsg,
                    inpEndMsg: endMsg
                },
            })
            .done(function(data) {
                if (parseInt(data.RXRESULTCODE) > 0) {
                    if (action == "CreateNew") {
                        inpBatchId = data.BATCHID;
                        $("#Batchid").val(data.BATCHID);
                    }
                    if (preview) {
                        $("#SMSPreview").trigger('click');
                    } else {
                        if (action == "CreateNew") {
                            var message = "Keyword have been saved!";
                        } else {
                            var message = "Keyword have been saved!";
                        }
                        bootbox.dialog({
                            message: '<h4 class="be-modal-title">Saved!</h4><p>'+message+'</p>',
                            title: '&nbsp;',
                            className: "be-modal",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "green-gd",
                                    callback: function(){
                                        location.href = "/session/sire/pages/sms-chat";
                                    }
                                },
                            },
                            onEscape: function() {
                                location.href = "/session/sire/pages/sms-chat";
                            }
                        });
                    }
                    action = "Edit";
                } else {
                    $(".modal").modal("hide");
                    alertBox(data.MESSAGE, "Oops!");
                }
            })
            .fail(function() {
                console.log("error");
            });
            
        } else {
            alertBox("Keyword is invalid!", "Oops!");
        }
    }

    $('.campaign-next').click(function(){
        if ($('#frm-campaign').validationEngine('validate')) {
            if (keywordValid) {
                var parentCp = $(this).closest('.cpedit');
                var nextParentCp = parentCp.next();
                nextParentCp.removeClass('hidden');
                $("html, body").animate({ scrollTop: $(nextParentCp).offset().top - $(".page-header").height() });

                if ($(this).hasClass('finished')) {
                    fnSaveKeyword(1);
                }
            } else {
                alertBox("Keyword is invalid!", "Oops!");
            }
        }
    });

    $("body").on('click', '.btn-finished', function(event) {
        event.preventDefault();
        if ($('#frm-campaign').validationEngine('validate')) {
            fnSaveKeyword(0);
        }
    });

});