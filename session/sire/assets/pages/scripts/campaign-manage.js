(function($){
	var strQueryAjax = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	var arrListChecked=[];
	var maxFilesize = $('#maximagesize').val();
	var RegName = new RegExp("^[a-zA-Z0-9_()-]+$");

	$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

	    if(oSettings.oFeatures.bServerSide === false){
	        var before = oSettings._iDisplayStart;
	        oSettings.oApi._fnReDraw(oSettings);
	        //iDisplayStart has been reset to zero - so lets change it back
	        oSettings._iDisplayStart = before;
	        oSettings.oApi._fnCalculateEnd(oSettings);
	    }

	    //draw the 'current' page
	    oSettings.oApi._fnDraw(oSettings);
	};


	var GetCampaignList = function(customFilterData){
		customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";

		var actionCol = function(object){
			var strReturn = "";
			if (parseInt(object.INPROCESS) > 0) {
				//strReturn = $('<div class="btn-re-edit processActions" title="Stop" data-pkid="'+object.INPROCESS+'" data-action="stop"><a style="color: #5c5c5c;"><i class="fa fa-hand-paper-o" aria-hidden="true"></i></a></div>')[0].outerHTML;
				//strReturn+= $('<div class="btn-re-xs processActions" data-pkid="'+object.INPROCESS+'" data-action="view"><a>PROCESSING</a></div>')[0].outerHTML;
				strReturn = $('<div class="btn-re-xs" style="cursor: inherit; valign: center;" data-pkid="'+object.INPROCESS+'" data-action="view">PROCESSING<br>'+object.INCONTACTQUE+'/'+object.TOTALSMS+' completed</div>')[0].outerHTML;
			} else {
				subURL="";
				if(object.TEMPLATETYPE==9)
				{
					subURL="&templateType=0&templateID="+object.TEMPLATEID+"&selectedTemplateType=9";
				}
				tempLink="template-new";
				strReturn = $('<a class="btn btn-default btn-edit-cp-new" href="/session/sire/pages/campaign-'+(object.TEMPLATEID > 0 ? tempLink : "edit")+'?campaignid='+object.ID+subURL+'" style="color: #5c5c5c;">Edit</a>')[0].outerHTML;
				if(object.TEMPLATEID!=9){
					// dropdown action list
					strReturn+= $('<div class="dropdown action-cp-dropdown-list"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu"><li><a href="/session/sire/pages/campaign-reports?campaignid='+object.ID+'">Report</a></li><li><a class="clone-cp" data-id="'+object.ID+'">Clone</a></li><li><a class="del-cp-new" data-id="'+object.ID+'">Delete</a></li></ul></div>')[0].outerHTML;
				}
				else{
					// dropdown action list
					strReturn+= $('<div class="dropdown action-cp-dropdown-list"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu"><li><a href="/session/sire/pages/campaign-reports?campaignid='+object.ID+'">Report</a></li><li><a href="/session/sire/pages/sms-response?keyword='+object.KEYWORD+'" target="_blank" title="Respond to customer"><i class="fa fa-commenting-o" aria-hidden="true"></i><span>'+object.TOTALCHAT+'<span></a></li><li><a class="clone-cp" data-id="'+object.ID+'">Clone</a></li><li><a class="del-cp-new" data-id="'+object.ID+'">Delete</a></li></ul></div>')[0].outerHTML;
				}
				// strReturn+= $('<div class="btn-re-xs"><a href="/session/sire/pages/campaign-reports?campaignid='+object.ID+'">REPORT</a></div>')[0].outerHTML;
				// if(object.TEMPLATEID==9)
				// {
				// 	strReturn+= $('<div class="btn-chat-xs"><a href="/session/sire/pages/sms-response?keyword='+object.KEYWORD+'" target="_blank" title="Respond to customer"><i class="fa fa-commenting-o" aria-hidden="true"></i><span>'+object.TOTALCHAT+'<span></a></div>')[0].outerHTML;
				// }
			}
			return strReturn;
		}

		// var selectCol = function(object){
		// 	var strReturn = "";
		// 	if (parseInt(object.INPROCESS) > 0) {
		// 		strReturn = $('<div class="tick-select-none"><i class="fa hidden" aria-hidden="true" data-id="'+object.ID+'"></i></div>')[0].outerHTML;
		// 	}else{
		// 		var chkClass="fa-square-o";
		// 		if(arrListChecked.indexOf(object.ID)> -1) var chkClass="fa-check-square-o";

		// 		strReturn = $('<div class="tick-select"><i class="fa '+chkClass+'" aria-hidden="true" data-id="'+object.ID+'"></i></div>')[0].outerHTML;
		// 	}
		// 	return strReturn;
		// }

		var keywordCol = function(object){
			return object.KEYWORD.toUpperCase();
		}
		var MlpCol = function (object) {
			if(object.MLPID != 0){
				var strReturn = '<a id ="'+object.MLPURL+'" class="getlink-mlp" title="Get Link"><i class="fa fa-external-link" aria-hidden="true"></i></a><a id ="'+object.ID+'" mlp-id = "'+object.MLPID+'" class="edit-mlp" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
			}else{
				var strReturn = '';
			}
			 return strReturn;
		}
		var colName = function(object){
			var strReturn="";
			if(object.ISFINISHED==1)
			{
				strReturn=object.NAME;
			}
			else
			{
				strReturn="<div class='cp-draft'>You're almost finished! <span class='cp-draft-draft'>Draft</span></div>" + object.NAME;
			}
			return strReturn;
		}

		var table = $('#tblCampaignList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	// {"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id","sWidth":"100px"},
				{"mData": colName, "sName": 'name', "sTitle": 'Name',"bSortable": false,"sClass":"name", "sWidth": "100px"},
				{"mData": keywordCol, "sName": 'keyword', "sTitle": 'Keyword',"bSortable": false,"sClass":"keyword", "sWidth": "100px"},
				// {"mData": "TOTALOPTIN", "sName": 'total-optin', "sTitle": "Opt-in's","bSortable": false,"sClass":"optin","sWidth":"90px"},
				// {"mData": "TOTALOPTOUT", "sName": 'total-optout', "sTitle": "Opt-out's","bSortable": false,"sClass":"optout","sWidth":"90px"},
				{"mData": "GROUPNAME", "sName": 'group-name', "sTitle": 'List',"bSortable": false,"sClass":"group-name", "sWidth":"100px"},
				{"mData": "TEMPNAME", "sName": 'temp-name', "sTitle": 'Template Type',"bSortable": false,"sClass":"temp-name","sWidth": "100px"},
				/* {"mData": MlpCol, "sName": 'action', "sTitle": 'MLP',"bSortable": false,"sClass":"action","sWidth":"10%"}, */
				{"mData": actionCol, "sName": 'action', "sTitle": 'Actions',"bSortable": false,"sClass":"action","sWidth":"160px"}
				// {"mData": selectCol, "sName": 'select', "sTitle": 'Select',"bSortable": false,"sClass":"selection","sWidth":"8%"}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetCampaignList'+strQueryAjax,
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
				$('#processingPayment').hide();
		    },
		     "fnFooterCallback": function(nRow){
		     	//var foooter = $('<td  colspan="'+ (this.fnSettings().aoColumns.length -1)+'"></td><td><div><span class="delete-selected-item"><i class="fa fa-trash" aria-hidden="true"></i></span><div></td>')

		    	//$(nRow).html(foooter);
		    },
		    "fnHeaderCallback": function(nRow){
		    	$(nRow).find('th.selection').html('<i class="select-all glyphicon glyphicon-ok"></i>');
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
				// $('#tblCampaignList tr:last').find('td.action').find('div.action-cp-dropdown-list').removeClass('dropdown');
				// $('#tblCampaignList tr:last').find('td.action').find('div.action-cp-dropdown-list').addClass('dropup');
			}
		});

		return function(){
			table.fnStandingRedraw();
		}

	};

	$('#processingPayment').show();
	GetCampaignList();


	$('body').on('click', 'table i.select-all', function(event){

		if(!$(this).hasClass('checked')){
			$('#tblCampaignList').find('input.campaign-item[type="checkbox"]').prop('checked', true);

			$('#tblCampaignList').find('div.tick-select > i').removeClass('fa-square-o');
			$('#tblCampaignList').find('div.tick-select > i').addClass('fa-check-square-o');
		} else {
			$('#tblCampaignList').find('input.campaign-item[type="checkbox"]').prop('checked', false);

			$('#tblCampaignList').find('div.tick-select > i').removeClass('fa-check-square-o');
			$('#tblCampaignList').find('div.tick-select > i').addClass('fa-square-o');
		}
		$(this).toggleClass('checked');

	});



	$('body').on('click', 'table a.btn-report', function(event){

		var campaignID = $(this).data('id');


		alertBox('Do action with campaign id: '+campaignID);
	});





	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
			{DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.BatchId_bi '},
			{DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' b.Created_dt '},
        	{DISPLAY_NAME: 'Keyword', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'KEYWORD', SQL_FIELD_NAME: ' k.Keyword_vch '}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			GetCampaignList();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		GetCampaignList(filterData);
	});


	$('body').on('click', 'div.delete-selected-item', function(event){

		var arrCampaignID = [];
		$('#tblCampaignList').find('tbody i.fa-check-square-o').each(function(index){
			arrCampaignID.push($(this).data('id'));
		});

		if(arrCampaignID.length){
			confirmBox('Are you sure you want to delete selected items ?', 'Delete selected campaigns', function(){
				$.ajax({
					url: "/session/sire/models/cfc/campaign.cfc?method=deleteCampaign"+strQueryAjax,
					dataType: "json",
					data: {
						inpBatchIDS: JSON.stringify(arrCampaignID)
					},
					success: function(data){
						alertBox(data.MESSAGE != "" ? data.MESSAGE : data.ERRMESSAGE, parseInt(data.RXRESULTCODE) == 1 ? "Success" : "Oops!");
						GetCampaignList();
					}
				});
			});
		} else {
			alertBox('No campaigns are selected!');
		}

	});

	$('body').on('click', '.del-cp-new', function(event){
		event.preventDefault();
		var arrCampaignID = [];
		arrCampaignID.push($(this).data('id'));
		confirmBox('Are you sure you want to delete this campaign?', 'Delete Campaign', function(){
			$.ajax({
				url: "/session/sire/models/cfc/campaign.cfc?method=deleteCampaign"+strQueryAjax,
				dataType: "json",
				data: {
					inpBatchIDS: JSON.stringify(arrCampaignID)
				},
				success: function(data){
					alertBox(data.MESSAGE != "" ? data.MESSAGE : data.ERRMESSAGE, parseInt(data.RXRESULTCODE) == 1 ? "Success" : "Oops!");
					GetCampaignList();
				}
			});
		});
	});

	$('body').on('click', '.clone-cp', function(event){
		event.preventDefault();
		haha = $(this).data('id');
		confirmBox('Are you sure you want to clone this campaign?', 'Clone Campaign', function(){
			$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/control-point.cfc?method=CloneCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data: {
					inpBatchId : haha,
					inpShortCode : $('.short-code-list-select option:selected').first().text()
					},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
				},
				success:function(d) {
					$('#processingPayment').hide();
					if(d.RXRESULTCODE == 1){
						alertBox(d.MESSAGE, 'Campaign Cloned Successfully!', function(){
							//location.href = '/session/sire/pages/campaign-edit?campaignid=' + d.NEXTCAMPAINID;
							window.location.reload();
						});
					}
					else
					{
						alertBox(d.MESSAGE, 'Clone Campaigns');
					}
				}
			});
		});
	});

	var mlpDomainPath = document.location.origin == 'https://www.mlp-x.com' ? 'https://www.mlp-x.com/lz/' : document.location.origin + '/mlp-x/lz/index.cfm?inpURL=';
	// MLP Campaign
	$("body").on("click", ".getlink-mlp", function(event){
		var MLPURL = $(this).attr('id');
		alertBox(mlpDomainPath+MLPURL , "Get URL");
		// get URL MLP of this campaign
	});

	$("body").on("click", ".edit-mlp", function(event){
		var campaignId = $(this).attr('id');
		var mlpid = $(this).attr('mlp-id');
		$.ajax({
			url: "/session/sire/models/cfc/mlp.cfc?method=ReadCPP"+strQueryAjax,
			dataType: "json",
			data: {
				ccpxDataId: mlpid
			},
			success: function(data){
				if(data.RXRESULTCODE == 1){
					$('#campaign-mlp-modal').find('.campaign-mlp-modal-body').html(data.RAWCONTENT);
					// CKEDITOR.config.allowedContent = true;
				 //    CKEDITOR.config.language = 'en';
				 //    CKEDITOR.disableAutoInline = true;
				 //    CKEDITOR.inline('campaign-mlp',{
				 //        toolbarGroups: [
				 //            { name: 'clipboard', groups: [ 'undo' ] },
				 //            { name: 'editing', groups: [ 'selection', 'editing' ] },
				 //            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
				 //            '/',
				 //            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
				 //            '/',
				 //            { name: 'links', groups: [ 'links' ] },
				 //            { name: 'insert', groups: [ 'insert' ] },
				 //            '/',
				 //            { name: 'styles', groups: [ 'styles' ] },
				 //            { name: 'colors', groups: [ 'colors' ] },
				 //            // { name: 'tools', groups: [ 'tools' ] },
				 //            { name: 'others', groups: [ 'others' ] }
				 //        ]
				 //    });
				 $('#campaign-mlp').popline();
				}else{
					alertBox(data.MESSAGE, "Error MLP");
					return false;
				}
			}
		});
		$('#campaign-mlp-modal').find('#cpMLPId').val(mlpid);

		$('#campaign-mlp-modal').modal('show');

		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#campaign-mlp-logo')
		                .attr('src', e.target.result);

					console.log('test');
		        };

		        reader.readAsDataURL(input.files[0]);

		    }
		}
	});

	function SaveMLP(){

		var inpRawData = $('.campaign-mlp-modal-body').children().html();
		inpRawData = inpRawData.replace(/<script/g, '<mlp-script');
		inpRawData = inpRawData.replace(/script\/>/g, '/mlp-script>');
		inpRawData = inpRawData.replace(/<iframe/g, '<mlp-iframe');
		inpRawData = inpRawData.replace(/iframe\/>/g, '/mlp-iframe>');

		var cppName = $('#Desc_vch').val() + 'MLP';

		var customCSS = $('#campaign-mlp-custom-css').val();

		if($('#cpMLPId').val() > 0){
			inpRawData = inpRawData.replace(/(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)/g, '<img style="max-height: 100%;" id="campaign-mlp-logo" data-toggle="modal" href="#" data-target="#ImageChooserModal" data-image-target="campaign-mlp-logo" src="' + $('#campaign-mlp-logo').attr('name') + '">');
			$.ajax({
				url: '/session/sire/models/cfc/mlp.cfc?method=SaveMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
				data: {ccpxDataId:$('#cpMLPId').val(),inpRawContent:inpRawData, inpCustomCSS: customCSS, inpDesc: cppName, inpMLPType: 2, isForCampaignMLP: 1},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				success: function(data){
					$('#processingPayment').hide();
					if(data.RXRESULTCODE == 1){
						$('#campaign-mlp-modal').modal('hide');
						alertBox(data.MESSAGE, 'Save Campaign MLP', function(){
							window.location = '/session/sire/pages/campaign-manage';
						});
					}
					else{
						alertBox(data.MESSAGE, 'Save Campaign MLP');
					}

				}
			});
		}
		else{


			$.ajax({
				url: '/session/sire/models/cfc/mlp.cfc?method=AddMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
				data: {inpRawContent:inpRawData, inpCustomCSS: customCSS, inpDesc: cppName, inpMLPType: 2, inpBatchId: inpBatchId},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				success: function(data){
					$('#processingPayment').hide();
					if(data.RXRESULTCODE == 1){
						$('#campaign-mlp-modal').modal('hide');
						alertBox(data.MESSAGE, 'Save Campaign MLP', function(){
							window.location = '/session/sire/pages/campaign-manage';
						});
					}
					else{
						alertBox(data.MESSAGE, 'Save Campaign MLP');
					}

				}
			});
		}
	}

	$('#save-campaign-mlp').on('click', function(event){
		event.preventDefault();

		SaveMLP();
	});


	$('body').on('click', '.tick-select', function(event) {
		event.preventDefault();
		if( $(this).children('i').hasClass('fa-square-o') ){
			$(this).children('i').removeClass('fa-square-o');
			$(this).children('i').addClass('fa-check-square-o');
		} else if( $(this).children('i').hasClass('fa-check-square-o') ){
			$(this).children('i').removeClass('fa-check-square-o');
			$(this).children('i').addClass('fa-square-o');
		}
	});

	/* Trigger process actions button */
	$('body').on('click', '.processActions', function (event) {
		var data = $(this).data();
		var method = '';
		var inpData = {};
		switch(data['action']) {
			case 'view':
				method = 'GetQueueProcessInfo';
				inpData.inpPKID = data['pkid'];
				processActions(data['action'], method, inpData);
				break;
			case 'stop':
				method = 'updateQueueStatus';
				inpData.inpPKID = data['pkid'];
				inpData.inpStatus = 0;
				inpData.inpNote = "User stopped";

				/* Display confirm modal first */
				bootbox.dialog({
				    message: "Are you sure you want to stop this blast process?",
				    title: 'Stop Process',
				    buttons: {
				        cancel: {
				        	label: "Cancel",
				        	className: "btn btn-medium btn-back-custom",
				        	callback: function () {}
				        },
				        success: {
				            label: "Confirm",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {
				            	processActions(data['action'], method, inpData);
				            }
				        }
				    }
				});
				break;
			default:
				break;
		}

		return true;
	});

	function processActions(action, method, inpData) {
		$.ajax({
			url: '/session/sire/models/cfc/queue.cfc?method='+method+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: inpData,
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(d) {
			if (parseInt(d.RXRESULTCODE) > 0) {
				switch(action) {
					case 'view':
						$('#progressouter').css('width', d.PERCENT+"%");
						$('#timecounter').text(d.PERCENT + '%');
						$('#loadQueueProcess').modal('show');

						/* If process is done, reload campaign table */
						if (parseInt(d.PERCENT) >= 100) {
							GetCampaignList();
						} else {
							/* setup an interval to auto reload progress bar after 1 minute */
							var processInterval = setInterval(function () {
								$.ajax({
									url: '/session/sire/models/cfc/queue.cfc?method='+method+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									type: 'POST',
									data: inpData
								})
								.done(function(rx) {
									if (parseInt(rx.RXRESULTCODE) > 0) {
										$('#progressouter').css('width', rx.PERCENT+"%");
										$('#timecounter').text(rx.PERCENT + '%');

										/* If process is done, reload campaign table */
										if (parseInt(rx.PERCENT) >= 100) {
											$('#loadQueueProcess').modal('hide');
											GetCampaignList();
										}
									}
								});
							}, 10000);

							/* clear interval when modal is closed */
							$('#loadQueueProcess').on("hide.bs.modal", function () {
								clearInterval(processInterval);
							});
						}
						break;
					case 'stop':
						bootbox.dialog({
						    message: d.MESSAGE,
						    title: 'Success',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {
						            	GetCampaignList();
						            }
						        }
						    }
						});
						break;
				}
			} else {
				bootbox.dialog({
				    message: d.MESSAGE,
				    title: 'Oops!',
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {
				            	GetCampaignList();
				            }
				        }
				    }
				});
			}
		})
		.fail(function(e) {
			console.log(e);
		})
		.always(function() {
			$('#processingPayment').hide();
		});
	}


	ChangeShortCodeCallback = function () {
        GetCampaignList();
	};

	$(document).ready(function(){
		var table = $('#tblCampaignList').dataTable();
		setInterval(function(){
			//table.fnDraw(false);
			arrListChecked=[];
			var haveInpocessBlast=0;
			var ajaxData=table.fnGetData();
			$.each(ajaxData, function(index, value ) {
				if(value.INPROCESS !=0)
				{
					haveInpocessBlast=1;
				}
			});

			if(haveInpocessBlast==1){
				// get all checkbox checked
				$(".fa-check-square-o").each(function(){
					arrListChecked.push($(this).data('id'));
				})
				table.fnDraw(false);
			}

		}, 10000);

	});

	// $('#campaign-mlp').on('click', function(){
	// 	var cpgMLPImage = $('.campaign-mlp-header img').attr('src');
	// 	if(cpgMLPImage != undefined && cpgMLPImage.search('data:image/') < 0){
	// 		$.ajax({
	// 			url: '/session/sire/models/cfc/mlp.cfc?method=ConvertMLPImageToBase64&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	// 			type: 'POST',
	// 			dataType: 'json',
	// 			data: {inpURL: $('.campaign-mlp-header img').attr('src')},
	// 			success: function(data){
	// 				if(data.RXRESULTCODE == 1){
	// 					$('.campaign-mlp-header img').attr('src', data.URL);
	// 				}
	// 			}
	// 		});
	// 	}


	// });

	$('body').on('click', '#campaign-mlp-logo', function(){
		$('#campaign-mlp-logo-upload').click();
	});

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#campaign-mlp-logo')
	                .attr('src', e.target.result);

				console.log('test');
	        };

	        reader.readAsDataURL(input.files[0]);

	    }
	}

$('#campaign-mlp').on('click', function(){

});

// <!--- Only load the image browser when it is clicked --->
$('a[data-toggle="MLPImagePicker"]').on("click touchstart", function(e){
    e.preventDefault()
    var loadurl = $(this).attr('href')
    var targ = $(this).attr('data-target')


    $.get(loadurl, function(data) {
        $(targ).html(data)

    });

    $('#btn-upload-image').hide();
    $('#btn-use-image-link').hide();

    $(this).tab('show')

    // <!--- hack for touch devices not to fire click/touch events twice --->
	e.stopPropagation();

});

$('a[data-toggle="tab"]').on("click touchstart", function(e){

    $('#btn-upload-image').show();
    $('#btn-use-image-link').hide();

    $(this).tab('show')

    // <!--- hack for touch devices not to fire click/touch events twice --->
	e.stopPropagation();

});

$('a[data-toggle="tab3"]').on("click touchstart", function(e){

    $('#btn-upload-image').hide();
    $('#btn-use-image-link').show();

 //    var $TargetObj = $('#ImageChooserModal').data('data-target');

	// // <!--- make sure this obj exists --->
	// if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
	// {
	// 	$('#MLPImageURL').val($TargetObj.find('img').attr('src'));
	// }
	// else
	// {
	// 	var bg_url = $TargetObj.css('background-image');
	//     // ^ Either "none" or url("...urlhere..")
	//     bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
	//     bg_url = bg_url ? bg_url[2] : ""; // If matched, retrieve url, otherwise ""

	// 	$('#MLPImageURL').val(bg_url);
	// }

    $(this).tab('show')

    // <!--- hack for touch devices not to fire click/touch events twice --->
	e.stopPropagation();

});

$('#btn-use-image-link').on("click touchstart", function(e){

  	// <!--- Store last state of the MLP --->
	// StoreMLPBeforeAction(true);

  	// <!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
	// var $TargetObj = $('#ImageChooserModal').data('data-target');

	// <!--- make sure this obj exists --->
	// if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
	// {
	// 	$TargetObj.find('img').attr('src', $('#MLPImageURL').val())
	// }
	// else
	// {
	// 	$TargetObj.css('background-image', 'url(' + $('#MLPImageURL').val() + ')');
	// 	$('#mlp-section-settings #mlp-background-image-url').html($('#MLPImageURL').val());
	// }

	$('#campaign-mlp-logo').attr('src', $('#MLPImageURL').val());
	$('#campaign-mlp-logo').attr('name', $('#MLPImageURL').val());


	$('#ImageChooserModal').modal('hide');

  	// <!--- hack for touch devices not to fire click/touch events twice --->
	e.stopPropagation();

});

$(".MLPDropZone").dropzone( {
// var myDropzone = new Dropzone( '.MLPDropZone', {
	url: "/session/sire/models/cfc/image.cfc?method=SaveImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
	thumbnailWidth: 125,
	thumbnailHeight: 125,
	uploadMultiple: false,
	autoQueue: false,
	maxFiles: 1,
	parallelUploads: 1,
	paramName: "upload",
	acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg,image/svg+xml,.svg",
	resize: function(file) {
    	var resizeInfo = {
            srcX: 0,
            srcY: 0,
            trgX: 0,
            trgY: 0,
            srcWidth: file.width,
            srcHeight: file.height,
            trgWidth: this.options.thumbnailWidth,
            trgHeight: this.options.thumbnailHeight
        };

		return resizeInfo;
    },
    init: function() {

	    this.on("maxfilesexceeded", function(file){
	       // this.removeFile(file);
	    });

	    this.on("addedfile", function() {
	      if (this.files[1]!=null){
	        this.removeFile(this.files[0]);
	      }
	    });

	    // Execute when file uploads are complete
	    this.on("complete", function() {

		  this.removeAllFiles(true);

	      // If all files have been uploaded
	      if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
	        var _this = this;
	        // Remove all files
	        _this.removeAllFiles(true);

	        if(this.files.length != 0){
	            for(i=0; i<this.files.length; i++){
	                this.files[i].previewElement.remove();
	                this.files[i].accepted = false;
	            }
	            this.files.length = 0;
	        }

	      }
	    });

	    this.on("addedfile", function(file) {


			var _this = this;

			// <!--- Bad bug - this click event would not work a second time because it was retaining an old reference to the "file" but not the most recent file ''doh --->
			$("#btn-upload-image").off('click');

			// Hookup the start button
			$("#btn-upload-image").on("click touchstart", function(e){
				_this.enqueueFile(file);

				// <!--- hack for touch devices not to fire click/touch events twice --->
				e.stopPropagation();

			 });

			if (file.size > maxFilesize) {
				_this.removeFile(file);
				bootbox.alert('Warning!! ' + "This file is too large! Max file size is 10MB");
			}

			var allFilesAdded = _this.getFilesWithStatus(Dropzone.ADDED);
			var userUsage = $("#inpUserUsage").val();
			var userStoragePlan = $("#inpUserStoragePlan").val();

			var totalFileSize = 0;
			$.each(allFilesAdded, function(index, val) {
				 totalFileSize += val.size;
			});

			if (parseInt(parseInt(userUsage) + parseInt(totalFileSize)) > parseInt(userStoragePlan)) {
				bootbox.alert('Storage limit exceeded! Cannot upload this file');
				$("#btn-upload-image").on("click touchstart", function(e){  <!--- hack for touch devices not to fire click/touch events twice --->
					e.stopPropagation();

				});
			}

			var plainFileName = file.name.substr(0, file.name.lastIndexOf('.'));
			var fileType = file.name.substr(file.name.lastIndexOf('.')+1, file.name.length);
			if (RegName.test(plainFileName) && plainFileName.length <= 255) {
				try {
					$.ajax({
						url: '/session/sire/models/cfc/image.cfc?method=CheckImageName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						dataType: 'json',
						data: {inpPlainFileName: plainFileName, inpFileType: fileType.toLowerCase()}
					})
					.done(function(data) {
						if (parseInt(data.RXRESULTCODE) == 1) {
							if (parseInt(data.CHECKRESULT) == 1) {
								bootbox.dialog({
									message: "This file name ("+plainFileName+"."+fileType+") is already exist! Do you want to replace existing file?",
									title: 'Warning!!',
									buttons: {
										yes: {
											label: "Yes",
											className: "btn btn-medium btn-success-custom",
											callback: function() {

											}
										},
										no: {
											label: "No",
											className: "btn btn-medium btn-primary btn-back-custom",
											callback: function() {
												_this.removeFile(file);
											}
										},
									}
								});
							} else if (parseInt(data.CHECKRESULT) == 2) {
								bootbox.alert('Filename error ' + data.MESSAGE);
							}
							else
							{
								// <!--- file added ok - nothing to do --->

							}
						} else {
							bootbox.alert('Warning!! ' + (data.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : data.ERRMESSAGE));
						}

						$('#processingPayment').hide();
					})
// <!---
// 							.complete(function() {
// 								$('#processingPayment').hide();
// 							});
// --->

					$("#btn-upload-image").prop("disabled", false);


				} catch (e) {

					bootbox.alert('Warning!! ' + "Can't connect to server! Please refresh and try again later!");
				}
			} else {
				_this.removeFile(file);
				if (!RegName.test(plainFileName)) {
					bootbox.alert('Warning!! ' + 'Filename syntax error!<br>Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only');
				} else {
					bootbox.alert('Warning!! ' + 'Filename is too long. Please use filename less than 256 characters');
				}
			}
		});

		// Update the total progress bar
		this.on("totaluploadprogress", function(progress) {
// <!--- 			document.querySelector("#total-progress .progress-bar").style.width = progress + "%"; --->
		});

		this.on("sending", function(file) {
			// Show the total progress bar when upload starts
// <!--- 			document.querySelector("#total-progress").style.opacity = "1"; --->
			// And disable the start button
			$("#btn-upload-image").prop("disabled", true);
		});

		// Hide the total progress bar when nothing's uploading anymore
		this.on("queuecomplete", function(progress) {
// <!--- 			document.querySelector("#total-progress").style.opacity = "0"; --->

		});

		// Hide file when upload done
		this.on("success", function (file, result) {

			this.removeFile(file);
			this.removeAllFiles(true);

			if (parseInt(result.RXRESULTCODE) == 1) {

				// <!--- Store removal target in global variable so it can be undone --->
				// StoreMLPBeforeAction(true);

				// // <!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
				// var $TargetObj = $('#ImageChooserModal').data('data-target');

				// // <!--- make sure this obj exists --->
				// // <!--- update img obj or background depeneding on where this is opened from  --->
				// if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
				// {
				// 	$TargetObj.find('img').attr('src', result.IMAGEURL)
				// }
				// else
				// {
				// 	$TargetObj.css('background-image', 'url(' + result.IMAGEURL + ')');
				// 	$('#mlp-section-settings #mlp-background-image-url').html(result.IMAGEURL);
				// }

				// var oldImg = $('img[src$="'+result.IMAGETHUMBURL+'"]');

				// if (oldImg.length > 0) {
				// 	var d = new Date();
				// 	oldImg.attr('src', oldImg.attr('src')+'?'+d.getTime());
				// } else {
				// 	var html = '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 image-list-item">'+
				// 					'<img class="image" src="'+result.IMAGETHUMBURL+'" alt="'+result.IMAGENAME+'">'+
				// 					'<button class="btn btn-small btn-danger image-delete" data-imageid="'+result.IMAGEID+'">Delete</button>'+
				// 				'</div>';
				// 	var imageList = $('#image-list');
				// 	if (imageList.children().length == 0) {
				// 		imageList.text('');
				// 	}
				// 	imageList.prepend(html);
				// }

				$('#campaign-mlp-logo').attr('src', result.IMAGEURL);
				$('#campaign-mlp-logo').attr('name', result.IMAGEURL);


				updateUserUsageInfo();

				// <!--- close the image chooser modal --->
				$('#ImageChooserModal').modal('hide');

				$("#btn-upload-image").prop("disabled", true);

			} else {
				bootbox.alert('Warning!! ' + (result.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : result.ERRMESSAGE));
			}
		});
	}
});

	function updateUserUsageInfo()
	{
		try {
			$.ajax({
				url: '/session/sire/models/cfc/image.cfc?method=GetUserStorageInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					var userUsage = parseInt(data.USERUSAGE);
					var userStoragePlan = parseInt(data.USERSTORAGEPLAN);

					var usagePercent = parseFloat(Math.round((userUsage/userStoragePlan) * 100 * 100) / 100).toFixed(2);

					$("#usageProgress").css('width', usagePercent+'%');
					$("#inpUserUsage").val(userUsage);

					var usageMBSize = userUsage / 1048576;
					if (usageMBSize > 1024) {
						usageMBSize = (usageMBSize / 1024);
						usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'GB';
					} else {
						usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'MB';
					}
					$("#userUsageSpan").text(usageMBSize);

					var userStoragePlanMBSize = userStoragePlan / 1048576;
					if (userStoragePlanMBSize > 1024) {
						userStoragePlanMBSize = (userStoragePlanMBSize / 1024);
						userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'GB';
					} else {
						userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'MB';
					}
					$("#userStoragePlanSpan").text(userStoragePlanMBSize);
				} else {
					alertMessage('Oops!', data.ERRMESSAGE == '' ? 'Error when checking storage! Please try again later!' : data.ERRMESSAGE);
				}
			})
// <!---
// 			.error(function() {
// 				alertMessage('Oops!', 'Internal Server Error!');
// 			});
// --->
		} catch (e) {
			alertMessage('Oops!', 'Internal Server Error!');
		}
	}

})(jQuery);
