(function($){
	var inpBatchId = $('input[name="inpBatchID"]').val();
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	var templateType = $('input[name="templateType"]').val();
	var templateId = $('input[name="templateId"]').val();
	var campaignName = $('input[name="campaignName"]').val();
	var filter = '';
	var sortDir = 'DESC';

	function InitSubcriberListDS(subID){

		var subID = typeof(subID)!='undefined'?subID:0;

		var table = $('#tblSubcriberListOldReport').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"sName": 'select', "sTitle": 'Subscribers', "bSortable": false, "sClass": "select", "sWidth": "200px"},
	            {"sName": 'Name', "sTitle": 'Number Of Messages Sent', "bSortable": false, "sClass": "name center", "sWidth": "250px"},
	            {"sName": 'opt-in', "sTitle": "Opt in's", "bSortable": false, "sClass": "opt-in center","sWidth": "120px"},
	            {"sName": 'opt-out', "sTitle": "Opt-out's", "bSortable": false, "sClass": "opt-out center","sWidth": "120px"},
	            {"sName": 'Action', "sTitle": 'Response Percentage', "bSortable": false, "sClass": "action center","sWidth": "250px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetCampaignReportById'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblSubcriberListOldReport > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblSubcriberListOldReport_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblSubcriberListOldReport_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpGroupId", "value": subID}
	            );

		        aoData.push(
		            { "name": "inpType", "value": templateType}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": $("#inpBatchId").val()}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){

			}
		});
	}

	function InitSubcriberListDSNew(subID){ // use for some template

		var subID = typeof(subID)!='undefined'?subID:0;
		var actionCol = function(object){

			// return 	$('<button class="btn-re-edit"></button>').attr('data-id', object.ID).append($('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'))[0].outerHTML+
			// 		$('<button class="btn-re-dowload"></button>').attr('data-id', object.ID).append($('<i class="fa fa-download" aria-hidden="true"></i>'))[0].outerHTML+
			// 		$('<button class="btn-re-delete"></button>').attr('data-id', object.ID).append($('<i class="fa fa-times" aria-hidden="true"></i>'))[0].outerHTML;

			return $('<a class="btn btn-edit-cp-new">Edit</a>').attr('data-id', object.ID)[0].outerHTML + $('<div class="dropdown action-cp-dropdown-list"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu"><li><a class="download-sub-new" data-id="'+object.ID+'">Download</a></li><li><a class="del-sub-new" data-id="'+object.ID+'">Delete</a></li></ul></div>')[0].outerHTML;

		}

		var selectCol = function(object){
			return $('<a class="select-item" data-id="'+object.ID+'"><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>')[0].outerHTML;
		}


		var table = $('#tblSubcriberList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [

		    	{"mData": selectCol, "sName": 'select', "sTitle": '', "bSortable": false, "sClass": "select", "sWidth": "10px"},
	            {"mData": "NAME", "sName": 'Subscribers List', "sTitle": 'Subscribers List', "bSortable": false, "sClass": "name", "sWidth": "190px"},
	            {"mData": "OPTIN", "sName": 'opt-in', "sTitle": "Opt-in's", "bSortable": false, "sClass": "opt-in", "sWidth": "110px"},
	            //{"mData": "OPTOUT", "sName": 'opt-out', "sTitle": "Opt-out's", "bSortable": false, "sClass": "opt-out", "sWidth": "110px"},
	            {"mData": actionCol, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "action", "sWidth": "160px"}

			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetCampaignReportByIdNew'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblSubcriberList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblSubcriberList_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblSubcriberList_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpGroupId", "value": subID}
	            );

		        aoData.push(
		            { "name": "inpType", "value": templateType}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": $("#inpBatchId").val()}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
				var firstRow = this.find('tbody tr').first();
				firstRow.find('td a.select-item')
					   .addClass('active').parents('tr')
					   .siblings().find('td a.select-item')
					   .removeClass('active');

				var scbID = firstRow.find('td a.select-item').data('id');
				if( scbID > 0){
					firstRow.addClass('active');
				}

				$('input[name="sub-id"]').val(scbID);
				$('#total-subcriber').text(firstRow.find('td.opt-in').text());
				InitSubcriberListDetailDS(scbID);
			}
		});
	}

	$('#tblSubcriberList').on('click', 'tbody tr', function(e){
		$(this).find('td a.select-item').addClass('active').parents('tr').siblings().find('td a.select-item').removeClass('active');

		$('#tblSubcriberList').find('tr').removeClass('active');

		$(this).find('td a.select-item').parents('tr').addClass('active');

		var scbID = $(this).find('td a.select-item').data('id');
		$('input[name="sub-id"]').val(scbID);
		$('#total-subcriber').text($(this).find('td.opt-in').text());
		InitSubcriberListDetailDS(scbID);
	});


	function InitSubcriberListDetailDS(GroupID ,keyword){
			var keyword = typeof(keyword) != 'undefined' ? keyword : "";
			var contactCol = function(object){
				return '<a class="link-edit-contact" href="/session/sire/pages/subscriber-edit?contactId=' + object.ID + '" title="Click to edit">' + object.CONTACT + '</a>';
			}

			var table = $('#subcriberListDetail').dataTable({
				"bStateSave": false,
				"iStateDuration": -1,
				"bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "input",
			    "bLengthChange": false,
				"iDisplayLength": 20,
				"bAutoWidth": false,
			    "aoColumns": [
			    	{"mData": contactCol, "sName": 'Phone', "sTitle": 'Phone', "bSortable": false, "sClass": "contact", "sWidth": "110px"},
		            {"mData": "OPTIN", "sName": 'Optin', "sTitle": 'Date Subscribed', "bSortable": false, "sClass": "optin", "sWidth": "190px"}
				],
				"sAjaxDataProp": "DATALIST",
				"sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupDetailsForDatatableReport'+strAjaxQuery,
			   	"fnDrawCallback": function( oSettings ) {
				  		if (oSettings._iDisplayStart < 0){
						oSettings._iDisplayStart = 0;
						$('input.paginate_text').val("1");
					}

					 /* hide paginate section if table is empty */
	                $('#subcriberListDetail > tbody').find('tr').each(function(){
	                    var tdFirst = $(this).find('td:first');
	                    if(tdFirst.hasClass('dataTables_empty')){
	                        $('#subcriberListDetail_paginate').hide();
	                        return false;
	                    }
	                    else{
	                        $('#subcriberListDetail_paginate').show();
	                    }
	                });
			    },
				"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
					aoData.push(
			            { "name": "INPGROUPID", "value": GroupID}
		            );
		            aoData.push(
			            { "name": "inpKeyword", "value": keyword}
		            );
		            aoData.push(
			            { "name": "inpBatchId", "value": $("#inpBatchId").val()}
		            );
					oSettings.jqXHR = $.ajax({
				        "dataType": 'json',
				        "type": "POST",
				        "url": sSource,
				        "data": aoData,
				        "success": function(data){
				        	if(data.DATALIST.length == 0) {
				        		$('#subcriberListDetail_paginate').hide();
				        	}
				        	fnCallback(data);

				        }
			      	});
		        },
				"fnInitComplete":function(oSettings){
					$(".link-edit-contact").mask("(000) 000-0000");
				}
			});
	}

	$('span.btn-search-contact').click(function(e){
		var keyword = $('input[name="contact-keyword"]').val();
		var subID = $('input[name="sub-id"]').val();
		InitSubcriberListDetailDS(subID, keyword);
	});

	//ADD NEW SUBCRIBER LIST
	$('#btn-add-new').click(function(){
		$('#subscriber_list_name').val('').validationEngine('hide');
		$('#AddNewSubscriberList button').prop('disabled', false);
    	$('#AddNewSubscriberList').modal('show');
    });


    $('#AddNewSubscriberList form[name="frm-add-new"]').validationEngine('attach', {
		promptPosition : "topLeft",
		autoPositionUpdate: true,
		showArrow: false,
		autoHidePrompt: true,
		autoHideDelay: 5000
	}).on('submit', function(event){
    	event.preventDefault();

		if ($(this).validationEngine('validate')) {
			var thisForm = $(this);
			var thisModal = $('#AddNewSubscriberList');
			var groupName = thisForm.find('#subscriber_list_name').val();
			thisForm.find('button').prop('disabled', true);

			$.ajax({
				url: '/session/cfc/multilists2.cfc?method=addgroup'+strAjaxQuery,
				type: 'POST',
				dataType: 'json',
				data: {INPGROUPDESC: groupName},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#processingPayment').hide();
					thisForm.find('button').prop('disabled', false);
				},
				success: function(data) {
					$('#processingPayment').hide();
					if (data.DATA && data.DATA.RXRESULTCODE && data.DATA.RXRESULTCODE[0] == 1) {
						alertBox("Add new subcriber list success!", 'ADD NEW SUBSCRIBER LIST', function(){
							thisModal.modal('hide');
							InitSubcriberListDSNew();
						});
					} else {
						alertBox(data.DATA.MESSAGE[0], 'ADD NEW SUBSCRIBER LIST');
					}
					thisForm.find('button').prop('disabled', false);
					InitSubcriberListDSNew();
					LoadSelectSubscriber();
				}
			});
		}
	});

	function LoadSelectSubscriber(){
		// $.ajax({
		// 	url: "/session/sire/models/cfc/subscribers.cfc?method=GetSubcriberListByUserID"+strAjaxQuery,
		// 	dataType: "json",
		// 	success: function(data){
		// 		var subSelectBox = $('select[name="subcriber-list"]');
		// 		subSelectBox.html('<option value="0">All</option>');
		// 		if(data.RXRESULTCODE == 1){
		// 			data.DATALIST.forEach(function(item){
		// 				subSelectBox.append($('<option></option>').attr('value', item.ID).text(item.NAME));
		// 			});
		// 		} else {
		// 			alertBox(data.MESSAGE, 'alert');
		// 		}
		// 	},
		// 	complete: function(responseText){

		// 	}
		// });

		$.ajax({
			url: "/session/sire/models/cfc/subscribers.cfc?method=GetSubcriberListByBatchID",
			dataType: "json",
			data: {
				inpBatchId: $("#inpBatchId").val(),
				inpType: $("#templateType").val()
			},
			success: function(data){
				var subSelectBox = $('select[name="subcriber-list"]');
				subSelectBox.html('<option value="0">All</option>');
				if(data.RXRESULTCODE == 1){
					data.DATALIST.forEach(function(item){
						subSelectBox.append($('<option></option>').attr('value', item.ID).text(item.NAME));
					});

					$('select[name="subcriber-list"]').selectpicker();
				} else {
					alertBox(data.MESSAGE, 'alert');
				}
			},
			complete: function(responseText){

			}
		});


	};

	$('select[name="subcriber-list"]').on('change', function(event){
		var subID = $(this).val();
		InitSubcriberListDSNew(subID);

		var startDate = $('input[name="dateStart"]').data('date-start');
		var endDate = $('input[name="dateEnd"]').data('date-end');
		cb(startDate, endDate);
	});

	var start = moment().subtract(29, 'days');
    var end = moment();
    var batchCreatedDate = $('#batchCreatedDate').val();
    var minDate = moment(batchCreatedDate);

    if(start.diff(minDate, 'days') < 0){
    	start = minDate;
    }

    $('.reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        "dateLimit": {
        	"years": 1
    	},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        minDate: minDate
    }, cb);

    function cb(start, end) {

        $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
        $('input[name="dateStart"]').data('date-start', start);
        $('input[name="dateEnd"]').data('date-end', end);
        var inpSubID = $('select[name="subcriber-list"]').val();

        $.ajax({
            url: "/session/sire/models/cfc/campaign.cfc?method=GetStatistic"+strAjaxQuery,
            method: "POST",
            dataType: "json",
            data: {
            	inpBatchId: inpBatchId,
            	inpGroupId: inpSubID,
            	inpType: $("#templateType").val(),
                inpStartDate: dateStart,
                inpEndDate: dateEnd,
                inpTemplateId: templateId
            },
            success: function(data){
                var series = [];
                var months = [];

                data.DataList.forEach(function(item){

                	var dayData;
					switch(item.format){
                    	case "day": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "week": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "month": {
                    		months.push(moment(item.Time).format('MMM DD YYYY'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    }

                    series.push({meta: dayData, value: item.Value});

                });


                var chart_data = {labels: months,series:[ series]};
                /* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */
				var options = {
					//fullWidth: true,
					chartPadding: {
						right: 0
					},
					axisY: {
						showGrid: false,
						offset: 20,
						onlyInteger: true,
						position: 'start',
					},
					axisX: {
						labelOffset: {
					      x: 1,
					      y: 1
					    }
					},
					plugins: [
					    Chartist.plugins.tooltip({
					    	currency: parseInt(templateId) !== 9 ? "Opt in's: " : "Response: ",
						    class: 'chart-tooltip-custom',
						    appendToBody: true
					    })
					],
					low: 0,
					height: '300px',
					lineSmooth: false,
					classNames: {
					    chart: 'ct-chart-line',
					    //label: 'ct-label-custom',
					    labelGroup: 'ct-labels',
					    series: 'ct-series',
					    line: 'ct-line',
					    point: 'ct-point',
					    area: 'ct-area',
					    grid: 'ct-grid-custom',
					    gridGroup: 'ct-grids',
					    gridBackground: 'ct-grid-background',
					    vertical: 'ct-vertical',
					    horizontal: 'ct-horizontal',
					    // start: 'ct-start',
					    // end: 'ct-end'
					 }
				};

				new Chartist.Line('#highchart', chart_data, options);
            }
        });
    }

    function InitSubcriberListDSBlast(subID){

		var subID = typeof(subID)!='undefined'?subID:0;

		var numberMessageSentCol = function(object){
			return $('<a class="get-blast-message-list" data-groupid="'+object[6]+'" data-id="'+$("#inpBatchId").val()+'">'+ object[1] +'</a>')[0].outerHTML;
		}

		var numberMessageQueuedCol = function(object){
			return $('<a class="get-queued-message-list" data-groupid="'+object[6]+'" data-id="'+$("#inpBatchId").val()+'">'+ object[2] +'</a>')[0].outerHTML;
		}

		var table = $('#tblSubcriberListBlast').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"sName": 'select', "sTitle": 'Subscribers', "bSortable": false, "sClass": "name", "sWidth": "120px"},
	            {"mData": numberMessageSentCol,"sTitle": 'Number Of Messages Sent', "bSortable": false, "sClass": "name center", "sWidth": "200px"},
	            {"mData": numberMessageQueuedCol, "sTitle": 'Number Of Messages Left in Queue', "bSortable": false, "sClass": "name center", "sWidth": "250px"},
	            // {"sName": numberMessageWaitCol, "sTitle": 'Messages Waiting', "bSortable": false, "sClass": "name center", "sWidth": "250px"},
	            {"sName": 'opt-in', "sTitle": "Opt in's", "bSortable": false, "sClass": "opt-in center", "bVisible":false},
	            // {"sName": 'opt-out', "sTitle": "Opt out", "bSortable": false, "sClass": "opt-out center","sWidth": "120px"},
	            // {"sName": 'Action', "sTitle": 'Response Percentage', "bSortable": false, "sClass": "action center"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetCampaignReportById'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblSubcriberListBlast > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpGroupId", "value": subID}
	            );

		        aoData.push(
		            { "name": "inpType", "value": templateType}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": $("#inpBatchId").val()}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){

			}
		});
	}

	// Get blast list by campaign id
	function GetBlastListByCampaignId(groupid,strFilter){
        $("#message-blast-sent-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}

        var table = $('#message-blast-sent-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                // {"mData": "ID", "sName": '', "sTitle": 'Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetBlastListByCampaignId'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchId", "value": $("#inpBatchId").val()}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                aoData.push(
                    { "name": "inpGroupId", "value": groupid}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
			        		$('#message-blast-sent-table_paginate').hide();
			        	}
			        	fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }

    function InitFilterBuyCreditDetailsTable (groupid) {
        $("#blastMessageSentFilterBox").html('');
        //Filter bar initialize
        $('#blastMessageSentFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                GetBlastListByCampaignId(groupid);
            }
        }, function(filterData){
            GetBlastListByCampaignId(groupid,filterData);
        });
    }

    $('body').on('click', '.get-blast-message-list', function(){
    	var GroupId = $(this).data('groupid');
        InitFilterBuyCreditDetailsTable(GroupId);
        GetBlastListByCampaignId(GroupId);
        $("#messages-sent-modal").modal("show");
        $("#blastMessageSentFilterBox .btn-add-item").hide();
    });

    // MULTIPLE CHOICE
	function InitMultipleChoiceBlast(BatchId){
    	var BatchId = typeof(BatchId)!='undefined'?BatchId:0;

    	var selectCol = function(object){
			return $('<a class="select-item" data-val="'+object.VAL+'"" data-id="'+object.ID+'"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>')[0].outerHTML;
		}

		var table = $('#tblMultiplechoiceList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": selectCol, "sName": 'select', "sTitle": '', "bSortable": false, "sClass": "select", "sWidth": "10px"},
		    	{"mData": 'TEXT', "sName": '', "sTitle": 'Response <i class="fa fa-question-circle report-popover" aria-hidden="true" data-toggle="popover" data-content=""></i>', "bSortable": false, "sClass": "select", "sWidth": "200px"},
	            {"mData": "SUM", "sName": '', "sTitle": 'Count', "bSortable": false, "sClass": "name", "sWidth": "200px"},
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCampaignReport'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				$('#tblMultiplechoiceList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpBatchId", "value": BatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0 || data.DATALIST.length < 20) {
			        		$('#tblMultiplechoiceList_paginate').hide();
			        	}
			        	fnCallback(data);
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				var firstRow = this.find('tbody tr').first();
				firstRow.find('a.select-item').addClass('active');
				firstRow.addClass('active');
				var aval = firstRow.find('a.select-item').data('val');
				InitMultipleChoiceBlastDetails(inpBatchId,aval);

				var question = oSettings.jqXHR.responseJSON.QUESTION;
               	$('.report-popover').attr('data-content',question);

				$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });
			}
		});
    }

	function InitMultipleChoiceBlastDetails(BatchID,SelectVal){

		var SelectVal = typeof(SelectVal) != 'undefined' ? SelectVal : "";

		var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.CONTACTSTRING+'</span>';
		}

		var table = $('#tblMultiplechoiceDetailsList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": "CREATED", "sName": '', "sTitle": 'Date & Time', "bSortable": false, "sClass": "contact", "sWidth": "200px"},
	            {"mData": formatContactString, "sName": '', "sTitle": 'Phone Number', "bSortable": false, "sClass": "optin", "sWidth": "200px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCustomerByResponseAndBatch'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				$('#tblMultiplechoiceDetailsList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				aoData.push(
		            { "name": "inpBatchId", "value": BatchID}
	            );
	            aoData.push(
		            { "name": "inpAVAL", "value": SelectVal}
	            );

	            aoData.push(
		            { "name": "inpCPType", "value": 'ONESELECTION'}
	            );
				oSettings.jqXHR = $.ajax({
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0) {
			        		$('#tblMultiplechoiceDetailsList_paginate').hide();
			        	}
			        	fnCallback(data);

			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
		});
	}

    function DisplayCampaignReportData(firstTime){

        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
        var templateReportArr = ['1','2','3','9','6','5','4','7','8','10'];
        var index = templateReportArr.indexOf(templateId);

        if(index >= 0)
        {
        	if(templateType == 1 || templateId==10 ){
    			$('.optin-display').hide();
				InitSubcriberListDSBlast();
				$('.wrapper-tblcCampaignReportData').removeClass('col-md-12');
				$('.wrapper-tblcCampaignReportData').addClass('col-md-6');
				$(".date-range").hide();
				$(".select-list").hide();
				//$("#export-multiple-choice").hide();
				// Show report response blast
				InitFilterResponseBlastTable();
				GetResponseBlastListByCampaignId();

			}
			else{
				displayFunctionKeywordTypeForNewReport(templateId);
    			$("#tblSubcriberListBlast").parent('div').hide();
			}

			$("#tblSubcriberListOldReport").parent('div').hide();
        }
        else{

        	$("#tblcCampaignReportData").parent('div').hide();
        	$("#tblSubcriberListBlast").parent('div').hide();
        	$('.optin-display').hide();
        	$('.export-data').hide();
        	displayFunctionKeywordTypeForOldReport();
        }

        if(templateId == 1 || templateId == 2 || templateId == 10){

        	displayCampaginBuildYourFirst(dateStart,dateEnd,firstTime);

        	if(templateType == 1){
        		//$(".wrapper-tblcCampaignReportData").parent('div').hide();
        	}

        	if(templateId == 10){
        		$("#export-multiple-choice").hide();
        		$(".wrapper-tblcCampaignReportData").parent('div').hide();
        	}
        }
    	else if(templateId == 9){
    		displayCampaginCollectFeedBack(dateStart,dateEnd,firstTime);
    	}
    	else if (templateId == 6){
    		displayCampaginOpenEnd(dateStart,dateEnd,firstTime);
    	}
    	else if (templateId == 5){ //Single Yes/No
    		$(".wrapper-tblcCampaignReportData").parent('div').hide();
    		$(".multiple-choice-section").hide();
    		displayCampaginYesNoQuestion(dateStart,dateEnd,firstTime);
    	}
    	else if(templateId == 4){
    		$(".wrapper-tblcCampaignReportData").parent('div').hide();
    		InitMultipleChoiceBlast(inpBatchId);
    	}
    	else if(templateId == 7){
    		$(".wrapper-tblcCampaignReportData").parent('div').hide();
    		InitMultipleChoiceSurvey(inpBatchId);
    		InitMultipleChoiceSurveyDetails(inpBatchId);
    	}
    	else if(templateId == 3 || templateId == 8){
    		$('.wrapper-tblcCampaignReportData, hr.sub-list-section, #export-multiple-choice').hide();
    	}
	}

	function displayFunctionKeywordTypeForNewReport(templateId){
		cb(start, end);

		if(templateId ==9){
			$('.optin-display').hide();
		}
		else{
			InitSubcriberListDSNew();
			LoadSelectSubscriber();
		}


	}

	function InitFilterResponseBlastTable () {
        $("#tblSubcriberListResponseBlastFilterBox").html('');
        //Filter bar initialize
        $('#tblSubcriberListResponseBlastFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Phone Number', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                GetResponseBlastListByCampaignId();
            }
        }, function(filterData){
            GetResponseBlastListByCampaignId(filterData);
        });
    }
    $('body').on("click", "button.btn-response-dowload.response-blast-list", function(event){
	    window.location.href="/session/sire/models/cfm/admin/admin-export-response-blast-list?inpBatchId="+$("#inpBatchId").val()+ "&sortDir="+sortDir +"&filter="+filter;
	});

    // Get response blast list by campaign id
	function GetResponseBlastListByCampaignId(strFilter){
        $("#tblSubcriberListResponseBlast").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}


        var table = $('#tblSubcriberListResponseBlast').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 20,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "BATCHID", "sName": '', "sTitle": 'Batch Id', "bSortable": false, "sClass": "", "sWidth": "120px"},
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone Number', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": "RESPONSE", "sName": '', "sTitle": 'Response', "bSortable": false, "sClass": "", "sWidth": "auto"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": true, "sClass": "", "sWidth": "200px"}
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetResponseBlastListByCampaignId'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchId", "value": $("#inpBatchId").val()}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                // this is set sortDir for export response blast data
                var hasSortDir = $("#tblSubcriberListResponseBlast th").hasClass("sorting_desc");
                if(hasSortDir){
                	sortDir = "DESC";
                }
                else{
                	sortDir = "ASC";
                }
                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
			        		$('#tblSubcriberListResponseBlast_paginate').hide();
			        	}
			        	fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }

	function displayFunctionKeywordTypeForOldReport(){
		cb(start, end);
		InitSubcriberListDS();
		LoadSelectSubscriber();
	}

	function displayCampaginCollectFeedBack(dateStart,dateEnd,firstTime){

		var totalDisplay = 20;
		if(firstTime == 1){
			var totalDisplay = 4;
		}

		var actionCol = function (object) {
			var strReturn = "";
			if (parseInt(object.LASTCP) == 3 && parseInt(object.SESSIONSTATE) == 4) {
				strReturn = '<a title="Respond to customer" class="chat-response" data-contactstring="'+object.CONTACTSTRING+'"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>';
			} else if (parseInt(object.SESSIONSTATE) == 1) {
				strReturn = '<a title="Respond to customer" href="/session/sire/pages/sms-response?ssid='+object.SESSIONID+'" target="_blank"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>';
			}
			return strReturn;
		}

		var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.CONTACTSTRING+'</span>';
		}

		var table = $('#tblcCampaignReportData').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": totalDisplay,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": 'CREATED', "sTitle": 'Date & Time', "bSortable": false, "sClass": "select", "sWidth": "220px"},
	            {"mData": formatContactString, "sTitle": 'Phone Number', "bSortable": false, "sClass": "name center","sWidth": "150px"},
	            {"mData": 'RESPONSE', "sTitle": 'Response <i class="fa fa-question-circle report-popover" aria-hidden="true" data-toggle="popover" data-content=""></i>', "bSortable": false, "sClass": "action center","sWidth": "220px"},
	            {"mData": actionCol, "sTitle": 'Action', "bSortable": false, "sClass": "center","sWidth": "150px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCampaignReport'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblcCampaignReportData > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpStartDate", "value": dateStart}
	            );

		        aoData.push(
		            { "name": "inpEndDate", "value": dateEnd}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": inpBatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },

			"fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
				if(firstTime == 1){
				 	$('#tblcCampaignReportData_paginate').hide();
				}
                if(oSettings._iRecordsTotal <= totalDisplay){
                	$('.expand-btn').hide();
                }
                else{
                	$('.expand-btn').removeClass('hidden');
                }

               var question = oSettings.jqXHR.responseJSON.QUESTION;
               $('.report-popover').attr('data-content',question);

				$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });
			}
		});
	}

	function displayCampaginBuildYourFirst(dateStart,dateEnd,firstTime){
		var totalDisplay = 20;
		if(firstTime == 1){
			var totalDisplay = 4;
		}

		var actionCol = function (object) {
			var strReturn = "";
			// if (parseInt(object.LASTCP) == 3 && parseInt(object.SESSIONSTATE) == 4) {
			// 	strReturn = '<a title="Respond to customer" class="chat-response" data-contactstring="'+object.CONTACTSTRING+'"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>';
			// } else if (parseInt(object.LASTCP) == 5 && parseInt(object.SESSIONSTATE) == 1) {
			// 	strReturn = '<a title="Respond to customer" href="/session/sire/pages/sms-response?ssid='+object.SESSIONID+'" target="_blank"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>';
			// }
			return strReturn;
		}
		var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.CONTACTSTRING+'</span>';
		}

		var table = $('#tblcCampaignReportData').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": totalDisplay,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": 'CREATED', "sTitle": 'Date & Time', "bSortable": false, "sClass": "select", "sWidth": "220px"},
	            {"mData": formatContactString, "sTitle": 'Phone Number', "bSortable": false, "sClass": "name center","sWidth": "150px"},
	            {"mData": 'RESPONSE', "sTitle": 'Response', "bSortable": false, "sClass": "action center","sWidth": "220px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCampaignReport'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblcCampaignReportData > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpStartDate", "value": dateStart}
	            );

		        aoData.push(
		            { "name": "inpEndDate", "value": dateEnd}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": inpBatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },

			"fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
				if(firstTime == 1){
				 	$('#tblcCampaignReportData_paginate').hide();
				}
                if(oSettings._iRecordsTotal <= totalDisplay){
                	$('.expand-btn').hide();
                }
                else{
                	$('.expand-btn').removeClass('hidden');
                }
			}
		});
	}


	function displayCampaginOpenEnd(dateStart,dateEnd,firstTime){

		var totalDisplay = 20;
		if(firstTime == 1){
			var totalDisplay = 4;
		}

		var table = $('#tblcCampaignReportData').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": totalDisplay,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": 'CREATED', "sTitle": 'Date & Time', "bSortable": false, "sClass": "select", "sWidth": "250px"},
	            {"mData": 'CONTACTSTRING', "sTitle": 'Phone Number', "bSortable": false, "sClass": "name center", "sWidth": "200px"},
	            {"mData": 'RESPONSE', "sTitle": 'Response <i class="fa fa-question-circle report-popover" aria-hidden="true" data-toggle="popover" data-content=""></i>', "bSortable": false, "sClass": "action center","sWidth": "180px"},
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCampaignReport'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblcCampaignReportData > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblcCampaignReportData_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblcCampaignReportData_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpStartDate", "value": dateStart}
	            );

		        aoData.push(
		            { "name": "inpEndDate", "value": dateEnd}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": inpBatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },

			"fnInitComplete":function(oSettings){

				if(firstTime == 1){
				 	$('#tblcCampaignReportData_paginate').hide();
				}
                if(oSettings._iRecordsTotal <= totalDisplay){
                	$('.expand-btn').hide();
                }
                else{
                	$('.expand-btn').removeClass('hidden');
                }

               var question = oSettings.jqXHR.responseJSON.QUESTION;
               $('.report-popover').attr('data-content',question);

				$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });
			}
		});
	}

	function displayCampaginYesNoQuestion(dateStart,dateEnd,firstTime){

		var totalDisplay = 20;
		if(firstTime == 1){
			var totalDisplay = 4;
		}

    	var selectCol = function(object){
			return '<a class="select-item" data-val="'+object.RESPONSE+'"><i class="fa fa-chevron-right icon-left" aria-hidden="true"></i></a>';
		}
		var table = $('#tblSingleYesNoList').dataTable({

			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": totalDisplay,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": selectCol, "sName": 'select', "sTitle": '', "bSortable": false, "sClass": "select", "sWidth": "10px"},
	            {"mData": 'RESPONSE', "sTitle": 'Response <i class="fa fa-question-circle syn-popup report-popover" aria-hidden="true" data-toggle="popover" data-content=""></i>', "bSortable": false, "sClass": "","sWidth": "220px"},
	            {"mData": 'COUNT', "sTitle": 'Count', "bSortable": false, "sClass": "select", "sWidth": "100px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCampaignReport'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				 /* hide paginate section if table is empty */
                $('#tblSingleYesNoList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblSingleYesNoList_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblSingleYesNoList_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpStartDate", "value": dateStart}
	            );

		        aoData.push(
		            { "name": "inpEndDate", "value": dateEnd}
	            );

		        aoData.push(
		            { "name": "inpBatchId", "value": inpBatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function (data) {
			        	$(".syn-popup").data('content', data.QUESTION);
			        	fnCallback(data);
			        }
		      	});
	        },

			"fnInitComplete":function(oSettings){

				if(firstTime == 1){
				 	$('#tblSingleYesNoList_paginate').hide();
				}
                if(oSettings._iRecordsTotal <= totalDisplay){
                	$('.expand-btn').hide();
                }
                else{
                	$('.expand-btn').removeClass('hidden');
                }

                var question = oSettings.jqXHR.responseJSON.QUESTION;
               	$('.report-popover').attr('data-content',question);

				$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });

				var firstRow = this.find('tbody tr').first();
				firstRow.find('td a.select-item').trigger('click');
			}
		});
	}

	function displayCampaginYesNoQuestionDetails(BatchID,SelectVal){

		var SelectVal = typeof(SelectVal) != 'undefined' ? SelectVal : "";

		var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.CONTACTSTRING+'</span>';
		}

		var table = $('#tblSingleYesNoDetailsList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": "CREATED", "sName": '', "sTitle": 'Date & Time', "bSortable": false, "sClass": "contact", "sWidth": "220px"},
	            {"mData": formatContactString, "sName": '', "sTitle": 'Phone Number', "bSortable": false, "sClass": "optin", "sWidth": "200px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetCustomerByResponseAndBatchSingleYesNo'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				$('#tblSingleYesNoDetailsList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#tblSingleYesNoDetailsList_paginate').hide();
                        return false;
                    }
                    else{
                        $('#tblSingleYesNoDetailsList_paginate').show();
                    }
                });
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				aoData.push(
		            { "name": "inpBatchId", "value": BatchID}
	            );
	            aoData.push(
		            { "name": "inpAVAL", "value": SelectVal}
	            );

	            aoData.push(
		            { "name": "inpCPType", "value": 'SHORTANSWER'}
	            );
				oSettings.jqXHR = $.ajax({
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0) {
			        		$('#tblSingleYesNoDetailsList_paginate').hide();
			        	}
			        	fnCallback(data);

			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
		});
	}

	$('#tblSingleYesNoList').on('click', 'tbody tr', function(e){
		$(this).find('td a.select-item').addClass('active').parents('tr').siblings().find('td a.select-item').removeClass('active');
		var aval = $(this).find('td a.select-item').data('val');
		$('#tblSingleYesNoList').find('tr').removeClass('active');
		$(this).find('td a.select-item').parents('tr').addClass('active');

		displayCampaginYesNoQuestionDetails(inpBatchId,aval);
	});

	$('#tblMultiplechoiceList').on('click', 'tbody tr', function(e){
		$(this).find('td a.select-item').addClass('active').parents('tr').siblings().find('td a.select-item').removeClass('active');
		var aval = $(this).find('td a.select-item').data('val');
		$('#tblMultiplechoiceList').find('tr').removeClass('active');
		$(this).find('td a.select-item').parents('tr').addClass('active');

		InitMultipleChoiceBlastDetails(inpBatchId,aval);
	});


	$('body').on("click", "#export-multiple-choice", function(event){
		event.preventDefault();
		var href = "";

		if(templateId == 7){
	    	href="/session/sire/models/cfm/export-survey-report.cfm?BatchId="+inpBatchId+"&BatchName="+campaignName;
	    }
	    else{
	    	href = "/session/sire/models/cfm/export-campaign.cfm?BatchId="+inpBatchId+"&BatchName="+campaignName+'&templateId='+templateId;
	    }
	    window.location.href = href;

   	 });


	$("body").on('click', '.chat-response', function(event) {
		event.preventDefault();
		var contactString = $(this).data('contactstring');

		StartChatSession(contactString);
	});

	var StartChatSession = function (contactString) {
		if (typeof contactString == "undefined" || contactString == "") {
			return false;
		}
		$.ajax({
			url: '/session/sire/models/cfc/smschat.cfc?method=MakeNewChatForTellMgr'+strAjaxQuery,
			type: 'POST',
			data: {
				inpBatchId: inpBatchId,
				inpContactString: contactString
			},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) > 0) {
				window.open("/session/sire/pages/sms-response?ssid="+data.SESSIONID, '_blank');
				$("a.chat-response[data-contactstring='"+contactString+"']").removeClass('chat-response').attr('href', '/session/sire/pages/sms-response?ssid='+data.SESSIONID+'').attr('target', '_blank');;
			} else {
				alertBox(data.MESSAGE, 'Oops!');
			}
		})
		.fail(function(e,msg) {
			console.log(msg);
		})
		.always(function() {
			$("#processingPayment").hide();
		});

	}

    $('button.download-subcriber').on('click', function(event){
    	var id = $('input[name="inpBatchId"]').val();
    	var groupID = $('select[name="subcriber-list"]').val();
    	var start = $('input[name="dateStart"]').data('date-start');
		var end = $('input[name="dateEnd"]').data('date-end');
		var type = $("#templateType").val();

		var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

		var params = {
			batchid: id,
			groupid: groupID,
			type: type,
			startdate: dateStart,
			enddate: dateEnd
		};

		params = $.param(params);
		window.location.href = "/session/sire/models/cfm/export-campaign-report.cfm?"+params
    });

    //Download subcriber list , comment out because same class btn-re-dowload, plz change to correct btn class
	/* $('body').on("click", "button.btn-re-dowload", function(event){
		var subID = $(this).data('id');
		window.location.href="/session/sire/models/cfm/export-subcriber-list.cfm?subid="+subID;
	}); */

	//Delete subcriber list
	$('body').on('click', 'button.btn-re-delete', function(event){
		var scbID = $(this).data('id');

		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetCampaignByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'json',
			data: {inpGroupId: scbID},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	error: function(XMLHttpRequest, textStatus, errorThrown) {
	     		$('#processingPayment').hide();
	     		alertBox('Send Get subscriber list detail request failed!', 'DELETE SUBCRIBER LIST', function(){});

	    	},
	    	success: function(data){
	    		$('#processingPayment').hide();
	    		if(data.RXRESULTCODE==1){
	    			var allCampaign = '';
	    			for (var i = 0; i < data['batchDetail'].length; i++) {
	    				allCampaign = allCampaign + '<li><a href="/session/sire/pages/campaign-edit?campaignid=' + data['batchDetail'][i].ID + '" target="_blank">' + data['batchDetail'][i].NAME + '</a></li>';
	    			}

	    			confirmBox("Are you sure you want to delete this subcriber list? It is being used by the following campaign(s): " + "<ol>" + allCampaign + "</ol>", 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Send Delete subscriber list request failed!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});

					     		InitSubcriberListDSNew();
					     		LoadSelectSubscriber();
					    	}
			    		});
	    			});
	    		}
	    		else if(data.RXRESULTCODE == 0){

	    			confirmBox('Are you sure you want to delete this subcriber list? ', 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Delete subscriber list request failed to send!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});

					     		InitSubcriberListDSNew();
					     		LoadSelectSubscriber();
					    	}
			    		});
	    			});
	    		}
	    		else{

		     		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});
	    		}
	    	}

		});
	});

	$('#rename-subscriber-list-form').on('submit', function(event){
		event.preventDefault();
		if($('#rename-subscriber-list-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			$.ajax({
    			url: '/session/sire/models/cfc/subscribers.cfc?method=UpdateSubscriberListNameByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
    			type: 'POST',
    			dataType: 'json',
    			data: {
    				inpGroupId: $('#contact-group-id').val(),
    				inpNewGroupName: $('#new-subscriber-list-name').val()
    			},
    			beforeSend: function( xhr ) {
		     		$('#processingPayment').show();
		    	},
		    	error: function(XMLHttpRequest, textStatus, errorThrown) {
		     		$('#processingPayment').hide();
		     		bootbox.dialog({
		         		message:'Update subscriber list detail failed!',
		         		title: "Rename subscriber list",
		         		buttons: {
		             		success: {
		                 		label: "Ok",
		                 		className: "btn btn-medium btn-success-custom",
		                 		callback: function() {}
		             		}
		         		}
		     		});
		    	},
		    	success: function(data){
		    		$('#processingPayment').hide();
		    		if(data.RXRESULTCODE == 1){
		    			$('#rename-subscriber-list').modal('hide');
			    			bootbox.dialog({
			         		message:data.MESSAGE,
			         		title: "Rename subscriber list",
			         		buttons: {
			             		success: {
			                 		label: "Ok",
			                 		className: "btn btn-medium btn-success-custom",
			                 		callback: function() {
			                 			location.reload();
			                 		}
			             		}
			         		}
			     		});
		    			InitGroupContact();
		    		}
		    		else{
		    			bootbox.dialog({
			         		message:data.MESSAGE,
			         		title: "Rename subscriber list",
			         		buttons: {
			             		success: {
			                 		label: "Ok",
			                 		className: "btn btn-medium btn-success-custom",
			                 		callback: function() {}
			             		}
			         		}
			     		});
		    		}
		    	}
    		});
		}
	});


	//Edit
	$('body').on('click', 'button.btn-re-edit', function(event){
		var groupId = $(this).data('id');
		$('#contact-group-id').val(groupId);
		$('#rename-subscriber-list').modal('show');
		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {inpGroupId: groupId},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	success: function(data){
	    		$('#new-subscriber-list-name').val(data.SUBSCRIBERLISTNAME);
	    		LoadSelectSubscriber();
		    },
		    complete: function(){
	    		$('#processingPayment').hide();

		    }
		});

	});

  	var ansIndex = 0;

	function InitMultipleChoiceSurveyDetails(BatchId){
		var BatchId = typeof(BatchId)!='undefined'?BatchId:0;
		var numberOfCol;
		var questionListText = [];
		var companyName = $('#companyName').val() + ':';

		$.ajax({
			url: '/session/sire/models/cfc/reports.cfc?method=GetCPByBatchIdAndType'+strAjaxQuery,
			type: 'POST',
			dataType: 'JSON',
			data: {inpBatchId: inpBatchId, inpCPType: 'ONESELECTION'},
			async: false,
			success: function(data){
				numberOfCol = data.DATALIST.length;
				for (var i = 0; i < numberOfCol; i++) {
					questionListText[i] = data.DATALIST[i].TEXT;
					questionListText[i] = questionListText[i].replace(companyName, "");
				}
			}
		});

		var allCol = [];
		allCol.push({"mData": 'CREATED', "sName": 'select', "sTitle": 'Date & Time', "bSortable": false, "sClass": "select", "sWidth": "220px"});
		allCol.push({"mData": 'PHONE', "sName": 'select', "sTitle": 'Phone Number', "bSortable": false, "sClass": "select", "sWidth": "200px"});

		var a = [];

		for (var i = 0; i < numberOfCol; i++) {
			a[i] = function(object){
				if (typeof object.ANSWERLIST[ansIndex] !== "undefined") {
					var html = '<h4>'+object.ANSWERLIST[ansIndex].ANSWER+'</h4>'
					ansIndex++;
				}
				if (ansIndex == numberOfCol) {
					ansIndex = 0;
				}
				return html;
			}
			allCol.push({"mData": a[i], "sName": 'select', "sTitle": "Question "+(i+1)+' '+'<i class="fa fa-question-circle report-popover" id="question-text-'+i+'" aria-hidden="true" data-toggle="popover" data-content="">', "bSortable": false, "sClass": "select", "sWidth": "150px"});
		}


		var table = $('#tblMultiplechoiceSurveyDetails').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": allCol,
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetReportDetailsForSurveyBatch'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpBatchId", "value": BatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0) {
			        		$('#tblMultiplechoiceSurveyDetails_paginate').hide();
			        	}
			        	fnCallback(data);
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				for (var i = 0; i < questionListText.length; i++) {
					$('#question-text-'+i).attr('data-content',questionListText[i]);
				}
               	$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });
			}
		});
	}

	function InitMultipleChoiceSurvey(BatchId){
		var BatchId = typeof(BatchId)!='undefined'?BatchId:0;
		var numberOfCol = 0;
		var questionListText = [];
		var companyName = $('#companyName').val() + ':';

		var answerChoice = {0 : 'A', 1 : 'B', 2 : 'C', 3 : 'D', 4 : 'E', 5 : 'F', 6 : 'G', 7 : 'H', 8 : 'I', 9 : 'J', 10 : 'K', 11 : 'L', 12 : 'M', 13 : 'N', 14 : 'O', 15 : 'P', 16 : 'Q', 17 : 'R', 18 : 'S', 19 : 'T', 20 : 'U', 21 : 'V', 22 : 'W', 23 : 'X', 24 : 'Y', 25 : 'Z'};

		$.ajax({
			url: '/session/sire/models/cfc/reports.cfc?method=GetMultipleChoiceBySurveyBatch'+strAjaxQuery,
			type: 'POST',
			dataType: 'JSON',
			data: {inpBatchId: inpBatchId},
			async: false,
			success: function(data){

                for (var i = 0; i < data.DATALIST.length; i++) {
                	if (numberOfCol < data.DATALIST[i].ALLVAL.length){
                		numberOfCol = data.DATALIST[i].ALLVAL.length
                	}

                }
			}
		});

		var allCol = [];
		var a = [];

		var nameCol = function(object){
			return $('<h4>'+object.NAME+'  '+'</h4>').append($('<i class="fa fa-question-circle report-popover" aria-hidden="true"></i>').attr('data-toggle', 'popover').attr('data-content', object.TEXT))[0].outerHTML;
		}
		allCol.push({"mData": nameCol, "sName": 'select', "sTitle": '', "bSortable": false, "sClass": "select", "sWidth": "150px"});

		for (var i = 0; i < numberOfCol; i++) {
			a[i] = function(object){
				if (typeof object.ALLVAL[ansIndex] !== "undefined") {
					var html = '<h4 class="report-popover" data-toggle="popover" data-content="'+object.ALLVAL[ansIndex].TEXT+'">'+object.ALLVAL[ansIndex].TOTALCHOOSE+'</h4>'
					ansIndex++;
				}
				else{
					var html = ''
					ansIndex++;
				}
				if (ansIndex == numberOfCol) {
					ansIndex = 0;
				}
				return html;
			}
			allCol.push({"mData": a[i], "sName": 'select', "sTitle": answerChoice[i], "bSortable": false, "sClass": "select", "sWidth": "120px"});
		}

		var totalNameCol = function(object){
			return $('<h4>'+object.ALLCHOOSE+'  '+'</h4>')[0].outerHTML;
		}

		allCol.push({"mData": totalNameCol, "sName": 'select', "sTitle": 'Total', "bSortable": false, "sClass": "select", "sWidth": "100px"});

		var table = $('#tblMultiplechoiceSurveyList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": allCol,
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetMultipleChoiceBySurveyBatch'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpBatchId", "value": BatchId}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0 || data.DATALIST.length <= 20) {
			        		$('#tblMultiplechoiceSurveyList_paginate').hide();
			        	}
			        	fnCallback(data);
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				// for (var i = 0; i < questionListText.length; i++) {
				// 	$('#question-text-'+i).attr('data-content',questionListText[i]);
				// }
               	$('[data-toggle="popover"]').popover({
			    	placement : 'top',
			    	trigger : 'hover'
			    });
			}
		});


	}

	$('body').on("click", "button.export-question-report", function(event){
        window.location.href="/session/sire/models/cfm/admin/admin-export-question-customer-survey.cfm" + "?campaignName=" + campaignName + "&question=" + $(this).data("id") + "&batchId=" + $('#inpBatchId').val();
    });

	$(document).ready(function() {

		if(templateId == 11){
			var inpBatchId1 = $('#inpBatchId').val();
			// $('.export-data-div, .options, .wrapper-tblcCampaignReportData, .sub-list-section').hide();
			function InitNewSurveyReport(BatchId){
				var BatchId = typeof(BatchId)!='undefined'?BatchId:0;

				var percentageCol = function(object){
					if(object.PERCENT.toString() != ''){
						return object.PERCENT.toString() + '%';
					}
					return object.PERCENT;
				}

				var responseCol = function(object){
					if(object.PERCENT.toString() != ''){
						return $('<a class="response-answer-details" data-regex="'+object.REGEX+'" data-text="'+object.TEXT+'" data-cpid="'+object.CPID+'" data-qid="'+object.QID+'" data-format="'+object.FORMAT+'">'+object.TOTALCHOOSE+'</a>')[0].outerHTML;
					}
					return object.TOTALCHOOSE;

				}
				$.ajax({
					url: '/session/sire/models/cfc/reports.cfc?method=GetDataForSurveyReport'+strAjaxQuery,
					type: 'POST',
					dataType: 'JSON',
					data: {inpBatchId: BatchId},
					success: function(data){
						console.log(data)

						for (var i = 0; i < data["DATALIST"].length; i++) {
							if(data["DATALIST"][i].TYPE=="ONESELECTION"){

								$('#tblNewSurvey'+i).addClass('campaign-survey-report')
								$('#tblNewSurvey'+i).dataTable({
									"aaData": data["DATALIST"][i].ALLVAL,
							        "bStateSave": false,
									"iStateDuration": -1,
									"bProcessing": true,
									"bFilter": false,
									"bServerSide":false,
									"bDestroy":true,
									"sPaginationType": "input",
								    "bLengthChange": false,
									"iDisplayLength":20,
									"bAutoWidth": false,
									"aaSorting": [],
								    "aoColumns": [
								        {"mData":"TEXT", "sTitle": 'Answer Choices', "bSortable": false, "sWidth": "170px" },
								        {"mData":responseCol, "sTitle": 'Responses', "bSortable": false, "sWidth": "140px" },
								        {"mData":percentageCol, "sTitle": 'Percentage', "bSortable": false, "sWidth": "140px" }
								    ]
								});

								var chartLabels = [];
								var chartData = [];
								var tempArray = data["DATALIST"][i].ALLVAL;
								var tempLabel;
								for (var j = 0; j < tempArray.length-1; j++) {
									if (tempArray[j].TEXT.toString().length > 5){
			                            tempLabel = tempArray[j].TEXT.toString().substring(0, 5)+'...';
			                        }
			                        else{
			                            tempLabel = tempArray[j].TEXT.toString();
			                        }

									chartLabels.push(tempArray[j].FORMAT.toString()+'-'+tempLabel);
									// chartLabels.push(tempArray[j].FORMAT.toString());
									chartData.push(tempArray[j].PERCENT);
								}

								// handle graph chart
						    	var horizontalBar1Data = {
						    		labels: chartLabels,
						    		datasets: [
						    			{
						    				label: "",
						    				backgroundColor: "#578ca5",
								    		borderColor: "#578ca5",
								    		borderWidth: 1,
								    		data: chartData
						    			}
						    		]
						    	}

								var options = {
									responsive: true,
									legend: {
										display: false
									},
								    scales: {
								        xAxes: [{
								            gridLines: {
								                display: true
								            },
								            ticks: {
							                	min: 0,
							                	max: 100,
							                	callback: function(value){return value+ "%"}
							                }
								        }],
								        yAxes: [{
								            gridLines: {
								                display: false
								            },
								            categoryPercentage: 1,
						            		barPercentage: 0.4
								        }]
								    },
								}

						    	var ctx = document.getElementById("graph-canvas-"+i).getContext("2d");
						    	window.horizontalBar1 = new Chart(ctx, {
						    		type: 'horizontalBar',
						    		data: horizontalBar1Data,
						    		options: options
						    	});
						    	// end graph chart
							}
							else if(data["DATALIST"][i].TYPE=="SHORTANSWER"){
								$('#tblNewSurvey'+i).dataTable({
									"aaData": data["DATALIST"][i]["DATALIST"],
							        "bStateSave": false,
									"iStateDuration": -1,
									"bProcessing": true,
									"bFilter": false,
									"bServerSide":false,
									"bDestroy":true,
									"sPaginationType": "input",
								    "bLengthChange": false,
									"iDisplayLength":20,
									"bAutoWidth": false,
									"aaSorting": [],
								    "aoColumns": [
								        {"mData":"ORDER","sTitle": '#', "bSortable": false, "sWidth": "100px" },
								        {"mData":"RESPONSE","sTitle": 'Responses', "bSortable": false, "sWidth": "210px"},
								        {"mData":"CREATED","sTitle": 'Date', "bSortable": false, "sWidth": "100px" }
								    ],
								    "fnDrawCallback": function( oSettings ) {
								  		if (oSettings._iDisplayStart <= 0){
											oSettings._iDisplayStart = 0;
											$('input.paginate_text').val("1");
										}
								    },
								    "fnInitComplete":function(aaData){
								    	if(data["DATALIST"][i]["DATALIST"].length == 0){
								    		$('#tblNewSurvey'+i+'_paginate').hide();
								    	}
									}
								});

							}

						}
					}
				});
			}

			function InitCustomerResponseDetail(BatchId, order){
				var BatchId = typeof(BatchId)!='undefined'?BatchId:0;
				$.ajax({
					url: '/session/sire/models/cfc/reports.cfc?method=GetReportDetailsForNewSurveyBatch'+strAjaxQuery,
					type: 'POST',
					dataType: 'JSON',
					data: {
						inpBatchId: BatchId,
						inpOrder: order
					},
					success: function(data){

						$('.stt-head-individual').show();
						if(data["DATALIST"].length > 0)
						{
							$('#customer-respondent-status').text(data["DATALIST"][0].STATUS);
							if(data["DATALIST"][0].STATUS == "INCOMPLETE"){
								$('#customer-respondent-status').addClass('red-status-incomplete');
							}
							else if(data["DATALIST"][0].STATUS == "COMPLETE" && $('#customer-respondent-status').hasClass('red-status-incomplete')){
								$('#customer-respondent-status').removeClass('red-status-incomplete');
							}
							$('#customer-respondent-date').text(data["DATALIST"][0].CREATED);
							$('#customer-respondent-phone').text(data["DATALIST"][0].PHONE.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3'));
							$('#respondent-answer-list').html('');
							var tempArray1 = data["DATALIST"][0].ANSWERLIST;
							for (var i = 0; i < tempArray1.length; i++) {
								$('#respondent-answer-list').append('<div class="anws-item"><h4 class="title">'+ tempArray1[i].QUESTION +'</h4><ul class="uk-list"><li>'+ tempArray1[i].ANSWER+'</li></ul></div>');
							}
						}
						else{
							$('#respondent-answer-list').html('');
							$('#customer-respondent-status').text(data.STATUS);
							$('#customer-respondent-date').text(data.CREATED);
							$('#customer-respondent-phone').text(data.PHONE);
						}
					}

				});

			}

			if($('#respondent-select').val() != null){
				InitCustomerResponseDetail(inpBatchId1,$('#respondent-select').val());
			}

			$('#respondent-select').on('change', function(){
				var customerOrder = $(this).val();
				InitCustomerResponseDetail(inpBatchId1,customerOrder);
			});

			InitNewSurveyReport(inpBatchId1);

			var minCustomerOrder = $('#minCustomerOrder').val();
			var maxCustomerOrder = $('#maxCustomerOrder').val();

			$('#previous-customer').on('click', function(){
				if($('#respondent-select').val() > 0){
					var previousOrder = parseInt($('#respondent-select').val()) - 1;
					if (previousOrder < minCustomerOrder){
						previousOrder = minCustomerOrder;
					}

					$('#respondent-select').val(previousOrder); // Change the value or make some change to the internal state
					$('#respondent-select').trigger('change.select2'); // Notify only Select2 of changes
					InitCustomerResponseDetail(inpBatchId1,previousOrder);
				}

			});

			$('#next-customer').on('click',function(){
				if($('#respondent-select').val() > 0){
					var nextOrder = parseInt($('#respondent-select').val()) + 1;
					if (nextOrder > maxCustomerOrder){
						nextOrder = maxCustomerOrder;
					}

					$('#respondent-select').val(nextOrder); // Change the value or make some change to the internal state
					$('#respondent-select').trigger('change.select2'); // Notify only Select2 of changes
					InitCustomerResponseDetail(inpBatchId1,nextOrder);
				}
			});
		}

		$(".Select2").select2(
        	{ theme: "bootstrap", width: 'auto'}
        );

		$('.expand-btn').on('click', function(event){
			event.preventDefault();
			DisplayCampaignReportData(0);
	    });

		if(templateId != 11){
			DisplayCampaignReportData(1);
		}

	    $('[data-toggle="popover"]').popover({
            placement : 'top',
            trigger : 'hover'
		});
		$(document).on('click','#make-a-chat',function(){
			var phone= $(this).closest('li').find('#customer-respondent-phone').text();
			var alertCheckExitChatSession=CheckExistChatSession(phone);
			if(alertCheckExitChatSession !=="")
			{
				alertBox(alertCheckExitChatSession,'Oops!','');
				return false;
			}

			bootbox.dialog({
				message: '<h4 class="be-modal-title">Confirmation</h4><p>You are about to launch a chat session with this subscriber, please confirm!</p>',
				title: '&nbsp;',
				className: "be-modal",
				buttons: {
					success: {
						label: "Continue",
						className: "btn btn-medium green-gd",
						callback: function(result) {
							//make new chat
							$.ajax({
								url: '/session/sire/models/cfc/smschat.cfc?method=CheckExistChatCampaign' + strAjaxQuery,
								type: 'POST',
								data: {

								},
								beforeSend: function () {
									$("#processingPayment").show();
								}
							})
							.done(function(data) {
								if (parseInt(data.RXRESULTCODE) == 1) {
									$("#processingPayment").hide();
									StartChatSession(phone,data.KEYWORD);
								} else {
									alertBox('There is no chat campaign available, please create one','Oops !','');
								}
							})
							.fail(function(e, msg) {
								console.log(msg);
							})
							.always(function() {
								$("#processingPayment").hide();
							});
						}
					},
					cancel: {
						label: "Cancel",
						className: "green-cancel"
					},
				}
			});


		});
		function CheckExistChatSession(phone)
		{
			var strReturn='';
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=CheckExistChatSession' + strAjaxQuery,
				type: 'POST',
				async: false,
				data: {
					inpContactString: phone
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					if(data.AVAILABLESESSION ==1)
					{
						strReturn=data.ALERTRETURN;
					}
				} else {
					alertBox(data.MESSAGE, 'Oops!');
				}
			})
			.fail(function(e, msg) {
				console.log(msg);
			})
			.always(function() {
				$("#processingPayment").hide();
			});
			return strReturn;
		}
		function StartChatSession(phone,keyword){
			//make new chat
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=MakeNewChatSessionForContactString' + strAjaxQuery,
				type: 'POST',
				data: {
					inpKeyword: keyword,
					inpContactString: phone,
					inpSkipCheckOPT:1
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				data = JSON.parse(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					location.href="/session/sire/pages/sms-response?keyword="+keyword+"&ssid="+data.NEWSESSIONID;
				} else {
					alertBox(data.MESSAGE, 'Oops!');
				}
			})
			.fail(function(e, msg) {
				console.log(msg);
			})
			.always(function() {
				$("#processingPayment").hide();
			});
			//
		}

	});

	// Get blast list by campaign id
	function GetBlastQueuedListByCampaignId(groupid,strFilter){
        $("#message-blast-queued-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}

        var table = $('#message-blast-queued-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                // {"mData": "ID", "sName": '', "sTitle": 'Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetQueuedBlastListByCampaignId'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchId", "value": $("#inpBatchId").val()}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                aoData.push(
                    { "name": "inpGroupId", "value": groupid}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
			        		$('#message-blast-queued-table_paginate').hide();
			        	}
			        	fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }

    function InitFilterBlastQueueTable (groupid) {
        $("#blastMessageQueuedFilterBox").html('');
        //Filter bar initialize
        $('#blastMessageQueuedFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Scheduled_dt '},
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                GetBlastQueuedListByCampaignId(groupid);
            }
        }, function(filterData){
            GetBlastQueuedListByCampaignId(groupid,filterData);
        });
    }

    $('body').on('click', '.get-queued-message-list', function(){
    	var GroupId = $(this).data('groupid');
        InitFilterBlastQueueTable(GroupId);
        GetBlastQueuedListByCampaignId(GroupId);
        $("#messages-queued-modal").modal("show");
        $("#blastMessageQueuedFilterBox .btn-add-item").hide();
    });

    // Get blast list by campaign id
	function GetBlastWaitByCampaignId(strFilter){
        $("#message-blast-wait-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}
        var table = $('#message-blast-wait-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                // {"mData": "ID", "sName": '', "sTitle": 'Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetBlastWaitListByCampaignId'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchId", "value": $("#inpBatchId").val()}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }

    function InitFilterBlastWaitTable () {
        $("#blastMessageWaitFilterBox").html('');
        //Filter bar initialize
        $('#blastMessageWaitFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                GetBlastWaitByCampaignId();
            }
        }, function(filterData){
            GetBlastWaitByCampaignId(filterData);
        });
    }

    $('body').on('click', '.get-wait-message-list', function(){
        InitFilterBuyCreditDetailsTable();
        InitFilterBlastWaitTable();
        $("#messages-wait-modal").modal("show");
        $("#blastMessageWaitFilterBox .btn-add-item").hide();
    });

    function GetOneSelectionResponseDetailList(cpid,qid,format,regex,strFilter){
        $("#response-answer-detail").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}

        var table = $('#response-answer-detail').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                // {"mData": "ID", "sName": '', "sTitle": 'Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports.cfc?method=GetOneSelectionResponseDetailList'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchId", "value": $("#inpBatchId").val()}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                aoData.push(
                    { "name": "inpCPID", "value": cpid}
                );
                aoData.push(
                    { "name": "inpQID", "value": qid}
                );
                aoData.push(
                    { "name": "inpFormat", "value": format}
                );
                aoData.push(
                    { "name": "inpRegex", "value": regex}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
			        		$('#response-answer-detail_paginate').hide();
			        	}
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }

    function InitFilterResponseDetailTable (cpid,qid,format,regex) {
        $("#responseAnswerFilterBox").html('');
        //Filter bar initialize
        $('#responseAnswerFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                GetOneSelectionResponseDetailList(cpid,qid,format,regex);
            }
        }, function(filterData){
            GetOneSelectionResponseDetailList(cpid,qid,format,regex,filterData);
        });
    }

    $('body').on('click', '.response-answer-details', function(){
    	var cpid = $(this).data('cpid');
    	var qid = $(this).data('qid');
    	var format = $(this).data('format');
    	var text = $(this).data('text');
    	var regex = $(this).data('regex');
    	$('#answer-cpid').val(cpid);
    	$('#answer-qid').val(qid);
    	$('#answer-format').val(format);
    	$('#answer-text').val(text);
    	$('#answer-regex').val(regex);

    	GetOneSelectionResponseDetailList(cpid,qid,format,regex);
    	InitFilterResponseDetailTable(cpid,qid,format,regex);
    	$("#response-answer-detail-modal").modal("show");
    	$("#responseAnswerFilterBox .btn-add-item").hide();
    });

    $('body').on("click", "button.total-response-answer-detail", function(event){
        window.location.href="/session/sire/models/cfm/admin/admin-export-response-answer-detail.cfm" + "?cpid=" + $('#answer-cpid').val() + "&qid=" + $('#answer-qid').val() + "&batchId=" + $('#inpBatchId').val() + "&format=" +$('#answer-format').val() + "&campaignname=" + campaignName + "&question=" + "Question " + $('#answer-cpid').val() + "&text=" +$('#answer-text').val() + "&filter=" + filter +"&regex=" + $('#answer-regex').val();
    });

    $('body').on('click', '.btn-edit-cp-new', function(event){

		var groupId = $(this).data('id');

		$('#contact-group-id').val(groupId);
		$('#rename-subscriber-list').modal('show');
		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {inpGroupId: groupId},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	success: function(data){
	    		$('#new-subscriber-list-name').val(data.SUBSCRIBERLISTNAME);
	    		LoadSelectSubscriber();
		    },
		    complete: function(){
	    		$('#processingPayment').hide();

		    }
		});

	});

	$('body').on("click", ".download-sub-new", function(event){
		var subID = $(this).data('id');

		window.location.href="/session/sire/models/cfm/export-subcriber-list.cfm?subid="+subID;
	});

	$('body').on('click', '.del-sub-new', function(event){
		var scbID = $(this).data('id');

		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetCampaignByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'json',
			data: {inpGroupId: scbID},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	error: function(XMLHttpRequest, textStatus, errorThrown) {
	     		$('#processingPayment').hide();
	     		alertBox('Send Get subscriber list detail request failed!', 'DELETE SUBCRIBER LIST', function(){});

	    	},
	    	success: function(data){
	    		$('#processingPayment').hide();
	    		if(data.RXRESULTCODE==1){
	    			var allCampaign = '';
	    			for (var i = 0; i < data['batchDetail'].length; i++) {
	    				allCampaign = allCampaign + '<li><a href="/session/sire/pages/campaign-edit?campaignid=' + data['batchDetail'][i].ID + '" target="_blank">' + data['batchDetail'][i].NAME + '</a></li>';
	    			}

	    			confirmBox("Are you sure you want to delete this subcriber list? It is being used by the following campaign(s): " + "<ol>" + allCampaign + "</ol>", 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Send Delete subscriber list request failed!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){
					    			location.reload();
					    		});
					    	}
			    		});
	    			});
	    		}
	    		else if(data.RXRESULTCODE == 0){

	    			confirmBox('Are you sure you want to delete this subcriber list? ', 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Delete subscriber list request failed to send!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){
					    			location.reload();
					    		});
					    	}
			    		});
	    			});
	    		}
	    		else{

		     		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});
	    		}
	    	}

		});
	});
	function GetOptOutListByBatch(batchId,strFilter){
        $("#tblOptOutList").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        var formatContactString = function(object){
			return '<span class="format-phone-number">'+object.PHONE+'</span>';
		}

        var table = $('#tblOptOutList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": formatContactString, "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "DATE", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/campaign.cfc?method=GetOptOutListByBatch'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpBatchIdList", "value": batchId}
                );


                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
							$('.dowload-optout-list').hide();
						}
						else
						{
							$('.dowload-optout-list').show();
						}
						fnCallback(data);
						$("#mdOptOutList").modal("show");
                    }
                });
            },
            "fnInitComplete":function(oSettings){
				$('.format-phone-number').mask('(000) 000-0000');
			}
        });
    }
	$(document).on("click","#totalOptout",function(){
		GetOptOutListByBatch($("#inpBatchId").val());
	})
	$('body').on("click", "button.dowload-optout-list", function(event){
		window.location.href="/session/sire/pages/reports-exportcsv?inprel1=campaign.GetOptOutListByBatch&prefixFileName=OptOutBatch&inprel2=CSV&inpBatchIdList="+$("#inpBatchId").val();
	});
})(jQuery);
