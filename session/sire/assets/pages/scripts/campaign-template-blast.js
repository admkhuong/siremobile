
// Begin Slide campaign template choice
	 $(window).bind('load resize', function () {
		var $box = $('.campaign-template.active').find('.campaign-template-content');
		var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
		calCulateHeightBox($box, $hBox);

		var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		if (width < 640) {
			$('.last-on-mobile').addClass('uk-flex-last');
			$('.first-on-mobile').addClass('uk-flex-first');
		} else {
			$('.last-on-mobile').removeClass('uk-flex-last');
			$('.first-on-mobile').removeClass('uk-flex-first');
		}
	});


	function calCulateHeightBox ($box, $hBox) {
		$box.height($hBox);
	}

	$("html, body").on('click', 'a[data-slide="prev"]', function(event) {
		event.stopPropagation();
		$(this).parents('.carousel').carousel('prev');
	});

	$("html, body").on('click', 'a[data-slide="next"]', function(event) {
		event.stopPropagation();
		$(this).parents('.carousel').carousel('next');
	});
// End Slide campaign template choice


