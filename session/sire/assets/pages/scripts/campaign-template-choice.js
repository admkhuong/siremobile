var CampaignComponents = function () {

    return {
        handleModalIntroduce: function () {
            $(".clickModal").bind('click', function(event) {
                /* Act on the event */
                event.preventDefault();
                UIkit.modal("#modal-introduce")[0].toggle();
            });
        },

        initHandleSelectTemplate: function () {
            // Day la code cua a

            $('.inner-retangle').bind('click', function (e) {
               
                e.preventDefault();
                $('.campaign-template').removeClass('active').find('.campaign-template-content').hide();
                $(this).addClass('active').find('.campaign-template-content').show();
                
                var heightBoxInner = $(this).find('.inner-content').outerHeight();
                $(this).find('.campaign-template-content').height(heightBoxInner);                

                
            });

            $(window).bind('load resize', function () {
                var $box = $('.campaign-template.active').find('.campaign-template-content');
                var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
                calCulateHeightBox($box, $hBox);

                var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
                if (width < 640) {
                    $('.last-on-mobile').addClass('uk-flex-last');
                    $('.first-on-mobile').addClass('uk-flex-first');
                } else {
                    $('.last-on-mobile').removeClass('uk-flex-last');
                    $('.first-on-mobile').removeClass('uk-flex-first');
                }
            });

            function calCulateHeightBox ($box, $hBox) {
                $box.height($hBox);
            }

            $("html, body").on('click', 'a[data-slide="prev"]', function(event) {
                event.stopPropagation();
                $(this).parents('.carousel').carousel('prev');
            });

            $("html, body").on('click', 'a[data-slide="next"]', function(event) {
                event.stopPropagation();
                $(this).parents('.carousel').carousel('next');
            });
        },

        // handleBootstrapSelect: function() {
        //     $('.bs-select').selectpicker({
        //         iconBase: 'fa',
        //         tickIcon: 'fa-check'
        //     });
        // },

        handleAddMoreAnswerText: function () {
            var $fieldStr = '';            

            $fieldStr += '<div class="row row-small">';
                $fieldStr += '<div class="col-xs-8 col-sm-10">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" class="form-control text-center" value="" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<button class="form-control delete-answer">';
                            $fieldStr += '<i class="fa fa-minus"></i>';
                        $fieldStr += '</button>';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
            $fieldStr += '</div>';

            $('.wrapper-answer-text').on('click', '.add-more-answer', function(event) {
                event.preventDefault();
                $(this).parents('.wrapper-answer-text').find('.field-anser-holder').append($fieldStr);
            });

            $('.field-anser-holder').on('click', '.delete-answer', function(event) {
                event.preventDefault();
                $(this).parents('.row.row-small').remove();
            });
        },

        handleAddAnotherMessage: function () {
            var $rendered = new EJS({url:'assets/pages/scripts/another-message.ejs'}).render();
            
            $('.wrap-box-another-mesage').on('click', '.another-prepend', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').prepend($rendered);
            });

            $('.wrap-box-another-mesage').on('click', '.another-append', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').append($rendered);
            });
        },
        
        init: function () {
            //this.initDialStep();
            this.handleModalIntroduce();
            this.initHandleSelectTemplate();            
            this.handleAddMoreAnswerText();
            //this.handleAddAnotherMessage();
            //this.handleBootstrapSelect();
        }

    };


   

}();

var doughnutStep = function () {
        return {
            doughnutStepHandle: function (element,totalStep) {
                totalStep = typeof totalStep == "undefined" ? 1 : totalStep;
                Chart.defaults.global.tooltips.enabled = false;
                Chart.defaults.global.hover.mode = 'index';

                var config = {
                    type: 'doughnut',
                    data: {
                        datasets: [
                            {
                                data: [100],
                                backgroundColor: [
                                    '#5c5c5c'
                                ],
                                hoverBackgroundColor: [
                                    '#5c5c5c'
                                ]
                            }
                        ]
                    },
                    options: {
                        responsive: false
                    }
                }

                // for (var i = 1; i <= totalStep; i++) {
                //     config.data.datasets[0].data.push(100);
                //     config.data.datasets[0].backgroundColor.push('#5c5c5c');
                //     config.data.datasets[0].hoverBackgroundColor.push('#5c5c5c');
                // }

                var ctx = document.getElementById(element).getContext("2d");
                window.myDoughnut = new Chart(ctx, config);

                function nextStep (step, totalStep) {
                    if (config.data.datasets.length > 0) {
                        if (step < totalStep) {
                            config.data.datasets.forEach(function(dataset) {
                                dataset.backgroundColor[step] = '#74c37f';
                                dataset.hoverBackgroundColor[step] = '#74c37f';
                            });
                        }
                        window.myDoughnut.update();                 
                    };
                }

                function finalStep (totalStep) {
                    for (var i = 0; i < totalStep; i++) {
                        config.data.datasets.forEach(function(dataset) {
                            dataset.backgroundColor[i] = '#74c37f';
                            dataset.hoverBackgroundColor[i] = '#74c37f';
                        });
                    };
                    window.myDoughnut.update();
                }

            },

            doughnutStepUpdate: function (currentStep) {
                var config = window.myDoughnut.config;
                for (var i = 0; i < currentStep; i++) {
                    config.data.datasets[0].backgroundColor[i] = '#74c37f';
                    config.data.datasets[0].hoverBackgroundColor[i] = '#74c37f';
                }

                window.myDoughnut.update();
            },

            init: function (element,totalStep) {
                this.doughnutStepHandle(element,totalStep);
            },

            update: function (currentStep) {
                this.doughnutStepUpdate(currentStep)
            }
        }
    }();

jQuery(document).ready(function() {    

    //doughnutStep.init("myStep", 0);

    CampaignComponents.init(); 

    //if(inpTemplateId > 0)
    //$("html, body").animate({ scrollTop: $(document).height() }, "fast");

    if (typeof OPTIN_GROUPID != 'undefined') {
        $("#SubscriberList").val(OPTIN_GROUPID);
    }

    if(action == 'CreateNew'){
        $(".cpedit:not(:first)").addClass('hidden');
        $('*[data-template-id="'+inpTemplateId+'"]').trigger('click');
        $(".start-edit").addClass('hidden');
    }

    // $('[data-uk-switcher]').on('show.uk.switcher', function(event, area){
    //     console.log("Switcher switched to ", area);
    // });

    var selectedTemplateId = 1;
    var selectedTemplateType = 0;
    var selectedTemplateName = ''

    $('.inner-retangle').click(function(event){
        /*
        ** Start Fix animated auto here
        */      
          
        var isAnimated = (selectedTemplateId != $(this).data('template-id'));

        if(inpBatchId == '0')
        {
            selectedTemplateId = $(this).data('template-id');
            selectedTemplateType = $(this).data('template-type');
            selectedTemplateName = $(this).find('.template-name').html();
            selectedTemplateName = selectedTemplateName.replace("<br>",' ');
            $('.dsp-template-name').html(selectedTemplateName);

        }

        if(!$('.showSelectType').hasClass('hidden'))
        $('.showSelectType').addClass('hidden');

        /*
        ** End Fix animated auto here
        */
        if(isAnimated) {
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        }

// auto show template
        if(selectedTemplateType == 0){
                                
                if (selectedTemplateId == 3){
                    //location.href='/session/sire/pages/campaign-template-blast?templateId='+selectedTemplateId+'&selectedTemplateType=1';
                    location.href='/session/sire/pages/campaign-template-new?templateId='+selectedTemplateId+'&selectedTemplateType=1';
                }
                else if(selectedTemplateId==8 ){
                    location.href='/session/sire/pages/campaign-template-new?templateId='+selectedTemplateId+'&selectedTemplateType=9';
                } 
                else if( selectedTemplateId==11){
                    location.href='/session/sire/pages/campaign-template-new?templateId='+selectedTemplateId+'&selectedTemplateType=9';
                } 
                else{                    
                    location.href='/session/sire/pages/campaign-template-new?templateId='+selectedTemplateId+'&selectedTemplateType='+selectedTemplateType;
                }

            }

            else{
                if($('.showSelectType').hasClass('hidden'))
                $('.showSelectType').removeClass('hidden');
            }

            $("html, body").animate({ scrollTop: $(document).height() }, 1000);

    });
    
    $('body').on('click', '.campaign-template .corner', function(event) {
        var _this= this;
        var modalBox=$("#mdLearmore-"+$(this).data("template-id")) ;
        modalBox.modal("show");
        $(this).css({"width": "70px", "height": "70px"});
        $(this).find(".retangle__learnmore").css({"opacity":"100"});
        //
        $(modalBox).on('hidden.bs.modal', function () {
            $(_this).removeAttr("style");
            $(_this).find(".retangle__learnmore").removeAttr("style");
        })
    });


    $('.subscriber-item').click(function(event){
        event.preventDefault();
        if(action == 'SelectTemplate'){
            
            var selectedTemplateType = $(this).data('type');

            
            var redirect_url = '/session/sire/pages/campaign-template-new?templateId='+selectedTemplateId+'&selectedTemplateType='+selectedTemplateType;   

            location.href = redirect_url;
        }
    });

    if(inpTemplateId != 1)
        $(".campaign-template[data-template-id='"+inpTemplateId+"']").trigger('click');

    
});