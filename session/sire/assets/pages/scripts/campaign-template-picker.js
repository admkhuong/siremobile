var CampaignTemplatePicker = function () {
	return {
		handleQuickTemplate: function () {
			$('.dismiss-quick-template').bind('click', function(e) {
				e.preventDefault();
				$('body').toggleClass('open-quick-request-template');
			});
		},

		init: function () {
			this.handleQuickTemplate();
		}
	}
}();

jQuery(document).ready(function() {    
   CampaignTemplatePicker.init(); 
});