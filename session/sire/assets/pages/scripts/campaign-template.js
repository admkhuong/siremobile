$(window).load(function() {

   $('.processingPaymentCampaign').delay(100).fadeOut(function(){
		$('.content-for-first-campaign').css('display','block');
		$('.page-footer').css('display','block');
		$('.box-widget-sidebar').css('display','block');

		 if(action == "CreateNew")
      	{
        	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        		$("html, body").animate({ scrollTop: $("#frm-campaign").offset().top - $(".page-header").height()}, "fast");
			}
			// else
			// 	$("html, body").animate({ scrollTop: $(document).height() }, "fast");
 
      	}

    });

});


var needRuleStr = 0;
var doBlastRealTime="0"; // this is for send now buttion purpose , do blast at current time 
var globalSelectedTemplateType=9; // 9 is for havent yet selected
var globalAlrDoSelectGreenOrBlue=0;

function hasUnicodeChar(str) {
	var UnicodeRegex = new RegExp('[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]');
	if(UnicodeRegex.test(str.replace(/\u00a0/g, " "))){
		return true;
	}
	return false;
}

$(document).ready(function() {

	$('body').on('click', 'i.emoji-picker-icon', function(e){
		
		var cpTextWithoutCDF = $(this).closest('.form-group').find('div.emoji-wysiwyg-editor').text();
		var cpTextWithoutCDF1 = $(this).closest('.form-group').find('textarea').val();
		cpTextWithoutCDF = cpTextWithoutCDF.replace(/\{\%(.\w*)\%\}/g, "");
		cpTextWithoutCDF1 = cpTextWithoutCDF1.replace(/\{\%(.\w*)\%\}/g, "");
		
		if(hasUnicodeChar(cpTextWithoutCDF) || hasUnicodeChar(cpTextWithoutCDF1)){
			if($(this).closest('.form-group').find('.see-unicode-mess').hasClass('hidden')){
				$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
			}
			else{
				$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
			}
			
		}
		else {
			$(this).closest('.form-group').find('.see-unicode-mess').addClass('hidden');
			
		}
	});

	$("body").on('click', function(e){
		$('div.emoji-wysiwyg-editor').each(function(){

			var cpTextWithoutCDF = $(this).text();
			cpTextWithoutCDF = cpTextWithoutCDF.replace(/\{\%(.\w*)\%\}/g, "");

	        if($(this).find('img').length > 0 || hasUnicodeChar(cpTextWithoutCDF)){
				if($(this).closest('.form-group').find('.see-unicode-mess').hasClass('hidden')){
					$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
					
				}
				else{
					$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');;
					
				}
				
			}
			else {
				$(this).closest('.form-group').find('.see-unicode-mess').addClass('hidden');
				
			}
		});
	});


	$("body").on('input change click','div.emoji-wysiwyg-editor',function(e){		
		e.preventDefault();
		$(this).validationEngine('hide');
		
		$(this).attr('maxlength', 459-$(this).find('img').length);

		var cpTextWithoutCDF = $(this).text();
		cpTextWithoutCDF = cpTextWithoutCDF.replace(/\{\%(.\w*)\%\}/g, "");

        if($(this).find('img').length > 0 || hasUnicodeChar(cpTextWithoutCDF)){
			if($(this).closest('.form-group').find('.see-unicode-mess').hasClass('hidden')){
				$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
				
			}
			else{
				$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
				
			}
			
		}
		else {
			$(this).closest('.form-group').find('.see-unicode-mess').addClass('hidden');
			
		}

		var counterId = $(this).parent('.form-group').find(".control-point-char").attr('id');
		$(this).parent('.form-group').find('textarea').val('');
		$(this).parent('.form-group').find('textarea').val($(this).text());

		var tmpString = $(this).text();
		

		//each emoji image equals to 2 characters
		for(var i=0;i<$(this).find('img').length*2;i++){
			tmpString = tmpString + 'a';
		}

		countText("#"+counterId, tmpString);		

    });

	$('.cpedit').first().focus();

	function ShowNeedRuleMessageAlert()	{
		$('.has-error-message').removeClass('hidden');
	    $('.need-rule-tdesc').addClass('need-rule-help-block');
	    $('.need-rule-message').addClass('need-rule-message-textarea');
	    
	    $('.need-rule-message').focus();
	}

	function HideNeedRuleMessageAlert() {
		$('.has-error-message').addClass('hidden');

		if($('.need-rule-tdesc').hasClass('need-rule-help-block')){
	    	$('.need-rule-tdesc').removeClass('need-rule-help-block');
		}

		if($('.need-rule-message').hasClass('need-rule-message-textarea')){
			$('.need-rule-message').removeClass('need-rule-message-textarea');
		}
	    
	}

	/* doughnutStep progress bar */
	var doughnutStep = function () {
		return {
			doughnutStepHandle: function (element,totalStep) {
				totalStep = typeof totalStep == "undefined" ? 1 : totalStep;
				Chart.defaults.global.tooltips.enabled = false;
				Chart.defaults.global.hover.mode = 'index';

				var config = {
					type: 'doughnut',
					data: {
						datasets: [
							{
								data: [100],
								backgroundColor: [
									'#74c37f'
								],
								hoverBackgroundColor: [
									'#74c37f'
								]
							}
						]
					},
					options: {
						responsive: false
					}
				}

				for (var i = 1; i <= totalStep; i++) {
					config.data.datasets[0].data.push(100);
					config.data.datasets[0].backgroundColor.push('#5c5c5c');
					config.data.datasets[0].hoverBackgroundColor.push('#5c5c5c');
				}

				var ctx = document.getElementById(element).getContext("2d");
				window.myDoughnut = new Chart(ctx, config);

				function nextStep (step, totalStep) {
					if (config.data.datasets.length > 0) {
						if (step < totalStep) {
							config.data.datasets.forEach(function(dataset) {
								dataset.backgroundColor[step] = '#74c37f';
								dataset.hoverBackgroundColor[step] = '#74c37f';
					        });
						}
				        window.myDoughnut.update();			        
					};
				}

				function finalStep (totalStep) {
					for (var i = 0; i < totalStep; i++) {
						config.data.datasets.forEach(function(dataset) {
							dataset.backgroundColor[i] = '#74c37f';
							dataset.hoverBackgroundColor[i] = '#74c37f';
				        });
					};
					window.myDoughnut.update();
				}

			},

			doughnutStepUpdate: function (currentStep) {
				var config = window.myDoughnut.config;
				for (var i = 0; i < currentStep; i++) {
					config.data.datasets[0].backgroundColor[i] = '#74c37f';
					config.data.datasets[0].hoverBackgroundColor[i] = '#74c37f';
				}

				window.myDoughnut.update();
			},

			init: function (element,totalStep) {
				this.doughnutStepHandle(element,totalStep);
			},

			update: function (currentStep) {
				this.doughnutStepUpdate(currentStep)
			}
		}
	}();

	var blastType = 1; 
	//1 - blast now
	//2 - set schedule
	//3 - save and blast later

	var ShowComplateModal = function() {
		//$("#CompleteModal").modal('show');
		// doughnutStep.init("completeStep", $("#totalStep").val());
		// doughnutStep.update($("#totalStep").val()+1);
		//UIkit.modal("#CompleteModalBlast"+inpTemplateId+"-"+blastType)[0].toggle();
		
		if(inpTemplateId != 3 && inpTemplateId != 8  && inpTemplateId != 11)
		{
			blastType = inpCampaignType
		}		
		$("#CompleteModalBlast"+inpTemplateId+"-"+blastType).modal('show');
	}

	/* show doughnut step progress bar on create new only */
	if (action == "CreateNew") {
		/* active campaign left menu for action create */
		/*$(".campaign-template-choice-li").addClass('active');
		$(".campaign-li").find(".sub-menu").toggle();*/

		/* init progress doughnut bar */
		$("#totalStep").val($(".campaign-next").length-1);
		doughnutStep.init("myStep",$("#totalStep").val());

		/* prepare step value for next campaign button */
		var initStep = 1;
		$(".campaign-next").each(function(index, el) {
			$(this).data('step-value', initStep++);
		});
	}

	/*if(action =="Edit"){
		$(".campaign-manage-li").addClass('active');
		$(".campaign-li").find(".sub-menu").toggle();
	}*/

	/* on modal complete hide, redirect user to campaign template choice */
	$('body').on('hidden.bs.modal', '#CompleteModal', function(event) {
		
		event.preventDefault();	
		if(inpTemplateId==9)
		{			
			location.href = "/session/sire/pages/sms-response?keyword="+$("#Keyword").val().replace(/\s/g,'');
		}
		else
		{
			location.href = "/session/sire/pages/campaign-template-choice";
		}		
	});
	$('.carousel-popup').on('hidden.bs.modal', function (e) {
	  	
		if(inpTemplateId==9)
		{			
			location.href = "/session/sire/pages/sms-response?keyword="+$("#Keyword").val().replace(/\s/g,'');
		}
		else
		{			
			location.href="/session/sire/pages/campaign-manage";
		}		
	})

	

	// Initializes and creates emoji set from sprite sheet
    window.emojiPicker = new EmojiPicker({
      emojiable_selector: '[data-emojiable=true]',
      assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
      popupButtonClasses: 'fa fa-smile-o'
    });
    // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
    // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
    // It can be called as many times as necessary; previously converted input fields will not be converted again
    // window.emojiPicker.discover();
    
	// new EmojiPicker().discover();

	/* init page form validation */
	$("#frm-campaign").validationEngine({promptPosition : "topLeft", scroll: true,focusFirstField : false, scrollOffset: $(".page-header").height()});

	/* button finish trigger, call save CP */
	$('body').on('click', '.btn-finished', function(event) {
		if ($('#frm-campaign').validationEngine('validate')) {
			var cpName= $("#Desc_vch").val();
			//not alow campaign name has unicode : for stupid user try to force next
			
			var contentUnicode=  /[^\u0000-\u007f]/.test(cpName);
			if(contentUnicode)
			{
				alertBox('Please Enter Campaign Name without Unicode','Alert','');																						
				setTimeout(function(){ 
									
					$("#Desc_vch").focus();
				}, 3000);
				$('html,body').animate({
					scrollTop: $("#cppNameDisplay").offset().top-100},
				'slow');
				return false;
			}
			//end check cpname unicode  	
			$("#ConfirmLaunchModal").modal("hide");
			
			if(needRuleStr == 1){

    		    if(inpTemplateId == 8){
					//var recentFirstMess = $('.control-point .need-rule-message-textarea').val();	
					var theFirstMess = "";	
					$('.control-point').each(function()
					{							
						if($(this).data("control-rq-id")==1)
						{
							theFirstMess=$(this).find('textarea').val();
						}							
					})
					
        		}
        		else{
        			var theFirstMess = $('.control-point').first().find('textarea').val();	
				}

        		if(!ruleReg.test(theFirstMess) || !ruleReg1.test(theFirstMess) || !ruleReg2.test(theFirstMess) || !ruleReg3.test(theFirstMess)){
        			saveCampaign = 0;
				  //   alertBox('You have entered invalid message. By law, all opt-in messages must include the following disclaimer: "HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply." Please adjust the # of messages and leave the remaining text intact.', firstMessAlertTitle, function(){
				  //   	$('.control-point').first().find('textarea').val($('.control-point').first().find('textarea').val() + 'HELP for help or STOP to cancel. Up to # msg/mo. Msg&Data rates may apply.');
						
				  //   	setTimeout(function() {
						// 	$('.control-point').first().find('textarea').focus();
						// }, 0);
				  //   });

				  	ShowNeedRuleMessageAlert();

				    setTimeout(function() {
						$('.control-point').first().find('textarea').focus();
					}, 0);
            	}
            	else{
            		HideNeedRuleMessageAlert();
					saveCampaign = 1;
					if(inpTemplateId ==8 || inpTemplateId==11)
					{						
						CP_Save(0,0,0,0);	
					}
					else
					{
						CP_Save(0,0,0,1);
					}		        	
		        }
        	}
        	else{
	        	if(inpTemplateId ==8 || inpTemplateId==11)
				{					
					CP_Save(0,0,0,0);	
				}
				else
				{
					CP_Save(0,0,0,1);
				}	
	        }
			
		}
	});
	$('body').on('click', '.btn-blast-now', function(event) {
		event.preventDefault();
		blastType = 1;
		doBlastRealTime="1";
	

		if ($('#frm-campaign').validationEngine('validate')) {
			
			var SCHEDULETIME = "";
			if ($('.schedule-section').length > 0) {
				SCHEDULETIME = GetSchedule();
				if (!SCHEDULETIME) {
					return false;
				}
			}
			var inpGroupId = $("#SubscriberBlastList").val().toString();
			var inpShortCode = $("#ShortCode").val();										
			BlastNow(inpBatchId, inpGroupId, inpShortCode);
		}
	});

	function htmlEncode(value){
	  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
	  //then grab the encoded contents back out.  The div never exists on the page.
	  return $('<div/>').text(value).html();
	}
	/* Save CP function */
	var CP_Save = function (skipCompletePopup, triggerPreview, skipBlast, skipNewCP) {

		/* check black list words for message: default 0 - dont have any black words */

		var inpblacklistwordcheck = 0;

		/* don't display complete pupup */
		skipCompletePopup = (typeof skipCompletePopup != "undefined") ? skipCompletePopup : 0;

		/* trigger preview button */
		triggerPreview = (typeof triggerPreview != "undefined") ? triggerPreview : 0;

		var  listDeleteRID = intListDeleteRID;
		var inpListOfEmails = [];
		var inpListOfPhones = [];

		if( triggerPreview==1 ) {
			checkClickPreview =1;
		}
		

		if( triggerPreview == 0 && checkClickPreview == 1 && inpTemplateId != 8) {
			listDeleteRID = [];
		}

		/* don't send blast */
		skipBlast = (typeof skipBlast != "undefined") ? skipBlast : 0;

		/* don't send newCP */
		skipNewCP = (typeof skipNewCP != "undefined") ? skipNewCP : 0;


		var inpBatchDesc = $("#Desc_vch").val();
		var inpOrganizationName = $("#OrganizationName_vch").val();
		
		var inpKeyword = $("#Keyword").val();
		var inpShortCode = $("#ShortCode").val();

		// if (hasUnicodeChar(inpBatchDesc)) {
		// 	alertBox("Campaign name contains unicode characters");
		// 	return false;
		// }
		// if (hasUnicodeChar(inpOrganizationName)) {
		// 	alertBox("Company name contains unicode characters");
		// }

		var SCHEDULETIME = "";

		if ($('.schedule-section').length > 0 && inpCampaignType == 1) {
			SCHEDULETIME = GetSchedule();
			if (!SCHEDULETIME) {
				return false;
			}
		}

		
		if(inpCampaignType == 1){
			var inpGroupId = $("#SubscriberBlastList").length > 0 ? $("#SubscriberBlastList").val().toString() : 0;
		}
		else{
			var inpGroupId = $("#SubscriberList").length > 0 ? $("#SubscriberList").val().toString() : 0;
		}


		/* if campaign type is blast check skip blast option */
		if (skipBlast && inpCampaignType==1) {
			inpGroupId = 0;
			inpKeyword = '';
		}
		if(inpCampaignType == 1)
		{
			inpKeyword = '';
		}
			

		var ListControlPointData = [];
		
		//var listCPOneSelection = $(".cpedit").find("[data-control-point-type='ONESELECTION']");

		/* get all CP on page */
		if(inpTemplateId == 8 && inpCampaignType == 1){
			var listCPOneSelection = $(".cpedit.first-msg-content").find(".control-point");
		}
		else{
			var listCPOneSelection = $(".cpedit").find(".control-point");
		}
		
		if(listCPOneSelection.length >0)
		{
			//Loop over specifed OPTIONS
			var OptionCounter = 1;

			listCPOneSelection.each(function( id, el ) {
				OptionCounter = 1;
				/* CP construct */
				var Options = [];
				var Esoptions=[];
				var Conditions = [];
				var ControlPointData = {
		            'AF' : "NOFORMAT",
		            'API_ACT' : "",
		            'API_DATA' : "",
		            'API_DIR' : "someplace/something",
		            'API_DOM' : "somwhere.com",
		            'API_PORT' : "80",
		            'API_RA' : "",
		            'API_TYPE' : "GET",
		            'BOFNQ' : "0",
		            'CONDITIONS' : Conditions,
		            'ERRMSGTXT' : "",
		            'GID' : "1",
		            'ID' : "0",
		            'IENQID' : "0",
		            'IHOUR' : "0",
		            'IMIN' : "0",
		            'IMRNR' : "2",
		            'INOON' : "01",
		            'INRMO' : "END",
		            'ITYPE' : "",
		            'IVALUE' : "0",
		            'OIG' : "0",
		            'OPTIONS' : Options,
		            'REQUIREDANS' : "undefined",
		            'RQ' : '',
		            'SCDF' : "0",
		            'TDESC' : "",
		            'TGUIDE' : "",
		            'TEXT' : "",
		            'TYPE' : '',
		            'SWT' : "1",
					'ESOPTIONS': Esoptions,
					'ANSTEMP':""
	        	};          

	        	/* get CP RQ */
				ControlPointData.RQ = $(this).attr('data-control-rq-id');
				
				var afSelect = $(this).find('.AF-select')[0];
				var textSelect = $(this).find('textarea')[0];
				var tdescSelect = $(this).find('.help-block')[0];
				var tguide = $(this).find('.tguide')[0];

				/* get CP DESC, TEXT, TYPE, AF */
				if(inpTemplateId == 9)
					ControlPointData.TEXT = $(textSelect).val();
				else
					ControlPointData.TEXT = typeof $(textSelect).val() != "undefined" ? $(textSelect).val() : ControlPointData.TEXT;
				//$(textSelect).val();
				ControlPointData.AF = typeof $(afSelect).val() != "undefined" ? $(afSelect).val() : ControlPointData.AF;
				ControlPointData.TDESC = $(tdescSelect).text();
				//ControlPointData.TYPE = $(this).data('controlPointType');
				ControlPointData.TYPE = $(this).attr('data-control-point-type');

				if($(tguide).length > 0)
					ControlPointData.TGUIDE = htmlEncode($(tguide).html());

				/* prepare data for interval type */
				if (ControlPointData.TYPE == "INTERVAL") {
					if(inpTemplateId==8)
					{
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						var time = $($(this).find(".ITIME")[0]).val();
						var hour = 9;
						var minute = 0;
						var noon = "00";
						var date= $($(this).find(".datetimepicker input")[0]).val();
												
						if(date.length > 0)					
						{
							var day = parseInt(date.split("/")[1]);;
							var month = parseInt(date.split("/")[0]);
							var year = parseInt(date.split("/")[2]);
						}
						else
						{
							var now=new Date();
							var day = now.getDate();
							var month =  now.getMonth() +1;
							var year =  now.getFullYear();
						}																		

						ControlPointData.IHOUR = hour;
						ControlPointData.IMIN = minute;
						ControlPointData.INOON = noon;
						
						if(ControlPointData.ITYPE=="SECONDS")						
						{							
							ControlPointData.IHOUR = 0;
							ControlPointData.IMIN = 0;
							ControlPointData.INOON = 0;
							ControlPointData.IVALUE = 0;
						}
						if(ControlPointData.ITYPE=="HOURS")						
						{																				
							ControlPointData.IHOUR = 0 ;
							ControlPointData.IMIN = 0;
							ControlPointData.INOON = "00";							
						}
						if(ControlPointData.ITYPE=="DATE")
						{							
							ControlPointData.IVALUE = year + "/" + month + "/" + day;
						}		
						// 
						// 
						// 
						// 
						// 
					}
					else
					{
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						var time = $($(this).find(".ITIME")[0]).val();
						var hour = parseInt(time.split(":")[0]);
						var minute = parseInt(time.split(":")[1]);
						var noon = "00";
						if (hour >= 12) {
							hour = hour == 24 ? 0 : (hour-12);
							noon = "01";
						}
						hour = hour < 10 ? "0"+hour : hour;
						minute = minute < 10 ? "0"+minute : minute;

						ControlPointData.IHOUR = hour;
						ControlPointData.IMIN = minute;
						ControlPointData.INOON = noon;

						var month = $($(this).find(".IMONTH")[0]).val();
						var year = $($(this).find(".IYEAR")[0]).val();
						var day = $($(this).find(".IDAY")[0]).val();
						if (month != "" && year != "" && day != "") {
							ControlPointData.ITYPE = "DATE";
							ControlPointData.IVALUE = year + "/" + month + "/" + day;
						}
					}					
				}

				/* prepare data for interval type response for template customer survey */
				if (ControlPointData.TYPE == "ONESELECTION" || ControlPointData.TYPE == "SHORTANSWER" ) {
					if($(this).find(".ITYPE").length > 0){
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						ControlPointData.IMRNR = $($(this).find(".IMRNR")[0]).val();
						ControlPointData.INRMO = $($(this).find(".INRMO")[0]).val();
					}
					if (ControlPointData.TYPE == "ONESELECTION")
					{
						ControlPointData.ANSTEMP=$(this).find('.cp_type').data('anstemp');		
												
					}
				}
				
				/* prepare options for ONESELECTION type */
				$(el).find('.AnswerItem').each(function(iid,eel){
					if( $(eel).find("#OPTION").val().trim().length > 0)
					{
						var OptionItem = { 
						'AVAL': $(eel).find("#AVAL").val(),
						'AVALREG': $(eel).find("#AVALREG").val(),
						'ID' : OptionCounter,
						'TEXT' : $(eel).find("#OPTION").val()
						}
						Options.push(OptionItem);
						OptionCounter++;
					}					
				});
				ControlPointData.OPTIONS = Options;

				/* detect CP action */
				var item = {};
				if(skipNewCP == 0)
					item.ACTION = $(el).attr('data-add-new');
				else
					item.ACTION = 'Edit';	

				item.CP = ControlPointData;
				ListControlPointData.push(item);
				//ListControlPointData[ControlPointData.RQ]= item;

			});
		}


		/* Tell the manager options */
		if ($("#tellmgr-sms-checkbox").length > 0) {
			if ($("#tellmgr-sms-checkbox").is(':checked')) {
				$(".sms-number-item").each(function(index, el) {
					var phone = $(el).find("input").val();
					if (phone !== "") {
						inpListOfPhones.push(phone);
					}
				});
			}
		}
		if ($("#tellmgr-email-checkbox").length > 0) {
			if ($("#tellmgr-email-checkbox").is(':checked')) {
				$(".email-add-item").each(function(index, el) {
					var email = $(el).find("input").val();
					if (email !== "") {
						inpListOfEmails.push(email);
					}
				});
			}
		}

		var inpSchdeduleOption ="0"; // now only schedule simple , advance schedule alr remove
		$.ajax({
			url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save_Simple&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			async: false,
			data: {
				inpBatchId: inpBatchId,
				inpSchdeduleOption:inpSchdeduleOption,
				controlPoints: JSON.stringify(ListControlPointData),
				inpBatchDesc: inpBatchDesc,
				inpOrganizationName: inpOrganizationName,
				inpGroupId: inpGroupId,
				inpKeyword: inpKeyword,
				inpShortCode: inpShortCode,
				inpScheduleTime: SCHEDULETIME,
				inpCampaignType: inpCampaignType,
				intListDeleteRID : JSON.stringify(listDeleteRID),
				inpIsTellTheManagerTemplate: (inpTemplateId == 9 ? 1 : 0),
				inpListOfEmails: inpListOfEmails.join(","),
				inpListOfPhones: inpListOfPhones.join(",")
			},
			beforeSend: function () {
				$("#processingPayment").show();
			}
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) > 0) {
				inpBatchName = data.BLASTNAME;

				//if (!skipCompletePopup && action == "CreateNew") {
				
				if (!skipCompletePopup) {
					ShowComplateModal();
					$(window).unbind('beforeunload');
				}
				if (triggerPreview) {
					$("#SMSPreview").trigger('click');
				}

				// if (action == "Edit" && !skipCompletePopup) {
				// 	$(".modal").modal("hide");

				//     bootbox.dialog({
				//         message: '<h4 class="be-modal-title">Saved!</h4><p>Your changes have been saved!</p>',
				//         title: '&nbsp;',
				//         className: "be-modal",
				//         buttons: {
				//             success: {
				//                 label: "OK",
				//                 className: "green-gd",
				//                 callback: function(){
				//                     location.href = "/session/sire/pages/campaign-manage";
				//                 }
				//             },
				//         },
				// 	    onEscape: function() {
				// 	        location.href = "/session/sire/pages/campaign-manage";
				// 	    }
				//     });
				// }
			} else {
					if(parseInt(data.RXRESULTCODE) == 0 && data.ERRMESSAGE == 'BlackWords'){
						alertBox(data.MESSAGE, "BLACK WORDS LIST");
						return;
					}else{

						if(data.MESSAGE ==''){
							$(".modal").modal("hide");
							alertBox("Can not save campaign.", "Oops!");
						}else{
							$(".modal").modal("hide");
							alertBox(data.MESSAGE, "Oops!");
						}
						
					}
			}
		})
		.fail(function(e, msg) {
			$(".modal").modal("hide");
			alertBox("Error. No Response from the remote server. Check your connection and try again.");
		})
		.always(function() {
			$("#processingPayment").hide();
		});
	}


	function GetSchedule() {
		var SCHEDULETIME = "";
		/* prepare schedule option */
		if ($('.schedule-section').length > 0) {

			var selectDateTime = $('#datetimepicker input').val();
			var parsetime = $('#schedule-time-start-hour').val();
			var year = 0;
			var month = 0;
			var day = 0;
			
			if(selectDateTime != ''){
				var arrDateTime = selectDateTime.split(" ");
				// var arrDate = arrDateTime[0];
				// var arrTime = arrDateTime[1];
				//var arrNoon = arrDateTime[2];
				
				/* parse date */
				arrDate = arrDateTime[0].split("/");

	            year = parseInt(arrDate[2]);
	            month = parseInt(arrDate[0]);
	            day = parseInt(arrDate[1]);

	           // /* parse time */
	           // arrTime = arrDateTime[1].split(":");
	           // var startHour = parseInt(arrTime[0]);
	           // var startMinute = parseInt(arrTime[1]);

	           // if(arrNoon =='AM')
	           // //var startNoon = parseInt($("#schedule-time-start-noon").val());
	        	//	var startNoon = 0;
	        	//else
	        	//	var startNoon = 1;	
			}

			if(parsetime != ''){

				var arrTime_noon = parsetime.split(" ");
				var arrNoon = arrTime_noon[1];
				 /* parse time */
	            arrTime = arrTime_noon[0].split(":");
	            var startHour = parseInt(arrTime[0]);
	            var startMinute = parseInt(arrTime[1]);

	            if(arrNoon =='AM')
	            //var startNoon = parseInt($("#schedule-time-start-noon").val());
	        		var startNoon = 0;
	        	else
	        		var startNoon = 1;	

			}
			else
			{
				var startHour ="9";
	            var startMinute = "00";
			}
            var endHour = "9";
            var endNoon = "1";
            var endMinute = "00";

			if (startNoon == 1) {
				startHour = startHour == 12 ? 12 : (parseInt(startHour) + 12);
			} else {
				if (startHour == 12) {
					startHour = 24;
				}
			}

			if (endNoon == 1) {
				endHour = endHour == 12 ? 12 : (parseInt(endHour) + 12);
			} else {
				if (endHour == 12) {
					endHour = 24;
				}
			}

			var thisYear = new Date();

            if ((year == "" || isNaN(year)) || (month == "" || isNaN(month)) || (day == "" || isNaN(day))) {
            	year = thisYear.getFullYear();
            	month = thisYear.getMonth()+1;
            	day = thisYear.getDate();
            	startHour = "9";
            	startMinute = "00";
            } else {

            	// if (startHour == endHour) {
            	// 	if (startMinute > endMinute) {
            	// 		alertBox("Start Time must fall before the End Time");
            	// 		return false;
            	// 	}
            	// } else if ( ((startHour > endHour ) && (startNoon == endNoon)) || ((startHour > endHour ) && (endNoon =='0')) ) {
            	// 	alertBox("Start Time must fall before the End Time 1");
            	// 	return false;
            	// }

            	if((endHour <= startHour ) && (startNoon == endNoon)){
            		alertBox("Start Time must fall before the End Time 1");
            		return false;
            	}



            	if (month == 2) {
            		if ((isLeapYear(year) && day > 29) || (!isLeapYear(year) && day > 28)) {
            			alertBox("The Scheduled date is incorrect, please select a valid date.");
            			return false;
            		}
            	}

            	if (month == 4 || month == 6 || month == 9 || month == 11)
            	{
            		if( day > 30){
            			alertBox("The Scheduled date is incorrect, please select a valid date.");
            			return false;
            		}
            	}
            }
			
			//override real time if click send now , leave it b4  :  var endYear = new Date(year + '/' + month + '/' + day);
			if(doBlastRealTime=="1")	
			{
				year = new Date().getFullYear();
            	month = new Date().getMonth()+1;
            	day = new Date().getDate();
            	startHour = "9";
				startMinute = "00";
								
			}
            if(month=="12")
			{
				var endYear = new Date( (year +1)+ '/' + '1' + '/' + day);
			}
			else
			{
				var endYear = new Date(year + '/' + (month+1) + '/' + day);
			}
            //endYear.setFullYear(endYear.getFullYear() + 10);
			

            var item = {};


			item.startDate = year + '/' + month + '/' + day;			
            item.stopDate = endYear.getFullYear() + '/' + (endYear.getMonth()+1) + '/' + (endYear.getDate());
            item.startHour = parseInt(startHour);
            item.startMinute = parseInt(startMinute);
            item.endHour = parseInt(endHour);
            item.endMinute = parseInt(endMinute);
            item.enabledBlackout = false;
            item.blackoutStartHour = 0;
            item.blackoutEndHour = 0;
            item.dayId = 0;
            item.scheduleType = 2;
            var SCHEDULETIME = [];
            SCHEDULETIME.push(item);

            SCHEDULETIME = JSON.stringify(SCHEDULETIME);
		} else {
			var SCHEDULETIME = "";
		}

		return SCHEDULETIME;
	}

	
	var saveCampaign = 1;
	// var ruleReg = new RegExp('Reply YES to confirm, HELP for help or STOP to cancel\. Up to [0-9]+ msg\/mo\. Msg&Data rates may apply\.');
	// var ruleReg = new RegExp('HELP|STOP|RATES|APPLY', 'i');
	// var ruleReg = new RegExp('(?=.*help)(?=.*stop)(?=.*rates)(?=.*apply)', 'i');
	var ruleReg = /\bhelp\b/i;
	var ruleReg1 = /\bstop\b/i;
	var ruleReg2 = /\brates\b/i;
	var ruleReg3 = /\bapply\b/i;
	var firstMess = $('.control-point').first().find('textarea').val();
	if($('#optin-detect').val() > 0){
		needRuleStr = 1;
	}
	if(inpCampaignType==1)
	{
		needRuleStr = 0;
	}
	var firstMessAlertTitle;

	if(action == "Edit"){
		firstMessAlertTitle = "Edit Campaign";
	}
	else{
		firstMessAlertTitle = "Create Campaign";
	}

	$('body').on('click', '.btn-check-blast', function(event) {
		event.preventDefault();
		if ($('#frm-campaign').validationEngine('validate')) {
			var SCHEDULETIME = "";

			if ($('.schedule-section').length > 0) {
				SCHEDULETIME = GetSchedule();
				if (!SCHEDULETIME) {
					return false;
				}
			}
			var inpGroupId = $("#SubscriberBlastList").val().toString();
			var inpShortCode = $("#ShortCode").val();
			
			if(needRuleStr == 1){

        		//var theFirstMess = $('.control-point').first().find('textarea').val();
    		    if(inpTemplateId == 8){
					//var recentFirstMess = $('.control-point .need-rule-message-textarea').val();	
					var theFirstMess = "";	
					$('.control-point').each(function()
					{							
						if($(this).data("control-rq-id")==1)
						{
							theFirstMess=$(this).find('textarea').val();
						}							
					})
					
        		}
        		else{
        			var theFirstMess = $('.control-point').first().find('textarea').val();	
				}

        		if(!ruleReg.test(theFirstMess) || !ruleReg1.test(theFirstMess) || !ruleReg2.test(theFirstMess) || !ruleReg3.test(theFirstMess)){
        			saveCampaign = 0;
				  //   alertBox('You have entered invalid message. By law, all opt-in messages must include the following disclaimer: "HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply." Please adjust the # of messages and leave the remaining text intact.', firstMessAlertTitle, function(){
				  //   	$('.control-point').first().find('textarea').val($('.control-point').first().find('textarea').val() + 'HELP for help or STOP to cancel. Up to # msg/mo. Msg&Data rates may apply.');
						
				  //   	setTimeout(function() {
						// 	$('.control-point').first().find('textarea').focus();
						// }, 0);
				  //   });
				  	ShowNeedRuleMessageAlert();

				    setTimeout(function() {
						$('.control-point').first().find('textarea').focus();
					}, 0);
            	}
            	else{
            		HideNeedRuleMessageAlert();
            		saveCampaign = 1;
		        	BlastNow(inpBatchId, inpGroupId, inpShortCode);
		        }
        	}
        	else{
	        	BlastNow(inpBatchId, inpGroupId, inpShortCode);
	        }
		}
	});

	var Checktimeblastdbquery = function () {
		var strReturn="";		
		if(blastType==2)
		{
			return strReturn;
		}
		$.ajax({					
			url: '/session/sire/models/cfc/subscribers.cfc?method=CheckTimeBlastDbNotbetween9amto8pm&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   					
			async: false,
			type: 'post',
			dataType: 'json',
			data: {},
			beforeSend: function(){
				
			},
			error: function(jqXHR, textStatus, errorThrown) {					
				alertBox('Check time blast Fail','Check time blast Fail','');						
			},
			success: function(data) {			
				if (data.RXRESULTCODE == 1) {				
					strReturn="";						
				} else {
					strReturn="Time blast invalid";	
				}						
			}
		});	
		return strReturn;
	};


	var BlastNow = function (inpBatchId, inpGroupId, inpShortCode) {
		var doBlast=function()
		{			
			/*<!--- Save Custom Responses - validation on server side - will return error message if not valid --->*/
			$.ajax({
				type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
				url: '/session/sire/models/cfc/control-point.cfc?method=GetBlastToListStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{
					inpBatchId : inpBatchId,
					inpGroupId : inpGroupId,
					inpShortCode : inpShortCode	
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/ 
					alertBox("Error. No Response from the remote server. Check your connection and try again.");
				},
				success:
				/*<!--- Default return function for Async call back --->*/
				function(d)
				{
					/*<!--- RXRESULTCODE is 1 if everything is OK --->*/
					if (d.RXRESULTCODE == 1) 
					{	
						$('#confirm-number-subscribers').text(d.LISTCOUNT);
						$('#confirm-credits-in-queue').text(d.QUEUECOUNT);
						$('#confirm-credits-available').text(d.BALANCE);
						
						$('#confirm-list-duplicates').text(d.LISTCOUNTDUPLICATE);

					// figures credits for campaign
						var nmessage = 0;
						if(inpTemplateId ==8){

							$(".cpedit.first-msg-content").find(".control-point").each(function()
							{							
								if($(this).attr("data-control-point-type")=='INTERVAL')
								{
									nmessage = nmessage +1;
									
								}else{																		
									nmessage=characterCountReturnCount($(this).find('textarea').val());
								}							
							})

						}else{							
							nmessage=characterCountReturnCount($('.msg-content textarea:first-child').val());
						}						
					// old version
						//var TotalElligable = parseInt(d.LISTCOUNT) - parseInt(d.LISTCOUNTDUPLICATE);
						//TotalElligable = TotalElligable > 0 ? TotalElligable : 0;
						var TotalElligable_sb = parseInt(d.LISTCOUNT) - parseInt(d.LISTCOUNTDUPLICATE);
						var TotalElligable = nmessage * TotalElligable_sb;
						TotalElligable = TotalElligable > 0 ? TotalElligable : 0;
							
						$('#confirm-total-elligable').text(TotalElligable);
						$('#confirm-credits-needed').text(TotalElligable);

						/* Check if this batch and subscriber list already in queue together */

						if (parseInt(d.HASACTIVEQUEUE) > 0) {
							alertBox('This campaign has a processing blast with this subscribers.<br>Please choose another list.');
						} else {
							// <!--- Disable send button and warn if credits not enough --->
							if (parseInt(d.BALANCENUMBER) >= parseInt(d.LISTCOUNT)) 
							{
								$('#btn-confirm-save-and-send').prop('disabled', false);
								$('#confirm-launch-alert-text').hide();
							}
							else
							{
								$('#btn-confirm-save-and-send').prop('disabled', true);
								$('#confirm-launch-alert-text').show();
							}
								
							
							$('#ConfirmLaunchModal').modal('show');
						}

						/*<!--- Re-enable button to send for next time --->*/
						$(".btn-save-send").prop("disabled",false);	
					}
					else
					{
						/*<!--- No result returned --->*/
						if(d.ERRMESSAGE != "")
						{	
							bootbox.alert(d.ERRMESSAGE);
							$(".btn-save-send").prop("disabled",false);
						}
					}
				}
			});
		}				

		var Checktimeblastdb = Checktimeblastdbquery();

		var checkListPhoneTimezoneNot31= CheckListPhoneTimezoneNot31(inpGroupId);
		if(checkListPhoneTimezoneNot31 !="" || Checktimeblastdb !="")
		{	
			if(checkListPhoneTimezoneNot31 !=""){
					title='Warning message !';
					message='"Heads up! Some of the numbers on your subscriber list are in a time zone that is past your currently selected schedule end times. These messages might not go out until the next day, subject to any other schedule limitations you might have defined."';
					bootbox.dialog({
						message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
						title: '&nbsp;',
						className: "be-modal",
						buttons: {
							success: {
								label: "Blast Away Anyway",
								className: "btn btn-medium green-gd",
								callback: function(result) {

									if (Checktimeblastdb !=""){
									
										title='Warning message !';
										message='"SEND NOW" option will use default schedule of 9AM to 8PM. There are phone numbers on your list that will not get them until 9AM local to their phone numbers time zone. If you need to blast outside of this window, please set your target hours in "Schedule for Later" options.';
										bootbox.dialog({
											message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
											title: '&nbsp;',
											className: "be-modal",
											buttons: {
												success: {
													label: "Blast Away Anyway",
													className: "btn btn-medium green-gd",
													callback: function(result) {
														doBlast();
													}
												},
												cancel: {
													label: "Cancel",
													className: "green-cancel"
												},
											}
										});

									}else{
										doBlast();
									}

								}
							},
							cancel: {
								label: "Cancel",
								className: "green-cancel"
							},
						}
					});
			}else{ // Checktimeblastdb not blank
				title='Warning message !';
				message='"SEND NOW" option will use default schedule of 9AM to 8PM. There are phone numbers on your list that will not get them until 9AM local to their phone numbers time zone. If you need to blast outside of this window, please set your target hours in "Schedule for Later" options.';
				bootbox.dialog({
					message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
					title: '&nbsp;',
					className: "be-modal",
					buttons: {
						success: {
							label: "Blast Away Anyway",
							className: "btn btn-medium green-gd",
							callback: function(result) {
								doBlast();
							}
						},
						cancel: {
							label: "Cancel",
							className: "green-cancel"
						},
					}
				});

			}	

		}
		else
		{
			doBlast();
		}									
	}

	// Validate keyword in real time - ajax with warnings
	$("#Keyword").delayOn("input", 800, function(element, event) {
		$('#KeywordStatus').removeClass('has-error');	    
		$('#KeywordStatus').removeClass('not-has-error');
	    if(element.value.length == 0)
	    {
		    // Give blank to erase warning if one was previously set
		    if( $('#KeywordDisplayValue').html.length > 0)
		    {
			    //$('#KeywordStatus').css('color', 'red');
			    $('#KeywordStatus').addClass('has-error');
		    	$('#KeywordStatus').html("Save blank to erase existing Keyword if any.");
		    }
		    else
		    {
		    	//$('#KeywordStatus').css('color', 'red');
		    	$('#KeywordStatus').addClass('has-error');
		    	$('#KeywordStatus').html('');			    
		    }
	    }		    
	    else
	    {
		    $('#KeywordStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');
		    
		    /*<!--- Save Custom Responses - validation on server side - will return error message if not valid --->*/				
			$.ajax({
				type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
				url: '/session/sire/models/cfc/control-point.cfc?method=ValidateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: false,
				data:  
				{ 
					inpBatchId : inpBatchId, 
					inpKeyword : element.value,
					inpShortCode : $("#ShortCode").val()					
					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
					alertBox("Error. No Response from the remote server. Check your connection and try again.");
				},					  
				success:		
				/*<!--- Default return function for Async call back --->*/
				function(d) 
				{
					
					/*<!--- RXRESULTCODE is 1 if everything is OK --->*/
					if (parseInt(d.RXRESULTCODE) == 1) 
					{	
						//$('#KeywordStatus').css('color', 'green');
						$('#KeywordStatus').addClass('not-has-error');
						$('#KeywordStatus').html(d.MESSAGE);
					}
					else
					{
						/*<!--- No result returned --->	*/
						if(d.ERRMESSAGE != "")
						{	
							//$('#KeywordStatus').css('color', 'red');
							$('#KeywordStatus').addClass('has-error');
							$('#KeywordStatus').html(d.ERRMESSAGE);
							setTimeout(function(){ 
						    	$("#Keyword").focus();
						    }, 1000);
						}
					}
					$('#KeywordStatus').show();										
				}
			});
		}
	});
	
	//if(inpTemplateId != 9 && inpTemplateId!= 3){
	//	$("#frm-campaign").find('.campaign-next:last').addClass('finished');
	//}
	
	
	$('.pre-fill-name-for-cpn').click(function(){    
		$("#Desc_vch").val("Customer Chat");
	});	
	function AddNewBatchFromTemplateNew()
	{
		//start to create new campaign aaaaaaaaaaaaa						
		var inpDesc=$("#Desc_vch").val();
		var inpKeyword="";
		if($("#Keyword").length > 0)
		{			
			var inpKeyword=$("#Keyword").val();
		}	
		var inpOrganizationName = $("#OrganizationName_vch").val();
		var ListControlPointData = [];
		var listCPOneSelection = $(".cpedit").find(".control-point");	
					
		if(listCPOneSelection.length >0)
		{
			//Loop over specifed OPTIONS
			var OptionCounter = 1;

			listCPOneSelection.each(function( id, el ) {
				OptionCounter = 1;
				/* CP construct */
				var Options = [];
				var Esoptions=[];
				var Conditions = [];
				var ControlPointData = {
		            'AF' : "NOFORMAT",
		            'API_ACT' : "",
		            'API_DATA' : "",
		            'API_DIR' : "someplace/something",
		            'API_DOM' : "somwhere.com",
		            'API_PORT' : "80",
		            'API_RA' : "",
		            'API_TYPE' : "GET",
		            'BOFNQ' : "0",
		            'CONDITIONS' : Conditions,
		            'ERRMSGTXT' : "",
		            'GID' : "1",
		            'ID' : "0",
		            'IENQID' : "0",
		            'IHOUR' : "0",
		            'IMIN' : "0",
		            'IMRNR' : "2",
		            'INOON' : "01",
		            'INRMO' : "END",
		            'ITYPE' : "",
		            'IVALUE' : "0",
		            'OIG' : "0",
		            'OPTIONS' : Options,
		            'REQUIREDANS' : "undefined",
		            'RQ' : '',
		            'SCDF' : "0",
		            'TDESC' : "",
		            'TGUIDE' : "",
		            'TEXT' : "",
		            'TYPE' : '',
		            'SWT' : "1",
					'ESOPTIONS': Esoptions,
					'ANSTEMP':""
	        	};          

	        	/* get CP RQ */
				ControlPointData.RQ = $(this).attr('data-control-rq-id');
				
				var afSelect = $(this).find('.AF-select')[0];
				var textSelect = $(this).find('textarea')[0];
				var tdescSelect = $(this).find('.help-block')[0];
				var tguide = $(this).find('.tguide')[0];

				/* get CP DESC, TEXT, TYPE, AF */
				if(inpTemplateId == 9)
					ControlPointData.TEXT = $(textSelect).val();
				else
					ControlPointData.TEXT = typeof $(textSelect).val() != "undefined" ? $(textSelect).val() : ControlPointData.TEXT;

				//$(textSelect).val();
				ControlPointData.AF = typeof $(afSelect).val() != "undefined" ? $(afSelect).val() : ControlPointData.AF;
				ControlPointData.TDESC = $(tdescSelect).text();
				//ControlPointData.TYPE = $(this).data('controlPointType');
				ControlPointData.TYPE = $(this).attr('data-control-point-type');

				if($(tguide).length > 0)
					ControlPointData.TGUIDE = htmlEncode($(tguide).html());

				/* prepare data for interval type */
				if (ControlPointData.TYPE == "INTERVAL") {
					if(inpTemplateId==8)
					{
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						var time = $($(this).find(".ITIME")[0]).val();
						var hour = 9;
						var minute = 0;
						var noon = "00";
						var date= $($(this).find(".datetimepicker input")[0]).val();
												
						if(date.length > 0)					
						{
							var day = parseInt(date.split("/")[1]);;
							var month = parseInt(date.split("/")[0]);
							var year = parseInt(date.split("/")[2]);
						}
						else
						{
							var now=new Date();
							var day = now.getDate();
							var month =  now.getMonth() +1;
							var year =  now.getFullYear();
						}																		

						ControlPointData.IHOUR = hour;
						ControlPointData.IMIN = minute;
						ControlPointData.INOON = noon;
						
						if(ControlPointData.ITYPE=="SECONDS")						
						{							
							ControlPointData.IHOUR = 0;
							ControlPointData.IMIN = 0;
							ControlPointData.INOON = 0;
							ControlPointData.IVALUE = 0;
						}
						if(ControlPointData.ITYPE=="HOURS")						
						{																				
							ControlPointData.IHOUR = 0 ;
							ControlPointData.IMIN = 0;
							ControlPointData.INOON = "00";							
						}
						if(ControlPointData.ITYPE=="DATE")
						{							
							ControlPointData.IVALUE = year + "/" + month + "/" + day;
						}					
					}
					else
					{
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						var time = $($(this).find(".ITIME")[0]).val();
						var hour = parseInt(time.split(":")[0]);
						var minute = parseInt(time.split(":")[1]);
						var noon = "00";
						if (hour >= 12) {
							hour = hour == 24 ? 0 : (hour-12);
							noon = "01";
						}
						hour = hour < 10 ? "0"+hour : hour;
						minute = minute < 10 ? "0"+minute : minute;

						ControlPointData.IHOUR = hour;
						ControlPointData.IMIN = minute;
						ControlPointData.INOON = noon;

						var month = $($(this).find(".IMONTH")[0]).val();
						var year = $($(this).find(".IYEAR")[0]).val();
						var day = $($(this).find(".IDAY")[0]).val();
						if (month != "" && year != "" && day != "") {
							ControlPointData.ITYPE = "DATE";
							ControlPointData.IVALUE = year + "/" + month + "/" + day;
						}
					}					
				}

				/* prepare data for interval type response for template customer survey */
				if (ControlPointData.TYPE == "ONESELECTION" || ControlPointData.TYPE == "SHORTANSWER" ) {
					if($(this).find(".ITYPE").length > 0){
						ControlPointData.ITYPE = $($(this).find(".ITYPE")[0]).val();
						ControlPointData.IVALUE = $($(this).find(".IVALUE")[0]).val();
						ControlPointData.IMRNR = $($(this).find(".IMRNR")[0]).val();
						ControlPointData.INRMO = $($(this).find(".INRMO")[0]).val();
					}
					if (ControlPointData.TYPE == "ONESELECTION")
					{
						ControlPointData.ANSTEMP=$(this).find('.cp_type').data('anstemp');		
						
					}
				}
				
				/* prepare options for ONESELECTION type */
				$(el).find('.AnswerItem').each(function(iid,eel){
					var OptionItem = { 
					'AVAL': $(eel).find("#AVAL").val(),
					'AVALREG': $(eel).find("#AVALREG").val(),
					'ID' : OptionCounter,
					'TEXT' : $(eel).find("#OPTION").val()
					}
					Options.push(OptionItem);
					OptionCounter++;
				});
				ControlPointData.OPTIONS = Options;

				/* detect CP action */
				var item = {};						
				
					
				item.ACTION = $(el).attr('data-add-new');

				item.CP = ControlPointData;
				ListControlPointData.push(item);
				if($(el).attr('data-add-new').toUpperCase()=='NEW')
				{					
					$(el).addClass('rolback-to-new-mode');
				}	
				$(el).attr('data-add-new','EDIT');					
									

			});
		}
		//

		var inpListOfEmails = [];
		var inpListOfPhones = [];

		/* Tell the manager options */
		if ($("#tellmgr-sms-checkbox").length > 0) {
			if ($("#tellmgr-sms-checkbox").is(':checked')) {
				$(".sms-number-item").each(function(index, el) {
					var phone = $(el).find("input").val();
					if (phone !== "") {
						inpListOfPhones.push(phone);
					}
				});
			}
		}
		if ($("#tellmgr-email-checkbox").length > 0) {
			if ($("#tellmgr-email-checkbox").is(':checked')) {
				$(".email-add-item").each(function(index, el) {
					var email = $(el).find("input").val();
					if (email !== "") {
						inpListOfEmails.push(email);
					}
				});
			}
		}	

		listDeleteRID = [];
		$.ajax({					
			url: '/session/sire/models/cfc/control-point.cfc?method=AddNewBatchFromTemplateNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   					
			async: false,
			type: 'post',
			dataType: 'json',
			data: {
				inpTemplateId: inpTemplateId,
				inpTemplateType:inpTemplateType,
				inpDesc:inpDesc,
				inpKeyword:inpKeyword ,
				inpShortCode:SHORTCODE
			},
			beforeSend: function(){
				
			},
			error: function(jqXHR, textStatus, errorThrown) {					
				alertBox('Create Campaign Fail','Create Campaign Fail','');									
			},
			success: function(data) {																		
				if (data.BATCHID > 0) {	

					inpBatchId = data.BATCHID;
					inpCampaignId = inpBatchId;
					// do save 
					$.ajax({
						url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save_Simple&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						dataType: 'json',
						async: false,
						data: {
							inpBatchId: data.BATCHID,
							inpSchdeduleOption:0,
							controlPoints: JSON.stringify(ListControlPointData),
							inpBatchDesc: inpDesc,
							inpOrganizationName: inpOrganizationName,
							inpGroupId: 0,
							inpKeyword: inpKeyword,
							inpShortCode: SHORTCODE,							
							inpCampaignType: inpTemplateType,
							
							intListDeleteRID : JSON.stringify(listDeleteRID),
							inpIsTellTheManagerTemplate: (inpTemplateId == 9 ? 1 : 0),
							inpListOfEmails: inpListOfEmails.join(","),
							inpListOfPhones: inpListOfPhones.join(",")
							
						},
						beforeSend: function () {
							$("#processingPayment").show();
						}
					})
					.done(function(data) {
						$("#processingPayment").hide();
						if (parseInt(data.RXRESULTCODE) > 0) {
							//
							$(window).unbind('beforeunload');																	
							//location.href=window.location.href + "&campaignid="+data.BATCHID;			
						} else {
							// rol back to new mode f0r control poit error
							$(".rolback-to-new-mode").attr('data-add-new','NEW');
							if(parseInt(data.RXRESULTCODE) == 0 && data.ERRMESSAGE =='BlackWords'){
								alertBox(data.MESSAGE, "BLACK WORDS LIST");
								return;
							}else{
								if(data.MESSAGE ==''){
									$(".modal").modal("hide");
									alertBox("Can not save campaign.", "Oops!");
								}else{
									$(".modal").modal("hide");
									alertBox(data.MESSAGE, "Oops!");
								}
							}
						}
					})
					.fail(function(e, msg) {
						$("#processingPayment").hide();
						$(".modal").modal("hide");
						alertBox("Error. No Response from the remote server. Check your connection and try again.");
					});
										
				}					
			}
		});	
		
	}
    $('.campaign-next').click(function(){  
		//check cpname unicode
		var cpName= $("#Desc_vch").val();
		//not alow campaign name has unicode : for stupid user try to force next
		
		var contentUnicode=  /[^\u0000-\u007f]/.test(cpName);
		if(contentUnicode)
		{
			alertBox('Please Enter Campaign Name without Unicode','Alert','');																						
			setTimeout(function(){ 
								
				$("#Desc_vch").focus();
			}, 3000);
			$('html,body').animate({
				scrollTop: $("#cppNameDisplay").offset().top-100},
			'slow');
			return false;
		}
		//end check cpname unicode  		
    	var allTextAreFilled = 1;

    	if(globalSelectedTemplateType!==9)							
		{	
			if(globalSelectedTemplateType=="1")							
			{
				$(".green-campaign").addClass('hidden');	
			}	
			else
			{
				$(".blue-campaign").addClass('hidden');
			}
		}
    	
    	$('textarea[data-emojiable="converted"]').each(function(index){
    		if($(this).val() == ''){
    			$(this).parent('.form-group').find('div.emoji-wysiwyg-editor').validationEngine('showPrompt', '*This field is required', 'load','topLeft',true);
    			// $("html, body").animate({ scrollTop: $('div.emoji-wysiwyg-editor').first().offset().top-500});
    			
    			$("html, body").animate({ scrollTop: $(this).parent('.form-group').find('div.emoji-wysiwyg-editor').offset().top-500});
    			allTextAreFilled = 0
    		}
    		else{
    			$(this).parent('.form-group').find('div.emoji-wysiwyg-editor').validationEngine('hide');
    		}

    		if(allTextAreFilled == 0){
    			return false;
    		}
    	});

    	// if($('textarea').first().val() ==''){
    	// 	$('div.emoji-wysiwyg-editor').first().validationEngine('showPrompt', 'Required', 'load','topLeft',true);
    	// 	$("html, body").animate({ scrollTop: $('div.emoji-wysiwyg-editor').first().offset().top-500});
    	// 	return;
    	// }
    	

        if (allTextAreFilled == 1) {	
			
			if(inpCampaignId==0 || inpCampaignId=="")
			{
				AddNewBatchFromTemplateNew();
				//return;
			}

			if ($(this).hasClass('btn-schedule-later')) {
            	$(".btn-confirm-section").hide(); 	
				$(".btn-schedule-later").hide();   
			}					
            var parentCp = $(this).closest('.cpedit');				
            var nextParentCp = parentCp.next();	

			//leave this section b4 nextParentCp= 	parentCp.next();				
			if(globalSelectedTemplateType!==9)							
			{	
				if(globalSelectedTemplateType=="1")							
				{
					nextParentCp = parentCp.nextAll(".blue-campaign").first();
					//$(".green-campaign").addClass('hidden');	
				}	
				else
				{
					nextParentCp = parentCp.next('.green-campaign');
					//$(".blue-campaign").addClass('hidden');
				}
			}

			// end section 

            if ($(this).hasClass('nextCpeditCheckRule') || $(this).hasClass('finished')) {
            	// var ruleStr = "You've chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply";
            	if(needRuleStr == 1){
            		
            		if(inpTemplateId == 8){
						//var recentFirstMess = $('.control-point .need-rule-message-textarea').val();	
						var recentFirstMess = "";	
						$('.control-point').each(function()
						{							
							if($(this).data("control-rq-id")==1)
							{
								recentFirstMess=$(this).find('textarea').val();
							}							
						})
						
            		}
            		else{
            			var recentFirstMess = $('.control-point').first().find('textarea').val();	
					}
					//alert(recentFirstMess);
            		if(!ruleReg.test(recentFirstMess) || !ruleReg1.test(recentFirstMess) || !ruleReg2.test(recentFirstMess) || !ruleReg3.test(recentFirstMess)){
            			saveCampaign = 0;
					 //    alertBox('You have entered invalid message. By law, all opt-in messages must include the following disclaimer: "HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply." Please adjust the # of messages and leave the remaining text intact.', firstMessAlertTitle, function(){
				  //   		$('.control-point').first().find('textarea').val($('.control-point').first().find('textarea').val() + 'HELP for help or STOP to cancel. Up to # msg/mo. Msg&Data rates may apply.');
				  //   	});
				  //   	setTimeout(function() {
						// 	$('.control-point').first().find('textarea').focus();
						// }, 0);
						
						ShowNeedRuleMessageAlert();

					    setTimeout(function() {
							$('.control-point').first().find('textarea').focus();
						}, 0);

						return false;
	            	}
	            	else{
	            		
	            		HideNeedRuleMessageAlert();
	            		saveCampaign = 1;
			        	nextParentCp.removeClass('hidden');
			        }
            	}
            	else{
		        	nextParentCp.removeClass('hidden');
		        }
            	

            }
            else{
	        	nextParentCp.removeClass('hidden');
	        }

            if ($(this).hasClass('finished') && saveCampaign == 1) {
            	$("#ConfirmLaunchModal").modal("hide");
        		CP_Save(1,1,1);
	        }


            $("html, body").animate({ scrollTop: $(nextParentCp).offset().top - $(".page-header").height() });

            if (action == "CreateNew") {
            	doughnutStep.update($(this).data('step-value'));
          //   	var firstMessageContent = $('.msg-content textarea:first-child').val();
		        // if($("#preview_message").length > 0){
		        //     $("#preview_message").text(firstMessageContent);
		        // }
            }


        }
    });	
	
    if (action == "CreateNew") {
		$(window).bind('beforeunload', function() {
		    return 'If you leave this page, your campaign will be discarded.';
		});
    }
	$("body").on('click', '.btn-cancel-schedule', function(event) {    			
		$(".btn-confirm-section").show(); 
		$(".btn-schedule-later").show(); 
		
		$(".schedule-section").addClass("hidden");
	});	

    $("body").on('click', '.btn-save-blast', function(event) {
    	event.preventDefault();
    	blastType = 3;		
    	if ($('#frm-campaign').validationEngine('validate')) {

    		CP_Save(0,1,1,0);
    	}
    });
	$("body").on('click', '.btn-save-schedule', function(event) {
		doBlastRealTime=0;
    	event.preventDefault();
    	blastType = 2;		    	
		if ($('#frm-campaign').validationEngine('validate')) {
			
			var SCHEDULETIME = "";
			if ($('.schedule-section').length > 0) {
				SCHEDULETIME = GetSchedule();
				if (!SCHEDULETIME) {
					return false;
				}
			}
			var inpGroupId = $("#SubscriberBlastList").val().toString();
			var inpShortCode = $("#ShortCode").val();										
			BlastNow(inpBatchId, inpGroupId, inpShortCode);
		}
		//
    });	
    $("body").on('click', '.inner-retangle', function(event) {
    	event.preventDefault();
    	if (action == "Edit") {
    		return true;
    	}

    	var id = $(this).data('template-id');
    	if(inpCampaignId > 1){
    		if (id != inpTemplateId) {
	    		bootbox.dialog({
			        message: '<h4 class="be-modal-title">Alert</h4><p>By selecting this template, your current campaign draft will be discarded. Are you sure you would like to continue?</p>',
			        title: '&nbsp;',
			        className: "be-modal",
			        buttons: {
			            success: {
			                label: "Yes",
			                className: "btn btn-medium green-gd",
			                callback: function(result) {
								//ConfirmLeavePage(id);

								$(window).unbind('beforeunload');
				    			if(id == 11 || id == 8 )
				    				var	selectedTemplateTypeRe = 9;
				    			else if(id == 1 || id == 9 )
				    				var	selectedTemplateTypeRe = 0;
				    			else if(id == 3 )
				    				var	selectedTemplateTypeRe = 1;
				    			else
				    				var selectedTemplateTypeRe = 0;									
								
								location.href = "/session/sire/pages/campaign-template-new?templateid=" +id+"&selectedTemplateType="+selectedTemplateTypeRe;
								

			                }
			            },
			            cancel: {
			                label: "NO",
			                className: "green-cancel",
			                callback: function () {
			                	$(".inner-retangle[data-template-id='"+inpTemplateId+"']").trigger('click');
			                }
			            },
			        }
			    });
    		}
    	}
    	else{
    		if (id != inpTemplateId) {
	    		$(window).unbind('beforeunload');
    			if(id == 11 || id == 8 )
    				var	selectedTemplateTypeRe = 9;
    			else if(id == 1 || id == 9 )
    				var	selectedTemplateTypeRe = 0;
    			else if(id == 3 )
    				var	selectedTemplateTypeRe = 1;
    			else
    				var selectedTemplateTypeRe = 0;	
					location.href = "/session/sire/pages/campaign-template-new?templateid=" +id+"&selectedTemplateType="+selectedTemplateTypeRe;
    			
    		}
    	}
    	
    });

    var ConfirmLeavePage = function (id) {
    	$.ajax({
    		url: '/session/sire/models/cfc/campaign.cfc?method=UpdateCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
    		type: 'POST',
    		data: {campaignId: inpCampaignId},
    	})
    	.done(function(data) {
    		if (parseInt(data.RXRESULTCODE) > 0) {
    			$(window).unbind('beforeunload');
    			if(id == 11 || id == 8 )
    				var	selectedTemplateTypeRe = 9;
    			else if(id == 1 || id == 9 )
    				var	selectedTemplateTypeRe = 0;
    			else if(id == 3 )
    				var	selectedTemplateTypeRe = 1;
    			else
    				var selectedTemplateTypeRe = 0;	

    			location.href = "/session/sire/pages/campaign-template-new?templateid=" +id+"&selectedTemplateType="+selectedTemplateTypeRe;
    		} else {
    			alertBox("Something wrong happend. Please try again later!");
    		}
    	})
    	.fail(function() {
    		alertBox("Error. No Response from the remote server. Check your connection and try again.");
    	});
    }

    

	//UTF8 decode for messages that have Unicode characters
	function utf8_decode (strData) {

		var tmpArr = []
		var i = 0
		var c1 = 0
		var seqlen = 0

		strData += ''

		while (i < strData.length) {
			c1 = strData.charCodeAt(i) & 0xFF
			seqlen = 0

			// http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
			if (c1 <= 0xBF) {
				c1 = (c1 & 0x7F)
				seqlen = 1
			} else if (c1 <= 0xDF) {
				c1 = (c1 & 0x1F)
				seqlen = 2
			} else if (c1 <= 0xEF) {
				c1 = (c1 & 0x0F)
				seqlen = 3
			} else {
				c1 = (c1 & 0x07)
				seqlen = 4
			}

		for (var ai = 1; ai < seqlen; ++ai) {
			c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
		}

		if (seqlen === 4) {
			c1 -= 0x10000
			tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
			tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
		} else {
			tmpArr.push(String.fromCharCode(c1))
			}

			i += seqlen
		}

		return tmpArr.join('')
	}
	$(document).on('keyup','.control-point textarea',function(){		
		var cpTextWithoutCDF = $(this).val();
		cpTextWithoutCDF = cpTextWithoutCDF.replace(/\{\%(.\w*)\%\}/g, "");
		if(hasUnicodeChar(cpTextWithoutCDF)){
			if($(this).closest('.form-group').find('.see-unicode-mess').hasClass('hidden')){
				$(this).closest('.form-group').find('.see-unicode-mess').removeClass('hidden');
			}	
		}
		else {
			$(this).closest('.form-group').find('.see-unicode-mess').addClass('hidden');
		}
	});
	

    $('body').on('click', '.see-unicode-mess', function(event){
    	emojiPickerDiv = $(this).closest('.form-group').find('div.emoji-wysiwyg-editor');
    	
    	// event.preventDefault();
    	currentSec = $(this);
		$('#edit-cp-text').modal('show');
		currentTextArea = $(this).closest('.form-group').find('textarea');
				
		currentMessage = currentTextArea.val();
		counterId = $(this).closest('.form-group').find(".control-point-char").attr('id');

		//$('#message-content').val(unescape(encodeURIComponent(currentMessage)));
		$('#message-content').val(unescape((currentMessage)));
		$('#edit-cp-text-form').on('submit', function(event){
			event.preventDefault();
			if($('#edit-cp-text-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
				
				var newMessage  = utf8_decode(unescape(encodeURIComponent($('#message-content').val())));
				
				// 
				emojiPickerDiv.empty();
				emojiPickerDiv.text(newMessage);
				currentTextArea.val(newMessage);
				
				
				
				//countText("#"+counterId, emojiPickerDiv.text());
				countText("#"+counterId, $('#message-content').val());
				
				if(!hasUnicodeChar(currentTextArea.val())){
					currentSec.hide();
				}
				$('#edit-cp-text').modal('hide');
			}
    	});
    });

    $(document).on('click', '#save-as-new-template', function(event){
		event.preventDefault();
		$('#AdminSaveNewTemplateModal').modal('show');
		GetMaxOrderByCategoryGroup();
		InitSaveNewTemplateForm();
		count_down_character_by_id('template-description-countdown','template-description', 1000);
		count_down_character_by_id('template-name-countdown','template-name', 255);
	});
	
	$('#AdminSaveNewTemplateModal').on('hide.bs.modal', function(){
		$('#select-template-image').ddslick('destroy');
		$('#template-name').val('');
		$('#template-description').val('');
	});
	
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;
	
	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');
	
	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};
	
	function count_down_character_by_id(countId, contentId, limit){
		$('#' + countId).text(limit-$('#'+ contentId).val().length);
	
		$('#' + contentId).on('keyup', function(){
			$('#' + countId).text(limit-$('#' + contentId).val().length);
		});
	
		var contentText = document.getElementById(contentId);
		if(contentText) {
		    contentText.addEventListener("input", function() {
		        if (contentText.value.length == limit) {
		        	return false;
		        } 
		        else if (contentText.value.length > limit) {
		        	contentText.value = contentText.value.substring(0, limit);
		        }
		    },  false);
		}
	
		$('#' + contentId).bind('paste', function(e) {
		    var elem = $(this);
		    setTimeout(function() {
		        var text = elem.val();
		        if (text.length > limit) {
					elem.val(text.substring(0,limit));
		        }
		        $('#' + countId).text(limit-parseInt(elem.val().length));
		    }, 100);
		});
	}

	var templateXML;
	var maxOrder;
	function InitSaveNewTemplateForm(){
		
		var batchId = getUrlParameter('campaignid');
		
		$.ajax({
			url: '/session/sire/models/cfc/campaign.cfc?method=GetTemplateDetailOfCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: 
			{	
				inpBatchId: batchId
			},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(data.RXRESULTCODE == 1){
					$('#select-template-category').html('');
					for (var i = 0; i < data.CATEGORIES.length; i++) {
						$('#select-template-category').append('<option value=' + data.CATEGORIES[i].ID + '>' + data.CATEGORIES[i].DISPLAYNAME + "</option>");
					}
					$('#select-template-image').html('');
					for (var j = 0; j < data.IMAGES.length; j++) {
						$('#select-template-image').append('<option value=' + data.IMAGES[j].NAME + ' data-imagesrc="/session/sire/images/template-picker/'+ data.IMAGES[j].NAME +'">' + data.IMAGES[j].NAME + '</option>');
					}
	
					$('#select-template-image').ddslick({
			         	height: '350px',
			         	width: '100%',
					    selectText: "Select Template Images",
					    onSelected: function (data) {
					        $("#select-template-image").val(data.selectedData.value)
					    }
			    	});
					templateXML = data.XMLCONTROLSTRING;
					var xml=data.XMLCONTROLSTRING;
					// xml = xml.replace(/<br>/g, "");
					// xml = xml.replace(/<ul>/g, "");
					// xml = xml.replace(/<\/ul>/g, "");
					// xml = xml.replace(/<li>/g, "");
					// xml = xml.replace(/<\/li>/g, "");
					// xml = xml.replace(/<pre>/g, "");
					// xml = xml.replace(/<\/pre>/g, "");
					// xml = xml.replace(/&gt;/g, ">");
					// xml = xml.replace(/&lt;/g, "<");
					// xml = xml.replace(/>\s*</g, "><");
					// xml=format_xml(xml);
					// xml = xml.replace(/>/g, "&gt;");
					// xml = xml.replace(/</g, "&lt;");
	
					$('#template-xml').val(xml)
				}
				else{
					bootbox.dialog({
			         	message: data.MESSAGE,
			         	title: "GET CAMPAIGN DETAIL",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	$('#processingPayment').hide();
		     	bootbox.dialog({
		         	message:'Get campaign detail failed!',
		         	title: "GET CAMPAIGN DETAIL",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
		    } 
		});
	}
	
	
	$('#confirm-save-new-template-btn').click(validate_save_new_template);
	
	
	function validate_save_new_template()
	{	if($('#save-as-new-template-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			var templateName = $('#template-name').val();
			var templateCategory = $('#select-template-category').val();
			var templateDescription = $('#template-description').val();
			var templateImage = $('#select-template-image').val();
			var templateOrder = $('#select-template-order').val();
			var batchId = getUrlParameter('campaignid');
			$.ajax({
				url: '/session/cfc/templates.cfc?method=CreateCampaignTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'JSON',
				data: 
				{	
					INPNAME: templateName,
					INPCATEGORY: templateCategory,
					INPDESC: templateDescription,
					CurrentReviewXMLString: templateXML,
					INPBATCHID: batchId,
					INPIMAGE: templateImage,
					INPORDER: maxOrder+1,
					inpTemplateType: $('#inpTemplateType').val()
				},
				async: false,
				beforeSend: function () {
					$("#processingPayment").show();
				},
				success: function(data){
					$("#processingPayment").hide();
					if(data.RXRESULTCODE == 1){
						$('#AdminSaveNewTemplateModal').modal('hide');
						bootbox.dialog({
				         	message: data.MESSAGE,
				         	title: "SAVE AS NEW TEMPLATE",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
					}
					else{
						bootbox.dialog({
				         	message: data.MESSAGE,
				         	title: "SAVE AS NEW TEMPLATE",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
			    	$('#processingPayment').hide();
			     	bootbox.dialog({
			         	message:'Save new template request failed!',
			         	title: "SAVE AS NEW TEMPLATE",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
			    } 
			});
			
		}
	}	

	function GetMaxOrderByCategoryGroup(){
		var catGroup = $("#select-template-category").val();
		$.ajax({
			url: '/session/sire/models/cfc/campaign.cfc?method=GetMaxOrderByCategoryGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: {inpCategoryGroup: catGroup},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(data.RXRESULTCODE == 1){
					$("#select-template-order").html('');
					// for (var i = 0; i < data.MAXORDER; i++) {
					// 	$("#select-template-order").append('<option value='+ (i+1) + '>' + (i+1) + '</option>');
					// }
					maxOrder = data.MAXORDER;
				}	
					
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	$('#processingPayment').hide();
		     	bootbox.dialog({
		         	message:'Get max order request failed!',
		         	title: "GET TEMPLATE DETAIL",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
		    } 
		});
		
	}

});

function openCampaignSimon(){
	$('#cancelCampaignTemplate').modal('show');
}

function dismissCampaignSimon(){
	$('#cancelCampaignTemplate').modal('hide');
}

function removeCampaignSimon(){
	var idCampaign = $("#idCampaignSimon").val();
	var dataObj = {
		inpBatchIDS: idCampaign
	};

	$.ajax({
    	type: "POST",
       	url:"/session/sire/models/cfc/campaign.cfc?method=deleteCampaignSimon&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
       	dataType: "json",
       	data: dataObj,          	
       	success: function(data){

			// Check if variable is part of JSON result string								
			if(typeof(data.RXRESULTCODE) != "undefined")
			{
				CurrRXResultCode = data.RXRESULTCODE;
				
				if(CurrRXResultCode > 0)
				{	
					$(window).unbind('beforeunload');
					location.href = "/session/sire/pages/campaign-template-new";
       			}else{
					alertBox("Error. No Response from the remote server. Check your connection and try again.");
				}
       		}
     	}
	});
}

// New function Blast
function VisibleBlackout(i) {
	var isBlackoutChecked = $('#chkEnableBlachout_' + i).is(':checked'); 
	if (isBlackoutChecked) {
		$("#divBlackoutTime_" + i).show();
	}
	else {
		$("#divBlackoutTime_" + i).hide();
	}			
}

function DoCheckAll() {			
			
	var isChecked = $('.check_all').is(':checked');
	$.each($('.check'), function(index, value) {
		if (isChecked) {
			if (IsInRange(parseInt($(this).val()))) {
				$(this).attr('checked', true);
			}
			else {
				$(this).attr('checked', false);
				
			}
			$(this).attr('disabled', 'disabled');
		}
		else {
			$(this).attr('disabled', false);
		}
	});

	// if(isChecked){
	// 	$('.start-end-time').show();
	// }
	// else{
	// 	$('.start-end-time').hide();	
	// }
}
function SelectDate(selector) {
	if(selector)
	{
		var date = $(selector).val();
	
		var elements = date.split('-');
		
		$('#' + selector.id + "_Month").val(elements[0]);
		$('#' + selector.id + "_Day").val(elements[1]);
		$('#' + selector.id + "_Year").val(elements[2]);
	}
}

function CreateHourList(selectControl) {
	$(selectControl).find('option').remove();
	var initStartHour = "09";
	var customField = $(selectControl).attr('customfield');
	if (customField.indexOf('blackout') != -1) {
		if (customField.indexOf('end') != -1) {
			initStartHour = 1;
		}
		else {
			initStartHour = 11;
		}
	}
	else {
		if (customField.indexOf('end') != -1) {
			initStartHour = 8;
		}
		else {
			initStartHour = 9;
		}
	}
	$(selectControl).append(new Option("12", "0"));
	var hour;			
	for (var i = 1; i < 12; i++) {
		hour = i < 10 ? "0" + i : i;
		$(selectControl).append(new Option(hour, i, true));
	}
	$(selectControl).val(initStartHour);
}

function CreateMinuteList(selectControl) {
	$(selectControl).find('option').remove();
	var minute;
	//for (var i = 0; i < 60; i++) {
		//minute = i < 10 ? "0" + i : i;
		//$(selectControl).append(new Option(minute, i));
	for (var i = 0; i < 4; i++) {
		minute = i < 1 ? "0" + i : i * 15;
		$(selectControl).append(new Option(minute, i));
	}
}

function CreateMonthList(selectControl) {
	$(selectControl).find('option').remove();
	var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
	/* $(selectControl).append(new Option("Month", "-1")); */
	
	for (var i = 0; i < months.length; i++) {
		$(selectControl).append(new Option(months[i], i + 1));
	}
}

function CreateDayList(selectControl) {
	$(selectControl).find('option').remove();
	/* $(selectControl).append(new Option("Day", "-1")); */
	var day;
	for (var i = 1; i < 32; i++) {
		day = i < 10 ? "0" + i : i;
		$(selectControl).append(new Option(day, i));
	}
}

function CreateYearList(selectControl) {
	$(selectControl).find('option').remove();
	/* $(selectControl).append(new Option("Year", "-1")); */
	
	for (var i = new Date().getFullYear() + 15; i >= 2012; i--) {
		$(selectControl).append(new Option(i, i));
	}
}

function CreateNoonList(selectControl) {
	$(selectControl).find('option').remove();
	$(selectControl).append(new Option('AM', 0));
	$(selectControl).append(new Option('PM', 1));
	
	var customField = $(selectControl).attr('customfield');
	var initValue = customField.indexOf('end') != -1 ? 1 : 0;
	$(selectControl).val(initValue);
}
function CreateDatePicker(controlId, isEndDate) {
	
	if (isEndDate) {
		$('#' + controlId).val(date_txt_1);
	}
	else {
		$('#' + controlId).val(date_txt_2);
	}

	$('#' + controlId).datepicker({
		numberOfMonths: 1,
		showButtonPanel: false,
		dateFormat: 'm-d-yy',
		defaultDate: $('#btnDate_FollowingTime').val(),
		showOn: 'both',
		buttonImage: "/public/images/calendar.png",
		buttonImageOnly: true,
		minDate : date_txt_2,
	});
	
	SelectDate(document.getElementById(controlId));
}

function GetDayNameById(dayId) {
	switch(dayId) {
		case 0: {
			return "All";
		}
		case 1: {
			return "Sunday";
		}
		case 2: {
			return "Monday";
		}
		case 3: {
			return "Tuesday";
		}
		case 4: {
			return "Wednesday";
		}
		case 5: {
			return "Thursday";
		}
		case 6: {
			return "Friday";
		}
		default: {
			return "Saturday";
		}
	}
}

function hideEditProfile()
{		
	if($("#OrganizationName_vch").val())
	{
		$(".cp-update-cpn").addClass('hidden');
	}
	else
	{
		$(".cp-update-cpn").removeClass('hidden');
	}
}
function CheckListPhoneTimezoneNot31(listSubscriberList)
{
	var strReturn="";
	var inpStartTime=9;	
	
	if(doBlastRealTime == "0")	
	{
		var inpStartTime=9;
		var parsetime = $('#schedule-time-start-hour').val();		
		if(parsetime != ''){
		
			var arrTime_noon = parsetime.split(" ");
			var arrNoon = arrTime_noon[1];
				/* parse time */
			arrTime = arrTime_noon[0].split(":");
			var startHour = parseInt(arrTime[0]);			

			if(arrNoon =='AM')
			{
				inpStartTime=startHour;
			}				
			else
			{
				if(startHour=="12")
					inpStartTime=startHour;
				else
					inpStartTime=startHour+12;	
			}			
		}						
	}
	$.ajax({					
		url: '/session/sire/models/cfc/subscribers.cfc?method=GetSubcriberListBringContactTimeZoneNot31&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   					
		async: false,
		type: 'post',
		dataType: 'json',
		data: {
				inpListGroupID:listSubscriberList,
				inpStartTime:inpStartTime,
				inpBlastNow:doBlastRealTime
		},
		beforeSend: function(){
			
		},
		error: function(jqXHR, textStatus, errorThrown) {					
			alertBox('Check Timezone Fail','Check Timezone Fail','');						
		},
		success: function(data) {			
			if (data.RXRESULTCODE == 1) {				
				strReturn="There some contact timezone not 31";						
			} else {
				
			}						
		}
	});	
	return strReturn;
}

function UpdateCampaignType(campaignID,selectedTemplateType)
{	
	
	doChange= function()
	{	
		if(campaignID == 0) {
			campaignID = inpCampaignId;
		}
		globalSelectedTemplateType=selectedTemplateType;
		//update	
		$.ajax({					
			url: '/session/sire/models/cfc/campaign.cfc?method=UpdateCampaignTemplateType&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   					
			async: false,
			type: 'post',
			dataType: 'json',
			data: {
				inpBatchId:campaignID,
				inpTemplateType:selectedTemplateType,
				inpTemplateTypeID:inpTemplateId
			},
			beforeSend: function(){
				$('#processingPayment').show();
			},
			error: function(jqXHR, textStatus, errorThrown) {			
				$('#processingPayment').hide();		
				alertBox('Update Campaign Template Type Fail','Update Campaign Template Type Fail','');						
			},
			success: function(data) {			
				$('#processingPayment').hide();
				if (data.RXRESULTCODE == 1) {	

					inpCampaignType = selectedTemplateType;
					if(inpTemplateId==8)
					{
						if(selectedTemplateType == 1)
						{
							var optinMaxRQID = 1;

							var listDeleteCP = $('.green-campaign.msg-content').find('.control-point');
							if(listDeleteCP.length > 0){

								listDeleteCP.each(function( index ) {
									var currentPID = $( this ).attr('data-control-point-physical-id');	
									if(parseInt(currentPID) > 0){
										var searchIndex = intListDeleteRID.indexOf(currentPID);
										if(index == -1){
											tempListDeleteRID.push(currentPID);
											intListDeleteRID.push(currentPID);		
										}
									}
								});
							}
							
							if(arrCpBranch.length > 0){
								for(var i = 0; i < arrCpBranch.length; i++){
									var searchIndexBranch = intListDeleteRID.indexOf(arrCpBranch[i]);
									if(searchIndexBranch == -1){
										intListDeleteRID.push(arrCpBranch[i]);  // Branch	
										tempListDeleteRID.push(arrCpBranch[i]);
									}
								}
							}
							
							if(arrCpOptIn.length > 0){
								for(var i = 0; i < arrCpOptIn.length; i++){
									var searchIndexOptin = intListDeleteRID.indexOf(arrCpOptIn[i]);
									if(searchIndexOptin == -1){
										intListDeleteRID.push(arrCpOptIn[i]);  // Option	
										tempListDeleteRID.push(arrCpOptIn[i]);
									}

									optinMaxRQID = arrCpOptIn[i];
								}
							}
							// intListDeleteRID.push('11'); // Optin
							// tempListDeleteRID.push('11');
							
							if(arrCpShortAns.length > 0){
								for(var i = 0; i < arrCpShortAns.length; i++){
									var searchIndexShortAns = intListDeleteRID.indexOf(arrCpShortAns[i]);
									if(searchIndexShortAns == -1){
										intListDeleteRID.push(arrCpShortAns[i]);  // ShortAnswer	
										tempListDeleteRID.push(arrCpShortAns[i]);
									}
								}
							}

							if(arrCpTrailer.length > 0){
								for(var i = 0; i < arrCpTrailer.length; i++){
									var searchIndexTrailer = intListDeleteRID.indexOf(arrCpTrailer[i]);
									if(searchIndexTrailer == -1){
										intListDeleteRID.push(arrCpTrailer[i]);  // TRAILER	
										tempListDeleteRID.push(arrCpTrailer[i]);
									}
								}
							}

							// intListDeleteRID.push('2');  //ShortAnswer
							// tempListDeleteRID.push('60'); // TRAILER
							
							currentRQID = optinMaxRQID+1;

							$('.first-msg-content').find(".control-point").each(function(index, el) {
					            //$(el).attr('data-control-rq-id', currentRQID++);
								var cpPhysicalId = $(el).attr('data-control-point-physical-id');
								
								if(cpPhysicalId.length)
								{
									console.log(cpPhysicalId);
									intListDeleteRID.push(cpPhysicalId);  // TRAILER	
									tempListDeleteRID.push(cpPhysicalId);
								}					            
					        });

					        $('.first-msg-content').find(".control-point").each(function(index, el) {
					            $(el).attr('data-control-rq-id', currentRQID++);
					            $(el).attr('data-add-new','New');
					        });

					        needRuleStr = 0;
						}
						else{

							if(tempListDeleteRID.length > 0){
								for(var i = 0; i < tempListDeleteRID.length; i++){
									var index = intListDeleteRID.indexOf(tempListDeleteRID[i]);	
									var index = intListDeleteRID.indexOf(tempListDeleteRID[i]);	

									if (index > -1) {
									    intListDeleteRID.splice(index, 1);
									}
								}
							}

							tempListDeleteRID = [];
							currentRQID = cpOptIn+1;

					        $('.first-msg-content').find(".control-point").each(function(index, el) {
					            $(el).attr('data-control-rq-id', currentRQID++);
					            var cpPhysicalId = $(el).attr('data-control-point-physical-id');

					            var indexInterval = arrCpInterval.indexOf(cpPhysicalId);	
								var indexStatment = arrCpStatement.indexOf(cpPhysicalId);	
								if(indexInterval > -1 || indexStatment > -1 ){
									$(el).attr('data-add-new','EDIT');
								}
					        });

							needRuleStr = 1;
						}
						
						
						BindCharacterCountAllPage();
					}	

					// globalAlrDoSelectGreenOrBlue=1;
					// if(selectedTemplateType==0)
					// {
					// 	$("#blueboxMain").removeClass("uk-active");
					// 	$("#blueboxMain").addClass("uk-unactive");
					// }
					// else
					// {
					// 	$("#greenboxMain").removeClass("uk-active");
					// 	$("#greenboxMain").addClass("uk-unactive");						
					// }
				}					
			}
		});	
	}

	if(globalAlrDoSelectGreenOrBlue==0)
	{
		doChange();	
		if(selectedTemplateType==1)
		{
			needRuleStr = 0;
		}
	}	
		
}



$(document).ready(function() {


// Begin Slide campaign template choice
	$(window).bind('load resize', function () {
		var $box = $('.campaign-template.active').find('.campaign-template-content');
		var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
		calCulateHeightBox($box, $hBox);

		var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		if (width < 640) {
			$('.last-on-mobile').addClass('uk-flex-last');
			$('.first-on-mobile').addClass('uk-flex-first');
		} else {
			$('.last-on-mobile').removeClass('uk-flex-last');
			$('.first-on-mobile').removeClass('uk-flex-first');
		}
	
	});

	function calCulateHeightBox ($box, $hBox) {
		$box.height($hBox);
	}

	$("html, body").on('click', 'a[data-slide="prev"]', function(event) {
		event.stopPropagation();
		$(this).parents('.carousel').carousel('prev');
	});

	$("html, body").on('click', 'a[data-slide="next"]', function(event) {
		event.stopPropagation();
		$(this).parents('.carousel').carousel('next');
	});
// End Slide campaign template choice
	// Timepicker
	if ($('.schedule-section').length > 0){
		$('#datetimepicker').datetimepicker({
			format: 'MM/DD/YYYY'
		});
		
		$('#schedule-time-start-hour').timepicker({ 
		      'step': 60, 
		  'scrollDefault': '9:00 AM',
		  'timeFormat': 'h:i A',
		  'disableTimeRanges': [
		       ['8:15pm', '11:59pm'],
		       ['12am', '8:59am']
		   ]
		  
		  });
		
		$('#time-picker-clock-icon').on('click', function(){
			$('#schedule-time-start-hour').timepicker('show');
		});
		// End timepicker
	}

	hideEditProfile();
	$('#SubscriberList').on('change', function(){
		$('#frm-campaign').validationEngine('hide');
	});	

	$('#SubscriberBlastList').on('change', function(){
		$('#frm-campaign').validationEngine('hide');
	});

	if (action == "CreateNew")
	{		
		$('#update-profile-from-cp').attr("data-toggle","modal");
		$('#update-profile-from-cp').attr("data-target","#modalCompanyUpdate");	
		$('#frCompanyUpdate').on('submit', function(){
			var cpnEnter= $("#txtCompanyUpdate").val();			
			var cpnFromProfile = $('#OrganizationName_vch').val();
			$('.portlet-body').find('textarea[data-control="text-message"]').each(function(){
							
				var content= $(this).val();				
				content=content.replace(cpnFromProfile, cpnEnter); // for replace Company Name pull from db	
				content=content.replace("Company Name", ""); // for replace default text "Company Name"	
				$('#OrganizationName_vch').val(cpnEnter);										
				// $(this).text(content);
				$(this).val(content);
				//$('.emoji-wysiwyg-editor').val(content);
			});	
			// update company name				
			$.ajax({					
				url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateUserCompanyName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   					
				async: true,
				type: 'post',
				dataType: 'json',
				data: {inpCompanyName: cpnEnter},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#processingPayment').hide();
					alert(errorThrown);
					alertBox('Update Company Name Fail','Update Company Name Fail','');						
				},
				success: function(data) {
					$('#processingPayment').hide();
					if (data.RXRESULTCODE == 1) {
						$("#modalCompanyUpdate").modal('hide');
						$(".modal-backdrop").fadeOut();							
					} else {
						/*modalAlert('Add New Subscriber List', data.MESSAGE);*/
						alertBox(data.MESSAGE,'Update Company Name Fail','');
					}						
				}
			});				
			hideEditProfile();
			return false;
		});	
	}
	else
	{
		$(".cp-update-cpn").addClass('hidden');	
	}	

	if(action == "CreateNew"){
		
		$('#cppNameDisplay, .edit-name-icon').on('click', function(){
			var cppnamedefault = $('#cppNameDisplay').text();
			$('#Desc_vch').val(cppnamedefault);
			$('#Desc_vch').show();
			$('#Desc_vch').focus().select();
			$('#cppNameDisplay').hide();
			$('.edit-name-icon').hide();
		});	
		var check_cpptemplate = $('#cppNameDisplay').val();

		if (check_cpptemplate != undefined){
			$('#Desc_vch').on('blur', function(){				
				var cpName= $("#Desc_vch").val();
				
				var cppnamechange = $('#Desc_vch').val();
				$('#cppNameDisplay').text(cppnamechange);
				$('#cppNameDisplay').show();
				$('.blue-pen').show();
				$('#Desc_vch').hide();
				$('.edit-name-icon').show();
			});	

			$('#Desc_vch').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				var cppnamechange_enter = $('#Desc_vch').val();
				$('#cppNameDisplay').text(cppnamechange_enter);
				$('#cppNameDisplay').show();
				$('#Desc_vch').hide();
				$('.edit-name-icon').show();
				if(inpCampaignId != 0){
					SaveCampaignNameOnEnterPress(cppnamechange_enter,"CreateNew");
				}
			}
			});

			// $('#Desc_vch').keyup(function(e){
			// if(e.keyCode=='27'){
			// 	var today_current = moment().format('YYYY-MM-DD');
			// 	$('#Desc_vch').val("Blast to Subscribers on " + today_current);
			// }       
			// });
		}

	}else{

		$('#Desc_vch').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				var cppnamechange_enter = $('#Desc_vch').val();
				SaveCampaignNameOnEnterPress(cppnamechange_enter,"Edit");
			}
		});

		var oldvaluecpp = $('#Desc_vch').val();

		$('#Desc_vch').keyup(function(e){
          if(e.keyCode=='27'){
            $('#Desc_vch').val(oldvaluecpp);
          }       
      	});
	}
	
	// Schedule blast
	$('#btn_advance_schedule').on('click', function(){
		$('#simple_schedule').hide();
		$('#btn_simple_schedule').show();

		$('#advance_schedule').show();
		$('#btn_advance_schedule').hide();			
	});	
	$('#btn_simple_schedule').on('click', function(){
		$('#simple_schedule').show();
		$('#btn_simple_schedule').hide();

		$('#advance_schedule').hide();
		$('#btn_advance_schedule').show();
		
	});

	function SaveCampaignNameOnEnterPress(newName,onAction){
		var batchToUpdateName = inpCampaignId ;

		$.ajax({
			url: '/session/sire/models/cfc/campaign.cfc?method=SaveCampaignName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'post',
			dataType: 'json',
			data: {
				inpBatchId: batchToUpdateName,
				inpBatchName: newName
			},
			success: function(d){
				if(parseInt(d.RXRESULTCODE) < 1){
					alertBox(d.MESSAGE, "Edit Campaign Name");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/ 
				alertBox("Error. No Response from the remote server. Check your connection and try again.");
			}
		});
		
	}	
	

	// subscriber list 
	if ($('#SubscriberList').length > 0){
		var data=BLAST_GROUPID;			
		if(data.length > 0)
		{
			var dataarray=data.split(",");
			// Set the value
			$("#SubscriberList").val(dataarray);
		}		
	}

	// if ($('#SubscriberBlastList').length > 0){
	// 	var data=BLAST_GROUPID;			
	// 	if(data.length > 0)
	// 	{
	// 		var dataarray=data.split(",");
	// 		// Set the value
	// 		$("#SubscriberBlastList").val(dataarray);
	// 	}		
	// }

	// auto change width timepicker
	$('#schedule-time-start-hour').on('focus', function(){
		var width_timepicker = $(".ui-timepicker-input").outerWidth();
		$(".ui-timepicker-wrapper").css("width",width_timepicker);
	});

	$('#time-picker-clock-icon').on('click', function(){
		var width_timepicker = $(".ui-timepicker-input").outerWidth();
		$(".ui-timepicker-wrapper").css("width",width_timepicker);
	});
	
	//for new inter val style	
	
	$(document).on('change','.itype-select-new', function(){		
		$(this).closest('.item-another-mesage').find(".selected-interval").addClass('hidden');
		$(this).closest('.item-another-mesage').find(".selected-date").addClass('hidden');		
		var selected= $(this).val();
		if(selected=="HOURS")
		{
			$(this).closest('.item-another-mesage').find(".selected-interval").removeClass('hidden');
		}
		else if (selected=="DAYS")
		{
			$(this).closest('.item-another-mesage').find(".selected-interval").removeClass('hidden');
		}
		else if (selected=="SECONDS")
		{

		}
		else if (selected=="WEEKS")
		{
			$(this).closest('.item-another-mesage').find(".selected-interval").removeClass('hidden');
		}
		else if (selected=="DATE")
		{
			$(this).closest('.item-another-mesage').find(".selected-date").removeClass('hidden');
			//$(this).closest('.item-another-mesage').find('.datetimepicker ').datetimepicker({
            //    format: 'MM/DD/YYYY'		
            //});
		}
	});
	$('.datetimepicker').datetimepicker({
		format: 'MM/DD/YYYY'		
	});
});



