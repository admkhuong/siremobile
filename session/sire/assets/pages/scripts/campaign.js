
var CampaignComponents = function () {

    return {

        initHandleSelectTemplate: function () {
            $('.inner-retangle').bind('click', function (e) {
                if(action == "CreateNew"){
                    $('.campaign-template').removeClass('active').find('.campaign-template-content').hide();
                    $(this).addClass('active').find('.campaign-template-content').show();

                    var heightBoxInner = $(this).find('.inner-content').outerHeight();
                    $(this).find('.campaign-template-content').height(heightBoxInner);
                }
            });

            $(window).bind('load resize', function () {
                var $box = $('.campaign-template.active').find('.campaign-template-content');
                var $hBox = $('.campaign-template.active').find('.inner-content').outerHeight();
                calCulateHeightBox($box, $hBox);
            });

            function calCulateHeightBox ($box, $hBox) {
                $box.height($hBox);
            }
        },

        initPreviewScroll: function () {
            $("#SMSHistoryScreenArea").mCustomScrollbar({
                theme: 'dark-2',
                setHeight: 455
            });
        },

        handleBootstrapSelect: function() {
            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        },

        handleAddMoreAnswerText: function () {
            var $fieldStr = '';

            $fieldStr += '<div class="row AnswerItem row-small">';
                $fieldStr += '<div class="col-xs-8 col-sm-10">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" id="OPTION" class="form-control" />';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1 hidden">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<input type="text" id="AVAL" class="form-control text-center" value="" /><input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value=""> ';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
                $fieldStr += '<div class="col-xs-2 col-sm-1">';
                    $fieldStr += '<div class="form-group">';
                        $fieldStr += '<button class="form-control delete-answer">';
                            $fieldStr += '<i class="fa fa-minus"></i>';
                        $fieldStr += '</button>';
                    $fieldStr += '</div>';
                $fieldStr += '</div>';
            $fieldStr += '</div>';

            $('.wrapper-answer-text').on('click', '.add-more-answer', function(event) {
                event.preventDefault();
                $(this).parents('.wrapper-answer-text').find('.field-anser-holder').append($fieldStr);
            });

            $('.field-anser-holder').on('click', '.delete-answer', function(event) {
                event.preventDefault();
                $(this).parents('.row.row-small').remove();
            });
        },

        handleAddAnotherMessage: function () {
            var $rendered = new EJS({url:'assets/pages/scripts/another-message.ejs'}).render();

            $('.wrap-box-another-mesage').on('click', '.another-prepend', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').prepend($rendered);
            });

            $('.wrap-box-another-mesage').on('click', '.another-append', function(event) {
                event.preventDefault();
                $(this).parents('.wrap-box-another-mesage').find('.wrap-item-message').append($rendered);
            });
        },

        handleAddSmsNumber: function () {
            // var $rendered = new EJS({url:'assets/pages/scripts/sms-number-template.ejs'}).render();
            var fieldStr = '';

            fieldStr += '<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">';
                fieldStr += '<div class="uk-width-expand">';
                    fieldStr += '<input type="text" class="form-control phone-style input-list-phone" />';
                fieldStr += '</div>';
                fieldStr += '<div class="uk-width-auto col-for-more">';
                    fieldStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
                    fieldStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
                fieldStr += '</div>';
            fieldStr += '</div>';

            $('.sms-number-wrap').on('click', '.trigger-plus', function(event) {
                event.preventDefault();

                if ($(".sms-number-item").length >= 10) {
                    return false;
                }

                $(this).parents('.hold-more-content').append(fieldStr);

                var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

                if ($item.length > 1) {
                    $('.sms-number-wrap').addClass('hasMinus');
                }
            });

            $('.sms-number-wrap').on('click', '.trigger-minus', function(event) {
                event.preventDefault();

                $(this).parents('.sms-number-item').remove();

                var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

                if ($item.length === 1) {
                    $('.sms-number-wrap').removeClass('hasMinus');
                }
            });
        },

        handleEmailAddress: function () {
            // var $rendered = new EJS({url:'assets/pages/scripts/sms-number-template.ejs'}).render();
            var fieldsStr = '';

            fieldsStr += '<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">';
                fieldsStr += '<div class="uk-width-expand">';
                    fieldsStr += '<input type="text" class="form-control validate[custom[email]] input-list-mail" />';
                fieldsStr += '</div>';
                fieldsStr += '<div class="uk-width-auto col-for-more">';
                    fieldsStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
                    fieldsStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
                fieldsStr += '</div>';
            fieldsStr += '</div>';

            $('.email-add-wrap').on('click', '.trigger-plus', function(event) {
                event.preventDefault();

                if ($(".email-add-item").length >= 10) {
                    return false;
                }

                $(this).parents('.hold-more-content').append(fieldsStr);

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length > 1) {
                    $('.email-add-wrap').addClass('hasMinus');
                }
            });

            $('.email-add-wrap').on('click', '.trigger-minus', function(event) {
                event.preventDefault();

                $(this).parents('.email-add-item').remove();

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length === 1) {
                    $('.email-add-wrap').removeClass('hasMinus');
                }
            });
        },
        init: function () {
            //this.initDialStep();
            this.initHandleSelectTemplate();
            this.handleAddMoreAnswerText();
            this.initPreviewScroll();
            //this.handleAddAnotherMessage();
            this.handleBootstrapSelect();
            this.handleAddSmsNumber();
            this.handleEmailAddress();
        }

    };




}();
var inpCompanyName = $('#OrganizationName_vch');


var BindCharacterCountAllPage = function () {
    var id = 1;
    $(".control-point-char").each(function(index, el) {
        $(el).attr('id', 'cp-char-count-'+(id++));
    });

    $(".control-point").each(function(index, el) {

        if ($(el).find('textarea').length > 0) {

            var counterId = $(el).find(".control-point-char").attr('id');;
            var text = $(el).find("textarea")[0];
            var opttext_add="";
            if($(this).find('#select-select-val').val() != undefined && $(this).find('#select-select-type').val() != undefined)
            {
                opttext_add="Reply YES to confirm, HELP for help or STOP to cancel.Up to "+$(this).find('#select-select-val').val()+ " msg/ "+$(this).find('#select-select-type').val()+". Msg&Data rates may apply.";
            }

            var str = $(text).val();

            //str = str.replace(new RegExp("\n", 'gi'),"23");
            //str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');

            countText("#"+counterId, str+ opttext_add);
            $(text).unbind('input change');
            $(text).bind('input change', function(e) {

                if($(this).closest(".cp-content").find('#select-select-val').val() != undefined && $(this).closest(".cp-content").find('#select-select-type').val() != undefined)
                {
                    opttext_add="Reply YES to confirm, HELP for help or STOP to cancel.Up to "+$(this).closest(".cp-content").find('#select-select-val').val()+ " msg/ "+$(this).closest(".cp-content").find('#select-select-type').val()+". Msg&Data rates may apply.";
                }
                countText("#"+counterId, $(text).val()+ opttext_add);
                $(text).val($(text).val().replace("’", "'"));
                $(text).val($(text).val().replace("‘", "'"));
                var firstMessageContent = $('.msg-content textarea:first-child').val();
                //console.log(firstMessageContent);
                    if($("#preview_message").length > 0){
                        $("#preview_message").text(doConvertString(firstMessageContent));
                    }
                //

                //var text1 = $(text).val();
                var text1 =  $(this).next('.emoji-wysiwyg-editor').html();
                var text2 =  $(text).val();

                if(text1)
                {
                    var divText = doConvertString(text1);
                    //$(this).next('.emoji-wysiwyg-editor').html(divText);
                }

                text2 = doConvertString(text2);


                var fultext= text2+ opttext_add;

                if(fultext.length > 160)
                {
                    $(this).closest(".control-point-body").find(".span_text").html(fultext.substring(0,160).replace(/\n/g, "</br>") + '...');
                }
                else
                {
                    $(this).closest(".control-point-body").find(".span_text").html(fultext.replace(/\n/g, "</br>"));
                }
            });

            $(text).trigger('input');
        }
    });
}

var countMgs = function(){
            if ($('.msg-number').length > 0 ){
            $('.msg-number').each(function(index, el) {
                $(el).html("Drip Message "+parseInt(index+1));
            });
        }
}

jQuery(document).ready(function()
{

    $(".campaign-template").off();

    if(action == 'CreateNew')
    {
        $(".cpedit:not(:first)").addClass('hidden');
        $(".cpedit-forceshow").removeClass('hidden');

        $('*[data-template-id="'+inpTemplateId+'"]').trigger('click');
        $(".start-edit").addClass('hidden');

        $(".campaign-template").each(function(index, el) {
            $(this).unbind("click");
        });
    }

    if (action == "Edit") {

        $(".campaign-template:not(.active)").addClass('cursor-disabled');
        $('.campaign-template').unbind('click');

        var firstMessageContent = $('.msg-content textarea:first-child').val();
        if($("#preview_message").length > 0){
            $("#preview_message").text(doConvertString(firstMessageContent));
        }

        $(".content-for-first-campaign").show();
    }


    CampaignComponents.init();

    displayQuestionNum();

    BindCharacterCountAllPage();



    $('.campaign-template.active').find('.campaign-template-content').show();



    $('.btn-create-new-list').click(function(){
        $('#subscriber_list_name').val('');
        $('#AddNewSubscriberList button').prop('disabled', false);
        $('#AddNewSubscriberList').modal('show');
    });

    $('#AddNewSubscriberList').on('shown.bs.modal', function(event){
        $('#SubscriberList').validationEngine('hide');
    });
    //

    // ADD ANOTHER QUESTION - ONE SELECTION
    $(document).on("click",".btn-add-another",function(e)
    {

        e.preventDefault();
        var parents = $(this).parents('.control-point');
        var currentRQID = parents.attr('data-control-rq-id');
        var currentPhysicalId=  parseInt(parents.attr('data-control-point-physical-id') )+ 1;
        var clone = parents.find('.Select2').select2('destroy').parents('.control-point').clone(true);

        clone.find("input[type=text], textarea").val("");

        if(inpTemplateId != 11){
            clone.find("select").prop('selectedIndex',0);
        }
        else{
            clone.find("select.IMRNR").prop('selectedIndex',2);
            clone.find("select.INRMO").prop('selectedIndex',1);
        }

        clone.find(".cp-update-cpn").addClass("hidden");

        clone.find('.ans-group').each(function(id,el){
            var id = $(el).attr('id');
            var ran = Math.floor((Math.random() * 100) + 1);
            id = id+'-'+ran;
            $(el).attr('id',id);
            $(el).next('.lbl-ans-group').attr('id',id);
            $(el).next('.lbl-ans-group').attr('for',id);
        });

        currentCPOneSelection++;
        countCPOneSelection++;

        clone.find('.paginate_text').val(currentCPOneSelection);
        clone.find('.paginate_of').html('of '+countCPOneSelection);
        clone.attr('data-add-new','New');


        var cpType = clone.attr('data-control-point-type');

        //clone.attr('data-control-point-physical-id',0);
        //console.log(clone);

        //parents.addClass('hidden');
        parents.after(clone).promise().done(function(){
            clone.find('[data-control="text-message"]').val((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");

            clone.find('div.emoji-wysiwyg-editor').text((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");

            $('.control-point .Select2').select2(
                { theme: "bootstrap", width: 'auto'}
            );

            // $('.control-point .IMRNR').last().val('2').trigger('change.select2');
            // $('.control-point .INRMO').last().val('NEXT').trigger('change.select2');

            clone.find('.cp_type').val(cpType);
        });
        var selectText = $(this).closest('.control-point').find(".cp_type option:selected").text();
        //clone.find('.cp_type').val(selectText);
        //
        clone.find(".cp_type option").filter(function() {
            //may want to use $.trim in here
            return $(this).text() == selectText;
        }).prop('selected', true);
        //

        // console.log(clone);

        var list = $(".cpedit").find("["+selectorCP+"]");
        var first = [];

        var i=1;
        //aaaaaaaaaaaaaaaa
        parents.nextAll(".control-point").each(function(index, el) {
            $(el).attr('data-control-rq-id', currentRQID++);
            $(el).attr('data-control-point-physical-id', currentPhysicalId++);
        });

        displayQuestionNum();

        //renderPaging(currentCPOneSelection,countCPOneSelection,clone.find('.paging-content'));

        // console.log("Count:"+countCPOneSelection);

        if(countCPOneSelection > 1){
            $(".btn-remove-cp").css("display","inline-block");
        }

        clone.find('.emoji-menu').css('display','none');
        clone.find('select.cp_type').val('SHORTANSWER').trigger('change');

        clone.find('div.emoji-wysiwyg-editor').text((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
        // Initializes and creates emoji set from sprite sheet
        var emojiPicker2 = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        emojiPicker2.discover();

        BindCharacterCountAllPage();
        countMgs();
    });

    //REMOVE QUESTION
     $(document).on("click",".btn-remove-cp",function(e)
    {
        e.preventDefault();

        var parents = $(this).parents('.control-point');
        var currentRQID = parents.attr('data-control-rq-id');
        var currentPID = parents.attr('data-control-point-physical-id');

        if(countCPOneSelection == 1){
            alertBox('You need at least one CP','Remove Fail','');
            return false;
        }

        var displayAfterEdit = parents.prev("["+selectorCP+"]");

        if(displayAfterEdit.length >0){
            displayAfterEdit.removeClass('hidden');
        }
        else{
            var displayAfterEdit = parents.next("["+selectorCP+"]");
            displayAfterEdit.removeClass('hidden');
        }

        if(parents.attr('data-add-new').toUpperCase() == 'EDIT')
        {
            intListDeleteRID.push(currentPID);
        }
        //aaaaaaaaaaaaaa
        parents.nextAll(".control-point").each(function(index, el) {
            $(el).attr('data-control-rq-id', currentRQID++);
            $(el).attr('data-control-point-physical-id', currentPID++);
        });

        //$(".control-point[data-control-point-type='TRAILER']").attr('data-control-rq-id', currentRQID++);
        //$(".control-point[data-control-point-type='TRAILER']").attr('data-control-point-physical-id', currentPID++);

        parents.remove();

        //console.log(parents);

        if(currentCPOneSelection > 1)
        {
            currentCPOneSelection--;
        }

        countCPOneSelection--;


        displayQuestionNum();

        displayAfterEdit.removeClass('hidden');
        //renderPaging(currentCPOneSelection,countCPOneSelection,displayAfterEdit.find('.paging-content'));


        if(countCPOneSelection <= 1){
            $(".btn-remove-cp").css("display","none");
        }

        BindCharacterCountAllPage();
        countMgs();
    });


    //show/hide time inteval
    $(document).on("click",".toogle-inteval",function(e)
    {
        e.preventDefault();
        var toogle_div = $(this).parent().next('.div-toogle-inteval');
        // console.log(toogle_div);

        if(toogle_div.hasClass('hidden'))
        {
            toogle_div.removeClass('hidden');
        }
        else
        {
            toogle_div.addClass('hidden');
        }

    });

    //  BEGIN PAGING
    if(typeof countCPOneSelection != "undefined" && countCPOneSelection > 1){

        //$(".cpedit").find("["+selectorCP+"]:not(:first)").addClass('hidden');
        $(".cpedit").find("["+selectorCP+"]").each(function( index ) {
          if(index > 0 && ( $(this).attr('data-control-point-type') == 'ONESELECTION' || $(this).attr('data-control-point-type') == 'SHORTANSWER' ) )
          {
            //$(this).addClass('hidden');
          }
        });
        //load paging
        //renderPaging(currentCPOneSelection,countCPOneSelection,$(".cpedit").find("["+selectorCP+"]:first").find('.paging-content'));
    }
    else{
         if( typeof countCPOneSelection != "undefined" && countCPOneSelection <= 1){
            $(".btn-remove-cp").css("display","none");
        }
    }


    $(document).on("change",".paginate_text",function() {
        var selectedPage = $(this).val();

         pagingAction($(this),selectedPage);
    });

    $(document).on("click",".paginate_button",function() {

        if($(this).hasClass('next')){
            var selectedPage = currentCPOneSelection+1;
            pagingAction($(this),selectedPage);
        }

        if($(this).hasClass('previous')){
            var selectedPage = currentCPOneSelection-1;
            pagingAction($(this),selectedPage);
        }

        if($(this).hasClass('last')){
            var selectedPage = countCPOneSelection;
            pagingAction($(this),selectedPage,'last');
        }

        if($(this).hasClass('first')){
            var selectedPage = 1;
            pagingAction($(this),selectedPage,'first');
        }

    });

    function displayQuestionNum(){

        var list = $(".cpedit").find(".control-point");
        if(list.length > 0)
        {
            var first = [];
            var i=1;

            list.each(function(id,el){

                var curr = parseInt($(el).attr('data-control-rq-id'));

                if(first.indexOf(curr)>=0)
                {
                    $(el).attr('data-control-rq-id', curr+1);
                    first.push(curr+1);
                }
                else
                {
                    first.push(curr);
                }

                $(el).find('.questionNumber').html('Question '+i);
                i++;
            });
        }

        var listONE = $(".cpedit").find("["+selectorCP+"]");
        if(listONE.length > 0)
        {
            var i=1;

            listONE.each(function(id,el){
                $(el).find('.questionNumber').html('Question '+i);
                i++;
            });
        }
    }
    function pagingAction(that,selectedPage,direction){

        if(selectedPage <= 0 || selectedPage > countCPOneSelection )
        return;

        that.parents('.control-point').addClass('hidden');

        if(direction == 'first')
        {
            var nextCPSelection = $(".cpedit").find("["+selectorCP+"]:first");
            //$(".cpedit").find("[data-control-point-type='ONESELECTION']:first").removeClass('hidden');
        }
        else if(direction == 'last')
        {
         var nextCPSelection = $(".cpedit").find("["+selectorCP+"]:not([data-control-point-type='TRAILER']):last");   //hmm this is not good, need to comeback later
         //$(".cpedit").find("[data-control-point-type='ONESELECTION']:last").removeClass('hidden');
        }

        else if( selectedPage > currentCPOneSelection || direction == 'next'){
            var nextCPSelection = that.parents('.control-point').next("["+selectorCP+"]");
        }
        else if( selectedPage < currentCPOneSelection || direction == 'previous')
        {
            var nextCPSelection = that.parents('.control-point').prev("["+selectorCP+"]");
        }


        currentCPOneSelection = selectedPage
        nextCPSelection.removeClass('hidden');

        if(countCPOneSelection > 1){
            $(".btn-remove-cp").css("display","inline-block");
        }

        renderPaging(currentCPOneSelection,countCPOneSelection,nextCPSelection.find('.paging-content'));
    }

    function renderPaging(currentPage,totalPage,divObj){
        var html_paging ='<div class="dataTables_paginate paging_input"><span class="paginate_button first"><img class="paginate-arrow" src="/session/sire/images/double-left.png"></span><span class="paginate_button previous" id="tblCampaignList_previous"><img class="paginate-arrow" src="/session/sire/images/left.png"></span><span class="paginate_page">Question </span><input type="text" class="paginate_text" style="display: inline;" value='+currentPage+'><span class="paginate_of"> of '+totalPage+'</span><span class="paginate_button next" id="tblCampaignList_next"><img class="paginate-arrow" src="/session/sire/images/right.png"></span><span class="paginate_button last" id="tblCampaignList_last"><img class="paginate-arrow" src="/session/sire/images/double-right.png"></span></div>';
        divObj.html(html_paging);
    }
    //  END PAGING

    // ADD NEW SUBCRIBER LIST
    $('#AddNewSubscriberList #btn-save-group').click(function(){
        if ($('#frm_subscriber_list').validationEngine('validate', {scroll: false ,focusFirstField : false})) {
            $('#AddNewSubscriberList button').prop('disabled', true);
            var groupName = $('#subscriber_list_name').val();
            var inpCPE = $('#btn-save-group').data('inpCPE');

            $.ajax({
                url:'/session/sire/models/cfc/control-point.cfc?method=AddGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {INPGROUPDESC: groupName},
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('Create Subscriber Fail','Create New Subscriber','');
                    $('#AddNewSubscriberList button').prop('disabled', false);
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        GroupsListBox = null;
                        $('#AddNewSubscriberList').modal('hide');
                        //PopulateSubscriberListSelectBox(inpCPE.find('#OIG'), d.INPGROUPID)
                        PopulateSubscriberListSelectBox($("#SubscriberList"), data.INPGROUPID)
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Create New Subscriber','');
                    }
                    $('#AddNewSubscriberList button').prop('disabled', false);
                }
            });
        }
    });


    // Becuase these items can be dynamically added - set here each time they are added inpObj here is the SELECT box --->
    function PopulateSubscriberListSelectBox(inpObj, inpOIG)
    {
        if(GroupsListBox != null)
        {
            //Remove current Questions from Box --->
            inpObj.empty();

            //Only hit DOM once to add all of the options --->
            inpObj.html(GroupsListBox.join(''));

            //Now set the selection here --->
            inpObj.val(inpOIG);
        }
        else
        {

            $.ajax({
                type: "POST", // Posts data as form data rather than on query string and allows larger data transfers than URL GET does
                url: '/session/sire/models/cfc/control-point.cfc?method=GetGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType: 'json',
                async: true,
                data:
                {

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { //No result returned
                    bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
                success:
                // Default return function for Async call back
                function(d)
                {
                    // RXRESULTCODE is 1 if everything is OK --->
                    if (d.RXRESULTCODE == 1)
                    {
                        // Remove current Questions from Box --->
                        inpObj.empty();

                        // Store Questions for Select box here --->
                        var output = [];

                        // Default 0 --->
                        output.push('<option value="0">Select Subscriber List</option>');


                        $.each(d.GROUPS.DATA.GROUPID_BI, function(i, val)
                        {
                          // HTML Encode Text for Display - needs to be 'quote safe'
                          // Use the Physical - non-changing ID for each reference
                          if(parseInt(inpOIG) == parseInt(d.GROUPS.DATA.GROUPID_BI[i]))
                            output.push('<option value="'+ d.GROUPS.DATA.GROUPID_BI[i] +'" selected>'+ d.GROUPS.DATA.GROUPNAME_VCH[i] +'</option>');
                          else
                            output.push('<option value="'+ d.GROUPS.DATA.GROUPID_BI[i] +'">'+ d.GROUPS.DATA.GROUPNAME_VCH[i] +'</option>');

                        });

                        // Store here to minimize trips to DB  --->
                        GroupsListBox = output;

                        // Only hit DOM once to add all of the options --->
                        inpObj.html(output.join(''));

                        // Be sure to select stored value if there is one --->
                        inpObj.val(parseInt(inpOIG));

                        $(".bs-select").selectpicker('refresh');

                        $("html, body").animate({ scrollTop: $("#SubscriberList").offset().top - $(".page-header").height() });
                    }
                    else
                    {
                        // No result returned --->
                        bootbox.alert("General Error processing your request.");
                    }

                }

            });

        }

    }

    //$('.btn-add-answer-list-template').click(function(e){
    $(document).on("click",".btn-add-answer-list-template",function(e){
            e.preventDefault();
            var inpCPE = '';
            //var answers_list = $(this).data('answer-list');
            var parents = $(this).parents('.control-point');

            var answers_list = parents.find('.answers_list');
            answers_list.html('');


            var selected_answer_template = $('input[name=ans-group]:checked').val();
            switch(selected_answer_template)
            {
                // Yes or No
                case '1':
                    AddOptionToList('Yes', 'Yes', '(?i)^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|in|ye|yas', answers_list);
                    AddOptionToList('No', 'No', '(?i)^n$|no|non|ne|nyet|no way|negative|^0$|out', answers_list);
                break;

                // True or False
                case '2':
                    AddOptionToList('True', '1', '', answers_list);
                    AddOptionToList('False', '0', '', answers_list);
                break;

                // Agree or Disagree
                case '3':
                    AddOptionToList('Strong Disagree', '1', '', answers_list);
                    AddOptionToList('Disagree', '2', '', answers_list);
                    AddOptionToList('Neither Disagree or Agree', '3', '', answers_list);
                    AddOptionToList('Agree', '4', '', answers_list);
                    AddOptionToList('Strong Agree', '5', '', answers_list);
                break;

                // Useful or Not
                case '4':
                    AddOptionToList('Not useful', '1', '', answers_list);
                    AddOptionToList('Somewhat useful', '2', '', answers_list);
                    AddOptionToList('Useful', '3', '', answers_list);
                    AddOptionToList('Mostly useful', '4', '', answers_list);
                    AddOptionToList('Very useful', '5', '', answers_list);
                break;

                // Easy or Not
                case '5':
                    AddOptionToList('Not easy', '1', '', answers_list);
                    AddOptionToList('Somewhat easy', '2', '', answers_list);
                    AddOptionToList('Easy', '3', '', answers_list);
                    AddOptionToList('Mostly easy', '4', '', answers_list);
                    AddOptionToList('Extremely easy', '5', '', answers_list);
                break;

                // Likely or Not
                case '6':
                    AddOptionToList('Extremely likely', '1', '', answers_list);
                    AddOptionToList('Very likely', '2', '', answers_list);
                    AddOptionToList('Moderately likely', '3', '', answers_list);
                    AddOptionToList('Slightly likely', '4', '', answers_list);
                    AddOptionToList('Not at all likely', '5', '', answers_list);
                break;

                // Important or Not
                case '7':
                    AddOptionToList('Extremely important', '1', '', answers_list);
                    AddOptionToList('Very important', '2', '', answers_list);
                    AddOptionToList('Moderately important', '3', '', answers_list);
                    AddOptionToList('Slightly important', '4', '', answers_list);
                    AddOptionToList('Not at all important', '5', '', answers_list);
                break;

                // Satisfied or Not
                case '8':
                    AddOptionToList('Extremely satisfied', '1', '', answers_list);
                    AddOptionToList('Very satisfied', '2', '', answers_list);
                    AddOptionToList('Moderately satisfied', '3', '', answers_list);
                    AddOptionToList('Slightly satisfied', '4', '', answers_list);
                    AddOptionToList('Not at all satisfied', '5', '', answers_list);
                break;

                // Net Promoter Score
                case '9':
                    AddOptionToList('0 Not at all satisfied', '0', '', answers_list);
                    AddOptionToList('1', '1', '', answers_list);
                    AddOptionToList('2', '2', '', answers_list);
                    AddOptionToList('3', '3', '', answers_list);
                    AddOptionToList('4', '4', '', answers_list);
                    AddOptionToList('5', '5', '', answers_list);
                    AddOptionToList('6', '6', '', answers_list);
                    AddOptionToList('7', '7', '', answers_list);
                    AddOptionToList('8', '8', '', answers_list);
                    AddOptionToList('9', '9', '', answers_list);
                    AddOptionToList('10 Extremely satisfied', '10', '', answers_list);

                break;

                default:


                break;

            }
        });

        // Render / Add each item to the list from Template
        function AddOptionToList(inpOptionText, inpOptionAval, inpOptionAvalReg, inpCPE)
        {

            //Create a jquery reference to the returned HTML and store it in 'CPE' --->
            var inpAnswerItem = $(AnswerItemjsVar);

            //Set OPTION text
            inpAnswerItem.find("#OPTION").val(inpOptionText);
            inpAnswerItem.find("#AVAL").val(inpOptionAval).closest(".col-xs-2").addClass("hidden");
            inpAnswerItem.find("#AVALREG").val(inpOptionAvalReg);


            //Apend to list
            //$('#'+inpCPE).append(inpAnswerItem);

            $(inpCPE).append(inpAnswerItem);
        }

    var ShortCode= $("#ShortCode").val();
    var newStatementHtml = new EJS({url: '/session/sire/assets/pages/views/new-statement.ejs'}).render({ShortCode: ShortCode}),
        newIntervalHtml  = new EJS({url: '/session/sire/assets/pages/views/new-interval.ejs'}).render();
        newIntervalHtmlTemplate8 = new EJS({url: '/session/sire/assets/pages/views/new-interval-template-8.ejs'}).render();
        newOpenEnd  = new EJS({url: '/session/sire/assets/pages/views/new-openend.ejs'}).render({ShortCode: ShortCode});
        newOneSelection  = new EJS({url: '/session/sire/assets/pages/views/new-oneselection.ejs?v=1'}).render( {selectedVal : "0"},{ShortCode: ShortCode});

    $(document).ready(function() {

        if(inpTemplateId == 8)
        {
            //
            if(inpCampaignType == 0 || inpCampaignType == 9)
                //var allStatement = $(".control-point[data-control-point-type='STATEMENT']").not(':first').not(':last');
                var allStatement = $(".control-point[data-control-point-type='STATEMENT']").not(':first').find(".form-gd .form-group");
            else
                var allStatement = $(".control-point[data-control-point-type='STATEMENT']").not(':first').find(".form-gd .form-group");

            if(allStatement.length > 0)
            {
                allStatement.each(function(index, el){
                    //var div = $(el).find('div.row')[0];
                    var html='<div class="row"> <div class="col-sm-7"><a href="##" class="btn green-gd add-plus add-another-append non-mb set-margin-10">add another message +</a><a href="##" class="btn blue-gd btn-remove-cp-interval set-margin-10">REMOVE</a></div></div>';
                    $(el).append(html);
                });
            }

            var firstStatement = $(".control-point[data-control-point-type='STATEMENT']:first").find(".form-gd .form-group");
            var html = '<div class="row"> <div class="col-md-7">'+
                                    '<a href="##" class="btn green-gd add-plus add-another-append set-margin-10">add another message +</a>'+
                                '</div> </div>';
            firstStatement.append(html);


        }

        $(document.body).on("change",".cp_type",function(e){
            e.preventDefault();
            var that =  $(this);
            that.closest('.control-point').find('.see-unicode-mess').addClass('hidden');

            var parents = $(this).parents('.control-point');
            var idquestion = parents.attr('id');
            var valueoldtemplate = $('#'+idquestion+' .control-point textarea:first-child').val();

            var oldSelected = $(this).attr('data-cp-type');
            var selectValue = this.value;
            that.attr('data-cp-type',selectValue);

            changeCPType(that,selectValue);

            //
            displayQuestionNum();

        });

        function changeCPType(obj,type){
            var content = obj.parents('.control-point').find('.cp-content');
            var currentContentHere= $(obj).closest('.control-point').find("textarea").val();

            content.html('');


            if(type == 'SHORTANSWER'){
                $(obj).closest('.cpedit').find('.control-point').attr('data-control-point-type','SHORTANSWER');
                content.html(newOpenEnd);
            }
            else if(type == 'ONESELECTION'){
                $(obj).closest('.cpedit').find('.control-point').attr('data-control-point-type','ONESELECTION');
                var selected_answer_template = $(obj).find('option:selected').data('anstemp');
                var newOneSelection  = new EJS({url: '/session/sire/assets/pages/views/new-oneselection.ejs?v=1'}).render({ selectedVal : selected_answer_template },{ShortCode: ShortCode});
                content.html(newOneSelection);
                // filter anwser list
                var answers_list = content.find('.answers_list');
                answers_list.html('');


                $(obj).attr('data-anstemp',selected_answer_template);
                switch(selected_answer_template)
                {
                    // Yes or No
                    case 1:
                        AddOptionToList('Yes', 'Yes', '(?i)^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|in|ye|yas', answers_list);
                        AddOptionToList('No', 'No', '(?i)^n$|no|non|ne|nyet|no way|negative|^0$|out', answers_list);
                    break;

                    // True or False
                    case 2:
                        AddOptionToList('True', '1', '', answers_list);
                        AddOptionToList('False', '0', '', answers_list);
                    break;

                    // Agree or Disagree
                    case 3:
                        AddOptionToList('Strong Disagree', '1', '', answers_list);
                        AddOptionToList('Disagree', '2', '', answers_list);
                        AddOptionToList('Neither Disagree or Agree', '3', '', answers_list);
                        AddOptionToList('Agree', '4', '', answers_list);
                        AddOptionToList('Strong Agree', '5', '', answers_list);
                    break;

                    // Useful or Not
                    case 4:
                        AddOptionToList('Not useful', '1', '', answers_list);
                        AddOptionToList('Somewhat useful', '2', '', answers_list);
                        AddOptionToList('Useful', '3', '', answers_list);
                        AddOptionToList('Mostly useful', '4', '', answers_list);
                        AddOptionToList('Very useful', '5', '', answers_list);
                    break;

                    // Easy or Not
                    case 5:
                        AddOptionToList('Not easy', '1', '', answers_list);
                        AddOptionToList('Somewhat easy', '2', '', answers_list);
                        AddOptionToList('Easy', '3', '', answers_list);
                        AddOptionToList('Mostly easy', '4', '', answers_list);
                        AddOptionToList('Extremely easy', '5', '', answers_list);
                    break;

                    // Likely or Not
                    case 6:
                        AddOptionToList('Extremely likely', '1', '', answers_list);
                        AddOptionToList('Very likely', '2', '', answers_list);
                        AddOptionToList('Moderately likely', '3', '', answers_list);
                        AddOptionToList('Slightly likely', '4', '', answers_list);
                        AddOptionToList('Not at all likely', '5', '', answers_list);
                    break;

                    // Important or Not
                    case 7:
                        AddOptionToList('Extremely important', '1', '', answers_list);
                        AddOptionToList('Very important', '2', '', answers_list);
                        AddOptionToList('Moderately important', '3', '', answers_list);
                        AddOptionToList('Slightly important', '4', '', answers_list);
                        AddOptionToList('Not at all important', '5', '', answers_list);
                    break;

                    // Satisfied or Not
                    case 8:
                        AddOptionToList('Extremely satisfied', '1', '', answers_list);
                        AddOptionToList('Very satisfied', '2', '', answers_list);
                        AddOptionToList('Moderately satisfied', '3', '', answers_list);
                        AddOptionToList('Slightly satisfied', '4', '', answers_list);
                        AddOptionToList('Not at all satisfied', '5', '', answers_list);
                    break;

                    // Net Promoter Score
                    case 9:
                        AddOptionToList('0 Not at all satisfied', '0', '', answers_list);
                        AddOptionToList('1', '1', '', answers_list);
                        AddOptionToList('2', '2', '', answers_list);
                        AddOptionToList('3', '3', '', answers_list);
                        AddOptionToList('4', '4', '', answers_list);
                        AddOptionToList('5', '5', '', answers_list);
                        AddOptionToList('6', '6', '', answers_list);
                        AddOptionToList('7', '7', '', answers_list);
                        AddOptionToList('8', '8', '', answers_list);
                        AddOptionToList('9', '9', '', answers_list);
                        AddOptionToList('10 Extremely satisfied', '10', '', answers_list);
                    break;
                    // custom
                     case 0:
                        AddOptionToList('', '1', '', answers_list);
                        AddOptionToList('', '2', '', answers_list);
                        AddOptionToList('', '3', '', answers_list);
                        AddOptionToList('', '4', '', answers_list);
                        AddOptionToList('', '5', '', answers_list);

                    break;

                    default:


                    break;

                }
                //

                content.find('.ans-group').each(function(id,el){
                    var id = $(el).attr('id');
                    var ran = Math.floor((Math.random() * 100) + 1);
                    id = id+'-'+ran;
                    $(el).attr('id',id);
                    $(el).next('.lbl-ans-group').attr('id',id);
                    $(el).next('.lbl-ans-group').attr('for',id);
                });



                //currentContent
                CampaignComponents.init();
            }
            content.find('.Select2').select2({
                theme: "bootstrap", width: "auto"
            });
            //xxxxxxxxxxxxxxxxxxxxxxxx


            // Initializes and creates emoji set from sprite sheet
            emojiPicker3 = new EmojiPicker({
              emojiable_selector: '[data-emojiable=true]',
              assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
              popupButtonClasses: 'fa fa-smile-o'
            });
            // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
            // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
            // It can be called as many times as necessary; previously converted input fields will not be converted again
            emojiPicker3.discover();

            content.find("textarea").val(currentContentHere);
            content.find(".span_text").html(currentContentHere);

            BindCharacterCountAllPage();
        }
    });

    $('body').on('click', '.add-another-append', function(event) {

        event.preventDefault();
        var el = $(this);
        var thisCP = el.parents(".control-point");
        // var currentRQID = thisCP.data('control-rq-id');
        var currentRQID = thisCP.attr('data-control-rq-id');
        var currentPhysicalId=  thisCP.attr('data-control-point-physical-id');
        if(inpTemplateId==8)
        {
            var newInterval = $(newIntervalHtmlTemplate8);
            for (var i = 0; i <= 60; i++) {
                newInterval.find('.IVALUE').append('<option value="'+i+'">'+i+'</option>');
            }
            newInterval.find('.datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        }
        else
        {
            var newInterval = $(newIntervalHtml);
            newInterval.find('.IVALUE').html('');
            newInterval.find('.ITIME').html('');
            newInterval.find('.IMONTH').html('<option value="">Month</option>');
            newInterval.find('.IYEAR').html('<option value="">Year</option>');
            newInterval.find('.IDAY').html('<option value="">Day</option>');


            for (var i = 0; i <= 60; i++) {
                newInterval.find('.IVALUE').append('<option value="'+i+'">'+i+'</option>');
            }
            for (var i = 0; i < 24; i++) {
                for (var j = 0; j < 60; j += 15) {
                    var time = (i < 10 ? "0"+i : i) + ":" + (j < 10 ? "0"+j : j);
                    newInterval.find('.ITIME').append('<option value="'+time+'">'+time+'</option>');
                }
            }
            for (var i = 1; i <= 31; i++) {
                newInterval.find('.IDAY').append('<option value="'+i+'">'+i+'</option>');
            }
            for (var i = 1; i <= 12; i++) {
                newInterval.find('.IMONTH').append('<option value="'+i+'">'+i+'</option>');
            }
            var thisYear = new Date();
            thisYear = thisYear.getFullYear();
            for (var i = 0; i <= 10; i++) {
                newInterval.find('.IYEAR').append('<option value="'+(thisYear+i)+'">'+(thisYear+i)+'</option>');
            }

        }

        newInterval.find('#selected-hour-select').val('1');


        $(newStatementHtml).insertAfter(thisCP).promise().done(function(ele){
            ele.find('[data-control="text-message"]').val((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
            ele.find('div.emoji-wysiwyg-editor').text((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
            ele.find(".span_text").html((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
        });

        // Initializes and creates emoji set from sprite sheet
        var emojiPicker1 = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        emojiPicker1.discover();

        newInterval.insertAfter(thisCP).promise().done(function(ele){
            ele.find('[data-control="text-message"]').val((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
            ele.find('div.emoji-wysiwyg-editor').text((inpCompanyName.val()!==""? inpCompanyName.val() :"Company Name") +":");
            //ele.append('<div class="row"><div class="col-sm-12"><a href="##" class="btn  green-gd add-plus add-another-append non-mb">add another message +</a><a href="##" class="btn  blue-gd btn-remove-cp-interval">REMOVE</a></div></div>')
        });

        thisCP.nextAll(".control-point").each(function(index, el) {
            $(el).attr('data-control-rq-id', ++currentRQID);
            $(el).attr('data-control-point-physical-id', ++currentPhysicalId);
        });

        newInterval.find('.Select2').select2({
            theme: "bootstrap", width: "auto"
        });



        BindCharacterCountAllPage();
        countMgs();


    });

    $('body').on('click', '.btn-remove-cp-interval', function(event) {
        event.preventDefault();
        var el = $(this);
        var thisCP = el.parents(".control-point");
        var prevCP = thisCP.prev(".control-point[data-control-point-type='INTERVAL']");
        var currentRQID = prevCP.attr('data-control-rq-id');
        var currentPhysicalId=  prevCP.attr('data-control-point-physical-id');

        checkClickPreview = 0;

        //var currentRQID = prevCP.attr('data-control-rq-id');
        thisCP.nextAll(".control-point").each(function(index, el) {
            $(el).attr('data-control-rq-id', currentRQID++);
            $(el).attr('data-control-point-physical-id', currentPhysicalId++);
        });
        if (prevCP.data("add-new") == "Edit") {
            intListDeleteRID.push(prevCP.data("control-point-physical-id"));
        }
        if (thisCP.data("add-new") == "Edit") {
            intListDeleteRID.push(thisCP.data("control-point-physical-id"));
        }
        prevCP.remove();
        thisCP.remove();

        BindCharacterCountAllPage();
        countMgs();
    });

    $(document).ready(function() {

        if (typeof OPTIN_GROUPID != 'undefined') {
            $("#SubscriberList").val(OPTIN_GROUPID);
        }

        $(".Select2").select2(
            { theme: "bootstrap", width: 'auto'}
        );

        $('.Select2').removeClass('hidden');

        if(action == 'CreateNew'){
            inpCompanyName.trigger('input');
        }

        $('.datetimepicker').datetimepicker();
    });
    if(action == 'CreateNew'){
        var textControls = $('[data-control="text-message"]');

        inpCompanyName.delayOn('input', 1000, function(ele, event){
            var value = $(ele).val();
             if(value=="")
            {
                value="Company Name";
            }

            $('div.emoji-wysiwyg-editor').each(function(){
                var divBox = $(this);
                var content = divBox.text();
                var posStart = content.indexOf(":") > -1 ? content.indexOf(":") + 1 : 0;
                content = value+":"+content.substring(posStart).trimFirstSpace();
                divBox.text(content);
            });
            if(textControls.length){
                textControls.each(function(index){
                    var textBox = $(this);
                    var content = textBox.val();
                    var posStart = content.indexOf(":") > -1 ? content.indexOf(":") + 1 : 0;
                    content = value+":"+content.substring(posStart).trimFirstSpace();
                    textBox.val(content).trigger('input');
                });
            }
        });
    }
    else{
        if(inpCampaignType == 1){
            var selectSub = $('#SubscriberBlastList').val();
            var totalContactString = 0;
            if($.isArray(selectSub)){
                if(selectSub.length > 0){
                    $.each( selectSub, function( key, value ) {
                        var val = value;
                        var i;
                        for (i = 0; i < subcriberList.length; ++i) {
                            if(val == subcriberList[i][0])
                            {
                                totalContactString+=subcriberList[i][2]
                            }
                        }
                    });
                }
            }
            $(".totalSubcriberSelected").text(parseInt(totalContactString));
        }

        countMgs();
    }

    // $('body').on('DOMNodeInserted', '.Select2', function () {
    //     $(this).select2({ theme: "bootstrap", width: 'auto'});
    // });

    $("#tellmgr-sms-checkbox").change(function(event) {
        if (!this.checked) {
            var firstSMSInput = $('.sms-number-item').first();
            firstSMSInput.siblings('.sms-number-item').remove();
            firstSMSInput.find('input').val('');
            //firstSMSInput.find('input').prop('disabled', 'true');
            $(".sms-number-wrap").addClass('noMinus');
        } else {
            var firstSMSInput = $('.sms-number-item').first();
            //firstSMSInput.find('input').prop('disabled', false);
            $(".sms-number-wrap").removeClass('noMinus');
            $(".sms-number-wrap").removeClass('hasMinus');
        }
    });

    $("#tellmgr-email-checkbox").change(function(event) {
        if (!this.checked) {
            var firstEmailInput = $('.email-add-item').first();
            firstEmailInput.siblings('.email-add-item').remove();
            firstEmailInput.find('input').val('');
            //firstEmailInput.find('input').prop('disabled', 'true');
            $(".email-add-wrap").addClass('noMinus');
        } else {
            var firstEmailInput = $('.email-add-item').first();
            //firstEmailInput.find('input').prop('disabled', false);
            $(".email-add-wrap").removeClass('noMinus');
            $(".email-add-wrap").removeClass('hasMinus');
        }
    });
    //input-list-phone,input-list-mail
    $(document).on("keyup",".input-list-phone",function(){
        if($(this).val().length > 0 && $("#tellmgr-sms-checkbox").is(":checked")==false )
        {
            $("#tellmgr-sms-checkbox").prop("checked",true).trigger("change");
        }
        else
        {
            //$("#tellmgr-sms-checkbox").prop("checked",false).trigger("change");
        }
    });
    $(document).on("keyup",".input-list-mail",function(){
        if($(this).val().length > 0 && $("#tellmgr-email-checkbox").is(":checked")==false)
        {
            $("#tellmgr-email-checkbox").prop("checked",true).trigger("change");
        }
        else
        {
            //$("#tellmgr-email-checkbox").prop("checked",false).trigger("change");
        }
    });

    // $(document).on("select2:select", function (e) {


    // });

    $('body').on('select2:select select2:unselect', '#SubscriberBlastList', function(event) {
        if(inpCampaignType == 1 || inpCampaignType == 9 ){
            var selectSub = $(this).val();
            var totalContactString = 0;
            if($.isArray(selectSub)){
                if(selectSub.length > 0){
                    $.each( selectSub, function( key, value ) {
                        var val = value;
                        var i;
                        for (i = 0; i < subcriberList.length; ++i) {
                            if(val == subcriberList[i][0])
                            {
                                totalContactString+=subcriberList[i][2]
                            }
                        }
                    });
                }
            }

            $(".totalSubcriberSelected").text(parseInt(totalContactString));
        }

    });
    $('body').on('click', '.campaign-template .corner', function(event) {
        var _this= this;
        var modalBox=$("#mdLearmore-"+$(this).data("template-id")) ;
        var img_link= modalBox.find(".load-back").data("img");
        modalBox.find(".load-back").attr("src", img_link);

        modalBox.modal("show");
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

        }
        else
        {
            $(this).css({"width": "70px", "height": "70px"});
            $(this).find(".retangle__learnmore").css({"opacity":"100"});
        }

        //
        $(modalBox).on('hidden.bs.modal', function () {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

            }
            else
            {
                $(_this).removeAttr("style");
                $(_this).find(".retangle__learnmore").removeAttr("style");
            }
        })
    });


});
