(function(){
	$('.tick-select').click(function(){
		if( $(this).children().hasClass('fa-square-o') ){
			$(this).children().removeClass('fa-square-o');
			$(this).children().addClass('fa-check-square-o');
		} else if( $(this).children().hasClass('fa-check-square-o') ){
			$(this).children().removeClass('fa-check-square-o');
			$(this).children().addClass('fa-square-o');
		}
	});
})();