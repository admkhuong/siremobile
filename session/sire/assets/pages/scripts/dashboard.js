(function() {
    // var dashboad = function() {
    //     return {
    //         //main function to initiate the module
    //         init_archive_shorturl: function() {
    //             //knob does not support ie8 so skip it
    //             if (!jQuery().knob || App.isIE8()) {
    //                 return;
    //             }

    //             // general dialStep
    //             $("#archive-shorturl").knob({
    //                 width: 230,
    //                 height: 230,
    //                 bgColor: '#74c37f',
    //                 fgColor: '#5c5c5c',
    //                 readOnly: true,
    //                 displayInput: false
    //             });

    //             animateChart('archive-shorturl', 0, $('#shorturl-percent-complete').val());

    //             /* Update circle value when input.animate changes */
    //             $("#archive-shorturl").change(function() {
    //                 $(this).parents('.archive-shorturl-content').find(".pie_value").text($(this).val());
    //             });

    //             /* Animate chart from start_val to end_val */
    //             function animateChart(chart_id, start_val, end_val) {
    //                 $({ chart_value: start_val }).animate({ chart_value: end_val }, {
    //                     duration: 1000,
    //                     easing: 'swing',
    //                     step: function() {
    //                         $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
    //                     }
    //                 });
    //             }
    //         },
    //         mlps : function(){
    //             //knob does not support ie8 so skip it
    //             if (!jQuery().knob || App.isIE8()) {
    //                 return;
    //             }

    //             // general dialStep
    //             $("#mlps").knob({
    //                 width: 230,
    //                 height: 230,
    //                 bgColor: '#578ca5',
    //                 fgColor: '#5c5c5c',
    //                 readOnly: true,
    //                 displayInput: false
    //             });

    //             animateChart('mlps', 0, $('#mlps-percent-complete').val());

    //             /* Update circle value when input.animate changes */
    //             $("#mlps").change(function() {
    //                 $(this).parents('.mlps-content').find(".pie_value").text($(this).val());
    //             });

    //             /* Animate chart from start_val to end_val */
    //             function animateChart(chart_id, start_val, end_val) {
    //                 $({ chart_value: start_val }).animate({ chart_value: end_val }, {
    //                     duration: 1000,
    //                     easing: 'swing',
    //                     step: function() {
    //                         $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
    //                     }
    //                 });
    //             };
    //         },
    //         init: function() {
    //             this.init_archive_shorturl();
    //             this.mlps();
    //         }
    //     };

    // }();

    // dashboad.init();




})();

(function($){

    /* Waitlist datatable initializer */
    function GetActivityList(strFilter){

        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var table = $('#activityList').dataTable({
            "bStateSave": true,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 20,
            "bAutoWidth": false,
            "bSortable": false,
            "sAjaxDataProp": "ActivityList",
            "sSortDir_0": "desc",
            "aoColumns": [
                {"sName": 'ID', "sTitle": 'ID',"bSortable": false,"sClass":"camp-name", "sWidth": "70px"},
                {"sName": 'Campaign Name', "sTitle": 'Campaign Name',"bSortable": false,"sClass":"camp-name", "sWidth": "160px"},
                {"sName": 'List', "sTitle": 'List',"bSortable": false,"sClass":"camp-name", "sWidth":"120px"},
                {"sName": 'Template', "sTitle": 'Template',"bSortable": false,"sClass":"camp-name", "sWidth":"125px"},
                {"sName": 'SMS', "sTitle": 'SMS',"bSortable": false,"sClass":"camp-name", "sWidth":"150px"},
                {"sName": 'SMS', "sTitle": 'Created',"bSortable": false,"sClass":"camp-name", "sWidth":"70px"}
            ],
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetCampaignReport&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }

                /* hide paginate section if table is empty */
                $('#activityList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#activityList_paginate').hide();
                        return false;
                    }
                    else{
                        $('#activityList_paginate').show();
                    }
                });

                $('[data-toggle="popover"]').popover();
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {

                if (aoData[3].value < 0){
                    aoData[3].value = 0;
                }
                aoData.push(
                    { "name": "customFilter", "value": customFilterData},
                    { "name": "isFirstLoad", "value": 1}

                    );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                $('[data-toggle="popover"]').popover();
                $('#activityList_paginate').hide();
            }
        });
    }

    function GetMoreActivityList(strFilter){

        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


        var table = $('#activityList').dataTable({
            "bStateSave": true,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "bSortable": false,
            "sAjaxDataProp": "ActivityList",
            "sSortDir_0": "desc",
            "aoColumns": [
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false}
            ],
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetCampaignReport&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }

                /* hide paginate section if table is empty */
                $('#activityList > tbody').find('tr').each(function(){
                    var tdFirst = $(this).find('td:first');
                    if(tdFirst.hasClass('dataTables_empty')){
                        $('#activityList_paginate').hide();
                        return false;
                    }
                    else{
                        $('#activityList_paginate').show();
                    }
                });

                $('[data-toggle="popover"]').popover();
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {

                if (aoData[3].value < 0){
                    aoData[3].value = 0;
                }
                aoData.push(
                    { "name": "customFilter", "value": customFilterData},
                    { "name": "isFirstLoad", "value": ""}

                    );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data){
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                $('[data-toggle="popover"]').popover();
                $('.see-more').hide();
                $('.tbl-activity-feed').css('height','600px !important');
            }
        });
    }

    function CountActivityFeed(){
        $.ajax({
            url: '/session/sire/models/cfc/reports/dashboard.cfc?method=CountActivityFeed&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function(data){
                if (data.COUNT <= 3) {
                    $('.see-more').hide();
                }
                else if(data.RXRESULTCODE != 1){
                    //console.log(data);
                }
            },
            error: function(e){
                //console.log(e);
            }
        });

    }

    CountActivityFeed();

    $('.see-more').on('click', function(){
        GetMoreActivityList();
    });
    // slide show
    function GetSlideShowStatus()
	{
		return new Promise( function (resolve, reject) {
            var data ={
                MESSAGE:"",
                RXRESULTCODE:1,
                STATUS:1
            };
            var status=1;
            var variableStatus= userId+'SlideStatus';
            if (localStorage) {
                // LocalStorage is supported!
                status = localStorage.getItem(variableStatus);
            } else {
                // No support. Use a fallback such as browser cookies or store on the server.
                status= getCookie(variableStatus);
            }
            if(status !=undefined &&  status != null)
            {
                data.STATUS=status;
            }
            resolve(data);
		});
    }
    function UpdateSlideShowStatus(status)
	{
		return new Promise( function (resolve, reject) {
            var data ={
                MESSAGE:"",
                RXRESULTCODE:1,
                STATUS:1
            };
			var variableStatus= userId+'SlideStatus';
            if (localStorage) {
                // LocalStorage is supported!
                localStorage.setItem(variableStatus, status);
            } else {
                // No support. Use a fallback such as browser cookies or store on the server.
                document.cookie = variableStatus+"="+status;
            }
            resolve(data);
		});
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    //
    $(document).ready(function() {
        /* when document is ready */

        /* init waitlist datatable */
        GetActivityList();

        /* fix body shrink issue after a message model disappear */
        if(firstLogin == 1){
                $(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });
                // integrate popup
                GetSlideShowStatus().then(function(data){
                    if(data.RXRESULTCODE==1 && data.STATUS==1){
                        $("#mdIntegrate-0").modal("show");
                    }
                }).catch(function(data){

            });
        }


        $(document).on("click",".btn-itegrate",function(){
            var id= $(this).data("id") ;
            var nextId= id +1;
            $("#mdIntegrate-"+id).modal("hide");
            $("#mdIntegrate-"+nextId).modal("show");
        });
        $(document).on("click",".md-chk-remember",function(){
            var status=1;
            if($(this).is(":checked")){
                status=0;
            }
            else
            {
                status=1;
            }
            UpdateSlideShowStatus(status).then(function(data){
                if(data.RXRESULTCODE==1){
                    if(status==1){
                        $(".md-chk-remember").prop('checked', false);

                    }
                    else
                    {
                        $(".md-chk-remember").prop('checked', true);
                    }
                }
            });
        });



    });

    var needToShowModal = 0;
    if ($('#custom-stop-message').val() == '' || $('#custom-help-message').val() == '' || $('#company-name').val() == ''){
        needToShowModal = 1;
    }

    /* var showCompleteProfileModal = Cookies.get('CompleteProfileModal');
    if (showCompleteProfileModal != 0 && needToShowModal == 1){
        $('#modal-complete-profile').modal('show');
        Cookies.set('CompleteProfileModal', 0, { expires: 7, path: '/session/sire/pages/'});
    } */

})(jQuery);

(function($){

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    function InitKeywordListDS(keyword){

        var keyword = typeof(keyword)!='undefined'?keyword:"";

        var table = $('#keywordListDetail').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 20,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "KEYWORD", "sName": 'Keyword', "sTitle": 'Name', "bSortable": false, "sClass": "keyword-table-detail","sWidth": "80px"},
                {"mData": "TIMEUSED", "sName": 'Number of Times Used', "sTitle": 'Times Used', "bSortable": false, "sClass": "keyword-table-detail times-used-detail","sWidth": "90px"}
            ],
            "sAjaxDataProp": "LISTKEYWORD",
            "sAjaxSource": '/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserId'+strAjaxQuery,
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpKeyword", "value": keyword},
                    {"name": "isDashboard", "value": 1}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                $('#keywordListDetail_paginate').hide();
            }
        });
    }


    InitKeywordListDS();

    var _tblListEMS;
    function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
        var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        //init datatable for active agent
        _tblListEMS = $('#tblListEMS').dataTable({
            "bStateSave": true,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {
            },
            "fnStateSaveParams": function (oSettings, oData) {
            },


            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 20,
            "bAutoWidth": false,
            "aoColumns": [
                {"sName": 'name', "sTitle": 'Name',"bSortable": false,"sClass":"camp-name", "sWidth": "180px"},
                {"sName": 'template', "sTitle": 'Template Type',"bSortable": false,"sClass":"camp-name", "sWidth": "160px"},
                {"sName": 'keyword', "sTitle": 'Keyword',"bSortable": false,"sClass":"camp-name", "sWidth":"140px"},
            ],
            "sAjaxDataProp": "ListEMSData",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetCampaignListForDashboard&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            },
           "fnDrawCallback": function( oSettings ) {
              // MODIFY TABLE
                //$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }

            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
                if (aoData[3].value < 0){
                    aoData[3].value = 0;
                }
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                $.ajax({dataType: 'json',
                         type: "POST",
                         url: sSource,
                         data: aoData,
                         success: function (data) {
                            fnCallback(data);
                         }
                });
            },
            "fnInitComplete":function(oSettings, json){
                $('#tblListEMS_paginate').hide();
            }
        });

    }

    function IsMobileTablet(){//check if web page is opened on iOS or Android OS device
        var md = new MobileDetect(window.navigator.userAgent);
        if(md.os() == "iOS" || md.os() == "AndroidOS"){
            return true;
        }
        else{
            return false;
        }
    }

    InitControl();


    $(window).on('resize load', function(e){

        $.ajax({
            type: "POST",
            url: '/session/sire/models/cfc/reports/opt.cfc?method=ConsoleSummaryOptCounts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType: 'json',
            data:
            {
            },
            success: function(d)
            {
                var window_with = $( window ).width();
                var set_height = 220;
                var set_distop = 20;
                var needReverseData = false;

                if (IsMobileTablet() && window_with > 1368){
                    set_height = 325;
                    set_distop = 20;
                }
                else if (!IsMobileTablet() && window_with > 1351){
                    set_height = 325;
                    set_distop = 20;
                }



                if (d.RXRESULTCODE == 1)
                {

                    new Chartist.Bar('#opt-rate', {
                      labels: [''],
                      series: [
                        [d.OPTOUTTOTALCOUNT],
                        [d.OPTINTOTALCOUNT]
                      ]
                    },{stackBars: true, axisX:{showGrid: false, showLabel: false}, axisY: {showGrid: false, showLabel: false}, height: set_height}).on('draw', function(data) {
                      if(data.type === 'bar') {
                        data.element.attr({
                          style: 'stroke-width: 40px'
                        });
                      }
                    });

                }
                else{
                    //console.log(d);
                }

            },
            error: function(e){
                //console.log(e);
            }

        });

        $.ajax({
            url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetMLPShortURL"+strAjaxQuery,
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function(data){
                if(data.RXRESULTCODE == 1){
                    var setHeight1 = "150px";
                    var setHeight2 = "170px";
                    var shortUrlChartHeight;
                    var mlpViewChartHeight;
                    var shortUrlClick;
                    var mlpView;

                    var window_with = $( window ).width();
                    var set_font = "18px";
                    var set_height;
                    var set_distop = 0;

                    shortUrlClick = data.TOTALCLICKSHORTURL;
                    mlpView = data.MLPVIEWS;
                    if ((shortUrlClick >= mlpView && IsMobileTablet() && window_with > 1368) || (shortUrlClick >= mlpView && !IsMobileTablet() && window_with > 1351)){
                        set_height = setHeight2;
                    }
                    else if ((shortUrlClick >= mlpView && IsMobileTablet() && window_with <= 1368) || (shortUrlClick >= mlpView && !IsMobileTablet() && window_with <= 1351)){
                        set_height = setHeight1;
                    }
                    else if ((shortUrlClick < mlpView && IsMobileTablet() && window_with > 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with > 1351)){
                        set_height = Math.ceil(180*shortUrlClick/mlpView) + "px";
                    }
                    else if ((shortUrlClick < mlpView && IsMobileTablet() && window_with <= 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with <= 1351)){
                        set_height = Math.ceil(160*shortUrlClick/mlpView) + "px";
                    }

                    if (IsMobileTablet() && window_with > 1368){
                        set_font = "26px";
                        set_distop = 0;
                    }
                    else if (!IsMobileTablet() && window_with > 1351){
                        set_font = "26px";
                        set_distop = 0;
                    }

                    var defaultOptions = {
                        // Options for X-Axis
                        axisX: {
                            // The offset of the chart drawing area to the border of the container
                            offset: set_distop,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'end',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: true,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum width in pixel of the scale steps
                            scaleMinSpace: 30,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                        },
                        // Options for Y-Axis
                        axisY: {
                        // The offset of the chart drawing area to the border of the container
                            offset: 5,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'start',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: false,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum height in pixel of the scale steps
                            scaleMinSpace: 20,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                            },
                        // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
                        width: undefined,
                        // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
                        height: set_height,
                        // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
                        high: undefined,
                        // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
                        low: undefined,
                        // Unless low/high are explicitly set, bar chart will be centered at zero by default. Set referenceValue to null to auto scale.
                        referenceValue: 0,
                        // Padding of the chart drawing area to the container element and labels as a number or padding object {top: 5, right: 5, bottom: 5, left: 5}
                        chartPadding: {
                            top: 0,
                            right: 15,
                            bottom: 5,
                            left: 10
                        },
                        // Specify the distance in pixel of bars in a group
                        seriesBarDistance: 15,
                        // If set to true this property will cause the series bars to be stacked. Check the `stackMode` option for further stacking options.
                        stackBars: false,
                        // If set to 'overlap' this property will force the stacked bars to draw from the zero line.
                        // If set to 'accumulate' this property will form a total for each series point. This will also influence the y-axis and the overall bounds of the chart. In stacked mode the seriesBarDistance property will have no effect.
                        stackMode: 'accumulate',
                        // Inverts the axes of the bar chart in order to draw a horizontal bar chart. Be aware that you also need to invert your axis settings as the Y Axis will now display the labels and the X Axis the values.
                        horizontalBars: false,
                        // If set to true then each bar will represent a series and the data array is expected to be a one dimensional array of data values rather than a series array of series. This is useful if the bar chart should represent a profile rather than some data over time.
                        distributeSeries: true,
                        // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
                        reverseData: false,
                        // If the bar chart should add a background fill to the .ct-grids group.
                        showGridBackground: false,
                        // Override the class names that get used to generate the SVG structure of the chart
                        classNames: {
                            chart: 'ct-chart-bar',
                            horizontalBars: 'ct-horizontal-bars',
                            label: 'ct-label',
                            labelGroup: 'ct-labels',
                            series: 'ct-series',
                            bar: 'ct-bar',
                            grid: 'ct-grid',
                            gridGroup: 'ct-grids',
                            gridBackground: 'ct-grid-background',
                            vertical: 'ct-vertical',
                            horizontal: 'ct-horizontal',
                            start: 'ct-start',
                            end: 'ct-end'
                        }
                    };

                    var mlpshorturlChart = new Chartist.Bar('#shorturl-chart', {
                      labels: [''],
                      series: [11]
                    }, defaultOptions);

                    // mlpshorturlChart.on('draw', function(chart_data) {
                    //     var barHorizontalCenter, barVerticalCenter, label, value;
                    //     if (chart_data.type === "bar") {
                    //         // barHorizontalCenter = chart_data.x1 + (chart_data.element.width() * .5);
                    //         barHorizontalCenter = chart_data.x1;
                    //         barVerticalCenter = chart_data.y1 + (chart_data.element.height() * -1) - 10;
                    //         // barVerticalCenter = chart_data.y1;

                    //         value = chart_data.element.attr('ct:value');
                    //         // if (value !== '0') {
                    //             label = new Chartist.Svg('text');
                    //             label.text(value);
                    //             label.addClass("ct-barlabel");
                    //             label.attr({
                    //                 x: barHorizontalCenter,
                    //                 y: barVerticalCenter,
                    //                 'text-anchor': 'middle'
                    //             });
                    //             return chart_data.group.append(label);
                    //         // }
                    //     }
                    // });
                }

                else {
                    //console.log(data);
                }
            },
            error: function(e){
                //console.log(e);
            }

        });

        $.ajax({
            url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetMLPShortURL"+strAjaxQuery,
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function(data){
                if(data.RXRESULTCODE == 1){
                    var setHeight1 = "150px";
                    var setHeight2 = "170px";
                    var shortUrlChartHeight;
                    var mlpViewChartHeight;
                    var shortUrlClick;
                    var mlpView;

                    var window_with = $( window ).width();
                    var set_font = "18px";
                    var set_height;
                    var set_distop = 0;

                    shortUrlClick = data.TOTALCLICKSHORTURL;
                    mlpView = data.MLPVIEWS;
                    if ((mlpView >= shortUrlClick && IsMobileTablet() && window_with > 1368) || (mlpView >= shortUrlClick && !IsMobileTablet() && window_with > 1351)){
                        set_height = setHeight2;
                    }
                    else if ((mlpView >= shortUrlClick && IsMobileTablet() && window_with <= 1368) || (mlpView >= shortUrlClick && !IsMobileTablet() && window_with <= 1351)){
                        set_height = setHeight1;
                    }
                    else if ((mlpView < shortUrlClick && IsMobileTablet() && window_with > 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with > 1351)){
                        set_height = Math.ceil(180*mlpView/shortUrlClick) + "px";
                    }
                    else if ((mlpView < shortUrlClick && IsMobileTablet() && window_with <= 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with <= 1351)){
                        set_height = Math.ceil(160*mlpView/shortUrlClick) + "px";
                    }

                    if (IsMobileTablet() && window_with > 1368){
                        set_font = "26px";
                        set_distop = 0;
                    }
                    else if (!IsMobileTablet() && window_with > 1351){
                        set_font = "26px";
                        set_distop = 0;
                    }

                    var defaultOptions = {
                        // Options for X-Axis
                        axisX: {
                            // The offset of the chart drawing area to the border of the container
                            offset: set_distop,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'end',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: true,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum width in pixel of the scale steps
                            scaleMinSpace: 30,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                        },
                        // Options for Y-Axis
                        axisY: {
                        // The offset of the chart drawing area to the border of the container
                            offset: 5,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'start',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: false,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum height in pixel of the scale steps
                            scaleMinSpace: 20,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                            },
                        // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
                        width: undefined,
                        // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
                        height: set_height,
                        // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
                        high: undefined,
                        // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
                        low: undefined,
                        // Unless low/high are explicitly set, bar chart will be centered at zero by default. Set referenceValue to null to auto scale.
                        referenceValue: 0,
                        // Padding of the chart drawing area to the container element and labels as a number or padding object {top: 5, right: 5, bottom: 5, left: 5}
                        chartPadding: {
                            top: 0,
                            right: 15,
                            bottom: 5,
                            left: 10
                        },
                        // Specify the distance in pixel of bars in a group
                        seriesBarDistance: 15,
                        // If set to true this property will cause the series bars to be stacked. Check the `stackMode` option for further stacking options.
                        stackBars: false,
                        // If set to 'overlap' this property will force the stacked bars to draw from the zero line.
                        // If set to 'accumulate' this property will form a total for each series point. This will also influence the y-axis and the overall bounds of the chart. In stacked mode the seriesBarDistance property will have no effect.
                        stackMode: 'accumulate',
                        // Inverts the axes of the bar chart in order to draw a horizontal bar chart. Be aware that you also need to invert your axis settings as the Y Axis will now display the labels and the X Axis the values.
                        horizontalBars: false,
                        // If set to true then each bar will represent a series and the data array is expected to be a one dimensional array of data values rather than a series array of series. This is useful if the bar chart should represent a profile rather than some data over time.
                        distributeSeries: true,
                        // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
                        reverseData: false,
                        // If the bar chart should add a background fill to the .ct-grids group.
                        showGridBackground: false,
                        // Override the class names that get used to generate the SVG structure of the chart
                        classNames: {
                            chart: 'ct-chart-bar',
                            horizontalBars: 'ct-horizontal-bars',
                            label: 'ct-label',
                            labelGroup: 'ct-labels',
                            series: 'ct-series',
                            bar: 'ct-bar',
                            grid: 'ct-grid',
                            gridGroup: 'ct-grids',
                            gridBackground: 'ct-grid-background',
                            vertical: 'ct-vertical',
                            horizontal: 'ct-horizontal',
                            start: 'ct-start',
                            end: 'ct-end'
                        }
                    };

                    var mlpshorturlChart = new Chartist.Bar('#mlp-chart', {
                      labels: [''],
                      series: [11]
                    }, defaultOptions);

                    // mlpshorturlChart.on('draw', function(chart_data) {
                    //     var barHorizontalCenter, barVerticalCenter, label, value;
                    //     if (chart_data.type === "bar") {
                    //         // barHorizontalCenter = chart_data.x1 + (chart_data.element.width() * .5);
                    //         barHorizontalCenter = chart_data.x1;
                    //         barVerticalCenter = chart_data.y1 + (chart_data.element.height() * -1) - 10;
                    //         // barVerticalCenter = chart_data.y1;

                    //         value = chart_data.element.attr('ct:value');
                    //         // if (value !== '0') {
                    //             label = new Chartist.Svg('text');
                    //             label.text(value);
                    //             label.addClass("ct-barlabel");
                    //             label.attr({
                    //                 x: barHorizontalCenter,
                    //                 y: barVerticalCenter,
                    //                 'text-anchor': 'middle'
                    //             });
                    //             return chart_data.group.append(label);
                    //         // }
                    //     }
                    // });
                }

                else {
                    //console.log(data);
                }
            },
            error: function(e){
                //console.log(e);
            }

        });



        // $.ajax({
        //     url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetMLPShortURL"+strAjaxQuery,
        //     type: 'POST',
        //     dataType: 'json',
        //     data: {},
        //     success: function(data){
        //         if(data.RXRESULTCODE == 1){
        //             var setHeight1 = "160px";
        //             var setHeight2 = "180px";
        //             var shortUrlChartHeight;
        //             var mlpViewChartHeight;
        //             var shortUrlClick;
        //             var mlpView;

        //             var window_with = $( window ).width();
        //             var set_font = "18px";
        //             var set_height;
        //             var set_distop = 0;

        //             shortUrlClick = 17;
        //             mlpView = 17;
        //             if ((mlpView >= shortUrlClick && IsMobileTablet() && window_with > 1368) || (mlpView >= shortUrlClick && !IsMobileTablet() && window_with > 1351)){
        //                 set_height = setHeight2;
        //             }
        //             else if ((mlpView >= shortUrlClick && IsMobileTablet() && window_with <= 1368) || (mlpView >= shortUrlClick && !IsMobileTablet() && window_with <= 1351)){
        //                 set_height = setHeight1;
        //             }
        //             else if ((mlpView < shortUrlClick && IsMobileTablet() && window_with > 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with > 1351)){
        //                 set_height = Math.ceil(200*mlpView/shortUrlClick) + "px";
        //             }
        //             else if ((mlpView < shortUrlClick && IsMobileTablet() && window_with <= 1368) || (shortUrlClick < mlpView && !IsMobileTablet() && window_with <= 1351)){
        //                 set_height = Math.ceil(180*mlpView/shortUrlClick) + "px";
        //             }

        //             console.log(set_height);

        //             if (IsMobileTablet() && window_with > 1368){
        //                 set_font = "26px";
        //                 set_distop = 0;
        //             }
        //             else if (!IsMobileTablet() && window_with > 1351){
        //                 set_font = "26px";
        //                 set_distop = 0;
        //             }

        //             var defaultOptions = {
        //                 // Options for X-Axis
        //                 axisX: {
        //                     // The offset of the chart drawing area to the border of the container
        //                     offset: set_distop,
        //                     // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
        //                     position: 'end',
        //                     // Allows you to correct label positioning on this axis by positive or negative x and y offset.
        //                     labelOffset: {
        //                     x: 0,
        //                     y: 0
        //                     },
        //                     // If labels should be shown or not
        //                     showLabel: true,
        //                     // If the axis grid should be drawn or not
        //                     showGrid: false,
        //                     // Interpolation function that allows you to intercept the value from the axis label
        //                     labelInterpolationFnc: Chartist.noop,
        //                     // This value specifies the minimum width in pixel of the scale steps
        //                     scaleMinSpace: 30,
        //                     // Use only integer values (whole numbers) for the scale steps
        //                     onlyInteger: false
        //                 },
        //                 // Options for Y-Axis
        //                 axisY: {
        //                 // The offset of the chart drawing area to the border of the container
        //                     offset: 5,
        //                     // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
        //                     position: 'start',
        //                     // Allows you to correct label positioning on this axis by positive or negative x and y offset.
        //                     labelOffset: {
        //                     x: 0,
        //                     y: 0
        //                     },
        //                     // If labels should be shown or not
        //                     showLabel: false,
        //                     // If the axis grid should be drawn or not
        //                     showGrid: false,
        //                     // Interpolation function that allows you to intercept the value from the axis label
        //                     labelInterpolationFnc: Chartist.noop,
        //                     // This value specifies the minimum height in pixel of the scale steps
        //                     scaleMinSpace: 20,
        //                     // Use only integer values (whole numbers) for the scale steps
        //                     onlyInteger: false
        //                     },
        //                 // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
        //                 width: undefined,
        //                 // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
        //                 height: set_height,
        //                 // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
        //                 high: undefined,
        //                 // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
        //                 low: undefined,
        //                 // Unless low/high are explicitly set, bar chart will be centered at zero by default. Set referenceValue to null to auto scale.
        //                 referenceValue: 0,
        //                 // Padding of the chart drawing area to the container element and labels as a number or padding object {top: 5, right: 5, bottom: 5, left: 5}
        //                 chartPadding: {
        //                     top: 0,
        //                     right: 15,
        //                     bottom: -2,
        //                     left: 10
        //                 },
        //                 // Specify the distance in pixel of bars in a group
        //                 seriesBarDistance: 15,
        //                 // If set to true this property will cause the series bars to be stacked. Check the `stackMode` option for further stacking options.
        //                 stackBars: false,
        //                 // If set to 'overlap' this property will force the stacked bars to draw from the zero line.
        //                 // If set to 'accumulate' this property will form a total for each series point. This will also influence the y-axis and the overall bounds of the chart. In stacked mode the seriesBarDistance property will have no effect.
        //                 stackMode: 'accumulate',
        //                 // Inverts the axes of the bar chart in order to draw a horizontal bar chart. Be aware that you also need to invert your axis settings as the Y Axis will now display the labels and the X Axis the values.
        //                 horizontalBars: false,
        //                 // If set to true then each bar will represent a series and the data array is expected to be a one dimensional array of data values rather than a series array of series. This is useful if the bar chart should represent a profile rather than some data over time.
        //                 distributeSeries: true,
        //                 // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
        //                 reverseData: false,
        //                 // If the bar chart should add a background fill to the .ct-grids group.
        //                 showGridBackground: false,
        //                 // Override the class names that get used to generate the SVG structure of the chart
        //                 classNames: {
        //                     chart: 'ct-chart-bar',
        //                     horizontalBars: 'ct-horizontal-bars',
        //                     label: 'ct-label',
        //                     labelGroup: 'ct-labels',
        //                     series: 'ct-series',
        //                     bar: 'ct-bar',
        //                     grid: 'ct-grid',
        //                     gridGroup: 'ct-grids',
        //                     gridBackground: 'ct-grid-background',
        //                     vertical: 'ct-vertical',
        //                     horizontal: 'ct-horizontal',
        //                     start: 'ct-start',
        //                     end: 'ct-end'
        //                 }
        //             };

        //             var mlpshorturlChart = new Chartist.Bar('#mlp-chart', {
        //               labels: [''],
        //               series: [12]
        //             }, defaultOptions);

        //             // mlpshorturlChart.on('draw', function(chart_data) {
        //             //     var barHorizontalCenter, barVerticalCenter, label, value;
        //             //     if (chart_data.type === "bar") {
        //             //         // barHorizontalCenter = chart_data.x1 + (chart_data.element.width() * .5);
        //             //         barHorizontalCenter = chart_data.x1;
        //             //         barVerticalCenter = chart_data.y1 + (chart_data.element.height() * -1) - 10;
        //             //         // barVerticalCenter = chart_data.y1;

        //             //         value = chart_data.element.attr('ct:value');
        //             //         // if (value !== '0') {
        //             //             label = new Chartist.Svg('text');
        //             //             label.text(value);
        //             //             label.addClass("ct-barlabel");
        //             //             label.attr({
        //             //                 x: barHorizontalCenter,
        //             //                 y: barVerticalCenter,
        //             //                 'text-anchor': 'middle'
        //             //             });
        //             //             return chart_data.group.append(label);
        //             //         // }
        //             //     }
        //             // });
        //         }

        //         else {
        //             //console.log(data);
        //         }
        //     },
        //     error: function(e){
        //         //console.log(e);
        //     }

        // });

        $.ajax({
            url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetSubscriberDetail"+strAjaxQuery,
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function(d){
                if(d.RXRESULTCODE == 1 || d.RXRESULTCODE == 0){
                    $('#all-sub').text(d.ALLSUB);
                    var window_with = $( window ).width();
                    var set_height = 220;
                    var set_font = "18px";
                    var set_limitchar = 7;

                    if (IsMobileTablet() && window_with > 1368){
                        set_height = 300;
                        set_font = "26px";
                    }
                    else if (!IsMobileTablet() && window_with > 1351){
                        set_height = 300;
                        set_font = "26px";
                    }
                    else if(IsMobileTablet() && window_with <= 768){
                        set_height = 195;
                    }
                    else if (!IsMobileTablet() && window_with <= 751){
                        set_height = 195;
                    }

                    var subList = [];
                    var series = [];

                    for (var i = 0; i < d['LISTDETAIL'].length; i++) {
                        if (d['LISTDETAIL'][i].NAME.length > set_limitchar){
                            d['LISTDETAIL'][i].NAME = '<a class="subname-tooltip" data-toggle="tooltip" title="' + d['LISTDETAIL'][i].NAME + '">'+d['LISTDETAIL'][i].NAME.substring(0, set_limitchar)+"... </a>";
                        }
                        else{
                            d['LISTDETAIL'][i].NAME = '<a class="subname-tooltip" data-toggle="tooltip" title="' + d['LISTDETAIL'][i].NAME + '">'+d['LISTDETAIL'][i].NAME+"</a>";
                        }

                        subList.push(d['LISTDETAIL'][i].NAME);
                        series.push(d['LISTDETAIL'][i].TOTALSUBSCRIBER);
                    }

                    var chart_data = {labels: subList,series:[ series]};


                    var defaultOptions = {
                        // Options for X-Axis
                        axisX: {
                            // The offset of the chart drawing area to the border of the container
                            offset: 80,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'end',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: true,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum width in pixel of the scale steps
                            scaleMinSpace: 30,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                        },
                        // Options for Y-Axis
                        axisY: {
                        // The offset of the chart drawing area to the border of the container
                            offset: 10,
                            // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
                            position: 'start',
                            // Allows you to correct label positioning on this axis by positive or negative x and y offset.
                            labelOffset: {
                            x: 0,
                            y: 0
                            },
                            // If labels should be shown or not
                            showLabel: false,
                            // If the axis grid should be drawn or not
                            showGrid: false,
                            // Interpolation function that allows you to intercept the value from the axis label
                            labelInterpolationFnc: Chartist.noop,
                            // This value specifies the minimum height in pixel of the scale steps
                            scaleMinSpace: 20,
                            // Use only integer values (whole numbers) for the scale steps
                            onlyInteger: false
                            },
                        // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
                        width: undefined,
                        // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
                        height: set_height,
                        // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
                        high: undefined,
                        // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
                        low: undefined,
                        // Unless low/high are explicitly set, bar chart will be centered at zero by default. Set referenceValue to null to auto scale.
                        referenceValue: 0,
                        // Padding of the chart drawing area to the container element and labels as a number or padding object {top: 5, right: 5, bottom: 5, left: 5}
                        chartPadding: {
                            top: 20,
                            right: 15,
                            bottom: 15,
                            left: 10
                        },
                        // Specify the distance in pixel of bars in a group
                        seriesBarDistance: 15,
                        // If set to true this property will cause the series bars to be stacked. Check the `stackMode` option for further stacking options.
                        stackBars: false,
                        // If set to 'overlap' this property will force the stacked bars to draw from the zero line.
                        // If set to 'accumulate' this property will form a total for each series point. This will also influence the y-axis and the overall bounds of the chart. In stacked mode the seriesBarDistance property will have no effect.
                        stackMode: 'accumulate',
                        // Inverts the axes of the bar chart in order to draw a horizontal bar chart. Be aware that you also need to invert your axis settings as the Y Axis will now display the labels and the X Axis the values.
                        horizontalBars: false,
                        // If set to true then each bar will represent a series and the data array is expected to be a one dimensional array of data values rather than a series array of series. This is useful if the bar chart should represent a profile rather than some data over time.
                        distributeSeries: false,
                        // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
                        reverseData: false,
                        // If the bar chart should add a background fill to the .ct-grids group.
                        showGridBackground: false,
                        // Override the class names that get used to generate the SVG structure of the chart
                        classNames: {
                            chart: 'ct-chart-bar ct-chart-bar-dashboard',
                            horizontalBars: 'ct-horizontal-bars',
                            label: 'ct-label',
                            labelGroup: 'ct-labels',
                            series: 'ct-series',
                            bar: 'ct-bar-dashboard',
                            grid: 'ct-grid',
                            gridGroup: 'ct-grids',
                            gridBackground: 'ct-grid-background',
                            vertical: 'ct-vertical',
                            horizontal: 'ct-horizontal',
                            start: 'ct-start',
                            end: 'ct-end'
                        }
                    };

                    var barChart = new Chartist.Bar('#subscriber-detail-chart', chart_data, defaultOptions);
                    }

                    barChart.on('draw', function(chart_data) {
                        $('[data-toggle="tooltip"]').tooltip();
                        var barHorizontalCenter, barVerticalCenter, label, value;
                        if (chart_data.type === "bar") {
                            // barHorizontalCenter = chart_data.x1 + (chart_data.element.width() * .5);
                            // barVerticalCenter = chart_data.y1 + (chart_data.element.height() * -1) - 10;
                            barHorizontalCenter = chart_data.x1;
                            barVerticalCenter = chart_data.y1-10;
                            value = chart_data.element.attr('ct:value');
                            // if (value !== '0') {
                                label = new Chartist.Svg('text');
                                label.text(value);
                                label.addClass("ct-barlabel");
                                label.attr({
                                    x: barHorizontalCenter,
                                    y: barVerticalCenter,
                                    'text-anchor': 'middle'
                                });
                                return chart_data.group.append(label);
                            // }
                        }
                    });

                // else{
                //     //console.log(d);
                // }
            },
            error: function(e){

            }
        });
    });

    $('.short-code-list-select').on('change', function () {
        location.reload();
    });

    // jQuery(document).ready(function($) {
    //     $('#total-sent, #opt-in-number, #opt-out-number, #short-url-click, #mlp-views').textfill({
    //         maxFontPixels: 77,
    //         minFontPixels: 4
    //     });


    //     $('.mlp-url-title').textfill({
    //         maxFontPixels: 26,
    //         minFontPixels: 4
    //     });
    // });

    function ShortUrlClickList(strFilter){
        $("#total-short-url-click-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var table = $('#total-short-url-click-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "DESC", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "180px"},
                {"mData": "URL", "sName": '', "sTitle": 'Short URL', "bSortable": false, "sClass": "", "sWidth": "180px"},
                {"mData": "DYNAMICDATA", "sName": '', "sTitle": 'Dynamic Data', "bSortable": false, "sClass": "", "sWidth": "160px"},
                {"mData": "TARGET", "sName": '', "sTitle": 'Actual', "bSortable": false, "sClass": "", "sWidth": "160px"},
                {"mData": "COUNT", "sName": '', "sTitle": 'Clicks', "bSortable": false, "sClass": "", "sWidth": "80px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "160px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=ShortUrlClickList'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
                            $('#total-short-url-click-table_paginate').hide();
                        }
                    }
                });
            }
        });
    }

    function InitFilterShortURLClickTable () {
        $("#shortURLClickFilterBox").html('');
        //Filter bar initialize
        $('#shortURLClickFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' Desc_vch '},
                {DISPLAY_NAME: 'Short URL', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ShortURL_vch '},
                {DISPLAY_NAME: 'Actual', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' TargetURL_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
                //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                ShortUrlClickList();
            }
        }, function(filterData){
            ShortUrlClickList(filterData);
        });
    }

    $('#short-url-click, #short-url-total').click(function(event){
        InitFilterShortURLClickTable();
        ShortUrlClickList();
        $("#total-short-url-click-modal").modal("show");
        // $("#shortURLClickFilterBox > .filter-item > div:nth-child(2)").hide();
        $("#shortURLClickFilterBox .btn-add-item").hide();
    });

    $('body').on("click", "button.btn-re-dowload.total-shorturl-click-list", function(event){
        window.location.href="/session/sire/models/cfm/admin/admin-export-total-short-url-click-list.cfm"+"?filter="+filter;
    });

    function MLPViewList(strFilter){
        $("#total-mlp-view-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var table = $('#total-mlp-view-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "pagingType": "simple_numbers",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "NAME", "sName": '', "sTitle": 'Description', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": "URL", "sName": '', "sTitle": 'URL', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": "COUNT", "sName": '', "sTitle": 'Views', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "130px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=MLPViewList'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );

                filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
                            $('#total-mlp-view-table_paginate').hide();
                        }
                    }
                });
            }
        });
    }

    function InitFilterMLPViewTable () {
        $("#mlpViewFilterBox").html('');
        //Filter bar initialize
        $('#mlpViewFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Description', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' cd.cppxName_vch '},
                {DISPLAY_NAME: 'URL', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' cd.cppxURL_vch '},
                {DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' cd.created_dt '},
                //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
            ],
            clearButton: true,
            error: function(ele){

            },
            clearCallback: function(){
                MLPViewList();
            }
        }, function(filterData){
            MLPViewList(filterData);
        });
    }

    $('#mlp-views, #mlp-total').click(function(event){
        InitFilterMLPViewTable();
        MLPViewList();
        $("#total-mlp-view-modal").modal("show");
        // $("#mlpViewFilterBox > .filter-item > div:nth-child(2)").hide();
        $("#mlpViewFilterBox .btn-add-item").hide();
    });

    $('body').on("click", "button.btn-re-dowload.total-mlp-view-list", function(event){
        window.location.href="/session/sire/models/cfm/admin/admin-export-total-mlp-view-list.cfm"+"?filter="+filter;
    });



    $(window).on('resize', function(){
        $('#total-sent, #opt-in-number, #opt-out-number, #short-url-click, #mlp-views').textfill({
            maxFontPixels: 77,
            minFontPixels: 4
        });


        $('.mlp-url-title').textfill({
            maxFontPixels: 26,
            minFontPixels: 4
        });
    });

})(jQuery);
