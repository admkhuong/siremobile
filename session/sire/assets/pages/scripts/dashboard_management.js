var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var start = moment();
var end = moment();
var startTime = '';
var endTime = '';
var rfresh = 0;
var globalFilterLevel=0;
var globalUseridSelect=0;
var globalExportUserID=0;

$('.reportrange').daterangepicker({
    "dateLimit": {
        "years": 1
    },
    startDate: start,
    endDate: end,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, GetReport);
function GetReport(start, end, rfresh)
{
    dateStart_crm = start; 
    dateEnd_crm = end;; 
    $('.reportrange span.time').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
    dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
    
    startTime = dateStart;
    endTime = dateEnd;
    SumaryManagementData();
    BillingHistory();
}
function SumaryManagementData()
{    
    $.ajax({
        url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetAllReportCount"+strAjaxQuery,
        type: "POST",
        dataType: "json",
        data: {
            inpStartDate: dateStart,
            inpEndDate: dateEnd,
            inpIncludeSubAccLv:globalFilterLevel
        },
        asynce: false,
        beforeSend: function(xhr){
            $("#processingPayment").show();
        },
        success: function(data){
            data = jQuery.parseJSON(data);
            if(parseInt(data.RXRESULTCODE) === 1){               
                $("#crm-msg-sent-numb").text(data.MESSAGESENT);
                $("#crm-msg-received-numb").text(data.MESSAGERECEIVED);
                $("#crm-opt-in-numb").text(data.TOTALOPTIN);
                $("#crm-opt-out-numb").text(data.TOTALOPTOUT);                               
                $("#total-user-div").text(data.TOTALUSER);   
                
            }        
            $("#processingPayment").hide();
        }
    });
}
function GetTotalSentReceivedMessageList(strFilter){    
    $("#total-message-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
    var sentCol = function(object){
        if(object.USERID == ''){
            return object.SENT;
        }
        return $('<a class="sent-received-details" data-id="'+object.USERID+'">'+object.SENT+'</a>')[0].outerHTML;
    }
    
    var receivedCol = function(object){
        if(object.USERID == ''){
            return object.RECEIVED;
        }
        return $('<a class="sent-received-details" data-id="'+object.USERID+'">'+object.RECEIVED+'</a>')[0].outerHTML;
    }
    
    var table = $('#total-message-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
            {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
            {"mData": sentCol, "sName": '', "sTitle": 'Sent Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
            {"mData": receivedCol, "sName": '', "sTitle": 'Rec\'d Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetTotalSentReceivedMessageList'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpIncludeSubAccLv", "value": globalFilterLevel}                
            );        
            filterTotalSentReceived = customFilterData;
            
            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {                    
                    fnCallback(data);                                        
                    if(data.DATALIST.length == 0){
                        $('#total-message-table_paginate').hide();
                    }
                }
            });
        }
    });
}

function InitFilterMessageTable () {    
    $("#totalMessageFilterBox").html('');
    //Filter bar initialize
    $('#totalMessageFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.UserId_int '},
            {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},            
        ],
        clearButton: true,
        error: function(ele){

        },
        clearCallback: function(){            
            GetTotalSentReceivedMessageList();
        }
    }, function(filterData){        
        GetTotalSentReceivedMessageList(filterData);
        
    });
}

function GetSummarySentReceivedMessage(strFilter){        
    $("#total-message-summary-table").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
    var sentCol = function(object){
        // if(object.USERID == ''){
        //     return object.SENT;
        // }
        if(object.BATCHID >0 )
        {
            return $('<a class="preview-campaign" data-batchid="'+object.BATCHID +'" data-userid="'+object.USERID+'">'+object.SENT+'</a>')[0].outerHTML;
        }
        else
        {
            return $('<span class="" data-batchid="'+object.BATCHID +'" data-userid="'+object.USERID+'">'+object.SENT+'</span>')[0].outerHTML;
        }
    }
    
    var table = $('#total-message-summary-table').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "NAME", "sName": '', "sTitle": 'Campaign', "bSortable": false, "sClass": "", "sWidth": "270px"},
            {"mData": sentCol, "sName": '', "sTitle": 'Sent Count', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "RECEIVED", "sName": '', "sTitle": 'Rec\'d Count', "bSortable": false, "sClass": "", "sWidth": "150px"},
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetSummarySentReceivedMessage'+strAjaxQuery,
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );
            aoData.push(
                { "name": "inpUserId", "value": globalUseridSelect}
            );
            
            filterSummarySentReceived = customFilterData;
            
            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                    if(data.DATALIST.length == 0){
                        $('#total-message-summary-table_paginate').hide();
                    }
                }
            });
        }
    });
}
function InitFilterSummaryMessageTable () {
    $("#totalMessageSummaryFilterBox").html('');
    //Filter bar initialize
    $('#totalMessageSummaryFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ba.Desc_vch '},
            {DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.BatchId_bi '},
        ],
        clearButton: true,
        error: function(ele){
            
        },
        clearCallback: function(){
            GetSummarySentReceivedMessage();
        }
    }, function(filterData){
        GetSummarySentReceivedMessage(filterData);
    });
}

// biling history
function BillingHistory(strFilter){    
    $("#management-biling").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
    var reiceptCol = function(object){
        var strReturn = $('<a class="btn newbtn green-gd download-billing-details" title="Download Billing History Details">Download</a>')
                            .attr('data-id', object.ID)[0].outerHTML;
        return strReturn;
    }


    var table = $('#management-biling').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "fnStateLoadParams": function (oSettings, oData) {

        },
        "fnStateSaveParams": function (oSettings, oData) {

        },
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "DATE", "sName": '', "sTitle": 'Date',"bSortable": false,"sClass":"", "sWidth":"130px"},
            {"mData": "AMOUNT", "sName": '', "sTitle": 'Amount',"bSortable": false,"sClass":"", "sWidth":"120px"},
            {"mData": "PRODUCT", "sName": '', "sTitle": "Product","bSortable": false,"sClass":"", "sWidth":"290px"},
            {"mData": "SUBACCID", "sName": '', "sTitle": "Billing Account","bSortable": false,"sClass":"", "sWidth":"150px"},
            {"mData": "STATUS", "sName": '', "sTitle": "Status","bSortable": false,"sClass":"", "sWidth":"200px"},
            
            /* {"mData": reiceptCol, "sName": '', "sTitle": "Receipt","bSortable": false,"sClass":"", "sWidth":"150px"} */
        ],
        "sAjaxDataProp": "BILLINGHISTORYLIST",
        "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetBillingHistoryList' + strAjaxQuery,

        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            // console.log(fnCallback);
            aoData.push(
                { "name": "inpStartDate", "value": startTime}
            );
            aoData.push(
                { "name": "inpEndDate", "value": endTime}
            );
            aoData.push(
                { "name": "inpIncludeSubAccLv", "value": globalFilterLevel}
            );
            if (aoData[3].value < 0){
                aoData[3].value = 0;
            }

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data){                    
                    fnCallback(data);                    
                }
              });
        }
    });
}
function GetUserList(strFilter){
    $("#userTable").html('');
    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
    var table = $('#userTable').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
            //{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=getUsers'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );   
            aoData.push(
                { "name": "inpIncludeSubAccLv", "value": globalFilterLevel}
            );                      
            
            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                    if(data.datalist.length == 0){
                        $('#userTable_paginate').hide();
                    }
                }
            });
        }
    });
}
function InitFilterForUserTable () {
    $("#userFilterBox").html('');
    //Filter bar initialize
    $('#userFilterBox').sireFilterBar({
        fields: [
            {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
            {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
            {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
            {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
            {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
            {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
            {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
            //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
        ],
        clearButton: true,
        error: function(ele){
            
        },
        clearCallback: function(){
            GetUserList();
        }
    }, function(filterData){
        GetUserList(filterData);
    });
}

$(document).ready(function() {
    globalFilterLevel=$("#select-level").val();

    GetReport(start, end);    
    $("#select-level").select2();
    BillingHistory();
    
    $(document).on("change","#select-level",function(){
        globalFilterLevel=$("#select-level").val();
        SumaryManagementData();        
        BillingHistory();
    });

    $(document).on("click","#crm-msg-sent-numb, #crm-msg-received-numb",function(){  
        $("#total-message-modal").modal("show");          
        InitFilterMessageTable();
        GetTotalSentReceivedMessageList();
    });
    
    $(document).on('click', '.sent-received-details', function(){
        globalUseridSelect=$(this).data("id");
        InitFilterSummaryMessageTable();
        GetSummarySentReceivedMessage();
        $("#total-message-summary-modal").modal("show");
    });

    $(document).on('click', '#total-user-div', function(){        
        InitFilterForUserTable();
        GetUserList();
        $("#user-modal").modal("show");
    });

    $('body').on("click", "button.btn-re-dowload.total-message-list", function(event){
        window.location.href="/session/sire/models/cfm/dashboard/management-export-total-sent-received-msg-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filterTotalSentReceived+ "&inpIncludeSubAccLv="+globalFilterLevel;
    });
    $('body').on("click", "button.btn-re-dowload.total-message-summary-list", function(event){
        window.location.href="/session/sire/models/cfm/dashboard/management-export-summary-msg-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filterSummarySentReceived+"&userId="+globalUseridSelect;
    });
    $('#crm-opt-in-numb').click(function(event){
        InitFilterOptTable();
        GetOptInOutList();
        $("#total-optin-modal").modal("show");
        $("#totalOptInFilterBox > .filter-item > div:nth-child(2)").hide();
        $("#totalOptInFilterBox .btn-add-item").hide();
    });
    
    $('#crm-opt-out-numb').click(function(event){
        InitFilterOptOutTable();
        GetOptOutList();
        $("#total-optout-modal").modal("show");
        $("#totalOptOutFilterBox > .filter-item > div:nth-child(2)").hide();
        $("#totalOptOutFilterBox .btn-add-item").hide();
    });
    function InitFilterOptTable () {
        $("#totalOptInFilterBox").html('');
        //Filter bar initialize
        $('#totalOptInFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
                {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.UserId_int '},
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '}           
            ],
            clearButton: true,
            error: function(ele){
                
            },
            clearCallback: function(){
                GetOptInOutList();
            }
        }, function(filterData){
            GetOptInOutList(filterData);
        });
    }
    function InitFilterOptOutTable () {
        $("#totalOptOutFilterBox").html('');
        //Filter bar initialize
        $('#totalOptOutFilterBox').sireFilterBar({
            fields: [            
                {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' oi.ContactString_vch '},
                {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '}
            ],
            clearButton: true,
            error: function(ele){
                
            },
            clearCallback: function(){
                GetOptOutList();
            }
        }, function(filterData){
            GetOptOutList(filterData);
        });
    }
    function GetOptInOutList(strFilter){    
        $("#total-optin-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        
        var optInCol = function(object){
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
        }
        
        var optOutCol = function(object){       
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;        
        }    
        var table = $('#total-optin-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "160px"},
                {"mData": "NAME", "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px"},
                {"mData": "EMAIL", "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px"},
                {"mData": optInCol, "sName": '', "sTitle": 'Opt-In Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
                {"mData": optOutCol, "sName": '', "sTitle": 'Opt-Out Cnt', "bSortable": false, "sClass": "", "sWidth": "120px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetOptInOutList'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpStartDate", "value": startTime}
                );
                aoData.push(
                    { "name": "inpEndDate", "value": endTime}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                aoData.push(
                    { "name": "inpIncludeSubAccLv", "value": globalFilterLevel}
                );   
                
                filter = customFilterData;
                
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    beforeSend: function(){
                        $('#processingPayment').show();                                                     
                    },  
                    error: function(jqXHR, textStatus, errorThrown) {                                   
                        $("#processingPayment").hide();
                        alertBox('System is timeout!','Oops!','');                                                              
                    },
                    "success": function(data) {
                        $("#processingPayment").hide();
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
                            $('#total-optin-table_paginate').hide();
                        }
                    }
                });
            }
        });
    }
    function GetOptOutList(strFilter){       
        var intialUserIds = function(object){           
        
    
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
        }
    
        $("#total-optout-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
    
        var optInCol = function(object){
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTIN+'</a>')[0].outerHTML;
        }
        
        var optOutCol = function(object){                     
            return $('<a class="opt-in-out-details" data-id="'+object.USERID+'">'+object.OPTOUT+'</a>')[0].outerHTML;
        }
    
        var table = $('#total-optout-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [                
                {"mData": "PHONE", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},            
                {"mData": "DATE", "sName": '', "sTitle": 'Date', "bSortable": false, "sClass": "", "sWidth": "150px"},                      
                // {"mData": "USERID", "sName": '', "sTitle": 'User Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CAMPAIGNNAME", "sName": '', "sTitle": 'Campaign Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": intialUserIds, "sName": '', "sTitle": 'Inital userids', "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false},
                 // {"mData": null, "sName": '', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "160px", "bVisible": false},
                 //    {"mData": null, "sName": '', "sTitle": 'Email', "bSortable": false, "sClass": "", "sWidth": "180px", "bVisible": false},
                 //    {"mData": optInCol, "sName": '', "sTitle": 'Opt-In Cnt',"sId": "bttttt", "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false},
                 //    {"mData": optOutCol, "sName": '', "sTitle": 'Opt-Out Cnt', "bSortable": false, "sClass": "", "sWidth": "120px", "bVisible": false}       
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetOptOutListByDateRange'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    {"name": "inpStartDate", "value": startTime}
                );
                aoData.push(
                    {"name": "inpEndDate", "value": endTime}                
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );      
                aoData.push(
                    { "name": "inpIncludeSubAccLv", "value": globalFilterLevel}
                );                         
                filter = customFilterData;
    
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        if(data.DATALIST.length == 0) {
                            $('.dowload-optout-list').hide();    
                            $("#total-optout-table_paginate").hide();
                        }
                        else
                        {
                            $('.dowload-optout-list').show();
                        }
                        fnCallback(data);
                        $("#mdOptOutList").modal("show");
                    }
                });
            }
        });   
    }
    function InitFilterSummaryOptTable () {
        $("#totalOptSummaryFilterBox").html('');
        //Filter bar initialize
        $('#totalOptSummaryFilterBox').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
                {DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.BatchId_bi '},
            ],
            clearButton: true,
            error: function(ele){
                
            },
            clearCallback: function(){
                GetOptInOutSummaryList();
            }
        }, function(filterData){
            GetOptInOutSummaryList(filterData);
        });
    }
    function GetOptInOutSummaryList(strFilter){
        $("#total-opt-summary-table").html('');
        var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
        
        var table = $('#total-opt-summary-table').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "NAME", "sName": '', "sTitle": 'Campaign', "bSortable": false, "sClass": "", "sWidth": "270px"},
                {"mData": "OPTIN", "sName": '', "sTitle": 'Opt-In count', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "OPTOUT", "sName": '', "sTitle": 'Opt-Out count', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetOptInOutSummaryList'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpStartDate", "value": startTime}
                );
                aoData.push(
                    { "name": "inpEndDate", "value": endTime}
                );
                aoData.push(
                    { "name": "customFilter", "value": customFilterData}
                );
                aoData.push(
                    { "name": "inpUserId", "value": globalExportUserID}
                );
                
                filter = customFilterData;
                
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    beforeSend: function(){
                        $('#processingPayment').show();                                                     
                    },  
                    error: function(jqXHR, textStatus, errorThrown) {                                   
                        $("#processingPayment").hide();
                        alertBox('System is timeout!','Oops!','');                                                                    
                    },
                    "success": function(data) {
                        $("#processingPayment").hide();
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
                            $('#total-opt-summary-table_paginate').hide();
                        }
                    }
                });
            }
        });
    }
    $('body').on('click', '.opt-in-out-details', function(){
        globalExportUserID = $(this).data('id');        
        InitFilterSummaryOptTable();
        GetOptInOutSummaryList();
        $("#total-opt-summary-modal").modal("show");
        $("#totalOptSummaryFilterBox > .filter-item > div:nth-child(2)").hide();
        $("#totalOptSummaryFilterBox .btn-add-item").hide();
    });
    $('body').on("click", "button.btn-re-dowload.total-optin-list", function(event){
        window.location.href="/session/sire/models/cfm/dashboard/management-export-total-optin-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+ "&inpIncludeSubAccLv="+globalFilterLevel;
    });
    $('body').on("click", "button.btn-re-dowload.total-optout-list", function(event){
        window.location.href="/session/sire/models/cfm/dashboard/management-export-total-optout-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+ "&inpIncludeSubAccLv="+globalFilterLevel;
    });
    $('body').on("click", "button.btn-re-dowload.total-opt-summary-list", function(event){
        window.location.href="/session/sire/models/cfm/dashboard/management-export-total-opt-summary-list.cfm"+"?startdate="+startTime+"&enddate="+endTime+"&filter="+filter+ "&inpUserID="+globalExportUserID;
    });
});