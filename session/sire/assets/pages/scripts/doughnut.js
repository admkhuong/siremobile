var doughnutStep = function () {
	return {
		doughnutStepHandle: function () {
			Chart.defaults.global.tooltips.enabled = false;
			Chart.defaults.global.hover.mode = 'index';

			var config = {
				type: 'doughnut',
				data: {
					datasets: [
						{
							data: [100, 100, 100, 100, 100, 100, 100],
							backgroundColor: [
								'#74c37f',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c'
							],
							hoverBackgroundColor: [
								'#74c37f',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c',
								'#5c5c5c'
							]
						}
					]
				},
				options: {
					responsive: true,
					cutoutPercentage: 60
				}
			}

			var ctx = document.getElementById('myStep').getContext("2d");
			window.myDoughnut = new Chart(ctx, config);

			document.getElementById('nextStep').addEventListener('click', function (e) {
				e.preventDefault();
				var totalStep = 7;
				nextStep(1, totalStep);

				//finalStep(totalStep);
			});

			function nextStep (step, totalStep) {
				if (config.data.datasets.length > 0) {
					if (step < totalStep) {
						config.data.datasets.forEach(function(dataset) {
							dataset.backgroundColor[step] = '#74c37f';
							dataset.hoverBackgroundColor[step] = '#74c37f';
				        });
					}
			        window.myDoughnut.update();			        
				};
			}

			function finalStep (totalStep) {
				for (var i = 0; i < totalStep; i++) {
					config.data.datasets.forEach(function(dataset) {
						dataset.backgroundColor[i] = '#74c37f';
						dataset.hoverBackgroundColor[i] = '#74c37f';
			        });
				};
				window.myDoughnut.update();
			}

		},

		init: function () {
			this.doughnutStepHandle();
		}
	}
}();

jQuery(document).ready(function() {
	doughnutStep.init();
});