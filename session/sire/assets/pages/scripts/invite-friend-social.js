// handle comsumer key for envinroments
var friendLimitSelected = 5;
var hostname = location.hostname;
var dialogTitle = 'Friend invitation';

// By info to a tag social network
var setSocialData = function(){
    var apisInfo = [
        {color: '3b5998', type: 'facebook', shareUrl: 'https://www.facebook.com/sharer.php?p[url]='+shareUrl},
        {color: '55acee', type: 'twitter', shareUrl: 'https://twitter.com/intent/tweet?url='+shareUrl},
        {color: 'dd4b39', type: 'google-plus', shareUrl: 'https://plus.google.com/share?url='+shareUrl}
        // {color: '007bb5', type: 'linkedin', shareUrl: 'https://www.linkedin.com/cws/share?url='+shareUrl},
        // {color: 'cb2027', type: 'pinterest', shareUrl: 'http://pinterest.com/pin/create/button/?media=https://s3-us-west-1.amazonaws.com/siremobile/sire-refer-a-friend-facebook-post.jpg?v=2.0&url='+shareUrl}
    ];
    for(var i in apisInfo){
        var objNetwork = apisInfo[i];
        for(var prop in objNetwork){
            $('div.share-url > a.social-auth i.fa-'+objNetwork.type+'-square, div.share-url > a.social-auth-tw i.fa-'+objNetwork.type+'-square').parent()
                                                              .attr('href', objNetwork.shareUrl)
                                                              .css({color: '#'+objNetwork.color, cursor: 'pointer'});
        }
    }
}


//Call setSocialData function
$(document).ready(function(){
    setSocialData();
});


//Trigger event when user click to a tag social network
$('div.share-url > a.social-auth').click(function(event){
    event.preventDefault();
    var fbShareUrl = shareUrl;
    console.log(fbShareUrl);
    var shareUrl = $(this).attr('href');

    //Recache Facebook Open Graph
    if($(this).find('i').hasClass('fa-facebook-square')){
        var grapApiFBurl = "https://graph.facebook.com/?id="+fbShareUrl+"&amp;scrape=true&amp;method=post";
        $.ajax({
            url: grapApiFBurl,
            type: "GET",
            dataType: "json",
            success: function(data){}
        });
    }
    window.open(shareUrl,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
});

// define item template for contact list, name and email field will be use for searching
var twOptions = {
    valueNames: [ 'name' ],
    item: '<div data-id="" class="friend-item col-sm-4 col-md-4 col-lg-6 col-xs-6">'+
                            '<div class="friend-list-item custom-checkbox">'+
                                '<input value="" type="checkbox" id="" class="contact-checkbox" />'+
                                '<label for="ckb">'+
                                    '<img src="" alt="" />'+
                                    '<span class="name" style="word-break: break-all; margin-left:0.1rem"></span>' +
                                '</label>'+
                            '</div>'+       
                        '</div>'
};

// contact list constructor
var twContactList = new List('twitter', twOptions);

var twitterGetContacts = function(accessToken){

    twContactList.clear();

    var requestUrl = '/session/sire/models/cfc/referral.cfc?method=GetTwitterFollowers&inpOAuthVerifier='+accessToken+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';

    $.ajax({
        url: requestUrl,
        type: 'POST',
        dataType: 'json',
        data: {},
        beforeSend: function(){
            $("#processingPayment").show();
        },
        success: function(data){
            if(parseInt(data.RXRESULTCODE) === 1){
                loadFriendList($.parseJSON(data.FOLLOWERS));
                clearSelectedFriends();

                $(".selected-friends-section").show();
            }
            $("#twitter").show();
        },
        error: function(e){

        },
        complete: function(){
            $("#processingPayment").hide();
        }
    });
}


// handle send direct message
var sendMesage = function(friends){

    var twDirectMsgUrl = '/session/sire/models/cfc/referral.cfc?method=SendTwitterDirectMessages&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
    try{
        $.ajax({
            url: twDirectMsgUrl,
            type: 'POST',
            dataType: 'json',
            data: {inpContacts: JSON.stringify(friends)},
            beforeSend: function(){
                $("#processingPayment").show();
            },
            success: function(data){
                if(data.SENDFAILED.length > 0){
                    var listFailured = $('<ul></ul>');
                    for (var i in data.SENDFAILED){
                        var uName = data.SENDFAILED[i].USERNAME;
                        // console.log( data.SENDFAILED[i]);
                        listFailured.append('<li>'+uName+'</li>');
                    }
                    var msg = listFailured.html();

                    bootbox.dialog({
                        title: 'The invitation message can not send to your friend(s)',
                        message: '<ul>'+listFailured.html()+'</ul>',
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success btn-success-custom",
                                callback: function() {
                                    
                                }
                            }
                        }
                    });

                    $("#twitter").hide();

                    $(".selected-friends-section").hide();
                } else {
                    bootbox.dialog({
                        title: 'Social Friend Invitation',
                        message: ' Your invitation has been sent!',
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn-success btn-success-custom",
                                callback: function() {
                                    
                                }
                            }
                        }
                    });
                    $("#twitter").hide();
                }
                clearSelectedFriends();
            },
            complete: function(responseText){
                $("#processingPayment").hide();
            }
        });
    } catch(e) {
        bootbox.dialog({
            title: 'Alert',
            message: e,
            buttons: {
                success: {
                    label: "OK",
                    className: "btn-success btn-success-custom",
                    callback: function() {
                        
                    }
                }
            }
        });
    }
    
}

//bind html from response data
var loadFriendList = function(data){
    var friendList = data.users;
    $('#twitter-friend-list').empty();
    for(i in friendList){
        var friend = friendList[i];
        var htmlItem  = '<div data-id="'+friend.id_str+'" class="friend-item col-sm-5 col-md-12 col-lg-12 col-xs-12" style="margin-left:0.5rem">'+
                            '<div class="friend-list-item custom-checkbox">'+
                                '<input value="'+friend.id_str+'" type="checkbox" id="ckb'+friend.id_str+'" class="contact-checkbox" />'+
                                '<label for="ckb'+friend.id_str+'">'+
                                    '<img src="'+friend.profile_image_url_https+'" alt="'+friend.name+'" />'+
                                    '<span class="name" style="word-break: break-all; margin-left:0.5rem">'+friend.name+'</span>' +
                                '</label>'+
                            '</div>'+       
                        '</div>';
        //$(htmlItem).data('friend-object', friend).appendTo('#twitter-friend-list');
        $('#twitter .list').append($(htmlItem).data('friend-object', friend));
    }
    twContactList = new List('twitter', twOptions);
    $('#twitter-friend-list').hide().fadeIn(500);
}

function twsearchContacts() {
    var inputSearch = $('#search-inp').val();

    var searchResult = twContactList.search(inputSearch);

    if (searchResult.length === 0) {
        bootbox.dialog({
            message: "No result found!",
            title: "Oops!",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        });
    }
}

// search current email list
$('body').on('click', '#twitter-search-btn', function(event) {
    twsearchContacts();
});

// search on enter key press
$('#search-inp').keyup(function(event) {
    if (event.keyCode == 13) {
        twsearchContacts();
    }
});



$('a.social-auth-tw').click(function(e){
    e.preventDefault();
    if(twContactList.items.length>0){
        twContactList.clear();
    }
    
    clearSelectedFriends();
    // authorize url
    var callbackURL = 'https://' + hostname + '/session/sire/pages/invite-friend-callback';

    var twAuthURL = '';

    $.ajax({
        url: '/session/sire/models/cfc/referral.cfc?method=GetTwitterAuthUrl&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST',
        dataType: 'JSON',
        async: false,
    })
    .done(function(data) {

        if (parseInt(data.RXRESULTCODE) == 1) {
            twAuthURL = data.AUTHURL;
        } else {
            alertError();
        }
    })
    .fail(function() {
        alertError();
    });

    if (twAuthURL != '') {
        // option for popup windows
        var popupWidth = 700,
            popupHeight = 500,
            popupLeft = (window.screen.width - popupWidth) / 2,
            popupTop = (window.screen.height - popupHeight) / 2;

        // open popup, display callback page
        var popup = window.open(callbackURL, '', 'width='+popupWidth+',height='+popupHeight+',left='+popupLeft+',top='+popupTop+'');

        // handle popup action
        popup.onload = function() {
            //open authorize url in pop-up
            popup.open(twAuthURL, '_self');
        }

        //an interval runs to get the access token from the pop-up
        var interval = setInterval(function() {
            try {
                // check if error occurred
                if (popup.location.href.indexOf('error') >= 0 || popup.location.href.indexOf('denied') >= 0) {
                    clearInterval(interval);
                    popup.close();
                } else {
                    //check if hash exists
                    if(popup.location.search.length) {
                        //hash found, that includes the access token
                        clearInterval(interval);

                        // extract authorize code from return url
                        var authHash = popup.location.search.split('&');
                        var accessToken = authHash[1].slice(15); //slice #access_token= from string

                        popup.close();

                        // pass access token to get contact function

                        twitterGetContacts(accessToken);
                    }
                }
            }
            catch(evt) {
                //permission denied
            }
        }, 100);
    }
}); 





//send direct message twitter
$('button.send-invitation').click(function(e){
    var friendSelected = [];
    $('#twitter-friend-list-selected >div').each(function(){
        var item = {};
        item.userId = $(this).data('friend-id');
        item.userName = $(this).data('name');
        friendSelected.push(item);
    });

    sendType = 3;

    if (typeof friendSelected === 'undefined' || friendSelected.length <= 0) {
        alertNothingSelected();
    } else {
        grecaptcha.reset();
        $('#captcha-message').text('');

        $('#ConfirmSendInvitation').modal('show');
    }
    
});




//handle when user click to select friend 
$('#twitter-friend-list').on('click', 'div.friend-list-item',  function(event){

    var thisCheckbox = $(this).find('input[type="checkbox"]');

    var friendObject = thisCheckbox.parents('div.friend-item').data('friend-object');

    if($('#twitter-friend-list div.friend-list-item input[type="checkbox"]:checked').length){
        $('button.send-invitation').prop('disabled', false);
    } else {
        $('button.send-invitation').prop('disabled', true);
        
    }

    if(thisCheckbox.prop('checked')){
        if($('#twitter-friend-list').find('div input[type="checkbox"]:checked').length <= friendLimitSelected){
            addFriendItem(friendObject);    
        } else {
            // var thisCheckbox = thisCheckbox;
            bootbox.dialog({
                message: "Selected friend not greater than "+ friendLimitSelected +" !",
                title: "Friend Invitation",
                buttons: {
                    success: {
                      label: "Ok!",
                      className: "btn-success btn-success-custom",
                      callback: function(){
                            
                        }
                    }
                }
            });
            thisCheckbox.prop('checked', false);
        }
    } else {
        removeFriendItem(friendObject); 
    }

});

// add friend item
var addFriendItem = function(friendObject){
    var html = '<div class="selected-email-item" data-friend-id="'+friendObject.id_str+'" data-name="'+friendObject.name+'"><div class="alert">'+friendObject.name+'<a class="my-close"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>';    
    $('#twitter-friend-list-selected').append(html);
}

//remove friend item
var removeFriendItem = function(friendObject){
    $('#twitter-friend-list-selected').find('div[data-friend-id="'+friendObject.id_str+'"]').remove(); 
}


// handle when user click to close icon on selected item contacts
$('#twitter-friend-list-selected').on('click', 'a.my-close',  function(){
    var divEle = $(this).parent().parent();
    var friendId = divEle.data('friend-id');
    $('#twitter-friend-list').find($('#twitter-friend-list div.friend-list-item input[type="checkbox"][value="'+friendId+'"]')).trigger('click');
    
});


//clear selected contact items box
var clearSelectedFriends = function(){
    $("#twitter").hide();
    $('#twitter-friend-list-selected').empty();
    $('#twitter-friend-list div.friend-list-item input[type="checkbox"]').prop('checked', false);
}