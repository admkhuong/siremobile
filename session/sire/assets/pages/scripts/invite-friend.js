$('.link-myplan').click(function(e){
    e.preventDefault();
    location.href= $(this).attr('href');
});

var MyplanComponents = function () {

    var handleBootstrapSelect = function() {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var handleMyPlanTab = function () {
        $(window).bind("load resize", function() {
            var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            if (width < 768) {
                $('#toggle-xs').addClass('uk-hidden');
            } else {
                $('#toggle-xs').removeClass('uk-hidden');
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {      
            handleBootstrapSelect();
            handleMyPlanTab();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
        MyplanComponents.init(); 
    });
}

// constructor for validate engine
$("#email_form").validationEngine({promptPosition : "bottomLeft", scroll: false,focusFirstField : false, binded: false});

// check for current working section, 1-Gmail, 2-Outlook, 3-Yahoo
var inSection = 1;

// construct selected email array
var selectedEmail = Array();

// detect send type: 1-sms, 2-emai
var sendType = 0;

var ajaxTimeout = 10000;

if (typeof hostname === 'undefined' || hostname === '') {
    hostname = 'https://www.siremobile.com';
}

// set global callback uri and redirect uri
var callbackURI = hostname+'/session/sire/pages/invite-friend-callback';

var windowsScope = 'https://outlook.office.com/contacts.read';

// define item template for contact list, name and email field will be use for searching
var options = {
    valueNames: [ 'name', 'email' ],
    item: '<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">'+
                '<div class="clearfix">'+
                    '<div class="col-xs-1 no-padding custom-checkbox">'+
                        '<input type="checkbox" class="contact-checkbox"/>'+
                    '</div>'+
                    '<div class="col-xs-11 no-padding">'+
                        '<label for="">'+
                            '<span class="name" style="word-break: break-all"></span><br>'+
                            '<span class="email" style="word-break: break-all"></span>'+
                            '<span class="status"></span>'+
                        '</label>'+
                    '</div>'+
                '</div>'+
            '</div>'
};

// contact list constructor

var gmailContactList = new List('gmail', options);
var outlookContactList = new List('outlook', options);
var yahooContactList = new List('yahoo', options);

$('body').on('click', '.gmail-contacts-btn', googleAuth);
$('body').on('click', '.outlook-contacts-btn', windowsAuth);
$('body').on('click', '.yahoo-contacts-btn', yahooAuth);

$('body').on('click', '#gmail-contacts-btn', function () {
    inSection = 1;
    $('#input-search').val('');
    $("#refer_via_email .friends-list").hide();
    $('#refer_via_email .friends-list#gmail').show();
});
$('body').on('click', '#outlook-contacts-btn', function () {
    inSection = 2;
    $('#input-search').val('');
    $("#refer_via_email .friends-list").hide();
    $('#refer_via_email .friends-list#outlook').show();
});
$('body').on('click', '#yahoo-contacts-btn', function () {
    inSection = 3;
    $('#input-search').val('');
    $("#refer_via_email .friends-list").hide();
    $('#refer_via_email .friends-list#yahoo').show();
});

// check contact status for an email list
function checkListContactStatus(mailList) {
    var checkedResult = {};
    checkedResult.RXRESULTCODE = -1;

    $.ajax({
        url: '/session/sire/models/cfc/referral.cfc?method=CheckListContactStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST',
        dataType: 'json',
        timeout: ajaxTimeout,
        data: {inpContacts: JSON.stringify(mailList)},
        async: false
    })
    .done(function(data) {
        if (parseInt(data.RXRESULTCODE) == 1) {
            checkedResult.registedUserEmail = data.REGISTEDUSEREMAIL;
            checkedResult.sentUserEmail = data.SENTUSEREMAIL;
            checkedResult.RXRESULTCODE = 1;
        }
    });

    return checkedResult;
}

// check contact status for an email
function checkContactStatus(contact, type) {
    var checkedResult = {};
    checkedResult.RXRESULTCODE = -1;

    $.ajax({
        url: '/session/sire/models/cfc/referral.cfc?method=CheckContactStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST',
        dataType: 'json',
        timeout: ajaxTimeout,
        data: {
            inpContact: contact,
            inpType: type
        },
        async: false
    })
    .done(function(data) {
        if (parseInt(data.RXRESULTCODE) == 1) {
            checkedResult.CONTACTSTATUS = parseInt(data.CONTACTSTATUS);
            checkedResult.DATEDIFF = parseInt(data.DATEDIFF)
            checkedResult.RXRESULTCODE = 1;
        }
    });

    return checkedResult;
}

// reload list function, create new list with defined template and a new contact list
function reloadContactList(list, checkedList) {
    // variable for inject input id
    var count = 1;
    var inputClass = (inSection === 1 ? 'gmail-mail-' : (inSection === 2 ? 'outlook-mail-' : 'yahoo-mail-'));

    // loop through list, create item and append on dom
    $.each(list, function(index, val) {

        if (typeof val['email'] !== 'undefined') {
            // variable to detect contact status is registed or already sent invite
            var isRegisted = -1;
            var isSent = -1;

            // if registedUserEmail exist, check if this contact is already signed up
            if (checkedList.registedUserEmail.length > 0) {
                isRegisted = checkedList.registedUserEmail.indexOf(val['email']);
            }

            // if sentUserEmail exist, check if this contact is already been sent an invite, if user is already signed up, by pass this step
            if (checkedList.sentUserEmail.length > 0 && isRegisted < 0) {
                isSent = checkedList.sentUserEmail.indexOf(val['email']);
            }

            // append count on inputClass to identify each checkbox
            inputClass = inputClass + count;

            if (typeof val['name'] === 'undefined') {
                val['name'] = '';
            }

            var item = '<div class="friend-list-item col-xs-6 col-sm-4 col-md-4 col-lg-6">'+
                    '<div class="'+(isRegisted >= 0 ? 'signed ' : (isSent >= 0 ? 'sent ' : ''))+'clearfix">'+
                        '<div class="col-xs-12 custom-checkbox">'+
                            '<input type="checkbox" id="'+inputClass+'" class="contact-checkbox" '+(isRegisted >= 0 ? 'disabled ' : (isSent >= 0 ? 'disabled ' : ''))+'/>'+
                            '<label for="'+inputClass+'">'+
                                '<span class="name" style="word-break: break-all">'+(val['name'] !== '' ? '<b>'+val['name']+'</b></span><br>' : '</span>' )+
                                '<span class="email" style="word-break: break-all">'+val['email']+'</span><br>'+
                                '<span class="status">'+(isRegisted >= 0 ? 'Already signed up' : (isSent >= 0 ? 'Invitation sent' : ''))+'</span>'+
                            '</label>'+
                    '</div>'+
                '</div>';

            switch(parseInt(inSection)) {
                case 1:
                    $('#gmail .list').append(item);
                    break;
                case 2:
                    $('#outlook .list').append(item);
                    break;
                case 3:
                    $('#yahoo .list').append(item);
                    break;
                default:
                    break;
            }
            count++;
            inputClass = inputClass.substr(0, inputClass.lastIndexOf('-')+1);
        }
    });
    switch(parseInt(inSection)) {
        case 1:
            options.searchClass = 'search-gmail';
            gmailContactList = new List('gmail', options);
            break;
        case 2:
            options.searchClass = 'search-outlook';
            outlookContactList = new List('outlook', options);
            break;
        case 3:
            options.searchClass = 'search-yahoo';
            yahooContactList = new List('yahoo', options);
            break;
        default:
            break;
    }

    $("#processingPayment").hide();
}

// reset all data on current sesstion
function resetCurrentMailData(type) {
    switch(type) {
        case '1':
            gmailContactList.clear();
            $('#gmail-contacts-btn').addClass('gmail-contacts-btn');
            break;
        case '2':
            outlookContactList.clear();
            $('#outlook-contacts-btn').addClass('outlook-contacts-btn');
            break;
        case '3':
            yahooContactList.clear();
            $('#yahoo-contacts-btn').addClass('yahoo-contacts-btn');
            break;
        case 'all':
        default:
            gmailContactList.clear();
            outlookContactList.clear();
            yahooContactList.clear();

            $('#selected-email').empty();

            selectedEmail = Array();

            $('#gmail-contacts-btn').addClass('gmail-contacts-btn');
            $('#outlook-contacts-btn').addClass('outlook-contacts-btn');
            $('#yahoo-contacts-btn').addClass('yahoo-contacts-btn');

            $('#input-search').val('');
    }
}

// alert user we found nothing from their email
function alertFoundNothing() {
    $("#processingPayment").hide();

    alertBox('Sorry, we couldn\'t find any contact from your email', 'Oops!');
}

// alert user when unidentified error happen
function alertError(errMessage, type) {
    $('#processingPayment').hide();
    errMessage = (typeof errMessage !== 'undefined' || errMessage !== "") ? errMessage : "Sorry, we have some issue at the moment, please try again later";
    type = (typeof type !== 'undefined') ? type : 'all';

    alertBox(errMessage, 'Oops!', function(){
        resetCurrentMailData(type);
    });
    return false;
}

function alertNothingSelected() {
    $('#processingPayment').hide();

    alertBox('No email address selected.', 'Oops!');
    return false;
}

// alert user if email is already selected
function alertDuplicated() {
    alertBox('This email address is already selected. Please use another email address.', 'Oops!');
}

// alert user if email is already selected
function alertRegisted(type) {
    var alert_message = '';

    if(type == 1)
    {
        alert_message = 'This phone number is already registered.  Please use another phone number.';
    }
    else
    {
        alert_message = 'This email address is already registered.  Please use another email address.';
    }

    alertBox(alert_message,'REGISTERED');
}

// alert user if email is already sent invite
function alertSentEmail() {
    alertBox('This contact were invited before. Please use another contact.', 'Oops!');
}

// alert user if phone number is already sent invite
function alertSentSMS(dateDiff) {
    dateDiff = typeof dateDiff !== 'undefined' ? dateDiff : -1;
    var message = "Sorry, this number were invited by other person. Please use another number.";
    if (dateDiff >= 0) {
        message = "You can't send invite to the same number within 30 days. Please use another phone number. Blocked day remain: " + (30 - parseInt(dateDiff));
    }

    alertBox(message,'Oops!');
}

function alertSentSuccess() {
    $('#processingPayment').hide();

    alertBox('Your invitation has been sent!', 'Invite Friends');
    return false;
}

// handle google authentication, display authentication popup for user and then get the access token
function googleAuth() {
    // clear current data
    gmailContactList.clear();

    try {
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetGmailConfig&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType: 'json',
            timeout: ajaxTimeout,
            async: false
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                var config = {};
                config.client_id = data.CONFIG.CLIENT_ID;
                config.scope = data.CONFIG.SCOPE;
                // display authentication popup
                gapi.auth.authorize(config, function() {
                    // pass the access token to get contact function
                    if (gapi.auth.getToken() !== null) {
                        googleGetContacts(gapi.auth.getToken().access_token);
                    }
                });
            } else {
                alertError("", '1');
            }
        })
        .fail(function(e, msg) {
            alertError("", '1');
        });
    } catch (e) {
        alertError("", '1');
    }
}

// access google api to get user's contact
function googleGetContacts(accessToken) {
    // display process loading
    $("#processingPayment").show();

    try {
        // get contacts by goole contacts api, limit by 1000 result
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetUserGmailContacts&inpAccessToken='+accessToken+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType: "json",
            type: "POST",
            timeout: ajaxTimeout,
            success:function(data) {

                if (parseInt(data.RXRESULTCODE) == 1) {
                    var contacts = $.parseJSON(data.CONTACTS).feed.entry;
                    if (typeof contacts !== 'undefined' && contacts.length > 0) {
                        // extract email and name
                        var contactList = Array();
                        var mailList = Array();
                        $.each(contacts, function(index, val) {
                            // contacts found, make new list and reload
                            if('gd$email' in val) {
                                var item = [];
                                item['email'] = val.gd$email[0].address;
                                item['name'] = val.title.$t;
                                contactList.push(item);
                                mailList.push(item['email']);
                            }
                        });

                        var checkedList = checkListContactStatus(mailList);

                        if (parseInt(checkedList.RXRESULTCODE) != 1) {
                            throw new Error("Can't get information from server");
                        }

                        // reload contact list
                        reloadContactList(contactList, checkedList);

                        $('.gmail-contacts-btn').removeClass('gmail-contacts-btn');

                    } else {
                        // if nothing was found, display alert
                        alertFoundNothing();
                    }
                } else {
                    alertError("", '1');
                }
            },
            error: function (e) {
                alertError("", '1');
            }
        });
    } catch(ex) {
        alertError("", '1');
        return false;
    }
}

// handle windows authentication, display authorize popup and get access token
// this code is from https://github.com/radykal/instagram-popup-login
function windowsAuth () {
    // clear current data
    outlookContactList.clear();

    try {
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetOutlookAuthUrl&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType: 'json',
            timeout: ajaxTimeout,
            async: false
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                var getTokenURL = data.AUTHURL;

                // option for popup windows
                var popupWidth = 700,
                    popupHeight = 500,
                    popupLeft = (window.screen.width - popupWidth) / 2,
                    popupTop = (window.screen.height - popupHeight) / 2;

                // show popup, display callback uri page
                var popup = window.open(callbackURI, '', 'width='+popupWidth+',height='+popupHeight+',left='+popupLeft+',top='+popupTop+'');

                // handle popup action
                popup.onload = function() {
                    //open authorize url in pop-up
                    if(window.location.hash.length == 0) {
                        popup.open(getTokenURL, '_self');
                    }

                    //an interval runs to get the access token from the pop-up
                    var interval = setInterval(function() {
                        try {
                            // check if error occurred
                            if (popup.location.href.indexOf('error') >= 0) {
                                clearInterval(interval);
                                popup.close();
                            } else {
                                //check if hash exists
                                if(popup.location.hash.length) {
                                    //hash found, that includes the access token
                                    clearInterval(interval);

                                    // extract access token from return url
                                    var authHash = popup.location.hash.split('&');
                                    var accessToken = authHash[0].slice(14); //slice #access_token= from string
                                    var state = authHash[4].slice(6);
                                    popup.close();

                                    // pass access token to get contact function
                                    windowsGetContacts(accessToken, state);
                                }
                            }
                        }
                        catch(evt) {
                            //permission denied
                        }
                    }, 100);
                }
            } else {
                alertError("", '2');
            }
        })
        .fail(function(e) {
            alertError("", '2');
        });
    } catch (e) {
        alertError("", '2');
    }
}

// get user's outlook contact
function windowsGetContacts (accessToken, state) {
    // display process loading
    $("#processingPayment").show();

    try {
        // get contacts from outlook api with recived access token, select email only, limit 1000 result
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetUserOutlookContacts&inpAccessToken='+accessToken+'&inpState='+state+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType: 'json',
            timeout: ajaxTimeout
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                var contacts = $.parseJSON(data.CONTACTS).value;
                if (typeof contacts !== 'undefined' && contacts.length > 0) {
                    // contacts found, make new list and reload
                    var contactList = Array();
                    var mailList = Array();
                    $.each(contacts, function(index, val) {
                        if('EmailAddresses' in val && typeof val.EmailAddresses[0] !== 'undefined') {
                            var item = [];
                            item['email'] = val.EmailAddresses[0].Address;
                            item['name'] = val.EmailAddresses[0].Name;
                            contactList.push(item);
                            mailList.push(item['email']);
                        }
                    });

                    var checkedList = checkListContactStatus(mailList);

                    if (parseInt(checkedList.RXRESULTCODE) != 1) {
                        throw new Error("Can't get information from server");
                    }

                    // reload contact list
                    reloadContactList(contactList, checkedList);

                    $('.outlook-contacts-btn').removeClass('outlook-contacts-btn');
                } else {
                    // if nothing was found, display alert
                    alertFoundNothing();
                }
            } else {
                alertError("Sorry we can't get your contacts right now. Please try again later!", '2');
            }
        })
        .fail(function (e) {
            alertError("", '2');
        });
    } catch(ex) {
        alertError("", '2');
        return false;
    }
}


// handle yahoo authentication, display authorize popup and get access token
// this code is from https://github.com/radykal/instagram-popup-login
function yahooAuth () {
    // clear current data
    yahooContactList.clear();

    try {
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetYahooAuthUrl&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType: 'json',
            timeout: ajaxTimeout,
            async: false
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                var getTokenURL = data.AUTHURL;

                // option for popup windows
                var popupWidth = 700,
                    popupHeight = 500,
                    popupLeft = (window.screen.width - popupWidth) / 2,
                    popupTop = (window.screen.height - popupHeight) / 2;

                // open popup, display callback page
                var popup = window.open(callbackURI, '', 'width='+popupWidth+',height='+popupHeight+',left='+popupLeft+',top='+popupTop+'');

                // handle popup action
                popup.onload = function() {
                    //open authorize url in pop-up
                    if(window.location.hash.length == 0) {
                        popup.open(getTokenURL, '_self');
                    }

                    //an interval runs to get the access token from the pop-up
                    var interval = setInterval(function() {
                        try {
                            // check if error occurred
                            if (popup.location.href.indexOf('error') >= 0) {
                                clearInterval(interval);
                                popup.close();
                            } else {
                                //check if hash exists
                                if(popup.location.hash.length) {
                                    //hash found, that includes the access token
                                    clearInterval(interval);

                                    // extract authorize code from return url
                                    var authHash = popup.location.hash.split('&');
                                    var accessToken = authHash[0].slice(14); //slice #access_token= from string
                                    var state = authHash[3].slice(6); // slice #state= from string
                                    popup.close();

                                    // pass access token to get contact function
                                    yahooGetContacts(accessToken, state);
                                }
                            }
                        }
                        catch(evt) {
                            //permission denied
                        }
                    }, 100);
                }
            } else {
                alertError("", '3');
            }
        })
        .fail(function(e) {
            alertError("", '3');
        });
    } catch (e) {
        alertError("", '3');
    }
}

// get user's yahoo contacts
function yahooGetContacts (accessToken, state) {
    // display process loading
    $("#processingPayment").show();

    try{
        // call internal cfc function to get contacts
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetUserYahooContacts&inpAccessToken='+accessToken+'&inpState='+state+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            dataType:'json',
            timeout: ajaxTimeout,
            success: function(data) {
                if (parseInt(data.RXRESULTCODE) == 1) {
                    var contacts = $.parseJSON(data.CONTACTS);
                    if (typeof contacts !== 'undefined' && contacts.contacts.total > 0) {
                        // contact found, make new list and reload
                        var contactList = Array();
                        var mailList = Array();
                        $.each(contacts.contacts.contact, function(index, val) {
                            var fields = val.fields;
                            var item = [];
                            $.each(fields, function(index, val) {
                                if('type' in val) {
                                    if(val.type == 'email') {
                                        item['email'] = val.value;
                                    } else if (val.type == 'name') {
                                        item['name'] = (typeof val.value.givenName !== 'undefined' ? val.value.givenName : '');
                                    }
                                }
                            });
                            contactList.push(item);
                            mailList.push(item['email']);
                        });

                        var checkedList = checkListContactStatus(mailList);

                        if (parseInt(checkedList.RXRESULTCODE) != 1) {
                            throw new Error("Can't get information from server");
                        }

                        // reload contact list
                        reloadContactList(contactList, checkedList);

                        $('.yahoo-contacts-btn').removeClass('yahoo-contacts-btn');
                    } else {
                        // if nothing was found, display alert
                        alertFoundNothing();
                    }
                } else {
                    alertError("Sorry we can't get your contacts right now. Please try again later!", '3');
                }
            },
            error: function(e) {
                alertError("", '3');
            }
        });
    } catch(ex) {
        alertError("", '3');
        return false;
    }
}

// handle check and uncheck on email checkbox
$('body').on('change', '#refer_via_email .contact-checkbox', function(event) {
    if(this.checked) {
        // if action is check

        // get email from it's label
        var email = $(this).parent().parent().find('.email').text();
        if (typeof email !== 'undefined') {
            // get name from lable
            var name = $(this).parent().parent().find('.name').text();
            if (typeof name === 'undefined') {
                name = '';
            }
            // check if this email is already checked
            var isSelected = $('#selected-email').find("[data-email='"+email+"']");
            if (isSelected.length == 0) {
                // generate item html, contain data-email attribute for further handling
                var item = '<div class="selected-email-item" data-name="'+name+'" data-email="'+email+'">'+
                            '<div class="alert">'+email+'<a class="my-close"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                            '</div>'+
                        '</div>';
                // append on selected section
                $('#selected-email').append(item);

                // set data-email attribute to further handle
                $(this).attr('data-email', email);

                // check on other mail section if this email existed
                var duplicate = $("span.email:contains('"+email+"')");
                if (duplicate.length > 0) {
                    duplicate.each(function(index, el) {
                        if ($(this).text() === email) {
                            var inputCheckbox = $(this).parent().parent().parent().find(".contact-checkbox");
                            // check and set data-email in those checkbox too
                            inputCheckbox.prop('checked', true);
                            inputCheckbox.attr('data-email', email);
                        }
                    });
                }
            } else {
                $(this).attr('data-email', email);
                // alert user this email is already selected
                alertDuplicated();
            }
        }
    } else {
        // if action is uncheck

        // get email from label
        var email = $(this).parent().parent().find('.email').text();
        if (typeof email !== 'undefined') {
            // remove selected item from selected section
            $('#selected-email').find("[data-email='" + email + "']").remove();
            // uncheck all duplicate checkbox from other mail section
            $(".contact-checkbox[data-email='"+email+"']").prop('checked', false);
        }
    }
});

// handle remove action when click on selected item
$('body').on('click', 'div.selected-email-item .my-close', function(event) {
    // get email from it's parent data-email field
    var email = $(this).parent().parent().attr('data-email');
    if (typeof email !== 'undefined') {
        // remove itself
        $('#selected-email').find("[data-email='" + email + "']").remove();
        // uncheck all checkbox in contact list has the same email
        $(".contact-checkbox[data-email='"+email+"']").prop('checked', false);
    }
});

// handle manual email adding
$('body').on('click', '#add-email-btn', function(event) {
    if ($('#email_form').validationEngine('validate')) {
        // get email from input box
        var inputEmail = $('#emailadd').val().toLowerCase();
        // check if this email is already selected
        var isSelected = $('#selected-email').find("[data-email='"+inputEmail+"']");
        if (isSelected.length == 0) {

            // check email status, if is were sent invitation or already signed up
            var checkStatus = checkContactStatus(inputEmail, 2);
            var status = checkStatus.CONTACTSTATUS;
            switch(parseInt(status)) {
                case 0:
                    case 0:
                    // generate selected item html
                    var item = '<div class="selected-email-item" data-email="'+inputEmail+'">'+
                                    '<div class="alert">'+inputEmail+'<a class="my-close"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                                    '</div>'+
                                '</div>';
                    // append on selected section
                    $('#selected-email').append(item);
                    // clear old input
                    $('#emailadd').val('');

                    break;
                case 1:
                    alertRegisted(2);
                    break;
                case 2:
                    alertSentEmail();
                    break;
                default:
                    alertError("", 'all');
                    break;
            }
        } else {
            // alert user this email is already selected
            alertDuplicated();
        }
    }
});

function searchContacts() {
    var inputSearch = $('#input-search').val();
    // check inSection button to detect contact list
    switch(parseInt(inSection)) {
        case 1:
            var searchResult = gmailContactList.search(inputSearch);
            break;
        case 2:
            var searchResult = outlookContactList.search(inputSearch);
            break;
        case 3:
            var searchResult = yahooContactList.search(inputSearch);
            break;
        default:
            break;
    }

    if (searchResult.length === 0) {
        alertBox('No result found!','Oops!');
    }
}

// search current email list
$('body').on('click', '#search-btn', function(event) {
    searchContacts();
});

// search on enter key press
$('#input-search').keyup(function(event) {
    if (event.keyCode == 13) {
        searchContacts();
    }
});

// display confirm send popup
$('body').on('click', '#send-email-invitation-btn', function(event) {
    sendType = 2;
    // reset submit array
    selectedEmail = Array();
    // get all element in selected email section
    $('#refer_via_email div.selected-email .selected-email-item').each(function(index, el) {
        // get email and name from data attribute
        var data = {};
        data.name = (typeof $(this).attr('data-name') !== 'undefined' ? $(this).attr('data-name') : '');
        data.email = $(this).attr('data-email');
        selectedEmail.push(data);
    });

    if (typeof selectedEmail === 'undefined' || selectedEmail.length <= 0) {
        alertNothingSelected();
    } else {
        grecaptcha.reset();
        $('#captcha-message').text('');
                $('#ConfirmSendInvitation').modal('show');
     }
});

function checkUserInviteQuota(){
    var dataout = {};
    dataout.RXRESULTCODE = -1;
    try {
        $.ajax({
            url: '/session/sire/models/cfc/referral.cfc?method=GetSMSQuota&queryformat=column&_cf_nodebug=true&_cf_nocache=true&returnformat=json',
            type: 'POST',
            async: false,
            timeout: ajaxTimeout
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                dataout = data;
            } else {
                alertError("", "all");
            }
        })
        .fail(function(e) {
            alertError("", "all");
        });
    } catch (e) {
        alertError("", "all");
    }

    return dataout;
}

//add new phone number
function addNewPhoneNumber(){
    var phoneNumber = $('#phonenumber').val();
    var addNewNumber = '<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 selected-phone-item"> <div class="alert text-center" id="'+ phoneNumber +'"> '+ phoneNumber +'<a data-id="'+ phoneNumber +'" href="#" class="close">&times;</a></div></div>';
    //check duplicated phone number
    if (checkDuplicatedPhoneNumber(phoneNumber)){
        alertBox('Duplicated Phone Number! Please add another.', 'Oops!');

    }
    else{
        var checkUserQuota = checkUserInviteQuota();

        if (parseInt(checkUserQuota.RXRESULTCODE) == 1) {
            var quota = parseInt(checkUserQuota.SMSQUOTA);
            if (quota > 0) {
                $('#sms-quota-index').text(quota);
                $('#sms-quota').val(quota);
                // check contact status first
                var checkStatus = checkContactStatus(phoneNumber, 1);

                if (parseInt(checkStatus.RXRESULTCODE) == 1) {
                    switch(parseInt(checkStatus.CONTACTSTATUS)) {
                        case 0:
                            // get user sms quota
                            var sms_quota = parseInt($('#sms-quota').val());

                            // add new number to selected section
                            $('#selected-sms').append(addNewNumber);

                            // count selected number
                            var selected_sms = parseInt($('.selected-phone-item').size());

                            // if selectd number still less than sms quote, update quota index on status, if not show the message
                            var remain_quota = sms_quota - selected_sms;

                            if (remain_quota > 0) {
                                $('#sms-quota-index').text(remain_quota);
                                $('#sms-quota-status').removeAttr('style');
                            } else {
                                $('#sms-quota-status').css({color: 'red'}).text("You can't not add any phone number");
                                $('#phonenumber').prop('disabled', true);
                                $('#add-new-phone-btn').prop('disabled', true);
                            }
                            break;
                        case 1:
                            // alert user this contact is already signed up
                            alertRegisted(1);
                            resetSMSQuoteIndex();
                            break;
                        case 2:
                            // alert user this contact were invited
                            alertSentSMS(parseInt(checkStatus.DATEDIFF));
                            resetSMSQuoteIndex();
                            break;
                        case 3:
                            // alert user valid number format
                            alertError('Phone Number is not a valid US telephone number !', "all");
                            resetSMSQuoteIndex();
                            break;
                        case 4:
                            alertError('This phone number had otpout and does not want to receive marketing message. Please use another phone number', "all");
                            resetSMSQuoteIndex();
                            break;
                        default:
                            break;
                    }
                } else {
                    alertBox('Something went wrong! Please try again later.', 'Oops!');
                }
            } else {
                $('#sms-quota-status').css('color', 'red');
                $('#sms-quota-status').text('Sorry, You used all your sms invite quota');
                $('#phonenumber').prop('disabled', true);
                $('#add-new-phone-btn').prop('disabled', true);
                $('#btn-send-sms-invitation').prop('disabled', true);
            }

        }
    }
    $('#phonenumber').val('');
}

function resetSMSQuoteIndex() {
    var sms_quota = parseInt($('#sms-quota').val());
    var selected_sms = parseInt($('div.selected-email div.selected-phone-item').size());
    var remain_quota = sms_quota - selected_sms;
    $('#sms-quota-index').text(remain_quota);
}

//check duplicated phone number function
function checkDuplicatedPhoneNumber(phoneNumber) {
    var rsCheck = false;
    $('div.selected-email div.selected-phone-item').each(function(index){
        var phoneNum = $(this).find('div.alert').attr('id');
        if(phoneNum == phoneNumber){
            rsCheck = true;
            return false;
        }
    });
    return rsCheck;
}

//remove phone number
$('body').on('click', 'div.selected-phone-item .close', function(e){
    e.preventDefault();
    $(this).parent().parent().remove();

    // get user sms quota
    var sms_quota = parseInt($('#sms-quota').val());

    // count selected number
    var selected_sms = parseInt($('.selected-phone-item').size());

    var remain_quota = sms_quota - selected_sms;

    if (remain_quota == 1) {
        var html = 'You can invite <span id="sms-quota-index">'+remain_quota+'</span> more friends today';
        $('#sms-quota-status').removeAttr('style').html(html);
        $('#phonenumber').prop('disabled', false);
        $('#add-new-phone-btn').prop('disabled', false);
    } else {
        $('#sms-quota-index').text(remain_quota);
    }
});

 //validate US phone number
//$("#phonenumber").mask("(000)000-0000");
$("#add_new_phonenumber").validationEngine('attach', {
    promptPosition : "bottomLeft",
    scroll: false,
    focusFirstField : false,
    onValidationComplete: function(form, status){
        if(status === true){
            addNewPhoneNumber();
        }
    }
});

// handle sms invitation send button
$('#btn-send-sms-invitation').click(function(event){
    sendType = 1;
    var phones = [];
    var items = $('div.selected-email div.selected-phone-item');

    // check if any phone number is provided
    items.each(function(index){
        var phoneItem = {
            phone: $(this).find('div.alert').attr('id')
        }
        phones.push(phoneItem);
    });
    if(phones.length > 0){
        // reset and display captcha each submit
        grecaptcha.reset();
        $('#captcha-message').text('');

        $('#ConfirmSendInvitation').modal('show');
    } else {
        alertBox('No phone number selected.', 'Oops!');
    }
});

// submit phones number
$('#btn-send-invitation-confirm').click(function(event) {

    // prepare for display captcha error
    var message_forgot = $('#captcha-message').text('');

    // only if captcha was passed, data being submit to server
    if ($('#g-recaptcha-response').val() != '') {

        $('#ConfirmSendInvitation').modal('hide');

        var contacts = [];

        // get submit data for sms or email
        switch(parseInt(sendType)) {
            case 1:
                var items = $('div.selected-email div.selected-phone-item');

                items.each(function(index){
                    var phoneItem = {
                        phone: $(this).find('div.alert').attr('id')
                    }
                    contacts.push(phoneItem);
                });
                break;
            case 2:
                var contacts = selectedEmail;
                break;
            case 3:
                var friendSelected = [];
                $('#twitter-friend-list-selected >div').each(function(){
                    var item = {};
                    item.userId = $(this).data('friend-id');
                    item.userName = $(this).data('name');
                    friendSelected.push(item);
                });
                break;
            default:
                break;
        }

        try {
            $("#processingPayment").show();
            if (parseInt(sendType) != 3) {
                $.ajax({
                    type: 'POST',
                    url: '/session/sire/models/cfc/referral.cfc?method=SaveUserInvitation&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                    dataType: 'json',
                    timeout: ajaxTimeout,
                    data: {
                        inpType: sendType,
                        inpContacts: JSON.stringify(contacts)
                    },
                    success: function(data){
                        if(parseInt(data.RXRESULTCODE) > 0){
                            alertSentSuccess();

                            switch(parseInt(sendType)) {
                                case 1:
                                    // update sms quota
                                    var sms_quota = parseInt($('#sms-quota').val());
                                    var remain_quota = sms_quota - parseInt(contacts.length);

                                    if (remain_quota <= 0) {
                                        $('#btn-send-sms-invitation').prop('disabled', true);
                                    }

                                    $('#sms-quota').val(remain_quota);

                                    $('#selected-sms').html('');
                                    break;
                                case 2:
                                    resetCurrentMailData();
                                    break;
                                default:
                                    break;
                            }

                            $("#processingPayment").hide();
                        } else {
                            var sms_quota = parseInt($('#sms-quota').val());
                            $('#sms-quota-index').text(sms_quota);
                            sendType == 1 ? alertSMSSendError() : alertError();
                        }
                    },
                    error: function (e) {
                        var sms_quota = parseInt($('#sms-quota').val());
                        $('#sms-quota-index').text(sms_quota);
                        sendType == 1 ? alertSMSSendError() : alertError();
                    },
                    complete: function () {
                        $('#refer_via_email .friends-list').hide();
                    }
                });
            } else {
                sendMesage(friendSelected);
                twContactList.clear();
                $('#search-inp').val('');
            }
        } catch(e) {
            sendType == 1 ? alertSMSSendError() : alertError();
        }
    } else {
        // disply captcha error message
        message_forgot.text('Please click captcha');
    }
});

function alertSMSSendError() {
    $("#processingPayment").hide();
    $('#selected-sms').html('');
    var checkUserQuota = checkUserInviteQuota();
    if (parseInt(checkUserQuota.RXRESULTCODE) == 1) {
        var quota = parseInt(checkUserQuota.SMSQUOTA);
        if (quota > 0) {
            $('#sms-quota').val(quota);
            var html = 'You can invite <span id="sms-quota-index">'+quota+'</span> more friends today';
            $('#sms-quota-status').removeAttr('style').html(html);
            $('#phonenumber').prop('disabled', false);
            $('#add-new-phone-btn').prop('disabled', false);
        } else {
            $('#sms-quota-status').css('color', 'red');
            $('#sms-quota-status').text('Sorry, You used all your sms invite quota');
            $('#phonenumber').prop('disabled', true);
            $('#add-new-phone-btn').prop('disabled', true);
            $('#btn-send-sms-invitation').prop('disabled', true);
        }
        alertError("Sorry! You've exceeded your invitation limit");
    } else {
        alertError("Send invitation fail! Please try again later!");
    }
}

$('#emailadd').focus();

$(document).ready(function() {
    $(".cardholder_details").find('input, select').prop('disabled', true);
    $(".btn-update-profile").prop('disabled', true);
    $(".newbtn.green-cancel").prop('disabled', true);

    $("body").on('click', '.btn-update-profile, .newbtn.green-cancel', function(event) {
        event.preventDefault();
    });

    var modalAlert = function(title, message, event) {
        var bootstrapAlert = $('#bootstrapAlert');
        bootstrapAlert.find('.modal-title').text(title);
        bootstrapAlert.find('.alert-message').text(message);
        bootstrapAlert.modal('show');
    }

     $("#unscriber_keyword_form")
        .validationEngine({
            promptPosition: "topLeft",
            scroll: false,
            focusFirstField : false
        });
     $('#unscriber_keyword_form').submit(function(event){
        event.preventDefault();
        $('#UnscriberKeywordModal #Unsubscribe').click();
        }).validationEngine({
        promptPosition : "topLeft", 
        autoPositionUpdate: true, 
        showArrow: false, 
        scroll: false,
        focusFirstField : false
    });
    
    $('#UnscriberKeywordModal #Unsubscribe').click(function(){
        if ($('#unscriber_keyword_form').validationEngine('validate')) {
            var numberKeywordUnscriber = $('#numberKeywordUnscriber').val();
            $('#processingPayment').show();
            $.ajax({
                url: '/session/sire/models/cfc/order_plan.cfc?method=UnsubscribeKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {NUMBERKEYWORDUNSUBSCRIBE: numberKeywordUnscriber},
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                },
                success: function(data) {
                    //console.log(data);
                    $('#processingPayment').hide();
                    if (data && data.RXRESULTCODE == 1) {
                        $('#UnscriberKeywordModal').modal('hide');
                        location.reload();
                    } else {
                        modalAlert(data.MESSAGE);
                    }
                }
            });
            
        }
    });
});