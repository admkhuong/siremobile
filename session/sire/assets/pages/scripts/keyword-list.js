(function($){

	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	var currentPage = 0;

	function InitKeywordListDS(keyword){

		var keyword = typeof(keyword)!='undefined'?keyword:"";
		var actionCol = function(object){
			return $('<button class="btn-re-delete"><i class="fa fa-times" aria-hidden="true"></button>').attr('data-id', object.KEYWORDID)[0].outerHTML;

		}

		var table = $('#keywordListDetail').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
	            {"mData": "KEYWORD", "sName": 'Keyword', "sTitle": 'Keyword', "bSortable": false, "sClass": "", "sWidth": "125px"},
	            {"mData": "TIMEUSED", "sName": 'Number of Times Used', "sTitle": 'Number of Times Used', "bSortable": false, "sClass": "", "sWidth": "220px"},
	            {"mData": "CREATED", "sName": 'Date Created', "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "165px"},
	            {"mData": actionCol, "sName": 'Delete', "sTitle": 'Delete', "bSortable": false, "sClass": "", "sWidth": "100px"}
			],
			"sAjaxDataProp": "LISTKEYWORD",
			"sAjaxSource": '/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserId'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {		   		
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				
		        aoData.push(
		            { "name": "inpKeyword", "value": keyword}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data) {
			        	
			        	fnCallback(data);
			        	if(data.LISTKEYWORD.length == 0) {
			        		$('#keywordListDetail_paginate').hide();
			        	}
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){

				
			}
		});
	}


	InitKeywordListDS();

	$('body').on('click', 'button.btn-re-delete', function(event){
		var keywordId = $(this).data('id');

		confirmBox('Are you sure you want to delete this keyword? ', 'Delete Keyword', function(){
			$.ajax({
				url: "/session/sire/models/cfc/keywords.cfc?method=DeleteKeyword"+strAjaxQuery,
				type: "POST",
				dataType: "json",
				data: {inpKeywordId: keywordId},
				beforeSend: function(xhr){

				},
				success: function(data){
					if(data.RXRESULTCODE === 1){
						alertBox(data.MESSAGE, 'Confirmation', function(){
							currentPage = $('.paginate_text').val();
							$('#keywordListDetail_wrapper .paginate_text').keyup();
							LoadSelectKeyword();
						});
					} else {
						alertBox(data.ERRMESSAGE, 'Confirmation', function(){
							$('#keywordListDetail_wrapper .paginate_text').keyup();
							LoadSelectKeyword();
						});
					}
					//location.reload();
				}
			});
		});
	});

	function LoadSelectKeyword(){

		$.ajax({
			url: "/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserIdForSelect"+strAjaxQuery,
			dataType: "json",
			success: function(data){
				var subSelectBox = $('select[id="keyword-list-select"]');
				subSelectBox.html('<option value="">All</option>');
				if(data.RXRESULTCODE == 1){
					data.LISTKEYWORD.forEach(function(item){
						subSelectBox.append($('<option></option>').attr('value', item.KEYWORD).text(item.KEYWORD));
					});
				} else {
					// alertBox(data.MESSAGE, 'alert');
				}
			},
			complete: function(responseText){

			}
		});

	};

	LoadSelectKeyword();

	$('#keyword-list-select').on('change', function(){
		var keywordString = $(this).val();
		InitKeywordListDS(keywordString);

		var startDate = $('input[name="dateStart"]').data('date-start');
		var endDate = $('input[name="dateEnd"]').data('date-end');


		cb(startDate, endDate);

	});


	var start = moment().subtract(29, 'days');
    var end = moment();

    $('.reportrange').daterangepicker({
    	 "dateLimit": {
        	"years": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);


    function cb(start, end) {

        $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));


        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
        $('input[name="dateStart"]').data('date-start', start);
        $('input[name="dateEnd"]').data('date-end', end);
        var inpKeyword = $('#keyword-list-select').val();
        $.ajax({
            url: "/session/sire/models/cfc/keywords.cfc?method=GetStatistic"+strAjaxQuery,
            method: "POST",
            dataType: "json",
            data: {
            	inpKeyword: inpKeyword,
                inpStartDate: dateStart,
                inpEndDate: dateEnd
            },
            success: function(data){
                var series = [];
                var months = [];
                // var window_with = $( window ).width();
                // var set_paddingBottom = 20;

                // if(window_with < 1351)
                // {
                // 	var set_paddingBottom = 40;
                // }

                data.DataList.forEach(function(item){
                    var dayData = '';
					switch(item.format){
                    	case "day": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData = moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "week": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData = moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "month": {
                    		months.push(moment(item.Time).format('MMM DD YYYY'));
                    		dayData = moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    }
                    series.push({meta: dayData, value: item.Value});
                    
                });

                var chart_data = {labels: months,series:[ series]};
                /* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */

				var options = {
					//fullWidth: true,
					chartPadding: {
						right: 60,
						// bottom: set_paddingBottom
					},
					axisY: {
						showGrid: false,
						offset: 20,
						onlyInteger: true,
						position: 'start',
						labelOffset: {
					      x: 0,
					      y: 0
					    }
					},
					axisX: { 
						labelOffset: {
					      x: 1,
					      y: 1
					    }
					},
					low: 0,
					height: '300px',
					plugins: [
					    Chartist.plugins.tooltip({
					    	currency: 'Times used: ',
						    class: 'chart-tooltip-custom'
					    })
					],
					lineSmooth: false,
					classNames: {
					    chart: 'ct-chart-line',
					    //label: 'ct-label-custom',
					    labelGroup: 'ct-labels',
					    series: 'ct-series',
					    line: 'ct-line',
					    point: 'ct-point',
					    area: 'ct-area',
					    grid: 'ct-grid-custom',
					    gridGroup: 'ct-grids',
					    gridBackground: 'ct-grid-background',
					    vertical: 'ct-vertical',
					    horizontal: 'ct-horizontal',
					    // start: 'ct-start',
					    // end: 'ct-end'
					 }
				};

                new Chartist.Line('#highchart', chart_data, options);


            }
        });
    }

    cb(start, end);

    $(".Select2").select2(
	{ theme: "bootstrap", width: 'auto'}
	);

    ChangeShortCodeCallback = function () {
        InitKeywordListDS();

		var startDate = $('input[name="dateStart"]').data('date-start');
		var endDate = $('input[name="dateEnd"]').data('date-end');

		cb(startDate, endDate);
	}

})(jQuery);

