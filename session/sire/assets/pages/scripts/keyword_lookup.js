(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (keyword){
        $("#keywordlist").html('');

        var showKeyword = function (object) {
            return "<a href='#' class='show_keyword_lookup' data-batch-id='"+ object.BatchId_bi+"' data-user-id = '"+object.UserId_int+"'>"+object.Keyword_vch+"</a>";
        }

        var table = $('#keywordlist').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
            {"mData": showKeyword, "sName": 'Keyword', "sTitle": 'Keyword', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "UserName_vch", "sName": 'UserName', "sTitle": 'UserName', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "UserId_int", "sName": 'UserId', "sTitle": 'UserId', "bSortable": false, "sClass": "", "sWidth": "150px"},
            {"mData": "Created_dt", "sName": 'Created', "sTitle": 'Used Date', "bSortable": false, "sClass": "", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/keyword-lookup.cfc?method=getListKeyword'+strAjaxQuery,
            //this fn is used for filtering data
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {

                aoData.push(
                    { "name": "inpKeyword", "value": keyword}
                    );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,

                    "beforeSend": function() {
                        $('#processingPayment').show();
                    },

                    "success": function(data) {
                       $('#processingPayment').hide();
                       fnCallback(data);
                   }
               });
            },
            "fnHeaderCallback": function( thead, data, start, end, display, object) {
                $(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
            },
        });
    }

    $('#keywordlist').on('click', ".show_keyword_lookup", function(){
        var batchId = $(this).data('batch-id');
        var userId = $(this).data('user-id');
        try{
            $.ajax({
                type: "GET",
                url: '/session/sire/models/cfm/template_preview.cfm?templateid='+batchId+'&templateFlag=0&userId='+userId,
                beforeSend: function( xhr ) {
                    $('#processingPayment').show();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message: "Get preview Fail",
                        title: "Campaigns",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                },
                success:function(d){

                    if(d.indexOf('class="home_page"') > -1){
                        return false;
                    }

                    $('#processingPayment').hide();
                    $('#previewCampaignModal .modal-body').html(d);
                    $('#previewCampaignModal').modal('show');
                }
            });
        }catch(ex){
            $('#processingPayment').hide();
            bootbox.dialog({
                message: "Get preview Fail",
                title: "Campaigns",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
        }
    });



    /* Search keyword */
    $('input[type=search]').on('search', function () {
        var keyword = $(this).val();
        fnGetDataList(keyword);
    });

    $('#btnKeyWordLookUp').on('click',function(){
        var keyword = $('input[type=search]').val();
        fnGetDataList(keyword);
    });

    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();
    });

})();