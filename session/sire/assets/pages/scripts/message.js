(function($){
	/*Ajax query string default param*/
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	var fnGetDataList = function (searchMessage){
		var actionBtn = function (object) {
			var strReturn = '<a href="/session/sire/models/cfc/admin-tool.cfc?method=DeleteUserMessageById&inpMessageID='+object.ID+'" class="delete-message-link">Delete</a>';
			return strReturn;
		}

		var table = $('#user-message-list').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
			"aoColumns": [
				{"mData": "Content_vch", "sName": 'Content_vch', "sTitle": 'Message', "bSortable": false, "sClass": "", "sWidth": ""},
				{"mData": "Created_dt", "sName": 'Created_dt', "sTitle": 'Message', "bSortable": true, "sClass": "", "sWidth": ""},
				{"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
			],
			"sAjaxDataProp": "aaData",
			"sAjaxSource": '/session/sire/models/cfc/admin-tool.cfc?method=getMessagesByUserSignIn' + strAjaxQuery,
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {
				aoData.push(
					{ "name": "inpSearchMessage", "value": searchMessage}
				);

				oSettings.jqXHR = $.ajax({
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function(data) {
						fnCallback(data);
					}
				});
			}            
		});
	}

	fnGetDataList();

	$( "#searchbox" ).on( "keypress", function(e) { 
		if (e.keyCode == 13) {
			fnGetDataList($( this ).val()); 
		}
	});

	$("#user-message-list").on("click", ".delete-message-link", function(event){
		event.preventDefault();
		var delUrl = $(this).attr('href');
		confirmBox('Are you sure you want to delete this message?', 'Delete a message', function(){
			$.ajax({
				url: delUrl+strAjaxQuery,
				dataType: "json",
				success: function(data){
					if(data && data.MESSAGE && data.MESSAGE != '') {
						alertBox(data.MESSAGE, 'Delete a message');
					}

					if(data && data.RXRESULTCODE && data.RXRESULTCODE == 1) {
						$("#user-message-list_paginate .paginate_text").trigger(jQuery.Event('keyup', { keycode: 13 }));
					}
				}
			});
		});
	});

})(jQuery);