<!--- Experiment: Store js as .cfm so we can have better code documentation comments - cf comments are removed in final served page automatically --->
<!--- Also able to include parameters passed into the page as <cfoutput>#XXX#</cfoutput> control values in the js --->


<script type="text/javascript">

	<!--- Used to clone MLP on changes - can be undone --->
	var UndoArray = new Array();
	var RedoArray = new Array();

	var maxFilesize = "<cfoutput>#_IMGMAXSIZE#</cfoutput>";
	var RegName = new RegExp("^[a-zA-Z0-9_()-]+$");

	<!--- Not sure if this is valid or needed - where did it come from? Why is it used? --->
	var isAjax = 0;

	<!--- Options to clean up code for coder view - http://lovasoa.github.io/tidy-html5/ --->
	<!--- http://tidy.sourceforge.net/docs/quickref.html --->
	  var TidyOptions = {
	  "indent":"auto",
	  "indent-spaces":5,
	  "wrap":500,
	  "markup":true,
	  "output-xml":false,
	  "numeric-entities":true,
	  "quote-marks":true,
	  "quote-nbsp":false,
	  "show-body-only":true,
	  "quote-ampersand":false,
	  "break-before-br":true,
	  "uppercase-tags":false,
	  "uppercase-attributes":false,
	  "drop-font-tags":false,
	  "tidy-mark":false	,
	  "show-warnings":false,
	  "quiet" : true ,
	  "drop-empty-elements": false, <!--- there is a new version of the docs somewhere - http://stackoverflow.com/questions/18372185/html-tidy-removes-empty-tags-such-as-i-class-icon-foo-i --->
	  "drop-empty-paras": false,
	  "merge-divs": false,
	  "merge-spans": false,
	  'new-blocklevel-tags':'qqq_script,style,article aside audio bdi canvas details dialog figcaption figure footer header hgroup main menu menuitem nav section source summary template track video',
	  'new-empty-tags' : 'qqq_script,style,command embed keygen source track wbr',
	  'new-inline-tags' : 'style,audio command datalist embed keygen mark menuitem meter output progress source time video wbr'

	}

	<!--- not ideal.... --->
	var ClearFixSwitchery;

	$( function() {

		// Select all elements with data-toggle="popover" in the document - mlp-sim
		$('[data-toggle="popover-manual"]').popover({trigger:'manual'});

		<!--- Hide manual pop-overs --->
		$(document).on('click touchstart', function (e) {

		    $('[data-toggle="popover-manual"],[data-original-title]').each(function () {
		        //the 'is' for buttons that trigger popups
		        //the 'has' for icons within a button that triggers a popup
		    //    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
		            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
		    //    }

		    });
		});

		<!--- Set up all the select2 boxes --->
		$(".Select2").select2( { theme: "bootstrap"} );

		$("#btn-upload-image").prop("disabled", true);

		<!--- hide these buttons by default --->
		$('#btn-use-image-link').hide();

		<!--- inplace editing of description  see http://jsfiddle.net/egstudio/aFMWg/1/ --->
		var replaceWith = $('<input name="temp" type="text" style="color: #000; display: inline; width: 100%; box-sizing: border-box; text-align:right; padding-right:2em;" />'),
		connectWith = $('input[name="cppxName"]');

		$('#cppxNameDisplay').inlineEdit(replaceWith, connectWith);

		<!--- Only load the image browser when it is clicked --->
		$('a[data-toggle="MLPImagePicker"]').on("click touchstart", function(e){
		    e.preventDefault()
		    var loadurl = $(this).attr('href')
		    var targ = $(this).attr('data-target')


		    $.get(loadurl, function(data) {
		        $(targ).html(data)

		    });

		    $('#btn-upload-image').hide();
		    $('#btn-use-image-link').hide();

		    $(this).tab('show')

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('a[data-toggle="tab"]').on("click touchstart", function(e){

		    $('#btn-upload-image').show();
		    $('#btn-use-image-link').hide();

		    $(this).tab('show')

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('a[data-toggle="tab3"]').on("click touchstart", function(e){

		    $('#btn-upload-image').hide();
		    $('#btn-use-image-link').show();

		    var $TargetObj = $('#ImageChooserModal').data('data-target');

			<!--- make sure this obj exists --->
			if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
			{
				$('#MLPImageURL').val($TargetObj.find('img').attr('src'));
			}
			else
			{
				var bg_url = $TargetObj.css('background-image');
			    // ^ Either "none" or url("...urlhere..")
			    bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
			    bg_url = bg_url ? bg_url[2] : ""; // If matched, retrieve url, otherwise ""

				$('#MLPImageURL').val(bg_url);
			}

		    $(this).tab('show')

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('#btn-use-image-link').on("click touchstart", function(e){

		  	<!--- Store last state of the MLP --->
			StoreMLPBeforeAction(true);

		  	<!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
			var $TargetObj = $('#ImageChooserModal').data('data-target');

			<!--- make sure this obj exists --->
			if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
			{
				$TargetObj.find('img').attr('src', $('#MLPImageURL').val())
			}
			else
			{
				$TargetObj.css('background-image', 'url(' + $('#MLPImageURL').val() + ')');
				$('#mlp-section-settings #mlp-background-image-url').html($('#MLPImageURL').val());
			}

			$('#ImageChooserModal').modal('hide');

		  	<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- Bind draggable to widgets only --->
		$('.draggable').each(function( index ) {

			<!--- For tile-item menu items - use clone. Everyhting else use special indicators --->
			if( $(this).hasClass("tile-item")   )
			{	$(this).draggable(
				   {
					    appendTo: '#mlp-builder',
					    revert: "invalid",
						containment: "window",
						stack: "#mlp-builder",
						opacity: 0.8,
						helper: "clone",
						zIndex: 300,
						cursor: "move"
					}
			    );
			}
			else
			{

			}

		});


	    <!--- initialize all the jquery bound objects here so it can be called when coder modal or Undo updates anything --->
	    InitEditorStuff();


	    $('#Undo').on("click", function(e){

		  	if(UndoArray.length > 0)
		  	{
			  	<!--- Allow Redo of Undo Evens --->
			  	StoreMLPBeforeUndo();

			  	<!--- Remove the current version of main-stage-content --->
			  	$('#main-stage-content').empty();

			  	var UndoObj = UndoArray.pop();

		  		<!--- Pop the last version of the MLP from an array --->

		  		<!--- When I am bored I need to figure out why the clone(true, true) is not restoring events on append later on. I am just re-init events manually and only using the HTML for now --->
<!---
		  		//  UndoObj.CopiedDataAndEvents.appendTo('#main-stage-content');
		  		// $('#main-stage-content').replaceWith(UndoObj.CopiedDataAndEvents);
--->
		  		$('#main-stage-content').html(UndoObj.HTML);

		  		InitEditorStuff();

		  		<!--- Cleanup setting bar - obj no longer exists technically --->
		  		$('#mlp-section-settings').data('data-target', null);
		  		$('.side-nav .widgets').click();
		  		$(".SettingsOverlay").remove();
		  	}
		  	else
		  	{
				document.execCommand('undo', false, null);
		  	}

		});

		$('#Redo').on("click", function(e){

		  	if(RedoArray.length > 0)
		  	{
			  	StoreMLPBeforeAction(false);

			  	<!--- Remove the current version of main-stage-content --->
			  	$('#main-stage-content').empty();

			  	var RedoObj = RedoArray.pop();

		  		<!--- Pop the last version of the MLP from an array --->

		  		<!--- When I am bored I need to figure out why the clone(true, true) is not restoring events on append later on. I am just re-init events manually and only using the HTML for now --->
<!---
		  		//  RedoObj.CopiedDataAndEvents.appendTo('#main-stage-content');
		  		// $('#main-stage-content').replaceWith(RedoObj.CopiedDataAndEvents);
--->
		  		$('#main-stage-content').html(RedoObj.HTML);

		  		InitEditorStuff();

		  		<!--- Cleanup setting bar - obj no longer exists technically --->
		  		$('#mlp-section-settings').data('data-target', null);
		  		$('.side-nav .widgets').click();
		  		$(".SettingsOverlay").remove();
		  	}
		  	else
		  	{
			  	document.execCommand('redo', false, null);
		  	}

		});

		$('#SaveContent').on("click touchstart", function(e){

			SaveMLP(false);

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- Close side panel option --->
		$('.panel-close-button').on("click touchstart", function(e){

			$('.sidebar-second').hide(400);
			$('.menu-item').removeClass('panel-active');

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- Show Widgets panel --->
		$('.side-nav .widgets').on("click touchstart", function(e){

			$('.menu-item').removeClass('panel-active');
			$(this).addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('#mlp-section-settings').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-history-panel').hide();
		  	$('#mlp-toc-panel').hide();
		  	$('.widgets-panel').show();
		  	$('.sidebar-second').show(400);

		  	<!--- Esoteric feature/bug if I do not do this -> This is to prevent a new object being dropped and then deleted from deleting old section that was previously selected --->
			$('#mlp-section-settings').data('data-target', null);

		});

		<!--- Show Coder panel --->
		$('.side-nav .styles').on("click touchstart", function(e){

			$('.menu-item').removeClass('panel-active');
			$(this).addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-section-settings').hide();
		  	$('#mlp-history-panel').hide();
		  	$('#mlp-toc-panel').hide();

		  	<!--- Copy raw code to object --->
			$('#inpRawHTML').val($('#main-stage-content').html());

		  	$('.coder-panel').show();
		  	$('.sidebar-second').show(400);

		});

		<!--- Show Publish panel --->
		$('.side-nav .publish-mlp').on("click touchstart", function(e){

			$('.menu-item').removeClass('panel-active');
			$(this).addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-section-settings').hide();
		  	$('#mlp-history-panel').hide();
		  	$('#mlp-toc-panel').hide();
		  	$('.publish-mlp-panel').show();
		  	$('.sidebar-second').show(400);

		});

		<!--- Show History panel --->
		$('.side-nav .mlp-history').on("click touchstart", function(e){

			$('.menu-item').removeClass('panel-active');
			$(this).addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-section-settings').hide();
		  	$('.publish-mlp-panel').hide();
		  	$('#mlp-toc-panel').hide();
		  	$('#mlp-history-panel').show();
		  	$('.sidebar-second').show(400);

		});

		<!--- Show TOC panel --->
		$('.side-nav #mlp-toc').on("click touchstart", function(e){

			$('.menu-item').removeClass('panel-active');
			$(this).addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-section-settings').hide();
		  	$('.publish-mlp-panel').hide();
		  	$('#mlp-history-panel').hide();
		  	$('#mlp-toc-panel').show();
		  	$('.sidebar-second').show(400);

		});

		<!--- Toggle Setting for Background Image options --->
		$('#background-image-toggle').on("click touchstart", function(e){
		    $('#background-image-toggle .fa-caret-square-down').toggleClass( "active" );
		    $('#background-image-section').slideToggle('slow');

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- Open by default --->
		$('#background-image-toggle .fa-caret-square-down').toggleClass( "active" );
	    $('#background-image-section').slideToggle('slow');

		<!--- Toggle Setting for Background Image options --->
		$('#width-height-toggle').on("click touchstart", function(e){
		    $('#width-height-toggle .fa-caret-square-down').toggleClass( "active" );
		    $('#width-height-section').slideToggle('slow');

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- Open by default --->
		$('#width-height-toggle .fa-caret-square-down').toggleClass( "active" );
	    $('#width-height-section').slideToggle('slow');

		<!--- Coder setion - allow editing of raw HTML for advanced users --->

		$('#CoderModal').on('show.bs.modal', function () {

		    $('#CoderModal').find('.modal-dialog').css({
		              width:'auto', //probably not needed
		              height:'auto', //probably not needed
		              'max-height':'100%',
		              'max-width':'90%'
		       });

		    $(this).find('.modal-body').css({
		        'max-height':'100%'
		    });

		});

		$('#mlp-coder').on("click", function(e){

			var result = tidy_html5($('#main-stage-content').html(), TidyOptions);

			$('#RawCode').val(result);

			<!--- Show modal with for raw code --->
			$('#CoderModal').modal('show');

		});

		$('#btn-save-raw').on("click", function(e){

			<!--- Store removal target in global variable so it can be undone --->
			StoreMLPBeforeAction(true);

			<!--- To avoid memory leaks, jQuery removes other constructs such as data and event handlers from the child elements before removing the elements themselves. --->
			$('#main-stage-content').empty();

			<!--- Copy raw code to object --->
			$('#main-stage-content').html($('#RawCode').val());

			<!--- Copy raw code to object --->
			$('#inpRawHTML').val($('#RawCode').val());

			<!--- Re-initialize all the jquery bound objects --->
			InitEditorStuff();

		});

		$('#PublishMLP').on("click touchstart", function(e){

		  	try
			{

			  if(isAjax == 1) return false;
		      $.ajax({
		        type: "POST",
		        url: '/session/sire/models/cfc/mlp.cfc?method=PublishMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		        dataType: 'json',
		        data: {MLPId:$('#ccpxDataId').val()},
		        beforeSend: function( xhr ) {
		            $('#processingPayment').show();
		            isAjax = 1;
		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
		            $('#processingPayment').hide();
		            bootbox.dialog({
		                message:'Publish MLP Failed. Please try again later.',
		                title: "Publish MLP Error",
		                buttons: {
		                    success: {
		                        label: "Ok",
		                        className: "btn btn-medium btn-success-custom",
		                        callback: function() {}
		                    }
		                }
		            });
		            isAjax = 0;
		        },
		        success:
		            function(d) {
		            	isAjax = 0;
		                $('#processingPayment').hide();
		                if(d.RXRESULTCODE == 1)
		                {
			                bootbox.dialog({
		                        message:'MLP is now Published.',
		                        title: "Publish MLP",
		                        buttons: {
		                            success: {
		                                label: "OK",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() { UpdateSaveCountSinceLastPublished(); }
		                            }
		                        }
		                    });
		                }
		                else
		                {
		                    bootbox.dialog({
		                        message: d.MESSAGE,
		                        title: "Publish MLP Error",
		                        buttons: {
		                            success: {
		                                label: "Ok",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() {}
		                            }
		                        }
		                    });
		                }
		            }
		        });
		    }
		    catch(ex)
		    {
		        $('#processingPayment').hide();
		        bootbox.dialog({
		            message:'Publish MLP Failed',
		            title: "Publish MLP Error",
		            buttons: {
		                success: {
		                    label: "Ok",
		                    className: "btn btn-medium btn-success-custom",
		                    callback: function() {}
		                }
		            }
		        });

		    }

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('#RevertHistorical').on("click touchstart", function(e){

		  	try
			{
		      $.ajax({
		        type: "POST",
		        url: '/session/sire/models/cfc/mlp.cfc?method=RevertMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		        dataType: 'json',
		        data: {MLPId:$('#ccpxDataId').val(), inpHDT: $('#inpHDT').val()},
		        beforeSend: function( xhr ) {
		            $('#processingPayment').show();

		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
		            $('#processingPayment').hide();
		            bootbox.dialog({
		                message:'Revert MLP Failed. Please try again later.',
		                title: "Revert MLP Error",
		                buttons: {
		                    success: {
		                        label: "Ok",
		                        className: "btn btn-medium btn-success-custom",
		                        callback: function() {}
		                    }
		                }
		            });

		        },
		        success:
		            function(d) {

		                $('#processingPayment').hide();
		                if(d.RXRESULTCODE == 1)
		                {

			                <!--- Reload page --->


			                bootbox.dialog({
		                        message:'MLP is now Reverted.',
		                        title: "Revert MLP",
		                        buttons: {
		                            success: {
		                                label: "OK",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() { location.reload();		 }
		                            }
		                        }
		                    });
		                }
		                else
		                {
		                    bootbox.dialog({
		                        message: d.MESSAGE,
		                        title: "Revert MLP Error",
		                        buttons: {
		                            success: {
		                                label: "Ok",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() {}
		                            }
		                        }
		                    });
		                }
		            }
		        });

		    }
		    catch(ex)
		    {
		        $('#processingPayment').hide();
		        bootbox.dialog({
		            message:'Revert MLP Failed',
		            title: "Revert MLP Error",
		            buttons: {
		                success: {
		                    label: "Ok",
		                    className: "btn btn-medium btn-success-custom",
		                    callback: function() {}
		                }
		            }
		        });

		    }

		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		$('#CSSModal').on('show.bs.modal', function () {

		    $('#CSSModal').find('.modal-dialog').css({
		              width:'auto', //probably not needed
		              height:'auto', //probably not needed
		              'max-height':'100%',
		              'max-width':'90%'
		       });

		    $(this).find('.modal-body').css({
		        'max-height':'100%'
		    });


		});

		$('#RawCSSEdit').on("click touchstart", function(e){

			$('#RawCSS').val($('#inpRawCSS').val());

			<!--- Show modal with for raw code --->
			$('#CSSModal').modal('show');

		});

		$('#btn-save-raw-css').on("click touchstart", function(e){

			<!--- Copy raw code to object --->
			$('#inpRawCSS').val($('#RawCSS').val());

		});


		$('#Meta-Tag-Editor-Modal').on('show.bs.modal', function () {

		    <!--- Copy raw code to object --->
			var CSSObj = "<div>" +  $('#inpRawCSS').val() + "</div>";

		    <!--- Facebook --->
		    $('#og-title').val($(CSSObj).find("meta[property='og:title']").attr('content')) ;
			$('#og-description').val($(CSSObj).find("meta[property='og:description']").attr('content')) ;
			$('#og-name').val($(CSSObj).find("meta[property='og:name']").attr('content')) ;
		    $('#og-image').val($(CSSObj).find("meta[property='og:image']").attr('content')) ;
 	        $('#og-type').val($(CSSObj).find("meta[property='og:type']").attr('content')) ;
 			$('#og-locale').val($(CSSObj).find("meta[property='og:locale']").attr('content')) ;
		    $('#og-id').val($(CSSObj).find("meta[property='og:id']").attr('content')) ;

		    <!--- Google and SEO --->
		    $('#google-title').val($(CSSObj).find("title").text());
		    $('#google-description').val($(CSSObj).find("meta[property='description']").attr('content')) ;
		    $('#google-author').val($(CSSObj).find("meta[rel='author']").attr('content')) ;
		    $('#google-publisher').val($(CSSObj).find("meta[rel='publisher']").attr('content')) ;
		    $('#google-copyright').val($(CSSObj).find("meta[name='copyright']").attr('content')) ;

		    <!--- twitter --->
			$('#twitter-card').val($(CSSObj).find("meta[name='twitter:card']").attr('content')) ;
			$('#twitter-title').val($(CSSObj).find("meta[name='twitter:title']").attr('content')) ;
			$('#twitter-description').val($(CSSObj).find("meta[name='twitter:description']").attr('content')) ;
			$('#twitter-name').val($(CSSObj).find("meta[name='twitter:name']").attr('content')) ;
			$('#twitter-url').val($(CSSObj).find("meta[name='twitter:url']").attr('content')) ;
			$('#twitter-image').val($(CSSObj).find("meta[name='twitter:image']").attr('content')) ;
			$('#twitter-image-alt').val($(CSSObj).find("meta[name='twitter:image:alt']").attr('content')) ;

		});


		$('#btn-save-meta-tags').on("click touchstart", function(e){

			<!--- Copy raw code to object --->
			var CSSObj = $("<div>" +  $('#inpRawCSS').val() + "</div>");


			<!--- Set meta tags based on form values --->

			<!--- facebook --->

			<!--- Set defaults for empty --->
			if($('#og-locale').val() == "")
				$('#og-locale').val("en_US");

			<!--- Self repair - add back in tag on save - can be deleting in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:title']").length > 0)
				CSSObj.find("meta[property='og:title']").attr('content', $('#og-title').val());
			else
			{
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.prepend('<meta property="og:title" content="' + $('#og-title').val() + '">');
				CSSObj.prepend('\n<!-- for Facebook -->\n');
			}
			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:description']").length > 0)
				CSSObj.find("meta[property='og:description']").attr('content', $('#og-description').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:title']").after('\n<meta property="og:description" content="' + $('#og-description').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:name']").length > 0)
				CSSObj.find("meta[property='og:name']").attr('content', $('#og-name').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:description']").after('\n<meta property="og:name" content="' + $('#og-name').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:image']").length > 0)
				CSSObj.find("meta[property='og:image']").attr('content', $('#og-image').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:name']").after('\n<meta property="og:image" content="' + $('#og-image').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:type']").length > 0)
				CSSObj.find("meta[property='og:type']").attr('content', $('#og-type').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:name']").after('\n<meta property="og:type" content="' + $('#og-type').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:locale']").length > 0)
				CSSObj.find("meta[property='og:locale']").attr('content', $('#og-locale').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:image']").after('\n<meta property="og:locale" content="' + $('#og-locale').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[property='og:id']").length > 0)
				CSSObj.find("meta[property='og:id']").attr('content', $('#og-id').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[property='og:locale']").after('\n<meta property="og:id" content="' + $('#og-id').val() + '">\n\n');

			<!--- twitter --->
			<!--- Self repair - add back in tag on save - can be deleting in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:card']").length > 0)
				CSSObj.find("meta[name='twitter:card']").attr('content', $('#twitter-card').val());
			else
			{
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.prepend('<meta name="twitter:card" content="' + $('#twitter-card').val() + '">\n');
				CSSObj.prepend('\n\n<!-- for twitter -->\n');
			}

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:title']").length > 0)
				CSSObj.find("meta[name='twitter:title']").attr('content', $('#twitter-title').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:card']").after('\n<meta name="twitter:title" content="' + $('#twitter-title').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:description']").length > 0)
				CSSObj.find("meta[name='twitter:description']").attr('content', $('#twitter-description').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:title']").after('\n<meta name="twitter:description" content="' + $('#twitter-description').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:name']").length > 0)
				CSSObj.find("meta[name='twitter:name']").attr('content', $('#twitter-name').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:title']").after('\n<meta name="twitter:name" content="' + $('#twitter-name').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:url']").length > 0)
				CSSObj.find("meta[name='twitter:url']").attr('content', $('#twitter-url').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:name']").after('\n<meta name="twitter:url" content="' + $('#twitter-url').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:image']").length > 0)
				CSSObj.find("meta[name='twitter:image']").attr('content', $('#twitter-image').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:url']").after('\n<meta name="twitter:image" content="' + $('#twitter-image').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='twitter:image:alt']").length > 0)
				CSSObj.find("meta[name='twitter:image:alt']").attr('content', $('#twitter-image-alt').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='twitter:image']").after('\n<meta name="twitter:image:alt" content="' + $('#twitter-image-alt').val() + '">');

			<!--- Google --->
			<!--- Self repair - add back in tag on save - can be deleting in coder-advanced editor --->
			if(CSSObj.find("meta[name='description']").length > 0)
				CSSObj.find("meta[name='description']").attr('content', $('#google-description').val());
			else
			{
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.prepend('<meta name="description" content="' + $('#google-description').val() + '">');
				CSSObj.prepend('<!-- for Google -->\n');
			}

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("title").length > 0)
				CSSObj.find("title").text($('#google-title').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='description']").before('\n<title>' + $('#google-title').val() + '</title>\n');


			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[rel='author']").length > 0)
				CSSObj.find("meta[rel='author']").attr('content', $('#google-author').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[name='description']").after('\n<meta rel="author" content="' + $('#google-author').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[rel='publisher']").length > 0)
				CSSObj.find("meta[rel='publisher']").attr('content', $('#google-publisher').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[rel='author']").after('\n<meta rel="publisher" content="' + $('#google-publisher').val() + '">');

			<!--- Self repair - add back in tag on save - can be deleted in coder-advanced editor --->
			if(CSSObj.find("meta[name='copyright']").length > 0)
				CSSObj.find("meta[name='copyright']").attr('content', $('#google-copyright').val());
			else
				<!--- Create a default obj - use a \n for newline instead of html <BR/> to make it more readable in editor --->
				CSSObj.find("meta[rel='publisher']").after('\n<meta name="copyright" content="' + $('#google-copyright').val() + '">');


			<!--- Set data in RawCSS to new content --->
			$('#inpRawCSS').val(CSSObj.html());

		});

		SetupSectionSettingsPanel();

		$('#section-delete').darkTooltip({
			content: 'Are you sure?',
			trigger: 'click',
			modal: false,
			animation:'flipIn',
			confirm:true,
			yes:'Delete',
			no:'Cancel',
<!--- 			finalMessage: 'deleted!', --->
			onYes: function(){

				<!--- Store removal target in global variable so it can be undone --->
				if($('#mlp-section-settings').data('data-target'))
				{
					<!--- Store last state of the MLP --->
					StoreMLPBeforeAction(true);

					$('#mlp-section-settings').data('data-target').remove();
				}

				<!--- Hide current selection / hover menu if tool tip is closed for any reason --->
				$('.side-nav .widgets').click();

			},
			onClose: function(){


			}
		});

		<cfoutput>
			var #toScript(MLPPreviewTemplate, "MLPPreviewTemplate")#;
		</cfoutput>


		$('#ImageChooserModal').on('hide.bs.modal', function (e) {

		});

		$('#ImageChooserModal').on('shown.bs.modal', function (e) {

		}); <!--- close on modal show --->

		$(".MLPDropZone").dropzone( {
		// var myDropzone = new Dropzone( '.MLPDropZone', {
			url: "/session/sire/models/cfc/image.cfc?method=SaveImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			thumbnailWidth: 125,
			thumbnailHeight: 125,
			uploadMultiple: false,
			autoQueue: false,
			maxFiles: 1,
			parallelUploads: 1,
			paramName: "upload",
			acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg,image/svg+xml,.svg",
			resize: function(file) {
	        	var resizeInfo = {
		            srcX: 0,
		            srcY: 0,
		            trgX: 0,
		            trgY: 0,
		            srcWidth: file.width,
		            srcHeight: file.height,
		            trgWidth: this.options.thumbnailWidth,
		            trgHeight: this.options.thumbnailHeight
		        };

				return resizeInfo;
		    },
		    previewTemplate : MLPPreviewTemplate,
		    init: function() {

			    this.on("maxfilesexceeded", function(file){
			       // this.removeFile(file);
			    });

			    this.on("addedfile", function() {
			      if (this.files[1]!=null){
			        this.removeFile(this.files[0]);
			      }
			    });

			    // Execute when file uploads are complete
			    this.on("complete", function() {

				  this.removeAllFiles(true);

			      // If all files have been uploaded
			      if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
			        var _this = this;
			        // Remove all files
			        _this.removeAllFiles(true);

			        if(this.files.length != 0){
			            for(i=0; i<this.files.length; i++){
			                this.files[i].previewElement.remove();
			                this.files[i].accepted = false;
			            }
			            this.files.length = 0;
			        }

			      }
			    });

			    this.on("addedfile", function(file) {


					var _this = this;

					<!--- Bad bug - this click event would not work a second time because it was retaining an old reference to the "file" but not the most recent file ''doh --->
					$("#btn-upload-image").off('click');

					// Hookup the start button
					$("#btn-upload-image").on("click touchstart", function(e){

						_this.enqueueFile(file);

						<!--- hack for touch devices not to fire click/touch events twice --->
						e.stopPropagation();


					 });

					if (file.size > maxFilesize) {
						_this.removeFile(file);
						bootbox.alert('Warning!! ' + "This file is too large! Max file size is 10MB");
					}

					var allFilesAdded = _this.getFilesWithStatus(Dropzone.ADDED);
					var userUsage = $("#inpUserUsage").val();
					var userStoragePlan = $("#inpUserStoragePlan").val();

					var totalFileSize = 0;
					$.each(allFilesAdded, function(index, val) {
						 totalFileSize += val.size;
					});

					if (parseInt(parseInt(userUsage) + parseInt(totalFileSize)) > parseInt(userStoragePlan)) {
						bootbox.alert('Storage limit exceeded! Cannot upload this file');
						$("#btn-upload-image").on("click touchstart", function(e){  <!--- hack for touch devices not to fire click/touch events twice --->
							e.stopPropagation();

						});
					}

					var plainFileName = file.name.substr(0, file.name.lastIndexOf('.'));
					var fileType = file.name.substr(file.name.lastIndexOf('.')+1, file.name.length);
					if (RegName.test(plainFileName) && plainFileName.length <= 255) {
						try {
							$.ajax({
								url: '/session/sire/models/cfc/image.cfc?method=CheckImageName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								type: 'POST',
								dataType: 'json',
								data: {inpPlainFileName: plainFileName, inpFileType: fileType.toLowerCase()}
							})
							.done(function(data) {
								if (parseInt(data.RXRESULTCODE) == 1) {
									if (parseInt(data.CHECKRESULT) == 1) {
										bootbox.dialog({
											message: "This file name ("+plainFileName+"."+fileType+") is already exist! Do you want to replace existing file?",
											title: 'Warning!!',
											buttons: {
												yes: {
													label: "Yes",
													className: "btn btn-medium btn-success-custom",
													callback: function() {

													}
												},
												no: {
													label: "No",
													className: "btn btn-medium btn-primary btn-back-custom",
													callback: function() {
														_this.removeFile(file);
													}
												},
											}
										});
									} else if (parseInt(data.CHECKRESULT) == 2) {
										bootbox.alert('Filename error ' + data.MESSAGE);
									}
									else
									{
										<!--- file added ok - nothing to do --->

									}
								} else {
									bootbox.alert('Warning!! ' + (data.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : data.ERRMESSAGE));
								}

								$('#processingPayment').hide();
							})
<!---
							.complete(function() {
								$('#processingPayment').hide();
							});
--->

							$("#btn-upload-image").prop("disabled", false);


						} catch (e) {

							bootbox.alert('Warning!! ' + "Can't connect to server! Please refresh and try again later!");
						}
					} else {
						_this.removeFile(file);
						if (!RegName.test(plainFileName)) {
							bootbox.alert('Warning!! ' + 'Filename syntax error!<br>Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only');
						} else {
							bootbox.alert('Warning!! ' + 'Filename is too long. Please use filename less than 256 characters');
						}
					}
				});

				// Update the total progress bar
				this.on("totaluploadprogress", function(progress) {
		<!--- 			document.querySelector("#total-progress .progress-bar").style.width = progress + "%"; --->
				});

				this.on("sending", function(file) {
					// Show the total progress bar when upload starts
		<!--- 			document.querySelector("#total-progress").style.opacity = "1"; --->
					// And disable the start button
					$("#btn-upload-image").prop("disabled", true);
				});

				// Hide the total progress bar when nothing's uploading anymore
				this.on("queuecomplete", function(progress) {
		<!--- 			document.querySelector("#total-progress").style.opacity = "0"; --->

				});

				// Hide file when upload done
				this.on("success", function (file, result) {

					this.removeFile(file);
					this.removeAllFiles(true);

					if (parseInt(result.RXRESULTCODE) == 1) {

						<!--- Store removal target in global variable so it can be undone --->
						StoreMLPBeforeAction(true);

						<!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
						var $TargetObj = $('#ImageChooserModal').data('data-target');

						<!--- make sure this obj exists --->
						<!--- update img obj or background depeneding on where this is opened from  --->
						if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
						{
							$TargetObj.find('img').attr('src', result.IMAGEURL)
						}
						else
						{
							$TargetObj.css('background-image', 'url(' + result.IMAGEURL + ')');
							$('#mlp-section-settings #mlp-background-image-url').html(result.IMAGEURL);
						}

						var oldImg = $('img[src$="'+result.IMAGETHUMBURL+'"]');

						if (oldImg.length > 0) {
							var d = new Date();
							oldImg.attr('src', oldImg.attr('src')+'?'+d.getTime());
						} else {
							var html = '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 image-list-item">'+
											'<img class="image" src="'+result.IMAGETHUMBURL+'" alt="'+result.IMAGENAME+'">'+
											'<button class="btn btn-small btn-danger image-delete" data-imageid="'+result.IMAGEID+'">Delete</button>'+
										'</div>';
							var imageList = $('#image-list');
							if (imageList.children().length == 0) {
								imageList.text('');
							}
							imageList.prepend(html);
						}

						updateUserUsageInfo();

						<!--- close the image chooser modal --->
						$('#ImageChooserModal').modal('hide');

						$("#btn-upload-image").prop("disabled", true);

					} else {
						bootbox.alert('Warning!! ' + (result.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : result.ERRMESSAGE));
					}
				});
			}
		});




		<!--- Auto update links when this changes --->
		$('#cppxURL').change(function() {
			$('.mlp-url-key').html($('#cppxURL').val()  );
		});

		<!---  --->
		var customFilterObj = "";
		var _tblListHistory;


		function InitControl(customFilterObj)
		{//customFilterObj will be initiated and passed from datatable_filter
			var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
			//init datatable for active agent
			_tblListHistory = $('#tblListHistory').dataTable({
				"bStateSave": true,
				"iStateDuration": -1,
				"fnStateLoadParams": function (oSettings, oData) {

				},
				"fnStateSaveParams": function (oSettings, oData) {

				},
				"aaSorting": [[ 2, 'desc' ]],
			    "bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "two_button",
				// "sPaginationType": "input",// full_numbers  // two_button
				// "pagingType": "simple_numbers",
				oLanguage: {
		            oPaginate: {
		                sNext: '<button type="button" class="mlp-btn" style="margin-right:2em; padding:5px 12px;">Next</button>',
		                sPrevious: '<button type="button" class="mlp-btn" style="padding:5px 12px;">Prev</button>'
		            }
		        },
			    "bLengthChange": false,
				"iDisplayLength": 10,
				"bAutoWidth": false,
				"responsive": true,
			    "aoColumns": [
					// {"mDataProp": "PKID", "sName": 'PKID', "sTitle": 'PKID', "bSortable": false, "sWidth":"100px", "bVisible":false},
					{"mDataProp": "CREATED_DT", "sName": 'Last Save Date', "sTitle": 'Last Save Date', "bSortable": true, "sWidth": "100px"}
				],
				"sAjaxDataProp": "ListHistoryData",
				"sAjaxSource": '/session/sire/models/cfc/mlp.cfc?method=getMLPHistoryList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true&MLPID=<cfoutput>#ccpxDataId#</cfoutput>',
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				    return nRow;
		       	},
			   "fnDrawCallback": function( oSettings ) {
			      // MODIFY TABLE
					//$("#tblListHistory thead tr").find(':last-child').css("border-right","0px");
					if (oSettings._iDisplayStart < 0){
						oSettings._iDisplayStart = 0;
						$('input.paginate_text').val("1");
					}
					$('#tblListHistory > tbody').find('tr').each(function(){
						var tdFirst = $(this).find('td:first');
						if(tdFirst.hasClass('dataTables_empty')){
							$('#tblListHistory_paginate').hide();
							return false;
						}
						else{
							$('#tblListHistory_paginate').show();
						}
					})

					$('.mlp-historical-date').each(function( index ) {

					   bindPreviewLinks($(this));

					});

			    },
				"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data

			        aoData.push(
			            { "name": "customFilter", "value": customFilterData}
		            );
			        $.ajax({dataType: 'json',
			                 type: "POST",
			                 url: sSource,
				             data: aoData,
				             success: function(data){
				             	fnCallback(data);
				             }
				 	});
		        },
				"fnInitComplete":function(oSettings, json){
					// MODIFY TABLE

					$('#tblListHistory > tbody').find('tr').each(function(){
						var tdFirst = $(this).find('td:first');

						if(tdFirst.hasClass('dataTables_empty')){
							$('#tblListHistory_paginate').hide();
							return false;
						}
					})

				}
		    });
		}

		InitControl();

		<!--- removed .mlp-historical-date - done in table callback now --->
		$('#preview-icon-desktop, #preview-icon-tablet, #preview-icon-mobile').on("click touchstart", function(e){

			<!--- Set this to 0 for just regular previews --->
			$('#inpHDT').val('0');
			$('#RevertHistorical').hide();

		    var height = $(this).attr('data-height') || 300;
		    var width = $(this).attr('data-width') || 400;

		    $("#PreviewModal iframe").attr({ 'height': height, 'width': width});

            $('#PreviewModal').find('.modal-dialog').css({
		              width:'auto', //probably not needed
		              height:'auto', //probably not needed
		              'max-height':'100%'
		       });


            $('#PreviewModal').find('.modal-body').css({
		              width:width/2,
		              height:height/2,
		              'max-height':'100%',
		              'overflow':'hidden',
		              'padding':'0',
		       });


	    	SaveMLP(true);

	    	<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});


		<!--- UI feedback on which element is being changed --->
		$('#mlp-section-settings').mouseenter(function() {

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if($TargetObj.length)
			if($TargetObj.data("data-overlay") != "1")
			{
				$TargetObj.data("data-overlay", "1");
				$("<div class='SettingsOverlay'/>").appendTo($TargetObj);
  			}

		});

		$('#mlp-section-settings').mouseleave(function() {

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.data("data-overlay", "0")

			$(".SettingsOverlay").remove();


		});
<!---

		$('.modal').on('show.bs.modal', function() {
		  $(this).show();
		  setModalMaxHeight(this);
		});

		$(window).resize(function() {
		  if ($('.modal.in').length != 0) {
		    setModalMaxHeight($('.modal.in'));
		  }
		});
--->


		<!--- Update counter - default is 0 --->
		UpdateSaveCountSinceLastPublished();

		<!--- Start realtime MLP URL Validation available --->
		InitCPPURLValidation();


		<cfif RetCPPXData.AUTOPUBLISH GT 0>
			var switchery = new Switchery(document.querySelector('#AutoPublish'),{ disabled: false });
		<cfelse>
			var switchery = new Switchery(document.querySelector('#AutoPublish'),{ disabled: true });
		</cfif>

		ClearFixSwitchery = new Switchery(document.querySelector('#ClearFix'),{ disabled: false });


		<!--- Setup for TOC --->

		$('#btn-save-mlp-toc').on("click touchstart", function(e){
			MLPAddNewTOC();
		});

		$('#btn-save-mlp-topic').on("click touchstart", function(e){
			MLPAddTopic();
		});

		MLPLoadTOCs();

		$('#TOCList').on('change', function () {

			<!--- Clear everything in case there is nothing found --->
			$('#mlp-toc-panel').children('.MLP-Top-Topic').children('.MLPSubTopicsSection').children().remove();

			<!--- SLide up the empty sub section --->
			$('#mlp-toc-panel').children('.MLP-Top-Topic').children('.MLPSubTopicsSection').slideUp('fast');

		    MLPLoadTopics($('#mlp-toc-panel').children('.MLP-Top-Topic'), $(this).val(), 1 );
		});

		<!--- Auto load menu --->
		if(parseInt('<cfoutput>#tocid#</cfoutput>') > 0)
		{
			 <!--- Bind the top menu items - will need to call this again for each subtopic --->
			 BindSortableTopics($('#mlp-toc-panel').children('.TopLevelParent').find('.top-sortable-topic-container'));

			 MLPLoadTopics($('#mlp-toc-panel').children('.TopLevelParent').children('.MLP-Top-Topic'), parseInt('<cfoutput>#tocid#</cfoutput>'), 1 );

			 <!--- Show the topics panel --->
			 $('.side-nav #mlp-toc').click();

		}

		$('#MLP-Topic-Editor-Edit-MLP').on("click", function(e){

			if(parseInt($(this).attr('data-attr-mlp-id')) > 0)
			{
				var url = "mlpx-edit?tocid=<cfoutput>#tocid#</cfoutput>&ccpxDataId=" + $(this).attr('data-attr-mlp-id') ;
				window.location = url;
			}
		});

		$('#MLP-Topic-Template-Picker-Modal').on('show.bs.modal', function () {


		});

		$('.mlp-select-template').on("click", function(e){

			inpMLPTemplateId = $(this).attr('data-attr-template-id');
			inpTopicId = $('#MLP-Topic-Template-Picker-Modal').attr('data-attr-topic-id');

			$('#MLP-Topic-Template-Picker-Modal').modal('hide');

			<!--- Topic Id is stored as an Attr of the Modal --->
			if(parseInt(inpMLPTemplateId) > 0 && parseInt(inpTopicId) > 0)
				MLPLinkNewMLPToTopic(inpTopicId, inpMLPTemplateId, true);


		});

		<!--- This even gets applied to dynamically added elements automatically - cool! --->
		$.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {

                var $ThisContextMenuObj = $(this);

            	var CurrTopicId =  $ThisContextMenuObj.children('.MLP-Top-Topic-Link').attr('data-attr-topic-id');
            	var CurrTopicMLPId =  $ThisContextMenuObj.children('.MLP-Top-Topic-Link').attr('data-attr-mlp-id');
            	var CurrParentId =  $ThisContextMenuObj.children('.MLP-Top-Topic-Link').attr('data-attr-topic-parent');
            	var CurrParentIdType =  $ThisContextMenuObj.children('.MLP-Top-Topic-Link').attr('data-attr-topic-parent-type');
            	var $SubTopicSection = $ThisContextMenuObj.children('.MLPSubTopicsSection');


				if(key == 'edit')
				{
					<!--- inplace editing of description  see http://jsfiddle.net/egstudio/aFMWg/1/ --->
					var replaceWith = $('<input name="temp" type="text" style="color: #000; display: inline; width: 100%; box-sizing: border-box; text-align:right; padding-right:2em;" />');

					<!--- inline edit functionality - custom so can save on blur--->
					var elem = $(this).children('.MLP-Top-Topic-Link').children('.TopicDesc');

				    elem.hide();
				    elem.after(replaceWith);
				    replaceWith.val(elem.text());
				    replaceWith.focus();

					<!--- Look for Blur or the Enter key --->
					replaceWith.on('blur keyup',function(e) {
						if (e.type == 'blur' || e.keyCode == '13')
						{
						   if ($(this).val() != "") {

						        elem.text($(this).val());
						    }

						    $(this).remove();

						    MLPSaveTopicDesc( CurrTopicId , $(this).val())

						    elem.show();
						}

				     });
				}
				else
				if (key == 'delete')
				{

					// console.log($(".MLP-Top-Topic-Link[data-attr-topic-id='" + CurrTopicId + "']").parent().children('.ConfirmDeleteTopic'));
					// console.log($(".MLP-Top-Topic-Link[data-attr-topic-id='" + CurrTopicId + "']").parent());

				 	$(".context-menu-icon-delete").darkTooltip({
						content: 'Are you sure?',
						trigger: 'click',
						modal: false,
						animation:'flipIn',
						confirm:true,
						modal:false,
						yes:'Delete',
						no:'Cancel',
						<!--- 	finalMessage: 'deleted!', --->
						onYes: function(){

							MLPDeleteTopic(CurrTopicId);

							<!--- Hide the menu since we are done with it --->
							$ThisContextMenuObj.contextMenu("hide");

							<!--- Since I am removing the object anyway - kill the tool tip --->
							$(".dark-tooltip:visible").remove();

						},
						onClose: function(){

							$ThisContextMenuObj.contextMenu("hide");

							<!--- Since I am removing the object anyway - kill the tool tip --->
							$(".dark-tooltip:visible").remove();

						}
					});

				 	return false;

				}
				else
				if(key == 'editmlp')
				{
					if(parseInt(CurrTopicMLPId) > 0)
					{
						<!--- Edit MLP --->
						var url = "mlpx-edit?tocid=<cfoutput>#tocid#</cfoutput>&ccpxDataId=" + CurrTopicMLPId ;
						window.location = url;
					}
					else
					{
						<!--- Attached the current Topic Id to the Modal --->
						$('#MLP-Topic-Template-Picker-Modal').attr('data-attr-topic-id', CurrTopicId);

						<!--- Choose an MLP template to link and then edit --->
						$('#MLP-Topic-Template-Picker-Modal').modal('show');
					}
				}
                else
				if(key == 'addtopic')
				{

					$("#MLP-Add-Topic-Modal").attr('data-attr-topic-id', 0);
					$("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent', CurrTopicId);
					$("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent-type', 2);

					<!--- Store jquery reference in data instead of attr --->
					$("#MLP-Add-Topic-Modal").data('data-attr-topic-section', $SubTopicSection);

					<!--- Choose an MLP template to link and then edit --->
					$('#MLP-Add-Topic-Modal').modal('show');

				}

            },
            items: {

                "edit": {name: "Edit Description", icon: "edit"},
                "editmlp": {name: "Edit Linked MLP", icon: "edit"},
                "addtopic": {name: "Add Sub Topic", icon: "fa-plus"},
                "delete": {name: "Delete", icon: "delete"},
                "sep1": "---------",
                "cancel": {name: "Cancel", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });

		<!--- Allow MLP to expire - if served in javascript iframe loader, then expired will not show by default --->
        var InitExpire = $( "#Expire_dt" ).val();
		$( "#Expire_dt" ).datepicker({

			onSelect: function(dateText, inst) {
		      var dateAsString = dateText; //the first parameter of this function
		      var dateAsObject = $(this).datepicker( 'getDate' ); //the getDate method
		   }

		});
		$( "#Expire_dt" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
		$( "#Expire_dt" ).datepicker('setDate', InitExpire);

	});

	<!--- https://codepen.io/dimbslmh/full/mKfCc --->
	function setModalMaxHeight(element) {
		  this.$element     = $(element);
		  this.$content     = this.$element.find('.modal-content');
		  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
		  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
		  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
		  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
		  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
		  var maxHeight     = contentHeight - (headerHeight + footerHeight);

		  this.$content.css({
		      'overflow': 'hidden'
		  });

		  this.$element
		    .find('.modal-body').css({
		      'max-height': maxHeight,
		      'overflow-y': 'auto'
		  });
	}

	function InitEditorStuff()
	{
		<!--- Setup sortables --->
		$('.sortable').each(function( index ) {

		   BindSortable($(this));

		});

		<!--- Set up all the #main-stage-content select2 boxes --->
		$("#main-stage-content .Select2").select2( { theme: "bootstrap"} );

		<!--- Setup Dropabble events --->
	    SetupDroppables($( ".droppable" ));

	    $('#main-stage-content').popline();

	    $('.MLPEditable').each(function( index ) {

		   BindDeletableEvents($(this));
		   BindEditMenu($(this));
		   BindSectionSettings($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-settings'));

		});

		$('.MLPMasterSection').each(function( index ) {

           	BindSectionSettings($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-sub-settings') );

	   //      BindSplitEvents($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-split-section') );
	       <!--- loop over each possible rows sections --->
	       $(this).find('.MLPMasterSection').each(function( index ) {

		    	BindSplitEvents($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-split-section') );

		    });

           	SetupDroppables($(this));

			<!--- These are already bound as part of .MLPEditable
			           	// BindEditMenu($(this));
			           	// BindSectionSettings($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-settings') );
			           	// BindDeletableEvents($(this));
			--->

		});

		$('.MLPImageContainer').each(function( index ) {

		   BindImgSubMenu($(this));

		   <!--- Special section for <img> tags --->
           BindSectionSettings($(this).find('img'), $(this).find('.mlp-icon-img-settings'));

		});


	}

	function updateUserUsageInfo()
	{
		try {
			$.ajax({
				url: '/session/sire/models/cfc/image.cfc?method=GetUserStorageInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					var userUsage = parseInt(data.USERUSAGE);
					var userStoragePlan = parseInt(data.USERSTORAGEPLAN);

					var usagePercent = parseFloat(Math.round((userUsage/userStoragePlan) * 100 * 100) / 100).toFixed(2);

					$("#usageProgress").css('width', usagePercent+'%');
					$("#inpUserUsage").val(userUsage);

					var usageMBSize = userUsage / 1048576;
					if (usageMBSize > 1024) {
						usageMBSize = (usageMBSize / 1024);
						usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'GB';
					} else {
						usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'MB';
					}
					$("#userUsageSpan").text(usageMBSize);

					var userStoragePlanMBSize = userStoragePlan / 1048576;
					if (userStoragePlanMBSize > 1024) {
						userStoragePlanMBSize = (userStoragePlanMBSize / 1024);
						userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'GB';
					} else {
						userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'MB';
					}
					$("#userStoragePlanSpan").text(userStoragePlanMBSize);
				} else {
					alertMessage('Oops!', data.ERRMESSAGE == '' ? 'Error when checking storage! Please try again later!' : data.ERRMESSAGE);
				}
			})
<!---
			.error(function() {
				alertMessage('Oops!', 'Internal Server Error!');
			});
--->
		} catch (e) {
			alertMessage('Oops!', 'Internal Server Error!');
		}
	}

	function BindEditMenu(inpObj)
	{
		inpObj.on('mouseout', function(e) {

			<!--- Dont hide current selection / hover menu if tool tip is open --->
			if(!$(".dark-tooltip:visible").length)
			{
			 	inpObj.find('.MLPEditMenu').hide();
		 	}
		});

		inpObj.on('mouseover', function(e) {

		  	// inpObj.find('.MLPEditMenu').first().show();

		  	inpObj.children('.MLPEditMenu').show();


		  	// <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		<!--- for touch devices that do not support hover --->
		inpObj.on('click', function(e) {

			<!--- Dont hide current selection / hover menu if tool tip is open --->
			if(!$(".dark-tooltip:visible").length)
			{
			 	$('.MLPEditMenu').hide();
		 	}

		  	inpObj.children('.MLPEditMenu').show();

		  	// <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});
	}

	<!--- Call here so we can intialize current objects as well as new objects dropped on the page --->
	function BindDeletableEvents(inpObj)
	{

		inpObj.find('.fa-remove, .fa-trash-alt').darkTooltip({
			content: 'Are you sure?',
			trigger: 'click',
			modal: false,
			animation:'flipIn',
			confirm:true,
			yes:'Delete',
			no:'Cancel',
			finalMessage: 'deleted!',
			onYes: function(){

				<!--- Get parent object before child object removal  --->
				var parentObj = inpObj.parent();
				var parentObj2 = parentObj.parent();

				<!--- Store removal target in global variable so it can be undone --->
				StoreMLPBeforeAction(true);

				inpObj.find('.fa-remove, .fa-trash-alt').closest('.MLPEditable').remove();

				<!--- Remove empty rows added by splits --->
				if(parentObj.hasClass('row'))
				{
					if(!parentObj.children().length)
					{
						parentObj.remove();

						parentObj = parentObj2;
					}
				}

				<!--- Special MLP feedback borders --->
				if(parentObj.attr('data-helper-text') == "Box Container")
				{
					<!--- Add back in empty box classes --->
					parentObj.addClass("MLPMasterRowEmpty");
				}

				<!--- Since I am removing the object anyway - kill the tool tip --->
				$(".dark-tooltip:visible").remove();

				<!--- Hide current selection / hover menu if tool tip is closed for any reason --->
				$('.side-nav .widgets').click();

			},
			onClose: function(){

				<!--- Hide current selection / hover menu if tool tip is closed for any reason --->
				inpObj.find('.MLPEditMenu').hide();

			}
		});


	}

	function BindSplitEvents(inpObj, inpClickObj)
	{

		inpClickObj.on('click', function(e) {

			<!--- Store last state of the MLP --->
			StoreMLPBeforeAction(true);

			SplitSection(inpObj);

		  	// <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});
	}

	<!--- Turn current section into a double column row --->
	function SplitSection(inpObj)
	{

		<!--- Verify inpObj is not alreay 0, 1, 2 is the max - 3 levels deep or more - no more splitting!!! --->
		if(inpObj.parents("div .MLPMasterSection").length > 2)
		{
			return;
		}

		<cfoutput>
			var #toScript(MLPSplitTemplate, "MLPSplitTemplate")#;
		</cfoutput>

       	var NewObj = $(MLPSplitTemplate);

       	<!--- Bind any sub section sections settings --->
	   	NewObj.find('.MLPMasterSection').each(function( index ) {

           BindSectionSettings($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-settings'));

		   BindDeletableEvents($(this));

		   BindEditMenu($(this));

		   BindSortable($(this));

		   SetupDroppables($(this));

		   BindSplitEvents($(this), $(this).children('.MLPEditMenu').find('.mlp-icon-split-section'));

		});

      	var $ref = null;

      	inpObj.css('padding', '2em');
	    $ref = inpObj.append(NewObj);

	    <!--- Remove empty box classes --->
        inpObj.removeClass("MLPMasterRowEmpty");

	}

	<!--- Call here so we can intialize current objects as well as new objects dropped on the page --->
	function BindSectionSettings(inpObj, inpClickObj)	{


		<!--- Set the click to both the objectitself as well as a settings icon or other clickobject --->
		inpClickObj.add(inpObj).on("click touchstart", function(e){

			$('#mlp-section-settings').data('data-target', inpObj);


			<!--- Hide the delete option for the master section --->
			if(inpObj.attr('id') == 'MLP-Master-Section')
			{
				$('#section-delete-container').hide();

			}
			else
			{
				$('#section-delete-container').show();
			}

			<!--- Read in current settings from current inpObj --->
			if(inpObj.attr('id') != '' && inpObj.attr('id') != undefined)
			{
				$('#mlp-section-settings #section-id').val(inpObj.attr('id'));
			}
			else
			{
				if(inpObj.is('img'))
				{
					<!--- Get count of number of sections ---- Attribute Starts With Selector [name^=”value”] --->
					var NextItemNumber = parseInt($('*[id^="New Image"]').length) + 1;

					inpObj.attr('id', 'New Image ' + NextItemNumber);

				}
				else
				if(inpObj.attr('data-helper-text') == 'Line Container')
				{
					<!--- Get count of number of sections ---- Attribute Starts With Selector [name^=”value”] --->
					var NextItemNumber = parseInt($('*[id^="New Line Section"]').length) + 1;

					inpObj.attr('id', 'New Line Section ' + NextItemNumber);

				}
				else
				if(inpObj.attr('data-helper-text') == 'Headline Container')
				{
					<!--- Get count of number of sections ---- Attribute Starts With Selector [name^=”value”] --->
					var NextItemNumber = parseInt($('*[id^="New Headline Section"]').length) + 1;

					inpObj.attr('id', 'New Headline Section ' + NextItemNumber);

				}
				else
				if(inpObj.attr('data-helper-text') == 'Text Container')
				{
					<!--- Get count of number of sections ---- Attribute Starts With Selector [name^=”value”] --->
					var NextItemNumber = parseInt($('*[id^="New Text Section"]').length) + 1;

					inpObj.attr('id', 'New Text Section ' + NextItemNumber);

				}
				else
				{
					<!--- Get count of number of sections ---- Attribute Starts With Selector [name^=”value”] --->
					var NextItemNumber = parseInt($('*[id^="New Section"]').length) + 1;

					inpObj.attr('id', 'New Section ' + NextItemNumber);
				}

				$('#mlp-section-settings #section-id').val(inpObj.attr('id'));
			}


			var bg_url = inpObj.css('background-image');
		    // ^ Either "none" or url("...urlhere..")
		    bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
		    bg_url = bg_url ? bg_url[2] : ""; // If matched, retrieve url, otherwise ""

			$('#mlp-section-settings #mlp-background-image-url').html(bg_url);


			$('#mlp-section-settings #background-color').spectrum("set", inpObj.css('background-color'));

			<!--- background-repeat --->
			if(inpObj.css('background-repeat') != undefined && inpObj.css('background-repeat') != 'undefined')
				$('#mlp-section-settings #background-repeat').val(inpObj.css('background-repeat')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #background-repeat').val('no-repeat').trigger('change.select2');
				inpObj.css('background-repeat','no-repeat');
			}

			<!--- background-position --->
			if(inpObj.css('background-position') != undefined && inpObj.css('background-position') != 'undefined')
			{
				$('#mlp-section-settings #background-position').val(inpObj.css('background-position')).trigger('change.select2');
			}
			else
			{
				$('#mlp-section-settings #background-position').val('initial').trigger('change.select2');
				inpObj.css('background-position','initial');
			}

			<!--- background-size --->
			if(inpObj.css('background-size') != undefined && inpObj.css('background-size') != 'undefined')
			{
				$('#mlp-section-settings #background-size').val(inpObj.css('background-size')).trigger('change.select2');
			}
			else
			{
				$('#mlp-section-settings #background-size').val('initial').trigger('change.select2');
				inpObj.css('background-size','initial');
			}

			<!--- background-attachment --->
			if(inpObj.css('background-attachment') != undefined && inpObj.css('background-attachment') != 'undefined')
				$('#mlp-section-settings #background-attachment').val(inpObj.css('background-attachment'));
			else
			{
				$('#mlp-section-settings #background-attachment').val('initial').trigger('change.select2');
				inpObj.css('background-attachment','initial');
			}

			$('#mlp-section-settings #color').spectrum("set", inpObj.css('color'));

			$('#mlp-section-settings #border-color').spectrum("set", inpObj.css('border-color'));


			<!--- Set Font Style --->
			if(inpObj.css('font-weight') == 'bold')
			{
				$('#mlp-section-settings #bold').addClass("active");
			}
			else
			{
				$('#mlp-section-settings #bold').removeClass("active");
			}

			if(inpObj.css('font-style') == 'italic')
			{
				$('#mlp-section-settings #italic').addClass("active");
			}
			else
			{
				$('#mlp-section-settings #italic').removeClass("active");
			}

			<!--- Set alignment --->
			if(inpObj.css('text-align') == 'left')
			{
				$('#mlp-section-settings #Alignment .fa-align-left').addClass("active");
				$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");
			}
			else
			if(inpObj.css('text-align') == 'center')
			{
				$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-center').addClass("active");
				$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");
			}
			else
			if(inpObj.css('text-align') == 'right')
			{
				$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-right').addClass("active");
				$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");
			}
			else <!--- assume default to inherit if(inpObj.css('text-align') == 'inherit') --->
			{
				$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
				$('#mlp-section-settings #Alignment .fa-circle').addClass("active");
			}

			$('#mlp-section-settings #padding-top').val(parseInt(inpObj.css('padding-top')));
			$('#mlp-section-settings #padding-bottom').val(parseInt(inpObj.css('padding-bottom')));
			$('#mlp-section-settings #padding-left').val(parseInt(inpObj.css('padding-left')));
			$('#mlp-section-settings #padding-right').val(parseInt(inpObj.css('padding-right')));
			$('#mlp-section-settings #margin-top').val(parseInt(inpObj.css('margin-top')));
			$('#mlp-section-settings #margin-bottom').val(parseInt(inpObj.css('margin-bottom')));
			$('#mlp-section-settings #margin-left').val(parseInt(inpObj.css('margin-left')));
			$('#mlp-section-settings #margin-right').val(parseInt(inpObj.css('margin-right')));

			$('#mlp-section-settings #top').val(parseInt(inpObj.css('top')));
			$('#mlp-section-settings #bottom').val(parseInt(inpObj.css('bottom')));
			$('#mlp-section-settings #left').val(parseInt(inpObj.css('left')));
			$('#mlp-section-settings #right').val(parseInt(inpObj.css('right')));

			<!--- Set slider positions --->
			$('#mlp-section-settings #padding-top-fader').val(parseInt(inpObj.css('padding-top')));
			$('#mlp-section-settings #padding-bottom-fader').val(parseInt(inpObj.css('padding-bottom')));
			$('#mlp-section-settings #padding-left-fader').val(parseInt(inpObj.css('padding-left')));
			$('#mlp-section-settings #padding-right-fader').val(parseInt(inpObj.css('padding-right')));
			$('#mlp-section-settings #margin-top-fader').val(parseInt(inpObj.css('margin-top')));
			$('#mlp-section-settings #margin-bottom-fader').val(parseInt(inpObj.css('margin-bottom')));
			$('#mlp-section-settings #margin-left-fader').val(parseInt(inpObj.css('margin-left')));
			$('#mlp-section-settings #margin-right-fader').val(parseInt(inpObj.css('margin-right')));
			$('#mlp-section-settings #top-fader').val(parseInt(inpObj.css('top')));
			$('#mlp-section-settings #bottom-fader').val(parseInt(inpObj.css('bottom')));
			$('#mlp-section-settings #left-fader').val(parseInt(inpObj.css('left')));
			$('#mlp-section-settings #right-fader').val(parseInt(inpObj.css('right')));

			<!--- Border Width --->
			$('#mlp-section-settings #border-width').val(parseInt(inpObj.css('border-width')));
			$('#mlp-section-settings #border-width-fader').val(parseInt(inpObj.css('border-width')));

			<!--- Border Style --->
			if(inpObj.css('border-style') != undefined && inpObj.css('border-style') != 'undefined')
				$('#mlp-section-settings #border-style').val(inpObj.css('border-style')).trigger('change.select2');
			else
				$('#mlp-section-settings #border-style').val('none').trigger('change.select2');

			<!--- Border Radius --->
			$('#mlp-section-settings #border-radius').val(parseInt(inpObj.css('border-radius')));
			$('#mlp-section-settings #border-radius-fader').val(parseInt(inpObj.css('border-radius')));

			<!--- Set arrow border --->
			if(inpObj.hasClass('section-arrow-top'))
			{
				$('#mlp-section-settings #arrow-border .fa-caret-up').addClass("active");
				$('#mlp-section-settings #arrow-border .fa-caret-down').removeClass("active");
				$('#mlp-section-settings #arrow-border .fa-circle').removeClass("active");
			}
			else
			if(inpObj.hasClass('section-arrow-bottom'))
			{
				$('#mlp-section-settings #arrow-border .fa-caret-up').removeClass("active");
				$('#mlp-section-settings #arrow-border .fa-caret-down').addClass("active");
				$('#mlp-section-settings #arrow-border .fa-circle').removeClass("active");
			}
			else
			{
				$('#mlp-section-settings #arrow-border .fa-caret-up').removeClass("active");
				$('#mlp-section-settings #arrow-border .fa-caret-down').removeClass("active");
				$('#mlp-section-settings #arrow-border .fa-circle').addClass("active");
			}

			<!--- width-units --->
			if(inpObj.attr('width-units') != undefined && inpObj.attr('width-units') != 'undefined')
				$('#mlp-section-settings #width-units').val(inpObj.attr('width-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #width-units').val('px').trigger('change.select2');
				inpObj.attr('width-units','px');
			}

			<!--- min-width-units --->
			if(inpObj.attr('min-width-units') != undefined && inpObj.attr('min-width-units') != 'undefined')
				$('#mlp-section-settings #min-width-units').val(inpObj.attr('min-width-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #min-width-units').val('px').trigger('change.select2');
				inpObj.attr('min-width-units','px');
			}

			<!--- max-width-units --->
			if(inpObj.attr('max-width-units') != undefined && inpObj.attr('max-width-units') != 'undefined')
				$('#mlp-section-settings #max-width-units').val(inpObj.attr('max-width-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #max-width-units').val('px').trigger('change.select2');
				inpObj.attr('max-width-units','px');
			}

			<!--- height-units --->
			if(inpObj.attr('height-units') != undefined && inpObj.attr('height-units') != 'undefined')
				$('#mlp-section-settings #height-units').val(inpObj.attr('height-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #height-units').val('px').trigger('change.select2');
				inpObj.attr('height-units','px');
			}

			<!--- min-height-units --->
			if(inpObj.attr('min-height-units') != undefined && inpObj.attr('min-height-units') != 'undefined')
				$('#mlp-section-settings #min-height-units').val(inpObj.attr('min-height-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #min-height-units').val('px').trigger('change.select2');
				inpObj.attr('min-height-units','px');
			}

			<!--- max-height-units --->
			if(inpObj.attr('max-height-units') != undefined && inpObj.attr('max-height-units') != 'undefined')
				$('#mlp-section-settings #max-height-units').val(inpObj.attr('max-height-units')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #max-height-units').val('px').trigger('change.select2');
				inpObj.attr('max-height-units','px');
			}

			<!--- http://stackoverflow.com/questions/28295072/how-can-i-convert-px-to-vw-in-javascript/28295133#28295133 --->
			<!--- Width --->
			if(typeof inpObj.attr('mlp-width') != 'undefined')
			{
				$('#mlp-section-settings #width').val(inpObj.attr('mlp-width'))
				$('#mlp-section-settings #width-fader').val(inpObj.attr('mlp-width'));

			}
			else if($('#mlp-section-settings #width-units').val() == 'vw')
			{
				$('#mlp-section-settings #width').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('width'))));
				$('#mlp-section-settings #width-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('width'))));
			}
			else if($('#mlp-section-settings #width-units').val() == 'vh')
			{
				$('#mlp-section-settings #width').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('width'))));
				$('#mlp-section-settings #width-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('width'))));
			}
			else
			{
				$('#mlp-section-settings #width').val(parseInt(inpObj.css('width')));
				$('#mlp-section-settings #width-fader').val(parseInt(inpObj.css('width')));
			}

			if(typeof inpObj.attr('mlp-min-width') != 'undefined')
			{
				$('#mlp-section-settings #min-width').val(inpObj.attr('mlp-min-width'))
				$('#mlp-section-settings #min-width-fader').val(inpObj.attr('mlp-min-width'));

			}
			else if($('#mlp-section-settings #min-width-units').val() == 'vw')
			{
				$('#mlp-section-settings #min-width').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('min-width'))));
				$('#mlp-section-settings #min-width-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('min-width'))));
			}
			else if($('#mlp-section-settings #min-width-units').val() == 'vh')
			{
				$('#mlp-section-settings #min-width').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('min-width'))));
				$('#mlp-section-settings #min-width-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('min-width'))));
			}
			else
			{
				$('#mlp-section-settings #min-width').val(parseInt(inpObj.css('min-width')));
				$('#mlp-section-settings #min-width-fader').val(parseInt(inpObj.css('min-width')));
			}

			if(typeof inpObj.attr('mlp-max-width') != 'undefined')
			{
				$('#mlp-section-settings #max-width').val(inpObj.attr('mlp-max-width'))
				$('#mlp-section-settings #max-width-fader').val(inpObj.attr('mlp-max-width'));

			}
			else if($('#mlp-section-settings #max-width-units').val() == 'vw')
			{
				$('#mlp-section-settings #max-width').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('max-width'))));
				$('#mlp-section-settings #max-width-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('max-width'))));
			}
			else if($('#mlp-section-settings #max-width-units').val() == 'vh')
			{
				$('#mlp-section-settings #max-width').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('max-width'))));
				$('#mlp-section-settings #max-width-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('max-width'))));
			}
			else
			{
				$('#mlp-section-settings #max-width').val(parseInt(inpObj.css('max-width')));
				$('#mlp-section-settings #max-width-fader').val(parseInt(inpObj.css('max-width')));
			}

			<!--- Height --->
			if(typeof inpObj.attr('mlp-height') != 'undefined')
			{
				$('#mlp-section-settings #height').val(inpObj.attr('mlp-height'))
				$('#mlp-section-settings #height-fader').val(inpObj.attr('mlp-height'));

			}
			else if($('#mlp-section-settings #height-units').val() == 'vw')
			{
				$('#mlp-section-settings #height').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('height'))));
				$('#mlp-section-settings #height-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('height'))));
			}
			else if($('#mlp-section-settings #height-units').val() == 'vh')
			{
				$('#mlp-section-settings #height').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('height'))));
				$('#mlp-section-settings #height-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('height'))));
			}
			else
			{
				$('#mlp-section-settings #height').val(parseInt(inpObj.css('height')));
				$('#mlp-section-settings #height-fader').val(parseInt(inpObj.css('height')));
			}

			if(typeof inpObj.attr('mlp-min-height') != 'undefined')
			{
				$('#mlp-section-settings #min-height').val(inpObj.attr('mlp-min-height'))
				$('#mlp-section-settings #min-height-fader').val(inpObj.attr('mlp-min-height'));

			}
			else if($('#mlp-section-settings #min-height-units').val() == 'vw')
			{
				$('#mlp-section-settings #min-height').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('min-height'))));
				$('#mlp-section-settings #min-height-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('min-height'))));
			}
			else if($('#mlp-section-settings #min-height-units').val() == 'vh')
			{
				$('#mlp-section-settings #min-height').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('min-height'))));
				$('#mlp-section-settings #min-height-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('min-height'))));
			}
			else
			{
				$('#mlp-section-settings #min-height').val(parseInt(inpObj.css('min-height')));
				$('#mlp-section-settings #min-height-fader').val(parseInt(inpObj.css('min-height')));
			}

			if(typeof inpObj.attr('mlp-max-height') != 'undefined')
			{
				$('#mlp-section-settings #max-height').val(inpObj.attr('mlp-max-height'))
				$('#mlp-section-settings #max-height-fader').val(inpObj.attr('mlp-max-height'));

			}
			else if($('#mlp-section-settings #max-height-units').val() == 'vw')
			{
				$('#mlp-section-settings #max-height').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('max-height'))));
				$('#mlp-section-settings #max-height-fader').val(Math.round((100/document.documentElement.clientWidth) * parseInt(inpObj.css('max-height'))));
			}
			else if($('#mlp-section-settings #max-height-units').val() == 'vh')
			{
				$('#mlp-section-settings #max-height').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('max-height'))));
				$('#mlp-section-settings #max-height-fader').val(Math.round((100/document.documentElement.clientHeight) * parseInt(inpObj.css('max-height'))));
			}
			else
			{
				$('#mlp-section-settings #max-height').val(parseInt(inpObj.css('max-height')));
				$('#mlp-section-settings #max-height-fader').val(parseInt(inpObj.css('max-height')));
			}

			<!--- Check for auto margin set --->
			if(inpObj.attr('data-auto-margin') == '1em auto' )
			{
				$('#mlp-section-settings #auto-margin').prop('checked', true);
			}
			else
			{
				$('#mlp-section-settings #auto-margin').prop('checked', false);
			}


			<!--- Advanced Stuff --->
			<!--- Position --->
			if(inpObj.css('position') != undefined && inpObj.css('position') != 'undefined')
				$('#mlp-section-settings #position').val(inpObj.css('position')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #position').val('relative').trigger('change.select2');
				inpObj.css('position','relative');
			}

			<!--- Float --->
			if(inpObj.css('float') != undefined && inpObj.css('float') != 'undefined')
				$('#mlp-section-settings #float').val(inpObj.css('float')).trigger('change.select2');
			else
			{
				$('#mlp-section-settings #float').val('relative').trigger('change.select2');
				inpObj.css('float','none');
			}

			<!--- ClearFix --->
			if(inpObj.hasClass('clearfix'))
			{
				$('#mlp-section-settings #ClearFix').prop('checked', true);
				ClearFixSwitchery.setPosition(true);
				$('#mlp-section-settings #ClearFix').click();
			}
			else
			{
				$('#mlp-section-settings #ClearFix').prop('checked', false);
				ClearFixSwitchery.setPosition(false);

			}

			$('.menu-item').removeClass('panel-active');
			$(this).find('.menu-item').addClass('panel-active');
			<!--- Hide everything else... --->
		  	$('.tools-panel').hide();
		  	$('.publish-mlp-panel').hide();
		  	$('#mlp-history-panel').hide();
		  	$('#mlp-header-settings').hide();
		  	$('#mlp-section-settings').show();

		  	$('.sidebar-second').show(400);

		  	<!--- stop event propogation to prevent multiple click throughs --->
			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			e.preventDefault();


		});
	}

	function SaveMLP(inpShowPreview)
	{
		<!--- Watch out for server masking/renaming Script and other tags --->
		<!--- A giant thank you to: http://www.cjboco.com/blog.cfm/post/how-invalidtag-destroyed-my-morning/  --->

		<cfif action NEQ "template" AND  action NEQ "add">
			<!--- Validate valid URL File Name--->
			var re = new RegExp(/^[0-9a-zA-Z\-\_\']+$/);
			if (!re.test($('#cppxURL').val().trim()))
			{
			    <!--- Show the Publish Side Panel - dont use $('.side-nav .publish-mlp').click(); click to mess with popover --->
				$('.menu-item').removeClass('panel-active');
				$('.side-nav .publish-mlp').addClass('panel-active');
				<!--- Hide everything else... --->
			  	$('.tools-panel').hide();
			  	$('#mlp-header-settings').hide();
			  	$('#mlp-section-settings').hide();
			  	$('#mlp-history-panel').hide();
			  	$('.publish-mlp-panel').show();
			  	$('.sidebar-second').show(400);

	            $('#cppxURL').attr('data-content', "Invalid Characters in URL - Must only be alphanumeric with no special characters and with no spaces allowed");
	            $('#cppxURL').popover('show');
	            return;
			}
		</cfif>

		try
		{
	        if (typeof $('#action').val() !== 'undefined' && $('#action').val() == 'template' ) {
	            url = '/session/sire/models/cfc/mlp.cfc?method=SaveMLPTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
	        } else {
	            url = '/session/sire/models/cfc/mlp.cfc?method=SaveMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
	        }

	        cppName = $('#cppxName').val().trim();

	        if( typeof $('#cppxURL').val() != 'undefined' )
		        cppxURL = $('#cppxURL').val().trim();
		    else
		    	cppxURL = '';

	        ccpxDataId = $('#ccpxDataId').val();

	        // inpRawContent = JSON.stringify({'inpRawContent':editor.getContent()});

		  var inpRawData = $('#main-stage-content').html();
		  inpRawData = inpRawData.replace(/<script/g, '<mlp-script');
		  inpRawData = inpRawData.replace(/script\/>/g, '/mlp-script>');
	      inpRawData = inpRawData.replace(/<iframe/g, '<mlp-iframe');
		  inpRawData = inpRawData.replace(/iframe\/>/g, '/mlp-iframe>');


		  <!--- Use regular expression to replace all instances --->
		  var inpCustomCSS = $('#inpRawCSS').val();
		  inpCustomCSS = inpCustomCSS.replace(/<script/g, '<mlp-script');
		  inpCustomCSS = inpCustomCSS.replace(/script\/>/g, '/mlp-script>');
		  inpCustomCSS = inpCustomCSS.replace(/<iframe/g, '<mlp-iframe');
		  inpCustomCSS = inpCustomCSS.replace(/iframe\/>/g, '/mlp-iframe>');
		  inpCustomCSS = inpCustomCSS.replace(/<meta/g, '<mlp-meta');
		  inpCustomCSS = inpCustomCSS.replace(/meta\/>/g, '/mlp-meta>');

		  if(isAjax == 1) return false;
	      $.ajax({
	        type: "POST",
	        url: url,
	        dataType: 'json',
	        data: {ccpxDataId:ccpxDataId, cppxName:cppName, cppxURL:cppxURL, inpRawContent: inpRawData, inpCustomCSS: inpCustomCSS, inpAutoPublish: $('#AutoPublish').is(':checked'), inpExpire:  $.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate'))    },
	        beforeSend: function( xhr ) {
	            $('#processingPayment').show();
	            isAjax = 1;
	        },
	        error: function(XMLHttpRequest, textStatus, errorThrown) {
	            $('#processingPayment').hide();
	            bootbox.dialog({
	                message:'Save MLP Fail. Please try again later.',
	                title: "Setup MLP",
	                buttons: {
	                    success: {
	                        label: "Ok",
	                        className: "btn btn-medium btn-success-custom",
	                        callback: function() {}
	                    }
	                }
	            });
	            isAjax = 0;
	        },
	        success:
	            function(d) {
	            	isAjax = 0;
	                $('#processingPayment').hide();
	                if(d.RXRESULTCODE == 1)
	                {

		                <cfif action NEQ "template" AND  action NEQ "add">
		               		$('#tblListHistory').dataTable().fnDraw();
		               	</cfif>

		               	if(inpShowPreview)
		               	{
			               	$('#MLPURLStatus').html('');

			               	UpdateSaveCountSinceLastPublished();

			               	<!--- Dont update the iframe until most recent changes are auto-saved --->
			               	var src = '/mlp-x/lz/index.cfm?isPreview=1&inpURL=' + $('#cppxURL').val();
			                $("#PreviewModal iframe").attr({'src':src});
			               	$('#PreviewModal').modal('show');
			            }
			            else
			            {
			                bootbox.dialog({
		                        message:'MLP is saved successfully.',
		                        title: "Save MLP",
		                        buttons: {
		                            success: {
		                                label: "OK",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() {UpdateSaveCountSinceLastPublished(); }
		                            }
		                        }
		                    });
	                   }

	                }
	                else if(d.RXRESULTCODE == 2)
	                {
		                if(inpShowPreview)
		               	{
			               	UpdateSaveCountSinceLastPublished();

			               	<!--- Dont update the iframe untill most recent changes are auto-saved --->
			               	var src = '/mlp-x/lz/index.cfm?isPreview=1&inpURL=' + $('#cppxURL').val();
			                $("#PreviewModal iframe").attr({'src':src});
			               	$('#PreviewModal').modal('show');
			            }
			            else
			            {
			                bootbox.dialog({
		                        message:'MLP is already up to date.',
		                        title: "Save MLP",
		                        buttons: {
		                            success: {
		                                label: "OK",
		                                className: "btn btn-medium btn-success-custom",
		                                callback: function() {UpdateSaveCountSinceLastPublished(); }
		                            }
		                        }
		                    });
	                   }

	                }
	                else if(d.RXRESULTCODE == -3)
	                {
		            	<!--- mlp-sim --->
		            	<!--- Invalid URL --->

		                <!--- Show the Publish Side Panel --->
						$('.side-nav .publish-mlp').click();

		                $('#cppxURL').attr('data-content', "URL Already In Use");
		                $('#cppxURL').popover('show');

	                }
	                else
	                {
	                    bootbox.dialog({
	                        message: d.MESSAGE, // 'Create MLP Fail3. Please try again later.',
	                        title: "MLP Error",
	                        buttons: {
	                            success: {
	                                label: "Ok",
	                                className: "btn btn-medium btn-success-custom",
	                                callback: function() {}
	                            }
	                        }
	                    });
	                }
	            }
	        });
	    }
	    catch(ex)
	    {
	        $('#processingPayment').hide();
	        bootbox.dialog({
	            message:'Save MLP Failed' + ex,
	            title: "MLP Error",
	            buttons: {
	                success: {
	                    label: "Ok",
	                    className: "btn btn-medium btn-success-custom",
	                    callback: function() {}
	                }
	            }
	        });
	        $('.btn-group-answer-security-question').show();
	    }

	}


	function SetupDroppables(inpObj)
	{
		inpObj.droppable({
		  greedy: true,
		  accept: ".draggable",
	      classes: {
	        "ui-droppable-active": "ui-state-default"
	      },
	      <!--- Visual feedback cues --->
			over: function(event, ui)
			{

				$(".SettingsOverlay").remove();

				<!--- Only display this overlay when a widget is being dragged onto the stage --->
				if(ui.draggable.hasClass("tile-item"))
					$("<div class='SettingsOverlay'/>").appendTo($(this));

<!---
		          	 <!--- Get where obj was dropped --->
	            var $newPosX = ui.offset.left - $(this).offset().left;
				var $newPosY = ui.offset.top - $(this).offset().top;

	<!--- Keep the MLP-Master-Section top dog --->
		            if($newPosY <=  ($(this).height()/2) )
		          	{
						console.log('upper');
		          	}
		          	else
		          	{
				        console.log('lower');
		          	}

--->

			},
			<!--- Visual feedback cues --->
			out: function(event, ui)
			{
				$(".SettingsOverlay").remove();
			},
			drop: function( event, ui ) {

				$(".SettingsOverlay").remove();

	            <!--- Remove empty box classes --->
	            $(this).removeClass("MLPMasterRowEmpty");

	            <!--- Get where obj was dropped --->
	            var $newPosX = ui.offset.left - $(this).offset().left;
				var $newPosY = ui.offset.top - $(this).offset().top;


	            <!--- Check what is being dropped --->
	            if(ui.draggable.hasClass("mlp-headline"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPHeadlineTemplate, "MLPHeadlineTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPHeadlineTemplate);

		            BindDeletableEvents(NewObj);
		            BindEditMenu(NewObj);
		            BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		            var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}

	            }

 	            if(ui.draggable.hasClass("mlp-text"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPTextTemplate, "MLPTextTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPTextTemplate);

		            BindDeletableEvents(NewObj);
		            BindEditMenu(NewObj);
		            BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		            var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}
	            }

	            if(ui.draggable.hasClass("mlp-article-one"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPArticleOneTemplate, "MLPArticleOneTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPArticleOneTemplate);

		            BindDeletableEvents(NewObj);
		            BindEditMenu(NewObj);
		            BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		            var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}
	            }

				if(ui.draggable.hasClass("mlp-section-three"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPSectionThreeTemplate, "MLPSectionThreeTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPSectionThreeTemplate);

		            BindDeletableEvents(NewObj);
		            BindEditMenu(NewObj);
		            BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		             	<!--- Bind any sub section sections settings --->
				   	NewObj.find('.MLPEditable').each(function( index ) {

			           	BindSectionSettings($(this), $(this).find('.MLPEditMenu').find('.mlp-icon-settings'));

 					   	BindEditMenu($(this));

					   	BindDeletableEvents($(this));

					   	SetupDroppables($(this));

					   	<!--- Special section for <img> tags --->
					   	BindSectionSettings($(this).find('img'), $(this).find('.mlp-icon-img-settings'));

					   	if($(this).find('img'))
					   		BindImgSubMenu($(this));

					});

		            var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}
	            }


	            if(ui.draggable.hasClass("mlp-line"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPLineTemplate, "MLPLineTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPLineTemplate);

		            BindDeletableEvents(NewObj);

		            BindEditMenu(NewObj);

		            <!--- Special section for <hr> tags --->
		            BindSectionSettings(NewObj.find('hr'), NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		            var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}
	            }

				if(ui.draggable.hasClass("mlp-master-section"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <!--- col-sm-10 col-md-offset-1 --->

		            <cfoutput>
						var #toScript(MLPSectionTemplate, "MLPSectionTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPSectionTemplate);

		           	BindSortable(NewObj);

		           	BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings') );

		           	BindEditMenu(NewObj);

		           	BindSplitEvents(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-split-section') );

		           	SetupDroppables(NewObj);

		           	BindDeletableEvents(NewObj);

		          	var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}

	            }

				if(ui.draggable.hasClass("mlp-img"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPImgContainer, "MLPImgContainerjsVar")#;
					</cfoutput>

					var NewObj = $(MLPImgContainerjsVar);

		            BindDeletableEvents(NewObj);
		            BindEditMenu(NewObj);
		            BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings'));

		            <!--- Special section for <img> tags --->
		            BindSectionSettings(NewObj.find('img'), NewObj.find('.mlp-icon-img-settings'));

		            BindImgSubMenu(NewObj);

		           	<!--- Keep the MLP-Master-Section top dog --->
		            if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}

	            }


	            <!--- MLPHero --->
	            if(ui.draggable.hasClass("mlp-hero"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <cfoutput>
						var #toScript(MLPHeroTemplate, "MLPHeroTemplate")#;
					</cfoutput>

		            var NewObj = $(MLPHeroTemplate);

		            <!--- Bind the main section settings --->
		            BindSectionSettings(NewObj, NewObj.find('.MLPEditMenu').find('.mlp-icon-settings'));
		            BindEditMenu(NewObj);
		            BindDeletableEvents(NewObj);

		           	<!--- Bind any sub section sections settings --->
				   	NewObj.find('.MLPMasterSection').each(function( index ) {

			           BindSectionSettings($(this), $(this).find('.MLPEditMenu').find('.mlp-icon-settings'));

 					   BindEditMenu($(this));

					   BindDeletableEvents($(this));

					   SetupDroppables($(this));
					});


					<!--- setup any droppables --->
		            NewObj.find('.MLPEditable').each(function( index ) {



					});

					NewObj.find('div').each(function( index ) {

					 	<!--- Allow changing of background image --->
						if ($(this).css('background-image') != 'none' && $(this).css('background-image') != '')
						{
							if($(this).find('.hover-menu-container').length > 0)
								BindImgSubMenu($(this));
						}
					});

		          	var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}

	            }

	            <!--- mlp-section-template --->
	            if(ui.draggable.hasClass("mlp-section-template"))
	            {

		            <!--- Default just put in a regular section --->

		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <!--- col-sm-10 col-md-offset-1 --->

		            <cfoutput>
						var #toScript(MLPSectionTemplate, "MLPSectionTemplate")#;
					</cfoutput>

		           	var NewObj = $(MLPSectionTemplate);

		           	BindSectionSettings(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-settings') );

		           	BindEditMenu(NewObj);

		           	BindSplitEvents(NewObj, NewObj.children('.MLPEditMenu').find('.mlp-icon-split-section') );

		           	SetupDroppables(NewObj);

		           	BindDeletableEvents(NewObj);

		          	var $ref = null;

		          	<!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(NewObj);
		          	}
		          	else
		          	{
				        $ref = $(this).append(NewObj);
		          	}


		          	<!--- Then allow user to popup a template seletion tool --->

		            <!--- Store reference to this section so it can be replaced with template selection --->
		            $('#Section-Template-Picker').attr('data-dropped-target');


		            <!--- Show modal picker --->
					$('#Section-Template-Picker').modal('show');

	            }


	            <!--- Handle the drag and drop of objects around on stage --->
	            if(ui.draggable.hasClass("MLPImageContainer") || ui.draggable.hasClass("MLPEditable") || ui.draggable.hasClass("MLPMasterSectionContainer"))
	            {
		            <!--- On start of helper will store current state - no need to store here - if you do it still has helper(s) visible so dont store it for .sortable-item --->
		            if(ui.draggable.hasClass("tile-item"))
		            	<!--- Store last state of the MLP --->
		            	StoreMLPBeforeAction(true);

		            <!--- Get parent object before child object moves  --->
					var parentObj = ui.draggable.parent();
					var parentObj2 = parentObj.parent();

		            var $ref = null;

		            <!--- Keep the MLP-Master-Section top dog --->
			        if($newPosY <=  ($(this).height()/2) && $(this).attr('id') != "MLP-Master-Section")
		          	{
						$ref = $(this).prepend(ui.draggable);
		          	}
		          	else
		          	{
				        $ref = $(this).append(ui.draggable);
		          	}

		            <!--- look for empty rows added by splits --->
					if(parentObj.hasClass('row'))
					{
						if(!parentObj.children().length)
						{
							parentObj.remove();

							parentObj = parentObj2;
						}
					}

					<!--- Special MLP feedback borders --->
					if(parentObj.attr('data-helper-text') == "Box Container")
					{
						<!--- Add back in empty box classes --->
						parentObj.addClass("MLPMasterRowEmpty");
					}

	            }

	      	}
	    });


	}

	function SetupSectionSettingsPanel()
	{
		<!--- Section Settings Items --->
		$('#mlp-section-settings #section-id').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr( "id", $(this).val() );

		});

		<!--- Show Image Chooser Modal --->
		$('#mlp-section-settings .ImageChooserModal').on("click touchstart", function(e){

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			<!--- Bind target to this modal --->
			$('#ImageChooserModal').data('data-target', $TargetObj);

			$('#ImageChooserModal').attr('data-image-target', $(this).attr('data-image-target')  );

			$('#ImageChooserModal').modal('show');

			<!--- stop event propogation --->
			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		<!--- Section Settings Background Color Picker --->
		$('#mlp-section-settings #background-color').spectrum({
		     	color: "#ECC",
			    showInput: true,
			    showAlpha: true,
			    className: "full-spectrum",
			    showInitial: true,
			    showPalette: true,
			    showSelectionPalette: true,
			    maxSelectionSize: 10,
			    preferredFormat: "hex",
			    localStorageKey: "spectrum.demo",
			    move: function (color) {

			        var $TargetObj = $('#mlp-section-settings').data('data-target');

			        if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
					  	<!--- Store last state of the MLP --->
					  	StoreMLPBeforeAction(true);

				        $TargetObj.css( "background-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );

				        <!--- Special case border color stays in sync with section --->
					  	if($TargetObj.hasClass('section-arrow-top') || $TargetObj.hasClass('section-arrow-bottom'))
					  	{
						  	$TargetObj.css( "border-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );
					  	}
					}

			    },
			    show: function () {

			    },
			    beforeShow: function () {

			    },
			    hide: function () {

				    var $TargetObj = $('#mlp-section-settings').data('data-target');

				  	if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
					  	$TargetObj.css( "background-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );

					  	<!--- Special case border color stays in sync with section --->
					  	if($TargetObj.hasClass('section-arrow-top') || $TargetObj.hasClass('section-arrow-bottom'))
					  	{
						  	$TargetObj.css( "border-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );
					  	}
					}

			    },
			    change: function() {

				  	var $TargetObj = $('#mlp-section-settings').data('data-target');

				  	if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
					  	$TargetObj.css( "background-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );

					  	<!--- Special case border color stays in sync with section --->
					  	if($TargetObj.hasClass('section-arrow-top') || $TargetObj.hasClass('section-arrow-bottom'))
					  	{
						  	$TargetObj.css( "border-color", $('#mlp-section-settings #background-color').spectrum("get").toRgbString() );
					  	}
					}

			    },
			    palette: [
			        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
			        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
			        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
			        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
			        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
			        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
			        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
			        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
			        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
			        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
			        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
			        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
			    ]
		});

		<!--- Section Settings Background Color Picker --->
		$('#mlp-section-settings #color').spectrum({
		     	color: "#ECC",
			    showInput: true,
			    showAlpha: true,
			    className: "full-spectrum",
			    showInitial: true,
			    showPalette: true,
			    showSelectionPalette: true,
			    maxSelectionSize: 10,
			    preferredFormat: "hex",
			    localStorageKey: "spectrum.demo",
			    move: function (color) {

				    var $TargetObj = $('#mlp-section-settings').data('data-target');

			        if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
						<!--- Store last state of the MLP --->
						StoreMLPBeforeAction(true);

			       		 $TargetObj.css( "color", $('#mlp-section-settings #color').spectrum("get").toRgbString() );
				  	}
			    },
			    show: function () {

			    },
			    beforeShow: function () {

			    },
			    hide: function () {

				    var $TargetObj = $('#mlp-section-settings').data('data-target');
				  	if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
				  		$TargetObj.css( "color", $('#mlp-section-settings #color').spectrum("get").toRgbString() );
				  	}
			    },
			    change: function() {

				  	var $TargetObj = $('#mlp-section-settings').data('data-target');
				  	if(typeof $TargetObj != 'undefined' && $TargetObj != null)
				  	{
				  		$TargetObj.css( "color", $('#mlp-section-settings #color').spectrum("get").toRgbString() );
				  	}
			    },
			    palette: [
			        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
			        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
			        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
			        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
			        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
			        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
			        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
			        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
			        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
			        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
			        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
			        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
			    ]
		});

		<!--- background-repeat Style --->
		$('#mlp-section-settings #background-repeat').change(function() {

		    <!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "background-repeat", $(this).val());

		});

		<!--- background-position Style --->
		$('#mlp-section-settings #background-position').change(function() {

		    <!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "background-position", $(this).val());
		});

		<!--- background-size Style --->
		$('#mlp-section-settings #background-size').change(function() {

		    <!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "background-size", $(this).val());
		});

		<!--- background-attachment Style --->
		$('#mlp-section-settings #background-attachment').change(function() {

		    <!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "background-attachment", $(this).val());
		});

		<!--- Section Settings Border Color Picker --->
		$('#mlp-section-settings #border-color').spectrum({
		     	color: "#ECC",
			    showInput: true,
			    className: "full-spectrum",
			    showInitial: true,
			    showPalette: true,
			    showSelectionPalette: true,
			    maxSelectionSize: 10,
			    preferredFormat: "hex",
			    localStorageKey: "spectrum.demo",
			    move: function (color) {

			        <!--- Store last state of the MLP --->
					StoreMLPBeforeAction(true);

			        var $TargetObj = $('#mlp-section-settings').data('data-target');
				  	$TargetObj.css( "border-color", $('#mlp-section-settings #border-color').spectrum("get").toRgbString() );

			    },
			    show: function () {

			    },
			    beforeShow: function () {

			    },
			    hide: function () {
			    	<!--- Store last state of the MLP --->
					StoreMLPBeforeAction(true);

				    var $TargetObj = $('#mlp-section-settings').data('data-target');
				  	$TargetObj.css( "border-color", $('#mlp-section-settings #border-color').spectrum("get").toRgbString() );

			    },
			    change: function() {

			        <!--- Store last state of the MLP --->
					StoreMLPBeforeAction(true);

				  	var $TargetObj = $('#mlp-section-settings').data('data-target');
				  	$TargetObj.css( "border-color", $('#mlp-section-settings #border-color').spectrum("get").toRgbString() );

			    },
			    palette: [
			        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
			        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
			        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
			        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
			        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
			        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
			        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
			        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
			        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
			        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
			        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
			        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
			    ]
		});



		<!--- Set Font Style --->
		$('#mlp-section-settings #bold').on("click touchstart", function(e){
  			var $TargetObj = $('#mlp-section-settings').data('data-target');

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			if($TargetObj.css( "font-weight" ) == "bold")
			{
				$TargetObj.css( "font-weight", 'normal' );
				$('#mlp-section-settings #bold').removeClass("active");
			}
			else
			{
				$TargetObj.css( "font-weight", 'bold');
				$('#mlp-section-settings #bold').addClass("active");
			}

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		$('#mlp-section-settings #italic').on("click touchstart", function(e){
  			var $TargetObj = $('#mlp-section-settings').data('data-target');

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			if($TargetObj.css( "font-style" ) == "italic")
			{
				$TargetObj.css( "font-style", 'normal' );
				$('#mlp-section-settings #italic').removeClass("active");
			}
			else
			{
				$TargetObj.css( "font-style", 'italic');
				$('#mlp-section-settings #italic').addClass("active");
			}

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		<!--- Alignment --->
		$('#mlp-section-settings #Alignment .fa-align-left').on("click touchstart", function(e){

  			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "text-align", 'left' );

			$('#mlp-section-settings #Alignment .fa-align-left').addClass("active");
			$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		$('#mlp-section-settings #Alignment .fa-align-center').on("click touchstart", function(e){

  			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "text-align", 'center' );

			$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-center').addClass("active");
			$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		$('#mlp-section-settings #Alignment .fa-align-right').on("click touchstart", function(e){

  			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "text-align", 'right' );

			$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-right').addClass("active");
			$('#mlp-section-settings #Alignment .fa-circle').removeClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		$('#mlp-section-settings #Alignment .fa-circle').on("click touchstart", function(e){

  			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "text-align", 'inherit' );

			$('#mlp-section-settings #Alignment .fa-align-left').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-center').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-align-right').removeClass("active");
			$('#mlp-section-settings #Alignment .fa-circle').addClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});


		<!--- Border Width --->
		$('#mlp-section-settings #border-width').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "border-width", $(this).val() + 'px' );

			$('#mlp-section-settings #border-width-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #border-width-fader').on('input', function () {
		    $(this).trigger('change');
		});

		$('#mlp-section-settings #border-width-fader').change(function() {
  			$('#mlp-section-settings #border-width').val($(this).val());
  			$('#mlp-section-settings #border-width').change();
  		});


  		<!--- Border Radius --->
		$('#mlp-section-settings #border-radius').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "border-radius", $(this).val() + 'px' );

			$('#mlp-section-settings #border-radius-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #border-radius-fader').on('input', function () {
		    $(this).trigger('change');
		});

		$('#mlp-section-settings #border-radius-fader').change(function() {
  			$('#mlp-section-settings #border-radius').val($(this).val());
  			$('#mlp-section-settings #border-radius').change();
  		});

		<!--- Border Style --->
		$('#mlp-section-settings #border-style').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "border-style", $(this).val());
		});



		$('#mlp-section-settings #arrow-border .fa-caret-up').on("click touchstart", function(e){

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.addClass( "section-arrow-top");
			$TargetObj.removeClass( "section-arrow-bottom");

			$TargetObj.css("border-color", $TargetObj.css("background-color"));
			$('#mlp-section-settings #border-color').spectrum("set", $TargetObj.css("background-color"));

			$('#mlp-section-settings #arrow-border .fa-caret-up').addClass("active");
			$('#mlp-section-settings #arrow-border .fa-caret-down').removeClass("active");
			$('#mlp-section-settings #arrow-border .fa-circle').removeClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('#mlp-section-settings #arrow-border .fa-caret-down').on("click touchstart", function(e){

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.removeClass( "section-arrow-top");
			$TargetObj.addClass( "section-arrow-bottom");

			$TargetObj.css("border-color", $TargetObj.css("background-color"));
			$('#mlp-section-settings #border-color').spectrum("set", $TargetObj.css("background-color"));

			$('#mlp-section-settings #arrow-border .fa-caret-up').removeClass("active");
			$('#mlp-section-settings #arrow-border .fa-caret-down').addClass("active");
			$('#mlp-section-settings #arrow-border .fa-circle').removeClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('#mlp-section-settings #arrow-border .fa-circle').on("click touchstart", function(e){

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.removeClass( "section-arrow-top");
			$TargetObj.removeClass( "section-arrow-bottom");

			$('#mlp-section-settings #arrow-border .fa-caret-up').removeClass("active");
			$('#mlp-section-settings #arrow-border .fa-caret-down').removeClass("active");
			$('#mlp-section-settings #arrow-border .fa-circle').addClass("active");

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

		$('#mlp-section-settings #padding-top').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "padding-top", $(this).val() + 'px' );

			$('#mlp-section-settings #padding-top-fader').val(parseInt($(this).val()));

		});


		$('#mlp-section-settings #padding-top-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #padding-top-fader').change(function() {
  			$('#mlp-section-settings #padding-top').val($(this).val());
  			$('#mlp-section-settings #padding-top').change();
  		});

		$('#mlp-section-settings #padding-bottom').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "padding-bottom", $(this).val() + 'px' );

			$('#mlp-section-settings #padding-bottom-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #padding-bottom-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #padding-bottom-fader').change(function() {
  			$('#mlp-section-settings #padding-bottom').val($(this).val());
  			$('#mlp-section-settings #padding-bottom').change();
  		});

		$('#mlp-section-settings #padding-left').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "padding-left", $(this).val() + 'px' );

			$('#mlp-section-settings #padding-left-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #padding-left-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #padding-left-fader').change(function() {
  			$('#mlp-section-settings #padding-left').val($(this).val());
  			$('#mlp-section-settings #padding-left').change();
  		});

		$('#mlp-section-settings #padding-right').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "padding-right", $(this).val() + 'px' );

			$('#mlp-section-settings #padding-right-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #padding-right-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #padding-right-fader').change(function() {
  			$('#mlp-section-settings #padding-right').val($(this).val());
  			$('#mlp-section-settings #padding-right').change();
  		});

  		$('#mlp-section-settings #margin-top').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "margin-top", $(this).val() + 'px' );

			$('#mlp-section-settings #margin-top-fader').val(parseInt($(this).val()));

		});


		$('#mlp-section-settings #margin-top-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #margin-top-fader').change(function() {
  			$('#mlp-section-settings #margin-top').val($(this).val());
  			$('#mlp-section-settings #margin-top').change();
  		});

		$('#mlp-section-settings #margin-bottom').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "margin-bottom", $(this).val() + 'px' );

			$('#mlp-section-settings #margin-bottom-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #margin-bottom-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #margin-bottom-fader').change(function() {
  			$('#mlp-section-settings #margin-bottom').val($(this).val());
  			$('#mlp-section-settings #margin-bottom').change();
  		});

		$('#mlp-section-settings #margin-left').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "margin-left", $(this).val() + 'px' );

			$('#mlp-section-settings #margin-left-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #margin-left-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #margin-left-fader').change(function() {
  			$('#mlp-section-settings #margin-left').val($(this).val());
  			$('#mlp-section-settings #margin-left').change();
  		});

		$('#mlp-section-settings #margin-right').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "margin-right", $(this).val() + 'px' );

			$('#mlp-section-settings #margin-right-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #margin-right-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #margin-right-fader').change(function() {
  			$('#mlp-section-settings #margin-right').val($(this).val());
  			$('#mlp-section-settings #margin-right').change();
  		});

		$('#mlp-section-settings #width').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "width", $(this).val() + $('#mlp-section-settings #width-units').val() );
				$TargetObj.attr('mlp-width', $(this).val());

				$('#mlp-section-settings #width-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "width", 'initial' );
				$TargetObj.attr('mlp-width', 'initial');
				$('#mlp-section-settings #width-fader').val(0);
			}
		});

		$('#mlp-section-settings #width-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #width-fader').change(function() {
  			$('#mlp-section-settings #width').val($(this).val());
  			$('#mlp-section-settings #width').change();
  		});

		$('#mlp-section-settings #min-width').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "min-width", $(this).val() + $('#mlp-section-settings #min-width-units').val() );
				$TargetObj.attr('mlp-min-width', $(this).val());
				$('#mlp-section-settings #min-width-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "min-width", 'initial' );
				$TargetObj.attr('mlp-min-width', 'initial');
				$('#mlp-section-settings #min-width-fader').val(0);
			}

		});

		$('#mlp-section-settings #min-width-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #min-width-fader').change(function() {
  			$('#mlp-section-settings #min-width').val($(this).val());
  			$('#mlp-section-settings #min-width').change();
  		});

  		$('#mlp-section-settings #max-width').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "max-width", $(this).val() + $('#mlp-section-settings #max-width-units').val() );
				$TargetObj.attr('mlp-max-width', $(this).val());
				$('#mlp-section-settings #max-width-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "max-width", 'initial' );
				$TargetObj.attr('mlp-max-width', 'initial');
				$('#mlp-section-settings #max-width-fader').val(0);
			}
		});

		$('#mlp-section-settings #max-width-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #max-width-fader').change(function() {
  			$('#mlp-section-settings #max-width').val($(this).val());
  			$('#mlp-section-settings #max-width').change();
  		});



  		$('#mlp-section-settings #height').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "height", $(this).val() + $('#mlp-section-settings #height-units').val() );
				$TargetObj.attr('mlp-height', $(this).val());
				$('#mlp-section-settings #height-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "height", 'initial' );
				$TargetObj.attr('mlp-height', 'initial');

				$('#mlp-section-settings #height-fader').val(0);
			}
		});

		$('#mlp-section-settings #height-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #height-fader').change(function() {
  			$('#mlp-section-settings #height').val($(this).val());
  			$('#mlp-section-settings #height').change();
  		});

  		$('#mlp-section-settings #min-height').change(function() {

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "min-height", $(this).val() + $('#mlp-section-settings #min-height-units').val() );
				$TargetObj.attr('mlp-min-height', $(this).val());
				$('#mlp-section-settings #min-height-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "min-height", 'initial' );
				$TargetObj.attr('mlp-min-height', 'initial');
				$('#mlp-section-settings #min-height-fader').val(0);
			}

		});

		$('#mlp-section-settings #min-height-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #min-height-fader').change(function() {
  			$('#mlp-section-settings #min-height').val($(this).val());
  			$('#mlp-section-settings #min-height').change();
  		});

  		$('#mlp-section-settings #max-height').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if(parseInt($(this).val()) > 0 || $(this).val() == 'auto' || $(this).val() == 'initial' || $(this).val() == 'inherit')
			{
				$TargetObj.css( "max-height", $(this).val() + $('#mlp-section-settings #max-height-units').val() );
				$TargetObj.attr('mlp-max-height', $(this).val());
				$('#mlp-section-settings #max-height-fader').val(parseInt($(this).val()));
			}
			else
			{
				$TargetObj.css( "max-height", 'initial' );
				$TargetObj.attr('mlp-max-height', 'initial');
				$('#mlp-section-settings #max-height-fader').val(0);
			}
		});

		$('#mlp-section-settings #max-height-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #max-height-fader').change(function() {
  			$('#mlp-section-settings #max-height').val($(this).val());
  			$('#mlp-section-settings #max-height').change();
  		});


  		$('#mlp-section-settings #width-units').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('width-units', $('#mlp-section-settings #width-units').val())

			$('#mlp-section-settings #width-fader').trigger('change');

		});

		$('#mlp-section-settings #min-width-units').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('min-width-units', $('#mlp-section-settings #min-width-units').val())

			$('#mlp-section-settings #min-width-fader').trigger('change');

		});

		$('#mlp-section-settings #max-width-units').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('max-width-units', $('#mlp-section-settings #max-width-units').val())

			$('#mlp-section-settings #max-width-fader').trigger('change');

		});

		$('#mlp-section-settings #height-units').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('height-units', $('#mlp-section-settings #height-units').val())

			$('#mlp-section-settings #height-fader').trigger('change');

		});

  		$('#mlp-section-settings #min-height-units').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('min-height-units', $('#mlp-section-settings #min-height-units').val())

			$('#mlp-section-settings #min-height-fader').trigger('change');

		});

		$('#mlp-section-settings #max-height-units').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.attr('max-height-units', $('#mlp-section-settings #max-height-units').val())

			$('#mlp-section-settings #max-height-fader').trigger('change');

		});

  		<!--- Auto margin option --->
  		$('#mlp-section-settings #auto-margin').change(function() {

	  		<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

  			var $TargetObj = $('#mlp-section-settings').data('data-target');

  			if($(this).is(":checked"))
  			{

	            $TargetObj.css( "margin", '1em auto' );
	            $TargetObj.attr( "data-auto-margin", '1em auto' );

	        }
	        else
	        {
		        $TargetObj.css( "margin", '');
		        $TargetObj.attr( "data-auto-margin", 'not' );
	        }


  		});


  		<!--- Advanced Stuff --->
		<!--- Position --->
		$('#mlp-section-settings #position').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css('position', $('#mlp-section-settings #position').val())

		});

		$('#mlp-section-settings #top').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "top", $(this).val() + 'px' );

			$('#mlp-section-settings #top-fader').val(parseInt($(this).val()));

		});


		$('#mlp-section-settings #top-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #top-fader').change(function() {
  			$('#mlp-section-settings #top').val($(this).val());
  			$('#mlp-section-settings #top').change();
  		});

		$('#mlp-section-settings #bottom').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "bottom", $(this).val() + 'px' );

			$('#mlp-section-settings #bottom-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #bottom-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #bottom-fader').change(function() {
  			$('#mlp-section-settings #bottom').val($(this).val());
  			$('#mlp-section-settings #bottom').change();
  		});

		$('#mlp-section-settings #left').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "left", $(this).val() + 'px' );

			$('#mlp-section-settings #left-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #left-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #left-fader').change(function() {
  			$('#mlp-section-settings #left').val($(this).val());
  			$('#mlp-section-settings #left').change();
  		});

		$('#mlp-section-settings #right').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css( "right", $(this).val() + 'px' );

			$('#mlp-section-settings #right-fader').val(parseInt($(this).val()));

		});

		$('#mlp-section-settings #right-fader').on('input', function () {
		    $(this).trigger('change');
		});


		$('#mlp-section-settings #right-fader').change(function() {
  			$('#mlp-section-settings #right').val($(this).val());
  			$('#mlp-section-settings #right').change();
  		});


		<!--- Float --->
		$('#mlp-section-settings #float').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');
			$TargetObj.css('float', $('#mlp-section-settings #float').val())

		});

		<!--- ClearFix --->
		$('#mlp-section-settings #ClearFix').change(function() {

			<!--- Store last state of the MLP --->
            StoreMLPBeforeAction(true);

			var $TargetObj = $('#mlp-section-settings').data('data-target');

			if($('#ClearFix').is(':checked'))
				$TargetObj.addClass('clearfix');
			else
				$TargetObj.removeClass('clearfix');
		});

	}

	function BindImgSubMenu(inpObj)
	{
<!--- old logic
		<!--- Show Publish panel --->
		inpObj.on("click touchstart", function(e){

			$(this).children('.hover-menu-container').toggle();

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

		<!--- http://stackoverflow.com/questions/1403615/use-jquery-to-hide-a-div-when-the-user-clicks-outside-of-it --->
		$(document).mouseup(function (e)
		{
			<!--- if the target of the click isnt the container... --->
		    if (!inpObj.is(e.target)
		        && inpObj.has(e.target).length === 0) <!--- ... nor a descendant of the container --->
		    {
		        $(this).find('.hover-menu-container').hide();
		    }
		});
--->

		<!--- Show Image Chooser --->
		inpObj.find('.ImageChooserModal').on("click touchstart", function(e){

			<!--- Bind target to this modal --->
			$('#ImageChooserModal').data('data-target', inpObj);
			$('#ImageChooserModal').modal('show');

			$('#ImageChooserModal').attr('data-image-target', $(this).attr('data-image-target')  );

			<!--- stop event propogation to prevent hide of menu on click --->
			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

		});

	}

	<!--- http://jsfiddle.net/egstudio/aFMWg/1/ --->
	$.fn.inlineEdit = function(replaceWith, connectWith) {

	    $(this).hover(function() {
	        $(this).addClass('hover');
	    }, function() {
	        $(this).removeClass('hover');
	    });

	    $(this).on("click touchstart", function(e){

	        var elem = $(this);

	        elem.hide();
	        elem.after(replaceWith);
	        replaceWith.val(elem.text());
	        replaceWith.focus();

	        replaceWith.blur(function() {

	            if ($(this).val() != "") {
	                connectWith.val($(this).val()).change();
	                elem.text($(this).val());
	            }

	            $(this).remove();
	            elem.show();
	        });

	        <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

	    });
	};

	function bindPreviewLinks(inpObj)
	{

		inpObj.off();

		inpObj.on("click touchstart", function(e){

			$('#inpHDT').val( $(this).attr('data-hdt'));

			var src = '/mlp-x/lz/index.cfm?inpURL=' + $('#cppxURL').val() + '&inpHDT=' + $(this).attr('data-hdt');
		    var height = $(this).attr('data-height') || 300;
		    var width = $(this).attr('data-width') || 400;

		    $("#PreviewModal iframe").attr({'src':src,
                               'height': height,
                               'width': width});


            $('#PreviewModal').find('.modal-dialog').css({
		               width:'auto', //probably not needed
		              height:'auto', //probably not needed
		              'max-height':'100%'
		       });


             $('#PreviewModal').find('.modal-body').css({
		              width:width/2,
		              height:height/2,
		              'max-height':'100%',
		              'overflow':'hidden',
		              'padding':'0',
		       });


		    <!--- Enable the revert button --->
			$('#RevertHistorical').show();

			<!--- Show modal with iframe for preview --->
			$('#PreviewModal').modal('show');

			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();


		});

	}

	function UpdateSaveCountSinceLastPublished()
	{

		<cfif action NEQ "template" AND  action NEQ "add">

		  	try
			{

		      $.ajax({
		        type: "POST",
		        url: '/session/sire/models/cfc/mlp.cfc?method=GetCountSavesSinceLastPublishMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		        dataType: 'json',
		        data: {MLPId:$('#ccpxDataId').val()},
		        beforeSend: function( xhr ) {

		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {

		        },
		        success:
		            function(d) {
		            	isAjax = 0;
		                $('#processingPayment').hide();
		                if(d.RXRESULTCODE == 1)
		                {
			                $('#CountSavedSinceLastPublished').html(d.COUNT);
			                $('#LastPublishedDate').html(d.LASTPUBLISHED);
			                $('#LastSavedDate').html(d.LASTSAVED);
			            }
		                else
		                {

		                }
		            }
		        });
		    }
		    catch(ex)
		    {


		    }

		</cfif>

	}

<!---
	<!--- Need to bind draggabe to new objects well as existing new ones --->
	function BindDraggable(inpObj, inpHelperText)
	{
		return;

		if(typeof inpHelperText == 'undefined')
			inpHelperText = '';

		if(typeof inpObj.attr("data-helper-text") !== 'undefined')
			inpHelperText = inpObj.attr("data-helper-text");

		 inpObj.draggable(
		   {
			    appendTo: '#mlp-builder',
			    revert: "invalid",
				containment: "#main-stage-content",
				stack: "#mlp-builder",
				opacity: 0.8,
				// helper: "original",
				// helper: "clone",
				cursor: "move",
			      cursorAt: { top: 0, left: 75 },
			      helper: function( event ) {
			        return $( "<div class='ui-widget-header CustomHelperObj'><div style='margin-top:-2.2em; min-height:2em; line-height:1.2em;'><i class='fa fa-plus mlp-icon'></i></div><div class='mlp-obj-name-container'><div class='mlp-obj-name'>" +  inpHelperText + "</div></div></div>" );
			      },
				zIndex: 300,
				handle: ".mlp-icon-drag"
			}
	    );

	}
--->


	<!--- Need to bind sortable to new section objects --->
	function BindSortable(inpObj, inpHelperText)
	{
		if(typeof inpHelperText == 'undefined')
			inpHelperText = '';

		if(typeof inpObj.attr("data-helper-text") !== 'undefined')
			inpHelperText = inpObj.attr("data-helper-text");


		inpObj.sortable({

					containment: "#main-stage-content",
					connectWith: ".connectedSortable",
				    placeholder: "ui-state-highlight",
				    opacity: 0.8,
				    cursor: "move",
				    cursorAt: { top: 0, left: 75 },
				    helper: function( event, ui ) {

				    	<!--- Store values before sorting is started - target in global variable so it can be undone --->
						StoreMLPBeforeAction(true);

				   		return $( "<div class='xui-widget-header CustomHelperObj'><div style='margin-top:-2.2em; min-height:2em; line-height:1.2em;'><i class='fa fa-plus mlp-icon'></i></div> <div class='mlp-obj-name-container'><div class='mlp-obj-name'>" + ui.attr("data-helper-text") + "</div></div></div>" );
			      	},
				    items: '.sortable-item',
				    cancel: ':input,button,p,h1,h2,h3,h4,h5,h6,h7',
				    handle: ".mlp-icon-drag",
				    start: function(e, ui ){
					     ui.placeholder.height(ui.helper.outerHeight());
					},
				    change: function( event, ui ) {

					},
					sort: function(e,ui){
		                /* The trick is that jQuery hides the inline element while user drags an absolute positioned clone so we want to deal only with the visible elements. Index order + 1 = UI order.*/
		                // $(ui.placeholder).html(Number(ui.parent(). $("#categories > .connectedSortable").index(ui.placeholder)+1));

<!---  SettingsOverlay ui-state-highlight
<!---
		                console.log(ui);
						console.log(ui.item);
		                console.log(ui.helper.parent());
--->
		                console.log(ui);
		                console.log(ui.item);

		                <!--- Get where obj was dropped --->
<!---
			            var $newPosX = ui.offset.left - $(this).offset().left;
						var $newPosY = ui.offset.top - $(this).offset().top;
--->


 console.log(ui.placeholder);
  console.log(ui.placeholder.position.left);
   console.log(ui.placeholder.position.top);

						var $newPosX = ui.placeholder.left - $(this).offset().left;
						var $newPosY = ui.placeholder.top - $(this).offset().top;

					//		console.log($newPosX);
					//		console.log($newPosY);




				          	<!--- Keep the MLP-Master-Section top dog --->
					        if($newPosY <=  (ui.helper.parent().height()/2) )
				          	{
								console.log('top');
				          	}
				          	else
				          	{
						       console.log('bottom');
				          	}

--->

		              }

		});

	}


	function InitCPPURLValidation()
	{
		<!--- Validate keyword in real time - ajax with warnings  --->
		$("#cppxURL").on("input", function() {

		    $('#MLPURLStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');

			<!--- Validate valid URL File Name--->
			var re = new RegExp(/^[0-9a-zA-Z\-\_\']+$/);
			if (!re.test($('#cppxURL').val().trim()))
			{
				$('#MLPURLStatus').css('color', 'red');
				$('#MLPURLStatus').html("Invalid Characters in URL - Must only be alphanumeric with no special characters and with no spaces allowed");
				return;
			}

		    <!--- Save Custom Responses - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/mlp.cfc?method=checkMLPUrlAvailability&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpURL:$('#cppxURL').val().trim(),
					MLPId:$('#ccpxDataId').val()

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						$('#MLPURLStatus').css('color', '#74c37f');
						$('#MLPURLStatus').html(d.MESSAGE);
					}
					else if (d.RXRESULTCODE == -3)
					{
						$('#MLPURLStatus').css('color', 'red');
						$('#MLPURLStatus').html(d.MESSAGE);
					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
						{
							$('#MLPURLStatus').css('color', 'red');
							$('#MLPURLStatus').html(d.MESSAGE);
						}
					}
				}

			});

		});
	}

	function StoreMLPBeforeAction(inpClearRedo)
	{
		var UndoObj = {};

		<!--- When I am bored I need to figure out why the clone(true, true) is not restoring events on append later on. I am just re-init events manually and only using the HTML for now --->
		UndoObj.HTML = 	$('#main-stage-content').html();
		UndoObj.CopiedDataAndEvents = $('#main-stage-content').children().clone(true, true);

		<!--- Max undo 500 events - What is the max number that wont cause problems here? --->
		if(UndoArray.length > 500)
			UndoArray.shift();  <!--- The shift() method removes the first item of an array, and returns that item. Note: This method changes the length of an array! --->

		<!--- Store last state of the MLP --->
		UndoArray.push(UndoObj);

		if(inpClearRedo)
		{	<!--- Clear the Redo Array - only allow redo unitl last change--->
			RedoArray = [];
		}
	}

	function StoreMLPBeforeUndo()
	{

		var RedoObj = {};

		<!--- When I am bored I need to figure out why the clone(true, true) is not restoring events on append later on. I am just re-init events manually and only using the HTML for now --->
		RedoObj.HTML = 	$('#main-stage-content').html();
		RedoObj.CopiedDataAndEvents = $('#main-stage-content').children().clone(true, true);

		<!--- Max undo 500 events - What is the max number that wont cause problems here? --->
		if(RedoArray.length > 500)
			RedoArray.shift();  <!--- The shift() method removes the first item of an array, and returns that item. Note: This method changes the length of an array! --->

		<!--- Store last state of the MLP --->
		RedoArray.push(RedoObj);

	}


	function MLPLoadTOCs()
	{

		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/mlp-toc.cfc?method=GetTOCs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						$('#TOCList').empty();

						<!--- Add the native list item --->
					    $('#TOCList').append($('<option>', {value: 0, text: 'Select TOC'}));

					    <!--- Add the data to the select2 object --->
					    $('#TOCList').select2('data', {value: 0, text: 'Select TOC'});

						for(var i=0; i<d.QUERYRES.ROWCOUNT; i++)
						{
						  	<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  	<!--- Use the Physical - non-changing ID for each reference --->

						    <!--- Add the native list item --->
						    $('#TOCList').append($('<option>', {value: d.QUERYRES.DATA.PKID_BI[i], text: d.QUERYRES.DATA.DESC_VCH[i]}));

						    <!--- Add the data to the select2 object --->
						    $('#TOCList').select2('data', {id: d.QUERYRES.DATA.PKID_BI[i], text: d.QUERYRES.DATA.DESC_VCH[i]});

						};
					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});

	}


	function MLPAddNewTOC()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=AddTOC&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpDesc : $('#New-TOC-Name').val()

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					MLPLoadTOCs();

				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});

	}

	function MLPLoadTopics(inpObj, inpParentId, inpParentIdType )
	{
		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/mlp-toc.cfc?method=GetTopics&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpParentId : inpParentId,
					inpParentIdType : inpParentIdType
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{

						var $SubTopicSection = inpObj.children('.MLPSubTopicsSection');

						<!--- Empty inpObj of sub content  --->
						$SubTopicSection.children().remove();

<!---
						<!--- default TOP level stays open by default - no option to close --->
						if(parseInt(inpParentIdType) == 1)
						{
							inpObj.addClass('active')
							$SubTopicSection.slideDown('fast');
						}
--->


						if(parseInt(d.QUERYRES.ROWCOUNT) == 0 )
						{
							// MLPAddTopicToDOM( $SubTopicSection, 0, 'No Topics Found', inpParentId, inpParentIdType, 0, true);

							 MLPAddTopicToDOM( $SubTopicSection, 0, '', inpParentId, inpParentIdType, 0, true);
							// BindSortableTopics($SubTopicSection);
						}
						else
						for(var i=0; i<d.QUERYRES.ROWCOUNT; i++)
						{
							MLPAddTopicToDOM( $SubTopicSection, d.QUERYRES.DATA.PKId_bi[i], d.QUERYRES.DATA.Desc_vch[i], inpParentId, inpParentIdType, d.QUERYRES.DATA.MLPId_int[i], false);
						};


						if(parseInt(inpParentIdType) == 1)
						{
							<!--- Create an Add Topic Button at the end of each section--->
							var TopicAddButton = $("<div><a class='trigger-MLP-TOC-Add-Topic' data-toggle='modal' href='#' data-target='#MLP-Add-Topic-Modal'>--Add Topic--</a></div>");

							<!--- Bind attr to modal --->
							TopicAddButton.find('.trigger-MLP-TOC-Add-Topic').on("click touchstart", function(e){

								$("#MLP-Add-Topic-Modal").attr('data-attr-topic-id', 0);
								$("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent', inpParentId);
								$("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent-type', inpParentIdType);
								<!--- Store jquery reference in data instead of attr --->
								$("#MLP-Add-Topic-Modal").data('data-attr-topic-section', $SubTopicSection);


							});

							<!--- Add a button or other object at the bottom of the subsection so we can add more if wanted --->
							$SubTopicSection.append(TopicAddButton);
						}


					}
					else
					{
						<!--- No result returned --->
					}

				}

			});

	}

	function MLPAddTopicToDOM(inpObj, inpTopicId, inpTopicDesc, inpParentId, inpParentIdType, inpMLPId, inpPlaceHolderFlag)
	{
		<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
	  	<!--- Use the Physical - non-changing ID for each reference --->

		<!--- Append a new top level topic panel --->
		var NewTopic = $("<div class='MLP-Top-Topic context-menu-one sortable-topic'><a class='MLP-Top-Topic-Link' data-attr-mlp-id='" + inpMLPId + "' data-attr-topic-id='" + inpTopicId + "' data-attr-topic-parent='" + inpParentId + "' data-attr-topic-parent-type='" + inpParentIdType + "'><h5 class='TopicDesc'>" + inpTopicDesc + "</h5> </a> <div class='MLPSubTopicsSection sortable-topic-container'> </div> </div>");

		// draggable-topic sortable-topic

		<!--- Dont bind event to place holders --->
		if(!inpPlaceHolderFlag)
		{
			<!--- Bind click event --->
			NewTopic.find('.MLP-Top-Topic-Link').on("click", function(e){

				<!--- Load data into MLP-Topic-Editor --->

				<!--- Only load $(this) one time for better efficiency in recursive operations --->
				var $this = $(this);
				var $parent = $this.parent();
				var $SubTopicSection = $parent.children('.MLPSubTopicsSection');

<!---
				if($parent.hasClass('active'))
				{
					$parent.removeClass('active')
					$SubTopicSection.slideUp('fast');
				}
				else
				{
					if(!inpPlaceHolderFlag)
					{
						<!--- All Sub-Topics have parent type 2 - Topic is parent --->
						MLPLoadTopics($parent, $this.attr('data-attr-topic-id'), 2 );
					}

					$parent.addClass('active');
					$SubTopicSection.slideDown('fast');
				}
--->

				<!--- Setup Edit link --->
				$('#MLP-Topic-Editor-Edit-MLP').attr('data-attr-mlp-id', inpMLPId);

			});

		}
		else
		{
			<!--- No need for a conext menu here - may add this back in later if I move the add option into the context --->
			NewTopic.removeClass('context-menu-one');

		}

		<!--- Make each sub topic sortable --->
		BindSortableTopics(NewTopic.children('.MLPSubTopicsSection'));

		var $ref = null;

		<!--- Check i fthis topic already has an add option - if so insert before it --->
		if(inpObj.find('a.trigger-MLP-TOC-Add-Topic:first').length > 0)
			$ref = inpObj.find('a.trigger-MLP-TOC-Add-Topic:first').before(NewTopic);
		else
			$ref = inpObj.append(NewTopic);

		var AutoExpandTopics = true;

		if(AutoExpandTopics)
		{
			<!--- Auto Load --->
			MLPLoadTopics(NewTopic, inpTopicId, 2 )

			NewTopic.addClass('active')
			NewTopic.children('.MLPSubTopicsSection').slideDown('fast');
		}


	}

	<!--- Setup each topic to be sortable via drag and drop --->
	function BindSortableTopics(inpObj)
	{

		inpObj.sortable({
		//	containment: ".TopLevelParent",
			placeholder: "ui-state-highlight",
			connectWith: ".MLPSubTopicsSection, .TopLevelParent ",  //.MLP-Top-Topic, .MLPSubTopicsSection
		//    connectWith: ".MLP-Top-Topic",
		    opacity: 0.8,
		    cursor: "move",
		    // cursorAt: { top: 0, left: 75 },
		    helper: "clone",
		    items: '.sortable-topic',
		    // handle: ".mlp-icon-drag",
		    // tolerance: 'pointer',
		    dropOnEmpty: true,
		    start: function(e, ui ){


			    ui.item.startPos = ui.item.index();

			   	ui.item.startParent = ui.item.parent().parent().children('.MLP-Top-Topic-Link').attr('data-attr-topic-id');

			    if(typeof ui.item.startParent == 'undefined')
			    {
			    	ui.item.startParent = '<cfoutput>#tocid#</cfoutput>'
			    	ui.item.startParentType = '1';
			    }
			    else
			    {
					ui.item.startParentType = '2';
			    }


			    $('.TopLevelParent').find('.MLP-Top-Topic-Link h5').addClass('MLPSubTopicsSectionDragOver');
			    ui.helper.find('.MLP-Top-Topic-Link h5').removeClass('MLPSubTopicsSectionDragOver');


			//	$('.top-sortable-topic-container').sortable("refresh");
			//	$('.top-sortable-topic-container').sortable("refreshPositions");

				//  $(this).sortable( "refreshPositions" );

			},
		    change: function( event, ui ) {

			},
			stop: function( event, ui ) {
			   $('.TopLevelParent').find('.MLP-Top-Topic-Link h5').removeClass('MLPSubTopicsSectionDragOver');

			},
			sort: function(e,ui){
                /* The trick is that jQuery hides the inline element while user drags an absolute positioned clone so we want to deal only with the visible elements. Index order + 1 = UI order.*/
                // $(ui.placeholder).html(Number(ui.parent(). $("#categories > .connectedSortable").index(ui.placeholder)+1));


            },
            update: function( event, ui ) {

	            console.log(ui.item);
	            console.log(ui.item.startParent);

	            var DroppedParentId = ui.item.parent().parent().children('.MLP-Top-Topic-Link').attr('data-attr-topic-id');
	            var DroppedParentType = '2';

	            if(typeof DroppedParentId == 'undefined')
				{
			    	DroppedParentId = '<cfoutput>#tocid#</cfoutput>'
			    	DroppedParentType = '1';
			    }
			    else
			    {
					DroppedParentType = '2';
			    }

		        <!--- Topic dropped --->
	            console.log('MLP-Top-Topic dropped');

	            console.log(ui.item.children('.MLP-Top-Topic-Link').attr('data-attr-topic-id') );
	            console.log(ui.item.startParent);
	            console.log(ui.item.startParentType);
	            console.log("0 based Start Pos=" + ui.item.startPos)

	            console.log('on Target of');

	            console.log("DroppedParentId=" + DroppedParentId);
	            console.log("DroppedParentType=" + DroppedParentType);
	            console.log("0 based Drop Pos=" + ui.item.index())


	            var inpTopicId = ui.item.children('.MLP-Top-Topic-Link').attr('data-attr-topic-id');
	            var inpParentId = ui.item.startParent;
	            var inpParentIdType = ui.item.startParentType;
	            var inpTargetParentId = DroppedParentId;
	            var inpTargetParentIdType = DroppedParentType;
	            var inpTargetPos = ui.item.index();

				MLPMoveTopic(inpTopicId, inpParentId, inpParentIdType, inpTargetParentId, inpTargetParentIdType, inpTargetPos);

            }

		});

	}

	function MLPAddTopic()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=AddTopic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpDesc : $('#New-Topic-Name').val(),
				inpParentId : $("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent'),
				inpParentIdType : $("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent-type'),
				inpMLPId : 0

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					if(parseInt(d.NEWMLPTOPICID) > 0)
					{
						<!--- Add to the DOM - use data from results, as well as #MLP-Add-Topic-Modal --->
						MLPAddTopicToDOM( $("#MLP-Add-Topic-Modal").data('data-attr-topic-section'), d.NEWMLPTOPICID, $('#New-Topic-Name').val(), $("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent'), $("#MLP-Add-Topic-Modal").attr('data-attr-topic-parent-type'), 0, false);
					}

				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});

	}

	function MLPLinkNewMLPToTopic(inpTopicId, inpMLPTemplateId, inpOpen)
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=CreateMLPAndLinkToTopic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpTopicId : inpTopicId,
				inpMLPTemplateId : inpMLPTemplateId
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{

					<!--- Update current --->
					$(".MLP-Top-Topic-Link[data-attr-topic-id='" + inpTopicId + "']").attr("data-attr-mlp-id", d.NEWMLPID);

					if(inpOpen)
					{
						if(parseInt(d.NEWMLPID) > 0)
						{
							<!--- Edit MLP --->
							var url = "mlpx-edit?tocid=<cfoutput>#tocid#</cfoutput>&ccpxDataId=" + d.NEWMLPID ;
							window.location = url;
						}
					}

				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});
	}

	function MLPSaveTopicDesc(inpTopicId, inpDesc)
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=UpdateTopicDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpTopicId : inpTopicId,
				inpDesc : inpDesc
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});


	}


	function MLPDeleteTopic(inpTopicId)
	{

		<!--- Are you sure? --->


		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=DeleteMLPTopic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpTopicId : inpTopicId
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					<!--- Remove the topic from the DOM --->
					$(".MLP-Top-Topic-Link[data-attr-topic-id='" + inpTopicId + "']").remove();

				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});


	}

	function MLPMoveTopic(inpTopicId, inpParentId, inpParentIdType, inpTargetParentId, inpTargetParentIdType, inpTargetPos)
	{

		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/mlp-toc.cfc?method=MoveMLPTopic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpTopicId : inpTopicId,
				inpParentId : inpParentId,
				inpParentIdType : inpParentIdType,
				inpTargetParentId : inpTargetParentId,
				inpTargetParentIdType : inpTargetParentIdType,
				inpTargetPos : inpTargetPos
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});


	}



</script>
