(function($) {    
    $("#white_glove_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});
    // var listKeyword = [];
    // var listKeywordName = [];
    // var listMLP= [];
    // var listMLPName= [];
    // var listShortUrl = [];
    // var listShortUrlName = [];
    var totalStepDowngrade = [];
    var downgradeNote = {};

    var totalStepUpgrade = [];
    var upgradeNote = {};
    var action = 'upgrade';
    var maxActiveKeyword = 0;
    var maxActiveShortUrl = 0;
    var maxActiveMLP = 0;

    var upgradePlan = 0;
    var buy_credit_keyword = $("#buy-credit-keyword");
    var newPlanName = '';
    // get value parameter from Url
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }
    var token = getURLParameter('token');
    var payerid = getURLParameter('PayerID');
    var numberSMSToSend = getURLParameter('numberSMSToSend');
    var numberKeywordToSend = getURLParameter('numberKeywordToSend');
    var action = getURLParameter('action');
    var paymentResult = getURLParameter('result');
    var paymentMSG = getURLParameter('msg');

    $(document).ready(function($) {
        couponValid = 0;
        if(action=='finish_payment'){
            var buy_addon_title = '';
            if( parseInt(paymentResult) == 1){
                alertBox(paymentMSG, buy_addon_title,function(){
                    location.href="/session/sire/pages/my-plan";
                });
            }
            else{
                alertBox(paymentMSG,buy_addon_title,function(){
                    location.href="/session/sire/pages/my-plan";
                });
            }
        }
        if(action=='update_billing'){
            var title= "Update Payment Method";
            alertBox(paymentMSG, title, function () {
                location.href="/session/sire/pages/billing";    
            });
        }
    });

    buy_credit_keyword.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing' && checkTimeout == 0)
         return 'Payment processing. Please don\'t refresh and close.';
    });

    $("#sms_calculate").click(function(e) {
    	var intRegex = /^\d+$/;
    	var amountSms = 0;
    	var numberSMS = $('#numberSMS').val();
    	var pricemsgafter = $('#pricemsgafter').val()
    	if(intRegex.test(numberSMS)) {
    		amountSms = numberSMS * pricemsgafter/100;
    	}
    	$('#amount-sms').text("$" + amountSms.toFixed(3));
    });
    
    $("#numberSMS").change(function(e){
    	var intRegex = /^\d+$/;
    	var amountSms = 0;
    	var numberSMS = $('#numberSMS').val();
    	var pricemsgafter = $('#pricemsgafter').val()
    	if(intRegex.test(numberSMS)) {
    		amountSms = numberSMS * pricemsgafter/100;
    	}

        var amountKeyword = 0;
        var numberKeyword = $('#numberKeyword').val();
        var pricekeywordafter = $('#pricekeywordafter').val()
        if(intRegex.test(numberKeyword)) {
            amountKeyword = numberKeyword * pricekeywordafter;
        }

        $('#amount').val((parseFloat(amountKeyword)+parseFloat(amountSms)).toFixed(2));
        $('#numberSMSToSend').val(numberSMS);
    });

    $("#keyword_calculate").click(function(e) {
        var intRegex = /^\d+$/;
        var amountKeyword = 0;
        var numberKeyword = $('#numberKeyword').val();
        var pricekeywordafter = $('#pricekeywordafter').val();
        if(intRegex.test(numberKeyword)) {
            amountKeyword = numberKeyword * pricekeywordafter;
        }
        $('#amount-keyword').text("$" + parseFloat(amountKeyword).toFixed(2));
    });
    
    $("#numberKeyword").change(function(e){
        var intRegex = /^\d+$/;
        var amountKeyword = 0;
        var numberKeyword = $('#numberKeyword').val();
        var pricekeywordafter = $('#pricekeywordafter').val()
        if(intRegex.test(numberKeyword)) {
            amountKeyword = numberKeyword * pricekeywordafter;
        }

        var amountSms = 0;
        var numberSMS = $('#numberSMS').val();
        var pricemsgafter = $('#pricemsgafter').val()
        if(intRegex.test(numberSMS)) {
            amountSms = numberSMS * pricemsgafter/100;
        }
        $('#amount').val((parseFloat(amountSms)+parseFloat(amountKeyword)).toFixed(2));
        $('#numberKeywordToSend').val(numberKeyword);
    });

    $('#show-buyaddon-modal').on('click', function(e){
        
        if (parseFloat($('#amount').val()) < 5 ){
            alertBox("Your purchase must be larger than $5.", buy_addon_title, function(){
               return false;
            });
        }else{
             //if($('select[name=payment_method]').val() == 0){// Paypal
            upgradePlan = 0;
            e.preventDefault();
            if($('#numberSMS').val() == '' && $('#numberKeyword').val() == ''){
                alertBox('Please input number of credit or keyword, at least one field is required!', 'Buy Credits - Keywords');
            }
            else if(($('#numberSMS').val() != '' || $('#numberKeyword').val() != '') && buy_credit_keyword.validationEngine('validate')){
                $('#section-2.inner-update-cardholder').hide();
                UIkit.modal('#buyaddon-modal-sections')[0].toggle();
            }
        }
       
    });
    // A function below using for button Sign Me Up of tab White Glove
    $('#show-white-glove-modal').on('click', function(e){         
        e.preventDefault();                
        if ($('#white_glove_form').validationEngine('validate', {scroll: false, focusFirstField : true})) { 
            try{
                $.ajax({
                    method: 'POST',
                    url: '/session/sire/models/cfc/order_plan.cfc?method=UpdateUserWhiteGlove&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                    data: {'inpGloveActive': 1},
                    dataType: 'json',                        
                        success: function(data) {                        
                        $("#processingPayment").hide();

                        if(data.RXRESULTCODE == 1){                        
                            alertBox("Our greatest joy comes from helping our clients grow.  A campaign consultant will contact you shortly to discuss the next steps.  We're looking forward to partnering with you and making big things happen!", "Thanks for joining the Premium White Glove service.");
                            //$('#ckagree-white-glove').prop("disabled", true);                            
                            $(".white-glove-agree").hide();
                            $('#show-white-glove-modal').hide();
                            $(".whiteglove_statusText").text('Active');
                        }
                    }
               });

            }catch(ex){                
                $('#processingPayment').hide();
                alertBox('Active White Glove failed please try again later.', "White Glove");
            }        
        }
    });

    // Buy credit request
    var buy_addon = $("#addon-form");

    var buy_addon_title = 'Buy Credits - Keywords';
    
    buy_addon.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

    //POP UP DISPLAY AMOUNT
    buy_addon.submit(function(event){
        event.preventDefault();
        var paymentdata = [];
        if (buy_addon.validationEngine('validate') && buy_credit_keyword.validationEngine('validate')) {
            var self = $("#addon-form");
            var result = { };
            $.each($('#addon-form').serializeArray(), function() {
                result[this.name] = this.value;
            });
            var saveccinfo =  result.save_cc_info || 0;

            var numberSMSToSend = result.numberSMSToSend;
            var numberKeywordToSend = result.numberKeywordToSend;
            var select_used_card = result.select_used_card || 2;
            var h_email = result.h_email;
            var h_email_save = result.h_email_save;
            var payment_method = result.payment_method;
            var number = result.number;
            var cvv = result.cvv;
            var expirationDate = result.expirationDate;
            var firstName = result.firstName;
            var lastName = result.lastName;
            var city = result.city;
            var line1 = result.line1;
            var state = result.state;
            var zip = result.zip;
            var country = result.country;
            var phone = result.phone;
            var email = result.email;
            var expiration_date_month_upgrade = result.expiration_date_month_upgrade;
            var expiration_date_year_upgrade = result.expiration_date_year_upgrade;

            var formData ={
                'numberSMSToSend': numberSMSToSend,
                'numberKeywordToSend': numberKeywordToSend,
                'select_used_card': select_used_card,
                'h_email': h_email,
                'h_email_save': h_email_save,
                'payment_method': payment_method,
                'inpNumber': number,
                'inpCvv2': cvv,
                'expirationDate': expirationDate,
                'inpFirstName': firstName,
                'inpLastName': lastName,
                'inpCity': city,
                'inpLine1': line1,
                'inpState': state,
                'inpPostalCode': zip,
                'inpCountryCode': country,
                'phone': phone,
                'inpPhone':phone,
                'inpEmail': email,
                'paymentGateway': $('#payment_method_value').val(),
                'inpExpireMonth': expiration_date_month_upgrade, 
                'inpExpreYear' : expiration_date_year_upgrade
            };

            var form99 = {
                'numberSMSToSend': numberSMSToSend,
                'numberKeywordToSend': numberKeywordToSend,
                'amount':$('#amount').val()
            }

            var jsondata99 = JSON.stringify(form99);

            // cardObject = {
            //     inpExpireMonth: $('#expiration_date_month').val(),
            //     inpExpreYear: $('#expiration_date_year').val(),
            // }
            // var formData = $("#addon-form").serializeArray(); // Create array of object
            var jsondata = JSON.stringify(formData);
            var suspociousTransaction= false;
            var _cardObject= jsondata;
            //
            var _paymentGateway= $('#payment_method_value').val();    
            console.log (_paymentGateway);                    
            
            var _moduleName= "Upgrade Plan";                        
            var _amount= 0;
            var _numberKW=0;
            var _numberCredit=0;
            var _buyPlanId=0;
            var _checkData;
            _cardObject= JSON.parse(_cardObject);
            _cardObject.inpPSHA1= SHA1(_cardObject.inpNumber).toString();
            
            if(upgradePlan == 0){
                _moduleName= "Buy Credits and Keywords";    
                _amount=$('#amount').val();
                _numberCredit=numberSMSToSend;
                _numberKW=numberKeywordToSend;
                
            }
            else{
                _buyPlanId= $('#planId').val();
                var planId =  $('#planId').val();
                var inpPromotionCode =  $('#inpPromotionCode').val();
                var planType = $('#monthYearSwitch').val();
                $.ajax({
                    type: "POST",
                    url: '/session/sire/models/cfc/order_plan.cfc?method=getPurchaseAmout&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                    dataType: 'json',
                    async:false,
                    data: {
                        'inpPlanId':planId, 
                        'inpPromotionCode': inpPromotionCode,
                        'inpPlanType':planType,
                        'inpLastPlanId':$("#currentPlan").val()
                    },
                    beforeSend: function( xhr ) {
                        $('#processingPayment').hide();
                    },                    
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#processingPayment').hide();                       
                    },                    
                    success:        
                    function(d) {
                        $('#processingPayment').hide();

                        if(d.RXRESULTCODE == 1){  
                            _amount=d.TOTALAMOUNT;
                        }
                    }
                });      
                
                _cardObject.inpPromotionCode= $("#inpPromotionCode").val() || '';
                _cardObject.monthYearSwitch= $("#monthYearSwitch").val() || '';
                _cardObject.planId= $("#planId").val() || '';
                               
            }
            
            _cardObject.saveccinfo= saveccinfo || 0;
            _cardObject=JSON.stringify(_cardObject);
            
            
            // sire fraud           
                          
            $.ajax({
                type:"POST",
                url:'/session/sire/models/cfc/payment/payment.cfc?method=SireFraudCheck&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType:'json',                              
                data:{				                                      
                    inpCardObject: _cardObject,
                    inpPaymentGateway: _paymentGateway  ,
                    inpPaymentType:  select_used_card              
                }
                ,beforeSend:function(xhr){
                    $('#processingPayment').show();
                }
                ,error:function(XMLHttpRequest,textStatus,errorThrown){					                        
                    alertBox("Unable to get your infomation at this time. An error occurred.","Oops!");
                    $('#processingPayment').hide();                    
                }
                ,success:function(data){
                    $('#processingPayment').hide();                        
                    if(parseInt(data.RXRESULTCODE)==1)
                    {
                         // BUY KEYWORD & CC
                        if(upgradePlan == 0){
                            var amount = $('#amount').val();
                            makepayment_info = '';
                            var saveccinfo = result.save_cc_info;

                            $.ajax({
                                url: "/session/sire/models/cfc/billing.cfc?method=GetUserInforLimitPurchaseAmount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                                type: 'POST',
                                dataType: 'json',
                                data: {},
                                success: function(data) {
                                    if(data.RXRESULTCODE ==1){
                                        var limited_purchase_amount = data.AMOUNTLIMITED;
                                        var userId_vch = data.USERID;
                                        var phonenumber_vch = data.PHONENUMBER;
                                        var companyname_vch = data.COMPANYNAME;
                                        var username_vch = data.USERNAME;
                                        GetTransactionExceededLimit($('#payment_method_value').val()).then(function(data){
                                            // if limited_purchase_amount = 0 : unlimited purchase amount.
                                            if(parseFloat(amount) > parseFloat(data.TRANSACTIONEXCEEDEDLIMIT) && parseFloat(data.TRANSACTIONEXCEEDEDLIMIT) > 0){
                                                bootbox.dialog({
                                                    message: '<p>You have exceeded your purchase limit [$'+data.TRANSACTIONEXCEEDEDLIMIT+'], our support will review and get back to you soon.</p>',
                                                    title: '<h3 style= "padding-left: 40px;"><b>Transaction Exceeded Limit</b></h3>',
                                                    className: "be-modal",
                                                    buttons: {
                                                        success: {
                                                            label: "Continue",
                                                            className: "btn btn-medium green-gd",
                                                            callback: function(result) {
                                                                $("#processingPayment").show();
                                                                var datalogs= {
                                                                    inpPaymentMethod: $('#payment_method_value').val(),
                                                                    inpValue: amount,
                                                                    inpTransaction: "Buy Credits and Keywords",
                                                                    inpItem: "Buy "+ numberKeywordToSend + " Keywords, "+numberSMSToSend+ " Credits",
                                                                    
                                                                }
                                                                UpdateTransactionExceededLimitLogs(datalogs).then(function(data){
                                                                    alertBox('Your transaction is now pending for review, please contact Sire support team if you need this sooner.<br><br><a href="https://www.siremobile.com/contact-us">https://www.siremobile.com/contact-us</a><br><br>(888) 747-4411 between 9am and 5pm, PST', "Transaction Exceeded Limit",function(){
                                                                        location.href = '/session/sire/pages/my-plan';
                                                                    });
                                                                }).catch(function(data){
                                                                    alertBox(data.MESSAGE,"Opps!");
                                                                })
                                                            }
                                                        },
                                                        cancel: {
                                                            label: "Cancel",
                                                            className: "green-cancel"
                                                        },
                                                    }
                                                });
                                            }
                                            else if(parseFloat(amount) > parseFloat(limited_purchase_amount) && parseFloat(limited_purchase_amount) > 0){
                                                UIkit.modal('#buyaddon-modal-sections')[0].hide();

                                                if(select_used_card == 2 || !select_used_card ){
                                                    //makepayment_info = '<br> New CC info will be saved.';
                                                    if(!saveccinfo)
                                                    {
                                                        alertBox('You need save your card before continue',"User Limit Payment");
                                                        return;
                                                    }
                                                    $("#processingPayment").show();
                                                    $.ajax({
                                                        method: 'POST',
                                                        url: '/session/sire/models/cfc/payment/vault.cfc?method=storeCreditCardInVault&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
                                                        data: {inpCardObject:jsondata, inpPaymentGateway:$('#payment_method_value').val()},
                                                        dataType: 'json',
                                                        error: function(jqXHR, textStatus, errorThrown) {
                                                            $("#processingPayment").hide();
                                                        },
                                                        success: function(data) {
                                                            $("#processingPayment").hide();
                                                            if(data.RXRESULTCODE == 1){
                                                                bootbox.dialog({
                                                                    message: '<p>You have exceeded your purchase limit, our support will review and get back to you soon.</p>',
                                                                    title: '<h3 style= "padding-left: 40px;"><b>'+buy_addon_title+'</b></h3>',
                                                                    className: "be-modal",
                                                                    buttons: {
                                                                        success: {
                                                                            label: "Continue",
                                                                            className: "btn btn-medium green-gd",
                                                                            callback: function(result) {
                                                                                $("#processingPayment").show();
                                                                                AddPurchaseAmountLimited(userId_vch,amount,phonenumber_vch,companyname_vch,username_vch,jsondata99,$('#payment_method_value').val(),buy_addon_title);
                                                                            }
                                                                        },
                                                                        cancel: {
                                                                            label: "Cancel",
                                                                            className: "green-cancel"
                                                                        },
                                                                    }
                                                                });
                                                            }else{
                                                                $("#processingPayment").hide();
                                                                alertBox('The transaction cannot be processed due to invalid credit card info.', buy_addon_title);
                                                            }
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    
                                                    $.ajax({
                                                        url: '/session/sire/models/cfc/payment/vault.cfc?method=getDetailCreditCardInVault&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
                                                        async: true,
                                                        type: 'post',
                                                        dataType: 'json',
                                                        data: {
                                                            userId:userId_vch,
                                                            inpPaymentGateway:$('#payment_method_value').val()
                                                        },
                                                        beforeSend: function(){
                                                            $("#processingPayment").show();
                                                        },
                                                        error: function(jqXHR, textStatus, errorThrown) {
                                                            $("#processingPayment").hide();
                                                        },
                                                        success: function(data) {
                                                            $("#processingPayment").hide(); 
                                                            if (data.RXRESULTCODE == 1) {
                                                                bootbox.dialog({
                                                                    message: '<p>You have exceeded your purchase limit, our support will review and get back to you soon.</p>',
                                                                    title: '<h3 style= "padding-left: 40px;"><b>'+buy_addon_title+'</b></h3>',
                                                                    className: "be-modal",
                                                                    buttons: {
                                                                        success: {
                                                                            label: "Continue",
                                                                            className: "btn btn-medium green-gd",
                                                                            callback: function(result) {
                                                                                $("#processingPayment").show();
                                                                                // Purchase no need Admin verify
                                                                                AddPurchaseAmountLimited(userId_vch,amount,phonenumber_vch,companyname_vch,username_vch,jsondata99,$('#payment_method_value').val(),buy_addon_title);
                                                                            }
                                                                        },
                                                                        cancel: {
                                                                            label: "Cancel",
                                                                            className: "green-cancel"
                                                                        },
                                                                    }
                                                                });
                                                            } else {
                                                                $("#processingPayment").hide();
                                                                alertBox(data.MESSAGE,'Purchase Payment Transaction','');
                                                            }                   
                                                        }
                                                    });
                                                }                                                                                              
                                            }
                                            else // payment under 99
                                            {
                                                var jsondatatemp = jsondata;
                                                confirmBox('Amount to be paid: <b>$'+ amount +'</b><br> Are you sure you would like to Make Payment?', buy_addon_title, function(){
                                                // Purchase no need Admin verify
                                                    $("#actionStatus").val('Processing');
                                                    $("#processingPayment").show();
                                                    DoPurchasedCreditKeywordWithSaveCC(jsondatatemp,$('#payment_method_value').val(),buy_addon_title);                                                                    
                                                });
                                            }

                                        }).catch(function(){

                                        });                            
                                    }
                                }
                            });
                        }
                        // UPDATE PLAN
                        else{
                            var planId =  $('#planId').val();
                            var inpPromotionCode =  $('#inpPromotionCode').val();
                            var planType = $('#monthYearSwitch').val();
                            // var planType = '';
                            upgradePlan = 1;

                            if (inpPromotionCode != "") {
                                if (couponValid == 0) {
                                    alertBox('You must apply your coupon code first!', 'Upgrade Plan', function(){});
                                    return false;
                                } 
                                else if (couponValid == -1) {
                                    alertBox('Coupon code is not valid', 'Upgrade Plan', function(){});
                                    return false;
                                }
                            }

                            try{

                                $.ajax({
                                    type: "POST",
                                    url: '/session/sire/models/cfc/order_plan.cfc?method=getPurchaseAmout&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                                    dataType: 'json',
                                    data: {
                                        'inpPlanId':planId, 
                                        'inpPromotionCode': inpPromotionCode,
                                        'inpPlanType':planType,
                                        'inpLastPlanId':$("#currentPlan").val()
                                    },
                                    beforeSend: function( xhr ) {
                                        $('#processingPayment').hide();
                                    },                    
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        $('#processingPayment').hide();
                                        bootbox.dialog({
                                            message:"Can not upgrade",
                                            title: "",
                                            buttons: {
                                                success: {
                                                    label: "Ok",
                                                    className: "btn btn-medium btn-success-custom",
                                                    callback: function() {}
                                                }
                                            }
                                        });
                                    },                    
                                    success:        
                                    function(d) {
                                        $('#processingPayment').hide();

                                        if(d.RXRESULTCODE == 1){
                                            $('#upgradePlanAmount').val(d.TOTALAMOUNT);

                                            var buy_addon_title = 'Upgrade Plan';
                                            var planAmount = d.TOTALAMOUNT;
                                            var planId = $('#planId').val();
                                            var confirmMsg='Amount to be paid: <b>$'+ planAmount +'</b> <br> Are you sure you would like to Make Payment?';
                                            if(d.INCLUDELASTPOSTPAIDPLANAMOUNT > 0)
                                            {
                                                confirmMsg='Amount to be paid: <b>$'+ planAmount +'</b>  (Included $'+d.INCLUDELASTPOSTPAIDPLANAMOUNT+' for last Postpaid Plan)<br> Are you sure you would like to Make Payment?';
                                            }
                                            //
                                            GetTransactionExceededLimit($('#payment_method_value').val()).then(function(data){
                                                // if limited_purchase_amount = 0 : unlimited purchase amount.
                                                if(parseFloat(planAmount) > parseFloat(data.TRANSACTIONEXCEEDEDLIMIT) && parseFloat(data.TRANSACTIONEXCEEDEDLIMIT) > 0){
                                                    bootbox.dialog({
                                                        message: '<p>You have exceeded your purchase limit [$'+data.TRANSACTIONEXCEEDEDLIMIT+'], our support will review and get back to you soon.</p>',
                                                        title: '<h3 style= "padding-left: 40px;"><b>Transaction Exceeded Limit</b></h3>',
                                                        className: "be-modal",
                                                        buttons: {
                                                            success: {
                                                                label: "Continue",
                                                                className: "btn btn-medium green-gd",
                                                                callback: function(result) {
                                                                    $("#processingPayment").show();
                                                                    var datalogs= {
                                                                        inpPaymentMethod: $('#payment_method_value').val(),
                                                                        inpValue: planAmount,
                                                                        inpTransaction: "By Plan",
                                                                        inpItem: "Buy Plan "+ $("#select-plan-2 option:selected").text()
                                                                        
                                                                    }
                                                                    UpdateTransactionExceededLimitLogs(datalogs).then(function(data){
                                                                        alertBox('Your transaction is now pending for review, please contact Sire support team if you need this sooner.<br><br><a href="https://www.siremobile.com/contact-us">https://www.siremobile.com/contact-us</a><br><br>(888) 747-4411 between 9am and 5pm, PST', "Transaction Exceeded Limit",function(){
                                                                            location.href = '/session/sire/pages/my-plan';
                                                                        });
                                                                    }).catch(function(data){
                                                                        alertBox(data.MESSAGE,"Opps!");
                                                                    })
                                                                }
                                                            },
                                                            cancel: {
                                                                label: "Cancel",
                                                                className: "green-cancel"
                                                            },
                                                        }
                                                    });
                                                }
                                                else {
                                                    confirmBox(confirmMsg, buy_addon_title, function(){
                                                        $("#actionStatus").val('Processing');
                                                        $("#processingPayment").show();
                                                        try{
                                                            $(window).unbind('beforeunload');
                                                            var formdata = self;
                                                            upgradePlanProcess(formdata,planAmount);    
                                                        }catch(ex){
                                                            $('#processingPayment').hide();
                                                            alertBox(ex, buy_addon_title);
                                                        }
                                                    });
                                                }                                    
                                            }).catch(function(){
                
                                            });     
                                        }
                                    }       
                                });
                            }catch(ex){
                                $('#processingPayment').hide();
                                bootbox.dialog({
                                    message:"Can not upgrade",
                                    title: "",
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn btn-medium btn-success-custom",
                                            callback: function() {}
                                        }
                                    }
                                });
                            }
                        }
                    }
                    else if(parseInt(data.RXRESULTCODE) == -2)
                    {                        
                        alertBox(data.MESSAGE,'We Couldn’t Process Your Payment');
                        return false;
                    }    
                    else if(parseInt(data.RXRESULTCODE) == -1)
                    {                        
                        alertBox("There is an error with fraud check",'We Couldn’t Process Your Payment');
                        return false;
                    }
                                        
                    else if(data.RXRESULTCODE ==0 ){                                              
                        _checkData=  JSON.stringify(data.DATACHECK);    
                        _cardObject=data.RTCARDOBJECT;                                                         
                        LogPaymentNeedReview(_paymentGateway, _cardObject,select_used_card, _moduleName, _amount, _numberKW, _numberCredit, _buyPlanId, _checkData);                            
                    }                        
                }
            });                                                           
        }
    });

    function upgradePlanProcess(formData,planAmount){

        var buy_addon_title = "Upgrade Plan" ; 
        var result = { };
        $.each(formData.serializeArray(), function() {
            result[this.name] = this.value;
        });
        var planId = result.planId;
        var inpPromotionCode = result.inpPromotionCode;
        var monthYearSwitch = result.monthYearSwitch;
        var listKeyword =  result.listKeyword;
        var listMLP =  result.listMLP;
        var listShortUrl =  result.listShortUrl;
        var selectusercard = result.select_used_card;
        var saveccinfo = result.save_cc_info;
        var numberSMSToSend = result.numberSMSToSend;
        var numberKeywordToSend = result.numberKeywordToSend;
        var select_used_card = result.select_used_card;
        var h_email = result.h_email;
        var h_email_save = result.h_email_save;
        var payment_method = result.payment_method;
        var number = result.number;
        var cvv = result.cvv;
        var expirationDate = result.expirationDate;
        var firstName = result.firstName;
        var lastName = result.lastName;
        var city = result.city;
        var line1 = result.line1;
        var state = result.state;
        var zip = result.zip;
        var country = result.country;
        var phone = result.phone;
        var email = result.email;
        var expiration_date_month_upgrade = result.expiration_date_month_upgrade;
        var expiration_date_year_upgrade = result.expiration_date_year_upgrade;

        var plandata ={
            'planId': planId,
            'inpPromotionCode': inpPromotionCode,
            'monthYearSwitch': monthYearSwitch,
            'listKeyword':  listKeyword,
            'listMLP':  listMLP,
            'listShortUrl': listShortUrl,
            'numberSMSToSend': numberSMSToSend,
            'numberKeywordToSend': numberKeywordToSend,
            'select_used_card': select_used_card,
            'h_email': h_email,
            'h_email_save': h_email_save,
            'payment_method': payment_method,
            'inpNumber': number,
            'inpCvv2': cvv,
            'expirationDate': expirationDate,
            'inpFirstName': firstName,
            'inpLastName': lastName,
            'inpCity': city,
            'inpLine1': line1,
            'inpState': state,
            'inpPostalCode': zip,
            'inpCountryCode': country,
            'phone': phone,
            'inpEmail': email,
            'paymentGateway': $('#payment_method_value').val(),
            'inpExpireMonth': expiration_date_month_upgrade, 
            'inpExpreYear' : expiration_date_year_upgrade
        };

        var jsondata = JSON.stringify(plandata);
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=DoPurchasedBuyPlanWithSaveCC&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data: {
                inpOrderAmount: planAmount,
                inpPaymentdata: jsondata,
                inpPaymentGateway: $('#payment_method_value').val(),
                inpSelectUsedCard: selectusercard,
                inpSaveCCInfo: saveccinfo
            },
            dataType: 'json',
            success: function(data) {
                $("#processingPayment").hide();
                if (data && data.RXRESULTCODE == 1) {
                    alertBox(data.MESSAGE, buy_addon_title, function(){
                            location.href = '/session/sire/pages/my-plan';
                    });
                } else {
                    alertBox(data.ERRMESSAGE, buy_addon_title);
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                }
            }
        });
    }


    function DoPurchasedCreditKeywordWithSaveCC(jsondatatemp,paymentGateway,buy_addon_title)
    {
       var result = { };
        $.each($('#addon-form').serializeArray(), function() {
            result[this.name] = this.value;
        });
        var numberSMS = result.numberSMSToSend;
        var numberKeyword = result.numberKeywordToSend;
        var amountpayment = result.amount;
        var user_email = result.h_email;
        var selectusercard = result.select_used_card;
        var saveccinfo = result.save_cc_info;

        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=DoPurchasedCreditKeywordWithSaveCC&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data: {
                inpOrderAmount: amountpayment,
                inpPaymentdata: jsondatatemp,
                inpPaymentGateway: paymentGateway,
                inpSelectUsedCard: selectusercard,
                inpSaveCCInfo: saveccinfo,
            },
            dataType: 'json',
                //timeout: 6000,
                success: function(data) {

                $("#actionStatus").val('Make Payment');
                $("#processingPayment").hide();

                if (data.RXRESULTCODE == 1) {
                    alertBox(data.MESSAGE, buy_addon_title, function(){
                        UIkit.modal('#buyaddon-modal-sections')[0].toggle();
                        location.href = '/session/sire/pages/my-plan';
                    });
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                } else {
                    alertBox(data.MESSAGE, buy_addon_title);
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                }

            }
        });
    }


    function AddPurchaseAmountLimited(userId_vch,amount,phonenumber_vch,companyname_vch,username_vch,jsondata99,paymentGateway,buy_addon_title)
    {
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=AddPurchaseAmountLimited&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data: {
                inpUserId :userId_vch,
                inpPaymentAmount: parseFloat(amount),
                inpPhoneNumber: phonenumber_vch,
                inpCompanyName: companyname_vch,
                inpUserName:username_vch,
                inpPaymentInfor:jsondata99,
                inpPaymentGateway:paymentGateway
            },
            dataType: 'json',
            success: function(data) {
                $("#processingPayment").hide();
                if(data.RXRESULTCODE == 1){
                    alertBox('Your transaction is now pending for review, please contact Sire support team if you need this sooner.<br><br><a href="https://www.siremobile.com/contact-us">https://www.siremobile.com/contact-us</a><br><br>(888) 747-4411 between 9am and 5pm, PST', buy_addon_title,function(){
                        location.href = '/session/sire/pages/my-plan';
                    });
                }else{
                    alertBox('Action failed please try again later.', buy_addon_title);
                }
            }
        });
    }

    // transaction exceeded by payment gateway
    function GetTransactionExceededLimit(paymentMethod){
        return new Promise( function (resolve, reject) {
            $.ajax({
                type:"POST",
                url:'/session/sire/models/cfc/billing.cfc?method=GetTransactionExceededLimit&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType:'json',
                data:{					
                        inpPaymentMethod:paymentMethod
                }
                ,beforeSend:function(xhr){
                    $('#processingPayment').show()
                }
                ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                    var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
                    reject(data);
                    $('#processingPayment').hide()
                }
                ,success:function(data){
                    $('#processingPayment').hide();
                    if(data.RXRESULTCODE==1){						
                        resolve(data);
                    }
                    else
                    {
                        reject(data);
                    }					
                }
            }); 			
        });
    }
    function UpdateTransactionExceededLimitLogs(updateData){
        return new Promise( function (resolve, reject) {
            $.ajax({
                type:"POST",
                url:'/session/sire/models/cfc/billing.cfc?method=UpdateTransactionExceededLimitLogs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType:'json',
                data:{					
                    inpPaymentMethod: updateData.inpPaymentMethod,
                    inpValue: updateData.inpValue,
                    inpTransaction: updateData.inpTransaction,
                    inpItem: updateData.inpItem                    
                }
                ,beforeSend:function(xhr){
                    $('#processingPayment').show()
                }
                ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                    var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
                    reject(data);
                    $('#processingPayment').hide()
                }
                ,success:function(data){
                    $('#processingPayment').hide();
                    if(data.RXRESULTCODE==1){						
                        resolve(data);
                    }
                    else
                    {
                        reject(data);
                    }					
                }
            }); 			
        });
    }

    // suspocious transaction    
    function LogPaymentNeedReview(paymentGateway, cardObject,paymentType, moduleName, amount, numberKW, numberCredit, buyPlanId, checkData){                
        $.ajax({
            type:"POST",
            url:'/session/sire/models/cfc/payment/payment.cfc?method=LogPaymentNeedReview&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',            
            data:{				                                                           
                inpPaymentGateway: paymentGateway,
                inpCardObject: cardObject,
                inpPaymentType:paymentType,
                inpModuleName: moduleName,
                inpAmount: amount,
                inpNumberKeyword: numberKW,
                inpNumberCredit: numberCredit,
                inpBuyPlanId: buyPlanId,
                inpCheckData: checkData
            }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					                                
                $('#processingPayment').hide()
                alertBox("Unable to save your infomation at this time. An error occurred.","Oops!");
            }
            ,success:function(data){
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){
                    alertBox('Thank you for your order.  We haven\'t processed your payment because this <br>transaction is under review. You will receive an email once the validation process<br>is complete.  Our Customer Support team usually completes the review process<br>within one business day.  If you have any questions about this or need a faster<br>response, please contact us at <a href="https://www.siremobile.com/contact-us">support@siremobile.com.</a><br><br>','Transaction Pending - Under Review',function(){                    
                        location.href = '/session/sire/pages/my-plan'
                    });
                }
                else
                {
                    alertBox(data.MESSAGE,"Oops!");
                }
            }
        }); 			        
    }
    // end
    function SHA1(card){
        var hash = "fraudlabspro_".concat(card);
        for(var i = 0 ; i < 65536 ; i++){
            hash = CryptoJS.SHA1("fraudlabspro_".concat(hash));
        }
        return hash ;
   }   

buy_addon.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});


$('#buyaddon-modal-sections .select_used_card').click(function(){

    $('#buyaddon-modal-sections #card_number').val('');
    $('#buyaddon-modal-sections #security_code').val('');
    $('#buyaddon-modal-sections #expiration_date').val('');
        // $('#expiration_date_month').val('');
        // $('#expiration_date_year').val('');
        $('#buyaddon-modal-sections #first_name').val('');
        $('#buyaddon-modal-sections #last_name').val('');

        buy_addon.validationEngine('hideAll');

        // my_plan[0].reset();

        if($(this).val() == 2){
            $('.fset_cardholder_info').show();
            $('.btn-update-profile').prop('disabled', false);
        }
        else{
            $('.fset_cardholder_info').hide();
            $('.btn-update-profile').prop('disabled', true);
        }
    });

$('#buyaddon-modal-sections .select_payment_method').click(function(){

    $('#buyaddon-modal-sections #card_number').val('');
    $('#buyaddon-modal-sections #security_code').val('');
    $('#buyaddon-modal-sections #expiration_date').val('');
    // $('#expiration_date_month').val('');
    // $('#expiration_date_year').val('');
    $('#buyaddon-modal-sections #first_name').val('');
    $('#buyaddon-modal-sections #last_name').val('');

    //buy_addon.validationEngine('hideAll');
    // my_plan[0].reset();
    if($(this).val() == 2){
        // $('#payment_method_value').val(2);
        // $('.inner-update-cardholder').hide();
        // $('.card-support-type').hide();
        // $('.btn-update-profile').prop('disabled', false);
        //  if(upgradePlan == 1){
        //     $('#section-2').show();    
        // }
        $('#payment_method_value').val(2);
        $('.inner-update-cardholder').show();
        $('.card-support-type').show();
        if(upgradePlan == 0){
            $('#section-2').hide();    
        }
        
        $('.btn-update-profile').prop('disabled', true);
    }
    else{
        $('#payment_method_value').val(1);
        $('.inner-update-cardholder').show();
        $('.card-support-type').show();
        if(upgradePlan == 0){
            $('#section-2').hide();    
        }
        
        $('.btn-update-profile').prop('disabled', true);
    }
});

/**************************************** UPGRADE PROCESS ********************************************/

$('.btn-upgrade').on('click', function(e){
    if($(this).is('[disabled=""]')){
        e.preventDefault();
        return false;
    }
    $('#section-2.inner-update-cardholder').show();
    listKeyword = [];
    listKeywordName = [];
    listMLP= [];
    listMLPName= [];
    listShortUrl = [];
    listShortUrlName = [];
    totalStepUpgrade = [];
    totalStepDowngrade = [];
    upgradeNote = {};
    $('#downgrade-list-confirm-modal-sections .list_item').html('');
    $('#downgrade-list-confirm-modal-sections .item_action').html('');
    $('#downgrade-list-confirm-modal-sections .item_type').html('');

    action = 'upgrade';

    e.preventDefault(); 
    var planId = $(this).data('plan-id');
    $('#planId').val(planId);

    try{
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/order_plan.cfc?method=getUserUpgradeInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            data: {'inpPlanId':planId},
            dataType: 'json',
                //timeout: 6000,
                success: function(data) {
                    $("#processingPayment").hide();

                    if(data.RXRESULTCODE == 1){                        
                            if(data.RXRESULTCODE == 1){
                                downgradeNote.KEYWORDNOTE = data.KEYWORDNOTE;
                                downgradeNote.MLPNOTE = data.MLPNOTE;
                                downgradeNote.SHORTURLNOTE = data.SHORTURLNOTE;

                                maxActiveKeyword = data.KEYWORDAVAILABLE;
                                maxActiveMLP = data.MLPAVAIABLE;
                                maxActiveShortUrl = data.SHORTURLAVAIABLE;

                                if(data.TOTALKEYWORDENABLE > 0){
                                    totalStepDowngrade.push('keyword');
                                }   

                                if(data.TOTALSHORTURLENABLE > 0 ){
                                    totalStepDowngrade.push('shorturl');
                                }

                                if(data.TOTALMLPENABLE > 0 ){
                                    totalStepDowngrade.push('mlp');
                                }

                                if(totalStepDowngrade.length > 0)
                                {
                                    displayDowngrade(totalStepDowngrade[0],downgradeNote);   
                                }
                                else{
                                    checkUpgrade();
                                }
                            }
                }
           }
       });

    }catch(ex){
        $('#processingPayment').hide();
        alertBox('Downgrade failed please try again later', buy_addon_title);
    }

        //doUpgrade(planId);
    });


function checkUpgrade(){

    var planId =  $('#planId').val();
    var inpPromotionCode =  $('#inpPromotionCode').val();
    upgradePlan = 1;
    var currentPlan = $('#currentPlan').val();

    $(".span-plan-price").hide();
    $("#span-plan-price-"+planId).show();
    $("#span-plan-price-yearly-"+planId).show();
    
    if(currentPlan==1){
        $('#plan-1').detach();
        $('#plan-'+planId).prop('selected', true);
        
    }
    else if(currentPlan==2){
        $('#plan-1').detach();
        $('#plan-2').detach();
        $('#plan-'+planId).prop('selected', true);
    }
    else if(currentPlan==6){
        $('#plan-1').detach();
        $('#plan-2').detach();
        $('#plan-6').detach();
        $('#plan-'+planId).prop('selected', true);    
    }
    else
    {
        $('#plan-'+planId).prop('selected', true);    
    }

    try{

        $.ajax({
            type: "POST",
            url: '/session/sire/models/cfc/order_plan.cfc?method=getPlanUpgradeInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
            dataType: 'json',
            data: {'inpPlanId':planId, 'inpPromotionCode': inpPromotionCode},
            beforeSend: function( xhr ) {
                $('#processingPayment').hide();
            },                    
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('#processingPayment').hide();
                bootbox.dialog({
                    message:"Can not upgrade",
                    title: "",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                });
            },                    
            success:        
            function(d) {
                $('#processingPayment').hide();
                
                if(d.RXRESULTCODE == 1){
                    $('#upgradePlanAmount').val(d.TOTALAMOUNT);
                    $("#planStatus").val(d.PLANSTATUS);
                    // force select monthly
                    if(d.PLANSTATUS !=1)
                    {                                                
                        $('#month-Year-Switch').attr('checked', false).triggerHandler('click');
                        $(".month-year-plan-switch").hide();
                        $(".coupon-code").hide();
                    }  
                    else
                    {
                        $(".month-year-plan-switch").show(); 
                        $(".coupon-code").show();
                    }                  
                    // end force select monthly
                    upgradePlan = 1;
                    //UIkit.modal('#buyaddon-modal-sections').show();
                    var modal = UIkit.modal("#buyaddon-modal-sections")[0];
                    if ( modal.isActive() ) {
                        //modal.hide();
                    } else {
                        modal.show();
                    }
                    
                    $('#listKeyword').val(listKeyword)
                    $('#listMLP').val(listMLP)
                    $('#listShortUrl').val(listShortUrl);
                }
            }       
        });
    }catch(ex){
        $('#processingPayment').hide();
        bootbox.dialog({
            message:"Can not upgrade",
            title: "",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        });
    }
}



/****************************************************************************************************
**************************************** DOWNGRADE PROCESS *******************************************
****************************************************************************************************
*/

//DOWNGRADE
// UIkit.modal("#my-id").show();
if(showPopupDowngrade == 1){
    setTimeout(function(){  
        $('.btn-downgrade').trigger('click');
    }, 500);        
}
    
$('.btn-downgrade').on('click', function(e){
    if($(this).is('[disabled=""]')){
        e.preventDefault();
        return false;
    }
    // listKeyword = [];
    // listKeywordName = [];
    // listMLP= [];
    // listMLPName= [];
    // listShortUrl = [];
    // listShortUrlName = [];
    totalStepDowngrade = [];
    
    downgradeNote = {};
    $('#downgrade-list-confirm-modal-sections .list_item').html('');
    $('#downgrade-list-confirm-modal-sections .item_action').html('');
    $('#downgrade-list-confirm-modal-sections .item_type').html('');
    //$('#downgrade-success-modal-sections .newPlanName').html(''); 

    action = 'downgrade';

    e.preventDefault();
    //var planId = $(this).data('plan-id');
    //$('#downgradePlanId').val(planId);
    $('#downgradePlanId').val(downgradePlanId);

    newPlanName = downgradePlanName;

    UIkit.modal('#downgrade-modal-sections')[0].toggle();
});



// CHECK USER DOWNGRADE INFO
$('.btn-downgrade-next').on('click', function(e){
    e.preventDefault();
    // STEP : check if user have to remove keyword,mlp,short url
    var planId = $('#downgradePlanId').val();    
    try{
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/order_plan.cfc?method=getUserDowngradeInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            data: {'inpPlanId':planId},
            dataType: 'json',
            //timeout: 6000,
            success: function(data) {
                $("#processingPayment").hide();

                if(data.RXRESULTCODE == 1){
                    downgradeNote.KEYWORDNOTE = data.KEYWORDNOTE;
                    downgradeNote.MLPNOTE = data.MLPNOTE;
                    downgradeNote.SHORTURLNOTE = data.SHORTURLNOTE;
                    //data.KEYWORDAVAILABLE = 0;
                    if(data.KEYWORDAVAILABLE < 0){
                        totalStepDowngrade.push('keyword');
                    }   

                    if(data.SHORTURLAVAIABLE < 0){
                        totalStepDowngrade.push('shorturl');
                    }

                    if(data.MLPAVAIABLE < 0 ){
                        totalStepDowngrade.push('mlp');
                    }

                    if(totalStepDowngrade.length > 0)
                    {
                     displayDowngrade(totalStepDowngrade[0],downgradeNote);   
                 }
                 else{
                    doDowngrade();
                }
            }
        }
    });

    }catch(ex){
        $('#processingPayment').hide();
        alertBox('Downgrade failed please try again later', buy_addon_title);
    }
});
    



    $(document).on('change', '.selectKeyword, .selectMLP', function() {
        var type = $(this).data('type');
        var id = $(this).data('id');
        var name = $(this).data('name');

        if($(this).is(":checked")) {
            if(type=='keyword'){
                if(action == 'downgrade'){
                    listKeyword.push(id);   
                    listKeywordName.push(name);   
                }
                else{

                    if(listKeyword.length < maxActiveKeyword)
                    {
                        listKeyword.push(id);   
                        listKeywordName.push(name);   
                    }
                    else
                    {
                        $(this).prop('checked', false);
                        alertBox('You have select max active keyword', 'Upgrade Plan');
                    }
                }
            }
            else if(type=='shorturl'){
                if(action=='downgrade'){
                    listShortUrl.push(id);   
                    listShortUrlName.push(name);   
                }
                else{
                    if(listShortUrl.length < maxActiveShortUrl || maxActiveShortUrl=='Unlimited')
                    {
                        listShortUrl.push(id);   
                        listShortUrlName.push(name);
                    }
                    else
                    {
                        $(this).prop('checked', false);
                        alertBox('You have select max active shortUrl', 'Upgrade Plan');
                    }    
                }
            }
            else if(type=='mlp'){
                if(action=='downgrade'){
                    listMLP.push(id);   
                    listMLPName.push(name);       
                }
                else{
                    if(listMLP.length < maxActiveMLP || maxActiveMLP=='Unlimited')
                    {
                        listMLP.push(id);   
                        listMLPName.push(name);    
                    }
                    else{
                        $(this).prop('checked', false);
                        alertBox('You have select max active MLP', 'Upgrade Plan');
                    }
                }
            }
        }
        else{

         if(type=='keyword'){
            var i = listKeyword.indexOf(id.toString());
            if(i != -1) {
                listKeyword.splice(i, 1);
            }
            
            var j = listKeywordName.indexOf(name);
            if(j != -1) {
                listKeywordName.splice(j, 1);
            }
        }
        else if(type=='shorturl'){
            var i = listShortUrl.indexOf(id).toString();
            if(i != -1) {
                listShortUrl.splice(i, 1);
            }

            var j = listShortUrlName.indexOf(name);
            if(j != -1) {
                listShortUrlName.splice(j, 1);
            }
        }
        else if(type=='mlp'){
            var i = listMLP.indexOf(id.toString());
            if(i != -1) {
                listMLP.splice(i, 1);
            }

            var j = listMLPName.indexOf(name);
            if(j != -1) {
                listMLPName.splice(j, 1);
            }
        }
    }
});

    // GO TO CONFIRM POPUP
    $('.btn-downgrade-next-1').on('click', function(e){
        e.preventDefault();
        var type = $(this).attr('data-type');

        if(action=='downgrade'){
            $('#downgrade-list-confirm-modal-sections .item_action').html('removed');
        }
        else{
            $('#downgrade-list-confirm-modal-sections .item_action').html('actived');
        }

        $('#downgrade-list-confirm-modal-sections .list_item').html('');

        if(type =='keyword'){
            if(listKeywordName.length > 0)
            {
                var htmlItemList = '';
                for (var i = 0; i < listKeywordName.length; i++) {
                    htmlItemList+= listKeywordName[i]+"<br/>";
                }
                $('#downgrade-list-confirm-modal-sections .list_item').html(htmlItemList);    
               
            }
             $('#downgrade-list-confirm-modal-sections .item_type').html('keywords');
            totalStepDowngrade.shift();
        }

        if(type =='mlp'){
            if(listMLPName.length > 0)
            {
                var htmlItemList = '';
                for (var i = 0; i < listMLPName.length; i++) {
                    htmlItemList+= listMLPName[i]+"<br/>";
                }
                $('#downgrade-list-confirm-modal-sections .list_item').html(htmlItemList)  
               
            }
             $('#downgrade-list-confirm-modal-sections .item_type').html('MLPs');  
            totalStepDowngrade.shift();
        }

        if(type =='shorturl'){
            if(listShortUrlName.length > 0)
            {
                var htmlItemList = '';
                for (var i = 0; i < listShortUrlName.length; i++) {
                    htmlItemList+= listShortUrlName[i]+"<br/>";
                }
                $('#downgrade-list-confirm-modal-sections .list_item').html(htmlItemList)    
               
            }
             $('#downgrade-list-confirm-modal-sections .item_type').html('ShortUrls');
            totalStepDowngrade.shift();
        }

        UIkit.modal('#downgrade-list-confirm-modal-sections')[0].toggle();
    });

    $('.btn-remove-item-downgrade').on('click', function(e){
        e.preventDefault();
        if(totalStepDowngrade.length > 0)
        {
            displayDowngrade(totalStepDowngrade[0],downgradeNote);    
        }
        else{
            if(action =='downgrade'){
                doDowngrade();                
            }
            else{
                checkUpgrade();    
            }

        }

    });

    
    function displayDowngrade(type,data){
        //type ='keyword','url','mlp'
        //UIkit.modal('#downgrade-modal-sections')[0].toggle();
        if(type == 'keyword'){  
            $(".btn-downgrade-next-1").attr('data-type','keyword');                
            GetDownGradeKeywordList(action);
            $('.downgradeNote').html(data.KEYWORDNOTE)
        }
        else if(type == 'shorturl'){
            $(".btn-downgrade-next-1").attr('data-type','shorturl');  
            GetDownGradeShortList(action);
            $('.downgradeNote').html(data.SHORTURLNOTE);
        }
        else if(type == 'mlp'){
            $(".btn-downgrade-next-1").attr('data-type','mlp');
            GetDownGradeMLPList(action);
            $('.downgradeNote').html(data.MLPNOTE);
        }

        UIkit.modal('#downgrade-list-keyword-modal-sections')[0].toggle();    
    }

    function doDowngrade(){

     var planId = $('#downgradePlanId').val();    
     $('#processingPayment').show();
     try{
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/order_plan.cfc?method=doDowngrade&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            data: {'inpPlanId':planId,'inpListKeyword':listKeyword,'inpListShortUrl':listShortUrl,'inpListMLP':listMLP},
            dataType: 'json',
                //timeout: 6000,
                success: function(data) {
                    $("#processingPayment").hide();
                    if(data.RXRESULTCODE == 1)
                    {
                        $('#processingPayment').hide();
                        UIkit.modal('#downgrade-success-modal-sections')[0].toggle();
                        //$('#downgrade-success-modal-sections .newPlanName').html(newPlanName); 
                    }
                    else{
                        UIkit.modal('#downgrade-list-confirm-modal-sections')[0].toggle();
                        alertBox('Downgrade failed please try again later');
                    }
                }
            });

        }catch(ex){
            $('#processingPayment').hide();
            alertBox('Downgrade failed please try again later');
        }
    }

$('#downgrade-success-modal-sections').on({

    'show.uk.modal': function(){
            // console.log("Modal is visible.");
        },

        'hide.uk.modal': function(){
            //location.reload();
        }
    });

function GetDownGradeKeywordList(inpAction){

    var keyword = typeof(keyword)!='undefined'?keyword:"";

    var actionCol = function(object){
        var checkbox = $('<input type="checkbox" class="selectKeyword" data-type="keyword"></input>');
        checkbox = checkbox.attr('data-id', object.KEYWORDID);
        checkbox = checkbox.attr('data-name', object.KEYWORD);
        return checkbox[0].outerHTML;
    }

    var table = $('#keywordListDetail').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 2,
        "bAutoWidth": false,
        "aoColumns": [
        {"mData": actionCol, "sName": 'Delete', "sTitle": '<input type="checkbox" class="hidden" data-type="keyword" value="checkall"></input>', "bSortable": false, "sClass": "", "sWidth": "100px"},
        {"mData": "KEYWORD", "sName": 'Keyword', "sTitle": 'Keyword', "bSortable": false, "sClass": "", "sWidth": "125px"},
        {"mData": "TIMEUSED", "sName": 'Number of Times Used', "sTitle": 'Number of Times Used', "bSortable": false, "sClass": "", "sWidth": "220px"},
        {"mData": "CREATED", "sName": 'Date Created', "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "165px"}
        ],
        "sAjaxDataProp": "LISTKEYWORD",
        "sAjaxSource": '/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }

            $("#keywordListDetail input.selectKeyword").each(function(index, el) {
                if (listKeyword.indexOf($(el).data('id').toString()) >= 0) {
                    $(el).prop('checked', true);
                }
            });
        },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                aoData.push(
                    { "name": "inpAction", "value": inpAction}
                    );
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {

                        fnCallback(data);
                        if(data.LISTKEYWORD.length == 0) {
                            $('#keywordListDetail_paginate').hide();
                        }
                    }
                });
            },
            "fnInitComplete":function(oSettings){

            }
        });
}

function GetDownGradeMLPList(inpAction){

    var keyword = typeof(keyword)!='undefined'?keyword:"";

    var actionCol = function(object){
        var checkbox = $('<input type="checkbox" class="selectMLP" data-type="mlp"></input>');
        checkbox = checkbox.attr('data-id', object.ID);
        checkbox = checkbox.attr('data-name', object.NAME);
        return checkbox[0].outerHTML;
    }

    var table = $('#keywordListDetail').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 5,
        "bAutoWidth": false,
        "aoColumns": [
        {"mData": actionCol, "sName": 'Delete', "sTitle": '<input type="checkbox" class="hidden" data-type="mlp" value="checkall"></input>', "bSortable": false, "sClass": "", "sWidth": "100px"},
        {"mData": "NAME", "sName": 'Keyword', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "125px"},
        {"mData": "URL", "sName": 'Number of Times Used', "sTitle": 'Public Url', "bSortable": false, "sClass": "", "sWidth": "220px"},
        {"mData": "CREATED", "sName": 'Date Created', "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "165px"}
        ],
        "sAjaxDataProp": "ListMLPData",
        "sAjaxSource": '/session/sire/models/cfc/mlp.cfc?method=GetMLPList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }

            $("#keywordListDetail input.selectMLP").each(function(index, el) {
                if (listMLP.indexOf($(el).data('id').toString()) >= 0) {
                    $(el).prop('checked', true);
                }
            });
        },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpAction", "value": inpAction}
                    );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {

                        fnCallback(data);
                        if(data.ListMLPData.length == 0) {
                            $('#keywordListDetail_paginate').hide();
                        }
                    }
                });
            },
            "fnInitComplete":function(oSettings){

            }
        });
}

function GetDownGradeShortList(inpAction){

    var keyword = typeof(keyword)!='undefined'?keyword:"";

    var actionCol = function(object){
        var checkbox = $('<input type="checkbox" class="selectMLP" data-type="shorturl"></input>');
        checkbox = checkbox.attr('data-id', object.ID);
        checkbox = checkbox.attr('data-name', object.NAME);
        return checkbox[0].outerHTML;
    }

    var table = $('#keywordListDetail').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 5,
        "bAutoWidth": false,
        "aoColumns": [
        {"mData": actionCol, "sName": 'Delete', "sTitle": '<input type="checkbox" class="hidden" data-type="shorturl" value="checkall"></input>', "bSortable": false, "sClass": "", "sWidth": "100px"},
        {"mData": "NAME", "sName": 'Keyword', "sTitle": 'Name', "bSortable": false, "sClass": "", "sWidth": "125px"},
        {"mData": "URL", "sName": 'Number of Times Used', "sTitle": 'Short Url', "bSortable": false, "sClass": "", "sWidth": "220px"},
        {"mData": "CREATED", "sName": 'Date Created', "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "165px"}
        ],
        "sAjaxDataProp": "ListShortURLData",
        "sAjaxSource": '/session/sire/models/cfc/urltools.cfc?method=GetShortListForDowngrade&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        "fnDrawCallback": function( oSettings ) {
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }

            $("#keywordListDetail input.selectMLP").each(function(index, el) {
                if (listShortUrl.indexOf($(el).data('id').toString()) >= 0) {
                    $(el).prop('checked', true);
                    //listShortUrlName.push($(el).data('name'));
                }
            });
        },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpAction", "value": inpAction}
                    );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {

                        fnCallback(data);
                        if(data.ListShortURLData.length == 0) {
                            $('#keywordListDetail_paginate').hide();
                        }
                    }
                });
            },
            "fnInitComplete":function(oSettings){

            }
        });
}

$('.btn-downgrade-cancel').on('click', function(e){
    e.preventDefault();
    UIkit.modal('#downgrade-modal-sections')[0].toggle();
    cancelDowngrade();
 });

function cancelDowngrade(){
    bootbox.dialog({
            message: '<p>Are you sure you want to cancel Downgrade request ?</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                'Yes I Am': {
                    title: 'Remove From Current List',
                    className: 'btn btn btn-medium btn-success-custom',
                    callback: function() {
                        $('#processingPayment').show();
                        $.ajax({
                            type: "POST",
                            url: '/session/sire/models/cfc/order_plan.cfc?method=cancelDowngrade&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                            dataType: 'json',
                            data: {
                            },
                            error: function() {
                                $('#processingPayment').hide();
                            },
                            complete: function() {
                                $('#processingPayment').hide();
                            },
                            success: function(d2) {
                                $('#processingPayment').hide();
                                if(parseInt(d2.RXRESULTCODE) == 1){
                                    bootbox.dialog({
                                        message:'Your Downgrade request has been cancel.',
                                        title: "&nbsp;",
                                        className: "be-modal",
                                        buttons: {
                                            success: {
                                                label: "Ok",
                                                className: "btn btn-medium btn-success-custom",
                                                callback: function() {}
                                            }
                                        }
                                    }); 
                                }
                                else{
                                    bootbox.alert(d2.MESSAGE);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    title: 'Cancel',
                    className: 'btn btn btn-medium btn-back-custom'
                }
            }
        });
}
    


/****************************************************************************************************
**************************************** END DOWNGRADE PROCESS *******************************************
****************************************************************************************************
*/

$('#clear-calc-field').on('click', function(){
    document.getElementById("buy-credit-keyword").reset();
    $('#numberSMSToSend').val(0);
    $('#numberKeywordToSend').val(0);
    $('#amount').val(0);
    $('#amount-sms').text('$0');
    $('#amount-keyword').text('$0');
});

$('#select-plan-2').on('change', function(){
    var newPlanId = $(this).val();
    $('#planId').val(newPlanId);
});

$("#inpPromotionCode").keyup(function(event) {
    var code = $(this).val();
    couponValid = 0;
    if (code.length == 0) {
        couponValid = 0;
        $("#apply-btn").show();
        $("#remove-btn").hide();
        $("#help-block").hide();
        $(".form-group").removeClass('has-error').removeClass('has-ok');
    }
});

$(".span-plan-price").hide();
$("#select-plan-2").change(function(event) {
    var planid = this.value;
    if (planid > 1) {
        $(".section-payment").show();
        $('#purchase-btn').text('PURCHASE');
    } else {
        $(".section-payment").hide();
        $('#purchase-btn').text('GET STARTED');
    }
    $(".span-plan-price").hide();
    $("#span-plan-price-"+planid).show();
    $("#span-plan-price-yearly-"+planid).show();
});


$("body").on('click', '#apply-btn', function(event) {
    event.preventDefault();
    var code = $("#inpPromotionCode").val();
    getCouponCode(code);
});

$("body").on('click', '#remove-btn', function(event) {
    event.preventDefault();
    $("#inpPromotionCode").val('');
    $("#apply-btn").show();
    $("#remove-btn").hide();
    $("#help-block").hide().html('');
    $("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
});



function getCouponCode (code) {
    if (code != '') {
        $.ajax({
            url: '/session/sire/models/cfc/promotion.cfc?method=CheckCouponForUpgradePlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            type: 'POST',
            data: {inpCouponCode: code},
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                $("#help-block").html(data.PROMOTION_DISCOUNT);
                $("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
                $("#apply-btn").hide();
                $("#remove-btn").show();
                couponValid = 1;
            } else {
                $("#help-block").html(data.MESSAGE);
                $("#help-block").closest('.form-group').addClass('has-error').removeClass('has-ok');
                couponValid = -1;
                $("#apply-btn").hide();
                $("#remove-btn").show();
            }
            $("#help-block").show();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    }
}

function updateYearlyRenewChoice(val){
    $.ajax({
        url: '/session/cfc/administrator/userstool.cfc?method=UpdateYearlyRenewChoice&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST',
        dataType: 'json',
        data: {inpVal: val},
        // beforeSend: function( xhr ) {
        //     $('#processingPayment').hide();
        // },                    
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            // $('#processingPayment').hide();
            
        },                    
        success:function(d) {
            // $('#processingPayment').hide();
            
            alertBox(d.MESSAGE, 'My Plan - Plan Features');
            $('#to-renew-plan').val(val);
        }       
    });
    
}

$('input:radio[name=yearly-renew]').change(function() {

    if (this.value == '1') {

        bootbox.dialog({
            message: '<h4 class="be-modal-title">'+'My Plan - Plan Features'+'</h4><p>'+'Are you sure you want your plan to be renewed annually?'+'</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                       updateYearlyRenewChoice(1);
                    }
                },
                cancel: {
                    label: "NO",
                    className: "green-cancel",
                    callback: function(result) {
                        $("#uniform-agree-yearly-renew span").removeClass('checked');
                        $("#uniform-disagree-yearly-renew span").addClass('checked');
                        $('input:radio[name=yearly-renew][value="'+ $('#to-renew-plan').val() +'"]').prop('checked', true);
                    }
                },
            }
        });
    }
    else if (this.value == '0') {

        bootbox.dialog({
            message: '<h4 class="be-modal-title">'+'My Plan - Plan Features'+'</h4><p>'+'Are you sure you don\'t want your plan to be renewed annually?'+'</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                       updateYearlyRenewChoice(0);
                    }
                },
                cancel: {
                    label: "NO",
                    className: "green-cancel",
                    callback: function(result) {
                        $("#uniform-disagree-yearly-renew span").removeClass('checked');
                        $("#uniform-agree-yearly-renew span").addClass('checked');
                        $('input:radio[name=yearly-renew][value="'+ $('#to-renew-plan').val() +'"]').prop('checked', true);
                    }
                },
            }
        });
    }
});

$('#inpPromotionCode').keypress(function( e ) {
    if(e.which === 32) 
        return false;
});

window.myOpts = $("option.annually-plan").detach();

$('#month-Year-Switch').on('click', function(){
    // $('#select-plan-1').prop('selectedIndex',0);
    // for postpaid plan force select monthly
    if($("#planStatus").val() !=1)
    {
        return false;
    }
    $("#select-plan-1").val($("#no-plan-selected").val()).trigger('change');
    $('.triangle-isosceles.left').toggleClass('changed');
    if($(this).is(':checked')){
        $('#is-yearly-plan').val(1);
        $('.monthly_price').toggleClass('hidden');
        $('.yearly_price').toggleClass('hidden');
        // $("#select-plan-1").append(window.myOpts);
        // window.myOpts1 = $("option.monthly-plan").detach();
        $('#monthly-plan-text').css('color', '#929292');
        $('#yearly-plan-text').css('color', '#568ca5');
        $('.triangle-isosceles').css('background', '#568ca5');
        // $('#monthly-or-yearly-amount').text('Yearly Amount:');
        $('#monthYearSwitch').val(2);        
    }
    else{
        $('#is-yearly-plan').val(0);
        $('.monthly_price').toggleClass('hidden');
        $('.yearly_price').toggleClass('hidden');
        // $("#select-plan-1").append(window.myOpts1);
        // window.myOpts2 = $("option.annually-plan").detach();
        $('#monthly-plan-text').css('color', '#568ca5');
        $('#yearly-plan-text').css('color', '#929292');
        $('.triangle-isosceles').css('background', '#929292');
        // $('#monthly-or-yearly-amount').text('Monthly Amount:');
        // var myOpts = $("option.annually-plan").detach();        
        $('#monthYearSwitch').val(1);        
    }    
    // checkUpgrade();
});

})(jQuery);


$(document).ready(function() {

   $.ajax({
        method: 'POST',
        url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentMethodSettingByUserId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
        data:{},
        dataType: 'json',
        success: function(data) {
            
            if (data && data.RXRESULTCODE == 1) {
                $('#payment_method_value').val(data.RESULT);
                $('.inner-update-cardholder').show();
                $('.card-support-type').show();
                $('#section-2').hide();
                $('.btn-update-profile').prop('disabled', true);
            }
        }
    });

    $('#monthly-plan-text').css('color', '#568ca5');

    function update_expiration_date_upgrade() {
        $('#expiration_date_upgrade').val($('#expiration_date_month_upgrade').val() + '/' + $('#expiration_date_year_upgrade').val());
    }

    $('#expiration_date_month_upgrade, #expiration_date_year_upgrade').change(function(){
        update_expiration_date_upgrade();
    });


    function update_expiration_date() {
        $('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
    }

    $('#expiration_date_month, #expiration_date_year').change(function(){
        update_expiration_date();
    });

    if($('#fset_cardholder_info').is(':visible')){
        $('.btn-update-profile').prop('disabled', false);
    }
    else
        $('.btn-update-profile').prop('disabled', true);

    //Set default country
    setTimeout(function() {
        var country_val = $( "#country option:selected" ).val();
        if(!country_val){
            $("#country").val("US");
        }
    }, 1000);

    if(activeTab == 'addon'){
         $("#addon_link").trigger('click');
    }
    else if(activeTab == 'upgrade'){
        $("#upgradeplan_tab").trigger('click');
    }

    $("#pp_create_billing_agreement").on('click', function(){
       
        var formData ={
        };
        var jsondata = JSON.stringify(formData);
        $("#processingPayment").show();
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=SetPPExpressCheckout&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data:{
                inpAmount: 0,
                inpPaymentDescription: jsondata,
                inpCancelUrl:"http://"+window.location.hostname+"/session/sire/pages/billing",
                inpReturnlUrl:"http://"+window.location.hostname+"/session/sire/models/cfm/paypal_create_billing_agreement"
            },
            dataType: 'json',
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#processingPayment").hide();
            },          
            success: function(data) {
                $("#processingPayment").hide();
                if (data && data.RXRESULTCODE == 1) {
                    var url_direct = paypalCheckoutUrl+data.RESULT;
                    location.href = url_direct;
                } else {
                    alertBox(data.MSG, buy_addon_title);
                }
            }
        });
    });

});