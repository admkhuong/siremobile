function isNumber(n) 
{
return !isNaN(parseFloat(n)) && isFinite(n);
}

(function($){
    
    $('#phonenumber-option').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topLeft"});
    $("#phone-number").mask("(000)000-0000");
    
	var new_cc_status = 0;
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	
	//Filter bar initialize
	var StatusCustomData = function(){
		var options   = '<option value= "99" selected> </option>'+
						'<option value="0">Not a Survey</option>'+
						'<option value="1">Open</option>'+
						'<option value="2">Interval Hold</option>'+
						'<option value="3">Response Interval</option>'+
						'<option value="4">Close</option>'+
						'<option value="5">Cancelled</option>'+
						'<option value="6">Expired</option>'+
						'<option value="7">Terminated</option>'+
						'<option value="8">Stop</option>'+
						'<option value="9">Too Many Sessions</option>';
		return $('<select class="filter-form-control form-control"></select>').append(options);
		};
	
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Campaign Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.BatchId_bi '},
			{DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
			{DISPLAY_NAME: 'Create Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' b.Created_dt '},
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' sire.SessionState_int ',CUSTOM_DATA:StatusCustomData}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	$('#report-select').on('change', function(){
		var report_type =  $('#report-select').val();
		if(report_type == 1){
			$('#campaign-list').show();
			$('#optin-optout-list').hide();
			$('#MO-list').hide();
			$('#report-detail').text('The Campaign List');
			document.getElementById('box-filter').innerHTML = "";
			$('#box-filter').sireFilterBar({
				fields: [
					{DISPLAY_NAME: 'Campaign Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.BatchId_bi '},
					{DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
					{DISPLAY_NAME: 'Create Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' b.Created_dt '},
					{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '}
				],
				clearButton: true,
				// rowLimited: 5,
				error: function(ele){
					console.log(ele);
				},
				// limited: function(msg){
				// 	alertBox(msg);
				// },
				
			}, function(filterData){
				//called if filter valid and click to apply button
			});
		}else{
			if(report_type == 2){
				$('#campaign-list').hide();
				$('#optin-optout-list').show();
				$('#MO-list').hide();
				$('#report-detail').text('Opt In and Opt Out List');
				document.getElementById('box-filter').innerHTML = "";
				$('#box-filter').sireFilterBar({
					fields: [
						{DISPLAY_NAME: 'Batch Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ire.BatchId_bi '},
						{DISPLAY_NAME: 'Batch Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '}
					],
					clearButton: true,
					// rowLimited: 5,
					error: function(ele){
						console.log(ele);
					},
					// limited: function(msg){
					// 	alertBox(msg);
					// },
					
				}, function(filterData){
					//called if filter valid and click to apply button
				});
			}else{
				$('#campaign-list').hide();
				$('#optin-optout-list').hide();
				$('#MO-list').show();
				$('#report-detail').text('Message Received from Device (MO) List');
				document.getElementById('box-filter').innerHTML = "";
				$('#box-filter').sireFilterBar({
					fields: [
						{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' UserId_int '},
						{DISPLAY_NAME: 'Batch Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' BatchId_bi '},
						{DISPLAY_NAME: 'Message Description', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' Response_vch '}
					],
					clearButton: true,
					// rowLimited: 5,
					error: function(ele){
						console.log(ele);
					},
					// limited: function(msg){
					// 	alertBox(msg);
					// },
					
				}, function(filterData){
					//called if filter valid and click to apply button
				});
			}
		}
		InitControl();
	});

	//InitControl();

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
		if($('#phone-number').val() == ''){
			return false;
		}
		var reporttype = $('#report-select').val();
		var phonenumberlookup = $('#phone-number').val().replace('(','');
			phonenumberlookup = phonenumberlookup.replace(')','');
			phonenumberlookup = phonenumberlookup.replace('-','');
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
        var actionShowDetail = function (object) {
			var strReturn = '<a href="" id ="'+object.BATCH_ID+'" phone-number ="'+phonenumberlookup+'" session-id="'+object.SESSIONID+'" class="view-conversation-detail">View detail</a>';
            return strReturn;
		}
		var viewDetail = function (object) {
			if(object.STATUS == 'OPT IN'){
				var strReturn = '<a href="" id ="'+object.BATCH_ID+'" phone-number ="'+phonenumberlookup+'" session-id="'+object.SESSIONID+'" class="view-optinout-detail">View detail</a>';
			}else{
				var strReturn = '';
			}
			return strReturn;
		}

		
		var actionCloseSeesion = function (object) {
			if(object.STATUS == 'Open'){
				var strReturn = '<a href="" id ="'+object.SESSIONID+'" id-name ="" class="deactive-batch">Close</a>';
           }else{
				var strReturn = '';
			}
            return strReturn;
		}
		// Phone number list 1
		if(reporttype ==1){
			_tblCampaignList = $('#tblCampaignList').dataTable( {
				"bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "input",
				"bLengthChange": false,
				"iDisplayLength": 20,
				"aoColumns": [
					{"mData": "SESSIONID", "sName": 'ire.IRESessionId_bi', "sTitle": 'Session Id', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
					{"mData": "BATCH_ID", "sName": 'ire.BatchId_bi', "sTitle": 'Campaign Id', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
					{"mData": "BATCH_NAME", "sName": 'b.Desc_vch', "sTitle": 'Campaign Name', "sWidth": '16%',"bSortable": false},
					{"mData": "CREATE_DATE", "sName": 'sire.Created_dt', "sTitle": 'Created Date', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
					{"mData": "UPDATE_DATE", "sName": 'sire.LastUpdated_dt', "sTitle": 'Updated Date', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
					{"mData": "STATUS", "sName": 'b.BatchId_bi', "sTitle": 'Status', "sWidth": '8%',"bSortable": false,"sClass": "col-center"},
					{"mData": actionShowDetail, "sName": 'Action', "sTitle": 'Conversation Detail', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
					{"mData": actionCloseSeesion, "sName": 'Action', "sTitle": 'Close Session', "bSortable": false, "sClass": "col-center", "sWidth": "8%"}
				],
				"bAutoWidth": false,
				"sAjaxDataProp": "ListEMSData",
				"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=PhoneNumberLookupList1&inpPhoneNumber='+phonenumberlookup+strAjaxQuery,
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				   },
				   "fnDrawCallback": function( oSettings ) {
					var paginateBar = this.siblings('div.dataTables_paginate');
					  if (oSettings._iDisplayStart < 0){
						oSettings._iDisplayStart = 0;
						paginateBar.find('input.paginate_text').val("1");
					}
					if(oSettings._iRecordsTotal < 1){
						paginateBar.hide();
					} else {
						paginateBar.show();
					}
				},
				"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
				   aoData.push(
						{ "name": "customFilter", "value": customFilterData}
					);
					$.ajax({dataType: 'json',
							 type: "POST",
							 url: sSource,
							 data: aoData,
							 success: fnCallback
					 });
				},
				"fnInitComplete":function(oSettings, json){
					$("#tblCampaignList thead tr").find(':last-child').css("border-right","0px");
				}
			});
		}else{
			if(reporttype==2){
				// Phone number list 2
				_tblOptInOptOutList = $('#tblOptInOptOutList').dataTable( {
					"bProcessing": true,
					"bFilter": false,
					"bServerSide":true,
					"bDestroy":true,
					"sPaginationType": "input",
					"bLengthChange": false,
					"iDisplayLength": 20,
					"aoColumns": [
						//{"mData": "USERID", "sName": 'u.UserId_int', "sTitle": 'User ID', "sWidth": '7%',"bSortable": false,"sClass": "col-center"},
						{"mData": "SESSIONID", "sName": 'ire.IRESessionId_bi', "sTitle": 'Session Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
						{"mData": "BATCH_ID", "sName": 'ire.BatchId_bi', "sTitle": 'Batch Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
						{"mData": "BATCH_NAME", "sName": 'b.Desc_vch', "sTitle": 'Batch Name', "sWidth": '50%',"bSortable": false},
						{"mData": "DATETIME_OPT", "sName": 'ire.Created_dt', "sTitle": 'Date Time', "sWidth": '15%',"bSortable": false,"sClass": "col-center"},
						{"mData": "STATUS", "sName": 'Action', "sTitle": 'Status', "sWidth": '15%',"bSortable": false,"sClass": "col-center"},
						{"mData": viewDetail, "sName": 'Action', "sTitle": 'Conversation Detail', "bSortable": false, "sClass": "col-center", "sWidth": "15%"}
					],
					"bAutoWidth": false,
					"sAjaxDataProp": "ListEMSData",
					"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=PhoneNumberLookupList2&inpPhoneNumber='+phonenumberlookup+strAjaxQuery,
					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					},
					"fnDrawCallback": function( oSettings ) {
						var paginateBar = this.siblings('div.dataTables_paginate');
						if (oSettings._iDisplayStart < 0){
							oSettings._iDisplayStart = 0;
							paginateBar.find('input.paginate_text').val("1");
						}
						if(oSettings._iRecordsTotal < 1){
							paginateBar.hide();
						} else {
							paginateBar.show();
						}
					},
					"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
					aoData.push(
							{ "name": "customFilter", "value": customFilterData}
						);
						$.ajax({dataType: 'json',
								type: "POST",
								url: sSource,
								data: aoData,
								success: fnCallback
						});
					},
					"fnInitComplete":function(oSettings, json){
						$("#tblOptInOptOutList thead tr").find(':last-child').css("border-right","0px");
					}
				});
			}else{
				// Phone number list 3
				_tblMOList = $('#tblMOList').dataTable( {
					"bProcessing": true,
					"bFilter": false,
					"bServerSide":true,
					"bDestroy":true,
					"sPaginationType": "input",
					"bLengthChange": false,
					"iDisplayLength": 20,
					"aoColumns": [
						{"mData": "IRE_ID", "sName": '', "sTitle": 'IRE Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
						{"mData": "BATCH_ID", "sName": 'BatchId_bi', "sTitle": 'Batch Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
						{"mData": "USERID", "sName": 'UserId_int', "sTitle": 'User Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
						{"mData": "CREATE_DATE", "sName": 'Created_dt', "sTitle": 'Create Date', "sWidth": '15%',"bSortable": false,"sClass": "col-center"},
						{"mData": "MESSAGE_DESC", "sName": 'Response_vch', "sTitle": 'Message Description', "sWidth": '55%',"bSortable": false}
					],
					"bAutoWidth": false,
					"sAjaxDataProp": "ListEMSData",
					"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=PhoneNumberLookupList3&inpPhoneNumber='+phonenumberlookup+strAjaxQuery,
					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					},
					"fnDrawCallback": function( oSettings ) {
						var paginateBar = this.siblings('div.dataTables_paginate');
						if (oSettings._iDisplayStart < 0){
							oSettings._iDisplayStart = 0;
							paginateBar.find('input.paginate_text').val("1");
						}
						if(oSettings._iRecordsTotal < 1){
							paginateBar.hide();
						} else {
							paginateBar.show();
						}
					},
					"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
					aoData.push(
							{ "name": "customFilter", "value": customFilterData}
						);
						$.ajax({dataType: 'json',
								type: "POST",
								url: sSource,
								data: aoData,
								success: fnCallback
						});
					},
					"fnInitComplete":function(oSettings, json){
						$("#tblMOList thead tr").find(':last-child').css("border-right","0px");
					}
				});
			}
		}
	}	

	$('#box-filter').on('change', function(event){
		InitControl();
	});
	
	$('#btn_Lookup').on('click', function(event){
		if($('#phonenumber-option').validationEngine('validate')){
			InitControl();
		}
	});
    
    $("#tblCampaignList").on("click", ".deactive-batch", function(event){
		event.preventDefault();
		var SESSIONID = $(this).attr('id');
        confirmBox('Are you sure you want to close this session?', 'Close Session', function(){
            $.ajax({
                url: '/session/sire/models/cfc/campaign.cfc?method=CloseBatch'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
					inpSessionId:SESSIONID
                },
                beforeSend: function(){
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alertBox('Close Session Error!','Close Session',''); 
                },
                success: function(data) {
                    if (data.RXRESULTCODE == 1) {
                        InitControl(); 
                        alertBox(data.MESSAGE,'Close Session','');
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Close Session','');
                    }                       
                }
            });
        });
	});
	
	$("#tblCampaignList").on("click", ".view-conversation-detail, .view-optinout-detail", function(event){
		event.preventDefault();
		var Batch_ID = $(this).attr('id');
		var Contactstring = $(this).attr('phone-number');
		var sessionid = $(this).attr('session-id');
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";

		_tblPhoneConversationDetail = $('#tblPhoneConversationDetail').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"mData": "SHORT_CODE", "sName": '', "sTitle": 'Address', "sWidth": '15%',"bSortable": false,"sClass": "col-center"},
				{"mData": "USERID", "sName": 'bll.BatchId_bi', "sTitle": 'User Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": "IRE_ID", "sName": 'b.Desc_vch', "sTitle": 'IRE Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": "DATE_TIME", "sName": 'bll.Created_dt', "sTitle": 'Date Time', "sWidth": '12%',"bSortable": true,"sClass": "col-center"},
				{"mData": "CP_ID", "sName": 'bll.ActualLoaded_int', "sTitle": 'CP Id', "sWidth": '8%',"bSortable": true,"sClass": "col-center"},
				{"mData": "MESSAGE_TYPE", "sName": 'bll.AmountLoaded_int', "sTitle": 'Message Type', "sWidth": '15%',"bSortable": true},
				{"mData": "MESSAGE_DESC", "sName": 'bll.Status_int', "sTitle": 'Message Description', "sWidth": '30%',"bSortable": false}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=PhoneConversationList&inpPhoneNumber='+Contactstring+'&inpBatchId='+Batch_ID+'&inpSessionId='+sessionid+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblPhoneConversationDetail thead tr").find(':last-child').css("border-right","0px");
			}
		});

		$('#phone-conversation-view').modal('show');
	});
	
	$("#tblOptInOptOutList").on("click", ".view-optinout-detail", function(event){
		event.preventDefault();
		var Batch_ID = $(this).attr('id');
		var Contactstring = $(this).attr('phone-number');
		var sessionid = $(this).attr('session-id');

		_tblPhoneConversationDetail = $('#tblPhoneConversationDetail').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "SHORT_CODE", "sName": '', "sTitle": 'Address', "sWidth": '15%',"bSortable": false,"sClass": "col-center"},
				{"mData": "USERID", "sName": 'bll.BatchId_bi', "sTitle": 'User Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": "IRE_ID", "sName": 'b.Desc_vch', "sTitle": 'IRE Id', "sWidth": '10%',"bSortable": false,"sClass": "col-center"},
				{"mData": "DATE_TIME", "sName": 'bll.Created_dt', "sTitle": 'Date Time', "sWidth": '12%',"bSortable": true,"sClass": "col-center"},
				{"mData": "CP_ID", "sName": 'bll.ActualLoaded_int', "sTitle": 'CP Id', "sWidth": '8%',"bSortable": true,"sClass": "col-center"},
				{"mData": "MESSAGE_TYPE", "sName": 'bll.AmountLoaded_int', "sTitle": 'Message Type', "sWidth": '15%',"bSortable": true},
				{"mData": "MESSAGE_DESC", "sName": 'bll.Status_int', "sTitle": 'Message Description', "sWidth": '30%',"bSortable": false}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=PhoneConversationList&inpPhoneNumber='+Contactstring+'&inpBatchId='+Batch_ID+'&inpSessionId='+sessionid+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	       	},
		   	"fnDrawCallback": function( oSettings ) {
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": ""}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblPhoneConversationDetail thead tr").find(':last-child').css("border-right","0px");
			}
		});

		$('#phone-conversation-view').modal('show');
    });
    

})(jQuery);