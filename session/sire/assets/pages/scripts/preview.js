(function($){
	
	var inpTimeTraveledSoFarInMinutes = 0;
	var AllowBlanks = 0;
	
	function escapeHtml(text) {
	
	  // Preserve newlines		
	  text = text.replace(/<br>/gi, 'nexlinex');
	 
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };
	
	  text = text.replace(/[&<>"']/g, function(m) { return map[m]; });	  
	  text = text.replace(/nexlinex/gi, '<br>');
	  
	  return text;
	}
			
	$(function() {	
		$.ajaxSetup({ cache: false });
		
		//$('#SMSTextInputArea').autogrow();
						
		
		$('#SMSTextInputArea').resize(function() {
						
		  	//$("#mCSB_1_container").height($('#PhoneScreenArea').height() - $('#SMSTextInputAreaContainer').height() - 31 - $('#SMSToAddress').height());
			//$("#mCSB_1_container").animate({ scrollTop: $('#mCSB_1_container')[0].scrollHeight}, 1000);
			
		});

	
		$('#STARTCP').click(function(){
			SetStartCP();
		});
		
		
		
		$("#CPOptionsLink").click(function(){
			
			$('#StartCPContainer').toggle('slow', function() {
				
			  });
  
		});	
		
		$("#JSONOptionsLink").click(function(){
			
			$('#JSONOptionsContainer').toggle('slow', function() {
				
				$('#MenuTipSlideMiddle').toggle();
				
			  });
  
		});		
										
		
		$("#TimeMachineOptionsLink").click(function(){
			
			$('#TimeMachineContainer').toggle('slow', function() {
				
			  });
  
		});		
		

		
		$('#SMSSend').click(function(){
			var inpContactString = $('#inpContactString').val(); 
			if(inpContactString.length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			var SMSTextInputArea = $('#SMSTextInputArea').val();
			
			if(AllowBlanks == 0)
				if(SMSTextInputArea.length == 0 )
				{
					//$.alerts.okButton = '&nbsp;OK&nbsp;';
					//jConfirm("Text to send has not been entered.\n"  + "Text to send can not normally be blank. Hit OK if you want to allow future blanks to be sent in this session." + "\n", "Warning!", function(result) { AllowBlanks = 1 } );										
					return false;						
				}
			
			var inpSMSToAddress = $('#inpSMSToAddress').val();		
			if(inpSMSToAddress.length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}			

			
			if(SMSTextInputArea == "")
				$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + '&nbsp;' + '</div>');
			else
				$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + SMSTextInputArea + '</div>');
					
			$("#mCSB_1_container").stop(true, true).animate({ scrollTop: $('#mCSB_1_container')[0].scrollHeight}, 1000);
						
					
			GetResponseSimple(inpSMSToAddress, SMSTextInputArea, inpContactString, 0 );
			
		
			
			
		});	
		
		$('#TIMEMACHINE').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}		
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			
			TimeMachineProcess($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
			
			
		});	
		
		$('#TIMEMACHINECHECK').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			
			CheckNextResponse($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
			
			
		});	
		
		$('.ottdemokeyword').click(function(){			
			
			var inpContactString = $('#inpContactString').val();
			var inpSMSToAddress = $('#inpSMSToAddress').val();					
						
			
			if($(this).attr("rel1") == "")
				$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + '&nbsp;' + '</div>');
			else
				$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + $(this).attr("rel1") + '</div>');
					
			$("#mCSB_1_container").stop(true, true).animate({ scrollTop: $('#mCSB_1_container')[0].scrollHeight}, 1000);
						
					
			GetResponseSimple(inpSMSToAddress, $(this).attr("rel1"), inpContactString, 0 );
			
			
			
		});	
				
		
		$("#SMSTextInputArea").keyup(function (e) {
			if (e.keyCode == 13 && !$('#SMSSend').prop('disabled')) {
				$('#SMSSend').click();
			}
		});



		$('#SMSClear').click(function(){	
			
			$('#mCSB_1_container').html("");
			$('#SMSTextInputArea').resize();
		});



		$('#MenuTipSlide').click(function(){
			$('#MenuTipSlide').hide();
		});
		
		
	
		$('#PhoneMenuToggle, #SMSPreview').click(function(){
			$('#mCSB_1_container').html("");
			var keywordId = $('#keywordId').val();
			
			
	//		if ($.isNumeric(keywordId) && keywordId > 0) 
	//		{
				GetResponseSimple($('#inpSMSToAddress').val(), $('#Keyword').length > 0 ? $('#Keyword').val() : "", $('#inpContactString').val(), 0 );
				var txtKeyword = $.trim($('#txtKeyword').val());
				if (txtKeyword != '') 
				{
					$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + txtKeyword + '</div>');
				}
				else
				{					
					txtKeyword = $.trim($('#Keyword').val());
					if (txtKeyword != '') 
					{
						$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">' + txtKeyword + '</div>');
					}
					else
					{						
						$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble me">Preview This Campaign: '+ inpBatchName +'</div>');
					}					
				}
		//	}
	//		else 
	//		{
	//			$('#ConfirmSaveModal').modal('show');
	//		}
		});
					
	});
	
	function MidStr(str, start, len)
        /***
                IN: str - the string we are LEFTing
                    start - our string's starting position (0 based!!)
                    len - how many characters from start we want to get

                RETVAL: The substring from start to start+len
        ***/
        {
                // Make sure start and len are within proper bounds
                if (start < 0 || len < 0) return "";

                var iEnd, iLen = String(str).length;
                if (start + len > iLen)
                        iEnd = iLen;
                else
                        iEnd = start + len;

                return String(str).substring(start,iEnd);
        }
	
	
	var inpflag = 0;
	var MaxStatements = 20;
	var TIMEZONEOFFSETPST = 0;
	
	function GetResponseSimple(inpShortCode, inpKeyword, inpContactString, inpOverRideInterval) {
			
							
		$.ajax({
		
		url: '/session/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpQATool : 1,
					inpBatchId : inpBatchId					
				},
		beforeSend: function(){
			$('#processingPayment').show();
		},							  
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#processingPayment').hide();
		},					  
		success:		
			function(d) 
			{
					$('#processingPayment').hide();																
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0)
						{
							
							if(d.RESPONSE.length > 0)
							{	
							
								var ConcatenateOffFlag = 0;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.RESPONSE;		
									
									
									
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;																		
									}
									
									
									AmountToGrab = AmountToGrab - 1;
									
									
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									
									
									$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guesspreview you">' + escapeHtml(BuffStrOut) + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										
										
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											
										}
										
										
										AmountToGrab = AmountToGrab -1;
													
										
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										
										CurrPos = CurrPos + AmountToGrab;
										
										$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guesspreview you">' + escapeHtml(BuffStrOut) + '</div>');									
									}
								
								}
								else
								{
									$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guesspreview you" rel="' + d.RESPONSELENGTH + '">' + escapeHtml(d.RESPONSE) + '</div>');																		
								}
								
								$('#SMSTextInputArea').resize();																	
															
								//$("#mCSB_1_container").animate({ scrollTop: $('#mCSB_1_container')[0].scrollHeight}, 1000);
							}
							
							
							if(d.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF' || d.RESPONSETYPE == 'RESET')
							{
								EBMText = 'SIRE - ' +  d.RESPONSETYPE;
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								if(inpflag < MaxStatements)
									GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
								
								
																		
							}
							else if(d.RESPONSETYPE == 'AGENT')
							{
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								
															
							}
							else if(d.RESPONSETYPE == 'INTERVAL')
							{																
								//QueueNextResponse(inpShortCode, 'EBM - INTERVAL', inpContactString, d.INTERVALSCHEDULED, d.INTERVALEXPIREDNEXTQID)
								if(d.INTERVALTYPE == 'SECONDS' && parseInt(d.INTERVALVALUE) <= 30 )
								{
								
									if(parseInt(d.INTERVALVALUE) < 5)
										d.INTERVALVALUE = 3;
									
											
									<!---console.log('Settimeout interval');	--->
									setTimeout(function(){ GetResponseSimple(inpShortCode, 'SIRE - INTERVAL', inpContactString, 1); }, parseInt(d.INTERVALVALUE)*1000);									
								}
								else
								{
									QueueNextResponse(inpShortCode, 'SIRE - INTERVAL', inpContactString, d.INTERVALSCHEDULED, d.INTERVALEXPIREDNEXTQID, d.IRESESSIONID)
								}								
								
								$('#INTERVALSCHEDULED').html(d.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.TIMEZONEOFFSETPST);
								$('#IntervalOptions').show();
							}
							else
							{
								
								MaxStatements = inpflag + 20; 
								
							}							
						}
						else
						{
							
						}
					}
					else
					{	
						
					}
						   var isTestKeyword = d.ISTESTKEYWORD;
								
								if(isTestKeyword == 1)
								{
									$("#isTestKeyword").val("0");
									$("#isEmitChatContent").val("0");
									$("#Keyword").val(inpKeyword);
								}
								else if(isTestKeyword == -1)
								{
									$("#isTestKeyword").val("1");
								}	
								else
								{
									$("#isTestKeyword").val("");
									$("#isEmitChatContent").val("");
								}
								
								var isEmitChatContent = $("#isEmitChatContent").val();
								isTestKeyword = $("#isTestKeyword").val();
								if(isEmitChatContent == 0 && isTestKeyword == 1 && inpKeyword != "")
								{
									$("#isEmitChatContent").val("1");
									$('#SMSTextInputArea').resize();
								}	 		
					$('#SMSTextInputArea').val('');							
			} 		
			
		});
		return false;
	}
	
	function TimeMachineProcess(inpShortCode, inpKeyword, inpContactString) {
	
		MaxStatements = inpflag + 20; 
	
		$.ajax({
		url: '/session/cfc/csc/csc.cfc?method=TimeMachineGetNextResponseQA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:{ 					
			inpContactString : inpContactString,
			inpShortCode : inpShortCode,					
			inpNewLine : "<BR>",
			inpTimeTraveledSoFarInMinutes : inpTimeTraveledSoFarInMinutes
		},
		beforeSend: function(){
			$('#processingPayment').show();
		},							  
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#processingPayment').hide();
		},					  
		success:		
			
			function(d) 
			{
					$('#processingPayment').hide();
									
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
											
						if(CurrRXResultCode > 0 && typeof(d.GETRESPONSE.RESPONSE) != "undefined" )
						{
							if(d.GETRESPONSE.RESPONSE.length > 0 )
							{	
								var ConcatenateOffFlag = 0;
							
								inpTimeTraveledSoFarInMinutes = d.INPTIMETRAVELEDSOFARINMINUTES;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.GETRESPONSE.RESPONSE;		
									
									
									
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
																		
									}
									
									
									AmountToGrab = AmountToGrab - 1;
									
									
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									
									
									$('#mCSB_1_container').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');								
									$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guess you">' + escapeHtml(BuffStrOut) + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										
										
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											
										}
										
										
										AmountToGrab = AmountToGrab -1;
													
										
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										
										CurrPos = CurrPos + AmountToGrab;
										
										$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guess you">' + escapeHtml(BuffStrOut) + '</div>');									
									}
								
								}
								else
								{
										$('#mCSB_1_container').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');
										$('#mCSB_1_container').append('<div style="clear:both"></div><div class="bubble guess you" rel="' + d.GETRESPONSE.RESPONSELENGTH + '">' + escapeHtml(d.GETRESPONSE.RESPONSE) + '</div>');
								}
							
										
								$('#SMSTextInputArea').resize();					
								$("#mCSB_1_container").animate({ scrollTop: $('#mCSB_1_container')[0].scrollHeight}, 1000);
						
							}
							
							
							CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
							
							if(typeof(d.GETRESPONSE.RESPONSETYPE) != "undefined")
							{		
								
								if(d.GETRESPONSE.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF')
								{
									
									EBMText = 'SIRE - ' +  d.GETRESPONSE.RESPONSETYPE;
									d.GETRESPONSE.RESPONSETYPE = '';
									
																		
									inpflag++;
									
									if(inpflag < MaxStatements)
										GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
									
																		
									
																			
								}
								else if(d.RESPONSETYPE == 'AGENT')
								{
									d.RESPONSETYPE = '';
									
									inpflag++;
									
									
																			
								}
								else if(d.GETRESPONSE.RESPONSETYPE == 'INTERVAL')
								{									
									QueueNextResponse(inpShortCode, 'SIRE - INTERVAL', inpContactString, d.GETRESPONSE.INTERVALSCHEDULED, d.GETRESPONSE.INTERVALEXPIREDNEXTQID)
									
									
									
									
									
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();
								}
								else
								{
									
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();									
									
								}
							}
							
						}
						else
						{
							
						}
					}
					else
					{	
						
					}
											
			} 		
			
		});
		return false;
	}
	
	
	function QueueNextResponse(inpShortCode, inpKeyword, inpContactString, inpScheduled, inpTimeOutNextQID)
	{		
		var data = 
		{ 
			username : 'demo.sire',
			password : 'A0DF875079D6267E05FC',
			inpContactString : inpContactString,
			inpShortCode : inpShortCode,
			inpQueueState : '4',
			inpScheduled : inpScheduled,
			inpTimeOutNextQID : inpTimeOutNextQID
		};
				
		ServerProxy.PostToServerStruct('/session/cfc/csc/csc.cfc', 'QueueNextResponseSMS', data, "Error: Next Interval response has not been set!", function(d ) {
			CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
		});	
	
	}
	
	function CheckNextResponse(inpShortCode, inpKeyword, inpContactString)
	{		
		
		
	
	
	
		var data = 
		{ 
			username : 'demo.sire',
			password : 'A0DF875079D6267E05FC',
			inpContactString : inpContactString,
			inpShortCode : inpShortCode
		};
		
		
				
		ServerProxy.PostToServerStruct('/session/cfc/csc/csc.cfc', 'TimeMachineCheckNextResponseQA', data, "Error: Next Interval response can not be found!", function(d ) {		
				
			if(typeof(d.RXRESULTCODE) != "undefined")
			{							
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && typeof(d.NEXTEVENT) != "undefined" )
				{
					$('#INTERVALSCHEDULED').html(d.NEXTEVENT);	
					$('#IntervalOptions').show();						
				}
				else
				{
					$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');					
				}
			}
			else
			{
				$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');				
			}
							
		});	
	
	}
	
	
	function SetStartCP()
	{		
		var data = 
		{ 
			inpContactString : $('#inpContactString').val(),
			inpShortCode : $('#inpSMSToAddress').val(),
			inpKeyword : $('#inpCPKeyword').val(),
			inpCP : $('#inpCP').val()
		};
				
		ServerProxy.PostToServerStruct('/session/cfc/csc/csc.cfc', 'SetSurveyStateToCP', data, "Error: Starting Control Point has not been set!", function(d ) {
			
			if(typeof(d.RXRESULTCODE) != "undefined")
			{		
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && d.PROGRAMSETCPOK > 0 )
				{
					inpTimeTraveledSoFarInMinutes = 0;
					
					MaxStatements = inpflag + 20; 
			
					inpflag++;
			
					if(inpflag < MaxStatements)
						GetResponseSimple($('#inpSMSToAddress').val(), inpflag, $('#inpContactString').val(), 1); 					
				}
			}
		});	
	}
		
})(jQuery);