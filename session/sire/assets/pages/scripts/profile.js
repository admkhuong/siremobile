var ProfileComponents = function () {

    return {
        //#percent-complete

        //main function to initiate the module
        initDialStep: function () {
            //knob does not support ie8 so skip it
            if (!jQuery().knob || App.isIE8()) {
                return;
            }

            // general dialStep            
            $("#dialStep").knob({
                width: '100%',
                bgColor: '#5c5c5c',
                fgColor: '#74c37f',
                readOnly: true,
                displayInput: false
            });

            animateChart('dialStep', 0, $('#percent-complete').val());

            /* Update circle value when input.animate changes */
            $("#dialStep").change(function () {
                $(this).parents('.wrap-dial-step').find(".pie_value").text($(this).val());
            });

            /* Animate chart from start_val to end_val */
            function animateChart (chart_id, start_val, end_val) {
                $({chart_value: start_val}).animate({chart_value: end_val}, {
                    duration: 1000,
                    easing: 'swing',
                    step: function () {
                        $('#' + chart_id).val(Math.ceil(this.chart_value)).trigger('change');
                    }
                });
            };  
        },

        initDoughnutStep: function () {
            Chart.defaults.global.tooltips.enabled = false;

            var val1 = parseFloat($('#percent-complete').val()).toFixed(0);
            var val2 = 100 - val1;

            checkFinalCounter(val1);
            

            var config = {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: [val1, val2],
                            backgroundColor: [
                                '#74c37f',
                                '#5c5c5c'
                            ],
                            hoverBackgroundColor: ['#74c37f'],
                            borderWidth: 0,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 70
                }
            }
            
            window.onload = function () {
                $('.wrap-dial-step').find('.pie_value').text(val1);
                var ctx = document.getElementById('dialStep').getContext("2d");
                window.myDoughnut = new Chart(ctx, config);    
            }            

            function updatePercentStep (val1, val2) {
                if (config.data.datasets.length > 0) {
                    config.data.datasets.forEach(function(dataset) {
                        dataset.data[0] = val1;
                        dataset.data[1] = val2;
                    });

                    window.myDoughnut.update();

                    $('.wrap-dial-step').find('.pie_value').text(val1);

                    checkFinalCounter(val1);
                }
            }

            function checkFinalCounter (val1) {
                if (val1 == 100) {
                    $('.wrap-dial-step').find('.sub-stt-step').removeClass('hide');
                } else {
                    $('.wrap-dial-step').find('.sub-stt-step').addClass('hide');
                }
            }

            // event to Test when value change
            $(".click-test").bind('click', function(e) {
                e.preventDefault();
                updatePercentStep(75, 25);    
            });     
        },
        
        init: function () {
            // this.initDialStep();
            this.initDoughnutStep();
        }

    };

}();

jQuery(document).ready(function() {    
   ProfileComponents.init(); 
});

(function($){
    
    if($('#custom-help-message').length > 0){
        countText('#help-block',$('#custom-help-message').val());   
    }
    
    if($('#custom-stop-message').length > 0){
        countText('#stop-block',$('#custom-stop-message').val());
    }

    $("#sms_number").mask("(000) 000-0000");
    $("#OrganizationPhone_vch").mask("(000) 000-0000");

    var check_update = 0;

    var hasUploadLogo = 0;

    $(document).ready(function($) {
        $('.radio-sendtype').each(function(){
            if ($(this).is(':checked')){
                $('#sendType').val($(this).val());
            }
        });
    });

    $("#add_security_question_form").validationEngine({promptPosition : "topLeft", scroll: false, focusFirstField : true, onFailure: function(){
        // window.scrollTop();
    }});

    $("#my_account_form").validationEngine({promptPosition : "topLeft", scroll: false, focusFirstField : true, onFailure: function(){
        // window.scrollTop();
    }});

    $("#btn_save_my_account").click(updatemyaccountinfo);
    $("#my_account_form").submit(updatemyaccountinfo);

    $("#select_question_1").on('focus click', function(){
        $('#input_answer_1').validationEngine('hide');
    });

    $("#select_question_2").on('focus click', function(){
        $('#input_answer_2').validationEngine('hide');
    });

    $("#select_question_3").on('focus click', function(){
        $('#input_answer_3').validationEngine('hide');
    });

    $("#btn_update_security_question").click(addSecurityQuestion);

    $("#add_security_question_form").submit(addSecurityQuestion);

    $("#edit_security_question_form").submit(editSecurityQuestion);

    var fields = $('#my_account_form input, #my_account_form select');
    
    fields.focus(function(){
        $(this).validationEngine('hide');
        var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
        setTimeout(function(){
            nextField.validationEngine('hide');
        },1);
    });

    $('#SecurityQuestionEnabled').change(function() {
        return false;
        if($(this).is(":checked")) {
           var check_value = 1
           //$('.link-to-edit').show();
        }
        else{
            var check_value = 0
            //$('.link-to-edit').hide();
        }

        try{
            $.ajax({
            type: "POST",
            url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateSecurityQuestionStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
            dataType: 'json',
            data: {SecurityQuestionEnabled:check_value},
            beforeSend: function( xhr ) {
                $('#processingPayment').show();
            },                    
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //bootbox.alert('Update Fail', function() {});
                //$("#enable_security_question_form")[0].reset();
                $('#processingPayment').hide();
                bootbox.dialog({
                    message: "Update Fail",
                    title: "My Account",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                });
            },                    
            success:        
                function(d) {
                    $('#processingPayment').hide();
                    $('.btn-group-answer-security-question').show();
                    
                    bootbox.dialog({
                        message: d.MESSAGE,
                        title: "My Account",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });

                    $(window).unbind('beforeunload');
                }       
            });
        }catch(ex){
            $('#processingPayment').hide();
            bootbox.dialog({
                message:'Update Fail',
                title: "My Account",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
            $('.btn-group-answer-security-question').show();
            //$("#enable_security_question_form")[0].reset();           
        }

    });

    $(".select_question").on('change', function() {
        var old_selected = $(this).data('selected');
        var input_answer =  $(this).data('input-answer');
        if ($(this).val() == old_selected){
            check_update = false;
            $('#'+input_answer).removeClass('validate[required]');
        } else {
            check_update = true;
            // add validate for input box
            $('#'+input_answer).addClass('validate[required]');
        }
    });

    $('.input_answer').blur(function(){
        if( $(this).val().length > 0 ) {
            check_update = true;
        }
        else
            check_update = false;
    });


    function addSecurityQuestion(event){
        event.preventDefault();
        
        if ($('#add_security_question_form').validationEngine('validate', {focusFirstField : true} )) {

            try{
                $.ajax({
                type: "POST",
                url: '/session/sire/models/cfc/myaccountinfo.cfc?method=AddUserSecurityQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                data: $('#add_security_question_form').serialize(),
                beforeSend: function( xhr ) {
                    $('#processingPayment').show();
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message:'Update Fail',
                        title: "My Account",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                },                    
                success:        
                    function(d) {
                        $('#processingPayment').hide();
                        $('.btn-group-answer-security-question').show();
                        if(d.RESULT == "SUCCESS"){
                            //goto success page
                            /*bootbox.alert(d.MESSAGE, function() {
                                window.location.href='/session/sire/pages/my-account';
                            });*/
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {window.location.href='/session/sire/pages/my-account';}
                                    }
                                }
                            });

                            $(window).unbind('beforeunload');
                        }
                        else{
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                    }       
                });
            }catch(ex){
                $('#processingPayment').hide();
                bootbox.dialog({
                    message:'Update Fail',
                    title: "My Account",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                }); 
                $('.btn-group-answer-security-question').show();            
            }
        }
    }

    function editSecurityQuestion(event){
        event.preventDefault();

        if(!check_update){
            //bootbox.alert('Nothing changed.', function() {});
            bootbox.dialog({
                message:'Nothing changed.',
                title: "My Account",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            }); 
            return false;
        }
        else{
            $("#edit_security_question_form").validationEngine('attach',{'binded':false});  
        }


        if ($('#edit_security_question_form').validationEngine('validate')) {
            try{
                $.ajax({
                type: "POST",
                url: '/session/sire/models/cfc/myaccountinfo.cfc?method=EditUserSecurityQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                data: $('#edit_security_question_form').serialize(),
                beforeSend: function( xhr ) {
                    $('#processingPayment').show();
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message:'Update Fail',
                        title: "My Account",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                },                    
                success:        
                    function(d) {
                        $('#processingPayment').hide();
                        $('.btn-group-answer-security-question').show();
                        if(d.RESULT == "SUCCESS"){
                            //goto success page
                            /*bootbox.alert(d.MESSAGE, function() {
                                window.location.href='/session/sire/pages/my-account';
                            });*/
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {window.location.href='/session/sire/pages/my-account';}
                                    }
                                }
                            });

                            $(window).unbind('beforeunload');
                        }
                        else{
                            //bootbox.alert(d.MESSAGE, function() {});  
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                    }       
                });
            }catch(ex){
                $('#processingPayment').hide();
                bootbox.dialog({
                    message:'Update Fail',
                    title: "My Account",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                }); 
                $('.btn-group-answer-security-question').show();            
            }
        }
    }

    function updatemyaccountinfo(event){
        event.preventDefault();
        var helpmess=$("#custom-help-message").val().trim();
        var stopmess=$("#custom-stop-message").val().trim();    

        if((helpmess.trim().length < 10 && helpmess.trim().length >0 ) || (stopmess.trim().length < 10 && stopmess.trim().length > 0))
        {
            alertBox("The Help and Stop message must be of minimum length 10 characters. You can leave it blank, default messages will be used.","OOPS!","");
            return false;
        }
                
        var element = $('#time-zone').find('option:selected'); 
        var timeZone = element.attr("timeZoneId");

        $('#TimezoneId_int').val(timeZone);
        if ($('#my_account_form').validationEngine('validate')) {
            try{

                console.log($('#my_account_form').serialize());
                $.ajax({
                type: "POST",
                url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateUserSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                data: $('#my_account_form').serialize(),
                beforeSend: function( xhr ) {
                    $('.btn').prop('disabled',true);
                    $('#processingPayment').show();
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //bootbox.alert('Update Fail', function() {});
                    $('.btn').prop('disabled',false);
                    $('#processingPayment').hide();

                    alertBox('Unable to update your profile settings at this time. An error occurred.', 'My Profile'); 
                },                    
                success:        
                    function(d) {
                        //$('#btn_save_my_account').prop('disabled',false);
                        $('.btn').prop('disabled',false);
                        $('#processingPayment').hide();

                        if(d.RESULT == "SUCCESS"){
                            //goto success page
                            /*bootbox.alert(d.MESSAGE, function() {
                                window.location.href='/session/sire/pages/my-account';
                            });*/

                            alertBox(d.MESSAGE, 'My Profile',function(){
                                window.location.href='/session/sire/pages/profile';
                            });  

                            $(window).unbind('beforeunload');
                        }
                        else{
                            //bootbox.alert(d.MESSAGE, function() {});  
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Profile",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium green-gd",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                    }       
                });
            }catch(ex){
                //bootbox.alert('Update Fail', function() {});
                $('.btn').prop('disabled',false);
                $('#processingPayment').hide();
                alertBox('Unable to update your profile settings at this time. An error occurred.', 'My Profile');
            }
        }
        
    }

var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/session/sire/models/cfc/upload.cfc?method=uploadImg&queryformat=column&_cf_nodebug=true&_cf_nocache=true", // Set the url
    paramName: "image",
    thumbnailWidth: 200,
    thumbnailHeight: 200,
    parallelUploads: 1,
    maxFilesize: 10,
    filesizeBase: 1024,
    previewTemplate: previewTemplate,
    autoQueue: true, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
    acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg",
    maxFiles: 1,
    init: function() {
      this.on('addedfile', function(file) {
        if (this.files.length > 1) {
          this.removeFile(this.files[0]);
        }
      });
    }
});

myDropzone.on("sending", function(file) {
    $('#processingPayment').show();
});

myDropzone.on("success", function (file, result) {
    $('#processingPayment').hide();
    myDropzone.removeFile(file);
    var obj = jQuery.parseJSON(result);
    if(obj.RESULT == 'SUCCESS'){
        $('#OrganizationLogo_vch').val(obj.IMAGE);
        $('#OrgLogo').attr('src',obj.IMAGE_URL);
        $('#OrgLogo').show();
        $('#OrgLogo').parent().show();
        $('#UpdateOrganizationLogo').val(1);
        $('#removeLogo').show();
        $('.OrgLogo-wrapper').show();
        hasUploadLogo = 1;
    }
    else{
        bootbox.dialog({
            message:obj.MESSAGE,
            title: "My Account",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        });
    }
});
    

    $('#removeLogo').on('click', function(e){
        e.preventDefault();

        confirmBox('Are you sure you want to remove your organization photo?',"My Profile", function(){
            $('#OrgLogo').attr('src','');
            $(this).hide();
            //removeLogo();
            $('#UpdateOrganizationLogo').val(1);
            $('#OrganizationLogo_vch').val('');
            $('.OrgLogo-wrapper').hide();
            $('#removeLogo').hide();
        }); 
        
    })

    function removeLogo(){
        var logo_name = $('#OrganizationLogo_vch').val();
        if(logo_name != '')
        {
            try{
                $.ajax({
                type: "POST",
                url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateOrgLogo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                data: {'action':'remove','logo_name':logo_name},
                beforeSend: function( xhr ) {
                    $('.btn').prop('disabled',true);
                    $('#processingPayment').show();
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //bootbox.alert('Update Fail', function() {});
                    $('.btn').prop('disabled',false);
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message:"Unable to remove your organization logo at this time. An error occurred.",
                        title: "My Account",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                },                    
                success:        
                    function(d) {
                        $('.btn').prop('disabled',false);
                        $('#processingPayment').hide();
                        console.log(d);
                        if(d.RXRESULTCODE == 1){
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                        else{
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "My Account",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                    }       
                });
            }catch(ex){
                $('.btn').prop('disabled',false);
                $('#processingPayment').hide();
                bootbox.dialog({
                    message:'Unable to remove your organization logo at this time. An error occurred.',
                    title: "My Account",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                }); 
            }
        }
    }

    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }

    function isImage(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'bmp':
            case 'png':
                //etc
                return true;
        }
        return false;
    }

    function GetFileSize(fileid) {
         try {
         var fileSize = 0;
         //for IE
         if ($.browser.msie) {
         //before making an object of ActiveXObject, 
         //please make sure ActiveX is enabled in your IE browser
         var objFSO = new ActiveXObject("Scripting.FileSystemObject"); 
         var filePath = $("#" + fileid)[0].value;
         
         var objFile = objFSO.getFile(filePath);
         var fileSize = objFile.size; //size in kb
         fileSize = fileSize / 1048576; //size in mb 
         }
         //for FF, Safari, Opeara and Others
         else {
         fileSize = $("#" + fileid)[0].files[0].size //size in kb
        
         //fileSize = fileSize / 1048576; //size in mb 
         fileSize = fileSize / 1000000;
         }
           //alert("Uploaded File Size is" + fileSize + "MB");
           return fileSize;
         }
         catch (e) {
          alert("Error is :" + e);
          return 100;
         }
    }

    var LIMIT_MESSAGE = 160;
    
    if($('#custom-help-message').length > 0){
        
        $("#custom-help-message").bind('keyup change', function(e) {
            total_primary_character = $("#custom-help-message").val().length;
            //characters_available(LIMIT_MESSAGE, total_primary_character, 'help-block');
            countText('#help-block',$('#custom-help-message').val());
        });
    }

       if($('#custom-stop-message').length > 0){
        
        $("#custom-stop-message").bind('keyup change', function(e) {
            total_primary_character = $("#custom-stop-message").val().length;
            //characters_available(LIMIT_MESSAGE, total_primary_character, 'stop-block');
            countText('#stop-block',$('#custom-stop-message').val());
        });
    }         

     function characters_available (limit, total_character, help_class) {
        if (limit - total_character < 0 ) {
            $('#'+help_class).text((total_character - limit) + ' characters over');
            $('#'+help_class).addClass('text-danger');
        } else  {
            $('#'+help_class).text((limit - total_character) + '/' + limit +' characters available');
            $('#'+help_class).removeClass('text-danger');
        } 
    }

    $.ajax({
        url: '/session/sire/models/cfc/myaccountinfo.cfc?method=GetUserOrganization&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        type: 'POST',
        dataType: 'json',
        data: {},
        async: false,
        success: function(data){
            $('#time-zone > option').each(function(){
                if(parseInt($(this).attr('timeZoneId')) == parseInt(data.ORGINFO.TIMEZONEID_INT)){
                    $(this).prop('selected', true);
                }
            });
        }
    });
    
})(jQuery);

function isValidUrl(field, rules, i, options){
    if(/(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/.test(field.val()) === false){
        return "Invalid URL!";
    }
}

// js for progress bar in organization information
$(document).ready(function(){
    $( "#ProfileProgressBar" ).progressbar({
        value: parseInt($('input[name="progress-bar-percent"]').val()) 
    });

  $("body").on('change keyup', 'textarea, input', function(event) {
        event.preventDefault();
        $(window).bind('beforeunload', function() {
            return 'You have unsaved changes on this page.';
        });

    });

    $('img#OrgLogo').on('load', function (event) {

        event.preventDefault();
        $(window).bind('beforeunload', function() {
            return 'You have unsaved changes on this page.';
        });
        console.log($('img#OrgLogo').attr('src'));
    });

});


function hiddenImage(){
    $('.OrgLogo-wrapper').css({'display': 'none'});
    $('input[name="OrganizationLogo_vch"]').val('');
}




