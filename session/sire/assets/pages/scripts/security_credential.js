(function($){
var _tblListCredentials;
var ajaxResQueryString = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

//init datatable for active agent
function InitGroupCredentials(customFilterObj) {
    var customFilterData = typeof (customFilterObj) != 'undefined' ? JSON.stringify(customFilterObj) : "";

    var noCol = function(object){
        return object[0];
    };

    var statusCol = function(object){
        var strReturn = "";
        if(object.STATUS == 1){
            strReturn = $('<div><span class="stt-active">Active</span></div>').addClass('sc-action').append($('<a href="#"></a>').attr('data-access-key', object.ACCESSKEY).addClass('btn-action deactive btn-sin').html("Make Inactive"))[0].outerHTML;
        } else {
            strReturn = $('<div><span class="stt-active">Inactive</span></div>').addClass('sc-action').append($('<a href="#"></a>').attr('data-access-key', object.ACCESSKEY).addClass('btn-action active btn-sin').text("Active")).append($('<a href="#"></a>').attr('data-access-key', object.ACCESSKEY).addClass('btn-action delete btn-sin').html("Delete"))[0].outerHTML;
        }
        return strReturn;
    };

    var secretCol = function(object){
        var strReturn = '<p>'+object.SECRETKEY+'</p>';
            strReturn+= '<p><strong>URI/URL Endcode: </strong>'+encodeURIComponent(object.SECRETKEY)+'</p>'
        return strReturn;
    };

    _tblListCredentials = $('#tblListCredentials').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bServerSide": true,
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bLengthChange": false,
        "sPaginationType": "input",
        "bAutoWidth": false,
        "oLanguage": {"sZeroRecords": "", "sEmptyTable": "Click 'CREATE NEW ACCESS KEY' to enable your API access."},
        "aoColumns": [
            {"mData": "NO", "sName": 'No', "sTitle": 'No.', "bSortable": false, "sClass": "", "sWidth": "80px"},
            {"mData": "CREATED", "sName": 'Created', "sTitle": 'Created Date', "bSortable": false},
            {"mData": "ACCESSKEY", "sName": 'AccessKey', "sTitle": 'Access Key ID', "bSortable": false},
            {"mData": secretCol, "sName": 'SecretKey', "sTitle": 'Secret Access Key', "bSortable": false,"sWidth": "480px"},  
            {"mData": statusCol, "sName": 'Active', "sTitle": 'Status', "bSortable": false}
        ],
        "sAjaxSource": '/session/sire/models/cfc/security-credential.cfc?method=GetCredentialsListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        "fnServerParams": function ( aoData ) {
         aoData.push( { "name": "No", "value": "4" });
        },   
        "fnServerData": function(sSource, aoData, fnCallback) {
            
            $.ajax({
                dataType: 'json',
                type: "POST",
                url: sSource,
                data: aoData,
                success: function(data){
                    var createBtn = $('button.btn-new-access-key');
                    if(data.ALLOWCREATE === false){
                        createBtn.prop('disabled', true);
                    } else {
                        createBtn.prop('disabled', false);
                    }
                    if(data.iTotalRecords==0)
                    {
                        fnCallback(data);
                    }
                    else
                    {
                        fnCallback(data);
                    }                    
                }
            });
        },
        "fnInitComplete": function(oSettings, json) {
            //in order to prevent this talbe from collapsing, we must fix its min width 
            $('#tblListCredentials').attr('style', '');
            $('#tblListCredentials').css('min-width', '1053px');
        }
    });
}

InitGroupCredentials();


function createAccessKey() {
    var cango=true;
    // check check permission
    $.ajax({
        url: '/public/sire/models/users.cfc?method=GetUserInfor'+ajaxResQueryString,
        type: 'POST',
        dataType: 'json',
        async:false,
        data: {inpiduser: UserIDLogin},
        beforeSend: function () {
            $('#processingPayment').show();
        }
    })
    .done(function(data) {        
        if (parseInt(data.RXRESULTCODE) == 1) {
            if(data.SECURITYCREDENTIAL==0)
            {
                alertBox('The use of this feature requires additional permissions. In order to be granted authorization, please contact <a data-toggle="modal" data-target="#md-contact-support" data-dismiss="modal">customer support</a> and request access to this feature.','Oops!');
                $('#processingPayment').hide();
                cango=false;
            }
        } else {
            alertBox(data.MESSAGE, 'Oops!');
        }
    })
    .fail(function(e) {
        bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
    })
    .always(function() {
        $('#processingPayment').hide();
    });    
    if(!cango)
    {
        return false;
    }
    // end check permission
    //start check help stop messsage
    $.ajax({
        url: '/session/sire/models/cfc/myaccountinfo.cfc?method=CheckHelpStopSetup'+ajaxResQueryString,
        type: 'POST',
        dataType: 'json',
        async:false,
        data: {},
        beforeSend: function () {
            $('#processingPayment').show();
        }
    })
    .done(function(data) {        
        if (parseInt(data.RXRESULTCODE) == 1) {
            if(data.NOTSETUP==1)
            {
                alertBox('A compteled \'My Account\' <a href="/session/sire/pages/profile" target="_blank">profile</a> is a prerequisite to use Security Credentials. Please fill out your <a href="/session/sire/pages/profile" target="_blank">profile</a>, including your Company Name, Help and Stop message in order to continue.','Oops!');
                $('#processingPayment').hide();
                cango=false;
            }
        } else {
            alertBox(data.MESSAGE, 'Oops!');
        }
    })
    .fail(function(e) {
        bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
    })
    .always(function() {
        $('#processingPayment').hide();
    });    
    if(!cango)
    {
        return false;
    }
    //end check help stop message
    confirmBox('Are you sure you want to create a new access key?', 'Create Access Key',function(){
        $.ajax({
            url: '/session/cfc/administrator/credential.cfc?method=createAccesskey'+ajaxResQueryString,
            type: 'post',
            dataType: 'json',
            beforeSend: function(){
                $('#processingPayment').show();
            },
            success: function(d) {
                $('#processingPayment').hide();
                if (d.SUCCESS == 1) {
                    InitGroupCredentials();
                }
            }
        });
    });
    
}

$('button.btn-new-access-key').click(function(e){
    if($(this).prop('disabled')){
        return false;
    }
    createAccessKey();
});


$('body table').on('click', 'a.active', function(e){
    var accessKey = $(this).data('access-key');
    makeActiveInactiveDialog(accessKey, 1);
});

$('body table').on('click', 'a.deactive', function(e){
    var accessKey = $(this).data('access-key');
    makeActiveInactiveDialog(accessKey, 0);
});

$('body table').on('click', 'a.delete', function(e){
    var accessKey = $(this).data('access-key');
    deleteAccessKey(accessKey);
});



function makeActiveInactiveDialog(accessKey, activeInactive) {
    confirmBox('Are you sure you would like to make <span class="active">' + accessKey + '</span> ' + (activeInactive == 0 ? 'inactive?' : 'active?'), 'Access key' ,function(){
        $.ajax({
            url: '/session/cfc/administrator/credential.cfc?method=activeInactiveAccessKey'+ajaxResQueryString,
            type: 'post',
            dataType: 'json',
            data: {
                accessKey: accessKey,
                activeInactive: activeInactive
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            success: function(d) {
                $('#processingPayment').hide();
                InitGroupCredentials();
            }
        });
    });
}



function deleteAccessKey(accessKey) {
    confirmBox('Are you sure delete Access key with ID <span class="active">' + accessKey + '</span>? This action is permanent and cannot be undone.', 'Delete Security Credential', function(){
        $.ajax({
            url: '/session/cfc/administrator/credential.cfc?method=deleteAccessKey'+ajaxResQueryString,
            type: 'post',
            dataType: 'json',
            data: {
                accessKey: accessKey
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.SUCCESS == 1) {
                    InitGroupCredentials();
                } else {
                    jAlert(data.MESSAGE);
                }
            }
        });
    });
}


$('form[name="change-password"]').validationEngine('attach', {promptPosition : "topLeft", autoHidePrompt: true, autoHideDelay: 5000, scroll: false}).on('submit', function(event){
    event.preventDefault();

    if($(this).validationEngine('validate')){
        var oldPassword = $('input[name="old-password"]').val(), 
        newPassword = $('input[name="new-password"]').val(), 
        reNewPassword = $('input[name="renew-password"]').val();


        confirmBox('Are you sure you want to change current password?', 'Change password', function(){
            $.ajax({
                method: 'POST',
                url: "/session/cfc/administrator/userstool.cfc?method=changePassword"+ajaxResQueryString,
                data: {currentPassword: oldPassword, password: newPassword, confirmPassword: reNewPassword},
                beforeSend: function( xhr ) {
                    
                },  
                success: function(DATA) {

                    console.log("Change password");
                    if(DATA.SUCCESS){
                        alertBox(DATA.MESSAGE, "Change password", function(){ window.location.reload() });
                    } else {
                        alertBox(DATA.MESSAGE);
                    }
                }
            });
        });

    }
});

// Update security question
$('form[name="frm-security-questions"]').validationEngine('attach', {promptPosition : "topLeft", autoHidePrompt: true, autoHideDelay: 5000, scroll: false}).on('submit', function(event){
    event.preventDefault();
    if($(this).validationEngine('validate')){
        var dataPost = $(this).serialize();

        confirmBox("Are you sure you want to update AQ security?", "Confirmation", function(){
            $.ajax({
                url: "/session/sire/models/cfc/myaccountinfo.cfc?method=EditUserSecurityQuestion"+ajaxResQueryString,
                type: "POST",
                dataType: "json",
                data: dataPost,
                success: function(data){
                    if(data.RESULT == "SUCCESS"){
                        alertBox(data.MESSAGE, 'Update security questions')
                    }
                    $("#inp_simon_1").val("******");
                    $("#inp_simon_2").val("******");
                    $("#inp_simon_3").val("******");
                }
            });
        });
    }
});

// Add security question
$('form[name="add_security_question_form"]').validationEngine('attach', {promptPosition : "topLeft", autoHidePrompt: true, autoHideDelay: 5000, scroll: false}).on('submit', function(event){
    event.preventDefault();
    if($(this).validationEngine('validate')){
        var dataPost = $(this).serialize();

        confirmBox("Are you sure you want to add AQ security?", "Confirmation", function(){
            $.ajax({
                url: "/session/sire/models/cfc/myaccountinfo.cfc?method=AddUserSecurityQuestion"+ajaxResQueryString,
                type: "POST",
                dataType: "json",
                data: dataPost,
                success: function(data){
                    if(data.RESULT == "SUCCESS"){
                        alertBox(data.MESSAGE, 'Create successfully');
                        location.href = "/session/sire/pages/account";
                    }
                }
            });
        });
    }
});



})(jQuery);

