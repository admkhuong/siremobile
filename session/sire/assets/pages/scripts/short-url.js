(function($){


	function InitControl(customFilterObj)
	{//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent



		var nameCol = function(object){
			return object.DESC;
		}

		var actionCol = function(object){
			var strReturn = $('<a href="short-url-add?inpPK='+object.ID+'"></a>').attr('data-toggle', "modal")
												 .attr('data-surl-action', 'edit')
												 .attr('data-target', "#AddShortURLModal")
												 .addClass('btn green-gd simon-gd btn-edit btn-report btn-actions')
												 .html("<i class='glyphicon glyphicon-edit'></i>")[0].outerHTML;
				strReturn+= $('<a href=""></a>').attr('data-toggle', "modal")
												 .attr('data-surl-id', object.ID)
												 .attr('data-surl-action', 'delete')
												 .addClass('btn green-cancel simon-cancel btn-delete-surl btn-report btn-actions')
												 .html("<i class='fa fa-times' aria-hidden='true'></i>")[0].outerHTML;
			return strReturn;
		}
		var shortURLCol = function(object){
			return $('<a href="'+object.SHORTURL+'"></a>').attr('target', "_blank").text(object.SHORTURL)[0].outerHTML;
		}

		var actualURLCol = function(object){
			return $('<a href="'+object.TARGETURL+'"></a>').attr('target', "_blank").text(object.TARGETURL)[0].outerHTML;
		}


// <a class="btn green-gd btn-edit btn-report btn-actions" data-toggle="modal"  data-surl-action="edit" href="short-url-add?inpPK=#GetShortURLList.PKId_bi#" data-target="##AddShortURLModal">Edit</a>
// <button class="btn green-cancel btn-delete-surl btn-report btn-actions" data-surl-action="delete" data-surl-name='#GetShortURLList.Desc_vch#' data-surl-id='#GetShortURLList.PKId_bi#'>Delete</button>



		var table = $('#tblListShortURL').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"responsive": true,
		    "aoColumns": [
				{"mData": nameCol,"sName": 'Short URL Data', "sTitle": 'Name',"bSortable": false,"sClass":"surl-name", "sWidth":"200px"},
				{"mData": shortURLCol,"sName": 'ShortUrl', "sTitle": 'Short URL',"bSortable": false,"sClass":"short-url", "sWidth": "400px"},			
				{"mData": actualURLCol,"sName": 'Clicks', "sTitle": 'Actual',"bSortable": false,"sClass":"actual","sWidth": "380px"},
				{"mData": "CLICKCOUNT","sName": 'Clicks', "sTitle": 'Clicks',"bSortable": false,"sClass":"click-count","sWidth": "130px"},
				{"mData": actionCol,"sName": 'Actions', "sTitle": 'Actions',"bSortable": false,"sClass":"surl-action","sWidth": "150px"}
			],
			"sAjaxDataProp": "ListShortURLData",

			"sAjaxSource": '/session/sire/models/cfc/urltools.cfc?method=GetShortURLList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnDrawCallback": function(oSettings){

				//hide paging when *No data available in table*
				if(oSettings._iRecordsTotal == 0){
					this.parent().find('.dataTables_paginate').hide();
				} else {
					this.parent().find('.dataTables_paginate').show();
				}
			},
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );

		        $.ajax({dataType: 'json',
		                type: "POST",
		                url: sSource,
			            data: aoData,
			            success: function(data){

			             	fnCallback(data);
			             	//set button disable status by reponse data from server
			             	if(data.LOCKADDBUTTON === true){
			             		$('a.add-shorturl-simon-button[href="short-url-add"]').attr('disabled', 'disabled');
			             		$('p.warning-text-simon').removeClass("hidden");
			             	} else {
			             		$('a.add-shorturl-simon-button[href="short-url-add"]').removeAttr('disabled');
			             		$('p.warning-text-simon').addClass("hidden");
			             	}

			            } 
			    });
	        },
			"fnInitComplete":function(oSettings, json){

			}
	    });
	}

	InitControl();





	// Delete Short URL 
	$( document ).on( "click", ".btn-delete-surl", function(event) {
		event.preventDefault();
		
		var inpPKId = parseInt($(this).data('surl-id'));
		var currentPage = $('input.paginate_text').val();

		var dialogText = 'Are you sure you want to delete this short URL?';		

		
		try{

			$('.btn').prop('disabled',true);
			bootbox.dialog({
	        message: dialogText,
	        title: 'Delete Short URL',
	        className: "",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "btn btn-medium green-gd",
	                callback: function(result) {
	                    if(result){
							$.ajax(
							{
								type: "POST",
								url: '/session/sire/models/cfc/urltools.cfc?method=DeleteShortURL&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
								dataType: 'json',
								data: { inpPKId: inpPKId },
								beforeSend: function( xhr ) {
									$('.tblListShortURL_processing').show();
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
									    message: "There is an error.Please try again later.",
									    title: "Delete Short URL Error",
									    buttons: {
									        success: {
									            label: "OK",
									            className: "btn btn-medium green-gd",
									            callback: function() {}
									        }
									    }
									});
									$('.btn').prop('disabled',false);
									$('.tblListShortURL_processing').hide();
								},					  
								success:		
									function(d) {
										$('#processingPayment').hide();
										$('.btn').prop('disabled',false);
										$('.tblListShortURL_processing').hide();
										if(d.RXRESULTCODE == 1){
											//goto success page
											bootbox.dialog({
											    message: d.MESSAGE,
											    title: "Delete Short URL",
											    buttons: {
											        success: {
											            label: "OK",
											            className: "btn btn-medium green-gd",
											            callback: function() {}
											        }
											    }
											});
											var oTable = $('#tblListShortURL').dataTable();
											//oTable.fnDraw();
											oTable.fnPageChange(parseInt(currentPage-1));
											
																					
											return;
										}
										else{
											bootbox.dialog({
											    message: "There is an error.Please try again later.",
											    title: "Delete Short URL - Error",
											    buttons: {
											        success: {
											            label: "OK",
											            className: "btn btn-medium green-gd",
											            callback: function() {}
											        }
											    }
											});
										}
									} 		
								});
							}
		                }
		            },
		            cancel: {
		                label: "Cancel",
		                className: "btn btn-medium green-cancel",
		                callback: function() {
						$('.btn').prop('disabled',false);
		                }
		            },
	        	}
	    	});
			
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.dialog({
			    message: "There is an error.Please try again later.",
				title: "Delete Short URL - Error",
			    buttons: {
			        success: {
			            label: "OK",
			            className: "btn btn-medium green-gd",
			            callback: function() {}
			        }
			    }
			});
			$('.btn').prop('disabled',false);
			$('.tblListShortURL_processing').hide();
		}
				
	});
	

	$(function(){
		// Prevent vent if button has been disabled
		var allowedClick = true;
		$('a.add-shorturl-simon-button').click(function(event){

			setTimeout(function(){
				allowedClick = true;
			}, 1000);
			//prevent double click
			if(!allowedClick){
				return false;
			}

			if($(this).attr('disabled') == 'disabled'){
				return false;	
			}

			allowedClick = false;
		});
	});


})(jQuery);