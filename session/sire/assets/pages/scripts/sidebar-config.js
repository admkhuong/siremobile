var SidebarConfigs = function () {
	return {
		handleSidebar: function () {
			$(window).bind('load resize', function() {
				var topOffset = 130;
				var bottomOffset = 95;

				var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;

				var height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;

				height = height - (topOffset + bottomOffset);

				if (width < 992) {
					$('.page-sidebar-wrapper').removeClass('sidebar-fixed');
					$('.page-sidebar-wrapper').removeAttr('style');
					$('.handle-scroll').height('auto');
					$('.handle-scroll').mCustomScrollbar('destroy');
				} else {
					$('.page-sidebar-wrapper').addClass('sidebar-fixed');
					$('.sidebar-fixed').height(height);
					$('.handle-scroll').height(height);
					$('.handle-scroll').mCustomScrollbar({
						theme: 'dark-2',
						scrollbarPosition: 'outside',
						snapOffset: 100
					});
				};

			});
		},

		init: function () {
			this.handleSidebar();
		}
	}
}();

jQuery(document).ready(function() {    
   SidebarConfigs.init(); 
});