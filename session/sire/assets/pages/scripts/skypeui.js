var skypeComponent = function () {
	return {
		handleCurrentNumberScroll: function () {
			$('#wrap-new-curent').mCustomScrollbar({
				theme: 'my-theme',
				scrollbarPosition: 'outside'
			});
		},
		handleScreenScroll: function () {
			$('.screen-non-ip').mCustomScrollbar({
				theme: 'my-theme',
				scrollbarPosition: 'inside'
			});
		},
		init: function () {
			this.handleCurrentNumberScroll();
			this.handleScreenScroll();
		}
	}
}();

$(document).ready(function() {
	skypeComponent.init();
});