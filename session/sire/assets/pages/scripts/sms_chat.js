(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    /*Keyword list datatable*/
    fnGetDataList = function (keyword){
        $("#keywordlist").html('');

        var showKeyword = function (object) {
            return "<a href='/session/sire/pages/sms-response?keyword="+object.Keyword_vch+"'>"+object.Keyword_vch+"</a>";
        }

        var actionBtn = function (object) {
            var strReturn = $('<a href="/session/sire/pages/sms-chat-new?keyword='+object.Keyword_vch+'" title="Setting"><i class="fa fa-cog" style="font-size:15px" aria-hidden="true"></i></a>')
                                .attr('data-batchid', object.BatchId_bi)[0].outerHTML;
            return strReturn;
        }

        var table = $('#keywordlist').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": showKeyword, "sName": 'Keyword', "sTitle": 'Keyword', "bSortable": true, "sClass": "", "sWidth": "200px"},
                {"mData": "Created_dt", "sName": 'Created', "sTitle": 'Created Date', "bSortable": true, "sClass": "", "sWidth": "150px"},
                {"mData": "KeywordStatusActive", "sName": 'Active', "sTitle": 'Active', "bSortable": true, "sClass": "col-center", "sWidth": "100px"},
                {"mData": "KeywordStatusClosed", "sName": 'Closed', "sTitle": 'Closed', "bSortable": true, "sClass": "col-center", "sWidth": "100px"},
                {"mData": "KeywordTotal", "sName": 'Total', "sTitle": 'Total', "bSortable": true, "sClass": "col-center", "sWidth": "100px"},
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": true, "sClass": "col-center", "sWidth": "100px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/smschat.cfc?method=getListKeyword'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpKeyword", "value": keyword}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "fnHeaderCallback": function( thead, data, start, end, display, object) {
                $(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
            },
        });
    }

    $("#showModal").click(function(event) {
        $("#Keyword").val('');
        $("#KeywordStatus").html('');
        $("#save-keyword").addClass('hidden');
        $('input[type="checkbox"]').prop('checked', false);
        $('textarea').val('');
    });

    $("#Keyword").keydown(function(event) {
        $("#save-keyword").addClass('hidden');
    });

    // Validate keyword in real time - ajax with warnings
    $("#Keyword").delayOn("input", 800, function(element, event) {
        if ($('#keyword-form').validationEngine('validate', {scroll: false, focusFirstField : false})) {
            $('#KeywordStatus').removeClass('has-error');       
            $('#KeywordStatus').removeClass('not-has-error');
            if(element.value.length == 0)
            {
                // Give blank to erase warning if one was previously set
                if( $('#KeywordDisplayValue').html.length > 0)
                {
                    //$('#KeywordStatus').css('color', 'red');
                    $('#KeywordStatus').addClass('not-has-error');
                    $('#KeywordStatus').html('');
                    $("#save-keyword").addClass('hidden');
                    //$('#KeywordStatus').html("Save blank to erase existing Keyword if any.");
                }
                else
                {
                    //$('#KeywordStatus').css('color', 'red');
                    $('#KeywordStatus').addClass('has-error');
                    $('#KeywordStatus').html('');
                    $("#save-keyword").addClass('hidden');
                }
            }           
            else
            {
                $('#KeywordStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');
                
                /*<!--- Save Custom Responses - validation on server side - will return error message if not valid --->*/               
                $.ajax({
                    type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
                    url: '/session/sire/models/cfc/control-point.cfc?method=ValidateKeywordWithoutBatchId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                    dataType: 'json',
                    async: false,
                    data:  
                    {
                        inpKeyword : element.value,
                        inpShortCode : $("#ShortCode").val()
                        
                    },                    
                    error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
                        alertBox("Error. No Response from the remote server. Check your connection and try again.");
                    },                    
                    success:        
                    /*<!--- Default return function for Async call back --->*/
                    function(d) 
                    {
                        
                        /*<!--- RXRESULTCODE is 1 if everything is OK --->*/
                        if (parseInt(d.RXRESULTCODE) == 1) 
                        {   
                            //$('#KeywordStatus').css('color', 'green');
                            $('#KeywordStatus').addClass('not-has-error');
                            $('#KeywordStatus').html(d.MESSAGE);

                            $("#save-keyword").removeClass('hidden');
                        }
                        else
                        {
                            /*<!--- No result returned ---> */
                            if(d.ERRMESSAGE != "")
                            {   
                                //$('#KeywordStatus').css('color', 'red');
                                $('#KeywordStatus').addClass('has-error');
                                $('#KeywordStatus').html(d.ERRMESSAGE);
                                $("#save-keyword").addClass('hidden');
                            }
                        }
                        $('#KeywordStatus').show();                                     
                    }
                });
            }
        } else {
            $('#KeywordStatus').html('');
        }
    });

    $("#save-keyword").click(function(event) {
        $("#save-keyword").addClass('hidden');
        var keyword = $("#Keyword").val();
        var sendEmailStart = $("#SendEmailStart").is(':checked');
        var sendEmailReceived = $("#SendEmailReceived").is(':checked');
        var emailList = $("#EmailList").val();
        var sendSMSStart = $("#SendSMSStart").is(':checked');
        var sendSMSReceived = $("#SendSMSReceived").is(':checked');
        var SMSList = $("#SMSList").val();
        if (keyword.length > 0) {
            $.ajax({
                url: '/session/sire/models/cfc/smschat.cfc?method=createKeword' + strAjaxQuery,
                type: 'POST',
                data: {
                    inpKeyword: keyword,
                    inpSendEmailStart: sendEmailStart,
                    inpSendEmailReceived: sendEmailReceived,
                    inpEmailList: emailList,
                    inpSendSMSStart: sendSMSStart,
                    inpSendSMSReceived: sendSMSReceived,
                    inpSMSList: SMSList
                },
                beforeSend: function () {
                    $("#processingPayment").show();
                }
            })
            .done(function(data) {
                data = JSON.parse(data);
                if (parseInt(data.RXRESULTCODE) > 0) {
                    fnGetDataList($("input[type='search']").val());
                    $("#AddNewKeyword").modal('hide');
                    alertBox(data.MESSAGE, 'Add new keyword');
                } else {
                    alertBox(data.MESSAGE);
                }
            })
            .fail(function(e, msg) {
                console.log(msg);
            })
            .always(function() {
                $("#processingPayment").hide();
            });
            
        }
    });


    /* Search keyword */
    $('input[type=search]').on('search', function () {
        var keyword = $(this).val();
        fnGetDataList(keyword);
    });

    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();
    });

    $("#keywordlist").on('click', '.btn-setting', function(event) {
        event.preventDefault();
        var self = $(this);
        var batchId = self.data('batchid');

        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=getAPISettingsByBatchId&inpBatchId=' + batchId + strAjaxQuery,
            type: 'GET',
            beforeSend: function () {
                $("#processingPayment").show();
            }
        }).done(function(data){
            if (data && parseInt(data.RXRESULTCODE) == 1) {
                var KeywordSettingsModal = $("#KeywordSettings");
                $('#KeywordSettingsKeywordText').text(self.parents('tr').first().find('td').first().text());
                $('#SettingsBatchId').val(batchId);

                if (data.SETTINGS.inpSendEmailStart) {
                    $('#SettingsSendEmailStart').prop('checked', 'true' == data.SETTINGS.inpSendEmailStart);
                } else {
                    $('#SettingsSendEmailStart').prop('checked', false);
                }

                if (data.SETTINGS.inpSendEmailReceived) {
                    $('#SettingsSendEmailReceived').prop('checked', 'true' == data.SETTINGS.inpSendEmailReceived);
                } else {
                    $('#SettingsSendEmailReceived').prop('checked', false);
                }

                if (data.SETTINGS.inpEmailList) {
                    $('#SettingsEmailList').val(data.SETTINGS.inpEmailList);
                } else {
                    $('#SettingsEmailList').val('');
                }

                if (data.SETTINGS.inpSendSMSStart) {
                    $('#SettingsSendSMSStart').prop('checked', 'true' == data.SETTINGS.inpSendSMSStart);
                } else {
                    $('#SettingsSendSMSStart').prop('checked', false);
                }

                if (data.SETTINGS.inpSendSMSReceived) {
                    $('#SettingsSendSMSReceived').prop('checked', 'true' == data.SETTINGS.inpSendSMSReceived);
                } else {
                    $('#SettingsSendSMSReceived').prop('checked', false);
                }

                if (data.SETTINGS.inpSMSList) {
                    $('#SettingsSMSList').val(data.SETTINGS.inpSMSList);
                } else {
                    $('#SettingsSMSList').val('');
                }

                KeywordSettingsModal.modal('show');
            } else if (data && data.MESSAGE) {
                alertBox(data.MESSAGE);
            }
        }).fail(function(e, msg) {
            console.log(msg);
        }).always(function() {
            $("#processingPayment").hide();
        });
    });

    $("#save-settings").click(function(event) {
        var batchId = $('#SettingsBatchId').val();
        var sendEmailStart = $("#SettingsSendEmailStart").is(':checked');
        var sendEmailReceived = $("#SettingsSendEmailReceived").is(':checked');
        var emailList = $("#SettingsEmailList").val();
        var sendSMSStart = $("#SettingsSendSMSStart").is(':checked');
        var sendSMSReceived = $("#SettingsSendSMSReceived").is(':checked');
        var SMSList = $("#SettingsSMSList").val();
        
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=saveAPISettings' + strAjaxQuery,
            type: 'POST',
            data: {
                inpBatchId: batchId,
                inpSendEmailStart: sendEmailStart,
                inpSendEmailReceived: sendEmailReceived,
                inpEmailList: emailList,
                inpSendSMSStart: sendSMSStart,
                inpSendSMSReceived: sendSMSReceived,
                inpSMSList: SMSList
            },
            beforeSend: function () {
                $("#processingPayment").show();
            }
        })
        .done(function(data) {
            if (data && parseInt(data.RXRESULTCODE) == 1) {
                $("#KeywordSettings").modal('hide');

                alertBox(data.MESSAGE, 'Save settings');
            } else {
                alertBox(data.MESSAGE, 'Save settings');
            }
        })
        .fail(function(e, msg) {
            console.log(msg);
        })
        .always(function() {
            $("#processingPayment").hide();
        });
    });

    ChangeShortCodeCallback = function () {
        fnGetDataList();
    }

})();