jQuery(document).ready(function() {

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    $('#text-form').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topRight"});

    $('body').on('click', '#btn_send', function(event) {
        event.preventDefault();
        if ($("#text-form").validationEngine('validate')) {
            var inpTextToSend = $("#TextToSend").val();
            var inpSessionIds = $("#SessionIds").val();
            $.ajax({
                url: '/session/sire/models/cfc/smschat.cfc?method=groupResponse' + strAjaxQuery,
                type: 'POST',
                data: {
                    inpSessionIds: inpSessionIds,
                    inpTextToSend: inpTextToSend
                },
                beforeSend: function () {
                    $("#processingPayment").show();
                }
            })
            .done(function(data) {
                if (parseInt(data.RXRESULTCODE) > 0) {
                    $("#TextToSend").val('');

                    if (data.SENDERRORS.length > 0) {
                        var message =   'Some customers were not able to received message:<div class="alert alert-warning"><strong>';
                        for (var i = 0; i < data.SENDERRORS.length; i++) {
                            message += data.SENDERRORS[i] + '<br/></strong>';
                        }
                        message += '</div>';
                        bootbox.dialog({
                            message: '<h4 class="be-modal-title">Send multiple</h4><p>'+message+'</p>',
                            title: '&nbsp;',
                            className: "be-modal",
                            buttons: {
                                success: {
                                    label: "OK",
                                    className: "green-gd",
                                    callback: function(){
                                    }
                                },
                            }
                        });
                    } else {
                        alertBox(data.MESSAGE, "Send multiple");
                    }
                } else {
                    alertBox(data.MESSAGE, 'Oops!');
                }
            })
            .fail(function(e, msg) {
                console.log(msg);
            })
            .always(function() {
                $("#processingPayment").hide();
            });
        }
    });

});
