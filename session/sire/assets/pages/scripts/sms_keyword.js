(function() {

    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    var selectedSessions = [];

    /*User Plan Details Report***********************************************************************/
    fnGetDataList = function (contactstring){
        $("#sessionList").html('');
        
        var showCheckItem = function(object){
            if (object.SessionState_int == 1) {
                var index = selectedSessions.indexOf(parseInt(object.SessionId_bi));
                return "<div class='custom-checkbox'>"+
                            "<input "+(index >= 0 ? "checked" : "")+" class='select-send-multiple' id='select-send-"+object.SessionId_bi+"' type='checkbox' value='"+object.SessionId_bi+"'>"+
                        "<label for='select-send-"+object.SessionId_bi+"'></label></div>";
            } else {
                //return "<input class='' type='checkbox' disabled>";
                return "";
            }
        };

        var showContactString = function (object) {
            return "<a href='/session/sire/pages/sms-response?sessionid="+object.SessionId_bi+"'>"+object.ContactString_vch+"</a>"
        };

        var showAdminResponse = function (object) {
            if (object.LastMSGTime_dt == object.LastResponseTime_dt) {
                return object.Response_vch;
            } else {
                return "";
            }
        };

        var showAutoResponse = function (object) {
            return "";
        };

        var showCompleted = function (object) {
            if (object.SessionState_int != 1) {
                return '<i class="fa fa-check-square" aria-hidden="true" style="color:#7DCD88"></i>';
            } else {
                return "<div class='custom-checkbox'>"+
                            "<input class='select-mark-completed' type='checkbox' value='"+object.SessionId_bi+"' id='select-completed-"+object.SessionId_bi+"''>"+
                        "<label for='select-completed-"+object.SessionId_bi+"'></label></div>";
            }
        }

        var table = $('#sessionList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": showCheckItem, "sName": 'select', "sTitle": "","bSortable": false,"sClass":"select td-select", "sWidth": "50px"},
                {"mData": showContactString, "sName": 'Phone', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "LastMSGTime_dt", "sName": 'MsgTime', "sTitle": 'MsgTime', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": "Receive_vch", "sName": 'User Last Msg', "sTitle": 'User Last Msg', "bSortable": false, "sClass": "", "sWidth": "250px"},
                {"mData": showAdminResponse, "sName": 'Admin Last Msg', "sTitle": 'Admin Last Msg', "bSortable": false, "sClass": "", "sWidth": "250px"},
                //{"mData": showAutoResponse, "sName": 'Auto Response', "sTitle": 'Auto Response', "bSortable": false, "sClass": "", "sWidth": "130px"},
                {"mData": showCompleted, "sName": 'Completed', "sTitle": 'Completed', "bSortable": false, "sClass": "col-center", "sWidth": "120px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/smschat.cfc?method=getListSession'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpContactString", "value": contactstring}
                );

                aoData.push(
                    { "name": "inpNPA", "value": NPA}
                );

                aoData.push(
                    { "name": "inpNXX", "value": NXX}
                );

                aoData.push(
                    { "name": "inpBatchId", "value": batchId}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);

                        if (fnCheckSelectedAllOnPage()) {
                            $("#select-all").prop('checked', true);
                        }

                        fnShowSendMultipleButton();
                    }
                });
            },
            "fnHeaderCallback": function( thead, data, start, end, display, object) {
                $(thead).find('th.select').html("<div class='custom-checkbox'><input id='select-all' class='select-all' type='checkbox' value='0'><label for='select-all'></label></div>");
            },
        });
    }

    var fnCheckSelectedAllOnPage = function () {
        var isSelectedAll = true;
        if (selectedSessions.length == 0) {
            return false;
        }
        $("#sessionList").find('.select-send-multiple').each(function(index, el) {
            var ssid = parseInt(el.value);
            var index = selectedSessions.indexOf(ssid);
            if (index < 0) {
                isSelectedAll = false
            }
        });

        return isSelectedAll;
    }

    var fnShowSendMultipleButton = function () {
        if (selectedSessions.length > 0) {
            $("#sendMultipleBtn").removeClass('hidden');
        } else {
            $("#sendMultipleBtn").addClass('hidden');
        }
    }

    $("body").on('click', '#sendMultipleBtn', function(event) {
        event.preventDefault();
        location.href = "/session/sire/pages/sms-group-response?keyword="+keyword+"&NPA="+NPA+"&NXX="+NXX+"&inpSessionIds="+selectedSessions.join(',');
    });

    $("body").on('change', '#select-all', function(event) {
        event.preventDefault();
        var isChecked = this.checked ? true : false;
        if (isChecked) {
            $('#sessionList').find(".select-send-multiple").prop('checked', true);
            $('#sessionList').find(".select-send-multiple").each(function(index, el) {
                var ssid = parseInt($(el).val());
                if(selectedSessions.indexOf(ssid) < 0) {
                    selectedSessions.push(ssid);
                }
            });
        } else {
            $('#sessionList').find(".select-send-multiple").prop('checked', false);
            $('#sessionList').find(".select-send-multiple").each(function(index, el) {
                var index = selectedSessions.indexOf(parseInt($(el).val()));
                if(index >= 0) {
                    selectedSessions.splice(index, 1);
                }
            });
        }
        //console.log(selectedSessions);
        fnShowSendMultipleButton();
    });

    $("body").on('change', '.select-send-multiple', function(event) {
        event.preventDefault();
        var isChecked = this.checked ? true : false;
        if (isChecked) {
            if (selectedSessions.indexOf(parseInt(this.value)) < 0) {
                selectedSessions.push(parseInt(this.value));

                if (fnCheckSelectedAllOnPage()) {
                    $("#select-all").prop('checked', true);
                }
            }
        } else {
            var index = selectedSessions.indexOf(parseInt(this.value));
            if (index >= 0) {
                selectedSessions.splice(index, 1);
            }
            $("#select-all").prop('checked', false);
        }
        //console.log(selectedSessions);
        fnShowSendMultipleButton();
    });

    var fnMarkSessionComplete = function(sessionid) {
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=completeSession' + strAjaxQuery,
            type: 'POST',
            data: {inpSessionId: sessionid},
            beforeSend: function() {
                $("#processingPayment").show();
            }
        })
        .done(function(data) {
            data = JSON.parse(data);
            if (parseInt(data.RXRESULTCODE) > 0) {
                fnGetDataList($('input[type=search]').val());
            } else {
                alertBox(data.MESSAGE);
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        })
        .always(function() {
            $("#processingPayment").hide();
        });
    };

    $("body").on('change', '.select-mark-completed', function(event) {
        event.preventDefault();
        var select = $(this);
        var sessionid = $(this).val();
        bootbox.dialog({
            message: '<h4 class="be-modal-title">Close conversation</h4><p>Are you sure you want to close this conversation?</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                       fnMarkSessionComplete(sessionid);
                    }
                },
                cancel: {
                    label: "NO",
                    className: "green-cancel",
                    callback: function () {
                        select.attr('checked', false);
                    }
                },
            }
        });
    });

    $('input[type=search]').on('search', function () {
        var contactstring = $(this).val();
        fnGetDataList(contactstring);
    });

    $(document).ready(function() {
        fnGetDataList();
    });

})();