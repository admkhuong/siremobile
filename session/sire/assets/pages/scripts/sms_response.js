jQuery(document).ready(function() {
    $("#chat-phone-number").mask("(000) 000-0000");
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

    var currSessionId = ssid;
    var currContactString = '';

    var iDisplayStart = 0;
    var iDisplayLength = 1000;
    var currChannel = '';

    var CampaignComponents = function () {
        return {
            initPreviewScroll: function () {
                $("#SMSHistoryScreenArea").mCustomScrollbar({
                    theme: 'my-theme',
                    //setHeight: 455
                });
            },
            handleCurrentNumberScroll: function () {
                $('#wrap-new-curent').mCustomScrollbar({
                    theme: 'my-theme',
                    scrollbarPosition: 'outside'
                });
            },
            handleScreenScroll: function () {
                $('.screen-non-ip').mCustomScrollbar({
                    theme: 'my-theme',
                    scrollbarPosition: 'inside'
                });
            },
            init: function () {
                this.initPreviewScroll();
                //this.handleScreenScroll();
                this.handleCurrentNumberScroll();
            }
        };
    }();

    var DisplayMessage = function(css, msg, time) {
        $('#mCSB_1_container').append('<div style="clear:both"></div><div class="'+css+'">' + (typeof msg != "undefined" ? msg.replace(/\r?\n/g, '<br />') : "") + '</div><span>'+ (typeof time != "undefined" ? time : "") +'</span>');
    }

    var UpdateScrollBar = function () {
        if ($("#mCSB_1_container").children('div').length > 0) {
            $("#SMSHistoryScreenArea").mCustomScrollbar("update");
            $("#SMSHistoryScreenArea").mCustomScrollbar("scrollTo", "last");
        }
    }

    /* Socket io */
    var socket = io('https://ws.siremobile.com:8443/');

    var RemoveListener = function (channel){
        socket.removeListener(channel);
    }

    var SocketListen = function (channel) {
        // Remove current channel being listening
        RemoveListener(currChannel);

        socket.on(channel, function(data){
            //console.log('receive'+ Date.now()+" "+channel);
            data = jQuery.parseJSON(data);
            if (data.type == 2) {
                var cssClass = 'newbubble guess';
            } else {
                var cssClass = 'newbubble me';
            }

            DisplayMessage(cssClass, escapeHtml(data.msg), data.time);

            UpdateScrollBar();

            if (data.type == 2 && data.msg.toLowerCase() == "exit") {
                if (data.sessionid == currSessionId) {
                    location.href = "/session/sire/pages/sms-response?ssid="+currSessionId;
                } else {
                    setTimeout(function () {
                        GetSessionList(1);
                    }, 2000);
                }
            }

            if (data.sessionid == currSessionId) {
                setTimeout(function () {
                    SetSessionRead(currSessionId);
                    $(".chat-session[data-id='"+currSessionId+"']").find('span').text('');
                }, 2000);
            }
        });
        currChannel = channel;
    }

    socket.on(us_chn, function(data){
        data = jQuery.parseJSON(data);
        if (data.msg.toLowerCase() == "exit") {
            setTimeout(function () {
                GetSessionList(1);
            }, 2000);
        } else {
            GetSessionList(1);
        }
    });

    var SetSessionRead = function (sessionid) {
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=setSessionRead' + strAjaxQuery,
            type: 'POST',
            data: {inpSessionId: sessionid}
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) <= 0) {
                console.log(data.MESSAGE);
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        });
    }

    $('#text-form').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topRight"});

    $('body').on('click', '#SMSSend', function(event) {
        event.preventDefault();
        SendSMS();
    });

    var SendSMS = function () {
        if ($("#text-form").validationEngine('validate')) {
            if (currSessionId <= 0) {
                $("#SMSTextInputArea").val('');
                return false;
            }
            var inpTextToSend = $("#SMSTextInputArea").val();
            $.ajax({
                url: '/session/sire/models/cfc/smschat.cfc?method=SendSMS' + strAjaxQuery,
                type: 'POST',
                data: {
                    inpSessionId: currSessionId,
                    inpTextToSend: inpTextToSend,
                    inpShortCodeId: $("#ShortCodeId").val()
                },
                beforeSend: function () {
                    $("#processingPayment").show();
                }
            })
            .done(function(data) {
                data = JSON.parse(data);
                if (parseInt(data.RXRESULTCODE) > 0) {
                    $("#SMSTextInputArea").val('');
                } else {
                    bootbox.dialog({
                        message: '<h4 class="be-modal-title">Oops!</h4><p>'+ data.MESSAGE +'</p>',
                        title: '&nbsp;',
                        className: "be-modal",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "green-gd",
                                callback: function(){
                                    $(".chat-session[data-id='"+currSessionId+"']").trigger('click');
                                    $("#SMSTextInputArea").val();
                                }
                            },
                        },
                        onEscape: function () {
                            $(".chat-session[data-id='"+currSessionId+"']").trigger('click');
                            $("#SMSTextInputArea").val();
                        }
                    });
                }
            })
            .fail(function(e, msg) {
                console.log(msg);
            })
            .always(function() {
                $("#processingPayment").hide();
                fnUpdateUserCurrentBalance();
            });
        }
    }

    $('body').on('click', '#make-new-chat', function(event) {
        event.preventDefault();
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=MakeNewChatSessionForContactString' + strAjaxQuery,
            type: 'POST',
            data: {
                inpKeyword: keyword,
                inpContactString: currContactString
            },
            beforeSend: function () {
                $("#processingPayment").show();
            }
        })
        .done(function(data) {
            data = JSON.parse(data);
            if (parseInt(data.RXRESULTCODE) > 0) {
                GetSessionList();
                setTimeout(function() {
                    $(".chat-session[data-id='"+data.NEWSESSIONID+"']").trigger('click');
                }, 500);

                $("#processingPayment").hide();
            } else {
                alertBox(data.MESSAGE, 'Oops!');
            }
        })
        .fail(function(e, msg) {
            console.log(msg);
        })
        .always(function() {
            $("#processingPayment").hide();
        });
        
    });

    var BindCharacterCountAllPage = function () {

        var counterId = "cp-char-count";
        var text = $("#SMSTextInputArea");

        if (text.length) {
            countText("#"+counterId, $(text).val());

            $(text).bind('input change', function(e) {
                countText("#"+counterId, $(text).val());
            });
        }
    }

    $("body").on('click', '.chat-session', function(event) {
        event.preventDefault();
        if (!$(this).hasClass('active')) {
            $(this).children('span').text('');
            $(".chat-session").removeClass('active');
            $(this).addClass('active');

            var sessionid = $(this).data('id');
            var contactstring = $(this).data('phone');
            var state = $(this).data('state');
            currSessionId = sessionid;
            currContactString = contactstring;

            ShowSpinner();

            GetResponseList(sessionid);

            if ($(this).data('state') != 1) {
                $("#btn-end-chat").hide();
            } else {
                $("#btn-end-chat").show();
            }

            if (history.pushState) {
                history.pushState({}, null, location.origin + location.pathname + "?keyword="+keyword+"&ssid="+currSessionId + (showPreview?"&showPreview=1":""));
            }

            if(!$("#message-section").is(':visible')) {
                $("#list-section").hide('slide', {direction: "left"}, 200);
                $("#message-section").show('slide', {direction: "right"}, 500);

                $("#phone-number").text(currContactString);
            }

            $(".view-old").show();
        }
    });

    $("body").on('click', '#back-to-list', function(event) {
        event.preventDefault();
        $("#message-section").hide('slide', {direction: "right"}, 200);
        $("#list-section").show('slide', {direction: "left"}, 500);
        $(".chat-session").removeClass('active');
        currSessionId = 0;
    });

    var GetResponseList = function (sessionid) {
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=GetResponseForIRESession' + strAjaxQuery,
            type: 'POST',
            data: {inpSessionId: sessionid}
        })
        .done(function(data) {
            $('#mCSB_1_container').html('');
            if (parseInt(data.RXRESULTCODE) > 0) {
                if (data.PREVIEW) {
                    $("#SMSTextInputArea").val('');
                    $("#SMSTextInputArea").prop('disabled', true);
                    $("#SMSSend").prop('disabled', true);
                    DisplayMessage('newbubble info', "This session is for preview function. You can't send message on this.");
                    $(".newbubble.info").css('margin-bottom', '20px');
                    SocketListen(data.SSCHN);
                }
                for (var i = 0; i < data.RESPONSE.length; i++) {
                    var m = data.RESPONSE[i];
                    var cssClass = (m.TYPE == 1) ? "newbubble me" : "newbubble guess";
                    DisplayMessage(cssClass,escapeHtml(m.MSG), m.TIME);
                }
                if (!data.PREVIEW) {
                    if (parseInt(data.SESSIONSTATE) != 1) {
                        if (parseInt(data.EMSFLAG) == 20) {
                            CheckActiveSession();
                        } else {
                            var msg = 'This session is closed.';
                            DisplayMessage('newbubble info', msg);
                        }
                        $("#SMSTextInputArea").val('');
                        $("#SMSTextInputArea").prop('disabled', true);
                        $("#SMSSend").prop('disabled', true);
                        RemoveListener(currChannel);
                    } else {
                        $("#SMSTextInputArea").val('');
                        $("#SMSTextInputArea").prop('disabled', false);
                        $("#SMSSend").prop('disabled', false);
                        SocketListen(data.SSCHN);
                    }
                }
            }
            UpdateScrollBar();
        })
        .fail(function(e,msg) {
            console.log(msg);
        });
    }

    var CheckActiveSession = function () {
        if (currContactString == '') {
            return false;
        }
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=GetActiveSessionForContactString' + strAjaxQuery,
            type: 'POST',
            data: {inpContactString: currContactString},
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) == 1) {
                var msg = 'Session Closed. You have another active conversation with this customer, please click <a href="/session/sire/pages/sms-response?ssid='+data.SESSIONID+'" class="alert-link">here</a> to enter.';
            } else {
                var msg = 'Session Closed. Please click <a id="make-new-chat" class="alert-link">here</a> to make a new conversation with this customer.';
            }
            DisplayMessage('newbubble info', msg);
        })
        .fail(function(e,msg) {
            console.log(msg);
        });
    }

    $("body").on('click', '#change-view', function(event) {
        event.preventDefault();
        var view_change = 1 - default_view;
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=UpdateUserSetting' + strAjaxQuery,
            type: 'POST',
            data: {
                inpSettingType: 1,
                inpValue: view_change
            },
        })
        .always(function() {
            var href = "/session/sire/pages/sms-response?keyword="+keyword;
            if (currSessionId > 0) {
                href += "&ssid=" + currSessionId;
            }
            location.href = href;
        });
    });

    $("#search").keyup(function(event) {
        GetSessionList();
    });

    $("body").on('click', '.filter-child', function(event) {        
        $("#mCSB_1_container").html('');
        currState = $(this).data('state');
        GetSessionList();
        iDisplayStart = 0;
    });

    var GetSessionList = function (skipRspLoad) {
        skipRspLoad = (typeof skipRspLoad == "undefined" ? 0 : skipRspLoad);
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=getListSessionForView2' + strAjaxQuery,
            type: 'POST',
            data: {
                inpBatchId: batchid,
                inpContactString: $("#search").val(),
                inpSessionState: currState,
                inpShowPreview: showPreview
            },
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) > 0) {
                $("#new-list").html('');
                $("#current-list").html('');
                $("#close-list").html('');
                $("#preview-list").html('');
                var pos = [];
                for (var i = 0; i < data.aaData.length; i++) {
                    var index = data.aaData[i];
                    if (index.Preview) {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a>'+index.ContactString_vch+'</a>'+
                                    '</li>';
                        $("#preview-list").append(html);
                    } else if (index.NewMessage > 0 && index.SessionState_int == 1) {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a><b>'+index.ContactString_vch+'</b></a> <span>'+(index.MessageUnread_int > 0 ? index.MessageUnread_int : '')+'</span>'+
                                    '</li>';
                        $("#new-list").append(html);
                    } else if (index.SessionState_int == 1 ) {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a>'+index.ContactString_vch+'</a> <span>'+(index.MessageUnread_int > 0 ? index.MessageUnread_int : '')+'</span>'+
                                    '</li>';
                        $("#current-list").append(html);
                    } else {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a class="text-color-gray text-italic">'+index.ContactString_vch+'</a>'+
                                    '</li>';
                        $("#close-list").append(html);
                    }
                }

                if ($("#new-list").children('li').length <= 0) {
                    $(".first-new").hide();
                } else {
                    $(".first-new").show();
                }
                if ($("#current-list").children('li').length <= 0) {
                    $(".second-current").hide();
                } else {
                    $(".second-current").show();
                }
                if ($("#close-list").children('li').length <= 0) {
                    $(".third-close").hide();
                } else {
                    $(".third-close").show();
                }
                if ($("#preview-list").children('li').length <= 0) {
                    $(".forth-preview").hide();
                } else {
                    $(".forth-preview").show();
                }

                if (!$(".chat-session[data-id="+currSessionId+"]").hasClass('active')) {
                    if (!skipRspLoad) {
                        $(".chat-session[data-id="+currSessionId+"]").trigger('click');
                    }
                    $(".chat-session[data-id="+currSessionId+"]").addClass('active');
                }

                if ($(".chat-session").length < parseInt(data.iTotalRecords)) {
                    $("#current-list").append('<li id="view-more"><a>More</a></li>')
                }
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        });
    }

    $("body").on('click', '.filter-child', function(event) {
        event.preventDefault();
        $("#filter-parent").children('span').text($(this).text()).trigger('click');
    });

    $("body").on('click', '#SMSTextInputArea', function(event) {
        event.preventDefault();
        $(".chat-session[data-id='"+currSessionId+"']").children('span').text('');
    });

    $("#SMSTextInputArea").keypress(function(e) {
        if (enter_to_send && e.keyCode == 13) {
            e.preventDefault();
            SendSMS();
        }
    });

    $("body").on('change', '#enter-to-send', function(event) {
        event.preventDefault();
        var value = this.checked ? 1 : 0;
        var type = 2;
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=UpdateUserSetting' + strAjaxQuery,
            type: 'POST',
            data: {
                inpSettingType: 2,
                inpValue: value
            },
        });
        enter_to_send = value;
    });

    $("body").on('click', '#view-more', function(event) {
        event.preventDefault();
        $(this).remove();
        iDisplayStart += iDisplayLength;
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=getListSessionForView2' + strAjaxQuery,
            type: 'POST',
            data: {
                inpBatchId: batchid,
                inpContactString: $("#search").val(),
                inpSessionState: currState,
                iDisplayStart: iDisplayStart
            },
        })
        .done(function(data) {
            if (parseInt(data.RXRESULTCODE) > 0) {
                var pos = [];
                for (var i = 0; i < data.aaData.length; i++) {
                    var index = data.aaData[i];
                    if ((index.NewMessage > 0 && index.SessionState_int == 1) || index.MessageUnread_int > 0) {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a><b>'+index.ContactString_vch+'</b></a> <span>'+(index.MessageUnread_int > 0 ? index.MessageUnread_int : '')+'</span>'+
                                    '</li>';
                        $("#new-list").append(html);
                    } else {
                        var html = '<li class="chat-session" data-id="'+index.SessionId_bi+'" data-state="'+index.SessionState_int+'" data-phone="'+index.ContactString_vch+'">'+
                                        '<a class="'+(index.SessionState_int != 1 ? 'text-color-gray text-italic' : '')+'">'+index.ContactString_vch+'</a>'+
                                    '</li>';
                        $("#current-list").append(html);
                    }
                }
                $(".chat-session[data-id="+currSessionId+"]").addClass('active');
                if ($(".chat-session").length < parseInt(data.iTotalRecords)) {
                    $("#current-list").append('<li id="view-more"><a>More</a></li>')
                }
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        });
    });

    $("body").on('click', '#search-old', function(event) {
        event.preventDefault();
        $("#search").val(currContactString);
        GetSessionList();
        if (!$("#list-section").is(":visible")) {
            $("#back-to-list").trigger('click');
        }
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    /* Search keyword */
    $('input[type=search]').on('search', function () {
        GetSessionList();
    });

    $(".sms-chat-li").addClass('active');

    //BindCharacterCountAllPage();
    CampaignComponents.init();

    GetSessionList();

    var ShowSpinner = function () {
        var html = '<div style="margin-top:50%;'+(default_view == 0 ? "margin-left:50%" : "")+'"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></div>';

        $("#mCSB_1_container").html('').append(html);
    }

    var fnMarkSessionComplete = function(sessionid) {
        $.ajax({
            url: '/session/sire/models/cfc/smschat.cfc?method=completeSession' + strAjaxQuery,
            type: 'POST',
            data: {inpSessionId: sessionid},
            beforeSend: function() {
                $("#processingPayment").show();
            }
        })
        .done(function(data) {
            data = JSON.parse(data);
            if (parseInt(data.RXRESULTCODE) > 0) {
                GetSessionList();
            } else {
                alertBox(data.MESSAGE);
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        })
        .always(function() {
            $("#processingPayment").hide();
        });
    };

    $("body").on('click', '#btn-end-chat', function(event) {
        event.preventDefault();
        bootbox.dialog({
            message: '<h4 class="be-modal-title">End chat</h4><p>Are you sure you want to close this chat?</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                       fnMarkSessionComplete(currSessionId);
                    }
                },
                cancel: {
                    label: "NO",
                    className: "green-cancel",
                    callback: function () {
                    }
                },
            }
        });
    });
    $("#mdAddNewChatSession").on("shown.bs.modal", function () { 
        $("#chat-phone-number").val('');
    });
    $("#frDddNewChatSession").on('click','#btn-start-chat-session',function(){
        if ($("#frDddNewChatSession").validationEngine('validate')) {
            var phone=$("#chat-phone-number").val();            
            StartChatSession(phone);
        }
    });

    function StartChatSession(phone){
        $.ajax({
            url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber' + strAjaxQuery,
            type: 'POST',
            data: {PhoneNumber: phone},
            beforeSend: function() {
                $("#processingPayment").show();
            }
        })
        .done(function(data) {            
            if (parseInt(data.RXRESULTCODE) == 0) {                                
                //make new chat
                $.ajax({
                    url: '/session/sire/models/cfc/smschat.cfc?method=MakeNewChatSessionForContactString' + strAjaxQuery,
                    type: 'POST',
                    data: {
                        inpKeyword: keyword,
                        inpContactString: phone,
                        inpSkipCheckOPT:1
                    },
                    beforeSend: function () {
                        $("#processingPayment").show();
                    }
                })
                .done(function(data) {
                    data = JSON.parse(data);
                    if (parseInt(data.RXRESULTCODE) > 0) {
                        GetSessionList();
                        setTimeout(function() {
                            $(".chat-session[data-id='"+data.NEWSESSIONID+"']").trigger('click');
                        }, 500);
        
                        $("#processingPayment").hide();
                        $("#mdAddNewChatSession").modal('hide');
                    } else {
                        alertBox(data.MESSAGE, 'Oops!');
                    }
                })
                .fail(function(e, msg) {
                    console.log(msg);
                })
                .always(function() {
                    $("#processingPayment").hide();
                });
                //
            } else {
                alertBox(data.MESSAGE);
            }
        })
        .fail(function(e,msg) {
            console.log(msg);
        })
        .always(function() {
            $("#processingPayment").hide();
        });
    }    
});
