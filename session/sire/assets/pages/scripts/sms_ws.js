jQuery(document).ready(function() {
    /* Socket io */
    var socket = io('https://ws.siremobile.com:8443/');
    var currentSessionId = 0;
    if (typeof sessionid == "undefined") {
        sessionid = 0;
    }

    toastr.options = {
        "timeOut": "10000",
        "closeButton": true,
        "extendedTimeOut": "5000"
    }

    if (location.href.indexOf("sms-response") >= 0) {
        toastr.options.onclick = function () {
            $(".chat-session[data-id='"+currentSessionId+"']").trigger('click');
        }
    } else {
        toastr.options.onclick = function () {
            window.open('/session/sire/pages/sms-response?ssid='+currentSessionId, '_blank');
        }
    }

    /* Listen on ss chn */
    socket.on(us_chn, function(data){
        data = jQuery.parseJSON(data);
        currentSessionId = data.sessionid;
        if (data.type == 2) {
            if (data.phone.length >= 14) {
                if (location.href.indexOf("sms-response") >= 0) {
                    toastr.options.onclick = function () {
                        location.href = '/session/sire/pages/sms-response?showPreview=1&ssid='+currentSessionId;
                    }
                } else {
                    toastr.options.onclick = function () {
                        window.open('/session/sire/pages/sms-response?showPreview=1&ssid='+currentSessionId, '_blank');
                    }
                }
            }
            var phone = data.phone;
            var msg = data.msg;
            toastr.info(escapeHtml(msg).replace(/\r?\n/g, '<br />'), phone.toPhoneFormat());
        }

        if (data.type == 2) {
            $("#alertMessage").text('You have new message.');
        } else {
            $("#alertMessage").text('You have sent new message.');
        }

        $("#refreshAlert").removeClass('hidden');
    });

    $("body").on('click', '#refreshTable', function(event) {
        event.preventDefault();
        fnGetDataList();
        fnUpdateUserCurrentBalance();
        $("#refreshAlert").addClass('hidden');
    });
});
