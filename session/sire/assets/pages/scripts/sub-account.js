
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
fnGetDataList = function (customFilterData){
    customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   
    $("#sub-account-list").html('');    

    var actionBtn = function (object) {
        var strReturn = '<a title="Edit sub account" class="simon-item user-update-btn" data-id="'+object.USERID+'" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';            
        return strReturn;
    }
    var colFirstSMS = function (object) {
        var strReturn = '<input type="text" class="creditVal form-control form-control validate[required min[0],custom[number]]" value="'+object.FIRSTSMSINCLUDE+'"  style="width: calc(100% - 40px); display:inline; margin-right:5px"><i class="fa fa-check-square save-rows"  data-id="'+object.USERID+'" data-type="1"></i>';            
        return strReturn;
    }
    var colThreshold = function (object) {
        var strReturn = '<input type="text" class="thresHoldVal form-control form-control validate[required min[0],custom[number]]" value="'+object.THRESHOLD+'" style="width: calc(100% - 40px); display:inline; margin-right:5px"><i class="fa fa-check-square save-rows"  data-id="'+object.USERID+'" data-type="2"></i>';            
        return strReturn;
    }
   
    var table = $('#sub-account-list').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
            {"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
            {"mData": "USERNAME", "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": colFirstSMS, "sName": 'Monthly Credit Limit', "sTitle": 'Monthly Credit Limit', "bSortable": false, "sClass": "", "sWidth": "200px"},
            {"mData": colThreshold, "sName": 'Threshold Alert', "sTitle": 'Threshold Alert', "bSortable": false, "sClass": "", "sWidth": "200px"},
            /* {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "", "sWidth": "200px"},             */
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/users.cfc?method=getUsers'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { "name": "customFilter", "value": customFilterData}
            );   
            aoData.push(
                { "name": "inpIncludeSubAccLv", "value": 0}
            );                      
            
            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                    
                }
            });
        }
    });
}

function SaveSubAccountBilling()
{        
    if ($('#frUpdateSubAccBilling').validationEngine('validate', {scroll: false, focusFirstField : false})) {          
        var modalUpdate=$("#mdUpdateSubAccBilling");
        var subAccId= modalUpdate.find("#subAccId").val();
        var credit=modalUpdate.find("#creditVal").val();
        var threshold=modalUpdate.find("#thresHoldVal").val();

        $.ajax({
            url: '/session/sire/models/cfc/users.cfc?method=UpdateUserBillingInfo'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpUserID: subAccId,
                inpThreshold :threshold,
                inpBallance: credit           
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();                
                alertBox(errorThrown); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    $("#mdUpdateSubAccBilling").modal('hide');
                    alertBox("Update Sub account successfully",'SUCCESSFULLY!','');
                    fnGetDataList(); 
                } else {                    
                    alertBox(data.MESSAGE,'Opps!','');
                }                       
            }
        });
    }
    else
    {                           
        return false;
    }
}
function GetSubAccountBillingInfo(subAccId)
{
    if(subAccId >0)
    {
        $.ajax({
            url: '/session/sire/models/cfc/users.cfc?method=GetUserBillingInfo'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpUserID: subAccId
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();                
                alertBox(errorThrown); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    var modalUpdate=$("#mdUpdateSubAccBilling");
                    modalUpdate.find("#subAccId").val(subAccId);
                    modalUpdate.find("#creditVal").val(data.BALANCE);
                    modalUpdate.find("#thresHoldVal").val(data.THRESHOLD);
                    modalUpdate.modal("show");
                } else {                    
                    alertBox(data.MESSAGE,'Opps!','');
                }                       
            }
        });
    }
}
function SaveRows(type,subAccId,credit,threshold)
{        
    if ($('#frSubAcc').validationEngine('validate', {scroll: false, focusFirstField : false})) {          
        
        $.ajax({
            url: '/session/sire/models/cfc/users.cfc?method=UpdateUserBillingInfo'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpUserID: subAccId,
                inpThreshold :threshold,
                inpBallance: credit           
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();                
                alertBox(errorThrown); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (data.RXRESULTCODE == 1) {
                    $("#mdUpdateSubAccBilling").modal('hide');
                    if(type==1){
                        alertBox("Update Monthly Credit Limit successfully",'SUCCESSFULLY!','');
                    }
                    else
                    {
                        alertBox("Update Threshold Alert successfully",'SUCCESSFULLY!','');
                    }
                    
                    fnGetDataList(); 
                } else {                    
                    alertBox(data.MESSAGE,'Opps!','');
                }                       
            }
        });
    }
    else
    {                           
        return false;
    }
}
$(document).ready(function(){
    $("#plan").select2({
        theme: "bootstrap",
        width: 'auto'
    });
    InitFilterForUserTable();
    fnGetDataList();             
    //Filter bar initialize
    function InitFilterForUserTable () {
        $("#box-filter").html('');
        //Filter bar initialize
        $('#box-filter').sireFilterBar({
            fields: [
                {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
                {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
                {DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
                {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
                {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
                {DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
                {DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
                //{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
            ],
            clearButton: true,
            error: function(ele){
                
            },
            clearCallback: function(){
                fnGetDataList();
            }
        }, function(filterData){
            fnGetDataList(filterData);
        });
    }  
    $(document).on("click",".user-update-btn",function(){
        
        var subAccId=$(this).data("id");        
        GetSubAccountBillingInfo(subAccId);        
    });
    $("#frUpdateSubAccBilling").on('submit',function(e){        
        e.preventDefault();   
        SaveSubAccountBilling();
    });    
    $(document).on("click",".save-rows",function(e){   
        e.preventDefault();   
        var subAccId = $(this).data("id");
        var type = $(this).data("type");
        var credit = $(this).closest("tr").find(".creditVal").val();
        var threshold=  $(this).closest("tr").find(".thresHoldVal").val();
        SaveRows(type,subAccId,credit,threshold);                          
    });
    
});