var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var subcriberListId = 0;
var tblSubcriberList = '';

(function($){

	//Set up all the select2 boxes
	$(".Select2").select2( { theme: "bootstrap"} );

	function InitSubcriberListDS(subID){

		var subID = typeof(subID)!='undefined'?subID:0;
		// var actionCol = function(object){
		// 	return 	$('<button class="btn-re-edit"></button>').attr('data-id', object.ID).append($('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'))[0].outerHTML+
		// 			$('<button class="btn-re-dowload"></button>').attr('data-id', object.ID).append($('<i class="fa fa-download" aria-hidden="true"></i>'))[0].outerHTML+
		// 			$('<button class="btn-re-delete subscriber-list-delete"></button>').attr('data-id', object.ID).append($('<i class="fa fa-times" aria-hidden="true"></i>'))[0].outerHTML;
		// }

		var actionCol = function(object){
			return $('<a class="btn btn-edit-cp-new">Edit</a>').attr('data-id', object.ID)[0].outerHTML + $('<div class="dropdown action-cp-dropdown-list"><button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu"><li><a class="download-sub-new" data-id="'+object.ID+'">Download</a></li><li><a class="del-sub-new" data-id="'+object.ID+'">Delete</a></li></ul></div>')[0].outerHTML;
		}

		var selectCol = function(object){
			return $('<a class="select-item" data-id="'+object.ID+'"><span class="icon-bullet-green"></span></a>')[0].outerHTML;
		}

		tblSubcriberList = $('#tblSubcriberList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": selectCol, "sName": 'select', "sTitle": '', "bSortable": true, "sClass": "select", "sWidth": "10px"},
	            {"mData": "NAME", "sName": 'Subscribers List', "sTitle": 'Subscribers List', "bSortable": true, "sClass": "name", "sWidth": ""},
	            {"mData": "OPTIN", "sName": 'opt-in', "sTitle": "Opt-in's", "bSortable": false, "sClass": "opt-in", "sWidth": "150px"},
	            /* {"mData": "OPTOUT", "sName": 'opt-out', "sTitle": "Opt-out's", "bSortable": false, "sClass": "opt-out", "sWidth": "110px"}, */
	            {"mData": actionCol, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "action", "sWidth": "120px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupListForDatatableNew'+strAjaxQuery,
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				// if(selectSubList == 0){
				// 	var firstRow = this.find('tbody tr').first();
				// 	var firstSubID = firstRow.find('a.select-item').data('id');
				// 	firstRow.find('a.select-item').addClass('active');
				// 	if(firstSubID){
				// 		$('#total-subcriber').text(firstRow.find('td.opt-in').text());
				// 		$('input[name="sub-id"]').val(firstSubID);
				// 		InitSubcriberListDetailDS(firstSubID);
				// 		subcriberListId = firstSubID;
				// 	}
				// 	selectSubList = 0;
				// }

				var firstSubID = this.find("tbody tr a[data-id='"+parseInt(subcriberListId)+"']");
				if(firstSubID.length>0){
					firstSubID.addClass('active');
					//$('#total-subcriber').text(firstRow.find('td.opt-in').text());
					$('#total-subcriber').text(firstSubID.parent().siblings('.opt-in').text());
					$('input[name="sub-id"]').val(subcriberListId);
					//InitSubcriberListDetailDS(subcriberListId);
					subcriberListId = firstSubID;
				}
				else{
					var firstRow = this.find('tbody tr').first();
					var firstSubID = firstRow.find('a.select-item').data('id');
					firstRow.find('a.select-item').addClass('active');
					if(firstSubID){
						$('#total-subcriber').text(firstRow.find('td.opt-in').text());
						$('input[name="sub-id"]').val(firstSubID);
						InitSubcriberListDetailDS(firstSubID);
						subcriberListId = firstSubID;
					}
				}

		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

		        aoData.push(
		            { "name": "inpGroupId", "value": subID}
	            );

		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0) {
			        		$('#tblSubcriberList_paginate').hide();
			        	}
			        	fnCallback(data);
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				// $('#tblSubcriberList tr:last').find('td.action').find('div.action-cp-dropdown-list').removeClass('dropdown');
				// $('#tblSubcriberList tr:last').find('td.action').find('div.action-cp-dropdown-list').addClass('dropup');
			}
		});
	}


	InitSubcriberListDS();


	$('span.btn-search-contact').click(function(e){
		var keyword = $('input[name="contact-keyword"]').val();
		var subID = $('input[name="sub-id"]').val();

		InitSubcriberListDetailDS(subID, keyword);

	});


	$('body').on('click', '.btn-edit-cp-new', function(event){

		var groupId = $(this).data('id');

		$('#contact-group-id').val(groupId);
		$('#rename-subscriber-list').modal('show');
		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {inpGroupId: groupId},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	success: function(data){
	    		$('#new-subscriber-list-name').val(data.SUBSCRIBERLISTNAME);
	    		LoadSelectSubscriber();
		    },
		    complete: function(){
	    		$('#processingPayment').hide();

		    }
		});

	});

	$('#rename-subscriber-list-form').on('submit', function(event){
		event.preventDefault();
		if($('#rename-subscriber-list-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			$.ajax({
    			url: '/session/sire/models/cfc/subscribers.cfc?method=UpdateSubscriberListNameByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
    			type: 'POST',
    			dataType: 'json',
    			data: {
    				inpGroupId: $('#contact-group-id').val(),
    				inpNewGroupName: $('#new-subscriber-list-name').val()
    			},
    			beforeSend: function( xhr ) {
		     		$('#processingPayment').show();
		    	},
		    	success: function(data){
		    		$('#processingPayment').hide();
		    		if(data.RXRESULTCODE == 1){
		    			$('#rename-subscriber-list').modal('hide');
		    			alertBox(data.MESSAGE, 'RENAME SUBSCRIBER LIST', function(){
		    				InitSubcriberListDS();
		    			});
		    		} else {
		    			alertBox(data.MESSAGE, 'RENAME SUBSCRIBER LIST');
		    		}
		    		LoadSelectSubscriber();
		    	}
    		});
		}
	});





	// $('body').on('click', 'button.btn-re-dowload', function(event){
	// 	var scbID = $(this).data('id');
	// });

	$('body').on('click', '.del-sub-new', function(event){
		var scbID = $(this).data('id');

		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetCampaignByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'json',
			data: {inpGroupId: scbID},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	error: function(XMLHttpRequest, textStatus, errorThrown) {
	     		$('#processingPayment').hide();
	     		alertBox('Send Get subscriber list detail request failed!', 'DELETE SUBCRIBER LIST', function(){});

	    	},
	    	success: function(data){
	    		$('#processingPayment').hide();
	    		if(data.RXRESULTCODE==1){
	    			var allCampaign = '';
	    			for (var i = 0; i < data['batchDetail'].length; i++) {
	    				allCampaign = allCampaign + '<li><a href="/session/sire/pages/campaign-template-new?campaignid=' + data['batchDetail'][i].ID + '" target="_blank">' + data['batchDetail'][i].NAME + '</a></li>';
	    			}

	    			confirmBox("Are you sure you want to delete this subcriber list? It is being used by the following campaign(s): " + "<ol>" + allCampaign + "</ol>", 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Send Delete subscriber list request failed!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});

					     		InitSubcriberListDS();
								LoadSelectSubscriber();
								InitSubcriberListDetailDS();
					    	}
			    		});
	    			});
	    		}
	    		else if(data.RXRESULTCODE == 0){

	    			confirmBox('Are you sure you want to delete this subcriber list? ', 'Delete Subcriber List', function(){
	    				$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'JSON',
			    			data: {inpGroupId: scbID},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();

					     		alertBox('Delete subscriber list request failed to send!', 'DELETE SUBCRIBER LIST', function(){});
					    	},
					    	success: function(data) {
					    		$('#processingPayment').hide();

					    		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});

					     		InitSubcriberListDS();
					     		LoadSelectSubscriber();
					    	}
			    		});
	    			});
	    		}
	    		else{

		     		alertBox(data.MESSAGE, 'DELETE SUBCRIBER LIST', function(){});
	    		}
	    	}

		});
	});

	$('#btn-add-new').click(function(){
		$('#subscriber_list_name').val('').validationEngine('hide');
		$('#AddNewSubscriberList button').prop('disabled', false);
    	$('#AddNewSubscriberList').modal('show');
    });


    $('#AddNewSubscriberList form[name="frm-add-new"]').validationEngine('attach', {
		promptPosition : "topLeft",
		autoPositionUpdate: true,
		showArrow: false,
		autoHidePrompt: true,
		autoHideDelay: 5000
	}).on('submit', function(event){
    	event.preventDefault();

		if ($(this).validationEngine('validate')) {
			var thisForm = $(this);
			var thisModal = $('#AddNewSubscriberList');
			var groupName = thisForm.find('#subscriber_list_name').val();
			thisForm.find('button').prop('disabled', true);

			$.ajax({
				url: '/session/cfc/multilists2.cfc?method=addgroup'+strAjaxQuery,
				type: 'POST',
				dataType: 'json',
				data: {INPGROUPDESC: groupName},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#processingPayment').hide();
					thisForm.find('button').prop('disabled', false);
				},
				success: function(data) {
					$('#processingPayment').hide();
					if (data.DATA && data.DATA.RXRESULTCODE && data.DATA.RXRESULTCODE[0] == 1) {
						alertBox("Add new subcriber list success!", 'ADD NEW SUBSCRIBER LIST', function(){
							thisModal.modal('hide');
							InitSubcriberListDS();
						});
					} else {
						alertBox(data.DATA.MESSAGE[0], 'ADD NEW SUBSCRIBER LIST');
					}
					thisForm.find('button').prop('disabled', false);
					InitSubcriberListDS();
					LoadSelectSubscriber();
				}
			});
		}
	});


	$('#tblSubcriberList').on('click', 'tbody td.name', function(e){
		$(this).parent('tr').find('td a.select-item')
			   .addClass('active').parents('tr')
			   .siblings().find('td a.select-item')
			   .removeClass('active');
		//var scbID = $(this).siblings().find('td a.select-item').data('id');
		var scbID = $(this).prev().find('a.select-item').data('id');

		$('input[name="sub-id"]').val(scbID);
		$('#total-subcriber').text($(this).find('td.opt-in').text());
		subcriberListId = scbID;
		InitSubcriberListDetailDS(scbID);
	});

	$('.btn-default').on('click', function(event){
		event.preventDefault();
	});

	function InitSubcriberListDetailDS(GroupID ,keyword){

		var keyword = typeof(keyword) != 'undefined' ? keyword : "";

		var contactCol = function(object){
			return '<a class="link-edit-contact" href="/session/sire/pages/subscriber-edit?contactId=' + object.ID + '" title="Click to edit">' + object.CONTACT + '</a>';
			//return '<a class="lnk-edit-contact" data-id="' + object.ID + '" title="Click to edit">' + object.CONTACT + '</a>';
		}

		var actionCol = function(object){
			return 	$('<button class="btn-re-delete subscriber-delete"></button>').attr('data-id', object.ID).append($('<i class="fa fa-times" aria-hidden="true"></i>'))[0].outerHTML;
		}		

		var table = $('#subcriberListDetail').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": contactCol, "sName": 'Phone', "sTitle": 'Phone', "bSortable": false, "sClass": "contact", "sWidth": "110px"},
	            {"mData": "OPTIN", "sName": 'Optin', "sTitle": 'Date Subscribed', "bSortable": false, "sClass": "optin", "sWidth": "190px"},
	            {"mData": actionCol, "sName": 'Optin', "sTitle": 'Action', "bSortable": false, "sClass": "optin", "sWidth": "60px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupDetailsForDatatableNew'+strAjaxQuery,
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

				aoData.push(
		            { "name": "INPGROUPID", "value": GroupID}
	            );
	            aoData.push(
		            { "name": "inpKeyword", "value": keyword}
	            );
				oSettings.jqXHR = $.ajax({
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function(data){
			        	if(data.DATALIST.length == 0) {
			        		if(oSettings.iDraw == 1)
			        			$('#subcriberListDetail_paginate').hide();
			        		else
			        			$("#subcriberListDetail_previous").trigger('click');
			        	}
			        	fnCallback(data);

			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				$('.link-edit-contact').mask('(000) 000-0000');
			}
		});
	}





	function LoadSelectSubscriber(){

		$.ajax({
			url: "/session/sire/models/cfc/subscribers.cfc?method=GetSubcriberListByUserID"+strAjaxQuery,
			dataType: "json",
			success: function(data){
				var subSelectBox = $('select[name="subcriber-list"]');
				subSelectBox.html('<option value="0">All</option>');
				if(data.RXRESULTCODE == 1){
					data.DATALIST.forEach(function(item){
						subSelectBox.append($('<option></option>').attr('value', item.ID).text(item.NAME));
					});
				} else {
					alertBox(data.MESSAGE, 'alert');
				}
			},
			complete: function(responseText){

			}
		});

	};

	LoadSelectSubscriber();



	$('select[name="subcriber-list"]').on('change', function(event){
		var subID = $(this).val();
		InitSubcriberListDS(subID);

		var dateStart = $('input[name="dateStart"]').data('date-start');
		var dateEnd = $('input[name="dateEnd"]').data('date-end');
		cb(dateStart, dateEnd);
	});




	$('body').on("click", ".download-sub-new", function(event){
		var subID = $(this).data('id');

		window.location.href="/session/sire/models/cfm/export-subcriber-list.cfm?subid="+subID;
	});





	var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
        var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });
        $('input[name="dateStart"]').data('date-start', start);
        $('input[name="dateEnd"]').data('date-end', end);
        var inpGroupId = $('select[name="subcriber-list"]').val();
        $.ajax({
            url: "/session/sire/models/cfc/subscribers.cfc?method=GetStatistic"+strAjaxQuery,
            method: "POST",
            dataType: "json",
            data: {
            	inpGroupId: inpGroupId,
                inpStartDate: dateStart,
                inpEndDate: dateEnd
            },
            success: function(data){

                var series = [];
                var months = [];

                data.DataList.forEach(function(item){
                	var dayData = '';

					switch(item.format){
                    	case "day": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "week": {
                    		months.push(moment(item.Time).format('MMM DD'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    	case "month": {
                    		months.push(moment(item.Time).format('MMM DD YYYY'));
                    		dayData =  moment(item.Time).format('MMMM DD, YYYY');
                    	}
                    	break;
                    }
                    series.push({meta: dayData, value: item.Value});

                });

                 var chart_data = {labels: months,series:[ series]};
                /* Set some base options (settings will override the default settings in Chartist.js *see default settings*). We are adding a basic label interpolation function for the xAxis labels. */
				var options = {
					//fullWidth: true,
					chartPadding: {
						right: 60
					},
					axisY: {
						showGrid: false,
						offset: 20,
						onlyInteger: true,
						position: 'start',
					},
					axisX: {
						labelOffset: {
					      x: 1,
					      y: 1
					    }
					},
					plugins: [
					    Chartist.plugins.tooltip({
					    	currency: 'Subscribers: ',
						    class: 'chart-tooltip-custom',
						    appendToBody: true
					    })
					],
					low: 0,
					height: '300px',
					lineSmooth: false,
					classNames: {
					    chart: 'ct-chart-line',
					    //label: 'ct-label-custom',
					    labelGroup: 'ct-labels',
					    series: 'ct-series',
					    line: 'ct-line',
					    point: 'ct-point',
					    area: 'ct-area',
					    grid: 'ct-grid-custom',
					    gridGroup: 'ct-grids',
					    gridBackground: 'ct-grid-background',
					    vertical: 'ct-vertical',
					    horizontal: 'ct-horizontal',
					    // start: 'ct-start',
					    // end: 'ct-end'
					 }
				};

                new Chartist.Line('#highchart', chart_data, options);

    //             new Chartist.Line('#highchart', {
				// 	labels: months,
				// 	series: [
				// 		series
				// 	],
				// }, {
				// 	fullWidth: true,
				// 	chartPadding: {
				// 		right: 60
				// 	},
				// 	axisY: {
				// 		showGrid: false,
				// 		offset: 20,
				// 		onlyInteger: true,
				// 		position: 'start',
				// 		// type: function(){
				// 		// 	console.log(arguments);
				// 		// }
				// 	},
				// 	axisX: {
				// 		labelOffset: {
				// 	      x: 1,
				// 	      y: 1
				// 	    }
				// 	},
				// 	height: '400px',
				// 	lineSmooth: false,
				// 	classNames: {
				// 	    chart: 'ct-chart-line',
				// 	    label: 'ct-label-custom',
				// 	    labelGroup: 'ct-labels',
				// 	    series: 'ct-series',
				// 	    line: 'ct-line',
				// 	    point: 'ct-point',
				// 	    area: 'ct-area',
				// 	    grid: 'ct-grid-custom',
				// 	    gridGroup: 'ct-grids',
				// 	    gridBackground: 'ct-grid-background',
				// 	    vertical: 'ct-vertical',
				// 	    horizontal: 'ct-horizontal',
				// 	    start: 'ct-start',
				// 	    end: 'ct-end'
				// 	 }

				// });

                $('.highcharts-credits').hide();
            }
        });
    }

    var dateRangerPicker = $('.reportrange').daterangepicker({
        startDate: start,
        endDate: end,
         "dateLimit": {
        	"years": 1
    	},
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);



    cb(start, end);

    $('.select2').select2({
    	width: 'auto',
    	theme: 'bootstrap'
    });

    ChangeShortCodeCallback = function () {
        InitSubcriberListDS();
        LoadSelectSubscriber();

        var dateStart = $('input[name="dateStart"]').data('date-start');
        var dateEnd = $('input[name="dateEnd"]').data('date-end');
        cb(dateStart, dateEnd);
    };

    $('#subcriberListDetail').on('click', '.subscriber-delete', function(event){
		event.preventDefault();
		var self = $(this);
		var contactString = self.parents('tr').first().find('a.link-edit-contact').text();
		var contactType = 3;
		//var groupId = subcriberListId;
		var groupId = $('#tblSubcriberList').find('.active').attr('data-id');
		var contactId = self.attr('data-id');

		if(groupId > 0 && contactId > 0){
				bootbox.dialog({
				title: 'Delete a subscriber',
				message: 'Are you sure you want to delete the contact <b> '+ contactString +'<b/>?',
				buttons: {
					'Remove From Current List': {
						title: 'Remove From Current List',
						className: 'btn btn btn-medium btn-success-custom',
						callback: function() {
							$('#processingPayment').show();
					        $.ajax({
								type: "POST",
								url: '/session/sire/models/cfc/subscribers.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								//url: '/session/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					            dataType: 'json',
					            data: {
					            	INPCONTACTSTRING: contactString,
									INPGROUPID: groupId,
					            	INPCONTACTID: contactId,
					            	//INPSHORTCODE: shortCode
				            	},
				            	error: function() {
				            		$('#processingPayment').hide();
				            	},
				            	complete: function() {
				            		$('#processingPayment').hide();
				            	},
					        	success: function(d2) {
					        		$('#processingPayment').hide();
				                    if(parseInt(d2.RXRESULTCODE) == 1){
				                    	$('#tblSubcriberList_wrapper').find('.paginate_text').trigger('keyup');
										$('#subcriberListDetail').DataTable().fnDraw(false );
										// $('#tblSubcriberList').DataTable().fnDraw(false );
				                    }
				                    else{
				                    	bootbox.alert(d2.MESSAGE);
				                    }
					            }
					    	});
						}
					},
					'Remove From All Lists': {
						title: 'Remove From All Lists',
						className: 'btn btn btn-medium btn-success-custom',
						callback: function() {
							$('#processingPayment').show();
					        $.ajax({
								type: "POST",
								url: '/session/sire/models/cfc/subscribers.cfc?method=RemoveContactFromAllGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								//url: '/session/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					            dataType: 'json',
					            data: {
					            	INPCONTACTSTRING: contactString,
									INPGROUPID: groupId,
					            	INPCONTACTID: self.data('contact-id'),
					            	//INPSHORTCODE: shortCode
				            	},
				            	error: function() {
				            		$('#processingPayment').hide();
				            	},
				            	complete: function() {
				            		$('#processingPayment').hide();
				            	},
					        	success: function(d2) {
					        		$('#processingPayment').hide();
				                    if(parseInt(d2.RXRESULTCODE) == 1){
										$('#tblSubcriberList_wrapper').find('.paginate_text').trigger('keyup');
										$('#subcriberListDetail').DataTable().fnDraw(false );
				                    }
				                    else{
				                    	bootbox.alert(d2.MESSAGE);
				                    }
					            }
					    	});
						}
					},
					cancel: {
						title: 'Cancel',
						className: 'btn btn btn-medium btn-back-custom'
					}
				}
			});
		}
		else{
			bootbox.alert('Invalid subcriber list');
		}

	});


})(jQuery);

(function($){

	$("#subcriberListDetail").on("click", ".lnk-edit-contact", function(event) {
		var contactId = $(this).data("id");
		$.ajax({
			url: "/session/sire/models/cfc/subscribers.cfc?method=getContactStringData" + strAjaxQuery,
			method: "POST",
			dataType: "json",
			data: { inpContactId: contactId },
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#ContactFormEdit .modal-body").html(data.FORM);
				$("#ContactFormEdit").modal('show');
			}
		}).always(function() {
			$("#processingPayment").hide();
		});
	});

	$("#subscribed_contact_form").submit(function(event){
		event.preventDefault();
		var self = $(this);

		if(self.validationEngine('validate', { promptPosition : "topLeft", scroll: false, focusFirstField : true })){
			$.ajax({
				url: '/session/sire/models/cfc/subscribers.cfc?method=saveContactStringData' + strAjaxQuery,
				type: 'POST',
				data: self.serialize(),
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) > 0) {
					$("#ContactFormEdit").modal('hide');
					alertBox(data.MESSAGE, 'Edit contact');
				} else {
					alertBox(data.MESSAGE);
				}
			})
			.fail(function(e, msg) {
				console.log(msg);
			})
			.always(function() {
				$("#processingPayment").hide();
			});
		}
	});


})(jQuery);

// https://awsqa.siremobile.com&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&toSend=false&INPGROUPID=263
