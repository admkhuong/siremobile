(function($){
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$('#unsubscribe').click(function(event){
		event.preventDefault();
		bootbox.dialog({
			message:'Are you sure you want to unsubscribe this phone number?',
			title: "Unsubscribe",
			buttons: {
				success: {
					label: "Unsubscribe",
					className: "btn btn-medium btn-success-custom",
					callback: function() {
						var self = $('#unsubscribe');
						if (self.prop('unning') !== true) {
							self.prop('unning', true);
							$('<img src="/session/sire/images/loading.gif" />').insertBefore(self);
							var contactString = self.data('contact-string');

							$.ajax({
								url: '/session/sire/models/cfc/subscribers.cfc?method=unsubscribe&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								type: 'POST',
								dataType: 'json',
								data: {'contactString': contactString},
								beforeSend: function(){
									$('#processingPayment').show();
								},
								complete: function(){
									self.prev().remove();
									self.prop('unning', false);
									$('#processingPayment').hide();
								},
								error: function( jqXHR, textStatus, errorThrown ) {
									errorSave('Unsubscribe', 'We\'re Sorry! Unable to complete unsubscribe at this time.');
								},
								success: function( data, textStatus, jqXHR ) {
									if (data && data.status == 1) {
										$('#SubscriberState').text('Unsubscribed');
										$('#unsubscribeView').text(data.optOutDate);
									} else {
										if (data && data.msg)
											errorSave('Unsubscribe', data.msg);
										else {
											errorSave('Unsubscribe', 'We\'re Sorry! Unable to complete unsubscribe at this time.');
										}
									}
								}
							});
						}

					}
				},
				danger: {
					label: "Cancel",
					className: "btn btn-primary btn-back-custom",
					callback: function() {

					}
				},
			}
		});
	});

	$('.control-cf-view .btn-cf-edit').click(function(event){
		event.preventDefault();
		var self = $(this);
		var view = self.parents('.control-cf-view').first();
		var edit = view.next('.control-cf-edit');
		view.hide();
		edit.show();
	});

	$('.control-cf-btns .btn-cancel').click(function(event){
		event.preventDefault();
		var self = $(this);
		var edit = self.parents('.control-cf-edit').first();
		var view = edit.prev('.control-cf-view');
		edit.hide();
		view.show();
		edit.find('.form-control').prop('readonly', false).removeClass('input-loading');
		self.prev().prop('disabled', true);
		self.parent('.control-cf-btns').show();
	});

	$('.control-cf-edit .form-control').on('input', function(){
		var self = $(this);
		var btnSave = self.parents('.control-cf-edit').first().find('.control-cf-btns .btn-save');
		if (self.attr('value') != self.val()) {
			btnSave.prop('disabled', false);
		} else {
			btnSave.prop('disabled', true);
		}
	}).keyup(function(event){
		var self = $(this);
		var btnWrapper = self.parents('.control-cf-edit').first();
		if(event.keyCode == 13) {
			btnWrapper.find('.control-cf-btns .btn-save').click();
		} else if(event.keyCode == 27) {
			btnWrapper.find('.control-cf-btns .btn-cancel').click();
		}
	});

	$('.control-cf-btns .btn-save').click(function(){
		var self = $(this);
		var btnWrapper = self.parent('.control-cf-btns').hide();
		self.prop('disabled', true);
		var _input = self.parents('.control-cf-edit').first().find('.form-control').prop('readonly', true).addClass('input-loading');
		var cfValue = _input.val();
		var cfvId = _input.data('cfv-id');
		var contactId = $('#contactid').val();

		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=updateCFV&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {
				'contactId': contactId, 
				'fieldId': cfvId, 
				'fieldValue': cfValue
			},
			beforeSend: function(){
				$('#processingPayment').show();
			},
			complete: function(){
				btnWrapper.show();
				$('#processingPayment').hide();
			},
			error: function( jqXHR, textStatus, errorThrown ) {
				errorSave('Save custom data field', 'We\'re Sorry! Unable to complete save at this time.');
			},
			success: function( data, textStatus, jqXHR ) {
				if (data && data.status == 1) {
					var edit = self.parents('.control-cf-edit').first();
					var view = edit.prev('.control-cf-view');
					view.find('.control-view').text(cfValue != '' ? cfValue : 'N/A');
					edit.hide();
					view.show();
					edit.find('.form-control').removeClass('input-loading').attr('value', cfValue).prop('readonly', false);
					self.prop('disabled', true);
					btnWrapper.show();
				} else {
					if (data && data.msg)
						errorSave('Save  custom data field', data.msg);
					else {
						errorSave('Save custom data field', 'We\'re Sorry! Unable to complete save at this time.');
					}
				}
			}
		});
	});

})(jQuery);