
/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
var troubleCommentId = 0;
GetListComment = function (ticketId){
    $("#comments-list").html('');    
        var actionBtn = function (object) {
            var strReturn = '';
            strReturn = '<a href="" data-id="' + object.ID+'" data-comment-text="'+ object.CONTENT +'" class="edit-comment" title="Edit">Edit</a> | <a data-id="'+object.ID+'" class="delete-comment">Delete</a>';
            return strReturn;
        }     

    var table = $('#comments-list').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "CONTENT", "sName": 'Comments', "sTitle": '', "bSortable": false, "sClass": "", "sWidth": ""},                
            {"mData": "USERFULLNAME", "sName": '', "sTitle": '', "bSortable": false, "sClass": "", "sWidth": "150px"},                
            {"mData": "CREATEDATE", "sName": 'Date Time', "sTitle": '', "bSortable": false, "sClass": "", "sWidth": "150px"},                
            {"mData": actionBtn, "sName": 'Action', "sTitle": '<button type="button" class="btn green-gd btn-add-comment">Add Comment</button>', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
        ],
        "sAjaxDataProp": "ListTroubleTicketComment",
        "sAjaxSource": '/session/sire/models/cfc/troubleTicket/comment.cfc?method=GetListComments'+strAjaxQuery,
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {						
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
            
            aoData.push(
                { "name": "inpTicketId", "value": ticketId}
            );
            aoData.push(
                { "name": "inpSkipPaging", "value": "1"}
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                }
            });
        },
        "fnDrawCallback": function(oSettings) {
            var paginateBar = this.siblings('div.dataTables_paginate');
            if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                paginateBar.find('input.paginate_text').val("1");
            }
            if(oSettings._iRecordsTotal < 1){
                paginateBar.hide();
            } else {
                paginateBar.show();
            }
        }
    });
}
// function edit/add new comment
function SaveComment(commentId,content)
{   
    troubleCommentId = 0;
    if ($('#form-edit-comment').validationEngine('validate', {scroll: false, focusFirstField : false})) {          

        $.ajax({
            url: '/session/sire/models/cfc/troubleTicket/comment.cfc?method=InsertUpdateComment'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpId:commentId,
                inpTicketId:ticketId,
                inpContent: content
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();
                if(commentId ==0)
                {alertBox('Add Comment Fail','Add Comment Fail',''); }
                else
                {alertBox('Update Comment Fail','Update Comment Fail',''); }

            },
            success: function(data) {
                $('#processingPayment').hide();
                $("#TicketComment").val('');
                if (parseInt(data.RXRESULTCODE) == 1) {
                    $("#edit-comment").modal('hide');                                            
                    GetListComment(ticketId); 
                    GetListHistory(ticketId);
                } else {                        
                    if(commentId ==0)
                    {alertBox('Add Comment Fail','Add Comment Fail',''); }
                    else
                    {alertBox('Update Comment Fail','Update Comment Fail',''); }
                }   
                                   
            }
        });
    }
    else
    {                      
        return false;
    }    
}
// function delete comment
function DeleteComment(commentId)
{        
    $.ajax({
        url: '/session/sire/models/cfc/troubleTicket/comment.cfc?method=DeleteCommentByID'+strAjaxQuery,
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            inpId:commentId,
            inpTicketId: ticketId               
        },
        beforeSend: function(){
            $('#processingPayment').show();
        },
        error: function(jqXHR, textStatus, errorThrown) {            
            $('#processingPayment').hide();               
            alertBox('Delete Comment Fail','Delete Comment Fail',''); 

        },
        success: function(data) {
            $('#processingPayment').hide();            
            if (parseInt(data.RXRESULTCODE) == 1) {                    
                GetListComment(ticketId); 
                GetListHistory(ticketId);
            } else {                        
                alertBox(data.MESSAGE,'Delete Comment Fail','');
            }                       
        }
    });
}

$(document).ready(function() {  

    $(document).on('click', '.edit-comment', function (e) {      
        e.preventDefault();            
        var commentId = $(this).data("id");
        $("#edit-comment #commentId").val(commentId);        
        $("#edit-comment #commentText").val($(this).data("comment-text").replace(/<br>/gi,'\n'));
        
        
        $("#edit-comment #modal-tit").text("EDIT COMMENT");
        $("#edit-comment").modal("show");
    });      
    $(document).on('click', '.btn-add-comment ', function (e) { 
        $("#edit-comment").find("#commentId").val(0);
        $("#edit-comment #modal-tit").text("ADD COMMENT");
        $("#edit-comment #commentText").val("");        
        $("#edit-comment").modal("show");
    });     

    // Save comment 
    $("#form-edit-comment").on( "submit", function(e) {           
        e.preventDefault();  
        var modal = $("#edit-comment");        
        var commentId= modal.find("#commentId").val();
        var content= modal.find("#commentText").val();            
        SaveComment(commentId,content);        
    });  

    // delete comment
    $("#comments-list").on("click", ".delete-comment", function(event){        
        troubleCommentId=$(this).data("id");
        confirmBox('Are you sure you want to delete this comment?', 'Delete a comment', function(){
            DeleteComment(troubleCommentId);
        });
    });
});    

