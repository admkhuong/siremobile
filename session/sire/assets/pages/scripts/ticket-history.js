
/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
// get history list
GetListHistory = function (ticketId){
    $("#history-list").html('');       

    var table = $('#history-list').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "bPaginate": false, 
        "sDom": 'rt',
        "sPaginationType": "input",
        "bLengthChange": false,                
        "bAutoWidth": false,             
        "aoColumns": [            
            
            {"mData": "DATE", "sName": 'Date Time', "sTitle": 'Date Time', "bSortable": false, "sClass": "", "sWidth": "140px"},                
            {"mData": "USER_FULL_NAME", "sName": 'User', "sTitle": 'User', "bSortable": false, "sClass": "", "sWidth": "120px"},                            
            {"mData": "NOTES", "sName": 'Notes', "sTitle": 'Notes', "bSortable": false, "sClass": "", "sWidth": ""},                
            
        ],
        "sAjaxDataProp": "aaData",
        "sAjaxSource": '/session/sire/models/cfc/troubleTicket/history.cfc?method=GetListHistory'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
            
            aoData.push(
                { "name": "inpTicketId", "value": ticketId}
            );
            aoData.push(
                { "name": "inpSkipPaging", "value": "1"}
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);                    
                }
            });
        },
        "fnDrawCallback": function() {
            
        },
        "aaSorting": [[1,'desc']]
    });
}
