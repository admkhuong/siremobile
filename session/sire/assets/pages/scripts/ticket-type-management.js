(function() {

    /*Ajax query string default param*/
    var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
    
    fnGetDataList = function (type){
        $("#ticket-type-list").html('');    

        var actionBtn = function (object) {
            var strReturn = '<a href="" data-id="'+object.ID+'" data-type="'+object.TYPE+'"  class="edit-ticket-type" data-toggle="modal" data-action="edit" data-target="#edit-ticket-type"  title="Edit">Edit</a> | <a href=""  data-id="'+object.ID+'" class="delete-ticket-type">Delete</a>';
            return strReturn;
        }

        var table = $('#ticket-type-list').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "TYPE", "sName": 'Type', "sTitle": 'Type', "bSortable": true, "sClass": "", "sWidth": ""},                
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=GetListTicketType'+strAjaxQuery,
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                
                aoData.push(
                    { "name": "inpType", "value": type}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },
            "aaSorting": [[1,'desc']]
        });
    }
    function SaveTicketType(inpId)
    {        
        if ($('#form-edit-ticket-type').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var inpType=$("#ticket-type-name").val();            

            $.ajax({
                url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=InsertUpdateTicketType'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpId:inpId,
                    inpType: inpType
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    if(inpId ==0)
                    {alertBox('Define Ticket Type Fail','Define Ticket Type Fail',''); }
                    else
                    {alertBox('Update Ticket Type Fail','Update Ticket Type Fail',''); }

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (parseInt(data.RXRESULTCODE) == 1) {
                        $("#edit-ticket-type").modal('hide');
                        $(".modal-backdrop").fadeOut();
                        fnGetDataList(); 
                    } else {                        
                        alertBox(data.MESSAGE,'Define Ticket Type Fail','');
                    }                       
                }
            });
        }
        else
        {                           
            return false;
        }
    }
    function DeleteTicketType(inpId)
    {        
        $.ajax({
            url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=DeleteTicketTypeByID'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpId:inpId                
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();               
                alertBox('Delete Ticket Type Fail','Delete Ticket Type Fail',''); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (parseInt(data.RXRESULTCODE) == 1) {                    
                    fnGetDataList(); 
                } else {                        
                    alertBox(data.MESSAGE,'Delete Ticket Type Fail','');
                }                       
            }
        });
    }
    /* Init datatable */
    $(document).ready(function() {
        fnGetDataList();        
        $( "#searchbox" ).on( "keypress", function(e) { 
            if (e.keyCode == 13)
            {
                fnGetDataList($( this ).val()); 
            }            
        });            
        $("#form-edit-ticket-type").on( "submit", function(e) {   
            e.preventDefault();          
            var ticketTypeId= $("#ticketTypeId").val();
            SaveTicketType(ticketTypeId);             
        });  
        var table = $('#ticket-type-list').DataTable();     
        $('#ticket-type-list tbody').on('click', '.edit-ticket-type', function (e) {      
            //e.preventDefault();        
            $("#modal-tit").text("EDIT TICKET TYPE");
        });      
        $(document).on('click', '.pop-ticket-type ', function (e) {      
            //e.preventDefault();        
            $("#modal-tit").text("NEW TICKET TYPE");
        });      
        
        //
        $('#edit-ticket-type').on('show.bs.modal', function (event) {            
            var button = $(event.relatedTarget) // Button that triggered the modal
            var modal = $(this)                        

            modal.find("#ticketTypeId").val(button.data('id'));    
            modal.find("#ticket-type-name").val(button.data('type'));            
            
        })
        
        $("#ticket-type-list").on("click", ".delete-ticket-type", function(event){
            event.preventDefault();
            var  inpId=  $(this).data("id");
            confirmBox('Are you sure you want to delete this ticket type?', 'Delete a ticket type', function(){
                DeleteTicketType(inpId);
            });
        });

    });    

})();