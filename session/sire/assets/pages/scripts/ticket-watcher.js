

/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
// get list watcher
GetListWatcher = function (ticketId,watcherName){    
    $("#tblWatcherList").html('');    

    var actionBtn = function (object) {
        var strReturn = '<a href="" data-id="'+object.ID+'" data-type="'+object.TYPE+'" data-name="'+object.NAME+'"  class="edit-ticket-watcher" data-toggle="modal" data-action="edit" data-target="#edit-ticket-watcher"  title="Edit">Edit</a> | <a href=""  data-id="'+object.ID+'" class="delete-ticket-watcher">Delete</a>';
        return strReturn;
    }
    
    var table = $('#tblWatcherList').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            /* {"mData": "TYPE", "sName": 'Type', "sTitle": 'Type', "bSortable": true, "sClass": "", "sWidth": ""},                 */
            {"mData": "NAME", "sName": 'Email/Phone', "sTitle": 'Email/Phone', "bSortable": true, "sClass": "", "sWidth": ""},                
            {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
        ],
        "sAjaxDataProp": "aaData",
        "sAjaxSource": '/session/sire/models/cfc/troubleTicket/watcher.cfc?method=GetListWatcher'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
            aoData.push(
                { "name": "inpName", "value": watcherName}
            );
            aoData.push(
                { "name": "inpTicketId", "value": ticketId}
            );
            

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                }
            });
        },
        "aaSorting": [[1,'desc']]
    });
}
// save watcher
function SaveWatcher(watcherId,watcherType, watcherName)
{        
    if ($('#form-edit-ticket-watcher').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
        var inpType=$("#watcher-name").val();            
        $.ajax({
            url: '/session/sire/models/cfc/troubleTicket/watcher.cfc?method=InsertUpdateWatcher'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpId:watcherId,
                inpTicketId: ticketId,
                inpWatcherType: watcherType,
                inpWatcherName: watcherName
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();
                if(inpId ==0)
                {alertBox('Define Ticket Watcher Fail','Define Ticket Watcher Fail',''); }
                else
                {alertBox('Update Ticket Watcher Fail','Update Ticket Watcher Fail',''); }

            },
            success: function(data) {
                $('#processingPayment').hide();
                if (parseInt(data.RXRESULTCODE) == 1) {
                    $("#edit-ticket-watcher").modal('hide');
                    //$(".modal-backdrop").fadeOut();
                    GetListWatcher(ticketId); 
                } else {                        
                    alertBox(data.MESSAGE,'Define Ticket Watcher Fail','');
                }                       
            }
        });
    }
    else
    {                           
        return false;
    }
}
// delete watcher
function DeleteWatcher(ticketId,watcherId)
{        
    $.ajax({
        url: '/session/sire/models/cfc/troubleTicket/watcher.cfc?method=DeleteWatcherByID'+strAjaxQuery,
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            inpTicketID:ticketId,
            inpWatcherID:watcherId                
        },
        beforeSend: function(){
            $('#processingPayment').show();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#processingPayment').hide();               
            alertBox('Delete Ticket Watcher Fail','Delete Ticket Watcher Fail',''); 

        },
        success: function(data) {
            $('#processingPayment').hide();
            if (parseInt(data.RXRESULTCODE) == 1) {                    
                GetListWatcher(ticketId); 
            } else {                        
                alertBox(data.MESSAGE,'Delete Ticket Watcher Fail','');
            }                       
        }
    });
}

$(document).ready(function() {
         
       
    $("#form-edit-ticket-watcher").on( "submit", function(e) {   
        e.preventDefault();          
        var watcherId= $("#form-edit-ticket-watcher #watcherId").val();
        var watcherType= $("#form-edit-ticket-watcher #watcher-type").val();
        var watcherName= $("#form-edit-ticket-watcher #watcher-name").val();
        SaveWatcher(watcherId,watcherType, watcherName);             
    });  
    
    $('#tblWatcherList tbody').on('click', '.edit-ticket-watcher', function (e) {      
        //e.preventDefault();        
        $("#modal-tit").text("EDIT WATCHER");
    });      
    $(document).on('click', '.pop-ticket-watcher ', function (e) {      
        //e.preventDefault();        
        $("#modal-tit").text("NEW WATCHER");
    });      
        
    $('#edit-ticket-watcher').on('show.bs.modal', function (event) {            
        var button = $(event.relatedTarget) // Button that triggered the modal
        var modal = $(this)                        
        modal.find("#watcherId").val(button.data('id'));  
        var type=  button.data('type') || 1;        
        modal.find("#watcher-name").removeClass("validate[custom[email]]");
        modal.find("#watcher-name").unmask().val(button.data('name'));            
        modal.find("#watcher-type").val(type);                 
                
        if(type==2)
        {
            modal.find("#watcher-name").mask("(000) 000-0000");            
        }
        else
        {
            modal.find("#watcher-name").addClass("validate[custom[email]]");
        }                        
    })
    
    $("#tblWatcherList").on("click", ".delete-ticket-watcher", function(event){
        event.preventDefault();
        var  watcherId=  $(this).data("id");
        confirmBox('Are you sure you want to delete this watcher?', 'Delete a watcher', function(){
            DeleteWatcher(ticketId,watcherId);
        });
    });
    $( "#form-list-watcher #searchbox" ).on( "keypress", function(e) { 
        if (e.keyCode == 13)
        {            
            GetListWatcher(ticketId,$( this ).val()); 
        }            
    });   

    $(document).on("change","#watcher-type",function(){        
        $("#form-edit-ticket-watcher #watcher-name").unmask();        
        $("#watcher-name").removeClass("validate[custom[email]]");
        if($(this).val()==1){
            $("#watcher-name").addClass("validate[custom[email]]");
            
        }
        else
        {            
            $("#form-edit-ticket-watcher #watcher-name").mask("(000) 000-0000");
        }
    });

});    

