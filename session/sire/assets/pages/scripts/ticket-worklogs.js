
/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

GetListWorkLog = function (ticketId){
    $("#worklogs-list").html('');    
    var colTotalTime = function (object) {            
        var strReturn = object.TOTALTIME +  (object.TOTALTIME == 1 ? " hour": " hours");
        return strReturn;
    }
    var colFullName = function (object) {
        var strReturn = object.USER_FULL_NAME + " Spend: ";
        return strReturn;
    }
    var actionBtn = function (object) {
        var strReturn = '';
        if(object.SHOWACTION=="Y"){
            strReturn = '<a href="" data-id="'+object.ID+'" data-start-date-time="'+object.PICKER_DATE+'" data-total-time="'+object.TOTALTIME+'"  class="edit-work-log" title="Edit">Edit</a> | <a href=""  data-id="'+object.ID+'" class="delete-work-log">Delete</a>';
        }
        
        return strReturn;
    }

    var table = $('#worklogs-list').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "bPaginate": false, 
        "sDom": 'rt',
        "sPaginationType": "input",
        "bLengthChange": false,
        
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "START_DATE", "sName": 'Logged', "sTitle": 'Logged', "bSortable": false, "sClass": "", "sWidth": ""},                
            
            {"mData": colFullName, "sName": '', "sTitle": '', "bSortable": false, "sClass": "", "sWidth": "150px"},                
            {"mData": colTotalTime, "sName": '', "sTitle": '<div id="total-time-loged">0 h</div>', "bSortable": false, "sClass": "", "sWidth": "100px"},                
            {"mData": actionBtn, "sName": 'Action', "sTitle": '<button type="button" class="btn green-gd btn-add-work-log">Add Work Log</button>', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
        ],
        "sAjaxDataProp": "aaData",
        "sAjaxSource": '/session/sire/models/cfc/troubleTicket/work-log.cfc?method=GetListWorkLog'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {           
            aoData.push(
                { "name": "inpTicketId", "value": ticketId}
            );
            aoData.push(
                { "name": "inpSkipPaging", "value": "1"}
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);      
                    $("#total-time-loged").html(data.TOTALTIME +  (data.TOTALTIME == 1 ? " hour": " hours") );
                }
            });
        },
        "fnDrawCallback": function() {
            //$("#worklogs-list thead").addClass("hidden");
        },
        "oLanguage": {
            "sEmptyTable": "No work has yet been logged on this issue."
        },
        "aaSorting": [[1,'desc']]
    });
}

function SaveWorkLog(worklogId,startDateTime,spendTime)
{         
    if ($('#form-edit-work-log').validationEngine('validate', {scroll: false, focusFirstField : false})) {          
        // Validate Time spend first                     
        if(parseInt(spendTime) >= 100){
            var message='Time spend must be less than 100 Hours.';
            var title= "Add Work Log";
            alertBox(message, title);       
        }
        else{
            $.ajax({
                url: '/session/sire/models/cfc/troubleTicket/work-log.cfc?method=InsertUpdateWorkLog'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpId: worklogId,
                    inpTicketId: ticketId,
                    inpStartDate: startDateTime,
                    inpSpendTime: spendTime
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    if(worklogId ==0)
                    {alertBox('Add Work Log Fail','Add Work Log Fail',''); }
                    else
                    {alertBox('Update Work Log Fail','Update Work Log Fail',''); }

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (parseInt(data.RXRESULTCODE) == 1) {
                        $("#edit-work-log").modal('hide');                        
                        GetListWorkLog(ticketId); 
                        GetListHistory(ticketId);
                    } else {                        
                        if(worklogId ==0)
                        {alertBox('Add Work Log Fail','Add Work Log Fail',''); }
                        else
                        {alertBox('Update Work Log Fail','Update Work Log Fail',''); }
                    }                       
                }
            });
        }        
    }
    else
    {                      
        return false;
    }
}
function DeleteWorkLog(workLogId)
{        
    $.ajax({
        url: '/session/sire/models/cfc/troubleTicket/work-log.cfc?method=DeleteWorkLogByID'+strAjaxQuery,
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            inpId:workLogId ,
            inpTicketId: ticketId          
        },
        beforeSend: function(){
            $('#processingPayment').show();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#processingPayment').hide();               
            alertBox('Delete Work Log Fail.','Delete Work Log Fail',''); 

        },
        success: function(data) {
            $('#processingPayment').hide();
            if (parseInt(data.RXRESULTCODE) == 1) {                    
                GetListWorkLog(ticketId); 
                GetListHistory(ticketId);
            } else {                        
                alertBox(data.MESSAGE,'Delete Work Log Fail','');
            }                       
        }
    });
}
/* Init datatable */
$(document).ready(function() {    
    $('#datetimepicker').datetimepicker({        
        format: 'MM/DD/YYYY HH:mm'
        // showClose: true       
    });  
        
    $("#form-edit-work-log").on( "submit", function(e) {   
        e.preventDefault();  
        var modal = $("#edit-work-log");        
        var worklogId= modal.find("#workLogId").val();
        var startDateTime= modal.find("#date-start").val();            
        
        var spendTime= modal.find("#totalTime").val();
        
        SaveWorkLog(worklogId,startDateTime,spendTime);             
    });  
    
    $(document).on('click', '.edit-work-log', function (e) {      
        e.preventDefault();    
        var workLogId=$(this).data("id");
        $("#edit-work-log #workLogId").val(workLogId);
        $("#edit-work-log #date-start").val($(this).data("start-date-time"));
        $("#edit-work-log #totalTime").val($(this).data("total-time"));
        
        
        $("#edit-work-log #modal-tit").text("EDIT WORK LOG");
        $("#edit-work-log").modal("show");
    });      
    $(document).on('click', '.btn-add-work-log ', function (e) {      
        $("#edit-work-log #modal-tit").text("ADD WORK LOG");
        $("#edit-work-log #totalTime").val("");        
        $("#edit-work-log #workLogId").val(0);
        // Init default date time
        var today = new Date();
        $('#edit-work-log #date-start').val(moment(today).format('MM/DD/YYYY HH:mm'));

        $("#edit-work-log").modal("show");
    });      
    
    
    
    $("#worklogs-list").on("click", ".delete-work-log", function(event){
        event.preventDefault();
        var workLogId=$(this).data("id");
        confirmBox('Are you sure you want to delete this work log?', 'Delete a work log', function(){
            DeleteWorkLog(workLogId);
        });
    });

});    

