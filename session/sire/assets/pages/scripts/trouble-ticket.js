var ticketId=0;
(function($){
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	$("#PhoneNumber").mask("(000) 000-0000");
	$("#EditPhoneNumber").mask("(000) 000-0000");    


	var start = moment();
    var end = moment();
    var startTime = '';
    var endTime = '';

	$('.reportrange').daterangepicker({
        "dateLimit": {
            "years": 1
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, GetReport);

    // Get/set data report of daterange
	function GetReport (start, end, rfresh) { 		
        dateStart_crm = start;
        dateEnd_crm = end;                  

        $('.reportrange span.time').html(moment(start).format('MMMM D, YYYY') + ' - ' + moment(end).format('MMMM D, YYYY'));
        var dateStart = JSON.stringify({day: moment(start).format('DD'), month: moment(start).format('MM'), year: moment(start).format('YYYY') });
        var dateEnd = JSON.stringify({day: moment(end).format('DD'), month: moment(end).format('MM'), year: moment(end).format('YYYY') });    	
        startTime = dateStart;
        endTime = dateEnd;
    }    

    $(document).ready(function() {      		   	  
	    var today = new Date();
	    var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);		   		         
	    firstDayOfMonth = new Date(firstDayOfMonth);
        
        var dateStart = JSON.stringify({day: firstDayOfMonth.getDate(), month: firstDayOfMonth.getMonth()+ 1, year: firstDayOfMonth.getFullYear() });
        var dateEnd = JSON.stringify({day: today.getDate(), month: today.getMonth()+1, year: today.getFullYear() });
        
        startTime = dateStart;
        endTime = dateEnd;

        GetReport(firstDayOfMonth, today, 1);

	});

    // Select ticket type by name
    $("select[name='ticket-typeId']").select2({
        placeholder: "Select type",
        allowClear: true,
        ajax: {
            url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=GetListTicketTypeByUserId'+strAjaxQuery,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    inpTypeName: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {                
                return {                                
                    results: $.map(data.OutData, function (item) {
                        return {
                            text: item.TYPE,
                            id: item.ID
                        }
                    })
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        theme: "bootstrap",
        width: 'auto'
    });  
    // On change value when selected a ticket type
    $("#ticket-typeId").change(function() {                
        var placeholderSesion = $('#select2-ticket-typeId-container .select2-selection__placeholder').text();      
        if(placeholderSesion == ""){
            // var renderSelectType = $("#select2-ticket-typeId-container").attr("title");   
            // var res = renderSelectType.split(" - ");                
            // $("#selected-ticket-typeId").val(res[0]);             
            var ticketTypeSelected = $("#ticket-typeId").val();
            $("#selected-ticket-typeId").val(ticketTypeSelected);
        }
        else{
            $("#selected-ticket-typeId").val('');
        }  
    });   

    // Select ticket type by name - mode edit
    $("select[name='edit-ticket-typeId']").select2({
        placeholder: "Select type",
        allowClear: true,
        ajax: {
            url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=GetListTicketTypeByUserId'+strAjaxQuery,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    inpTypeName: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {                
                return {                                
                    results: $.map(data.OutData, function (item) {
                        return {
                            text: item.TYPE,
                            id: item.ID
                        }
                    })
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        theme: "bootstrap",
        width: 'auto'
    });  
    // On change value when selected a ticket type
    $("#edit-ticket-typeId").change(function() {   
        var placeholderSesion = $('#select2-edit-ticket-typeId-container .select2-selection__placeholder').text();    
        if(placeholderSesion == ""){
            // var renderSelectType = $("#select2-edit-ticket-typeId-container").attr("title"); 
            // var res = renderSelectType.split(" - ");     
            // $("#selected-edit-ticket-typeId").val(res[0]);             
            var ticketTypeSelected = $("#edit-ticket-typeId").val();          
            $("#selected-edit-ticket-typeId").val(ticketTypeSelected);             
        }
        else{
            $("#selected-edit-ticket-typeId").val('');
        }  
    });   

    // initi option status
	var StatusCustomData = function(){
	var options   = '<option value= "0" selected>All Status</option>'+
					'<option value="1">Open</option>'+
					'<option value="2">In Progress</option>'+
					'<option value="3">On Hold</option>'+
					'<option value="4">Closed</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	// Init option Priority
	var PriorityCustomData = function(){
	var options   = '<option value= "1" selected>Major</option>'+
					'<option value="2">Minor</option>';
	return $('<select class="filter-form-control form-control"></select>').append(options);
	};

	//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [					
			{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' tt.TicketStatus_ti ', CUSTOM_DATA:StatusCustomData},    
			{DISPLAY_NAME: 'Ticket No', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'TEXT', SQL_FIELD_NAME: ' tt.PKId_bi ' },
			{DISPLAY_NAME: 'Priority', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' tt.TicketPriority_ti ', CUSTOM_DATA:PriorityCustomData},    
			{DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' tt.ContactString_vch '}
		],
		clearButton: true,	
		error: function(ele){			
		},
		
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	// init control
	InitControl();
	
	// Init data view - list all trouble ticket
	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter		
        var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";

        var total = 0;
    	var open = 0;
    	var closed = 0;
    	var inprogress = 0;
    	var onhode = 0;
    	var overdue = 0;

        var actionBtn = function (object) {                 	        
        	var strReturn = '<a href="#" data-id ="'+object.ID+'" data-action="view-trouble-ticket-detail" class="trouble-ticket-action" title="View trouble ticket detail"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'
        			+ '| <a data-id ="'+object.ID+'" class="notify-trouble-ticket" title="Notify trouble ticket" data-target="#TroubleTicketNotifyModal"><i class="fa fa-bell" style="font-size:15px" aria-hidden="true"></i></a>';
        			// + '| <a href="#" data-id ="'+object.ID+'" data-action="report-trouble-ticket" class="trouble-ticket-action" title="View report trouble ticket"></label><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i></a> ';			        	                       	

            return strReturn;
        }     
        var formatPhoneNumber = function (object) {                 	                	
			  return object.PHONE.replace(/(\d{3})(\d{3})(\d{0})/, '($1) $2-$3');
		} 

        var formatOverDue = function(object){
            var formatValue = '';
            if(object.OVERDUE == 0){
                formatValue = object.OVERDUE;
            }          
            else{
                formatValue = object.OVERDUE.toFixed(2) + "h";
            }
            return formatValue;
        }         
		
		//init datatable for trouble ticket listing		
		__tblListTroubleTickets = $('#tblListTroubleTickets').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"mData": "ID", "sName": 'Ticket No', "sTitle": 'Ticket No.', "sWidth": '200px',"bSortable": false,"sClass": "col-center"},
				{"mData": formatPhoneNumber, "sName": 'Phone', "sTitle": 'Phone Number', "sWidth": '250px',"bSortable": false},				                
				{"mData": "STATUS", "sName": 'Status', "sTitle": 'Status', "sWidth": '200px',"bSortable": false,"sClass": "col-center"},
				{"mData": "PRIORITY", "sName": 'Priority', "sTitle": 'Priority', "sWidth": '100px',"bSortable": false,"sClass": "col-center"},
				{"mData": "CREATEDDATE", "sName": 'Created Date', "sTitle": 'Created Date', "sWidth": '250px',"bSortable": false,"sClass": "col-center"},	
				{"mData": "UPDATEDDATE", "sName": 'Updated Date', "sTitle": 'Updated Date', "sWidth": '250px',"bSortable": false,"sClass": "col-center"},	
				{"mData": formatOverDue, "sName": 'Over Due', "sTitle": 'Over Due <img src="../assets/layouts/layout4/img/info.png" class="overdue-formular">', "sWidth": '250px',"bSortable": false,"sClass": "col-center"},				               
                {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "250px"}
			],
			"bAutoWidth": false,
            "sAjaxDataProp": "ListTroubleTicketData",            
			"sAjaxSource": '/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=ListAllTroubleTickets'+strAjaxQuery,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {						
	       	},
		   	"fnDrawCallback": function( oSettings ) {		   		
				var paginateBar = this.siblings('div.dataTables_paginate');
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					paginateBar.find('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal < 1){
					paginateBar.hide();
				} else {
					paginateBar.show();
				}
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering datad				
		       	aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	           	);
	           	if(typeof(customFilterObj) =='undefined'){
	           		var today = new Date();
				    var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);		   		         
				    firstDayOfMonth = new Date(firstDayOfMonth);
			        
			        var dateStart = JSON.stringify({day: firstDayOfMonth.getDate(), month: firstDayOfMonth.getMonth()+ 1, year: firstDayOfMonth.getFullYear() });
			        var dateEnd = JSON.stringify({day: today.getDate(), month: today.getMonth()+1, year: today.getFullYear() });
			        
			        startTime = dateStart;
			        endTime = dateEnd;
	           	}	           	
		       	aoData.push(
		            { "name": "inpStartDate", "value": startTime}
	            );
	            aoData.push(
		            { "name": "inpEndDate", "value": endTime}
	            );	            
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback,
			             complete: function (data) {                         
			              	$("#txtTotal").text(data.responseJSON.CountTotal);
							$("#txtOpen").text(data.responseJSON.CountOpen);
							$("#txtInProgress").text(data.responseJSON.CountInProgress);
							$("#txtOnHold").text(data.responseJSON.CountOnHold);			
							$("#txtClosed").text(data.responseJSON.CountClosed);
							$("#txtOverDue").text(data.responseJSON.CountOverDue);                              
                            $('.overdue-formular').popover({title: "Formular", content: "Over Due = Current Date time - (ETA + Created Date).", placement: "top", trigger:"hover"});                           
					     }      
			 	});
	        },	       
			"fnInitComplete":function(oSettings, json){
				$("#tblListTroubleTickets thead tr").find(':last-child').css("border-right","0px");
			}
	    });
	}

	// on change box filter
	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		InitControl();
    });		

    // on remove search
	$(".glyphicon-remove").click(function() {
	  	var today = new Date();
	    var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);		   		         
	    firstDayOfMonth = new Date(firstDayOfMonth);
        
        var dateStart = JSON.stringify({day: firstDayOfMonth.getDate(), month: firstDayOfMonth.getMonth()+ 1, year: firstDayOfMonth.getFullYear() });
        var dateEnd = JSON.stringify({day: today.getDate(), month: today.getMonth()+1, year: today.getFullYear() });
        
        startTime = dateStart;
        endTime = dateEnd;


        GetReport(firstDayOfMonth, today, 1);
	});
    

    // Session Add new ticket
    var ticket_form = $("#create_ticket_form");

    //Validate engine
    ticket_form.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});
    
    // Submit save new ticket
    ticket_form.submit(function(event){
        event.preventDefault();        
        if (ticket_form.validationEngine('validate', {focusFirstField : true, scroll: false, promptPosition: "topLeft" })) {      
            // Validate ETA first             
            var eTAValue = parseInt($("#TicketETA").val());
            if(eTAValue >= 100){
                var message='ETA must be less than 100 Hours.';
                var title= "Create new ticket";
                alertBox(message, title);       
            }
            else{
                $('.btn').prop('disabled',true);        
                try{
                    // inital object of trouble ticket                                  
                    var ticketTypeId = $("#selected-ticket-typeId").val();
                    // var ticketTypeId =  typeof($("#TicketType option:selected").val())!='undefined'?$("#TicketType option:selected").val(): 0;                        
                    ticketObject = {
                                inpTicketNo: '',
                                inpTicketName: $("#TicketName").val(),
                                inpTicketContactString: $("#PhoneNumber").val(),
                                inpTicketStatusId: $("#TicketStatus option:selected").val(),
                                inpTicketTypeId: ticketTypeId,                            
                                inpTicketPriorityId: $("#TicketPriority option:selected").val(),
                                inpTicketETA: $("#TicketETA").val(),
                                inpDescription: $("#TicketDescription").val()
                    }                
                    // Call ajax to save data
                    $.ajax({
                        method: 'POST',            
                        url:'/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=SaveTroubleTicket'+strAjaxQuery,
                        data: {
                            'inpTroubleTicketObject':JSON.stringify(ticketObject)                        
                        },
                        dataType: 'json',                    
                        beforeSend: function( xhr ) {
                            $('#processingPayment').show();
                            $('.btn').prop('disabled',true);                        
                        },                    
                        error: function(XMLHttpRequest, textStatus, errorThrown) {                        
                            $('#processingPayment').hide();
                            var message='Save information fail';
                            var title= "Create new ticket";
                            alertBox(message, title);                        
                            $('.btn').prop('disabled',false);                        
                        },                   
                        success: function(data) {
                            $('#processingPayment').hide();                                                                       
                            if(parseInt(data.RXRESULTCODE) == 1){
                                var message = data.MESSAGE;
                                var title= "Create new ticket";
                                alertBox(message, title, function () {
                                    if(parseInt(data.RXRESULTCODE) == 1) location.reload();
                                });
                        
                                $('.btn').prop('disabled',false);                           
                            }  
                            else{
                                var message = data.MESSAGE;
                                var title= "Edit trouble ticket";
                                alertBox(message, title, function () {                                
                                });
                                $('.btn').prop('disabled',false);
                            }                                                          
                        }
                    });              
                }
                catch(ex){                
                    $('#processingPayment').hide();
                    var message='Save information fail!';
                    var title= "Create new ticket";
                    alertBox(message, title);
                    $('.btn').prop('disabled',false);               
                }
            }
        }            
    });

    // Get list ticket type and clear data first when completed show popup
    $('#modal-sections').on('show.bs.modal', function () { 
    	//OnloadDropdowTicketType();         
        $("#selected-ticket-typeId").val('');
        $("#select2-ticket-typeId-container").text('');         
        $("#select2-ticket-typeId-container").attr("title",''); 
        $("#select2-ticket-typeId-container").append('<span class="select2-selection__placeholder">Select type</span>');          
    	$("#TicketName").val('');
    	$("#PhoneNumber").val('');
    	$("#TicketETA").val('');
    	$("#TicketDescription").val('');    	
	})

	// On load data to dropdow for ticket type
	// function OnloadDropdowTicketType(){        
	// 	$.ajax({
 //            url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=GetListTicketTypeByUserId'+strAjaxQuery,
 //            async: false,
 //            type: 'post',
 //            dataType: 'json',  
 //            data: null,         
 //            error: function(jqXHR, textStatus, errorThrown) {                     
 //                alertBox('Error when get ticket type!','Opps',''); 
 //            },
 //            success: function(data) {              
 //                if (parseInt(data.RXRESULTCODE) == 1) {  
 //                	var options = ''; 
 //                	if(data.OutData != null){
 //                		for (i = 0; i < data.OutData.length; i++) { 
	// 				    	options += '<option value='+ data.OutData[i].ID +'>'+ data.OutData[i].TYPE +'</option>';					    
	// 					}
 //                	}                 	
 //                	var dropdownTicketTypeHtml = $('<select class="form-control cardholder" id="TicketType" name="TicketType"></select>').append(options); 
 //                	var dropdownTicketTypeEditHtml = $('<select class="form-control cardholder" id="EditTicketType" name="TicketType"></select>').append(options);                    	
	//                 	$(".ticket-type-zone").html(dropdownTicketTypeHtml);
	//                 	$(".edit-ticket-type-zone").html(dropdownTicketTypeEditHtml);	                	
                    
 //                } else {                        
 //                    alertBox(data.MESSAGE,'Opps','');
 //                }                       
 //            }
 //        });
	// }	

	// Open Add new type	
	$(".pop-add-ticket-type").click(function() {		
		$('#new-ticket-type').modal('toggle');	
		$("#ticket-type-name").val('');   
	});

	//Save ticket type
	$("#form-edit-ticket-type").on( "submit", function(e) {   
            e.preventDefault();                      
            SaveTicketType();             
    });  
    // Save ticket type 
	function SaveTicketType()
    {            	 
        if ($('#form-edit-ticket-type').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
            var inpType=$("#ticket-type-name").val();            
            $.ajax({
                url: '/session/sire/models/cfc/troubleTicket/ticket-type.cfc?method=InsertUpdateTicketType'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {                    
                    inpType: inpType
                },
                beforeSend: function(){   
                	$('#processingPayment').show();                 
                },
                error: function(jqXHR, textStatus, errorThrown) {   
                 	$('#processingPayment').hide();                                   
                	alertBox("Error when save ticket type!",'Oops','');                               
                },
                success: function(data) {         
                	$('#processingPayment').hide();                                   
                    if (parseInt(data.RXRESULTCODE) == 1) {
                    	//OnloadDropdowTicketType();
                        $("#new-ticket-type").modal('hide');                                         
                    } else {                        
                        alertBox(data.MESSAGE,'Define Ticket Type Fail','');
                    }                       
                }
            });
        }
        else
        {                           
            return false;
        }
    }
	// End Session add new ticket
    // Start Session View ticket details and any action there
    
    // Action of ticket
    $("#tblListTroubleTickets").on("click", ".trouble-ticket-action", function(event){    	
		event.preventDefault();        		
		// Clear on form first		                   
		var action= $(this).data("action");		
		
		if(action=="view-trouble-ticket-detail"){
            // Bind variable ticketId when click show ticket details
			ticketId= $(this).data("id");			
			$('#ViewTroubleTicketModal').modal('toggle');						
		}			      
    });     
    
    // Onload view trouble ticket
    $('#ViewTroubleTicketModal').on('show.bs.modal', function () {             
    	// Load data for dropdow ticket type first          
        //OnloadDropdowTicketType();    	
		// Load coment tab
		GetListComment(ticketId);
        // Load work log tab
		GetListWorkLog(ticketId);
		// Load history tab
		GetListHistory(ticketId);
        // Default active tab comment
        $("#tabComment").click();

        // Load data for details form                
        $.ajax({
            url: '/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=GetTroubleTicketDetailById'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',  
            data: {
                inpTicketId: ticketId
            },         
            error: function(jqXHR, textStatus, errorThrown) {                     
                alertBox('Error when get ticket details!','Opps',''); 
            },
            success: function(data) {                              
                if (parseInt(data.RXRESULTCODE) == 1) {                                          
                    $("#EditTicketNo").text(data.ID);
                    $("#EditTicketName").val(data.TICKETNAME);

                    //$("#EditTicketType").val(data.TICKETTYPEID);                                              
                    if(data.TICKETTYPEID != ''){
                        $("#selected-edit-ticket-typeId").val(data.TICKETTYPEID);  
                        $("#select2-edit-ticket-typeId-container").text(data.TICKETTYPENAME);
                    }    
                    else{
                        $("#selected-edit-ticket-typeId").val('');  
                        $("#select2-edit-ticket-typeId-container").html('<span class="select2-selection__placeholder">Select type</span>');                              
                    }                                                

                    $("#EditTicketStatus").val(data.STATUSID);
                    $("#EditTicketPriority").val(data.TICKETPRIORITYID);                    

                    $("#EditTicketETA").val(data.TICKETETA);                   
                    $("#EditPhoneNumber").val(data.PHONE.replace(/(\d{3})(\d{3})(\d{0})/, '($1) $2-$3')); // just only use to show
                    $("#EditTicketDescription").val(data.DESCRIPTION); 

                    $("#EditTicketCreatedDate").text(data.CREATEDDATE);
                    $("#EditTicketUpdateDate").text(data.UPDATEDDATE); 

                } else {                        
                    alertBox(data.MESSAGE,'Opps','');
                }                       
            }
        });
	})

    // Update trouble ticket     
    var edit_ticket_form = $("#edit-trouble-ticket-form");

    //Validate engine on edit form
    edit_ticket_form.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});
    
    // Submit save trouble ticket    
    edit_ticket_form.submit(function(event){
        event.preventDefault();           
        if (edit_ticket_form.validationEngine('validate', {focusFirstField : true, scroll: false, promptPosition: "topLeft" })) {            
            var eTAValue = parseInt($("#EditTicketETA").val());
            if(eTAValue >= 100){
                var message='ETA must be less than 100 Hours.';
                var title= "Edit trouble ticket";
                alertBox(message, title);                  
            }
            else{
                $('.btn').prop('disabled',true);                                     
                var ticketTypeId = $("#selected-edit-ticket-typeId").val();
                // var ticketTypeId =  typeof($("#EditTicketType option:selected").val())!='undefined'?$("#EditTicketType option:selected").val(): 0;                        
                try{
                    // inital object of trouble ticket
                    ticketObject = {
                                inpTicketNo: ticketId,
                                inpTicketName: $("#EditTicketName").val(),
                                inpTicketContactString: $("#EditPhoneNumber").val(),
                                inpTicketStatusId: $("#EditTicketStatus option:selected").val(),
                                inpTicketTypeId: ticketTypeId,                            
                                inpTicketPriorityId: $("#EditTicketPriority option:selected").val(),
                                inpTicketETA: $("#EditTicketETA").val(),
                                inpDescription: $("#EditTicketDescription").val()
                    }                
                    // Call ajax to save data
                    $.ajax({
                        method: 'POST',            
                        url:'/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=SaveTroubleTicket'+strAjaxQuery,
                        data: {
                            'inpTroubleTicketObject':JSON.stringify(ticketObject)                        
                        },
                        dataType: 'json',                    
                        beforeSend: function( xhr ) {
                            $('#processingPayment').show();
                            $('.btn').prop('disabled',true);                        
                        },                    
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            $('#processingPayment').hide();
                            var message='Save information fail';
                            var title= "Edit trouble ticket";
                            alertBox(message, title);   

                            $('.btn').prop('disabled',false);                        
                        },                   
                        success: function(data) {
                            $('#processingPayment').hide();       

                            // when edit trouble ticket is success
                            if(parseInt(data.RXRESULTCODE) == 1){
                                var message = data.MESSAGE;
                                var title= "Edit trouble ticket";
                                alertBox(message, title, function () {
                                    if(parseInt(data.RXRESULTCODE) == 1) location.reload();
                                });
                        
                                $('.btn').prop('disabled',false);                           
                            }  
                            else{                                
                                var message = data.MESSAGE;
                                var title= "Edit trouble ticket";
                                alertBox(message, title, function () {                                
                                });
                                $('.btn').prop('disabled',false);
                            }                                                
                        }
                    });              
                }
                catch(ex){
                    $('#processingPayment').hide();                
                    var message='Save information fail!';
                    var title= "Edit trouble ticket";
                    alertBox(message, title);
                    $('.btn').prop('disabled',false);               
                }
            }            
        }
    });

    // End Session View ticket details and any action there
	// Trouble ticket notify form

	$("#notify-phone").mask("(000) 000-0000");
	CountDownCharacterById('notify-email-sub-countdown','notify-email-subject', 255);
	CountDownCharacterById('notify-email-mess-countdown','notify-email-message', 1000);
	CountDownCharacterById('notify-sms-mess-countdown','notify-sms-message', 160);

	function CountDownCharacterById(countId, contentId, limit){
		$('#' + countId).text(limit-$('#'+ contentId).val().length);

		$('#' + contentId).on('keyup', function(){
			$('#' + countId).text(limit-$('#' + contentId).val().length);
		});

		var contentText = document.getElementById(contentId);
		if(contentText) {
			contentText.addEventListener("input", function() {
				if (contentText.value.length == limit) {
					return false;
				} 
				else if (contentText.value.length > limit) {
					contentText.value = contentText.value.substring(0, limit);
				}
			},  false);
		}

		$('#' + contentId).bind('paste', function(e) {
			var elem = $(this);
			setTimeout(function() {
				var text = elem.val();
				if (text.length > limit) {
					elem.val(text.substring(0,limit));
				}
				$('#' + countId).text(limit-parseInt(elem.val().length));
			}, 100);
		});
	}

	$('body').on('click', '.notify-trouble-ticket', function(event) {
		var data = $(this).data('id');
		$('#notify-ticket-id').val(data);
		InitNotifyForm(data);
		$('#TroubleTicketNotifyModal').modal('show');
	});

	function InitNotifyForm(data){
		$.ajax({
			url: '/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=GetTroubleTicketNotifyDetailById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'JSON',
			data: {
				inpTicketId:data
			},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(parseInt(data.RXRESULTCODE) == 1){
					var phone = data.PHONE;
					var cusPhoneNumber = function () {                 	                	
						return phone.replace(/(\d{3})(\d{3})(\d{0})/, '($1) $2-$3');  
					} 
					$('#notify-phone').val(cusPhoneNumber);
					$('#notify-email-address').val(data.EMAIL);
				}
				else{
					bootbox.dialog({
						 message: data.MESSAGE,
						 title: "Trouble ticket details",
						 buttons: {
							 success: {
								 label: "Ok",
								 className: "btn btn-medium btn-success-custom",
								 callback: function() {}
							 }
						 }
					 });
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#processingPayment').hide();
				 bootbox.dialog({
					 message:'Get trouble ticket details failed!',
					 title: "Trouble ticket details",
					 buttons: {
						 success: {
							 label: "Ok",
							 className: "btn btn-medium btn-success-custom",
							 callback: function() {}
						 }
					 }
				 });
			} 
		});
		// Notify Email
		if($('#notify-method').val() == 1){
			$('#notify-email-subject').parents('.form-group').show();
			$('#notify-email-message').parents('.form-group').show();
			$('#notify-sms-message').parents('.form-group').hide();
			$('#notify-phone').parents('.form-group').parent().hide();
			$('#notify-email-address').parents('.form-group').parent().show();
		}else{ // Notify SMS
			$('#notify-email-subject').parents('.form-group').hide();
			$('#notify-email-message').parents('.form-group').hide();
			$('#notify-sms-message').parents('.form-group').show();
			$('#notify-phone').parents('.form-group').parent().show();
			$('#notify-email-address').parents('.form-group').parent().hide();
		}
	}

	function ValidateNotifyTicketForm(event){
        event.preventDefault();
		if ($('#notify-trouble-ticket-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){				
			var phone = $('#notify-phone').val();
			var emailAddress = $('#notify-email-address').val();
			var emailSubject = $("#notify-email-subject").val();
			var emailMessage = $("#notify-email-message").val();
			var SMSMessage = $("#notify-sms-message").val();
			var method = $("#notify-method").val();
			var ticket = $("#notify-ticket-id").val();
			if(method == 1){// Notify Email
				$.ajax({
					url: '/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=SendNotifyEmail&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'JSON',
					data: 
					{
						inpEmail: emailAddress,
						inpMessage: emailMessage,
						inpSubject: emailSubject,
						inpTicketId: ticket
					},
					beforeSend: function () {
						$("#processingPayment").show();
					},
					success: function(data){
						$("#processingPayment").hide();
						if(parseInt(data.RXRESULTCODE) == 1){
							$('#TroubleTicketNotifyModal').modal('hide');
							bootbox.dialog({
								 message:data.MESSAGE,
								 title: "Notify trouble ticket",
								 buttons: {
									 success: {
										 label: "Ok",
										 className: "btn btn-medium btn-success-custom",
										 callback: function() {}
									 }
								 }
							 });
							 InitControl();
						}
						else{
							bootbox.dialog({
								 message:data.MESSAGE,
								 title: "Notify trouble ticket",
								 buttons: {
									 success: {
										 label: "Ok",
										 className: "btn btn-medium btn-success-custom",
										 callback: function() {}
									 }
								 }
							 });
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('#processingPayment').hide();
						 bootbox.dialog({
							 message:'Send request failed!',
							 title: "Notify trouble ticket",
							 buttons: {
								 success: {
									 label: "Ok",
									 className: "btn btn-medium btn-success-custom",
									 callback: function() {}
								 }
							 }
						 });
					} 
				});
				
			}
			else if(method==0){// Notify SMS
				$.ajax({
					url: '/session/sire/models/cfc/troubleTicket/trouble-ticket.cfc?method=SendNotifySMS&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'JSON',
					data: 
					{
						inpPhone: phone,
						inpMessage: SMSMessage,
						inpTicketId: ticket
					},
					beforeSend: function () {
						$("#processingPayment").show();
					},
					success: function(data){
						$("#processingPayment").hide();
						if(parseInt(data.RXRESULTCODE) == 1){
							$('#TroubleTicketNotifyModal').modal('hide');
							bootbox.dialog({
								 message:data.MESSAGE,
								 title: "Notify trouble ticket",
								 buttons: {
									 success: {
										 label: "Ok",
										 className: "btn btn-medium btn-success-custom",
										 callback: function() {}
									 }
								 }
							 });
							 InitControl();
						}
						else{
							bootbox.dialog({
								 message:data.MESSAGE,
								 title: "Notify trouble ticket",
								 buttons: {
									 success: {
										 label: "Ok",
										 className: "btn btn-medium btn-success-custom",
										 callback: function() {}
									 }
								 }
							 });
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('#processingPayment').hide();
						 bootbox.dialog({
							 message:'Send request failed!',
							 title: "Notify trouble ticket",
							 buttons: {
								 success: {
									 label: "Ok",
									 className: "btn btn-medium btn-success-custom",
									 callback: function() {}
								 }
							 }
						 });
					} 
				});
			}
		}
	}
	// Change notify method
	$('#notify-method').on('change', function(){
		var data = $('#notify-ticket-id').val();
		InitNotifyForm(data);
	});

	$('#confirm-notify-ticket-btn').click(ValidateNotifyTicketForm);

	$('#TroubleTicketNotifyModal').on('hidden.bs.modal',function(){
		$('#notify-email-subject').val('');
		$('#notify-email-message').val('');
		$('#notify-sms-message').val('');
		CountDownCharacterById('notify-email-sub-countdown','notify-email-subject', 255);
		CountDownCharacterById('notify-email-mess-countdown','notify-email-message', 1000);
		CountDownCharacterById('notify-sms-mess-countdown','notify-sms-message', 160);
		document.getElementById("notify-trouble-ticket-form").reset();
		$('#notify-trouble-ticket-form').validationEngine('hide');
	});
	// End Trouble ticket notify form
	$(document).ready(function(){
		$("#ShowListWatchers").on('click',function(){			
			// List watcher			
			GetListWatcher(ticketId);   
			$("#list-watcher").modal("show");
		});
	});
})(jQuery);
