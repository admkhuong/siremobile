$('.link-myplan').click(function(e){
    e.preventDefault();
    location.href= $(this).attr('href');
});

var MyplanComponents = function () {

    var handleBootstrapSelect = function() {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    var handleMyPlanTab = function () {
        $(window).bind("load resize", function() {
            var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            if (width < 768) {
                $('#toggle-xs').addClass('uk-hidden');
            } else {
                $('#toggle-xs').removeClass('uk-hidden');
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {      
            handleBootstrapSelect();
            handleMyPlanTab();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
        MyplanComponents.init(); 
    });
}

(function($) {

	
    loadCountry();
    

    function loadExpirationDate(){

        var expiration_date = $('#expiration_date').val();

        if(expiration_date != ''){
            var n = expiration_date.search("/");
            if( n > 0)
            {
                var res = expiration_date.split("/");
                var month = res[0];
                var year = res[1];

                $('#expiration_date_month').val(month);
                $('#expiration_date_year').val(year);
            }
        }
    }

    
    function loadCountry(){
        if(country == '') country = "US";
        $('#country').val(country);
    }
    

    function update_expiration_date() {
        $('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
    }


    $('#expiration_date_month, #expiration_date_year').change(function(){
        update_expiration_date();
    });

    // Buy credit request
    var buy_credits = $("#buy_credits");
	
	buy_credits.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});
	
	buy_credits.submit(function(event){
		event.preventDefault();
		var amount = $('#amount').val();
		if (buy_credits.validationEngine('validate')) {
			var self = $(this);
			bootbox.dialog({
			    message: 'Amount to be paid: <b>$'+ amount +'</b> <br> Are you sure you would like to Make Payment?',
			    title: "Confirm Payment",
			    className: "confirm-make-payment",
			    buttons: {
			        success: {
			            label: "OK",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			                
							$('.btn-payment-credits').attr('disabled', 'disabled').text('Processing');
							$('.form-footer a.btn-primary-custom').hide();
							$("#actionStatus").val('Processing');
							$("#processingPayment").show();
							
							$.ajax({
								method: 'POST',
								url: '/session/sire/models/cfm/order-plan.cfm',
								data: self.serialize(),
								dataType: 'json',
								//timeout: 6000,
								success: function(data) {
								
									$("#actionStatus").val('Make Payment');
									$("#processingPayment").hide();
				
									var bootstrapAlert = $('#bootstrapAlert');
									bootstrapAlert.find('.modal-title').text('Payment process');
									bootstrapAlert.find('.alert-message').text(data.MSG);
									if (data && data.ID == 1) {

										bootbox.dialog({
						                    message: data.MSG,
						                    title: 'UPGRADE PLAN',
						                    buttons: {
						                        success: {
						                            label: "OK",
						                            className: "btn green-gd",
						                            callback: function() {
						                            	if (typeof data.URL_REDIRECT != 'undefined' && data.URL_REDIRECT != '') {
															location.href = data.URL_REDIRECT;
														}
														else {
															location.href = '/session/sire/pages/my-plan';
														}
						                            }
						                        }
						                    }
						                });
						                // $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
						                $('.btn-payment-credits').removeAttr('disabled').text('UPGRADE NOW');
									} else {
										bootbox.dialog({
						                    message: data.MSG,
						                    title: 'UPGRADE PLAN',
						                    buttons: {
						                        success: {
						                            label: "OK",
						                            className: "btn green-gd",
						                            callback: function() {
						                            }
						                        }
						                    }
						                });
										// $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
										$('.btn-payment-credits').removeAttr('disabled').text('UPGRADE NOW');
									}
									
								}
							});
			            }
			        },
			        cancel: {
			            label: "Cancel",
			            className: "btn btn-medium btn-back-custom",
			            callback: function() {}
			        },
			    }
			});
			
			
		}
		
	});

	var checked_payment_method = $('input.check_payment_method:checked').val();
	function switchPaymentMethod() {
		var check_payment_method = $('input.check_payment_method:checked').val();
		if (checked_payment_method == check_payment_method) return;
		checked_payment_method = check_payment_method;
		switch(check_payment_method) {
			case 0:
			case '0':
				$('#card_number').attr('class', 'form-control validate[required,custom[creditCardFunc]]')
					.removeAttr('data-errormessage-custom-error').parent().prev().html('Card Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid security code',
					'maxlength': 4
				}).parent().prev().html('Security Code:<span class="text-danger">*</span> <a href="javascript:$(\'#scModal\').modal(\'show\')" data-toggle="modal" data-target="#scModal"><img src="/session/sire/images/help-small.png"/></a>');
				$('#ExpirationDate').show();
				$('#card_name_label').html('Cardholder Name:<span class="text-danger">*</span>');
				break;
			case 1:
			case '1':
				$('#card_number').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid routing number'
				}).parent().prev().html('Routing Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid account number',
					'maxlength': 32
				}).parent().prev().html('Account Number:<span class="text-danger">*</span>');
				$('#ExpirationDate').hide();
				$('#card_name_label').html('Account Name:<span class="text-danger">*</span>');
				break;
		}
	}
	
	$('input.check_payment_method').click(switchPaymentMethod);


    $('.select_used_card').click(function(){

        $('#card_number').val('');
        $('#security_code').val('');
        $('#expiration_date').val('');
        $('#expiration_date_month').val('');
        $('#expiration_date_year').val('');
        $('#first_name').val('');
        $('#last_name').val('');

        // my_plan[0].reset();

        if($(this).val() == 2){
            // $('.update_card_info').show();
            // $('.update_cardholder_info fieldset').prop('disabled',false);
            // $('.btn-update-profile').prop('disabled',false);
            // $(".cardholder").each( function( index, element ){
            //     $(this).val('');
            // });
            // $('#country').val('US');
            // $('#email').val($('#h_email').val());
            $('#fset_cardholder_info').show();
            $('.btn-update-profile').prop('disabled', false);
        }
        else{
            // $('.update_card_info').hide();  
            // $('.update_cardholder_info fieldset').prop('disabled',true);
            // $('.btn-update-profile').prop('disabled',true);

            // $( ".cardholder" ).each( function( index, element ){
            //     var data = $(this).data('value');
            //     $(this).val(data);
            // });
            $('#fset_cardholder_info').hide();
            $('.btn-update-profile').prop('disabled', true);
        }
    });
    
    jQuery(document).ready(function($) {
        if($('#fset_cardholder_info').is(':visible')){
            $('.btn-update-profile').prop('disabled', false);
        }
        else
            $('.btn-update-profile').prop('disabled', true);
    });


    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing' && checkTimeout == 0)
	  		return 'Payment processing. Please don\'t refresh and close.';
	});
    $("#order-plan").change(function(e) {
        var plan = $("#order-plan").val();
        $.ajax({
            type: "GET",
            url: "/session/sire/models/cfc/order_plan.cfc?method=GetOrderPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: 'json',
            data: {
                plan: plan
            },
            beforeSend: function(){
				// $('#processingPayment').show();
			},
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            	// $('#processingPayment').hide();
            },
            success: function(data) {
            	// $('#processingPayment').hide();
            	$('#plan').val(plan);
            	$('#MonthlyAmount').text('$' + data.DATA.AMOUNT);
                $('#NumberKeyword').text(data.DATA.KEYWORDSLIMITNUMBER);
                $('#FirstSMSInclude').text(data.DATA.FIRSTSMSINCLUDED);
                $('#MLPAvailable').text(data.DATA.MLPSLIMITNUMBER);
                $('#ShortURLAvailable').text(data.DATA.SHORTURLSLIMITNUMBER);

                var discount_plan_price_percent = $('#discount_plan_price_percent').val();
                var discount_plan_price_flat_rate = $('#discount_plan_price_flat_rate').val();

                $.ajax({
		            type: "GET",
		            url: "/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		            dataType: 'json',
		            data: {
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {},
		            success: function(data1) {
		            	
		            	var BuyKeywordNumber = 0;
		            	var totalAMOUNT = data.DATA.AMOUNT;
                        var planTotalAMOUNT = data.DATA.AMOUNT;

                        if(discount_plan_price_percent > 0 || discount_plan_price_flat_rate > 0 ){
                           totalAMOUNT = parseInt(totalAMOUNT) - (parseInt(totalAMOUNT) * parseInt(discount_plan_price_percent) / 100) - parseInt(discount_plan_price_flat_rate);
                        }
                            
		            	var priceBuyKeywordNumber = 0;
		            	
		            	if (parseInt(data1.DATA.TOTALKEYWORD) > parseInt(data.DATA.KEYWORDSLIMITNUMBER))  {
		            		BuyKeywordNumber = parseInt(data1.DATA.TOTALKEYWORD) - parseInt(data.DATA.KEYWORDSLIMITNUMBER);
		            		priceBuyKeywordNumber = BuyKeywordNumber * data1.DATA.PRICEKEYWORDAFTER;
		            		planTotalAMOUNT = parseInt(planTotalAMOUNT) + (BuyKeywordNumber * data1.DATA.PRICEKEYWORDAFTER);
		            	}    
		                  
		                $('#BuyKeywordNumber').text('$' + priceBuyKeywordNumber);
		                $('#MonthlyAmountTotal').text('$' + planTotalAMOUNT);
		                $('#amount').val(totalAMOUNT);
		            }
		        });
                
            }
        });
        
    });

    $('#remove-payment-btn').click(function(event){
        event.preventDefault();

        var title = 'Remove Payment Confirmation';
        var message = 'Are you sure you want to remove this card?';


        confirmBox(message, title, function () {
            $.ajax({
                url: "/session/sire/models/cfc/billing.cfc?method=removeUserPaymentInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                type: "POST",
                dataType: "json",
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                success: function(data){
                    // console.log(data);
                    var titleAL = 'Remove Result';
                    var messageAL = data.MESSAGE;

                    try{
                        alertBox(messageAL, titleAL, function () {
                            if(data.RXRESULTCODE == 1){
                                window.location.reload();
                            }
                        });
                    } catch(error) {

                    }
                },
                complete: function(){
                    $('#processingPayment').hide();
                }
            });
        });
    });


})(jQuery);

function paymentConfirm() {
	$('#rsSelectYourPlan').text($('#order-plan option:selected').text());
	$('#rsFirstCreditsInclude').text($('#FirstSMSInclude').text());
	$('#rsNumberofPlanKeyword').text($('#NumberKeyword').text());
	$('#rsMonthlyKeywordFee').text($('#BuyKeywordNumber').text());
	$('#rsMonthlyPlanPrice').text($('#MonthlyAmount').text());
	$('#rsAmount').text($('#MonthlyAmountTotal').text());
}


$(document).ready(function() {
    setTimeout(function() {
        var country_val = $( "#country option:selected" ).val();
        if(!country_val){
            $("#country").val("US");
        }
    }, 1300);

    var modalAlert = function(title, message, event) {
        var bootstrapAlert = $('#bootstrapAlert');
        bootstrapAlert.find('.modal-title').text(title);
        bootstrapAlert.find('.alert-message').text(message);
        bootstrapAlert.modal('show');
    }

     $("#unscriber_keyword_form")
        .validationEngine({
            promptPosition: "topLeft",
            scroll: false,
            focusFirstField : false
        });
     $('#unscriber_keyword_form').submit(function(event){
        event.preventDefault();
        $('#UnscriberKeywordModal #Unsubscribe').click();
        }).validationEngine({
        promptPosition : "topLeft", 
        autoPositionUpdate: true, 
        showArrow: false, 
        scroll: false,
        focusFirstField : false
    });

    $('#UnscriberKeywordModal #Unsubscribe').click(function(){
        if ($('#unscriber_keyword_form').validationEngine('validate')) {
            var numberKeywordUnscriber = $('#numberKeywordUnscriber').val();
            $('#processingPayment').show();
            $.ajax({
                url: '/session/sire/models/cfc/order_plan.cfc?method=UnsubscribeKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {NUMBERKEYWORDUNSUBSCRIBE: numberKeywordUnscriber},
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                },
                success: function(data) {
                    //console.log(data);
                    $('#processingPayment').hide();
                    if (data && data.RXRESULTCODE == 1) {
                        $('#UnscriberKeywordModal').modal('hide');
                        location.reload();
                    } else {
                        modalAlert(data.MESSAGE);
                    }
                }
            });
            
        }
    });
});
