(function() {
    
        /*Ajax query string default param*/
        var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
    
        /*Keyword list datatable*/
        fnGetDataList = function (customFilterData){
            customFilterData  = typeof(customFilterData ) != "undefined" ? JSON.stringify(customFilterData) : "";   
            $("#chat-campaign").html('');    
    
            var actionBtn = function (object) {
                var strReturn = '<div class="btn-chat-xs"><a href="/session/sire/pages/sms-response?keyword='+object.Keyword+'" target="_blank" title="Respond to customer"><i class="fa fa-commenting-o" aria-hidden="true"></i><span>'+object.TOTALCHAT+'</span></a></div>';                
                return strReturn;
            }
    
            var table = $('#chat-campaign').dataTable({
                "bStateSave": false,
                "iStateDuration": -1,
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "Name", "sName": 'Name', "sTitle": 'Name', "bSortable": true, "sClass": "", "sWidth": "200"},
                    {"mData": "Keyword", "sName": 'Keyword', "sTitle": 'Keyword', "bSortable": true, "sClass": "", "sWidth": "200"},      
                    {"mData": "CreateDate", "sName": 'Create Date', "sTitle": 'Create Date', "bSortable": true, "sClass": "", "sWidth": "200"},                              
                    {"mData": actionBtn, "sName": 'Action', "sTitle": 'Action', "bSortable": false, "sClass": "col-center", "sWidth": "150px"}
                ],
                "sAjaxDataProp": "aaData",
                "sAjaxSource": '/session/sire/models/cfc/user-chat-tools.cfc?method=GetListChatCampaign'+strAjaxQuery,
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                    
                    aoData.push(
                        { "name": "customFilter", "value": customFilterData}
                    );
    
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {
                            if(data.iTotalDisplayRecords == 0)
                            {                                                                
                                $(".nodata-div").removeClass('hidden');
                            }
                            else
                            {
                                $(".table-data").removeClass('hidden');
                                fnCallback(data);
                            }
                        }
                    });
                },
                "aaSorting": [[1,'desc']]
            });
        }
        
        /* Init datatable */
        $(document).ready(function() {            
            fnGetDataList();                   
            //Filter bar initialize
            $('#box-filter').sireFilterBar({
                fields: [
                    {DISPLAY_NAME: 'Campaign Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' b.Desc_vch '},
                    {DISPLAY_NAME: 'Campaign ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' b.BatchId_bi '},
                    {DISPLAY_NAME: 'Created Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' b.Created_dt '},
                    {DISPLAY_NAME: 'Keyword', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'KEYWORD', SQL_FIELD_NAME: ' k.Keyword_vch '}
                ],
                clearButton: true,
                // rowLimited: 5,
                error: function(ele){
                    
                },
                // limited: function(msg){
                // 	alertBox(msg);
                // },
                clearCallback: function(){
                    fnGetDataList();
                }
            }, function(filterData){
                //called if filter valid and click to apply button
                fnGetDataList(filterData);
            });
        });    
    
    })();