
/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
//select 2

function formatDeleteIcon (data) {  
    if (data.id == null) {
        return data.text;
      }
      
      var $option = $("<spam></span>");
      var $preview = $("<a href='javascript:;' class='option-delete'><i class='fa fa-trash-o pull-right' aria-hidden='true' data-id='"+data.id+"'></i></a>");      
      $preview.on('mouseup', function (evt) {        
        evt.stopPropagation();
      });
      
      $preview.on('click', function (evt) {
            DeleteCDF(data.id);
      });
      
      $option.text(data.text);
      $option.append($preview);
      
      return $option;
};  
function DeleteCDF(id){
    
    if(id.length){
        title='Warning message !';
        message='Are you sure you want to delete this CDF?';
        bootbox.dialog({
            message: '<h4 class="be-modal-title">'+ title +'</h4><p>'+ message +'</p>',
            title: '&nbsp;',
            className: "be-modal",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                        doDelete();
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "green-cancel"
                },
            }
        });
        var doDelete=function()
		{	
            $.ajax({
                url: '/session/sire/models/cfc/user-import-contact.cfc?method=DeleteCDF'+strAjaxQuery,
                async: true,
                type: 'post',
                dataType: 'json',
                data: {
                    inpCDFId:id                
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                
                    alertBox('Delete CDF Fail','Delete CDF Fail',''); 

                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {                    
                        ReloadDataCDF();
                        
                    } else {                    
                        alertBox(data.MESSAGE,'Delete CDF  Fail','');
                    }                       
                }
            });
        }
    }
}
function SaveCDF()
{        
    if ($('#fr-edit-cdf').validationEngine('validate', {scroll: false, focusFirstField : false})) {  
        var inpFieldType=$("#cdf-field-type").val();
        var inpFieldName=$("#cdf-field-name").val();      
        //
        
        //var regexTableName = new RegExp("^(?:[\p{N}\p{L}_][\p{L}\p{N}@$#_]{0,127}|\[.{1,126}\])$");
        //alert(regexTableName.test(inpFieldName));
                      
        $.ajax({
            url: '/session/sire/models/cfc/user-import-contact.cfc?method=InsertUpdateCDF'+strAjaxQuery,
            async: true,
            type: 'post',
            dataType: 'json',
            data: {
                inpFieldType:inpFieldType,
                inpFieldName: inpFieldName
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();
               
                alertBox('Insert CDF Fail','Insert CDF Fail',''); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                var fromSource =$("#cdf-field-source").val();
                var idSeleted=data.ID;
                if (data.RXRESULTCODE == 1) {
                    $("#edit-cdf").modal('hide');
                    $(".modal-backdrop").fadeOut();
                    
                    ReloadDataCDF(fromSource,idSeleted);
                    
                } 
                else if(data.RXRESULTCODE == -2)
                {                    
                    $("#"+fromSource).val(idSeleted);
                    ReloadDataCDF(fromSource,idSeleted);
                    alertBox(data.MESSAGE,'Duplicate CDF','');
                    
                }
                else {
                    /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                    alertBox(data.MESSAGE,'Insert CDF  Fail','');
                }                       
            }
        });
    }
    else
    {                           
        return false;
    }
}  

function ReloadDataCDF(fromSource,idSeleted)
{
    $.ajax({
        url: '/session/sire/models/cfc/user-import-contact.cfc?method=GetUserCDF'+strAjaxQuery,
        async: true,
        type: 'post',
        dataType: 'json',
        data: {
            
        },
        beforeSend: function(){
            
        },
        error: function(jqXHR, textStatus, errorThrown) {            

        },
        success: function(data) {
            
            if (data.RXRESULTCODE == 1) {
                //   
                $(".cdf-data").each(function(){
                    var selected= $(this).val();
                    $(this).select2(
                        { 
                            theme: "bootstrap", width: 'auto'
                            /*,templateResult:formatDeleteIcon*/
                    }
                    );
    
                    $(this).empty();
                    // add new link
                    var $LinkAddNew = $("<option/>", {value: '0', text: 'Create New Custom Field'});                    
                    $LinkAddNew.on('mouseup', function (evt) {        
                        //evt.stopPropagation();
                        alert(123)
                    });                    
                    $LinkAddNew.on('click', function (evt) {
                        alert(123)
                    });                                                            
                    
                    //listData
                    
                    for (i = 0; i < data.listData.length; i++) { 
                        var o = $("<option/>", {value: data.listData[i][0], text: data.listData[i][1]});
                        $(this).append(o);   
                    } 
                    // add new link    
                    $(this).append($LinkAddNew);                                    

                    $(this).val(selected);
                });                  
                $("#"+fromSource).val(idSeleted);                                            
            }                        
        }
    });
}

$(document).ready(function() {
    $("#edit-cdf").on('submit',function(e){
        e.preventDefault();  
        SaveCDF();
    }); 
    // click cancel
    $("#edit-cdf").on('click','.green-cancel',function(e){
        e.preventDefault();  
        var col_fromSource=$("#cdf-field-source").val(); 
        $("#"+col_fromSource).val('ContactString_vch').trigger("change");
    }); 
    // hidden modal
    $('body').on('hidden.bs.modal', '#edit-cdf', function(event) {				
        event.preventDefault();  
        if($("#cdf-field-name").val() == ''){
            var col_fromSource=$("#cdf-field-source").val(); 
            $("#"+col_fromSource).val('ContactString_vch').trigger("change");
        }
    });
    
    $(document).on('select2:close','.cdf-data', function (e) {
        if($(this).val()==0)
        {
            $("#cdf-field-source").val($(this).attr("id"));
            $("#edit-cdf").find('#cdf-field-name').val("");
            $("#edit-cdf").find('#cdf-field-type').val(0).trigger("change");
            $("#edit-cdf").modal('show');
        }
    });    
}); 