var refreshContactList;
var globalAceptTerms=0;
var globalAceptTerms=0; // 0 file import, 1 indivudual import
//Define FileRecord statuses 
const STARTLOADING = 2, PENDING = -1, DELETED = -2, JUSTCREATE = 0,  COMPLETED = 3, LOADING = 1, STOPPED = 4,OUTSTANDING=-4;
//Define Type
const SUBCRIBER_LIST = 1, CAMPAIGN_BLAST = 2;
//Define contact status
const CT_DUPLICATE = 10, CT_INVALID = 11, CT_VALID = 3, CT_DELETED = -2; CT_DNC=13;
    
/*Ajax query string default param*/
var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

$('body').on('click', '#btn-advance', function(event) {
    event.preventDefault();
    $('body').find("#section-advance").toggle();
});

fnGetImportIndividualSumary=function()
{
    $("#individual-summary").html('');  
    var statusCol = function(object){ 
        var status = "";
        if (object.STATUS ==1) {            
            status = 'Ready';            
        } 
        else if (object.STATUS ==2){
            status = 'Processing';            
        }
        else if (object.STATUS ==3){
            status = 'Completed';            
        }
        else if (object.STATUS ==4){
            status = 'Rejected';            
        }
        else {
            status = 'Wait for approval';  
        }
        return status;
    };
    
    var colTotal=function(object){
        return "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='0' title='click to view data'>"+object.TOTALCONTACT+"</a>" ;                   
    };
    var colImported=function(object){
        return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='1' title='click to view data'>"+object.IMPORTED+"</a>" ;                   
    };
    var colFailed=function(object){
        return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='2' title='click to view data'>"+object.FAILED+"</a>" ;                   
    };
    var colDNC=function(object){
        return  "<a class='admin-action-indi' data-action='view' data-id='"+object.ID+"' data-type='3' title='click to view data'>"+object.DNC+"</a>" ;                   
    };
    var table = $('#individual-summary').dataTable({
        "bStateSave": false,                
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            {"mData": "SUBSCRIBERLIST", "sName": 'Subscriber List', "sTitle": 'Subscriber List', "bSortable": true, "sClass": "", "sWidth": "200"},                                
            {"mData": "IMPORTDATE", "sName": 'Create Date', "sTitle": 'Create Date', "bSortable": true, "sClass": "", "sWidth": "200"},                   
            {"mData": colTotal, "sName": 'Total Contact', "sTitle": 'Total Contact', "bSortable": true, "sClass": "", "sWidth": "120"},  

            {"mData": colImported, "sName": 'Imported', "sTitle": 'Imported', "bSortable": false, "sClass": "", "sWidth": "120"}, 
            {"mData": colFailed, "sName": 'Failed', "sTitle": 'Failed', "bSortable": false, "sClass": "", "sWidth": "120"}, 
            {"mData": colDNC, "sName": 'DNC', "sTitle": 'DNC', "bSortable": false, "sClass": "", "sWidth": "120"}, 

            {"mData": statusCol, "sName": 'Status', "sTitle": 'Status', "bSortable": true, "sClass": "", "sWidth": "200"} ,                  
        ],
        "sAjaxDataProp": "aaData",
        "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=IndividualImprtSummary'+strAjaxQuery, 
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                {

                }
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                }
            });
        },
        "aaSorting": [[1,'desc']]                
    });  
}
/*Keyword list datatable*/
fnGetDataList = function (){
    $("#import-summary").html('');    

    var statusCol = function(object){ 
        var status = "";
        if (object.STATUS in uploadStatus) {
            if ((uploadStatus[object.STATUS] == "Completed" || uploadStatus[object.STATUS] == "Waiting") && object.SENDTOQUEUE == "0") {
                status += "<b>Send Partial</b> " 
            }
            status += uploadStatus[object.STATUS];
            
            if(object.STATUS==-3 && object.UNIQUE > 0)
            {
                status = 'Completed';
            }
        } else {
            status = 'Loading';
        }
        return status;
    };
    var listTypeCol = function(object){ 
        var type = "";
        switch(object.LISTTYPE){ 
            case SUBCRIBER_LIST: type = ""; break;
            case CAMPAIGN_BLAST: type = "Campaign Blast"; break;
        } 
        return type;
    };
    
    var isHyperlink = function(object){        
        return (object.STATUS !== LOADING) ? true : false;
    }
    //
    
    
    
    var colDelete="";
    //
    var colValid=function(object){
        var strReturn = "";
        if(isHyperlink(object)){
            strReturn= (parseInt(object.UNIQUE) <= 0) ? object.UNIQUE : $("<a href=''></a>").text(object.UNIQUE).attr('data-id', object.ID)
            .attr('data-type', 'valid').addClass('editable')[0].outerHTML;                  
        }else{
            strReturn=object.UNIQUE;   
        }        
        return strReturn;
    };
    var colInValid=function(object){
        var strReturn = "";
        if(isHyperlink(object)){
            strReturn= (parseInt(object.INVALID) <= 0) ? object.INVALID : $("<a href=''></a>").text(object.INVALID).attr('data-id', object.ID)
            .attr('data-type', 'invalid').addClass('editable')[0].outerHTML;              
        }else{
            strReturn=object.INVALID;   
        }        
        return strReturn;
    };
    var colDupplicate=function(object){
        var strReturn = "";
        if(isHyperlink(object)){
            strReturn=(parseInt(object.DUPLICATE) <= 0) ? object.DUPLICATE : $("<a href=''></a>").text(object.DUPLICATE).attr('data-id', object.ID)
            .attr('data-type', 'duplicate').addClass('editable')[0].outerHTML;                
        }else{
            strReturn=object.DUPLICATE;   
        }        
        return strReturn;
    };
    var colDNC=function(object){
        var strReturn = "";
        if(isHyperlink(object)){
            strReturn=(parseInt(object.DNC) <= 0) ? object.DNC : $("<a href=''></a>").text(object.DNC).attr('data-id', object.ID)
            .attr('data-type', 'dnc').addClass('editable')[0].outerHTML;                
        }else{
            strReturn=object.DNC;   
        }        
        return strReturn;
    };
   //
        
    var statsCol = function (object) {
        
        var strReturn = "";
        strReturn += "Valid: ";
        if(isHyperlink(object)){
            strReturn += (parseInt(object.UNIQUE) <= 0) ? object.UNIQUE : $("<a href=''></a>").text(object.UNIQUE).attr('data-id', object.ID)
                                            .attr('data-type', 'valid').addClass('editable')[0].outerHTML;
            strReturn += "<br/>Duplicate: ";
            strReturn += (parseInt(object.DUPLICATE) <= 0) ? object.DUPLICATE : $("<a href=''></a>").text(object.DUPLICATE).attr('data-id', object.ID)
                                            .attr('data-type', 'duplicate').addClass('editable')[0].outerHTML;
            strReturn += "<br/>Invalid: ";
            strReturn += (parseInt(object.INVALID) <= 0) ? object.INVALID : $("<a href=''></a>").text(object.INVALID).attr('data-id', object.ID)
                                            .attr('data-type', 'invalid').addClass('editable')[0].outerHTML;
        } else {
            strReturn += object.UNIQUE;
            strReturn += "<br/>Duplicate: ";
            strReturn += object.DUPLICATE;
            strReturn += "<br/>Invalid: ";
            strReturn += object.INVALID;
        }
        strReturn += "<br/>Deleted: ";
        if(parseInt(object.DELETED) > 0 && uploadStatus[object.STATUS] != "Deleted"){
            strReturn += $("<a href=''></a>").text(object.DELETED).attr('data-id', object.ID)
                                            .attr('data-type', 'deleted').addClass('editable')[0].outerHTML;
        } else {
            strReturn += object.DELETED;
        }

        return strReturn;
    }

    var totalCol = function(object){ 
        var strReturn = object.LOADED;
        if(isHyperlink(object)){
            strReturn = (parseInt(object.LOADED) <= 0) ? object.LOADED : $("<a href=''></a>").text(object.LOADED).attr('data-id', object.ID)
                                            .attr('data-type', 'loaded').addClass('editable')[0].outerHTML;
        }
        return strReturn;
    };

    var actionCol = function(object){ 
        var strReturn = "";

        if (uploadStatus[object.STATUS] != "Deleted") {
            if (uploadStatus[object.STATUS] == "Pending") {
            }
            if (uploadStatus[object.STATUS] == "Waiting") {
                strReturn = $("<a class='action-import action btn-send-now' title='Send partial' data-type='"+object.LISTTYPE+"' data-action='send' href='/session/sire/pages/admin-import-contact-detail?uploadid="+object.ID+"'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/send.png">').attr('data-id', object.ID)[0].outerHTML;
            }
            if (uploadStatus[object.STATUS] == "Processing") {
                strReturn = $("<a class='action-import action btn-stop' title='Pause' data-type='"+object.LISTTYPE+"' data-action='stop' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/stop.png">').attr('data-id', object.ID)[0].outerHTML;
            }
            if (uploadStatus[object.STATUS] == "Paused") {
                strReturn = $("<a class='action-import action btn-resume' title='Resume' data-type='"+object.LISTTYPE+"' data-action='resume' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/resume.png">').attr('data-id', object.ID)[0].outerHTML;
            }
            if (object.SENDTOQUEUE == "0" && (uploadStatus[object.STATUS] == "Completed" || uploadStatus[object.STATUS] == "Failed")) {
                strReturn = $("<a class='action-import action btn-send-now' title='View send partial history' data-type='"+object.LISTTYPE+"' data-action='send' href='/session/sire/pages/admin-import-contact-detail?uploadid="+object.ID+"'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/send.png">').attr('data-id', object.ID)[0].outerHTML;
            }
            strReturn+= $("<a class='action-import action btn-report' title='Report' data-action='report'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/chart.png">').attr('data-id', object.ID)[0].outerHTML;

            if (uploadStatus[object.STATUS] != "Completed" && uploadStatus[object.STATUS] != "Failed") {
                strReturn+= $("<a class='action-import action btn-delete' title='Delete' data-type='"+object.LISTTYPE+"' data-action='delete' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/delete.png">').attr('data-id', object.ID)[0].outerHTML;
            }
        }
        return strReturn;
    };

    var updatedBy = function(object){
        var strReturn = "";
        if(object.UPDATEDBY != "" && object.UPDATEDAT != "" && uploadStatus[object.STATUS] != "Deleted"){
            strReturn+= "<p><strong>"+object.UPDATEDBY+"</strong></p>";
            strReturn+= "<p class='lbl'><a class='show-history' data-file-name='"+object.FILENAME+"' data-id='"+object.ID+"' href=''>"+object.UPDATEDAT+"</a></p>"
        } else {
            strReturn += object.IMPORTEDBY;
        }
        return strReturn;
    }

    var infoCol = function (object) {
        var strReturn = "";
        var type = "";
        switch(object.LISTTYPE){ 
            case SUBCRIBER_LIST: type = "Subcriber List"; break;
            case CAMPAIGN_BLAST: type = "Campaign Blast"; break;
        }
        strReturn += "<b>User Name:</b> "+object.USERNAME+"<br/>";
        strReturn += "<b>File Name:</b> "+object.FILENAME+"<br/>";
        strReturn += "<b>File Date:</b> "+object.CREATEDAT+"<br/>";
        strReturn += "<b>List Type:</b> "+type;
        return strReturn;
    }
    var table = $('#import-summary').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            /*
            {"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id", "sWidth": "80px"},           
            {"mData": infoCol, "sName": 'listtype', "sTitle": 'File Info',"bSortable": false,"sClass":"file-info col-xl"},
            {"mData": "LOADED", "sName": 'loaded', "sTitle": "Org Rec Cnt","bSortable": false,"sClass":"loaded col-md", "sWidth":"150px"},
            {"mData": statsCol, "sName": 'valid', "sTitle": "Stats","bSortable": false,"sClass":"stats-col col-lg" , "sWidth":"200px"},
            
            {"mData": updatedBy, "sName": 'updatedby', "sTitle": 'Updated by',"bSortable": false,"sClass":"updated-by", "sWidth":"150px"},
            {"mData": statusCol,"sName": 'status', "sTitle": "Status","bSortable": false,"sClass":"status col-md",  "sWidth": "150px"},            
            
            */
                        
            {"mData": "FILENAME", "sName": 'File Name', "sTitle": "File Name","bSortable": false,"sClass":"", "sWidth":"250px"},
            {"mData": "CREATEDAT", "sName": 'File Date', "sTitle": "File Date","bSortable": false,"sClass":"", "sWidth":"200px"},
            {"mData": "SUBSCRIBERLIST", "sName": 'Subscriber List', "sTitle": "Subscriber List","bSortable": false,"sClass":"", "sWidth":"180px"},
            {"mData": "LOADED", "sName": 'Total', "sTitle": "Total","bSortable": false,"sClass":"", "sWidth":"80px"},
            {"mData": colValid, "sName": 'Valid', "sTitle": "Valid","bSortable": false,"sClass":"", "sWidth":"80px"},
            {"mData": colDupplicate, "sName": 'Duplicate', "sTitle": "Duplicate","bSortable": false,"sClass":"", "sWidth":"80px"},
            {"mData": colInValid, "sName": 'Invalid', "sTitle": "Invalid","bSortable": false,"sClass":"", "sWidth":"80px"},
            {"mData": colDNC, "sName": 'DNC', "sTitle": "DNC","bSortable": false,"sClass":"", "sWidth":"80px"},            
            {"mData": statusCol,"sName": 'status', "sTitle": "Status","bSortable": false,"sClass":"status col-md",  "sWidth": "180px"},   
        ],
        "sAjaxDataProp": "datalist",
        "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=GetReport'+strAjaxQuery,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            
            aoData.push(
                { }
            );

            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function(data) {
                    fnCallback(data);
                }
            });
        },
        "aaSorting": [[1,'desc']]
    });
}     
  
        
    
function goMapFieldStep(){
    var filenameupload=$("#file-name-upload").val();
    ///session/sire/pages/user-import-contact-map?filenameupload=contact-string-608-Sep-06-2017-18-27-41.csv
    if(filenameupload.length)
    {
        $.ajax({
            url: '/session/sire/pages/user-import-contact-map?filenameupload='+filenameupload+'&imp_skipline='+$("#inpSkipLine").val()+"&imp_ErrorsIgnore="+$("#inpErrorsIgnore").val(),
            async: true,
            type: 'post',
            dataType: 'html',
            data: {
                
            },
            beforeSend: function(){
                $('#processingPayment').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#processingPayment').hide();               
                alertBox('Load Map Field Name Fail','Load Map Field Name Fail',''); 

            },
            success: function(data) {
                $('#processingPayment').hide();
                //      
                $("#mapFieldContent").empty();
                $("#mapFieldContent").append(data);   
                                
                document.getElementById('mapFieldContent').scrollIntoView(); 
                /*
                var subsCDF=$(".cdf-data").select2(                             
                    { 
                        theme: "bootstrap", width: 'auto'                        
                    }
                ).data('select2');                           
                */
                ReloadDataCDF();
            }
        });
    }
    else
    {
        alertBox('Please select a csv file to upload !','Upload','');
    }  
}   
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}
function GetContact(id, type){
    $('#show-contact-ds').html();
    var tzList = JSON.parse($('input[name="tzlist"]').val());
    var showCheckItem = function(object){
        return (object.STATUS != -2) ? "<input class='select-item' type='checkbox' value='"+object.ID+"'>" : "";
    };

    var actionCol = function(object){
        var strReturn = "";
        if (object.STATUS != -2) {
            strReturn = $("<button class='action btn btn-success' title='edit' data-action='edit' href=''></button>").html('<i class="glyphicon glyphicon-edit"></i>').attr('data-timezone', object.TIMEZONE).attr('data-contact', object.CONTACT).attr('data-id', object.ID)[0].outerHTML;
            strReturn+= $("<button class='action btn btn-danger' title='delete' data-action='delete' href=''></button>").html('<i class="glyphicon glyphicon-trash"></i>').attr('data-id', object.ID)[0].outerHTML;								
        }
        return strReturn;
    }

    var tzCol = function(object){
        var strReturn = "";
        tzList.forEach(function(item){
            if(object.TIMEZONE === item.VALUE){
                strReturn = item.NAME;
            }
        });
        return strReturn;
    }

    var table = $('#show-contact-ds').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": false,
        "aoColumns": [
            /*
            {"mData": showCheckItem, "sName": 'select', "sTitle": "","bSortable": false,"sClass":"select td-select"},
            */
            {"mData": "CONTACT", "sName": 'contact', "sTitle": 'Contact',"bSortable": false,"sClass":"contact", "sWidth": "180px"},
            /*
            {"mData": tzCol, "sName": 'timezon', "sTitle": 'TimeZone',"bSortable": false,"sClass":"timezone"},
            {"mData": actionCol, "sName": 'action', "sTitle": 'Actions',"bSortable": false,"sClass":"action td-action", "sWidth":"130px"}
            */
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=getContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
           "fnDrawCallback": function( oSettings ) {
              if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }

            if(oSettings._iRecordsTotal == 0){
                this.parent().find('div.dataTables_paginate').hide();
            } else {
                this.parent().find('div.dataTables_paginate').show();
            }
        },
        "fnHeaderCallback": function( thead, data, start, end, display, object) {
            if (data.length > 0) {
                if (!(uploadStatus[data[0].STATUS] == "Deleted")) {
                    $(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
                }
            } else {
                $(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            aoData.push(
                { "name": "inpFileRecordID", "value": id}
            );
             aoData.push(
                { "name": "inpType", "value": type}
            );
            oSettings.jqXHR = $.ajax( {
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function (data) {
                    fnCallback(data);
                    if (type == "-2") {
                        $('#show-contact-ds').find('.td-action, .td-select').addClass('hidden');
                    } else {
                        $('#show-contact-ds').find('.td-action, .td-select').removeClass('hidden');
                    }
                }
              });
        },
        "fnInitComplete":function(oSettings){
            if(oSettings._iRecordsTotal == 0){
                this.parent().find('div.dataTables_paginate').hide();
            } else {
                this.parent().find('div.dataTables_paginate').show();
            }
        }
    });

    return function(){
        table.fnStandingRedraw();
    }

}
function deleteContacts(selectedIDs, inpFileRecordID, callback, type, deleteAll){
    $.ajax({
        url: "/session/sire/models/cfc/import-contact.cfc?method=deleteContacts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
        type: "POST",
        dataType: "json",
        data: {inpItemIDs: JSON.stringify(selectedIDs), inpFileRecordID: inpFileRecordID, inpType: type, inpDeleteAll: deleteAll},
        success: function(data){
            if(data.RXRESULTCODE == 1){
                bootbox.dialog({
                    message: "Record(s) has been successfully deleted.",
                    title: "Alert",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {

                            }
                        }
                    }
                });
            }
            callback();
        }
    });
}
function getHistory(id, date){
    $('#show-file-history-ds').html('');
    var listTypeCol = function(object){ 
        var type = "";
        switch(object.LISTTYPE){ 
            case 1: type = "Subcriber List"; break;
            case 2: type = "Campaign Blast"; break;
        } 
        return type;
    };

    var updatedCol = function(object){
        var strReturn = "";
            
            strReturn+= "<p class='lbl'>"+object.CREATED+"</p>"
            strReturn+= "<p>By: <strong>"+object.USERNAME+"</strong></p>";
        return strReturn;
    }

    var table = $('#show-file-history-ds').dataTable({
        "bStateSave": false,
        "iStateDuration": -1,
        "bProcessing": true,
        "bFilter": false,
        "bServerSide":true,
        "bDestroy":true,
        "sPaginationType": "input",
        "bLengthChange": false,
        "iDisplayLength": 10,
        "bAutoWidth": true,
        "aoColumns": [
            {"mData": listTypeCol, "sName": 'listtype', "sTitle": 'List Type',"bSortable": false,"sClass":"listtype"},
            {"mData": "FILENAME", "sName": 'filename', "sTitle": 'File Name',"bSortable": false,"sClass":"file-name", "sWidth": "320px"},
            {"mData": "FIELD", "sName": 'fieldname', "sTitle": "Field Name","bSortable": false,"sClass":"fieldname"},
            {"mData": "OLDVALUE", "sName": 'oldvalue', "sTitle": 'Old Value',"bSortable": false,"sClass":"oldvalue"},
            {"mData": "NEWVALUE", "sName": 'newvalue', "sTitle": 'New Value',"bSortable": false,"sClass":"newvalue"},
            {"mData": updatedCol, "sName": 'updatedat', "sTitle": 'Updated',"bSortable": false,"sClass":"updatedat"}
        ],
        "sAjaxDataProp": "DATALIST",
        "sAjaxSource": '/session/sire/models/cfc/import-contact-history.cfc?method=getUpdateHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
           "fnDrawCallback": function( oSettings ) {
              if (oSettings._iDisplayStart < 0){
                oSettings._iDisplayStart = 0;
                $('input.paginate_text').val("1");
            }
            if(oSettings._iRecordsTotal == 0){
                this.parent().find('div.dataTables_paginate').hide();
            } else {
                this.parent().find('div.dataTables_paginate').show();
            }
        },
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
            if(typeof(id) != "undefined" && id > 0){
                aoData.push(
                    { "name": "inpFileRecordID", "value": id}
                );
            }

            if(typeof(date) != "undefined"){
                aoData.push(
                    { "name": "inpDateStart", "value": date.start}
                );
                aoData.push(
                    { "name": "inpDateEnd", "value": date.end}
                );
            }
            
            oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function (data) {
                    fnCallback(data);
                    $("#show-file-history-ds").css('width', '100%');
                }
              });
        },
        "fnInitComplete": function(oSettings){
            if(oSettings._iRecordsTotal == 0){
                this.parent().find('div.dataTables_paginate').hide();
            } else {
                this.parent().find('div.dataTables_paginate').show();
            }
        }
    });
}
/* Init datatable */
$(document).ready(function() {
    var impSkipLine = $("#inpSkipLine").val();
    var impErrorsIgnore = $("#inpErrorsIgnore").val();
    //start dropzone
    //File Upload response from the server
    $('#file-import').addClass('disabled');
    var drzone=new Dropzone("#dropzone2", {
        url:"/session/sire/models/cfc/user-import-contact.cfc?method=UploadCSVContact" + "&imp_skipline=" + impSkipLine + "&imp_ErrorsIgnore=" + impErrorsIgnore,
        method: "post",
        maxFiles: 1,       
        maxFilesize: 100,

        clickable: true,
        ignoreHiddenFiles: true,        
        autoProcessQueue: true,
        autoQueue: true,

        acceptedFiles:'.csv', 
        addedfile: function(file) {
            $('#processingPayment').show();
            //console.log(file);
            if(file.type !=='application/vnd.ms-excel' && file.type !=='text/csv')
            {
                $('#processingPayment').hide();
                alertBox('The format of the file you selected is invalid. Please select a .csv file only. !','Upload Fail','');
            }
        },      
       
        complete: function(file){
            //
            var j, k, l, len, len1, len2, node, ref, ref1, ref2, removeFileEvent, removeLink, results;
            if (this.element === this.previewsContainer) {
                this.element.classList.add("dz-started");
            }                                       
            //hide upload
            //$('.dz-default').hide();
            if (this.previewsContainer && file.status=='success') {
                file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
                file.previewTemplate = file.previewElement;
                this.previewsContainer.appendChild(file.previewElement);          
                ref = file.previewElement.querySelectorAll("[data-dz-name]");
                for (j = 0, len = ref.length; j < len; j++) {
                node = ref[j];
                node.textContent =file.name;
                }                          
                

                ref1 = file.previewElement.querySelectorAll("[data-dz-size]");
                for (k = 0, len1 = ref1.length; k < len1; k++) {
                node = ref1[k];
                node.innerHTML = this.filesize(file.size);
                }
                if (this.options.addRemoveLinks) {
                file._removeLink = Dropzone.createElement("<a class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
                file.previewElement.appendChild(file._removeLink);
                }
                removeFileEvent = (function(_this) {
                return function(e) {
                    $('#file-import').addClass('disabled');
                    $('#inpSkipLine').prop("disabled", false);
                    $('#inpErrorsIgnore').prop("disabled", false);
                    $('#btn-save').addClass('disabled');
                    e.preventDefault();
                    e.stopPropagation();
                    if (file.status === Dropzone.UPLOADING) {
                    return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function() {
                        return _this.removeFile(file);
                    });
                    } else {
                    if (_this.options.dictRemoveFileConfirmation) {
                        return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function() {
                        return _this.removeFile(file);
                        });
                    } else {
                        return _this.removeFile(file);
                    }
                    }
                };
                })(this);
                ref2 = file.previewElement.querySelectorAll("[data-dz-remove]");
                results = [];
                for (l = 0, len2 = ref2.length; l < len2; l++) {
                removeLink = ref2[l];
                results.push(removeLink.addEventListener("click", removeFileEvent));
                }
                //
                console.log(file);
                if(file.xhr != undefined) 
                {              
                    $('#processingPayment').hide();    
                    var result = JSON.parse(file.xhr.response);                
                    if(result.RXRESULTCODE ==1){
                        $("#file-name-upload") .val(JSON.parse(file.xhr.response).FILENAME);
                        $("#file-name-upload-org") .val(JSON.parse(file.xhr.response).FILENAME_ORG);  
                        $('#file-import').removeClass('disabled');
                        $('#inpSkipLine').attr("disabled", "");
                        $('#inpErrorsIgnore').attr("disabled", "");
                    }
                    else {
                        alertBox(result.MESSAGE,'Import Contact','');                        
                        $('#file-import').addClass('disabled');
                        $('#inpSkipLine').prop("disabled", false);
                        $('#inpErrorsIgnore').prop("disabled", false);
                    }  
                }      
                return results;
            }                        
        },
        maxfilesreached: function(file){
            //
        },
        maxfilesexceeded: function(file){
            $('#processingPayment').hide();
            alertBox('Max file reached','Max file reached','');
        }
    });
    $(".open-upload").on('click',function(){
        $("#dropzone2").trigger( "click" );
    });

    $(document).on('change','#inpSkipLine, #inpErrorsIgnore',function(){
        impSkipLine = $("#inpSkipLine").val();
        impErrorsIgnore = $("#inpErrorsIgnore").val();
        if(impSkipLine == ''){
            alertBox('Total line ignore is invalid!.','Data Invalid','');
            $("#inpSkipLine").val(0);
            return false;
        }

        if(impErrorsIgnore == ''){
            alertBox('Number of errors allowed is invalid!.','Data Invalid','');
            $("#inpErrorsIgnore").val(0);
            return false;
        }
      
        if(impSkipLine < 0){
            alertBox('Total line ignore is invalid!.','Data Invalid','');
            $("#inpSkipLine").val(0);
            return false;
        }else if(impErrorsIgnore < 0){
            alertBox('Number of errors allowed is invalid!.','Data Invalid','');
            $("#inpErrorsIgnore").val(0);
            return false;
        }
            drzone.options.url = "/session/sire/models/cfc/user-import-contact.cfc?method=UploadCSVContact" + "&imp_skipline=" + impSkipLine + "&imp_ErrorsIgnore=" + impErrorsIgnore;
        
    });

    //end dropzone
    var inpFileDateStart   = $('input[type="hidden"][name="file-date-start"]');
    var inpFileDateEnd   = $('input[type="hidden"][name="file-date-end"]');

    var DatePickerFh = $('input[data-type="date-single"]').daterangepicker({
        opens: "right",
        drops: "down",
        timeZone: 'UTC',
        buttonClasses: "btn btn-sm",
        applyClass: "btn-success",
        cancelClass: "btn-default",
        startDate: inpFileDateStart.data('value'),
        endDate: inpFileDateEnd.data('value')
    }, function(start, end, label){
        inpFileDateStart.val(start._d.getSireDate());
        inpFileDateEnd.val(end._d.getSireDate());
    });
    fnGetDataList();
    fnGetImportIndividualSumary();

    $(".SubscriberList").select2(
        { 
            theme: "bootstrap", 
            width: 'auto'            
        }
    );


    $(document).on('change','.SubscriberList',function(){
        selected= $(this).val();        
        $('.SubscriberList').each(function(){
            //$(this).val(selected); 
            if($(this).val() !== selected)                       
            $(this).val(selected).change();            
        });
        
        
    });
    $("#file-import").on('click',function(){    
        if($("#SubscriberList").val()== null){
            alertBox('Subscriber list can not blank!.','Alert','');
            return false;
        } 

        globalImportType=0;        
        if(globalAceptTerms==0)
        {
            $("#confirm-import").modal('show');
        }
        else
        {
            goMapFieldStep();
        }                
    });    

    $("#chkAgreeTerms").on('click',function(){
        globalAceptTerms=1;
        $("#confirm-import").modal('hide');
        if(globalImportType==0)
        {
            goMapFieldStep();
        }      
        else
        {
            goImportIduvidualStep();
        }      
    });
    $("#individual-import").on('click',function(){    
        globalImportType=1;            
        if(globalAceptTerms==0)
        {
            $("#confirm-import").modal('show');
        }
        else
        {
            goImportIduvidualStep();
        }                
    });
    function validatePhone(phone) {  
        phone=  phone.replace(/\(/g, '');    
        phone=  phone.replace(/\)/g, '');    
        phone=  phone.replace(/\-/g, '');    
        phone = phone.replace(/ /g, '');

        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(phone) && phone.length ==10) {
            return true;
        }
        else {
            return false;
        }
    }
    function goImportIduvidualStep()
    {
        var subscriberList= $("#SubscriberList").val();   
        var listNumber=   $("#ListTextImport").val();  
        var listNumberValid=1;
        arrayCheck = listNumber.match(/[^\r\n]+/g); 
        
        // console.log(arrayOfLines);
        // return false;
        // var arrayCheck=listNumber.split(',');

        if(arrayCheck.length > 100){
            alertBox('Manual imports must not exceed 100 contacts. Please reduce your list size and try again.','List Size Exceeds Limit','');
            return false;
        }
        for (i = 0; i < arrayCheck.length; i++) { 
            if(! validatePhone(arrayCheck[i]))
            {
                alertBox('Invalid Phone Number','Invalid Phone Number','');
                return false;
            }
        }
        
        if(subscriberList.length)
        {
            $.ajax({
                url:'/session/sire/models/cfc/user-import-contact.cfc?method=IndividualImprtToQuee&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {
                    inpListNumber:arrayCheck.toString(),
                    inpSubcriberListID: subscriberList
    
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('Update Fail','Update Fail','');                
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {  
                        if(data.IMPORTSTATUS==1)                                              
                        {
                            alertBox("Individual import has been added on queue. Please waiting for processing result",'Alert','');
                        }
                        else
                        {
                            alertBox("Individual import has been added on queue. Please waiting for approval from administrator.",'Alert','');
                        }
                        
                        $("#ListTextImport").val('');
                        fnGetImportIndividualSumary();
                        //
                    } else {
                        
                        alertBox(data.MESSAGE,'Update Fail','');
                    }                
                }
            });
            /*
            $('#processingPayment').show();                      
            var table = $('#import-indi-result').dataTable({
                "bStateSave": false,
                "iStateDuration": -1,
                "bProcessing": true,
                "bFilter": false,
                "bServerSide":true,
                "bDestroy":true,
                "sPaginationType": "input",
                "bLengthChange": false,
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "aoColumns": [
                    {"mData": "NUM", "sName": 'Contact Number', "sTitle": 'Contact Number',"bSortable": false, "sWidth": "250px"},
                    {"mData": "MSG", "sName": 'Result', "sTitle": "Result","bSortable": false,"sWidth": "350px"}
                ],
                "sAjaxDataProp": "LIST",
                "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=ImportListContactToSubcriberList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                    
                    aoData.push(
                        { "name": "inpListNumber", "value": arrayCheck.toString()},
                        { "name": "inpSubcriberListID", "value": subscriberList}
                    );
    
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(data) {                            
                            fnCallback(data);
                            
                            if (data.RXRESULTCODE == 1) {                                                                                 
                                showIndividualModalResult(data.LIST);                        
                            } else {
                                
                                alertBox(data.MESSAGE,'Update Fail','');
                            } 
                            $('#processingPayment').hide();   
                        }
                    });
                }
            });
            
            */                        
            
        }         
    }
    
    function showIndividualModalResult(datalist)
    {
    
        //
        var uploadSuccess=0;
        var uploadFail=0;
        for(i=0; i< datalist.length; i++)
        {
            if(datalist[i].MSG==="Import Success")
            {
                uploadSuccess=uploadSuccess+1;
            }
            else
            {
                uploadFail=uploadFail+1;
            }
        }
        
        $("#ModalImportSucess_Success").text(uploadSuccess);
        $("#ModalImportSucess_Fail").text(uploadFail);
        $("#ModalImportSucess").modal('show');
    }
    $('body').on('hidden.bs.modal', '#ModalImportSucess', function(event) {				
		$("#ListTextImport").val('');
    });
    //cancel
    $(document).on('click','#btn-cancel',function(){
        
        bootbox.dialog({
            message: "Are you sure you want to cancel this import? ",
            title: "Cancel Confirmation",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {
                        $('#mapFieldContent').empty();                        
                        $("#file-name-upload").val('');                        
                        $('#dropzone2 .dz-remove')[0].click();                        
                        document.getElementById('dropzone2').scrollIntoView(); 
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "btn btn-medium btn-back-custom",
                    callback: function () {
                        
                    }
                }
            },
            onEscape: function (){
                deleteBtn.attr('disabled', false);
            }
        });
        
    });
    // save
    $(document).on('click','#btn-save',function(){

        var totalField=0;
        var listField=[];
        var colNumberPhone=0;
        var subscriberList= $("#SubscriberList").val();        
        var fileName = $("#file-name-upload").val();
        var orgFileName = $("#file-name-upload-org").val();

       
       
        var inpImportID=guid();
        var running=0;
        var haveDuppliateField=0;
        $('.cdf-data').each(function(){
            
            running=running+1;
            totalField=totalField+1;                                    

            var cdfText = $(this).find('option:selected').text();

            if( $(this).closest("th").hasClass("col-rm"))
            {
                cdfText="@dummy";
            }
            else if($(this).find('option:selected').text()=='PhoneNumber'){
                var cdfText = 'ContactString_vch';
            }
            else
            {
                //cdfText="_"+cdfText;
            }
            cdfText = cdfText.replace(/ /g, '_');
            

            if(listField.indexOf(cdfText) > -1 && cdfText !== "@dummy" && !$(this).closest("th").hasClass("col-rm"))
            {
                alertBox('A column with that field name already exists.  Please select a unique field name to continue.','Duplicate Field Name','');
                haveDuppliateField=1;
                return false; 
            }
            
            listField.push(cdfText);
            
            if(colNumberPhone==0 && cdfText=='ContactString_vch')
            {
                colNumberPhone=running;
            }
            
            
        });
        if(colNumberPhone==0)
        {
            alertBox('Phone number should be required field','Alert','');
            return false;
        }
        if(subscriberList.length && totalField > 0 && fileName.length && colNumberPhone>0 && haveDuppliateField==0)
        {
            //
            $.ajax({
                url:'/session/sire/models/cfc/user-import-contact.cfc?method=SaveCSVContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {
                    inpNewName:fileName,
                    inpSubcriberListID: subscriberList,
                    inpCampaignID: 0,
                    inptProcessType: 1,
                    inpImportID: inpImportID,
                    inpColumnNumber: totalField,
                    inpColumnNames: listField.toString(),
                    inpContactStringCol: colNumberPhone,
                    inpSeparator: ',',
                    inpSkipLine: impSkipLine,
                    inpErrorsIgnore: 0,
                    inpSendToQueue: 1,
                    inpOrgFileName: orgFileName
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('There was a connectivity issue when trying to import your list.  This could be a result of local network issues or a slow connection causing the import to timeout.  Please check your internet connection and try again.','Connectivity Issue','');                
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {                        
                        $('#AddNewSubscriberList').modal('hide');  
                        var mes= "You’re list is now uploading. This may take a few minutes depending on the file size.  Check back in a few or click refresh to see the status of your import.";
                        bootbox.dialog({
                            message: mes,
                            title: "Alert",
                            buttons: {
                                success: {
                                    label: "Ok",
                                    className: "btn btn-medium btn-success-custom",
                                    callback: function() {
                                        //
                                        location.href="/session/sire/pages/user-import-contact-file";   
                                        //
                                    }
                                }
                            },
                            onEscape: function (){
                                deleteBtn.attr('disabled', false);
                            }
                        });                                                                                       
                        
                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox("Upload Import Fail",'Oops!','');
                    }                
                }
            });
            //
            
            
        }        
    });
    // ADD NEW SUBCRIBER LIST
    $('#AddNewSubscriberList #btn-save-group-sub').click(function(){
        if ($('#frm_subscriber_list').validationEngine('validate', {scroll: false ,focusFirstField : false})) {
            $('#AddNewSubscriberList button').prop('disabled', true);
            var groupName = $('#subscriber_list_name').val();
            var inpCPE = $('#btn-save-group-sub').data('inpCPE');

            $.ajax({
                url:'/session/sire/models/cfc/control-point.cfc?method=AddGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'post',
                dataType: 'json',
                data: {INPGROUPDESC: groupName},
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    alertBox('Create Subscriber Fail','Create New Subscriber','');
                    $('#AddNewSubscriberList button').prop('disabled', false);
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (data.RXRESULTCODE == 1) {
                        //
                        $("#subscriber_list_name").val('');
                        $('#AddNewSubscriberList').modal('hide');
                        $(".SubscriberList").select2(
                            { theme: "bootstrap", width: 'auto'}
                        );                                
                        //listData
                        var o = $("<option/>", {value: data.INPGROUPID, text: data.INPGROUPDESC});
                        $('.SubscriberList').append(o);                        
                        $('.SubscriberList').val(data.INPGROUPID);    

                    } else {
                        /*modalAlert('Add New Subscriber List', data.MESSAGE);*/
                        alertBox(data.MESSAGE,'Create New Subscriber','');
                    }
                    $('#AddNewSubscriberList button').prop('disabled', false);
                }
            });
        }
    });    
    // modal show
    $("table").on('click', 'a.editable', function(event){
		event.preventDefault();
		var id = $(this).data("id"), type = $(this).data('type'), modal = $("#show-contact");
		modal.modal('show');



		modal.find('h4.modal-title').text(type+" Records");
		modal.find('input[name="id"]').val(id);

		switch(type){ 
			case "invalid": type = CT_INVALID; break;
			case "duplicate": type = CT_DUPLICATE; break;
            case "valid": type = CT_VALID; break;
            case "dnc" : type=CT_DNC; break;
			case "deleted": type = CT_DELETED; break;
			default: type = 0;
		} 

		if(type != 10){
			modal.find('button.visible').addClass('hidden');
		} else {
			modal.find('button.visible').removeClass('hidden');
		}

		modal.find('input[name="type"]').val(type);
		refreshContactList = GetContact(id, type);

		if (type == -2) {
			modal.find('button[name="deleteSelectedItem"]').addClass('hidden');
		} else {
			modal.find('button[name="deleteSelectedItem"]').removeClass('hidden');
		}
    });
    //save
    $('#update-contact').on('click', 'button[name="save"]', function(event){
        
        thisModal = $('#update-contact');
        var inpContact = thisModal.find('input[name="contact"]').val(),
            inpFileRecordID	= thisModal.find('input[name="fileID"]').val(),
            inpItemID = thisModal.find('input[name="id"]').val(),
            inpTimezone = thisModal.find('select[name="contact-tz"]').val(),
            type =  thisModal.find('input[name="type"]').val();

        $.ajax({
            url: "/session/sire/models/cfc/import-contact.cfc?method=updateContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            type: "POST",
            dataType: "json",
            data: {inpFileRecordID: inpFileRecordID,inpItemID: inpItemID,inpContact: inpContact, inpTimezone: inpTimezone, inpType: type},
            success: function(data){
                if(data.RXRESULTCODE == 1){
                    thisModal.modal('hide').find('div.alert').addClass('hidden');
                    refreshContactList();
                    fnGetDataList();
                } else {
                    thisModal.find('div.alert').html(data.MESSAGE).removeClass('hidden');
                }
            }
        });
    });
    //delete
    $("table#show-contact-ds").on('click', 'button.action', function(event){
		event.preventDefault();
		var deleteBtn = $(this);
		var action = $(this).data('action'), id = $(this).data('id'), contact = $(this).data('contact'), timezone = $(this).data('timezone');
		var thisModal = $('#show-contact');
		var inpFileRecordID = thisModal.find('input[name="id"]').val();
		var type = thisModal.find('input[name="type"]').val();

		if(action == "delete"){
			deleteBtn.attr('disabled', true);
			bootbox.dialog({
				message: "Are you sure you want to delete this number? ",
			    title: "Delete Confirmation",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	deleteContacts([id], inpFileRecordID, function(){
                                refreshContactList();
                                fnGetDataList();
			            	}, type, 0);
			            	deleteBtn.attr('disabled', false);
			            }
			        },
			        cancel: {
			        	label: "Cancel",
			            className: "btn btn-medium btn-back-custom",
			            callback: function () {
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function (){
			    	deleteBtn.attr('disabled', false);
			    }
			});
		}
		if(action == "edit"){
			var updateModal = $('#update-contact');
			updateModal.on('hide.bs.modal', function(){
				updateModal.find('div.alert').addClass('hidden');
			});
			updateModal.find('input[name="contact"]').val(contact);
			updateModal.find('input[name="fileID"]').val(inpFileRecordID);
			updateModal.find('input[name="id"]').val(id);
			updateModal.find('input[name="type"]').val(type);
			updateModal.find('select[name="contact-tz"] option').each(function(){
				if(this.value == timezone){
					$(this).prop('selected', true);
				}
			});
			updateModal.modal('show');
		}
    });
    //check alll
    $("table").not("#send-contact-table").on('click', 'input.select-all', function(event){
		if($(this).prop('checked') ==  true){
			$(this).parents('table').find('input.select-item').prop('checked', true);
		} else {
			$(this).parents('table').find('input.select-item').prop('checked', false);
		}
    });
    // delete all
    $('body').on('click', 'button[name="deleteSelectedItem"]', function(event){
		event.preventDefault();
		var deleteBtn = $(this);
		deleteBtn.attr('disabled', true);
		var thisModal  = $('#show-contact');
		var deleteAll = $(this).data("delete-all");

		var selectedItems = thisModal.find('table input.select-item:checked');
		var selectedIDs = new Array();
		selectedItems.each(function(index){
			selectedIDs.push($(this).val());
		});
		var type = thisModal.find('input[name="type"]').val();

		if(selectedIDs.length || deleteAll){
			if (!deleteAll) {
				var message = "Are you sure you want to delete selected number(s) from your list? ";
			} else {
				var message = "Are you sure you want to delete all contact from this list? ";
			}
			bootbox.dialog({
				message: message,
			    title: "Delete confirmation",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	var id = thisModal.find('input[name="id"]').val();
			            	var type =  thisModal.find('input[name="type"]').val();

			            	deleteContacts(selectedIDs, id, function(){
                                refreshContactList();
                                fnGetDataList();
			            	}, type, deleteAll);
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function () {
			    	deleteBtn.attr('disabled', false);
			    }
			});
		} else {
			bootbox.dialog({
				message: "No have number(s) selected!",
			    title: "Alert",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function () {
			    	deleteBtn.attr('disabled', false);
			    }
			});
		}
		
    });
    // print
    $('button.btn-print').on('click', function(event){
		var fileRecordID = $('#show-contact').find('input[name="id"]').val();
		window.location.href = "/session/sire/models/cfm/export-contact.cfm?file_id="+fileRecordID;
    });
    // history
    $("table#import-summary").on('click', 'a.show-history', function(event){
		event.preventDefault();
		var id = $(this).data('id'),fileName = $(this).data('file-name'),
		modal = $('#show-file-history');
		modal.find('input[name="id"]').val(id);

		getHistory(id);
		modal.modal('show');


		var defaultData = [{id: id, text: fileName}];
		$('select[name="file-name"]').html("");	 
    });
    //
    $("form[name='file-update-history'] button[name='view']").on('click', function(event){
		var thisModal = $('#show-file-history');
		var thisForm = $("form[name='file-update-history']");
		var fileID = thisModal.find('input[name="id"]').val();
		var fileDateStart = thisForm.find('input[type="hidden"][name="file-date-start"]').val(),
		    fileDateEnd = thisForm.find('input[type="hidden"][name="file-date-end"]').val();
		    var date = {start: fileDateStart, end: fileDateEnd};
		getHistory(fileID, date);
	});

	$("form[name='file-update-history'] button[name='clear']").on('click', function(event){

		var thisModal = $('#show-file-history');
		var fileID = thisModal.find('input[name="id"]').val();
		thisModal.find('input[name="file-date"]').val("");
		getHistory(fileID);
    });
    //
    $(document).on('click','.admin-action-indi',function(){
        var messconfirm="";
        var action=$(this).data('action');
        var id=$(this).data('id');
        var type=$(this).data('type');
        if(action.length)
        {
            ViewIndividualImportData(id,type);                   
        }
    });
    function ViewIndividualImportData(id,type)
    {
        //aa
        $("#tbl-indi-view-data").html('');  
        var formatContactString = function(object){
            return '<span class="format-phone-number">'+object.PHONENUMBER+'</span>';     
        }  

        var table = $('#tbl-indi-view-data').dataTable({
            "bStateSave": false,                
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": formatContactString, "sName": 'Contact Number', "sTitle": 'Contact Number', "bSortable": false, "sClass": "", "sWidth": "300"}                    
            ],
            "sAjaxDataProp": "aaData",
            "sAjaxSource": '/session/sire/models/cfc/user-import-contact.cfc?method=GetIndiDataByUploadId'+strAjaxQuery, 
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    {                            
                        "name": "inpUploadId", "value": id                                                                               
                    }
                );
                aoData.push(
                    { "name": "inpType", "value": type}
                );
                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                    }
                });
            },            
            "fnInitComplete":function(oSettings){
                $('.format-phone-number').mask('(000) 000-0000');
            }              
        });  
        $("#md-indi-view-data").modal('show');
    }
    //
    $("#individual-import").addClass("disabled");
    $(document).on("blur","#ListTextImport",function(){
        if($(this).val().length > 9)
        {
            $("#individual-import").removeClass("disabled");
        }
        else
        {
            $("#individual-import").addClass("disabled");
        }
    });
    //
    $(document).on("click",".remove-column",function(){
        $(this).addClass("keep-column");
        $(this).removeClass("remove-column");


        var colnum = $(this).closest("th").prevAll("th").length;
        $(this).closest("table").find("tr").find("td:eq(" + colnum + ")").addClass("col-rm");
        $(this).closest("th").addClass("col-rm");
                

        $(this).html("Undo");
    });
    $(document).on("click",".keep-column",function(){
        $(this).removeClass("keep-column");
        $(this).addClass("remove-column");
        
        var colnum = $(this).closest("th").prevAll("th").length;
        $(this).closest("table").find("tr").find("td:eq(" + colnum + ")").removeClass("col-rm");
        $(this).closest("th").removeClass("col-rm");

        $(this).html("Remove");
    });
    
    //
    var table = $('#import-summary').dataTable();				
    var table1 = $('#individual-summary').dataTable();
    setInterval(function(){                 
        table.fnDraw(false);                    
        table1.fnDraw(false);  
    }, 10000);	  
}); 
    