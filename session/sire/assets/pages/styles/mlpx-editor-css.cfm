<!--- Experiment: Store css as .cfm so we can have better code documentation comments - cf comments are removed in final served page automatically --->

<!--- MMLP Editor CSS --->

<style>
	
	body, html
	{		
		margin: 0 0;
		background-color: #274e60;	
	}

    .mlp-btn
    {
	    display: inline-block;
	    vertical-align: middle;
	    padding: 10px 24px;
	    border-radius: 15em;
	    border: 1px solid;
	    margin: 2px;
	    min-width: 60px;
	    font-size: .875rem;
	    text-align: center;
	    outline: 0;
	    line-height: 1;
	    cursor: pointer;	
	    color: #FFF;
	    background-color: #74c37f;
	    border-color: #74c37f;
	    text-transform: uppercase;
	    font-weight: 700;    
    }	
    
    .mlp-btn:hover
    {
	    background-color: #83dd8f;
	}	
    
	.widgets-panel
	{
		height: 100%;
		width: 100%;
		background-color: #274e60;	
		color: #fff;
		padding: 2em 2em 0 2em;
		position: relative;
		display: block;
		visibility: visible;
		transition: all .4s ease-out;
	    
	    
	    -webkit-align-content: flex-start;
	    -ms-flex-line-pack: start;
	    align-content: flex-start;
	    -webkit-align-items: flex-start;
	    -ms-flex-align: start;
	    align-items: flex-start;
	    display: -webkit-flex;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-flex-wrap: wrap;
	    -ms-flex-wrap: wrap;
	    flex-wrap: wrap;
	    -webkit-justify-content: flex-start;
	    -ms-flex-pack: start;
	    justify-content: flex-start;
	    border-left: dashed 1px rgba(177,186,204,.5);
	}
	
	.panel-active
	{
		color: #74c37f;
	}
			
	.panel-close-button
	{
		position: absolute;
		top: 1em;
		right: 1em;	
		color: #fff;	
		z-index:30; 
	}
	
	#darktooltip-section-delete
	{

	}
	
	
	.coder-panel, .publish-mlp-panel, .mlp-section-settings, .mlp-history-panel
	{
		height: 100%;
		width: 100%;
		background-color: #274e60;	
		color: #fff;
		padding: 2em 2em 0 2em;
		display: block;
	    transition: all .4s ease-out;	    
	    display: none;
	    position: relative;
	    border-left: dashed 1px rgba(177,186,204,.5);
	        	    		
	}
		
	.mlp-section-settings
	{
		color: #ccc;		
	}
	
	.mlp-section-settings h1, .mlp-section-settings h2, .mlp-section-settings h3, .mlp-section-settings h4, .mlp-section-settings h5, .mlp-section-settings i
	{
		color: #fff;		
	}
	
	#stage-container
	{		
	    height: 100%;
	    position: relative;
	    transition: all .4s ease-in-out;
	    width: 100%;
		left: 0;
		
	}

	#top-menu
	{
		display: block;
	    position: relative;
 	    z-index: 1; 
        width: 100%;
		
	}
	
	#top-menu-content
	{
		background: #fff;
	    border-bottom: 1px solid #b1bacc;
		height: 72px;
	    margin: 0;
	    padding: 0 20px 0 12px;
	    position: relative;
        width: 100%;
	}
	
	#main-stage
	{			
		display: block;
	    position: relative;
	    transition: -webkit-transform .4s linear;
	    transition: transform .4s linear;
	    transition: transform .4s linear,-webkit-transform .4s linear;
        width: 100%;
        overflow: visible;        			
	}

	#main-stage-content
	{
		background-color: #ffffff;	
		height: calc(100vh - 1px);
		overflow: auto;
	    position: relative;
	    width: 100%;	
	}

	.tile-item-container
	{		
	    display: block;
	    -webkit-flex: 0 0 117px;
	    -ms-flex: 0 0 117px;
	    flex: 0 0 117px;
	    margin-right: 6px;		
	}

	.tile-item
	{
		background: rgba(177,186,204,.5);
	    border-radius: 3px;
	    cursor: move;
	    display: -webkit-flex;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-flex-direction: column;
	    -ms-flex-direction: column;
	    flex-direction: column;
	    height: 84px;
	    margin: 0 0 6px;
	    opacity: 1;
	    padding: 0 3px 10px;
	    text-align: center;
	    transition: background-color .15s ease,opacity .15s ease,-webkit-transform .2s ease;
	    transition: background-color .15s ease,opacity .15s ease,transform .2s ease;
	    transition: background-color .15s ease,opacity .15s ease,transform .2s ease,-webkit-transform .2s ease;
	    width: 117px;	    	    	
	}

	.tile-item:hover
	{
		background: rgba(227,236,254,.5);
	}
		
	.tile-item-icon 
	{
	    color: #fff;
	    font-size: 1.75em;
	    margin: 12px auto 6px;
	}

	.tile-item-label {
	    color: #fff;
	    font-size: .75em;
	    line-height: 1;
	}
		
	#SaveContent
	{
		float:right;	
	}	
	
	
	
	.MLPMasterSection
	{			
		position: relative;				
	}
	
	.MLPMasterSectionEmpty
	{
<!---  		border: 1px solid #d9d9d9;  --->
	}
	
	.MLPMasterRowEmpty
	{
 		border: 1px solid #d9d9d9; 
		min-height: 6em;	
	}
	
	.MLPMasterSectionEmpty:hover, .MLPMasterRowEmpty:hover
	{
		outline: dashed 1px rgba(96, 109, 188, 0.68) !important;
		box-shadow: 0 0 3pt 2pt rgba(96, 109, 188, 0.68);
	}
	
	.MLPMasterSectionEdit
	{			
		position: absolute; 
		top: .5em;
		left: .5em;	
		text-align: center;	
		display:none;
		box-sizing: border-box; 
	}
	
	.MLPMasterSectionEdit:hover
	{
		cursor: pointer;
	}

	.MLPMasterSectionContainer
	{
		position: relative;	
		
	}
	
	.MLPMasterSectionContainer:hover .MLPMasterSectionEdit
	{		
		display:block;		
	}
			
	.MLPEditMenuContainer
	{		
	    left: 0;
	    right:0;
		top: 0; 
	    position: absolute;
	}

	div.MLPEditMenu
	{
		left: 0;
	    right:0;
		top: 0; 
		margin-top: -1.75em;  
	    position: absolute;
	    display: none;  
		text-align: center;  		
		border: 5px none rgba(0, 0, 0, 0);
	}
	
	.MLPEditable:hover {
       outline: dashed 0px rgba(96, 109, 188, 0.68) !important;
       box-shadow: 0 0 3pt 2pt rgba(96, 109, 188, 0.68);
	}
	
	<!--- For special case master div - show the edit menu inside the div so it is not clipped at top of overflow --->
	#MLP-Master-Section > div.MLPEditMenu
	{
		margin-top: 0 !important; 		
	}
			
	.MLPEditMenu .mlp-icon, .MLPMasterSectionEdit .mlp-icon {
	   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#a7cfdf+0,23538a+100;Blue+3d+%238 */
		background: #a7cfdf; /* Old browsers */
		background: -moz-linear-gradient(top, #a7cfdf 0%, #23538a 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #a7cfdf 0%,#23538a 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #a7cfdf 0%,#23538a 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a7cfdf', endColorstr='#23538a',GradientType=0 ); /* IE6-9 */	    border-radius: 50%;
	    color: #fff;
	    font-size: 1.125rem;
	    height: 27px;
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;
	    width: 27px;
	    line-height: 18px;
	}
	
	.MLPEditMenu .mlp-icon:hover, .MLPMasterSectionEdit .mlp-icon:hover {
	    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#c5deea+0,8abbd7+31,066dab+100;Web+2.0+Blue+3D+%231 */
		background: #c5deea; /* Old browsers */
		background: -moz-linear-gradient(top, #c5deea 0%, #8abbd7 31%, #066dab 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #c5deea 0%,#8abbd7 31%,#066dab 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #c5deea 0%,#8abbd7 31%,#066dab 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c5deea', endColorstr='#066dab',GradientType=0 ); /* IE6-9 */
		-webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		cursor: pointer;
	}
	
	.MLPEditMenu .mlp-icon-alert {
	    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff3019+0,cf0404+100;Red+3D */
		background: #ff3019; /* Old browsers */
		background: -moz-linear-gradient(top, #ff3019 0%, #cf0404 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff3019 0%,#cf0404 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff3019 0%,#cf0404 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff3019', endColorstr='#cf0404',GradientType=0 ); /* IE6-9 */
	    border-radius: 50%;
	    color: #fff;
	    font-size: 1.125rem;
	    height: 27px;
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;
	    width: 27px;
        line-height: 18px;
	}

	.MLPEditMenu .mlp-icon-alert:hover {
	   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff1a00+0,ff1a00+100;Red+Flat+%231 */
		background: #ff1a00; /* Old browsers */
		background: -moz-linear-gradient(top, #ff1a00 0%, #ff1a00 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff1a00 0%,#ff1a00 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff1a00 0%,#ff1a00 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff1a00', endColorstr='#ff1a00',GradientType=0 ); /* IE6-9 */
	    -webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
	    
	}

	:focus {
	    outline:none;
	}

	.dark-tooltip.dark ul.confirm li.darktooltip-yes
	{
		background-color: #930000;		
	}
	
	.dark-tooltip.dark ul.confirm li.darktooltip-yes:hover
	{
		background-color: #bb0000;		
	}
	

	.color_input, .cssinput {
	    border: 0px !important;
	    margin-bottom: 0px !important;
	    width: 108px	!important;
	    color: #333333 !important;
	    min-height: 34px;
		line-height: 34px;
	  }

	.coder-panel span.colorChooser {
	   
	    margin-left: .1em;
	    position: relative;
	    display: inline-block;
	    overflow: visible;
	    vertical-align: middle;
	    background-image: none !important;
	    xwidth: 64px !important;
	    margin-bottom: .1em !important;
	    cursor: pointer !important;
	    border: 1px solid #CCCCCC;	    	    
	}	
		
	.DropWidth
	{
		width:100% !important;		
	}	
	
	.coder-panel .select2-container
	{
		width:100% !important;		
	}
	
	.coder-panel input
	{
		padding-left: 12px;	
	}
	
	
	#mlp-section-settings .section-label, #mlp-header-settings .section-label, #MLPCoder .section-label, #mlp-toc .section-label, #mlp-toc-panel .section-label
	{
		margin-right: 1em;
		text-transform: uppercase;	
		font-size: .8em;	
	}
	
	.MLPImagePlaceHolder
	{
		
		min-height:6em; 
		text-align:center; 
		line-height:6em; 
		border:dashed 1px ##ccc;
	}
		
	.hover-menu-container
	{
		left: 0;
	    right: 0;
	    top: 2em;
	    position: absolute;
		display: none; 
	    text-align: center;
	    border: 5px none rgba(0, 0, 0, 0);		
	}
	
	<!--- Hover menu options --->
	.hover-menu-options
	{
		background-color: #030030;
		border-color: #030030;
		align-items: center;
	    border-radius: 2em;
	    box-shadow: 0 0 5px rgba(0,0,0,.4);
	    padding: 1.5em 1em 1em 1em;
	    text-align: center;  
	    color: #ddd; 		    
	}
	
	.hover-menu-options i
	{
		font-size: 1.5em;		
		
	}
	.hover-menu-options i:hover
	{
		color: #fff;
		cursor: pointer;
	}
		
	<!--- Center modal vertically --->
	<!--- <!--- https://codepen.io/dimbslmh/full/mKfCc ---> --->
	.modal {
	  text-align: center;
	  padding: 0!important;
	}
	
	.modal:before {
	  content: '';
	  display: inline-block;
	  height: 100%;
	  vertical-align: middle;
	  margin-right: -4px;
	}
	
	.modal-dialog {
	  display: inline-block;
	  text-align: left;
	  vertical-align: middle;
	}
	
	<!--- Image preview section --->
	
	.scaled-frame {		
		zoom: 0.50;		
		-moz-transform: scale(0.50);
		-moz-transform-origin: 0 0;
		-o-transform: scale(0.50);
		-o-transform-origin: 0 0;
		-webkit-transform: scale(0.50);
		-webkit-transform-origin: 0 0;
	}
	
	@media screen and (-webkit-min-device-pixel-ratio:0) {
		.scaled-frame  { zoom: 1;  }
	}
		
	.dz-success-mark, .dz-error-mark
	{
		display: inline;		
	}	
	
	#ImageChooserModal .modal-body img {
	    vertical-align: middle;
	    max-width: 100%;
	    max-height: 100%;
	}
	
	.tab-content {
	    border-left: 1px solid #ddd;
	    border-right: 1px solid #ddd;
	    border-bottom: 1px solid #ddd;
	    padding: 10px;
	}
	
	.nav-tabs {
	    margin-bottom: 0;
	}
	
	.dark-tooltip.small{  max-width:250px; }
	.dark-tooltip.medium{ max-width:250px; }
	.dark-tooltip.large{  max-width:250px; }
	
			
	.preview-icon:hover, .preview-icon:hover .tile-item-label, .mlp-historical-date:hover
	{
		color: #74c37f;
		cursor: pointer;		
	}	
	
	#cppxNameDisplay:hover
	{
		color: #74c37f;
		cursor: pointer;	
		text-decoration: underline;	
	}
	
	#RawCode { white-space: pre; }


	#mlp-section-settings .fa:hover, #mlp-section-settings #alignment .active, #arrow-border .active, .mlp-icon .active, #mlp-toc-panel #alignment .active, #mlp-toc-panel .fa:hover
	{		
		color: #74c37f;
		cursor: pointer;		
	}
	
	.SettingsOverlay
	{
		
		position: absolute;
	    width: 100%;
	    height: 100%;
	    left: 0;
	    top: 0;
	    zIndex: 1000000;  
	    opacity: .3;		    
	    background: 
		  /* On "top" */
		  repeating-linear-gradient(
		    45deg,
		    transparent,
		    transparent 10px,
		    #606dbc 10px,
		    #606dbc 20px
		  ),
		  /* on "bottom" */
		  linear-gradient(
		    to bottom,
		    #eee,
		    #999
		  );		  	
	}
		
	#tblListHistory thead, table.dataTable tr:nth-child(even)
	{
		color: #fff;		
	}

	.mlp-section-settings .font-style
	{
		color: #fff;
		font-size: 1.2em;
		
	}

	.font-style:hover
	{
		color: #74c37f;
		cursor: pointer;
	}


	.mlp-section-settings .active
	{
		color: #74c37f !important;
	}


	.CustomHelperObj
	{
		border: dashed 3px rgba(96, 109, 188, 0.68) !important;
		-webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);

		width:150px;
		max-width:150px; 
		
		height:75px;
		max-height:75px;

		background-color: white;
		text-align: center;
		position: relative;
		display: table;
	}

	.CustomHelperObj .mlp-icon{
		
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#b4ddb4+0,83c783+17,52b152+33,008a00+67,005700+83,002400+100;Green+3D+%231 */
		background: #b4ddb4; /* Old browsers */
		background: -moz-linear-gradient(top, #b4ddb4 0%, #83c783 17%, #52b152 33%, #008a00 67%, #005700 83%, #002400 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #b4ddb4 0%,#83c783 17%,#52b152 33%,#008a00 67%,#005700 83%,#002400 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #b4ddb4 0%,#83c783 17%,#52b152 33%,#008a00 67%,#005700 83%,#002400 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4ddb4', endColorstr='#002400',GradientType=0 ); /* IE6-9 */

	    border-radius: 50%;
	    -webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
	    color: #fff;
	    font-size: 1em;	   
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;	    
	}
	
	.CustomHelperObj .mlp-obj-name-container
	{
		display: table;
		width:150px; 
		height:75px;		
	}
	
	.CustomHelperObj .mlp-obj-name
	{
		display: table-cell; 
		vertical-align: middle;
	}

	#background-image-section, 
	#width-height-section
	{
	    display:none;
	}
	
	
	.popline .popline-textfield
	{
		color: black;
		
	}
	
	.mlp-hidden
	{
		display:none;		
	}

	.ui-state-highlight 
	{ 	
	
		min-height: 2em; 
		line-height: 1.2em; 
		background-color: #cc2f2f;
		 background: 
		  /* On "top" */
		  repeating-linear-gradient(
		    45deg,
		    transparent,
		    transparent 10px,
		    #cc2f2f 10px,
		    #cc2f2f 20px
		  ),
		  /* on "bottom" */
		  linear-gradient(
		    to bottom,
		    #eee,
		    #999
		  );		
	}
	
	
	#MLP-Master-Section
	{
		
		min-height: 100vh;
	}


	.social-tags-section
	{
		padding: 1em;
		border-radius: 4px;
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#feffe8+0,d6dbbf+100;Wax+3D+%231 */
		background: #feffe8; /* Old browsers */
		background: -moz-linear-gradient(top, #feffe8 0%, #d6dbbf 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #feffe8 0%,#d6dbbf 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #feffe8 0%,#d6dbbf 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffe8', endColorstr='#d6dbbf',GradientType=0 ); /* IE6-9 */
	}

	.social-tags
	{		
		width: 100%;
	    margin-bottom: 1em;
		border-radius: 4px;
	}
	
	.social-label
	{
		margin-bottom: 0;		
	}
	
	
	<!--- TOC Styles --->
	.trigger-MLP-TOC-Add-Topic
	{
			
	}
	
	a .trigger-MLP-TOC-Add-Topic{outline : none;}
	
	.focus a .trigger-MLP-TOC-Add-Topic, .trigger-MLP-TOC-Add-Topic:focus{
		outline: none !important;
		color: #337ab7;
		text-decoration: none;
	}
	
	.trigger-MLP-TOC-Add-Topic:hover, .trigger-MLP-TOC-Add-Topic:active
	{
		color: #74c37f;	
		cursor: pointer;	
	}
	
	.MLP-Top-Topic-Link
	{		
		text-decoration: none;
	}
	
	.MLP-Top-Topic-Link:hover
	{
		
		text-decoration: underline;
	}
			
	.MLP-Top-Topic:hover, .MLP-Top-Topic h5:hover, .MLP-Top-Topic-Link:hover, .MLP-Top-Topic-Link h5:hover 
	{
	 	color: #74c37f;	
		cursor: pointer;
		
	}
	
	.context-menu-active > a h5
	{
		text-decoration: underline;
		color: #74c37f;
		outline: red;
		outline-style: dotted;
		outline-width: 1px;				
	}
	
	.context-menu-active > a h5::selection{ background: none; }
		
	.MLPSubTopicsSection
	{
		<!--- display: none; --->
		padding-left: .5em;	
	}
	
	.MLPSubTopicsSectionDragOver
	{
		min-height: 1em;;
		
		
			
	}
	
	<!--- Dont show lines for first child - only the 2cd child on --->
	#mlp-toc-panel .MLPSubTopicsSection:nth-child(n+2)
	{
		margin-left: 1em;	
		border-left:dashed 1px #fff;		
		padding-left: .5em;	
		
	}	
	
<!---
	.MLP-Top-Topic-Link h5
	{
		
		min-height: 1em;
		
	}
--->
	
</style>



<!--- Basic flex grid layout --->
<style>
	
	body, html
	{
	  margin: 0;
	  background: #274e60;
	} 
	
	.wrapper{
	  min-height: 100vh;
	  background: #274e60;
	  display: flex;
	  flex-direction: column;
	}
	
	.header{
	  padding: 1em;
<!--- 	  height: 78px; --->
	  background: #274e60;
	  color: #fff;
	}
			
	.content {
	  display: flex;
	  flex: 1;
	  background: #999;
	  color: #000; 
	}
	
	.columns{
	  display: flex;
	  flex:1;
	}
	
	.main{
	  flex: 1;
	  order: 3;
	  background: #fff;
	  margin-bottom: -10em;		
	  background-color: #274e60;
	}
	
	<!--- Navigation options --->
	.sidebar-first
	{			
		width: 72px;
		order: 1;
		background: #274e60;	
		opacity: 1;
		position: relative;
		visibility: visible;
		height: 100%;
	    
	}
	
	.side-nav {
	    display: -webkit-flex;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-flex-direction: column;
	    -ms-flex-direction: column;
	    flex-direction: column;
	    list-style: none;
	    margin: 0;
	    padding: 1em 0 0 .5em;
	    color: #fff;
	    
	}

	.side-nav .menu-item {
	    cursor: pointer;
	    margin-bottom: 12px;
	    text-align: center;
	}

	.side-nav .menu-item:hover {
		color: #83dd8f;
	}
	
	
	.side-nav .menu-item i{
	    font-size: 2em;
	}
	
	.side-nav .menu-item p{
	    font-size: 1em;
	}

	<!--- Tool Panels --->
	.sidebar-second{ 
	  width: 310px;
	  order: 2;
	  background: #274e60;
	  height: 100%;
      transition: all .4s ease-out;
      position: relative;
     
	}
		
			
</style>	


<style>
	
	
	.ui-widget-header {
	    border: 1px solid #dddddd;
	    background: #e9e9e9;
	    color: #333333;
	    font-weight: bold;
	}

	.ui-widget-content {
	    border: 1px solid #dddddd;
	    background: #ffffff;
	    color: #333333;
	}
	
	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
	    border: 1px solid #dad55e;
	    background: #fffa90;
	    color: #777620;
	}
	
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
	    border: 1px solid #c5c5c5;
	    background: #f6f6f6;
	    font-weight: normal;
	    color: #454545;
	}
	
	.ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
	    display: block;
	    position: absolute;
	    left: 50%;
	    margin-left: -8px;
	    top: 50%;
	    margin-top: -8px;
	}
	
	.ui-widget-header .ui-icon {
	    background-image: url(/public/sire/js/jquery-ui-1.12.1.custom/images/ui-icons_444444_256x240.png);
	}
	
	.ui-datepicker .ui-datepicker-next {
	    right: 2px;
	}
	
	.ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next {
	    position: absolute;
	    top: 2px;
	    width: 1.8em;
	    height: 1.8em;
	}
	
	.ui-widget-header a {
	    color: #333333;
	}

	.ui-icon-circle-triangle-e {
	    background-position: -48px -192px;
	}
	
	.ui-icon {
	    width: 16px;
	    height: 16px;
	}
	
	.ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
	    display: block;
	    position: absolute;
	    left: 50%;
	    margin-left: -8px;
	    top: 50%;
	    margin-top: -8px;
	}
	
	
	.ui-widget-header .ui-icon {
	    background-image: url(/public/sire/js/jquery-ui-1.12.1.custom/images/ui-icons_444444_256x240.png);
	}
	
	.ui-datepicker .ui-icon {
	    display: block;
	    text-indent: -99999px;
	    overflow: hidden;
	    background-repeat: no-repeat;
	    left: .5em;
	    top: .3em;
	}
	
	.ui-icon-circle-triangle-w {
	    background-position: -80px -192px;
	}



/*!
 * jQuery UI Datepicker 1.9.0
 * http://jqueryui.com
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Datepicker#theming
 */
.ui-datepicker {
    display: none;
    padding: .2em .2em 0;
    width: 17em;
}

.ui-datepicker .ui-datepicker-header {
    padding: .2em 0;
    position: relative;
}

.ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next {
    height: 1.8em;
    position: absolute;
    top: 2px;
    width: 1.8em;
}

	

.ui-datepicker .ui-datepicker-prev-hover, .ui-datepicker .ui-datepicker-next-hover { top: 1px; }

.ui-datepicker .ui-datepicker-prev { left: 2px; }

.ui-datepicker .ui-datepicker-next { right: 2px; }

.ui-datepicker .ui-datepicker-prev-hover { left: 1px; }

.ui-datepicker .ui-datepicker-next-hover { right: 1px; }

.ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
    display: block;
    left: 50%;
    margin-left: -8px;
    margin-top: -8px;
    position: absolute;
    top: 50%;
}

.ui-datepicker .ui-datepicker-title {
    line-height: 1.8em;
    margin: 0 2.3em;
    text-align: center;
}

.ui-datepicker .ui-datepicker-title select {
    font-size: 1em;
    margin: 1px 0;
}

.ui-datepicker select.ui-datepicker-month-year { width: 100%; }

.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year { width: 49%; }

.ui-datepicker table {
    border-collapse: collapse;
    font-size: .9em;
    margin: 0 0 .4em;
    width: 100%;
}

.ui-datepicker th {
    border: 0;
    font-weight: bold;
    padding: .7em .3em;
    text-align: center;
}

.ui-datepicker td {
    border: 0;
    padding: 1px;
}

.ui-datepicker td span, .ui-datepicker td a {
    display: block;
    padding: .2em;
    text-align: right;
    text-decoration: none;
}

.ui-datepicker .ui-datepicker-buttonpane {
    background-image: none;
    border-bottom: 0;
    border-left: 0;
    border-right: 0;
    margin: .7em 0 0 0;
    padding: 0 .2em;
}

.ui-datepicker .ui-datepicker-buttonpane button {
    cursor: pointer;
    float: right;
    margin: .5em .2em .4em;
    overflow: visible;
    padding: .2em .6em .3em .6em;
    width: auto;
}

.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current { float: left; }

/* with multiple calendars */

.ui-datepicker.ui-datepicker-multi { width: auto; }

.ui-datepicker-multi .ui-datepicker-group { float: left; }

.ui-datepicker-multi .ui-datepicker-group table {
    margin: 0 auto .4em;
    width: 95%;
}

.ui-datepicker-multi-2 .ui-datepicker-group { width: 50%; }

.ui-datepicker-multi-3 .ui-datepicker-group { width: 33.3%; }

.ui-datepicker-multi-4 .ui-datepicker-group { width: 25%; }

.ui-datepicker-multi .ui-datepicker-group-last .ui-datepicker-header { border-left-width: 0; }

.ui-datepicker-multi .ui-datepicker-group-middle .ui-datepicker-header { border-left-width: 0; }

.ui-datepicker-multi .ui-datepicker-buttonpane { clear: left; }

.ui-datepicker-row-break {
    clear: both;
    font-size: 0em;
    width: 100%;
}

/* RTL support */

.ui-datepicker-rtl { direction: rtl; }

.ui-datepicker-rtl .ui-datepicker-prev {
    left: auto;
    right: 2px;
}

.ui-datepicker-rtl .ui-datepicker-next {
    left: 2px;
    right: auto;
}

.ui-datepicker-rtl .ui-datepicker-prev:hover {
    left: auto;
    right: 1px;
}

.ui-datepicker-rtl .ui-datepicker-next:hover {
    left: 1px;
    right: auto;
}

.ui-datepicker-rtl .ui-datepicker-buttonpane { clear: right; }

.ui-datepicker-rtl .ui-datepicker-buttonpane button { float: left; }

.ui-datepicker-rtl .ui-datepicker-buttonpane button.ui-datepicker-current { float: right; }

.ui-datepicker-rtl .ui-datepicker-group { float: right; }

.ui-datepicker-rtl .ui-datepicker-group-last .ui-datepicker-header {
    border-left-width: 1px;
    border-right-width: 0;
}

.ui-datepicker-rtl .ui-datepicker-group-middle .ui-datepicker-header {
    border-left-width: 1px;
    border-right-width: 0;
}

/* IE6 IFRAME FIX (taken from datepicker 1.5.3 */

.ui-datepicker-cover {
    filter: mask(); /*must have*/
    height: 200px; /*must have*/
    left: -4px; /*must have*/
    position: absolute; /*must have*/
    top: -4px; /*must have*/
    width: 200px; /*must have*/
    z-index: -1; /*must have*/
}	
</style>
	

