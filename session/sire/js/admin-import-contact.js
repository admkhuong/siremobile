(function($){
	//Define FileRecord statuses 
	const STARTLOADING = 2, PENDING = -1, DELETED = -2, JUSTCREATE = 0,  COMPLETED = 3, LOADING = 1, STOPPED = 4;
	//Define Type
	const SUBCRIBER_LIST = 1, CAMPAIGN_BLAST = 2;
	//Define contact status
	const CT_DUPLICATE = 10, CT_INVALID = 11, CT_VALID = 3, CT_DELETED = -2;

	var refreshContactList;



	$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

	    if(oSettings.oFeatures.bServerSide === false){
	        var before = oSettings._iDisplayStart;
	        oSettings.oApi._fnReDraw(oSettings);
	        //iDisplayStart has been reset to zero - so lets change it back
	        oSettings._iDisplayStart = before;
	        oSettings.oApi._fnCalculateEnd(oSettings);
	    }
	      
	    //draw the 'current' page
	    oSettings.oApi._fnDraw(oSettings);
	};


	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			  .toString(16)
			  .substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}

	$(document).on('change', ':file', function() {
		
		var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});


	$('select.non-ds').select2({
		theme: "bootstrap",
		width: 'auto'
	});

  	$("select[name='user-id'], select[name='select-user-id-filter']").select2({
  		placeholder: "Search by email or id",
  		allowClear: true,
  		ajax: {
		    url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      	return {
			        inpEmailAddress: params.term, // search term
			        page: params.page
		      	};
		    },
		    processResults: function (data, params) {
		   		return {
					results: $.map(data, function (item) {
	                    return {
	                        text: item.EMAIL,
	                        id: item.ID
	                    }
	                })
		      	};
		    },
		    cache: true
	  	},
		minimumInputLength: 1,
		theme: "bootstrap",
		width: 'auto'
  	});

	$(".create-new-subcriberlist").on('click', function(event){
		$("#new-subcriber-list").modal('show');
	});

	$("#new-subcriber-list").on('show.bs.modal', function (e) {
	  $('.txt-add-subcriber-list').val('');
	})
  	

  	$("select[name='user-id']").on('change', function(event){

  		$(".create-new-subcriberlist").addClass('hidden');

  		var thisValue = $(this).val();
  		var subcribersDropdown = $('select[name="subcriber-id"]'), campaignDromdown = $('select[name="campaign-id"]');
  		subcribersDropdown.html($('<option></option>').attr('value', 0).text("--Select Subscribers--"));
  		campaignDromdown.html($('<option></option>').attr('value', 0).text("--Select Campaign--"));
  		if(thisValue > 0){
  			$.ajax({
	  			url: "/session/sire/models/cfc/subscribers.cfc?method=GetSubcriberListByUserID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
	  			type: "POST",
	  			dataType: "json",
	  			data: {inpUserID: thisValue},
	  			success: function(data){
	  			
	  				var subcribers = data.DATALIST;
					if(subcribers.length){
						subcribers.forEach(function(item){
	  						var option = $('<option></option>').attr('value', item.ID).text(item.NAME).appendTo(subcribersDropdown);
	  					});
	  				}
	  				//subcribersDropdown.find('option[value="0"]').after($('<option></option>').attr('value', -1).text("--New Subcriber--"));
	  				subcribersDropdown.select2({
						theme: "bootstrap",
						width: 'auto'
					});

					$(".create-new-subcriberlist").removeClass('hidden');
	  			}

	  		});

  			$.ajax({
	  			url: "/session/sire/models/cfc/campaign.cfc?method=GetCampaignsByUserID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
	  			type: "POST",
	  			dataType: "json",
	  			data: {inpUserID: thisValue},
	  			success: function(data){
	  			
	  				var campaigns = data.DATALIST;
					if(campaigns.length){
						campaigns.forEach(function(item){
	  						var option = $('<option></option>').attr('value', item.ID).text(item.NAME).appendTo(campaignDromdown);
	  					});
	  					
	  				}
	  				// campaignDromdown.find('option[value="0"]').after($('<option></option>').attr('value', -1).text("--New Campaign--"));
					campaignDromdown.select2({
						theme: "bootstrap",
						width: 'auto'
					});
	  			}
	  		});
  		}
  	});

  	// $('select').on('select2:select', function(){
  	// 	if($(this).val() == -1){
  	// 		$('#'+$(this).data('modal-id')).modal('show');
  	// 	}
  	// });

  	$('#import-contact').on('submit', function(event){
  		event.preventDefault();
  		var self = this;
  		var progressBar = $("#progressbar-upload");
  		var guidStr = guid();
  		var formData	 = new FormData();
  		var processType = $('input[name="option-to"]:checked').val();
  		var campaignID = $('select[name="campaign-id"]').val();
  		var subcriberListID = $('select[name="subcriber-id"]').val();

  		var inpColumnNumber = $("#inpColumnNumber").val();
  		var inpColumnNames = $("#inpColumnNames").val().split('\n').join(',');
  		var inpContactStringCol = $("#inpContactStringCol").val();
  		var inpSeparator = $("#inpSeparator").val();
  		var inpSkipLine = $("#inpSkipLine").val();
  		var inpErrorsIgnore = $("#inpErrorsIgnore").val();

  		var inpSendToQueue = $('input[name="inpSendToQueue"]:checked').val();

	    formData.append('file', $('#csv-file').prop('files')[0] );
	    formData.append('inpUserId', $('#user-id').val());
	    formData.append('inpSubcriberListID', subcriberListID);
	    formData.append('inpCampaignID', campaignID);
	    formData.append('inptProcessType', processType);
	    formData.append('inpImportID', guidStr);

	    formData.append('inpColumnNumber', inpColumnNumber);
	    formData.append('inpColumnNames', inpColumnNames);
	    formData.append('inpContactStringCol', inpContactStringCol);
	    formData.append('inpSeparator', inpSeparator);
	    formData.append('inpSkipLine', inpSkipLine);
	    formData.append('inpErrorsIgnore', inpErrorsIgnore);

	    formData.append('inpSendToQueue', inpSendToQueue);

	    if($(this).validationEngine('validate')){
	    	if(processType == 1)
	    	{
	    		if(subcriberListID <= 0)
	    		{
	    			bootbox.dialog({
					    message: "Please select an subcriber list",
					    title: "Admin Import Tools - Import Error",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
					return false;
	    		}
	    	}	
	    	else if(processType == 2)
	    	{
	    		if(campaignID <= 0 && inpSendToQueue == 1)
	    		{
	    			bootbox.dialog({
					    message: "Please select a campaign",
					    title: "Admin Import Tools - Import Error",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
					return false;
	    		}
	    	}
	    	
	    	$('input[type="hidden"][name="guid"]').val(guidStr);

	    	$.ajax({
	  			url: '/session/sire/models/cfc/import-contact.cfc?method=SaveCSVContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	  			type: 'POST',
	  			dataType: 'JSON',
	  			data: formData,
	  			enctype: 'multipart/form-data',
	  			contentType: false,
	  			processData: false,
	  			xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							progressBar.css({width: percentComplete+'%'}).text(percentComplete+'%');

							if (percentComplete === 100) {
								progressBar.removeClass('active');
							}
						}
					}, false);

					return xhr;
				},
				beforeSend: function(){
					progressBar.addClass('active');
					progressBar.css({width: '0%'}).text('0%');
					$('button.btn-import').prop('disabled', true);
					$('#processingPayment').show();
					// var step = 0;
					// $('a.wz-step').removeClass('btn-success');
					// var intervalCheck = setInterval(function(){
					// 	$.ajax({
					// 		url: "/session/sire/models/cfc/import-contact.cfc?method=getProcessStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
					// 		type: "POST",
					// 		dataType: "json",
					// 		data: {inpImportID: guidStr},
					// 		success: function(data){
					// 			step = data.STATUS+1;
					// 			$('a.wz-step').each(function(index, item){
					// 				if($(item).data('step') <= step){
					// 					$(item).addClass('btn-success');
					// 				} else {
					// 					$(item).addClass('btn-default');
					// 				}
					// 			});

					// 			$('a.wz-step[data-step="'+step+'"]').removeClass('btn-default').addClass('btn-primary');
					// 			if(step == 4){
					// 				clearInterval(intervalCheck);
					// 			}
					// 		}
					// 	});
					
					// }, 1000);
				},
				complete: function(){
					$('button.btn-import').prop('disabled', false);
	  				$('#processingPayment').hide();
	  				GetReport();
				},
				success: function(data){

					if(data.RXRESULTCODE == 1){

						bootbox.dialog({
						    message: "Import CSV Success!",
						    title: "Import result",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});

						for(var prop in data){
							var label = $('strong[data-prop="'+prop.toUpperCase()+'"]');
							if(label.length > 0){
								label.text(data[prop]);
							}
						}
						resetImportForm();
						resetAdvanceForm();
					} else {

						var errMsg = data.ERRMESSAGE;
						if(errMsg == '') errMsg = 'Import Error!';
						
						bootbox.dialog({
						    message: errMsg,
						    title: "Import result",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
					}
				}
			});
	    }
  	
  	});


  	function resetImportForm (){
  		var form = $("#import-contact");

  		form.find('select.select2').each(function(index, item){
  			$(item).html("");
  		});
  		form.find('div.bootstrap-filestyle input[type="text"]').val("");
  		form.find('input.image-preview-filename').val("");
  		$('#csv-file').val("");
  		$("#preview-button").text('Choose file');
  	}





  	$('#import-contact, #frm-new-subcriber-list').validationEngine('attach', {showOneMessage: true, autoHideDelay: 10000, scroll: false,  promptPosition: "topLeft"});

	$('input').iCheck({
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});


	$('input').not("#rd-queue-now").not("#rd-queue-later").on("ifChecked", function(event){
		var control = $(this).data('control');
		$('div.form-group[data-control]').addClass('hidden').find('select.select2').each(function(item){
			$(this).find('option[value="0"]').prop('selected', true);
			$(this).trigger('change');
		});
		$('div.form-group[data-control="'+control+'"]').removeClass('hidden');

	});

	$('input#rd-queue-later').on('ifChecked', function(event) {
		event.preventDefault();
		$(".campaign-select").addClass('hidden');
	});

	$('input#rd-queue-now').on('ifChecked', function(event) {
		event.preventDefault();
		$(".campaign-select").removeClass('hidden');
	});


	$('#frm-new-subcriber-list').on('submit', function(event){

		event.preventDefault();

		var groupName = $('#frm-new-subcriber-list input[name="subcriber-list"]').val();
		var userid    = $('select[name="user-id"]').val();
		
		if($(this).validationEngine('validate')){

			$.ajax({
				url: '/session/sire/models/cfc/subscribers.cfc?method=addgroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'post',
				dataType: 'json',
				data: {INPGROUPDESC: groupName, INPUSERID: userid},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					
				},
				success: function(data) {
					$('#processingPayment').hide();
					if (data.DATA && data.DATA.RXRESULTCODE && data.DATA.RXRESULTCODE[0] == 1) {
						
						$('#new-subcriber-list').modal('hide');

						var newSubName = data.DATA.INPGROUPDESC[0], newSubID = data.DATA.INPGROUPID[0];

						var newOption = $("<option></option>").attr('value', newSubID).prop('selected', true).text(newSubName);


						$('select[name="subcriber-id"] option').each(function(){
							$(this).prop('selected', false);
						});


						//$('select[name="subcriber-id"] > option[value="-1"]').after(newOption);
						$('select[name="subcriber-id"]').append(newOption);

						$('select[name="subcriber-id"]').select2({
							theme: "bootstrap",
							width: 'auto'
						});

					} else {
						/*modalAlert('Add New Subscriber List', data.MESSAGE);*/
						bootbox.dialog({
						    message: data.DATA.MESSAGE[0],
						    title: "Subscriber",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
					}
				}
			});
		}
			
	});



	var refreshReportTable = GetReport();
	$('#show-contact').on('hide.bs.modal', function(){
		refreshReportTable();
	});


	
	function GetReport(strFilter){
		$('#report-file-upload').html('');
		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

		var statusCol = function(object){ 
			var status = "";
			// switch(object.STATUS){ 
			// 	case -2: status = "Deleted"; break;
			// 	case -1: status = "Pending"; break;
			// 	case  0: status = "Just Created"; break;
			// 	case  1: status = "Loading"; break;
			// 	case  2: status = "Loading"; break;
			// 	case  3: status = "Stopped"; break;
			// 	case  5: status = "Completed"; break;
			// 	default: status = 'Just Created';
			// } 
			if (object.STATUS in uploadStatus) {
				if ((uploadStatus[object.STATUS] == "Completed" || uploadStatus[object.STATUS] == "Waiting") && object.SENDTOQUEUE == "0") {
					status += "<b>Send Partial</b> " 
				}
				status += uploadStatus[object.STATUS];
			} else {
				status = 'Loading';
			}
			return status;
		};

		var listTypeCol = function(object){ 
			var type = "";
			switch(object.LISTTYPE){ 
				case SUBCRIBER_LIST: type = "Subcriber List"; break;
				case CAMPAIGN_BLAST: type = "Campaign Blast"; break;
			} 
			return type;
		};

		var isHyperlink = function(object){
			return (object.STATUS == PENDING) ? true : false;
		}

		var statsCol = function (object) {
			var strReturn = "";
			strReturn += "Valid: ";
			if(isHyperlink(object)){
				strReturn += (parseInt(object.UNIQUE) <= 0) ? object.UNIQUE : $("<a href=''></a>").text(object.UNIQUE).attr('data-id', object.ID)
												.attr('data-type', 'valid').addClass('editable')[0].outerHTML;
				strReturn += "<br/>Duplicate: ";
				strReturn += (parseInt(object.DUPLICATE) <= 0) ? object.DUPLICATE : $("<a href=''></a>").text(object.DUPLICATE).attr('data-id', object.ID)
												.attr('data-type', 'duplicate').addClass('editable')[0].outerHTML;
				strReturn += "<br/>Invalid: ";
				strReturn += (parseInt(object.INVALID) <= 0) ? object.INVALID : $("<a href=''></a>").text(object.INVALID).attr('data-id', object.ID)
												.attr('data-type', 'invalid').addClass('editable')[0].outerHTML;
			} else {
				strReturn += object.UNIQUE;
				strReturn += "<br/>Duplicate: ";
				strReturn += object.DUPLICATE;
				strReturn += "<br/>Invalid: ";
				strReturn += object.INVALID;
			}
			strReturn += "<br/>Deleted: ";
			if(parseInt(object.DELETED) > 0 && uploadStatus[object.STATUS] != "Deleted"){
				strReturn += $("<a href=''></a>").text(object.DELETED).attr('data-id', object.ID)
												.attr('data-type', 'deleted').addClass('editable')[0].outerHTML;
			} else {
				strReturn += object.DELETED;
			}

			return strReturn;
		}

		var totalCol = function(object){ 
			var strReturn = object.LOADED;
			if(isHyperlink(object)){
				strReturn = (parseInt(object.LOADED) <= 0) ? object.LOADED : $("<a href=''></a>").text(object.LOADED).attr('data-id', object.ID)
												.attr('data-type', 'loaded').addClass('editable')[0].outerHTML;
			}
			return strReturn;
		};

		var actionCol = function(object){ 
			var strReturn = "";

			if (uploadStatus[object.STATUS] != "Deleted") {
				if (uploadStatus[object.STATUS] == "Pending") {
				}
				if (uploadStatus[object.STATUS] == "Waiting") {
					strReturn = $("<a class='action-import action btn-send-now' title='Send partial' data-type='"+object.LISTTYPE+"' data-action='send' href='/session/sire/pages/admin-import-contact-detail?uploadid="+object.ID+"'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/send.png">').attr('data-id', object.ID)[0].outerHTML;
				}
				if (uploadStatus[object.STATUS] == "Processing") {
					strReturn = $("<a class='action-import action btn-stop' title='Pause' data-type='"+object.LISTTYPE+"' data-action='stop' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/stop.png">').attr('data-id', object.ID)[0].outerHTML;
				}
				if (uploadStatus[object.STATUS] == "Paused") {
					strReturn = $("<a class='action-import action btn-resume' title='Resume' data-type='"+object.LISTTYPE+"' data-action='resume' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/resume.png">').attr('data-id', object.ID)[0].outerHTML;
				}
				if (object.SENDTOQUEUE == "0" && (uploadStatus[object.STATUS] == "Completed" || uploadStatus[object.STATUS] == "Failed")) {
					strReturn = $("<a class='action-import action btn-send-now' title='View send partial history' data-type='"+object.LISTTYPE+"' data-action='send' href='/session/sire/pages/admin-import-contact-detail?uploadid="+object.ID+"'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/send.png">').attr('data-id', object.ID)[0].outerHTML;
				}
				strReturn+= $("<a class='action-import action btn-report' title='Report' data-action='report'></a>").html('<img src="/session/sire/assets/layouts/layout4/img/chart.png">').attr('data-id', object.ID)[0].outerHTML;

				if (uploadStatus[object.STATUS] != "Completed" && uploadStatus[object.STATUS] != "Failed") {
					strReturn+= $("<a class='action-import action btn-delete' title='Delete' data-type='"+object.LISTTYPE+"' data-action='delete' href=''></a>").html('<img src="/session/sire/assets/layouts/layout4/img/delete.png">').attr('data-id', object.ID)[0].outerHTML;
				}
			}
			return strReturn;
		};

		var updatedBy = function(object){
			var strReturn = "";
			if(object.UPDATEDBY != "" && object.UPDATEDAT != "" && uploadStatus[object.STATUS] != "Deleted"){
				strReturn+= "<p><strong>"+object.UPDATEDBY+"</strong></p>";
				strReturn+= "<p class='lbl'><a class='show-history' data-file-name='"+object.FILENAME+"' data-id='"+object.ID+"' href=''>"+object.UPDATEDAT+"</a></p>"
			} else {
				strReturn += object.IMPORTEDBY;
			}
			return strReturn;
		}

		var infoCol = function (object) {
			var strReturn = "";
			var type = "";
			switch(object.LISTTYPE){ 
				case SUBCRIBER_LIST: type = "Subcriber List"; break;
				case CAMPAIGN_BLAST: type = "Campaign Blast"; break;
			}
			strReturn += "<b>User Name:</b> "+object.USERNAME+"<br/>";
			strReturn += "<b>File Name:</b> "+object.FILENAME+"<br/>";
			strReturn += "<b>File Date:</b> "+object.CREATEDAT+"<br/>";
			strReturn += "<b>List Type:</b> "+type;
			return strReturn;
		}

		var table = $('#report-file-upload').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": "ID", "sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"id", "sWidth": "50px"},
				// {"mData": "USERNAME", "sName": 'username', "sTitle": 'User Name',"bSortable": false,"sClass":"username"},
				// {"mData": "FILENAME", "sName": 'fileName', "sTitle": 'File Name',"bSortable": false,"sClass":"file-name"},
				// {"mData": "CREATEDAT", "sName": 'createdAt', "sTitle": 'File Date',"bSortable": false,"sClass":"file-name"},
				// {"mData": listTypeCol, "sName": 'listtype', "sTitle": 'List Type',"bSortable": false,"sClass":"list-type"},
				{"mData": infoCol, "sName": 'listtype', "sTitle": 'File Info',"bSortable": false,"sClass":"file-info col-xl"},
				{"mData": "LOADED", "sName": 'loaded', "sTitle": "Org Rec Cnt","bSortable": false,"sClass":"loaded col-md"},
				{"mData": statsCol, "sName": 'valid', "sTitle": "Stats","bSortable": false,"sClass":"stats-col col-lg"},
				{"mData": statusCol,"sName": 'status', "sTitle": "Status","bSortable": false,"sClass":"status col-md"},
				// {"mData": "IMPORTEDBY", "sName": 'importedby', "sTitle": 'Imported By',"bSortable": false,"sClass":"imported-by col-md"},
				{"mData": updatedBy, "sName": 'updatedby', "sTitle": 'Updated by',"bSortable": false,"sClass":"updated-by", "sWidth":"150px"},
				{"mData": actionCol, "sName": 'action', "sTitle": 'Actions',"bSortable": false,"sClass":"action","sWidth":"120px"},
			],
			"sAjaxDataProp": "datalist",
			"sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=GetReport&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "async": false,
			        "success": function(data){

			        	var summary = data.summary;
			        	for(var item in summary){
			        		$('a[data-label="'+item+'"] span.label').text(summary[item]);
			        	}

			        	fnCallback(data);

			        	$(".stats-col").width(100);
			        }
		      	});
	        },
			"fnInitComplete":function(oSettings){
				if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
			}
		});

		return function(){
			table.fnStandingRedraw();
		}
	}



		var inpStartDate = $('input[type="hidden"][name="date-start"]');
		var inpEndDate   = $('input[type="hidden"][name="date-end"]');
		var inpFileDateStart   = $('input[type="hidden"][name="file-date-start"]');
		var inpFileDateEnd   = $('input[type="hidden"][name="file-date-end"]');


		var DatePicker = $('input[data-type="daterange"]').daterangepicker({
			opens: "right",
			drops: "down",
			timeZone: 'UTC',
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			startDate: inpStartDate.data('value'),
		    endDate: inpStartDate.data('value'),
		}, function(start, end, label){
			inpStartDate.val(start._d.getSireDate());
			inpEndDate.val(end._d.getSireDate());

		}).on('apply.daterangepicker', function(event, DRPObject){
			var range = DRPObject; 
		});

		var DatePickerFh = $('input[data-type="date-single"]').daterangepicker({
		    opens: "right",
			drops: "down",
			timeZone: 'UTC',
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			startDate: inpFileDateStart.data('value'),
		    endDate: inpFileDateEnd.data('value')
		}, function(start, end, label){
			inpFileDateStart.val(start._d.getSireDate());
			inpFileDateEnd.val(end._d.getSireDate());
		});




		Date.prototype.getSireDate = function(){
			var month = this.getMonth() + 1;
				month = (month < 10 ? '0'+month : month);

			var date = this.getDate();	
				date = (date < 10 ? '0'+date : date);
			var year = this.getFullYear();

			return year+'-'+month+'-'+date;
		}

		$('#report-filter').on('submit', function(event){
			event.preventDefault();

			var userID 	  = $('select[name="select-user-id-filter"]').val() ? $('select[name="select-user-id-filter"]').val(): 0 ,
			    startDate = inpStartDate.val(),
			    endDate   = inpEndDate.val();


			var customFilter = {
				userID: userID,
				startDate: startDate,
				endDate: endDate
			};

			GetReport(customFilter);

		});


		$('table').on('click', 'button.btn-show-file', function(event){
			var fileName = $(this).data('file-name');

			var modal = $('#show-file');


			modal.find('p.show-file').text(fileName);


			modal.modal('show');
		});

	$('#csv-file').change(function(event) {
		var fileType = $(this)[0].files[0].type;
		if (fileType != "application/vnd.ms-excel" && fileType != "text/csv" && fileType != "application/csv") {
			$(this).val('');
			bootbox.alert('File format not supported! Accept CSV file only!');
		}
		$("#preview-button").text("Change");
	});


	$("table").on('click', 'a.editable', function(event){
		event.preventDefault();
		var id = $(this).data("id"), type = $(this).data('type'), modal = $("#show-contact");
		modal.modal('show');



		modal.find('h4.modal-title').text(type+" Records");
		modal.find('input[name="id"]').val(id);

		switch(type){ 
			case "invalid": type = CT_INVALID; break;
			case "duplicate": type = CT_DUPLICATE; break;
			case "valid": type = CT_VALID; break;
			case "deleted": type = CT_DELETED; break;
			default: type = 0;
		} 

		if(type != 10){
			modal.find('button.visible').addClass('hidden');
		} else {
			modal.find('button.visible').removeClass('hidden');
		}

		modal.find('input[name="type"]').val(type);
		refreshContactList = GetContact(id, type);

		if (type == -2) {
			modal.find('button[name="deleteSelectedItem"]').addClass('hidden');
		} else {
			modal.find('button[name="deleteSelectedItem"]').removeClass('hidden');
		}
	});


	$("table").not("#send-contact-table").on('click', 'input.select-all', function(event){
		if($(this).prop('checked') ==  true){
			$(this).parents('table').find('input.select-item').prop('checked', true);
		} else {
			$(this).parents('table').find('input.select-item').prop('checked', false);
		}
	});

	$("table#show-contact-ds").on('click', 'button.action', function(event){
		event.preventDefault();
		var deleteBtn = $(this);
		var action = $(this).data('action'), id = $(this).data('id'), contact = $(this).data('contact'), timezone = $(this).data('timezone');
		var thisModal = $('#show-contact');
		var inpFileRecordID = thisModal.find('input[name="id"]').val();
		var type = thisModal.find('input[name="type"]').val();

		if(action == "delete"){
			deleteBtn.attr('disabled', true);
			bootbox.dialog({
				message: "Are you sure you want to delete this number? ",
			    title: "Delete Confirmation",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	deleteContacts([id], inpFileRecordID, function(){
			            		refreshContactList();
			            	}, type, 0);
			            	deleteBtn.attr('disabled', false);
			            }
			        },
			        cancel: {
			        	label: "Cancel",
			            className: "btn btn-medium btn-back-custom",
			            callback: function () {
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function (){
			    	deleteBtn.attr('disabled', false);
			    }
			});
		}
		if(action == "edit"){
			var updateModal = $('#update-contact');
			updateModal.on('hide.bs.modal', function(){
				updateModal.find('div.alert').addClass('hidden');
			});
			updateModal.find('input[name="contact"]').val(contact);
			updateModal.find('input[name="fileID"]').val(inpFileRecordID);
			updateModal.find('input[name="id"]').val(id);
			updateModal.find('input[name="type"]').val(type);
			updateModal.find('select[name="contact-tz"] option').each(function(){
				if(this.value == timezone){
					$(this).prop('selected', true);
				}
			});
			updateModal.modal('show');
		}
	});


	$('#update-contact').on('click', 'button[name="save"]', function(event){

		thisModal = $('#update-contact');
		var inpContact = thisModal.find('input[name="contact"]').val(),
			inpFileRecordID	= thisModal.find('input[name="fileID"]').val(),
			inpItemID = thisModal.find('input[name="id"]').val(),
			inpTimezone = thisModal.find('select[name="contact-tz"]').val(),
			type =  thisModal.find('input[name="type"]').val();

		$.ajax({
			url: "/session/sire/models/cfc/import-contact.cfc?method=updateContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: "POST",
			dataType: "json",
			data: {inpFileRecordID: inpFileRecordID,inpItemID: inpItemID,inpContact: inpContact, inpTimezone: inpTimezone, inpType: type},
			success: function(data){
				if(data.RXRESULTCODE == 1){
					thisModal.modal('hide').find('div.alert').addClass('hidden');
					refreshContactList();
				} else {
					thisModal.find('div.alert').html(data.MESSAGE).removeClass('hidden');
				}
			}
		});
	});

	$("table#report-file-upload").on('click', 'a.show-history', function(event){
		event.preventDefault();
		var id = $(this).data('id'),fileName = $(this).data('file-name'),
		modal = $('#show-file-history');
		modal.find('input[name="id"]').val(id);

		getHistory(id);
		modal.modal('show');


		var defaultData = [{id: id, text: fileName}];
		$('select[name="file-name"]').html("");
	  // 	$('select[name="file-name"]').select2({
	  // 		placeholder: "Search File Name ",
	  // 		allowClear: true,
	  // 		ajax: {
			//     url: "/session/sire/models/cfc/import-contact-history.cfc?method=searchFileName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
			//     dataType: 'json',
			//     delay: 250,
			//     data: function (params) {
			//       	return {
			// 	        inpFileName: params.term, // search term
			// 	        page: params.page
			//       	};
			//     },
			//     processResults: function (data, params) {
			//     	return {
			// 			results: $.map(data, function (item) {
		 //                    return {
		 //                        text: item.ID+" - "+item.NAME,
		 //                        id: item.ID
		 //                    }
		 //                })
			//       	};
			//     },
			//     cache: true
		 //  	},
			// minimumInputLength: 1,
			// theme: "bootstrap",
			// width: 'auto',
			// data: defaultData
	  // 	});
	});

	function GetContact(id, type){
		$('#show-contact-ds').html();
		var tzList = JSON.parse($('input[name="tzlist"]').val());
		var showCheckItem = function(object){
			return (object.STATUS != -2) ? "<input class='select-item' type='checkbox' value='"+object.ID+"'>" : "";
		};

		var actionCol = function(object){
			var strReturn = "";
			if (object.STATUS != -2) {
				strReturn = $("<button class='action btn btn-success' title='edit' data-action='edit' href=''></button>").html('<i class="glyphicon glyphicon-edit"></i>').attr('data-timezone', object.TIMEZONE).attr('data-contact', object.CONTACT).attr('data-id', object.ID)[0].outerHTML;
				strReturn+= $("<button class='action btn btn-danger' title='delete' data-action='delete' href=''></button>").html('<i class="glyphicon glyphicon-trash"></i>').attr('data-id', object.ID)[0].outerHTML;								
			}
			return strReturn;
		}

		var tzCol = function(object){
			var strReturn = "";
			tzList.forEach(function(item){
				if(object.TIMEZONE === item.VALUE){
					strReturn = item.NAME;
				}
			});
			return strReturn;
		}

		var table = $('#show-contact-ds').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": showCheckItem, "sName": 'select', "sTitle": "","bSortable": false,"sClass":"select td-select"},
				{"mData": "CONTACT", "sName": 'contact', "sTitle": 'Contact',"bSortable": false,"sClass":"contact", "sWidth": "180px"},
				{"mData": tzCol, "sName": 'timezon', "sTitle": 'TimeZone',"bSortable": false,"sClass":"timezone"},
				{"mData": actionCol, "sName": 'action', "sTitle": 'Actions',"bSortable": false,"sClass":"action td-action", "sWidth":"130px"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=getContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
		    },
		    "fnHeaderCallback": function( thead, data, start, end, display, object) {
		    	if (data.length > 0) {
			    	if (!(uploadStatus[data[0].STATUS] == "Deleted")) {
			    		$(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
			    	}
		    	} else {
		    		$(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'>");
		    	}
			},
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				aoData.push(
		            { "name": "inpFileRecordID", "value": id}
	            );
	             aoData.push(
		            { "name": "inpType", "value": type}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function (data) {
			        	fnCallback(data);
			        	if (type == "-2") {
			        		$('#show-contact-ds').find('.td-action, .td-select').addClass('hidden');
			        	} else {
			        		$('#show-contact-ds').find('.td-action, .td-select').removeClass('hidden');
			        	}
			        }
		      	});
	        },
	        "fnInitComplete":function(oSettings){
	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
			}
		});

		return function(){
			table.fnStandingRedraw();
		}

	}




	function getHistory(id, date){
		$('#show-file-history-ds').html('');
		var listTypeCol = function(object){ 
			var type = "";
			switch(object.LISTTYPE){ 
				case 1: type = "Subcriber List"; break;
				case 2: type = "Campaign Blast"; break;
			} 
			return type;
		};

		var updatedCol = function(object){
			var strReturn = "";
				
				strReturn+= "<p class='lbl'>"+object.CREATED+"</p>"
				strReturn+= "<p>By: <strong>"+object.USERNAME+"</strong></p>";
			return strReturn;
		}

		var table = $('#show-file-history-ds').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": true,
		    "aoColumns": [
				{"mData": listTypeCol, "sName": 'listtype', "sTitle": 'List Type',"bSortable": false,"sClass":"listtype"},
				{"mData": "FILENAME", "sName": 'filename', "sTitle": 'File Name',"bSortable": false,"sClass":"file-name", "sWidth": "320px"},
				{"mData": "FIELD", "sName": 'fieldname', "sTitle": "Field Name","bSortable": false,"sClass":"fieldname"},
				{"mData": "OLDVALUE", "sName": 'oldvalue', "sTitle": 'Old Value',"bSortable": false,"sClass":"oldvalue"},
				{"mData": "NEWVALUE", "sName": 'newvalue', "sTitle": 'New Value',"bSortable": false,"sClass":"newvalue"},
				{"mData": updatedCol, "sName": 'updatedat', "sTitle": 'Updated',"bSortable": false,"sClass":"updatedat"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/import-contact-history.cfc?method=getUpdateHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
		    },
		    "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
		    	if(typeof(id) != "undefined" && id > 0){
		    		aoData.push(
			            { "name": "inpFileRecordID", "value": id}
		            );
		    	}

		    	if(typeof(date) != "undefined"){
		    		aoData.push(
			            { "name": "inpDateStart", "value": date.start}
		            );
		            aoData.push(
			            { "name": "inpDateEnd", "value": date.end}
		            );
		    	}
				
		        oSettings.jqXHR = $.ajax({
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function (data) {
			        	fnCallback(data);
			        	$("#show-file-history-ds").css('width', '100%');
			        }
		      	});
	        },
	        "fnInitComplete": function(oSettings){
	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
	        }
		});
	}



	$('body').on('click', 'button[name="deleteSelectedItem"]', function(event){
		event.preventDefault();
		var deleteBtn = $(this);
		deleteBtn.attr('disabled', true);
		var thisModal  = $('#show-contact');
		var deleteAll = $(this).data("delete-all");

		var selectedItems = thisModal.find('table input.select-item:checked');
		var selectedIDs = new Array();
		selectedItems.each(function(index){
			selectedIDs.push($(this).val());
		});
		var type = thisModal.find('input[name="type"]').val();

		if(selectedIDs.length || deleteAll){
			if (!deleteAll) {
				var message = "Are you sure you want to delete selected number(s) from your list? ";
			} else {
				var message = "Are you sure you want to delete all contact from this list? ";
			}
			bootbox.dialog({
				message: message,
			    title: "Delete confirmation",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	var id = thisModal.find('input[name="id"]').val();
			            	var type =  thisModal.find('input[name="type"]').val();

			            	deleteContacts(selectedIDs, id, function(){
			            		refreshContactList();
			            	}, type, deleteAll);
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function () {
			    	deleteBtn.attr('disabled', false);
			    }
			});
		} else {
			bootbox.dialog({
				message: "No have number(s) selected!",
			    title: "Alert",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	deleteBtn.attr('disabled', false);
			            }
			        }
			    },
			    onEscape: function () {
			    	deleteBtn.attr('disabled', false);
			    }
			});
		}
		
	});

	var deleteContacts = function(selectedIDs, inpFileRecordID, callback, type, deleteAll){
		$.ajax({
    		url: "/session/sire/models/cfc/import-contact.cfc?method=deleteContacts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		type: "POST",
    		dataType: "json",
    		data: {inpItemIDs: JSON.stringify(selectedIDs), inpFileRecordID: inpFileRecordID, inpType: type, inpDeleteAll: deleteAll},
    		success: function(data){
    			if(data.RXRESULTCODE == 1){
    				bootbox.dialog({
						message: "Record(s) has been successfully deleted.",
					    title: "Alert",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {

					            }
					        }
					    }
					});
    			}
    			callback();
    		}
    	});
	}

	/*Advance options section************/

	$('body').on('click', '#btn-advance', function(event) {
		event.preventDefault();
		$('body').find("#section-advance").toggle();
	});

	$('body').on('change', '#inpColumnNumber', function(event) {
		event.preventDefault();
		var colNumb = $(this).val();
		if (isNaN(colNumb)) {
			bootbox.alert('Please enter a valid number.');
		} else if (parseInt(colNumb) > 20) {
			bootbox.alert('Maximum column number is 20');
		} else {
			var inpContactStringColSelect = $('#inpContactStringCol');
			inpContactStringColSelect.find('option').remove();
			for (var i = 1; i <= parseInt(colNumb); i++) {
				inpContactStringColSelect.append('<option value="'+i+'">'+i+'</option>');
			}

			var inpColumnNamesInput = $('#inpColumnNames');
			var columnNames = 'ContactString_vch';
			for (var i = 2; i <= parseInt(colNumb); i++) {
				columnNames += '\n' + 'COL' + i + '_vch';
			}
			inpColumnNamesInput.val(columnNames);
		}
	});

	$('body').on('change', '#inpContactStringCol', function(event) {
		event.preventDefault();
		var col = $(this).val();
		var colNumb = $('#inpColumnNumber').val();

		var inpColumnNamesInput = $('#inpColumnNames');
		var columnNames = '';
		for (var i = 1; i <= parseInt(colNumb); i++) {
			columnNames += (i == 1 ? '' : '\n');
			if (parseInt(col) == i) {
				columnNames += 'ContactString_vch'
			} else {
				columnNames += 'COL' + i + '_vch';
			}
		}
		inpColumnNamesInput.val(columnNames);
	});

	var resetAdvanceForm = function () {
		$("#section-advance").hide();
		$("#inpColumnNumber").val("1");
		$("#inpColumnNumber").trigger('change');
		$("#inpSeparator").val(",");
		$("#inpSkipLine").val("0");
		$("#inpErrorsIgnore").val("0");
	}
	/************End advance options section*/

	/*Action section************/

	$('body').on('click', '.btn-stop', function(event) {
		event.preventDefault();
		var data = $(this).data();
		var message = '';
		if (data.type == 2) {
			message = "Are you sure you want to stop this import? <strong>This action will stop all of your queued contact that was imported before on this campaign</strong>";
		} else {
			message = "Are you sure you want to stop this import?";
		}
		bootbox.dialog({
			message: message,
			title: "Stop Import",
			buttons: {
				success: {
					className: "btn btn-success-custom btn-medium",
					label: "Ok",
					callback: function () {
						stopImport(data.id, data.type);
					}
				}
			}
		});
	});

	var stopImport = function (id, type) {
		$.ajax({
			url: "/session/sire/models/cfc/import-contact.cfc?method=StopImportById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: 'POST',
			data: {inpPKId: id, inpType: type},
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(rx) {
			rx = jQuery.parseJSON(rx);
			var message = (rx.MESSAGE != "") ? rx.MESSAGE : "Import Stopped";
			bootbox.dialog({
				message: message,
				title: "Stop Import",
				buttons: {
					success: {
						className: "btn btn-success-custom btn-medium",
						label: "Ok",
						callback: function () {

						}
					}
				}
			});
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			$('#processingPayment').hide();
			GetReport();
		});
	}

	$('body').on('click', '.btn-resume', function(event) {
		event.preventDefault();
		var data = $(this).data();
		bootbox.dialog({
			message: "Are you sure you want to resume this import?",
			title: "Resume Import",
			buttons: {
				success: {
					className: "btn btn-success-custom btn-medium",
					label: "Ok",
					callback: function () {
						resumeImport(data.id);
					}
				}
			}
		});
	});

	var resumeImport = function (id) {
		$.ajax({
			url: "/session/sire/models/cfc/import-contact.cfc?method=ResumeImportById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: 'POST',
			data: {inpPKId: id},
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(rx) {
			rx = jQuery.parseJSON(rx);
			var message = (rx.MESSAGE != "") ? rx.MESSAGE : "Import Resumed";
			bootbox.dialog({
				message: message,
				title: "Resume Import",
				buttons: {
					success: {
						className: "btn btn-success-custom btn-medium",
						label: "Ok",
						callback: function () {

						}
					}
				}
			});
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			$('#processingPayment').hide();
			GetReport();
		});
	}

	$('body').on('click', '.btn-delete', function(event) {
		event.preventDefault();
		var data = $(this).data();
		bootbox.dialog({
			message: "Are you sure you want to delete this import? This action will stop all current activity of this import",
			title: "Delete Import",
			buttons: {
				success: {
					className: "btn btn-success-custom btn-medium",
					label: "Ok",
					callback: function () {
						deleteImport(data.id);
					}
				}
			}
		});
	});

	var deleteImport = function (id) {
		$.ajax({
			url: "/session/sire/models/cfc/import-contact.cfc?method=DeleteImportById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: 'POST',
			data: {inpPKId: id},
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(rx) {
			rx = jQuery.parseJSON(rx);
			var message = (rx.MESSAGE != "") ? rx.MESSAGE : "Import Deleted";
			bootbox.dialog({
				message: message,
				title: "Delete Import",
				buttons: {
					success: {
						className: "btn btn-success-custom btn-medium",
						label: "Ok",
						callback: function () {

						}
					}
				}
			});
		})
		.fail(function() {
			//console.log("error");
		})
		.always(function() {
			$('#processingPayment').hide();
			GetReport();
		});
	}

	$('body').on('click', '.btn-send-now', function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		// modal = $('#send-now-modal');
		// modal.find('input[name="id"]').val(id);

		// $("#inpContactNumberToSend").val('');

		// GetContactToSend(id);
		// modal.modal('show');
		location.href = "/session/sire/pages/admin-import-contact-detail?uploadid=" + id;
	});

	function GetContactToSend(id){
		$('#send-contact-table').html('');
		var showCheckItem = function(object){
			return "<input class='select-item' type='checkbox' value='"+object.ID+"'>";
		};

		var table = $('#send-contact-table').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": "CONTACT", "sName": 'contact', "sTitle": 'Contact',"bSortable": false,"sClass":"contact"}
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=GetContactToSend&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
				$("#send-contact-table input.select-item").each(function(index, el) {
					if (selectedContact.indexOf($(el).val()) >= 0) {
						$(el).prop('checked', true);
					}
				});
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				aoData.push(
		            { "name": "inpFileRecordID", "value": id}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": function (data) {
			        	$("#totalSent").text(data.TOTALSENT);
			        	$("#totalRemain").text(data.TOTALREMAIN);

			        	fnCallback(data);
			        }
		      	});
	        },
	        "fnInitComplete":function(oSettings){
	        	if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
			}
		});
	}

	$('body').on('click', '#btn-send-confirm', function(event) {
		event.preventDefault();
		var modal = $('#send-now-modal');
		var id = modal.find('input[name="id"]').val();
		var number = $("#inpContactNumberToSend").val();
		if (number == "" || isNaN(number) || parseInt(number) <= 0) {
			bootbox.dialog({
				message: "Input is not a valid number!",
				title: "Oops!",
				buttons: {
					close: {
						className: "btn btn-primary btn-medium",
						label: "Close",
						callback: function () {}
					}
				}

			})
		} else {
			SendToQueue(id, 0, number);
		}
	});

	$('body').on('click', '#btn-send-all-confirm', function(event) {
		event.preventDefault();
		var modal = $('#send-now-modal');
		var id = modal.find('input[name="id"]').val();
		SendToQueue(id, 1, 0);
	});

	var SendToQueue = function (id, sendAll, number) {
		if (typeof number == "undefined" || number == "" || isNaN(number) || parseInt(number) <= 0) {
			bootbox.dialog({
				message: "Input is not a valid number!",
				title: "Oops!",
				buttons: {
					close: {
						className: "btn btn-primary btn-medium",
						label: "Close",
						callback: function () {}
					}
				}

			})
		} else {
			$.ajax({
				url: '/session/sire/models/cfc/import-contact.cfc?method=SendToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpPKId: id, inpNumberToSend: number, inpSendAll: sendAll},
				beforeSend: function () {
					$('#processingPayment').show();
				}
			})
			.done(function(data) {
				data = jQuery.parseJSON(data);
				var message = (data.MESSAGE != "") ? data.MESSAGE : "Contacts sent!";
				bootbox.dialog({
					message: message,
					title: "Send to queue",
					buttons: {
						success: {
							className: "btn btn-success-custom btn-medium",
							label: "Ok",
							callback: function () {

							}
						}
					}
				});
			})
			.fail(function() {
				//console.log("error");
			})
			.always(function() {
				$('#processingPayment').hide();
				$('#send-now-modal').modal('hide');
				GetReport();
				selectedContact = [];
			});
		}
	}

	/*************End action section*/
	
	$("form[name='file-update-history'] button[name='view']").on('click', function(event){
		var thisModal = $('#show-file-history');
		var thisForm = $("form[name='file-update-history']");
		var fileID = thisModal.find('input[name="id"]').val();
		var fileDateStart = thisForm.find('input[type="hidden"][name="file-date-start"]').val(),
		    fileDateEnd = thisForm.find('input[type="hidden"][name="file-date-end"]').val();
		    var date = {start: fileDateStart, end: fileDateEnd};
		getHistory(fileID, date);
	});

	$("form[name='file-update-history'] button[name='clear']").on('click', function(event){

		var thisModal = $('#show-file-history');
		var fileID = thisModal.find('input[name="id"]').val();
		thisModal.find('input[name="file-date"]').val("");
		getHistory(fileID);
	});



	$('#report-filter button[name="clear-filter"]').click(function(e){
		GetReport();
	});


	$(document).on("click",".btn-report",function() {
        $("#show-report").modal('show');
        var uploadId = $(this).data('id');
        var id = uploadId;
        var type = 0;

        var dialogTitle = "Report";


        if(uploadId > 0)
        {

        	$('#reportContactQueue').html(0);
			$('#reportIreResult').html(0);
			$('#reportContactResult').html(0);	
			$('#reportLoadSuccess').html(0);
			$('#reportLoadError').html(0);
			$('#reportBlocked').html(0);
			$('#reportDuplicatedInQueue').html(0);

			var tzList = JSON.parse($('input[name="tzlist"]').val());

			var tzCol = function(object){
				var strReturn = "";
				tzList.forEach(function(item){
					if(object.TIMEZONE === item.VALUE){
						strReturn = item.NAME;
					}
				});
				return strReturn;
			}

        	try{
				var table = $('#show-report-ds').dataTable({
					"bStateSave": false,
					"iStateDuration": -1,
					"bProcessing": true,
					"bFilter": false,
					"bServerSide":true,
					"bDestroy":true,
					"sPaginationType": "input",
				    "bLengthChange": false,
					"iDisplayLength": 10,
					"bAutoWidth": false,
				    "aoColumns": [
				    	{"mData": "ID", "sName": 'ID', "sTitle": "","bSortable": false,"sClass":"select"},
						{"mData": "CONTACT", "sName": 'Contact', "sTitle": 'Contact',"bSortable": true,"sClass":"contact","sWidth": "150px"},
						{"mData": tzCol, "sName": 'Timezone', "sTitle": 'TimeZone',"bSortable": true,"sClass":"timezone col-lg"},
						{"mData": "STATUS", "sName": "Status", "sTitle": 'Status',"bSortable": true,"sClass":"status"},
						{"mData": "NOTE", "sName": "Note", "sTitle": 'Note',"bSortable": false,"sClass":"note"}
					],
					"sAjaxDataProp": "DATALIST",
					"sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=getReportByUploadId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				   	"fnDrawCallback": function( oSettings ) {
				  		if (oSettings._iDisplayStart < 0){
							oSettings._iDisplayStart = 0;
							$('input.paginate_text').val("1");
						}
			        	if(oSettings._iRecordsTotal == 0){
							this.parent().find('div.dataTables_paginate').hide();
						} else {
							this.parent().find('div.dataTables_paginate').show();
						}
				    },
				    "fnHeaderCallback": function( thead, data, start, end, display ) {
						//$(thead).find('th.select').html("<input class='select-all' type='checkbox' value='0'> All");
					},
					"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
						aoData.push(
				            { "name": "inpUploadIdPK", "value": id}
			            );
			             aoData.push(
				            { "name": "inpType", "value": type}
			            );
				        oSettings.jqXHR = $.ajax( {
					        "dataType": 'json',
					        "type": "POST",
					        "url": sSource,
					        "data": aoData,
					        "success": function(data){
					        	if(data.RXRESULTCODE == 1){
					        		if(data.Processtype ==2){

					        			$('#reportContactQueue').html(data.totalContactQueue).removeClass('hidden');
					        			$('#reportContactQueue').attr('title',data.totalContactQueue);
					        			$('#reportContactQueueOverall').html(data.totalContactQueueOverall).removeClass('hidden');
					        			$('#reportContactQueueOverall').attr('title',data.totalContactQueueOverall);
					        			$('#reportIreResult').html(data.totalIreResult).removeClass('hidden');
					        			$('#reportIreResult').attr('title',data.totalIreResult);
					        			$('#reportContactResult').html(data.totalContactResults).removeClass('hidden');
					        			$('#reportContactResult').attr('title',data.totalContactResults);
					        			$('#reportBlocked').html(data.Blocked).removeClass('hidden');
					        			$('#reportBlocked').attr('title',data.Blocked);
										$('#reportDuplicatedInQueue').html(data.DuplicatedInQueue).removeClass('hidden');
										$('#reportDuplicatedInQueue').attr('title',data.DuplicatedInQueue);

										$('.for_batch_report').removeClass('hidden');
					        		}
					        		else
					        		{
					        			if(!$('.for_batch_report').hasClass('hidden'))
					        			$('.for_batch_report').addClass('hidden');
					        		}
					        		
					        		$('#reportLoadSuccess').html(data.loadSuccess);
					        		$('#reportLoadSuccess').attr('title',data.loadSuccess);
					        		$('#reportLoadError').html(data.loadError);
					        		$('#reportLoadError').attr('title',data.loadError);

					        		$("#header_file_date").text(data.createdAt);
					        		$("#header_file_date").attr('title',data.createdAt);
					        		$("#header_file_name").text(data.fileName);
					        		$("#header_file_name").attr('title',data.fileName);
					        		$("#header_campaign_name").text(data.CampaignName);
					        		$("#header_campaign_name").attr('title',data.CampaignName);
					        	}
				        		fnCallback(data);
				        	}

				      	});
			        },
			        "fnInitComplete":function(oSettings){
			        	if(oSettings._iRecordsTotal == 0){
							this.parent().find('div.dataTables_paginate').hide();
						} else {
							this.parent().find('div.dataTables_paginate').show();
						}
					}
				});
				
			
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
						        message: "Get Report Fail",
						        title: dialogTitle,
						        buttons: {
						            success: {
						                label: "Ok",
						                className: "btn btn-medium btn-success-custom",
						                callback: function() {
						                }
						            }
						        }
				});
			}
        }
        else
        {
        	bootbox.dialog({
		        message: "Invalid UploadPKID",
		        title: dialogTitle,
		        buttons: {
		            success: {
		                label: "Ok",
		                className: "btn btn-medium btn-success-custom",
		                callback: function() {
		                }
		            }
		        }
			});
        }
    });



	$('button.btn-print').on('click', function(event){
		var fileRecordID = $('#show-contact').find('input[name="id"]').val();
		window.location.href = "/session/sire/models/cfm/export-contact.cfm?file_id="+fileRecordID;
	});


	$("#btn-clear-filter").click(function(event) {
		$('select[name="select-user-id-filter"]').html("");

		var inpStartDate = $('input[type="hidden"][name="date-start"]');
		var inpEndDate   = $('input[type="hidden"][name="date-end"]');

		var DatePicker = $('input[data-type="daterange"]').daterangepicker({
			opens: "right",
			drops: "down",
			timeZone: 'UTC',
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			startDate: inpStartDate.data('value'),
		    endDate: inpStartDate.data('value'),
		}, function(start, end, label){
			inpStartDate.val(start._d.getSireDate());
			inpEndDate.val(end._d.getSireDate());

		}).on('apply.daterangepicker', function(event, DRPObject){
			var range = DRPObject; 
		});
	});

})(jQuery);