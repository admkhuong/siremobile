var _tblListEMS;
$(document).ready(function(){
	InitControl();

});


function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	//init datatable for active agent
	_tblListEMS = $('#tblListEMS').dataTable( {
	    "bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
	    "bLengthChange": false,
		"iDisplayLength": 10,
	    "aoColumns": [
			{"sName": 'Id', "sTitle": 'No', "sWidth": '4%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Account ID', "sWidth": '6%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Email', "sWidth": '13%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Plan', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Plan ID', "sWidth": '4%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Purchased Keyword', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Credit Card Number', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Due Date', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Payment Date', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Amount($)', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Status Fail', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Reason', "sWidth": '15%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Sent email', "sWidth": '8%',"bSortable": false},
			{"sName": 'Id', "sTitle": 'Status', "sWidth": '8%',"bSortable": false},
			{"sName": 'link', "sTitle": 'Billing', "sWidth": '8%',"bSortable": false},
			{"sName": 'link', "sTitle": 'Action', "sWidth": '8%',"bSortable": false}
			
			
		],
		"sAjaxDataProp": "ListEMSData",
		"sAjaxSource": '/public/sire/models/cfc/reportings.cfc?method=GetRecurringLog&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			$(nRow).children('td:first').addClass("avatar");

			if (aData && aData[8] == 'Not running') {
				$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
			}
			else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
				$(nRow).removeClass('odd').removeClass('even').addClass('paused');
			}

            return nRow;
       	},
	   "fnDrawCallback": function( oSettings ) {
	     
	    },
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
	       aoData.push(
	            { "name": "customFilter", "value": customFilterData}
            );
	        $.ajax({dataType: 'json',
	                 type: "POST",
	                 url: sSource,
		             data: aoData,
		             success: fnCallback
		 	});
        },
		"fnInitComplete":function(oSettings, json){
			$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
		}
    });
}

// delete EMS
function deleteEMS(TID){
	bootbox.dialog({
        message: 'Are you sure you want to delete this item?',
        title: 'Recurring Report',
        className: "",
        buttons: {
            success: {
                label: "OK",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if(result){
					  	try{
							$.ajax({
							type: "POST",
							url: '/public/sire/models/cfc/reportings.cfc?method=DeleteRecurringLogItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							dataType: 'json',
							data: { TID : TID},
							beforeSend: function( xhr ) {
								$('#processingPayment').show();
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								$('#processingPayment').hide();
								bootbox.dialog({
								    message: "Delete Fail",
								    title: "Recurring Report",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
								});
							},					  
							success:		
								function(d) {
									$('#processingPayment').hide();
									if(d.RXRESULTCODE > 0)
									{
										// reload data
										var oTable = $('#tblListEMS').dataTable();
									  	oTable.fnDraw();
										return;
									}
									else
									{
			
										if (d.DATA.ERRMESSAGE[0] != '') {
											 bootbox.alert("This item has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
										}
									}
								} 		
							});
						}catch(ex){
							$('#processingPayment').hide();
							bootbox.dialog({
									        message: "Delete Fail",
									        title: "Recurring Report",
									        buttons: {
									            success: {
									                label: "Ok",
									                className: "btn btn-medium btn-success-custom",
									                callback: function() {
									                }
									            }
									        }
							});
						}
					}
                }
            },
            cancel: {
                label: "Cancel",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });
	
	
}

function sendMail(TID){

	bootbox.dialog({
        message: 'Are you want to sendmail to client?',
        title: 'Recurring Report',
        className: "",
        buttons: {
            success: {
                label: "OK",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if(result){
					  	try{
							$.ajax({
							type: "POST",
							url: '/public/sire/models/cfc/reportings.cfc?method=SendMailRecurringLogItem&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							dataType: 'json',
							data: { TID : TID},
							beforeSend: function( xhr ) {
								$('#processingPayment').show();
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								$('#processingPayment').hide();
								bootbox.dialog({
								    message: "Delete Fail",
								    title: "Recurring Report",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
								});
							},					  
							success:		
								function(d) {
									$('#processingPayment').hide();
									if(d.RXRESULTCODE > 0)
									{
										// reload data
										var oTable = $('#tblListEMS').dataTable();
									  	oTable.fnDraw();
										return;
									}
									else
									{
			
										if (d.DATA.ERRMESSAGE[0] != '') {
											 bootbox.alert("Email don't send'.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
										}
									}
								} 		
							});
						}catch(ex){
							$('#processingPayment').hide();
							bootbox.dialog({
									        message: "Send Fail",
									        title: "Recurring Report",
									        buttons: {
									            success: {
									                label: "Ok",
									                className: "btn btn-medium btn-success-custom",
									                callback: function() {
									                }
									            }
									        }
							});
						}
					}
                }
            },
            cancel: {
                label: "Cancel",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });
	
}

function ReRunBilling(TID){
	bootbox.dialog({
        message: 'Are you want to Re-run Billing?',
        title: 'Recurring Report',
        className: "",
        buttons: {
            success: {
                label: "OK",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if(result){
					  	try{
							$.ajax({
							type: "POST",
							url: '/public/sire/models/cfc/reportings.cfc?method=ReRunBilling&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							dataType: 'json',
							data: { TID : TID},
							beforeSend: function( xhr ) {
								$('#processingPayment').show();
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								$('#processingPayment').hide();
								bootbox.dialog({
								    message: "Delete Fail",
								    title: "Recurring Report",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
								});
							},					  
							success:		
								function(d) {
									$('#processingPayment').hide();
									if(d.RXRESULTCODE > 0)
									{
										
										// reload data
										var oTable = $('#tblListEMS').dataTable();
									  	oTable.fnDraw();
										return;
									}
									else
									{
			
										if (d.ERRMESSAGE[0] != '') {
											 //bootbox.alert(d.MESSAGE + "\n" + d.ERRMESSAGE, function() { return; } );
											 bootbox.dialog({
											    message: d.MESSAGE + "\n" + d.ERRMESSAGE,
											    title: "Recurring Report",
											    buttons: {
											        success: {
											            label: "Ok",
											            className: "btn btn-medium btn-success-custom",
											            callback: function() {}
											        }
											    }
											});
										}
									}
								} 		
							});
						}catch(ex){
							$('#processingPayment').hide();
							bootbox.dialog({
									        message: "Re-run Billing Fail",
									        title: "Recurring Report",
									        buttons: {
									            success: {
									                label: "Ok",
									                className: "btn btn-medium btn-success-custom",
									                callback: function() {
									                }
									            }
									        }
							});
						}
					}
                }
            },
            cancel: {
                label: "Cancel",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });
	
}
function isNumber(n) 
{
  return !isNaN(parseFloat(n)) && isFinite(n);
}
