var _tblListEMS;

$(document).ready(function(){
	InitControl();

});


function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	//init datatable for active agent
	_tblListEMS = $('#tblListEMS').dataTable( {
	    "bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
	    "bLengthChange": false,
		"iDisplayLength": 10,
	    "aoColumns": [
			{"sName": 'ID', "sTitle": 'ID', "sWidth": '3%',"bSortable": false},
			{"sName": 'Message', "sTitle": 'Message', "sWidth": '25%',"bSortable": true},
			{"sName": 'Request', "sTitle": 'Request Url', "sWidth": '',"bSortable": true},
			{"sName": 'UserId', "sTitle": 'User Id', "sWidth": '8%',"bSortable": false},
			{"sName": 'UserIp', "sTitle": 'User Ip', "sWidth": '8%',"bSortable": false},
			{"sName": 'Created', "sTitle": 'Created', "sWidth": '8%',"bSortable": false},
			{"sName": 'Status', "sTitle": 'Status', "sWidth": '5%',"bSortable": false},
			{"sName": 'link', "sTitle": '', "sWidth": '5%',"bSortable": false}
		],
		"sAjaxDataProp": "ListEMSData",
		"sAjaxSource": '/public/sire/models/cfc/error_logs.cfc?method=GetErrorList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			$(nRow).children('td:first').addClass("avatar");

			if (aData && aData[8] == 'Not running') {
				$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
			}
			else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
				$(nRow).removeClass('odd').removeClass('even').addClass('paused');
			}

            return nRow;
       	},
	   "fnDrawCallback": function( oSettings ) {
	      
	    },
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
	       aoData.push(
	            { "name": "customFilter", "value": customFilterData}
            );
	        $.ajax({dataType: 'json',
	                 type: "POST",
	                 url: sSource,
		             data: aoData,
		             success: fnCallback
		 	});
        },
		"fnInitComplete":function(oSettings, json){
			$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
		}
    });
	
}
function isNumber(n) 
{
  return !isNaN(parseFloat(n)) && isFinite(n);
}