(function($){

	$('.select2').select2({
		theme: "bootstrap",
		width: 'auto'
	});

	var filter_field_option = "";
	for (var i = 0; i < CDFS.length; i++) {
		filter_field_option += '<option value="'+CDFS[i]+'">'+CDFS[i]+'</option>';
	}
	var filter_row_html111 = '<div class="col-md-12 col-sm-12 col-lg-12 filter-row">'+
						'<button class="btn btn-sq btn-back-custom pull-left btn-remove-filter"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>'+
						'<div class="row filter-body">'+
							'<div class="col-md-2 col-sm-2 col-lg-2">'+
								'<div class="form-group">'+
									'<select class="form-control filter-field">'+
										'<option value="State_vch">State</option>'+
										'<option value="City_vch">City</option>'+
										'<option value="Zipcode_vch">Zipcode</option>'+
										'<option value="TimeZone_int">TimeZone</option>'+
										filter_field_option+
									'</select>'+
								'</div>'+
							'</div>'+
							'<div class="col-md-2 col-sm-2 col-lg-2">'+
								'<div class="form-group">'+
									'<select class="form-control filter-action">'+
										'<option value="=">Is</option>'+
										'<option value="!=">Is Not</option>'+
										'<option value="LIKE">Similar To</option>'+
										'<option value="NOT LIKE">Not Similar To</option>'+
									'</select>'+
								'</div>'+
							'</div>'+
							'<div class="col-md-2 col-sm-2 col-lg-2">'+
								'<div class="form-group">'+
									'<input type="text" class="form-control filter-value">'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>';

	var filter_row_html_new = '<div class="uk-grid uk-grid-upsmall uk-flex-middle filter-body filter-row">'+
		'<div class="uk-width-auto uk-first-column">'+
			'<button type="button" class="btn green-cancel btn-remove-filter"><i class="fa fa-minus"></i></button>'+
		'</div>'+
		'<div class="uk-width-1-5@s">'+
			'<select class="form-control filter-field">'+
				'<option value="State_vch">State</option>'+
				'<option value="City_vch">City</option>'+
				'<option value="Zipcode_vch">Zipcode</option>'+
				'<option value="TimeZone_int">TimeZone</option>'+
				filter_field_option+
			'</select>'+
		'</div>'+
		'<div class="uk-width-1-5@s">'+
			'<select class="form-control filter-action">'+
				'<option value="=">Is</option>'+
				'<option value="!=">Is Not</option>'+
				'<option value="LIKE">Similar To</option>'+
				'<option value="NOT LIKE">Not Similar To</option>'+
			'</select>'+
		'</div>'+
		'<div class="uk-width-1-6@s">'+
			'<input type="text" class="form-control filter-value">'+
		'</div>'+
	'</div>'
	;

	$("body").on('click', '.btn-add-filter', function(event) {
		event.preventDefault();

		$(filter_row_html_new).insertBefore($(this).parentsUntil('uk-flex-middle')[1]);

		// if( $(".filter-row").length > 0 ){
		// 	$("#send-number").val('').attr("disabled", "disabled"); 
		// }
	});

	$("body").on('click', '.btn-remove-filter', function(event) {
		event.preventDefault();
		//$(this).parent("div").remove();
		$(this).parentsUntil('filter-row')[1].remove();


		// if($(".filter-row").length == 0 ){
		// 	$("#send-number").val('').removeAttr("disabled"); 
		// }
	});

	$("body").on('click', '.btn-send', function(event) {
		event.preventDefault();
		/* Act on the event */
	});

	$("body").on('click', '.btn-delete', function(event) {
		event.preventDefault();
		/* Act on the event */
	});

	var GetAllFilters = function () {
		var filters = [];
		$(".filter-row").each(function(index, el) {
			var filter = {};
			filter.NAME = $(el).find(".filter-field").val();
			filter.OPERATOR = $(el).find(".filter-action").val();
			filter.VALUE = $(el).find(".filter-value").val();
			if (filter.NAME.indexOf("int") >= 0) {
				filter.TYPE = "CF_SQL_INTEGER";
			} else {
				filter.TYPE = "CF_SQL_VARCHAR";
			}
			if (filter.VALUE != "") {
				filters.push(filter);
			}
		});
		return filters;
	}

	$("body").on('click', '.btn-apply-filter', function(event) {
		event.preventDefault();
		var filters = GetAllFilters();

		$.ajax({
			url: '/session/sire/models/cfc/import-contact.cfc?method=GetEligibleCount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpUploadId: uploadid, customFilter: JSON.stringify(filters)},
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (data.RXRESULTCODE > 0) {
				html = '<div class="uk-width-expand eligible-count">'+
							'<label for="" class="color-5c"><b>Total left eligible to send: </b> '+data.ELIGIBLE+'</label>'+
						'</div>';
				//$(".eligible-count").remove();
				var filter_body = $("body").find('.filter-body:last');
				if (filter_body.length <= 0) {
					$(".filter-section").find(".eligible-count").remove();
					$(".filter-section").prepend($(html).removeClass('uk-width-expand'));
				} else {
					$(".filter-section").find(".eligible-count").not('.uk-width-expand').remove();
					filter_body.find('.eligible-count').remove();
					filter_body.append(html);
				}

				totalEligible = data.ELIGIBLE;
				// if(totalEligible > 0)
				// $("#send-number").val(totalEligible).addClass('disabled');
				//$("#send-number").val(totalEligible);
			}
		})
		.fail(function(e, msg) {
			bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
		})
		.always(function() {
			$('#processingPayment').hide();
		});
		
	});

	$("body").on('change', '.filter-field', function(event) {
		event.preventDefault();
		var field = $(this).val();
		var filter_action = $(this).parents(".filter-row").find('.filter-action');
		if (field.indexOf("int") >= 0) {
			filter_action.html('');
			var action_html = '<option value="=">Equal</option>'+
								'<option value="<">Less Than</option>'+
								'<option value=">">Greater Than</option>';
			filter_action.html(action_html);
		} else {
			filter_action.html('');
			var action_html = '<option value="=">Is</option>'+
								'<option value="!=">Is Not</option>'+
								'<option value="LIKE">Similar To</option>'+
								'<option value="NOT LIKE">Not Similar To</option>';
			filter_action.html(action_html);
		}
	});

	function GetBlastLog(id, type){
		var table = $('#report-upload-blast').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
			"aoColumns": [
			{"mData": "CREATED", "sName": 'created', "sTitle": "Sent Date","bSortable": false,"sClass":"select td-select", "sWidth":"150px"},
			{"mData": "BATCHID", "sName": 'created', "sTitle": "Campaign ID","bSortable": false,"sClass":"batchid col-center", "sWidth":"125px"},
			{"mData": "CAMPAIGNNAME", "sName": 'campaign', "sTitle": "Campaign Name","bSortable": false,"sWidth":"45%"},
			{"mData": "TOTALSEND", "sName": 'send', "sTitle": "Total Sent","bSortable": false,"sClass":"timezone"},
			{"mData": "BALANCE", "sName": 'balance', "sTitle": "Balance","bSortable": false,"sClass":"action td-action"},
			{"mData": "ADMINUSERNAME", "sName": 'admin', "sTitle": "Sent By","bSortable": false,"sClass":"action td-action"}
			],
			"sAjaxDataProp": "BlastReport",
			"sAjaxSource": '/session/sire/models/cfc/import-contact.cfc?method=GetUploadBlastReport&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnDrawCallback": function( oSettings ) {
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
			},
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {
				aoData.push(
		            { "name": "inpUploadId", "value": uploadid}
	            );
				oSettings.jqXHR = $.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data) {
						fnCallback(data);
					}
				});
			},
			"fnInitComplete":function(oSettings){
				if(oSettings._iRecordsTotal == 0){
					this.parent().find('div.dataTables_paginate').hide();
				} else {
					this.parent().find('div.dataTables_paginate').show();
				}
			}
		});
	}

	$("body").on('change', '#CampaignId', function(event) {
		event.preventDefault();
		var id = $(this).val();
		var name = $(this).find("option:selected").text();
		name = name.slice(name.indexOf("-")+2);
		$(".span-batchId").text(id);
		$(".span-batchDesc").text(name);

		$(".btn-show-scheduler").removeClass('hidden');
	});

	$('.send-partial-form').validationEngine('attach', {
		promptPosition : "topLeft", 
		autoPositionUpdate: true, 
		showArrow: false, 
		scroll: false,
		focusFirstField : false
	});

	$("body").on('click', '.btn-send', function(event) {
		event.preventDefault();
		if (totalEligible <= 0) {
			bootbox.dialog({
				message: "There is no eligible number to send",
				title: "Send",
				buttons: {
					success: {
						className: "btn btn-success-custom btn-medium",
						label: "Ok",
						callback: function () {
						}
					}
				}
			});

			return false;
		}

		if ($('.send-partial-form').validationEngine("validate")) {
			var filters = GetAllFilters();
			var sendNumber = $("input#send-number").val();
			var batchId = $("#CampaignId").val();
			if (sendNumber > totalEligible) {
				bootbox.dialog({
					message: "Send number must not greater than total eligible number",
					title: "Send",
					buttons: {
						success: {
							className: "btn btn-success-custom btn-medium",
							label: "Ok",
							callback: function () {
							}
						}
					}
				});

				return false;
			}

			$.ajax({
				url: '/session/sire/models/cfc/import-contact.cfc?method=SendPartial&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {
					inpUploadId: uploadid,
					inpSendNumber: sendNumber,
					customFilter: JSON.stringify(filters),
					inpBatchId: batchId
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					bootbox.dialog({
						message: data.MESSAGE,
						title: "Success",
						buttons: {
							success: {
								className: "btn btn-success-custom btn-medium",
								label: "Ok",
								callback: function () {
									location.href = "/session/sire/pages/admin-import-contact";
								}
							}
						},
						onEscape: function () {
							location.href = "/session/sire/pages/admin-import-contact";
						}
					});
				} else {
					bootbox.dialog({
						message: data.MESSAGE,
						title: "Oops!",
						buttons: {
							success: {
								className: "btn btn-success-custom btn-medium",
								label: "Ok",
								callback: function () {
								}
							}
						}
					});
				}
			})
			.fail(function() {
				bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
			})
			.always(function() {
				$("#processingPayment").hide();
			});
			
		}
	});

	$(document).ready(function() {
		GetBlastLog();

		$("#CampaignId").val($("#CampaignId option:first").val()).trigger('change');

		//$(".btn-apply-filter").trigger('click');
	});
})(jQuery);