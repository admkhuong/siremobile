(function($){
	function updatePurchase() {
		var purchase_amount = $('.purchase-amount:checked');
		$('.number-credits').text(purchase_amount.data('credits'));
		$('.amount').text(purchase_amount.data('price'));
	}
	
	updatePurchase();
	
	$('.purchase-amount').click(function(){
		updatePurchase();
	});
	
	$('.btn-buy-credits').click(function(){
		var purchase_amount = $('.purchase-amount:checked');
		var purchase_num = purchase_amount.data('index');
		location.href = '/session/sire/pages/buy-credits-payment?num=' + purchase_num;
	});
})(jQuery);