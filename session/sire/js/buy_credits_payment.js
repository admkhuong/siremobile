(function($){
	"use strict";
	function update_expiration_date() {
		$('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
	}

	$("#numberKeyword").focus();
	
	update_expiration_date();
	
	$('#expiration_date_month, #expiration_date_year').change(function(){
		update_expiration_date();
	});
	
	loadCountry();

	function loadCountry(){
		if(country == '') country = "US";
		$('#country').val(country);
	}

	var buy_credits = $("#buy_credits");
	
	buy_credits.validationEngine('attach', {promptPosition : "topLeft", scroll: true, binded: false});
	
	buy_credits.submit(function(event){
		event.preventDefault();
		var amount = $('#amount').val();
		if (buy_credits.validationEngine('validate')) {
			var self = $(this);
			bootbox.dialog({
			    message: 'Amount to be paid: <b>$'+ amount +'</b> <br> Are you sure you would like to Make Payment?',
			    title: "Confirm Payment",
			    className: "confirm-make-payment",
			    buttons: {
			        success: {
			            label: "OK",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			                
							$('.btn-payment-credits').attr('disabled', 'disabled').text('Progressing');
							$('.form-footer a.btn-primary-custom').hide();
							$("#actionStatus").val('Processing');
							$("#processingPayment").show();
							
							$.ajax({
								method: 'POST',
								url: self.attr('action'),
								data: self.serialize(),
								dataType: 'json',
								//timeout: 6000,
								success: function(data) {
								
									$("#actionStatus").val('Make Payment');
									$("#processingPayment").hide();
				
									var bootstrapAlert = $('#bootstrapAlert');
									bootstrapAlert.find('.modal-title').text('Payment process');
									bootstrapAlert.find('.alert-message').text(data.MSG);
									if (data && data.ID == 1) {
										bootstrapAlert.on('hidden.bs.modal', function(){
											if (typeof data.URL_REDIRECT != 'undefined' && data.URL_REDIRECT != '') {
												location.href = data.URL_REDIRECT;
											}
											else {
												location.href = '/session/sire/pages/my-plan';
											}
										});
									} else {
										$('.btn-payment-credits').removeAttr('disabled').text('Make Payment');
										$('.form-footer a.btn-primary-custom').show();
									}
									bootstrapAlert.modal('show');
								}
							});
			            }
			        },
			        cancel: {
			            label: "Cancel",
			            className: "btn btn-medium btn-back-custom",
			            callback: function() {}
			        },
			    }
			});
			
			
		}
		
	});
	
	
	$('#btnStep1').click(function(e) {

		var usedCard = $('input[name=select_used_card]:checked');

		if(usedCard.length && usedCard.val() == 1)
		{
			$('#step1').hide(500);
			$('#step2').show(500);
			paymentConfirm();
		}
		else 
		{
			if (buy_credits.validationEngine('validate')) 
			{
				var d = new Date();
				var expiredMonth = parseInt($('#expiration_date_month').val());
				var expiredYear = parseInt($('#expiration_date_year').val());
				if ( (expiredYear < d.getYear()) || (expiredMonth <= d.getMonth()+1 && expiredYear <= d.getFullYear())) {

					bootbox.dialog({
						message: 'Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card.',
						title: 'PAYMENT FAILED',
						buttons: {
					        success: {
					            label: "OK",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
				}

				$('#step1').hide(500);
				$('#step2').show(500);
				paymentConfirm();	
			}
		}
		
	});
	
	
	$('#btnStep2').click(function(e){
		if (buy_credits.validationEngine('validate')) {
			$('#step2').hide(500);
			$('#step3').show(500);

			if ($('input[name=select_used_card]:checked').val() == 1) {
				$('#rsFirstName').text($('#h_firstName').val());
				$('#rsLastName').text($('#h_lastName').val());
			}
			else {
				$('#rsFirstName').text($('#first_name').val());
				$('#rsLastName').text($('#last_name').val());
			}
			$('#rsAddress').text($('#line1').val());
			$('#rsCity').text($('#city').val());
			$('#rsState').text($('#state').val());
			$('#rsZipCode').text($('#zip').val());
			$('#rsCountry').text($('#country').val());
			$('#rsEmail').text($('#email').val());
		}

	});
	
	$('#btnStep2_back').click(function(e) {
		$('#step2').hide(500);
		$('#step1').show(500);
	});
	
	$('#btnStep3_back').click(function(e) {
		$('#step3').hide(500);
		$('#step2').show(500);
	});
	
	/*
	var fields = $('#buy_credits input, #buy_credits select');
	
	fields.focus(function(){
		var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
		setTimeout(function(){
			nextField.validationEngine('hide');
		},1);
	});*/
	
	
	var checked_payment_method = $('input.check_payment_method:checked').val();
	function switchPaymentMethod() {
		var check_payment_method = $('input.check_payment_method:checked').val();
		if (checked_payment_method == check_payment_method) return;
		checked_payment_method = check_payment_method;
		switch(check_payment_method) {
			case 0:
			case '0':
				$('#card_number').attr('class', 'form-control validate[required,custom[creditCardFunc]]')
					.removeAttr('data-errormessage-custom-error').parent().prev().html('Card Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid security code',
					'maxlength': 4
				}).parent().prev().html('Security Code:<span class="text-danger">*</span> <a href="javascript:$(\'#scModal\').modal(\'show\')" data-toggle="modal" data-target="#scModal"><img src="/session/sire/images/help-small.png"/></a>');
				$('#ExpirationDate').show();
				$('#card_name_label').html('Cardholder Name:<span class="text-danger">*</span>');
				break;
			case 1:
			case '1':
				$('#card_number').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid routing number'
				}).parent().prev().html('Routing Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid account number',
					'maxlength': 32
				}).parent().prev().html('Account Number:<span class="text-danger">*</span>');
				$('#ExpirationDate').hide();
				$('#card_name_label').html('Account Name:<span class="text-danger">*</span>');
				break;
		}
	}
	
	$('input.check_payment_method').click(switchPaymentMethod);

	if($('.select_used_card').length > 0){

		$('.select_used_card').click(function(){
			$('#card_number').val('');
			$('#security_code').val('');
			$('#expiration_date').val('');
			$('#expiration_date_month').val('');
			$('#expiration_date_year').val('');
			$('#first_name').val('');
			$('#last_name').val('');

			buy_credits.validationEngine('hideAll');

			if($(this).val() == 2){
				$('.update_card_info').show();
				$('.update_cardholder_info fieldset').prop('disabled',false);
				$( ".cardholder" ).each( function( index, element ){
	    			$(this).val('');
				});
				$('#country').val('US');
				$('#email').val($('#h_email').val());
			}
			else{
				$('.update_card_info').hide();	
				$('.update_cardholder_info fieldset').prop('disabled',true);
				$( ".cardholder" ).each( function( index, element ){
	    			var data = $(this).data('value');
	    			$(this).val(data);
				});
			}
		});
	}
	
	$('#numberSMS').focus();
})(jQuery);