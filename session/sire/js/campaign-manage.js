var campaign_url = '';

$(document).ready(function(){
	InitControl();
	
	$("#form-create-campaign").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$('#btn-create-campaign').click(function(){
		if (typeof  $("input:radio[name='optradio']:checked").val() != 'undefined') {
				if ($("input:radio[name='optradio']:checked").val() == 'opt-in') {
					window.location.href = '/session/sire/pages/opt-in';
				}
				if ($("input:radio[name='optradio']:checked").val() == 'sms') {
					window.location.href = '/session/sire/pages/sms-message-create';
				}
				
		}
		$('#createCampaignModal').modal('hide');
	});
	

	var _callAjax = false;

	/* DO NOT USE 
	$('#btn-rename-campaign').click(renameCampaign);
	$("#form-rename-campaign").submit(createCampaign);
	
	function createCampaign(event){
		event.preventDefault();
		
		if(_callAjax) return false;
		$('.btn').prop('disabled',true);

		if($('#form-create-campaign').validationEngine('validate')){
			try{

				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/campaign.cfc?method=CreateCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: $('#form-create-campaign').serialize(),
				beforeSend: function( xhr ) {
					
					_callAjax = true;
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('Create Fail', function() {});
					$('.btn').prop('disabled',false);
					_callAjax = false;
				},					  
				success:		
					function(d) {

						if(d.RXRESULTCODE == 1){
							//goto success page
							$('#createCampaignModal').modal('hide');
							bootbox.alert(d.MESSAGE, function() {
								$('.btn').prop('disabled',false);
								_callAjax = false;
							});
							var oTable = $('#tblListEMS').dataTable();
							oTable.fnDraw();
						}
						else{
							bootbox.alert(d.MESSAGE, function() {});
							_callAjax = false;
							$('.btn').prop('disabled',false);
						}
					} 		
				});
			}catch(ex){
				bootbox.alert('Create Fail', function() {});
				$('.btn').prop('disabled',false);
				_callAjax = false;
			}
		}
		$('.btn').prop('disabled',false);
	}
	*/

	/*
	// RENAME CAMPAIGN - DO NOT USE 

	function renameCampaign(event){
		event.preventDefault();
		if($('#form-rename-campaign').validationEngine('validate')){
			if(_callAjax) return false;
			try{
				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/campaign.cfc?method=RenameCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: $('#form-rename-campaign').serialize(),
				beforeSend: function( xhr ) {
					$('.btn').prop('disabled',true);
					_callAjax = true;
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('Rename Fail', function() {});
					//$("#enable_security_question_form")[0].reset();
				},					  
				success:		
					function(d) {
						if(d.RXRESULTCODE == 1){
							//goto success page
							$('#renameCampaignModal').modal('hide');
							bootbox.alert(d.MESSAGE, function() {});
							var oTable = $('#tblListEMS').dataTable();
							oTable.fnDraw();
							_callAjax = false;
							$('.btn').prop('disabled',false);
							return;
						}
						else{
							bootbox.alert(d.MESSAGE, function() {});
							_callAjax = false;
							$('.btn').prop('disabled',false);
						}
					} 		
				});
			}catch(ex){
				bootbox.alert('Rename Fail', function() {});
				_callAjax = false;
				$('.btn').prop('disabled',false);		
			}
		}

		$('#renameCampaignModal #campaignId').val('');
		$('#renameCampaignModal #campaignName').val('');
	}


		$( document ).on( "click", ".btn-rename-campaign", function(event) {
			event.preventDefault();
			var campaignId = $(this).data('campaign-id');
			var campaignName = $(this).data('campaign-name');

			if(parseInt(campaignId) >0 && campaignName !='')
			{
				$('#renameCampaignModal #campaignId').val(parseInt(campaignId));
				$('#renameCampaignModal #campaignName').val(campaignName);
				$('#renameCampaignModal').modal();
			}
			else
				bootbox.alert('Invalid data', function() {});
		});
	*/

	var keywordValidateTimer;
	$('#cloneCampaignModal #keyword').on('input', function(){
		var self = $(this);
		clearTimeout(keywordValidateTimer);
		keywordValidateTimer = setTimeout(function(){
			self.validationEngine('validate');
		}, 300);
	});

	$( document ).on( "click", "#btn-clone-campaign1", function(event) {
		event.preventDefault();
		
	});

	//DELETE MODAL
	$( document ).on( "click", ".btn-delete-campaign", function(event) {
	 	event.preventDefault();
	 	var dialogTitle =  "Delete campaign";
	 	var campaignId = $(this).data('campaign-id');
	 	if(parseInt(campaignId) > 0){

			bootbox.dialog({
			        message: 'Are you sure you want to delete this campaign?',
			        title: dialogTitle,
			        className: "",
			        buttons: {
			            success: {
			                label: "OK",
			                className: "btn btn-medium btn-success-custom",
			                callback: function(result) {
			                    if(result){
								  	try{
										$.ajax({
										type: "POST",
										url: '/session/sire/models/cfc/campaign.cfc?method=UpdateCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
										dataType: 'json',
										data: { campaignId: campaignId},
										beforeSend: function( xhr ) {
											$('#processingPayment').show();
										},					  
										error: function(XMLHttpRequest, textStatus, errorThrown) {
											$('#processingPayment').hide();
											bootbox.dialog({
											    message: "Delete Fail",
											    title: dialogTitle,
											    buttons: {
											        success: {
											            label: "Ok",
											            className: "btn btn-medium btn-success-custom",
											            callback: function() {}
											        }
											    }
											});
										},					  
										success:		
											function(d) {
												$('#processingPayment').hide();
												if(d.RXRESULTCODE == 1){
													//goto success page
													/*bootbox.alert(d.MESSAGE, function() {});*/
													bootbox.dialog({
												        message: d.MESSAGE,
												        title: dialogTitle,
												        buttons: {
												            success: {
												                label: "Ok",
												                className: "btn btn-medium btn-success-custom",
												                callback: function() {
												                }
												            }
												        }
													});	
													
													/*var oTable = $('#tblListEMS').dataTable();
													oTable.fnDraw();*/
													$('#tblListEMS_wrapper .paginate_text').keyup();
													return;
												}
												else{
													/*bootbox.alert(d.MESSAGE, function() {});*/
													bootbox.dialog({
												        message: d.MESSAGE,
												        title: dialogTitle,
												        buttons: {
												            success: {
												                label: "Ok",
												                className: "btn btn-medium btn-success-custom",
												                callback: function() {
												                }
												            }
												        }
													});	
												}
											} 		
										});
									}catch(ex){
										$('#processingPayment').hide();
										bootbox.dialog({
												        message: "Delete Fail",
												        title: dialogTitle,
												        buttons: {
												            success: {
												                label: "Ok",
												                className: "btn btn-medium btn-success-custom",
												                callback: function() {
												                }
												            }
												        }
										});
									}
								}
			                }
			            },
			            cancel: {
			                label: "Cancel",
			                className: "btn btn-medium btn-back-custom",
			                callback: function() {
			
			                }
			            },
			        }
			    });
		 	}
	 	else
	 	{
	 		//bootbox.alert('Invalid data', function() {});
	 		bootbox.dialog({
			    message: 'Invalid data',
			    title: dialogTitle,
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
			});
	 	}
	});
	
	

	// TEMPLATE PICKER
	$( document ).on( "mouseenter", ".item-grid .content", function(event) {
		event.preventDefault();
		$('.item_action').hide();
		$(".item-grid .content").removeClass('selected');
		$(this).addClass('selected');
		campaign_url = $(this).attr("href");
		$('#btn-select-campaign').prop('disabled',false);

		$(this).find('.item_action').show();

	});
	
	$( document ).on( "mouseleave", ".item-grid .content", function(event) {
	
		// Keep the selection sticky if the preview option is still active		
		if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
		{
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
		}
	});
	

	$( document ).on( "click", "#btn-select-campaign, .btn-select-campaign", function(event) {
		if(typeof campaign_url != 'undefined' && campaign_url != '')
		window.location.href = campaign_url;
	});



	$( ".item-grid .content" ).dblclick(function(event) {
		event.preventDefault();
  		campaign_url = $(this).attr("href");
  		window.location.href = campaign_url;
	});

	$('#createCampaignModal a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
	  e.target // newly activated tab
	  e.relatedTarget // previous active tab
	  $(".item-grid .content").removeClass('selected');
	  campaign_url = '';
	  $('#btn-select-campaign').prop('disabled',true);
	});
	
	//BTN-ACTION
	$( document ).on( "click", ".statusAction", function(event) {
		event.preventDefault();
		var campaignid = $(this).parents('ul.dropdown-menu').data('campaign-id');
		var action = $(this).data('action');
		var currentPage = $('input.paginate_text').val();

		if(campaignid > 0 && action != ''){
			try{
				$('.btn').prop('disabled',true);
				$.ajax(
				{
					type: "POST",
					url: '/session/sire/models/cfc/campaign.cfc?method=updateCampaignStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: { inpBatchId: campaignid,inpAction:action},
					beforeSend: function( xhr ) {
						$('#processingPayment').show();
						$('.tblListEMS_processing').show();
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('#processingPayment').hide();
						bootbox.dialog({
						    message: "Update Fail",
						    title: "Campaigns",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						$('.btn').prop('disabled',false);
						$('.tblListEMS_processing').hide();
					},					  
					success:		
						function(d) {
							$('#processingPayment').hide();
							$('.btn').prop('disabled',false);
							$('.tblListEMS_processing').hide();
							if(d.RXRESULTCODE == 1){
								//goto success page
								/*bootbox.alert(d.MESSAGE, function() {});*/
								bootbox.dialog({
								    message: d.MESSAGE,
								    title: "Campaigns",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
								});
								var oTable = $('#tblListEMS').dataTable();
								//oTable.fnDraw();
								oTable.fnPageChange(parseInt(currentPage-1));
								return;
							}
							else{
								/*bootbox.alert(d.MESSAGE, function() {});*/
								bootbox.dialog({
								    message: d.MESSAGE,
								    title: "Campaigns",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
								});
							}
						} 		
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
								    message: "Update Fail",
								    title: "Campaigns",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {}
								        }
								    }
				});
				$('.btn').prop('disabled',false);
				$('.tblListEMS_processing').hide();
			}
		}
	});
	
});

var _tblListEMS;
function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
	var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
	//init datatable for active agent
	_tblListEMS = $('#tblListEMS').dataTable({
		"bStateSave": true,
		"iStateDuration": -1,
		"fnStateLoadParams": function (oSettings, oData) {
			if (location.hash == "") {
				oData.oSearch.sSearch = "";
				return false;
			}
		},
		"fnStateSaveParams": function (oSettings, oData) {
			if (history.replaceState) {
				if (oData.iStart > 0) {
					history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
				} else {
					history.replaceState(history.state, document.title, location.pathname + location.search);
				}
			}
		},

		
	    "bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
	    "bLengthChange": false,
		"iDisplayLength": 20,
		"bAutoWidth": false,
	    "aoColumns": [
			{"sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"camp-id", "sWidth":"80px"},
			{"sName": 'name', "sTitle": 'Campaign Name',"bSortable": true,"sClass":"camp-name", "sWidth":"200px"},
			{"sName": 'keyword', "sTitle": "Keyword","bSortable": false,"sClass":"camp-keyword"},
			{"sName": 'link', "sTitle": 'Actions',"bSortable": false,"sClass":"camp-action"}
		],
		"sAjaxDataProp": "ListEMSData",
		"sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetCampaignList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			$(nRow).children('td:first').addClass("avatar");

			if (aData && aData[8] == 'Not running') {
				$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
			}
			else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
				$(nRow).removeClass('odd').removeClass('even').addClass('paused');
			}

			if (aData && aData[5] == 'In Process') {
				$(nRow).addClass('in_process');
				$(nRow).data('PKID', aData[4]);
			}

            return nRow;
       	},
	   "fnDrawCallback": function( oSettings ) {
	      // MODIFY TABLE
			//$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			if (oSettings._iDisplayStart < 0){
				oSettings._iDisplayStart = 0;
				$('input.paginate_text').val("1");
			}
			$("#tblListEMS tbody tr td").find('td:last-child a').hide();
			$("#tblListEMS tbody tr td").find('.EMSIcon').hide();
			
			$('#tblListEMS > tbody').find('tr').each(function(){
				var tdFirst = $(this).find('td:first');
				if(tdFirst.hasClass('dataTables_empty')){
					$('#tblListEMS_paginate').hide();
					return false;
				}
				else{
					$('#tblListEMS_paginate').show();
				} 
				var campaignId = tdFirst.text();
				var campaignName = $(this).find('td:eq(1)').html();
				var campaignStatus = $.trim($(this).find('td:eq(2)').text());

				var html = '';

				if ($(this).hasClass('in_process')) {
			        html += '<div class="dropdown"><button type="button" class="btn btn-success-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
					html += 'Processing <span class="caret"></span>';
					html += '</button>';
					html += '<ul class="dropdown-menu">';
						
					html += '<li><a class="processActions" data-pkid="'+$(this).data('PKID')+'" data-action="view">View</a></li>';
						
					html += '<li><a class="processActions" data-pkid="'+$(this).data('PKID')+'" data-action="stop">Stop</a></li>';
						
					html += '</ul></div>';
				} else {
					if(['In Process'].indexOf(campaignStatus) >= 0) {
						html +='<a class="btn btn-success-custom btn-report" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'&af_batch_id='+campaignId+'">Report</a>';
						html +='<a class="btn btn-success-custom" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						html +=' <div class="btn-group btn-manager-wrapper"> <a class="btn btn-back-custom btn-delete-campaign-disabled disabled" data-campaign-id='+campaignId+' >Delete</a> </div>';  //data-campaign-name="'+campaignName+'"
					}
					else
					{

						html +='<a class="btn btn-success-custom btn-report" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'&af_batch_id='+campaignId+'">Report</a>';
				      	html += '<div class="btn-group btn-manager-wrapper">';
						
						/*
						if (['Pause', 'Finished'].indexOf(campaignStatus) >= 0) {
						html +='	<a class="btn btn-success-custom" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'">Report</a>';
						} else {
						html +='	<a class="btn btn-success-custom" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						}*/
						
						
						if (['Paused', 'Finished'].indexOf(campaignStatus) < 0) {
							html +='<a class="btn btn-success-custom" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						}
						
						if (['Scheduled', 'Paused'].indexOf(campaignStatus) >= 0) {
						
					        html += '	<button type="button" class="btn btn-success-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
							html += '	<span class="caret"></span>';
							html += '	<span class="sr-only">Toggle Dropdown</span>';
							html += '	</button>';
							html += '	<ul class="dropdown-menu" data-campaign-id='+campaignId+'>';
								
							if ('Paused' == campaignStatus) {
								html += '<li><a href="#" class="statusAction" data-action="Run">Run</a></li>';
							}
								
							if ('Scheduled' == campaignStatus) {
								html += '<li><a href="#" class="statusAction" data-action="Pause">Pause</a></li>';
							}
								
							if (['Scheduled', 'Paused'].indexOf(campaignStatus) >= 0) {
								html += '<li><a href="#" class="statusAction" data-action="Stop">Stop</a></li>';
							}
								
							/*if (['In Review', 'Pause'].indexOf(campaignStatus) < 0) {
									
									if (['Scheduled'].indexOf(campaignStatus) >= 0) {
							html += '		<li role="separator" class="divider"></li>';
									}
									
							html += '		<li><a href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'">Report</a></li>';
							}*/
								
							html += '</ul>';
						}

						html += '</div>';

						html+='<button class="btn btn-back-custom btn-delete-campaign" data-campaign-id='+campaignId+' >Delete</button>'; // data-campaign-name="'+campaignName+'"
					}
				}

				$(this).find('td:last').html(html);
			})
	    },
		"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
	        if (aoData[3].value < 0){
				aoData[3].value = 0;
			}
	        aoData.push(
	            { "name": "customFilter", "value": customFilterData}
            );
	        $.ajax({dataType: 'json',
	                 type: "POST",
	                 url: sSource,
		             data: aoData,
		             success: fnCallback
		 	});
        },
		"fnInitComplete":function(oSettings, json){
			// MODIFY TABLE
			//$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			$("#tblListEMS tbody tr td").find('td:last-child a').hide();
			$("#tblListEMS tbody tr td").find('.EMSIcon').hide();
			
			$('#tblListEMS > tbody').find('tr').each(function(){
				var tdFirst = $(this).find('td:first');

				if(tdFirst.hasClass('dataTables_empty')){
					$('#tblListEMS_paginate').hide();
					return false;
				} 

				var campaignId = tdFirst.text();
				var campaignName = $(this).find('td:eq(1)').html();
				var campaignStatus = $.trim($(this).find('td:eq(2)').text());

				//var html ='<button class="btn btn-medium btn-success-custom btn-rename-campaign" data-campaign-name="'+campaignName+'" data-campaign-id="'+campaignId+'" >Rename</button>';
				var html = '';

				if ($(this).hasClass('in_process')) {
			        html += '<div class="dropdown"><button type="button" class="btn btn-success-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
					html += 'Processing <span class="caret"></span>';
					html += '</button>';
					html += '<ul class="dropdown-menu">';
						
					html += '<li><a class="processActions" data-pkid="'+$(this).data('PKID')+'" data-action="view">View</a></li>';
						
					html += '<li><a class="processActions" data-pkid="'+$(this).data('PKID')+'" data-action="stop">Stop</a></li>';
						
					html += '</ul></div>';
				} else {
					if(['In Process'].indexOf(campaignStatus) >= 0) {
						html +='<a class="btn btn-success-custom btn-report disabled" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'&af_batch_id='+campaignId+'">Report</a>';
						html +='<a class="btn btn-success-custom disabled" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						html +=' <div class="btn-group btn-manager-wrapper"> <a class="btn btn-back-custom btn-delete-campaign-disabled disabled" data-campaign-id='+campaignId+' >Delete</a> </div>'; // data-campaign-name="'+campaignName+'"
					}
					else
					{

						html +='<a class="btn btn-success-custom btn-report" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'&af_batch_id='+campaignId+'">Report</a>';
				      	html += '<div class="btn-group btn-manager-wrapper">';
						
						/*
						if (['Pause', 'Finished'].indexOf(campaignStatus) >= 0) {
						html +='	<a class="btn btn-success-custom" href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'">Report</a>';
						} else {
						html +='	<a class="btn btn-success-custom" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						}*/
						
						
						if (['Paused', 'Finished'].indexOf(campaignStatus) < 0) {
							html +='<a class="btn btn-success-custom" href="/session/sire/pages/campaign-edit?campaignid=' + campaignId + '">Manage</a>';
						}
						
						if (['Scheduled', 'Paused'].indexOf(campaignStatus) >= 0) {
						
					        html += '	<button type="button" class="btn btn-success-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
							html += '	<span class="caret"></span>';
							html += '	<span class="sr-only">Toggle Dropdown</span>';
							html += '	</button>';
							html += '	<ul class="dropdown-menu" data-campaign-id='+campaignId+'>';
								
							if ('Paused' == campaignStatus) {
								html += '<li><a href="#" class="statusAction" data-action="Run">Run</a></li>';
							}
								
							if ('Scheduled' == campaignStatus) {
								html += '<li><a href="#" class="statusAction" data-action="Pause">Pause</a></li>';
							}
								
							if (['Scheduled', 'Paused'].indexOf(campaignStatus) >= 0) {
								html += '<li><a href="#" class="statusAction" data-action="Stop">Stop</a></li>';
							}
								
							/*if (['In Review', 'Pause'].indexOf(campaignStatus) < 0) {
									
									if (['Scheduled'].indexOf(campaignStatus) >= 0) {
							html += '		<li role="separator" class="divider"></li>';
									}
									
							html += '		<li><a href="/session/sire/pages/campaign-report?campaignid='+ campaignId +'">Report</a></li>';
							}*/
								
							html += '</ul>';
						}

						html += '</div>';

						html+='<button class="btn btn-back-custom btn-delete-campaign" data-campaign-id='+campaignId+' >Delete</button>'; // data-campaign-name="'+campaignName+'"	
					}
				}

				$(this).find('td:last').html(html);
			})
		}
    });

}

//load data when paging
$('body').on('keyup', '.paginate_text_active', function(event) {
	if($('.paginate_text_active').val() <= 0){
		$('.paginate_text_active').val('1');
	}

	if ( parseInt($('.paginate_text_active').val()) > parseInt($('#allPages').html()) ){
		$('.paginate_text_active').val(parseInt($('#allPages').html()));
	}
	ShowActivitySpinner();
	$.ajax({
		type: 'GET',
		url: '/session/sire/views/commons/active_feed.cfm',   

		dataType: 'html',
		data: {
			id: $(this).data('campaign-id'),
			af_start_date: $('#AFStartDate').val(),
			af_end_date: $('#AFEndDate').val(),
			af_phone_number: $('#AFPhoneNumber').val(),
			af_batch_id: $('#AFBatchId').val(),
			page: $('.paginate_text_active').val()
		},
		success: function(html) {
			var newAFTable = $(html);
			$('#totalFeed').val('0');
			$('#AFTable').html(newAFTable.html());
			HideActivitySpinner()
		}
	});
});

$('body').on('click', '.paginate_button_af_next', function(event) {
	if ( parseInt($('.paginate_text_active').val()) < parseInt($('#allPages').html()) ) {
		var nextPage = parseInt($('.paginate_text_active').val()) + 1;
		$('.paginate_text_active').val(nextPage);
		ShowActivitySpinner();
		$.ajax({
			type: 'GET',
			url: '/session/sire/views/commons/active_feed.cfm',   

			dataType: 'html',
			data: {
				id: $(this).data('campaign-id'),
				af_start_date: $('#AFStartDate').val(),
				af_end_date: $('#AFEndDate').val(),
				af_phone_number: $('#AFPhoneNumber').val(),
				af_batch_id: $('#AFBatchId').val(),
				page: $('.paginate_text_active').val()
			},
			success: function(html) {
				var newAFTable = $(html);
				$('#totalFeed').val('0');
				$('#AFTable').html(newAFTable.html());
				HideActivitySpinner()
			}
		});
	}
	
});
$('body').on('click', '.paginate_button_af_previous', function(event) {
	if (parseInt($('.paginate_text_active').val()) > 1){
		ShowActivitySpinner();
		var nextPage = parseInt($('.paginate_text_active').val()) - 1;
		$('.paginate_text_active').val(nextPage);

		$.ajax({
			type: 'GET',
			url: '/session/sire/views/commons/active_feed.cfm',   

			dataType: 'html',
			data: {
				id: $(this).data('campaign-id'),
				af_start_date: $('#AFStartDate').val(),
				af_end_date: $('#AFEndDate').val(),
				af_phone_number: $('#AFPhoneNumber').val(),
				af_batch_id: $('#AFBatchId').val(),
				page: $('.paginate_text_active').val()
			},
			success: function(html) {
				var newAFTable = $(html);
				$('#totalFeed').val('0');
				$('#AFTable').html(newAFTable.html());
				HideActivitySpinner()
			}
		});
	}
	
});

$('body').on('click', '.paginate_button_af_first', function(event) {
	$('.paginate_text_active').val('1');
	ShowActivitySpinner();
	$.ajax({
		type: 'GET',
		url: '/session/sire/views/commons/active_feed.cfm',   

		dataType: 'html',
		data: {
			id: $(this).data('campaign-id'),
			af_start_date: $('#AFStartDate').val(),
			af_end_date: $('#AFEndDate').val(),
			af_phone_number: $('#AFPhoneNumber').val(),
			af_batch_id: $('#AFBatchId').val(),
			page: $('.paginate_text_active').val()
		},
		success: function(html) {
			var newAFTable = $(html);
			$('#totalFeed').val('0');
			$('#AFTable').html(newAFTable.html());
			HideActivitySpinner()
		}
	});
});

$('body').on('click', '.paginate_button_af_last', function(event) {
	$('.paginate_text_active').val(parseInt($('#allPages').html()));
	ShowActivitySpinner();
	$.ajax({
		type: 'GET',
		url: '/session/sire/views/commons/active_feed.cfm',   

		dataType: 'html',
		data: {
			id: $(this).data('campaign-id'),
			af_start_date: $('#AFStartDate').val(),
			af_end_date: $('#AFEndDate').val(),
			af_phone_number: $('#AFPhoneNumber').val(),
			af_batch_id: $('#AFBatchId').val(),
			page: $('.paginate_text_active').val()
		},
		success: function(html) {
			var newAFTable = $(html);
			$('#totalFeed').val('0');
			$('#AFTable').html(newAFTable.html());
			HideActivitySpinner()
		}
	});
});

function ShowActivitySpinner() {
	$('.activity-spinner').show();
}

function HideActivitySpinner() {
	$('.activity-spinner').hide();
}
function ReloadDataTable() {
	var filtered = CheckFilterd();
	if (filtered) {
		ApplyFilter_();
	} else {
		InitControl();
	}
}
/* Check if any filtes input have value */
function CheckFilterd () {
	var filtered = false;
	$('input.filter_val').each(function(index, el) {
		if ($(el).val().length > 0) {
			filtered = true;
		}
	});
	return filtered;
}

/* Trigger process actions button */
$('body').on('click', '.processActions', function (event) {
	var data = $(this).data();
	var method = '';
	var inpData = {};
	switch(data['action']) {
		case 'view':
			method = 'GetQueueProcessInfo';
			inpData.inpPKID = data['pkid'];
			processActions(data['action'], method, inpData);
			break;
		case 'stop':
			method = 'updateQueueStatus';
			inpData.inpPKID = data['pkid'];
			inpData.inpStatus = 0;
			inpData.inpNote = "User stopped";

			/* Display confirm modal first */
			bootbox.dialog({
			    message: "Are you sure you want to stop this blast process?",
			    title: 'Stop Process',
			    buttons: {
			        cancel: {
			        	label: "Cancel",
			        	className: "btn btn-medium btn-back-custom",
			        	callback: function () {}
			        },
			        success: {
			            label: "Confirm",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	processActions(data['action'], method, inpData);
			            }
			        }
			    }
			});
			break;
		default:
			break;
	}

	return true;
});

function processActions(action, method, inpData) {
	$.ajax({
		url: '/session/sire/models/cfc/queue.cfc?method='+method+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		type: 'POST',
		data: inpData,
		beforeSend: function () {
			$('#processingPayment').show();
		}
	})
	.done(function(d) {
		if (parseInt(d.RXRESULTCODE) > 0) {
			switch(action) {
				case 'view':
					$('#progressouter').css('width', d.PERCENT+"%");
					$('#timecounter').text(d.PERCENT + '%');
					$('#loadQueueProcess').modal('show');

					/* If process is done, reload campaign table */
					if (parseInt(d.PERCENT) >= 100) {
						ReloadDataTable();
					} else {
						/* setup an interval to auto reload progress bar after 1 minute */
						var processInterval = setInterval(function () {
							$.ajax({
								url: '/session/sire/models/cfc/queue.cfc?method='+method+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								type: 'POST',
								data: inpData
							})
							.done(function(rx) {
								if (parseInt(rx.RXRESULTCODE) > 0) {
									$('#progressouter').css('width', rx.PERCENT+"%");
									$('#timecounter').text(rx.PERCENT + '%');

									/* If process is done, reload campaign table */
									if (parseInt(rx.PERCENT) >= 100) {
										$('#loadQueueProcess').modal('hide');
										ReloadDataTable();
									}
								}
							});
						}, 10000);

						/* clear interval when modal is closed */
						$('#loadQueueProcess').on("hide.bs.modal", function () {
							clearInterval(processInterval);
						});
					}
					break;
				case 'stop':
					bootbox.dialog({
					    message: d.MESSAGE,
					    title: 'Success',
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
					break;
			}
			ReloadDataTable();
		} else {
			bootbox.dialog({
			    message: d.MESSAGE,
			    title: 'Oops!',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
			ReloadDataTable();
		}
	})
	.fail(function(e) {
		console.log(e);
	})
	.always(function() {
		$('#processingPayment').hide();
	});
}

(function($){
	
		$('body').on('click', 'a[data-target="#ActFeeFilModal"]', function(){
		});

	if($('#AFStartDate').length >0)
	{
		$('#AFStartDate').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'mm/dd/yy',
			showOn: 'both',
			buttonImage: "/public/images/calendar.png",
	        buttonImageOnly: true
		});
		
		$('#AFEndDate').datepicker({
			numberOfMonths: 1,
			showButtonPanel: false,
			dateFormat: 'mm/dd/yy',
			showOn: 'both',
			buttonImage: "/public/images/calendar.png",
	        buttonImageOnly: true
		});
	}
	
	$('#AFSubmit').click(function(){
		
		var start_date= new Date($('#AFStartDate').val());
		var end_date= new Date($('#AFEndDate').val());
        
        
        if (start_date > end_date) {
			bootbox.dialog({
			    message:'Start Date must less than End Date',
			    title: 'Activity Filter',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
        } else {
        	if ($('#active_feed_form').validationEngine('validate')) {
				$.ajax({
					type: 'GET',
					url: '/session/sire/views/commons/active_feed.cfm',   
					dataType: 'html',
					data: {
						id: $(this).data('campaign-id'),
						af_start_date: $('#AFStartDate').val(),
						af_end_date: $('#AFEndDate').val(),
						af_phone_number: $('#AFPhoneNumber').val(),
						af_batch_id: $('#AFBatchId').val(),
						page: 1
					},
					success: function(html) {
						var newAFTable = $(html);
						$('#totalFeed').val('0');
						//$('.download-subscribers').attr('href', '/session/sire/models/cfm/feed.xls.cfm?id='+$(this).data('campaign-id')+'&af_start_date='+$('#AFStartDate').val()+'&af_end_date='+$('#AFEndDate').val()+'&af_phone_number='+$('#AFPhoneNumber').val()+'&af_batch_id='+$('#AFBatchId').val()).show();
						$('div.feed_block').html(html);
						$('.paginate_text_active').val("1");
						$('#ActFeeFilModal').modal('hide');
					}
				});

			}
        }
	});
	
	$('#AFReset').click(function(){
		$('#active_feed_form')[0].reset();
	});
	
	$('.download-subscribers').click(function(event) {
		var _iRecordsTotal = $('#totalFeed').val();
		if (!$.isNumeric(_iRecordsTotal) || _iRecordsTotal < 1) {
			event.preventDefault();
			bootbox.dialog({
			    message:'There are currently no Feed in this list to download.',
			    title: 'Download Activity Feed',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
		} else {
			// $("#processingPayment").show();
			// $.ajax({
   //      		url:'/session/sire/models/cfm/feed.xls.cfm?&af_start_date='+$('#AFStartDate').val()+'&af_end_date='+$('#AFEndDate').val()+'&af_phone_number='+$('#AFPhoneNumber').val()+'&af_batch_id='+$('#AFBatchId').val(),
   //      		dataType: 'json',
   //      		success: function(data) {
   //      			$("#processingPayment").hide();
			// 		for (var key in data) {
			// 			  if (data.hasOwnProperty(key)) {
			// 			    SaveToDisk(data[key]);
			// 			  }
  	// 			   }
		
   //      		}
   //      	});
   			window.location.href = '/session/sire/models/cfm/feed.xls.cfm?&af_start_date='+$('#AFStartDate').val()+'&af_end_date='+$('#AFEndDate').val()+'&af_phone_number='+$('#AFPhoneNumber').val()+'&af_batch_id='+$('#AFBatchId').val();

		}
	});
})(jQuery);

function SaveToDisk(fileUrl, fileName) {
    var hyperlink = document.createElement('a');
    hyperlink.href = fileUrl;
    hyperlink.target = '_self';
    hyperlink.download = fileName || fileUrl;

    (document.body || document.documentElement).appendChild(hyperlink);
    hyperlink.onclick = function() {
       (document.body || document.documentElement).removeChild(hyperlink);
    };

    var mouseEvent = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
    });

    hyperlink.dispatchEvent(mouseEvent);
    
    // NEVER use "revoeObjectURL" here
    // you can use it inside "onclick" handler, though.
    // (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);

    // if you're writing cross-browser function:
    if(!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
       window.URL.revokeObjectURL(hyperlink.href);
    }
}
