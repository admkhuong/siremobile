$("#save-as-new-template-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});
function htmlEncode(value){
  return $('<div/>').text(value).html();
}

function htmlDecode(value){
  return $('<div/>').html(value).text();
}

var RXxml_special_to_escaped_one_map = {
	'&' : '&amp;',
	'"' : '&quot;',
	'<' : '&lt;',
	'>' : '&gt;',
	//"'" : '&apos;',
	"?" : '&#63;'	
};

var RXescaped_one_to_xml_special_map = {
	'&amp;' : '&',
	'&quot;' : '"',
	'&lt;' : '<',
	'&gt;' : '>',
	'&apos;' : "'",
	'&#63;' : "?" 
};
var RXspace = {
	' ' : '%20'
}
var RXspacedecode = {
	'%20' : ' '
}

function RXencodeXML(string) {
	
	
	var retStr = String(string);
	if (isNaN(retStr)) {
		//retStr = retStr.replace(/([\&"<'>?])/g, function(str, item) {
		retStr = retStr.replace(/([\&"<>?])/g, function(str, item) {		
			return RXxml_special_to_escaped_one_map[item];
		});
	}
	return retStr;
};

function RXdecodeXML(string) {
	
	var retStr = String(string);
	if (isNaN(retStr)) {
		retStr = retStr.replace(/(&quot;|&lt;|&gt;|&amp;|&apos;|&#63;)/g,  function(str, item) {
			return RXescaped_one_to_xml_special_map[item];
		});
	}
	return retStr;
}

function RXencodeSpace(string) {

	var retStr = String(string); 
	return retStr.replace(/([\ ])/g, function(str, item) {
		return RXspace[item];
	});
}
function RXdencodeSpace(string) {
	
	var retStr = String(string); 
	return retStr.replace(/(%20)/g, function(str, item) {
		return RXspacedecode[item];
	});
}


(function($){
	var _action, _moveTo, 
		_saving = false,
		cpUpdated = false,
		cpIdUpdated = 0,
		cpObjUpdate = {};
	
	$('body').append($('body div .modal'));
	
	var modalAlert = function(title, message, event) {
		var bootstrapAlert = $('#bootstrapAlert');
		bootstrapAlert.find('.modal-title').text(title);
		bootstrapAlert.find('.alert-message').text(message);
		bootstrapAlert.modal('show');
	}
	
	var _cData;
	
	$('#CustomHelpMessage_vch, #CustomStopMessage_vch').on('input', function(){
		var nmessage = 0;
		var count = $(this).val().length;
		if (count > 0)
			var message = 'Character Count {' + count + '}';
		else
			var message = 'Character Count {<span style="color:#FF0000">' + count + '</span>}';
	
		if (count > 160) {
			if ((count % 153) == 0) {
			  nmessage = (count / 153)
			} else {
			  nmessage = Math.floor(count / 153) + 1;
			}
		}
		if (nmessage > 1 ) {
			message = message + ' <span style="color:#FF0000">' + nmessage + ' Increments</span>'
		}
		
		$(this).next().html(message);
	}).trigger('input');
	
	var keywordValidateTimer;
	$('#txtKeyword').on('input', function(){
		var self = $(this);
		clearTimeout(keywordValidateTimer);
		keywordValidateTimer = setTimeout(function(){
			self.validationEngine('validate');
		}, 300);
	});
	
	
	$('#EMS_Flag_2, #EMS_Flag_1, #EMS_Flag_0').click(function(){
		switch($(this).val()) {
			case 2:
			case '2':
				$('.cbic').hide();
				$('.cbems').hide();
				$('.cbpush').show();
				$('#btn-save-send').show();
				if ($('input.push-subscriber-radio:checked').val() == 0) {
					$('#btn-save-send').prop('disabled', true);
				} else {
					$('#btn-save-send').prop('disabled', false);
				}
				$('#btn-cancel').addClass('btn-cancel-clear');
				break;
			case 1:
			case '1':
				$('.cbic').hide();
				$('.cbpush').hide();
				$('.cbems').show();
				if ($('input.subscriber-radio:checked').val() == 0) {
					$('#btn-save-send').prop('disabled', true);
				} else {
					$('#btn-save-send').prop('disabled', false);
				}
				$('#btn-cancel').addClass('btn-cancel-clear');
				break;
			case 0:
			case '0':
				$('.cbic').show();
				$('.cbpush').hide();
				$('.cbems').hide();
				$('#btn-cancel').removeClass('btn-cancel-clear');
				$('#txtKeyword').validationEngine('attach');
		}
	});
			
	$('#format_answer').change(function(){
		var format = $(this).val();
		$('#answers_list .answer-item-list-style').each(function(id, el){
			$(el).text(getOptionListStyle(format, id));
		});
		$('#ONESELECTIONEDITPOPUP input').first().trigger('input');
	});
	
	$('#ONESELECTIONEDITPOPUP').on('input', 'input', function(){
		var cpLen = 0;
		var format_answer = $('#format_answer').val();
		
		switch(format_answer) {
			case 'NUMERICPAR':
			case 'ALPHAPAR':
				var styleLen = 4;
				break;
			case 'NUMERIC':
			case 'ALPHA':
				var styleLen = 7;
				break;
			default:
				var styleLen = 1;
		}
		
		$('#ONESELECTIONEDITPOPUP input').each(function(id, el){
			if (0 != id) {
				if (0 == (id % 10)) {
					switch(format_answer) {
						case 'NUMERICPAR':
						case 'ALPHAPAR':
							styleLen++;
							break;
					}
				}
				cpLen += styleLen;
			}
			cpLen += $(el).val().length;
		});
		
		if (cpLen > 160) {
			
			var CalcVal = Math.ceil(cpLen/153);					
			$('#ONESELECTIONEDITPOPUP .control-point-char').html( 'Character Count {' + cpLen + '}  ' + '<span class="text-danger">' + CalcVal + ' Increments</span>');
		} 
		else 
		{
			$('#ONESELECTIONEDITPOPUP .control-point-char').text( (160 - cpLen) + ' characters available' );
		}
		
		cpUpdated = true;
	});
			
	function updateCPCharecterCount(txt) {
		var cpLen = txt.length;
		if (cpLen > 160) {
			
			var CalcVal = Math.ceil(cpLen/153);	
			$('#CONTROLPOINTEDITPOPUP .control-point-char').html( 'Character Count {' + cpLen + '} '+ 	'<span class="text-danger">' + CalcVal + ' Increments</span>');
		} else {
			$('#CONTROLPOINTEDITPOPUP .control-point-char').text( (160 - cpLen) + ' characters available' );
		}

		cpUpdated = true;
	}
	
	$('#cdf_select, #subscriber_list_select').change(function(){
		cpUpdated = true;
	});
	
		
	$('#subscriber_list_select').change(function(){
		var self = $(this);
		if ('' == self.val()) {
			$('#subscriber_list_name').val('').validationEngine('hide');
			$('#AddNewSubscriberList').modal('show');
			self.val('a');
		}
	});
	
		
	$('#newTxtKeyword').on('input', function(){
		var self = $(this);
		clearTimeout(keywordValidateTimer);
		keywordValidateTimer = setTimeout(function(){
			self.validationEngine('validate');
		}, 300);
	});
			
	
			
	$('#confirm-launch-buy-credits').click(function(){
		$('#ConfirmLaunchModal').modal('hide');
	});	

	
	
	 
	 $('#txtKeyword').mouseout(function() {
	 	var txtKeyword = $.trim($('#txtKeyword').val())
		$('#txtKeyword').val(txtKeyword)	
	 });
	 
	 $('#keyword').mouseout(function() {
	 	var keyword = $.trim($('#keyword').val())
		$('#keyword').val(keyword)	
	 });
	
})(jQuery);


$(document).on('click', '#save-as-new-template', function(event){
	event.preventDefault();
	$('#AdminSaveNewTemplateModal').modal('show');
	GetMaxOrderByCategoryGroup();
	InitSaveNewTemplateForm();
	count_down_character_by_id('template-description-countdown','template-description', 1000);
	count_down_character_by_id('template-name-countdown','template-name', 255);
});

$('#AdminSaveNewTemplateModal').on('hide.bs.modal', function(){
	$('#select-template-image').ddslick('destroy');
	$('#template-name').val('');
	$('#template-description').val('');
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function format_xml(str){
    var xml = '';

    // add newlines
    str = str.replace(/(>)(<)(\/*)/g,"$1\r$2$3");

    // add indents
    var pad = 0;
    var indent;
    var node;

    // split the string
    var strArr = str.split("\r");

    // check the various tag states
    for (var i = 0; i < strArr.length; i++) {
            indent = 0;
            node = strArr[i];

            if(node.match(/.+<\/\w[^>]*>$/)){ //open and closing in the same line
                    indent = 0;
            } else if(node.match(/^<\/\w/)){ // closing tag
                    if (pad > 0){pad -= 1;}
            } else if (node.match(/^<\w[^>]*[^\/]>.*$/)){ //opening tag
                    indent = 1;
            } else
                    indent = 0;
            //}

            xml += spaces(pad) + node + "\r";
            pad += indent;
    }

    return xml;
}

function count_down_character_by_id(countId, contentId, limit){
	$('#' + countId).text(limit-$('#'+ contentId).val().length);

	$('#' + contentId).on('keyup', function(){
		$('#' + countId).text(limit-$('#' + contentId).val().length);
	});

	var contentText = document.getElementById(contentId);
	if(contentText) {
	    contentText.addEventListener("input", function() {
	        if (contentText.value.length == limit) {
	        	return false;
	        } 
	        else if (contentText.value.length > limit) {
	        	contentText.value = contentText.value.substring(0, limit);
	        }
	    },  false);
	}

	$('#' + contentId).bind('paste', function(e) {
	    var elem = $(this);
	    setTimeout(function() {
	        var text = elem.val();
	        if (text.length > limit) {
				elem.val(text.substring(0,limit));
	        }
	        $('#' + countId).text(limit-parseInt(elem.val().length));
	    }, 100);
	});
}

function spaces(len){
    var s = '';
    var indent = len*4;
    for (i= 0;i<indent;i++) {s += " ";}
    
    return s;
}
var templateXML;
var maxOrder;
function InitSaveNewTemplateForm(){
	
	var batchId = getUrlParameter('campaignid');
	
	$.ajax({
		url: '/session/sire/models/cfc/campaign.cfc?method=GetTemplateDetailOfCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		type: 'POST',
		dataType: 'JSON',
		data: 
		{	
			inpBatchId: batchId
		},
		async: false,
		beforeSend: function () {
			$("#processingPayment").show();
		},
		success: function(data){
			$("#processingPayment").hide();
			if(data.RXRESULTCODE == 1){
				$('#select-template-category').html('');
				for (var i = 0; i < data.CATEGORIES.length; i++) {
					$('#select-template-category').append('<option value=' + data.CATEGORIES[i].ID + '>' + data.CATEGORIES[i].DISPLAYNAME + "</option>");
				}
				$('#select-template-image').html('');
				for (var j = 0; j < data.IMAGES.length; j++) {
					$('#select-template-image').append('<option value=' + data.IMAGES[j].NAME + ' data-imagesrc="/session/sire/images/template-picker/'+ data.IMAGES[j].NAME +'">' + data.IMAGES[j].NAME + '</option>');
				}

				$('#select-template-image').ddslick({
		         	height: '350px',
		         	width: '100%',
				    selectText: "Select Template Images",
				    onSelected: function (data) {
				        $("#select-template-image").val(data.selectedData.value)
				    }
		    	});
				templateXML = data.XMLCONTROLSTRING;
				var xml=data.XMLCONTROLSTRING;
				// xml = xml.replace(/<br>/g, "");
				// xml = xml.replace(/<ul>/g, "");
				// xml = xml.replace(/<\/ul>/g, "");
				// xml = xml.replace(/<li>/g, "");
				// xml = xml.replace(/<\/li>/g, "");
				// xml = xml.replace(/<pre>/g, "");
				// xml = xml.replace(/<\/pre>/g, "");
				// xml = xml.replace(/&gt;/g, ">");
				// xml = xml.replace(/&lt;/g, "<");
				// xml = xml.replace(/>\s*</g, "><");
				// xml=format_xml(xml);
				// xml = xml.replace(/>/g, "&gt;");
				// xml = xml.replace(/</g, "&lt;");

				$('#template-xml').val(xml)
			}
			else{
				bootbox.dialog({
		         	message: data.MESSAGE,
		         	title: "GET CAMPAIGN DETAIL",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
	    	$('#processingPayment').hide();
	     	bootbox.dialog({
	         	message:'Get campaign detail failed!',
	         	title: "GET CAMPAIGN DETAIL",
	         	buttons: {
	             	success: {
	                 	label: "Ok",
	                 	className: "btn btn-medium btn-success-custom",
	                 	callback: function() {}
	             	}
	         	}
	     	});
	    } 
	});
}


$('#confirm-save-new-template-btn').click(validate_save_new_template);


function validate_save_new_template()
{	if($('#save-as-new-template-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		var templateName = $('#template-name').val();
		var templateCategory = $('#select-template-category').val();
		var templateDescription = $('#template-description').val();
		var templateImage = $('#select-template-image').val();
		var templateOrder = $('#select-template-order').val();
		var batchId = getUrlParameter('campaignid');
		$.ajax({
			url: '/session/cfc/templates.cfc?method=CreateCampaignTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: 
			{	
				INPNAME: templateName,
				INPCATEGORY: templateCategory,
				INPDESC: templateDescription,
				CurrentReviewXMLString: templateXML,
				INPBATCHID: batchId,
				INPIMAGE: templateImage,
				INPORDER: maxOrder+1,
				inpTemplateType: $('#inpTemplateType').val()
			},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(data.RXRESULTCODE == 1){
					$('#AdminSaveNewTemplateModal').modal('hide');
					bootbox.dialog({
			         	message: data.MESSAGE,
			         	title: "SAVE AS NEW TEMPLATE",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
				}
				else{
					bootbox.dialog({
			         	message: data.MESSAGE,
			         	title: "SAVE AS NEW TEMPLATE",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	$('#processingPayment').hide();
		     	bootbox.dialog({
		         	message:'Save new template request failed!',
		         	title: "SAVE AS NEW TEMPLATE",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
		    } 
		});
		
	}
}	

function GetMaxOrderByCategoryGroup(){
	var catGroup = $("#select-template-category").val();
	$.ajax({
		url: '/session/sire/models/cfc/campaign.cfc?method=GetMaxOrderByCategoryGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		type: 'POST',
		dataType: 'JSON',
		data: {inpCategoryGroup: catGroup},
		async: false,
		beforeSend: function () {
			$("#processingPayment").show();
		},
		success: function(data){
			$("#processingPayment").hide();
			if(data.RXRESULTCODE == 1){
				$("#select-template-order").html('');
				// for (var i = 0; i < data.MAXORDER; i++) {
				// 	$("#select-template-order").append('<option value='+ (i+1) + '>' + (i+1) + '</option>');
				// }
				maxOrder = data.MAXORDER;
			}	
				
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
	    	$('#processingPayment').hide();
	     	bootbox.dialog({
	         	message:'Get max order request failed!',
	         	title: "GET TEMPLATE DETAIL",
	         	buttons: {
	             	success: {
	                 	label: "Ok",
	                 	className: "btn btn-medium btn-success-custom",
	                 	callback: function() {}
	             	}
	         	}
	     	});
	    } 
	});
	
}

$("#select-template-category").change(GetMaxOrderByCategoryGroup);



(function($){
	var modal = $('#EditControlString');
	var inpXMLCS = $('#inpRawXMLControlString');
	var campaignid = $('input[type="hidden"][name="campaignid"]').val();


	$('#frmEditXMLCString').validationEngine('attach', {});

	modal.on('shown.bs.modal', function(event){
		$.ajax({
			url: "/session/sire/models/cfm/xmlcs.cfm",
			type: "POST",
			dataType: "json",
			async: false,
			data: {batchID: campaignid},
			success: function(data){
				inpXMLCS.val(data.XMLCONTROLSTRING);
			},
			complete: function(){

			}
		});
	});


	$('#frmEditXMLCString').submit(function(event){

		event.preventDefault();
		if($(this).validationEngine('validate')){
			bootbox.dialog({
	         	message:'Are you sure you want to update XML Control String ?',
	         	title: "Update XML Control String",
	         	buttons: {
	             	success: {
	                 	label: "Ok",
	                 	className: "btn btn-medium btn-success-custom",
	                 	callback: function() {
	                 		$.ajax({
								url: "/session/sire/models/cfm/xmlcs.cfm?action=saveXMLCS",
								type: "POST",
								dataType: "json",
								data: {batchID: campaignid, xmlcs: inpXMLCS.val()},
								success: function(data){
									if(data.RXRESULTCODE == 1){

										bootbox.dialog({
								         	message:'Update success!',
								         	title: "Edit XML Control String",
								         	buttons: {
								             	success: {
								                 	label: "Ok",
								                 	className: "btn btn-medium btn-success-custom",
								                 	callback: function() {
								                 		modal.modal('hide');
								                 		window.location.reload();
								                 	}
								             	}
								         	}
								     	});


										
									}
								}
							});		
	                 	}
	             	},
	             	cancel: {
	                 	label: "Cancel",
	                 	className: "btn btn-medium btn-back-custom"
	             	}
	         	}
	     	});
		}
	});
})(jQuery);


// $(document).ready(function() {
// 		$(".page-sidebar-menu").find('li.active').removeClass('active');
// 		$('.simon-parent-li').addClass('open');
// 		$('.simon-parent-li').find('ul.sub-menu').attr("style", "display: block;");
// 		$('.simon-menu-li').addClass('active');
// });