(function($){
	var modal 			 = $('#ScheduleOptionsModal');
	var selectCampgBox   = $('select[name="campaign-id"]');
	var btnShowScheduler = $(".btn-show-scheduler");
	var createText         = $('.text-create-hidden');

	var alertBox = $('<div style="margin-top: 10px; margin-bottom: 0px;" class="alert alert-danger alert-box">'+
		'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
	  	'<strong>Notice!</strong> This campaign\'s schedule has not set.'+
	'</div>');



	var defaultScheduleTime = {
		startDate: "2017/2/1",
		stopDate: "2017/3/3",
		startHour:9,
		startMinute:0,
		endHour:19,
		endMinute:0,
		enabledBlackout:false,
		blackoutStartHour:0,
		blackoutEndHour:0,
		dayId:1,
		scheduleType:2
	};

	btnShowScheduler.on('click', function(event){
		event.preventDefault();
		modal.modal('show');
	});


	selectCampgBox.on('change', function(event){
		var campaignID = $(this).val();
		INPBATCHID = campaignID;


		if(campaignID > 0){
			BindScheduleDataEdit(INPBATCHID);
			btnShowScheduler.removeClass('hidden');
		} else {
			btnShowScheduler.addClass('hidden');
		}

	});


	// $('.time-picker').timepicker({
	// 	template: 'dropdown',
	// 	minuteStep: 5,
	// 	secondStep: 20
	// }).on('changeTime.timepicker', function(e) {
	// 	console.log(this);
	// 	console.log(e.time);
 //  	});;




  	// $('input.date-picker').daterangepicker({
  	// 	"singleDatePicker": true,
  	// }).on('apply.daterangepicker', function(e){
  	// 	console.log(this.name);
  	// });



	function BindScheduleDataEdit(batchID) {

		$.ajax({
			url : '/session/sire/models/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: "POST",
			dataType: "json",
			async: false,
			data : { 
				INPBATCHID : batchID, 
			},
			beforeSend: function(){
				$('#processingPayment').show();
			}
			
		}).done(function(d){
			$('#processingPayment').hide();
			var data = d.DATA;

			if (data.RXRESULTCODE[0] == 1) {
				var dataRows = data.ROWS[0];
				
				if (dataRows.length > 0) {
					/*console.log(dataRows[0]);*/
					var scheduleType = dataRows[0].SCHEDULETYPE_INT;
					$('#rdoSchedultType_' + scheduleType).attr('checked', true);
					
					if (scheduleType == 1 || scheduleType == 0) {
						
						$('#FollowingTimeDate').val(dataRows[0].START_DT);
						SelectDate(document.getElementById('FollowingTimeDate'));

						$('#FollowingTimeEndDate').val(dataRows[0].STOP_DT);
						SelectDate(document.getElementById('FollowingTimeEndDate'));

						$('#cboTimeHour_FollowingTime').val(parseInt(dataRows[0].STARTHOUR_TI) % 12);

						$('#FollowingTimeStart_minute').val(dataRows[0].STARTMINUTE_TI)
						$('#FollowingTimeStart_hour').val(Math.floor(parseInt(dataRows[0].STARTHOUR_TI) % 12));
						if(dataRows[0].STARTHOUR_TI > 11){
							$("#FollowingTimeStart_noon_option").val("1");
						}else{
							$("#FollowingTimeStart_noon_option").val("0");
						}

						$('#FollowingTimeEnd_minute').val(dataRows[0].ENDMINUTE_TI)
						$('#FollowingTimeEnd_hour').val(Math.floor(parseInt(dataRows[0].ENDHOUR_TI) % 12));
						if(dataRows[0].ENDHOUR_TI > 11){
							$("#FollowingTimeEnd_noon_option").val("1");
						}else{
							$("#FollowingTimeEnd_noon_option").val("0");
						}
						
					}
					else if (scheduleType == 2) {
						var schedule = dataRows[0];

						$('#AcrossMultipleDaysStartDate').val(schedule.START_DT);
						SelectDate(document.getElementById('AcrossMultipleDaysStartDate'));
						$('#AcrossMultipleDaysEndDate').val(schedule.STOP_DT);
						SelectDate(document.getElementById('AcrossMultipleDaysEndDate'));

						$('#cboTimeZone_AcrossMultipleDays').val(schedule.TIMEZONE_VCH);
						
						if (dataRows.length == 1 & GetDayIdBySchedule(schedule) == 0) {

							$('#FollowingTimeStart_minute').val(schedule.STARTMINUTE_TI)
							$('#FollowingTimeStart_hour').val(Math.floor(parseInt(schedule.STARTHOUR_TI) % 12));
							if(schedule.STARTHOUR_TI > 11){
								$("#FollowingTimeStart_noon_option").val("1");
							}else{
								$("#FollowingTimeStart_noon_option").val("0");
							}

							$('#FollowingTimeEnd_minute').val(schedule.ENDMINUTE_TI)
							$('#FollowingTimeEnd_hour').val(Math.floor(parseInt(schedule.ENDHOUR_TI) % 12));
							if(schedule.ENDHOUR_TI > 11){
								$("#FollowingTimeEnd_noon_option").val("1");
							}else{
								$("#FollowingTimeEnd_noon_option").val("0");
							}

							$('.check_all').attr('checked', true);
							BindScheduleOnTab(0, schedule)
						}
						else {
							for (var i = 0; i < dataRows.length; ++i) {
								schedule = dataRows[i];
								var dayId = GetDayIdBySchedule(schedule);
								BindScheduleOnTab(dayId, schedule)
							}
							
							$('.check_all').attr('checked', false);
						}
						DoCheckAll($('.check_all'));
					}
				}
				alertBox.addClass('hidden');
				createText.addClass('hidden');
			} else {

				$.each($('.hour'), function(index, value) {
					CreateHourList(this);
				});
				
				$.each($('.minute'), function(index, value) {
					CreateMinuteList(this);
				});		
				
				$.each($('.month'), function(index, value) {
					CreateMonthList(this);
				});

				$.each($('.day'), function(index, value) {
					CreateDayList(this);
				});
				
				$.each($('.year'), function(index, value) {
					CreateYearList(this);
				});
				
				$.each($('.noon_list'), function(index, value) {
					CreateNoonList(this);
				});
				
				CreateDatePicker('FollowingTimeDate');
				CreateDatePicker('FollowingTimeEndDate',true);
				CreateDatePicker('AcrossMultipleDaysStartDate');
				CreateDatePicker('AcrossMultipleDaysEndDate', true);

				if($('.alert-box').length){
					$('.alert-box').removeClass('hidden');
					createText.removeClass('hidden');
				} else {
					$('a.btn-show-scheduler').before(alertBox.removeClass('hidden'));
				}
				
			}
		});
	}

	$("#ScheduleOptionsModal").on('hide.bs.modal', function(event){

		if($('.alert-box').length && !$('.alert-box').hasClass('hidden')){
			$('.alert-box').addClass('hidden');
		}
	});


})(jQuery);