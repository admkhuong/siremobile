(function($){
	
	$("#form-add-link").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : false});
	$("#form-sms").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : false});
	countText('#help-msg-block',$('#txtSMS').val());


	$('input[type=radio][name=optionsSubscriberList]').change(function() {
    	var total_subcribers = $(this).data('total-subcribers');
    	var subcribers_txt = 'subcriber';
    	if(total_subcribers > 1) subcribers_txt+='s';
    	$(".total_num_subcribers").html(total_subcribers+' '+subcribers_txt);
	});

	 $("#txtSMS").bind('keyup change', function(e) {
	 	var msg_txt = $(this).val();
	 	if(msg_txt!='')
	 	{
	 		$('.bubble').show();
	 		$('#SMSHistoryScreenArea .you').text(msg_txt);
	 		//$('#characters_available1').html(msg_txt.length);
	 	}
	 	countTextSMS('#help-msg-block',$('#txtSMS').val());
    });

	if(mode=='edit'){
		$("#txtSMS").keyup();
		$('#SMSHistoryScreenArea .you').text($("#txtSMS").val());
        $('.bubble').show();
      
	}
		 
	$('#opt-out-checkbox').change(function() {
        if($(this).is(":checked")) {
        	var msg_txt = $("#txtSMS").val();
        	var opt_out_txt = $("#opt-out-txt").val();
        	//$('#SMSHistoryScreenArea .you').text(msg_txt+opt_out_txt);
        	//$("#txtSMS").val(msg_txt+opt_out_txt);
        	//$('.bubble').show();
        	//$("#txtSMS").keyup();
        	wrapValueForm(opt_out_txt);
        }
    });

	 /*
	$(".btn-save-for-review").click(function(event){
		
	});
	*/
	$(document).on("click",".btn-save-for-review",function(event) {
		event.preventDefault();
		AddNewEmergency(true);	
    });

	$(".btn-lunch").click(function(event){
		event.preventDefault();
		if ($('#form-sms').validationEngine('validate')){
			var selectSucriberList = $('input[name=optionsSubscriberList]:checked').val();
			if(selectSucriberList){
				OpenEMSConfirmPopup();	
			}
			else{
				bootbox.alert('Please select at least one subscriber List.', function() {});
			}
			
		}
	}); 

	$(document).on("click",".btn-update-for-review",function(event) {
		event.preventDefault();
		UpdateEmergency(true);	
    });

    $(document).on("click",".btn-update-lunch",function(event) {
		event.preventDefault();
		UpdateEmergency(false);
    });



	function wrapValueForm(text) {
	    var textArea = $("#txtSMS"),
	    len = textArea.val().length,
	    start = textArea[0].selectionStart,
	    end = textArea[0].selectionEnd,
	    selectedText = textArea.val().substring(start, end);
	    textArea.val(textArea.val().substring(0, start) + text + textArea.val().substring(end, len));

	    var maxLength = textArea.attr('maxlength');  
      
        //if (textArea.val().length > maxLength) {  

        //	bootbox.alert("Text messages are limited to 160 characters",function(result){});
        //    textArea.val(textArea.val().substring(0, maxLength));  
        //}  

	    $("#txtSMS").keyup();
	}

	
	$("#btn-add-link").click(function(event){
        event.preventDefault();
    	try{
            event.preventDefault();
            var link = $("#tblLink").val();
            if ($('#form-add-link').validationEngine('validate')){
            	$('.btn').prop('disabled',true);
            	$.ajax({
                    type: "GET",
                    url: "/session/sire/models/cfc/optin.cfc?method=getShortLink&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
                    dataType: 'json',
                    data: {
                        longUrl: link
                    },
                    beforeSend: function(){
						$('#processingPayment').show();
					},
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                    	$('.btn').prop('disabled',false);
                    	$('#processingPayment').hide();
                	},
                    success: function(data) {
                    	$('#processingPayment').hide();
                        var filecontent = jQuery.parseJSON(data.filecontent);
                        if (filecontent.status_code == 200) {
                            wrapValueForm(filecontent.data.url);
                        }else if (filecontent.status_code == 500){
				        	wrapValueForm(id,link);
				        }
				       $("#AddLinkModal").modal('hide');
				       $('.btn').prop('disabled',false);
                    }
            	});
        	}
        }
		catch(ex){
			$('#processingPayment').hide();
			bootbox.alert('Error. Please try again', function() {});
			$('.btn').prop('disabled',false);
		}
    });

	$("#btn-add-field").click(function(event){
		event.preventDefault();
		var msg_txt = $("#txtSMS").val();
		var field_val = $('input[name=optradio]:checked', '#form-add-field').val();
        wrapValueForm(field_val);
        $("#AddFieldModal").modal('hide');
     });

	$('#ConfirmLaunchModal').on('hidden.bs.modal', function(e) {
    	$(e.target).removeData('bs.modal').find('.modal-content').empty();
	});
	

})(jQuery);

var saveOnly = false;
var update_saveOnly = false;
var totalMessage = 1;

function characterCountSMS(text) {
		var count = text.length;
		var message = 'Character Count {' + count + '}';
		var nmessage = 0;
		
		if (count > 160) {
			if ((count % 153) == 0) {
			  nmessage = (count / 153)
			} else {
			  nmessage = Math.floor(count / 153) + 1;
			}
		}
		if (nmessage > 1 ) {
			totalMessage = nmessage;
			message = message + ' <span style="color:#FF0000">' + nmessage + ' Increments</span>'
		}

		
		return message;
	}

function countTextSMS(id, text) {
	var message = characterCountSMS(text);
	$(id).html(message);
}

//this function return contact type string
function BuildContactType(isBitFormat){
	var contactTypes;
	if(isBitFormat){
		//build contact types for using in bitwise "AND" operator later, it could be 001,010,100,etc
		contactTypes = 4;
	}
	else{
		
		//build contact Types as list format. For example: 1,2,3 etc
		contactTypes = "3,";
	}
	return contactTypes;
}

function OpenEMSConfirmPopup(){
	var INPGROUPID = $('input[name=optionsSubscriberList]:checked').val();
	var INPSHORTCODE = SHORTCODE;
	var CONTACTTYPES = BuildContactType(false);
	var CDFDATA = JSON.stringify(new Array());;
	var CUSTOMSTRING = "";

	//alert(totalMessage);
				
	var options = {
		show: true,
		"remote" : '/session/sire/pages/dsp_confirm_launch_ems.cfm?INPGROUPID='+INPGROUPID+"&INPSHORTCODE="+INPSHORTCODE+"&CONTACTTYPES="+CONTACTTYPES+"&CDFDATA="+CDFDATA+"&CUSTOMSTRING="+CUSTOMSTRING+"&TOTALMESSAGE="+parseInt(totalMessage)
	}

	$('#ConfirmLaunchModal').modal(options);	
}


//this function should be used to create new EMS
function CreateNewEMS(urlRecordFile, libId, eleId, scrId)
{
	//if user dont choose voice method, pass caller id as empty string to server
	var DefaultCIDVch = "";

    //SMSCONTENT
	var SMSCONTENT = {
		MESSAGE: $("#txtSMS")?$("#txtSMS").val():"",
		SHORTCODE: SHORTCODE
	};

	//VOICECONTENT
	var VOICECONTENT = {
		LIBID : typeof(libId)!= "undefined"?libId:"",
		ELEID : typeof(eleId)!= "undefined"?eleId:"",
		SCRID : typeof(scrId)!= "undefined"?scrId:"",
		URLRECORDFILE:typeof(urlRecordFile)!= "undefined"?urlRecordFile:"" 
	};
	
	//filter data, include contact filter string and cdf filter rows data
	var filterData = {
		CONTACTFILTER: "",
		//CDFFILTER: BuildCDFFilterArray()//this function is defined in dsp_contact_preview which return an array of cdf filter object
		CDFFILTER: new Array()
	}
	// Enable Immediate Delivery 
	var INPENABLEIMMEDIATEDELIVERY = 0;
	var schedule_time_data = "";

	if(typeof ScheduleTimeData != "undefined")
	{
		INPENABLEIMMEDIATEDELIVERY = 2;
		schedule_time_data = JSON.stringify(ScheduleTimeData);
	}
	
	//Build parameters for call to emergencymessages.cfc AddNewEmergency method
	var dataValue = {
		INPBATCHDESC: $("#campaign-name-txt").val(),
		INPXMLCONTROLSTRING : "",
		INPGROUPID : $('input[name=optionsSubscriberList]:checked').val(),//$("#inpGroupPickerId").val(),
		SCHEDULETYPE_INT : "",
		START_DT : "",
		STOP_DT : "",
		STARTHOUR_TI : "",
		ENDHOUR_TI : "",
		STARTMINUTE_TI : "",
		ENDMINUTE_TI : "",
		DAYID_INT : "",
		DefaultCIDVch : DefaultCIDVch,
		CONTACTTYPES : BuildContactType(false),
		CONTACTTYPES_BITFORMAT : BuildContactType(true),
		EMAILCONTENT: "",
		SMSCONTENT:JSON.stringify(SMSCONTENT),
		VOICECONTENT:JSON.stringify(VOICECONTENT),
		INPENABLEIMMEDIATEDELIVERY: INPENABLEIMMEDIATEDELIVERY,
		SAVEONLY: saveOnly,
		LOOPLIMIT_INT : $('input[name=LOOPLIMIT_INT]').val(),
		SCHEDULETIMEDATA: schedule_time_data  ,
		ELIGIBLECOUNT:$("#ELIGIBLECOUNT").val(),	
		UNITCOUNT:$("#UNITCOUNT").val(),	
		DNCCOUNT:$("#DNCCOUNT").val(),	
		DUPLICATECOUNT:$("#DUPLICATECOUNT").val(),	
		INVALIDTIMEZONECOUNT:$("#INVALIDTIMEZONECOUNT").val(),	
		COSTCURRENTQUEUE:$("#COSTCURRENTQUEUE").val(),	
		CURRENTBALANCE:$("#CURRENTBALANCE").val(),				
		CONTACTFILTER:JSON.stringify(filterData),
		IVRBatchId: $("#IVRBatchId").val()
	};

	console.log(dataValue);
	//return false;

	$('.btn').prop('disabled',true);
	
	try{
		$.ajax({
	        type: "POST",
	        url: '/session/sire/models/cfc/sms.cfc?method=AddNewEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	        //url : '/session/cfc/emergencymessages.cfc?method=AddNewEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	        data: dataValue,
	        async: false,
			dataType:"json",
			beforeSend: function(){
				$('#processingPayment').show();
			},
	        success: function(dStruct ) {
				// Check if variable is part of JSON result string
				$('#processingPayment').hide();
				if(dStruct.RXRESULTCODE > 0)
				{
					
					if(saveOnly == "true" || saveOnly == true)
					{
						
						bootbox.alert("Success!<BR><BR>EMS Campaign Created OK!",function(result){
							window.location.href="/session/sire/pages";
						});
						
					}
					else
					{
						$("#ConfirmLaunchModal").modal('hide');
						$('.btn').prop('disabled',false);
						bootbox.alert("Success!<BR><BR>EMS Campaign Created And Launched OK!",function(result){
							window.location.href="/session/sire/pages";
						});
					}
					return false;
				}
				else
				{						
					bootbox.alert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE + " - Failure!", function(result) {  } );	
					$('.btn').prop('disabled',false);						
				}

				
	        },
			error: function(jq,status,message) {
				$('#processingPayment').hide();
		        bootbox.alert('An error has occurred. Status: ' + status + ' - Message: ' + message,function() {});
		        $('.btn').prop('disabled',false);
		    }
		});
	}catch(ex){
		$('#processingPayment').hide();
		bootbox.alert('Error. Please try again', function() {});
		$('.btn').prop('disabled',false);
	}
}

 //function add new emergency
function AddNewEmergency(review){
	if ($('#form-sms').validationEngine('validate')){

		var selectSucriberList = $('input[name=optionsSubscriberList]:checked').val();
		if(selectSucriberList){
			//If review is true, System has been saved only(not launch)
			saveOnly = review;
			CreateNewEMS();
		}
		else{
			bootbox.alert('Please select at least one subscriber List.', function() {});
		}
		return false;
	}
}

function UpdateEmergency(saveOnly)
{
	if ($('#form-sms').validationEngine('validate')){
		var selectSucriberList = $('input[name=optionsSubscriberList]:checked').val();
		if(selectSucriberList){
			//validate data
			update_saveOnly = saveOnly;
			UpdateEmergencyMesage(saveOnly);
		}
		else{
			bootbox.alert('Please select at least one subscriber List.', function() {});
		}
	}	
		
}
		
//this function should be used to update new emergency
function UpdateEmergencyMesage(saveOnly, urlRecordFile, libId, eleId, scrId){
	
	//if user dont choose voice method, pass caller id as empty string to server
	var DefaultCIDVch = "";
	/*
	//EMAILCONTENT
	var EMAILCONTENT = {
		FROM : $("#txtEmail")?$("#txtEmail").val():"",
		SUBJECT :$("#txtSubject")?$("#txtSubject").val():"",
		CONTENT:tinymce.EditorManager.get("txtContent")?tinymce.EditorManager.get("txtContent").getContent():""
	};
	*/
	
   //SMSCONTENT
	var SMSCONTENT = {
		MESSAGE: $("#txtSMS")?$("#txtSMS").val():"",
		SHORTCODE: SHORTCODE
	};

	//VOICECONTENT
	var VOICECONTENT = {
		LIBID : typeof(libId)!= "undefined"?libId:"",
		ELEID : typeof(eleId)!= "undefined"?eleId:"",
		SCRID : typeof(scrId)!= "undefined"?scrId:"",
		URLRECORDFILE:typeof(urlRecordFile)!= "undefined"?urlRecordFile:"" 
	};

	// Enable Immediate Delivery 
	var INPENABLEIMMEDIATEDELIVERY = 0;
	var schedule_time_data = "";
	
	if(typeof ScheduleTimeData != "undefined")
	{
		INPENABLEIMMEDIATEDELIVERY = 2;
		schedule_time_data = JSON.stringify(ScheduleTimeData);
	}
	
	// Build parameters for call to emergencymessages.cfc AddNewEmergency method  --->
	
	//filter data, include contact filter string and cdf filter rows data
	//filter data, include contact filter string and cdf filter rows data
	var filterData = {
		CONTACTFILTER: "",
		//CDFFILTER: BuildCDFFilterArray()//this function is defined in dsp_contact_preview which return an array of cdf filter object
		CDFFILTER: new Array()
	}
	
	var dataValue = {
		BATCHID: $('#IVRBatchId').val(),
		INPBATCHDESC: $("#campaign-name-txt").val(),
		INPXMLCONTROLSTRING : "",
		INPGROUPID : $('input[name=optionsSubscriberList]:checked').val(), //$("#inpGroupPickerId").val(),
		SCHEDULETYPE_INT : "",
		START_DT : "",
		STOP_DT : "",
		STARTHOUR_TI : "",
		ENDHOUR_TI : "",
		STARTMINUTE_TI : "",
		ENDMINUTE_TI : "",
		DAYID_INT : "",
		DefaultCIDVch : DefaultCIDVch,
		CONTACTTYPES : BuildContactType(false),
		CONTACTTYPES_BITFORMAT : BuildContactType(true),
		EMAILCONTENT: "",
		SMSCONTENT:JSON.stringify(SMSCONTENT),
		VOICECONTENT:JSON.stringify(VOICECONTENT),
		INPENABLEIMMEDIATEDELIVERY: INPENABLEIMMEDIATEDELIVERY,
		SAVEONLY: saveOnly,			
		LOOPLIMIT_INT : $('input[name=LOOPLIMIT_INT]').val(),
		SCHEDULETIMEDATA: schedule_time_data,
		ELIGIBLECOUNT:$("#ELIGIBLECOUNT").val(),	
		UNITCOUNT:$("#UNITCOUNT").val(),	
		DNCCOUNT:$("#DNCCOUNT").val(),	
		DUPLICATECOUNT:$("#DUPLICATECOUNT").val(),	
		INVALIDTIMEZONECOUNT:$("#INVALIDTIMEZONECOUNT").val(),	
		COSTCURRENTQUEUE:$("#COSTCURRENTQUEUE").val(),	
		CURRENTBALANCE:$("#CURRENTBALANCE").val(),
		CONTACTFILTER:JSON.stringify(filterData)
	};
	
	console.log(dataValue);
	//return false;

	$('.btn').prop('disabled',true);

	try{
		$.ajax({
	        type: "POST",
	        url: '/session/sire/models/cfc/sms.cfc?method=UpdateEmergency&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	        data: dataValue,
	        async: false,
			dataType:"json",
			beforeSend: function(){
				$('#processingPayment').show();
			},
	        success: function(dStruct ) {
				// Check if variable is part of JSON result string	
				$('#processingPayment').hide();
				if(dStruct.RXRESULTCODE > 0)
				{
											
					if(saveOnly == "true" || saveOnly == true){
						bootbox.alert("EMS Campaign Updated OK!", function(result) {
							window.location.href="/session/sire/pages";
						});
					}else{
						bootbox.alert("EMS Campaign Updated And Launched OK!", function(result) {
							window.location.href="/session/sire/pages";
						});
					}
					return false;
				}
				else
				{
					
					bootbox.alert("\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE + " Failure!", function(result) { } );							
				}

				$('.btn').prop('disabled',false);
	        },
			error: function(jq,status,message) {
				$('#processingPayment').hide();
		        bootbox.alert('An general errorhas occurred. Status: ' + status + ' - Message: ' + message);
		    	$('.btn').prop('disabled',false);
		    }
		});
	}
	catch(ex){
		$('#processingPayment').hide();
		bootbox.alert('Error. Please try again', function() {});
		$('.btn').prop('disabled',false);
	}
}