(function($){
	
	if($('#custom-help-message').length > 0){
		countText('#help-block',$('#custom-help-message').val());	
	}
	
	if($('#custom-stop-message').length > 0){
    	countText('#stop-block',$('#custom-stop-message').val());
	}

	$("#sms_number").mask("(000)000-0000");
	$("#OrganizationPhone_vch").mask("(000)000-0000");

	var check_update = 0;

	var hasUploadLogo = 0;

	$("#add_security_question_form").validationEngine({promptPosition : "topLeft", scroll: false, focusFirstField : true, onFailure: function(){
		window.scrollTop();
	}});

	$("#my_account_form").validationEngine({promptPosition : "topLeft", scroll: false, focusFirstField : true, onFailure: function(){
		window.scrollTop();
	}});

	$("#btn_save_my_account").click(updatemyaccountinfo);
	$("#my_account_form").submit(updatemyaccountinfo);

	$("#select_question_1").on('focus click', function(){
		$('#input_answer_1').validationEngine('hide');
	});

	$("#select_question_2").on('focus click', function(){
		$('#input_answer_2').validationEngine('hide');
	});

	$("#select_question_3").on('focus click', function(){
		$('#input_answer_3').validationEngine('hide');
	});

	$("#btn_update_security_question").click(addSecurityQuestion);

	$("#add_security_question_form").submit(addSecurityQuestion);

	$("#edit_security_question_form").submit(editSecurityQuestion);

	var fields = $('#my_account_form input, #my_account_form select');
	
	fields.focus(function(){
		$(this).validationEngine('hide');
		var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
		setTimeout(function(){
			nextField.validationEngine('hide');
		},1);
	});

	$('#SecurityQuestionEnabled').change(function() {
		return false;
        if($(this).is(":checked")) {
           var check_value = 1
           //$('.link-to-edit').show();
        }
        else{
        	var check_value = 0
        	//$('.link-to-edit').hide();
        }

        try{
			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateSecurityQuestionStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data: {SecurityQuestionEnabled:check_value},
			beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				//bootbox.alert('Update Fail', function() {});
				//$("#enable_security_question_form")[0].reset();
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: "Update Fail",
				    title: "My Account",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			},					  
			success:		
				function(d) {
					$('#processingPayment').hide();
					$('.btn-group-answer-security-question').show();
					
					bootbox.dialog({
					    message: d.MESSAGE,
					    title: "My Account",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
				} 		
			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.dialog({
			    message:'Update Fail',
			    title: "My Account",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
			$('.btn-group-answer-security-question').show();
			//$("#enable_security_question_form")[0].reset();			
		}

    });

	$(".select_question").on('change', function() {
		var old_selected = $(this).data('selected');
		var input_answer =  $(this).data('input-answer');
	    if ($(this).val() == old_selected){
	        check_update = false;
	        $('#'+input_answer).removeClass('validate[required]');
	    } else {
	        check_update = true;
	        // add validate for input box
	        $('#'+input_answer).addClass('validate[required]');
	    }
	});

	$('.input_answer').blur(function(){
	    if( $(this).val().length > 0 ) {
	        check_update = true;
	    }
	    else
	    	check_update = false;
	});


	function addSecurityQuestion(event){
		event.preventDefault();
		
		if ($('#add_security_question_form').validationEngine('validate', {focusFirstField : true} )) {

			try{
				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/myaccountinfo.cfc?method=AddUserSecurityQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: $('#add_security_question_form').serialize(),
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#processingPayment').hide();
					bootbox.dialog({
					    message:'Update Fail',
					    title: "My Account",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
				},					  
				success:		
					function(d) {
						$('#processingPayment').hide();
						$('.btn-group-answer-security-question').show();
						if(d.RESULT == "SUCCESS"){
							//goto success page
							/*bootbox.alert(d.MESSAGE, function() {
								window.location.href='/session/sire/pages/my-account';
							});*/
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {window.location.href='/session/sire/pages/my-account';}
							        }
							    }
							});
						}
						else{
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
						}
					} 		
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message:'Update Fail',
				    title: "My Account",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});	
				$('.btn-group-answer-security-question').show();			
			}
		}
	}

	function editSecurityQuestion(event){
		event.preventDefault();

		if(!check_update){
			//bootbox.alert('Nothing changed.', function() {});
			bootbox.dialog({
			    message:'Nothing changed.',
			    title: "My Account",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});	
			return false;
		}
		else{
			$("#edit_security_question_form").validationEngine('attach',{'binded':false});	
		}


		if ($('#edit_security_question_form').validationEngine('validate')) {
			try{
				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/myaccountinfo.cfc?method=EditUserSecurityQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: $('#edit_security_question_form').serialize(),
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#processingPayment').hide();
					bootbox.dialog({
					    message:'Update Fail',
					    title: "My Account",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});	
				},					  
				success:		
					function(d) {
						$('#processingPayment').hide();
						$('.btn-group-answer-security-question').show();
						if(d.RESULT == "SUCCESS"){
							//goto success page
							/*bootbox.alert(d.MESSAGE, function() {
								window.location.href='/session/sire/pages/my-account';
							});*/
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {window.location.href='/session/sire/pages/my-account';}
							        }
							    }
							});	
						}
						else{
							//bootbox.alert(d.MESSAGE, function() {});	
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
						}
					} 		
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message:'Update Fail',
				    title: "My Account",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});	
				$('.btn-group-answer-security-question').show();			
			}
		}
	}

	function updatemyaccountinfo(event){
		event.preventDefault();
		if ($('#my_account_form').validationEngine('validate')) {
			try{
				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateUserSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: $('#my_account_form').serialize(),
				beforeSend: function( xhr ) {
					$('.btn').prop('disabled',true);
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					//bootbox.alert('Update Fail', function() {});
					$('.btn').prop('disabled',false);
					$('#processingPayment').hide();
					bootbox.dialog({
					    message:'Unable to update your profile settings at this time. An error occurred.',
					    title: "My Account",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});	
				},					  
				success:		
					function(d) {
						//$('#btn_save_my_account').prop('disabled',false);
						$('.btn').prop('disabled',false);
						$('#processingPayment').hide();

						if(d.RESULT == "SUCCESS"){
							//goto success page
							/*bootbox.alert(d.MESSAGE, function() {
								window.location.href='/session/sire/pages/my-account';
							});*/
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {window.location.href='/session/sire/pages/my-account';}
							        }
							    }
							});	
						}
						else{
							//bootbox.alert(d.MESSAGE, function() {});	
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
						}
					} 		
				});
			}catch(ex){
				//bootbox.alert('Update Fail', function() {});
				$('.btn').prop('disabled',false);
				$('#processingPayment').hide();
				bootbox.dialog({
				    message:'Unable to update your profile settings at this time. An error occurred.',
				    title: "My Account",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});	
			}
		}
	}

var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
	url: "/session/sire/models/cfc/upload.cfc?method=uploadImg&queryformat=column&_cf_nodebug=true&_cf_nocache=true", // Set the url
	paramName: "image",
	thumbnailWidth: 200,
	thumbnailHeight: 200,
	parallelUploads: 1,
	maxFilesize: 10,
	filesizeBase: 1024,
	previewTemplate: previewTemplate,
	autoQueue: true, // Make sure the files aren't queued until manually added
	previewsContainer: "#previews", // Define the container to display the previews
	clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
	acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg",
	maxFiles: 1,
	init: function() {
	  this.on('addedfile', function(file) {
	    if (this.files.length > 1) {
	      this.removeFile(this.files[0]);
	    }
	  });
	}
});

myDropzone.on("sending", function(file) {
	$('#processingPayment').show();
});

myDropzone.on("success", function (file, result) {
	$('#processingPayment').hide();
	myDropzone.removeFile(file);
	var obj = jQuery.parseJSON(result);
	if(obj.RESULT == 'SUCCESS'){
		$('#OrganizationLogo_vch').val(obj.IMAGE);
		$('#OrgLogo').attr('src',obj.IMAGE_URL);
		$('#OrgLogo').show();
		$('#OrgLogo').parent().show();
		$('#UpdateOrganizationLogo').val(1);
		$('#removeLogo').show();
		$('.OrgLogo-wrapper').show();
		hasUploadLogo = 1;
	}
	else{
		bootbox.dialog({
		    message:obj.MESSAGE,
		    title: "My Account",
		    buttons: {
		        success: {
		            label: "Ok",
		            className: "btn btn-medium btn-success-custom",
		            callback: function() {}
		        }
		    }
		});
	}
});
	
	// if($('#imageUpload').length > 0)
	// {
	// 	//UPLOAD LOGO
	// 	new AjaxUpload('imageUpload', {
	// 		action: '/session/sire/models/cfc/upload.cfc?method=uploadImg&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	// 		name: 'image',
	// 		responseType: 'json',
	// 		onSubmit: function(file, extension) {

	// 			if(!isImage(file))
	// 			{
	// 				//bootbox.alert('Invalid image file.', function() {});
	// 				bootbox.dialog({
	// 				    message:'Invalid support file. Only accept jpg, jpge, gif, bmp and png files.',
	// 				    title: "My Account",
	// 				    buttons: {
	// 				        success: {
	// 				            label: "Ok",
	// 				            className: "btn btn-medium btn-success-custom",
	// 				            callback: function() {}
	// 				        }
	// 				    }
	// 				});	
	// 				return false;
	// 			}
	// 			//if( GetFileSize("imageUpload") > 4000000)	
	// 			//{
	// 			//	bootbox.alert('The maximum size of avarta image is 4MB', function() {});
	// 			//	return false;
	// 			//}	
	// 			$('.btn').prop('disabled', true);
	// 			$("#imageUpload").html('<strong>Uploading</strong>');
	// 			$('#processingPayment').show();
	// 			$('#UpdateOrganizationLogo').val(0);
	// 			$('#removeLogo').hide();
	// 		},
	// 		onComplete: function(file, response) {
	// 			$('.btn').prop('disabled', false);
	// 			$("#imageUpload").html('<strong>Upload Logo</strong>');
	// 			$('#processingPayment').hide();

	// 			var obj = response;

	// 			if(obj.RESULT == 'SUCCESS')
	// 			{
	// 				$('#OrganizationLogo_vch').val(obj.IMAGE);
	// 				$('#OrgLogo').attr('src',obj.IMAGE_URL);
	// 				$('#OrgLogo').show();
	// 				$('#OrgLogo').parent().show();
	// 				$('#UpdateOrganizationLogo').val(1);
	// 				$('#removeLogo').show();
	// 				$('.OrgLogo-wrapper').show();
	// 				hasUploadLogo = 1;
	// 			}
	// 			else
	// 			{
	// 				//bootbox.alert(obj.MESSAGE, function() {});	
	// 				bootbox.dialog({
	// 				    message:obj.MESSAGE,
	// 				    title: "My Account",
	// 				    buttons: {
	// 				        success: {
	// 				            label: "Ok",
	// 				            className: "btn btn-medium btn-success-custom",
	// 				            callback: function() {}
	// 				        }
	// 				    }
	// 				});	
	// 			}
	// 		}
	// 	});
	// }

	$('#removeLogo').on('click', function(e){
		e.preventDefault();
		bootbox.dialog({
		    message:'Are you sure you want to remove your organization photo ?',
		    title: "My Profile",
		    buttons: {
		        success: {
		            label: "Confirm",
		            className: "btn btn-medium btn-success-custom",
		            callback: function() {
		            	$('#OrgLogo').attr('src','');
		            	$(this).hide();
		            	//removeLogo();
						$('#UpdateOrganizationLogo').val(1);
						$('#OrganizationLogo_vch').val('');
						$('.OrgLogo-wrapper').hide();
						$('#removeLogo').hide();
		            }
		        },
		         danger: {
			      label: "Cancel",
			      className: "btn btn-primary btn-back-custom",
			      callback: function() {
			      }
			    },
		    }
		});	
		
	})


	function removeLogo(){
		var logo_name = $('#OrganizationLogo_vch').val();
		if(logo_name != '')
		{
			try{
				$.ajax({
				type: "POST",
				url: '/session/sire/models/cfc/myaccountinfo.cfc?method=UpdateOrgLogo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data: {'action':'remove','logo_name':logo_name},
				beforeSend: function( xhr ) {
					$('.btn').prop('disabled',true);
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					//bootbox.alert('Update Fail', function() {});
					$('.btn').prop('disabled',false);
					$('#processingPayment').hide();
					bootbox.dialog({
					    message:"Unable to remove your organization logo at this time. An error occurred.",
					    title: "My Account",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});	
				},					  
				success:		
					function(d) {
						$('.btn').prop('disabled',false);
						$('#processingPayment').hide();
						console.log(d);
						if(d.RXRESULTCODE == 1){
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
						}
						else{
							bootbox.dialog({
							    message:d.MESSAGE,
							    title: "My Account",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
						}
					} 		
				});
			}catch(ex){
				$('.btn').prop('disabled',false);
				$('#processingPayment').hide();
				bootbox.dialog({
				    message:'Unable to remove your organization logo at this time. An error occurred.',
				    title: "My Account",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});	
			}
		}
	}

	function getExtension(filename) {
		var parts = filename.split('.');
		return parts[parts.length - 1];
	}

	function isImage(filename) {
		var ext = getExtension(filename);
		switch (ext.toLowerCase()) {
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'bmp':
			case 'png':
				//etc
				return true;
		}
		return false;
	}

	function GetFileSize(fileid) {
		 try {
		 var fileSize = 0;
		 //for IE
		 if ($.browser.msie) {
		 //before making an object of ActiveXObject, 
		 //please make sure ActiveX is enabled in your IE browser
		 var objFSO = new ActiveXObject("Scripting.FileSystemObject"); 
		 var filePath = $("#" + fileid)[0].value;
		 
		 var objFile = objFSO.getFile(filePath);
		 var fileSize = objFile.size; //size in kb
		 fileSize = fileSize / 1048576; //size in mb 
		 }
		 //for FF, Safari, Opeara and Others
		 else {
		 fileSize = $("#" + fileid)[0].files[0].size //size in kb
		
		 //fileSize = fileSize / 1048576; //size in mb 
		 fileSize = fileSize / 1000000;
		 }
		   //alert("Uploaded File Size is" + fileSize + "MB");
		   return fileSize;
		 }
		 catch (e) {
		  alert("Error is :" + e);
		  return 100;
		 }
	}

	var LIMIT_MESSAGE = 160;
    
    if($('#custom-help-message').length > 0){
    	
    	$("#custom-help-message").bind('keyup change', function(e) {
        	total_primary_character = $("#custom-help-message").val().length;
        	//characters_available(LIMIT_MESSAGE, total_primary_character, 'help-block');
        	countText('#help-block',$('#custom-help-message').val());
    	});
    }

       if($('#custom-stop-message').length > 0){
    	
    	$("#custom-stop-message").bind('keyup change', function(e) {
        	total_primary_character = $("#custom-stop-message").val().length;
        	//characters_available(LIMIT_MESSAGE, total_primary_character, 'stop-block');
        	countText('#stop-block',$('#custom-stop-message').val());
    	});
    }         

     function characters_available (limit, total_character, help_class) {
		if (limit - total_character < 0 ) {
			$('#'+help_class).text((total_character - limit) + ' characters over');
			$('#'+help_class).addClass('text-danger');
		} else  {
			$('#'+help_class).text((limit - total_character) + '/' + limit +' characters available');
			$('#'+help_class).removeClass('text-danger');
		} 
	}
	
})(jQuery);

function isValidUrl(field, rules, i, options){
	if(/(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/.test(field.val()) === false){
		return "Invalid URL!";
	}
}

// js for progress bar in organization information
$(document).ready(function(){
	$( "#ProfileProgressBar" ).progressbar({
		value: parseInt($('input[name="progress-bar-percent"]').val()) 
	});
});

