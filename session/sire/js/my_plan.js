(function($){

	//loadExpirationDate();
	loadCountry();

	function loadExpirationDate(){

		var expiration_date = $('#expiration_date').val();

		if(expiration_date != ''){
			var n = expiration_date.search("/");
			if( n > 0)
			{
				var res = expiration_date.split("/");
				var month = res[0];
				var year = res[1];

				$('#expiration_date_month').val(month);
				$('#expiration_date_year').val(year);
			}
		}
	}

	function loadCountry(){
		if(country == '') country = "US";
		$('#country').val(country);
	}

	function update_expiration_date() {
		$('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
	}


	var my_plan = $("#my_plan_form");

	$('#expiration_date_month, #expiration_date_year').change(function(){
		update_expiration_date();
	});

		
	my_plan.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});


	$('.select_used_card').click(function(){

		$('#card_number').val('');
		$('#security_code').val('');
		$('#expiration_date').val('');
		$('#expiration_date_month').val('');
		$('#expiration_date_year').val('');
		$('#first_name').val('');
		$('#last_name').val('');

		my_plan.validationEngine('hideAll');

		//my_plan[0].reset();

		if($(this).val() == 2){
			$('.update_card_info').show();
			$('.update_cardholder_info fieldset').prop('disabled',false);
			$('.btn-update-profile').prop('disabled',false);
			$(".cardholder").each( function( index, element ){
	    		$(this).val('');
			});
			$('#country').val('US');
			$('#email').val($('#h_email').val());
		}
		else{
			$('.update_card_info').hide();	
			$('.update_cardholder_info fieldset').prop('disabled',true);
			$('.btn-update-profile').prop('disabled',true);

			$( ".cardholder" ).each( function( index, element ){
    			var data = $(this).data('value');
    			$(this).val(data);
			});
		}
	});


	my_plan.submit(function(event){
		event.preventDefault();
		
		if (my_plan.validationEngine('validate', {focusFirstField : true, scroll: false })) {
			var d = new Date();
			var expiredMonth = parseInt($('#expiration_date_month').val());
			var expiredYear = parseInt($('#expiration_date_year').val());

			if ( (expiredYear < d.getYear()) || (expiredMonth <= d.getMonth()+1 && expiredYear <= d.getFullYear())){
				bootbox.dialog({
					message: 'Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card.',
					title: 'PAYMENT FAILED',
					buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
			else {
				$('.btn').prop('disabled',true);
				$('.btn-update-profile').text('Processing');
				try{
					$.ajax({
						method: 'POST',
						url: '/session/sire/models/cfc/billing.cfc?method=UpdateCustomer&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						data: my_plan.serialize(),
						dataType: 'json',
						//timeout: 6000,
						beforeSend: function( xhr ) {
							$('#processingPayment').show();
							$('.btn').prop('disabled',true);
							$('.btn-update-profile').text('Processing');
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							$('#processingPayment').hide();
							//bootbox.alert('Update Fail', function() {});
							bootbox.dialog({
							    message:'Update Fail',
							    title: "My Plan",
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {}
							        }
							    }
							});	
							$('.btn').prop('disabled',false);
							$('.btn-update-profile').text('Update Payment Info');
							
						},					 
						success: function(data) {
							$('#processingPayment').hide();
							/*bootbox.alert(data.MESSAGE, function() {
								if(data.RXRESULTCODE == 1)
									location.reload();

							});*/
							
							bootbox.dialog({
							    message:data.MESSAGE,
							    title: "My Plan",
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {
							            	if(data.RXRESULTCODE == 1)
									location.reload();
							            }
							        }
							    }
							});	
							$('.btn').prop('disabled',false);
							$('.btn-update-profile').text('Update Payment Info');
						}
					});
				}
				catch(ex){
					$('#processingPayment').hide();
					//bootbox.alert('Update Fail', function() {});
					bootbox.dialog({
					    message:'Update Fail',
					    title: "My Plan",
					    buttons: {
					        success: {
					            label: "OK",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});	
					$('.btn').prop('disabled',false);
					$('.btn-update-profile').text('Update Payment Info');
				}
			}
			
		}
	});
	/*
	var fields = $('#my_plan_form input, #my_plan_form select');
	
	fields.focus(function(){
		var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
		setTimeout(function(){
			nextField.validationEngine('hide');
		},1);
	});*/
	
	
	var checked_payment_method = $('input.check_payment_method:checked').val();
	function switchPaymentMethod() {
		var check_payment_method = $('input.check_payment_method:checked').val();
		if (checked_payment_method == check_payment_method) return;
		checked_payment_method = check_payment_method;
		switch(check_payment_method) {
			case 0:
			case '0':
				$('#card_number').attr('class', 'form-control validate[required,custom[creditCardFunc]]')
					.removeAttr('data-errormessage-custom-error').parent().prev().html('Card Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid security code',
					'maxlength': 4
				}).parent().prev().html('Security Code:<span class="text-danger">*</span> <a href="javascript:$(\'#scModal\').modal(\'show\')" data-toggle="modal" data-target="#scModal"><img src="/session/sire/images/help-small.png"/></a>');
				$('#ExpirationDate').show();
				$('#card_name_label').html('Cardholder Name:<span class="text-danger">*</span>');
				break;
			case 1:
			case '1':
				$('#card_number').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid routing number'
				}).parent().prev().html('Routing Number:<span class="text-danger">*</span>');
				$('#security_code').attr({
					'class': 'form-control validate[required,custom[onlyNumber]]',
					'data-errormessage-custom-error': '* Invalid account number',
					'maxlength': 32
				}).parent().prev().html('Account Number:<span class="text-danger">*</span>');
				$('#ExpirationDate').hide();
				$('#card_name_label').html('Account Name:<span class="text-danger">*</span>');
				break;
		}
	}
	
	$('input.check_payment_method').click(switchPaymentMethod);


	var modalAlert = function(title, message, event) {
		var bootstrapAlert = $('#bootstrapAlert');
		bootstrapAlert.find('.modal-title').text(title);
		bootstrapAlert.find('.alert-message').text(message);
		bootstrapAlert.modal('show');
	}

	 $("#unscriber_keyword_form")
	    .validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
	        focusFirstField : false
	    });
	 $('#unscriber_keyword_form').submit(function(event){
		event.preventDefault();
		$('#UnscriberKeywordModal #Unsubscribe').click();
		}).validationEngine({
		promptPosition : "topLeft", 
		autoPositionUpdate: true, 
		showArrow: false, 
		scroll: false,
		focusFirstField : false
	});
	
	$('#UnscriberKeywordModal #Unsubscribe').click(function(){
		if ($('#unscriber_keyword_form').validationEngine('validate')) {
			var numberKeywordUnscriber = $('#numberKeywordUnscriber').val();
			$('#processingPayment').show();
			$.ajax({
				url: '/session/sire/models/cfc/order_plan.cfc?method=UnsubscribeKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'post',
				dataType: 'json',
				data: {NUMBERKEYWORDUNSUBSCRIBE: numberKeywordUnscriber},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#processingPayment').hide();
				},
				success: function(data) {
					//console.log(data);
					$('#processingPayment').hide();
					if (data && data.RXRESULTCODE == 1) {
						$('#UnscriberKeywordModal').modal('hide');
						location.href ='/session/sire/pages/my-plan';
					} else {
						modalAlert(data.MESSAGE);
					}
				}
			});
			
		}
	});



	$('#remove-payment-btn').click(function(event){
		event.preventDefault();

		bootbox.dialog({
			message: 'Are you sure you want to remove this card?',
			title: 'Remove Payment Confirmation',
			buttons: {
		        success: {
		            label: "OK",
		            className: "btn btn-medium btn-success-custom",
		            callback: function() {
		            	$.ajax({
		            		url: "/session/sire/models/cfc/billing.cfc?method=removeUserPaymentInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		            		type: "POST",
		            		dataType: "json",
		            		beforeSend: function(){
		            			$('#processingPayment').show();
		            		},
		            		success: function(data){
		            			
		            			try{
		            				bootbox.dialog({
		            					message: data.MESSAGE,
									    title: "Remove Result",
									    closeButton: false,
									    buttons: {
									        success: {
									            label: "OK",
									            className: "btn btn-medium btn-success-custom",
									            callback: function() {
									            	if(data.RXRESULTCODE == 1){
											    		window.location.reload();
											    	}
									            }
									        }
									    }
		            				});
		            			} catch(error) {

		            			}
		            		},
		            		complete: function(){
		            			$('#processingPayment').hide();
		            		}
		            	});
		            }
		        },
		        cancel: {
		        	label: "CANCEL",
		            className: "btn btn-medium btn-back-custom",
		            callback: function() {}
		        }
		    }
		});
	});

})(jQuery);