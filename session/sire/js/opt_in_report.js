(function($){
	if ('undefined' != typeof(subscriber_growth_chart_data) ) {
		var options = {
			responsive: true
		};
		if ($(window).width() < 480 || $(window).height() < 480) {
			for(var i = 0; i < subscriber_growth_chart_data.labels.length; i++) {
				subscriber_growth_chart_data.labels[i] = 
					new Date(Date.parse(subscriber_growth_chart_data.labels[i] +" 1, 2012")).getMonth() + 1;
			}
		}
		var ctx = document.getElementById("subscriber_growth_chart").getContext("2d");
		var subscriberBarChart = new Chart(ctx).Bar(subscriber_growth_chart_data, options);
		$('#subscriber_growth_legend').html( subscriberBarChart.generateLegend() );
	}
})(jQuery);

(function($){
	$('#AFStartDate').datepicker({
		numberOfMonths: 1,
		showButtonPanel: false,
		dateFormat: 'mm/dd/yy',
		showOn: 'both',
		buttonImage: "/public/images/calendar.png",
        buttonImageOnly: true
	});
	
	$('#AFEndDate').datepicker({
		numberOfMonths: 1,
		showButtonPanel: false,
		dateFormat: 'mm/dd/yy',
		showOn: 'both',
		buttonImage: "/public/images/calendar.png",
        buttonImageOnly: true
	});
	
	$('#AFSubmit').click(function(){
		
		var start_date= new Date($('#AFStartDate').val());
		var end_date= new Date($('#AFEndDate').val());
        
        
        if (start_date > end_date) {
			bootbox.dialog({
			    message:'Start Date must less than End Date',
			    title: 'Activity Filter',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
        } else {
			if ($('#active_feed_form').validationEngine('validate')) {
				$.ajax({
					type: 'GET',
					url: '/session/sire/views/commons/active_feed.cfm',   
					dataType: 'html',
					data: {
						id: $(this).data('campaign-id'),
						af_start_date: $('#AFStartDate').val(),
						af_end_date: $('#AFEndDate').val(),
						af_phone_number: $('#AFPhoneNumber').val(),
						af_batch_id: $('#AFBatchId').val()
					},
					success: function(html) {
						var newAFTable = $(html);
						$('#totalFeed').val('0');
						//$('.download-subscribers').attr('href', '/session/sire/models/cfm/feed.xls.cfm?id='+$(this).data('campaign-id')+'&af_start_date='+$('#AFStartDate').val()+'&af_end_date='+$('#AFEndDate').val()+'&af_phone_number='+$('#AFPhoneNumber').val()).show();
						$('#AFTable').html(newAFTable.html());
						$('#ActFeeFilModal').modal('hide');
					}
				});
			}
		}
	});
	
	$('#AFReset').click(function(){
		$('#active_feed_form')[0].reset();
	});
	$('.download-subscribers').click(function(event) {
		var _iRecordsTotal = $('#totalFeed').val();
		if (!$.isNumeric(_iRecordsTotal) || _iRecordsTotal < 1) {
			event.preventDefault();
			bootbox.dialog({
			    message:'There are currently no Feed in this list to download.',
			    title: 'Download Activity Feed',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
		} else {
				window.location.href = '/session/sire/models/cfm/feed.xls.cfm?&af_start_date='+$('#AFStartDate').val()+'&af_end_date='+$('#AFEndDate').val()+'&af_phone_number='+$('#AFPhoneNumber').val()+'&af_batch_id='+$(this).data('campaign-id');
			
		}
	});

	//load data when paging
	$('body').on('keyup', '.paginate_text_active', function(event) {
		if($('.paginate_text_active').val() <= 0){
			$('.paginate_text_active').val('1');
		}

		if ( parseInt($('.paginate_text_active').val()) > parseInt($('#allPages').html()) ){
			$('.paginate_text_active').val(parseInt($('#allPages').html()));
		}

		$.ajax({
			type: 'GET',
			url: '/session/sire/views/commons/active_feed.cfm',   

			dataType: 'html',
			data: {
				id: $('.download-subscribers').data('campaign-id'),
				af_start_date: $('#AFStartDate').val(),
				af_end_date: $('#AFEndDate').val(),
				af_phone_number: $('#AFPhoneNumber').val(),
				af_batch_id: $('#AFBatchId').val(),
				page: $('.paginate_text_active').val()
			},
			success: function(html) {
				var newAFTable = $(html);
				$('#totalFeed').val('0');
				$('#AFTable').html(newAFTable.html());
			}
		});
	});

	$('body').on('click', '.paginate_button_af_next', function(event) {
		if ( parseInt($('.paginate_text_active').val()) < parseInt($('#allPages').html()) ) {
			var nextPage = parseInt($('.paginate_text_active').val()) + 1;
			$('.paginate_text_active').val(nextPage);

			$.ajax({
				type: 'GET',
				url: '/session/sire/views/commons/active_feed.cfm',   

				dataType: 'html',
				data: {
					id: $('.download-subscribers').data('campaign-id'),
					af_start_date: $('#AFStartDate').val(),
					af_end_date: $('#AFEndDate').val(),
					af_phone_number: $('#AFPhoneNumber').val(),
					af_batch_id: $('#AFBatchId').val(),
					page: $('.paginate_text_active').val()
				},
				success: function(html) {
					var newAFTable = $(html);
					$('#totalFeed').val('0');
					$('#AFTable').html(newAFTable.html());
				}
			});
		}
		
	});
	$('body').on('click', '.paginate_button_af_previous', function(event) {
		if (parseInt($('.paginate_text_active').val()) > 1){
			
			var nextPage = parseInt($('.paginate_text_active').val()) - 1;
			$('.paginate_text_active').val(nextPage);

			$.ajax({
				type: 'GET',
				url: '/session/sire/views/commons/active_feed.cfm',   

				dataType: 'html',
				data: {
					id: $('.download-subscribers').data('campaign-id'),
					af_start_date: $('#AFStartDate').val(),
					af_end_date: $('#AFEndDate').val(),
					af_phone_number: $('#AFPhoneNumber').val(),
					af_batch_id: $('#AFBatchId').val(),
					page: $('.paginate_text_active').val()
				},
				success: function(html) {
					var newAFTable = $(html);
					$('#totalFeed').val('0');
					$('#AFTable').html(newAFTable.html());
				}
			});
		}
		
	});

	$('body').on('click', '.paginate_button_af_first', function(event) {
		$('.paginate_text_active').val('1');
		$.ajax({
			type: 'GET',
			url: '/session/sire/views/commons/active_feed.cfm',   

			dataType: 'html',
			data: {
				id: $('.download-subscribers').data('campaign-id'),
				af_start_date: $('#AFStartDate').val(),
				af_end_date: $('#AFEndDate').val(),
				af_phone_number: $('#AFPhoneNumber').val(),
				af_batch_id: $('#AFBatchId').val(),
				page: $('.paginate_text_active').val()
			},
			success: function(html) {
				var newAFTable = $(html);
				$('#totalFeed').val('0');
				$('#AFTable').html(newAFTable.html());
			}
		});
	});

	$('body').on('click', '.paginate_button_af_last', function(event) {
		$('.paginate_text_active').val(parseInt($('#allPages').html()));
		$.ajax({
			type: 'GET',
			url: '/session/sire/views/commons/active_feed.cfm',   

			dataType: 'html',
			data: {
				id: $('.download-subscribers').data('campaign-id'),
				af_start_date: $('#AFStartDate').val(),
				af_end_date: $('#AFEndDate').val(),
				af_phone_number: $('#AFPhoneNumber').val(),
				af_batch_id: $('#AFBatchId').val(),
				page: $('.paginate_text_active').val()
			},
			success: function(html) {
				var newAFTable = $(html);
				$('#totalFeed').val('0');
				$('#AFTable').html(newAFTable.html());
			}
		});
	});
})(jQuery);

function SaveToDisk(fileUrl, fileName) {
    var hyperlink = document.createElement('a');
    hyperlink.href = fileUrl;
    hyperlink.target = '_blank';
    hyperlink.download = fileName || fileUrl;

    (document.body || document.documentElement).appendChild(hyperlink);
    hyperlink.onclick = function() {
       (document.body || document.documentElement).removeChild(hyperlink);
    };

    var mouseEvent = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
    });

    hyperlink.dispatchEvent(mouseEvent);
    
    // NEVER use "revoeObjectURL" here
    // you can use it inside "onclick" handler, though.
    // (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);

    // if you're writing cross-browser function:
    if(!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
       window.URL.revokeObjectURL(hyperlink.href);
    }
}