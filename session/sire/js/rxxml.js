// JavaScript Document
var RXxml_special_to_escaped_one_map = {
	'&' : '&amp;',
	'"' : '&quot;',
	'<' : '&lt;',
	'>' : '&gt;',
	"'" : '&apos;',
	"?" : '&#63;'	
};

var RXescaped_one_to_xml_special_map = {
	'&amp;' : '&',
	'&quot;' : '"',
	'&lt;' : '<',
	'&gt;' : '>',
	'&apos;' : "'",
	'&#63;' : "?" 
};
var RXspace = {
	' ' : '%20'
}
var RXspacedecode = {
	'%20' : ' '
}

function RXencodeXML(string) {
	
	//console.log(string);
	//console.log('string=(' + string + ')');
	
	var retStr = String(string);
	if (isNaN(retStr)) {
		retStr = retStr.replace(/([\&"<'>?])/g, function(str, item) {
			return RXxml_special_to_escaped_one_map[item];
		});
	}
	return retStr;
};

function RXdecodeXML(string) {
	
	var retStr = String(string);
	if (isNaN(retStr)) {
		retStr = retStr.replace(/(&quot;|&lt;|&gt;|&amp;|&apos;|&#63;)/g,  function(str, item) {
			return RXescaped_one_to_xml_special_map[item];
		});
	}
	return retStr;
}

function RXencodeSpace(string) {

	var retStr = String(string); 
	return retStr.replace(/([\ ])/g, function(str, item) {
		return RXspace[item];
	});
}
function RXdencodeSpace(string) {
	
	var retStr = String(string); 
	return retStr.replace(/(%20)/g, function(str, item) {
		return RXspacedecode[item];
	});
}