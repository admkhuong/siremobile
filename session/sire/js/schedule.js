var BindScheduleDataEdit = function(){};

$(function() {	

			schedule_data = $("#SCHEDULEDATA").val();	
			
			isEditScheduleByDialog = String(window.location).indexOf('schedule') == -1;
			var ul = $("<ul></ul>")
			var objects = new Array();
			for (var i = 0; i <= 7; ++i) {
				var obj = new Object();
				obj.i = i;
				obj.dayName = GetDayNameById(i);
				objects.push(obj);
				$("#tmplScheduleDay").tmpl(obj).appendTo(ul);
			}
			
			ul.appendTo('#MyscheduleTabs');
			$("#tmplScheduleDayTime").tmpl(objects).appendTo('#MyscheduleTabs');

			// create tabs for schedule days
			$("#MyscheduleTabs").tabs();
			
			/* <cfif NOT Isnull(Campaign_Schedule_Title)>
				$('#subTitleText').text('<cfoutput>#Campaign_Schedule_Title#</cfoutput>');
				$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
			</cfif>*/
			
			$("#SetScheduleSBDiv #Cancel").click( function() {
				$('#ScheduleOptionsModal').modal('hide');				
			}); 
						
			$.each($('.hour'), function(index, value) {
				CreateHourList(this);
			});
			
			$.each($('.minute'), function(index, value) {
				CreateMinuteList(this);
			});		
			
			$.each($('.month'), function(index, value) {
				CreateMonthList(this);
			});

			$.each($('.day'), function(index, value) {
				CreateDayList(this);
			});
			
			$.each($('.year'), function(index, value) {
				CreateYearList(this);
			});
			
			$.each($('.noon_list'), function(index, value) {
				CreateNoonList(this);
			});
			
			CreateDatePicker('FollowingTimeDate');
			CreateDatePicker('FollowingTimeEndDate',true);
			CreateDatePicker('AcrossMultipleDaysStartDate');
			CreateDatePicker('AcrossMultipleDaysEndDate', true);

			// DoCheckAll(); calling this here was causing data from DB read to be overwirtten with chechs when they shouldnt be
			

			if(schedule_data != "" || INPBATCHID > 0)
			{
				BindScheduleDataEdit();	
			}
			
			//VisibleDaySelector();

			$('.check_all').click(function() {
				DoCheckAll();
			});
			
			$('.multiple_days').change(function() {
				DoCheckAll();
			});			
			
			/*
			$('input[name=SCHEDULETYPE]').click(function() {
				VisibleDaySelector();
			});
			*/

			// Based on parameter - load from DB //

			$('#showAdvanceSchedule').click(function(event){
				event.preventDefault();
				$('#divDaySelector').toggle();
			})
			
		});
		
		function SelectScheduleTab(index) {
			$("#MyscheduleTabs").tabs("select" , index)
		}
		function IsInRange(dayId) {
			var startDate = new Date(parseInt($('#AcrossMultipleDaysStartDate_Year').val()), parseInt($('#AcrossMultipleDaysStartDate_Month').val()) - 1, parseInt($('#AcrossMultipleDaysStartDate_Day').val()));
			var endDate = new Date(parseInt($('#AcrossMultipleDaysEndDate_Year').val()), parseInt($('#AcrossMultipleDaysEndDate_Month').val()) - 1, parseInt($('#AcrossMultipleDaysEndDate_Day').val()))
			var today = new Date();
			today = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate());
			if (GetDiffDays(today, startDate) > 0) {
				startDate = today;
			}
			var diffDays = GetDiffDays(endDate, startDate);
			if (diffDays > 6) {
				return true;
			}
			else if (diffDays < 0) {
				return false;
			}
			var result = false;
			for (var i = 0; i <= diffDays; ++i) {
				var checkDate = new Date(startDate.getTime() + i*24*60*60*1000);
				if (checkDate.getDay() == dayId - 1) {
					result = true;
					break;
				}
			}
			return result;
		}
		
		function GetDiffDays(date1, date2) {
			return (date1.getTime() - date2.getTime()) / (1000 * 60 * 60 * 24);
		}


		if ( parseInt(INPBATCHID) > 0 ){
			
			BindScheduleDataEdit = function() {

				$.ajax({
					url : '/session/sire/models/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: "POST",
					dataType: "json",
					async: false,
					data : { 
						INPBATCHID : INPBATCHID, 
					},
					beforeSend: function(){
						$('#processingPayment').show();
					}
					
				}).done(function(d){
					$('#processingPayment').hide();
					var data = d.DATA;

					if (data.RXRESULTCODE[0] == 1) {
						var dataRows = data.ROWS[0];
						
						if (dataRows.length > 0) {
							/*console.log(dataRows[0]);*/
							var scheduleType = dataRows[0].SCHEDULETYPE_INT;
							$('#rdoSchedultType_' + scheduleType).attr('checked', true);
							
							if (scheduleType == 1 || scheduleType == 0) {
								
								$('#FollowingTimeDate').val(dataRows[0].START_DT);
								SelectDate(document.getElementById('FollowingTimeDate'));

								$('#FollowingTimeEndDate').val(dataRows[0].STOP_DT);
								SelectDate(document.getElementById('FollowingTimeEndDate'));

								$('#cboTimeHour_FollowingTime').val(parseInt(dataRows[0].STARTHOUR_TI) % 12);

								$('#FollowingTimeStart_minute').val(dataRows[0].STARTMINUTE_TI)
								$('#FollowingTimeStart_hour').val(Math.floor(parseInt(dataRows[0].STARTHOUR_TI) % 12));
								if(dataRows[0].STARTHOUR_TI > 11){
									$("#FollowingTimeStart_noon_option").val("1");
								}else{
									$("#FollowingTimeStart_noon_option").val("0");
								}

								$('#FollowingTimeEnd_minute').val(dataRows[0].ENDMINUTE_TI)
								$('#FollowingTimeEnd_hour').val(Math.floor(parseInt(dataRows[0].ENDHOUR_TI) % 12));
								if(dataRows[0].ENDHOUR_TI > 11){
									$("#FollowingTimeEnd_noon_option").val("1");
								}else{
									$("#FollowingTimeEnd_noon_option").val("0");
								}
								
							}
							else if (scheduleType == 2) {
								var schedule = dataRows[0];

								$('#AcrossMultipleDaysStartDate').val(schedule.START_DT);
								SelectDate(document.getElementById('AcrossMultipleDaysStartDate'));
								$('#AcrossMultipleDaysEndDate').val(schedule.STOP_DT);
								SelectDate(document.getElementById('AcrossMultipleDaysEndDate'));

								$('#cboTimeZone_AcrossMultipleDays').val(schedule.TIMEZONE_VCH);
								
								if (dataRows.length == 1 & GetDayIdBySchedule(schedule) == 0) {

									$('#FollowingTimeStart_minute').val(schedule.STARTMINUTE_TI)
									$('#FollowingTimeStart_hour').val(Math.floor(parseInt(schedule.STARTHOUR_TI) % 12));
									if(schedule.STARTHOUR_TI > 11){
										$("#FollowingTimeStart_noon_option").val("1");
									}else{
										$("#FollowingTimeStart_noon_option").val("0");
									}

									$('#FollowingTimeEnd_minute').val(schedule.ENDMINUTE_TI)
									$('#FollowingTimeEnd_hour').val(Math.floor(parseInt(schedule.ENDHOUR_TI) % 12));
									if(schedule.ENDHOUR_TI > 11){
										$("#FollowingTimeEnd_noon_option").val("1");
									}else{
										$("#FollowingTimeEnd_noon_option").val("0");
									}

									$('.check_all').attr('checked', true);
									BindScheduleOnTab(0, schedule)
								}
								else {
									for (var i = 0; i < dataRows.length; ++i) {
										schedule = dataRows[i];
										var dayId = GetDayIdBySchedule(schedule);
										BindScheduleOnTab(dayId, schedule)
									}
									
									$('.check_all').attr('checked', false);
								}
								DoCheckAll($('.check_all'));
							}
						}
					}
				});
			}
			
			function updateScheduleEdit(){	
				var scheduleTime = GetScheduleTimeList();	

				if (scheduleTime.length > 0 && scheduleTime[0].scheduleType == 2) {
					for (var i = 0; i < scheduleTime.length; ++i) {
						var dayName = GetDayNameById(scheduleTime[i].dayId);
						if (!IsBefore(scheduleTime[i].startHour, scheduleTime[i].startMinute, scheduleTime[i].endHour, scheduleTime[i].endMinute)) {
							bootbox.alert("Start Time must fall before the End Time", function(result) {});	
							return false;						
						}
						if (scheduleTime[i].enabledBlackout) {
							if (IsBefore(scheduleTime[i].blackoutStartHour, 0, scheduleTime[i].startHour, scheduleTime[i].startMinute) ||
								IsBefore(scheduleTime[i].endHour, scheduleTime[i].endMinute, scheduleTime[i].blackoutEndHour, 0)) {
								
								bootbox.alert("Your blackout times must fall within the start and end time", function(result) {});
								return false;
							}
							else {
								if (!IsBefore(scheduleTime[i].blackoutStartHour, 0, scheduleTime[i].blackoutEndHour, 0)) {
									bootbox.alert("Blackout start time must be before blackout end time", function(result) {});	
									return false;	
								}
							}
						}
					}
				}
				if (scheduleTime.length == 1 && scheduleTime[0].scheduleType == 1) {
					var year = parseInt($('#FollowingTimeDate_Year').val());
					var month = parseInt($('#FollowingTimeDate_Month').val());
					var day = parseInt($('#FollowingTimeDate_Day').val());
					var hour = 	parseInt(scheduleTime[0].startHour);
					var minute = parseInt(scheduleTime[0].startMinute);
					var followingDate = new Date(year, month - 1, day, hour, minute, 0 , 0);

					var end_year = parseInt($('#FollowingTimeEndDate_Year').val());
					var end_month = parseInt($('#FollowingTimeEndDate_Month').val());
					var end_day = parseInt($('#FollowingTimeEndDate_Day').val());
					var end_hour = 	parseInt(scheduleTime[0].endHour);
					var end_minute = parseInt(scheduleTime[0].endMinute);

					var followingDate = new Date(year, month - 1, day, hour, minute, 0 , 0);

					var followingEndDate = new Date(end_year, end_month - 1, end_day, end_hour, end_minute, 0 , 0);

					if (followingDate > followingEndDate) {
						bootbox.alert("The start date cannot be after the end date", function(result) {});
						return;
					}
					

					if (followingDate < new Date()) 
					{
						SaveSchedule(scheduleTime);
					}
					else 
					{
						SaveSchedule(scheduleTime);
					}
				}
				else {
					var isSaved = false;
					if (scheduleTime.length >= 1 && scheduleTime[0].scheduleType == 2) {
						var startYear = parseInt($('#AcrossMultipleDaysStartDate_Year').val());
						var startMonth = parseInt($('#AcrossMultipleDaysStartDate_Month').val());
						var startDay = parseInt($('#AcrossMultipleDaysStartDate_Day').val());
						
						var endYear = parseInt($('#AcrossMultipleDaysEndDate_Year').val());
						var endMonth = parseInt($('#AcrossMultipleDaysEndDate_Month').val());
						var endDay = parseInt($('#AcrossMultipleDaysEndDate_Day').val());
						
						var startDate = new Date(startYear, startMonth - 1, startDay);
						var endDate = new Date(endYear, endMonth - 1, endDay);
						
						if (startDate > endDate) {
							bootbox.alert("The start date cannot be after the end date", function(result) {});
							return;
						}
						if (startDate < new Date()) 
						{							
							SaveSchedule(scheduleTime);	
							isSaved = true;																				
						}
						else
						{
							SaveSchedule(scheduleTime);	
							isSaved = true;		
						}
					}
					if (!isSaved) {
						SaveSchedule(scheduleTime);
						isSaved = true;	
					}
				}
				return false;
			}
		
		}
		else{ //CREATE NEW
			
			function BindScheduleData(dataRows) 
			{			
				dataRows = JSON.parse(dataRows);
				if (dataRows.length > 0) {
					var scheduleType = dataRows[0].SCHEDULETYPE_INT;
					$('#rdoSchedultType_' + scheduleType).attr('checked', true);
					
					if (scheduleType == 1) {
						$('#FollowingTimeDate').val(dataRows[0].START_DT);
						SelectDate(document.getElementById('FollowingTimeDate'));
						$('#cboTimeHour_FollowingTime').val(parseInt(dataRows[0].STARTHOUR_TI) % 12);
						$('#cboTimeMinute_FollowingTime').val(dataRows[0].STARTMINUTE_TI)
						$('#cboNoonOption_Time').val(Math.floor(parseInt(dataRows[0].STARTHOUR_TI) / 12));
						$('#cboTimeZone_FollowingTime').val(dataRows[0].TIMEZONE_VCH)
					}
					else if (scheduleType == 2) {
						var schedule = dataRows[0];
						$('#AcrossMultipleDaysStartDate').val(schedule.START_DT);
						SelectDate(document.getElementById('AcrossMultipleDaysStartDate'));
						$('#AcrossMultipleDaysEndDate').val(schedule.STOP_DT);
						SelectDate(document.getElementById('AcrossMultipleDaysEndDate'));
						$('#cboTimeZone_AcrossMultipleDays').val(schedule.TIMEZONE_VCH);
						
						if (dataRows.length == 1 & GetDayIdBySchedule(dataRows[0]) == 0) {
							$('.check_all').attr('checked', true);
							$('.check').prop('disable', true);
							BindScheduleOnTab(0, dataRows[0])
						}
						else {
							for (var i = 0; i < dataRows.length; ++i) {
								schedule = dataRows[i];
								var dayId = GetDayIdBySchedule(schedule);
								BindScheduleOnTab(dayId, schedule)
							}
							
							$('.check_all').attr('checked', false);
						}
						DoCheckAll($('.check_all'));
					}
				}
			}
			
			function updateScheduleCreate()
			{	
				var scheduleTime = GetScheduleTimeList();	

				if (scheduleTime.length > 0 && scheduleTime[0].scheduleType == 2) {
					for (var i = 0; i < scheduleTime.length; ++i) {
						var dayName = GetDayNameById(scheduleTime[i].dayId);
						if (!IsBefore(scheduleTime[i].startHour, scheduleTime[i].startMinute, scheduleTime[i].endHour, scheduleTime[i].endMinute)) {
							bootbox.alert("Start Time must fall before the End Time", function(result) {});	
							return false;						
						}
						if (scheduleTime[i].enabledBlackout) {
							if (IsBefore(scheduleTime[i].blackoutStartHour, 0, scheduleTime[i].startHour, scheduleTime[i].startMinute) ||
								IsBefore(scheduleTime[i].endHour, scheduleTime[i].endMinute, scheduleTime[i].blackoutEndHour, 0)) {
								
								bootbox.alert("Your blackout times must fall within the start and end time", function(result) {});
								return false;
							}
							else {
								if (!IsBefore(scheduleTime[i].blackoutStartHour, 0, scheduleTime[i].blackoutEndHour, 0)) {
									bootbox.alert("Blackout start time must be before blackout end time", function(result) {});	
									return false;	
								}
							}
						}
					}
				}

				if (scheduleTime.length == 1 && scheduleTime[0].scheduleType == 1) {
					var year = parseInt($('#FollowingTimeDate_Year').val());
					var month = parseInt($('#FollowingTimeDate_Month').val());
					var day = parseInt($('#FollowingTimeDate_Day').val());
					var hour = 	parseInt(scheduleTime[0].startHour);
					var minute = parseInt(scheduleTime[0].startMinute);
					var followingDate = new Date(year, month - 1, day, hour, minute, 0 , 0);

					var end_year = parseInt($('#FollowingTimeEndDate_Year').val());
					var end_month = parseInt($('#FollowingTimeEndDate_Month').val());
					var end_day = parseInt($('#FollowingTimeEndDate_Day').val());
					var end_hour = 	parseInt(scheduleTime[0].endHour);
					var end_minute = parseInt(scheduleTime[0].endMinute);

					var followingDate = new Date(year, month - 1, day, hour, minute, 0 , 0);

					var followingEndDate = new Date(end_year, end_month - 1, end_day, end_hour, end_minute, 0 , 0);

					if (followingDate > followingEndDate) {
						bootbox.alert("The start date cannot be after the end date", function(result) {});
						return;
					}


					ScheduleTimeData = scheduleTime;
					$('#ScheduleOptionsModal').modal('hide');
					return true;
				}
				else {
					var isSaved = false;
					if (scheduleTime.length >= 1 && scheduleTime[0].scheduleType == 2) {
						var startYear = parseInt($('#AcrossMultipleDaysStartDate_Year').val());
						var startMonth = parseInt($('#AcrossMultipleDaysStartDate_Month').val());
						var startDay = parseInt($('#AcrossMultipleDaysStartDate_Day').val());
						
						var endYear = parseInt($('#AcrossMultipleDaysEndDate_Year').val());
						var endMonth = parseInt($('#AcrossMultipleDaysEndDate_Month').val());
						var endDay = parseInt($('#AcrossMultipleDaysEndDate_Day').val());
						
						var startDate = new Date(startYear, startMonth - 1, startDay);
						var endDate = new Date(endYear, endMonth - 1, endDay);
						
						if (startDate > endDate) {
							bootbox.alert("The start date cannot be after the end date", function(result) {});
							return;
						}					
						ScheduleTimeData = scheduleTime;
						$('#ScheduleOptionsModal').modal('hide');
						return true;
					}
					if (!isSaved) {
						ScheduleTimeData = scheduleTime;
						$('#ScheduleOptionsModal').modal('hide');
						return true;
					}
				}
				return false;
			}
		
		}
		
		function DoCheckAll() {			
			
			var isChecked = $('.check_all').is(':checked');
			$.each($('.check'), function(index, value) {
				if (isChecked) {
					if (IsInRange(parseInt($(this).val()))) {
						$(this).attr('checked', true);
					}
					else {
						$(this).attr('checked', false);
						
					}
					$(this).attr('disabled', 'disabled');
				}
				else {
					$(this).attr('disabled', false);
				}
			});

			// if(isChecked){
			// 	$('.start-end-time').show();
			// }
			// else{
			// 	$('.start-end-time').hide();	
			// }
		}
		
		function BindScheduleOnTab(dayId, schedule) {
			$('#chkAcrossMultipleDays_' + dayId).attr('checked', true);
			// Bind start time
			$('#cboStartTimeHour_AcrossMultipleDays_' + dayId).val(parseInt(schedule.STARTHOUR_TI) % 12);
			$('#cboStartTimeMinute_AcrossMultipleDays_' + dayId).val(schedule.STARTMINUTE_TI);
			$('#cboNoonOption_StartTime_' + dayId).val(Math.floor(parseInt(schedule.STARTHOUR_TI) / 12));

			// Bind end time
			$('#cboEndTimeHour_AcrossMultipleDays_' + dayId).val(parseInt(schedule.ENDHOUR_TI) % 12);
			$('#cboEndTimeMinute_AcrossMultipleDays_' + dayId).val(schedule.ENDMINUTE_TI);
			$('#cboNoonOption_EndTime_' + dayId).val(Math.floor(parseInt(schedule.ENDHOUR_TI) / 12));
			
			// Bind blackout data
			$('#chkEnableBlachout_' + dayId).attr('checked', schedule.ENABLEDBLACKOUT_BI == 1);
			VisibleBlackout(dayId);
			if (schedule.ENABLEDBLACKOUT_BI == 1) {
				$('#cboBlackoutStartTimeHour_AcrossMultipleDays_' + dayId).val(parseInt(schedule.BLACKOUTSTARTHOUR_TI) % 12);
				/* $('#cboBlackoutStartTimeMinute_AcrossMultipleDays_' + dayId).val(schedule.BLACKOUTSTARTMINUTE_TI); */
				$('#cboNoonOption_BlackOutStartTime_' + dayId).val(Math.floor(parseInt(schedule.BLACKOUTSTARTHOUR_TI) / 12));
	
				// Bind end time
				$('#cboBlackoutEndTimeHour_AcrossMultipleDays_' + dayId).val(parseInt(schedule.BLACKOUTENDHOUR_TI) % 12);
				/* $('#cboBlackoutEndTimeMinute_AcrossMultipleDays_' + dayId).val(schedule.BLACKOUTENDMINUTE_TI); */
				$('#cboNoonOption_BlackOutEndTime_' + dayId).val(Math.floor(parseInt(schedule.BLACKOUTENDHOUR_TI) / 12));
			}
		}
		
		function VisibleBlackout(i) {
			var isBlackoutChecked = $('#chkEnableBlachout_' + i).is(':checked'); 
			if (isBlackoutChecked) {
				$("#divBlackoutTime_" + i).show();
			}
			else {
				$("#divBlackoutTime_" + i).hide();
			}			
		}
					
		function GetDayNameById(dayId) {
			switch(dayId) {
				case 0: {
					return "All";
				}
				case 1: {
					return "Sunday";
				}
				case 2: {
					return "Monday";
				}
				case 3: {
					return "Tuesday";
				}
				case 4: {
					return "Wednesday";
				}
				case 5: {
					return "Thursday";
				}
				case 6: {
					return "Friday";
				}
				default: {
					return "Saturday";
				}
			}
		}
		
		function GetDayIdBySchedule(schedule) {
			var result = -1;
			var count = 0;
			if (schedule.SUNDAY_TI == 1) {
				count++;
				result = 1;
			}
			if (schedule.MONDAY_TI == 1) {
				count++;
				result = 2;
			}
			if (schedule.TUESDAY_TI == 1) {
				count++;
				result = 3;
			}
			if (schedule.WEDNESDAY_TI == 1) {
				count++;
				result = 4;
			}
			if (schedule.THURSDAY_TI == 1) {
				count++;
				result = 5;
			}
			if (schedule.FRIDAY_TI == 1) {
				count++;
				result = 6;
			}
			if (schedule.SATURDAY_TI == 1) {
				count++;
				result = 7;
			}
			if (count == 7) {
				return 0
			}
			
			return result;
		}
		
		function GetScheduleTimeList() {
			var result = new Array();
			var inpdsdown =  2; //$('input[name=SCHEDULETYPE]:checked').val();
			if (inpdsdown == 0) { 
				var obj = new Object();
				obj.scheduleType = inpdsdown;
				obj.startDate = null;
				obj.stopDate = '';
				obj.startHour = 0;
				obj.startMinute = 0;
				obj.endHour = 0;
				obj.endMinute = 0;	
				obj.enabledBlackout = true;
				obj.blackoutStartHour = 0;
				/* obj.blackoutStartMinute = 0;	 */					
				obj.blackoutEndHour = 0;
				/* obj.blackoutEndMinute = 0; */
				obj.dayId = 0;
/* 				obj.timeZone = ""; */
				result.push(obj);
			}
			else if (inpdsdown == 1) {
				var obj = new Object();
				var startNoonOption = parseInt($('#FollowingTimeStart_noon_option').val());
				var endNoonOption = parseInt($('#FollowingTimeEnd_noon_option').val());	

				obj.scheduleType = inpdsdown;
				obj.startDate = $('#FollowingTimeDate_Year').val() + "/" + $('#FollowingTimeDate_Month').val() + "/" + $('#FollowingTimeDate_Day').val();
				//obj.stopDate = '';
				obj.stopDate = $('#FollowingTimeEndDate_Year').val() + "/" + $('#FollowingTimeEndDate_Month').val() + "/" + $('#FollowingTimeEndDate_Day').val();
				//obj.startHour = 0;
				obj.startHour = parseInt($('#FollowingTimeStart_hour').val()) + 12 * startNoonOption;
				//obj.startMinute = 0;
				obj.startMinute = parseInt($('#FollowingTimeStart_minute').val());

				//obj.endHour = 0;
				obj.endHour = parseInt($('#FollowingTimeEnd_hour').val()) + 12 * endNoonOption;

				//obj.endMinute = 0;	
				obj.endMinute = parseInt($('#FollowingTimeEnd_minute').val());

				obj.enabledBlackout = true;
				obj.blackoutStartHour = 0;
				/* obj.blackoutStartMinute = 0;	 */					
				obj.blackoutEndHour = 0;
				/* obj.blackoutEndMinute = 0; */
				obj.dayId = 0;
				/* obj.timeZone = $('#cboTimeZone_FollowingTime').val(); */
				result.push(obj);			
			}
			else {
				if ($('.check_all').is(':checked')) {
					var obj = GetScheduleTimeByTabId(0);
					obj.dayId = 0;
					obj.scheduleType = inpdsdown;
					result.push(obj);
				}
				else {
					for (var i = 1; i <= 7; ++i) {
						if ($('#chkAcrossMultipleDays_' + i).is(':checked')) {
							var obj = GetScheduleTimeByTabId(i);
							obj.dayId = i;
							obj.scheduleType = inpdsdown;
							result.push(obj);
						}
					}
				}
			}
			
			return result;
		}
		
		/*
		function VisibleDaySelector() {
			var inpdsdown =  $('input[name=SCHEDULETYPE]:checked').val();
			if (inpdsdown == 2) {

				$('#divDaySelector').show();
				if(mode !='edit' && $('.check_all').is(':checked')){
					$.each($('.check'), function(index, value) {
						$(this).prop('disabled', true);
					});
				}
			}
			else {
				$('#divDaySelector').hide();
			}
		}
		*/

		function IsBefore(hour1, minute1, hour2, minute2) {
			if (hour1 < hour2) {
				return true;
			}
			else if (hour1 == hour2) {
				return minute1 < minute2;
			}
			else {
				return false;
			}
		}
		function GetScheduleTimeByTabId(i) {
			var obj = new Object();
			obj.startDate = $('#AcrossMultipleDaysStartDate_Year').val() + "/" + $('#AcrossMultipleDaysStartDate_Month').val() + "/" + $('#AcrossMultipleDaysStartDate_Day').val();
			obj.stopDate = $('#AcrossMultipleDaysEndDate_Year').val() + "/" + $('#AcrossMultipleDaysEndDate_Month').val() + "/" + $('#AcrossMultipleDaysEndDate_Day').val();						
			if(i == 0){

				var startNoonOption = parseInt($('#FollowingTimeStart_noon_option').val());
				var endNoonOption = parseInt($('#FollowingTimeEnd_noon_option').val());	

				obj.startHour = parseInt($('#FollowingTimeStart_hour').val()) + 12 * startNoonOption;
				obj.startMinute = parseInt($('#FollowingTimeStart_minute').val());

				obj.endHour = parseInt($('#FollowingTimeEnd_hour').val()) + 12 * endNoonOption;
				obj.endMinute = parseInt($('#FollowingTimeEnd_minute').val());
			}
			else{
				obj.startHour = parseInt($('#cboStartTimeHour_AcrossMultipleDays_' + i).val())  + 12*parseInt($('#cboNoonOption_StartTime_' + i).val());
				obj.startMinute = parseInt($('#cboStartTimeMinute_AcrossMultipleDays_' + i).val());
			
				obj.endHour = parseInt($('#cboEndTimeHour_AcrossMultipleDays_' + i).val())  + 12*parseInt($('#cboNoonOption_EndTime_' + i).val());
				obj.endMinute = parseInt($('#cboEndTimeMinute_AcrossMultipleDays_' + i).val());	
			}
			
			
			obj.enabledBlackout = $('#chkEnableBlachout_' + i).is(':checked');
			if (obj.enabledBlackout) {
				obj.blackoutStartHour = parseInt($('#cboBlackoutStartTimeHour_AcrossMultipleDays_' + i).val()) + 12*parseInt($('#cboNoonOption_BlackOutStartTime_' + i).val());
				/* obj.blackoutStartMinute = parseInt($('#cboBlackoutStartTimeMinute_AcrossMultipleDays_' + i).val()); */
				
				obj.blackoutEndHour = parseInt($('#cboBlackoutEndTimeHour_AcrossMultipleDays_' + i).val())  + 12*parseInt($('#cboNoonOption_BlackOutEndTime_' + i).val());
				/* obj.blackoutEndMinute = parseInt($('#cboBlackoutStartTimeMinute_AcrossMultipleDays_' + i).val()); */
			}
			else {
				obj.blackoutStartHour = 0;
				/* obj.blackoutStartMinute = 0; */
				
				obj.blackoutEndHour = 0;
				/* obj.blackoutEndMinute = 0; */
			}
			
			return obj;
		}
		function SelectDate(selector) {
			if(selector)
			{
				var date = $(selector).val();
			
				var elements = date.split('-');
				
				$('#' + selector.id + "_Month").val(elements[0]);
				$('#' + selector.id + "_Day").val(elements[1]);
				$('#' + selector.id + "_Year").val(elements[2]);
			}
		}
		
		function CreateHourList(selectControl) {
			$(selectControl).find('option').remove();
			var initStartHour = "09";
			var customField = $(selectControl).attr('customfield');
			if (customField.indexOf('blackout') != -1) {
				if (customField.indexOf('end') != -1) {
					initStartHour = 1;
				}
				else {
					initStartHour = 11;
				}
			}
			else {
				if (customField.indexOf('end') != -1) {
					initStartHour = 7;
				}
				else {
					initStartHour = 9;
				}
			}
			$(selectControl).append(new Option("12", "0"));
			var hour;			
			for (var i = 1; i < 12; i++) {
				hour = i < 10 ? "0" + i : i;
				$(selectControl).append(new Option(hour, i, true));
			}
			$(selectControl).val(initStartHour);
		}
		
		function CreateMinuteList(selectControl) {
			$(selectControl).find('option').remove();
			var minute;
			for (var i = 0; i < 60; i++) {
				minute = i < 10 ? "0" + i : i;
				$(selectControl).append(new Option(minute, i));
			}
		}
		
		function CreateMonthList(selectControl) {
			$(selectControl).find('option').remove();
			var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
			/* $(selectControl).append(new Option("Month", "-1")); */
			
			for (var i = 0; i < months.length; i++) {
				$(selectControl).append(new Option(months[i], i + 1));
			}
		}
		
		function CreateDayList(selectControl) {
			$(selectControl).find('option').remove();
			/* $(selectControl).append(new Option("Day", "-1")); */
			var day;
			for (var i = 1; i < 32; i++) {
				day = i < 10 ? "0" + i : i;
				$(selectControl).append(new Option(day, i));
			}
		}
		
		function CreateYearList(selectControl) {
			$(selectControl).find('option').remove();
			/* $(selectControl).append(new Option("Year", "-1")); */
			
			for (var i = new Date().getFullYear() + 15; i >= 2012; i--) {
				$(selectControl).append(new Option(i, i));
			}
		}
		
		function CreateNoonList(selectControl) {
			$(selectControl).find('option').remove();
			$(selectControl).append(new Option('AM', 0));
			$(selectControl).append(new Option('PM', 1));
			
			var customField = $(selectControl).attr('customfield');
			var initValue = customField.indexOf('end') != -1 ? 1 : 0;
			$(selectControl).val(initValue);
		}
		function CreateDatePicker(controlId, isEndDate) {
			
			if (isEndDate) {
				$('#' + controlId).val(date_txt_1);
			}
			else {
				$('#' + controlId).val(date_txt_2);
			}

			$('#' + controlId).datepicker({
				numberOfMonths: 1,
				showButtonPanel: false,
				dateFormat: 'm-d-yy',
				defaultDate: $('#btnDate_FollowingTime').val(),
				showOn: 'both',
				buttonImage: "/public/images/calendar.png",
		        buttonImageOnly: true,
		        minDate : date_txt_2,
			});
			
			SelectDate(document.getElementById(controlId));
		}
		
		function setValForDatePicker(controlId){
			$('#'+controlId).datepicker("setDate", new Date($('#'+controlId+'_Year').val(), $('#'+controlId+'_Month').val() -1, $('#'+controlId+'_Day').val()) );
		}
		
		
		function SaveSchedule(scheduleTime) {

			$.ajax({
				url : '/session/sire/models/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: "POST",
				dataType: "json",
				beforeSend: function(){
					$('#processingPayment').show();
				},
				data : { 
					INPBATCHID : INPBATCHID,//$('input[name=INPBATCHID]').val(), 
					/* SUNDAY_TI : sundayVal, 
					MONDAY_TI : mondayVal, 
					TUESDAY_TI : tuesdayVal, 
					WEDNESDAY_TI : wednesdayVal, 
					THURSDAY_TI : thursdayVal, 
					FRIDAY_TI : fridayVal , 
					SATURDAY_TI : saturdayVal,  */
					LOOPLIMIT_INT : $('input[name=LOOPLIMIT_INT]').val(), 
					/* STARTHOUR_TI : $('select[name=STARTHOUR_TI]').val(),
					ENDHOUR_TI : $('select[name=ENDHOUR_TI]').val(),
					STARTMINUTE_TI : $('select[name=STARTMINUTE_TI]').val(),
					ENDMINUTE_TI : $('select[name=ENDMINUTE_TI]').val(),
					BLACKOUTSTARTHOUR_TI : $('select[name=BLACKOUTSTARTHOUR_TI]').val(),
					BLACKOUTENDHOUR_TI : $('select[name=BLACKOUTENDHOUR_TI]').val(), */
					SCHEDULETIME: JSON.stringify(scheduleTime)
				}
				
			}).done(function(d){
					//var d = jQuery.parseJSON(data);
					/* Alert if failure */												
						/* Get row 1 of results if exisits*/
						$('#processingPayment').hide();
																														
						/* Check if variable is part of JSON result string */								
						if(typeof(d.RXRESULTCODE) != "undefined")
						{							
							CurrRXResultCode = d.RXRESULTCODE;	
							
							if(CurrRXResultCode > 0)
							{					
								/*bootbox.alert(d.DATA.MESSAGE[0], function(result){
									$('#ScheduleOptionsModal').modal('hide');
									return;
								});*/
								bootbox.dialog({
								    message:d.MESSAGE,
								    title: "Update Schedule",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {
								            	$('#ScheduleOptionsModal').modal('hide');
								            }
								        }
								    }
								});	
								return;
								
							}
							else
							{	
								bootbox.dialog({
								    message:d.MESSAGE,
								    title: "Update Schedule",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {
								            	$('#ScheduleOptionsModal').modal('hide');
								            }
								        }
								    }
								});									
								
								return false;
							}
						}
						else
						{/* Invalid structure returned */	
							
						}
						
						
				});
		}
		