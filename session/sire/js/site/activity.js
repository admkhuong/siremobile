(function($){
	/* Waitlist datatable initializer */
	function GetActivityList(strFilter){

		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


		var table = $('#activityList').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
			"bSortable": false,
			"sAjaxDataProp": "ActivityList",
			"sSortDir_0": "desc",
			"aoColumns": [
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
			],
			"sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetRecentActiveFeedTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {
			},
			"fnDrawCallback": function( oSettings ) {
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				/* hide paginate section if table is empty */
				$('#activityList > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#activityList_paginate').hide();
						return false;
					}
					else{
						$('#activityList_paginate').show();
					} 
				});

				$('[data-toggle="popover"]').popover();
			},
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {

				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
				aoData.push(
					{ "name": "customFilter", "value": customFilterData}
					);

				oSettings.jqXHR = $.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			"fnInitComplete":function(oSettings){
				$('[data-toggle="popover"]').popover();
			}
		});
	}

	$(document).ready(function() {
		/* when document is ready */

		/* init waitlist datatable */
		GetActivityList();

		/* fix body shrink issue after a message model disappear */
		$(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });
	});

})(jQuery);