$(document).ready(function() {

	var selectedBatchId = 0;

  	$("#select-source-user").select2({
  		placeholder: "Search by email",
  		allowClear: true,
  		ajax: {
	    url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
	    dataType: 'json',
	    delay: 250,
	    data: function (params) {
	      return {
	        inpEmailAddress: params.term, // search term
	        page: params.page
	      };
	    },
	    processResults: function (data, params) {
	   
	    return {

	            results: $.map(data, function (item) {
                    return {
                        text: item.EMAIL,
                        id: item.ID
                    }
                })
	      };
	    },
	    cache: true
	  },
	  minimumInputLength: 3
  	});

    $("#select-target-user").select2({
  		placeholder: "Search by email",
  		allowClear: true,
  		ajax: {
	    url: "/session/sire/models/cfc/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach",
	    dataType: 'json',
	    delay: 250,
	    data: function (params) {
	      return {
	        inpEmailAddress: params.term, // search term
	        page: params.page
	      };
	    },
	    processResults: function (data, params) {
	    return {

	            results: $.map(data, function (item) {
                    return {
                        text: item.EMAIL,
                        id: item.ID,
                        name: item.NAME,
                    }
                })
	      };
	    },
	    cache: true
	  },
	  minimumInputLength: 3
  	});

    $('#select-source-user').on("select2:close", function(e) { 
    	var sourceUserId = $('#select-source-user').val();
    	var campaignArray = '';		
   		// get list batch

   		if(sourceUserId >0)
   		{
			// var sampleArray = [{id:0,text:'enhancement'}, {id:1,text:'bug'}
   //                     ,{id:2,text:'duplicate'},{id:3,text:'invalid'}
   //                     ,{id:4,text:'wontfix'}];

   //          $("#select-batch").select2({ data: sampleArray });           
   //          return false;
    		$("#select-batch").removeClass('hidden');
        	
			if( $("#select-batch").data('select2')) {
				$("#select-batch").select2('destroy');
				//$("#select-batch").select2(campaignArray, null);
				$("#select-batch option").remove();
			}

        
		    $.ajax({
		      type: "GET",
		      url: "/session/sire/models/cfc/campaign.cfc?method=GetCampaignListById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach&inpSourceUserId="+sourceUserId,
		      beforeSend: function( xhr ) {
		        $('#processingPayment').show();
		      },            
		      error: function(XMLHttpRequest, textStatus, errorThrown) {
		        $('#processingPayment').hide();
		        bootbox.dialog({
		            message: "Get Campaign List Fail",
		            title: "Campaigns",
		            buttons: {
		                success: {
		                    label: "Ok",
		                    className: "btn btn-medium btn-success-custom",
		                    callback: function() {}
		                }
		            }
		        });
		      },            
		      success:function(d){

		        /*if(d.indexOf('class="home_page"') > -1){
		          return false;
		        }*/
		        
		        $('#processingPayment').hide();
		        if(d.RXRESULTCODE ==1)
		        {
		        	campaignArray = d.CAMPAIGNLIST;
					
					$("#select-batch").select2({ data: campaignArray });   						

		        	selectedBatchId = $('#select-batch').val();	
					if(selectedBatchId > 0)
						$("#btn-preview").removeClass('hidden');		
					else
						$("#btn-preview").addClass('hidden');		
		        }
		        else{
		        	bootbox.dialog({
			            message: "Get Campaign List Fail",
			            title: "Campaigns",
			            buttons: {
			                success: {
			                    label: "Ok",
			                    className: "btn btn-medium btn-success-custom",
			                    callback: function() {
			                    	$("#select-batch").addClass('hidden');
			                    }
			                }
			            }
		       		});
		        }
		        
		      }
		    });
   		}
	});
	
	// Display preview link after select batchid
	$('#select-batch').on("select2:close", function(e) { 
		selectedBatchId = $('#select-batch').val();	
		if(selectedBatchId > 0)
			$("#btn-preview").removeClass('hidden');		
		else
			$("#btn-preview").addClass('hidden');			
	});

	// Remove select batch after unselect source user

	$('#select-source-user').on("select2:unselecting", function(e) { 
		if( $("#select-batch").data('select2')) {
			$("#select-batch").select2('destroy');
			$("#select-batch").addClass('hidden');
		} 
	});

	

	$("#btn-preview").click(function(e) {
		e.preventDefault();
		var sourceUserId = $('#select-source-user').val();

		if(selectedBatchId > 0){
	  		try{
		        $.ajax({
		          type: "GET",
		          url: '/session/sire/models/cfm/template_preview.cfm?templateid='+selectedBatchId+'&templateFlag=0&userId='+sourceUserId,   
		          beforeSend: function( xhr ) {
		            $('#processingPayment').show();
		          },            
		          error: function(XMLHttpRequest, textStatus, errorThrown) {
		            $('#processingPayment').hide();
		            bootbox.dialog({
		                message: "Get preview Fail",
		                title: "Campaigns",
		                buttons: {
		                    success: {
		                        label: "Ok",
		                        className: "btn btn-medium btn-success-custom",
		                        callback: function() {}
		                    }
		                }
		            });
		          },            
		          success:function(d){

		            if(d.indexOf('class="home_page"') > -1){
		              return false;
		            }
		            
		            $('#processingPayment').hide();
		            $('#previewCampaignModal .modal-body').html(d);
		            $('#previewCampaignModal').modal('show');
		          }
		        });
		    }catch(ex){
		        $('#processingPayment').hide();
		        bootbox.dialog({
		            message: "Get preview Fail",
		            title: "Campaigns",
		            buttons: {
		                success: {
		                    label: "Ok",
		                    className: "btn btn-medium btn-success-custom",
		                    callback: function() {}
		                }
		            }
		        });
	      	}
	    }
	    else{
	    	bootbox.dialog({
			    message: "Invalid batchId",
			    title: 'Preview Campaign',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
	    }
	});

  	$('#btn-popup-confirm').click(function() {
  		
  		var sourceUserId = $('#select-source-user').val();		
		var targetUserId = $('#select-target-user').val();
		var batchId = $("#select-batch").val();
		var ticketId = $('#txt-trouble-ticket-id').val();
		var dialogTitle = 'Copy Campaign';
		var errorContent = '';

		if(!sourceUserId)
		{
			errorContent+='Invalid source user';
		}

		if(!targetUserId)
		{
			errorContent+='<br/> Invalid target user';
		}

		if(!batchId)
		{
			errorContent+='<br/> Invalid batch';
		}

		if(errorContent != '')
		{
			bootbox.dialog({
			    message: errorContent,
			    title: dialogTitle,
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
		}
		else
			$("#user-account-update").modal('show');
		
	});

	// Copy Campaign

	$("#btn-copy-campaign").click(function()
	{	
		$("#user-account-update").modal('hide');
		var sourceUserId = $('#select-source-user').val();		
		var targetUserId = $('#select-target-user').val();
		var batchId = $("#select-batch").val();
		var ticketId = $('#txt-trouble-ticket-id').val();
		var dialogTitle = 'Copy Campaign';
		var errorContent = '';

		try{
			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/campaign.cfc?method=copyCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data: { InpSourceUserId: sourceUserId,InpTargetUserid: targetUserId ,InpCampaignId: batchId, InpTicketId:ticketId },
			beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: "Copy Fail",
				    title: dialogTitle,
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			},					  
			success:		
				function(d) {
					$('#processingPayment').hide();
					
					//goto success page
					bootbox.dialog({
				        message: d.MESSAGE,
				        title: dialogTitle,
				        buttons: {
				            success: {
				                label: "Ok",
				                className: "btn btn-medium btn-success-custom",
				                callback: function() {
				                	resetCopyBatchForm();
				                }
				            }
				        }
					});	
				} 		
			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.dialog({
		        message: "Delete Fail",
		        title: dialogTitle,
		        buttons: {
		            success: {
		                label: "Ok",
		                className: "btn btn-medium btn-success-custom",
		                callback: function() {
		                }
		            }
		        }
			});
		}												
	});

	$('#btn-reset-form').click(function() {
		resetCopyBatchForm();
	});	

	function resetCopyBatchForm(){
		document.getElementById('frm-copy-batch').reset();
		$('#select-source-user option').remove();
		$('#select-target-user option').remove();
		$("#select-batch option").remove();

		$("#select-batch").select2('destroy');
		$("#select-batch").addClass('hidden');
		$("#btn-preview").addClass('hidden');
	}	

});
