var globalMasterUser=0;
var globalMasterUserLevel=1;
function stringable(field, rules, i, options){
	var value = field.val();
	if(value.toString().toLowerCase() != "unlimited" && !(parseInt(value) >= 0))
	return "Value is not a number!";

}

(function($){
	function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }
    var token = getURLParameter('token');
    var payerid = getURLParameter('PayerID');
    var numberSMSToSend = getURLParameter('numberSMSToSend');
    var numberKeywordToSend = getURLParameter('numberKeywordToSend');
    var action = getURLParameter('action');
    var paymentResult = getURLParameter('result');
    var paymentMSG = getURLParameter('msg');

	var creditModal = $('#user-credit-update');
	var profileModal = $('#user-account-profile');
	var accountLimitModal = $('#user-account-update');

	var statusemail = true;
	var statusphone = true;
	var statusamount = true;

	$(document).ready(function($) {
		couponValid = 0;

		if(action=='finish_payment'){
            var buy_addon_title = '';
            if( parseInt(paymentResult) == 1){
                alertBox(paymentMSG, buy_addon_title,function(){
                    //location.href="/session/sire/pages/admin-account-update";
                });
            }
            else{
                alertBox(paymentMSG,buy_addon_title,function(){
                    //location.href="/session/sire/pages/admin-account-update";
                });
            }
        }

        $("#inpEndDate").datepicker({
        	inline: true,
			calendars: 4,
			extraWidth: 100,
			dateFormat: 'yy-mm-dd'
        });

		$("#user_phone").mask("(000) 000-0000");

		$("#user_email").on('input', function(event){
			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			var emailstr = $('#user_email').val();
			if (reg.test(emailstr) == false)
			{
				$('#invalidemail').show();
				statusemail = false;
				return false;
			}else{
				$('#invalidemail').hide();
				statusemail = true;
				return true;
			}

		});

		$("#user_firstname").on('input', function(event){
		var fname = $('#user_firstname').val();
		if (fname == '')
		{
			$('#invalidfname').show();
			return false;
		}else{
			$('#invalidfname').hide();
			return true;
		}

		});

		$("#user_lastname").on('input', function(event){
			var lname = $('#user_lastname').val();
			if (lname == '')
			{
				$('#invalidlname').show();
				return false;
			}else{
				$('#invalidlname').hide();
				return true;
			}
		});

		$("#user_phone").on('input', function(event){
			var reg = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
			var userphone = $('#user_phone').val();
			if (reg.test(userphone) == false)
			{
				$('#invalidphone').show();
				statusphone = false;
				return false;
			}else{
				$('#invalidphone').hide();
				statusphone = true;
				return true;
			}
		});

		$("#user_amountlimited").on('input', function(event){
			var amountcheck = $('#user_amountlimited').val();
			var regexp = /^[0-9]\d{0,9}(\.\d{1,2})?%?$/;
			if(!regexp.test(amountcheck)){
				$('#invalidnumber').show();
				statusamount = false;
				return false;
			}

			if(isNaN(amountcheck)){
				$('#invalidnumber').show();
				statusamount = false;
				return false;
			}else{
				$('#invalidnumber').hide();
				statusamount = true;
				return true;
			}
		});
		$('body').on('click', '.user-name-span', function(event){
			globalMasterUser=$(this).data("id");
			globalMasterUserLevel=  $(this).data("user-level");
			if(globalMasterUserLevel ==1)
			{
				$("#user-modal-2").modal("show");
			}
			else
			{
				$("#user-modal-3").modal("show");
			}
			InitFilterForUserTable();
			GetSubUser();
		});
    });

	$('#user-list-update').DataTable({
		"bFilter": false,
		"bDestroy":true,
		"sPaginationType": "input",
	    "bLengthChange": false,
		"iDisplayLength": 10
	});

	var GetAccountDetails = function(userId) {
        try{
			$.ajax({
				url: "/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: 'json',
				data: {InpUserId: userId},
				beforeSend: function(){
					$("#processingPayment").show();
				},
				success: function(data){
					accountLimitModal.find('#amount_of_credit').text(data.DATA.USERPLANFIRSTSMSINCLUDED[0]);
					accountLimitModal.find('#cost_per_credit').text('$'+(data.DATA.PRICEMSGAFTER[0]/100).toFixed(3));
					//accountLimitModal.find('#amount_of_promotion').text(data.DATA.PROMOTIONCREDIT[0]);
					accountLimitModal.find('#amount_of_keyword').text(data.DATA.KEYWORDSLIMITNUMBER[0]);
					accountLimitModal.find('#keyword_purchased').text(data.DATA.KEYWORDPURCHASED[0]);
					//accountLimitModal.find('#keyword_unpaid').text(data.DATA.TOTALKEYWORDHAVETOPAID[0]);
					accountLimitModal.find('#min_of_char_keyword').text(data.DATA.KEYWORDMINCHARLIMIT[0]);

					accountLimitModal.find('#amount_of_mlp_').text(data.DATA.MLPSLIMITNUMBER[0]);
					accountLimitModal.find('#amount_of_short_url_').text(data.DATA.SHORTURLSLIMITNUMBER[0]);
					accountLimitModal.find('#amount_of_image_capacity').text(data.DATA.IMAGECAPACITYLIMIT[0]);
					accountLimitModal.find('#field-to-change-plan').val(data.DATA.PLANID[0]);

					$.ajax({
						method: 'POST',
						url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentMethodSettingByUserId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
						data:{
							inpUserId: userId
						},
						dataType: 'json',
						beforeSend:function(xhr){
							$('#processingPayment').show();
						}
						,error:function(XMLHttpRequest,textStatus,errorThrown){
							alertBox("Unable to get your infomation at this time. An error occurred.","Oops!");
							$('#processingPayment').hide();
						},
						dataType: 'json',
						success: function(data) {
							if (data && data.RXRESULTCODE == 1) {
								$('#payment_method_value').val(data.RESULT);
								$(".select_card_type").show();
								$(".card-support-type").show();
								$(".inner-update-cardholder").show();
								$("#section-2.inner-update-cardholder").show();
								//

								$.ajax({
									url: "/session/sire/models/cfc/payment/vault.cfc?method=getDetailCreditCardInVault&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
									type: "POST",
									dataType: 'json',
									data: {
										inpUserId: userId,
										inpPaymentGateway:$('#payment_method_value').val()
									},
									beforeSend: function(){
										// $("#processingPayment").show();
									},
									success: function(data){
										if(data.RXRESULTCODE != 1){
											accountLimitModal.find('.select_card_type').hide();
											accountLimitModal.find('.fset_cardholder_info').show()
											$("#select_used_card_value").val(2);
										}else{
											accountLimitModal.find('.select_card_type').show();
											accountLimitModal.find('.fset_cardholder_info').hide()
											$("#select_used_card_value").val(1);
										}
									},
									complete: function(){
									}
								});
							}
						}
					});


					GetAccountUpdateLog();
				},
				complete: function(){
					$("#processingPayment").hide();
					$('#user-account-update').modal('show');
				}
			});
		}catch(e){

		}

        return false;
    }

    var GetDetailsForMonthlyPlanLimit = function(userId) {
    	try{
			$.ajax({
				url: "/public/sire/models/users.cfc?method=GetDetailsForMonthlyPlanLimit&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: 'json',
				data: {inpUserId: userId},
				beforeSend: function(){
					$("#processingPayment").show();
				},
				success: function(data){
					accountLimitModal.find('#acc-plan-name').text(data.PLANENAME + '/$' + data.MONTHLYAMOUNT);
					accountLimitModal.find('#acc-plan-active-date').text(data.ACTIVESINCE);
				},
				complete: function(){
					$("#processingPayment").hide();
				}
			});
		}catch(e){

		}

        return false;
    }

	$('#update-acc-limit').validationEngine('attach', {focusFirstField: false, autoHidePrompt: false, scroll: false, promptPosition : "topLeft"});

	$('body').on('click', 'a.user-update-btn', function(event){
		// Admin buy credit & Keyword and Plan
		event.preventDefault();
		var userID = $(this).data('id');
		var userName = $(this).data('name');
		var userEmail = $(this).data('email');
		var billingType = $(this).data('billingtype');
		accountLimitModal.find('input[name="user_id"]').val(userID);
		accountLimitModal.find('input[name="user_name"]').val(userName);
		accountLimitModal.find('input[name="user_email"]').val(userEmail);
		accountLimitModal.find('#user-name').text(userName);
		accountLimitModal.find('#user-email').text(userEmail);
		if (billingType == '2') accountLimitModal.find('.monthYearSwitch').prop('checked', true);
		else accountLimitModal.find('.monthYearSwitch').prop('checked', false);
		$('#update-acc-limit').validationEngine('hide');

		GetAccountDetails(userID);
		GetDetailsForMonthlyPlanLimit(userID);

	});


	// set up load contact limit
	$('body').on('click', 'a.user-set-limit-up-ct', function(event){
		event.preventDefault();
		var userID = $(this).data('id');
		GetUserLimitUploadContact(userID);
		$('#ModalLimitUploadContact').modal("show");
		$('#ModalLimitUploadContact').find('#hiddenUserID').val(userID);


	});
	$("#btn-limit-upload-contact").on('click',function(){
		if ($("#frm_limit_upload_contact").validationEngine('validate'))
		{
			var userID=$('#ModalLimitUploadContact').find('#hiddenUserID').val();
			inpLimitUploadContact=$('#ModalLimitUploadContact').find('#inpLimitUploadContact').val();
			var inpPermissionUpload=$('#ModalLimitUploadContact').find('#inpUploadPermission').val();

			UpdateUserLimitUploadContact(userID,inpLimitUploadContact,inpPermissionUpload);
		}
	});

	function GetUserLimitUploadContact(userID)
	{

		$.ajax({
    		url: '/session/sire/models/cfc/user-import-contact.cfc?method=GetUserMaxImportNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			async:false,
    		data: {inpUserId: userID},
    	})
    	.done(function(data) {
    		if (data.RXRESULTCODE == 1) {
				$('#ModalLimitUploadContact').find('#inpLimitUploadContact').val(data.MAXLIMIT);
				$('#ModalLimitUploadContact').find('#inpUploadPermission').val(data.UPLOADPERMISSION);

    		}
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
		});

	}

	function UpdateUserLimitUploadContact(userID,inpLimitUploadContact,inpPermissionUpload)
	{
		$.ajax({
    		url: '/session/sire/models/cfc/user-import-contact.cfc?method=UpdateUserMaxImportNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
    		data: {
				inpUserId: userID,
				inpLimitUploadContact:inpLimitUploadContact,
				inpPermissionUpload:inpPermissionUpload
			},
    	})
    	.done(function(data) {
    		if (data.RXRESULTCODE == 1) {
				alertBox('Update upload contact setting successfully','Update Successful','');
    			$('#ModalLimitUploadContact').modal("hide");
    		}
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
		});
	}

	$('input.stringable').on('input', function(event){
		var target = $(this).data('target');
		var targetInput = $('input[name="'+target+'"]');
		if(this.value.toString().toLowerCase() == 'unlimited') targetInput.val(0);
		else targetInput.val(this.value);
	});

	$('body').on('click', 'a.btn-renew-plan', function(event){
		event.preventDefault();
		var userID = $(this).data('id');
		$('#ModalRenewPlan').modal("show");
		$('#ModalRenewPlan').find('#hiddenUserID').val(userID);
		GetUserPlan(userID);
	});
	$("#btn-save-renew-plan").on('click',function(){
		if ($("#frm_renew_plan").validationEngine('validate'))
		{
			var userID=$('#ModalRenewPlan').find('#hiddenUserID').val();
			var inpFirstSMSIncluded =$('#ModalRenewPlan').find('#inpFirstSMSIncluded').val();
			var inpKeyWordLimit=$('#ModalRenewPlan').find('#inpKeyWordLimit').val();
			var inpEndDate=$('#ModalRenewPlan').find('#inpEndDate').val();

			var selectedDate = new Date(inpEndDate);
			var selectedDay = selectedDate.getDate();
			var selectedMonth = (parseInt(selectedDate.getMonth()) + 1);
			var selectedYear = selectedDate.getFullYear();

			// Case the day selected in 29th, 30th, 31st will be set 28th.
			if(parseInt(selectedDay) > 28){
				confirmBox('The day you have selected in 29th, 30th, 31st will be set 28th. Are you sure you want to renew this plan?', 'NOTICE!', function(){
					selectedDay = 28;
					selectedDate = (selectedYear + '-' + selectedMonth + '-' + selectedDay);
					RenewPlan(userID, inpFirstSMSIncluded, inpKeyWordLimit, selectedDate);
				});
			}
			else{
				RenewPlan(userID, inpFirstSMSIncluded, inpKeyWordLimit, inpEndDate);
			}
		}
	});

	function RenewPlan(userID, inpFirstSMSIncluded, inpKeyWordLimit, inpEndDate)
	{
		$.ajax({
    		url: '/session/sire/models/cfc/order_plan.cfc?method=RenewUserPlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
    		data: {
				inpUserId: userID,
				inpFirstSMSIncluded:inpFirstSMSIncluded,
				inpKeyWordLimit:inpKeyWordLimit,
				inpEndDate:inpEndDate
			},
    	})
    	.done(function(data) {
    		if (data.RXRESULTCODE == 1) {
				alertBox('Renew user plan successfully','Update Successful','');
    			$('#ModalRenewPlan').modal("hide");
    		}
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
		});
	}


	function GetUserPlan(userID)
	{
		// Clear form first
		$('#ModalRenewPlan').find('#inpFirstSMSIncluded').val('');
		$('#ModalRenewPlan').find('#inpKeyWordLimit').val('');
		$('#ModalRenewPlan').find('#inpEndDate').val('');
		$.ajax({
    		url: '/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
    		data: {InpUserId: userID},
    	})
    	.done(function(data) {
    		console.log(data.DATA)   		;
    		$('#ModalRenewPlan').find('#inpFirstSMSIncluded').val(data.DATA.USERPLANFIRSTSMSINCLUDED);
			$('#ModalRenewPlan').find('#inpKeyWordLimit').val(data.DATA.KEYWORDSLIMITNUMBER);
			var currentDate = new Date(data.DATA.ENDDATE);
			var year = currentDate.getFullYear();
			var month = (parseInt(currentDate.getMonth()) + 1);
			var day = currentDate.getDate();
			if(month < 10){
				month = "0" + month;
			}
			if(day < 10){
				day = "0" + day;
			}


			$('#ModalRenewPlan').find('#inpEndDate').val(year + "-" + month + "-" + day);
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
		});
	}

	$('body').on('submit', '#update-acc-limit', function(event){
		event.preventDefault();

		if ($("#update-acc-limit").validationEngine('validate')) {
			var inpUserId = accountLimitModal.find('input[name="user_id"]').val();
			//var inpValue = accountLimitModal.find('input[name="value"]').val();
			var inpType = accountLimitModal.find('select[name="field-type"]').val();
			var inpUserEmail = accountLimitModal.find('input[name="user_email"]').val();
			var payment_method_value = $('#payment_method_value').val();

			switch(inpType) {
				case '7':
					var inpValue = accountLimitModal.find('select[name="value"]').val();
					break;
				default :
					var inpValue = accountLimitModal.find('input[name="value"]').val();
					break;
			}

			if(inpValue == "Unlimited" && (inpType == 4 || inpType == 5)){
				inpValue = 0;
			}
			else if (inpValue == "Unlimited" && (inpType < 4 || inpType > 5)){
				inpValue = -1;
			}
			else if (inpValue > 0){
				inpValue = inpValue;
			}
			else if (inpValue == 0 && inpType==10){
				inpValue = inpValue;
			}
			else{
				inpValue = -1;
			}
			var updateData = {
					inpUserId: inpUserId,
					inpValue: inpValue,
					inpType: inpType,
					inpUserEmail: inpUserEmail
				};

			if (inpType == '7' || inpType == '8' || inpType == '9' ) {
				updateData.expirationDate = update_acc_limit.find('.expiration_date_month_upgrade').val() + '/' + update_acc_limit.find('.expiration_date_year_upgrade').val();
				updateData = $.param(updateData) + '&' + update_acc_limit.serialize();

				//if(payment_method_value == 2)
			}
			$("#processingPayment").show();


			if((inpType == '7' || inpType == '8' || inpType == '9') && $('.payopt').is(':checked')){
				var customAmount = accountLimitModal.find('input[name="inpCustomAmount"]').val();
				if(inpType == '7'){// Admin buy Plan
					upgradePlanProcess(update_acc_limit,customAmount,inpValue,inpUserId,inpType,inpUserEmail);
				}else{ // Admin buy Credit & Keyword
					DoAdminPurchasedCreditKeywordWithSaveCC(update_acc_limit,payment_method_value,customAmount,inpValue,inpUserId,inpType,inpUserEmail);
				}
			}
			else{
				$.ajax({
					url: "/session/sire/models/cfc/users.cfc?method=UpdateUserAccountLimitationNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
					type: "POST",
					dataType: "json",
					async: false,
					data: updateData,
					beforeSend: function(){
						$("#processingPayment").show();
					},
					success: function(rspData){
						$("#processingPayment").hide();
						if(rspData.RXRESULTCODE == 1){
							alertBox(rspData.MESSAGE, 'Update Result', function(){
								if($('input.filter-form-control').val()!= ''){
				            		$('.btn-apply-filter').trigger('click');
				            	}
				            	else{
				            		var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
				            		GetUserList([], getStartFrom);
				            	}
				            	GetAccountDetails(inpUserId);
				            	GetDetailsForMonthlyPlanLimit(inpUserId);
								GetAccountUpdateLog();
								$('#field-to-change').val('');
								location.href = '/session/sire/pages/admin-account-update';
							});
						}
						else{
							alertBox(rspData.MESSAGE, 'Update Result', function(){
								$('#field-to-change').val('');
							});
						}
						if(inpType==7 || inpType==8 || inpType==9)
						{
							$( ".payopt" ).prop( "checked", false );
							$(".inner-update-cardholder").hide();
						}
					}
				});
			}
		}
	});

	function DoAdminPurchasedCreditKeywordWithSaveCC(data,paymentGateway,customAmount,inpValue,inpUserId,inpType,inpUserEmail){
        var result = { };
        $.each(data.serializeArray(), function() {
            result[this.name] = this.value;
		});
		var planId = 0;
		var numberSMSToSend = 0;
		var numberKeywordToSend = 0;

		if(inpType == 7){
			var planId = inpValue;
		}else if(inpType == 8){
			var numberSMSToSend = inpValue;
		}else{
			var numberKeywordToSend = inpValue;
		}
		var monthYearSwitch = result.monthYearSwitch;
		var h_email = result.user_email;
		var h_email_save = result.email;
        var payment_method = result.payment_method;
        var number = result.number;
        var cvv = result.cvv;
        var expirationDate = update_acc_limit.find('.expiration_date_month_upgrade').val() + '' + update_acc_limit.find('.expiration_date_year_upgrade').val();
        var firstName = result.firstName;
        var lastName = result.lastName;
        var city = result.city;
        var line1 = result.line1;
        var state = result.state;
        var zip = result.zip;
        var country = result.country;
        var phone = result.phone;
		var email = result.email;
		var inpexpiremonth = update_acc_limit.find('.expiration_date_month_upgrade').val();
		var inpexpireyear = update_acc_limit.find('.expiration_date_year_upgrade').val();

		// var selectusercard = result.select_used_card;
		var selectusercard = update_acc_limit.find('#select_used_card_value').val();

		if(typeof(result.save_cc_info)!='undefined'){
			var saveccinfo = result.save_cc_info;
		}else{
			var saveccinfo = 0;
		}

		var paymentdata ={
            'planId': planId,
            'monthYearSwitch': monthYearSwitch,
            'numberSMSToSend': numberSMSToSend,
            'numberKeywordToSend': numberKeywordToSend,
            'select_used_card': selectusercard,
            'h_email': h_email,
            'h_email_save': h_email_save,
            'payment_method': payment_method,
            'inpNumber': number,
            'inpCvv2': cvv,
            'expirationDate': expirationDate,
            'inpFirstName': firstName,
            'inpLastName': lastName,
            'inpCity': city,
            'inpLine1': line1,
            'inpState': state,
            'inpPostalCode': zip,
            'inpCountryCode': country,
            'phone': phone,
            'inpEmail': email,
			'paymentGateway': result.select_payment_method,
			'save_cc_info': saveccinfo,
			'inpExpireMonth': inpexpiremonth,
			'inpExpreYear': inpexpireyear
        };

		var jsondatatemp = JSON.stringify(paymentdata);

        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=DoAdminPurchasedCreditKeywordWithSaveCC&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data: {
				inpUserId:	inpUserId,
                inpOrderAmount: customAmount,
                inpPaymentdata: jsondatatemp,
                inpPaymentGateway: paymentGateway,
                inpSelectUsedCard: selectusercard,
                inpSaveCCInfo: saveccinfo
            },
            dataType: 'json',
                //timeout: 6000,
                success: function(data) {
                $("#actionStatus").val('Make Payment');
                $("#processingPayment").hide();
                if (data.RXRESULTCODE == 1) {
                    alertBox(data.MESSAGE, "Update Result", function(){
                        UIkit.modal('#buyaddon-modal-sections')[0].toggle();
                        location.href = '/session/sire/pages/admin-account-update';
                    });
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                } else {
                    alertBox(data.MESSAGE, "Update Result");
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                }
            }
        });
	}

	function upgradePlanProcess(datainfo,planAmount,inpValue,inpUserId,inpType,inpUserEmail){

		var buy_addon_title = "Upgrade Plan" ;
		var result = { };

		$.each(datainfo.serializeArray(), function() {
			result[this.name] = this.value;
		});

		var planId = inpValue;
		var inpPromotionCode = result.inpPromotionCode;

		if(typeof result.monthYearSwitch != "undefined")
			var monthYearSwitch = result.monthYearSwitch;
		else
			var monthYearSwitch = result.monthYearSwitchAdmin;

		if(typeof result.expirationDate != "undefined")
			var expirationDate = result.expirationDate;
		else
			var expirationDate = result.expiration_date_admin;

		if(typeof result.expiration_date_month_upgrade != "undefined")
			var expiration_date_month_upgrade = result.expiration_date_month_upgrade;
		else
			var expiration_date_month_upgrade = result.expiration_date_month_admin;

		if(typeof result.expiration_date_year_upgrade != "undefined")
			var expiration_date_year_upgrade = result.expiration_date_year_upgrade;
		else
			var expiration_date_year_upgrade = result.expiration_date_year_admin;

		if(inpUserEmail)
			var email = inpUserEmail;
		else
			var email = result.email;

		var listKeyword =  "";
		var listMLP =  "";
		var listShortUrl =  "";
		// var selectusercard = result.select_used_card;
		var selectusercard = update_acc_limit.find('#select_used_card_value').val();
		var saveccinfo = result.save_cc_info;
		var numberSMSToSend = result.numberSMSToSend;
		var numberKeywordToSend = result.numberKeywordToSend;
		var select_used_card = result.select_used_card;
		if(result.h_email){
			var h_email = result.h_email;
		}else{
			var h_email = email;
		}
		if(result.h_email_save){
			var h_email_save = result.h_email_save;
		}else{
			var h_email_save = email;
		}
		var payment_method = result.payment_method;
		var number = result.number;
		var cvv = result.cvv;
		//var expirationDate = update_acc_limit.find('.expiration_date_month_upgrade').val() + '' + update_acc_limit.find('.expiration_date_year_upgrade').val();
		var firstName = result.firstName;
		var lastName = result.lastName;
		var city = result.city;
		var line1 = result.line1;
		var state = result.state;
		var zip = result.zip;
		var country = result.country;
		var phone = result.phone;


		var plandata ={
			'planId': planId,
			'inpPromotionCode': inpPromotionCode,
			'monthYearSwitch': monthYearSwitch,
			'listKeyword':  listKeyword,
			'listMLP':  listMLP,
			'listShortUrl': listShortUrl,
			'numberSMSToSend': numberSMSToSend,
			'numberKeywordToSend': numberKeywordToSend,
			'select_used_card': select_used_card,
			'h_email': h_email,
			'h_email_save': h_email_save,
			'payment_method': payment_method,
			'inpNumber': number,
			'inpCvv2': cvv,
			'expirationDate': expirationDate,
			'inpFirstName': firstName,
			'inpLastName': lastName,
			'inpCity': city,
			'inpLine1': line1,
			'inpState': state,
			'inpPostalCode': zip,
			'inpCountryCode': country,
			'phone': phone,
			'inpEmail': email,
			'paymentGateway': $('#payment_method_value').val(),
			'inpExpireMonth': expiration_date_month_upgrade,
			'inpExpreYear' : expiration_date_year_upgrade
		};

		var jsondata = JSON.stringify(plandata);
		$.ajax({
			method: 'POST',
			url: '/session/sire/models/cfc/billing.cfc?method=DoAdminPurchasedBuyPlanWithSaveCC&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
			data: {
				inpUserId:	inpUserId,
				inpOrderAmount: planAmount,
				inpPaymentdata: jsondata,
				inpPaymentGateway: $('#payment_method_value').val(),
				inpSelectUsedCard: selectusercard,
				inpSaveCCInfo: saveccinfo
			},
			dataType: 'json',
			success: function(data) {
				$("#processingPayment").hide();
				if (data && data.RXRESULTCODE == 1) {
					alertBox(data.MESSAGE, buy_addon_title, function(){
						//location.href = '/session/sire/pages/my-plan';
						var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
						GetUserList([], getStartFrom);
						$('#user-account-update').modal('hide');
						$('#buyaddon-modal-sections').hide();
						location.href = '/session/sire/pages/admin-account-update';
					});
				} else {
					alertBox(data.ERRMESSAGE, buy_addon_title);
					$('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
				}
			}
		});
	}

	function buyKeywordCreditPlanPP(formData,customAmount,value,userId,type,userEmail){
        var result = { };
        $.each(formData.serializeArray(), function() {
            result[this.name] = this.value;
        });

        if(result.monthYearSwitch)
        	var monthYearSwitch = result.monthYearSwitch;
        else
        	var monthYearSwitch = '';
        var data ={
            'customAmount': customAmount,
            'value':value,
            'userId': userId,
            'type' : type
        };
        var returnUrlParam = "&userId="+userId+"&value="+value+"&monthYearSwitch="+monthYearSwitch+"&type="+type;
        var jsondata = JSON.stringify(data);
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=SetPPExpressCheckout&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data:{
                inpAmount: customAmount,
                inpPaymentDescription: jsondata,
                inpCancelUrl:"http://"+window.location.hostname+"/session/sire/pages/admin-account-update",
                inpReturnlUrl:"http://"+window.location.hostname+"/session/sire/models/cfm/paypal_process?action=admin_buy_credits_keywords"+returnUrlParam
            },
            dataType: 'json',
            success: function(data) {
                $("#processingPayment").hide();
                if (data && data.RXRESULTCODE == 1) {
                    var url_direct = paypalCheckoutUrl+data.RESULT;
                    location.href = url_direct;
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                } else {
                    alertBox(data.MSG, buy_addon_title);
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                }
            }
        });
    }

	accountLimitModal.find('select[name="field-type"]').on('change', function(){
		if($(this).val() == 7) {
			$('#field-to-change').hide().attr('name', '');
			$('#field-to-change-plan').show().attr('name', 'value');
			if (update_acc_limit.find('.monthYearSwitch').is(':checked')) {
				var price = parseFloat($('#field-to-change-plan').find(':selected').data('yearprice'));
			} else {
				var price = parseFloat($('#field-to-change-plan').find(':selected').data('price'));
			}
			if (price > 0) {
				accountLimitModal.find('.inpCustomAmount').val(price);
				//accountLimitModal.find('.fieldset-payment').show();
				accountLimitModal.find('.monthly-yearly-update').show();
				accountLimitModal.find('.monthly_price .uk-text-bold').text('Monthly Amount:');
			} else {
				//accountLimitModal.find('.fieldset-payment').hide();
			}
			if( parseFloat(update_acc_limit.find(':selected').data('yearprice')) == 0 && parseFloat(update_acc_limit.find(':selected').data('price')) == 0)
			{
				accountLimitModal.find('.fieldset-payment').hide();
			}
			else
			{
				accountLimitModal.find('.fieldset-payment').show();
				//Set default payment is uncheck
				$( ".payopt" ).prop( "checked", false );
			}
			$('.fieldset-payment').find('.inner-update-cardholder').hide();
		}
		else if($(this).val() == 8 || $(this).val() == 9){
			$('#field-to-change-plan').hide().attr('name', '');
			//show check payment
			accountLimitModal.find('.fieldset-payment').show();
			//Set default payment is uncheck
			$( ".payopt" ).prop( "checked", false );
			$('.fieldset-payment').find('.inner-update-cardholder').hide();
			$('#field-to-change').show().attr('name', 'value');
			accountLimitModal.find('.monthly-yearly-update').hide();
			accountLimitModal.find('.monthly_price .uk-text-bold').text('Amount:');
			accountLimitModal.find('.inpCustomAmount').val('');
		}
		else {
			$('#field-to-change-plan').hide().attr('name', '');
			accountLimitModal.find('.fieldset-payment').hide();
			$('#field-to-change').show().attr('name', 'value');

			if(($(this).val() < 4 || $(this).val() > 5) && $(this).val() != 2 ){
				accountLimitModal.find('input[name="value"]').prop('class', 'form-control validate[required,custom[onlyNumber],maxSize[9]]');
				accountLimitModal.find('input[name="value"]').prop('maxlength', '9');
			}
			else if($(this).val() == 2){
				accountLimitModal.find('input[name="value"]').prop('class', 'form-control validate[required,custom[number],maxSize[5]]');
				accountLimitModal.find('input[name="value"]').prop('maxlength', '5');
			}
			else{
				accountLimitModal.find('input[name="value"]').prop('class', 'form-control validate[required,custom[onlyLetterNumber],maxSize[9]]');
				accountLimitModal.find('input[name="value"]').prop('maxlength', '9');
			}
		}
	}).trigger('change');

	$('#field-to-change-plan').change(function(){
		if (update_acc_limit.find('.monthYearSwitch').is(':checked')) {
			var price = parseFloat($(this).find(':selected').data('yearprice'));
		} else {
			var price = parseFloat($(this).find(':selected').data('price'));
		}
		if (price > 0) {
			accountLimitModal.find('.inpCustomAmount').val(price);
			//accountLimitModal.find('.fieldset-payment').show();
		} else {
			//accountLimitModal.find('.fieldset-payment').hide();
		}
		if( parseFloat($(this).find(':selected').data('yearprice')) == 0 && parseFloat($(this).find(':selected').data('price')) == 0)
		{
			accountLimitModal.find('.fieldset-payment').hide();
			accountLimitModal.find(".payopt").prop('checked', false).triggerHandler("click");

		}
		else
		{
			accountLimitModal.find('.fieldset-payment').show();
		}
	});

	$('#cancel-button').click(function(event) {
		$('#update-acc-limit').validationEngine('hide');
	});

	Number.prototype.numberFormat = function(c, d, t){
		var n = this,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	$('input[name="amount_of_image_capacity"]').on('change keyup', function(event){
		var numberString = parseFloat($(this).val()/1024).numberFormat();
		$('input[name="mb_unit"]').val(numberString);
	})


		function GetUserList(strFilter, startFrom){
			var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
			var actionCol = function(object){
				//fa-address-card

					// Start new Control
					var strReturn = $('<a title="Edit account profile" class="btn-edit-account-profile btn btn-default btn-edit-cp-new"  style="color: #5c5c5c;">Edit</a>')
											.attr('data-id', object.ID)
											.attr('data-name', object.NAME)
											.attr('data-phone',object.PHONE)
											.attr('data-email', object.EMAIL)[0].outerHTML


					+
						'<div class="dropdown action-cp-dropdown-list">' + '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu">';



					strReturn +=  '<li>' + $('<a title="Edit account plan" class="simon-item '+(parseInt(object.STATUS) > 0 ? "user-update-btn" : "")+'" '+(parseInt(object.STATUS) > 0 ? "" : "disabled")+'><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit account plan</a>')
								.attr('data-id', object.ID)
								.attr('data-name', object.NAME)
								.attr('data-email', object.EMAIL)
								.attr('data-billingtype',  object.BILLINGTYPE)[0].outerHTML + '</li>' +


								'<li>' + $('<a class="user-set-limit-up-ct" title="Upload Contact Setting"><i class="fa fa-upload" aria-hidden="true"></i>Upload Contact Setting</a>')
								.attr('data-id', object.ID)[0].outerHTML + '</li>' +


								'<li>' + $('<a class="simon-item '+(parseInt(object.STATUS) > 0 ? "deactive" : "active")+'-account-btn" title="'+(parseInt(object.STATUS) > 0 ? 'Deactive account' : 'Active account')+'"><i class="fa fa-'+(parseInt(object.STATUS) > 0 ? "lock" : "unlock")+'" aria-hidden="true"></i>'+(parseInt(object.STATUS) > 0 ? 'Deactive account' : 'Active account')+'</a>')
								.attr('data-id', object.ID)
								.attr('data-name', object.NAME)
								.attr('data-email', object.EMAIL)
								.attr('data-status', object.STATUS)[0].outerHTML

								+ '</li>';



					if (parseInt(object.STATUS) == 1) {
						strReturn +=  '<li>' + $('<a class="btn-credit-action" title="Adjust billing credit"><i class="fa fa-usd" style="font-size:15px" aria-hidden="true"></i>Adjust billing credit</a>')
											.attr('data-id', object.ID)
											.attr('data-name', object.NAME)
											.attr('data-email', object.EMAIL)
											.attr('data-status', object.STATUS)[0].outerHTML + '</li>';

					}
					if (parseInt(object.ISTESTACCOUNT) == 0) {
						strReturn += '<li>' + $('<a class="btn-mark-test-account" title="Mark as test account"><i class="fa fa-user" style="font-size:15px" aria-hidden="true"></i>Mark as test account</a>')
											.attr('data-id', object.ID)
											.attr('data-name', object.NAME)
											.attr('data-email', object.EMAIL)
											.attr('data-status', object.STATUS)[0].outerHTML + '</li>';
					} else {
						strReturn += '<li>' + $('<a class="btn-unmark-test-account" title="Unmark as test account"><i class="fa fa-bug" style="font-size:15px" aria-hidden="true"></i>Unmark as test account</a>')
											.attr('data-id', object.ID)
											.attr('data-name', object.NAME)
											.attr('data-email', object.EMAIL)
											.attr('data-status', object.STATUS)[0].outerHTML + '</li>';
					}

					if (!object.USECUSTOMPLAN) {

						if (parseInt(object.PLANID) != 1 && object.USERDOWNGRADEDATE == '' && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1) ) {
							strReturn += '<li>' + $('<a class="btn-downgrade-acc" title="Downgrade account"><i class="fa fa-arrow-down" style="font-size:15px"  aria-hidden="true"></i>Downgrade account</a>')
												.attr('data-id', object.ID)
												.attr('data-email', object.EMAIL)
												.attr('data-name', object.NAME)
												.attr('data-planid', object.PLANID)
												.attr('data-enddate', object.ENDDATE)[0].outerHTML + '</li>';
						}
						else if(parseInt(object.PLANID) != 1 && object.USERDOWNGRADEDATE != '' && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1)){

							strReturn += '<li>' + $('<a class="btn-canceldowngrade-acc" title="Cancel Downgrade account"><i class="fa fa-arrow-down" style="font-size:15px"  aria-hidden="true"></i>Cancel Downgrade account</a>')
													.attr('data-id', object.ID)
													.attr('data-email', object.EMAIL)
													.attr('data-name', object.NAME)
													.attr('data-planid', object.PLANID)
													.attr('data-enddate', object.ENDDATE)[0].outerHTML + '</li>';
						}
						if(parseInt(object.PLANID) != 3 && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1)){
							strReturn += '<li>' + $('<a class="btn-upgrade-acc" title="Upgrade account"><i class="fa fa-arrow-up" style="font-size:15px"  aria-hidden="true"></i>Upgrade account</a>')
												.attr('data-id', object.ID)
												.attr('data-billingtype', object.BILLINGTYPE)
												.attr('data-pricemsg', object.PRICEMSGAFTER)
												.attr('data-email', object.EMAIL)
												.attr('data-planid', object.PLANID)[0].outerHTML + '</li>';
						}

					}

					strReturn += '<li>' + $('<a class="btn-account-report" title="Account Report"><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i>Account Report</a>')
						.attr('data-id', object.ID)
						.attr('data-name', object.NAME)
						.attr('data-email', object.EMAIL)
						.attr('data-status', object.STATUS)[0].outerHTML + '</li>';


					if(object.STATUS > 0 && object.ISTESTACCOUNT ==0){
						strReturn += '<li>' + $('<a class="btn-apply-affiliate" title="Apply Affiliate Code"><i class="fa fa-code-fork" style="font-size:15px" aria-hidden="true"></i>Apply Affiliate Code</a>')
							.attr('data-id', object.ID)[0].outerHTML + '</li>';

						strReturn += '<li>' + $('<a class="btn-commission-setting" title="Commission Setting"><i class="fa Example of cogs fa-cogs" style="font-size:15px" aria-hidden="true"></i>Commission Setting</a>')
							.attr('data-id', object.ID)[0].outerHTML + '</li>';


						if(object.AFFILIATE_CODE_ASSIGN !== "")
						{
							strReturn += '<li>' + $('<a class="btn-affiliate-coupon" title="Affiliate Linkage Coupon" data-affiliate-id-assign="'+object.AFFILIATE_ID_ASSIGN+'" data-affiliate-code-assign="'+object.AFFILIATE_CODE_ASSIGN+'"><i class="fa fa-link" style="font-size:15px" aria-hidden="true"></i>Affiliate Linkage Coupon</a>')
						 	.attr('data-id', object.ID)[0].outerHTML + '</li>';
						}
					}

					// List key word
					strReturn += '<li>' + $('<a class="btn-list-keyword" title="List Keyword"><i class="fa fa-list" style="font-size:15px" aria-hidden="true"></i>List Keyword</a>')
							.attr('data-shortcodeid', object.SHORTCODEID)
							.attr('data-userid', object.ID)
							.attr('data-shortcodevch', object.SHORTCODEVCH)[0].outerHTML + '</li>';

					// renew plan
					if (parseInt(object.STATUS) == 1) {
						strReturn += '<li>' + $('<a class="btn-renew-plan" title="Admin renew plan"><i class="fa fa-paper-plane-o" style="font-size:15px" aria-hidden="true"></i>Renew plan</a>')
							.attr('data-id', object.ID)[0].outerHTML + '</li>';
					}

					strReturn += '</ul></div>';
					// End of new Control

				// Start old control
				//fa-address-card
				// var strReturn =  $('<a title="Edit account profile" class="btn-edit-account-profile"><i class="fa fa-address-card" aria-hidden="true"></i>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-phone',object.PHONE)
				// 						.attr('data-email', object.EMAIL)[0].outerHTML;

				// strReturn +=  $('<a title="Edit account plan" class="simon-item '+(parseInt(object.STATUS) > 0 ? "user-update-btn" : "")+'" '+(parseInt(object.STATUS) > 0 ? "" : "disabled")+'><i class="fa fa-pencil-square-o" aria-hidden="true"></i>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-email', object.EMAIL)
				// 						.attr('data-billingtype',  object.BILLINGTYPE)[0].outerHTML +
				// 						$('<a class="user-set-limit-up-ct" title="Upload Contact Setting"><i class="fa fa-upload" aria-hidden="true"></i></a>')
				// 						.attr('data-id', object.ID)[0].outerHTML+
				// 						$('<a class="simon-item '+(parseInt(object.STATUS) > 0 ? "deactive" : "active")+'-account-btn" title="'+(parseInt(object.STATUS) > 0 ? "Deactive account" : "Active account")+'"><i class="fa fa-'+(parseInt(object.STATUS) > 0 ? "lock" : "unlock")+'" aria-hidden="true"></i></a>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-email', object.EMAIL)
				// 						.attr('data-status', object.STATUS)[0].outerHTML;
				// if (parseInt(object.STATUS) == 1) {
				// 	strReturn += $('<a class="btn-credit-action" title="Adjust billing credit"><i class="fa fa-usd" style="font-size:15px" aria-hidden="true"></i></a>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-email', object.EMAIL)
				// 						.attr('data-status', object.STATUS)[0].outerHTML;
				// }
				// if (parseInt(object.ISTESTACCOUNT) == 0) {
				// 	strReturn += $('<a class="btn-mark-test-account" title="Mark as test account"><i class="fa fa-user" style="font-size:15px" aria-hidden="true"></i></a>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-email', object.EMAIL)
				// 						.attr('data-status', object.STATUS)[0].outerHTML
				// } else {
				// 	strReturn += $('<a class="btn-unmark-test-account" title="Unmark as test account"><i class="fa fa-bug" style="font-size:15px" aria-hidden="true"></i></a>')
				// 						.attr('data-id', object.ID)
				// 						.attr('data-name', object.NAME)
				// 						.attr('data-email', object.EMAIL)
				// 						.attr('data-status', object.STATUS)[0].outerHTML
				// }

				// if (!object.USECUSTOMPLAN) {

				// 	if (parseInt(object.PLANID) != 1 && object.USERDOWNGRADEDATE == '' && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1) ) {
				// 		strReturn += $('<a class="btn-downgrade-acc" title="Downgrade account"><i class="fa fa-arrow-down" style="font-size:15px"  aria-hidden="true"></i></a>')
				// 							.attr('data-id', object.ID)
				// 							.attr('data-email', object.EMAIL)
				// 							.attr('data-name', object.NAME)
				// 							.attr('data-planid', object.PLANID)
				// 							.attr('data-enddate', object.ENDDATE)[0].outerHTML
				// 	}
				// 	else if(parseInt(object.PLANID) != 1 && object.USERDOWNGRADEDATE != '' && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1)){

				// 		strReturn += $('<a class="btn-canceldowngrade-acc" title="Cancel Downgrade account"><i class="fa fa-arrow-down" style="font-size:15px"  aria-hidden="true"></i></a>')
				// 								.attr('data-id', object.ID)
				// 								.attr('data-email', object.EMAIL)
				// 								.attr('data-name', object.NAME)
				// 								.attr('data-planid', object.PLANID)
				// 								.attr('data-enddate', object.ENDDATE)[0].outerHTML
				// 	}
				// 	if(parseInt(object.PLANID) != 3 && (object.USERPLANSTATUS ==0 || object.USERPLANSTATUS==1)){
				// 		strReturn += $('<a class="btn-upgrade-acc" title="Upgrade account"><i class="fa fa-arrow-up" style="font-size:15px"  aria-hidden="true"></i></a>')
				// 							.attr('data-id', object.ID)
				// 							.attr('data-billingtype', object.BILLINGTYPE)
				// 							.attr('data-pricemsg', object.PRICEMSGAFTER)
				// 							.attr('data-email', object.EMAIL)
				// 							.attr('data-planid', object.PLANID)[0].outerHTML
				// 	}

				// }

				// strReturn += $('<a class="btn-account-report" title="Account Report"><i class="fa fa-bar-chart" style="font-size:15px" aria-hidden="true"></i></a>')
				// 	.attr('data-id', object.ID)
				// 	.attr('data-name', object.NAME)
				// 	.attr('data-email', object.EMAIL)
				// 	.attr('data-status', object.STATUS)[0].outerHTML
				// if(object.STATUS > 0 && object.ISTESTACCOUNT ==0){
				// 	strReturn += $('<a class="btn-apply-affiliate" title="Apply Affiliate Code"><i class="fa fa-code-fork" style="font-size:15px" aria-hidden="true"></i></a>')
				// 		.attr('data-id', object.ID)[0].outerHTML
				// 	strReturn += $('<a class="btn-commission-setting" title="Commission Setting"><i class="fa Example of cogs fa-cogs" style="font-size:15px" aria-hidden="true"></i></a>')
				// 		.attr('data-id', object.ID)[0].outerHTML
				// }
				// End of old control

				return strReturn;
			},
			statusCol = function(object){
				return object.STATUS > 0 ? "Active": "Deactivated" ;
			},

			mlpCapCol = function(object){
				return (object.MLP_IMAGE_CAP/1048576).numberFormat();
			},
			totalImageCol = function(object){
				return (object.TOTAL_IMAGE_SIZE/1048576).numberFormat();
			},
			nameCol = function(object){
				return "<span class='user-name-span' data-user-level='1'  data-id='"+ object.ID+"'>"+ object.NAME+"</span>" ;
			};
			var formatContactString = function(object){
				return '<span class="format-phone-number">'+object.PHONE+'</span>';
			}

			var table = $('#tblListEMS').dataTable({
				"bStateSave": false,
				"iStateDuration": -1,
				"fnStateLoadParams": function (oSettings, oData) {

				},
				"fnStateSaveParams": function (oSettings, oData) {

				},
				"fnDrawCallback": function( oSettings ) {
					$('.action-cp-dropdown-list').on('show.bs.dropdown', function () {
						 $("#tableaccountmanagement .table-responsive #tblListEMS_wrapper").attr('style','margin-bottom: '+($(this).find(".dropdown-menu").outerHeight()+20)+'px;');
					});
					$('.action-cp-dropdown-list').on('hidden.bs.dropdown', function () {
						$("#tableaccountmanagement .table-responsive #tblListEMS_wrapper").attr('style','margin-bottom: 0;');
					});
		    	},
				"bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "input",
			    "bLengthChange": false,
				"iDisplayLength": 20,
				"iDisplayStart": startFrom,
				"bAutoWidth": false,
			    "aoColumns": [
					{"mData": "ID", "sName": 'u.userid_int', "sTitle": 'ID',"bSortable": false,"sClass":"user-id", "sWidth":"80px"},
					{"mData": nameCol, "sName": 'Full_Name', "sTitle": 'Name',"bSortable": true,"sClass":"user-name", "sWidth":"180px"},
					{"mData": "EMAIL", "sName": 'u.EmailAddress_vch', "sTitle": "Email","bSortable": true,"sClass":"user-email", "sWidth":"270px"},
					{"mData": formatContactString, "sName": 'u.HomePhoneStr_vch', "sTitle": "Phone","bSortable": false,"sClass":"user-phone", "sWidth":"212px"},
					{"mData": mlpCapCol, "sName": 'MlpsImageCapacityLimit_bi', "sTitle": "Capacity Limit(MB)","bSortable": true,"sClass":"user-cpt-limit", "sWidth":"200px"},
					{"mData": totalImageCol, "sName": 'TotalImageSize', "sTitle": "Total Used(MB)","bSortable": true,"sClass":"user-total-used", "sWidth":"200px"},
					{"mData": statusCol, "sName": 'status', "sTitle": "Status","bSortable": false,"sClass":"user-total-used", "sWidth":"140px"},
					{"mData": actionCol, "sName": 'TotalImageSize', "sTitle": "Actions","bSortable": false,"sClass":"user-total-used", "sWidth":"160px"}
				],
				"sAjaxDataProp": "UserList",
				"sAjaxSource": '/public/sire/models/users.cfc?method=GetUserList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',

				"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
					 //console.log(fnCallback);
					if (aoData[3].value < 0){
						aoData[3].value = 0;
					}

			        aoData.push(
			            { "name": "customFilter", "value": customFilterData}
		            );
			        oSettings.jqXHR = $.ajax( {
				        "dataType": 'json',
				        "type": "POST",
				        "url": sSource,
				        "data": aoData,
				        "success": fnCallback
			      	});
		        },
		        "fnInitComplete":function(oSettings){
					$('.format-phone-number').mask('(000) 000-0000');
				}
			});
		}
		GetUserList();

		// Show modal view list keyword
		$('body').on('click', '.btn-list-keyword', function(){
			var thisAcc = $(this);
			var shortCodeId  = thisAcc.data('shortcodeid');
			var shortCodeVch  = thisAcc.data('shortcodevch');
			var userID = thisAcc.data('userid');
	    	GetListKeywordByShortCodeId(shortCodeId, userID,shortCodeVch);
	    	$("#list-keyword-modal").modal("show");
		});
		// View list key word by account and shortcodeid
		function GetListKeywordByShortCodeId(shortCodeId, userID,shortCodeVch){
		    $("#list-keyword-table").html('');
		     var actionCol = function(object){
		        return $('<button class="btn-re-delete btn-delete-keyword" data-id="'+object.KEYWORDID+'" data-shortcodeid = "'+shortCodeId+'" data-userid = "'+userID+'" data-keyword="'+object.KEYWORD+'"><i class="fa fa-times" aria-hidden="true"></i></button>')[0].outerHTML;
		    }

		    var table = $('#list-keyword-table').dataTable({
		       	"bStateSave": false,
				"iStateDuration": -1,
				"bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "input",
			    "bLengthChange": false,
				"iDisplayLength": 10,
				"bAutoWidth": false,
		        "aoColumns": [
		            {"mData": "KEYWORD", "sName": '', "sTitle": 'Keyword', "bSortable": false, "sClass": "", "sWidth": "200px"},
		            {"mData": "CREATED", "sName": '', "sTitle": 'Date Created', "bSortable": false, "sClass": "", "sWidth": "200px"},
		            {"mData": actionCol, "sName": '', "sTitle": 'Delete', "bSortable": false, "sClass": "", "sWidth": "100px"}
		        ],
		        "sAjaxDataProp": "LISTKEYWORD",
		        "sAjaxSource": '/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserId'+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		        "fnDrawCallback": function( oSettings ) {
		            if (oSettings._iDisplayStart < 0){

		                oSettings._iDisplayStart = 0;
		                $('input.paginate_text').val("1");
		            }

		        },
		        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
			            aoData.push(
			                { "name": "inpShortCode", "value": shortCodeId},
			                { "name": "inpShortCodeVch", "value": shortCodeVch},
			                { "name": "inpUserId", "value": userID},
			                { "name": "inpAdminSearch", "value": 1},
			            );
			    		oSettings.jqXHR = $.ajax( {
				        "dataType": 'json',
				        "type": "POST",
				        "url": sSource,
				        "data": aoData,
				        "success": function(data) {
				        	if(data.TOTALKWHAVETOPAID > 0){
								$("#keyword-support-modal-title").text("List Keywords [ "+data.TOTALKWHAVETOPAID+" Keywords have to paid]");
							}
				        	fnCallback(data);
				        	if(data.LISTKEYWORD.length == 0) {
				        		$('#list-keyword-table_paginate').hide();
				        	}
				        }
			      	});
		        }
		    });
		}
		// delete keyword of account
		$('body').on('click', '.btn-delete-keyword', function(){
			var thisAcc = $(this);
			var keywordId  = thisAcc.data('id');
			var shortCodeId  = thisAcc.data('shortcodeid');
			var userID = thisAcc.data('userid');
			var keywordText = thisAcc.data('keyword');
			confirmBox('Are you sure you want to delete this keyword? ', 'Delete Keyword', function(){
				$.ajax({
					url: "/session/sire/models/cfc/keywords.cfc?method=AdminDeleteKeyword"+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: "POST",
					dataType: "json",
					data: {
						inpKeywordId: keywordId,
						inpUserId: userID,
						inpKeyword: keywordText
					},
					beforeSend: function(xhr){

					},
					success: function(data){
						if(data.RXRESULTCODE === 1){
							alertBox(data.MESSAGE, 'Confirmation', function(){
								currentPage = $('.paginate_text').val();
								$('#list-keyword-table_wrapper .paginate_text').keyup();
								GetListKeywordByShortCodeId(shortCodeId,userID);
							});
						} else {
							alertBox(data.ERRMESSAGE, 'Confirmation', function(){
								$('#list-keyword-table_wrapper .paginate_text').keyup();
								GetListKeywordByShortCodeId(shortCodeId,userID);
							});
						}
					}
				});
			});
		});



		//Filter bar initialize
		 $('#box-filter').sireFilterBar({
		  fields: [
		   {DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' EmailAddress_vch '},
		   {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' MFAContactString_vch '},
		   {DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' tmpTable.UserId_int '},
		   {DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' FirstName_vch '},
		   {DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' LastName_vch '},
		   {DISPLAY_NAME: 'User Type', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'SBOXUSERTYPE', SQL_FIELD_NAME: ' UserType_int'}
		  ],
		  clearButton: true,
		  // rowLimited: 5,
		  error: function(ele){
		   // console.log(ele);
		  },
		  // limited: function(msg){
		  //  alertBox(msg);
		  // },
		  clearCallback: function(){
		   GetUserList();
		  }
		 }, function(filterData){
			//called if filter valid and click to apply button
			GetUserList(filterData);
		 });



	function isNumber(n)
	{
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}

	$('body').on('click', '.deactive-account-btn', function(event) {
		var data = $(this).data();
		$(".modal-text").text('deactivate');
		showCaptchaConfirm(deactiveAccount, data);
	});

	$('body').on('click', '.active-account-btn', function(event) {
		var data = $(this).data();
		$(".modal-text").text('activate');
		showCaptchaConfirm(activeAccount, data);
	});

	var showCaptchaConfirm = function (callback, data) {
		grecaptcha.reset();
		$('#captcha-message').text('');
		$('#CaptchaConfirmModel').modal('show');
		$('#btn-captcha-confirm').data('callback', callback);
		$('#btn-captcha-confirm').data('data', data);
	}

	$('body').on('click', '#btn-captcha-confirm', function(event) {
		var message_forgot = $('#captcha-message').text('');

		if ($('#g-recaptcha-response').val() != '') {
			$('#CaptchaConfirmModel').modal('hide');
			var callback = $(this).data('callback');
			var data = $(this).data('data');
			callback(data['id'], data['email']);
		} else {
			// disply captcha error message
			message_forgot.text('Please click captcha');
		}
	});

	var deactiveAccount = function(userId, userEmail) {
		if ($('#g-recaptcha-response').val() != '') {
			try {
				$.ajax({
					url: '/public/sire/models/cfc/userstools.cfc?method=DeactiveAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'json',
					data: {inpUserId: userId, inpUserEmail: userEmail},
					beforeSend: function () {
						$('#processingPayment').show();
					}
				})
				.done(function(data) {
					if (parseInt(data.RXRESULTCODE) == 1) {
						alertMessage('Success', 'Account deactivated!');
						if($('input.filter-form-control').val()!= ''){
		            		$('.btn-apply-filter').trigger('click');
		            	}
		            	else{
		            		var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
		            		GetUserList([], getStartFrom);
		            	}
					} else {
						alertMessage('Oops!', data.ERRMESSAGE);
					}
				})
				.fail(function(e) {
					alertMessage();
				})
				.always(function() {
					$('#processingPayment').hide();
				});
			} catch (e) {
				alertMessage();
			}
		} else {
			alertMessage('Oops', 'Something bad happend!');
		}
		return false;
	}

	var activeAccount = function(userId, userEmail) {
		if ($('#g-recaptcha-response').val() != '') {
			try {
				$.ajax({
					url: '/public/sire/models/cfc/userstools.cfc?method=ActiveAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'json',
					data: {inpUserId: userId, inpUserEmail: userEmail},
					beforeSend: function () {
						$('#processingPayment').show();
					}
				})
				.done(function(data) {
					if (parseInt(data.RXRESULTCODE) == 1) {
						alertMessage('Success', 'Account activated!');
						if($('input.filter-form-control').val()!= ''){
		            		$('.btn-apply-filter').trigger('click');
		            	}
		            	else{
		            		var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
		            		GetUserList([], getStartFrom);
		            	}
					} else {
						alertMessage('Oops!', data.ERRMESSAGE);
					}
				})
				.fail(function(e) {
					alertMessage();
				})
				.always(function() {
					$('#processingPayment').hide();
				});
			} catch (e) {
				alertMessage();
			}
		} else {
			alertMessage('Oops', 'Something bad happend!');
		}
		return false;
	}

	var alertMessage = function (title, message) {
		$('#processingPayment').hide();
		title = (typeof(title) !== "undefined" ? title : "Oops!");
		message = (typeof(message) !== "undefined" ? message : "Sorry, we have some issue at the moment, please try again later");
		bootbox.dialog({
			message: message,
			title: title,
			buttons: {
				success: {
					label: "OK",
					className: "btn btn-medium btn-success-custom",
					callback: function() {
					}
				}
			}
		});
		return false;
	}


	$('body').on('click', 'table a.btn-credit-action', function(event){
		var userID = $(this).data('id'),
			email = $(this).data('email'),
			name = $(this).data('name');
			$.ajax({
				url: "/public/sire/models/users.cfc?method=Checkrechargedisplay&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: 'POST',
				dataType: 'json',
				data: {
					UserId: userID
				},
				success: function(data){
					if(data.RESULT == 1){
						if(data.PLANID == 1){
							if(data.BUYKEYWORDNUMBER == 0){
								creditModal.find('.btn-re-charge').hide();
							}else{
								creditModal.find('.btn-re-charge').show();
							}
						}else{
							if(data.RECURRINGKEYWORD == 0 && data.RECURRINGPLAN == 0){
								creditModal.find('.btn-re-charge').hide();
							}else{
								creditModal.find('.btn-re-charge').show();
							}
						}
					}else{
						creditModal.find('.btn-re-charge').hide();
					}
				}
			});

			creditModal.find('input[type="hidden"][name="userID"]').val(userID);
			creditModal.find('strong#user-name').text(name);
			creditModal.find('strong#user-email').text(email);
			GetUserBalance(userID);
			GetBillingUpdateLog();
			creditModal.find('#update-credit').validationEngine('hide');
			creditModal.find('input[name="value"]').val('');
			creditModal.modal('show');
	});

	$('body').on('click', 'table a.btn-edit-account-profile', function(event){
		var userID = $(this).data('id'),
			email = $(this).data('email'),
			name = $(this).data('name'),
			phone = $(this).data('phone')

			$.ajax({
				url: "/public/sire/models/users.cfc?method=GetUserInfor&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: 'POST',
				dataType: 'json',
				data: {
					inpiduser: userID
				},
				success: function(data){
					if(data.RXRESULTCODE == 1){
						profileModal.find('#user_id').val(userID);
						profileModal.find('#user_email').val(data.EMAIL);
						profileModal.find('#user_phone').val(data.PHONENUMBER);
						profileModal.find('#user_firstname').val(data.FIRSTNAME);
						profileModal.find('#user_lastname').val(data.LASTNAME);
						profileModal.find('#user_security_credential').val(data.SECURITYCREDENTIAL);
						profileModal.find('#user_amountlimited').val(data.AMOUNTLIMITED);
						profileModal.find('#user_default_payment_gw').val(data.PAYMENTGATEWAY);
						profileModal.modal('show');
						profileModal.find('#user_mfa > option').each(function(){
							if($(this).val() == data.MFATYPE){
								$(this).prop('selected', true);
							}
							else{
								$(this).prop('selected', false);
							}
						});

						profileModal.find('#user_google_mfa > option').each(function(){
							if($(this).val() == data.ALLOWNOTPHONE){
								$(this).prop('selected', true);
							}
							else{
								$(this).prop('selected', false);
							}
						});

						profileModal.find('#user_XMLCSEditable > option').each(function(){
							if($(this).val() == data.ALLEDITXMLCS){
								$(this).prop('selected', true);
							}
							else{
								$(this).prop('selected', false);
							}
						});


						profileModal.find('#userCaptureCCInfo > option').each(function(){
							if($(this).val() == data.CAPTURECC){
								$(this).prop('selected', true);
							}
							else{
								$(this).prop('selected', false);
							}
						});

						var fnamecheck = $('#user_firstname').val();
						if (fnamecheck == '')
						{
							$('#invalidfname').show();
						}else{
							$('#invalidfname').hide();
						}

						var lnamecheck = $('#user_lastname').val();
						if (lnamecheck == '')
						{
							$('#invalidlname').show();
						}else{
							$('#invalidlname').hide();
						}

						var amountcheck = $('#user_amountlimited').val();
						if(isNaN(amountcheck)){
							$('#invalidnumber').show();
						}else{
							$('#invalidnumber').hide();
						}

					}
				}
			});
	});


	profileModal.find('#btn-acc-profile').on('click', function(event){
			var id = profileModal.find('#user_id').val();
				email = profileModal.find('#user_email').val();
				firstname = profileModal.find('#user_firstname').val();
				lastname = profileModal.find('#user_lastname').val();
				phone = profileModal.find('#user_phone').val();
				mfatype = profileModal.find('#user_mfa').val();
				securityCredential = profileModal.find('#user_security_credential').val();
				ggmfa = profileModal.find('#user_google_mfa').val();
				useramountlimited = profileModal.find('#user_amountlimited').val();
				userXMLCSEditable = profileModal.find('#user_XMLCSEditable').val();
				userDefaultPaymentGw=  profileModal.find('#user_default_payment_gw').val();
				userCaptureCCInfo = profileModal.find('#userCaptureCCInfo').val();


			if(firstname == ""){
				profileModal.find('#user_firstname').focus();
				return;
			}

			if(lastname == ""){
				profileModal.find('#user_lastname').focus();
				return;
			}

			if(phone == "" || statusphone == false){
				profileModal.find('#user_phone').focus();
				return;
			}

			if(useramountlimited == "" || statusamount == false){
				profileModal.find('#user_amountlimited').focus();
				return;
			}

			if(email == "" || statusemail == false){
				profileModal.find('#user_email').focus();
				return;
			}else{
				$.ajax({
					url: "/public/sire/models/users.cfc?method=Checkcompulsoryemail&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
					type: 'POST',
					dataType: 'json',
					data: {
						inpemail: email,
						inpiduser: id
					},
					success: function(data){
						if(data.RXRESULTCODE == 1){
							alertBox('Duplicate the email address!', 'ERROR DATA');
							return;
						}else{
							$.ajax({
								url: "/public/sire/models/users.cfc?method=UpdateAccountProfile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
								type: 'POST',
								dataType: 'json',
								data: {
									inpuserID: id,
									inplastname: lastname,
									inpfirstname: firstname,
									inpphone: phone,
									inpmfatype: mfatype,
									inpemail: email,
									inpSecurityCredential:securityCredential,
									inpallowggmfa: ggmfa,
									inptamountlimited: useramountlimited,
									inpuserXMLCSEditable: userXMLCSEditable,
									inpDefaultPaymentGateway: userDefaultPaymentGw,
									inpUserCaptureCCInfo : userCaptureCCInfo
								},
								success: function(data){
									if(data.RXRESULTCODE == 1){
										alertBox('Update account profile successfully!', 'Update Account Profile');
										// Re load User list after update data successfully
										GetUserList();
										profileModal.modal("hide");
									}else{
										if(data.RXRESULTCODE == 0){
											alertBox(data.ERRMESSAGE, 'Update Account Profile');
										}else{
											alertBox(data.ERRMESSAGE, 'Update Account Profile');
										}
									}
								}
							});

						}
					}
				});
			}
	});


	creditModal.on('shown.bs.modal', function(event){
		//GetBillingUpdateLog();
	});

	creditModal.find('#update-credit').on('submit', function(event){
		event.preventDefault();

		if(creditModal.find('#update-credit').validationEngine('validate')){

			creditModal.find('.btn-add-credit').attr('disabled', true);
			var type = creditModal.find('select[name="field-type"]').val(),
				value = creditModal.find('input[name="value"]').val(),
				userID = creditModal.find('input[type="hidden"][name="userID"]').val();
			var currentBalance = 0;
			if (type == 1) {
				currentBalance = parseInt(creditModal.find("#buy-credit").text());
			} else if (type == 2) {
				currentBalance = parseInt(creditModal.find("#promotion-credit").text());
			} else {
				currentBalance = parseInt(creditModal.find("#plan-credit").text());
			}
			if (parseInt(value) + currentBalance < 0) {
				bootbox.dialog({
					message: "Add value can not less than " + (currentBalance > 0 ? '-' : '') + currentBalance,
					title: "Oops!",
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-medium btn-success-custom",
							callback: function() {
								creditModal.find('.btn-add-credit').attr('disabled', false);
							}
						}
					},
					onEscape: function () {
						creditModal.find('.btn-add-credit').attr('disabled', false);
					}
				});
				return true;
			}
			$.ajax({
				url: "/session/sire/models/cfc/billing.cfc?method=addCredit&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: 'POST',
				dataType: 'json',
				data: {
					inpUserID: userID,
					inpValue: value,
					inpType: type
				},
				success: function(data){
					if(data.RXRESULTCODE == 1){
						alertBox(data.MESSAGE, 'Add Credit');
						GetUserBalance(userID);
						GetBillingUpdateLog();
						creditModal.find('input[name="value"]').val('');
					}
					creditModal.find('.btn-add-credit').attr('disabled', false);
				}
			});
		}

	});
	// click to re-charge
	creditModal.find('#update-credit').on('click', '.btn-re-charge', function(event){
			event.preventDefault();
			var userID = creditModal.find('input[type="hidden"][name="userID"]').val();
			$.ajax({
				url: "/session/sire/models/cfc/billing.cfc?method=RechargeAmountCount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: 'POST',
				dataType: 'json',
				data: {
					UserId: userID
				},
				success: function(data){
					bootbox.dialog({
							message: '<h4 class="be-modal-title">Re-charge Recurring</h4><p>Re-charge for this account: '+userID+', Amount: $'+parseFloat(data.DATA)+'.<br/> Are you sure you would like to continue?</p>',
							title: '&nbsp;',
							className: "be-modal",
							buttons: {
								success: {
									label: "Yes",
									className: "btn btn-medium green-gd",
									callback: function(result) {
										$.ajax({
												url: "/session/sire/models/cfc/billing.cfc?method=DoRecharge&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
												type: 'POST',
												dataType: 'json',
												data: {
													UserId: userID
												},
												beforeSend: function () {
													$('#processingPayment').show();
												},
												success: function(data){
													if(data.RXRESULTCODE == 1){
														alertBox(data.MESSAGE, 'Re-charge Recurring');
													}else{
														alertBox(data.MESSAGE, 'Re-charge Recurring');
													}
												}
											})
											.done(function(data) {
												$('#processingPayment').hide();
												creditModal.modal('hide');
											});
									}
								},
								cancel: {
									label: "NO",
									className: "green-cancel",
									callback: function () {
									}
								},
							}
					});
				}
			})
	});

	creditModal.find('#update-credit').validationEngine('attach', {focusFirstField: false, scroll: false, autoHidePrompt: false, promptPosition : "topLeft"});



	function GetBillingUpdateLog(strFilter){
		var ownerID = $('input[type="hidden"][name="userID"]').val();
		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

		var userNameCol = function(object){
			return object.UFIRSTNAME.toString().ucWords();
		}

		var newValueCol = function(object){
			return '<span class="'+(parseInt(object.OLDVALUE) !== (parseInt(object.NEWVALUE)) ? 'has-changed' : '')+'">'+object.NEWVALUE+'</span>';
		}

		var createdCol = function(object){
			//return moment(object.CREATED).format('YYYY/MM/DD  h:mm:ss a');
			return object.CREATED;
		}
		var table = $('#logList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": createdCol, "sName": 'created', "sTitle": "Date","bSortable": false,"sClass":"created-at"},
				// {"mData": "ID", "sName": 'ID', "sTitle": 'ID',"bSortable": false,"sClass":"log-id"},
				{"mData": "FIELD", "sName": 'field_name', "sTitle": 'Field Name',"bSortable": false,"sClass":"field-name"},
				{"mData": "OLDVALUE", "sName": 'oldValue', "sTitle": "Old Value","bSortable": false,"sClass":"old-value"},
				{"mData": newValueCol, "sName": 'newValue', "sTitle": "New value","bSortable": false,"sClass":"user-phone"},
				{"mData": userNameCol, "sName": 'Full_Name', "sTitle": 'Updated By',"bSortable": false,"sClass":"user-name"},
			],
			"sAjaxDataProp": "dataList",
			"sAjaxSource": '/session/sire/models/cfc/billing.cfc?method=GetUpdateLogs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
		        aoData.push(
		            { "name": "inpOwnerID", "value": ownerID}
	            );
		        aoData.push(
		            { "name": "inpCustomFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        }
		});
	}

	//Filter bar initialize
	var CustomDataFilter = function(){
		return  $('<select>').append($('<option>').attr('value', 'Promotion Credit').text('Promotion Credit'))
					 		 .append($('<option>').attr('value', 'Buy Credit').text('Buy Credit'))
					 		 .addClass('form-control')[0].outerHTML;
	}
	$('#filterBox').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'User ID', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' BUL.UserId_int '},
			{DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' BUL.Created_dt '},
			{DISPLAY_NAME: 'Field Name', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'STATUS', SQL_FIELD_NAME: ' BUL.FieldName_vch ', CUSTOM_DATA: CustomDataFilter},
			{DISPLAY_NAME: 'Old Value', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'INTEGER', SQL_FIELD_NAME: ' BUL.OldValue_int '},
			{DISPLAY_NAME: 'New Value', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'INTEGER', SQL_FIELD_NAME: ' BUL.NewValue_int '}
		],
		clearButton: true,
		error: function(ele){
		// console.log(ele);
		},
		clearCallback: function(){
			GetBillingUpdateLog();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		GetBillingUpdateLog(filterData);
	});

    var GetUserBalance = function(userId) {
        try {
            $.ajax({
                url: '/session/sire/models/cfc/users.cfc?method=GetUserBalance&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'POST',
                dataType: 'json',
                data: {inpUserId: userId},
                beforeSend: function () {
                    $('#processingPayment').show();
                }
            })
            .done(function(data) {
                data = jQuery.parseJSON(data);
                if (parseInt(data.RXRESULTCODE) == 1) {
                    $("#plan-credit").text(data.PLANCREDIT);
                    $("#buy-credit").text(data.BUYCREDIT);
                    $("#promotion-credit").text(data.PROMOTIONCREDIT);
                    $("#total-credit").text(data.PROMOTIONCREDIT+data.BUYCREDIT+data.PLANCREDIT);
                    $("#price").text(data.PRICE);
                    $("#adjust-credit-modal").modal("show");
                } else {
                    alertMessage('Oops!', data.MESSAGE);
                }
            })
            .fail(function(e) {
                bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
            })
            .always(function() {
                $('#processingPayment').hide();
            });
        } catch (e) {
        }

        return false;
    }

    $("body").on('click', '.btn-mark-test-account', function(event) {
    	event.preventDefault();
    	var userid = $(this).data('id');
	    bootbox.dialog({
	        message: '<h4 class="be-modal-title">Mark test account</h4><p>Are you sure you want to mark this as test account ?</p>',
	        title: '&nbsp;',
	        className: "be-modal",
	        buttons: {
	            success: {
	                label: "Yes",
	                className: "btn btn-medium green-gd",
	                callback: function(result) {
	                   MarkTestAccount(userid);
	                }
	            },
	            cancel: {
	                label: "NO",
	                className: "green-cancel"
	            },
	        }
	    });
    });

    function MarkTestAccount(userid) {
    	$.ajax({
    		url: '/session/sire/models/cfc/users.cfc?method=MarkTestAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
    		type: 'POST',
    		data: {inpUserId: userid},
    	})
    	.done(function(data) {
    		data = jQuery.parseJSON(data);
    		alertBox(data.MESSAGE);
    		if (data.RXRESULTCODE > 0) {
    			if($('input.filter-form-control').val()!= ''){
            		$('.btn-apply-filter').trigger('click');
            	}
            	else{
            		var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
            		GetUserList([], getStartFrom);
            	}
    		}
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
    	});

    }

    $("body").on('click', '.btn-unmark-test-account', function(event) {
    	event.preventDefault();
    	var userid = $(this).data('id');
	    bootbox.dialog({
	        message: '<h4 class="be-modal-title">Unmark test account</h4><p>Are you sure you want to unmark this as test account ?</p>',
	        title: '&nbsp;',
	        className: "be-modal",
	        buttons: {
	            success: {
	                label: "Yes",
	                className: "btn btn-medium green-gd",
	                callback: function(result) {
	                   UnMarkTestAccount(userid);
	                }
	            },
	            cancel: {
	                label: "NO",
	                className: "green-cancel"
	            },
	        }
	    });
    });

    function UnMarkTestAccount(userid) {
    	$.ajax({
    		url: '/session/sire/models/cfc/users.cfc?method=UnMarkTestAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
    		type: 'POST',
    		data: {inpUserId: userid},
    	})
    	.done(function(data) {
    		data = jQuery.parseJSON(data);
    		alertBox(data.MESSAGE);
    		if (data.RXRESULTCODE > 0) {
    			if($('input.filter-form-control').val()!= ''){
            		$('.btn-apply-filter').trigger('click');
            	}
            	else{
            		var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
            		GetUserList([], getStartFrom);
            	}
    		}
    	})
	   	.fail(function() {
    		bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");
    	});

    }

    function GetAccountUpdateLog(strFilter){
		var ownerID = accountLimitModal.find('input[name="user_id"]').val();
		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

		var userNameCol = function(object){
			return object.UFIRSTNAME.toString().ucWords();
		}

		var newValueCol = function(object){
			return '<span class="'+(parseInt(object.OLDVALUE) !== (parseInt(object.NEWVALUE)) ? 'has-changed' : '')+'">'+object.NEWVALUE+'</span>';
		}

		var createdCol = function(object){
			//return moment(object.CREATED).format('YYYY/MM/DD  h:mm:ss a');
			return object.CREATED;
		}
		var table = $('#accountUpdateLogList').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
		    "aoColumns": [
		    	{"mData": createdCol, "sName": 'created', "sTitle": "Date","bSortable": false,"sClass":"created-at", "sWidth":"83px"},
				// {"mData": "ID", "sName": 'ID', "sTitle": 'ID',"bSortable": false,"sClass":"log-id"},
				{"mData": "FIELD", "sName": 'field_name', "sTitle": 'Field Name',"bSortable": false,"sClass":"field-name", "sWidth":"83px"},
				{"mData": "OLDVALUE", "sName": 'oldValue', "sTitle": "Old Value","bSortable": false,"sClass":"old-value", "sWidth":"83px"},
				{"mData": newValueCol, "sName": 'newValue', "sTitle": "New value","bSortable": false,"sClass":"user-phone", "sWidth":"83px"},
				{"mData": userNameCol, "sName": 'Full_Name', "sTitle": 'Updated By',"bSortable": false,"sClass":"user-name", "sWidth":"83px"},
			],
			"sAjaxDataProp": "dataList",
			"sAjaxSource": '/public/sire/models/users.cfc?method=GetAccountUpdateLogs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',

			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
		        if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "inpOwnerID", "value": ownerID}
	            );
		        aoData.push(
		            { "name": "inpCustomFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        }
		});
	}

	function doDowngrade(userId, planId){

	    $('#processingPayment').show();
		try{
		    $.ajax({
		        method: 'POST',
		        url: '/session/sire/models/cfc/order_plan.cfc?method=doDowngrade&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		        data: {'inpPlanId':planId, 'inpUserId': userId},
		        dataType: 'json',
		            //timeout: 6000,
		            success: function(data) {
		                $("#processingPayment").hide();
		                if(data.RXRESULTCODE == 1)
		                {
		                    // $('#processingPayment').hide();
		                    // UIkit.modal('#downgrade-success-modal-sections')[0].toggle();
		                    // $('#downgrade-success-modal-sections .newPlanName').html(newPlanName);
		                    alertBox(data.MESSAGE, 'Downgrade Account');
		                    var getStartFrom = parseInt($('input.paginate_text').val()-1)*20;
			            	GetUserList([], getStartFrom);
		                }
		                else{
		                    // UIkit.modal('#downgrade-list-confirm-modal-sections')[0].toggle();
		                    alertBox('Downgrade failed please try again later', 'Downgrade Account');
		                }
		            }
		        });

		}catch(ex){
		    $('#processingPayment').hide();
		    alertBox('Downgrade failed please try again later');
		}
	}

	$('body').on('click', '.btn-downgrade-acc', function(){
		var thisAcc = $(this);

		$.ajax({
			url: '/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {InpUserId: $(this).data('id')},
			success: function(data){
				var a = data.DATA;
				var selectObj;

				if(thisAcc.data('planid')==3){
					selectObj = '<div class="col-md-4 col-sm-4" style="padding-left: 0px"><div class="form-group"><span class="">Select Plan:</span></div></div><div class="col-md-8 col-sm-8"><select class="form-control" id="select-plan-to-dg"><option id="1" value="1">Free</option><option id="2" value="2">Individual</option><option id="6" value="6">Pro</option></select></div>';
				}
				else if(thisAcc.data('planid')==6){
					selectObj = '<div class="col-md-4 col-sm-4" style="padding-left: 0px"><div class="form-group"><span class="">Select Plan:</span></div></div><div class="col-md-8 col-sm-8"><select class="form-control" id="select-plan-to-dg"><option id="1" value="1">Free</option><option id="2" value="2">Individual</option></select></div>';
				}
				else if(thisAcc.data('planid')==2){
					selectObj = '<div class="col-md-4 col-sm-4" style="padding-left: 0px"><div class="form-group"><span class="">Select Plan:</span></div></div><div class="col-md-8 col-sm-8"><select class="form-control" id="select-plan-to-dg"><option id="1" value="1">Free</option></select></div>';
				}

				if(parseInt(a.RXRESULTCODE[0]) == 1){
					confirmBox(thisAcc.data('name') + ' - ' + thisAcc.data('email') + '</br>' + 'Plan: ' +
						a.PLANNAME[0] + '</br>' + 'Next recurring date: ' + thisAcc.data('enddate') +
						'</br>'+ selectObj + '</br></br>' +'This action will downgrade this account on the next recurring date.</br></br>' +
						'Downgrading will decrease the number of the included keywords, short URLS or MLPs that are included in this account.</br></br> Are you sure you would like to proceed?',
						'Downgrade Account', function(){
							doDowngrade(thisAcc.data('id'),$('#select-plan-to-dg').val());
						});
				}
				else{
					alertBox(data.MESSAGE, 'Downgrade Account');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alertBox('Send request failed!', 'Downgrade Account');
			}
		});

	});

	$('body').on('click', '.btn-upgrade-acc', function(){
		var thisAccount = $(this);
		document.getElementById("addon-form").reset();
		$('#select-plan-2').html('');

		if($('.monthly_price').hasClass('hidden')){
			$('.monthly_price').removeClass('hidden');
		}

		if($('.yearly_price').hasClass('hidden')){
			$('.yearly_price').removeClass('hidden');
		}

		if(thisAccount.data('billingtype') == 2){
			$('.monthly-yearly-update').hide();
			$('#month-Year-Switch').val('1');
			$('#monthYearSwitch').val('1');

			$('.monthly_price').addClass('hidden');

		}
		else{
			$('.monthly-yearly-update').show();
			$('#month-Year-Switch').val('2');
			$('#monthYearSwitch').val('2');

			$('.yearly_price').addClass('hidden');

		}

		if($(this).data('planid')==1){
	        $('#select-plan-2').append('<option id="plan-2" value="2">Individual</option>');
	        $('#select-plan-2').append('<option id="plan-6" value="6">Pro</option>');
	        $('#select-plan-2').append('<option id="plan-3" value="3">SMB</option>');

	    }
	    else if($(this).data('planid')==2){
	        $('#select-plan-2').append('<option id="plan-6" value="6">Pro</option>');
	        $('#select-plan-2').append('<option id="plan-3" value="3">SMB</option>');
	    }
	    else if($(this).data('planid')==6){
	        $('#select-plan-2').append('<option id="plan-3" value="3">SMB</option>');
	    }

		$.ajax({
			method: 'POST',
			url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentMethodSettingByUserId&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
			data:{
				inpUserId: $(this).data('id')
			},
			dataType: 'json',
			beforeSend:function(xhr){
				$('#processingPayment').show();
			}
			,error:function(XMLHttpRequest,textStatus,errorThrown){
				alertBox("Unable to get your infomation at this time. An error occurred.","Oops!");
				$('#processingPayment').hide();
			},
			success: function(data) {
				$('#processingPayment').hide();
				if (data && data.RXRESULTCODE == 1) {
					$('#payment_method_value').val(data.RESULT);
					$(".select_card_type").show();
					$(".card-support-type").show();
					$(".inner-update-cardholder").show();
					$("#section-2.inner-update-cardholder").show();
					//
					$.ajax({
						url: '/session/sire/models/cfc/payment/vault.cfc?method=getDetailCreditCardInVault&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						dataType: 'JSON',
						data: {
							inpPaymentGateway:$('#payment_method_value').val(),
							inpUserId: thisAccount.data('id')
						},
						beforeSend:function(xhr){
							$('#processingPayment').show();
						}
						,error:function(XMLHttpRequest,textStatus,errorThrown){
							alertBox("Unable to get your infomation at this time. An error occurred.","Oops!");
							$('#processingPayment').hide();
						},
						success: function(data){
							$("#processingPayment").hide();
							//get default coupon code for affiliate user only
							$("#remove-btn").trigger("click");
							$("#buyaddon-modal-sections #inpPromotionCode").val( GetUserDefaultCouponByAffiliateUser( thisAccount.data('id')) );
							if(parseInt(data.RXRESULTCODE) == 1){
								$("#select_used_card_value").val(1);
								if($('.triangle-isosceles.left').has('changed')){
									$('.triangle-isosceles.left').removeClass('changed');
								}

								$('#month-Year-Switch').prop('checked',false);

								if($(this).is(':checked')){
									$('#is-yearly-plan').val(1);

									$('#monthly-plan-text').css('color', '#929292');
									$('#yearly-plan-text').css('color', '#568ca5');
									$('.triangle-isosceles').css('background', '#568ca5');
									$('#monthYearSwitch').val(2);

								}
								else{
									$('#is-yearly-plan').val(0);

									$('#monthly-plan-text').css('color', '#568ca5');
									$('#yearly-plan-text').css('color', '#929292');
									$('.triangle-isosceles').css('background', '#929292');
									$('#monthYearSwitch').val(1);
								}

								$('#month-Year-Switch').val(thisAccount.data('billingtype'));
								$('#monthYearSwitch').val(thisAccount.data('billingtype'));
								$('#currentPlan').val(thisAccount.data('planid'));
								$('#pricemsgafter').val(thisAccount.data('pricemsg'));
								$('#h_email').val(thisAccount.data('email'));
								$('#h_email_save').val(thisAccount.data('email'));

								if(data.CUSTOMERINFO.DATA){
									$('#lmaskedNumber').text(data.CUSTOMERINFO.DATA.maskedNumber[0]);
									$('#lexpirationDate').text(data.CUSTOMERINFO.DATA.expirationDate[0]);
									$('#lfirstName-lastName').text(data.CUSTOMERINFO.DATA.firstName[0] + ' ' + data.CUSTOMERINFO.DATA.lastName[0]);
									$('#lline1').text(data.CUSTOMERINFO.DATA.line1[0]);
									$('#lcity').text(data.CUSTOMERINFO.DATA.city[0]);
									$('#lstate').text(data.CUSTOMERINFO.DATA.state[0]);
									$('#lzip').text(data.CUSTOMERINFO.DATA.zip[0]);
									$('#lcountry').text(data.CUSTOMERINFO.DATA.country[0]);
									$('#lemailAddress').text(data.CUSTOMERINFO.DATA.emailAddress[0]);
								}
								else{
									$('#lmaskedNumber').text(data.CUSTOMERINFO.MASKEDNUMBER);
									$('#lexpirationDate').text(data.CUSTOMERINFO.EXPIRATIONDATE);
									$('#lfirstName-lastName').text(data.CUSTOMERINFO.FIRSTNAME + ' ' + data.CUSTOMERINFO.LASTNAME);
									$('#lline1').text(data.CUSTOMERINFO.LINE1);
									$('#lcity').text(data.CUSTOMERINFO.CITY);
									$('#lstate').text(data.CUSTOMERINFO.STATE);
									$('#lzip').text(data.CUSTOMERINFO.ZIP);
									$('#lcountry').text(data.CUSTOMERINFO.COUNTRY);
									$('#lemailAddress').text(data.CUSTOMERINFO.EMAILADDRESS);
									$('#userId').val(thisAccount.data('id'));
								}

								UIkit.modal('#buyaddon-modal-sections')[0].toggle();
								var nextPlanId = 0;
								var inpPromotionCode =  $('#inpPromotionCode').val();
								upgradePlan = 1;
								var currentPlan = $('#currentPlan').val();

								if(currentPlan == 1){
									nextPlanId = 2;
								}
								else if(currentPlan == 2){
									nextPlanId = 6;
								}
								else if(currentPlan == 6){
									nextPlanId = 3;
								}

								$(".span-plan-price").hide();
								$("#span-plan-price-" + nextPlanId).show();
								$("#span-plan-price-yearly-" + nextPlanId).show();

								$('.hidden_card_form2').hide();
								$('.hidden_card_form1').show();


							}
							else{
								$("#select_used_card_value").val(2);
								if($('.triangle-isosceles.left').has('changed')){
									$('.triangle-isosceles.left').removeClass('changed');
								}

								$('#month-Year-Switch').prop('checked',false);

								if($(this).is(':checked')){
									$('#is-yearly-plan').val(1);

									$('#monthly-plan-text').css('color', '#929292');
									$('#yearly-plan-text').css('color', '#568ca5');
									$('.triangle-isosceles').css('background', '#568ca5');
									$('#monthYearSwitch').val(2);

								}
								else{
									$('#is-yearly-plan').val(0);

									$('#monthly-plan-text').css('color', '#568ca5');
									$('#yearly-plan-text').css('color', '#929292');
									$('.triangle-isosceles').css('background', '#929292');
									$('#monthYearSwitch').val(1);
								}

								$('#month-Year-Switch').val(thisAccount.data('billingtype'));
								$('#monthYearSwitch').val(thisAccount.data('billingtype'));
								$('#currentPlan').val(thisAccount.data('planid'));
								$('#pricemsgafter').val(thisAccount.data('pricemsg'));
								$('#h_email').val(thisAccount.data('email'));
								$('#h_email_save').val(thisAccount.data('email'));
								$('#userId').val(thisAccount.data('id'));
								UIkit.modal('#buyaddon-modal-sections')[0].toggle();
								var nextPlanId = 0;
								var inpPromotionCode =  $('#inpPromotionCode').val();
								upgradePlan = 1;
								var currentPlan = $('#currentPlan').val();

								if(currentPlan == 1){
									nextPlanId = 2;
								}
								else if(currentPlan == 2){
									nextPlanId = 6;
								}
								else if(currentPlan == 6){
									nextPlanId = 3;
								}

								$(".span-plan-price").hide();
								$("#span-plan-price-"+nextPlanId).show();
								$("#span-plan-price-yearly-"+nextPlanId).show();

								$('.hidden_card_form1').hide();
								$('.hidden_card_form2').show();
								$('#optionsRadios1').prop('checked', false);
								$('#optionsRadios2').prop('checked', true);
							}
						}
					});
				}
			}
		});

	});

	var buy_addon = $("#addon-form");
	buy_addon.find('.select_used_card').click(function(){
		if ($(this).val() == '2') {
			$("#select_used_card_value").val(2);
		} else {
			$("#select_used_card_value").val(1);
		}
	});

	//POP UP DISPLAY AMOUNT
    buy_addon.submit(function(event){
        event.preventDefault();
        if (buy_addon.validationEngine('validate')) {
            var self = $("#addon-form");
            var currentPlan = $('#currentPlan').val();

            var inpPromotionCode =  $('#inpPromotionCode').val();
            var planType = $('#monthYearSwitch').val();
            // var planType = '';
            upgradePlan = 1;

            if (inpPromotionCode != "") {
                if (couponValid == 0) {
                    alertBox('You must apply your coupon code first!', 'Upgrade Plan', function(){});
                    return false;
                }
                else if (couponValid == -1) {
                    alertBox('Coupon code is not valid', 'Upgrade Plan', function(){});
                    return false;
                }
            }

            try{

                $.ajax({
                    type: "POST",
                    url: '/session/sire/models/cfc/order_plan.cfc?method=getPurchaseAmount1&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                    dataType: 'json',
                    data: {'inpPlanId':$('#select-plan-2').val(), 'inpPromotionCode': $('#inpPromotionCode').val(),'inpPlanType':$('#monthYearSwitch').val(),'inpUserId':$('#userId').val()},
                    beforeSend: function( xhr ) {
                        $('#processingPayment').hide();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#processingPayment').hide();
                        bootbox.dialog({
                            message:"Can not upgrade",
                            title: "",
                            buttons: {
                                success: {
                                    label: "Ok",
                                    className: "btn btn-medium btn-success-custom",
                                    callback: function() {}
                                }
                            }
                        });
                    },
                    success:
                    function(d) {
                        $('#processingPayment').hide();

                        if(d.RXRESULTCODE == 1){
                            $('#upgradePlanAmount').val(d.TOTALAMOUNT);

                            var buy_addon_title = 'Upgrade Plan';
                            var planAmount = d.TOTALAMOUNT;
                            confirmBox('Amount to be paid: <b>$'+ planAmount +'</b> <br> Are you sure you would like to Make Payment?', buy_addon_title, function(){
                                $("#actionStatus").val('Processing');
                                $("#processingPayment").show();
                                try{
                                	$('#planId').val($('#select-plan-2').val());
									var formdata = self;
									upgradePlanProcess(formdata,planAmount,$('#select-plan-2').val(),$('#userId').val(),$('#monthYearSwitch').val());

                                }catch(ex){
                                    $('#processingPayment').hide();
                                    alertBox('Transaction failed please try again later' + ex, buy_addon_title);
                                }
                            });

                        }
                    }
                });
			}catch(ex){
			    $('#processingPayment').hide();
			    console.log(ex);
			    bootbox.dialog({
			        message:"Can not upgrade",
			        title: "",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {}
			            }
			        }
			    });
			}
		}
	});

    function upgradePlanPP(formData,planAmount,userId){
        var result = { };
        $.each(formData.serializeArray(), function() {
            result[this.name] = this.value;
        });
        var planId = result.planId;
        var inpPromotionCode = result.inpPromotionCode;
        var monthYearSwitch = result.monthYearSwitch;
        var listKeyword =  result.listKeyword;
        var listMLP =  result.listMLP;
        var listShortUrl =  result.listShortUrl;

        var data ={
            'planId': planId,
            'inpPromotionCode': inpPromotionCode,
            'userId': userId,
        };
        var returnUrlParam = "&userId="+userId+"&planId="+planId+"&inpPromotionCode="+inpPromotionCode+"&monthYearSwitch="+monthYearSwitch+"&listKeyword="+listKeyword+"&listMLP="+listMLP+"&listShortUrl="+listShortUrl
        var jsondata = JSON.stringify(data);
        $.ajax({
            method: 'POST',
            url: '/session/sire/models/cfc/billing.cfc?method=SetPPExpressCheckout&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
            data:{
                inpAmount: planAmount,
                inpPaymentDescription: jsondata,
                inpCancelUrl:"http://"+window.location.hostname+"/session/sire/pages/admin-account-update",
                inpReturnlUrl:"http://"+window.location.hostname+"/session/sire/models/cfm/paypal_process?action=admin_upgrade_plan"+returnUrlParam
            },
            dataType: 'json',
            success: function(data) {
                $("#processingPayment").hide();
                if (data && data.RXRESULTCODE == 1) {
                    var url_direct = paypalCheckoutUrl+data.RESULT;
                    location.href = url_direct;
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                } else {
                    alertBox(data.MSG, buy_addon_title);
                    $('.btn-payment-credits').removeAttr('disabled').text('PURCHASE');
                }
            }
        });
    }



	function getCouponCode (code) {
	    if (code != '') {
	        $.ajax({
	            url: '/session/sire/models/cfc/promotion.cfc?method=CheckCouponForUpgradePlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	            type: 'POST',
	            data: {inpCouponCode: code},
	        })
	        .done(function(data) {
	            if (parseInt(data.RXRESULTCODE) == 1) {
	                $("#help-block").html(data.PROMOTION_DISCOUNT);
	                $("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
	                $("#apply-btn").hide();
	                $("#remove-btn").show();
	                couponValid = 1;
	            } else {
	                $("#help-block").html(data.MESSAGE);
	                $("#help-block").closest('.form-group').addClass('has-error').removeClass('has-ok');
	                couponValid = -1;
	                $("#apply-btn").hide();
	                $("#remove-btn").show();
	            }
	            $("#help-block").show();
	        })
	        .fail(function() {
	            console.log("error");
	        })
	        .always(function() {
	            console.log("complete");
	        });

	    }
	}

	$("#inpPromotionCode").keyup(function(event) {
	    var code = $(this).val();
	    couponValid = 0;
	    if (code.length == 0) {
	        couponValid = 0;
	        $("#apply-btn").show();
	        $("#remove-btn").hide();
	        $("#help-block").hide();
	        $(".form-group").removeClass('has-error').removeClass('has-ok');
	    }
	});
	$("body").on('click', '#apply-btn', function(event) {
	    event.preventDefault();
	    var code = $("#inpPromotionCode").val();
	    getCouponCode(code);
	});
	$("body").on('click', '#remove-btn', function(event) {
	    event.preventDefault();
	    $("#inpPromotionCode").val('');
	    $("#apply-btn").show();
	    $("#remove-btn").hide();
	    $("#help-block").hide().html('');
	    $("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
	});

	$("#select-plan-2").change(function(event) {
	    var planid = this.value;
	    if (planid > 1) {
	        $(".section-payment").show();
	        $('#purchase-btn').text('PURCHASE');
	    } else {
	        $(".section-payment").hide();
	        $('#purchase-btn').text('GET STARTED');
	    }
	    $(".span-plan-price").hide();
	    $("#span-plan-price-"+planid).show();
	    $("#span-plan-price-yearly-"+planid).show();
	});

	buy_addon.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});


	$('#buyaddon-modal-sections .select_used_card').click(function(){

	    $('#buyaddon-modal-sections #card_number').val('');
	    $('#buyaddon-modal-sections #security_code').val('');
	    $('#buyaddon-modal-sections #expiration_date').val('');
        // $('#expiration_date_month').val('');
        // $('#expiration_date_year').val('');
        $('#buyaddon-modal-sections #first_name').val('');
        $('#buyaddon-modal-sections #last_name').val('');

        buy_addon.validationEngine('hideAll');

        // my_plan[0].reset();

        if($(this).val() == 2){

            $('.fset_cardholder_info').show();
            $('.btn-update-profile').prop('disabled', false);
        }
        else{
            $('.fset_cardholder_info').hide();
            $('.btn-update-profile').prop('disabled', true);
        }
    });

    $('#inpPromotionCode').keypress(function( e ) {
    	if(e.which === 32)
        	return false;
	});

	$('#month-Year-Switch').on('click', function(){
	    // $('#select-plan-1').prop('selectedIndex',0);

	    $('.triangle-isosceles.left').toggleClass('changed');
	    if($(this).is(':checked')){
	        $('#is-yearly-plan').val(1);
	        $('.monthly_price').toggleClass('hidden');
	        $('.yearly_price').toggleClass('hidden');
	        // $("#select-plan-1").append(window.myOpts);
	        // window.myOpts1 = $("option.monthly-plan").detach();
	        $('#monthly-plan-text').css('color', '#929292');
	        $('#yearly-plan-text').css('color', '#568ca5');
	        $('.triangle-isosceles').css('background', '#568ca5');
	        // $('#monthly-or-yearly-amount').text('Yearly Amount:');
	        $('#monthYearSwitch').val(2);


	        var planid = $('#select-plan-2').val();
			$(".span-plan-price").hide();
		    $("#span-plan-price-"+planid).hide();
		    $("#span-plan-price-yearly-"+planid).show();

	    }
	    else{
	        $('#is-yearly-plan').val(0);
	        $('.monthly_price').toggleClass('hidden');
	        $('.yearly_price').toggleClass('hidden');
	        // $("#select-plan-1").append(window.myOpts1);
	        // window.myOpts2 = $("option.annually-plan").detach();
	        $('#monthly-plan-text').css('color', '#568ca5');
	        $('#yearly-plan-text').css('color', '#929292');
	        $('.triangle-isosceles').css('background', '#929292');
	        // $('#monthly-or-yearly-amount').text('Monthly Amount:');
	        // var myOpts = $("option.annually-plan").detach();
	        $('#monthYearSwitch').val(1);

	        var planid = $('#select-plan-2').val();
			$(".span-plan-price").hide();
		    $("#span-plan-price-"+planid).show();
		    $("#span-plan-price-yearly-"+planid).hide();
	    }

	    // checkUpgrade();
	});

	$('#month-Year-Switch-Admin').on('click', function(){
		if($(this).is(':checked')){
			$('#monthYearSwitchAdmin').val(2);
		}
		else
		{
			$('#monthYearSwitchAdmin').val(1);
		}
	});

	$('body').on('click', '.btn-canceldowngrade-acc', function(){
		var thisAcc = $(this);

		$.ajax({
			url: '/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {InpUserId: $(this).data('id')},
			success: function(data){
				var a = data.DATA;
				var selectObj;

				if(parseInt(a.RXRESULTCODE[0]) == 1){
					var message = thisAcc.data('name') + ' - ' + thisAcc.data('email') + '</br>' + 'Plan: ' + a.PLANNAME[0] + '</br>' + 'Next recurring date: ' + thisAcc.data('enddate') +
						'</br></br>' +'This account will be downgraded to <strong>'+a.USERDOWNGRADEPLANNAME[0]+'</strong> by  <strong>'+thisAcc.data('enddate')+'</strong> .</br></br>' + 'You can cancel this process anytime by choosing below action';

					bootbox.dialog({
				        message: '<h4 class="be-modal-title">Downgrade Account</h4><p>'+ message +'</p>',
				        title: '&nbsp;',
				        className: "be-modal",
				        buttons: {
				            success: {
				                label: "CANCEL DOWNGRADE",
				                className: "btn btn-medium green-gd",
				                callback: function(result) {
				                   adminCancelDowngrade(thisAcc.data('id'))
				                }
				            },
				            cancel: {
				                label: "LEAVE",
				                className: "green-cancel"
				            },
				        }
				    });

				}
				else{
					alertBox(data.MESSAGE, 'Downgrade Account');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alertBox('Send request failed!', 'Downgrade Account');
			}
		});

	});

	function adminCancelDowngrade(userid){

		$.ajax({
            type: "POST",
            url: '/session/sire/models/cfc/order_plan.cfc?method=cancelDowngrade&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType: 'json',
           	data: {"InpUserId":userid,"inpAdminCancel":1},
            error: function() {
                $('#processingPayment').hide();
            },
            complete: function() {
                $('#processingPayment').hide();
            },
            success: function(d2) {
                $('#processingPayment').hide();
                if(parseInt(d2.RXRESULTCODE) == 1){
                    bootbox.dialog({
                        message:'Downgrade request has been cancel.',
                        title: "&nbsp;",
                        className: "be-modal",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {
                                	$('#tblListEMS').DataTable().fnDraw(false );
                                }
                            }
                        }
                    });
                }
                else{
                    bootbox.alert(d2.MESSAGE);
                }
            }
        });
	}

	$('#box-filter').find('select[name="field-name"]').on('change', function(event){
		GetUserList();
	});

	$('#box-filter').find('[name="value"]').on('keyup', function(event){
			var data =[];
			var fileName = $('#box-filter').find('select[name="field-name"]').val(),
				operator = $('#box-filter').find('select[name="operator"]').val(),
				type     = $('#box-filter').find('select[name="field-name"] option:selected').data('sql-type'),
				value    = $(this).val();
				if(value.length < 3){
					return;
				}
				if(value){
					data.push({"NAME": fileName, "OPERATOR": operator, "VALUE": value, "TYPE": type});
					GetUserList(data);
				}
	});

	var update_acc_limit = $('#update-acc-limit');
	accountLimitModal.on('hidden.bs.modal', function (e) {
		update_acc_limit[0].reset();
		$('#field-to-change-plan').hide().attr('name', '');
		accountLimitModal.find('.fieldset-payment').hide();
		$('#field-to-change').show().attr('name', 'value');
		update_acc_limit.find('.inner-update-cardholder').hide();
		update_acc_limit.find('.fset_cardholder_info').hide();
	});

	update_acc_limit.find('.payopt').click(function(){
		if ($(this).is(':checked')) {
			update_acc_limit.find('.inner-update-cardholder').show();
		} else {
			update_acc_limit.find('.inner-update-cardholder').hide();
		}
	});

	update_acc_limit.find('.monthYearSwitch').click(function(){
		if ($(this).is(':checked')) {
			update_acc_limit.find('.monthly_price .uk-text-bold').text('Yearly Amount:');
			var price = parseFloat($('#field-to-change-plan').find(':selected').data('yearprice'));
		} else {
			update_acc_limit.find('.monthly_price .uk-text-bold').text('Monthly Amount:');
			var price = parseFloat($('#field-to-change-plan').find(':selected').data('price'));
		}
		update_acc_limit.find('.inpCustomAmount').val(price);
	});

	update_acc_limit.find('.select_used_card').click(function(){
		if ($(this).val() == '2') {
			update_acc_limit.find('.fset_cardholder_info').show();
			$("#select_used_card_value").val(2);
		} else {
			update_acc_limit.find('.fset_cardholder_info').hide();
			$("#select_used_card_value").val(1);
		}
	});
	// show sub usser
	function GetSubUser(strFilter){
		if(globalMasterUserLevel == 1)
		{
			var tablediv="userTable-2";
		}
		else
		{
			var tablediv="userTable-3";
		}
		$("#"+tablediv).html('');

		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";
		nameCol = function(object){
			return "<span class='user-name-span'  data-id='"+ object.USERID+"' data-user-level='2'>"+ object.USERNAME+"</span>" ;
		};

		var table = $('#'+tablediv).dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
			"aoColumns": [
				{"mData": "USERID", "sName": 'User ID', "sTitle": 'User ID', "bSortable": false, "sClass": "", "sWidth": "100px"},
				{"mData": nameCol, "sName": 'User Name', "sTitle": 'User Name', "bSortable": false, "sClass": "", "sWidth": "200px"},
				{"mData": "USEREMAIL", "sName": 'User Email', "sTitle": 'User Email', "bSortable": false, "sClass": "", "sWidth": "300px"},
				{"mData": "CONTACTSTRING", "sName": 'Contact String', "sTitle": 'Contact String', "bSortable": false, "sClass": "", "sWidth": "200px"},
				{"mData": "REGISTEREDDATE", "sName": 'Sign Up Date', "sTitle": 'Sign Up Date', "bSortable": false, "sClass": "", "sWidth": "200px"},
				{"mData": "LASTLOGIN", "sName": 'Last Login', "sTitle": 'Last Login', "bSortable": false, "sClass": "", "sWidth": "200px"},
				//{"mData": "STATUS", "sName": 'Is Active', "sTitle": 'Is Active', "bSortable": false, "sClass": "", "sWidth": "110px"}
			],
			"sAjaxDataProp": "datalist",
			"sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=getUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

				aoData.push(
					{ "name": "customFilter", "value": customFilterData}
				);
				aoData.push(
					{ "name": "inpUserId", "value": globalMasterUser}
				);
				aoData.push(
					{ "name": "inpIncludeSubAccLv", "value": globalMasterUserLevel+1}
				);
				aoData.push(
					{ "name": "inpForceFilterByMasterUser", "value":1}
				);

				oSettings.jqXHR = $.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function(data) {
						fnCallback(data);
						if(data.datalist.length == 0){
							$('#userTable_paginate').hide();
						}
					}
				});
			}
		});
	}
	function InitFilterForUserTable () {
		if(globalMasterUserLevel == 1)
		{
			var userFilterBox="userFilterBox-2";
		}
		else
		{
			var userFilterBox="userFilterBox-3";
		}
		$("#"+userFilterBox).html('');
		//Filter bar initialize
		$('#'+userFilterBox).sireFilterBar({
			fields: [
				{DISPLAY_NAME: 'Email', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.EmailAddress_vch '},
				{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' ua.UserId_int '},
				{DISPLAY_NAME: 'Contact String', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' ua.MFAContactString_vch '},
				{DISPLAY_NAME: 'First Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.FirstName_vch '},
				{DISPLAY_NAME: 'Last Name', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'TEXT', SQL_FIELD_NAME: ' ua.LastName_vch '},
				{DISPLAY_NAME: 'Sign Up Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.Created_dt '},
				{DISPLAY_NAME: 'Last Login Date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' ua.LastLogIn_dt '},
				//{DISPLAY_NAME: 'Active', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' ua.Active_int '}
			],
			clearButton: true,
			error: function(ele){

			},
			clearCallback: function(){
				GetSubUser();
			}
		}, function(filterData){
			GetSubUser(filterData);
		});
	}

	function GetAccReport1(userID){
		$.ajax({
			url: '/public/sire/models/users.cfc?method=GetAccountReportLikeDashboard&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: {inpUserId: userID},
			success: function(data){
				if(data.RXRESULTCODE == 1){
					$('#acc-total-message-sent').text("Total message sent: " + data.MESSAGESENT);
					$('#acc-opt-in').text("Opt-in: " + data.OPTIN);
					$('#acc-opt-out').text("Opt-out: " + data.OPTOUT);
					$('#acc-short-url-clicks').text("Short URL clicks: " + data.URLCLICK);
					$('#acc-mlp-views').text("MLP views: " + data.MLPVIEW);

				}
			}
		});

		// $.ajax({
		// 	url: '/path/to/file',
		// 	type: 'default GET (Other values: POST)',
		// 	dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		// 	data: {param1: 'value1'},
		// 	success: function(){

		// 	}
		// });

		// $.ajax({
		// 	url: '/path/to/file',
		// 	type: 'default GET (Other values: POST)',
		// 	dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		// 	data: {param1: 'value1'},
		// 	success: function(){

		// 	}
		// });

	}

	function GetAccReport2(userID){ //get account report for datatable type
		$('#tblSubcriberList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "NAME", "sName": '', "sTitle": 'Subscribers List', "bSortable": false, "sClass": "","sWidth": "80px"},
                {"mData": "OPTIN", "sName": '', "sTitle": 'Optin', "bSortable": false, "sClass": "","sWidth": "90px"}
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupListForDatatableNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpUserId", "value": userID}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
	                        $('#tblSubcriberList_paginate').hide();
	                    }
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                // $('#tblKeywordList_paginate').hide();
            }
        });

		$('#tblCampaignList').dataTable({
            "bStateSave": true,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {
            },
            "fnStateSaveParams": function (oSettings, oData) {
            },


            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"sName": 'name', "sTitle": 'Name',"bSortable": false,"sClass":"camp-name", "sWidth": "180px"},
                {"sName": 'template', "sTitle": 'Template Type',"bSortable": false,"sClass":"camp-name", "sWidth": "160px"},
                {"sName": 'keyword', "sTitle": 'Keyword',"bSortable": false,"sClass":"camp-name", "sWidth":"140px"},
            ],
            "sAjaxDataProp": "ListEMSData",
            "sAjaxSource": '/session/sire/models/cfc/reports/dashboard.cfc?method=GetCampaignListForDashboard&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            },
           "fnDrawCallback": function( oSettings ) {
              // MODIFY TABLE
                //$("#tblCampaignList thead tr").find(':last-child').css("border-right","0px");
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }

            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
                if (aoData[3].value < 0){
                    aoData[3].value = 0;
                }

                aoData.push(
                    { "name": "inpUserId", "value": userID},
                    { "name": "isForReport", "value": 1}
                );

                $.ajax({dataType: 'json',
                         type: "POST",
                         url: sSource,
                         data: aoData,
                         success: function (data) {
                            fnCallback(data);
                            if(data.ListEMSData.length == 0){
		                        $('#tblCampaignList_paginate').hide();
		                    }
                         }
                });
            },
            "fnInitComplete":function(oSettings, json){
                // $('#tblCampaignList_paginate').hide();
            }
        });

        var keyword = typeof(keyword)!='undefined'?keyword:"";

        $('#tblKeywordList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "KEYWORD", "sName": 'Keyword', "sTitle": 'Name', "bSortable": false, "sClass": "keyword-table-detail","sWidth": "80px"},
                {"mData": "TIMEUSED", "sName": 'Number of Times Used', "sTitle": 'Times Used', "bSortable": false, "sClass": "keyword-table-detail times-used-detail","sWidth": "90px"}
            ],
            "sAjaxDataProp": "LISTKEYWORD",
            "sAjaxSource": '/session/sire/models/cfc/keywords.cfc?method=GetKeywordByUserId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpKeyword", "value": keyword},
                    { "name": "inpUserId", "value": userID}

                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.LISTKEYWORD.length == 0){
	                        $('#tblKeywordList_paginate').hide();
	                    }
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                // $('#tblKeywordList_paginate').hide();
            }
        });

	}

	function GetAccReport3(userID){//get user activity report
		$('#tblAccActivityList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "MODULE", "sName": '', "sTitle": 'Module Name', "bSortable": false, "sClass": "","sWidth": "80px"},
                {"mData": "OPERATOR", "sName": '', "sTitle": 'Operator', "bSortable": false, "sClass": "","sWidth": "90px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "","sWidth": "90px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetUserActivityReport&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpUserId", "value": userID}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
	                        $('#tblAccActivityList_paginate').hide();
	                    }
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                // $('#tblKeywordList_paginate').hide();
            }
        });
	}

	function GetAccReport4(userID){
		$('#tblApiTrackingList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "SUBJECT", "sName": '', "sTitle": 'Module Name', "bSortable": false, "sClass": "","sWidth": "80px"},
                {"mData": "OUTPUT", "sName": '', "sTitle": 'Operator', "bSortable": false, "sClass": "","sWidth": "90px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "","sWidth": "90px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetUserApiTrackingReport&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpUserId", "value": userID}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
	                        $('#tblApiTrackingList_paginate').hide();
	                    }
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                // $('#tblKeywordList_paginate').hide();
            }
        });
	}

	function GetAccReport5(userID){
		$('#tblBlastCampaignList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "fnStateLoadParams": function (oSettings, oData) {

            },
            "fnStateSaveParams": function (oSettings, oData) {

            },
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 5,
            "bAutoWidth": false,
            "aoColumns": [
                {"mData": "ID", "sName": '', "sTitle": 'Campaign Id', "bSortable": false, "sClass": "","sWidth": "80px"},
                {"mData": "NAME", "sName": '', "sTitle": 'Campaign Name', "bSortable": false, "sClass": "","sWidth": "120px"},
                // {"mData": "GROUPNAME", "sName": '', "sTitle": 'Subscriber List', "bSortable": false, "sClass": "","sWidth": "80px"},
                {"mData": "TOTALSEND", "sName": '', "sTitle": 'Sent', "bSortable": false, "sClass": "","sWidth": "70px"},
                {"mData": "TOTALINQUEUE", "sName": '', "sTitle": 'InQueue', "bSortable": false, "sClass": "","sWidth": "70px"},
                {"mData": "SCHEDULE", "sName": '', "sTitle": 'Schedule', "bSortable": false, "sClass": "","sWidth": "140px"},
            ],
            "sAjaxDataProp": "ListEMSData",
            "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetUserBlastCampaignReportNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

            },
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

                aoData.push(
                    { "name": "inpUserId", "value": userID}
                );

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        fnCallback(data);
                        if(data.DATALIST.length == 0){
	                        $('#tblBlastCampaignList_paginate').hide();
	                    }
                    }
                });
            },
            "fnInitComplete":function(oSettings){
                // $('#tblKeywordList_paginate').hide();
            }
        });
	}

	function GetAllAccReport(reportType,userID){
		if(reportType == 1){
			GetAccReport1(userID);
			GetAccReport2(userID);
			$('#acc-dashboard-report').show();
			$('#acc-campaign-in-queue').hide();
			$('#acc-activity').hide();
			$('#acc-api-report').hide();
		}
		else if(reportType == 2){
			GetAccReport5(userID);
			$('#acc-dashboard-report').hide();
			$('#acc-campaign-in-queue').show();
			$('#acc-activity').hide();
			$('#acc-api-report').hide();
		}
		else if(reportType == 3){
			GetAccReport3(userID);
			$('#acc-dashboard-report').hide();
			$('#acc-campaign-in-queue').hide();
			$('#acc-activity').show();
			$('#acc-api-report').hide();
		}
		else if(reportType == 4){
			GetAccReport4(userID);
			$('#acc-dashboard-report').hide();
			$('#acc-campaign-in-queue').hide();
			$('#acc-activity').hide();
			$('#acc-api-report').show();
		}
	}
	// get default coupon by affilite code that user apply
	function GetUserDefaultCouponByAffiliateUser(userID)
	{
		var rtCouponCode="";
		$.ajax({
			type:"POST",
			url:'/public/sire/models/cfc/affiliate.cfc?method=GetAppliedAffiliateCodeByUserID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType:'json',
			async: false,
			data:
				{
					inpUserId:userID
				}
			,beforeSend:function(xhr){
				$('#processingPayment').show()
			}
			,error:function(XMLHttpRequest,textStatus,errorThrown){
				$('#processingPayment').hide()

				alertBox("Oops!","Unable to get your infomation at this time. An error occurred.");
			}
			,success:function(data){
				$('#processingPayment').hide();
				if(data.RXRESULTCODE==1){
					//data.AFFILIATECODE
					$.ajax({
						type:"POST",
						url:'/public/sire/models/cfc/affiliate.cfc?method=GetCouponFromAffiliateCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType:'json',
						async: false,
						data:
							{
								inpAffiliateCode:data.AFFILIATECODE
							}
						,beforeSend:function(xhr){
							$('#processingPayment').show()
						}
						,error:function(XMLHttpRequest,textStatus,errorThrown){
							$('#processingPayment').hide()

							alertBox("Oops!","Unable to get your infomation at this time. An error occurred.");
						}
						,success:function(d){
							$('#processingPayment').hide();
							if(d.RXRESULTCODE==1){
								//
								rtCouponCode=d.COUPONCODE;
							}

						}
					});
				}

			}
		});
		return rtCouponCode;
	}
	$('body').on('click', 'a.btn-account-report', function(event){
		GetAllAccReport($('#report-select').val(), $(this).data('id'));

		$('#user-account-report').modal('show');

		$('#userIdToGetReport').val($(this).data('id'));
	});

	$('#report-select').on('change', function(){
		GetAllAccReport($('#report-select').val(), $('#userIdToGetReport').val());
	});

	$('#user-account-report').on('hidden.bs.modal', function(){
		document.getElementById("acc-report-form").reset();
	});

})(jQuery);


$(document).ready(function() {
	$.ajax({
        method: 'POST',
        url: '/session/sire/models/cfc/billing.cfc?method=GetPaymentMethodSetting&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
        data:{},
        dataType: 'json',
        success: function(data) {
            if (data && data.RXRESULTCODE == 1) {
				$('#payment_method_value').val(data.RESULT);
				$(".select_card_type").show();
				$(".card-support-type").show();
				$(".inner-update-cardholder").show();
				$("#section-2.inner-update-cardholder").show();
            }
        }
    });

	$('#monthly-plan-text').css('color', '#568ca5');
	$(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });

	function update_expiration_date_upgrade() {
        $('#expiration_date_upgrade').val($('#expiration_date_month_upgrade').val() + '/' + $('#expiration_date_year_upgrade').val());
    }

    $('#expiration_date_month_upgrade, #expiration_date_year_upgrade').change(function(){
        update_expiration_date_upgrade();
	});

	$('#expiration_date_month_admin, #expiration_date_year_admin').change(function(){
        update_expiration_date_admin();
    });

    function update_expiration_date_admin() {
        $('#expiration_date_admin').val($('#expiration_date_month_admin').val() + '/' + $('#expiration_date_year_admin').val());
    }

    $('#buyaddon-modal-sections .select_payment_method').click(function(){
	    $('#buyaddon-modal-sections #card_number').val('');
	    $('#buyaddon-modal-sections #security_code').val('');
	    $('#buyaddon-modal-sections #expiration_date').val('');
	    $('#buyaddon-modal-sections #first_name').val('');
	    $('#buyaddon-modal-sections #last_name').val('');

	    if($(this).val() == 2){
	        $('#payment_method_value').val(2);
	        $('.inner-update-cardholder').hide();
			$('.card-support-type').hide();
	        $('.btn-update-profile').prop('disabled', false);
	        if(upgradePlan == 1){
	            $('#section-2').show();
	        }
	    }
	    else{
	        $('#payment_method_value').val(1);
	        $('.inner-update-cardholder').show();
	        $('.card-support-type').show();
	        if(upgradePlan == 0){
	            $('#section-2').hide();
	        }
	    }
	});

	// $('#user-account-update .select_payment_method').click(function(){
    // 	if($(this).val() == 2){
    // 		$(".select_card_type").hide();
    // 		$('#payment_method_value').val(2);
	// 		$(".card-support-type").hide();
	// 		$('.fset_cardholder_info').hide();
    // 	}
    // 	else
    // 	{
    // 		$('#payment_method_value').val(1);
    // 		$(".select_card_type").show();
	// 		$(".card-support-type").show();
	// 		if($("#select_used_card_value").val() == 2){
	// 			$('.fset_cardholder_info').show();
	// 		}
	// 		else{
	// 			$('.fset_cardholder_info').hide();
	// 		}
    // 	}


	// });

	// affiliate code
	$(document).on('click', '.btn-apply-affiliate', function(event){
		event.preventDefault();
		var userID = $(this).data('id');
		GetAppliedAffiliateCodeByUserID(userID).then(function(data){
			$('#mdApplyAffiliate').modal("show");
			$('#mdApplyAffiliate').find('#selectUserID').val(userID);
			if(data.RXRESULTCODE==1)
			{
				$('#mdApplyAffiliate').find('#inpAffiliateCode').val(data.AFFILIATECODE);
			}

		})
	});
	$("#btn-update-affiliatecode").on('click',function(){
		if ($("#frmApplyAffiliate").validationEngine('validate'))
		{
			var userID=$('#mdApplyAffiliate').find('#selectUserID').val();
			var affiliateCode=$('#mdApplyAffiliate').find('#inpAffiliateCode').val();
			ApplyAffiliateCode(userID,affiliateCode).then(function(data){
				if(data.RXRESULTCODE==1){
					alertBox("Apply Affiliate Code Successfully","Successfully");
					$('#mdApplyAffiliate').modal("hide");
				}
				else
				{
					alertBox(data.MESSAGE,"Oops!");
				}
			}).catch(function(data){
				alertBox(data.MESSAGE,"Oops!");
			});
		}
	});
	function GetAppliedAffiliateCodeByUserID(userID)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=GetAppliedAffiliateCodeByUserID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	function ApplyAffiliateCode(userID,affiliateCode)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=ApplyAffiliateCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID,
						inpAffiliateCode:affiliateCode
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to update your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	//end affiliate code
	// commission setting
	function GetCommissionSetting(userID)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=GetCommissionSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	function SaveCommissionSetting(userID, setting)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=SaveCommissionSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID,
						inpSetting:	JSON.stringify(setting)
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to save your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	function CommissionSettingTemplate(condition,value,percent)
	{
		var commissionSettingTemplate='';
		commissionSettingTemplate+='<div class="row row-setting" >';
			commissionSettingTemplate+='<div class="col-md-3">';
				commissionSettingTemplate+='<div class="form-group">';
					commissionSettingTemplate+='<select name="condition" class="form-control condition validate[required]">';
					commissionSettingTemplate+='<option value="0" '+(condition == 0 ? "selected" : "")+'>Less Than Equals</option>';
					commissionSettingTemplate+='<option value="1" '+(condition == 1 ? "selected" : "")+'>Greater Than</option>';
					commissionSettingTemplate+='</select>';
				commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='<div class="col-md-4">';
				commissionSettingTemplate+='<div class="form-group">';
					commissionSettingTemplate+='<input type="text" maxlength="8" class="form-control value validate[required,custom[onlyNumber],maxSize[8]]" name="value" value="'+value+'">';
				commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='<div class="col-md-4">';
				commissionSettingTemplate+='<div class="form-group">';
					commissionSettingTemplate+='<input type="text" maxlength="3" class="form-control percent validate[required,custom[onlyNumber],maxSize[3]]" name="percent" value="'+percent+'">';
				commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='<div class="col-md-1">';
				commissionSettingTemplate+='<div class="form-group">';
					commissionSettingTemplate+='<i class="fa fa-plus-circle add-setting" aria-hidden="true"></i>';
					commissionSettingTemplate+='<i class="fa fa-minus-circle remove-setting" aria-hidden="true"></i>';
				commissionSettingTemplate+='</div>';
			commissionSettingTemplate+='</div>';
		commissionSettingTemplate+='</div>';
		return commissionSettingTemplate;
	}

	$(document).on('click', '.btn-commission-setting', function(event){
		event.preventDefault();
		var userID = $(this).data('id');
		GetCommissionSetting(userID).then(function(data){
			$('#mdCommissionSetting').modal("show");
			$('#mdCommissionSetting').find('#selectUserID').val(userID);
			if(data.RXRESULTCODE==1)
			{
				$('#mdCommissionSetting').find('#setting-content').html('');
				//
				data.SETTING.map(function(setting){
					var commissionSettingTemplate=CommissionSettingTemplate(setting.CONDITION,setting.VALUE,setting.PERCENT);
					$('#mdCommissionSetting').find('#setting-content').append(commissionSettingTemplate);
				})
			}
			else
			{
				alertBox(data.MESSAGE,"Oops!");
			}

		}).catch(function(data){
			alertBox(data.MESSAGE,"Oops!");
		});
	});
	$(document).on("click",".row-setting .add-setting",function(){
		var commissionSettingTemplate=CommissionSettingTemplate(0,0,0);
		$(this).closest(".row-setting").after(commissionSettingTemplate);
	})
	$(document).on("click",".row-setting .remove-setting",function(){
		if($(".row-setting").length > 1)
		{
			$(this).closest(".row-setting").remove();
		}

	})
	$("#btn-update-commission-setting").on('click',function(){
		if ($("#frmCommissionSetting").validationEngine('validate'))
		{
			var userID=$('#frmCommissionSetting').find('#selectUserID').val();
			var setting= [];
			$("#frmCommissionSetting").find(".row-setting").each(function(){
				var condition=$(this).find('.condition').val();
				var value=$(this).find('.value ').val();
				var percent=$(this).find('.percent ').val();
				var row= {
					condition:condition,
					value:value,
					percent:percent
				}
				setting.push(row);
			})


			SaveCommissionSetting(userID,setting).then(function(data){
				if(data.RXRESULTCODE==1){
					alertBox("Save commission setting Successfully","Successfully");
					$('#mdCommissionSetting').modal("hide");
				}
				else
				{
					alertBox(data.MESSAGE,"Oops!");
				}
			}).catch(function(data){
				alertBox(data.MESSAGE,"Oops!");
			});
		}
	});
	//end commission setting
	// affiliate linkage coupon
	function SaveAffiliateLinkageCoupon(userID,affiliateId, couponCode)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=SaveAffiliateLinkageCoupon&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID,
						inpAffiliateId: affiliateId,
						inpCouponCode:	couponCode
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to save your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	function GetAffiliateLinkageCoupon(userID)
	{
		return new Promise( function (resolve, reject) {
			$.ajax({
				type:"POST",
				url:'/public/sire/models/cfc/affiliate.cfc?method=GetAffiliateLinkageCoupon&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType:'json',
				data:
					{
						inpUserId:userID
					}
				,beforeSend:function(xhr)
					{
					$('#processingPayment').show()
				}
				,error:function(XMLHttpRequest,textStatus,errorThrown){
					$('#processingPayment').hide()
					var data ={MESSAGE:"Unable to get your infomation at this time. An error occurred."};
					reject(data);
				}
				,success:function(data)
					{
					$('#processingPayment').hide();
					if(data.RXRESULTCODE==1){
						resolve(data);
					}
					else
					{
						reject(data);
					}
				}
			});
		});
	}
	$(document).on("click",".btn-affiliate-coupon",function(){
		var userId= $(this).data("id");
		var affiliateId= $(this).data("affiliate-id-assign");
		var affiliateCode= $(this).data("affiliate-code-assign");
		var modalAffiliateCoupon= $('#mdAffiliateCoupon');
		modalAffiliateCoupon.find("#affiliate-code").text(affiliateCode);
		modalAffiliateCoupon.find("#affiliateId").val(affiliateId);
		GetAffiliateLinkageCoupon(userId).then(function(data){
			if(data.RXRESULTCODE==1){
				modalAffiliateCoupon.find("#userId").val(userId);
				modalAffiliateCoupon.find("#couponCode").val(data.COUPONCODE);
				modalAffiliateCoupon.modal("show");
			}
			else
			{
				alertBox(data.MESSAGE,"Oops!");
			}
		}).catch(function(data){
			alertBox(data.MESSAGE,"Oops!");
		});


	})
	$("#btn-affiliate-coupon-submit").on('click',function(){
		if ($("#frAffiliateCoupon").validationEngine('validate'))
		{
			var userID=$('#mdAffiliateCoupon').find('#userId').val();
			var couponCode=$('#mdAffiliateCoupon').find('#couponCode').val();
			var affiliateId= $('#mdAffiliateCoupon').find('#affiliateId').val();

			SaveAffiliateLinkageCoupon(userID,affiliateId,couponCode).then(function(data){
				if(data.RXRESULTCODE==1){
					alertBox("Affiliate Code and Coupon Code linkage successfully","Successfully");
					$('#mdAffiliateCoupon').modal("hide");
				}
				else
				{
					alertBox(data.MESSAGE,"Oops!");
				}
			}).catch(function(data){
				alertBox(data.MESSAGE,"Oops!");
			});
		}
	});
	// end affiliate linkage coupon

});
