$(document).ready(function(){

InitControl();	
var _tblListEMS;
	function InitControl(customFilterObj)
	{//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					} else {
						history.replaceState(history.state, document.title, location.pathname + location.search);
					}
				}
			},

			
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"sName": 'id', "sTitle": 'Unique ID',"bSortable": false,"sClass":"cpp-id"},
				{"sName": 'name', "sTitle": 'CPP Name',"bSortable": true,"sClass":"cpp-name"},
				{"sName": 'url', "sTitle": "CPP Url","bSortable": true,"sClass":"cpp-url"},
				{"sName": 'link', "sTitle": 'Actions',"bSortable": false,"sClass":"cpp-action"}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/mlp.cfc?method=getAdminCPPList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   "fnDrawCallback": function( oSettings ) {
		      // MODIFY TABLE
				//$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					}
					else{
						$('#tblListEMS_paginate').show();
					} 
				})
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		        if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				// MODIFY TABLE
				
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');

					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					} 
				})
			}
	    });
	}


	
	
});	