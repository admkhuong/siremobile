$(document).ready(function(){

InitControl();	
var _tblListEMS;
	function InitControl(customFilterObj)
	{//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					} else {
						history.replaceState(history.state, document.title, location.pathname + location.search);
					}
				}
			},

			
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"sName": 'id', "sTitle": 'ID',"bSortable": false,"sClass":"cpp-id","sWidth": '100'},
				{"sName": 'name', "sTitle": 'Template Name',"bSortable": false,"sClass":"cpp-name"},
				{"sName": 'status', "sTitle": 'Status',"bSortable": false,"sClass":"cpp-status","sWidth": '100'},
				{"sName": 'link', "sTitle": 'Actions',"bSortable": false,"sClass":"cpp-action"}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/mlp.cfc?method=getCPPListTemplate&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   "fnDrawCallback": function( oSettings ) {
		      // MODIFY TABLE
				//$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					}
					else{
						$('#tblListEMS_paginate').show();
					} 
				})
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		        if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				// MODIFY TABLE
				
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');

					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					} 
				})
			}
	    });
	}

//BTN-ACTION
	$( document ).on( "click", ".btn-change-cpp-status, .btn-delete-cpp", function(event) {
		event.preventDefault();
		
		var cppid = parseInt($(this).data('cpp-id'));
		var currentPage = $('input.paginate_text').val();
		
		var status = parseInt($(this).data('cpp-status'));
		var action = $(this).data('cpp-action');
		var urlName = "";

		if(action == 'delete'){
			var dialogText = 'Are you sure you want to delete this template?';
			urlName = '/session/sire/models/cfc/mlp.cfc?method=deleteCPPTempate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
		}
		else {
			var dialogText = 'Are you sure you want to update this template?';
			urlName = '/session/sire/models/cfc/mlp.cfc?method=updateStatusCPPTempate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
		}
		if(cppid > 0){
			try{

				$('.btn').prop('disabled',true);
				bootbox.dialog({
		        message: dialogText,
		        title: 'MLP Template',
		        className: "",
		        buttons: {
		            success: {
		                label: "OK",
		                className: "btn btn-medium btn-success-custom",
		                callback: function(result) {
		                    if(result){
								$.ajax(
								{
									type: "POST",
									url: urlName,   
									dataType: 'json',
									data: { inpCppId: cppid,inpStatus: status},
									beforeSend: function( xhr ) {
										$('.tblListEMS_processing').show();
										$('#processingPayment').show();
									},					  
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										$('#processingPayment').hide();
										bootbox.dialog({
										    message: "There is an error.Please try again later.",
										    title: "MLP Template",
										    buttons: {
										        success: {
										            label: "Ok",
										            className: "btn btn-medium btn-success-custom",
										            callback: function() {}
										        }
										    }
										});
										$('.btn').prop('disabled',false);
										$('.tblListEMS_processing').hide();
									},					  
									success:		
										function(d) {
											$('#processingPayment').hide();
											$('.btn').prop('disabled',false);
											$('.tblListEMS_processing').hide();
											if(d.RXRESULTCODE == 1){
												//goto success page
												bootbox.dialog({
												    message: d.MESSAGE,
												    title: "MLP Template",
												    buttons: {
												        success: {
												            label: "Ok",
												            className: "btn btn-medium btn-success-custom",
												            callback: function() {}
												        }
												    }
												});
												var oTable = $('#tblListEMS').dataTable();
												//oTable.fnDraw();
												oTable.fnPageChange(parseInt(currentPage-1));
												return;
											}
											else{
												bootbox.dialog({
												    message: "There is an error.Please try again later.",
												    title: "MLP Template",
												    buttons: {
												        success: {
												            label: "Ok",
												            className: "btn btn-medium btn-success-custom",
												            callback: function() {}
												        }
												    }
												});
											}
										} 		
									});
								}
			                }
			            },
			            cancel: {
			                label: "Cancel",
			                className: "btn btn-medium btn-back-custom",
			                callback: function() {
							$('.btn').prop('disabled',false);
			                }
			            },
		        	}
		    	});
				
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: "There is an error.Please try again later.",
					title: "MLP Template",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
				$('.btn').prop('disabled',false);
				$('.tblListEMS_processing').hide();
			}
		}
		else
		{
			bootbox.dialog({
			    message: "There is an error.Please try again later.",
				title: "MLP Template",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	$('.btn').prop('disabled',false);
			            }
			        }
			    }
			});
		}
	});
	
	
	
});	