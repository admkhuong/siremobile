function htmlEncode(value){
  return $('<div/>').text(value).html();
}

function htmlDecode(value){
  return $('<div/>').html(value).text();
}

var RXxml_special_to_escaped_one_map = {
    '&' : '&amp;',
    '"' : '&quot;',
    '<' : '&lt;',
    '>' : '&gt;',
    "'" : '&apos;',
    "?" : '&#63;'   
};

var RXescaped_one_to_xml_special_map = {
    '&amp;' : '&',
    '&quot;' : '"',
    '&lt;' : '<',
    '&gt;' : '>',
    '&apos;' : "'",
    '&#63;' : "?" 
};
var RXspace = {
    ' ' : '%20'
}
var RXspacedecode = {
    '%20' : ' '
}

var isPreview = 0;

function RXencodeXML(string) {
    
    //console.log(string);
    //console.log('string=(' + string + ')');
    
    var retStr = String(string);
    if (isNaN(retStr)) {
        retStr = retStr.replace(/([\&"<'>?])/g, function(str, item) {
            return RXxml_special_to_escaped_one_map[item];
        });
    }
    return retStr;
};

function RXdecodeXML(string) {
    
    var retStr = String(string);
    if (isNaN(retStr)) {
        retStr = retStr.replace(/(&quot;|&lt;|&gt;|&amp;|&apos;|&#63;)/g,  function(str, item) {
            return RXescaped_one_to_xml_special_map[item];
        });
    }
    return retStr;
}

function RXencodeSpace(string) {

    var retStr = String(string); 
    return retStr.replace(/([\ ])/g, function(str, item) {
        return RXspace[item];
    });
}
function RXdencodeSpace(string) {
    
    var retStr = String(string); 
    return retStr.replace(/(%20)/g, function(str, item) {
        return RXspacedecode[item];
    });
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

(function($){

    // CHeck term of service event
    $('#checkTerms').change(function () {
        var checkTerms = $('#checkTerms').is(':checked');
        if(checkTerms){
            $('.wrapper-terms_of_service').show();
        }
        else{
            $('.wrapper-terms_of_service').hide();   
        }
    });

    // Submit setup CPP form
    $("#create_cpp_frm").validationEngine({
        promptPosition : "topLeft", 
        autoPositionUpdate: true, 
        scroll: false,
        focusFirstField : false,
        onValidationComplete: function(form, status){
           
            if (status) {
                var checkTerms = $('#checkTerms').is(':checked');

                if(checkTerms){ // check if terms of service is check
                    tinymce.triggerSave();
                    var termText = $('#cppxTermOfService').val();
                    if(termText.length > 0){
                       // do nothing
                    }
                    else{
                        bootbox.dialog({
                            message:'Please input term of service',
                            title: "Setup MLP",
                            buttons: {
                                success: {
                                    label: "Ok",
                                    className: "btn btn-medium btn-success-custom",
                                    callback: function() {
                                        tinymce.execCommand('mceFocus',false,'terms_of_service');
                                    }
                                }
                            }
                        });
                        return false;
                    }
                    
                }

                // CALL AJAX TO SAVE CPP
                try{
                    var cppName = $('#cppxName').val();
                    var cppxURL = $('#cppxURL').val();
                    var checkTerms = $('#cppxURL').val();
                    var cppxTermOfService = RXencodeXML(tinymce.get('cppxTermOfService').getContent());

                    var checkTerms = $('#checkTerms').is(':checked');
                    if(checkTerms){
                       checkTerms = 1;
                    }
                    else{
                        checkTerms = 0;  
                    }

                    $.ajax({
                    type: "POST",
                    url: '/session/sire/models/cfc/mlp.cfc?method=SetupCPP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                    dataType: 'json',
                    //data: $('#create_cpp_frm').serialize(),
                    data: {cppxName:cppName,cppxURL:cppxURL,checkTerms:checkTerms,cppxTermOfService:cppxTermOfService},
                    beforeSend: function( xhr ) {
                        $('#processingPayment').show();
                    },                    
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#processingPayment').hide();
                        bootbox.dialog({
                            message:'Create MLP Fail.Please try again later.',
                            title: "Setup MLP",
                            buttons: {
                                success: {
                                    label: "Ok",
                                    className: "btn btn-medium btn-success-custom",
                                    callback: function() {}
                                }
                            }
                        });
                    },                    
                    success:        
                        function(d) {
                            $('#processingPayment').hide();
                            if(d.RXRESULTCODE == 1){
                                bootbox.dialog({
                                    message:d.MESSAGE,
                                    title: "Setup MLP",
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn btn-medium btn-success-custom",
                                            callback: function() {window.location.href='/session/sire/pages/mlp-edit?ccpxDataId='+parseInt(d.CPPID)+'&action=Create'}
                                        }
                                    }
                                });
                            }
                            else{
                                bootbox.dialog({
                                    message:'Create MLP Fail.Please try again later.',
                                    title: "Setup MLP",
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn btn-medium btn-success-custom",
                                            callback: function() {}
                                        }
                                    }
                                }); 
                            }

                            return false;
                        }       
                    });
                }catch(ex){
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message:'Update Fail',
                        title: "My Account",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                }
            } else {
            //validate fail, do nothing here
                 
            }
            
            form.validationEngine('showPrompt', null, 'load', 'topLeft');
        }
    }).submit(function(event){
        event.preventDefault();
    });

    // Submit Edit CPP form
    $("#edit_cpp_frm").validationEngine({
        promptPosition : "topLeft", 
        autoPositionUpdate: true, 
        scroll: false,
        focusFirstField : false,
        onValidationComplete: function(form, status){
            if (status) {
               saveCPP();
            } else {
                //validate fail, do nothing here
            }
            
            form.validationEngine('showPrompt', null, 'load', 'topLeft');
        }
    }).submit(function(event){
        event.preventDefault();
    });

	
    //SAVE CPP
   function saveCPP(){

            var inst, contents = new Object();
            for (inst in tinyMCE.editors) {
                if (tinyMCE.editors[inst].getContent)
                    contents[inst] = tinyMCE.editors[inst].getContent();
            }

            // GET DATA OBJECT TO POST
            var arrObject = [{'ID':ccpxDataId}];
            var inputObj = 0;
            var subListObj = 0;

            $('.cpp-object').each(function(){
                var object = new Object();

                if( $(this).hasClass('sms-number') || $(this).hasClass('voice-number') || $(this).hasClass('email-address') ){
                    object['TYPE'] = $(this).attr('name');
                    object['VALUE'] = RXencodeXML($(this).val());
                    arrObject.push(object);
                    inputObj = 1;
                }
                else if( $(this).hasClass('textHtml') ){
                    object['TYPE'] = $(this).attr('name');
                    var tinyMCEId = $(this).attr('id');
                    object['VALUE'] = RXencodeXML(tinymce.get(tinyMCEId).getContent());
                    arrObject.push(object);
                }
                else if( $(this).hasClass('subcriber-list-group') ){
                    var subName = $(this).find('.subcriber-name').val();
                    var subList = $(this).find('.subcriber-list').val();

                    object['TYPE'] = 'subcriber-list-group';
                    object['NAME'] = subName;
                    object['LIST'] = subList;

                    arrObject.push(object);
                    subListObj = 1;
                }
                else if($(this).hasClass('terms_of_service')){
                    var checkTerms = $('#checkTerms').is(':checked');
                    if(checkTerms){
                        object['TYPE'] = $(this).attr('name');
                        var tinyMCEId = $(this).attr('id');
                        object['VALUE'] = RXencodeXML(tinymce.get(tinyMCEId).getContent());
                        arrObject.push(object);
                    }
                    else{
                       
                    }
                }
                /*
                else if($(this).hasClass('multi_subcriber_list')){
                    var checkMultiSubList = $('#multi_subcriber_list').is(':checked');
                    if(checkMultiSubList){
                        object['TYPE'] = $(this).attr('name');
                        object['VALUE'] = 1
                        arrObject.push(object);
                    }
                    else{
                       
                    }
                } */
            });
            
            // At least one object is create
            if(arrObject.length == 1){

                bootbox.dialog({
                        message:'Please create at least one object to save.',
                        title: "Setup MLP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                return;
            }

            // If sms,email or voice is create, it have to move to one subcriber list
            if( inputObj == 1 && subListObj ==0)
            {
                 bootbox.dialog({
                        message:'Please create at least one subcriber list.',
                        title: "Setup MLP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                return;
            }

             // If sms,email or voice is create, it have to move to one subcriber list
            if( inputObj == 0 && subListObj == 1)
            {
                 bootbox.dialog({
                        message:'Please create at least one sms number/voice number/email address.',
                        title: "Setup MLP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                return;
            }
            
            var checkMultiSubList = $('#multi_subcriber_list').is(':checked');
            if(checkMultiSubList){
               checkMultiSubList = 1;
            }
            else{
                checkMultiSubList = 0;  
            }
            
            //console.log($(".select_used_css:checked").val());
            try{
                $.ajax({
                type: "POST",
                url: '/session/sire/models/cfc/mlp.cfc?method=UpdateCPPTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                dataType: 'json',
                data: {'ccpxXMLDataString':JSON.stringify(arrObject),checkMultiSubList:checkMultiSubList,'templateName':$('#templateName').val(),'Description':$('#Description').val()},
                beforeSend: function( xhr ) {
                    $('#processingPayment').show();
                },                    
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                    bootbox.dialog({
                        message:'Create MLP Fail.Please try again later.',
                        title: "Setup MLP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    });
                },                    
                success:        
                    function(d) {
                        $('#processingPayment').hide();
                        if(d.RXRESULTCODE == 1){
                            bootbox.dialog({
                                message:d.MESSAGE,
                                title: "Setup MLP",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {
                                            if(isPreview == 1)
                                                var win = window.open('/session/sire/pages/admin-mlp-template-preview-page?ccpxDataId='+d.CPPID, '_blank');
                                                if(win){
                                                    //Browser has allowed it to be opened
                                                    win.focus();
                                                    window.location.href='/session/sire/pages/admin-mlp-template-edit?ccpxDataId='+d.CPPID+'&action=Edit';
                                                }
                                            else    
                                                window.location.href='/session/sire/pages/admin-mlp-template';
                                        }
                                    }
                                }
                            });
                        }
                        else{
                            bootbox.dialog({
                                message:'Create MLP Fail.Please try again later.',
                                title: "Setup MLP",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {}
                                    }
                                }
                            }); 
                        }
                    }       
                });
            }catch(ex){
                $('#processingPayment').hide();
                bootbox.dialog({
                    message:'Update Fail',
                    title: "My Account",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {}
                        }
                    }
                }); 
                $('.btn-group-answer-security-question').show();            
            }
    }
    
    // INIT TINYMCE WHEN PAGE LOAD
    InitMCE('');

    reLoadSubList();
    
    //RELOAD SUBCRIBER LIST
    function reLoadSubList()
    {
    	$('.no-list').text("");
        $('.subcriber-list').parent().prepend('<div class="no-list"><strong> No subcriber list </strong></div>');
        $('.subcriber-list').hide();
    }
    
    // SORTABLE
    if( $( ".sortable" ).length >0)
    {
        $( ".sortable" ).sortable({

            cursor: "move",
            //delay: 150,
            //opacity: 0.5,

            start: function(){

                    newEditors = new Array();

                    for(i=0; i < $("textarea.normal").length; i++){
                        tinymce.triggerSave();
                        var content = tinymce.get(tinymce.editors[i].id).getContent();
                        //var id = tinymce.editors[i].id;
                        //newEditors[i] = {"id":id, "content":content};
                    }

                    //tinymce.remove(); //as a fix for FF

                    //setTimeout(function () {
                        //tinymce.remove('textarea.normal');
                       
                   // }, 1);
                       
            },
            update:function(){
                 $(this).find('.textHtml').each(function(){
                            var textareaId = $(this).attr('id');
                            tinymce.remove("#"+textareaId);
                        });
            },
            stop: function(){
                InitMCE(''); //re-initiate the tinymce again (if you don't do that, the field wont be editable)
                /*for(var nr in newEditors){
                    tinyMCE.get(newEditors[nr].id).setContent(newEditors[nr].content);
                }*/
            }

        });
    }
    
    //$( ".sortable" ).disableSelection();

    // INIT TINYMCE
    function InitMCE(id){
        if(id !='')
        {
            tinymce.init({
            selector: "li#"+id+' textarea.normal',
                menubar: false,
                plugins: "link,anchor,preview,code,textcolor",
                width:"99%",
                toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | link unlink anchor | forecolor | preview code",
                setup : function(ed) {
                    ed.on("click", function() {
                        ed.focus();
                    });
                }
            });
        }
        else{

            tinymce.init({
                selector: "textarea.normal",
                menubar: false,
                plugins: "link,anchor,preview,code,textcolor",
                width:"99%",
                toolbar: "bold italic underline | alignleft aligncenter alignright alignjustify | link unlink anchor | forecolor | preview code",
                setup : function(ed) {
                    ed.on("click", function() {
                        ed.focus();
                    });
                }
            });
        }
    }

    //LOAD SUBCRIBERLIST
    function loadSubcriberList(containerId){
    	$('.no-list').text("");
        $('.subcriber-list').parent().prepend('<div class="no-list"><strong> No subcriber list </strong></div>');
        $('.subcriber-list').hide();
    }

    //save state of select subcriberlist
    $( document ).on( "change", ".subcriber-list", function(event){
        var value = $(this).val();
        $(this).data('pre_val',value);
    });

    // Remove OBJECT POP MODAL
    $( document ).on( "click", ".cpp-object-btn-delete", function(event){
        var parent = $(this).parent().parent().parent();       
        var HTMLObject = $(this).parent().parent().find('textarea');

        bootbox.dialog({
            message: 'Are you sure you want to delete this object?',
            title: 'Customer Preference Portal',
            className: "",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium btn-success-custom",
                    callback: function(result) {
                        if(HTMLObject.length > 0){
                            tinymce.remove("#"+HTMLObject.attr('id'));
                        }
                        parent.remove();
                        countSubcriberList();
                        $( ".sortable" ).sortable( "refreshPositions" );
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "btn btn-medium btn-back-custom",
                    callback: function() {
                    
                    }
                },
            }
        });
    });

    // ADD NEW OBJECT POP MODAL
    $( document ).on( "click", ".btn-modal-add-obj", function(event){
        var parent = $(this).parent().parent();
        var currentObjectId = parent.attr('id');
        $('#addObjectModal #current_obj_id').val(currentObjectId);
        $('#addObjectModal').modal('show');
    });

    //ADD NEW OBJECT BUTTON EVENT
    $('#btn-add-object').on( "click", function() {
        var selected_type = $('#select_object').val();
        // CHECK ONLY ONE SMS,EMAIL,VOICE OBJECT
        var checkNumberSMS = $('.sms-number').length;
        var checkNumberVoice = $('.voice-number').length;
        var checkNumberEmail = $('.email-address').length;

        if(selected_type == 'SMS' && checkNumberSMS == 1 ){
            bootbox.dialog({
                message: "Can not input more than one SMS object",
                title: "Customer Preference Portal - Embeb code",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {
                        }
                    }
                }
            });

            return false;
        }

        if(selected_type == 'EMAIL' && checkNumberEmail == 1 ){
            bootbox.dialog({
                message: "Can not input more than one Email object",
                title: "Customer Preference Portal - Embeb code",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {
                        }
                    }
                }
            });
            
            return false;
        }

        if(selected_type == 'VOICE' && checkNumberVoice == 1 ){
            bootbox.dialog({
                message: "Can not input more than one Voice object",
                title: "Customer Preference Portal - Embeb code",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {
                        }
                    }
                }
            });
            
            return false;
        }

        var randomId = 'obj_'+Date.now();
        var template_html = '<li id='+randomId+'>';
        template_html+='<div class="btn-group pull-right"><button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button> </div>';
        template_html+='<div class="clearfix"></div>';
        template_html+='<div class="objectItem clearfix"><div class="col-sm-12"><label class="control-point-label">%LABEL%</label><a data-object-type="%OBJTYPE%" class="obj-cpp-preview">Preview</a><button type="button" class="btn btn-link pull-right cpp-object-btn-delete"><img src="/session/sire/images/close.png"></button></div><div class="col-sm-12 content-area">%CONTENT%</div></div>';
        template_html+='</li>';

        var selected_obj = objectList[selected_type];

        switch(selected_type){
            case 'HTML'  :
            case 'SMS'   :
            case 'EMAIL' :
            case 'VOICE' :{
                var html = template_html.replace("%LABEL%",selected_obj['LABEL']);
                html = html.replace("%CONTENT%",selected_obj['CONTENT']); 
                html = html.replace("%VALUE%",''); 
				html = html.replace("%OBJTYPE%",selected_obj['OBJTYPE']); 
				
                var currentPosition = $('#addObjectModal #current_obj_id').val();
                $('li#'+currentPosition).before(html);

                // INIT TEXTEDITOR IF HAVE 
                var checkTextArea = html.search("<textarea");
                if(checkTextArea > 0){

                   InitMCE(randomId);
                } 
                break;
            }
            case 'SUBCRIBERLIST':{

                var html = template_html.replace("%LABEL%",selected_obj['LABEL']);
                html = html.replace("%CONTENT%",selected_obj['CONTENT']); 
                html = html.replace("%VALUE%",''); 
				html = html.replace("%OBJTYPE%",selected_obj['OBJTYPE']); 
				
                var currentPosition = $('#addObjectModal #current_obj_id').val();
                $('li#'+currentPosition).before(html);

                loadSubcriberList('li#'+randomId);
                countSubcriberList();

                break;
            }
          }

           $( ".sortable" ).sortable( "refreshPositions" );

    });

    function countSubcriberList(){
        var list = $('.subcriber-list-group').length;
        if( parseInt(list) >= 2){
            $('.checkMultiSubList').show();
            $('.subcriber-name').addClass('validate[required]');
        }
        else{
            $('.subcriber-name').removeClass('validate[required]');
            $('#multi_subcriber_list').prop('checked', false);
            $('.checkMultiSubList').hide();   
        }
    }


    $( document ).on( "click", ".input_embed_code", function(event) {
         $(this).select();
    });

    $('.btn-preview-cpp-template').click(function(event) {
        event.preventDefault();
        bootbox.dialog({
            message: 'Please save the MLP Template for preview.',
            title: 'Customer Preference Portal Template',
            className: "",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium btn-success-custom",
                    callback: function(result) {
                        isPreview = 1;
                        $('.btn-save-cpp').trigger('click');
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "btn btn-medium btn-back-custom",
                    callback: function() {
                        isPreview = 0;
                    }
                },
            }
        });
     
    });

	 $( document ).on( "click", ".obj-cpp-preview", function(event){
        var parent = $(this).parent().parent().parent();       
        
        
        var objectType = "";
        var objData = [];
		
        
        var object = new Object();
        
        	//var tinyMCEId = $(this).parent().parent().find('.cpp-object');
        	//console.log(tinyMCEId);
      
      	if ($(this).data('object-type') == 'text-content') {
      		var obj = $(this).parent().parent().find('textarea');
      		var tinyMCEId = obj.attr('id')
            //object['VALUE'] = RXencodeXML(tinymce.get(tinyMCEId).getContent());
      		
      		objectType = $(this).data('object-type');
      		object['value'] = tinymce.get(tinyMCEId).getContent();
      	}
        if ($(this).data('object-type') == 'sms-number' || $(this).data('object-type') == 'email-address' || $(this).data('object-type') == 'voice-number') {
        	var obj = $(this).parent().parent().find('.cpp-object');
        	objectType = $(this).data('object-type');
        	object['value'] = obj.val();
        }
        
        if ($(this).data('object-type') == 'subcriber-name') {
        	var checkMultiSubList = $('#multi_subcriber_list').is(':checked');
        	var checkTotalSubList = $("input[name=\"subcriber-name\"]").length;
        	var ob = new Object();
        	object['checkMultiSubList'] = checkMultiSubList;
        	object['checkTotalSubList'] = checkTotalSubList;

        	$("input[name=\"subcriber-name\"]").each(function( index ) {
        		ob[index] = $(this).val();
        	});
        	object['value'] = ob;
			objectType = $(this).data('object-type');
        }
        
        objData.push(object);

        try{
			$.ajax({
				type: "POST",
				url: '/session/sire/models/cfm/mlp_object.cfm',
				data: {objectType:objectType, objData:JSON.stringify(objData)},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(errorThrown);
					$('#processingPayment').hide();
					bootbox.dialog({
					    message: "Get preview Fail",
					    title: "Campaigns",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
				},					  
				success:function(d){
					$('#processingPayment').hide();
					$('#previewObject .modal-body').html(d);
					$('#previewObject').modal('show');
				}
			});
		}catch(ex){
			console.log(ex);
			$('#processingPayment').hide();
			bootbox.dialog({
			    message: "Get preview Fail",
			    title: "Campaigns",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
		}
        
        
    });
    
})(jQuery);

