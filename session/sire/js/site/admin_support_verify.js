(function($){
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";

	function GetCarrierInfo(batchid){
        $("#tblList").html('');
        // var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

        var table = $('#tblList').dataTable({
            "bStateSave": false,
            "iStateDuration": -1,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide":true,
            "bDestroy":true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 100,
            "bAutoWidth": false,
            "aoColumns": [
                // {"mData": "ID", "sName": '', "sTitle": 'Id', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CONTACTSTRING", "sName": '', "sTitle": 'Phone', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CARRIER", "sName": '', "sTitle": 'Carrier', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "SHORTCODE", "sName": '', "sTitle": 'Shortcode', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "RESPONSE", "sName": '', "sTitle": 'Response', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "COUNTRY", "sName": '', "sTitle": 'Country', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "OPERATOR", "sName": '', "sTitle": 'Operator', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "OPERATORALIAS", "sName": '', "sTitle": 'Operator alias', "bSortable": false, "sClass": "", "sWidth": "200px"},
                {"mData": "CARRIERGROUP", "sName": '', "sTitle": 'Carrier Group', "bSortable": false, "sClass": "", "sWidth": "150px"},
                {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": false, "sClass": "", "sWidth": "150px"},
            ],
            "sAjaxDataProp": "DATALIST",
            "sAjaxSource": '/session/sire/models/cfc/admin.cfc?method=GetCarrierInfo'+strAjaxQuery,
            "fnDrawCallback": function( oSettings ) {
                if (oSettings._iDisplayStart < 0){
                    oSettings._iDisplayStart = 0;
                    $('input.paginate_text').val("1");
                }
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
                
                aoData.push(
                    { "name": "inpBatchId", "value": batchid}
                );
                // aoData.push(
                //     { "name": "customFilter", "value": customFilterData}
                // );

                // filter = customFilterData;

                oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                    	if(data.DATALIST.length == 0) {
			        		$('#tblList_paginate').hide();
			        	}
                        fnCallback(data);
                    }
                });
            }
        });
    }

    GetCarrierInfo($('#select-verify-batch').val());

    //Filter bar initialize
    // $('#box-filter').sireFilterBar({
    //     fields: [
    //         {DISPLAY_NAME: 'Campaign Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'IS', SQL_FIELD_NAME: 'si.BatchId_bi'},
    //         {DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: 'si.ContactString_vch'}
    //         ],
    //     clearButton: true,
    //     // rowLimited: 5,
    //     error: function(ele){
    //     // console.log(ele);
    //     },
    //     // limited: function(msg){
    //     //  alertBox(msg);
    //     // },
    //     clearCallback: function(){
    //         GetCarrierInfo();
    //         }
    //     }, function(filterData){
    //     //called if filter valid and click to apply button
    //     GetCarrierInfo(filterData);
    // });

    $('#select-verify-batch').on('change load', function(){
        GetCarrierInfo($('#select-verify-batch').val());
    });
})(jQuery);