(function($){

		var table;

		$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {

		    if(oSettings.oFeatures.bServerSide === false){
		        var before = oSettings._iDisplayStart;
		        oSettings.oApi._fnReDraw(oSettings);
		        //iDisplayStart has been reset to zero - so lets change it back
		        oSettings._iDisplayStart = before;
		        oSettings.oApi._fnCalculateEnd(oSettings);
		    }
		      
		    //draw the 'current' page
		    oSettings.oApi._fnDraw(oSettings);
		};


		function RefreshList(){
			table.fnStandingRedraw();
		}


		function GetCategoryList(strFilter, keepState){
			keepState = (typeof(keepState) == 'undefined' ? false : true );
			var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

			table = $('#tblListEMS').dataTable({
				"bStateSave": keepState,
				"iStateDuration": -1,
				"fnStateLoadParams": function (oSettings, oData) {

				},
				"fnStateSaveParams": function (oSettings, oData) {

				},
				"bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,
				"sPaginationType": "input",
			    "bLengthChange": false,
				"iDisplayLength": 20,
				"bAutoWidth": false,
			    "aoColumns": [
					{"sName": 'category', "sTitle": 'Category Group',"bSortable": false,"sClass":"cat-group"},
					{"sName": 'sub_category', "sTitle": 'Sub-Category',"bSortable": false,"sClass":"subcate-group"},
					{"sName": 'template', "sTitle": "Template","bSortable": false,"sClass":"template"}
				],
				"sAjaxDataProp": "DATALIST",
				"sAjaxSource": '/session/sire/models/cfc/category.cfc?method=GetDataList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				"fnRowCallback": function(nRow, aData, iDisplayIndex) {
					var objectInfo = aData[3];
					var removeBtn = $('<a class="btn btn-success-custom remove-template-item-btn"><i class="glyphicon glyphicon-trash"></a>').data('item', objectInfo);
					var renameBtn = $('<a></a>').data('item', objectInfo.TEMPLATEID).text(aData[2]);
					$(nRow).find('td.template').html(renameBtn);
					$('<td></td>').append($('<a class="btn btn-success-custom edit-template-item-btn"><i class="glyphicon glyphicon-edit"></a>')
											.data('item', objectInfo))
								  .append(removeBtn)
								  .append($('<div></div>').addClass('dropdown pull-left box-move-btns')
								  						  .html('<button class="btn btn-primary btn-success-custom dropdown-toggle" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-sort"></i></span></button>')
								  						  .append($('<ul class="dropdown-menu"></ul>').append($('<li></li>').append($('<a data-type="move-up" class="btn btn-change-order btn-success-custom"></a>').html('Up <i class="glyphicon glyphicon-arrow-up"></i>')))
								  						  											  .append($('<li></li>').append($('<a data-type="move-down" class="btn btn-change-order btn-success-custom"></a>').html('Down <i class="glyphicon glyphicon-arrow-down"></i>')))
								  						  											  .append($('<li></li>').append($('<a data-type="move-first" class="btn btn-change-order btn-success-custom"></a>').html('First <i class="glyphicon glyphicon-menu-up"></i>')))
								  						  											  .append($('<li></li>').append($('<a data-type="move-last" class="btn btn-change-order btn-success-custom"></a>').html('Last <i class="glyphicon glyphicon-menu-down"></i>')))
								  						  	     ).data('item', objectInfo)
								  		 )
								  .appendTo($(nRow));
		       	},
			   	"fnDrawCallback": function( oSettings ) {
			  		if (oSettings._iDisplayStart < 0){
						oSettings._iDisplayStart = 0;
						$('input.paginate_text').val("1");
					}
			    },
				"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
					
					if (aoData[3].value < 0){
						aoData[3].value = 0;
					}
			        aoData.push(
			            { "name": "customFilter", "value": customFilterData}
		            );
		        
			        oSettings.jqXHR = $.ajax({
				        "dataType": 'json',
				        "type": "POST",
				        "url": sSource,
				        "data": aoData,
				        "success": fnCallback
			      	});
		        },
				"fnInitComplete":function(oSettings){
					if(oSettings._iRecordsTotal == 0){
						this.parent().find('div.dataTables_paginate').hide();
					}
				},
				"fnHeaderCallback": function( nHead, aData, iStart, iEnd, aiDisplay ) {
					var Header = $(nHead);
					if(aData.length > 0){
						if(Header.find('th.action').length  == 0){
							Header.append('<th class="action">Actions</th>');
						}
					} else {
						Header.find('th.action').remove();
					}
			    }
			});
		}

		GetCategoryList();




		$('body').on('click', 'a.edit-template-item-btn', function(event){
			event.preventDefault();

			var itemObject = $(this).data('item');

			var hCategoryGroup = $('#hCategoryGroup');
			$("#category-template-update").data('itemObject', itemObject);

			hCategoryGroup.find('option').each(function(){
				var value = $(this).val();
				if(itemObject.CATGROUPID == value){
					$(this).prop('selected', true);	
				} 
			});

			hCategoryGroup.trigger('change');
			$('#category-template-update').modal('show');
		});




		$('#CategoryGroup').on('change', function(e){
			var value = $(this).val();
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=GetCategory&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: "json",
				data: {inpGroupID: value},
				async: false,
				success: function(data){
					var categorySelect = $('#Category');
					$('#TemplateID').html('<option value="0">All</option>');
					if(data.DATALIST.length){
						categorySelect.html('<option value="0">All</option>');
						for(var i in data.DATALIST){

							var option = data.DATALIST[i];
							$('<option></option>').attr('value', option.ID)
												  .text(option.NAME)
												  .appendTo(categorySelect);
						}
					}

					categorySelect.promise().done(function(thisDrop){
						categorySelect.trigger("change");
					});
				}
			}).done(function(){
				GetCategoryList(buildFilterData());
			});
		});


		$('#hCategoryGroup').on('change', function(e){
			var value = $(this).val();
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=GetCategory&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: "json",
				data: {inpGroupID: value},
				success: function(data){
					var categorySelect = $('#hCategory');
					categorySelect.empty();
					if(data.DATALIST.length){
						for(var i in data.DATALIST){

							var option = data.DATALIST[i];
							$('<option></option>').attr('value', option.ID)
												  .text(option.NAME)
												  .appendTo(categorySelect);
						}
					}
					
				}
			});
		});


		function buildFilterData(){
			var dataFilter = [];
			$('select.filter-control').each(function(index){
				var controlValue = parseInt($(this).val());
				if(controlValue){
					var item = {'field': $(this).attr('name'), 'value': controlValue};
					dataFilter.push(item);
				}
			});
			return dataFilter;
		}


		$('body').on('change', '#Category', function(event){
			var value = $(this).val();
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=GetItemBySubCatID&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: "json",
				data: {inpCatId: value},
				success: function(data){
					var templateSelect = $('#TemplateID');
					templateSelect.html('<option value="0">All</option>');
					if(data.DATALIST.length){
						for(var i in data.DATALIST){
							var option = data.DATALIST[i];
							$('<option></option>').attr('value', option.ID)
												  .text(option.NAME)
												  .appendTo(templateSelect);
						}
					}
					
				},
				complete: function(){

				}
			}).done(function(){
				GetCategoryList(buildFilterData());
			});

		});	

		$('body').on('change', '#TemplateID', function(event){
			var value = $(this).val();

			GetCategoryList(buildFilterData());

		});	




		$('body').on('click', 'a.remove-template-item-btn', function(event){

			var item = $(this).data('item');
			bootbox.confirm({
			    title: "Delete Template",
			    message: "Are you sure you want to delete this Template?",
			    buttons: {
			    	confirm: {
			            label: 'Confirm',
			            className: "btn btn-medium btn-success-custom"
			        },
			        cancel: {
			            label: '<i class="fa fa-times"></i> Cancel',
			            className: "btn btn-medium btn-back-custom"
			        }
			    },
			    callback: function (result) {
			    	if(result){
			    		$.ajax({
							url: "/session/sire/models/cfc/category.cfc?method=RemoveItem&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
							type: "POST",
							dataType: "json",
							data: {inpItemID: item.CATTEMPLATEID},
							success: function(data){
								if(data.RXRESULTCODE != 1){
									bootbox.dialog({
									    message:'Delete Template Failed!',
									    title: 'Delete Template',
									    buttons: {
									        success: {
									            label: "Ok",
									            className: "btn btn-medium btn-success-custom"
									        }
									    }
									});
								} else {
									bootbox.dialog({
									    message:'Delete Template Successfully!',
									    title: 'Delete Template',
									    buttons: {
									        success: {
									            label: "Ok",
									            className: "btn btn-medium btn-success-custom",
									            callback: function(){
									            	GetCategoryList(buildFilterData());
									            }
									        }
									    }
									});
								}
							}
						});
			    	}
			    }
			});
			
		});
		
		
		$('body').on('click', 'td.template a', function(event){
			event.preventDefault();
			$('#rename-template').modal('show');
			$('#new-template-name').val($(this).text());
			$('#template-id').val($(this).data('item'));
			$('#rename-template-form').on('submit', function(event){
				event.preventDefault();
				if($('#rename-template-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})) {
					$.ajax({
		    			url: '/session/sire/models/cfc/category.cfc?method=RenameTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		    			type: 'POST',
		    			dataType: 'json',
		    			data: {
		    				inpTemplateId: $('#template-id').val(),
		    				inpTemplateName: $('#new-template-name').val()
		    			},
		    			beforeSend: function( xhr ) {
				     		$('#processingPayment').show();
				    	},
				    	error: function(XMLHttpRequest, textStatus, errorThrown) {
				     		$('#processingPayment').hide();
				     		bootbox.dialog({
				         		message:'Rename template failed!',
				         		title: "Rename Template",
				         		buttons: {
				             		success: {
				                 		label: "Ok",
				                 		className: "btn btn-medium btn-success-custom",
				                 		callback: function() {}
				             		}
				         		}
				     		});
				    	},
				    	success: function(data){
				    		$('#processingPayment').hide();
				    		if (data.RXRESULTCODE == 1){
				    			$('#rename-template').modal('hide');
				    			bootbox.dialog({
					         		message:data.MESSAGE,
					         		title: "Rename Template",
					         		buttons: {
					             		success: {
					                 		label: "Ok",
					                 		className: "btn btn-medium btn-success-custom",
					                 		callback: function() {}
					             		}
					         		}
					     		});
					     		GetCategoryList(buildFilterData());
				    		}
				    		else {
				    			bootbox.dialog({
					         		message:data.MESSAGE,
					         		title: "Rename Template",
					         		buttons: {
					             		success: {
					                 		label: "Ok",
					                 		className: "btn btn-medium btn-success-custom",
					                 		callback: function() {}
					             		}
					         		}
					     		});
				    		}
				    	}
		    		});
				}
			});

		});

		$("#category-template-update").on('shown.bs.modal', function () {
			var itemObject = $("#category-template-update").data('itemObject');
			$('#hCategory option').each(function(){
				var value = $(this).val();
				if(itemObject.CATID == value){
					$(this).prop('selected', true);	
				} 
			});
			$('#hTemplateID option').each(function(){
				var value = $(this).val();
				if(itemObject.TEMPLATEID == value){
					$(this).prop('selected', true);	
				} 
			});

			$('.select2').select2({theme: "bootstrap"});
    	});

		$('#category-template-info').validationEngine('attach', {});

		$('#category-template-info').on('submit', function(event){
    		event.preventDefault();

    		var itemObject = $("#category-template-update").data('itemObject');
    		var inpTxID = $('#hTemplateID').val(), inpTcID = $('#hCategory').val();
    		$.ajax({
    			url: "/session/sire/models/cfc/category.cfc?method=UpdateItem&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    			type: "POST",
    			dataType: "json",
    			data: {
    				inpItemID: itemObject.CATTEMPLATEID,
    				inpTxID: inpTxID,
    				inpTcID: inpTcID
				},
    			success: function(data){
    				if(data.RXRESULTCODE == 1){
    					bootbox.dialog({
						    message:'Update Success!',
						    title: 'Update Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {
						            	$('#category-template-update').modal('hide');
						            	$("#Category").trigger('change');
						            }
						        }
						    }
						});
    				} else {
    					bootbox.dialog({
						    message:data.MESSAGE,
						    title: 'Update Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
    				}
    			}
    		});
    	});



    	$('#btnAddNew').on('click', function(event){
			$('#category-template-add').modal('show');
    	});



    	$('#category-template-add').on('shown.bs.modal', function(){
    		$('.select2').select2({theme: "bootstrap"});
    	});

		$('#category-template-addnew').validationEngine('attach', {showOneMessage: true});

		$('#category-template-addnew').on('submit', function(event){
    		event.preventDefault();

    		var CatID  = $('#aCategory').val(),
    			TempID = $('#aTemplateID').val();

    		$.ajax({
    			url: "/session/sire/models/cfc/category.cfc?method=AddItem&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    			type: "POST",
    			dataType: "json",
    			data: {inpCatID: CatID, inpTemID: TempID},
    			success: function(data){
    				if(data.RXRESULTCODE == 1){
    					bootbox.dialog({
						    message:data.MESSAGE,
						    title: 'Add template item',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {
						            	$('select.add-form-control').each(function(){
						            		$(this).find('option[value="0"]').prop('selected', true);
						            	});
										$('#category-template-add').modal('hide');
						            	GetCategoryList(buildFilterData());
						            	
						            }
						        }
						    }
						});
    				} else {
    					bootbox.dialog({
						    message:data.MESSAGE,
						    title: 'Add template item',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
    				}
    			},
    			complete: function(){

    			}

    		});

    	});




    	$('#aCategoryGroup').on('change', function(e){
			var value = $(this).val();
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=GetCategory&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				type: "POST",
				dataType: "json",
				data: {inpGroupID: value},
				success: function(data){
					var categorySelect = $('#aCategory');
					categorySelect.empty();
					categorySelect.append('<option value="0">--Select a sub-category--</option>');
					if(data.DATALIST.length){
						for(var i in data.DATALIST){
							var option = data.DATALIST[i];
							$('<option></option>').attr('value', option.ID)
												  .text(option.NAME)
												  .appendTo(categorySelect);
						}
					}
					
				}
			});
		});



    $('body').on('click', 'a.btn-change-order', function(event){

    	var type = $(this).data('type');

    	var itemTemplate = $(this).parents('div.box-move-btns').data('item');

    	var actions = [
    		{name: "move-up", method: 'moveUp' },
    		{name: "move-down", method: 'moveDown' },
    		{name: "move-first", method: 'moveToFirst' },
    		{name: "move-last", method: 'moveToLast' }
    	];

    	actions.forEach(function(item, index){
    		if(type == item.name){
    			var ajaxUrl = "/session/sire/models/cfc/category.cfc?method="+item.method+"&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true"
    			$.ajax({
    				url: ajaxUrl,
    				data: {inpID: itemTemplate.CATTEMPLATEID},
    				dataType: "json",
    				success: function(data){
    					// console.log(data);
    				},
    				complete: function(){
    					RefreshList();
    				}
    			});
    		}
    	});
    	event.stopPropagation();
    });

})(jQuery);


		
