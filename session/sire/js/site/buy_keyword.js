(function($) {
    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing' && checkTimeout == 0)
	  		return 'Payment processing. Please don\'t refresh and close.';
	});
    
    $("#keyword_calculate").click(function(e) {
    	var intRegex = /^\d+$/;
    	var amount = 0;
    	var numberKeyword = $('#numberKeyword').val();
    	var pricekeywordafter = $('#pricekeywordafter').val()
    	if(intRegex.test(numberKeyword)) {
    		amount = numberKeyword * pricekeywordafter;
    	}
    	$('#Amount').text("$" + amount);
    });
    
    $("#numberKeyword").change(function(e){
    	var intRegex = /^\d+$/;
    	var amount = 0;
    	var numberKeyword = $('#numberKeyword').val();
    	var pricekeywordafter = $('#pricekeywordafter').val()
    	if(intRegex.test(numberKeyword)) {
    		amount = numberKeyword * pricekeywordafter;
    	}
    	$('#amount').val(amount);
    });

})(jQuery);


function paymentConfirm() {
	var intRegex = /^\d+$/;
	var amount = 0;
	var numberKeyword = $('#numberKeyword').val();
	var pricekeywordafter = $('#pricekeywordafter').val()
	
	$('#rsNumberofKeywords').text($('#numberKeyword').val());
	
	if(intRegex.test(numberKeyword)) {
		amount = numberKeyword * pricekeywordafter;
	}
	$('#rsAmount').text("$" + amount);
}

