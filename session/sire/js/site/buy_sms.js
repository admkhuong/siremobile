(function($) {
    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing' && checkTimeout == 0)
	  		return 'Payment processing. Please don\'t refresh and close.';
	});
    $("#keyword_calculate").click(function(e) {
    	var intRegex = /^\d+$/;
    	var amount = 0;
    	var numberSMS = $('#numberSMS').val();
    	var pricemsgafter = $('#pricemsgafter').val()
    	if(intRegex.test(numberSMS)) {
    		amount = numberSMS * pricemsgafter/100;
    	}
    	$('#Amount').text("$" + amount.toFixed(2));
    });
    
    $("#numberSMS").change(function(e){
    	var intRegex = /^\d+$/;
    	var amount = 0;
    	var numberSMS = $('#numberSMS').val();
    	var pricemsgafter = $('#pricemsgafter').val()
    	if(intRegex.test(numberSMS)) {
    		amount = numberSMS * pricemsgafter/100;
    	}
    	
    	$('#amount').val(amount.toFixed(2));
    });
    
})(jQuery);

function paymentConfirm() {
	$('#rsNumberofCredit').text($('#numberSMS').val());
			
	var intRegex = /^\d+$/;
	var amount = 0;
	var numberSMS = $('#numberSMS').val();
	var pricemsgafter = $('#pricemsgafter').val()
	if(intRegex.test(numberSMS)) {
		amount = numberSMS * pricemsgafter/100;
	}
	$('#rsAmount').text("$" + amount.toFixed(2));
}