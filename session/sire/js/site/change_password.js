(function($){
	
	$("#change_password_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$("#change-password").click(validate_form);

	function validate_form(){
		if ($('#change_password_form').validationEngine('validate', {focusFirstField : true})) {
			
		
			var password = $("#inpPasswordSignup").val();
			var confirmPassword = $("#inpConfirmPassword").val();
			var currentPassword = $("#currentPassword").val();

			try{
				$.ajax({
					type: "POST",
					url: '/session/cfc/administrator/userstool.cfc?method=changePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						password : password,
						confirmPassword:confirmPassword,
						currentPassword:currentPassword
					},
					beforeSend: function(){
						$('#processingPayment').show();
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('#processingPayment').hide();
					},							  
					success:function(data){
						$('#processingPayment').hide();
						bootbox.dialog({
					        message: data.MESSAGE,
					        title: "Change Passwod",
					        className: "",
					        onEscape: function() {
					            if (data.SUCCESS) {
                                    window.location.href="/session/sire/pages/";
                                } else {
                                    location.reload();
                                }
					        },
					        buttons: {
					            success: {
					                label: "Ok",
					                className: "btn btn-medium btn-success-custom",
					                callback: function() {
                                        if (data.SUCCESS) {
                                            window.location.href="/session/sire/pages/";
                                        } else {
                                            location.reload();
                                        }
					                }
					            }
					        }
					    });
						
					} 		
						
				})
			}catch(ex){
				$('#processingPayment').hide();
				return false;
			}
		}
		return false;	
	}
	
})(jQuery);


function isValidPassword(field, rules, i, options){
			//$("#validateno1").css("color","#424242");
			//$("#validateno2").css("color","#424242");
			//$("#validateno3").css("color","#424242");
			//$("#validateno4").css("color","#424242");
			//$("#validateno5").css("color","#424242");
			//$("#err_inpConfirmPassword2").css("display","none");
			var password = field.val();
			if (password.length < 8)
			{
				//$("#validateno1").css("color","red");
				return options.allrules.validatePassword.alertTextLength;			 					  
			}
			  
			var hasLetter = /[a-z]/i.test(password);
			if (!hasLetter)
			{
				return options.allrules.validatePassword.alertTextNotFound;
			}
			
		/* Reduced this requirement to any case letter2017-08-18 Lee Peterson
			var hasUpperCase = /[A-Z]/.test(password);
			if (!hasUpperCase)
			{
				return options.allrules.validatePassword.alertTextUpperCase;
			}
		
			var hasLowerCase = /[a-z]/.test(password);
			if (!hasLowerCase)
			{
				return options.allrules.validatePassword.alertTextLowerCase;
			}
		*/
			
			var hasNumbers = /\d/.test(password);
			if (!hasNumbers)
			{
				return options.allrules.validatePassword.alertTextNumbers;
		
			}
		
		/* Removed this requirement	2017-08-18 Lee Peterson
			var hasNonalphas = /\W/.test(password);
			if (!hasNonalphas)
			{
				return options.allrules.validatePassword.alertTextNoAlpha;
			}
		*/
	}