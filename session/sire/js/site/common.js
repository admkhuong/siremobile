function doConvertString(str)
{
	 // Format newlines here--->
    str = str.replace(new RegExp("\n", 'gi'), String.fromCharCode(13)+String.fromCharCode(10));
    str = str.replace(new RegExp("<br />", 'gi'), String.fromCharCode(13)+String.fromCharCode(10));

	//https://unicodelookup.com/#173/1

	str = str.replace(new RegExp("\r", 'gi'), String.fromCharCode(13));
	//str = str.replace(new RegExp("\n", 'gi'), String.fromCharCode(10));

	str = str.replace(new RegExp(String.fromCharCode(8210), 'gi'), "-");
	str = str.replace(new RegExp(String.fromCharCode(8211), 'gi'), "-");
	str = str.replace(new RegExp(String.fromCharCode(8212), 'gi'), "-");
	str = str.replace(new RegExp(String.fromCharCode(8213), 'gi'), "-");

	str = str.replace(new RegExp(String.fromCharCode(8216), 'gi'), "'");
	str = str.replace(new RegExp(String.fromCharCode(8217), 'gi'), "'");

	str = str.replace(new RegExp(String.fromCharCode(8220), 'gi'), '"');
	str = str.replace(new RegExp(String.fromCharCode(8221), 'gi'), '"');

	str = str.replace(new RegExp(String.fromCharCode(8230), 'gi'), "-");

	// No backslash in SMS - replace with forward slash
	//str = str.replace(new RegExp("/\\", 'gi'), "aaaa");
	str = str.replace(/\\/gi, '/');

	//A E I O U a e i o u
	str = str.replace(new RegExp("&#192;", 'gi'), String.fromCharCode(192));
	str = str.replace(new RegExp("&#200;", 'gi'), String.fromCharCode(200));
	str = str.replace(new RegExp("&#204;", 'gi'), String.fromCharCode(204));
	str = str.replace(new RegExp("&#210;", 'gi'), String.fromCharCode(210));
	str = str.replace(new RegExp("&#217;", 'gi'), String.fromCharCode(217));

	str = str.replace(new RegExp("&#224;", 'gi'), String.fromCharCode(224));
	str = str.replace(new RegExp("&#232;", 'gi'), String.fromCharCode(232));
	str = str.replace(new RegExp("&#236;", 'gi'), String.fromCharCode(236));
	str = str.replace(new RegExp("&#242;", 'gi'), String.fromCharCode(242));
	str = str.replace(new RegExp("&#249;", 'gi'), String.fromCharCode(249));

	// --- Upside down ! ?
	str = str.replace(new RegExp("&#161;", 'gi'), String.fromCharCode(161));
	str = str.replace(new RegExp("&#191;", 'gi'), String.fromCharCode(191));


	// Ã± - > ñ
	str = str.replace(new RegExp(String.fromCharCode(195)+String.fromCharCode(177), 'gi'), String.fromCharCode(241));
	str = str.replace(new RegExp(String.fromCharCode(195)+String.fromCharCode(173), 'gi'), String.fromCharCode(241));

	str = str.replace(new RegExp("á", "gi"), "à");
	str = str.replace(new RegExp("í", "gi"), "ì");
	str = str.replace(new RegExp("ó", "gi"), "ò");
	str = str.replace(new RegExp("ú", "gi"), "ù");
	str = str.replace(new RegExp("&nbsp;", "gi"), " ");

	return str;
}

function hasUnicodeChar(str) {

	var haveUnicode = false;

	var mapGSMExtended=['^','{','}','\\','[','~',']','|','€'];
	str =  doConvertString(str);
	//var UnicodeRegex = new RegExp('[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]');
	var UnicodeRegex = new RegExp("[^A-Za-z0-9 \u00A0\\r\\n\\t@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"#$%&'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC\u2013\u2014õç`’´‘’′″“–‑−—一«»”！：•®οκ]")

	if(UnicodeRegex.test(str.replace(/\u00a0/g, " "))){
		haveUnicode = true;
	}
	return haveUnicode;
}


function characterCountReturnCount(text) {
	cpTextWithoutCDF = text.replace(/\{\%(.\w*)\%\}/g, "");

	text = doConvertString(text);
	var count = text.length;

	// text= text.replace(/\&/g,"&amp;");
	// text= text.replace(/\</g,"&lt;");
	// text= text.replace(/\>/g,"&gt;");
	// text= text.replace(/\'/g,"&apos;");
	// text= text.replace(/\"/g,"&quot;");

	var mapGSMExtended=["\n",'^','{','}','\\','[','~',']','|','€'];
	var haveUnicode=false;


	haveUnicode = hasUnicodeChar(cpTextWithoutCDF.replace(/\u00a0/g, " "));

	var newlinesRegex= new RegExp('(\r\n|\r|\n)','g');
	var nmessage = 1;
	if(haveUnicode)
	{
		if (count > 70 ) {
			if ((count % 66 ) == 0) {
			  nmessage = (count / 66 )
			} else {
			  nmessage = Math.floor(count / 66 ) + 1;
			}
		}
	}
	else
	{
		for(var i=0;i<count;i++){
			var ch=text.charAt(i);
			if(mapGSMExtended.indexOf(ch)!==-1)
			{
				count = count+1;
			}
		}

		if (count > 160) {
			if ((count % 153) == 0) {
			  nmessage = (count / 153)
			} else {
			  nmessage = Math.floor(count / 153) + 1;
			}
		}
	}

	return nmessage;
}

function characterCount(text) {

	cpTextWithoutCDF = text.replace(/\{\%(.\w*)\%\}/g, "");

	text = doConvertString(text);

	// console.log("characters count:"+text+' - '+text.length);
	var count = text.length;

	// text= text.replace(/\&/g,"&amp;");
	// text= text.replace(/\</g,"&lt;");
	// text= text.replace(/\>/g,"&gt;");

	// text= text.replace(/\'/g,"&apos;");
	// text= text.replace(/\"/g,"&quot;");



	var haveUnicode=false;
	var UnicodeRegex = new RegExp('[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]');
	var mapGSMExtended=['^','{','}','\\','[','~',']','|','€'];

	haveUnicode = hasUnicodeChar(cpTextWithoutCDF.replace(/\u00a0/g, " "));

	// var newlinesRegex= new RegExp('(\r\n|\r|\n)','g');

	// if(text.match(newlinesRegex) !== null)
	// {
	// 	var totalNewlines= text.match(newlinesRegex).length;
	// 	count= count + totalNewlines;
	// }

	var nmessage = 0;

	if(haveUnicode)
	{

		if (count > 70 ) {
			if ((count % 66 ) == 0) {
			  nmessage = (count / 66 )
			} else {
			  nmessage = Math.floor(count / 66 ) + 1;
			}
		}
	}
	else
	{

		for(var i=0;i<count;i++){
			var ch=text.charAt(i);
			if(mapGSMExtended.indexOf(ch)!==-1)
			{
				count = count+1;
			}
		}

		if (count > 160) {
			if ((count % 153) == 0) {
			  nmessage = (count / 153)
			} else {
			  nmessage = Math.floor(count / 153) + 1;
			}
		}
	}

	var message = 'Characters: ' + count;

	message=message
	+'   <a tabindex="0" class="info-popover popover-charcount" data-trigger="focus" role="button" data-placement="right" data-html="true" title="" data-content="'

	+'As a reminder, 1 SMS message (or credit) = 160 characters.  So if your message is 180 characters in length, it will require 2 credits.  I know what you’re thinking… no we’re not trying to rip you off.  Here’s a more in depth explanation of why SMS regulations limit one message to 160 characters. '

	+'</br></br><a class=tooltip-link href=https://www.siremobile.com/blog/support/messages-cost-one-credit/  target=_blank ><span>Click here</span></a> to learn more.'

	+'</br></br><img width=30px src=/session/sire/assets/layouts/layout4/img/Icon_Tip.png >The reason customers like SMS so much is because it is brief and eliminates marketing fluff.  Keep your messages short to manage cost and keep customers happy.'

	+'" data-original-title="">'
	+'<img  src="../assets/layouts/layout4/img/info.png"/></a>';


	if (nmessage > 1 ) {
		message = message + ' <span class="increments-indicator" style="color:#FF0000"> Credit: ' + nmessage + '</span> ';

	}

	return message;
}

function countText(id, text) {
	var message = characterCount(text);
	$(id).html(message);
}

var checkTimeout = 0;

$(document).ready(function() {
	$(window).on("load resize", function(event){
		if(event.type == "load"){
			$(".navbar-header .navbar-nav").removeClass('hidden');
		}
		var sttWidth = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		if(sttWidth < 768){
			$("#order-plan-spec .navbar-nav").addClass("dropdown-menu");
		}
		else{
			$("#order-plan-spec .navbar-nav").removeClass("dropdown-menu");
		}
	});


	var fields = $('input,select,textarea,checkbox');
		fields.focus(function(){
			$(this).validationEngine('hide');
			var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
			setTimeout(function(){
			nextField.validationEngine('hide');
			},1);
		});

	/*
	$.ajaxSetup({
        complete: function(xhr, textStatus) {
            // will be raised whenever an AJAX request completes (success or failure)
            // If it is homepage mean session time out -> redirect current page to homepage
            if(xhr.responseText.indexOf('class="home_page"') > -1){
            	checkTimeout = 1;
	            alert('Your session has timeout. Please login to continue');
	            window.location.href = '/';
	            return false;
          	}
          	return false;

        },
        success: function(result) {
            // will be raised whenever an AJAX request succeeds
            alert('xxx');

        },
    });
	*/
	// $( document ).ajaxComplete(function( event, xhr, settings ) {
     //
     //      console.log(xhr.responseText)
     //
	//  	if(xhr.responseText.indexOf('class="home_page"') > -1 && xhr.responseText.indexOf('class="device-register"') > -1) {
     //
	//  			checkTimeout = 1;
	//  			if($('#processingPayment').length > 0){
	//  				$('#processingPayment').hide();
	//  			}
	//  			bootbox.dialog({
	// 			    message: "Your session has timeout. Please login to continue.",
	// 			    title: "SIRE",
	// 			    buttons: {
	// 			        success: {
	// 			            label: "Ok",
	// 			            className: "btn btn-medium btn-success-custom",
	// 			            callback: function() {
	// 			            	 window.location.href = '/';
	// 			            }
	// 			        }
	// 			    }
	// 			});
	//             return false;
     //    }
	// });
});

$(document).ready(function() {
	$('#signout').on('show.bs.modal', function() {
		if ($("body").hasClass('open-canvas')){
	    	$(".closeNav").trigger("click");
		}
	});
});

// slide show
function GetSlideShowStatus()
{
	return new Promise( function (resolve, reject) {
        var data ={
            MESSAGE:"",
            RXRESULTCODE:1,
            STATUS:1
        };
        var status=1;
        var variableStatus= userId+'SlideStatus';
        if (localStorage) {
            // LocalStorage is supported!
            status = localStorage.getItem(variableStatus);
        } else {
            // No support. Use a fallback such as browser cookies or store on the server.
            status= getCookie(variableStatus);
        }
        if(status !=undefined &&  status != null)
        {
            data.STATUS=status;
        }
        resolve(data);
	});
}
function UpdateSlideShowStatus(status)
{
	return new Promise( function (resolve, reject) {
        var data ={
            MESSAGE:"",
            RXRESULTCODE:1,
            STATUS:1
        };
		var variableStatus= userId+'SlideStatus';
        if (localStorage) {
            // LocalStorage is supported!
            localStorage.setItem(variableStatus, status);
        } else {
            // No support. Use a fallback such as browser cookies or store on the server.
            document.cookie = variableStatus+"="+status;
        }
        resolve(data);
	});
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
