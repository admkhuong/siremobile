
$(document).ready(function(){
	(function($){
		var colorBar = ["#085f7a", "#7ccbe4", "#71c27a"];
		

		$('div.hyper-link').on('click', function(event){
			var href = $(this).data('href');

			window.location.href = href;
		});

		$('div.hyper-link a').on('click', function(event){
			event.stopPropagation();
		});


		$.ajax({
			url: "/session/sire/models/cfc/reports/dashboard.cfc?method=GetCampaignStatistic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: "GET",
			dataType: "json",
			success: function(rspData){
				var data = rspData.DATA;
				for(prop in data){
					var value = data[prop];
					$('p.campaign-info[data-prop="'+prop+'"]').text(value);
				}

			},
			complete: function() {

			}
		});



		$.ajax({
			url: "/session/sire/models/cfc/waitlistapp.cfc?method=GetAnalyticData&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			type: "GET",
			dataType: "json",
			data: {inpDateStart: "", inpDateEnd: "", inpListId: 0},
			success: function(rspData){
				var summary = rspData.DATA.SUMMARY;

				//prepare data for dates type=======================================
				for(var prop in summary){
					var summaryItemValue = summary[prop];
					$('p.summary-value[data-prop="'+prop+'"]').text(summaryItemValue);
				}
				//==================================================================

			}

		});


		
	
		$.ajax({
			type: "POST", 
			url: '/session/sire/models/cfc/control-point.cfc?method=ConsoleSummaryKeywords&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  
			{ 	
				inpLimit: 5				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $('#ProcessingKeywordCounts').hide();},
			success: function(d) 
			{			
				$('span.keywords-value[data-prop="KEYWORDSINUSE"]').text(d.KEYWORDSINUSE);
				$('span.keywords-value[data-prop="KEYWORDLIMIT"]').text(d.KEYWORDLIMIT);

				d.TOPACTIVE.forEach(function(item, index){
					$('<li></li>').append($('<a></a>').attr('title', item.DESC)
													  .attr('href', 'campaign-edit?campaignid='+item.BATCHID)
													  .text(item.KEYWORD))
								  .appendTo('#most-active-keyword');
				});
			} 		
				
		});	







		$.ajax({
			type: "POST", 
			url: '/session/sire/models/cfc/reports/opt.cfc?method=ConsoleSummaryOptCounts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  
			{ 					
			},					  				  
			success: function(d) 
			{			
				if (d.RXRESULTCODE == 1) 
				{
					for(prop in d){
						var item = d[prop];
						if($('p.subsciber-count[data-prop="'+prop+'"]').length){
							$('p.subsciber-count[data-prop="'+prop+'"]').text(item);
						}
					}	
				}	
								
			} 		
				
		});	


		$.ajax({
			type: "POST", 
			url: '/session/sire/models/cfc/reports/dashboard.cfc?method=GetRecentActiveFeed&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data: {},
			success: function(data){
				var actBox = $("ul.act-list-box");
				var dataList = data.DATA;				
				dataList.forEach(function(item, index){
					if(item.LOGTYPE == 1){
						$('<li></li>').append($('<p></p>').addClass("activity-name").html(item.PHONE + ' joined ' + item.CAMPAIGNNAME + ' Campaign'))
				 				  .append($('<p></p>').addClass("activity-time-modified").html(item.DATE))
				 				  .appendTo(actBox);
					}
					else if(item.LOGTYPE == 2){
						$('<li></li>').append($('<p></p>').addClass("activity-name").html(item.PHONE + ' left your campaign'))
				 				  .append($('<p></p>').addClass("activity-time-modified").html(item.DATE))
				 				  .appendTo(actBox);
					}
					else if(item.LOGTYPE == 3){
						$('<li></li>').append($('<p></p>').addClass("activity-name").html(item.PHONE + ' joined ' + item.SMS + "CPP"))
				 				  .append($('<p></p>').addClass("activity-time-modified").html(item.DATE))
				 				  .appendTo(actBox);
					}
					else{
						if(item.SMSTYPE == 1){
							$('<li></li>').append($('<p></p>').addClass("activity-name").html(item.PHONE + ' received: ' + '"' + item.SMS + '"'))
					 				  .append($('<p></p>').addClass("activity-time-modified").html(item.DATE))
					 				  .appendTo(actBox);
						}
						else if(item.SMSTYPE == 2){
							$('<li></li>').append($('<p></p>').addClass("activity-name").html(item.PHONE + ' sent: ' + '"' + item.SMS  + '"'))
					 				  .append($('<p></p>').addClass("activity-time-modified").html(item.DATE))
					 				  .appendTo(actBox);
						}
					}

				 	
				});

			}
		});




		$.ajax({
			url: '/session/sire/models/cfc/reports/mlp.cfc?method=DashboardMLPSummary&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'JSON',
			success: function(data){
				if(data.RXRESULTCODE == 1){
					if(!(data.MLPVIEWS == 0 && data.MLPACTIVE == 0 && data.MLPDEACTIVE == 0)){
						$('#mlp-views').text(data.MLPVIEWS);
						$('#mlp-active').text(data.MLPACTIVE);
						$('#mlp-deactive').text(data.MLPDEACTIVE);
					}
				}
			}
		});
		
		

		$("div.top-box").click(function(event){
			$(this).find('div.round-corner div.box-detail').slideToggle('fast');
		});

		$("div.top-box div.round-corner").find('div.box-detail').click(function(event){
			event.stopPropagation();
		});


	})(jQuery);
});

$(document).ready(function($) {

	if($("#LiveDemoVideo").length){
		$( "#LiveDemoVideo" ).trigger( "click" );
	}
	/*JS for Live Demo Video modal*/
	/* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var videoFrame = $("#ifrmVideo");
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#LiveDemoVideoModal").on('hide.bs.modal', function(){
        videoFrame.attr('src', '');
    });


});
