// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var RegName = new RegExp("^[a-zA-Z0-9_()-]+$");

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
	url: "/session/sire/models/cfc/image.cfc?method=SaveImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true", // Set the url
	paramName: "upload",
	thumbnailWidth: 200,
	thumbnailHeight: 200,
	parallelUploads: 20,
	maxFilesize: 10,
	filesizeBase: 1024,
	previewTemplate: previewTemplate,
	autoQueue: false, // Make sure the files aren't queued until manually added
	previewsContainer: "#previews", // Define the container to display the previews
	clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
	acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg"
});

myDropzone.on("addedfile", function(file) {
	// Hookup the start button
	file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };

	if (file.size > maxFilesize) {
		myDropzone.removeFile(file);
		alertMessage('File Size Error', "This file is too large! Max file size is 10MB");
	}

	var allFilesAdded = myDropzone.getFilesWithStatus(Dropzone.ADDED);
	var userUsage = $("#inpUserUsage").val();
	var userStoragePlan = $("#inpUserStoragePlan").val();

	var totalFileSize = 0;
	$.each(allFilesAdded, function(index, val) {
		 totalFileSize += val.size;
	});

	if (parseInt(parseInt(userUsage) + parseInt(totalFileSize)) > parseInt(userStoragePlan)) {
		file.previewElement.querySelector(".error").innerHTML = 'Storage limit exceeded! Cannot upload this file';
		file.previewElement.querySelector(".start").onclick = function () {};
	}

	var plainFileName = file.name.substr(0, file.name.lastIndexOf('.'));
	var fileType = file.name.substr(file.name.lastIndexOf('.')+1, file.name.length);
	if (RegName.test(plainFileName) && plainFileName.length <= 255) {
         try {
             $.ajax({
                 url: '/session/sire/models/cfc/image.cfc?method=CheckImageName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                 type: 'POST',
                 dataType: 'json',
                 data: {inpPlainFileName: plainFileName, inpFileType: fileType.toLowerCase()}
             })
             .done(function(data) {
                 if (parseInt(data.RXRESULTCODE) == 1) {
                     if (parseInt(data.CHECKRESULT) == 1) {
                         bootbox.dialog({
                             message: "A file with the same name ("+plainFileName+"."+fileType+") already exists! Would you like to replace the existing file?",
                             title: 'Attention:',
                             buttons: {
                                 yes: {
                                     label: "Yes",
                                     className: "btn btn-medium btn-success-custom",
                                     callback: function() {
                                     }
                                 },
                                 no: {
                                     label: "No",
                                     className: "btn btn-medium btn-primary btn-back-custom",
                                     callback: function() {
                                         myDropzone.removeFile(file);
                                     }
                                 },
                             }
                         });
                         $('#processingPayment').hide();
                     } else if (parseInt(data.CHECKRESULT) == 2) {
                         alertMessage('Filename Error', data.MESSAGE);
                     }
                 } else {
                     alertMessage('Oops!', (data.ERRMESSAGE == '' ? 'An error occurred when checking your file. Please try again later!' : data.ERRMESSAGE));
                 }
             })

             file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
         } catch (e) {
             console.log(e);
             alertMessage('Oops!', "Can't connect to the server at this time, please refresh or try again later!");
         }
     }
	else {
		myDropzone.removeFile(file);
		if (!RegName.test(plainFileName)) {
			alertMessage('Oops!', 'Filename syntax error!<br>Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only');
		} else {
			alertMessage('Oops!', 'Filename is too long. Please use filename less than 256 characters');
		}
	}
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
	document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file) {
	// Show the total progress bar when upload starts
	document.querySelector("#total-progress").style.opacity = "1";
	// And disable the start button
	file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
	document.querySelector("#total-progress").style.opacity = "0";
});

// Hide file when upload done
myDropzone.on("success", function (file, result) {
	myDropzone.removeFile(file);
	if (parseInt(result.RXRESULTCODE) == 1) {
		var oldImg = $('img[src$="'+result.IMAGETHUMBURL+'"]');

		if (oldImg.length > 0) {
			var d = new Date();
			oldImg.attr('src', oldImg.attr('src')+'?'+d.getTime());
		} else {
			var html = '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 image-list-item">'+
							'<img class="image" src="'+result.IMAGETHUMBURL+'" alt="'+result.IMAGENAME+'">'+
							'<button class="btn btn-small btn-danger image-delete" data-imageid="'+result.IMAGEID+'">Delete</button>'+
						'</div>';
			var imageList = $('#image-list');
			if (imageList.children().length == 0) {
				imageList.text('');
			}
			imageList.prepend(html);
		}

		updateUserUsageInfo();

	} else {
		alertMessage('Oops!', (result.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : result.ERRMESSAGE));
	}
});

var updateUserUsageInfo = function () {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/image.cfc?method=GetUserStorageInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
		})
		.done(function(data) {
			if (parseInt(data.RXRESULTCODE) == 1) {
				var userUsage = parseInt(data.USERUSAGE);
				var userStoragePlan = parseInt(data.USERSTORAGEPLAN);

				var usagePercent = parseFloat(Math.round((userUsage/userStoragePlan) * 100 * 100) / 100).toFixed(2);

				$("#usageProgress").css('width', usagePercent+'%');
				$("#inpUserUsage").val(userUsage);

				var usageMBSize = userUsage / 1048576;
				if (usageMBSize > 1024) {
					usageMBSize = (usageMBSize / 1024);
					usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'GB';
				} else {
					usageMBSize = parseFloat(Math.round(usageMBSize * 100) / 100).toFixed(2) + 'MB';
				}
				$("#userUsageSpan").text(usageMBSize);

				var userStoragePlanMBSize = userStoragePlan / 1048576;
				if (userStoragePlanMBSize > 1024) {
					userStoragePlanMBSize = (userStoragePlanMBSize / 1024);
					userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'GB';
				} else {
					userStoragePlanMBSize = parseFloat(Math.round(userStoragePlanMBSize * 100) / 100).toFixed(2) + 'MB';
				}
				$("#userStoragePlanSpan").text(userStoragePlanMBSize);
			} else {
				alertMessage('Oops!', data.ERRMESSAGE == '' ? 'An error occurred when checking your plan storage capacity. Please contact support or try again later!' : data.ERRMESSAGE);
			}
		})
	} catch (e) {
		console.log(e);
		alertMessage('Oops!', 'An internal server error occurred. Please try again later');
	}
}

$('body').on('click', 'div.image-list-item img', function(event) {
	event.preventDefault();
	$('div.image-list-item img.selected').removeClass('selected');
	$(this).addClass('selected');

	$('.image-delete').hide();
	$(this).siblings('.image-delete').show();
});

$('body').on('click', '.image-delete', function(event) {
	var imageId = $(this).data('imageid');
	var image = $(this).parent();
	var imageList = image.parent();
	bootbox.dialog({
		message: "Do you really want to delete this image?",
		title: "Delete image",
		buttons: {
			success: {
				label: "Confirm",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					$('#processingPayment').show();
					$.ajax({
						url: '/session/sire/models/cfc/image.cfc?method=DeleteImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						dataType: 'json',
						data: {inpImageId: imageId}
					})
					.done(function(data) {
						if (parseInt(data.RXRESULTCODE) == 1) {
							image.remove();
							if (imageList.children().length == 0) {
								imageList.append("You haven't uploaded any images yet.");
							}
							alertMessage('Image Deleted','Your image has been successfully deleted!');
						}

						updateUserUsageInfo();
						$('#processingPayment').hide();
					})
				}
			},
			cancel: {
				label: "Cancel",
				className: "btn btn-medium btn-primary btn-back-custom",
				callback: function() {

				}
			}
		}
	});
});

function alertMessage(title, message) {
	$('#processingPayment').hide();
	title = typeof title !== 'undefined' ? title : "Images";
	message = typeof message !== 'undefined' ? message : "Unexpected Error! Please try again later!";
	bootbox.dialog({
		message: message,
		title: title,
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
				}
			}
		}
	});
	return false;
}

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
// document.querySelector("#actions .start").onclick = function() {
// 	myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
// };
// document.querySelector("#actions .cancel").onclick = function() {
// 	myDropzone.removeAllFiles(true);
// };
