	$(function() {
        //var a = $("#resizable").html();
        //alert(a);
        var max_content_width = $('.content-body').width();

        $("#resizable").resizable({
        	maxWidth : max_content_width - 30,
        	minWidth : 200 
        });

        //Iframe CSS style
		$('iframe').load( function() {
		    $('iframe').contents().find("head")
		      .append($("<style type='text/css'> .page{padding: 10px 10px;}  @media (max-width: 480px){.page{padding: 0px !important;}}</style>"));
		});
    });