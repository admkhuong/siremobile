$(document).ready(function(){

InitControl();	
var _tblListEMS;
	function InitControl(customFilterObj)
	{//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					} else {
						history.replaceState(history.state, document.title, location.pathname + location.search);
					}
				}
			},
			"aaSorting": [[ 1, 'desc' ]],
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
			"bAutoWidth": false,
			"responsive": true,
		    "aoColumns": [
			    {"sWidth": "auto", "sName": 'MLP Data', "sTitle": 'MLP Data',"bSortable": true,"sClass":"surl-name", "orderSequence": [ "asc" ]},
				{"sWidth": "8em", "sName": 'Actions', "sTitle": 'Actions',"bSortable": false,"sClass":"surl-action"}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/mlp.cfc?method=getCPPList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   "fnDrawCallback": function( oSettings ) {
		      // MODIFY TABLE
				//$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					}
					else{
						$('#tblListEMS_paginate').show();
					} 
				})
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		        if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: function(data){
			             	fnCallback(data);

			             	//set button disable status by reponse data from server
			             	var addButton = $('a.add-mlp-button, a.add-template-button');
			             	if(data.LOCKBUTTON === true){
			             		addButton.attr('disabled', 'disabled');
			             		$('p.warning-text-mlp').removeClass("hidden");
			             	} else {
			             		addButton.removeAttr('disabled');
			             		$('p.warning-text-mlp').addClass("hidden");
			             	}
			             }

			             
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				// MODIFY TABLE
				
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');

					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					} 
				})
			}
	    });
	}


	//BTN-ACTION
	$( document ).on( "click", ".btn-change-cpp-status, .btn-delete-cpp", function(event) {
		event.preventDefault();

		if($(this).attr('disabled'))
		{
			return false;
		}
		
		var cppid = parseInt($(this).data('cpp-id'));
		var status = parseInt($(this).data('cpp-status'));
		var action = $(this).data('cpp-action');
		var currentPage = $('input.paginate_text').val();




		if(action == 'delete'){
			var dialogText = 'Are you sure you want to delete this MLP?';
			var dialogTitle = "Delete Marketing Landing Page";
		}
		else{
			var dialogText = 'Are you sure you want to update this MLP?';
			var dialogTitle = "Update Marketing Landing Page";
		}
			

		if(cppid > 0 && status >=0 ){
			try{

				$('.btn').prop('disabled',true);
				bootbox.dialog({
		        message: dialogText,
		        title: dialogTitle,
		        className: "",
		        buttons: {
		            success: {
		                label: "OK",
		                className: "btn btn-medium btn-success-custom",
		                callback: function(result) {
		                    if(result){
								$.ajax(
								{
									type: "POST",
									url: '/session/sire/models/cfc/mlp.cfc?method=updateCPPStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
									dataType: 'json',
									data: { inpCppId: cppid,inpStatus:status,inpAction:action},
									beforeSend: function( xhr ) {
										$('.tblListEMS_processing').show();
										$('#processingPayment').show();
									},					  
									error: function(XMLHttpRequest, textStatus, errorThrown) {
										$('#processingPayment').hide();
										bootbox.dialog({
										    message: "There is an error.Please try again later.",
										    title: "Marketing Landing Portal",
										    buttons: {
										        success: {
										            label: "Ok",
										            className: "btn btn-medium btn-success-custom",
										            callback: function() {}
										        }
										    }
										});
										$('.btn').prop('disabled',false);
										$('.tblListEMS_processing').hide();
									},					  
									success:		
										function(d) {
											$('#processingPayment').hide();
											$('.btn').prop('disabled',false);
											$('.tblListEMS_processing').hide();
											if(d.RXRESULTCODE == 1){
												//goto success page
												bootbox.dialog({
												    message: d.MESSAGE,
												    title: "Marketing Landing Portal",
												    buttons: {
												        success: {
												            label: "Ok",
												            className: "btn btn-medium btn-success-custom",
												            callback: function() {}
												        }
												    }
												});
												var oTable = $('#tblListEMS').dataTable();
												//oTable.fnDraw();
												oTable.fnPageChange(parseInt(currentPage-1));
												return;
											}
											else{
												bootbox.dialog({
												    message: "There is an error.Please try again later.",
												    title: "Marketing Landing Portal",
												    buttons: {
												        success: {
												            label: "Ok",
												            className: "btn btn-medium btn-success-custom",
												            callback: function() {}
												        }
												    }
												});
											}
										} 		
									});
								}
			                }
			            },
			            cancel: {
			                label: "Cancel",
			                className: "btn btn-medium btn-back-custom",
			                callback: function() {
							$('.btn').prop('disabled',false);
			                }
			            },
		        	}
		    	});
				
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: "There is an error.Please try again later.",
					title: "Marketing Landing Portal",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
				$('.btn').prop('disabled',false);
				$('.tblListEMS_processing').hide();
			}
		}
		else
		{
			bootbox.dialog({
			    message: "There is an error.Please try again later.",
				title: "Marketing Landing Portal",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	$('.btn').prop('disabled',false);
			            }
			        }
			    }
			});
		}
	});
	
	$( document ).on( "click", ".input_embed_code", function(event) {
		 $(this).select();
	});

	$( document ).on( "click", ".btn-embed-cpp", function(event) {
		event.preventDefault();
		var cppUUID = $(this).data('cpp-uuid');
		var cppURL = $(this).data('cpp-url');
		//var embebcode = '<iframe src="https://'+window.location.hostname+'/cppx?inpUUID='+cppUUID+'" scrolling="auto" frameborder="0" id="CPPReviewFrame" width="100%" height="100%"></iframe>';
		var embebcode = '<iframe src="https://mlp-x.com/lz/'+cppURL+'" scrolling="auto" frameborder="0" id="CPPReviewFrame" width="100%" height="100%"></iframe>';
		bootbox.dialog({
				size: 'large',
			    message: "<textarea class='input_embed_code' style='width:100%'>"+embebcode+"</textarea>",
				title: "Marketing Landing Page - Embeb code",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            }
			        }
			    }
			});
	});

	$(function(){
		// Prevent vent if button has been disabled
		var allowedClick = true;
		$('a.add-mlp-button, a.add-template-button').click(function(event){

			setTimeout(function(){
				allowedClick = true;
			}, 1000);
			//prevent double click
			if(!allowedClick){
				return false;
			}

			if($(this).attr('disabled') == 'disabled'){
				return false;	
			}

			allowedClick = false;
		});
	});
	
	
});	