  (function($){ 
    $( document ).on( "click", ".btn-select-cpp", function(event) {
		if(typeof campaign_url != 'undefined' && campaign_url != '')
		window.location.href = campaign_url;
	});
    
    
    
    // TEMPLATE PICKER
	$( document ).on( "mouseenter", ".item-grid .content", function(event) {
		event.preventDefault();
		$('.item_action').hide();
		$(".item-grid .content").removeClass('selected');
		$(this).addClass('selected');
		campaign_url = $(this).attr("href");
		$('#btn-select-campaign').prop('disabled',false);

		$(this).find('.item_action').show();

	});
	
	
	
	// TEMPLATE PICKER
	$( document ).on( "click", ".item-grid", function(event) {
		event.preventDefault();
		$('.item_action').hide();
		$(".item-grid .content").removeClass('selected');
		$(this).addClass('selected');
		campaign_url = $(this).attr("href");
		$('#btn-select-campaign').prop('disabled',false);

		$(this).find('.item_action').show();

	});
	
	$( document ).on( "mouseleave", ".item-grid .content", function(event) {
	
		// Keep the selection sticky if the preview option is still active		
		if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
		{
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
		}
	});
	

	$( document ).on( "click", "#btn-select-campaign, .btn-select-campaign", function(event) {
		if(typeof campaign_url != 'undefined' && campaign_url != '')
		window.location.href = campaign_url;
	});
	
})(jQuery);