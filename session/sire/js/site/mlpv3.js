function htmlEncode(value){
  return $('<div/>').text(value).html();
}

function htmlDecode(value){
  return $('<div/>').html(value).text();
}

var RXxml_special_to_escaped_one_map = {
    '&' : '&amp;',
    '"' : '&quot;',
    '<' : '&lt;',
    '>' : '&gt;',
    "'" : '&apos;',
    "?" : '&#63;'   
};

var RXescaped_one_to_xml_special_map = {
    '&amp;' : '&',
    '&quot;' : '"',
    '&lt;' : '<',
    '&gt;' : '>',
    '&apos;' : "'",
    '&#63;' : "?" 
};
var RXspace = {
    ' ' : '%20'
}
var RXspacedecode = {
    '%20' : ' '
}

var isPreview = 0;
var isCPPPreview = 0;
var isAjax = 0;

function RXencodeXML(string) {
    
    //console.log(string);
    //console.log('string=(' + string + ')');
    
    var retStr = String(string);
    if (isNaN(retStr)) {
        retStr = retStr.replace(/([\&"<'>?])/g, function(str, item) {
            return RXxml_special_to_escaped_one_map[item];
        });
    }
    return retStr;
};

function RXdecodeXML(string) {
    
    var retStr = String(string);
    if (isNaN(retStr)) {
        retStr = retStr.replace(/(&quot;|&lt;|&gt;|&amp;|&apos;|&#63;)/g,  function(str, item) {
            return RXescaped_one_to_xml_special_map[item];
        });
    }
    return retStr;
}

function RXencodeSpace(string) {

    var retStr = String(string); 
    return retStr.replace(/([\ ])/g, function(str, item) {
        return RXspace[item];
    });
}
function RXdencodeSpace(string) {
    
    var retStr = String(string); 
    return retStr.replace(/(%20)/g, function(str, item) {
        return RXspacedecode[item];
    });
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


var ValidationStatusIsTrue = false; 

$(document).ready(function()
{
      
    // Submit setup CPP form
    $("#create_cpp_frm").submit(function(event){
	         	     
        event.preventDefault();
    });

       
    $('#CreateCPPButton').on('click', function(e){
		
		// reset global to false so will only suceed once!
		ValidationStatusIsTrue = false;
		$("#create_cpp_frm").validationEngine('attach', {
	        promptPosition : "topLeft", 
	        autoPositionUpdate: true, 
	        scroll: false,
	        focusFirstField : false,
	        onValidationComplete: function(form, status){
	            	                    	           
	            if (status && !ValidationStatusIsTrue) 
	            {		            
		            // Let the stupid thing know it has already competed true on one of its checks
		            ValidationStatusIsTrue = true;	    
		             
		            CreateCPP();         
	                
	            } 
	            else 
	            {
					//validate fail, do nothing here	                 	
	            }	           
	        }
	    });
	
	});	

	   
     //save state of select subcriberlist
    $( document ).on( "change", ".subcriber-list", function(event){
        var value = $(this).val();
        $(this).data('pre_val',value);
    });

    // Remove OBJECT POP MODAL
    $( document ).on( "click", ".cpp-object-btn-delete", function(event){
        var parent = $(this).parent().parent().parent();       
        var HTMLObject = $(this).parent().parent().find('textarea');

        bootbox.dialog({
            message: 'Are you sure you want to delete this object?',
            title: 'Marketing Landing Pages',
            className: "",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium btn-success-custom",
                    callback: function(result) {
                        if(HTMLObject.length > 0){
                            tinymce.remove("#"+HTMLObject.attr('id'));
                        }
                        parent.remove();
                        countSubcriberList();
                        $( ".sortable" ).sortable( "refreshPositions" );
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "btn btn-medium btn-back-custom",
                    callback: function() {
                    
                    }
                },
            }
        });
    });
		   
});


function CreateCPP() {
		
    // CALL AJAX TO SAVE MLP
    try{
        var cppName = $('#cppxName').val();
        var cppxURL = $('#cppxURL').val();
        
        if(isAjax == 1) return false; 

        $.ajax({
        type: "POST",
        url: '/session/sire/models/cfc/mlp.cfc?method=SetupCPP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
        dataType: 'json',
        //data: $('#create_cpp_frm').serialize(),
        data: {cppxName:cppName,cppxURL:cppxURL,checkTerms:0, cppxTermOfService:''},
        beforeSend: function( xhr ) {
            $('#processingPayment').show();
            isAjax = 1;
        },                    
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('#processingPayment').hide();
            isAjax = 0;
            bootbox.dialog({
                message:'Create MLP Fail.Please try again later.',
                title: "Setup MLP",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
        },                    
        success:        
            function(d) {
                isAjax = 0;
                $('#processingPayment').hide();
                if(d.RXRESULTCODE == 1){
                    window.location.href='/session/sire/pages/mlp-edit?ccpxDataId='+parseInt(d.CPPID)+'&action=Create'
                    /*
                    bootbox.dialog({
                        message:d.MESSAGE,
                        title: "Setup CPP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {window.location.href='/session/sire/pages/cpp-edit?ccpxDataId='+parseInt(d.CPPID)+'&action=Create'}
                            }
                        }
                    });*/
                }
                else{
                    bootbox.dialog({
                        message:'Create MLP Fail.Please try again later.',
                        title: "Setup MLP",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                }

                return false;
            }       
        });
    }catch(ex){
        $('#processingPayment').hide();
        bootbox.dialog({
            message:'Update Fail',
            title: "My Account",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        }); 
    }
    
}



function SaveMLP(inpCustomCSS)
{		
	try
	{
        if (typeof $('#action').val() !== 'undefined' && $('#action').val() == 'template' ) {
            url = '/session/sire/models/cfc/mlp.cfc?method=SaveMLPTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
        } else {
            url = '/session/sire/models/cfc/mlp.cfc?method=SaveMLP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
        }

        cppName = $('#cppxName').val();
        cppxURL = $('#cppxURL').val();
        ccpxDataId = $('#ccpxDataId').val();       
        
        // inpRawContent = JSON.stringify({'inpRawContent':editor.getContent()});

	  if(isAjax == 1) return false; 
      $.ajax({
        type: "POST",
        url: url,   
        dataType: 'json',
        data: {ccpxDataId:ccpxDataId, cppxName:cppName, cppxURL:cppxURL, inpRawContent: editor.getContent(), inpCustomCSS: inpCustomCSS},
        beforeSend: function( xhr ) {
            $('#processingPayment').show();
            isAjax = 1;
        },                    
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('#processingPayment').hide();
            bootbox.dialog({
                message:'Save MLP Fail. Please try again later.',
                title: "Setup MLP",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
            isAjax = 0;
        },                    
        success:        
            function(d) {
            	isAjax = 0;
                $('#processingPayment').hide();
                if(d.RXRESULTCODE == 1)
                {
	               
	                $('#MLPURLLable').html("<div>{URL key}: https://MLP-X.com/lz/" + $('#cppxURL').val() + "</div>") ;
	                
	                bootbox.dialog({
                        message:'MLP is saved successfully.',
                        title: "Save MLP",
                        buttons: {
                            success: {
                                label: "OK",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                   
                   
                }
                else
                {
                    bootbox.dialog({
                        message: d.MESSAGE, // 'Create MLP Fail3. Please try again later.',
                        title: "MLP Error",
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn btn-medium btn-success-custom",
                                callback: function() {}
                            }
                        }
                    }); 
                }
            }       
        });
    }
    catch(ex)
    {
	   
        $('#processingPayment').hide();
        bootbox.dialog({
            message:'Save MLP Failed',
            title: "MLP Error",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {}
                }
            }
        }); 
        $('.btn-group-answer-security-question').show();            
    }

	
	
}


