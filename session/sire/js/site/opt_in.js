(function($) {
    "use strict";
    var LIMIT_MESSAGE = 160;
    var optin_message_length;
    var confirm_message_length;
    setOptin();
    countText('#help-block1',$('#optin_message').val());
    countText('#help-block2',$('#confirm_message').val());

    //opt-in-form
    $("#opt-in-form")
	    .validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
            focusFirstField : false
	    })
	    .on('keyup change', '#optin_message_part1', function() {
	    	setOptin();
	        countText('#help-block1',$('#optin_message').val());
	        $('.you1').empty();
	        $('.you1').append("<p>" + $("#optin_message").val() + "</p>");
        })
        .on('focus', '#optin_message_part1', function() {
        	$('#focusField').val('optin_message_part1');
        	$("#AddField").attr("disabled", false);
        	$("#AddLink").attr("disabled", false);
        })
        .on('focus', '#confirm_message', function() {
        	$('#focusField').val('confirm_message');
        	$("#AddField").attr("disabled", false);
        	$("#AddLink").attr("disabled", false);

        })
        .on('focusout', '#optin_message_part1', function() {
        	//$("#AddField").attr("disabled", true);
        	//$("#AddLink").attr("disabled", true);
        })
        .on('keyup change', '#confirm_message', function() {
	    	setOptin();
    		countText('#help-block2',$('#confirm_message').val());
	        
	        if ($('#txtKeyword').val() != '') {
		        $("#SMSHistoryScreenArea").empty();
	            $("#SMSHistoryScreenArea").append('<div class="bubble me">' + $("#txtKeyword").val() + '</div><div class="bubble you you1" rel="274">&nbsp' + $("#optin_message").val() + '</div>');
	            $("#SMSHistoryScreenArea").append('<div class="bubble me">YES</div><div class="bubble you" rel="274">&nbsp' + $("#confirm_message").val() + '</div>');
	        }
        })
	    .on('keyup change', '#txtKeyword', function() {
	    	if ($('#txtKeyword').val() == '') {
	    		$("#SMSHistoryScreenArea").empty();
	    	}
	    	checkKeywordAvailability();
        })
	    .on('click', '#optin-save', function(event) {
	    	event.preventDefault();
	    	validate_form('optin-save');
        })
        .on('click', '#optin-save-request', function(event) {
        	event.preventDefault();
	    	validate_form('optin-save-request');
        })
        .on('click', '#edit', function() {
	    	bootbox.dialog({
            title: "Opt-In Message",
            message: '<div class="row">  ' + '<div class="col-md-12"> ' + '<form class="form-horizontal"> ' + '<div class="form-group"> ' + '<div class="col-md-12"> ' + '<textarea class="form-control" id="op" data-prompt-position="topLeft:150" rows="3">' + $('#optin_message_part3').text() + '</textarea>' + '</div> ' + '</div> ' + '</form> </div>  </div>',
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium btn-success-custom",
                    callback: function() {
                        $('#optin_message_part3').text($('#op').val());
                        setOptin();
                        countText('#help-block1',$('#optin_message').val());
                        if ($('#txtKeyword').val() != '') {
					        $("#SMSHistoryScreenArea").empty();
				            $("#SMSHistoryScreenArea").append('<div class="bubble me">' + $("#txtKeyword").val() + '</div><div class="bubble you you1" rel="274">&nbsp' + $("#optin_message").val() + '</div>');
				            $("#SMSHistoryScreenArea").append('<div class="bubble me">YES</div><div class="bubble you" rel="274">&nbsp' + $("#confirm_message").val() + '</div>');
				        }
                    }
                },
                cancel: {
	                    label: "Cancel",
	                    className: "btn btn-medium btn-back-custom",
	                    callback: function() {}
	            },
            }
        	});
        })
        .on('click', '#AddField', function(event) {
        	event.preventDefault();
	    	$('#addFiledModal').modal();
        })
        .on('click','#AddLink', function (event) {
        	event.preventDefault();
	    	$('#addLinkModal').modal();
        })
    
	
	$("#form-add-field")
	    .validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
            focusFirstField : false
	    })
	    .on('click', '#btn-add-field', function() {
	    	var id = $('#focusField').val();
	    	if (typeof  $("input:radio[name='optradio']:checked").val() != 'undefined') {
				wrapValueForm(id,$("input:radio[name='optradio']:checked").val());
				$('#addFiledModal').modal('hide');
			}
        });
	$("#form-add-link")
		.validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
            focusFirstField : false
	    })
	    .on('click', '.btn-success-custom', function() {
			if ($('#form-add-link').validationEngine('validate')) {
				var link = $("#tblLink").val();
				var id = $('#focusField').val();
				$.ajax({
				    type: "GET",
				    url: "/session/sire/models/cfc/optin.cfc?method=getShortLink&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				    dataType: 'json',
				    data: {
				        longUrl: link
				    },
                    beforeSend: function(){
                        $('#processingPayment').show();
                    },
				    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $('#processingPayment').hide();
                    },
				    success: function(data) {
                        $('#processingPayment').hide();
				        var filecontent = jQuery.parseJSON(data.filecontent);
				        if (filecontent.status_code == 200) {
				            wrapValueForm(id, filecontent.data.url);
				        } else if (filecontent.status_code == 500){
				        	wrapValueForm(id,link);
				        }
				        $('#tblLink').val("");
				        $('#addLinkModal').modal('hide');
				    }
				});
			}
		});
	
})(jQuery);

function wrapValueForm(id,text) {
    var textArea = $('#'+id),
        len = textArea.val().length,
        start = textArea[0].selectionStart,
        end = textArea[0].selectionEnd,
        selectedText = textArea.val().substring(start, end);
    textArea.val(textArea.val().substring(0, start) + text + textArea.val().substring(end, len));
    $('#'+id).keyup();
}

function setOptin() {
    $('#optin_message').val($('#optin_message_part1').val() + $('#optin_message_part2').text() + $('#optin_message_part3').text());
}

function checkKeywordAvailability() {
    if ($.trim($('#txtKeyword').val()) != '') {
        $("#KeywordAvailabilityMessage").show();
        $("#KeywordAvailabilityText").removeClass();
        $("#KeywordAvailabilityText").addClass("vanityAvailable");
        //check invalid
        var letters = /^[0-9aA-zZ]+$/;
        //keyword = keyword.replace(' ', '');
        var result = letters.test($('#txtKeyword').val());
        if (!result) {
            $("#KeywordAvailabilityText").html('Keyword Invalid');
            $("#KeywordAvailabilityText").removeClass();
            $("#KeywordAvailabilityText").addClass("KeywordNotAvailable");
            $("#boxVanity").css("border", "1px solid red");
            $("#LeftVanity").css("border-right", "1px solid red");
            $("#txtKeyword").css("background-color", "#F6E1E1");
            $("#SMSHistoryScreenArea").empty();
        } else {
            $.ajax({
                type: "POST",
                url: "/session/cfc/csc/csc.cfc?method=CheckKeywordAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
                data: {
                    inpKeyword: $('#txtKeyword').val(),
                    inpShortCode: $('#shortCode').val(),
                },
                dataType: "json",
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                success: function(d) {
                    $('#processingPayment').hide();
                    $("#KeywordAvailabilityMessage").show();
                    if (d.RXRESULTCODE > 0) {
                        if (parseInt(d.EXISTSFLAG) == 0) {
                            $("#KeywordAvailabilityText").html('Available');
                            $("#KeywordAvailabilityText").removeClass();
                            $("#KeywordAvailabilityText").addClass("KeywordAvailable");
                            $("#boxVanity").css("border", "1px solid #CCCCCC");
                            $("#LeftVanity").css("border-right", "1px solid #CCCCCC");
                            $("#txtKeyword").css("background-color", "");
                            $("#SMSHistoryScreenArea").empty();
                            $("#SMSHistoryScreenArea").append('<div class="bubble me">' + $("#txtKeyword").val() + '</div><div class="bubble you you1" rel="274">&nbsp' + $("#optin_message").val() + '</div>');
                            if ($("#confirm_message").val() !=''){
                            	$("#SMSHistoryScreenArea").append('<div class="bubble me">YES</div><div class="bubble you" rel="274">&nbsp' + $("#confirm_message").val() + '</div>');
                            }
                        } else {
                            $("#KeywordAvailabilityText").html('In Use');
                            $("#KeywordAvailabilityText").removeClass();
                            $("#KeywordAvailabilityText").addClass("KeywordNotAvailable");
                            $("#boxVanity").css("border", "1px solid red");
                            $("#LeftVanity").css("border-right", "1px solid red");
                            $("#txtKeyword").css("background-color", "#F6E1E1");
                        }
                    }
                }
            });
        }
    } else {
        $('#KeywordAvailabilityText').html('');
    }
}

function validate_form(id) {
    if ($('#opt-in-form').validationEngine('validate') && $('#KeywordAvailabilityText').text() != 'In Use') {
        var KeywordId = $("#keywordId").val();
        var Keyword = $("#txtKeyword").val();
        var OptInMessage = $("#optin_message").val();
        var OptInMessageId = $("#optin_message_id").val();
        var ConfirmMessage = $("#confirm_message").val();
        var ConfirmMessageId = $("#confirm_message_id").val();
        var GroupId = $("#group_id").val();
        var ShortCodeRequestId = $("#ShortCodeRequestId").val();
        var shortCode = $("#shortCode").val();
        var inpBatchId = $("#inpBatchId").val();
        var shortCodeId = $("#shortCodeId").val();
        var INPBATCHDESC = $("#campaign_name").val();
        
        var CustomHELP = $("#CustomHELP").val();
        var CustomSTOP = $("#CustomSTOP").val();
        //request more
        var RequestMore1 = '';
        var SCDF1 = '';
        var RequestMore2 = '';
        var SCDF2 = '';
        var RequestMore3 = '';
        var SCDF3 = '';
        var RequestMore4 = '';
        var SCDF4 = '';
        var i = 1;
        $("[name=request_more]").each(function() {
            if (i == 1) {
                RequestMore1 = $(this).val();
            }
            if (i == 2) {
                RequestMore2 = $(this).val();
            }
            if (i == 3) {
                RequestMore3 = $(this).val();
            }
            if (i == 4) {
                RequestMore4 = $(this).val();
            }
            i++;
        });
        i = 1;
        $("[name=SCDF]").each(function() {
            if (i == 1) {
                SCDF1 = $(this).val();
            }
            if (i == 2) {
                SCDF2 = $(this).val();
            }
            if (i == 3) {
                SCDF3 = $(this).val();
            }
            if (i == 4) {
                SCDF4 = $(this).val();
            }
            i++;
        });
        try {
            var url = "";
            if (KeywordId) {
                url = "/session/sire/models/cfc/optin.cfc?method=UpdateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
            } else {
                url = "/session/sire/models/cfc/optin.cfc?method=AddOptIn&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
            }
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    INPBATCHDESC: INPBATCHDESC,
                    KeywordId: KeywordId,
                    Keyword: Keyword,
                    OptInMessage: RXencodeXML(OptInMessage),
                    OptInMessageId: OptInMessageId,
                    ConfirmMessage: RXencodeXML(ConfirmMessage),
                    ConfirmMessageId: ConfirmMessageId,
                    GroupId: GroupId,
                    ShortCodeRequestId: ShortCodeRequestId,
                    shortCode: shortCode,
                    inpBatchId: inpBatchId,
                    shortCodeId: shortCodeId,
                    Response: Keyword,
                    IsSurvey: 1,
                    CustomHELP :CustomHELP,
                    CustomSTOP: CustomSTOP,
                    RequestMore1: RXencodeXML(RequestMore1),
                    SCDF1: RXencodeXML(SCDF1),
                    RequestMore2: RXencodeXML(RequestMore2),
                    SCDF2: RXencodeXML(SCDF2),
                    RequestMore3: RXencodeXML(RequestMore3),
                    SCDF3: RXencodeXML(SCDF3),
                    RequestMore4: RXencodeXML(RequestMore4),
                    SCDF4: RXencodeXML(SCDF4)
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $('#processingPayment').hide();
                },
                success: function(data) {
                    $('#processingPayment').hide();	
                	inpBatchId = (inpBatchId != 0)? inpBatchId : data.BATCHID;
                	
                    if (id == 'optin-save') {
                        window.location.href = '/session/sire/pages/campaign-manage';
                    } else if (id == 'optin-save-request') {
                        window.location.href = '/session/sire/pages/opt-in-info?inpBatchId=' + inpBatchId;
                    }
                }
            })
        } catch (ex) {
            $('#processingPayment').hide();
            return false;
        }
    }
    return false;
}