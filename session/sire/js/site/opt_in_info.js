(function($) {
    "use strict";
    var LIMIT_MESSAGE = 160;
    var fieldIndex = 1;
    var i = 1;
	$("[name=request_more]").each(function () {
		fieldIndex++;
		//countTextAvail("#help-block-"+this.id,LIMIT_MESSAGE, this.value.length)
		countText("#help-block-"+this.id,this.value);
	});

    $("#add_more_data").click(function(e) {
    	
        
        var $template = $('#fieldTemplate');
        if (fieldIndex <= 4) {
            var $clone = $template.clone().removeClass('hide').removeAttr('id').attr('data-book-index', fieldIndex).insertBefore($template);

            $clone
                .find('[name="request_more_template"]').attr({
					  name: "request_more",
					  id: "requestmore_"+fieldIndex
					}).end()
				.find('[name="SCDF_template"]').attr({
					  name: "SCDF",
					  id: "SCDF"+fieldIndex
					}).end()
	            .find('#help-block-template').attr('id', 'help-block-requestmore_'+fieldIndex).end();

			fieldIndex++;
            
        } else {
            bootbox.dialog({
                message: "Add more over filed.",
                title: "Collect more data",
                className: "",
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn btn-medium btn-success-custom",
                        callback: function() {}
                    }
                }
            });
        }
    });
    

        
    $("#opt-in-form")
	    .validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
            focusFirstField : false
	    })
	    .on('click', '#optin-save', function(event) {
	    	event.preventDefault();
	    	
        
	    	validate_form('optin-save');
	    })
	    .on('click', '#optin-cancel', function() {
	    	var inpBatchId = $("#inpBatchId").val();
    		window.location.href = '/session/sire/pages/campaign-manage' ;
    		return false;
	    })
	    .on('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-book-index');
				fieldIndex--;
           		 // Remove element containing the fields
            	$row.remove();
            	requestMoreOnPhone();
            	$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
        })
        .on('keyup change','.request_more', function() {
        	//console.log(this.value.length);
        	//countTextAvail("#help-block-"+this.id,LIMIT_MESSAGE, this.value.length)
        	requestMoreOnPhone();
        	countText("#help-block-"+this.id,this.value);
        	$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
        });
    
})(jQuery);

function requestMoreOnPhone() {
    $("#SMSRequestMore").empty();
    $("[name=request_more]").each(function() {
        if (this.value != '') $("#SMSRequestMore").append('<div class="bubble you" rel="274">&nbsp' + this.value + '</div>');
    });
}
function countTextAvail(id,limit,length) {
    if (limit - length < 0) {
        $(id).text((length - limit) + ' characters over');
        $(id).addClass('text-danger');
    } else {
        $(id).text((limit - length) + '/' + limit + ' characters available');
        $(id).removeClass('text-danger');
    }
}

function validate_form(id) {
	
	//check duplicate
	//var $current;
	var duplicate = 0;
	$("[name=SCDF]").each(function(i, $current) {
	    $("[name=SCDF]").each(function() {
	        if ($(this).val() == $current.value && $(this).attr('id') != $current.id) {
	            duplicate = 1;
	            bootbox.dialog({
	                message: "Duplicate " + $current.value,
	                title: "Collect more data",
	                className: "",
	                buttons: {
	                    success: {
	                        label: "Ok",
	                        className: "btn btn-medium btn-success-custom",
	                        callback: function() {}
	                    }
	                }
	            });
	            if (duplicate) return false;
	        }
	    });
	    if (duplicate) return false;
	});
    
    if ($('#opt-in-form').validationEngine('validate') && !duplicate) {
    	
        var KeywordId = $("#keywordId").val();
        var Keyword = $("#txtKeyword").val();
        
        var OptInMessage = $("#optin_message").val();
        var OptInMessageId = $("#optin_message_id").val();
        
        var ConfirmMessage = $("#confirm_message").val();
        var ConfirmMessageId = $("#confirm_message_id").val();
        var GroupId = $("#group_id").val();
        
        var ShortCodeRequestId = $("#ShortCodeRequestId").val();
        var shortCode = $("#shortCode").val();
        var inpBatchId = $("#inpBatchId").val();
        var shortCodeId = $("#shortCodeId").val();
        var INPBATCHDESC = $("#campaign_name").val();
        
        var CustomHELP = $("#CustomHELP").val();
        var CustomSTOP = $("#CustomSTOP").val();
        
        
        //request more

        var RequestMore1 = '';
        var SCDF1 = '';
        
        var RequestMore2 = '';
        var SCDF2 = '';
        
        var RequestMore3 = '';
        var SCDF3 = '';
        
        var RequestMore4 = '';
        var SCDF4 = '';
		
		var i = 1;
		
		
		$("[name=request_more]").each(function () {
			
			if (i == 1) {
				RequestMore1 = $(this).val();
			}
			if (i == 2) {
				RequestMore2 = $(this).val();
			}
			if (i == 3) {
				RequestMore3 = $(this).val();
			}
			if (i == 4) {
				RequestMore4 = $(this).val();
			}
			i++;
           
        });
        
        
        i = 1;
        
        $("[name=SCDF]").each(function () {
						
			if (i == 1) {
				SCDF1 = $(this).val();
			}
			if (i == 2) {
				SCDF2 = $(this).val();
			}
			if (i == 3) {
				SCDF3 = $(this).val();
			}
			if (i == 4) {
				SCDF4 = $(this).val();
			}
			i++;
           
        });
        
        //console.log(SCDF4)
        
        try {
            var url = "";
            if (KeywordId) {
                url = "/session/sire/models/cfc/optin.cfc?method=UpdateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
            } 
            
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                	INPBATCHDESC:INPBATCHDESC,
                    KeywordId: KeywordId,
                    Keyword: Keyword,
                    OptInMessage: RXencodeXML(OptInMessage),
                    OptInMessageId: OptInMessageId,
                    ConfirmMessage: RXencodeXML(ConfirmMessage),
                    ConfirmMessageId: ConfirmMessageId,
                    GroupId : GroupId,
                    ShortCodeRequestId: ShortCodeRequestId,
                    shortCode: shortCode,
                    inpBatchId: inpBatchId,
                    shortCodeId: shortCodeId,
                    Response: Keyword,
                    IsSurvey: 1,
                    CustomHELP :CustomHELP,
                    CustomSTOP: CustomSTOP,
                    RequestMore1: RXencodeXML(RequestMore1),
                    SCDF1: RXencodeXML(SCDF1),
                    RequestMore2: RXencodeXML(RequestMore2),
                    SCDF2 : RXencodeXML(SCDF2),
                    RequestMore3: RXencodeXML(RequestMore3),
                    SCDF3: RXencodeXML(SCDF3),
                    RequestMore4: RXencodeXML(RequestMore4),
                    SCDF4 : RXencodeXML(SCDF4)
                    
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  $('#processingPayment').hide();  
                },
                success: function(data) {
                    $('#processingPayment').hide();
                    if (id == 'optin-save') {
                    	window.location.href = '/session/sire/pages/campaign-manage' ;
                    } 
                }
            })
        } catch (ex) {
            $('#processingPayment').hide();
            return false;
        }
    }
    
    
    return false;
}