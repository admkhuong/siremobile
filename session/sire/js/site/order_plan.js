(function($) {
    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing' && checkTimeout == 0)
	  		return 'Payment processing. Please don\'t refresh and close.';
	});
    $("#order-plan").change(function(e) {
        var plan = $("#order-plan").val();
        $.ajax({
            type: "GET",
            url: "/session/sire/models/cfc/order_plan.cfc?method=GetOrderPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: 'json',
            data: {
                plan: plan
            },
            beforeSend: function(){
				$('#processingPayment').show();
			},
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            	$('#processingPayment').hide();
            },
            success: function(data) {
            	$('#processingPayment').hide();
            	$('#plan').val(plan);
            	$('#MonthlyAmount').text('$' + data.DATA.AMOUNT);
                $('#NumberKeyword').text(data.DATA.KEYWORDSLIMITNUMBER);
                $('#FirstSMSInclude').text(data.DATA.FIRSTSMSINCLUDED);

                $.ajax({
		            type: "GET",
		            url: "/session/sire/models/cfc/order_plan.cfc?method=GetUserPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		            dataType: 'json',
		            data: {
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {},
		            success: function(data1) {
		            	
		            	var BuyKeywordNumber = 0;
		            	var totalAMOUNT = data.DATA.AMOUNT;
		            	var priceBuyKeywordNumber = 0
		            	
		            	if (parseInt(data1.DATA.TOTALKEYWORD) > parseInt(data.DATA.KEYWORDSLIMITNUMBER))  {
		            		BuyKeywordNumber = parseInt(data1.DATA.TOTALKEYWORD) - parseInt(data.DATA.KEYWORDSLIMITNUMBER);
		            		priceBuyKeywordNumber = BuyKeywordNumber * data1.DATA.PRICEKEYWORDAFTER;
		            		totalAMOUNT = parseInt(totalAMOUNT) + (BuyKeywordNumber * data1.DATA.PRICEKEYWORDAFTER);
		            	}    
		                  
		                $('#BuyKeywordNumber').text('$' + priceBuyKeywordNumber);
		                $('#MonthlyAmountTotal').text('$' + totalAMOUNT);
		                $('#amount').val(totalAMOUNT);
		            }
		        });
                
            }
        });
        
    });
})(jQuery);

function paymentConfirm() {
	$('#rsSelectYourPlan').text($('#order-plan option:selected').text());
	$('#rsFirstCreditsInclude').text($('#FirstSMSInclude').text());
	$('#rsNumberofPlanKeyword').text($('#NumberKeyword').text());
	$('#rsMonthlyKeywordFee').text($('#BuyKeywordNumber').text());
	$('#rsMonthlyPlanPrice').text($('#MonthlyAmount').text());
	$('#rsAmount').text($('#MonthlyAmountTotal').text());
}
