var _tblListCredentials;

//init datatable for active agent
function InitGroupCredentials(customFilterObj) {
    var customFilterData = typeof (customFilterObj) != 'undefined' ? JSON.stringify(customFilterObj) : "";
    _tblListCredentials = $('#tblListCredentials').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bServerSide": true,
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bLengthChange": false,

        "aoColumns": [
        	{
                "sName": 'Created',
                "sTitle": 'Created Date',
                "sWidth": '40',
                "bSortable": false
            },
            {
                "sName": 'AccessKey',
                "sTitle": 'Access Key ID',
                "sWidth": '110',
                "bSortable": false
            },
            {
                "sName": 'SecretKey',
                "sTitle": 'Secret Access Key',
                "sWidth": '90',
                "bSortable": false
            },			           
            {
                "sName": 'Active',
                "sTitle": 'Status',
                "sWidth": '80',
                "bSortable": false
            }
        ],
        "sAjaxSource": '/session/cfc/multilists2.cfc?method=GetCredentialsListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        "fnServerParams": function ( aoData ) {
         aoData.push( { "name": "No", "value": "4" });
	    },   
        "fnServerData": function(sSource, aoData, fnCallback) {
        	
            $.ajax({
                dataType: 'json',
                type: "POST",
                url: sSource,
                data: aoData,
                success: fnCallback
            });
        },
        "fnInitComplete": function(oSettings, json) {
            //in order to prevent this talbe from collapsing, we must fix its min width 
            $('#tblListCredentials').attr('style', '');
            $('#tblListCredentials').css('min-width', '1053px');
            appendColumnNo();
        }
    });
}

InitGroupCredentials();


function appendColumnNo() {
	$('table > thead').find('tr').each(function(){
	  $(this).prepend('<th style="width: 40px;">No</th>');
	})
	
	var i = 1;
	$('table > tbody').find('tr').each(function(){
	  $(this).prepend('<td>' + i + '</td>');
	  i++;
	})
}

function createAccessKey() {
    bootbox.dialog({
        message: 'Are you sure you would to create a new access key?',
        title: 'Create Access Key',
        className: "security-credentials-popup",
        buttons: {
            success: {
                label: "Yes",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if (result) {
                        $.ajax({
                            url: '/session/cfc/administrator/credential.cfc?method=createAccesskey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                            type: 'post',
                            dataType: 'json',
                            beforeSend: function(){
                                $('#processingPayment').show();
                            },
                            success: function(d) {
                                $('#processingPayment').hide();
                                if (d.SUCCESS == 1 && result) {
                                        location.reload();
                                }
                            }
                        });
                    }
                }
            },
            cancel: {
                label: "Cancel",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });
}

function showSecretKey(secretKey) {
    bootbox.dialog({
        message: '<div class="active">' + secretKey + '</div>' + '<h3>URI/URL Encoded</h3><div class="active">' + encodeURIComponent(secretKey) + '</div>',
        title: "Secret Access Key",
        className: "security-credentials-popup",
        buttons: {
            success: {
                label: "Ok",
                className: "btn btn-medium btn-success-custom",
                callback: function() {

                }
            }
        }
    });

}

function makeActiveInactiveDialog(accessKey, activeInactive) {
    bootbox.dialog({
        message: 'Are you sure you would like to make <span class="active">' + accessKey + '</span> ' + (activeInactive == 0 ? 'inactive?' : 'active?'),
        title: "Access key",
        className: "security-credentials-popup",
        buttons: {
            success: {
                label: "Yes",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if (result) {
                        $.ajax({
                            url: '/session/cfc/administrator/credential.cfc?method=activeInactiveAccessKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                accessKey: accessKey,
                                activeInactive: activeInactive
                            },
                            beforeSend: function(){
                                $('#processingPayment').show();
                            },
                            success: function(d) {
                                $('#processingPayment').hide();
                                location.reload();
                            }
                        });
                    }
                }
            },
            cancel: {
                label: "Cancel",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });

}

function deleteAccessKey(accessKey) {

    bootbox.dialog({
        message: 'Are you sure delete Access key with ID <span class="active">' + accessKey + '</span>? This action is permanent and cannot be undone.',
        title: "Delete Security Credential",
        className: "security-credentials-popup",
        buttons: {
            success: {
                label: "Yes",
                className: "btn btn-medium btn-success-custom",
                callback: function(result) {
                    if (result) {
                        $.ajax({
                            url: '/session/cfc/administrator/credential.cfc?method=deleteAccessKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                accessKey: accessKey
                            },
                            beforeSend: function(){
                                $('#processingPayment').show();
                            },
                            success: function(data) {
                                $('#processingPayment').hide();
                                if (data.SUCCESS == 1) {
                                    location.reload();
                                } else {
                                    jAlert(data.MESSAGE);
                                }
                            }
                        });
                    }
                }
            },
            cancel: {
                label: "No",
                className: "btn btn-medium btn-back-custom",
                callback: function() {

                }
            },
        }
    });
}



$('form[name="frm-security-questions"]').validationEngine('attach', {promptPosition : "topLeft", autoHidePrompt: true, autoHideDelay: 5000, scroll: false}).on('submit', function(event){
    event.preventDefault();
    if($(this).validationEngine('validate')){
        var dataPost = $(this).serialize();

        confirmBox("Are you sure you want to update AQ security?", "Confirmation", function(){
            $.ajax({
                url: "/session/sire/models/cfc/myaccountinfo.cfc?method=EditUserSecurityQuestion"+ajaxResQueryString,
                type: "POST",
                dataType: "json",
                data: dataPost,
                success: function(data){
                    if(data.RESULT == "SUCCESS"){
                        alertBox(data.MESSAGE, '')
                    }

                    $("#inp_simon_1").val("******");
                    $("#inp_simon_2").val("******");
                    $("#inp_simon_3").val("******");
                }
            });
        });
    }
});

$('form[name="add_security_question_form"]').validationEngine('attach', {promptPosition : "topLeft", autoHidePrompt: true, autoHideDelay: 5000, scroll: false}).on('submit', function(event){
    event.preventDefault();
    if($(this).validationEngine('validate')){
        var dataPost = $(this).serialize();

        confirmBox("Are you sure you want to add AQ security?", "Confirmation", function(){
            $.ajax({
                url: "/session/sire/models/cfc/myaccountinfo.cfc?method=AddUserSecurityQuestion"+ajaxResQueryString,
                type: "POST",
                dataType: "json",
                data: dataPost,
                success: function(data){
                    if(data.RESULT == "SUCCESS"){
                        alertBox(data.MESSAGE, '')
                    }
                }
            });
        });
    }
});