$(document).ready(function(){
		
	 // codes works on all bootstrap modal windows in application
    $('.modal').on('hidden.bs.modal', function(e)
    { 
        $(this).removeData();
    }) ;

    // Prevent vent if button has been disabled
   	$('a.btn-success-custom[href="short-url-add"]').click(function(event){

   		if($(this).attr('disabled') == 'disabled'){

   			return false;
   		}
   	});
    
InitControl();	
var _tblListShortURL;
	function InitControl(customFilterObj)
	{//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData = typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListShortURL = $('#tblListShortURL').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					} else {
						history.replaceState(history.state, document.title, location.pathname + location.search);
					}
				}
			},
    		"aaSorting": [[ 2, 'desc' ]],
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
			"responsive": true,
		    "aoColumns": [
				{"sWidth": "105px", "sName": 'Short URL Data', "sTitle": 'Short URL Data',"bSortable": true,"sClass":"surl-name", "orderSequence": [ "asc" ]},
				{"sWidth": "4em", "sName": 'Hit Counter', "sTitle": '<span class="visible-xs">Hits</span><span class="hidden-xs">Hits</span>',"bSortable": true,"sClass":"surl-counter", "orderSequence": [ "desc" ]},			
				{"sWidth": "6em", "sName": 'Actions', "sTitle": 'Actions',"bSortable": false,"sClass":"surl-action"}
			],
			"sAjaxDataProp": "ListShortURLData",
			"sAjaxSource": '/session/sire/models/cfc/urltools.cfc?method=GetShortURLList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   "fnDrawCallback": function( oSettings ) {
		      // MODIFY TABLE
				//$("#tblListShortURL thead tr").find(':last-child').css("border-right","0px");
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
				$('#tblListShortURL > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListShortURL_paginate').hide();
						return false;
					}
					else{
						$('#tblListShortURL_paginate').show();
					} 
							
					  
				})
			
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		        if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );

		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: function(data){
			             	fnCallback(data);
			             	//set button disable status by reponse data from server
			             	if(data.LOCKADDBUTTON === true){
			             		$('a.btn-success-custom[href="short-url-add"]').attr('disabled', 'disabled');
			             		$('p.warning-text-short-url').removeClass("hidden");
			             	} else {
			             		$('a.btn-success-custom[href="short-url-add"]').removeAttr('disabled');
			             		$('p.warning-text-short-url').addClass("hidden");
			             	}
			             } 
			             
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				// MODIFY TABLE
				$('#tblListShortURL > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');

					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListShortURL_paginate').hide();
						return false;
					} 
				})
			}
	    });
	}


	// Delete Short URL 
	$( document ).on( "click", ".btn-delete-surl", function(event) {
		event.preventDefault();
		
		var inpPKId = parseInt($(this).data('surl-id'));
		var currentPage = $('input.paginate_text').val();

		var dialogText = 'Are you sure you want to delete this short URL?';		

		
		try{

			$('.btn').prop('disabled',true);
			bootbox.dialog({
	        message: dialogText,
	        title: 'Delete Short URL',
	        className: "",
	        buttons: {
	            success: {
	                label: "OK",
	                className: "btn btn-medium btn-success-custom",
	                callback: function(result) {
	                    if(result){
							$.ajax(
							{
								type: "POST",
								url: '/session/sire/models/cfc/urltools.cfc?method=DeleteShortURL&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
								dataType: 'json',
								data: { inpPKId: inpPKId },
								beforeSend: function( xhr ) {
									$('.tblListShortURL_processing').show();
									$('#processingPayment').show();
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									bootbox.dialog({
									    message: "There is an error.Please try again later.",
									    title: "Delete Short URL Error",
									    buttons: {
									        success: {
									            label: "OK",
									            className: "btn btn-medium btn-success-custom",
									            callback: function() {}
									        }
									    }
									});
									$('.btn').prop('disabled',false);
									$('.tblListShortURL_processing').hide();
								},					  
								success:		
									function(d) {
										$('#processingPayment').hide();
										$('.btn').prop('disabled',false);
										$('.tblListShortURL_processing').hide();
										if(d.RXRESULTCODE == 1){
											//goto success page
											bootbox.dialog({
											    message: d.MESSAGE,
											    title: "Delete Short URL",
											    buttons: {
											        success: {
											            label: "OK",
											            className: "btn btn-medium btn-success-custom",
											            callback: function() {}
											        }
											    }
											});
											var oTable = $('#tblListShortURL').dataTable();
											//oTable.fnDraw();
											oTable.fnPageChange(parseInt(currentPage-1));
											
																					
											return;
										}
										else{
											bootbox.dialog({
											    message: "There is an error.Please try again later.",
											    title: "Delete Short URL - Error",
											    buttons: {
											        success: {
											            label: "OK",
											            className: "btn btn-medium btn-success-custom",
											            callback: function() {}
											        }
											    }
											});
										}
									} 		
								});
							}
		                }
		            },
		            cancel: {
		                label: "Cancel",
		                className: "btn btn-medium btn-back-custom",
		                callback: function() {
						$('.btn').prop('disabled',false);
		                }
		            },
	        	}
	    	});
			
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.dialog({
			    message: "There is an error.Please try again later.",
				title: "Delete Short URL - Error",
			    buttons: {
			        success: {
			            label: "OK",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
			$('.btn').prop('disabled',false);
			$('.tblListShortURL_processing').hide();
		}
				
	});
	
		
});	

