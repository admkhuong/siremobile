(function($){
	$('#signin').on('hidden.bs.modal', function(){
		$('#signin form')[0].reset();
		$('#signin .signin-message').text('');
	});
	
	$('#signin form').submit(function(event){
		event.preventDefault();
		
		if ($('#signin form').validationEngine('validate')) {
			var url_sign_in = '/public/sire/models/cfc/userstools.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
			var message_sign_in = $('#signin .signin-message').text('');
			$.ajax({
				method: 'POST',
				url: url_sign_in,
				data: $(this).serialize(),
				beforeSend: function(){
                	$('#processingPayment').show();
                },
				success: function(DATA) {
					$('#processingPayment').hide();
					if (DATA && DATA.RXRESULTCODE === 1) {
						
						if(parseInt(DATA.MFAREQ) == 1){
							window.location.href = '/device-register';
						}
						else
						location.href = location.href;
							
					} else {
						message_sign_in.text('Incorrect email or password.');
					}
				}
			});
		}
	});

	$('#signout .btn-modal-signout').click(function(event){
		event.preventDefault();

		if($('#signout #DALD').is(':checked')){
			var DALD = 1;
		}
		else
		var DALD = 0;
		var userId = $('#signout #SESuserId').val()
			
		var url_sign_out = '/signout?DALD='+DALD+'&userId='+userId;
		window.location.href = url_sign_out;
		
	});


})(jQuery);