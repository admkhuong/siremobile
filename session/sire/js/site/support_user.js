(function($){
    $("#support_form").validationEngine({
        promptPosition : "topLeft",
        scroll: false,
        focusFirstField : false
    });

    $("#send-support").click(validate_form);
})(jQuery);

function validate_form() {
    if ($('#support_form').validationEngine('validate')) {
        var subject = $("#infoSubject").val();
        var message = $("#infoMsg").val();
        var emailAddress = $('#infoContact').val();
        var username = $('#infoName').val();

        try {
            $.ajax({
                type: 'POST',
                url: '/public/sire/models/cfm/contact_us.cfm',
                dataType: 'json',
                data:  {
                    infoName: username,
                    infoContact: emailAddress,
                    infoSubject: subject,
                    infoMsg: message
                },
                beforeSend: function(){
                    $('#processingPayment').show();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alertBox('Sorry.', 'Request Fail!');
                    $('#processingPayment').hide();
                },
                success: function(data) {
                    $("#infoName").val("");
                    $("#infoContact").val("");
                    $("#infoSubject").val("");
                    $("#infoMsg").val("");
                    alertBox('Thank you,<br>Your message has been sent.', 'Request success!');
                },
                complete: function(){
                    $('#processingPayment').hide();
                }
            });
        } catch(ex) {
            return false;
        }
    }

    return false;
}