(function($){
	var cfcomponentQueryString = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	var modalAddNew = $('#add-new-category'), modalShowDesc = $('#show-description-mdl'), modalEdit = $('#edit-category');


	var GetCategoryList = function(strFilter){

		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


		var table = $('#tblListEMS').dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"mData": "NAME", "sName": 'NAME', "sTitle": 'Sub Category Name',"bSortable": true,"sClass":"user-name"},
				{"mData": "DISPLAY_NAME", "sName": 'DISPLAY_NAME', "sTitle": "Display Name","bSortable": true,"sClass":"display-name"},
				{"mData": "DESCRIPTION", "sName": 'DESCRIPTION', "sTitle": "Description","bSortable": false,"sClass":"description"},
				{"mData": "ORDER_NUMBER", "sName": 'ORDER_NUMBER', "sTitle": "Order","bSortable": true,"sClass":"order"},
				{"mData": "GROUP_NAME", "sName": 'GROUPNAME', "sTitle": "Category Group","bSortable": false,"sClass":"group"},
				{"mData": "ID", "sName": 'ID', "sTitle": "Actions","bSortable": false,"sClass":"id"},
			],
			"sAjaxDataProp": "DATALIST",
			"sAjaxSource": '/session/sire/models/cfc/category.cfc?method=GetCategory&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {
				$(nRow).find('td.id')
					   .html($('<a class="btn btn-success-custom btn-edit" ></a>').data('item', aData).append($('<i class="glyphicon glyphicon-edit">')))
					   .append($('<a class="btn btn-success-custom btn-remove" ></a>').data('item', aData).append($('<i class="glyphicon glyphicon-trash">')));



				$(nRow).find('td.description')
					   .html($('<a class="btn btn-success-custom btn-show-description" ></a>').data('description', aData.DESCRIPTION).append('show'));
				$('.dataTables_paginate').show();
	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				// console.log(fnCallback);
				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
		        aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
				if (oSettings._iRecordsTotal == 0) {
					$('tbody[role=alert]').remove();
					$('.dataTables_paginate').hide();
				} else {
					$('.dataTables_paginate').show();
					$('input.paginate_text').val('1');
				}
			}
		});
	}
	GetCategoryList();

	
	$('body').on('click', 'a.btn-edit', function(event){
		var item = $(this).data('item');

		// binding data to form controls ==========================================
		$('span[data-type="lbl-name"]').text(item.NAME);

		$('input[type="hidden"][name="inpID"]').val(item.ID);

		$('select.form-edit-control[name="inpGroupID"]').find('option').each(function(){
			$(this).prop('selected', false);
			if(this.value == item.GROUP_ID){
				$(this).prop('selected', true);
			}
		});

		$('input.form-edit-control[name="inpName"]').val(item.NAME);

		$('input.form-edit-control[name="inpDisplayName"]').val(item.DISPLAY_NAME);

		$('input.form-edit-control[name="inpOrder"]').val(item.ORDER_NUMBER);

		CKEDITOR.instances['inpDescriptionEdit'].setData(item.DESCRIPTION);

		//=========================================================================



	
		//show modal 
		modalEdit.modal('show');
	});


	$('body').on('click', 'a.btn-remove', function(event){
		var item = $(this).data('item');
		bootbox.confirm({
		    title: "Delete Sub Category",
		    message: "Are you sure you want to delete this Sub Category?",
		    buttons: {
		    	confirm: {
		            label: 'Confirm',
		            className: "btn btn-medium btn-success-custom"
		        },
		        cancel: {
		            label: '<i class="fa fa-times"></i> Cancel',
		            className: "btn btn-medium btn-back-custom"
		        }
		    },
		    callback: function (result) {
		    	if(result){
		    		removeCategory(item.ID, function(){
		    			bootbox.dialog({
						    message:'Delete Sub Category Successfully!',
						    title: 'Delete Sub Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function(){
						            	GetCategoryList(buildFilterData());
						            }
						        }
						    }
						});
		    		}, function(){
		    			bootbox.dialog({
						    message:'Delete Sub Category Failed!',
						    title: 'Delete Sub Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
		    		});
		    	}
		    }
		});

	});

	var removeCategory = function(catID, successCallback, errorCallback){
		$.ajax({
			url: "/session/sire/models/cfc/category.cfc?method=deleteCategory&"+cfcomponentQueryString,
			type: "POST",
			dataType: "json",
			data: {inpID: catID},
			success: function(data){
				if(data.RXRESULTCODE != 1){
					if(typeof errorCallback == 'function'){
						errorCallback();
					}
				} else {
					if(typeof successCallback == 'function'){
						successCallback();
					}
				}
			}
		});
	}

	var buildFilterData = function (){
		var dataFilter = [];
		$('.list-filter-control').each(function(index){
			var controlValue = $(this).val();
			if(controlValue){

				if($(this).prop('tagName') == "SELECT"){
					if(controlValue > 0){
						var item = {'field': $(this).data('field'), 'type': $(this).data('type'), 'operator': $(this).data('operator'), 'value': controlValue};
						dataFilter.push(item);
					}
				} else {
					var item = {'field': $(this).data('field'), 'type': $(this).data('type'), 'operator': $(this).data('operator'), 'value': controlValue};
					dataFilter.push(item);
				}	
			}
		});
		return dataFilter;
	}

	


	$('body').on('click', 'a.btn-show-description', function(event){
		modalShowDesc.find('div.modal-body').html($(this).data('description'));
		modalShowDesc.modal('show');
	});



	$('#addNew').click(function(event){
		modalAddNew.modal('show');
	});


	$('#add-new-category-form, #edit-category-form').validationEngine('attach', {promptPosition : "topLeft"});

	$('#add-new-category-form').on('submit', function(event){
		event.preventDefault();

		$(this).find('#inpDescription').val(CKEDITOR.instances['inpDescription'].getData());

		if($(this).validationEngine('validate')){
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=addNewCategory&"+cfcomponentQueryString,
				type: "post",
				dataType: "json",
				data: $(this).serialize(),
				beforeSend: function(){

				},
				success: function(rspData){

					if(rspData.RXRESULTCODE == 1){

						bootbox.dialog({
						    message:'Create Sub-Category Success',
						    title: 'Create Sub-Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function(){
						            	modalAddNew.modal("hide");
						            }
						        }
						    }
						});
						GetCategoryList(buildFilterData());
					} else if(rspData.RXRESULTCODE == -2) {
						bootbox.dialog({
						    message:'Order duplicate!',
						    title: 'Create Sub-Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
					}
				},
				complete: function(){

				}

			});
		}
	});

	$('#edit-category-form').on('submit', function(event){
		event.preventDefault();
		if($(this).validationEngine('validate')){
			$.ajax({
				url: "/session/sire/models/cfc/category.cfc?method=updateCategory&"+cfcomponentQueryString,
				type: "post",
				dataType: "json",
				data: $(this).serialize(),
				beforeSend: function(){

				},
				success: function(rspData){
					if(rspData.RXRESULTCODE == 1){
						
						bootbox.dialog({
						    message:'Updated Sub-Category Success!',
						    title: 'Update Sub-Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function(){
						            	modalEdit.modal("hide");
						            	GetCategoryList(buildFilterData());
						            }
						        }
						    }
						});
					} else if(rspData.RXRESULTCODE == -2) {
						bootbox.dialog({
						    message:'Order duplicate!',
						    title: 'Update Sub-Category',
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom"
						        }
						    }
						});
					}
				},
				complete: function(){

				}
			});
		}

	});

	modalAddNew.on('hidden.bs.modal', function(){
		resetForm('add-new-category-form');
	});
	

	var resetForm = function(formID){
		$('#'+formID).trigger('reset');
		$('#'+formID+' .select2').select2({theme: "bootstrap", width: 'auto'});
		CKEDITOR.instances['inpDescription'].setData("");
	};



	$('select.list-filter-control').on('change', function(event){
		GetCategoryList(buildFilterData());
	});



	var timeOut =  null;
	$('input.list-filter-control').on('keyup change', function(event){
		if(event.type == 'keyup'){
			clearTimeout(timeOut);
			timeOut = setTimeout(function(){
				GetCategoryList(buildFilterData());
			}, 500);
		}
		if(!timeOut && event.type == 'change'){
			GetCategoryList(buildFilterData());
		}
	});




	modalEdit.on('shown.bs.modal', function(){
			// Reinitialize select2
		CKEDITOR.instances['inpDescriptionEdit'].focus();
		$(this).find('.select2').select2({theme: "bootstrap", width: 'auto'});
	})
})(jQuery);