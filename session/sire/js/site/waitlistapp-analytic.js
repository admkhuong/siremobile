$(document).ready(function(){
	(function($){

		Date.prototype.getSireDate = function(){
			var month = this.getMonth() + 1;
				month = (month < 10 ? '0'+month : month);

			var date = this.getDate();	
				date = (date < 10 ? '0'+date : date);
			var year = this.getFullYear();

			return year+'-'+month+'-'+date;
		}

		Date.prototype.addDays = function(days) {
			var dat = new Date(this.valueOf())
			dat.setDate(dat.getDate() + days);
			return dat;
		}

		function getDates(startDate, stopDate) {
			var dateArray = new Array();
			var currentDate = startDate;
			while (currentDate <= stopDate) {
			dateArray.push(currentDate)
			currentDate = currentDate.addDays(1);
			}
			return dateArray;
		}

		var inpStartDate = $('#inpStartDate');
		var inpEndDate   = $('#inpEndDate');

		var DatePicker = $('input[data-type="daterange"]').daterangepicker({
			opens: "right",
			drops: "down",
			timeZone: 'UTC',
			singleDatePicker: true,
		    startDate: inpStartDate.data('value'),
		    endDate: inpStartDate.data('value'),
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default"
		}).on('apply.daterangepicker', function(event, DRPObject){
			var control = DRPObject.element.data('control');
			var dateStart = DRPObject.startDate._d.getSireDate(),
				endDate = DRPObject.endDate._d.getSireDate();

			if(control == 'start-date'){
				$('input[type="hidden"][data-control="'+control+'"]').val(dateStart);
			} else  if(control == 'end-date'){
				$('input[type="hidden"][data-control="'+control+'"]').val(endDate);
			}
		});

		// DatePicker.each(function(){
		// 	if($(this).attr('data-control') == 'start-date'){
		// 		$(this).data('daterangepicker').setStartDate(new Date(inpStartDate.val()));
		// 		$(this).data('daterangepicker').setEndDate(new Date(inpStartDate.val()));
		// 	}
		// });




		var validateDateSelected = function(){
			var startTime = (new Date(inpStartDate.val())).getTime();
			var endTime = (new Date(inpEndDate.val())).getTime();
			if(startTime > endTime){
				return false
			}
			return true;

		}

		var colorBar = ["#085f7a", "#7ccbe4", "#71c27a"];

		$('#view-analytic').click(function(){
			var dateStart = inpStartDate.val(),
				dateEnd   = inpEndDate.val();
				listId    = $("#waitlist-select-analytic").val();
			if(validateDateSelected()){	
				$.ajax({
					url: "/session/sire/models/cfc/waitlistapp.cfc?method=GetAnalyticData&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
					type: "GET",
					dataType: "json",
					data: {inpDateStart: dateStart, inpDateEnd: dateEnd, inpListId: listId},
					success: function(rspData){
						var dataWeek = rspData.DATA.WEEK;
						var dataTLine = rspData.DATA.TIMELINE;
						var dataHour = rspData.DATA.HOUR;
						var summary = rspData.DATA.SUMMARY;
						

						// Prepare data for week type=======================================
						var weekdays = [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
						for(var i in weekdays){

							var day = weekdays[i];
							weekdays[i] = {"period": day, "waitlisted": 0, "served": 0, "noshow": 0} ;
							for(var j in dataWeek){
								var dayD = dataWeek[j];
								if(day == dayD.period){
									weekdays[i] = dayD;
								}
							}
						}
						chartBarStacked.setData(weekdays);
						//=================================================================



						//prepare data for hour type=======================================
						var hours = [];
						for(var i = 0; i < 24; i++){

							var strHour = (i < 10 ? "0"+i : i)+":00";
							// console.log(strHour);
							hours[i] = {"period": strHour, "waitlisted": 0, "served": 0, "noshow": 0} ;


							for(var j in dataHour){
								var hour = dataHour[j];
								if(strHour == hour.period){
									hours[i] = hour;
								}
							}
						}

						chartBar.setData(hours);
						//==================================================================

						//prepare data for dates type=======================================
						var usertzOffsetTime = (new Date()).getTimezoneOffset()*60*1000;
						//pacific
						var tzOffsetTime = 8*60*60*1000;
						var sTime = (new Date(dateStart)).getTime(), eTime = (new Date(dateEnd)).getTime();
						var dates = getDates(new Date(sTime+tzOffsetTime+usertzOffsetTime), new Date(eTime+tzOffsetTime+usertzOffsetTime));


						dateDatas = [];
						for(var i = 0; i < dates.length; i++){
							var dateStr = dates[i].getSireDate();
							dateDatas[i] = {"period": dateStr, "waitlisted": 0, "served": 0, "noshow": 0};
							for(var j in dataTLine){

								var date = dataTLine[j];

								if(dateStr == date.period){
									dateDatas[i] = date;
								}
							}
						}

						chart.setData(dateDatas);
						//==================================================================



						//prepare data for dates type=======================================
						for(var prop in summary){
							var summaryItemValue = summary[prop];
							$('strong.summary-value[data-prop="'+prop+'"]').text(summaryItemValue);
						}
						//==================================================================


					}

				});
			} else {
				bootbox.dialog({
				    message: "End Date must greater than or equal Start Date",
				    title: "Date Selected Alert",
				    buttons: {
				        success: {
				            label: "Ok",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {
				            	
				            }
				        }
				    }
				});
			}
		});

			


		var chart =	Morris.Line({
			element: 'graph',
			data: [],
			xkey: 'period',
			ykeys: ['waitlisted', 'served', 'noshow'],
			labels: ['Waitlisted', 'Served', 'No-show'],
			resize: true,
			lineColors: colorBar,
			smooth: true,
			xLabels: "day",
			continuousLine: true,
			hideHover: true
		});



		var chartBarStacked = Morris.Bar({
			element: 'graph-bar-by-weekdays',
			data: [],
			xkey: 'period',
			ykeys: ['waitlisted', 'served', 'noshow'],
			labels: ['Waitlisted', 'Served', 'No-show'],
			resize: true,
			barColors: colorBar,
			hideHover: true
		});

		var chartBar = Morris.Bar({
		  	element: 'graph-bar-by-hours',
		  	data: [],
		  	xkey: 'period',
			ykeys: ['waitlisted', 'served', 'noshow'],
			labels: ['Waitlisted', 'Served', 'No-show'],
		  	resize: true,
		  	barColors: colorBar,
			hideHover: true
		});


		$('i.glyphicon-calendar').click(function(event){
			$(this).prev().trigger('click');
		});


		$(document).ready(function(){
			$('#graph > svg, #graph-bar-by-weekdays > svg, #graph-bar-by-hours > svg').attr('width', '100%');
			$('#view-analytic').trigger('click');
		})
		

	})(jQuery);

});



