(function($){
	/* Customer datatable initializer */
	var GetCustomer = function(filterString, listID){

		$("#tblListEMS").dataTable({
			"bStateSave": false,
			"iStateDuration": -1,
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
		    "aoColumns": [
				{"sName": 'name', "sTitle": 'Name',"bSortable": true,"sClass":"customer-name"},
				{"sName": 'phone', "sTitle": 'Phone',"bSortable": true,"sClass":"customer-phone"},
				{"sName": 'email', "sTitle": 'Email',"bSortable": true,"sClass":"customer-email"},
				{"sName": 'lastvisit', "sTitle": "Update Date","bSortable": false,"sClass":"last-visit"},
				{"sName": 'notes', "sTitle": "Notes","bSortable": false,"sClass":"notes"}
			],
			"sAjaxDataProp": "CUSTOMERLIST",
			"sAjaxSource": '/session/sire/models/cfc/waitlistapp.cfc?method=GetCustomerList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {

	       	},
		   	"fnDrawCallback": function( oSettings ) {
		  		if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				/* hide paginate section if table is empty */
				$('#tblListEMS > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#tblListEMS_paginate').hide();
						return false;
					}
					else{
						$('#tblListEMS_paginate').show();
					} 
				})
		    },
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
				
		        aoData.push(
		            { "name": "customFilter", "value": filterString}
	            );
				aoData.push({"name":"inpListId", "value": listID});
		        oSettings.jqXHR = $.ajax( {
			        "dataType": 'json',
			        "type": "POST",
			        "url": sSource,
			        "data": aoData,
			        "success": fnCallback
		      	});
	        },
			"fnInitComplete":function(oSettings){
			}
		});
	}

	/* datatable search function */
	$('#search').click(function(event){
		event.preventDefault();

		var q = $("#txtKeyword").val();
		var listID = $("#waitlist-select-customer").val();
		GetCustomer(q, listID);
	});

	$('#txtKeyword').on('keypress', function(e){
		if(e.keyCode === 13){
			$('#search').trigger('click');
		}
	});

	$(document).ready(function () {
		$('#search').trigger('click');
	})

})(jQuery);