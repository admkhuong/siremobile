/* history datatable initializer */
function GetWaitListHistory(strFilter){

	var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


	var table = $('#waitlisthistory').dataTable({
		"bStateSave": true,
		"iStateDuration": -1,
		"fnStateLoadParams": function (oSettings, oData) {

		},
		"fnStateSaveParams": function (oSettings, oData) {

		},
		"bProcessing": true,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bLengthChange": false,
		"iDisplayLength": 20,
		"bAutoWidth": false,
		"bSortable": false,
		"sAjaxDataProp": "WaitList",
		"aoColumns": [
			{"bSortable": true},
			{"bSortable": false},
			{"bSortable": false},
		],
		"sAjaxSource": '/session/sire/models/cfc/waitlistapp.cfc?method=GetWaitListHistory&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		"fnRowCallback": function(nRow, aData, iDisplayIndex) {

		},
		"fnDrawCallback": function( oSettings ) {
			if (oSettings._iDisplayStart < 0){
				oSettings._iDisplayStart = 0;
				$('input.paginate_text').val("1");
			}

			/* hide paginate section if table is empty */
			$('#waitlisthistory > tbody').find('tr').each(function(){
				var tdFirst = $(this).find('td:first');
				if(tdFirst.hasClass('dataTables_empty')){
					$('#waitlisthistory_paginate').hide();
					return false;
				}
				else{
					$('#waitlisthistory_paginate').show();
				} 
			});

			$('[data-toggle="popover"]').popover();
		},
		"fnServerData": function (sSource, aoData, fnCallback, oSettings) {

			if (aoData[3].value < 0){
				aoData[3].value = 0;
			}
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
				);
			aoData.push({"name":"inpListId", "value": $("#current-list-id").val()});
			oSettings.jqXHR = $.ajax( {
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			});
		},
		"fnInitComplete":function(oSettings){
			$('[data-toggle="popover"]').popover();
		}
	});
}

/* trigger undo served button */
$('body').on('click', '.btn-undo-served', function(event) {
	var id = $(this).data('waitid');
	
	bootbox.dialog({
		message: 'Are you sure you want to mark this waitlist as waiting?',
		title: 'Waitlist App',
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					UndoServed(id);
				}
			}
		}
	});
});

/* calling undo served function */
function UndoServed (id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=UndoServedWaitList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpWaitListId: id, inpListId: $("#current-list-id").val()},
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				alertMessage("Success", "Waitlist marked as waiting!");
				GetWaitListHistory();
				updateTodayStatistic();
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
		})
		.fail(function(e) {
			alertMessage("Oops!", $.parseJSON(e).MESSAGE);
		});
		
	} catch (e) {
	}
}

/* trigger done button */
$('body').on('click', '.btn-done', function(event) {
	var id = $(this).data('waitid');
	
	bootbox.dialog({
		message: 'Are you sure you want to mark this waitlist as done?',
		title: 'Waitlist App',
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					MarkDone(id);
				}
			}
		}
	});
});

/* calling done function */
function MarkDone (id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=DoneWaitList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpWaitListId: id, inpListId: $("#current-list-id").val()},
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				alertMessage("Success", "Waitlist marked as done!");
				GetWaitListHistory();
				updateTodayStatistic();
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
		})
		.fail(function(e) {
			alertMessage("Oops!", $.parseJSON(e).MESSAGE);
		});
		
	} catch (e) {
	}
}

/* trigger search function for datatable */
$('#search-history').click(function(event){
	event.preventDefault();

	var q = $("#txtKeyword-history").val();
	GetWaitListHistory(q);
});

$('#txtKeyword-history').on('keypress', function(e){
	if(e.keyCode === 13){
		$('#search').trigger('click');
	}
});

/* reset history table when select history tab */
$('#history-tab').click(function(event) {
	GetWaitListHistory();
	updateTodayStatistic();
});