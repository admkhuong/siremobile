$(function() {
	/* init mask for phone input */
	$("#customer-phone").mask("(000)000-0000");
	$("#customer-phone-edit").mask("(000)000-0000");
});

/* bind validate engine */
$("#update-confirmation-text-form, #update-alert-text-form, #update-confirmation-email-form, #update-alert-email-form, #add-waitlist-service-form, #add-waitlist-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});

/* url constance */
var url_update_seting = '/session/sire/models/cfc/waitlistapp.cfc?method=UpdateWaitlistAppSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_setting = '/session/sire/models/cfc/waitlistapp.cfc?method=GetUserWaitlistAppSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_services = '/session/sire/models/cfc/waitlistapp.cfc?method=GetUserWaitlistServices&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_delete_service = '/session/sire/models/cfc/waitlistapp.cfc?method=DeleteUserWaitlistAppService&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_add_service = '/session/sire/models/cfc/waitlistapp.cfc?method=CreateNewWaitlistAppService&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_list_by_id = '/session/sire/models/cfc/waitlistapp.cfc?method=GetListById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_update_capture_batch_id = '/session/sire/models/cfc/waitlistapp.cfc?method=UpdateCaptureBatchId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';

var userId = $('#user-id').val();

$(document).ready(function($) {
	/* bind input fields with countdown function */
	$('a.label-as-quote').click(function(event) {
		if(parseInt($(this).parents('.waitlistapp-content').find('label > a').text()) >= $(this).text().length+1){
			$(this).parents('.waitlistapp-content').find('textarea').val($(this).parents('.waitlistapp-content').find('textarea').val() + " " + $(this).text());
			count_down_character_by_id('confirm-text-countdown','confirmation-text-content', 160);
			count_down_character_by_id('alert-text-countdown','alert-text-content', 160);
			count_down_character_by_id('notice-text-countdown','notice-text-content', 160);
			count_down_character_by_id('confirm-email-sub-countdown','confirmation-email-subject', 255);
			count_down_character_by_id('confirm-email-mess-countdown','confirmation-email-message', 1000);
			count_down_character_by_id('alert-email-sub-countdown','alert-email-subject', 255);
			count_down_character_by_id('alert-email-mess-countdown','alert-email-message', 1000);
			count_down_character_by_id('notice-email-sub-countdown','notice-email-subject', 255);
			count_down_character_by_id('notice-email-mess-countdown','notice-email-message', 1000);
		}
		
	});
});


$(document).ready(function(){
	/* when document ready, init waitlist setting and services, setup create or edit campaign button */
	init_waitlist_app_setting_detail();
	init_waitlist_app_services();
	init_create_edit_campaign_button();
});

$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});


/* trigger create new service button */
$('#add-new-service').on('hidden.bs.modal', function () {
	$('#new-service-name').val('');
	$('#new-service-description').val('');
	$('#add-waitlist-service-form').validationEngine('hide');
});

/* trigger create new waitlist item button */
$('#add-new-waitlist').on('hidden.bs.modal', function () {
	document.getElementById("add-waitlist-form").reset();
	$('#add-waitlist-form').validationEngine('hide');
});


/* trigger save service button */
$('#save-service-btn').click(validate_add_waitlist_service_form);

/* trigger cancel save service button */
$('#cancel-service-btn').click(function(){
	$('#add-new-service').modal('hide');
});

/* trigger setting fields submit button */
$("#update-confirmation-text-form").submit(validate_confirm_text_form);
$("#update-alert-text-form").submit(validate_alert_text_form);
$("#update-notice-text-form").submit(validate_notice_text_form);
$('#update-confirmation-email-form').submit(validate_confirmation_email_form);
$('#update-alert-email-form').submit(validate_alert_email_form);
$('#update-notice-email-form').submit(validate_notice_email_form);

/* trigger reset defautl fields */
$('#reset-confirmation-text').click(function(event) {
	event.preventDefault();
	var confirmText = 'Hi [name]! You’re waitlisted to [company] as #[order] in line.\nPlease go to [link] for your waitlist’s details. Click on “Cancel Visit” if you can’t make it.';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpConfirmationText: confirmText,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#confirmation-text-content').val(confirmText);
			count_down_character_by_id('confirm-text-countdown','confirmation-text-content', 160);
		}
	});
});

$('#reset-alert-text').click(function(event) {
	event.preventDefault();
	var alertText = 'Hi [name]! It’s your turn at [company]. Please reply within 5 min. In case you can’t make it, go to [link] to cancel.';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpAlertText: alertText,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#alert-text-content').val(alertText);
			count_down_character_by_id('alert-text-countdown','alert-text-content', 160);
		}
	});
});

$('#reset-notice-text').click(function(event) {
	event.preventDefault();
	var noticeText = 'Hi [name]! Your waiting time at [company] remain [quote]. Go to [link] for more details.';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpNoticeText: noticeText,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#notice-text-content').val(noticeText);
			count_down_character_by_id('notice-text-countdown','notice-text-content', 160);
		}
	});
});

$('#reset-confirmation-email').click(function(event) {
	event.preventDefault();
	confirmEmailSub = 'You have been waitlisted to [company] on [date].';
	confirmEmailMess = 'Hi [name]!\nYou’ve been waitlisted to [company] on [date] as #[order] in line. Please go to [link] to view your waitlist’s details and cancel visit if you can’t make it.\nWe look forward to serve you soon!\nSincerely,\n[company]';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpConfirmationEmailSubject: confirmEmailSub,
			inpConfirmationEmailMessage: confirmEmailMess,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#confirmation-email-subject').val(confirmEmailSub);
			$('#confirmation-email-message').val(confirmEmailMess);
			count_down_character_by_id('confirm-email-sub-countdown','confirmation-email-subject', 255);
			count_down_character_by_id('confirm-email-mess-countdown','confirmation-email-message', 1000);
		}
	});
});

$('#reset-alert-email').click(function(event) {
	event.preventDefault();
	alertEmailSub = 'Great! It’s your turn at [company].';
	alertEmailMess = 'Hi [name]!\nYou’re waitlisted to [company] as #[order] in line.\nPlease go to [link] for your waitlist’s details. Click on “Cancel Visit” if you can’t make it.\nSincerely,\n[company]';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpAlertEmailSubject: alertEmailSub,
			inpAlertEmailMessage: alertEmailMess,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#alert-email-subject').val(alertEmailSub);
			$('#alert-email-message').val(alertEmailMess);
			count_down_character_by_id('alert-email-sub-countdown','alert-email-subject', 255);
			count_down_character_by_id('alert-email-mess-countdown','alert-email-message', 1000);
		}
	});
});

$('#reset-notice-email').click(function(event) {
	event.preventDefault();
	noticeEmailSub = 'Your waiting time at [company] has changed.';
	noticeEmailMess = 'Hi [name]!\nYour waiting time at [company] remain [quote]. Please go to [link] for more details.\nWe look forward to serve you soon!\nSincerely,\n[company]';
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpNoticeEmailSubject: noticeEmailSub,
			inpNoticeEmailMessage: noticeEmailMess,
			inpListId: $("#current-list-id").val()
		},
		success: function(data){
			$('#notice-email-subject').val(noticeEmailSub);
			$('#notice-email-message').val(noticeEmailMess);
			count_down_character_by_id('notice-email-sub-countdown','notice-email-subject', 255);
			count_down_character_by_id('notice-email-mess-countdown','notice-email-message', 1000);
		}
	});
});


/* trigger enable/disable button */
$('#confirm-text-status').click(function(event) {
	var confirmTextSta;
	if ($(this).prop('checked') == true){
		confirmTextSta = 1;
		$('#update-confirmation-text-form').show('fast');
	}
		
	else{
		confirmTextSta = 0;
		$('#update-confirmation-text-form').hide('fast');
	}
		
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpConfirmationTextStatus: confirmTextSta, inpListId: $("#current-list-id").val()
		},
	});
});

$('#alert-text-status').click(function(event) {
	var alertTextSta;
	if ($(this).prop('checked') == true){
		alertTextSta = 1;
		$('#update-alert-text-form').show('fast');
	}
	else{
		alertTextSta = 0;
		$('#update-alert-text-form').hide('fast');
	}

	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpAlertTextStatus: alertTextSta, inpListId: $("#current-list-id").val()
		},
	});
});

$('#notice-text-status').click(function(event) {
	var noticeTextSta;
	if ($(this).prop('checked') == true){
		noticeTextSta = 1;
		$('#update-notice-text-form').show('fast');
	}
	else{
		noticeTextSta = 0;
		$('#update-notice-text-form').hide('fast');
	}

	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpNoticeTextStatus: noticeTextSta, inpListId: $("#current-list-id").val()
		},
	});
});

$('#confirm-email-status').click(function(event) {
	var confirmEmailSta;
	if ($(this).prop('checked') == true){
		confirmEmailSta = 1;
		$('#update-confirmation-email-form').show('fast');
	}
	else{
		confirmEmailSta = 0;
		$('#update-confirmation-email-form').hide('fast');
	}
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpConfirmationEmailStatus: confirmEmailSta,
			inpListId: $("#current-list-id").val()
		},
	});
});

$('#alert-email-status').click(function(event) {
	var alertEmailSta;
	if ($(this).prop('checked') == true){
		alertEmailSta = 1;
		$('#update-alert-email-form').show('fast');
	}
	else{
		alertEmailSta = 0;
		$('#update-alert-email-form').hide('fast');
	}
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpAlertEmailStatus: alertEmailSta,
			inpListId: $("#current-list-id").val()
		},
	});
});

$('#notice-email-status').click(function(event) {
	var noticeEmailSta;
	if ($(this).prop('checked') == true){
		noticeEmailSta = 1;
		$('#update-notice-email-form').show('fast');
	}
	else{
		noticeEmailSta = 0;
		$('#update-notice-email-form').hide('fast');
	}
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpNoticeEmailStatus: noticeEmailSta,
			inpListId: $("#current-list-id").val()
		},
	});
});

$('#add-service-status').click(function(event) {
	var addServiceSta;
	if ($(this).prop('checked') == true){
		addServiceSta = 1;
		$('#add-service').show('fast');
		$('#add-service-btn').show('fast');
	}
	else{
		addServiceSta = 0;
		$('#add-service').hide('fast');
		$('#add-service-btn').hide('fast');
	}
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpAddServiceStatus: addServiceSta,
			inpListId: $("#current-list-id").val()
		},
	});
});

$('#quote-wait-status').click(function(event) {
	var quoteWaitSta;
	if ($(this).prop('checked') == true){
		quoteWaitSta = 1;
		$("#quote-time-intervals").show('fast');
		$('#quotetime-label').show('fast');
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {inpQuoteWait: $("#quote-time-intervals option:selected").val(), inpListId: $("#current-list-id").val()},
		});
		
	}
	else{
		quoteWaitSta = 0;
		$("#quote-time-intervals").hide('fast');
		$('#quotetime-label').hide('fast');
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {inpQuoteWait: 0, inpListId: $("#current-list-id").val()},
		});
		
	}
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpQuoteWaitStatus: quoteWaitSta, inpListId: $("#current-list-id").val()
		},
	});
});

/* trigger select setting fields */
$('#max-party-size').change(function(event) {
	
	var maxPartySize = $('#max-party-size').val();
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpMaxPartySize: maxPartySize, inpListId: $("#current-list-id").val()
		},		
	});
	
});

$('#quote-time-intervals').change(function(event) {
	
	var quoteTimeIntervals = $('#quote-time-intervals').val();
	$.ajax({
		url: url_update_seting,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpQuoteWait: quoteTimeIntervals, inpListId: $("#current-list-id").val()
		},		
	});
	
});

/* calling update setting functions */
function validate_confirm_text_form(event){
	if ($('#update-confirmation-text-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var confirmationText = $('#confirmation-text-content').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpConfirmationText: confirmationText, inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
	}
	
}

function validate_alert_text_form(event){
	if ($('#update-alert-text-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var alertText = $('#alert-text-content').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpAlertText: alertText, inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
	}
	
}

function validate_notice_text_form(event){
	if ($('#update-notice-text-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var noticeText = $('#notice-text-content').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpNoticeText: noticeText, inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
	}
}

function validate_confirmation_email_form(event){
	if ($('#update-confirmation-email-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var confirmationEmailSubject = $('#confirmation-email-subject').val();
		var confirmationEmailMessage = $('#confirmation-email-message').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpConfirmationEmailSubject: confirmationEmailSubject,
				inpConfirmationEmailMessage: confirmationEmailMessage,
				inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
		
	}
}

function validate_alert_email_form(event){
	if ($('#update-alert-email-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var alertEmailSubject = $('#alert-email-subject').val();
		var alertEmailMessage = $('#alert-email-message').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpAlertEmailSubject: alertEmailSubject,
				inpAlertEmailMessage: alertEmailMessage,
				inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
		
	}
}

function validate_notice_email_form(event){
	if ($('#update-notice-email-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var noticeEmailSubject = $('#notice-email-subject').val();
		var noticeEmailMessage = $('#notice-email-message').val();
		$.ajax({
			url: url_update_seting,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpNoticeEmailSubject: noticeEmailSubject,
				inpNoticeEmailMessage: noticeEmailMessage,
				inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				bootbox.dialog({
				    message: data.MESSAGE,
				    title: 'Waitlist App Setting',
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
			}
		});
	}
}


function validate_add_waitlist_service_form(event){
	if ($('#add-waitlist-service-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var newServiceName = $('#new-service-name').val();
		var newServiceDescription = $('#new-service-description').val();
		$.ajax({
			url: url_add_service,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpServiceName: newServiceName,
				inpServiceDescription: newServiceDescription,
				inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				if (data.RXRESULTCODE > 0) {
					bootbox.dialog({
					    message: data.MESSAGE,
					    title: 'Waitlist App Setting',
					    buttons: {
					        success: {
					            label: "OK",
					            className: "btn btn-success btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
					$('#add-new-service').modal('hide');
					$('#add-service').html('');
					init_waitlist_app_services();
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
			}
		});
		
	}
}

/* init setting options */
function init_waitlist_app_setting_detail(){
	$.ajax({
		url: url_get_setting,
		type: 'GET',
		dataType: 'JSON',
		data: {inpListId: $("#current-list-id").val()},
		success: function(data){
			$('#max-party-size').val(data.MAXSIZE);
			$('#confirmation-text-content').val(data.CONFIRMTEXT);
			$('#alert-text-content').val(data.ALERTTEXT);
			$('#notice-text-content').val(data.NOTICETEXT);
			$('#confirmation-email-subject').val(data.CONFIRMEMAILSUB);
			$('#confirmation-email-message').val(data.CONFIRMEMAILMESS);
			$('#alert-email-subject').val(data.ALERTEMAILSUB);
			$('#alert-email-message').val(data.ALERTEMAILMESS);
			$('#notice-email-subject').val(data.NOTICEEMAILSUB);
			$('#notice-email-message').val(data.NOTICEEMAILMESS);

			if (data.CONFIRMTEXTSTA == 1){
				$('#confirm-text-status').prop('checked', true);
				$('#update-confirmation-text-form').show();
			}

			else{
				$('#confirm-text-status').prop('checked', false);
				$('#update-confirmation-text-form').hide();
			}

			$('#quote-time-intervals > option').each(function(){
				if($(this).attr('value') == data.QUOTETIME)
					$(this).prop('selected', true);
				else
					$(this).prop('selected', false);
			});

			if (data.ALERTTEXTSTA == 1){
				$('#alert-text-status').prop('checked', true);
				$('#update-alert-text-form').show();

			}
			else{
				$('#alert-text-status').prop('checked', false);
				$('#update-alert-text-form').hide();
			}

			if (data.NOTICETEXTSTA == 1){
				$('#notice-text-status').prop('checked', true);
				$('#update-notice-text-form').show();
			}
			else{
				$('#notice-text-status').prop('checked', false);
				$('#update-notice-text-form').hide();
			}


			if (data.CONFIRMEMAILSTA == 1){
				$('#confirm-email-status').prop('checked', true);
				$('#update-confirmation-email-form').show();
			}
			else{
				$('#confirm-email-status').prop('checked', false);
				$('#update-confirmation-email-form').hide();
			}


			if (data.ALERTEMAILSTA == 1){
				$('#alert-email-status').prop('checked', true);
				$('#update-alert-email-form').show();
			}
			else{
				$('#alert-email-status').prop('checked', false);
				$('#update-alert-email-form').hide();
			}

			if (data.NOTICEEMAILSTA == 1){
				$('#notice-email-status').prop('checked', true);
				$('#update-notice-email-form').show();
			}
			else{
				$('#notice-email-status').prop('checked', false);
				$('#update-notice-email-form').hide();
			}


			if (data.QUOTEWAITSTA == 1){
				$('#quote-wait-status').prop('checked', true);
				$('#quote-time-intervals').show();
				$('#quotetime-label').show();
			}
			else{
				$('#quote-wait-status').prop('checked', false);
				$('#quote-time-intervals').hide();
				$('#quotetime-label').hide();
			}

			if (data.ADDSERVICESTA == 1){
				$('#add-service-status').prop('checked', true);
				$('#add-service').show();
				$('#add-service-btn').show();
			}
			else{
				$('#add-service-status').prop('checked', false);
				$('#add-service').hide();
				$('#add-service-btn').hide();
			}

			// $('a#confirm-text-countdown').text(1000-$('#confirmation-text-content').val().length);

			count_down_character_by_id('confirm-text-countdown','confirmation-text-content', 160);
			count_down_character_by_id('alert-text-countdown','alert-text-content', 160);
			count_down_character_by_id('alert-text-countdown','notice-text-content', 160);
			count_down_character_by_id('confirm-email-sub-countdown','confirmation-email-subject', 255);
			count_down_character_by_id('confirm-email-mess-countdown','confirmation-email-message', 1000);
			count_down_character_by_id('alert-email-sub-countdown','alert-email-subject', 255);
			count_down_character_by_id('alert-email-mess-countdown','alert-email-message', 1000);
			count_down_character_by_id('notice-email-sub-countdown','notice-email-subject', 255);
			count_down_character_by_id('notice-email-mess-countdown','notice-email-message', 1000);
		}

	});
	
}

/* init waitlist services */
function init_waitlist_app_services(){
	$.ajax({
		url: url_get_services,
		type: 'GET',
		dataType: 'JSON',
		data: {inpListId: $("#current-list-id").val()},
		success: function(data){
			$('#add-service').html('');
			for (var i = 0; i < data.services.length; i++) {
				$('#add-service').append('<div><form class="row" name="service-detail" id="'+ data.services[i][0] +'"><div class="form-group col-lg-5 col-md-5 col-sm-5"><label for="service" id="service-label">Service</label><input type="hidden" value="'+ data.services[i][0] +'"><input disabled type="text" class="form-control validate[required,custom[noHTML]]" id="service-name" placeholder="" name="service-name" value="'+ data.services[i][1] + '"></div><div class="form-group col-lg-5 col-md-5 col-sm-5"><label for="service-description" id="service-description-label">Description</label><textarea disabled class="form-control no-resize validate[required,custom[noHTML]]" id="service-description" name="service-description" placeholder="" rows="4" data-prompt-position="topLeft:100">'+ data.services[i][2] +'</textarea></div><div class="form-group"><a id="delete-service"><span class="glyphicon glyphicon-trash" id="delete-service-icon"></span></a></div></form></div>');
			}
		}
	});
	
}

/* trigger delete service button */
$("#add-service").on('click','#delete-service-icon',function(){
	var deleteServiceId = $(this).parents("[name='service-detail']").attr('id');

	bootbox.confirm({
		title: "Waitlist App Setting",
	    message: "Do you want to delete this service?",
	    buttons: {
	        confirm: {
	            label: 'Yes',
	            className: 'btn-success-custom yes-delete-service pull-right'
	        },
	        cancel: {
	            label: 'No',
	            className: 'btn-back-custom no-delete-service pull-right'
	        }
	    },
	    callback: function (result) {
	    	if (result == true){
	    		$.ajax({
			    	url: url_delete_service,
			    	type: 'POST',
			    	dataType: 'JSON',
			    	data: {
			    		inpServiceId: deleteServiceId,
			    		inpListId: $("#current-list-id").val()
			    	},
			    	beforeSend:function( xhr ) {
						$('#processingPayment').show();
					},
					success: function(data){
						$('#processingPayment').hide();
						bootbox.dialog({
						    message: data.MESSAGE,
						    title: 'Waitlist App Setting',
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						$('#delete-service-modal').modal('hide');
						$('#add-service').html('');
						init_waitlist_app_services();
					}
			    });
	    	}
	        
	    }
	});
});


/* countdown function, display countdown number and prevent user to exceed a limit number of character */
function count_down_character_by_id(countId, contentId, limit){
	$('#' + countId).text(limit-$('#'+ contentId).val().length);

	$('#' + contentId).on('keyup', function(){
		$('#' + countId).text(limit-$('#' + contentId).val().length);
	});

	var contentText = document.getElementById(contentId);
	if(contentText) {
	    contentText.addEventListener("input", function() {
	        if (contentText.value.length == limit) {
	        	return false;
	        } 
	        else if (contentText.value.length > limit) {
	        	contentText.value = contentText.value.substring(0, limit);
	        }
	    },  false);
	}

	$('#' + contentId).bind('paste', function(e) {
	    var elem = $(this);
	    setTimeout(function() {
	        var text = elem.val();
	        if (text.length > limit) {
				elem.val(text.substring(0,limit));
	        }
	        $('#' + countId).text(limit-parseInt(elem.val().length));
	    }, 100);
	});
}

/* init create or edit campaign button */
function init_create_edit_campaign_button(){
	var listId = $('#current-list-id').val();
	if ($('#edit-campaign-link').length){
		$('#edit-campaign-link').remove();
	}

	$.ajax({
		url: url_get_list_by_id,
		type: 'GET',
		dataType: 'JSON',
		data: {
			inpListId: listId
		},
		success: function(data){
			if(data.RXRESULTCODE == 1){
				if(data.CAPTUREBATCHID > 0){
					// $('#edit-campaign-link').show();
					$('#create-campaign-link').hide();
					$('#create-edit-campaign').append('<a id="edit-campaign-link" href="/session/sire/pages/campaign-edit?campaignid='+ data.CAPTUREBATCHID + '" class="btn btn-success-custom">Edit Campaign</a>')
				}
				else{
					$('#create-campaign-link').show();
				}
			}
		}
	});
	
}

// $('#waitlist-select').on('change', function(){
// 	init_create_edit_campaign_button();
// });

function update_capture_batch(){
	var listId = $('#current-list-id').val();
	var dafaultTemplateId = $('#dafault-template-id').text();
	$.ajax({
		url: url_update_capture_batch_id,
		type: 'POST',
		dataType: 'JSON',
		data: {
			inpListId: listId,
			inpTemplateId: parseInt(dafaultTemplateId)
		},
		beforeSend: function( xhr ) {
	    	$('#processingPayment').show();
	    },       
	    error: function(XMLHttpRequest, textStatus, errorThrown) {
	    	$('#processingPayment').hide();
	     	bootbox.dialog({
	         	message:'Create Campaign Failed',
	         	title: "Waitlist App Setting",
	         	buttons: {
	             	success: {
	                 	label: "Ok",
	                 	className: "btn btn-medium btn-success-custom",
	                 	callback: function() {}
	             	}
	         	}
	     	});
	    }, 
		success: function(data){
			if(data.RXRESULTCODE == 1){
				window.location.href = '/session/sire/pages/campaign-edit?campaignid=' + data.CAPTUREBATCHID;
			}
			else{
				bootbox.dialog({
		           	message:data.MESSAGE,
		           	title: "Waitlist App Setting",
		           	buttons: {
		               	success: {
		                   	label: "Ok",
		                   	className: "btn btn-medium btn-success-custom",
		                   	callback: function() {}
		               	}
		           	}
		       	});
			}	
		}
	});
	
}

$('#create-campaign-link').click(function(event){
	update_capture_batch();
});


