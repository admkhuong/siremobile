function isNumber(n) 
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}

/* function to display message */
function alertMessage(title, message) {
	$('#processingPayment').hide();
	$('div[role="tooltip"]').hide();
	$('.modal-backdrop').hide();
	message = message !== '' ? message : "Sorry, we have some issue at the moment, please try again later";
	title = title !== '' ? title : "Oops!";
	bootbox.dialog({
		message: message,
		title: title,
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					$('#processingPayment').hide();
					$('div[role="tooltip"]').hide();
					$('.modal-backdrop').hide();
				}
			}
		}
	});
	return false;
}

/* update today statistic section */
function updateTodayStatistic() {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=GetTodayStatistic&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpListId: $("#current-list-id").val()}
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				$(".span-served-today").text(data.STATUS['served']);
				$(".span-noshow-today").text(data.STATUS['noshow']);
				$(".span-texted-today").text(data.STATUS['texted']);
				$(".span-waitlisted-today").text(data.STATUS['waitlisted']);
			}
		})
		.fail(function(e) {

		});
		
	} catch (e) {
	}
}

/* countdown character for input fields, prevent user from enter more than a limit number of character */
function count_down_character_by_id(countId, contentId, limit){
	$('#' + countId).text(limit-$('#'+ contentId).val().length);

	$('#' + contentId).on('keyup', function(){
		$('#' + countId).text(limit-$('#' + contentId).val().length);
	});

	var contentText = document.getElementById(contentId);
	if(contentText) {
	    contentText.addEventListener("input", function() {
	        if (contentText.value.length == limit) {
	        	return false;
	        } 
	        else if (contentText.value.length > limit) {
	        	contentText.value = contentText.value.substring(0, limit);
	        }
	    },  false);
	}

	$('#' + contentId).bind('paste', function(e) {
	    var elem = $(this);
	    setTimeout(function() {
	        var text = elem.val();
	        if (text.length > limit) {
				elem.val(text.substring(0,limit));
	        }
	        $('#' + countId).text(limit-parseInt(elem.val().length));
	    }, 100);
	});
}

/* when user select a waitlist from dropdown menu, reset waitlist app datatable by this waitlist */
$('body').on('change', '#waitlist-select', function(event) {
	var listid = $('#waitlist-select option:selected').data('listid');

	getWaitListById(listid);

	$('#current-list-id').val(listid);
	$("#waiting-tab").trigger('click');

	var newUrl = "session/sire/pages/wait-list-app?listid="+listid

	window.history.pushState("object or string", "Title", "/" + newUrl );
});

function refineUrl()
{
    var url = window.location.href;
    var value = url.substring(0, url.lastIndexOf('?'));
    return value;
}

/* check if input waitlist still exist, if not redirect user to waitlist manage page */
function getWaitListById (id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistmanage.cfc?method=GetWaitListById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpListId: id},
			async: false,
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) < 0) {
				window.location.href = "/session/sire/pages/wait-list-manage";
			}	
		});
		
	} catch (e) {

	}
}

$(document).ready(function() {
	$(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });
});