(function($){
	function isNumber(n) 
	{
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	/* function for display message */
	function alertMessage(title, message) {
		$('#processingPayment').hide();
		$('.modal-backdrop').hide();
		message = message !== '' ? message : "Sorry, we have some issue at the moment, please try again later";
		title = title !== '' ? title : "Oops!";
		bootbox.dialog({
			message: message,
			title: title,
			buttons: {
				success: {
					label: "OK",
					className: "btn btn-medium btn-success-custom",
					callback: function() {
					}
				}
			}
		});
		$("body").removeAttr("style");
		return false;
	}

	/* Countdown character of a input field and prevent user enter more than a limit number of character */
	function CountdownInputById(countId, contentId, limit){
		$('#' + countId).text(limit-$('#'+ contentId).val().length);

		$('#' + contentId).on('keyup', function(){
			$('#' + countId).text(limit-$('#' + contentId).val().length);
		});

		var contentText = document.getElementById(contentId);
		if(contentText) {
		    contentText.addEventListener("input", function() {
		        if (contentText.value.length == limit) {
		        	return false;
		        } 
		        else if (contentText.value.length > limit) {
		        	contentText.value = contentText.value.substring(0, limit);
		        }
		    },  false);
		}

		$('#' + contentId).bind('paste', function(e) {
		    var elem = $(this);
		    setTimeout(function() {
		        var text = elem.val();
		        if (text.length > limit) {
					elem.val(text.substring(0,limit));
		        }
		        $('#' + countId).text(limit-parseInt(elem.val().length));
		    }, 100);
		});
	}

	/* Waitlist datatable initializer */
	function GetWaitList(strFilter){

		var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


		var table = $('#waitlist').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {

			},
			"fnStateSaveParams": function (oSettings, oData) {

			},
			"bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
			"bLengthChange": false,
			"iDisplayLength": 20,
			"bAutoWidth": false,
			"bSortable": false,
			"sAjaxDataProp": "WaitList",
			"aoColumns": [
				{"bSortable": false},
				{"bSortable": true},
				{"bSortable": true},
				{"bSortable": false, "sClass" : "action"},
				{"bSortable": false},
			],
			"sAjaxSource": '/session/sire/models/cfc/waitlistmanage.cfc?method=GetWaitListTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {
				var row = $(nRow);
				var settingButton  = $('<a></a>').attr('href', 'wait-list-app?com=setting&listid='+aData[5].LISTID)
												 .attr('data-toggle', 'popover')
												 .attr('data-placement', 'top')
												 .attr('data-trigger', 'hover')
												 .attr('data-content', 'Setting')
												 .css('margin-left', '5px')
											     .addClass('btn btn-success-custom')
												 .html('<i class="glyphicon glyphicon-cog"></i>');
				row.find('td.action').append(settingButton);
			},
			"fnDrawCallback": function( oSettings ) {
				if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}

				/* hide paginate section if table is empty */
				$('#waitlist > tbody').find('tr').each(function(){
					var tdFirst = $(this).find('td:first');
					if(tdFirst.hasClass('dataTables_empty')){
						$('#waitlist_paginate').hide();
						return false;
					}
					else{
						$('#waitlist_paginate').show();
					} 
				});

				$('[data-toggle="popover"]').popover();
			},
			"fnServerData": function (sSource, aoData, fnCallback, oSettings) {

				if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
				aoData.push(
					{ "name": "customFilter", "value": customFilterData}
					);

				oSettings.jqXHR = $.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": fnCallback
				});
			},
			"fnInitComplete":function(oSettings){
				$('[data-toggle="popover"]').popover();
			}
		});
	}

	/* datatable search function */
	$('#search').click(function(event){
		event.preventDefault();

		var q = $("#txtKeyword").val();
		GetWaitList(q);
	});

	$('#txtKeyword').on('keypress', function(e){
		if(e.keyCode === 13){
			$('#search').trigger('click');
		}
	});

	/* calling create new waitlist function */
	function createNewWaitlist(listName) {
		try {
			$.ajax({
				url: '/session/sire/models/cfc/waitlistmanage.cfc?method=CreateNewWaitlist&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpListName: listName},
				beforeSend: function () {
					$('#processingPayment').show();
				}
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					alertMessage('Waitlist', data.MESSAGE);
					$("#new-waitlist-name").val("");
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
				GetWaitList();
			})
			.fail(function(e) {
				alertMessage("Oops!", e.MESSAGE);
			});
			
		} catch (e) {

		} 
	}

	/* validate and trigger create waitlist button */
	$('#new-waitlist-name').validationEngine('attach', {promptPosition : "topLeft", scroll: false});
	$("#add-waitlist").click(function(event) {
		if ($('#new-waitlist-name').validationEngine('validate')){
			var name = $("#new-waitlist-name").val();
			createNewWaitlist(name);
		};
	});

	/* calling edit waitlist function */
	function editWaitlistName(id,name) {
		try {
			$.ajax({
				url: '/session/sire/models/cfc/waitlistmanage.cfc?method=UpdateWaitList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpListId: id, inpListName: name},
				beforeSend: function () {
					$('#processingPayment').show();
				}
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					alertMessage('Waitlist', data.MESSAGE);
					$("#edit-waitlist-name").val("");
					$("#edit-waitlist-modal").modal("hide");
				} else {
					alertMessage("Oops!", data.MESSAGE);
					$("#edit-waitlist-input").val($("#edit-waitlist-input").data('listname'));
					$('.modal-backdrop').show();
				}
				GetWaitList();
			})
			.fail(function(e) {
				alertMessage("Oops!", e.MESSAGE);
			});
		} catch (e) {

		}
		return 0;
	}

	/* validate and trigger edit waitlist button */
	$('#edit-waitlist-input').validationEngine('attach', {promptPosition : "topLeft", scroll: false});

	$('body').on('click', '#btn-edit-confirm', function() {
		var id = $("#edit-waitlist-input").data('listid');
		if ($('#edit-waitlist-input').validationEngine('validate')){
			var name = $("#edit-waitlist-input").val();
			editWaitlistName(id,name);
		};
	});

	/* trigger edit waitlist button */
	$('body').on('click', '.btn-edit', function(event) {
		var data = $(this).data();
		var inputEdit = $('#edit-waitlist-input');
		inputEdit.data('listid', data['listid']);
		inputEdit.data('listname', data['listname']);
		inputEdit.val(data['listname']);

		$("#edit-waitlist-modal").modal("show");
		inputEdit.focus();

		$("#edit-name-countdown").text(50-parseInt(data['listname'].length));
	});

	/* calling remove waitlist function */
	function removeWaitlistName(id) {
		try {
			$.ajax({
				url: '/session/sire/models/cfc/waitlistmanage.cfc?method=DeleteWaitList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpListId: id},
				beforeSend: function () {
					$('#processingPayment').show();
				}
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					alertMessage('Waitlist', data.MESSAGE);
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
				GetWaitList();
			})
			.fail(function(e) {
				alertMessage("Oops!", e.MESSAGE);
			});
		} catch (e) {

		}
		return 0;
	}

	/* trigger delete waitlist button */
	$('body').on('click', '.btn-remove', function(event) {
		var id = $(this).data('listid');

		bootbox.dialog({
			message: 'Are you sure you want to remove this waitlist?',
			title: 'Waitlist',
			buttons: {
				calcel: {
					label: "Cancel",
					className: "btn btn-medium btn-back-custom",
					callback: function() {
					}
				},
				success: {
					label: "Confirm",
					className: "btn btn-medium btn-success-custom",
					callback: function() {
						removeWaitlistName(id);
					}
				}
			}
		});
	});

	/* calling set waitlist to default function */
	function setDefaultWaitlist(id) {
		try {
			$.ajax({
				url: '/session/sire/models/cfc/waitlistmanage.cfc?method=SetDefaultList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpListId: id},
				beforeSend: function () {
					$('#processingPayment').show();
				}
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					alertMessage('Waitlist', data.MESSAGE);
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
				GetWaitList();
			})
			.fail(function(e) {
				alertMessage("Oops!", e.MESSAGE);
			});
		} catch (e) {

		}
		return 0;
	}

	/* trigger set waitlist to default button */
	$('body').on('click', '.btn-set-default', function(event) {
		var id = $(this).data('listid');

		bootbox.dialog({
			message: 'Are you sure you want to set this waitlist as default?',
			title: 'Waitlist',
			buttons: {
				cancel: {
					label: "Cancel",
					className: "btn btn-medium btn-back-custom",
					callback: function() {
					}
				},
				success: {
					label: "Confirm",
					className: "btn btn-medium btn-success-custom",
					callback: function() {
						setDefaultWaitlist(id);
					}
				}
			}
		});
	});

	$(document).ready(function() {
		/* when document is ready */

		/* init waitlist datatable */
		GetWaitList();

		/* bind input fields with countdown function */
		CountdownInputById('name-countdown','new-waitlist-name',50);
		CountdownInputById('edit-name-countdown','edit-waitlist-input',50);

		/* fix body shrink issue after a message model disappear */
		$(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });
	});

})(jQuery);