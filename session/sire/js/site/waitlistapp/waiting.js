/* bind validate engine for edit waitlist form */
$("#edit-waitlist-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});

$("#add-waitlist-service-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});

/* url constance */
var url_get_waitlist_detail = '/session/sire/models/cfc/waitlistapp.cfc?method=GetWaitlistById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_setting = '/session/sire/models/cfc/waitlistapp.cfc?method=GetUserWaitlistAppSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_services = '/session/sire/models/cfc/waitlistapp.cfc?method=GetUserWaitlistServices&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_update_waitlist = '/session/sire/models/cfc/waitlistapp.cfc?method=UpdateCustomerWaitlist&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_create_waitlist = '/session/sire/models/cfc/waitlistapp.cfc?method=CreateNewWaitlist&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
//get waiting customer by UserId
var url_get_waiting_customer_by_userid = '/session/sire/models/cfc/waitlistapp.cfc?method=GetWaitingCustomerByUserId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_get_waiting_customer_by_waitlistid = '/session/sire/models/cfc/waitlistapp.cfc?method=GetWaitingCustomerById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var url_add_service = '/session/sire/models/cfc/waitlistapp.cfc?method=CreateNewWaitlistAppService&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
var addServiceStatus;

var timerInterval = new Array();
var serverTimeInterval;

$('#confirm-edit-waitlist-btn').click(validate_edit_waitlist_form);
$('#confirm-add-waitlist-btn').click(validate_add_waitlist_form);

var waitlist_table;



// Add new method to Number object==========================
// Number.prototype.toWaitTimeString = function()
// {
// 	var days    = Math.floor(this / (3600*24));
//     var hours   = Math.floor(this / 3600);
//     var minutes = Math.floor((this - (hours * 3600)) / 60);
//     var seconds = this - (hours * 3600) - (minutes * 60);

//     days = (days > 0 ? days + "d " : "");
//     hours = (hours > 0 ? hours + "h " : "");
//     minutes = (minutes > 0 ? minutes + "m " : "");
//     seconds = (seconds > 0 ? seconds + "s" : "");
//     return hours+minutes+seconds;
// }
//==========================================================


//Add new method to jQuery object to bind to each span label timer 
// $.fn.showWaitedTimer = function(){
// 	var self = this;
// 	var createdTime = Date.parse(this.data('created') ? this.data('created') : "");
// 	var nowTime 	= Date.parse(this.data('now') ? this.data('now') : "");
// 	var offsetTime  = (nowTime.getTime() - createdTime.getTime())/1000;
// 	setInterval(function(){
// 		offsetTime += 1;
// 		self.html(offsetTime.toWaitTimeString());
// 	}, 1000);
// }
//==============================================================



/* Waitlist datatable initializer */
function GetWaitList(strFilter){

	var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";


	waitlist_table = $('#waitlist').dataTable({
		"iStateDuration": -1,
		"fnStateLoadParams": function (oSettings, oData) {

		},
		"fnStateSaveParams": function (oSettings, oData) {

		},
		"bProcessing": false,
		"bFilter": false,
		"bServerSide":true,
		"bDestroy":true,
		"sPaginationType": "input",
		"bStateSave": false,
		"bLengthChange": false,
		"iDisplayLength": 20,
		"bAutoWidth": false,
		"bSortable": false,
		"sAjaxDataProp": "WaitList",
		"aoColumns": [
			{"bSortable": true},
			{"bSortable": false},
			{"bSortable": false},
		],
		"sAjaxSource": '/session/sire/models/cfc/waitlistapp.cfc?method=GetWaitList&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		"fnRowCallback": function(nRow, aData, iDisplayIndex) {

		},
		"fnDrawCallback": function( oSettings ) {
			if (oSettings._iDisplayStart < 0){
				oSettings._iDisplayStart = 0;
				$('input.paginate_text').val("1");
			}

			// hide paginate if table have no record
			$('#waitlist > tbody').find('tr').each(function(){
				var tdFirst = $(this).find('td:first');
				if(tdFirst.hasClass('dataTables_empty')){
					$('#waitlist_paginate').hide();
					return false;
				}
				else{
					$('#waitlist_paginate').show();
				} 
			});

			// init bootstrap popover
			$('[data-toggle="popover"]').popover();

			// reset old timer interval 
			resetTimerInterval();

			// init new interval for timer, use for display waited time for each waitlist item
			if ($('.waited-timer').length > 0) {
				var servertime = $('.waited-timer').first().data('now');
				servertime = Date.parse(servertime, 'yyyy-mm-dd HH:mm:ss');
				serverTimeInterval = setInterval(function() {
				    servertime.setSeconds(servertime.getSeconds() + 1);
				}, 1000);
			}
			$('.waited-timer').each(function(index, el) {
				var interval = setInterval(function() {
				    var timespan = countdown(Date.parse($(el).data('created')), servertime);
				    var timecount = '';
				    if (timespan.years !== 0) {
				    	timecount += timespan.years + 'y';
				    }
				    if (timespan.months !== 0) {
				    	timecount += (timecount !== '' ? ':' : '') + timespan.months + 'm';
				    }
				    if (timespan.days !== 0) {
				    	timecount += (timecount !== '' ? ':' : '') + timespan.days + 'd';
				    }
				    if (timespan.hours !== 0) {
				    	timecount += (timecount !== '' ? ':' : '') + timespan.hours + 'h';
				    }
				    if (timespan.minutes !== 0) {
				    	timecount += (timecount !== '' ? ':' : '') + timespan.minutes + 'm';
				    }
				    if (timespan.value < 0) {
				    	timespan.seconds = 0;
				    }
				    timecount += (timecount !== '' ? ':' : '') + timespan.seconds + 's';
				    $(el).html(timecount) ;
				}, 1000);

				timerInterval.push(interval);
			});

		},
		"fnServerData": function (sSource, aoData, fnCallback, oSettings) {

			if (aoData[3].value < 0){
				aoData[3].value = 0;
			}
			aoData.push(
				{ "name": "customFilter", "value": customFilterData}
				);
			aoData.push({"name":"inpListId", "value": $("#current-list-id").val()});
			oSettings.jqXHR = $.ajax( {
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			});
		},
		"fnInitComplete":function(oSettings){
			$('[data-toggle="popover"]').popover();
		}
	});
}

/* clear old interval, keep browser clean */
function resetTimerInterval() {
	clearInterval(serverTimeInterval);
	for (var i = 0; i < timerInterval.length; i++) {
		clearInterval(timerInterval[i]);
	}
}


/*trigger served button*/
$('body').on('click', '.btn-served', function(event) {
	var id = $(this).data('waitid');
	
	bootbox.dialog({
		message: 'Are you sure you want to mark this waitlist as served?',
		title: 'Waitlist App',
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					MarkServed(id);
				}
			}
		}
	});
});

/*calling function served*/
function MarkServed (id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=ServedWaitList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpWaitListId: id, inpListId: $("#current-list-id").val()},
			beforeSend: function () {
				$('#processingPayment').show();
			}
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				alertMessage("Success", "Waitlist marked as served!");
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
			ReloadDataTable();
			updateTodayStatistic();
		})
		.fail(function(e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		});
		
	} catch (e) {
	}
}

/*trigeer noshow button*/
$('body').on('click', '.btn-noshow', function(event) {
	var id = $(this).data('waitid');

	bootbox.dialog({
		message: 'Are you sure you want to mark this waitlist as no-show?',
		title: 'Waitlist App',
		buttons: {
			success: {
				label: "OK",
				className: "btn btn-medium btn-success-custom",
				callback: function() {
					MarkNoShow(id);
				}
			}
		}
	});
});

/*calling function noshow*/
function MarkNoShow(id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=NoShowWaitList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpWaitListId: id, inpListId: $("#current-list-id").val()},
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				alertMessage("Success", "Waitlist marked as no-show!");
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
			ReloadDataTable();
			updateTodayStatistic();
		})
		.fail(function(e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		});
		
	} catch (e) {
	}
}

/*trigeer send alert button*/
$('body').on('click', '.btn-send-alert', function(event) {
	event.preventDefault();
	var data = $(this).data();

	SendAlert(data['waitid'], data['type']);
});

/*calling function send alert*/
function SendAlert(waitid, type) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=SendAlert&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpWaitListId: waitid, inpType: type, inpListId: $("#current-list-id").val()},
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				alertMessage("Success", "Sent alert successfully!");
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
			ReloadDataTable();
			updateTodayStatistic();
		})
		.fail(function(e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		});
		
	} catch (e) {
	}
}

/*trigger sms conversation button, display conversation window*/
$('body').on('click', '.btn-sms', function(event) {
	var data = $(this).data();
	$('ul.messages').html('');
	$('#sms').modal('show');
	var contactstring = data['contactstring'];
	$('#sms').data('contactstring', contactstring);
	$('#sms').data('waitid', data['waitid']);

	$('#chat-dialog-title').text('SMS CONVERSATION WITH ' + data['customername'].toUpperCase());

	getMessageByContactring(contactstring ,data['created']);

	$("#span-"+contactstring).text('');

	//an interval runs to get the new message
	var interval = setInterval(function() {
		try {
				$("#sms").on('hide.bs.modal', function () {
					clearInterval(interval);
				});

				getNewMessageByContactString(contactstring, data['created']);
		}
		catch(evt) {

		}
	}, 10000);
});

/*use for display conversation message properly, auto scroll to bottom*/
$("#sms").on('shown.bs.modal', function () {
	$('#chat-text-countdown').text('160');
	$('.messages').scrollTop($('.messages').prop("scrollHeight"));
});

/*get all conversation message for a contact string, trigger went user click on sms conversation button*/
function getMessageByContactring(contactstring, date) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=GetMessageByContactString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpContactString: contactstring, inpCreated: date, inpListId: $("#current-list-id").val()}
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				var messages = data.MESSAGES;
				for (var i = 0; i < messages.length; i++) {
					if (messages[i].TYPE === 'in') {
						// message received, display on the left of conversation window
						sendMessage(messages[i].MESSAGE, 'left');
					} else {
						// message sent, display on the right of conversation window
						sendMessage(messages[i].MESSAGE, 'right');
					}
				}
			} else {
				alertMessage("Oops!", data.MESSAGE);
			}
		})
		.fail(function(e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		});
	} catch (e) {

	}
}

/*get new conversation message for current conversation window, run every 10 sec*/
function getNewMessageByContactString(contactstring, date) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=GetNewMessageByContactString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			data: {inpContactString: contactstring, inpCreated: date, inpListId: $("#current-list-id").val()}
		})
		.done(function(data) {
			data = $.parseJSON(data);
			if (parseInt(data.RXRESULTCODE) === 1) {
				var messages = data.MESSAGES;
				if (messages.length > 0) {
					for (var i = 0; i < messages.length; i++) {
						// new message received, display on the left of conversation window
						sendMessage(messages[i], 'left');
					}
				}
			} else {
				
			}
		})
		.fail(function(e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		});
	} catch (e) {

	}
}

/*send new conversation message*/
function SendSMS(waitid, message) {
	if (message.length <= 160 && message.length > 0) {
		try {
			$.ajax({
				url: '/session/sire/models/cfc/waitlistapp.cfc?method=SendSMSToCustomer&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpWaitListId: waitid, inpText: message, inpListId: $("#current-list-id").val()},
			})
			.done(function(data) {
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) === 1) {
					return 0;
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
			})
			.fail(function(e) {
				alertMessage("Oops!", $.parseJSON(e).MESSAGE);
			});
		} catch (e) {
			alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
		}	
	}
	return 0;
}

$(document).ready(function(){
	/*when document is ready*/
	// init waitlist app datatable
	GetWaitList();

	// bind conversation input content with count down function
	count_down_character_by_id('chat-text-countdown','chat-content', 160);

	// main interval for reload datatable and today statistic section, run every 30 sec
	setInterval(function () {
		ReloadDataTable();
		updateTodayStatistic();
	}, 30000);
});

/*
	SMS conversation window
	Source: http://bootsnipp.com/snippets/ZlkBn
*/

var Message;
Message = function (arg) {
    this.text = arg.text, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var $message;
            $message = $($('.message_template').clone().html().replace('<ul>','').replace('</ul>',''));
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};

var getMessageText, message_side, sendMessage;
message_side = 'right';
getMessageText = function () {
    var $message_input;
    $message_input = $('.message_input');
    return $message_input.val();
};
sendMessage = function (text, side) {
    var $messages, message;
    if (text.trim() === '') {
        return;
    }
    $('.message_input').val('');
    $messages = $('.messages');
    message_side = typeof side === 'undefined' ? 'right' : side;
    message = new Message({
        text: text,
        message_side: message_side
    });
    message.draw();

    $('.messages').scrollTop($('.messages').prop("scrollHeight"));
};
$('.send_message').click(function (e) {
	SendSMS($('#sms').data('waitid'), getMessageText());
    return sendMessage(getMessageText());
});
$('.message_input').keyup(function (e) {
    if (e.which === 13) {
    	SendSMS($('#sms').data('waitid'), getMessageText());
        return sendMessage(getMessageText());
    }
});

/* add new waitlist validate and trigger */
function validate_add_waitlist_form(event){
	if ($('#add-waitlist-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var serviceId;
		if ( !$('#select-service').is(':visible')){
		    serviceId = 0;
		}
		else{
			serviceId = $("#select-service option:selected").val();
		}
		var customerOrder = $("#customer-order option:selected").val();
		var customerName = $("#customer-name").val();
		var customerEmail = $("#customer-email").val();
		var customerPhone = $("#customer-phone").val();
		var waitlistSize = $("#party-size").val();
		var waitTime = $("#wait-time option:selected").val();
		var note = $("#add-notes").val();

		$.ajax({
			url: url_create_waitlist,
			type: 'POST',
			dataType: 'JSON',
			data: {
				inpServiceId: serviceId,
				inpCustomerOrder: customerOrder,
				inpCustomerName: customerName,
				inpCustomerEmail: customerEmail,
				inpCustomerContactString: customerPhone,
				inpWaitlistSize: waitlistSize,
				inpWaitTime: waitTime,
				inpNote: note,
				inpListId: $("#current-list-id").val()
			},
			beforeSend:function( xhr ) {
			$('#processingPayment').show();
			},
			success: function(data){
				$('#processingPayment').hide();
				if (data.RXRESULTCODE==1){
					bootbox.dialog({
					    message: data.MESSAGE,
					    title: 'Waitlist App Setting',
					    buttons: {
					        success: {
					            label: "OK",
					            className: "btn btn-success btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
					$('#add-new-waitlist').modal('hide');
					GetWaitList();
				}
				else{
					bootbox.dialog({
					    message: data.MESSAGE,
					    title: 'Waitlist App Setting',
					    buttons: {
					        success: {
					            label: "OK",
					            className: "btn btn-success btn-success-custom",
					            callback: function() {}
					        }
					    }
					});
				}
			}
		});
		

	}
}

/* edit waitlist validate and trigger */
function validate_edit_waitlist_form(event){
	if ($('#edit-waitlist-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			event.preventDefault();
			var waitlistId = $('#waitlist-id').val();
			var serviceId = $("#select-service-edit option:selected").val();
			var customerOrder = $('#customer-order-edit option:selected').val();
			var customerName = $("#customer-name-edit").val();
			var customerEmail = $("#customer-email-edit").val();
			var customerPhone = $("#customer-phone-edit").val();
			var waitlistSize = $("#party-size-edit").val();
			var waitTime = $("#wait-time-edit option:selected").val();
			var note = $("#add-notes-edit").val();

			$.ajax({
				url: url_update_waitlist,
				type: 'POST',
				dataType: 'JSON',
				data: {
					inpWaitlistId: waitlistId,
					inpCustomerOrder: customerOrder,
					inpServiceId: serviceId,
					inpCustomerName: customerName,
					inpCustomerEmail: customerEmail,
					inpCustomerContactString: customerPhone,
					inpWaitlistSize: waitlistSize,
					inpWaitTime: waitTime,
					inpNote: note,
					inpListId: $("#current-list-id").val()
				},
				beforeSend:function( xhr ) {
				$('#processingPayment').show();
				},
				success: function(data){
					if(data.RXRESULTCODE==1){
						$('#processingPayment').hide();
						bootbox.dialog({
						    message: data.MESSAGE,
						    title: 'Waitlist App Setting',
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						$('#edit-waitlist').modal('hide');
						ReloadDataTable();
					}
					else{
						$('#processingPayment').hide();
						bootbox.dialog({
						    message: data.MESSAGE,
						    title: 'Waitlist App Setting',
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
					}
				}
			});
		}
}

/* trigger edit waitlist item button, init setting and display edit modal */
$('body').on('click', '.btn-edit', function(event) {
	var data = $(this).data('waitid');
	$('#waitlist-id').val(data);
	initSettingForEditWaitlist(data, "edit");
	$("#processingPayment").hide();
	$('#edit-waitlist').modal('show');
	set_css_for_safari();
});

/* init waitlist setting for edit modal */
function initSettingForEditWaitlist (id) {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=InitSettingForCreateEdit&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: {inpWaitListId: id, inpListId: $("#current-list-id").val()},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					if(data.WAITLISTSETTING.RXRESULTCODE > 0){
						$('#party-size-edit').html('');
						$('#party-size-edit').append('<option value="0">None</option>');
						for (var i = 1; i <= data.WAITLISTSETTING.MAXSIZE; i++) {
							$('#party-size-edit').append('<option value="'+ i +'">'+ i +'</option>');
						}

						if(data.WAITLISTSETTING.QUOTEWAITSTA == 1){
							$('#wait-time-edit').parent('div').show();
							$('#wait-time-edit').html('');
							$('#wait-time-edit').append('<option value="0">None</option>');
							if(data.WAITLISTSETTING.QUOTETIME < 60){
								for (var j = 1; j <= data.WAITLISTSETTING.QUOTETIME/5; j++) {
									$('#wait-time-edit').append('<option value="'+ j*5 +'">'+j*5 +' minutes</option>'); 
								}
							}
							else{
								for (var k = 1; k <= 6; k++) {
									$('#wait-time-edit').append('<option value="'+ k*5 +'">'+k*5 +' minutes</option>');
								}
								$('#wait-time-edit').append('<option value="60">1 hour</option>');
								for (var l = 1; l < data.WAITLISTSETTING.QUOTETIME/60; l++) {
									$('#wait-time-edit').append('<option value="'+ (l+1)*60 +'">'+(l+1) +' hours</option>');
								}
							}
						}
						else{
							$('#wait-time-edit').parent('div').hide();
						}
						addServiceStatus = data.WAITLISTSETTING.ADDSERVICESTA;
					} else {
						$('#wait-time-edit').parent('div').hide();
					}

					if(data.WAITLISTSERVICES.RXRESULTCODE > 0){
						if(addServiceStatus == 1){
							$('#select-service-edit').parent('div').show();
							$('#select-service-edit').html('');
							$('#select-service-edit').append('<option value="0">None</option>');
							for (var i = 0; i < data.WAITLISTSERVICES['services'].length; i++) {
								$('#select-service-edit').append('<option value="'+ data.WAITLISTSERVICES['services'][i][0] +'">'+ data.WAITLISTSERVICES['services'][i][1] +'</option>')
							}
						}
						else{
							$('#select-service-edit').parent('div').hide();
						}
						
					} else {
						$('#select-service-edit').parent('div').hide();
					}

					if(data.LISTORDER.RXRESULTCODE > 0){
						$('#customer-order-edit').html('');
						$('#customer-order-edit').append('<option value="0">Last order</option>');
						for (var i = 1; i < data.LISTORDER.TOTALINLINE; i++) {
							$('#customer-order-edit').append('<option value="' + i + '">' + i + '</option>');
						}
					}
					else{
						$('#customer-order-edit').html('');
						$('#customer-order-edit').append('<option value="0">Last order</option>');
					}

					if(data.WAITLISTINFO.RXRESULTCODE > 0){
						$('#customer-name-edit').val(data.WAITLISTINFO.CUSTOMERNAME);
						$('#customer-email-edit').val(data.WAITLISTINFO.CUSTOMEREMAIL);
						$('#customer-phone-edit').val(data.WAITLISTINFO.CUSTOMERPHONE);
						$('#add-notes-edit').val(data.WAITLISTINFO.CUSTOMERNOTE);

						$('#select-service-edit > option').each(function(){
							if(parseInt($(this).attr('value')) == parseInt(data.WAITLISTINFO.SERVICEID)){
								$(this).prop('selected', true);
							}
						});

						$('#party-size-edit > option').each(function(){
							if(parseInt($(this).attr('value')) == parseInt(data.WAITLISTINFO.CUSTOMERSIZE)){
								$(this).prop('selected', true);
							}
						});

						$('#wait-time-edit > option').each(function(){
							if(parseInt($(this).attr('value')) == parseInt(data.WAITLISTINFO.CUSTOMERWAITTIME)){
								$(this).prop('selected', true);
							}
						});
					}

					if (data.CUSTOMERINFO.RXRESULTCODE > 0) {
						if (data.CUSTOMERINFO.POSITION != data.CUSTOMERINFO.TOTALINLINE) {
							$('#customer-order-edit').val(data.CUSTOMERINFO.POSITION);
						} else {
							$('#customer-order-edit').val(0);
						}
					}
				}
			},
			error: function (e) {
				alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
			}
		});
	} catch (e) {
		alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
	}
}

/* additional css fix for safari */
function set_css_for_safari(){ 
    if (navigator.userAgent.indexOf('Safari') != -1 && 
    	navigator.userAgent.indexOf('Chrome') == -1 &&
    	navigator.userAgent.indexOf('Firefox') == -1 &&
    	navigator.userAgent.indexOf('Opera') == -1) {
        $('#party-size').css('text-indent', '20px');
    	$('#party-size-edit').css('text-indent', '20px');
    	$('#wait-time').css('text-indent', '20px');
    	$('#wait-time-edit').css('text-indent', '20px');
    }
}

/* trigger create new waitlist item button, init setting for create modal */
$('#add-to-waitlist-btn').click(function(event) {
	initSettingForCreateWaitlist();
	$("#processingPayment").hide();
	set_css_for_safari();
});

/* init waitlist setting for create new waitlist modal */
function initSettingForCreateWaitlist () {
	try {
		$.ajax({
			url: '/session/sire/models/cfc/waitlistapp.cfc?method=InitSettingForCreateEdit&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'JSON',
			data: {inpListId: $("#current-list-id").val()},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				data = $.parseJSON(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					if(data.WAITLISTSETTING.RXRESULTCODE == 1){
						$('#party-size').html('');
						$('#party-size').append('<option value="0">None</option>');
						for (var i = 1; i <= data.WAITLISTSETTING.MAXSIZE; i++) {
							$('#party-size').append('<option value="'+ i +'">'+ i +'</option>');
						}

						if(data.WAITLISTSETTING.QUOTEWAITSTA == 1){
							$('#wait-time').parent('div').show();
							$('#wait-time').html('');
							$('#wait-time').append('<option value="0">None</option>');
							if(data.WAITLISTSETTING.QUOTETIME < 60){
								for (var j = 1; j <= data.WAITLISTSETTING.QUOTETIME/5; j++) {
									$('#wait-time').append('<option value="'+ j*5 +'">'+j*5 +' minutes</option>'); 
								}
							}
							else{
								for (var k = 1; k <= 6; k++) {
									$('#wait-time').append('<option value="'+ k*5 +'">'+k*5 +' minutes</option>');
								}
								$('#wait-time').append('<option value="60">1 hour</option>');
								for (var l = 1; l < data.WAITLISTSETTING.QUOTETIME/60; l++) {
									$('#wait-time').append('<option value="'+ (l+1)*60 +'">'+(l+1) +' hours</option>');
								}
							}
						}
						else{
							$('#wait-time').parent('div').hide();
							$("#wait-time option:selected").val('0');
						}

						$('#party-size > option').each(function(){
							if($(this).attr('value') == data.WAITLISTSETTING.MAXSIZE)
								$(this).prop('selected', true);
						});

						$('#wait-time > option').each(function(){
							if($(this).attr('value') == data.WAITLISTSETTING.QUOTETIME)
								$(this).prop('selected', true);
						});
						
						addServiceStatus = data.WAITLISTSETTING.ADDSERVICESTA;
					} else {
						$('#wait-time').parent('div').hide();
						$("#wait-time option:selected").val('0');
					}

					if(data.WAITLISTSERVICES.RXRESULTCODE == 1){
						if(addServiceStatus == 1){
							$('#select-service').parent('div').show();
							$('#select-service').html('');
							$('#select-service').append('<option value="0">None</option>');
							for (var i = 0; i < data.WAITLISTSERVICES['services'].length; i++) {
								$('#select-service').append('<option value="'+ data.WAITLISTSERVICES['services'][i][0] +'">'+ data.WAITLISTSERVICES['services'][i][1] +'</option>');
							}
						}
						else{
							$('#select-service').parent('div').hide();
						}
					} else {
						$('#select-service').parent('div').hide();
					}

					if(data.LISTORDER.RXRESULTCODE == 1){
						$('#customer-order').html('');
						$('#customer-order').append('<option value="0">Default (last order)</option>');
						for (var i = 1; i <= data.LISTORDER.TOTALINLINE; i++) {
							$('#customer-order').append('<option value="' + i + '">' + i + '</option>');
						}
					}
					else{
						$('#customer-order').html('');
						$('#customer-order').append('<option value="0">Default (last order)</option>');
					}
				}
			},
			error: function (e) {
				alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
			}
		});
	} catch (e) {
		alertMessage("Oops!", "Sorry system is not available right now! Please try again later!");
	}
}

/* when user click on waiting tab, reload datatable and today statistic */
$("#waiting-tab").click(function(event) {
	ReloadDataTable();
	updateTodayStatistic();
});

/* reload datatable function, if filter are being use reload with filter options */
function ReloadDataTable() {
	var filtered = CheckFilterd();
	if (filtered) {
		ApplyFilter_();
	} else {
		GetWaitList();
	}
}
function CheckFilterd () {
	var filtered = false;
	$('input.filter_val').each(function(index, el) {
		if ($(el).val().length > 0) {
			filtered = true;
		}
	});
	return filtered;
}

/* reset create new waitlist modal input */
$('#add-new-waitlist').on('hide.bs.modal',function(){
 	document.getElementById("add-waitlist-form").reset();
});

/* init mask for phone input */
$("#customer-phone").mask("(000)000-0000");
$("#customer-phone-edit").mask("(000)000-0000");
$("#add-waitlist-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});

/* reset add waitlist service form when modal hide*/
$('#add-new-service-waiting').on('hide.bs.modal',function(){
 	document.getElementById("add-waitlist-service-form").reset();
});

// validate add waitlist form 
function validate_add_waitlist_service_form(event){
	if ($('#add-waitlist-service-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
		event.preventDefault();
		var newServiceName = $('#new-service-name').val();
		var newServiceDescription = $('#new-service-description').val();
		$.ajax({
			url: url_add_service,
			type: 'POST',
			dataType: 'JSON',
			async: false,
			data: {
				inpServiceName: newServiceName,
				inpServiceDescription: newServiceDescription,
				inpListId: $("#current-list-id").val()
			},
			success: function(data){
				$('#processingPayment').hide();
				if (data.RXRESULTCODE > 0) {
					$('#add-new-service-waiting').modal('hide');
					$('#add-service').html('');
					
					if ( $('#add-waitlist-form').is(':visible')){
						initSettingForCreateWaitlist();
						$('#select-service > option').last().prop('selected', true);
					}

					if ( $('#edit-waitlist-form').is(':visible')){
						var itemId = $('#waitlist-id').val();
						initSettingForEditWaitlist(itemId);
						$('#select-service-edit > option').last().prop('selected', true);
					}
					$('body').addClass('modal-open');
				} else {
					alertMessage("Oops!", data.MESSAGE);
				}
			}
		});
		
	}
}

// submit add new waitlist form
$('#save-service-btn').click(validate_add_waitlist_service_form);

// add modal-open class if add-new-waitlist modal is still open and hide validation error promt after close add waitlist service modal
$('#add-new-service-waiting').on('hidden.bs.modal', function(){
	addModalOpenClass();
	$('#add-waitlist-service-form').validationEngine('hide');
});

// add modal-open class if add-new-waitlist or edit-waitlist modal is still open
function addModalOpenClass(){
	if ( $('#add-new-waitlist').is(':visible')){
		$('body').addClass('modal-open');
	}

	if ( $('#edit-waitlist').is(':visible')){
		$('body').addClass('modal-open');
	}
}
	