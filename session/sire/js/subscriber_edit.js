(function($){
	var _cfList = {
		firstName: 'First Name',
		emailAddress: 'Email Address',
		dateOfBirth: 'Date of Birth',
		zipCode: 'Zip Code'
	};
	
	var _cfFieldIndex = 1;
	
	$.each(_cfList, function(_cfId, _cfItem){

		var cfFieldIndex = _cfFieldIndex++;

		var errorSave = function(title, msg) {
			var bootstrapAlert = $('#bootstrapAlert');
			bootstrapAlert.find('.modal-title').text(title);
			bootstrapAlert.find('.alert-message').text(msg);
			bootstrapAlert.modal('show');
			$('#' + _cfId + 'Input').prop('readonly', false).removeClass('input-loading');
			$('#' + _cfId + 'Edit .btn-save').prop('disabled', false);
		}
		
		$('#' + _cfId + 'View .btn-cf-edit').click(function(event){
			event.preventDefault()
			$('#' + _cfId + 'View').hide();
			$('#' + _cfId + 'Edit').show();
		});
		
		var _cfNewValue = $('#' + _cfId + 'Input').on('input', function(){
			if (_cfNewValue != $(this).val()) {
				$('#' + _cfId + 'Edit .btn-save').prop('disabled', false);
			} else {
				$('#' + _cfId + 'Edit .btn-save').prop('disabled', true);
			}
		}).keyup(function(event){
			if(event.keyCode == 13) {
				$('#' + _cfId + 'Edit .btn-save').click();
			} else if(event.keyCode == 27) {
				$('#' + _cfId + 'Edit .btn-cancel').click();
			}
		}).val();
		
		$('#' + _cfId + 'Edit .btn-save').click(function(){
			$('#' + _cfId + 'Edit .control-cf-btns').hide();
			$(this).prop('disabled', true);
			var _input = $('#' + _cfId + 'Input').prop('readonly', true).addClass('input-loading');
			var value = _input.val();
			var cfvId = _input.data('cfv-id');
			var contactId = _input.data('contact-id');

			if(_cfId == 'emailAddress')
			{
				if(!validateEmail(value))
				{
					bootbox.dialog({
					    message: "Invalid Email Address",
					    title: "Edit Subscriber",
					    buttons: {
					        success: {
					            label: "Ok",
					            className: "btn btn-medium btn-success-custom",
					            callback: function() {
					            	$('#' + _cfId + 'Edit .control-cf-btns').show();
					            	 $('#' + _cfId + 'Input').prop('readonly', true).removeClass('input-loading').prop('readonly', false);
					            }
					        }
					    }
					});
					return;
				}

			}


			$.ajax({
				url: '/session/sire/models/cfc/subscribers.cfc?method=updateCustomField&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'json',
				data: {'contactId': contactId, 'fieldId': cfvId, 'fieldIndex': cfFieldIndex, 'fieldValue': value},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				complete: function(){
					$('#' + _cfId + 'Edit .control-cf-btns').show();
					$('#processingPayment').hide();
				},
				error: function( jqXHR, textStatus, errorThrown ) {
					errorSave('Save ' + _cfItem, 'We\'re Sorry! Unable to complete save ' + _cfItem + ' at this time.');
				},
				success: function( data, textStatus, jqXHR ) {
					if (data && data.status == 1) {
						_cfNewValue = value;
						$('#' + _cfId + 'View .control-view').text(value);
						$('#' + _cfId + 'Edit').hide();
						$('#' + _cfId + 'View').show();
						$('#' + _cfId + 'Input').removeClass('input-loading').data('cfv-id', data.cfvId).prop('readonly', false);
						$('#' + _cfId + 'Edit .btn-save').prop('disabled', true);
						$('#' + _cfId + 'Edit .control-cf-btns').show();
					} else {
						if (data && data.msg)
							errorSave('Save ' + _cfItem, data.msg);
						else {
							errorSave('Save ' + _cfItem, 'We\'re Sorry! Unable to complete save ' + _cfItem + ' at this time.');
						}
					}
				}
			});
		});
		
		$('#' + _cfId + 'Edit .btn-cancel').click(function(){
			$('#' + _cfId + 'Edit').hide();
			$('#' + _cfId + 'View').show();
			$('#' + _cfId + 'Input').val(_cfNewValue).prop('readonly', false).removeClass('input-loading');
			$('#' + _cfId + 'Edit .btn-save').prop('disabled', true);
			$('#' + _cfId + 'Edit .control-cf-btns').show();
		});

	});
	
	(function($){
		
		var errorSave = function(title, msg) {
			var bootstrapAlert = $('#bootstrapAlert');
			bootstrapAlert.find('.modal-title').text(title);
			bootstrapAlert.find('.alert-message').text(msg);
			bootstrapAlert.modal('show');
		}

		$('#unsubscribe').click(function(event){
			event.preventDefault();
			bootbox.dialog({
		    message:'Are you sure you want to unsubscribe this phone number?',
		    title: "Unsubscribe",
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {
			            	var self = $('#unsubscribe');
							if (self.prop('unning') !== true) {
								self.prop('unning', true);
								$('<img src="/session/sire/images/loading.gif" />').insertBefore(self);
								var contactString = self.data('contact-string');
								
								$.ajax({
									url: '/session/sire/models/cfc/subscribers.cfc?method=unsubscribe&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									type: 'POST',
									dataType: 'json',
									data: {'contactString': contactString},
									beforeSend: function(){
										$('#processingPayment').show();
									},
									complete: function(){
										self.prev().remove();
										self.prop('unning', false);
										$('#processingPayment').hide();
									},
									error: function( jqXHR, textStatus, errorThrown ) {
										errorSave('Unsubscribe', 'We\'re Sorry! Unable to complete unsubscribe at this time.');
									},
									success: function( data, textStatus, jqXHR ) {
										if (data && data.status == 1) {
											$('#SubscriberState').text('Unsubscribed');
											$('#unsubscribeView').text(data.optOutDate);
										} else {
											if (data && data.msg)
												errorSave('Unsubscribe', data.msg);
											else {
												errorSave('Unsubscribe', 'We\'re Sorry! Unable to complete unsubscribe at this time.');
											}
										}
									}
								});
							}
			            	
			            }
			        },
			         danger: {
				      label: "Cancel",
				      className: "btn btn-primary btn-back-custom",
				      callback: function() {
				        
				      }
				    },
			    }
			});	

		});
	
	})($);
	
	(function($){
		function InitSubscriberHistory(customFilterObj) {
			var ContactString = $('#tblSubscriberHistory').data('contactstring');
			 var customFilterData = typeof(customFilterObj) != 'undefined' ? JSON.stringify(customFilterObj) : "";
			 _tblSubscriberHistory = $('#tblSubscriberHistory').dataTable({
				"bStateSave": true,
				"iStateDuration": -1,
				"fnStateLoadParams": function (oSettings, oData) {
					if (location.hash == "") {
						oData.oSearch.sSearch = "";
						return false;
					}
				},
				"fnStateSaveParams": function (oSettings, oData) {
					if (history.replaceState) {
						if (oData.iStart > 0) {
							history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
						} else {
							history.replaceState(history.state, document.title, location.pathname + location.search);
						}
					}
				},
		
				
			 	"bAutoWidth": false,
	            "bProcessing": true,
	            "bFilter": false,
	            "bServerSide": true,
	            "bDestroy": true,
	            "bLengthChange": false,
	            "sPaginationType": "input",
	            "iDisplayLength": 10,
	            "aoColumns": [
	                {"sName": 'IREResultsId_bi', "sTitle": '', "sWidth": '20px', "bSortable": false},
	                {"sName": 'Created_dt', "sTitle": 'Date', "sWidth": '20%', "bSortable": false},
	                {"sName": 'Direction', "sTitle": 'Direction', "sWidth": '15%', "bSortable": false},
	                {"sName": 'Message', "sTitle": 'Message', "sWidth": '', "bSortable": false},
	                {"sName": 'Status', "sTitle": 'Delivery Success', "sWidth": '20%', "bSortable": false}
	            ],
	            "sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetSubscriberHistoryForDatatable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&ContactString=' + ContactString +'&contactType='+contactType,
	            "fnServerData": function (sSource, aoData, fnCallback) {
	                if (aoData[3].value < 0){
						aoData[3].value = 0;
					}
	                aoData.push(
	                    {"name": "customFilter", "value": customFilterData}
	                );
	                $.ajax({
	                    dataType: 'json',
	                    type: "POST",
	                    url: sSource,
	                    data: aoData,
	                    success: fnCallback
	                });
	            },

	            "fnDrawCallback": function( oSettings ) {
					if (oSettings._iDisplayStart < 0){
						oSettings._iDisplayStart = 0;
						$('input.paginate_text').val("1");
					}
			    }

	        });
		}
		InitSubscriberHistory();
		
		$('#campaign_id').change(function(){
			InitSubscriberHistory([{"NAME":"BatchId","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":$(this).val()}]);
		});
	})($);
	
	$('#btn-back').click(function(event){
		if (history.length > 1) {
			event.preventDefault();
			history.back();
		}
	});

	function validateEmail(email) {
  		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		return re.test(email);
	}

})(jQuery);