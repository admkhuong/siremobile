(function($){
	$('body').append($('body div .modal'));

	$('body').on('shown.bs.modal','.bootbox-confirm', function(){
		$("body.modal-open").removeAttr("style")
	});

	$('body').on('hide.bs.modal','.bootbox-confirm', function(){
		$("body.modal-open").removeAttr("style")
	});

	$('body').on('shown.bs.modal','.bootbox-prompt', function(){
		$("body.modal-open").removeAttr("style")
	});

	$('body').on('hide.bs.modal','.bootbox-prompt', function(){
		$("body.modal-open").removeAttr("style")
	});


	var modalAlert = function(title, message, event) {
		var bootstrapAlert = $('#bootstrapAlert');
		bootstrapAlert.find('.modal-title').text(title);
		bootstrapAlert.find('.alert-message').text(message);
		bootstrapAlert.modal('show');
	}
	
	var _tblListGroupContact;
	//init datatable for active agent
	function InitGroupContact(customFilterObj){
		var customFilterData =  typeof(customFilterObj) != 'undefined' ? JSON.stringify(customFilterObj) : "";
		_tblListGroupContact = $('#tblListGroupContact').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					sessionStorage. removeItem('subscriber-list-string-filter');
					sessionStorage.removeItem('subscriber-list-status');
					sessionStorage.removeItem('subscriber-list-id');
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					} else {
						history.replaceState(history.state, document.title, location.pathname + location.search);
					}
				}
			},

			
			"bAutoWidth": false,
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'Select', "sTitle": '', "sWidth": '20px',"bSortable": false},
				{"sName": 'ID', "sTitle": 'ID', "sWidth": '40px',"bSortable": false},
				{"sName": 'Name', "sTitle": 'Name', "sWidth": '',"bSortable": false},
				{"sName": 'Voice', "sTitle": 'Voice', "sWidth": '',"bSortable": false, "bVisible": false},
				{"sName": 'SMS', "sTitle": 'Subscriber #', "sWidth": '100px',"bSortable": false},
				{"sName": 'Options', "sTitle": 'Actions', "sWidth": '60px',"sClass":"text-center","bSortable": false},

			],
			"sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupListForDatatableNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&isCreateEms=0',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
	        "fnDrawCallback": function( oSettings ) {

	        	if (oSettings._iRecordsDisplay > 0) {
					//var contactGroupId = $('#tblListGroupContact').data('group-id');
					var contactGroupId = sessionStorage.getItem('subscriber-list-id');
		        	$('#tblListGroupContact > tbody').find('tr').each(function(id){
						var tdFirst = $(this).find('td:first');
						var groupId = $.trim(tdFirst.text());
						tdFirst.html('<input type="radio" name="GroupContact" class="group-contact" id="group-contact-' + id + '" value="' + groupId + '">');
						if (contactGroupId == groupId) {
							tdFirst.find('#group-contact-' + id).prop('checked', true);

				    		var str = sessionStorage.getItem('subscriber-list-string-filter');
				    		var sta = sessionStorage.getItem('subscriber-list-status');

							$('#searchOptInOut').val('1').show();
					    	$('#searchContact').val('').show();
					    	
					    	if ((sta != null && sta >= 0) || (str != null && str != '')) {
					    		var customFilterObj = [];
					    		if (sta != null && sta >= 0) {
									$('#searchOptInOut').val(sta).show();
					    			customFilterObj.push({"NAME":"OPTINOUT_INT","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":sta});
				    			} else {
				    				sta = -1;
				    			}
					    		if (str != null && str != '') {
							    	$('#searchContact').val(str).show();
					    			customFilterObj.push({"NAME":"CONTACTSTRING_VCH","OPERATOR":"LIKE","TYPE":"CF_SQL_VARCHAR","VALUE":str});
				    			} else {
				    				str = '';
				    			}

					    		InitGroupDetails(customFilterObj);
					    	} else {
					    		sta = -1; str = '';
								InitGroupDetails();
							}

							//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=' + id + '&status=' + sta + '&search=' + str).show();
						}
						tdFirst.click(function(){
							var thisInput = $(this).find('input');
							var contactGroupId = thisInput.val();
							if (contactGroupId == sessionStorage.getItem('subscriber-list-id')) {
								sessionStorage.setItem('subscriber-list-id', '0');
								//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=0').show();
								thisInput.prop('checked', false);
								$('#searchOptInOut').val('1').show();
						    	$('#searchContact').val('').show();
							} else {
								sessionStorage.setItem('subscriber-list-id', contactGroupId);
								//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=' + contactGroupId).show();
								thisInput.prop('checked', true);
								$('#searchOptInOut').val('1').show();
						    	$('#searchContact').val('').show();
					    	}
					    	$('#tblListContact_paginate .paginate_text').keyup();
							InitGroupDetails();
							$('#tblListContact_paginate .first').click();
						});
					});
					$('#tblListGroupContact_wrapper > .dataTables_paginate').show();
				}
				else {
					$('#tblListGroupContact_wrapper > .dataTables_paginate').hide();
				}
			}
	    });
	}
	InitGroupContact();
	
    //init datatable for active agent
    function InitGroupDetails(customFilterObj) {
    	var contactGroupId = $('#tblListGroupContact > tbody input.group-contact:checked').val();
    	if (!$.isNumeric(contactGroupId)) contactGroupId = 0;
        var customFilterData = typeof(customFilterObj) != 'undefined' ? JSON.stringify(customFilterObj) : "";
        _tblListContact = $('#tblListContact').dataTable({
			"bStateSave": true,
			"iStateDuration": -1,
			"fnStateLoadParams": function (oSettings, oData) {
				if (location.hash == "") {
					oData.oSearch.sSearch = "";
					return false;
				}
			},
			"fnStateSaveParams": function (oSettings, oData) {
				if (history.replaceState) {
					if (oData.iStart > 0) {
						history.replaceState(history.state, document.title, location.pathname + location.search + '#' + (1 + oData.iStart/oData.iLength));
					}
				}
			},
			
			
        	"bAutoWidth": false,
            "bProcessing": true,
            "bFilter": false,
            "bServerSide": true,
            "bDestroy": true,
            "sPaginationType": "input",
            "bLengthChange": false,
            "iDisplayLength": 10,
            "aoColumns": [
                {"sName": 'ContactAddressId_bi', "sTitle": '', "sWidth": '20px', "bSortable": false},
                {"sName": 'ContactString_vch', "sTitle": 'Contacts', "sWidth": '', "bSortable": false},
                {"sName": 'Subscribed_dt', "sTitle": 'Date Subscribed', "sWidth": '', "bSortable": true},
                {"sName": 'Unsubscribed_dt', "sTitle": 'Date Unsubscribed', "sWidth": '', "bSortable": true, "bVisible": false,"sClass": 'hidden'},
                {"sName": 'Options', "sTitle": 'Actions', "sWidth": '60px', "bSortable": false, "sClass": 'text-center' }
            ],
            "sAjaxSource": '/session/sire/models/cfc/subscribers.cfc?method=GetGroupDetailsForDatatableNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&toSend=false&INPGROUPID=' + contactGroupId,
            "fnServerData": function (sSource, aoData, fnCallback) {
            	
                if (aoData[3].value < 0){
					aoData[3].value = 0;
				}
                aoData.push(
                        {"name": "customFilter", "value": customFilterData},
                        {"name": "shortCode", "value": shortCode}
                );
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    url: sSource,
                    data: aoData,
                    success: fnCallback
                });
            },
            "fnDrawCallback": function( oSettings ) {
            	if (oSettings._iRecordsTotal <= 0) {
            		$('#tblListContact_wrapper > .dataTables_paginate').hide();
            	}
            	else {
            		$('#tblListContact_wrapper > .dataTables_paginate').show();
            	}
            	if (oSettings._iDisplayStart < 0){
					oSettings._iDisplayStart = 0;
					$('input.paginate_text').val("1");
				}
            	$('.download-subscribers').data('_iRecordsTotal', oSettings._iRecordsTotal);
        	},
            "fnInitComplete": function (oSettings, json) {
                //in order to prevent this talbe from collapsing, we must fix its min width 
                //$('#tblListContact').attr('style', '');
                $('#subscriberTotal').text(oSettings._iRecordsTotal);
            }
        });
    }

	sessionStorage.setItem('subscriber-list-id', 0);
	//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=0').show();
	$('#searchOptInOut').val('1').show();
	$('#searchContact').val('').show();
    InitGroupDetails([
    	{"NAME":"OPTINOUT_INT","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":'1'}
    ]);
    
    $('#searchOptInOut').change(function(e){
    	e.preventDefault;
    	var self = $(this);
		var str = $.trim($('#searchContact').val());
		var sta = self.val();
		sessionStorage.setItem('subscriber-list-status', sta);
    	var contactGroupId = $('#tblListGroupContact > tbody input.group-contact:checked').val();
    	if (!$.isNumeric(contactGroupId)) contactGroupId = 0;
		//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=' + contactGroupId + '&status=' + sta + '&search=' + str).show();
		$('#tblListContact_paginate .paginate_text').keyup();
		InitGroupDetails([
			{"NAME":"CONTACTSTRING_VCH","OPERATOR":"LIKE","TYPE":"CF_SQL_VARCHAR","VALUE":str},
			{"NAME":"OPTINOUT_INT","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":sta}
		]);
		$('#tblListContact_paginate .first').click();
		
    });
    
    $('#searchContact').on('input', function(){
    	$(this).prop('changed', true);
    });
    
    $('#searchContact').keyup(function(event){
    	if($(this).prop('changed')) {
	    	if(event.keyCode == 13){
	    		var str = $.trim($(this).val());
	    		var sta = $('#searchOptInOut').val();
	    		sessionStorage.setItem('subscriber-list-string-filter', str);
		    	var contactGroupId = $('#tblListGroupContact > tbody input.group-contact:checked').val();
		    	if (!$.isNumeric(contactGroupId)) contactGroupId = 0;
				//$('.download-subscribers').attr('href', '/session/sire/models/cfm/subscribers.xls.cfm?id=' + contactGroupId + '&status=' + sta + '&search=' + str).show();
        		InitGroupDetails([
        			{"NAME":"CONTACTSTRING_VCH","OPERATOR":"LIKE","TYPE":"CF_SQL_VARCHAR","VALUE":str},
        			{"NAME":"OPTINOUT_INT","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":sta}
    			]);
	    		$(this).prop('changed', false);
			}
		}
    });
    
	$('#subscriber_list_form').submit(function(event){
		event.preventDefault();
	}).validationEngine({
		promptPosition : "topLeft", 
		autoPositionUpdate: true, 
		showArrow: false, 
		scroll: false,
		focusFirstField : false
	});

    $('#btn-add-new').click(function(){
		$('#subscriber_list_name').val('').validationEngine('hide');
		$('#AddNewSubscriberList button').prop('disabled', false);
    	$('#AddNewSubscriberList').modal('show');
    });
    
	$('#AddNewSubscriberList .btn-save-group').click(function(){
		if ($('#subscriber_list_form').validationEngine('validate', {focusFirstField: true, scroll: false})) {
			$('#AddNewSubscriberList button').prop('disabled', true);
			var groupName = $('#subscriber_list_name').val();
			$.ajax({
				url: '/session/cfc/multilists2.cfc?method=addgroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'post',
				dataType: 'json',
				data: {INPGROUPDESC: groupName},
				beforeSend: function(){
					$('#processingPayment').show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('#processingPayment').hide();
					modalAlert(textStatus, errorThrown);
					$('#AddNewSubscriberList button').prop('disabled', false);
				},
				success: function(data) {
					$('#processingPayment').hide();
					if (data.DATA && data.DATA.RXRESULTCODE && data.DATA.RXRESULTCODE[0] == 1) {
						$('#tblListGroupContact_wrapper input.paginate_text').keyup();
						$('#AddNewSubscriberList').modal('hide');
					} else {
						/*modalAlert('Add New Subscriber List', data.MESSAGE);*/
						bootbox.dialog({
						    message: data.DATA.MESSAGE[0],
						    title: "Subscriber",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
					}
					$('#AddNewSubscriberList button').prop('disabled', false);
				}
			});
		}
	});
	
	$('.download-subscribers').click(function(event) {
		var _iRecordsTotal = $(this).data('_iRecordsTotal');
		
		var str = $('#searchContact').val();
	    var sta = $('#searchOptInOut').val();
		var contactGroupId = $('#tblListGroupContact > tbody input.group-contact:checked').val();
		
		if (!$.isNumeric(_iRecordsTotal) || _iRecordsTotal < 1) {
	
			event.preventDefault();
			bootbox.dialog({
			    message:'There are currently no subscribers in this list to download.',
			    title: 'Download Subscriber List',
			    buttons: {
			        success: {
			            label: "Ok",
			            className: "btn btn-medium btn-success-custom",
			            callback: function() {

			           }
			        }
			    }
			});
		} else {
			// $("#processingPayment").show();
			// $.ajax({
   //      		url:'/session/sire/models/cfm/subscribers.xls.cfm?total='+$('#subscriberTotal').text()+'&id=' + contactGroupId + '&status=' + sta + '&search=' + str,
   //      		dataType: 'json',
   //      		success: function(data) {
   //      			$("#processingPayment").hide();
					
			// 		  for (var key in data) {
			// 			  if (data.hasOwnProperty(key)) {
			// 			    SaveToDisk(data[key]);
			// 			  }
			// 		  }
				  
       			
   //      		}
   //      	});
   			window.location.href = "/session/sire/models/cfm/subscribers.xls.cfm?total="+$('#subscriberTotal').text()+'&id=' + contactGroupId + '&status=' + sta + '&search=' + str;
		}
	});
	
	$('#tblListContact').on('click', '.subscriber-delete-icon', function(event){
		event.preventDefault();
		var self = $(this);
		var contactString = self.parents('tr').first().find('td').eq(1).text();
		var contactType = self.data('contact-type') !== 'undefined' ? self.data('contact-type') : 3;
		var groupId = self.data('contact-group-id');

		var contactGroupId = $('#tblListGroupContact > tbody input.group-contact:checked').val();

		var totalSub = parseInt($('#subscriberTotal').text());

		bootbox.dialog({
			title: 'Delete subscriber',
			message: 'Are you sure you want to delete the contact '+ contactString +'?',
			buttons: {
				ok: {
					title: ' OK ',
					className: 'btn btn btn-medium btn-success-custom',
					callback: function() {
						$('#processingPayment').show();
				        $.ajax({
							type: "POST",
							//url: '/session/sire/models/cfc/subscribers.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							url: '/session/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				            dataType: 'json',
				            data: {
				            	INPCONTACTSTRING: contactString, 
								INPCONTACTTYPE: contactType,
								INPGROUPID: groupId,
				            	INPCONTACTID: self.data('contact-id'),
				            	INPSHORTCODE: shortCode
			            	},
			            	error: function() {
			            		$('#processingPayment').hide();
			            	},
			            	complete: function() {
			            		$('#processingPayment').hide();
			            	},
				        	success: function(d2) {
				        		$('#processingPayment').hide();
				                if(d2.ROWCOUNT > 0){
				                    if(parseInt(d2.DATA.RXRESULTCODE) == 1){
				                    	if(contactGroupId > 0){
											$('#tblListContact_wrapper .paginate_text').keyup();
											$('#tblListGroupContact_wrapper input.paginate_text').keyup();
											return;
										}
										else{
											InitGroupContact();
											var customFilterObj = [];

											var status = $('#searchOptInOut').val();
											if(status >0)
												customFilterObj.push({"NAME":"OPTINOUT_INT","OPERATOR":"=","TYPE":"CF_SQL_INTEGER","VALUE":status});

											var str=$('#searchContact').val()
											if(str!='')
												customFilterObj.push({"NAME":"CONTACTSTRING_VCH","OPERATOR":"LIKE","TYPE":"CF_SQL_VARCHAR","VALUE":str});

											InitGroupDetails(customFilterObj);
											//totalSub= parseInt(totalSub-1)
											//$('#subscriberTotal').text(totalSub);
										}
				                    }
				                }

				                if(d2.DATA.MESSAGE !='')
				                	bootbox.alert(d2.DATA.MESSAGE);
				            }
				    	});
					}
				},
				cancel: {
					title: 'Cancel',
					className: 'btn btn btn-medium btn-back-custom'
				}
			}
		});
	});

	$('#tblListGroupContact').on('click', '#rename-contact-group', function(event){
		event.preventDefault();
		$('#rename-subscriber-list').modal('show');
		var self = $(this);
		var groupId = self.data('contact-group-id');
		$('#contact-group-id').val($(this).data('contact-group-id'));
		// bootbox.prompt('Rename subscriber list', function(result){ console.log(result); });
		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'POST',
			dataType: 'json',
			data: {inpGroupId: groupId},
			beforeSend: function( xhr ) {
	     		// $('#processingPayment').show();
	    	},
	    	error: function(XMLHttpRequest, textStatus, errorThrown) {
	     		// $('#processingPayment').hide();
	     		bootbox.dialog({
	         		message:'Get subscriber list detail failed!',
	         		title: "Edit subscriber list",
	         		buttons: {
	             		success: {
	                 		label: "Ok",
	                 		className: "btn btn-medium btn-success-custom",
	                 		callback: function() {}
	             		}
	         		}
	     		});
	    	},
	    	success: function(data){
	    		// $('#processingPayment').hide();
	    		$('#new-subscriber-list-name').val(data.SUBSCRIBERLISTNAME);
				$('#rename-subscriber-list-form').on('submit', function(event){
					event.preventDefault();
					if($('#rename-subscriber-list-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
						$.ajax({
			    			url: '/session/sire/models/cfc/subscribers.cfc?method=UpdateSubscriberListNameByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			    			type: 'POST',
			    			dataType: 'json',
			    			data: {
			    				inpGroupId: $('#contact-group-id').val(),
			    				inpNewGroupName: $('#new-subscriber-list-name').val()
			    			},
			    			beforeSend: function( xhr ) {
					     		$('#processingPayment').show();
					    	},
					    	error: function(XMLHttpRequest, textStatus, errorThrown) {
					     		$('#processingPayment').hide();
					     		bootbox.dialog({
					         		message:'Update subscriber list detail failed!',
					         		title: "Rename subscriber list",
					         		buttons: {
					             		success: {
					                 		label: "Ok",
					                 		className: "btn btn-medium btn-success-custom",
					                 		callback: function() {}
					             		}
					         		}
					     		});
					    	},
					    	success: function(data){
					    		$('#processingPayment').hide();
					    		if(data.RXRESULTCODE == 1){
					    			$('#rename-subscriber-list').modal('hide');
						    			bootbox.dialog({
						         		message:data.MESSAGE,
						         		title: "Rename subscriber list",
						         		buttons: {
						             		success: {
						                 		label: "Ok",
						                 		className: "btn btn-medium btn-success-custom",
						                 		callback: function() {}
						             		}
						         		}
						     		});
					    			InitGroupContact();
					    		}
					    		else{
					    			bootbox.dialog({
						         		message:data.MESSAGE,
						         		title: "Rename subscriber list",
						         		buttons: {
						             		success: {
						                 		label: "Ok",
						                 		className: "btn btn-medium btn-success-custom",
						                 		callback: function() {}
						             		}
						         		}
						     		});
					    		}
					    	}
			    		});
					}
		    	});
		    }
		});
	});

	$('#tblListGroupContact').on('click', '#delete-contact-group', function(event){
		event.preventDefault();
		var self = $(this);
		var groupId = self.data('contact-group-id');
		// bootbox.prompt('Rename subscriber list', function(result){ console.log(result); });
		$.ajax({
			url: '/session/sire/models/cfc/subscribers.cfc?method=GetCampaignByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'json',
			data: {inpGroupId: groupId},
			beforeSend: function( xhr ) {
	     		$('#processingPayment').show();
	    	},
	    	error: function(XMLHttpRequest, textStatus, errorThrown) {
	     		$('#processingPayment').hide();
	     		bootbox.dialog({
	         		message:'Send Get subscriber list detail request failed!',
	         		title: "Delete subscriber list",
	         		buttons: {
	             		success: {
	                 		label: "Ok",
	                 		className: "btn btn-medium btn-success-custom",
	                 		callback: function() {}
	             		}
	         		}
	     		});
	    	},
	    	success: function(data){
	    		$('#processingPayment').hide();
	    		if(data.RXRESULTCODE==1){
	    			var allCampaign = '';
	    			for (var i = 0; i < data['batchDetail'].length; i++) {
	    				allCampaign = allCampaign + '<li><a href="/session/sire/pages/campaign-edit?campaignid=' + data['batchDetail'][i].ID + '" target="_blank">' + data['batchDetail'][i].NAME + '</a></li>';
	    			}
	    			bootbox.confirm({
						title: "Delete subscriber list",
					    message: "Do you want to delete this subscriber list? It is being used by the following campaign(s): " + "<ol>" + allCampaign + "</ol>",
					    buttons: {
					        confirm: {
					            label: 'Yes',
					            className: 'btn-success-custom yes-delete-service pull-right'
					        },
					        cancel: {
					            label: 'No',
					            className: 'btn-back-custom no-delete-service pull-right'
					        }
					    },
					    callback: function (result) {
					    	if (result == true){
					    		$.ajax({
					    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					    			type: 'POST',
					    			dataType: 'JSON',
					    			data: {inpGroupId: groupId},
					    			beforeSend: function( xhr ) {
							     		$('#processingPayment').show();
							    	},
							    	error: function(XMLHttpRequest, textStatus, errorThrown) {
							     		$('#processingPayment').hide();
							     		bootbox.dialog({
							         		message:'Send Delete subscriber list request failed!',
							         		title: "Delete subscriber list",
							         		buttons: {
							             		success: {
							                 		label: "Ok",
							                 		className: "btn btn-medium btn-success-custom",
							                 		callback: function() {}
							             		}
							         		}
							     		});
							    	},
							    	success: function(data) {
							    		$('#processingPayment').hide();
							    		bootbox.dialog({
							         		message:data.MESSAGE,
							         		title: "Delete subscriber list",
							         		buttons: {
							             		success: {
							                 		label: "Ok",
							                 		className: "btn btn-medium btn-success-custom",
							                 		callback: function() {}
							             		}
							         		}
							     		});
							     		InitGroupContact();
							     		InitGroupDetails();
							    	}	
					    		});
					    		
					    	}
					        
					    }
					});
	    		}
	    		else if(data.RXRESULTCODE == 0){
	    			bootbox.confirm({
						title: "Delete subscriber list",
					    message: "Do you want to delete this subscriber list?",
					    buttons: {
					        confirm: {
					            label: 'Yes',
					            className: 'btn-success-custom yes-delete-service pull-right'
					        },
					        cancel: {
					            label: 'No',
					            className: 'btn-back-custom no-delete-service pull-right'
					        }
					    },
					    callback: function (result) {
					    	if (result == true){
					    		$.ajax({
					    			url: '/session/sire/models/cfc/subscribers.cfc?method=DeleteSubscriberListByGroupId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					    			type: 'POST',
					    			dataType: 'JSON',
					    			data: {inpGroupId: groupId},
					    			beforeSend: function( xhr ) {
							     		$('#processingPayment').show();
							    	},
							    	error: function(XMLHttpRequest, textStatus, errorThrown) {
							     		$('#processingPayment').hide();
							     		bootbox.dialog({
							         		message:'Delete subscriber list request failed!',
							         		title: "Delete subscriber list",
							         		buttons: {
							             		success: {
							                 		label: "Ok",
							                 		className: "btn btn-medium btn-success-custom",
							                 		callback: function() {}
							             		}
							         		}
							     		});
							    	},
							    	success: function(data) {
							    		$('#processingPayment').hide();
							    		bootbox.dialog({
							         		message:data.MESSAGE,
							         		title: "Delete subscriber list",
							         		buttons: {
							             		success: {
							                 		label: "Ok",
							                 		className: "btn btn-medium btn-success-custom",
							                 		callback: function() {}
							             		}
							         		}
							     		});
							     		InitGroupContact();
							     		InitGroupDetails();
							    	}	
					    		});
					    		
					    	}
					        
					    }
					});
	    		}
	    		else{
	    			bootbox.dialog({
		         		message:data.MESSAGE,
		         		title: "Delete subscriber list",
		         		buttons: {
		             		success: {
		                 		label: "Ok",
		                 		className: "btn btn-medium btn-success-custom",
		                 		callback: function() {}
		             		}
		         		}
		     		});
	    		}
	    	}

		});
	});
	
	
})(jQuery);

function SaveToDisk(fileUrl, fileName) {
    var hyperlink = document.createElement('a');
    hyperlink.href = fileUrl;
    hyperlink.target = '_self';
    hyperlink.download = fileName || fileUrl;

    (document.body || document.documentElement).appendChild(hyperlink);
    hyperlink.onclick = function() {
       (document.body || document.documentElement).removeChild(hyperlink);
    };

    var mouseEvent = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
    });

    hyperlink.dispatchEvent(mouseEvent);
    
    // NEVER use "revoeObjectURL" here
    // you can use it inside "onclick" handler, though.
    // (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);

    // if you're writing cross-browser function:
    if(!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
       window.URL.revokeObjectURL(hyperlink.href);
    }
}