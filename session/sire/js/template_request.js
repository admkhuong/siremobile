(function($){
	$("#template-request-form").validationEngine('attach',{promptPosition : "topLeft", scroll: false,focusFirstField : false});
	

	$("#submit-template-request").click(validate_form);

	$('#TemplateRequestModal').on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });

	$('#TemplateRequestModal').on('hide.bs.modal', function() { 
		$("#template-request-form").validationEngine('hide'); 
		$('#template-purpose').val('');
		$('#character-purpose-countdown').text(130-$('#template-purpose').val().length);
	});

	$('#character-purpose-countdown').text(130-$('#template-purpose').val().length);

	$('#template-purpose').on('keyup', function(){
		$('#character-purpose-countdown').text(130-$('#template-purpose').val().length);
	});

	var templatePurpose = document.getElementById('template-purpose');
	if(templatePurpose) {
	    templatePurpose.addEventListener("input", function() {
	        if (templatePurpose.value.length == 130) {
	        	return false;
	        } 
	        else if (templatePurpose.value.length > 130) {
	        	templatePurpose.value = templatePurpose.value.substring(0, 130);
	        }
	    },  false);
	}

	$('textarea#template-purpose').bind('paste', function(e) {
	    var elem = $(this);
	    setTimeout(function() {
	        var text = elem.val();
	        if (text.length > 130) {
				elem.val(text.substring(0,130));
	        }
	        $('#character-purpose-countdown').text(130-parseInt(elem.val().length));
	    }, 100);
	});

	function validate_form(event){
		event.preventDefault();
		var self = $(this);
		
		if ($('#template-request-form').validationEngine('validate')) {

			var userId = $('#user-id').val();
			var userContactString = $('#user-contactstring').val();
			var messageText = $('#template-purpose').val();
			var userFirstname = $('#user-firstname').val();
			var userEmail = $('#user-email').val();
			var userFullname = $('#user-fullname').val();


			var url_template_request = '/session/sire/models/cfc/template_request.cfc?method=SendTemplateRequestEmail';
			
			$.ajax({
				url: url_template_request,
				type: 'POST',
				dataType: 'json',
				data: {
					inpBatchId: '0', 
					inpContactString: userContactString,
					inpMessageText: messageText,
					inpUserId: userId,
					inpUserEmail: userEmail,
					inpUserFirstname: userFirstname,
					inpUserFullname: userFullname
				},
				beforeSend: function( xhr ) {
			 		$('#processingPayment').show();
			 	},
			 	success: function(data) {
			 		$('#processingPayment').hide();
			 		if(data.RXRESULTCODE==1){
			 			$('#TemplateRequestModal').modal('hide');
			 			bootbox.dialog({
							message: data.MESSAGE,
							title: "THANK YOU!",
							buttons: {
								success: {
									label: "OK",
									className: "btn btn-medium btn-success-custom",
									callback: function() {}
								}
							}
						});
						$('#template-purpose').val('');
						
						$('.template-slideout').click(); 
			 		}
			 		else{
			 			bootbox.dialog({
							message: data.MESSAGE,
							title: "TEMPLATE REQUEST FORM",
							buttons: {
								success: {
									label: "OK",
									className: "btn btn-medium btn-success-custom",
									callback: function() {}
								}
							}
						});
			 		}	
			 	}
			});			
		}
	}


})(jQuery);