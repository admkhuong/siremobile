
		var _tblListEMS;
		var StatusCustomData = function(){
			var options   = '<option value= "-1" selected>All</option>'+
							'<option value="0">Open</option>'+
							'<option value="1">Assigned</option>'+
							'<option value="2">In progress</option>'+
							'<option value="3">On hold</option>'+
							'<option value="4">Closed</option>';
			return $('<select class="filter-form-control form-control"></select>').append(options);
		},
		phoneCallBack = function(input){ input.mask("(000)000-0000");};
		$(document).ready(function() {
			$(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });
		});

	$('#notify-phone').mask("(000)000-0000");

	$("#edit-trouble-ticket-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});
	$("#notify-trouble-ticket-form").validationEngine('attach', {promptPosition : "topLeft", scroll: false,focusFirstField : true});
	$(document).ready(function(){
		InitControl();
		count_down_character_by_id('notify-email-sub-countdown','notify-email-subject', 255);
		count_down_character_by_id('notify-email-mess-countdown','notify-email-message', 1000);
		count_down_character_by_id('notify-sms-mess-countdown','notify-sms-message', 160);
	});

	$('#notify-method').on('change', function(){
		var data = $('#notify-ticket-id').val();
		InitNotifyForm(data);
	});
	$('#confirm-edit-ticket-btn').click(validate_edit_ticket_form);
	$('#confirm-notify-ticket-btn').click(validate_notify_ticket_form);

	$('#AdminTroubleTicketNotifyModal').on('hidden.bs.modal',function(){
		$('#notify-email-subject').val('');
		$('#notify-email-message').val('');
		$('#notify-sms-message').val('');
		count_down_character_by_id('notify-email-sub-countdown','notify-email-subject', 255);
		count_down_character_by_id('notify-email-mess-countdown','notify-email-message', 1000);
		count_down_character_by_id('notify-sms-mess-countdown','notify-sms-message', 160);
		document.getElementById("notify-trouble-ticket-form").reset();
		$('#notify-trouble-ticket-form').validationEngine('hide');

	});
	function count_down_character_by_id(countId, contentId, limit){
		$('#' + countId).text(limit-$('#'+ contentId).val().length);

		$('#' + contentId).on('keyup', function(){
			$('#' + countId).text(limit-$('#' + contentId).val().length);
		});

		var contentText = document.getElementById(contentId);
		if(contentText) {
		    contentText.addEventListener("input", function() {
		        if (contentText.value.length == limit) {
		        	return false;
		        } 
		        else if (contentText.value.length > limit) {
		        	contentText.value = contentText.value.substring(0, limit);
		        }
		    },  false);
		}

		$('#' + contentId).bind('paste', function(e) {
		    var elem = $(this);
		    setTimeout(function() {
		        var text = elem.val();
		        if (text.length > limit) {
					elem.val(text.substring(0,limit));
		        }
		        $('#' + countId).text(limit-parseInt(elem.val().length));
		    }, 100);
		});
	}

		//Filter bar initialize
	$('#box-filter').sireFilterBar({
		fields: [
			{DISPLAY_NAME: 'Batch Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' sa.BatchId_bi '},
			{DISPLAY_NAME: 'Created date', CF_SQL_TYPE: 'CF_SQL_TIMESTAMP', TYPE: 'DATE', SQL_FIELD_NAME: ' sa.Created_dt '},
			{DISPLAY_NAME: 'User Id', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'INTEGER', SQL_FIELD_NAME: ' sa.UserId_int '},
        	{DISPLAY_NAME: 'Status', CF_SQL_TYPE: 'CF_SQL_INTEGER', TYPE: 'STATUS', SQL_FIELD_NAME: ' sa.Status_int ', CUSTOM_DATA: StatusCustomData},
        	{DISPLAY_NAME: 'Phone', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'PHONE', SQL_FIELD_NAME: ' sa.ContactString_vch ', CALLBACK: phoneCallBack},
        	{DISPLAY_NAME: 'Message', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'TEXT', SQL_FIELD_NAME: ' sa.CustomerMessage_vch '}
		],
		clearButton: true,
		// rowLimited: 5,
		error: function(ele){
			console.log(ele);
		},
		// limited: function(msg){
		// 	alertBox(msg);
		// },
		clearCallback: function(){
			InitControl();
		}
	}, function(filterData){
		//called if filter valid and click to apply button
		InitControl(filterData);
	});

	function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		//init datatable for active agent
		_tblListEMS = $('#tblListEMS').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,
			"sPaginationType": "input",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'ID', "sTitle": 'Ticket No.', "sClass":"ticket-id-th", "bSortable": false},
				{"sName": 'BatchId', "sTitle": 'Batch ID', "sClass":"ticket-batchid-th","bSortable": true},
				{"sName": 'UserId', "sTitle": 'User Id',"sClass":"ticket-userid-th", "bSortable": true},
				{"sName": 'Phone', "sTitle": 'Phone',"sClass":"ticket-phone-th", "bSortable": false},
				{"sName": 'Created', "sTitle": 'Created',"sClass":"ticket-date-th", "bSortable": true},
				{"sName": 'Status', "sTitle": 'Status',"sClass":"ticket-status-th", "bSortable": false},
				{"sName": 'Details', "sTitle": 'Details',"sClass":"ticket-detail-th", "bSortable": false},
				{"sName": 'Note', "sTitle": 'Note',"sClass":"ticket-note-th", "bSortable": false},
				{"sName": 'link', "sTitle": 'Action', "sClass":"ticket-action-th", "bSortable": false}
			],
			"sAjaxDataProp": "ListEMSData",
			"sAjaxSource": '/session/sire/models/cfc/troubleticket.cfc?method=GetTroubleTicket&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				$(nRow).children('td:first').addClass("avatar");

				if (aData && aData[8] == 'Not running') {
					$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
				}
				else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
					$(nRow).removeClass('odd').removeClass('even').addClass('paused');
				}

	            return nRow;
	       	},
		   "fnDrawCallback": function( oSettings ) {
		      $('#tblListEMS').removeAttr('style');
		      if(oSettings._iRecordsDisplay > 0){
		      	$('.dataTables_paginate').show();
		      }
		      else{
		      	$('.dataTables_paginate').hide();
		      }
		    },
			"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
			}
	    });
		
	}
	function isNumber(n) 
	{
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}

	$('body').on('click', '.edit-trouble-ticket', function(event) {
		var data = $(this).data('id');
		$('#ticket-id').val(data);
		InitTicketDetail(data);
		$('#AdminTroubleTicketModal').modal('show');
	});

	$('body').on('click', '.notify-trouble-ticket', function(event) {
		var data = $(this).data('id');
		$('#notify-ticket-id').val(data);
		InitNotifyForm(data);
		$('#AdminTroubleTicketNotifyModal').modal('show');
	});

	function InitTicketDetail(data){
		$.ajax({
			url: '/session/sire/models/cfc/troubleticket.cfc?method=GetTroubleTicketDetailById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'JSON',
			data: {
				inpTicketId:data
			},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(data.RXRESULTCODE == 1){
					$('#user-phone').val(data.PHONE);
					$('#user-request-details').val(data.DETAIL);
					$('#notes-for-request').val(data.NOTE);
					$('#ticket-status > option').each(function(){
						if(parseInt($(this).attr('value')) == parseInt(data.STATUS)){
							$(this).prop('selected', true);
						}
					});
					// $('#user-id-select').val(data.USERID);
					// $("#user-id-select").select2("val", data.USERID);
					$("#user-id-select").append($('<option></option>').html(data.USERID).val(data.USERID).prop('selected', true));
					$("#batch-id-select").append($('<option></option>').html(data.BATCHID).val(data.BATCHID).prop('selected', true));
				}
				else{
					bootbox.dialog({
			         	message: data.MESSAGE,
			         	title: "Trouble ticket details",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	$('#processingPayment').hide();
		     	bootbox.dialog({
		         	message:'Get trouble ticket details failed!',
		         	title: "Trouble ticket details",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
		    } 
		});
		
	}

	function validate_edit_ticket_form(event){
		if ($('#edit-trouble-ticket-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			event.preventDefault();
			var phone = $('#user-phone').val();
			var detail = $('#user-request-details').val();
			var note = $('#notes-for-request').val();
			var status = $("#ticket-status option:selected").val();
			var ticket = $("#ticket-id").val();
			var userid = $("#user-id-select").val();
			var batchid = $("#batch-id-select").val();

			$.ajax({
				url: '/session/sire/models/cfc/troubleticket.cfc?method=UpdateTroubleTicketDetailById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				dataType: 'JSON',
				data:{
					inpTicketId: ticket,
					inpPhone: phone,
					inpDetail: detail,
					inpStatus: status,
					inpNote: note,
					inpUserId: userid,
					inpBatchId: batchid
				},
				async: false,
				beforeSend: function () {
					$("#processingPayment").show();
				},
				success: function(data){
					$("#processingPayment").hide();
					if(data.RXRESULTCODE == 1){
						$('#AdminTroubleTicketModal').modal('hide');
						bootbox.dialog({
				         	message:data.MESSAGE,
				         	title: "Update Trouble ticket details",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
				     	InitControl();
					}
					else{
						bootbox.dialog({
				         	message:data.MESSAGE,
				         	title: "Update Trouble ticket details",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
			    	$('#processingPayment').hide();
			     	bootbox.dialog({
			         	message:'Update trouble ticket details request failed!',
			         	title: "Update Trouble ticket details",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
			    } 
			});
			
		}
	}

	function validate_notify_ticket_form(event){
		if ($('#notify-trouble-ticket-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
			event.preventDefault();
			var phone = $('#notify-phone').val();
			var emailAddress = $('#notify-email-address').val();
			var emailSubject = $("#notify-email-subject").val();
			var emailMessage = $("#notify-email-message").val();
			var SMSMessage = $("#notify-sms-message").val();
			var method = $("#notify-method").val();
			var ticket = $("#notify-ticket-id").val();
			if(method == 1){
				$.ajax({
					url: '/session/sire/models/cfc/troubleticket.cfc?method=SendNotifyEmail&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'JSON',
					data: 
					{
						inpEmail: emailAddress,
						inpMessage: emailMessage,
						inpSubject: emailSubject,
						inpTicketId: ticket
					},
					beforeSend: function () {
						$("#processingPayment").show();
					},
					success: function(data){
						$("#processingPayment").hide();
						if(data.RXRESULTCODE == 1){
							$('#AdminTroubleTicketNotifyModal').modal('hide');
							bootbox.dialog({
					         	message:data.MESSAGE,
					         	title: "Notify trouble ticket",
					         	buttons: {
					             	success: {
					                 	label: "Ok",
					                 	className: "btn btn-medium btn-success-custom",
					                 	callback: function() {}
					             	}
					         	}
					     	});
					     	InitControl();
						}
						else{
							bootbox.dialog({
					         	message:data.MESSAGE,
					         	title: "Notify trouble ticket",
					         	buttons: {
					             	success: {
					                 	label: "Ok",
					                 	className: "btn btn-medium btn-success-custom",
					                 	callback: function() {}
					             	}
					         	}
					     	});
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
				    	$('#processingPayment').hide();
				     	bootbox.dialog({
				         	message:'Send request failed!',
				         	title: "Notify trouble ticket",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
				    } 
				});
				
			}
			else if(method==0){
				$.ajax({
					url: '/session/sire/models/cfc/troubleticket.cfc?method=SendNotifySMS&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					dataType: 'JSON',
					data: 
					{
						inpPhone: phone,
						inpMessage: SMSMessage,
						inpTicketId: ticket
					},
					beforeSend: function () {
						$("#processingPayment").show();
					},
					success: function(data){
						$("#processingPayment").hide();
						if(data.RXRESULTCODE == 1){
							$('#AdminTroubleTicketNotifyModal').modal('hide');
							bootbox.dialog({
					         	message:data.MESSAGE,
					         	title: "Notify trouble ticket",
					         	buttons: {
					             	success: {
					                 	label: "Ok",
					                 	className: "btn btn-medium btn-success-custom",
					                 	callback: function() {}
					             	}
					         	}
					     	});
					     	InitControl();
						}
						else{
							bootbox.dialog({
					         	message:data.MESSAGE,
					         	title: "Notify trouble ticket",
					         	buttons: {
					             	success: {
					                 	label: "Ok",
					                 	className: "btn btn-medium btn-success-custom",
					                 	callback: function() {}
					             	}
					         	}
					     	});
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
				    	$('#processingPayment').hide();
				     	bootbox.dialog({
				         	message:'Send request failed!',
				         	title: "Notify trouble ticket",
				         	buttons: {
				             	success: {
				                 	label: "Ok",
				                 	className: "btn btn-medium btn-success-custom",
				                 	callback: function() {}
				             	}
				         	}
				     	});
				    } 
				});
			}
		}
	}

	function InitNotifyForm(data){
		$.ajax({
			url: '/session/sire/models/cfc/troubleticket.cfc?method=GetTroubleTicketDetailById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			type: 'GET',
			dataType: 'JSON',
			data: {
				inpTicketId:data
			},
			async: false,
			beforeSend: function () {
				$("#processingPayment").show();
			},
			success: function(data){
				$("#processingPayment").hide();
				if(data.RXRESULTCODE == 1){
					$('#notify-phone').val(data.PHONE);
					$('#notify-email-address').val(data.EMAIL);
					// $('#notify-method option[value=0]').attr('selected','select');
				}
				else{
					bootbox.dialog({
			         	message: data.MESSAGE,
			         	title: "Trouble ticket details",
			         	buttons: {
			             	success: {
			                 	label: "Ok",
			                 	className: "btn btn-medium btn-success-custom",
			                 	callback: function() {}
			             	}
			         	}
			     	});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
		    	$('#processingPayment').hide();
		     	bootbox.dialog({
		         	message:'Get trouble ticket details failed!',
		         	title: "Trouble ticket details",
		         	buttons: {
		             	success: {
		                 	label: "Ok",
		                 	className: "btn btn-medium btn-success-custom",
		                 	callback: function() {}
		             	}
		         	}
		     	});
		    } 
		});

		if($('#notify-method').val() == 1){
			$('#notify-email-subject').parents('.form-group').show();
			$('#notify-email-message').parents('.form-group').show();
			$('#notify-sms-message').parents('.form-group').hide();
			$('#notify-phone').parents('.form-group').parent().hide();
			$('#notify-email-address').parents('.form-group').parent().show();
		}

		else{
			$('#notify-email-subject').parents('.form-group').hide();
			$('#notify-email-message').parents('.form-group').hide();
			$('#notify-sms-message').parents('.form-group').show();
			$('#notify-phone').parents('.form-group').parent().show();
			$('#notify-email-address').parents('.form-group').parent().hide();
		}
	}

(function($){

$('#AdminTroubleTicketModal').on('shown.bs.modal', function(){

	$('#user-id-select').select2({
		theme: "bootstrap", 
		width: 'auto',
		ajax: {
			url: "/session/sire/models/cfc/troubleticket.cfc?method=FindUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					inpKeyword: params.term
				};
			},
			processResults: function (data, params) {
				return {
	                results: $.map(data.userlist, function (item) {
	                    return {
	                        text: item.NAME,
	                        id: item.ID
	                    }
	                })
	            };
			},
			cache: true
		},
	  	escapeMarkup: function (markup) {return markup; },
	  	minimumInputLength: 1,
	  	templateResult: function(data){
	  		if(data.hasOwnProperty('loading') && data.loading === true) {
	  			return data.text;
	  		}
	  		return "UserID: <strong>" + data.id + "</strong> - Email: " + data.text;
	  	},
	  	templateSelection: function(data){
	  		return data.id;
	  	}
	});

	$('#batch-id-select').select2({
		theme: "bootstrap", 
		width: 'auto',
		ajax: {
			url: "/session/sire/models/cfc/troubleticket.cfc?method=FindBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					inpKeyword: params.term
				};
			},
			processResults: function (data, params) {
				return {
	                results: $.map(data.batchlist, function (item) {
	                    return {
	                        text: item.NAME,
	                        id: item.ID
	                    }
	                })
	            };
			},
			cache: true
		},
	  	escapeMarkup: function (markup) {return markup; },
	  	minimumInputLength: 1,
	  	templateResult: function(data){
	  		if(data.hasOwnProperty('loading') && data.loading === true) {
	  			return data.text;
	  		}
	  		return "CampaignId: <strong>" + data.id + "</strong> - Campaign name: " + data.text;
	  	},
	  	templateSelection: function(data){
	  		return data.id;
	  	}
	});

});
})(jQuery);
