"use strict";
window.BootstrapEditor = {
    version: "1.1",
    instances: [],
    loaded: false,
    baseURL: "",
    includeFramework: function(b) {
        var a = "css/bootstrap/css/bootstrap.min.css";
        if (b && b.toLowerCase() == "litestrap") {
            a = "css/litestrap/litestrap.css";
        }
        BootstrapEditor.Utils.addLinkToDoc(BootstrapEditor.getBaseURL() + a);
    },
    getBaseURL: function() {
        if (window.BootstrapEditorBaseURL) {
            return window.BootstrapEditorBaseURL;
        }
        var e = /(^|.*[\\\/])bootstrapeditor\.js(?:\?.*|;.*)?$/i;
        var d = "";
        if (!d) {
            var a = document.getElementsByTagName("script");
            for (var c = 0; c < a.length; c++) {
                var b = a[c].src.match(e);
                if (b) {
                    d = b[1];
                    break;
                }
            }
        }
        if (d.indexOf(":/") == -1 && d.slice(0, 2) != "//") {
            if (d.indexOf("/") === 0) {
                d = location.href.match(/^.*?:\/\/[^\/]*/)[0] + d;
            } else {
                d = location.href.match(/^[^\?]*\/(?:)/)[0] + d;
            }
        }
        if (!d) {
            throw "BootstrapEditor: unable to detect base URL of BootstrapEditor. Please define window.BootstrapEditorBaseURL before calling BootstrapEditor.init().";
        }
        return d;
    },
    init: function(c, e) {
        if (!e) {
            e = {};
        }
        e.get = function(g, f) {
            if (g in this) {
                return this[g];
            } else {
                return f;
            }
        };
        e.exists = function(f) {
            return f in this;
        };
        if (!this.loaded) {
            this.baseURL = BootstrapEditor.getBaseURL();
            e["baseURL"] = this.baseURL;
            if (e.get("frameworkInclude", false) == true) {
                BootstrapEditor.includeFramework(e.get("frameworkName", "litestrap"));
                this.frameworkInclude = true;
            }
            BootstrapEditor.Utils.addLinkToDoc(this.baseURL + "bootstrapeditor.css");
            
            

//            BootstrapEditor.Utils.addScriptToDoc(this.baseURL + "plugins/alphamanager/gplugin.js");
//            var d = [];
//            if (e.plugins && typeof e.plugins === "string") {
//                d = e.plugins.trim().split(/\s*,\s*/);
//            }
//            for (var a = 0; a < d.length; a++) {
//                BootstrapEditor.Utils.addScriptToDoc(this.baseURL + "plugins/" + d[a] + "/gplugin.js");
//            }

            window.BootstrapEditor.ColPlugins.initAll(e);
            this.loaded = true;
        }
        var b = this.newInstance(c, e);
        if (b) {
            this.instances.push(b);
            b.init();
            b.show();
        }
        return b;
    },
    newInstance: function(d, f) {
        var c;
        if (typeof d === "string") {
            c = document.getElementById(d);
            if (!c) {
                console.log("BootstrapEditor: Unable to find element with id `" + d + "`");
                return;
            }
        } else {
            c = d;
        }
        var e = null;
        if (c.tagName == "TEXTAREA") {
            c.style.display = "none";
            var a = document.createElement("div");
            a.innerHTML = c.value;
            c.parentElement.insertBefore(a, c);
            e = c;
            c = a;
        }
        if (f.exists("width")) {
            c.style.width = f.get("width", "100%");
        }
        var b = {
            elRoot: c,
            elTextArea: e,
            elCurrentColHover: null,
            elCurrentRowHover: null,
            elColOptions: null,
            elDlgWrap: null,
            elDlgBg: null,
            elPnlMoveRow: null,
            elBtnMoveRowUp: null,
            elBtnMoveRowDown: null,
            params: f,
            framework: BootstrapEditor.Frameworks.Bootstrap,
            getContent: function() {
                var k = document.createElement("div");
                k.innerHTML = this.elRoot.innerHTML;
                for (var m in BootstrapEditor.ColPlugins.plugins) {
                    var l = BootstrapEditor.ColPlugins.plugins[m];
                    var j = k.getElementsByClassName("bootstrapeditor-col-plugin-" + m);
                    for (var h = 0; h < j.length; h++) {
                        l.detach(null, j[h]);
                    }
                }
                var g;
                while ((g = k.getElementsByClassName("bootstrapeditor-el")).length > 0) {
                    g[0].parentNode.removeChild(g[0]);
                }
                return this.hide(k).innerHTML;
            },
            show: function() {
                BootstrapEditor.Utils.addClass(this.elRoot, "bootstrapeditor");
                if (!BootstrapEditor.Utils.hasClass(this.elRoot, this.framework.CLASS_CONTAINER)) {
                    BootstrapEditor.Utils.addClass(this.elRoot, this.framework.CLASS_CONTAINER);
                }
            },
            hide: function(h) {
                if (!h) {
                    h = this.elRoot;
                }
                BootstrapEditor.Utils.removeClass(h, "bootstrapeditor");
                var g = h.getElementsByClassName("bootstrapeditor-el");
                while (g.length > 0) {
                    g[0].parentNode.removeChild(g[0]);
                }
                return h;
            },
            init: function() {
                this.elRoot.setAttribute("data-bootstrapeditor-version", BootstrapEditor.version);
                if (this.params.get("createDefaultRow", false)) {
                    var j = this.elRoot.childNodes;
                    var h = false;
                    for (var k = 0; k < j.length && !h; k++) {
                        if (BootstrapEditor.Utils.hasClass(j[k], this.framework.CLASS_ROW)) {
                            h = true;
                        }
                    }
                    if (!h) {
                        var g = document.createElement("div");
                        g.className = "row";
                        var l = document.createElement("div");
                        l.className = "col-xs-12";
                        l.innerHTML = "&nbsp;";
                        g.appendChild(l);
                        this.elRoot.appendChild(g);
                    }
                }
                this.wrapContentWithRows();
                this.addControls();
                this.registerCols();
                this.attachPluginsToCols();
                this.addSaveAndCancelButtons();
            },
            addSaveAndCancelButtons: function() {
                if (this.params.get("showSaveButton", false) == false && this.params.get("showCancelButton", false) == false) {
                    return;
                }
                var h = document.createElement("div");
                h.className = "bootstrapeditor-el bootstrapeditor-save-and-cancel-buttons";
                this.elRoot.appendChild(h);
                if (this.params.get("showCancelButton", false) == true) {
                    var g = document.createElement("div");
                    g.className = "bootstrapeditor-el bootstrapeditor-btn bootstrapeditor-btn-cancel";
                    g.textContent = this.params.get("cancelButtonTitle", "Cancel");
                    h.appendChild(g);
                    var j = this;
                    h.onclick = (function() {
                        var l = j.params.get("onCancel", null);
                        var k = j;
                        return function() {
                            if (typeof l == "function") {
                                l(k);
                            }
                        };
                    })();
                }
                if (this.params.get("showSaveButton", false) == true) {
                    var i = document.createElement("div");
                    i.className = "bootstrapeditor-el bootstrapeditor-btn bootstrapeditor-btn-save";
                    i.textContent = this.params.get("saveButtonTitle", "Save");
                    h.appendChild(i);
                    var j = this;
                    h.onclick = (function() {
                        var k = j.params.get("onSave", null);
                        var m = j.params.get("onCancel", null);
                        var l = j;
                        return function() {
                            if (typeof k == "function") {
                                k(l);
                            } else {
                                alert("You need to define `onSave` listener");
                            }
                            if (typeof m == "function") {
                                m(l);
                            }
                        };
                    })();
                }
            },
            onAddButtonClick: function(g) {
                BootstrapEditor.Dialogs.open("new-row", this, {
                    elButton: g
                });
            },
            addControls: function() {
                var j = this.elRoot.childNodes;
                var s = document.createElement("div");
                var u = function(w, i) {
                    var v = document.createElement("div");
                    v.className = w.framework.CLASS_ROW + " bootstrapeditor-el bootstrapeditor-row-add";
                    var x = document.createElement("div");
                    x.className = "bootstrapeditor-btn-add";
                    x.textContent = "+";
                    v.appendChild(x);
                    i.appendChild(v);
                };
                u(b, s);
                while (j.length > 0) {
                    s.appendChild(j[0]);
                    u(b, s);
                }
                this.elRoot.innerHTML = s.innerHTML;
                var l = this.elRoot.getElementsByClassName("bootstrapeditor-btn-add");
                for (var n = 0; n < l.length; n++) {
                    l[n].onclick = (function() {
                        var i = b;
                        return function(v) {
                            i.onAddButtonClick(v.target);
                        };
                    })();
                }
                var k = document.createElement("div");
                k.className = "bootstrapeditor-el bootstrapeditor-dialog-bg";
                this.elRoot.appendChild(k);
                var g = document.createElement("div");
                g.className = "bootstrapeditor-el bootstrapeditor-dialog-wrap";
                this.elRoot.appendChild(g);
                this.elDlgWrap = g;
                this.elDlgBg = k;
                var m = document.createElement("div");
                m.className = "bootstrapeditor-el bootstrapeditor-pnl-move-row";
                var o = document.createElement("div");
                o.className = "bootstrapeditor-btn-delete-row";
                o.textContent = "×";
                var r = document.createElement("div");
                r.className = "bootstrapeditor-btn-move-row-up";
                r.textContent = "↑";
                var q = document.createElement("div");
                q.className = "bootstrapeditor-btn-move-row-down";
                q.textContent = "↓";
                m.appendChild(o);
                m.appendChild(document.createElement("br"));
                m.appendChild(r);
                m.appendChild(document.createElement("br"));
                m.appendChild(q);
                this.elRoot.appendChild(m);
                this.elPnlMoveRow = m;
                this.elBtnMoveRowUp = r;
                this.elBtnMoveRowDown = q;
                var p = this;
                o.onclick = (function() {
                    var i = p;
                    return function() {
                        i.deleteHoverRow();
                        
                        $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );
                    };
                })();
                r.onclick = (function() {
                    var i = p;
                    return function() {
                        i.moveHoverRow(-1);
                        
                        $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );
                    };
                })();
                q.onclick = (function() {
                    var i = p;
                    return function() {
                        i.moveHoverRow(1);
                        
                        $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );
                    };
                })();
                var t = document.createElement("div");
                t.className = "bootstrapeditor-el bootstrapeditor-col-options";
                var h = document.createElement("div");
                h.className = "bootstrapeditor-col-option-conf";
                h.textContent = "⌘";
                h.title = "Change type of column";
                t.appendChild(h);
                this.elRoot.appendChild(t);
                this.elColOptions = t;
                h.onclick = (function() {
                    var i = p;
                    return function() {
                        BootstrapEditor.Dialogs.open("col-type", i);
                    };
                })();
            },
            deleteHoverRow: function() {
                var g = this.elCurrentRowHover;
                var h = g.nextSibling;
                g.parentNode.removeChild(g);
                h.parentNode.removeChild(h);
                this.elCurrentRowHover = null;
                this.elCurrentColHover = null;
                BootstrapEditor.Utils.removeClass(this.elColOptions, "bootstrapeditor-show");
                BootstrapEditor.Utils.removeClass(this.elPnlMoveRow, "bootstrapeditor-show");
            },
            moveHoverRow: function(g) {
                var i = this.elCurrentRowHover;
                var h = g == -1 ? this.getPrevRow(i) : this.getNextRow(i);
                if (h == null) {
                    return;
                }
                var j = document.createElement("div");
                while (i.childNodes.length > 0) {
                    j.appendChild(i.childNodes[0]);
                }
                while (h.childNodes.length > 0) {
                    i.appendChild(h.childNodes[0]);
                }
                while (j.childNodes.length > 0) {
                    h.appendChild(j.childNodes[0]);
                }
                BootstrapEditor.Utils.removeClass(this.elColOptions, "bootstrapeditor-show");
                this.updatePnlMoveRowCoord();
            },
            getPrevRow: function(g) {
                var k = this.elRoot.childNodes;
                var l = null;
                for (var j = 0; j < k.length; j++) {
                    var h = k[j];
                    if (h == g) {
                        return l;
                    }
                    if (!BootstrapEditor.Utils.hasClass(h, "bootstrapeditor-el")) {
                        l = h;
                    }
                }
                console.log("BootstrapEditor: unable to find previous row");
            },
            getNextRow: function(g) {
                var k = this.elRoot.childNodes;
                var l = null;
                for (var j = 0; j < k.length; j++) {
                    var h = k[j];
                    if (l == g && !BootstrapEditor.Utils.hasClass(h, "bootstrapeditor-el")) {
                        return h;
                    }
                    if (!BootstrapEditor.Utils.hasClass(h, "bootstrapeditor-el")) {
                        l = h;
                    }
                }
                if (l == g) {
                    return null;
                }
                console.log("BootstrapEditor: unable to find next row");
            },
            registerCols: function() {
                var h = this.getAllCols();
                for (var g = 0; g < h.length; g++) {
                    this.registerCol(h[g]);
                }
            },
            registerCol: function(h) {
                var g = this;
                h.onmouseenter = (function() {
                    var i = h;
                    var j = g;
                    return function() {
                        j.onMouseOverCol(i);
                    };
                })();
                h.onmouseleave = (function() {
                    var i = h;
                    var j = g;
                    return function() {
                        j.onMouseOutCol(i);
                    };
                })();
            },
            updatePnlMoveRowCoord: function() {
                BootstrapEditor.Utils.addClass(this.elPnlMoveRow, "bootstrapeditor-show");
                var j = BootstrapEditor.Utils.getCumulativeOffset(this.elCurrentRowHover);
                var g = BootstrapEditor.Utils.getCumulativeOffset(this.elRoot);
                var i = j.left + this.elCurrentRowHover.offsetWidth + 3 - g.left;
                var h = j.top - g.top;
                BootstrapEditor.Utils.addStyle(this.elPnlMoveRow, "left", i + "px");
                BootstrapEditor.Utils.addStyle(this.elPnlMoveRow, "top", h + "px");
            },
            onMouseOverCol: function(h) {
                this.elCurrentRowHover = h.parentNode;
                this.updatePnlMoveRowCoord();
                if (this.getPrevRow(this.elCurrentRowHover) == null) {
                    BootstrapEditor.Utils.addClass(this.elBtnMoveRowUp, "bootstrapeditor-btn-disabled");
                } else {
                    BootstrapEditor.Utils.removeClass(this.elBtnMoveRowUp, "bootstrapeditor-btn-disabled");
                }
                if (this.getNextRow(this.elCurrentRowHover) == null) {
                    BootstrapEditor.Utils.addClass(this.elBtnMoveRowDown, "bootstrapeditor-btn-disabled");
                } else {
                    BootstrapEditor.Utils.removeClass(this.elBtnMoveRowDown, "bootstrapeditor-btn-disabled");
                }
                this.elCurrentColHover = h;
                BootstrapEditor.Utils.addClass(this.elColOptions, "bootstrapeditor-show");
                var k = BootstrapEditor.Utils.getCumulativeOffset(h);
                var g = BootstrapEditor.Utils.getCumulativeOffset(this.elRoot);
                var j = k.left + h.offsetWidth - 30 - g.left;
                var i = k.top - 27 - g.top;
                BootstrapEditor.Utils.addStyle(this.elColOptions, "left", j + "px");
                BootstrapEditor.Utils.addStyle(this.elColOptions, "top", i + "px");
                BootstrapEditor.ColPlugins.onMouseOverCol(this, h);
            },
            onMouseOutCol: function(g) {
                BootstrapEditor.ColPlugins.onMouseOutCol(this, g);
            },
            wrapContentWithRows: function() {
                var h = function(p, r, i) {
                    var o = null;
                    var q = null;
                    if (r.length > 0) {
                        o = document.createElement("div");
                        o.className = p.framework.CLASS_ROW;
                        q = document.createElement("div");
                        q.className = p.framework.getColumnClass(12);
                        o.appendChild(q);
                        for (var s = 0; s < r.length; s++) {
                            q.appendChild(r[s]);
                        }
                        i.appendChild(o);
                    }
                };
                var k = this.elRoot.childNodes;
                var j = [];
                for (var l = 0; l < k.length; l++) {
                    j.push(k[l]);
                }
                var n = [];
                var m = null;
                var g = document.createElement("div");
                for (var l = 0; l < j.length; l++) {
                    m = j[l];
                    if (BootstrapEditor.Utils.hasClass(m, this.framework.CLASS_ROW)) {
                        h(this, n, g);
                        n = [];
                        g.appendChild(m);
                    } else {
                        if (m.nodeType != 3 || m.textContent.trim().length > 0) {
                            n.push(m);
                        }
                    }
                }
                h(this, n, g);
                this.elRoot.innerHTML = g.innerHTML;
            },
            getAllCols: function() {
                var l = [];
                for (var k = 0; k < this.elRoot.childNodes.length; k++) {
                    var g = this.elRoot.childNodes[k];
                    if (!BootstrapEditor.Utils.hasClass(g, "bootstrapeditor-el")) {
                        for (var h = 0; h < g.childNodes.length; h++) {
                            var m = g.childNodes[h];
                            if (m.nodeType != 1) {
                                continue;
                            }
                            l.push(m);
                        }
                    }
                }
                return l;
            },
            attachPluginsToCols: function() {
                var h = this.getAllCols();
                for (var g = 0; g < h.length; g++) {
                    var j = h[g];
                    var k = BootstrapEditor.ColPlugins.getColPluginName(j);
                    if (k == null) {
                        k = BootstrapEditor.ColPlugins.getDefaultColPluginFor(j);
                        BootstrapEditor.Utils.addClass(j, "bootstrapeditor-col-plugin-" + k);
                    }
                    if (k != "editor") {
                        BootstrapEditor.ColPlugins.plugins["editor"].detach(this, j);
                    }
                    BootstrapEditor.ColPlugins.attachToCol(b, k, j);
                }
            }
        };
        return b;
    }
};
window.BootstrapEditor.Frameworks = {};
window.BootstrapEditor.Frameworks.Bootstrap = {
    CLASS_CONTAINER: "container",
    CLASS_ROW: "row",
    getColumnClass: function(a) {
        return "col-xs-" + a;
    }
};
window.BootstrapEditor.Utils = {
    setOnChangeFunc: function(b, a) {
        b.onchange = a;
        b.onkeyup = a;
        b.onpaste = a;
    },
    getCumulativeOffset: function(f) {
        var g = f.getBoundingClientRect();
        var h = document.body;
        var b = document.documentElement;
        var a = window.pageYOffset || b.scrollTop || h.scrollTop;
        var d = window.pageXOffset || b.scrollLeft || h.scrollLeft;
        var e = b.clientTop || h.clientTop || 0;
        var i = b.clientLeft || h.clientLeft || 0;
        var j = g.top + a - e;
        var c = g.left + d - i;
        return {
            top: Math.round(j),
            left: Math.round(c)
        };
    },
    number: 1,
    nextNumber: function() {
        this.number++;
        return this.number;
    },
    createElementsFromHtml: function(a) {
        var b = document.createElement("div");
        b.innerHTML = a;
        return b.childNodes;
    },
    getDocHtml: function() {
        return document.getElementsByTagName("html")[0];
    },
    getDocHead: function() {
        return document.getElementsByTagName("head")[0];
    },
    getDocBody: function() {
        return document.getElementsByTagName("body")[0];
    },
    addLinkToDoc: function(e) {
        if (!document) {
            return;
        }
        var a = document.getElementsByTagName("link");
        var d = false;
        for (var b = 0; b < a.length; b++) {
            if (a[b].href.indexOf(e) != -1) {
                d = true;
            }
        }
        if (!d) {
            var c = document.createElement("link");
            c.href = e;
            c.type = "text/css";
            c.rel = "stylesheet";
            this.getDocHead().appendChild(c);
        }
    },
    addScriptToDoc: function(e) {
        var a = document.getElementsByTagName("script");
        var d = false;
        for (var c = 0; c < a.length; c++) {
            if (a[c].src.indexOf(e) != -1) {
                d = true;
            }
        }
        if (!d) {
            var b = document.createElement("script");
            b.src = e;
            b.type = "text/javascript";
            this.getDocHead().appendChild(b);
        }
    },
    embedCssStylesToDoc: function(a) {
        var b = document.createElement("style");
        this.getDocHead().appendChild(b);
        b.innerHTML = a;
    },
    addClass: function(b, a) {
        if (this.hasClass(b, a)) {
            return;
        }
        b.className = b.className.length == 0 ? a : b.className + " " + a;
    },
    removeClass: function(d, a) {
        var c = this.getClasses(d);
        var e = "";
        for (var b = 0; b < c.length; b++) {
            if (e.length > 0) {
                e += " ";
            }
            if (c[b] != a) {
                e += c[b];
            }
        }
        d.className = e;
        d.className = d.className.trim();
    },
    getClasses: function(a) {
        if (typeof(a.className) === "undefined" || a.className == null) {
            return [];
        }
        return a.className.split(/\s+/);
    },
    setClasses: function(b, a) {
        if (a.length == 0) {
            b.removeAttribute("class");
        } else {
            b.className = a.join(" ");
        }
    },
    hasClass: function(d, a) {
        var c = this.getClasses(d);
        for (var b = 0; b < c.length; b++) {
            if (c[b].toLowerCase() == a.toLowerCase()) {
                return true;
            }
        }
        return false;
    },
    hasClassStartsWith: function(c, d) {
        var b = this.getClasses(c);
        for (var a = 0; a < b.length; a++) {
            if (b[a].indexOf(d) === 0) {
                return true;
            }
        }
        return false;
    },
    getStyles: function(c) {
        if (typeof(c.getAttribute("style")) === "undefined" || c.getAttribute("style") == null) {
            return {};
        }
        var e = {};
        var d = c.getAttribute("style").split(/;/);
        for (var b = 0; b < d.length; b++) {
            var f = d[b].trim();
            var a = f.indexOf(":");
            if (a > -1) {
                e[f.substr(0, a).trim()] = f.substr(a + 1);
            } else {
                e[f] = "";
            }
        }
        return e;
    },
    addStyle: function(d, c, a) {
        var e = this.getStyles(d);
        var h = "";
        var g = false;
        for (var b in e) {
            if (h.length > 0) {
                h += ";";
            }
            var f = e[b];
            if (b == c) {
                g = true;
                h += c + ":" + a;
            } else {
                h += b + ":" + f;
            }
        }
        if (!g) {
            if (h.length > 0) {
                h += ";";
            }
            h += c + ":" + a;
        }
        d.setAttribute("style", h);
    },
    hasStyle: function(d, c, a) {
        var e = this.getStyles(d);
        for (var b in e) {
            var f = e[b];
            if (b == c && f == a) {
                return true;
            }
        }
        return false;
    },
    toHumanSize: function(a) {
        if (typeof(a) == "undefined") {
            return "";
        }
        var d = 1000;
        if (a < d) {
            return a + " b";
        }
        var b = ["Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb"];
        var c = -1;
        do {
            a /= d;
            ++c;
        } while (a >= d);
        return a.toFixed(1) + " " + b[c];
    },
    escapeHtml: function(a) {
        return a.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }
};
window.BootstrapEditor.Dialogs = {
    dialogs: {},
    params: {},
    open: function(b, c, e) {
        var d = this.dialogs[b];
        if (e) {
            d.params = e;
        } else {
            d.params = {};
        }
        d.editor = c;
        var a = c.elDlgWrap;
        d.open(a);
        BootstrapEditor.Utils.addClass(c.elDlgBg, "bootstrapeditor-show");
        BootstrapEditor.Utils.addClass(a, "bootstrapeditor-show");
    },
    add: function(u, s, l, o, g, r, h, d, c, q) {
        var m = "";
        var t = {};
        for (var n = o.length - 1; n >= 0; n--) {
            var a = o[n];
            var f = "bootstrapeditor-dlg-btn-" + BootstrapEditor.Utils.nextNumber();
            var e = null;
            var k;
            if (a.type == "ok") {
                e = -1;
                k = "ok";
            } else {
                if (a.type == "cancel") {
                    e = -2;
                    k = "cancel";
                } else {
                    if (a.type == "custom" && typeof(a.onclick) != "undefined" && a.onclick != null) {
                        e = a.onclick;
                        k = a.name;
                    }
                }
            }
            t[k] = e;
            m += '<div class="bootstrapeditor-dlg-btn bootstrapeditor-dlg-btn-' + k + '">' + BootstrapEditor.Utils.escapeHtml(a.title) + "</div>";
        }
        var j = '<div class="bootstrapeditor-dlg">' + '<div class="bootstrapeditor-dlg-title">' + '<div class="bootstrapeditor-dlg-title-text">' + BootstrapEditor.Utils.escapeHtml(s) + "</div>" + '<div class="bootstrapeditor-dlg-btn-close">×</div>' + "</div>" + '<div class="bootstrapeditor-dlg-body">' + g + "</div>" + '<div class="bootstrapeditor-dlg-buttons">' + m + "</div>" + "</div>";
        var b = BootstrapEditor.Utils.createElementsFromHtml(j)[0];
        var p = {
            elDialog: b,
            editor: null,
            width: l,
            isFirstOpen: true,
            getBtn: function(i) {
                return this.elDialog.getElementsByClassName("bootstrapeditor-dlg-btn-" + i)[0];
            },
            setBtnEnabled: function(x, i) {
                var v = this.getBtn(x);
                if (i) {
                    BootstrapEditor.Utils.removeClass(v, "bootstrapeditor-dlg-btn-disabled");
                } else {
                    BootstrapEditor.Utils.addClass(v, "bootstrapeditor-dlg-btn-disabled");
                }
                if (x == "cancel") {
                    var w = this.elDialog.getElementsByClassName("bootstrapeditor-dlg-btn-close");
                    if (i) {
                        BootstrapEditor.Utils.removeClass(w, "bootstrapeditor-dlg-btn-disabled");
                    } else {
                        BootstrapEditor.Utils.addClass(w, "bootstrapeditor-dlg-btn-disabled");
                    }
                }
            },
            isBtnEnabled: function(v) {
                var i = this.getBtn(v);
                return !BootstrapEditor.Utils.hasClass(i, "bootstrapeditor-dlg-btn-disabled");
            },
            open: function(i) {
                i.innerHTML = "";
                if (this.width != null) {
                    BootstrapEditor.Utils.addStyle(this.elDialog, "width", l);
                }
                i.appendChild(this.elDialog);
                var x = this;
                for (var y in t) {
                    var v = t[y];
                    if (v != null) {
                        var w = p.getBtn(y);
                        if (v === -1) {
                            w.onclick = function() {
                                x.ok();
                            };
                        } else {
                            if (v === -2) {
                                w.onclick = function() {
                                    x.cancel();
                                };
                            } else {
                                w.onclick = function() {
                                    v();
                                };
                            }
                        }
                    }
                }
                x.elDialog.getElementsByClassName("bootstrapeditor-dlg-btn-close")[0].onclick = (function() {
                    var z = x;
                    return function() {
                        x.cancel();
                    };
                })();
                this.appendedToDOM = true;
                if (this.isFirstOpen && q != null) {
                    q(this);
                }
                this.isFirstOpen = false;
                if (d != null) {
                    d(this);
                }
            },
            close: function() {
                BootstrapEditor.Utils.removeClass(this.editor.elDlgWrap, "bootstrapeditor-show");
                BootstrapEditor.Utils.removeClass(this.editor.elDlgBg, "bootstrapeditor-show");
                if (c != null) {
                    c(this);
                }
            },
            ok: function() {
                if (!this.isBtnEnabled("ok")) {
                    return;
                }
                if (r != null) {
                    if (r(this) === false) {
                        return;
                    }
                }
                p.close();
            },
            cancel: function() {
                if (!this.isBtnEnabled("cancel")) {
                    return;
                }
                if (h != null) {
                    if (h(this) === false) {
                        return;
                    }
                }
                this.close();
            }
        };
        this.dialogs[u] = p;
        return p;
    }
};
window.BootstrapEditor.Dialogs.add("col-type", "Change type of column", "80%", [{
    title: "OK",
    type: "ok"
}, {
    title: "Cancel",
    type: "cancel"
}], '<div class="bootstrapeditor-dlg-change-col-type-plugins">' + "</div>", function(b) {
    var a = b.editor.elCurrentColHover;
    BootstrapEditor.ColPlugins.attachToCol(b.editor, b.selectedPlugin, a);
}, function(a) {}, function(e) {
    var a = e.elDialog.getElementsByClassName("bootstrapeditor-dlg-change-col-type-plugin");
    var c = e.editor.elCurrentColHover;
    var f = BootstrapEditor.ColPlugins.getColPluginName(c);
    for (var b = 0; b < a.length; b++) {
        var d = a[b];
        if (d.getAttribute("data-plugin") == f) {
            BootstrapEditor.Utils.addClass(d, "bootstrapeditor-dlg-change-col-type-plugin-selected");
            e.selectedPlugin = f;
            e.oldPlugin = f;
        } else {
            BootstrapEditor.Utils.removeClass(d, "bootstrapeditor-dlg-change-col-type-plugin-selected");
        }
    }
    e.setBtnEnabled("ok", false);
}, function(a) {}, function(g) {
    var h = g.elDialog.getElementsByClassName("bootstrapeditor-dlg-body")[0].childNodes[0];
    for (var f in BootstrapEditor.ColPlugins.plugins) {
        var e = BootstrapEditor.ColPlugins.plugins[f];
        var k = e.getTitle();
        var j = e.getHtmlIcon();
        var a = document.createElement("table");
        a.className = "bootstrapeditor-dlg-change-col-type-plugin";
        a.setAttribute("data-plugin", f);
        var b = BootstrapEditor.ColPlugins.getColPluginName(g.editor.elCurrentColHover);
        a.innerHTML = "<tbody>" + "<tr>" + "<td>" + '<div class="bootstrapeditor-dlg-change-col-type-plugin-icon">' + j + "</div>" + "</td>" + "</tr>" + "<tr>" + "<tr>" + "<td>" + "</div>" + '<div class="bootstrapeditor-dlg-change-col-type-plugin-title">' + BootstrapEditor.Utils.escapeHtml(k) + "</div>";
        "</td>" + "</tr>" + "</tbody>";
        h.appendChild(a);
        var d = h.getElementsByClassName("bootstrapeditor-dlg-change-col-type-plugin");
        for (var c = 0; c < d.length; c++) {
            a = d[c];
            a.onclick = (function() {
                var l = g;
                var i = a;
                return function() {
                    var o = i.getAttribute("data-plugin");
                    var m = l.elDialog.getElementsByClassName("bootstrapeditor-dlg-change-col-type-plugin");
                    for (var n = 0; n < m.length; n++) {
                        var p = m[n].getAttribute("data-plugin");
                        if (p == o) {
                            BootstrapEditor.Utils.addClass(m[n], "bootstrapeditor-dlg-change-col-type-plugin-selected");
                            l.selectedPlugin = p;
                        } else {
                            BootstrapEditor.Utils.removeClass(m[n], "bootstrapeditor-dlg-change-col-type-plugin-selected");
                        }
                    }
                    l.setBtnEnabled("ok", l.selectedPlugin != l.oldPlugin);
                };
            })();
        }
    }
});
window.BootstrapEditor.Dialogs.add("new-row", "Add new row", "600px", [{
    title: "Cancel",
    type: "cancel"
}], '<div class="row bootstrapeditor-mockup-row">' + '<div class="bootstrapeditor-mockups" style="width:480">' + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="12">' + '<div class="col-xs-12"><div>12</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="6-6">' + '<div class="col-xs-6"><div>6</div></div>' + '<div class="col-xs-6"><div>6</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="4-4-4">' + '<div class="col-xs-4"><div>4</div></div>' + '<div class="col-xs-4"><div>4</div></div>' + '<div class="col-xs-4"><div>4</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block"  data-cols="3-3-3-3">' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + "</div>" + "</div>" + "</div>" + "</div>" + '<div class="row bootstrapeditor-mockup-row">' + '<div class="bootstrapeditor-mockups" style="width:360px">' + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="6-3-3">' + '<div class="col-xs-6"><div>6</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="3-6-3">' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-6"><div>6</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="3-3-6">' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-6"><div>6</div></div>' + "</div>" + "</div>" + "</div>" + "</div>" + '<div class="row bootstrapeditor-mockup-row">' + '<div class="bootstrapeditor-mockups" style="width:360px">' + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="9-3">' + '<div class="col-xs-9"><div>9</div></div>' + '<div class="col-xs-3"><div>3</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="8-4">' + '<div class="col-xs-8"><div>8</div></div>' + '<div class="col-xs-4"><div>4</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="7-5">' + '<div class="col-xs-7"><div>7</div></div>' + '<div class="col-xs-5"><div>5</div></div>' + "</div>" + "</div>" + "</div>" + "</div>" + '<div class="row bootstrapeditor-mockup-row">' + '<div class="bootstrapeditor-mockups" style="width:360px">' + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="3-9">' + '<div class="col-xs-3"><div>3</div></div>' + '<div class="col-xs-9"><div>9</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="4-8">' + '<div class="col-xs-4"><div>4</div></div>' + '<div class="col-xs-8"><div>8</div></div>' + "</div>" + "</div>" + '<div class="bootstrapeditor-mockup-block-wrap">' + '<div class="row bootstrapeditor-mockup-block" data-cols="5-7">' + '<div class="col-xs-5"><div>5</div></div>' + '<div class="col-xs-7"><div>7</div></div>' + "</div>" + "</div>" + "</div>" + "</div>", function(a) {}, function(a) {}, function(a) {}, function(a) {}, function(d) {
    var b = d.elDialog.getElementsByClassName("bootstrapeditor-mockup-block");
    var c = function(k, m) {
        var o = k.params.elButton.parentNode;
        var e = document.createElement("div");
        e.className = d.editor.framework.CLASS_ROW + " bootstrapeditor-el bootstrapeditor-row-add";
        var j = document.createElement("div");
        j.className = "bootstrapeditor-btn-add";
        j.textContent = "+";
        e.appendChild(j);
        o.parentNode.insertBefore(e, o);
        j.onclick = (function() {
            var i = k.editor;
            return function(p) {
                i.onAddButtonClick(p.target);
            };
        })();
        e = document.createElement("div");
        e.className = d.editor.framework.CLASS_ROW + " bootstrapeditor-row-content";
        var l = m.split("-");
        var f;
        var g;
        var n;
        for (var h = 0; h < l.length; h++) {
            f = parseInt(l[h]);
            g = document.createElement("div");
            g.className = k.editor.framework.getColumnClass(f) + " bootstrapeditor-col-plugin-editor";
            g.textContent = "Column " + f + "/12";
            e.appendChild(g);
            k.editor.registerCol(g);
            BootstrapEditor.ColPlugins.attachToCol(d.editor, "editor", g);
        }
        o.parentNode.insertBefore(e, o);
        k.close();
    };
    for (var a = 0; a < b.length; a++) {
        b[a].onclick = (function() {
            var e = d;
            var f = b[a].getAttribute("data-cols");
            return function() {
                c(e, f);
            };
        })();
    }
});
window.BootstrapEditor.ColPlugins = {
    defaultPlugin: "editor",
    plugins: {},
    add: function(b, a) {
        this.plugins[b] = a;
    },
    initAll: function(c) {
        var a;
        for (var b in this.plugins) {
            a = this.plugins[b];
            a.init(c);
        }
    },
    getDefaultColPluginFor: function(a) {
        if (a.childNodes.length == 1) {
            var b = a.childNodes[0];
            if (b.tagName == "IMG") {
                return "image";
            }
            if (b.tagName == "A") {
                if (b.childNodes.length == 1 && b.childNodes[0].tagName == "IMG") {
                    return "image";
                }
            }
        }
        return this.defaultPlugin;
    },
    getColPluginName: function(c) {
        var b = BootstrapEditor.Utils.getClasses(c);
        for (var a = 0; a < b.length; a++) {
            if (b[a].indexOf("bootstrapeditor-col-plugin-") > -1) {
                return b[a].substr("bootstrapeditor-col-plugin-".length, b[a].length - "bootstrapeditor-col-plugin-".length);
            }
        }
        return null;
    },
    attachToCol: function(a, d, c) {
        var b = this.plugins[d];
        if (!b) {
            console.log("BootstrapEditor: unable to find plugin (" + d + ")");
            if (d == this.defaultPlugin) {
                console.log("BootstrapEditor fatal: unable to find default plugin (" + d + ")");
                return;
            }
            d = BootstrapEditor.ColPlugins.defaultPlugin;
            b = this.plugins[d];
        }
        var e = this.getColPluginName(c);
        if (e != null && e != d) {
            var f = this.plugins[e];
            if (f) {
                f.detach(a, c);
            }
            BootstrapEditor.Utils.removeClass(c, "bootstrapeditor-col-plugin-" + e);
            BootstrapEditor.Utils.addClass(c, "bootstrapeditor-col-plugin-" + d);
        }
        b.attach(a, c);
    },
    onMouseOverCol: function(a, b) {
        var c = this.getColPluginName(b);
        if (this.plugins[c]) {
            this.plugins[c].onMouseOverCol(a, b);
        }
    },
    onMouseOutCol: function(a, b) {
        var c = this.getColPluginName(b);
        if (this.plugins[c]) {
            this.plugins[c].onMouseOutCol(a, b);
        }
    }
};
window.BootstrapEditor.ColPlugins.add("editor", {
    ckeditorConfig: {},
    getTitle: function() {
        return "HTML Content";
    },
    getHtmlIcon: function() {
        return '<div style="float:left;width:80px;height:80px;text-align: center;background-color: #8A67C9; padding:6px 5px">' + '<div style="width:100%;margin-top:5px;height:4px;width:25px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;width:25px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;width:25px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;width:25px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:45px; height:35px;float:right;position: relative;top:-59px;display: inline-block;padding: 0 0 3px 5px">' + '<div style="width:38px;height:32px;background-color: #8A67C9;border:1px solid white; overflow: hidden">' + '<div style="border:1px solid white; width:35px;height:35px;-ms-transform: rotate:45deg);background-color: white;' + '-webkit-transform: rotate(45deg); transform: rotate(45deg);position:relative;top:15px;left:-4px"></div>' + '<div style="border:1px solid #8A67C9;width:20px;height:20px;-ms-transform: rotate:45deg);background-color: white;' + '-webkit-transform: rotate(45deg); transform: rotate(45deg);position:relative;top:-15px;left:21px"></div>' + '<div style="width:7px;height:7px;border-radius:4px;border:1px solid white;background-color:white;' + 'position: relative;top: -50px;left: 25px;"></div>' + "</div>" + "</div>";
    },
    init: function(b) {
        this.ckeditorConfig = b.get("CKEditorConfig", {});
        var a = b.get("includeCKEditor", true);
        if (a !== false) {
            BootstrapEditor.Utils.addScriptToDoc(b.get("baseURL") + "plugins/editor/ckeditor/ckeditor.js");
            
        }
    },
    attach: function(a, b) {
        b.setAttribute("contenteditable", "true");
        if (window.CKEDITOR) {
            CKEDITOR.inline(b, this.ckeditorConfig);
                    
            // JLP Mods*
            // Only add latest added editor or on change event will get fired multiple times
            // This also needs to be set on load            
                    
            for (var i in CKEDITOR.instances) 
            {	           	            
	            if(!$(CKEDITOR.instances[i]).attr('OnChangeSet'));  
	            {
		        	CKEDITOR.instances[i].on('change', function() { $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') ); });
		        	$(CKEDITOR.instances[i]).attr('OnChangeSet', true);
	            }
	                   
	        }
            
            $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );                 

        }
    },
    detach: function(c, d) {
        if (window.CKEDITOR) {
            for (var e in CKEDITOR.instances) {
                if ((typeof CKEDITOR.instances[e]._.editable !== "undefined") && CKEDITOR.instances[e]._.editable.$ == d) {
                    CKEDITOR.instances[e].destroy();
                }
            }
        }
        d.removeAttribute("contenteditable");
        d.removeAttribute("role");
        d.removeAttribute("tabindex");
        d.removeAttribute("spellcheck");
        d.removeAttribute("aria-label");
        d.removeAttribute("title");
        var b = BootstrapEditor.Utils.getClasses(d);
        var f = [];
        for (var a = 0; a < b.length; a++) {
            if (b[a].indexOf("cke_") !== 0) {
                f.push(b[a]);
            }
        }
        BootstrapEditor.Utils.setClasses(d, f);
        BootstrapEditor.Utils.removeClass(d, "bootstrapeditor-col-plugin-editor");
                
    },
    onMouseOverCol: function(a, b) {},
    onMouseOutCol: function(a, b) {}
});
window.BootstrapEditor.ColPlugins.add("text", {
    getTitle: function() {
        return "Plain text";
    },
    getHtmlIcon: function() {
        return '<div style="float:left;width:80px;height:80px;text-align: center;background-color: #8A67C9; padding:6px 5px">' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + '<div style="width:100%;margin-top:5px;height:4px;background-color: white"></div>' + "</div>";
    },
    init: function(a) {},
    attach: function(a, b) {
        var c = b.textContent;
        b.innerHTML = "";
        b.textContent = c;
        b.setAttribute("contenteditable", "true");
    },
    detach: function(a, b) {
        b.removeAttribute("contenteditable");
        BootstrapEditor.Utils.removeClass(b, "bootstrapeditor-col-plugin-text");
    },
    onMouseOverCol: function(a, b) {},
    onMouseOutCol: function(a, b) {}
});
window.BootstrapEditor.ColPlugins.add("none", {
    getTitle: function() {
        return "None";
    },
    getHtmlIcon: function() {
        return '<div style="float:left;width:80px;height:80px;text-align: center;background-color: white;border:1px solid #8A67C9;overflow:hidden">' + '<div style="border:1px solid #8A67C9;width:120px;height:120px;-ms-transform: rotate:45deg);' + '-webkit-transform: rotate(45deg); transform: rotate(45deg);position:relative;top:20px;left:22px"></div>' + '<div style="border:1px solid #8A67C9;width:120px;height:120px;-ms-transform: rotate:45deg);' + '-webkit-transform: rotate(45deg); transform: rotate(45deg);position:relative;top:-100px;left:-64px"></div>' + "</div>" + "</div>";
    },
    init: function(a) {},
    attach: function(a, b) {
        b.innerHTML = '<div class="bootstrapeditor-el">No content<br/>Change type of the column to make it editable</div>';
    },
    detach: function(a, b) {
        b.innerHTML = "";
        BootstrapEditor.Utils.removeClass(b, "bootstrapeditor-col-plugin-none");
    },
    onMouseOverCol: function(a, b) {},
    onMouseOutCol: function(a, b) {}
});
// window.demoLoaded();