/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		//  { name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced;image:Link';
	
	// Forms plug in is screwing up hidden fields - may have more to do with how bootstrap editor is grabbing content. innerHTML vs using getdata 
	config.removePlugins = 'forms';

	// config.extraPlugins = 'filebrowser';

	config.filebrowserBrowseUrl = '/session/sire/models/cfm/imagebrowser.cfm';

	config.filebrowserUploadUrl = '/session/sire/models/cfm/imageupload.cfm';
	
	config.allowedContent = true;
	
	config.fillEmptyBlocks = false;
	// config.basicEntities = false;
};


// JLP Mods*
// For each editor on the stage - set the on change event to fire	

for (var i in CKEDITOR.instances) {  
    // CKEDITOR.instances[i].unbind('change');
    if($('#PreviewMLP').length){
    	CKEDITOR.instances[i].on('change', function() { $('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );});
    	$(CKEDITOR.instances[i]).attr('OnChangeSet', true);
    } else {
    	CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
    }
}