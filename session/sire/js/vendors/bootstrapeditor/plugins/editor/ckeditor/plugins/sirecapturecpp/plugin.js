CKEDITOR.plugins.add( 'Sire', {
    icons: 'Sire',
    init: function( editor ) {
        editor.addCommand( 'sirecppcapture', new CKEDITOR.dialogCommand( 'SireDialog' ) );
        editor.ui.addButton( 'Sire', {
            label: 'Insert Abbreviation',
            command: 'sire',
            toolbar: 'insert'
        });

        CKEDITOR.dialog.add( 'SireDialog', this.path + 'dialogs/sire.js' );
    }
});