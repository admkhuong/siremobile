(function($){
    var optionsSubscriberList = $('input[type=radio][name=optionsSubscriberList]').change(function() {
        var total_subcribers = $(this).data('total-subcribers');
        var subcribers_txt = ' subcriber';
        if(total_subcribers > 1) subcribers_txt+='s';
        $("#num_subscriber").html('Send to ' + total_subcribers + subcribers_txt);
    });
    optionsSubscriberList.filter(':checked').change();

	var answersList = $('#answersList .answerItem');
	var num_answer = $('#num_answer').change(function(){
		var num = num_answer.val();
		answersList = $('#answersList .answerItem');
		answersList.filter(function( index ) {
    		return index < num;
  		}).show();
  		answersList.filter(function( index ) {
    		return index >= num;
  		}).hide();
		if (num_answer.val()<=2) {
			$('#answersList .answerItemDelete').hide();
		} else {
			$('#answersList .answerItemDelete').show();
		}
		$('#question_text').trigger('input');
	});
	num_answer.change();

	var answerItemShortCharList = $('#answersList .answerItemShortChar');
	var format_answer = $('#format_answer').change(function(){
		var format = format_answer.val();
		answerItemShortCharList = $('#answersList .answerItemShortChar');
		switch(format) {
			default:
			case 'NUMERICPAR':
				answerItemShortCharList.each(function(id, el){
					$(el).text(id + 1 + ')');
				});
				break;
			case 'NUMERIC':
				answerItemShortCharList.each(function(id, el){
					$(el).text(id + 1 + ' for');
				});
				break;
			case 'ALPHAPAR':
				answerItemShortCharList.each(function(id, el){
					$(el).text(String.fromCharCode(65+id) + ')');
				});
				break;
			case 'ALPHA':
				answerItemShortCharList.each(function(id, el){
					$(el).text(String.fromCharCode(65+id) + ' for');
				});
				break;
		}
		$('#question_text').trigger('input');
	});
	format_answer.change();
	
	$('#answersList .answerItemDelete').click(function(){
		var answerItem = $(this).parent().hide();
		answerItem.find('.answerItemText').val('');
		answersList = $('#answersList .answerItem');
		answersList.last().after(answerItem);
		format_answer.change();
		num_answer.val(num_answer.val()-1);
		if (num_answer.val()<=2) {
			$('#answersList .answerItemDelete').hide();
		} else {
			$('#answersList .answerItemDelete').show();
		}
	});

	$('#question_text, #answersList .answerItemText').on('input', function(){
		if ($.trim($('#question_text').val()) == '') {
			$('#SMSHistoryScreenArea .bubble').hide();
		} else {
			var bubble = $('#SMSHistoryScreenArea .bubble').css({
				'display': 'block',
				'unicode-bidi': 'embed',
				'white-space': 'pre'
			}).show();
			answersList = $('#answersList .answerItemText:visible');
			bubble.text($('#question_text').val() + '\n' + 
				$('#answersList .answerItemText:visible').map(function(id, el){
					var format = format_answer.val();
					switch(format) {
						default:
						case 'NUMERICPAR':
							return id + 1 + ') ' + $(this).val();
							break;
						case 'NUMERIC':
							return id + 1 + ' for ' + $(this).val();
							break;
						case 'ALPHAPAR':
							return String.fromCharCode(65+id) + ') ' + $(this).val();
							break;
						case 'ALPHA':
							return String.fromCharCode(65+id) + ' for ' + $(this).val();
							break;
					}
				}).get().join('\n'));
		}
	});
	$('#question_text').trigger('input');

})(jQuery);