$(document).ready(function($) {
	$('.si-config .tab-title ul li').click(function(event) {
		var index = $(this).index();
		$('.si-config .tab-title ul li').removeClass('active');
		$(this).addClass('active');
		$('.si-config .tab-content ul li').hide();
		$('.si-config .tab-content ul li:nth-child('+ ( index + 1 ) +')').fadeIn();
	});
});