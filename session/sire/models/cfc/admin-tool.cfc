<cfcomponent hint="Admin Tool Feature" output="false">

	<cfinclude template="/public/sire/configs/paths.cfm"/>

	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cffunction name="GetUserList" access="remote" output="true" hint="get user list with filter">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />				
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="ASC"/>				
		<cfargument name="inpSegment" default=""/>
		<cfargument name="customFilter" default=""/>

		<cfargument name="inpListUserBySegment" default="0"/>

		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />		
		<cfset var order = ''/>
		<cfset var filterData = DeserializeJSON(arguments.customFilter) />		
		<cfset var GetUserList = "" />
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<cfif !REFind("^\d+(,\d+)*$", arguments.inpListUserBySegment)>
			<cfset arguments.inpListUserBySegment = "0"/>
		</cfif>

		<!--- Build Order --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "UserId_int"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "Full_Name" />
			    </cfcase> 
			    <cfcase value="3"> 
			       <cfset order = "EmailAddress_vch" />
			    </cfcase> 
			    <cfcase value="4"> 
			       <cfset order = "MFAContactString_vch"/>
			    </cfcase> 
			    <cfcase value="5"> 
			       <cfset order = "Active_int"/>
			    </cfcase> 			   
			    <cfdefaultcase> 
			 		<cfset order = "UserId_int"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "UserId_int"/>
		</cfif>

		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["UserList"] = ArrayNew()>		
		<cfquery datasource="#Session.DBSourceEBM#" name="GetUserList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS
					UserId_int,
					Full_Name,
					EmailAddress_vch,
					MFAContactString_vch,					
					Active_int,
					IsTestAccount_ti,
					IF(UserId_int IN (#arguments.inpListUserBySegment#), 0, 1) AS Order_ti
				FROM (SELECT 
						u.UserId_int,
						CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
						u.EmailAddress_vch,
						u.MFAContactString_vch,
						IF(up.MlpsImageCapacityLimit_bi > 0 , up.MlpsImageCapacityLimit_bi, 0) AS MlpsImageCapacityLimit_bi,
						u.Active_int,
						u.IsTestAccount_ti
					FROM 
						simpleobjects.useraccount u
						INNER JOIN simplebilling.userplans up
						ON u.UserId_int = up.UserId_int 
						WHERE up.UserPlanId_bi IN ( SELECT MAX(UserPlanId_bi) FROM simplebilling.userplans GROUP BY userplans.UserId_int  )
					ORDER BY 
						up.UserPlanId_bi DESC
					) AS tmpTable
				WHERE
					tmpTable.Active_int = 1
				AND
					IsTestAccount_ti=0
				<cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						<cfif filterItem.NAME EQ ' u.Status_int ' AND  filterItem.VALUE LT 0>
						<!--- DO nothing --->
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>

				GROUP BY 
					tmpTable.UserId_int
				ORDER BY 
						Order_ti, #order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>

		<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfloop query="GetUserList" >
			<cfset var tmpUser = {
				ID = '#UserId_int#',
				NAME = '#Full_Name#',
				EMAIL = '#EmailAddress_vch#',
				PHONE = '#MFAContactString_vch#',				
				STATUS = '#Active_int#',
				ISTESTACCOUNT = '#IsTestAccount_ti#'
			} />
			
			<cfset ArrayAppend(dataout["UserList"], tmpUser) />
		</cfloop>

   		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetSegmentListInfo" access="remote" output="false" hint="Get Segment List Info">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="inpSegmentID" TYPE="numeric" required="no" default="0"  />	
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var ListUserBySegment="">
		<cfset var GetListSegmentByListSegmentID="">
		<cfset var GetListUserIDBySegment="">
					

		<cftry>					
			<cfquery name="GetListSegmentByListSegmentID" datasource="#Session.DBSourceREAD#">
				SELECT 					
					S.PKId_bi,
				  	S.SegmentName_vch,
				  	S.Desc_vch,
				  	S.ListCount_int,
				  	S.UserId_int,
				  	S.Created_dt,
				  	S.LastModifierId_int,
				  	S.LastEdited_dt,
				  	S.LastModifyDesc_vch,
				  	S.Status_int				
				FROM 
					simpleobjects.alertsegment S		
				WHERE					
					S.PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSegmentID#"/>	
			</cfquery>	
			<cfloop query="GetListSegmentByListSegmentID">
				<cfset dataout["SegmentName"] = SegmentName_vch>		
				<cfset dataout["SegmentDesc"] = Desc_vch>	
			</cfloop>
			<cfquery name="GetListUserIDBySegment" datasource="#Session.DBSourceREAD#">
				SELECT
					UserId_int
				FROM
					simpleobjects.alertsegmentusers
				WHERE
					SegmentId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpSegmentID#"/>
			</cfquery>
			<cfset ListUserBySegment=ValueList(GetListUserIDBySegment.UserId_int,",")>
			<cfset dataout["ListUserBySegment"] = ListUserBySegment>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="GetListSegment" access="remote" output="false" hint="Get list Segment">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="inpSegmentID" TYPE="numeric" required="no" default="0"  />
		<cfargument name="inpSegment" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		
		<cfset var GetListSegment="">
		<cfset var item="">
		<cfset var rsCount="">

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'S.SegmentName_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'S.Desc_vch' />
		    </cfcase>		    
		    <cfcase value="2">
		        <cfset orderField = 'S.ListCount_int' />
		    </cfcase>	
		</cfswitch>

		<cftry>					

			
			<cfquery name="GetListSegment" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					S.PKId_bi,
				  	S.SegmentName_vch,
				  	S.Desc_vch,
				  	S.ListCount_int,
				  	S.UserId_int,
				  	S.Created_dt,
				  	S.LastModifierId_int,
				  	S.LastEdited_dt,
				  	S.LastModifyDesc_vch,
				  	S.Status_int				
				FROM 
					simpleobjects.alertsegment S		
				WHERE
					S.Status_int = 1
				<cfif TRIM(arguments.inpSegment) neq "">
					AND
					S.SegmentName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpSegment#%">
				</cfif>					
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						M.PKId_bi DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListSegment.RecordCount GT 0>
				<cfloop query="GetListSegment">
					<cfset item = {
						"ID": PKId_bi, 
						"SegmentName":HTMLEditFormat(SegmentName_vch),
						"ListCount":ListCount_int,
						"Desc":HTMLEditFormat(Desc_vch)
												
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="InsertUpdateSegment" access="remote" output="false" hint="insert update segment">
		<cfargument name="inpSegmentID" TYPE="numeric" required="no" default="0"/>
		<cfargument name="inpSegmentName" TYPE="string" required="yes" default=""/>
		<cfargument name="inpSegmentDesc" TYPE="string" required="no" default="" />
		<cfargument name="inpListUserID" TYPE="string" required="no" default="" />

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var ListCountID=0>		

		<cfset var ListCountID = ""/>
		<cfset var SegmentIdAfterInsert = ""/>
		<cfset var userID = ""/>
		<cfset var InsertNewSegment = ""/>
		<cfset var UpdateSegment = ""/>
		<cfset var DeleteCurentList = ""/>
		<cfset var InsertList = ""/>
		<cfset var GetIdSegmentListJustInsert = ""/>
		<cfset var ListUserIDOfSegment = arguments.inpListUserID/>		

		
		

		<cfset ListCountID=listLen(ListUserIDOfSegment)>		
		<cfset SegmentIdAfterInsert=arguments.inpSegmentID>

		<cftry>					
			<cfif arguments.inpSegmentID is 0>
				<cfquery name="InsertNewSegment" datasource="#Session.DBSourceEBM#">
					INSERT INTO simpleobjects.alertsegment
					(
						PKId_bi,
						SegmentName_vch,
						Desc_vch,
						ListCount_int,
						UserId_int,
						Created_dt,
						LastModifierId_int,
						LastEdited_dt,
						LastModifyDesc_vch,
						Status_int		  	
					)
					VALUES
					(
						NULL,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSegmentName)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSegmentDesc)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListCountID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
						NOW(),
						NULL,
						1								
					)
				</cfquery>
				<!--- get segment list id just insert--->									
				<cfquery name="GetIdSegmentListJustInsert" datasource="#Session.DBSourceREAD#">
					SELECT 	PKId_bi
					FROM	simpleobjects.alertsegment
					WHERE	UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					ORDER BY PKId_bi DESC
					LIMIT 1
				</cfquery>
				<cfset SegmentIdAfterInsert=GetIdSegmentListJustInsert.PKId_bi>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Insert Successfully"/>
			<cfelse>
				<cfquery name="UpdateSegment" datasource="#Session.DBSourceEBM#">
					UPDATE simpleobjects.alertsegment
					SET						
						SegmentName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSegmentName)#">,
						Desc_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSegmentDesc)#">,
						ListCount_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListCountID#">,											
						LastModifierId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
						LastEdited_dt=NOW()																									  				 
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSegmentID#">
				</cfquery>		
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Update Successfully"/>
			</cfif>			
			

			<cfif SegmentIdAfterInsert GT 0>
				<cfquery name="DeleteCurentList" datasource="#Session.DBSourceEBM#">
					DELETE FROM simpleobjects.alertsegmentusers
					WHERE	
						SegmentId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SegmentIdAfterInsert#">
				</cfquery>
				<cfif LISTLEN(inpListUserID) GT 0>
					<cfloop list="#ListUserIDOfSegment#" index="userID" delimiters=",">
						<cfif userID GT 0>
							<cfquery name="InsertList" datasource="#Session.DBSourceEBM#">
								INSERT INTO simpleobjects.alertsegmentusers
								(
									PKId_bi,
								  	SegmentId_bi,
								  	UserId_int
								)
								VALUES
								(
									NULL,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SegmentIdAfterInsert#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#">
								)
							</cfquery>
						</cfif>					
					</cfloop>
				</cfif>
			</cfif>			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="DeleteSegmentById" access="remote" output="false" hint="delete segment by id">
		<cfargument name="inpSegmentID" type="numeric" required="true" hint="id of segment"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var segmentUsed = '' />
		<cfset var DeleteSegmentList = '' />

		<cftry>

			<cfquery name="segmentUsed" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS Total
				FROM
					simpleobjects.alertcampaign AS C
				WHERE
					C.SegmentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSegmentID#"/>
				AND
					C.Status_int = 1
			</cfquery>

			<cfif segmentUsed.Total GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Can not delete this segment. This segment is using in #segmentUsed.Total# campaign" & (segmentUsed.Total GT 1 ? "s." : ".") />
			<cfelse>
				<cfquery result="DeleteSegmentList" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.alertsegment AS S
					SET
						S.Status_int = 4
					WHERE
						S.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSegmentID#"/>
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Delete this segment successfully." />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	<!--- Blocked Phone List --->
	<cffunction name="GetListBlockPhone" access="remote" output="false" hint="Get list Blocked Phone">
		<cfargument name="inpPhone" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListPhone = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'PhoneNumber_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'DateUpdate_dt' />
		    </cfcase>		    
		</cfswitch>

		<cftry>					
			<cfquery name="GetListPhone" datasource="#Session.DBSourceREAD#">
				SELECT 
						SQL_CALC_FOUND_ROWS
						IdPhone,
						PhoneNumber_vch,
						PhoneNumber_num,
						DateUpdate_dt
				FROM 
						simpleobjects.phone_number_blocked		
				<cfif TRIM(arguments.inpPhone) neq "">
				WHERE
					PhoneNumber_num LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpPhone#%">
				OR 
					PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpPhone#%">
				</cfif>	

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>				
					ORDER BY IdPhone
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListPhone.RecordCount GT 0>
				<cfloop query="GetListPhone">
					<cfset item = {
						"ID": IdPhone, 
						"PhoneNumber_vch": HTMLEditFormat(PhoneNumber_vch), 
						"DateUpdate_dt": DATEFORMAT(DateUpdate_dt,'mm/dd/yyyy ') & TimeFormat(DateUpdate_dt, " HH:mm:ss"),
						"PhoneNumber_num": HTMLEditFormat(PhoneNumber_num)
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeletePhoneNumberById" access="remote" output="false" hint="delete Phone Number by id">
		<cfargument name="inpPhoneID" type="numeric" required="true" hint="id of Phonenumber"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var DeletePhoneNumber = '' />

		<cftry>
			<cfquery result="DeletePhoneNumber" datasource="#Session.DBSourceEBM#">
				DELETE FROM simpleobjects.phone_number_blocked
				WHERE IdPhone = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPhoneID#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Delete this phone number successfully." />
		
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	
	<cffunction name="InsertPhoneNumber" access="remote" output="false" hint="insert new Phone Number">
		<cfargument name="inpphone" TYPE="string" required="no" default="" />		
			

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		<cfset var inpphone_num = ''/>
		<cfset inpphone_num = Replace(Replace(Replace(inpphone,"(","","ALL"), ")", "", "ALL"), "-", "", "ALL")/>

		<cfset var InsertNewPhonNumber = ""/>
		<cfset var CheckPhoneNumberExist = ""/>
		<cfset var CheckPhoneNumberExistWhiteList = ""/>

		<cfif NOT isvalid('telephone', inpphone)>
			<cfset dataout.ERRMESSAGE = 'Phone number is not a valid US telephone number !'> 
			<cfset dataout.MESSAGE = 'Phone number is not a valid US telephone number !'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="CheckPhoneNumberExist" datasource="#Session.DBSourceREAD#">
			SELECT 
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt
			FROM 
					simpleobjects.phone_number_blocked		
		
			WHERE
				PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpphone#%">
		</cfquery>

		<cfif CheckPhoneNumberExist.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'Phone number exist.'> 
			<cfset dataout.MESSAGE = 'Phone number exist.'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="CheckPhoneNumberExistWhiteList" datasource="#Session.DBSourceREAD#">
			SELECT 
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt
			FROM 
					simpleobjects.whitelistphonenumber		
		
			WHERE
				PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpphone#%">
		</cfquery>

		<cfif CheckPhoneNumberExistWhiteList.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'The phone number you entered is already in White list. Please try another.'> 
			<cfset dataout.MESSAGE = 'The phone number you entered is already in White list. Please try another.'>
			<cfreturn dataout>
		</cfif>

		<cftry>					
			<cfquery name="InsertNewPhonNumber" datasource="#Session.DBSourceEBM#">
				INSERT INTO simpleobjects.phone_number_blocked
				(
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt				  	
				)
				VALUES
				(
					NULL,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpphone)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(inpphone_num)#">, 
					NOW()
				)
			</cfquery>					
			

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<!---End Blocked Phone Number --->

	<!--- White Phone List --->
	<cffunction name="GetListWhitePhone" access="remote" output="false" hint="Get list White Phone">
		<cfargument name="inpPhone" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListPhone = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'PhoneNumber_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'DateUpdate_dt' />
		    </cfcase>		    
		</cfswitch>

		<cftry>					
			<cfquery name="GetListPhone" datasource="#Session.DBSourceREAD#">
				SELECT 
						SQL_CALC_FOUND_ROWS
						IdPhone,
						PhoneNumber_vch,
						PhoneNumber_num,
						DateUpdate_dt
				FROM 
						simpleobjects.whitelistphonenumber		
				<cfif TRIM(arguments.inpPhone) neq "">
				WHERE
					PhoneNumber_num LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpPhone#%">
				OR 
					PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpPhone#%">
				</cfif>	

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>				
					ORDER BY IdPhone
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListPhone.RecordCount GT 0>
				<cfloop query="GetListPhone">
					<cfset item = {
						"ID": IdPhone, 
						"PhoneNumber_vch": HTMLEditFormat(PhoneNumber_vch), 
						"DateUpdate_dt": DATEFORMAT(DateUpdate_dt,'mm/dd/yyyy ') & TimeFormat(DateUpdate_dt, " HH:mm:ss"),
						"PhoneNumber_num": HTMLEditFormat(PhoneNumber_num)
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeleteWPhoneNumberById" access="remote" output="false" hint="Delete Phone Number by id">
		<cfargument name="inpPhoneID" type="numeric" required="true" hint="id of Phonenumber"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var DeletePhoneNumber = '' />

		<cftry>
			<cfquery result="DeletePhoneNumber" datasource="#Session.DBSourceEBM#">
				DELETE FROM simpleobjects.whitelistphonenumber
				WHERE IdPhone = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPhoneID#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Delete this phone number successfully." />
		
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	
	<cffunction name="InsertWPhoneNumber" access="remote" output="false" hint="Insert new Phone Number">
		<cfargument name="inpphone" TYPE="string" required="no" default="" />		
			

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		<cfset var inpphone_num = ''/>
		<cfset inpphone_num = Replace(Replace(Replace(inpphone,"(","","ALL"), ")", "", "ALL"), "-", "", "ALL")/>

		<cfset var InsertNewPhoneNumber = ""/>
		<cfset var CheckPhoneNumberExist = ""/>
		<cfset var CheckPhoneNumberExistBlockList = ""/>

		<cfif NOT isvalid('telephone', inpphone)>
			<cfset dataout.ERRMESSAGE = 'Phone number is not a valid US telephone number !'> 
			<cfset dataout.MESSAGE = 'Phone number is not a valid US telephone number !'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="CheckPhoneNumberExist" datasource="#Session.DBSourceREAD#">
			SELECT 
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt
			FROM 
					simpleobjects.whitelistphonenumber		
		
			WHERE
				PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpphone#%">
		</cfquery>

		<cfif CheckPhoneNumberExist.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'Phone number exist.'> 
			<cfset dataout.MESSAGE = 'Phone number exist.'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="CheckPhoneNumberExistBlockList" datasource="#Session.DBSourceREAD#">
			SELECT 
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt
			FROM 
					simpleobjects.phone_number_blocked		
		
			WHERE
				PhoneNumber_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpphone#%">
		</cfquery>

		<cfif CheckPhoneNumberExistBlockList.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'The phone number you entered is already in Block list. Please try another.'> 
			<cfset dataout.MESSAGE = 'The phone number you entered is already in Block list. Please try another.'>
			<cfreturn dataout>
		</cfif>

		<cftry>					
			<cfquery name="InsertNewPhoneNumber" datasource="#Session.DBSourceEBM#">
				INSERT INTO simpleobjects.whitelistphonenumber
				(
					IdPhone,
					PhoneNumber_vch,
					PhoneNumber_num,
					DateUpdate_dt				  	
				)
				VALUES
				(
					NULL,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpphone)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(inpphone_num)#">, 
					NOW()
				)
			</cfquery>					
			

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<!---End White Phone Number --->

	<!--- Block Ip Address --->
	<cffunction name="GetListIpAddress" access="remote" output="false" hint="Get list Ip Address">
		<cfargument name="inpipaddress" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListIp = '' />
		<cfset var rsCount = '' />
		<cfset var GetListIpAddress = ''/>

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'IpAddress_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'DateUpdate_dt' />
		    </cfcase>		    
		</cfswitch>

		<cftry>					
			<cfquery name="GetListIpAddress" datasource="#Session.DBSourceREAD#">
				SELECT 
						SQL_CALC_FOUND_ROWS
						IdIp,
						IpAddress_vch,
						DateUpdate_dt
				FROM 
						simpleobjects.blockipaddress		
				<cfif TRIM(arguments.inpipaddress) neq "">
				WHERE
					IpAddress_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpipaddress#%">
				OR 
					IpAddress_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpipaddress#%">
				</cfif>	

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>				
					ORDER BY IdIp
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListIpAddress.RecordCount GT 0>
				<cfloop query="GetListIpAddress">
					<cfset item = {
						"ID": IdIp, 
						"IpAddress_vch": HTMLEditFormat(IpAddress_vch), 
						"DateUpdate_dt": DATEFORMAT(DateUpdate_dt,'mm/dd/yyyy ') & TimeFormat(DateUpdate_dt, " HH:mm:ss"),
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeleteIpAddressById" access="remote" output="false" hint="Delete Ip Address by id">
		<cfargument name="inpIpID" type="numeric" required="true" hint="id of Ip Address"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var DeleteIpAddress = '' />

		<cftry>
			<cfquery result="DeleteIpAddress" datasource="#Session.DBSourceEBM#">
				DELETE FROM simpleobjects.blockipaddress
				WHERE IdIp = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIpID#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Delete this IP address successfully." />
		
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="InsertIpAddress" access="remote" output="false" hint="Insert new Ip Address">
		<cfargument name="inpipaddress" TYPE="string" required="no" default="" />		
			

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	
		<cfset var InsertNewIpAddress = ""/>
		<cfset var CheckIpAddressExist = ""/>

		<cfif inpipaddress eq ''>
			<cfset dataout.ERRMESSAGE = 'Invalid IP Address'> 
			<cfset dataout.MESSAGE = 'Invalid IP Address'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="CheckIpAddressExist" datasource="#Session.DBSourceREAD#">
			SELECT 
					IdIp,
					IpAddress_vch,
					DateUpdate_dt
			FROM 
					simpleobjects.blockipaddress		
			WHERE
				IpAddress_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpipaddress#%">
		</cfquery>

		<cfif CheckIpAddressExist.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'IP address exist.'> 
			<cfset dataout.MESSAGE = 'IP address exist.'>
			<cfreturn dataout>
		</cfif>

		<cftry>					
			<cfquery name="InsertNewIpAddress" datasource="#Session.DBSourceEBM#">
				INSERT INTO simpleobjects.blockipaddress
				(
					IdIp,
					IpAddress_vch,
					DateUpdate_dt				  	
				)
				VALUES
				(
					NULL,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpipaddress)#">, 
					NOW()
				)
			</cfquery>					
			

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert IP address successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<!---End Block Ip Address --->

	<!--- Black list words --->
	<cffunction name="GetListBlackWords" access="remote" output="false" hint="Get Black list words">
		<cfargument name="inpWords" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetBlackListWords = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'Words_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'UpdateDate_dt' />
		    </cfcase>		    
		</cfswitch>

		<cftry>					
			<cfquery name="GetBlackListWords" datasource="#Session.DBSourceREAD#">
				SELECT 
						SQL_CALC_FOUND_ROWS
						WordId,
						Words_vch,
						UpdateDate_dt
				FROM 
						simpleobjects.black_list_words		
				<cfif TRIM(arguments.inpWords) neq "">
				WHERE
					Words_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpWords#%">
				</cfif>	

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>				
					ORDER BY IdWords
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetBlackListWords.RecordCount GT 0>
				<cfloop query="GetBlackListWords">
					<cfset item = {
						"ID": WordId, 
						"Words_vch": HTMLEditFormat(Words_vch), 
						"UpdateDate_dt": DATEFORMAT(UpdateDate_dt,'mm/dd/yyyy ') & TimeFormat(UpdateDate_dt, " HH:mm:ss")
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeleteWordsById" access="remote" output="false" hint="delete Words by id">
		<cfargument name="inpWordsID" type="numeric" required="true" hint="id of Words"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var DeleteWords = '' />

		<cftry>
			<cfquery result="DeleteWords" datasource="#Session.DBSourceEBM#">
				DELETE FROM simpleobjects.black_list_words
				WHERE WordId = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWordsID#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Delete this words successfully." />
		
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	
	<cffunction name="InsertWords" access="remote" output="false" hint="insert new words">
		<cfargument name="inpwords" TYPE="string" required="no" default="" />		
			
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var InsertNewWords = ""/>
		<cfset var CheckWordsExist = ""/>

		<cfquery name="CheckWordsExist" datasource="#Session.DBSourceREAD#">
			SELECT 
					WordId
			FROM 
					simpleobjects.black_list_words		
		
			WHERE
				Words_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpwords)#">
		</cfquery>

		<cfif CheckWordsExist.RECORDCOUNT GT 0>
			<cfset dataout.ERRMESSAGE = 'Words exist.'> 
			<cfset dataout.MESSAGE = 'Words exist.'>
			<cfreturn dataout>
		</cfif>

		<cftry>					
			<cfquery name="InsertNewWords" datasource="#Session.DBSourceEBM#">
				INSERT INTO simpleobjects.black_list_words
				(
					WordId,
					Words_vch,
					UpdateDate_dt				  	
				)
				VALUES
				(
					NULL,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpwords)#">, 
					NOW()
				)
			</cfquery>					
			

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<!---End Black list words --->


	<cffunction name="GetListMessage" access="remote" output="false" hint="Get list Message">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="inpMessage" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListMessage = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'M.Subject_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'M.Created_dt' />
		    </cfcase>		    
		</cfswitch>

		<cftry>					

			
			<cfquery name="GetListMessage" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					M.PKId_bi, 
					M.Subject_vch, 
					M.Created_dt,
					M.SMS_Content_vch,
					M.Web_Alert_Content_vch,
					M.Email_Content_vch								
				FROM 
					simpleobjects.alertmessage M		
				WHERE
					M.Status_int = 1
				<cfif TRIM(arguments.inpMessage) neq "">
					AND
					M.Subject_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpMessage#%">
				</cfif>					
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						M.PKId_bi DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListMessage.RecordCount GT 0>
				<cfloop query="GetListMessage">
					<cfset item = {
						"ID": PKId_bi, 
						"Subject_vch": HTMLEditFormat(Subject_vch), 
						"Created_dt": DATEFORMAT(Created_dt,'mm/dd/yyyy ') & TimeFormat(Created_dt, " HH:mm:ss"),
						"SMS_Content": HTMLEditFormat(SMS_Content_vch), 
						"Web_Alert_Content": HTMLEditFormat(Web_Alert_Content_vch), 
						"Email_Content": HTMLEditFormat(Email_Content_vch)
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="InsertUpdateMessage" access="remote" output="false" hint="insert new Message">
		<cfargument name="inpMessageID" TYPE="numeric" required="no" default="0"/>
		<cfargument name="inpSubject" TYPE="string" required="yes" default="" />
		<cfargument name="inpSMSAlert" TYPE="string" required="no" default="" />
		<cfargument name="inpWebAlert" TYPE="string" required="no" default="" />
		<cfargument name="inpMailAlert" TYPE="string" required="no" default="" />		
			

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var InsertNewMessage = ""/>

		<cfif arguments.inpSubject is "">
			<cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "Error Blank Subject" />
   		    <cfset dataout.ERRMESSAGE = "Error Blank Subject" />
			<cfreturn dataout>
		</cfif>
		<cftry>					
			<cfif arguments.inpMessageID is 0>
				<cfquery name="InsertNewMessage" datasource="#Session.DBSourceEBM#">
					INSERT INTO simpleobjects.alertmessage
					(
						PKId_bi,
					  	Subject_vch,
					  	SMS_Content_vch,
					  	Web_Alert_Content_vch,
					  	Email_Content_vch,
					  	UserId_int,
					  	Created_dt,
					  	LastModifierId_int,
					  	LastEdited_dt,
					  	LastModifyDesc_vch,
					  	Status_int,
					  	Expire_dt				  	
					)
					VALUES
					(
						NULL,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSubject)#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSMSAlert)#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpWebAlert)#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpMailAlert)#">, 

						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">, 
						NOW(), 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">, 
						NOW(), 
						NULL, 
						1, 
						NULL
					)
				</cfquery>					
				

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Insert Successfully"/>
			<cfelse>
				<cfquery name="InsertNewMessage" datasource="#Session.DBSourceEBM#">
					UPDATE simpleobjects.alertmessage
					SET
						Subject_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSubject)#">,
					  	SMS_Content_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpSMSAlert)#">,
					  	Web_Alert_Content_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpWebAlert)#">,
					  	Email_Content_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpMailAlert)#">,
					  	LastModifierId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
					  	LastEdited_dt=NOW()									  						  	
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMessageID#">
				</cfquery>		
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Update Successfully"/>
			</cfif>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="DeleteMessageById" access="remote" output="false" hint="delete message by id">
		<cfargument name="inpMessageID" type="numeric" required="true" hint="id of message"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var messageUsed = '' />
		<cfset var DeleteMessage = '' />

		<cftry>

			<cfquery name="messageUsed" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS Total
				FROM
					simpleobjects.alertcampaign AS C
				WHERE
					C.MessageId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMessageID#"/>
				AND
					C.Status_int = 1
			</cfquery>

			<cfif messageUsed.Total GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Can not delete this message. This message is using in #messageUsed.Total# campaign" & (messageUsed.Total GT 1 ? "s." : ".") />
			<cfelse>
				<cfquery result="DeleteMessage" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.alertmessage AS M
					SET
						M.Status_int = 4
					WHERE
						M.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMessageID#"/>
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Delete this message successfully." />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<!--- Campaign --->
	<cffunction name="getCampaign" access="public" output="false" hint="get alert campaign by id">
		<cfargument name="inpCampaignId" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.Campaign = {
			RECORDCOUNT = 0,
			PKId_bi = 0,
			CampaignName_vch = "",
			MessageId_bi = 0,
			SegmentId_bi = 0,
			WebAlertSentCount_int = 0,
			EmailSentCount_int = 0,
			SMSSentCount_int = 0,
			ListCount_int = 0,
			SentTo_int = 0,
			Trigger_bi = 1,
			UserId_int = 0,
			Created_dt = javacast("null",""),
			LastModifierId_int = 0,
			LastEdited_dt = javacast("null",""),
			LastModifyDesc_vch = "",
			Status_int = 1,
			Expire_dt = javacast("null","")
		} />

		<cfset var getCampaign = '' />
		
		<cftry>

			<cfquery name="getCampaign" datasource="#Session.DBSourceREAD#">
				SELECT
					C.PKId_bi,
					C.CampaignName_vch,
					C.MessageId_bi,
					C.SegmentId_bi,
					C.ReceiverId_int,
					C.WebAlertSentCount_int,
					C.EmailSentCount_int,
					C.SMSSentCount_int,
					C.ListCount_int,
					C.SentTo_int,
					C.Trigger_bi,
					C.UserId_int,
					C.Created_dt,
					C.LastModifierId_int,
					C.LastEdited_dt,
					C.LastModifyDesc_vch,
					C.Status_int,
					C.Expire_dt
				FROM
					simpleobjects.alertcampaign AS C
				WHERE
					C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#"/>
			</cfquery>

			<cfif getCampaign.RECORDCOUNT GT 0>
				<cfset dataout.Campaign = getCampaign/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getMessages" access="public" output="false" hint="get message list">
		<cfargument name="inpGetAll" type="numeric" required="false" default="1"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.Messages = QueryNew("PKId_bi, Subject_vch, SMS_Content_vch, Web_Alert_Content_vch, Email_Content_vch, UserId_int, Created_dt, LastModifierId_int, LastEdited_dt, LastModifyDesc_vch, Status_int, Expire_dt", "bigint, varchar, varchar, varchar, varchar, int, time, int, time, varchar, tinyint, time")/>

		<cfset var getMessages = '' />
		
		<cftry>

			<cfquery name="getMessages" datasource="#Session.DBSourceREAD#">
				SELECT
					M.PKId_bi,
					M.Subject_vch,
					M.SMS_Content_vch,
					M.Web_Alert_Content_vch,
					M.Email_Content_vch,
					M.UserId_int,
					M.Created_dt,
					M.LastModifierId_int,
					M.LastEdited_dt,
					M.LastModifyDesc_vch,
					M.Status_int,
					M.Expire_dt
				FROM
					simpleobjects.alertmessage AS M
				<cfif arguments.inpGetAll EQ 0>
				WHERE
					M.Status_int = 1
				AND
					(M.Expire_dt IS NULL OR M.Expire_dt > NOW())
				</cfif>
			</cfquery>

			<cfif getMessages.RECORDCOUNT GT 0>
				<cfset dataout.Messages = getMessages/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getSegments" access="public" output="false" hint="get segment list">
		<cfargument name="inpGetAll" type="numeric" required="false" default="1"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.Segments = QueryNew("PKId_bi, SegmentName_vch, Desc_vch, ListCount_int, UserId_int, Created_dt, LastModifierId_int, LastEdited_dt, LastModifyDesc_vch, Status_int", "bigint, varchar, varchar, int, int, time, int, time, varchar, tinyint")/>

		<cfset var getSegments = '' />
		
		<cftry>

			<cfquery name="getSegments" datasource="#Session.DBSourceREAD#">
				SELECT
					S.PKId_bi,
					S.SegmentName_vch,
					S.Desc_vch,
					S.ListCount_int,
					S.UserId_int,
					S.Created_dt,
					S.LastModifierId_int,
					S.LastEdited_dt,
					S.LastModifyDesc_vch,
					S.Status_int
				FROM
					simpleobjects.alertsegment AS S
				<cfif arguments.inpGetAll EQ 0>
				WHERE
					S.Status_int = 1
				</cfif>
			</cfquery>

			<cfif getSegments.RECORDCOUNT GT 0>
				<cfset dataout.Segments = getSegments/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getUserById" access="public" output="false" hint="get user by UserId">
		<cfargument name="inpUserId" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.USER = ""/>

		<cfset var userAccount = '' />

		<cftry>

			<cfquery name="userAccount" datasource="#Session.DBSourceREAD#">
				SELECT
					*
				FROM
					simpleobjects.useraccount
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
			</cfquery>

			<cfif userAccount.RecordCount GT 0>
				<cfset dataout.USER = userAccount/>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="saveCampaign" access="remote" hint="save alert campaign by id">
		<cfargument name="PKId_bi" type="numeric" required="true"/>
		<cfargument name="CampaignName_vch" type="string" required="true"/>
		<cfargument name="MessageId_bi" type="numeric" required="true"/>
		<cfargument name="SegmentId_bi" type="numeric" required="false" default="0"/>
		<cfargument name="ReceiverId_int" type="numeric" required="false" default="0"/>
		<cfargument name="SentTo_int" type="string" required="true"/>
		<cfargument name="Trigger_bi" type="string" required="true"/>
		<cfargument name="Status_int" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var saveCampaign = '' />
		<cfset var triggerSum = 0 />

		<cftry>
			<cfset triggerSum = ArraySum(ListToArray(arguments.Trigger_bi))/>
			<cfset dataout.triggerSum = triggerSum/>
			<cfif BitAnd(triggerSum, ALERT_TRIGGER_SIGN_IN) GT 0 AND BitAnd(triggerSum, ALERT_TRIGGER_LOW_CREDIT + ALERT_TRIGGER_NO_CREDIT + ALERT_TRIGGER_DECLINED_CREDIT) GT 0>
				<cfset triggerSum = BitAnd(triggerSum, ALERT_TRIGGER_LOW_CREDIT + ALERT_TRIGGER_NO_CREDIT + ALERT_TRIGGER_DECLINED_CREDIT)/>
			</cfif>

			<cfif arguments.PKId_bi GT 0>
				<cfquery result="saveCampaign" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.alertcampaign AS C
					SET
						C.CampaignName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.CampaignName_vch)#"/>,
						C.MessageId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MessageId_bi#"/>,
						C.SegmentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.SegmentId_bi#"/>,
						C.ReceiverId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ReceiverId_int#"/>,
						C.ListCount_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#(arguments.ReceiverId_int GT 0 AND BitAnd(triggerSum, ALERT_TRIGGER_SIGN_UP) EQ 0 ? 1 : 0)#"/>,
						C.SentTo_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ArraySum(ListToArray(arguments.SentTo_int))#"/>,
						C.Trigger_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#triggerSum#"/>,
						C.LastModifierId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"/>,
						C.LastEdited_dt = NOW(),
						C.Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Status_int#"/>
					WHERE
						C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PKId_bi#"/>
					AND
						C.Status_int != 1
				</cfquery>
			<cfelse>
				<cfquery result="saveCampaign" datasource="#Session.DBSourceEBM#">
					INSERT INTO
						simpleobjects.alertcampaign (
							CampaignName_vch,
							MessageId_bi,
							SegmentId_bi,
							ReceiverId_int,
							ListCount_int,
							SentTo_int,
							Trigger_bi,
							UserId_int,
							Created_dt,
							Status_int
						)
					VALUES (
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.CampaignName_vch)#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MessageId_bi#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.SegmentId_bi#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ReceiverId_int#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#(arguments.ReceiverId_int GT 0 AND BitAnd(triggerSum, ALERT_TRIGGER_SIGN_UP) EQ 0 ? 1 : 0)#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ArraySum(ListToArray(arguments.SentTo_int))#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#triggerSum#"/>,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"/>,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Status_int#"/>
					)
				</cfquery>
				<cfset arguments.PKId_bi = saveCampaign.GENERATEDKEY/>
			</cfif>

			<cfif BitAnd(triggerSum, ALERT_TRIGGER_SIGN_UP) EQ 0 AND (arguments.SegmentId_bi GT 0 OR arguments.ReceiverId_int EQ 0)>
				<cfquery result="saveCampaign" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.alertcampaign AS C
					SET
						C.ListCount_int = (
							<cfif arguments.SegmentId_bi GT 0>
								SELECT 
									COUNT(*) 
								FROM 
									simpleobjects.alertsegmentusers
								WHERE
									SegmentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.SegmentId_bi#"/>
							<cfelse>
								SELECT 
									COUNT(*)
								FROM 
									(
									SELECT 
										u.UserId_int,
										u.CBPID_int,
										u.Active_int
									FROM 
										simpleobjects.useraccount u
									INNER JOIN 
										simplebilling.userplans up
									ON 
										u.UserId_int = up.UserId_int
									GROUP BY
										u.UserId_int
									HAVING
										(u.CBPID_int = 1 OR u.CBPID_int IS NULL)
									AND 
										u.Active_int = 1
									) AS T
							</cfif>
						)
					WHERE
						C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PKId_bi#"/>
				</cfquery>
			</cfif>

			<cfif BitAnd(triggerSum, ALERT_TRIGGER_SEND_NOW) GT 0 AND arguments.Status_int EQ 1>
				<cfinvoke method="sendAlertByCampaign" component="session.sire.models.cfc.alert-system">
					<cfinvokeargument name="alertCampaignId" value="#arguments.PKId_bi#"/>
					<cfinvokeargument name="trigger" value="#ALERT_TRIGGER_SEND_NOW#"/>
				</cfinvoke>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getCampaigns" access="remote" output="false" hint="Get list Campaign">
		<cfargument name="inpSearchName" type="string" required="false" default=""/>

		<cfargument name="iDisplayStart" type="numeric" required="false" default="0"/>
		<cfargument name="iDisplayLength" type="numeric" required="false" default="10"/>
		<cfargument name="iSortCol_0" type="numeric" required="false" default="-1"/>
		<cfargument name="sSortDir_0" type="string" required="false" default="ASC"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>

		<cfset var item = '' />
		<cfset var getCampaigns = '' />
		<cfset var campaignsCount = '' />
		<cfset var Trigger_vch = '' />

		<cftry>

			<cfset var orderField = '' />
			<cfswitch expression="#arguments.iSortCol_0#">
				<cfcase value="3">
					<cfset orderField = 'C.ListCount_int' />
				</cfcase>
				<cfcase value="4">
					<cfset orderField = 'C.Created_dt' />
				</cfcase>
			</cfswitch>

			<cfquery name="getCampaigns" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					C.PKId_bi,
					C.CampaignName_vch,
					C.MessageId_bi,
					M.Subject_vch, 
					C.SegmentId_bi,
					S.SegmentName_vch,
					C.ReceiverId_int,
					C.ListCount_int,
					C.Created_dt,
					C.Trigger_bi,
					C.Status_int
				FROM 
					simpleobjects.alertcampaign AS C
				LEFT JOIN
					simpleobjects.alertmessage AS M
					ON
						C.MessageId_bi = M.PKId_bi
				LEFT JOIN
					simpleobjects.alertsegment AS S
					ON
						C.SegmentId_bi = S.PKId_bi
				<cfif arguments.inpSearchName NEQ "">
					WHERE
						CampaignName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpSearchName#%"/>
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #UCase(arguments.sSortDir_0) EQ "DESC" ? "DESC" : "ASC"#
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>

			<cfif getCampaigns.RecordCount GT 0>
				<cfloop query="getCampaigns">
					<cfset Trigger_vch = ""/>
					<cfif BitAnd(Trigger_bi, ALERT_TRIGGER_SEND_NOW) GT 0>
						<cfset Trigger_vch = "Send NOW!"/>
					<cfelseif BitAnd(Trigger_bi, ALERT_TRIGGER_SIGN_UP) GT 0>
						<cfset Trigger_vch = "Sign up"/>
					<cfelseif BitAnd(Trigger_bi, ALERT_TRIGGER_SIGN_IN) GT 0>
						<cfset Trigger_vch = "Sign in"/>
					</cfif>
					<cfif BitAnd(Trigger_bi, ALERT_TRIGGER_LOW_CREDIT) GT 0>
						<cfset Trigger_vch = "Low credit"/>
					</cfif>
					<cfif BitAnd(Trigger_bi, ALERT_TRIGGER_NO_CREDIT) GT 0>
						<cfset Trigger_vch = listAppend(Trigger_vch, "No credit", ", ")/>
					</cfif>
					<cfif BitAnd(Trigger_bi, ALERT_TRIGGER_DECLINED_CREDIT) GT 0>
						<cfset Trigger_vch = listAppend(Trigger_vch, "Declined credit", ", ")/>
					</cfif>

					<cfset item = {
						"ID": PKId_bi, 
						"CampaignName_vch": HTMLEditFormat(CampaignName_vch),
						"MessageId_bi": MessageId_bi,
						"Subject_vch": HTMLEditFormat(Subject_vch), 
						"SegmentId_bi": SegmentId_bi,
						"SegmentName_vch": HTMLEditFormat(SegmentName_vch),
						"ListCount_int": (SegmentId_bi GT 0 OR ReceiverId_int GT 0 ? ListCount_int : 'n/a'),
						"Created_dt": DATEFORMAT(Created_dt,'mm/dd/yyyy'),
						"Trigger_bi": Trigger_vch, //(bitAnd(Trigger_bi, ALERT_TRIGGER_SEND_NOW) GT 0 ? "Manual" : "Scheduled"),
						"Status_int": Status_int
					} />
					<cfset arrayAppend(dataout["aaData"], item) />
				</cfloop>

				<cfquery name="campaignsCount" datasource="#Session.DBSourceREAD#">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfset dataout["iTotalDisplayRecords"] = campaignsCount.iTotalRecords/>
				<cfset dataout["iTotalRecords"] = campaignsCount.iTotalRecords/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getCampaignReport" access="public" output="false" hint="get alert campaign report by id">
		<cfargument name="inpCampaignId" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.Campaign = {
			RECORDCOUNT = 0,
			PKId_bi = 0,
			CampaignName_vch = "",
			MessageId_bi = 0,
			SegmentId_bi = 0,
			WebAlertSentCount_int = 0,
			EmailSentCount_int = 0,
			SMSSentCount_int = 0,
			ListCount_int = 0,
			SentTo_int = 0,
			Trigger_bi = 1,
			UserId_int = 0,
			Created_dt = javacast("null",""),
			LastModifierId_int = 0,
			LastEdited_dt = javacast("null",""),
			LastModifyDesc_vch = "",
			Status_int = 1,
			Expire_dt = javacast("null",""),

			MessageSubject_vch = "",
			MessageUserId_int = 0,
			MessageCreated_dt = javacast("null",""),
			SegmentName_vch = "",
			SegmentCreated_dt = javacast("null",""),
			SegmentUserId_int = 0,
			SegmentCreated_dt = javacast("null","")

		} />

		<cfset var getCampaign = '' />
		<cfset var getCampaignReceiverCount = '' />
		<cfset var getEmailSentCount = '' />
		<cfset var getSMSSentCount = '' />
		<cfset var getWebAlertSentCount = '' />
		<cfset var saveCampaign = '' />
		
		<cftry>

			<cfquery name="getCampaign" datasource="#Session.DBSourceREAD#">
				SELECT
					C.PKId_bi,
					C.CampaignName_vch,
					C.MessageId_bi,
					C.SegmentId_bi,
					C.ReceiverId_int,
					C.WebAlertSentCount_int,
					C.EmailSentCount_int,
					C.SMSSentCount_int,
					C.ListCount_int,
					C.SentTo_int,
					C.Trigger_bi,
					C.UserId_int,
					C.Created_dt,
					C.LastModifierId_int,
					C.LastEdited_dt,
					C.LastModifyDesc_vch,
					C.Status_int,
					C.Expire_dt,

					M.Subject_vch AS MessageSubject_vch,
					M.UserId_int AS MessageUserId_int,
					M.Created_dt AS MessageCreated_dt,
					S.SegmentName_vch,
					S.Created_dt AS SegmentCreated_dt,
					S.UserId_int AS SegmentUserId_int,
					S.Created_dt AS SegmentCreated_dt,
					A.EmailAddress_vch AS ReceiverEmailAddress_vch

				FROM
					(
					SELECT
						C.PKId_bi,
						C.CampaignName_vch,
						C.MessageId_bi,
						C.SegmentId_bi,
						C.ReceiverId_int,
						C.WebAlertSentCount_int,
						C.EmailSentCount_int,
						C.SMSSentCount_int,
						C.ListCount_int,
						C.SentTo_int,
						C.Trigger_bi,
						C.UserId_int,
						C.Created_dt,
						C.LastModifierId_int,
						C.LastEdited_dt,
						C.LastModifyDesc_vch,
						C.Status_int,
						C.Expire_dt
					FROM
						simpleobjects.alertcampaign AS C
					WHERE
						C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#"/>
					) AS C
				INNER JOIN
					simpleobjects.alertmessage AS M
				ON
					C.MessageId_bi = M.PKId_bi
				LEFT JOIN
					simpleobjects.alertsegment AS S
				ON
					C.SegmentId_bi = S.PKId_bi
				LEFT JOIN
					simpleobjects.useraccount AS A
				ON
					C.ReceiverId_int = A.UserId_int
			</cfquery>

			<cfif getCampaign.RECORDCOUNT GT 0>
				<cfif BitAnd(getCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_UP) EQ 0 AND getCampaign.SegmentId_bi EQ 0 AND getCampaign.ReceiverId_int EQ 0>
					<cfquery name="getCampaignReceiverCount" datasource="#Session.DBSourceREAD#">
						SELECT 
							COUNT(*) AS Total
						FROM 
							(
							SELECT 
								u.UserId_int,
								u.CBPID_int,
								u.Active_int
							FROM 
								simpleobjects.useraccount u
							INNER JOIN 
								simplebilling.userplans up
							ON 
								u.UserId_int = up.UserId_int
							GROUP BY
								u.UserId_int
							HAVING
								(u.CBPID_int = 1 OR u.CBPID_int IS NULL)
							AND 
								u.Active_int = 1
							) AS T
					</cfquery>

					<cfif getCampaign.ListCount_int NEQ getCampaignReceiverCount.Total>

						<cfquery result="saveCampaign" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.alertcampaign AS C
							SET
								C.ListCount_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaignReceiverCount.Total#"/>
							WHERE
								C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaign.PKId_bi#"/>
						</cfquery>

						<cfset getCampaign.ListCount_int = getCampaignReceiverCount.Total/>

					</cfif>

				</cfif>

				<cfif BitAnd(getCampaign.SentTo_int, ALERT_SEND_TO_EMAIL) GT 0>
					<cfquery name="getEmailSentCount" datasource="#Session.DBSourceREAD#">
						SELECT
							COUNT(*) AS Total
						FROM
							simpleobjects.alert2users AS L
						WHERE
							L.CampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaign.PKId_bi#"/>
						AND
							L.SentTo_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ALERT_SEND_TO_EMAIL#"/>
						AND
							L.JobThreadId_bi > 0
					</cfquery>
					<cfset getCampaign.EmailSentCount_int = getEmailSentCount.Total/>
				</cfif>

				<cfif BitAnd(getCampaign.SentTo_int, ALERT_SEND_TO_SMS) GT 0>
					<cfquery name="getSMSSentCount" datasource="#Session.DBSourceREAD#">
						SELECT
							COUNT(*) AS Total
						FROM
							simpleobjects.alert2users AS L
						WHERE
							L.CampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaign.PKId_bi#"/>
						AND
							L.SentTo_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ALERT_SEND_TO_SMS#"/>
						AND
							L.JobThreadId_bi > 0
					</cfquery>
					<cfset getCampaign.SMSSentCount_int = getSMSSentCount.Total/>
				</cfif>

				<cfif BitAnd(getCampaign.SentTo_int, ALERT_SEND_TO_WEB_ALERT) GT 0>
					<cfquery name="getWebAlertSentCount" datasource="#Session.DBSourceREAD#">
						SELECT
							COUNT(*) AS Total
						FROM
							simpleobjects.alert2users AS L
						WHERE
							L.CampaignId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaign.PKId_bi#"/>
						AND
							L.SentTo_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ALERT_SEND_TO_WEB_ALERT#"/>
						AND
							L.WebAlertHide_int = 1
					</cfquery>
					<cfset getCampaign.WebAlertSentCount_int = getWebAlertSentCount.Total/>
				</cfif>

				<cfset dataout.Campaign = getCampaign/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="getMessagesByUserSignIn" access="remote" output="false" hint="get message list by user id sign in">
		<cfargument name="inpSearchMessage" type="string" required="false" default=""/>

		<cfargument name="iDisplayStart" type="numeric" required="false" default="0"/>
		<cfargument name="iDisplayLength" type="numeric" required="false" default="10"/>
		<cfargument name="iSortCol_0" type="numeric" required="false" default="-1"/>
		<cfargument name="sSortDir_0" type="string" required="false" default="ASC"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>

		<cfset var item = '' />
		<cfset var getMessages = '' />
		<cfset var messagesCount = '' />

		<cftry>

			<cfset var orderField = '' />
			<cfswitch expression="#arguments.iSortCol_0#">
				<cfcase value="1">
					<cfset orderField = 'L.Created_dt' />
				</cfcase>
			</cfswitch>

			<cfquery name="getMessages" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					L.PKId_bi,
					L.Content_vch,
					L.Created_dt
				FROM
					simpleobjects.alert2users AS L
				WHERE
					L.SentTo_int = #ALERT_SEND_TO_WEB_ALERT#
				AND
					L.Status_int = 1
				AND
					L.ReceiverId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"/>
				<cfif arguments.inpSearchMessage NEQ "">
					AND
						L.Content_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpSearchMessage#%"/>
				</cfif>
				ORDER BY
					<cfif orderField NEQ "">
						#orderField# #UCase(arguments.sSortDir_0) EQ "DESC" ? "DESC" : "ASC"#
					<cfelse>
						L.Created_dt DESC
					</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	

			<cfif getMessages.RecordCount GT 0>
				<cfloop query="getMessages">
					<cfset item = {
						"ID": PKId_bi, 
						"Content_vch": Content_vch,
						"Created_dt": DATEFORMAT(Created_dt,'mm/dd/yyyy')
					} />
					<cfset arrayAppend(dataout["aaData"], item) />
				</cfloop>

				<cfquery name="messagesCount" datasource="#Session.DBSourceREAD#">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfset dataout["iTotalDisplayRecords"] = messagesCount.iTotalRecords/>
				<cfset dataout["iTotalRecords"] = messagesCount.iTotalRecords/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeleteUserMessageById" access="remote" output="false" hint="delete a user message by id">
		<cfargument name="inpMessageID" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var DeleteMessage = '' />

		<cftry>
			<cfquery result="DeleteMessage" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.alert2users AS L
				SET
					L.Status_int = 4
				WHERE
					L.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMessageID#"/>
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Delete this message successfully." />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="pauseCampaign" access="remote" output="false" hint="pause alert campaign report by id">
		<cfargument name="inpCampaignId" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var UpdateCampaignStatus= ""/>

		<cftry>

			<cfquery result="UpdateCampaignStatus" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.alertcampaign AS C
				SET
					C.Status_int = 2
				WHERE
					C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#"/>
				AND
					C.Status_int = 1
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="playCampaign" access="remote" output="false" hint="pause alert campaign report by id">
		<cfargument name="inpCampaignId" type="numeric" required="true"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset var UpdateCampaignStatus= ""/>

		<cftry>

			<cfquery result="UpdateCampaignStatus" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.alertcampaign AS C
				SET
					C.Status_int = 1
				WHERE
					C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#"/>
				AND
					C.Status_int = 2
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	<!--- Campaign --->
</cfcomponent>