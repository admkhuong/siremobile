<cfcomponent>

    <cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">

    <cffunction name="GetAdminPermission" access="public" output="false" hint="Lookup whether current logged in account has admin permissions">
        <cfargument name="inpSkipRedirect" TYPE="any" required="no" default="0"/>

        <cfset var dataout = {} />
        <cfset var listUser  = '' >
        <cfset var GetAdminUserList = ''>

        <cftry>
            <cfif structKeyExists(Session, 'AdminUserList')>
                <cfset listUser = Session.AdminUserList>
            <cfelse>

                <cfquery name="GetAdminUserList" datasource="#Session.DBSourceEBM#">
                    SELECT
                        Value_txt
                    FROM
                        simpleobjects.sire_setting
                    WHERE
                        VariableName_txt LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="AdminUserList">
                    LIMIT
                        1
                </cfquery>

                <cfset listUser = GetAdminUserList.Value_txt />
                <cfset Session.AdminUserList = listUser />
            </cfif>

            <!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.ISADMINOK = "0" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfif ListFind(listUser, Session.USERID) GT 0>

                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.ISADMINOK = "1" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />


            <cfelse>

                <cfif inpSkipRedirect EQ 0>
                    <cflocation url="/session/sire/pages/admin-no-permission" addToken="no" />
                </cfif>

            </cfif>

            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = "-2" />
                <cfset dataout.ISADMINOK = "1" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>


        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetDevPermission" access="public" output="false" hint="Lookup whether current logged in account has Developer permissions">

        <cfset var dataout = {} />
        <cfset var listUser = '' />
        <cfset var GetDevUserList = ''>

        <cftry>
            <!--- check in session --->
            <cfif structKeyExists(Session, 'DevUserList')>
                <cfset listUser = Session.DevUserList/>
            <cfelse>
                <cfquery name="GetDevUserList" datasource="#Session.DBSourceEBM#">
                    SELECT
                        Value_txt
                    FROM
                        simpleobjects.sire_setting
                    WHERE
                        VariableName_txt LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="DevUserList">
                    LIMIT
                        1
                </cfquery>
                <cfset listUser = GetDevUserList.Value_txt/>
                <!--- SET TO SESSION --->
                <cfset Session.DevUserList = listUser/>
            </cfif>


            <!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.ISDEVOK = "0" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />



            <cfif ListFind(listUser, Session.USERID) GT 0>

                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.ISDEVOK = "1" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />

            <cfelse>

                <cfset dataout.RXRESULTCODE = "1" />
                <cfset dataout.ISDEVOK = "0" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />

            </cfif>


            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = "-2" />
                <cfset dataout.ISDEVOK = "1" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />


    </cffunction>


    <cffunction name="GetUserList" access="public" output="false" hint="Read User List - Based on Filters - Limit 10">
        <cfargument name="inpAccountEMail" TYPE="string" required="yes"/>

        <cfset var dataout = {} />
        <cfset var GetUserListQuery = '' />

        <cftry>

            <!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.GETUSERLISTQUERY = "" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="GetUserListQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserId_int,
                    EmailAddress_vch,
                    FirstName_vch,
                    LastName_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    EmailAddress_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpAccountEMail#%">
                    AND (CBPID_int = 1 OR CBPID_int IS NULL)
            </cfquery>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.GETUSERLISTQUERY = GetUserListQuery />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = "-2" />
                <cfset dataout.GETUSERLISTQUERY = "" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <!---  http://www.bennadel.com/blog/1239-Updated-Converting-A-ColdFusion-Query-To-CSV-Using-QueryToCSV-.htm  --->
    <cffunction
        name="QueryToCSV"
        access="public"
        returntype="string"
        output="false"
        hint="I take a query and convert it to a comma separated value string.">

        <!--- Define arguments. --->
        <cfargument
            name="Query"
            type="query"
            required="true"
            hint="I am the query being converted to CSV."
            />

        <cfargument
            name="Fields"
            required="false"
            default="#arguments.query.columnList#"
            hint="I am the list of query fields to be used when creating the CSV value. Print everything by default"
            />

        <cfargument
            name="CreateHeaderRow"
            type="boolean"
            required="false"
            default="true"
            hint="I flag whether or not to create a row of header values."
            />

        <cfargument
            name="Delimiter"
            type="string"
            required="false"
            default=","
            hint="I am the field delimiter in the CSV value."
            />

        <!--- Define the local scope. --->
        <cfset var LOCAL = {} />

        <!---
            First, we want to set up a column index so that we can
            iterate over the column names faster than if we used a
            standard list loop on the passed-in list.
        --->
        <cfset LOCAL.ColumnNames = [] />

        <!---
            Loop over column names and index them numerically. We
            are working with an array since I believe its loop times
            are faster than that of a list.
        --->
        <cfloop
            index="LOCAL.ColumnName"
            list="#ARGUMENTS.Fields#"
            delimiters=",">

            <!--- Store the current column name. --->
            <cfset ArrayAppend(
                LOCAL.ColumnNames,
                Trim( LOCAL.ColumnName )
                ) />

        </cfloop>

        <!--- Store the column count. --->
        <cfset LOCAL.ColumnCount = ArrayLen( LOCAL.ColumnNames ) />


        <!---
            Now that we have our index in place, let's create
            a string buffer to help us build the CSV value more
            effiently.
        --->
        <cfset LOCAL.Buffer = CreateObject( "java", "java.lang.StringBuffer" ).Init() />

        <!--- Create a short hand for the new line characters. --->
        <cfset LOCAL.NewLine = (Chr( 13 ) & Chr( 10 )) />


        <!--- Check to see if we need to add a header row. --->
        <cfif ARGUMENTS.CreateHeaderRow>

            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />

            <!--- Loop over the column names. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">

                <!--- Add the field name to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#LOCAL.ColumnNames[ LOCAL.ColumnIndex ]#""" />

            </cfloop>

            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />

        </cfif>


        <!---
            Now that we have dealt with any header value, let's
            convert the query body to CSV. When doing this, we are
            going to qualify each field value. This is done be
            default since it will be much faster than actually
            checking to see if a field needs to be qualified.
        --->

        <!--- Loop over the query. --->
        <cfloop query="ARGUMENTS.Query">

            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />

            <!--- Loop over the columns. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">

                <!--- Add the field to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#Replace( ARGUMENTS.Query[ LOCAL.ColumnNames[ LOCAL.ColumnIndex ] ][ ARGUMENTS.Query.CurrentRow ], """", """""", "all" )#""" />

                <!--- JLP Data with newlines will screw up export so remove them - covers both linux and dos newlines--->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 13 ), "", "ALL") />
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 10 ), "", "ALL") />
            </cfloop>


            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />

        </cfloop>


        <!--- Return the CSV value. --->
        <cfreturn LOCAL.Buffer.ToString() />
    </cffunction>

    <cffunction name="GetAllReportCount" access="remote" output="false" hint="Get user plan report on entire system">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfset var dataout = {} />
        <cfset var getUserPlanReport = '' />
        <cfset var getUniqeLoginReport = '' />
        <cfset var getTotalUserReport = '' />
        <cfset var getSignUpInactiveReport = '' />
        <cfset var getKeywordSupportRequest = '' />
        <cfset var getActiveUserReport = '' />
        <cfset var getInActiveUserReport = '' />
        <cfset var getNewPaidUserReport = '' />
        <cfset var getUpgradesUserReport = '' />
        <cfset var getDowngradesUserReport = '' />
        <cfset var getTotalNewUserReport = '' />
        <cfset var getMessageReportResult = '' />
        <cfset var getAddonResult1 = '' />
        <cfset var getAddonResult2 = '' />
        <cfset var getEnterpriseUserReport = '' />
        <cfset var getEnterpriseUserRevenue = '' />
        <cfset var getWordPayReport = '' />
        <cfset var getGetBingSpend = ''/>
        <cfset var getGetFacebookAdsSpend = ''/>
        <cfset var getFailedMFAreport = ''/>
        <cfset var getRequestDowngradesUserReport = {}/>
        <cfset var getEnterpriseBillingReport = ''/>
        <cfset var todayDate	= '' />
        <cfset var getMTCountOfThisMonth	= '' />


        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

         <!--- Inital Today Server time --->
        <cfset todayDate = Now()>
        <cfset var CurrentDate   = CreateDate(#DatePart("yyyy", todayDate)#, #DatePart("m", todayDate)#, #DatePart("d", todayDate)#) />
        <cfset CurrentDate = DateFormat(CurrentDate, 'yyyy-mm-dd')/>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.FREE = 0/>
        <cfset dataout.INDV = 0/>
        <cfset dataout.PRO = 0/>
        <cfset dataout.SMB = 0/>
        <cfset dataout.UNIQUESIGNIN = 0/>
        <cfset dataout.TOTALUSER = 0/>
        <cfset dataout.SIGNUPINACTIVE = 0/>
        <cfset dataout.KEYWORDSUPPORTREQUEST = 0/>
        <cfset dataout.ACTIVEUSER = 0/>
        <cfset dataout.INACTIVEUSER = 0/>
        <cfset dataout.NEWPAIDUSER = 0/>
        <cfset dataout.UPGRAGES = 0/>
        <cfset dataout.DOWNGRADES = 0/>
        <cfset dataout.NEWUSER = 0/>
        <cfset dataout.MESSAGESENT = 0/>
        <cfset dataout.MESSAGERECEIVED = 0/>
        <cfset dataout.TOTALOPTIN = 0/>
        <cfset dataout.TOTALOPTOUT = 0/>
        <cfset dataout.BUYCREDIT = 0/>
        <cfset dataout.BUYKEYWORD = 0/>
        <cfset dataout.ENTERPRISECOUNT = 0/>
        <cfset dataout.WORDPAY = 0/>
        <cfset dataout.ENTERPRISEREVENUE = 0/>
        <cfset dataout.GETBINGSPEND = 0/>
        <cfset dataout.GETFBADSSPEND = 0/>
        <cfset dataout.FAILEDMFA = 0/>
        <cfset dataout.REQUESTDOWNGRADES = 0/>
        <cfset dataout.MTCOUNT = 0/>
        <cfset dataout.ENTERPRISEBILLING = 0/>
        <cfset dataout.CURRENTDATE = '#CurrentDate#'/>


        <cftry>
            <!--- Count User Plan --->
            <cfset getUserPlanReport = GetTotalUserPlan(arguments.inpEndDate)/>
            <cfset dataout.FREE = getUserPlanReport.FREE/>
            <cfset dataout.INDV = getUserPlanReport.INDV/>
            <cfset dataout.PRO = getUserPlanReport.PRO/>
            <cfset dataout.SMB = getUserPlanReport.SMB/>

            <!--- Count active User --->
            <cfset getActiveUserReport = GetTotalUserActive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ACTIVEUSER = getActiveUserReport.RESULT/>

            <!--- Count unique login --->
            <cfset getUniqeLoginReport = GetTotalUniqueLogin(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.UNIQUESIGNIN = getUniqeLoginReport.RESULT/>

            <!--- Count total number of inactive user --->
            <!---  This number does not use in crm any more
            <cfset getInActiveUserReport = GetTotalUserInActive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.INACTIVEUSER = getInActiveUserReport.RESULT/>
            --->

            <!--- Count total users --->
            <cfset getTotalUserReport = getTotalUsers(arguments.inpEndDate) />
            <cfset dataout.TOTALUSER = getTotalUserReport.RESULT/>

            <!--- Count total new users --->
            <cfset getTotalNewUserReport = getTotalNewUsers(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.NEWUSER = getTotalNewUserReport.RESULT/>

            <!--- Count total signup inactive users --->
            <cfset getSignUpInactiveReport = GetTotalUserSignUpInactive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.SIGNUPINACTIVE = getSignUpInactiveReport.RESULT/>

            <!--- Count total number keyword support request --->
            <cfset getKeywordSupportRequest = GetTotalKeywordSupportRequestReports(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.KEYWORDSUPPORTREQUEST = getKeywordSupportRequest.RESULT/>

            <!--- Count total number of upgrages user --->
            <cfset getUpgradesUserReport = getTotalUserUpgradesNew(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.UPGRAGES = getUpgradesUserReport.RESULT/>

            <!--- Count total number of downgrades user --->
            <cfset getDowngradesUserReport = getTotalUserDowngrades(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.DOWNGRADES = getDowngradesUserReport.RESULT/>

            <!--- Count total message sent-received, opt in-out of the system --->
            <cfset getMessageReportResult = GetMessageReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.MESSAGESENT = getMessageReportResult.MESSAGESENT/>
            <cfset dataout.MESSAGERECEIVED = getMessageReportResult.MESSAGERECEIVED/>
            <cfset dataout.TOTALOPTIN = getMessageReportResult.TOTALOPTIN/>
            <cfset dataout.TOTALOPTOUT = getMessageReportResult.TOTALOPTOUT/>

            <!--- Count total buy credit, buy keyword --->
            <cfset getAddonResult1 = GetCreditAddonReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset getAddonResult2 = GetKeywordAddonReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.BUYCREDIT = getAddonResult1.BUYCREDIT/>
            <cfset dataout.BUYKEYWORD = getAddonResult2.BUYKEYWORD/>

            <cfset getEnterpriseUserReport = CountEnterpriseUser(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISECOUNT = getEnterpriseUserReport.RESULT/>

            <cfset getEnterpriseUserRevenue = GetEnterpriseRevenue(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISEREVENUE = getEnterpriseUserRevenue.RESULT/>

            <cfset getGetBingSpend = GetBingSpend(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.GETBINGSPEND = getGetBingSpend.RESULT/>

            <cfset getGetFacebookAdsSpend = getFacebookAdsSpend(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.GETFBADSSPEND = getGetFacebookAdsSpend.RESULT/>

            <cfset getWordPayReport = Getrevenuewoldpaytotal(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.WORDPAY = getWordPayReport.TOTALREVENUEWP/>
            <cfset dataout.WORDPAYREFUND = getWordPayReport.TOTALREFUNDWP/>
            <cfset dataout.WORDPAYTOTAL = getWordPayReport.TOTALREVENUEWPALL/>

            <!--- Total Failed MFA --->
            <cfset getFailedMFAreport = GetFailedMFA(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.FAILEDMFA = getFailedMFAreport.RESULT/>

            <!--- New Paid Users --->
            <cfset getNewPaidUserReport = getTotalNewPaidUsers(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.NEWPAIDUSER = getNewPaidUserReport.RESULT/>

            <!--- Count total number of user request downgrades --->
            <cfset getRequestDowngradesUserReport = getTotalUserRequestDowngrades(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.REQUESTDOWNGRADES = getRequestDowngradesUserReport.RESULT/>

            <cfset getMTCountOfThisMonth = getTotalMTCountOfThisMonth()/>
            <cfset dataout.MTCOUNT = getMTCountOfThisMonth.RESULT/>

            <!--- Count total Enterprise Billing --->
          <!---   <cfset getEnterpriseBillingReport = GetEnterpriseBillingTotal(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISEBILLING = getEnterpriseBillingReport.ENTERPRISEBILLING/> --->

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="GetAllReportCountWOCRMNew" access="public" output="false" hint="Get data CRM report without New Table">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfset var dataout = {} />
        <cfset var getUserPlanReport = '' />
        <cfset var getUniqeLoginReport = '' />
        <cfset var getTotalUserReport = '' />
        <cfset var getSignUpInactiveReport = '' />
        <cfset var getActiveUserReport = '' />
        <cfset var getInActiveUserReport = '' />
        <cfset var getUpgradesUserReport = '' />
        <cfset var getDowngradesUserReport = '' />
        <cfset var getTotalNewUserReport = '' />
        <cfset var getEnterpriseUserReport = '' />
        <cfset var getNewPaidUserReport = '' />
        <!---<cfset var getEnterpriseUserRevenue = ''/>--->
        <cfset var getGetBingSpend = ''/>
        <cfset var getGetFacebookAdsSpend = ''/>
        <cfset var getFailedMFAreport = ''/>
        <cfset var getRequestDowngradesUserReport = {}/>
        <cfset var getEnterpriseBillingReport = ''/>
        <cfset var getMTCountOfThisMonth	= '' />

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.FREE = 0/>
        <cfset dataout.INDV = 0/>
        <cfset dataout.PRO = 0/>
        <cfset dataout.SMB = 0/>
        <cfset dataout.UNIQUESIGNIN = 0/>
        <cfset dataout.TOTALUSER = 0/>
        <cfset dataout.SIGNUPINACTIVE = 0/>
        <cfset dataout.ACTIVEUSER = 0/>
        <cfset dataout.INACTIVEUSER = 0/>
        <cfset dataout.NEWPAIDUSER = 0/>
        <cfset dataout.UPGRAGES = 0/>
        <cfset dataout.DOWNGRADES = 0/>
        <cfset dataout.NEWUSER = 0/>
        <cfset dataout.ENTERPRISECOUNT = 0/>
        <!---<cfset dataout.ENTERPRISEREVENUE = 0/>--->
        <cfset dataout.GETBINGSPEND = 0/>
        <cfset dataout.GETFBADSSPEND = 0/>
        <cfset dataout.FAILEDMFA = 0/>
        <cfset dataout.REQUESTDOWNGRADES = 0/>
        <cfset dataout.MTCOUNT = 0/>
        <cfset dataout.ENTERPRISEBILLING = 0/>

        <cftry>
            <!--- Count User Plan --->
            <cfset getUserPlanReport = GetTotalUserPlan(arguments.inpEndDate)/>
            <cfset dataout.FREE = getUserPlanReport.FREE/>
            <cfset dataout.INDV = getUserPlanReport.INDV/>
            <cfset dataout.PRO = getUserPlanReport.PRO/>
            <cfset dataout.SMB = getUserPlanReport.SMB/>

            <!--- Count active User --->
            <cfset getActiveUserReport = GetTotalUserActive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ACTIVEUSER = getActiveUserReport.RESULT/>

            <!--- Count unique login --->
            <cfset getUniqeLoginReport = GetTotalUniqueLogin(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.UNIQUESIGNIN = getUniqeLoginReport.RESULT/>

            <!--- Count total number of inactive user --->
            <cfset getInActiveUserReport = GetTotalUserInActive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.INACTIVEUSER = getInActiveUserReport.RESULT/>

            <!--- Count total users --->
            <cfset getTotalUserReport = getTotalUsers(arguments.inpEndDate) />
            <cfset dataout.TOTALUSER = getTotalUserReport.RESULT/>

            <!--- Count total new users --->
            <cfset getTotalNewUserReport = getTotalNewUsers(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.NEWUSER = getTotalNewUserReport.RESULT/>

            <!--- Count total signup inactive users --->
            <cfset getSignUpInactiveReport = GetTotalUserSignUpInactive(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.SIGNUPINACTIVE = getSignUpInactiveReport.RESULT/>

            <!--- Count total number of upgrages user --->
            <cfset getUpgradesUserReport = getTotalUserUpgradesNew(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.UPGRAGES = getUpgradesUserReport.RESULT/>

            <!--- Count total number of downgrades user --->
            <cfset getDowngradesUserReport = getTotalUserDowngrades(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.DOWNGRADES = getDowngradesUserReport.RESULT/>/>

            <!--- Count Enterprise User --->
            <cfset getEnterpriseUserReport = CountEnterpriseUser(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISECOUNT = getEnterpriseUserReport.RESULT/>
            <!---
            <cfset getEnterpriseUserRevenue = GetEnterpriseRevenue(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISEREVENUE = getEnterpriseUserRevenue.RESULT/>
            --->
            <cfset getGetBingSpend = GetBingSpend(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.GETBINGSPEND = getGetBingSpend.RESULT/>

            <cfset getGetFacebookAdsSpend = getFacebookAdsSpend(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.GETFBADSSPEND = getGetFacebookAdsSpend.RESULT/>

            <!--- Total Failed MFA --->
            <cfset getFailedMFAreport = GetFailedMFA(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.FAILEDMFA = getFailedMFAreport.RESULT/>

            <!--- New Paid Users --->
            <cfset getNewPaidUserReport = getTotalNewPaidUsers(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.NEWPAIDUSER = getNewPaidUserReport.RESULT/>

            <!--- Count total number of user request downgrades --->
            <cfset getRequestDowngradesUserReport = getTotalUserRequestDowngrades(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.REQUESTDOWNGRADES = getRequestDowngradesUserReport.RESULT/>

            <cfset getMTCountOfThisMonth = getTotalMTCountOfThisMonth()/>
            <cfset dataout.MTCOUNT = getMTCountOfThisMonth.RESULT/>

            <!--- Count total Enterprise Billing --->
           <!---  <cfset getEnterpriseBillingReport = GetEnterpriseBillingTotal(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISEBILLING = getEnterpriseBillingReport.ENTERPRISEBILLING/> --->

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="GetAllReportCountCRMNew" access="public" output="false" hint="Get data CRM from Detail Table">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfset var dataout = {} />
        <cfset var getKeywordSupportRequest = '' />
        <cfset var getMessageReportResult = '' />
        <cfset var getAddonResult1 = '' />
        <cfset var getAddonResult2 = '' />
        <cfset var getWordPayReport = '' />
        <cfset var getEnterpriseReport = ''/>
        <cfset var getEnterpriseBillingReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.KEYWORDSUPPORTREQUEST = 0/>
        <cfset dataout.MESSAGESENT = 0/>
        <cfset dataout.MESSAGERECEIVED = 0/>
        <cfset dataout.TOTALOPTIN = 0/>
        <cfset dataout.TOTALOPTOUT = 0/>
        <cfset dataout.BUYCREDIT = 0/>
        <cfset dataout.BUYKEYWORD = 0/>
        <cfset dataout.WORDPAY = 0/>
        <cfset dataout.WORDPAYREFUND = 0/>
        <cfset dataout.WORDPAYTOTAL = 0/>
        <cfset dataout.ENTERPRISEREVENUE = 0/>
        <cfset dataout.ENTERPRISEBILLING = 0/>
        <cftry>
             <!--- Count total number keyword support request --->
            <cfset getKeywordSupportRequest = GetTotalKeywordSupportRequestReports(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.KEYWORDSUPPORTREQUEST = getKeywordSupportRequest.RESULT/>

            <!--- Count total message sent-received, opt in-out of the system --->
            <cfset getMessageReportResult = GetMessageReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.MESSAGESENT = getMessageReportResult.MESSAGESENT/>
            <cfset dataout.MESSAGERECEIVED = getMessageReportResult.MESSAGERECEIVED/>
            <cfset dataout.TOTALOPTIN = getMessageReportResult.TOTALOPTIN/>
            <cfset dataout.TOTALOPTOUT = getMessageReportResult.TOTALOPTOUT/>

            <!--- Count total buy credit, buy keyword --->
            <cfset getAddonResult1 = GetCreditAddonReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset getAddonResult2 = GetKeywordAddonReport(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.BUYCREDIT = getAddonResult1.BUYCREDIT/>
            <cfset dataout.BUYKEYWORD = getAddonResult2.BUYKEYWORD/>

            <cfset getWordPayReport = Getrevenuewoldpaytotal(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.WORDPAY = getWordPayReport.TOTALREVENUEWP/>
            <cfset dataout.WORDPAYREFUND = getWordPayReport.TOTALREFUNDWP/>
            <cfset dataout.WORDPAYTOTAL = getWordPayReport.TOTALREVENUEWPALL/>

            <cfset getEnterpriseReport = GetEnterpriseRevenue(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout.ENTERPRISEREVENUE = getEnterpriseReport.RESULT/>


            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

        <!--- Get data CRM SUMMARY --->
    <cffunction name="GetDataCRMSummary" access="remote" hint="Get data CRM Summary">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset var DateTime_str = ""/>
        <cfset var Dateend = ""/>
        <cfset var Datestart = ""/>
        <cfset var DateTimeStart_curr = ""/>
        <cfset var DateTimeEnd_curr = ""/>
        <cfset var RetVarGetAllReportCount = ""/>
        <cfset var RetVarGetAllReportCountWOCRMNew = ""/>
        <cfset var RetVarGetAllReportCountCRMNew = ""/>
        <cfset var GetDataCrmNew = ""/>
        <cfset var GetDateQuery = ""/>
        <cfset var todayDate	= '' />
         <!--- Inital Today Server time --->
        <cfset todayDate = Now()>
        <cfset var CurrentDate   = CreateDate(#DatePart("yyyy", todayDate)#, #DatePart("m", todayDate)#, #DatePart("d", todayDate)#) />
        <cfset CurrentDate = DateFormat(CurrentDate, 'yyyy-mm-dd')/>

        <cfquery name="GetDateQuery" datasource="#Session.DBSourceEBM#">
            SELECT
                    DATE_ADD(Max(DateTime_dt), INTERVAL 1 DAY) AS StartDate,
                    Max(DateTime_dt) AS MaxDate
            From
                    simplexresults.crm_summary
        </cfquery>

        <cfif GetDateQuery.MaxDate eq "">
            <!--- New Table empty --->
                <cfset RetVarGetAllReportCount = GetAllReportCount(arguments.inpStartDate, arguments.inpEndDate)/>
                <cfreturn RetVarGetAllReportCount />
        <cfelse>
                <cfset var dataout = {} />
                <cfset dataout.FREE = 0/>
                <cfset dataout.INDV = 0/>
                <cfset dataout.PRO = 0/>
                <cfset dataout.SMB = 0/>
                <cfset dataout.UNIQUESIGNIN = 0/>
                <cfset dataout.TOTALUSER = 0/>
                <cfset dataout.SIGNUPINACTIVE = 0/>
                <cfset dataout.ACTIVEUSER = 0/>
                <cfset dataout.INACTIVEUSER = 0/>
                <cfset dataout.NEWPAIDUSER = 0/>
                <cfset dataout.NEWUSER = 0/>
                <cfset dataout.UPGRAGES = 0/>
                <cfset dataout.DOWNGRADES = 0/>
                <cfset dataout.ENTERPRISECOUNT = 0/>
                <cfset dataout.GETBINGSPEND = 0/>
                <cfset dataout.GETFBADSSPEND = 0/>
                <cfset dataout.FAILEDMFA = 0/>
                <cfset dataout.REQUESTDOWNGRADES = 0/>

                <cfset dataout.KEYWORDSUPPORTREQUEST = 0/>
                <cfset dataout.MESSAGESENT = 0/>
                <cfset dataout.MESSAGERECEIVED = 0/>
                <cfset dataout.TOTALOPTIN = 0/>
                <cfset dataout.TOTALOPTOUT = 0/>
                <cfset dataout.BUYCREDIT = 0/>
                <cfset dataout.BUYKEYWORD = 0/>
                <cfset dataout.WORDPAY = 0/>
                <cfset dataout.WORDPAYREFUND = 0/>
                <cfset dataout.WORDPAYTOTAL = 0/>
                <cfset dataout.ENTERPRISEREVENUE = 0/>
                <cfset dataout.MTCOUNT = 0/>
                <cfset dataout.ENTERPRISEBILLING = 0/>
                <cfset dataout.CURRENTDATE = '#CurrentDate#'/>

                <cfset Dateend = DateFormat(endDate, 'yyyy-mm-dd')/>
                <cfset Datestart = DateFormat(startDate, 'yyyy-mm-dd')/>

                <cfset dataout.MESSAGE = '' />
                <cfset dataout.MESSAGELOG = '' />
                <cfset dataout.ERRMESSAGE = '' />
                <cfset dataout.TYPE = '' />
                <cfset dataout.RXRESULTCODE = -1 />

            <cfif DateDiff( "d", GetDateQuery.MaxDate, endDate ) lte 0>
            <!--- Max(date) > inpEndDate --->
                <cfset dataout.MESSAGELOG = "Max(date) > inpEndDate "/>
                <cftry>
                    <!--- Get data WO CRM New --->
                    <cfset RetVarGetAllReportCountWOCRMNew = deserializeJSON(GetAllReportCountWOCRMNew(arguments.inpStartDate, arguments.inpEndDate))/>
                    <cfset dataout.FREE = RetVarGetAllReportCountWOCRMNew['FREE']/>
                    <cfset dataout.INDV = RetVarGetAllReportCountWOCRMNew['INDV']/>
                    <cfset dataout.PRO = RetVarGetAllReportCountWOCRMNew['PRO']/>
                    <cfset dataout.SMB = RetVarGetAllReportCountWOCRMNew['SMB']/>
                    <cfset dataout.UNIQUESIGNIN = RetVarGetAllReportCountWOCRMNew['UNIQUESIGNIN']/>
                    <cfset dataout.TOTALUSER = RetVarGetAllReportCountWOCRMNew['TOTALUSER']/>
                    <cfset dataout.SIGNUPINACTIVE = RetVarGetAllReportCountWOCRMNew['SIGNUPINACTIVE']/>
                    <cfset dataout.ACTIVEUSER = RetVarGetAllReportCountWOCRMNew['ACTIVEUSER']/>
                    <cfset dataout.INACTIVEUSER = RetVarGetAllReportCountWOCRMNew['INACTIVEUSER']/>
                    <cfset dataout.NEWPAIDUSER = RetVarGetAllReportCountWOCRMNew['NEWPAIDUSER']/>
                    <cfset dataout.NEWUSER = RetVarGetAllReportCountWOCRMNew['NEWUSER']/>
                    <cfset dataout.UPGRAGES = RetVarGetAllReportCountWOCRMNew['UPGRAGES']/>
                    <cfset dataout.DOWNGRADES = RetVarGetAllReportCountWOCRMNew['DOWNGRADES']/>
                    <cfset dataout.ENTERPRISECOUNT = RetVarGetAllReportCountWOCRMNew['ENTERPRISECOUNT']/>
                    <!---<cfset dataout.ENTERPRISEREVENUE = RetVarGetAllReportCountWOCRMNew['ENTERPRISEREVENUE']/>--->
                    <cfset dataout.GETBINGSPEND = RetVarGetAllReportCountWOCRMNew['GETBINGSPEND']/>
                    <cfset dataout.GETFBADSSPEND = RetVarGetAllReportCountWOCRMNew['GETFBADSSPEND']/>
                    <cfset dataout.FAILEDMFA = RetVarGetAllReportCountWOCRMNew['FAILEDMFA']/>
                    <cfset dataout.REQUESTDOWNGRADES = RetVarGetAllReportCountWOCRMNew['REQUESTDOWNGRADES']/>
                    <cfset dataout.MTCOUNT = RetVarGetAllReportCountWOCRMNew['MTCOUNT']/>
                    <cfset dataout.ENTERPRISEBILLING = RetVarGetAllReportCountWOCRMNew['ENTERPRISEBILLING']/>
                    <!--- Get data CRM New --->
                    <cfquery name="GetDataCrmNew" datasource="#Session.DBSourceREAD#">
                        SELECT
                            IFNULL(SUM(p.KeyWordSupportRequest_int),0) As KEYWORDSUPPORTREQUEST,
                            IFNULL(SUM(p.MessageSent_int),0) As MESSAGESENT,
                            IFNULL(SUM(p.MessageReceived_int),0) As MESSAGERECEIVED,
                            IFNULL(SUM(p.TotalOptin_int),0) As TOTALOPTIN,
                            IFNULL(SUM(p.TotalOptout_int),0) As TOTALOPTOUT,
                            IFNULL(SUM(p.BuyCreadit_int),0) As BUYCREDIT,
                            IFNULL(SUM(p.BuyKeyWord_int),0) As BUYKEYWORD,
                            (IFNULL(SUM(p.WordPay_int),0) + IFNULL(SUM(p.Paypal_dbl),0)) As WORDPAY,
                            IFNULL(SUM(p.WordPayRefund_int),0) As WORDPAYREFUND,
                            IFNULL(SUM(p.EnterpriseRevenue_dbl),0) As ENTERPRISEREVENUE,
                            (IFNULL(SUM(p.WordPay_int),0) + IFNULL(SUM(p.Paypal_dbl),0) - IFNULL(SUM(p.WordPayRefund_int),0)) As WORDPAYTOTAL
                        FROM
                            simplexresults.crm_summary as p
                        WHERE
                            DATE(p.DateTime_dt) >= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Datestart#">)
                        AND
                            DATE(p.DateTime_dt) <= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Dateend#">)
                    </cfquery>

                    <cfif GetDataCrmNew.RECORDCOUNT GT 0>
                        <cfset dataout.KEYWORDSUPPORTREQUEST = GetDataCrmNew.KEYWORDSUPPORTREQUEST/>
                        <cfset dataout.MESSAGESENT = GetDataCrmNew.MESSAGESENT/>
                        <cfset dataout.MESSAGERECEIVED = GetDataCrmNew.MESSAGERECEIVED/>
                        <cfset dataout.TOTALOPTIN = GetDataCrmNew.TOTALOPTIN/>
                        <cfset dataout.TOTALOPTOUT = GetDataCrmNew.TOTALOPTOUT/>
                        <cfset dataout.BUYCREDIT = GetDataCrmNew.BUYCREDIT/>
                        <cfset dataout.BUYKEYWORD = GetDataCrmNew.BUYKEYWORD/>
                        <cfset dataout.WORDPAY = GetDataCrmNew.WORDPAY/>
                        <cfset dataout.WORDPAYREFUND = GetDataCrmNew.WORDPAYREFUND/>
                        <cfset dataout.WORDPAYTOTAL = GetDataCrmNew.WORDPAYTOTAL/>
                        <cfset dataout.ENTERPRISEREVENUE = GetDataCrmNew.ENTERPRISEREVENUE/>
                    </cfif>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = "Max(date) > inpEndDate"/>

                    <cfcatch type="any">
                        <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                        <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                        <cfset dataout.TYPE = cfcatch.TYPE />
                    </cfcatch>

                </cftry>
                <cfreturn serializeJSON(dataout)/>

            <cfelseif DateDiff( "d", GetDateQuery.MaxDate, startDate ) gt 0>
            <!--- Max(date) < inpStartDate--->
                <cfset dataout.MESSAGELOG = "Max(date) < inpStartDate"/>
                <cfset RetVarGetAllReportCount = GetAllReportCount(arguments.inpStartDate, arguments.inpEndDate)/>
                <cfreturn RetVarGetAllReportCount />

            <cfelseif DateDiff( "d", GetDateQuery.MaxDate, startDate ) lte 0 AND DateDiff( "d", GetDateQuery.MaxDate, endDate ) gt 0>
            <!---inpStartDate <= Max(date) <=  inpEndDate--->

                <cftry>
                    <cfset DateTimeEnd_curr = DateFormat(GetDateQuery.MaxDate, 'yyyy-mm-dd')/>
                    <cfset DateTimeStart_curr = {
                            day: DateFormat(GetDateQuery.StartDate, 'dd'),
                            month: DateFormat(GetDateQuery.StartDate, 'mm'),
                            year : DateFormat(GetDateQuery.StartDate, 'yyyy')
                    }>

                    <cfset RetVarGetAllReportCountWOCRMNew = deserializeJSON(GetAllReportCountWOCRMNew(arguments.inpStartDate, arguments.inpEndDate))/>

                    <cfset dataout.FREE = RetVarGetAllReportCountWOCRMNew['FREE']/>
                    <cfset dataout.INDV = RetVarGetAllReportCountWOCRMNew['INDV']/>
                    <cfset dataout.PRO = RetVarGetAllReportCountWOCRMNew['PRO']/>
                    <cfset dataout.SMB = RetVarGetAllReportCountWOCRMNew['SMB']/>
                    <cfset dataout.UNIQUESIGNIN = RetVarGetAllReportCountWOCRMNew['UNIQUESIGNIN']/>
                    <cfset dataout.TOTALUSER = RetVarGetAllReportCountWOCRMNew['TOTALUSER']/>
                    <cfset dataout.SIGNUPINACTIVE = RetVarGetAllReportCountWOCRMNew['SIGNUPINACTIVE']/>
                    <cfset dataout.ACTIVEUSER = RetVarGetAllReportCountWOCRMNew['ACTIVEUSER']/>
                    <cfset dataout.INACTIVEUSER = RetVarGetAllReportCountWOCRMNew['INACTIVEUSER']/>
                    <cfset dataout.NEWPAIDUSER = RetVarGetAllReportCountWOCRMNew['NEWPAIDUSER']/>
                    <cfset dataout.NEWUSER = RetVarGetAllReportCountWOCRMNew['NEWUSER']/>
                    <cfset dataout.UPGRAGES = RetVarGetAllReportCountWOCRMNew['UPGRAGES']/>
                    <cfset dataout.DOWNGRADES = RetVarGetAllReportCountWOCRMNew['DOWNGRADES']/>
                    <cfset dataout.ENTERPRISECOUNT = RetVarGetAllReportCountWOCRMNew['ENTERPRISECOUNT']/>
                    <!---<cfset dataout.ENTERPRISEREVENUE = RetVarGetAllReportCountWOCRMNew['ENTERPRISEREVENUE']/>--->
                    <cfset dataout.GETBINGSPEND = RetVarGetAllReportCountWOCRMNew['GETBINGSPEND']/>
                    <cfset dataout.GETFBADSSPEND = RetVarGetAllReportCountWOCRMNew['GETFBADSSPEND']/>
                    <cfset dataout.FAILEDMFA = RetVarGetAllReportCountWOCRMNew['FAILEDMFA']/>
                    <cfset dataout.REQUESTDOWNGRADES = RetVarGetAllReportCountWOCRMNew['REQUESTDOWNGRADES']/>
                    <cfset dataout.MTCOUNT = RetVarGetAllReportCountWOCRMNew['MTCOUNT']/>
                    <cfset dataout.ENTERPRISEBILLING = RetVarGetAllReportCountWOCRMNew['ENTERPRISEBILLING']/>
                    <!--- data from  inpStartDate To DateTimeEnd_curr--->
                    <!--- Get data CRM New --->
                    <cfquery name="GetDataCrmNew" datasource="#Session.DBSourceREAD#">
                        SELECT
                            IFNULL(SUM(p.KeyWordSupportRequest_int),0) As KEYWORDSUPPORTREQUEST,
                            IFNULL(SUM(p.MessageSent_int),0) As MESSAGESENT,
                            IFNULL(SUM(p.MessageReceived_int),0) As MESSAGERECEIVED,
                            IFNULL(SUM(p.TotalOptin_int),0) As TOTALOPTIN,
                            IFNULL(SUM(p.TotalOptout_int),0) As TOTALOPTOUT,
                            IFNULL(SUM(p.BuyCreadit_int),0) As BUYCREDIT,
                            IFNULL(SUM(p.BuyKeyWord_int),0) As BUYKEYWORD,
                            (IFNULL(SUM(p.WordPay_int),0) + IFNULL(SUM(p.Paypal_dbl),0)) As WORDPAY,
                            IFNULL(SUM(p.WordPayRefund_int),0) As WORDPAYREFUND,
                            IFNULL(SUM(p.EnterpriseRevenue_dbl),0) As ENTERPRISEREVENUE,
                            (IFNULL(SUM(p.WordPay_int),0) + IFNULL(SUM(p.Paypal_dbl),0) - IFNULL(SUM(p.WordPayRefund_int),0)) As WORDPAYTOTAL
                        FROM
                            simplexresults.crm_summary as p
                        WHERE
                            DATE(p.DateTime_dt) >= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Datestart#">)
                        AND
                            DATE(p.DateTime_dt) <= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DateTimeEnd_curr#">)
                    </cfquery>

                    <cfset RetVarGetAllReportCountCRMNew = deserializeJSON(GetAllReportCountCRMNew(serializeJSON(DateTimeStart_curr), arguments.inpEndDate))/>

                    <cfset dataout.KEYWORDSUPPORTREQUEST = GetDataCrmNew.KEYWORDSUPPORTREQUEST + RetVarGetAllReportCountCRMNew['KEYWORDSUPPORTREQUEST']/>
                    <cfset dataout.MESSAGESENT = GetDataCrmNew.MESSAGESENT + RetVarGetAllReportCountCRMNew['MESSAGESENT']/>
                    <cfset dataout.MESSAGERECEIVED = GetDataCrmNew.MESSAGERECEIVED + RetVarGetAllReportCountCRMNew['MESSAGERECEIVED']/>
                    <cfset dataout.TOTALOPTIN = GetDataCrmNew.TOTALOPTIN + RetVarGetAllReportCountCRMNew['TOTALOPTIN']/>
                    <cfset dataout.TOTALOPTOUT = GetDataCrmNew.TOTALOPTOUT + RetVarGetAllReportCountCRMNew['TOTALOPTOUT']/>
                    <cfset dataout.BUYCREDIT = GetDataCrmNew.BUYCREDIT + RetVarGetAllReportCountCRMNew['BUYCREDIT']/>
                    <cfset dataout.BUYKEYWORD = GetDataCrmNew.BUYKEYWORD + RetVarGetAllReportCountCRMNew['BUYKEYWORD']/>
                    <cfset dataout.WORDPAY = GetDataCrmNew.WORDPAY + RetVarGetAllReportCountCRMNew['WORDPAY']/>
                    <cfset dataout.WORDPAYREFUND = GetDataCrmNew.WORDPAYREFUND + RetVarGetAllReportCountCRMNew['WORDPAYREFUND']/>
                    <cfset dataout.WORDPAYTOTAL = GetDataCrmNew.WORDPAYTOTAL + RetVarGetAllReportCountCRMNew['WORDPAYTOTAL']/>
                    <cfset dataout.ENTERPRISEREVENUE = GetDataCrmNew.ENTERPRISEREVENUE + RetVarGetAllReportCountCRMNew['ENTERPRISEREVENUE']/>

                    <cfset dataout.MESSAGELOG = "inpStartDate <= Max(date) <=  inpEndDate"/>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfcatch type="any">
                        <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                        <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                        <cfset dataout.TYPE = cfcatch.TYPE />
                    </cfcatch>
                </cftry>
             <cfreturn serializeJSON(dataout)/>
            </cfif>
        </cfif>
    </cffunction>

    <!--- Set job refresh data crm report --->
    <cffunction name="SetJobRefreshCrmData" access="remote" hint="Set job refresh Crm data">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />

        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.WORDPAY = 0 />

        <cfset var SetjobRefreshDataCrm = '' />
        <cfset var SetFlagRefreshDataCrm = '' />
        <cfset var URL_vch_new = '' />
        <cfset URL_vch_new = 'http://cron.siremobile.com/public/sire/models/cfm/noc/act_sire_NOC_refresh_data_crm_sumary.cfm?inpStartDate='&startDate&'&inpEndDate='&endDate/>

        <cftry>
            <!--- Set Flag for refresh data CRM --->
             <cfquery name="SetFlagRefreshDataCrm" datasource="#session.DBSourceEBM#">
                UPDATE
                    simplexresults.crm_summary
                SET
                    RefreshStatus_int = 1
                WHERE
                    DATE(DateTime_dt) >= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">)
                AND
                    DATE(DateTime_dt) <= DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">)
            </cfquery>
            <!--- Set job refresh data CRM --->
            <cfquery name="SetjobRefreshDataCrm" datasource="#session.DBSourceEBM#">
                UPDATE
                    simpleobjects.sire_noc_settings
                SET
                    URL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#URL_vch_new#">,
                    LaunchedTime_bi = 0
                WHERE
                    Name_vch = 'Refresh data CRM'
            </cfquery>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTotalUserPlan" access="public" output="false" hint="Get user plan count">
        <cfargument name="inpEndDate" type="string" required="true" default="" />

        <cfset var dataout = {} />
        <cfset dataout.FREE = 0/>
        <cfset dataout.INDV = 0/>
        <cfset dataout.PRO = 0/>
        <cfset dataout.SMB = 0/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />

        <cfset var getUserPlanReport = '' />

        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>

        <cftry>
            <!--- Count User Plan --->
            <cfquery datasource="#Session.DBSourceREAD#" name="getUserPlanReport">
                SELECT
                    pl.PlanName_vch AS NAME,
                    COUNT(*) AS TOTAL
                FROM
                    simpleobjects.useraccount ua
                INNER JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int = up.UserId_int
                INNER JOIN
                    simplebilling.plans pl
                ON
                    up.PlanId_int = pl.PlanId_int
                WHERE
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    up.UserPlanId_bi IN
                    (
                        SELECT
                            Max(UserPlanId_bi) AS UserPlanId_bi
                        FROM
                            simplebilling.userplans
                        WHERE
                        (
                            StartDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            OR
                            EndDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        )
                        GROUP BY
                            UserId_int
                    )
                GROUP BY
                    up.PlanId_int
            </cfquery>

            <cfloop query="getUserPlanReport">
                <cfswitch expression="#getUserPlanReport.NAME#">
                    <cfcase value="Free">
                        <cfset dataout.FREE = getUserPlanReport.TOTAL/>
                    </cfcase>
                    <cfcase value="Individual">
                        <cfset dataout.INDV = getUserPlanReport.TOTAL/>
                    </cfcase>
                    <cfcase value="Pro">
                        <cfset dataout.PRO = getUserPlanReport.TOTAL/>
                    </cfcase>
                    <cfcase value="SMB">
                        <cfset dataout.SMB = getUserPlanReport.TOTAL/>
                    </cfcase>
                    <cfdefaultcase>
                        <cfthrow type="Application" message="Can not get user plan report!" detail="Wrong plan type in DB"/>
                    </cfdefaultcase>
                </cfswitch>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetAllUserPlanReport" access="remote" output="false" hint="Get user plan report on entire system">
        <cfargument name="inpStartDate" type="string" required="true" default=""/>
        <cfargument name="inpEndDate" type="string" required="true" default=""/>
        <cfargument name="inpPlanName" type="string" required="true" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = '' />

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var rsCount = '' />
        <cfset var filterItem = '' />


        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cftry>
            <cfquery datasource="#Session.DBSourceREAD#" name="countReport">
                SELECT
                    COUNT(ua.UserId_int) AS TOTAL
                FROM
                    simpleobjects.useraccount ua
                INNER JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int = up.UserId_int
                INNER JOIN
                    simplebilling.plans pl
                ON
                    up.PlanId_int = pl.PlanId_int
                WHERE
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    up.UserPlanId_bi IN
                    (
                        SELECT
                            Max(UserPlanId_bi) AS UserPlanId_bi
                        FROM
                            simplebilling.userplans
                        WHERE
                        (
                            StartDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            OR
                            EndDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        )
                        GROUP BY
                            UserId_int
                    )
                AND
                    pl.PlanName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlanName#"/>
               <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = countReport.TOTAL/>
            <cfset dataout["iTotalRecords"] = countReport.TOTAL/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    ua.UserId_int AS UserId,
                    ua.EmailAddress_vch AS UserEmail,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    uo.OrganizationName_vch,
                    ua.Created_dt AS RegisteredDate,
                    up.StartDate_dt AS PlanStartDate,
                    up.EndDate_dt AS PlanEndDate,
                    up.Status_int AS PlanStatus,
                    pl.PlanName_vch AS PlanName,
                    ua.MFAContactString_vch AS ContactString
                FROM
                    simpleobjects.useraccount ua
                INNER JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int = up.UserId_int
                INNER JOIN
                    simplebilling.plans pl
                ON
                    up.PlanId_int = pl.PlanId_int
                LEFT OUTER JOIN
                    simpleobjects.organization as uo
                ON
                   ua.UserId_int = uo.UserId_int
                WHERE
                    DATE(ua.Created_dt) >= "#_UserStartDate#"
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    up.UserPlanId_bi IN
                    (
                        SELECT
                            Max(UserPlanId_bi) AS UserPlanId_bi
                        FROM
                            simplebilling.userplans
                        WHERE
                        (
                            StartDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            OR
                            EndDate_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        )
                        GROUP BY
                            UserId_int
                    )
                AND
                    pl.PlanName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlanName#"/>

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>

                ORDER BY
                    UserId DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        USERID = '#getReport.UserId#',
                        USEREMAIL =  '#getReport.UserEmail#',
                        USERNAME = "#getReport.FirstName_vch# #getReport.LastName_vch#",
                        ORGNAME = "#getReport.OrganizationName_vch#",
                        REGISTEREDDATE = '#DateFormat(getReport.RegisteredDate, "yyyy-mm-dd")#',
                        PLANSTARTDATE = "#DateFormat(getReport.PlanStartDate, "yyyy-mm-dd")#",
                        PLANENDDATE = "#DateFormat(getReport.PlanEndDate, "yyyy-mm-dd")#",
                        PLANSTATUS = "#getReport.PlanStatus#",
                        PLANNAME = "#getReport.PlanName#",
                        CONTACTSTRING = "#getReport.ContactString#"
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetTotalUniqueLogin" access="public" returntype="struct" output="false" hint="Return total users.">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getTotalUsers = ''/>
        <cfset var filterItem = '' />

        <cftry>
            <cfquery name="getTotalUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.LastLogIn_dt
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalUsers">
                    <cfset dataout.RESULT += getTotalUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetUniqueLoginReport" access="remote" output="false" hint="Count unique user account sign ins per day">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = ''/>
        <cfset var filterItem = '' />

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TOTALCOUNT = 0/>

        <cftry>

            <cfset countReport = GetTotalUniqueLogin(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
            <cfset dataout["iTotalRecords"] = countReport.RESULT/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    ua.UserId_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.Active_int,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    DATE(ua.LastLogIn_dt ) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#endDate#"/>
                AND
                    DATE(ua.LastLogIn_dt ) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#startDate#"/>

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        USERID = '#getReport.UserId_int#',
                        USEREMAIL =  '#getReport.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getReport.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getReport.FirstName_vch# #getReport.LastName_vch#",
                        FIRSTNAME = "#getReport.FirstName_vch#",
                        LASTNAME = "#getReport.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getReport.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getReport.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getReport.Active_int#",
                        CONTACTSTRING = "#getReport.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="GetTotalUserActive"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total users."
        >
        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getTotalUsers = ''/>
        <cfset var filterItem = '' />

        <cftry>
            <cfquery name="getTotalUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        AND
                            ua.LastLogIn_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalUsers">
                    <cfset dataout.RESULT += getTotalUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetUserActiveReport" access="remote" output="false" hint="Get a list of active user in given time range">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TOTALCOUNT = 0/>
        <cfset var filterItem = '' />

        <cftry>

            <cfset countReport = GetTotalUserActive(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
            <cfset dataout["iTotalRecords"] = countReport.RESULT/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    ua.UserId_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.Active_int,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.LastLogIn_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        USERID = '#getReport.UserId_int#',
                        USEREMAIL =  '#getReport.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getReport.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getReport.FirstName_vch# #getReport.LastName_vch#",
                        FIRSTNAME = "#getReport.FirstName_vch#",
                        LASTNAME = "#getReport.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getReport.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getReport.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getReport.Active_int#",
                        CONTACTSTRING = "#getReport.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="GetTotalUserInActive"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total users."
        >
        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getTotalUsers = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />

        <cftry>
            <cfquery name="getTotalUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.LastLogIn_dt NOT BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalUsers">
                    <cfset dataout.RESULT += getTotalUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetUserInActiveReport" access="remote" output="false" hint="Get a list of inactive user in given time range">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TOTALCOUNT = 0/>
        <cfset var filterItem = '' />

        <cftry>

            <cfset countReport = GetTotalUserInActive(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
            <cfset dataout["iTotalRecords"] = countReport.RESULT/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    ua.UserId_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.Active_int,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.LastLogIn_dt NOT BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        USERID = '#getReport.UserId_int#',
                        USEREMAIL =  '#getReport.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getReport.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getReport.FirstName_vch# #getReport.LastName_vch#",
                        FIRSTNAME = "#getReport.FirstName_vch#",
                        LASTNAME = "#getReport.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getReport.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getReport.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getReport.Active_int#",
                        CONTACTSTRING = "#getReport.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="GetTotalUserSignUpInactive"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total users."
        >
        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getTotalUsers = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />

        <cftry>
            <cfquery name="getTotalUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.LastLogIn_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    DATE(LastLogIn_dt) = DATE(Created_dt)
                AND
                    DATE(Created_dt) != DATE(NOW())
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalUsers">
                    <cfset dataout.RESULT += getTotalUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetUserSignUpInactive" access="remote" output="false" hint="Get Users Not Active Since Sign Up">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TOTALCOUNT = 0/>
        <cfset var filterItem = '' />

        <cftry>

            <cfset countReport = GetTotalUserSignUpInactive(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
            <cfset dataout["iTotalRecords"] = countReport.RESULT/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    ua.UserId_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.Active_int,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    ua.LastLogIn_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    DATE(LastLogIn_dt) = DATE(Created_dt)
                AND
                    DATE(Created_dt) != DATE(NOW())

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        USERID = '#getReport.UserId_int#',
                        USEREMAIL =  '#getReport.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getReport.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getReport.FirstName_vch# #getReport.LastName_vch#",
                        FIRSTNAME = "#getReport.FirstName_vch#",
                        LASTNAME = "#getReport.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getReport.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getReport.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getReport.Active_int#",
                        CONTACTSTRING = "#getReport.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="GetTotalKeywordSupportRequestReports"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total users."
        >
        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceREAD#" name="countReport">
                SELECT
                    COUNT(PKId_bi) AS TOTAL
                FROM
                    simplequeue.supportapp sp
                WHERE
                    Userid_int = 0
                AND
                    DATE(sp.Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#endDate#"/>
                AND
                    DATE(sp.Created_dt ) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#startDate#"/>
               <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif countReport.RECORDCOUNT GT 0>
                <cfloop query="countReport">
                    <cfset dataout.RESULT += countReport.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetKeywordSupportRequestReports" access="remote" output="false" hint="Get a list of all keyword Support request in system">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {} />
        <cfset var getReport = '' />
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TOTALCOUNT = 0/>
        <cfset var filterItem = '' />

        <cftry>

            <cfset countReport = GetTotalKeywordSupportRequestReports(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
            <cfset dataout["iTotalRecords"] = countReport.RESULT/>

            <cfquery datasource="#Session.DBSourceREAD#" name="getReport">
                SELECT
                    sp.PKId_bi AS RequestID,
                    sp.BatchId_bi AS BatchID,
                    sp.Status_int AS Status,
                    sp.Notified_int AS Notified,
                    sp.Created_dt AS Created,
                    sp.LastUpdated_dt AS Updated,
                    sp.ContactString_vch AS ContactString,
                    sp.CustomerMessage_vch AS CustomerMEssage,
                    sp.Note_vch AS Note
                FROM
                    simplequeue.supportapp sp
                WHERE
                    Userid_int = 0
                AND
                    DATE(sp.Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#endDate#"/>
                AND
                    DATE(sp.Created_dt ) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#startDate#"/>

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    RequestID DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getReport.RECORDCOUNT GT 0>
                <cfloop query="getReport">
                    <cfset var tempItem = {
                        REQUESTID = '#getReport.RequestID#',
                        BATCHID =  '#getReport.BatchID#',
                        STATUS = '#getReport.Status#',
                        NOTIFIED = "#getReport.Notified#",
                        CREATED = "#DateFormat(getReport.Created, "yyyy-mm-dd")# #TimeFormat(getReport.Created, "HH:mm:ss")#",
                        UPDATED = "#DateFormat(getReport.Updated, "yyyy-mm-dd")# #TimeFormat(getReport.Updated, "HH:mm:ss")#",
                        CONTACTSTRING = "#getReport.ContactString#",
                        CUSTOMERMESSAGE = "#getReport.CustomerMEssage#",
                        NOTE = "#getReport.Note#"
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="getTotalUsers"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total users."
        >

        <cfargument name="inpEndDate" type="string" required="true" default="" />

        <cfargument name="customFilter" default=""/>

        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getTotalUsers = ''/>
        <cfset var filterItem = '' />
        <cftry>
            <cfquery name="getTotalUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalUsers">
                    <cfset dataout.RESULT += getTotalUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction
        name="getUsers"
        access="remote"
        returntype="struct"
        output="false"
        hint="Return new users list."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var getNewUsers = ''/>
        <cfset var filterItem = '' />
        <cfset var countReport = '' />

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>

        <cftry>
            <cfset countReport = getTotalUsers(arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout['iTotalRecords'] = countReport.RESULT/>
            <cfset dataout['iTotalDisplayRecords'] = countReport.RESULT/>

            <cfquery name="getNewUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getNewUsers.RECORDCOUNT GT 0>
                <cfloop query="getNewUsers">
                    <cfset var tempItem = {
                        USERID = '#getNewUsers.UserId_int#',
                        USEREMAIL =  '#getNewUsers.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getNewUsers.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getNewUsers.FirstName_vch# #getNewUsers.LastName_vch#",
                        FIRSTNAME = "#getNewUsers.FirstName_vch#",
                        LASTNAME = "#getNewUsers.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getNewUsers.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getNewUsers.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getNewUsers.Active_int#",
                        CONTACTSTRING = "#getNewUsers.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction
        name="getTotalNewUsers"
        access="public"
        output="false"
        hint="Return total new users."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>

        <cfset var item = {}/>
        <cfset var getTotalNewUsers = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />

        <cftry>
            <cfquery name="getTotalNewUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    DATE(ua.Created_dt) >= "#_UserStartDate#"
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>

            <cfif getTotalNewUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalNewUsers">
                    <cfset dataout.RESULT += getTotalNewUsers.Total/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="getNewUsers"
        access="remote"
        returntype="struct"
        output="false"
        hint="Return new users list."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />
        <!--- <cfargument
            name="activated"
            type="string"
            required="false"
            default="0,1"
            hint=""
            /> --->

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var getNewUsers = ''/>
        <cfset var filterItem = '' />
        <cfset var countReport = '' />

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cftry>
            <cfset countReport = getTotalNewUsers(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

            <cfset dataout['iTotalRecords'] = countReport.RESULT/>
            <cfset dataout['iTotalDisplayRecords'] = countReport.RESULT/>

            <cfquery name="getNewUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    `simpleobjects`.`useraccount` ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                    DATE(ua.Created_dt) >= "#_UserStartDate#"

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfif getNewUsers.RECORDCOUNT GT 0>
                <cfloop query="getNewUsers">
                    <cfset var tempItem = {
                        USERID = '#getNewUsers.UserId_int#',
                        USEREMAIL =  '#getNewUsers.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getNewUsers.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getNewUsers.FirstName_vch# #getNewUsers.LastName_vch#",
                        FIRSTNAME = "#getNewUsers.FirstName_vch#",
                        LASTNAME = "#getNewUsers.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getNewUsers.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getNewUsers.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getNewUsers.Active_int#",
                        CONTACTSTRING = "#getNewUsers.MFAContactString_vch#",
                        SOURCE= MarketingSource_vch
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction
        name="getTotalSignInUsers"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total signin users."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var getTotalSignInUsers = "" />

        <cftry>
            <cfquery name="getTotalSignInUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    IF(LastLogIn_dt IS NULL, 0, (LastLogIn_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpStartDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEndDate#">, INTERVAL 86399 SECOND)
                        )) AS SignIn,
                    Active_int AS Activated,
                    COUNT(*) AS Total
                FROM
                    `simpleobjects`.`useraccount`
                GROUP BY
                    SignIn
                HAVING
                    Active_int = 1
            </cfquery>

            <cfif getTotalSignInUsers.RECORDCOUNT GT 0>
                <cfloop query="getTotalSignInUsers">
                    <cfset item.SignIn = getTotalSignInUsers.SignIn/>
                    <cfset item.Total = getTotalSignInUsers.Total/>
                    <cfset arrayAppend(dataout.RESULT, item)/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

    </cffunction>

    <cffunction
        name="getSignInUsers"
        access="public"
        returntype="struct"
        output="false"
        hint="Return signin users list."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="activated"
            type="string"
            required="false"
            default="1"
            hint=""
            />

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>

        <cfset var getSignInUsers = ''/>

        <cftry>
            <cfquery name="getSignInUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int AS UserId,
                    Active_int AS Activated,
                    EmailAddress_vch AS EmailAddress,
                    FirstName_vch AS FirstName,
                    LastName_vch AS LastName,
                    Created_dt AS SignUpDate,
                    LastLogIn_dt AS LastSignInDate
                FROM
                    `simpleobjects`.`useraccount`
                WHERE
                    Active_int = 1 AND
                    IF(LastLogIn_dt IS NULL, 0, (LastLogIn_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpStartDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEndDate#">, INTERVAL 86399 SECOND)
                        )) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.activated#">
            </cfquery>

            <cfif getSignInUsers.RECORDCOUNT GT 0>
                <cfloop query="getSignInUsers">
                    <cfset item.UserId = getSignInUsers.UserId/>
                    <cfset item.Activated = getSignInUsers.Activated/>
                    <cfset item.EmailAddress = getSignInUsers.EmailAddress/>
                    <cfset item.FirstName = getSignInUsers.FirstName/>
                    <cfset item.LastName = getSignInUsers.LastName/>
                    <cfset item.SignUpDate = getSignInUsers.SignUpDate/>
                    <cfset item.LastSignInDate = getSignInUsers.LastSignInDate/>
                    <cfset arrayAppend(dataout.RESULT, item)/>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

    </cffunction>

    <cffunction
        name="getTotalUserDowngrades"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total new user downgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = '' />
        <cfset var userIds = [] />
        <cfset var filterItem = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cftry>
            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int AS UserId
                FROM
                    `simplebilling`.`userplans`
                WHERE
                    DowngradeDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    AND  NumOfRecurringPlan_int = 0
                    AND  NumOfRecurringKeyword_int = 0
                OR

                    <!---
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                    UserDowngradeDate_dt BETWEEN

                    <cfif endDate EQ startDate AND endDate NEQ dateFormat(now(),'yyyy-mm-dd')>
                        DATE_SUB(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 1 MONTH)
                    AND
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    <cfelse>
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    </cfif>
                     --->
                    (
                        EndDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        AND  UserDowngradeDate_dt IS NOT NULL
                        AND  UserDowngradePlan_int = PlanId_int

                    )

                    -- AND
                    -- status_int = 0
                    AND
                    DowngradeDate_dt is not null
            </cfquery>

            <cfif getUserDowngrades.RECORDCOUNT GT 0>

                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
                </cfloop>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        COUNT(*) AS TOTAL
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                </cfquery>

                <cfset dataout.RESULT = getUsers.TOTAL/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="getUserDowngrades"
        access="remote"
        returntype="struct"
        output="false"
        hint="Return total new user downgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var userIds = []/>
        <cfset var userDowngrades = []/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />
        <cfset var userDowngrade = ''/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = ''/>
        <cfset var downgradeDate = ''/>

        <cftry>

            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId,
                    DowngradeDate,
                    UserDowngradeDate,

                    UserDowngradePLanName,
                    CurrentPlanName,
                    EndDate,

                    ApprovedBy
                FROM
                    (SELECT
                        up.UserId_int AS UserId,
                        up.DowngradeDate_dt AS DowngradeDate,
                        up.UserDowngradeDate_dt AS UserDowngradeDate,

                        p1.PlanName_vch AS UserDowngradePLanName,
                        p2.PlanName_vch AS CurrentPlanName,
                        up.EndDate_dt AS EndDate,
                        CONCAT(ua.FirstName_vch,' ',ua.LastName_vch) AS ApprovedBy

                    FROM
                        `simplebilling`.`userplans` up

                    LEFT JOIN
                        `simplebilling`.`plans` p1
                    ON  p1.PlanId_int = up.PreviousPlanId_int

                    LEFT JOIN
                        `simplebilling`.`plans` p2
                    ON  p2.PlanId_int = up.PlanId_int

                    LEFT JOIN
                        `simpleobjects`.`useraccount` ua
                    ON  up.UserIdApprovedDowngrade_int = ua.UserId_int

                    WHERE
                        up.DowngradeDate_dt BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        AND  NumOfRecurringPlan_int = 0
                        AND  NumOfRecurringKeyword_int = 0
                    OR
                     <!---    UserDowngradeDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                     --->
                      (
                     up.EndDate_dt BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                     AND  up.UserDowngradeDate_dt IS NOT NULL
                    )
                    -- AND
                    -- up.status_int = 0
                    AND
                    up.DowngradeDate_dt < up.EndDate_dt

                    AND
                    up.DowngradeDate_dt is not null

                    ORDER BY up.DowngradeDate_dt DESC
                    ) AS userplans
                GROUP BY
                    UserId
            </cfquery>

            <cfif getUserDowngrades.RECORDCOUNT GT 0>
                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
                    <cfif getUserDowngrades.DowngradeDate NEQ ''>
                        <cfset downgradeDate = getUserDowngrades.DowngradeDate/>
                    <cfelseif getUserDowngrades.UserDowngradeDate NEQ ''>
                         <cfset downgradeDate = getUserDowngrades.UserDowngradeDate/>
                    </cfif>
                    <cfset arrayAppend(userDowngrades, {UserId: getUserDowngrades.UserId, DowngradeDate: downgradeDate, UserDowngradePLanName: UserDowngradePLanName, CurrentPlanName: CurrentPlanName, EndDate: EndDate, ApprovedBy: ApprovedBy})/>
                </cfloop>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        ua.UserId_int AS UserId,
                        ua.Active_int AS Activated,
                        ua.EmailAddress_vch AS EmailAddress,
                        ua.FirstName_vch AS FirstName,
                        ua.LastName_vch AS LastName,
                        ua.Created_dt AS SignUpDate,
                        ua.LastLogIn_dt AS LastSignInDate,
                        ua.MFAContactString_vch AS ContactString
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>

                <cfif getUsers.RECORDCOUNT GT 0>
                    <cfset countReport = getTotalUserDowngrades(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

                    <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
                    <cfset dataout["iTotalRecords"] = countReport.RESULT/>

                    <cfloop query="getUsers">
                        <cfset var item = {} />
                        <cfset item.USERID = getUsers.UserId/>
                        <cfset item.STATUS = getUsers.Activated/>
                        <cfset item.USEREMAIL = getUsers.EmailAddress/>
                        <cfset item.CONTACTSTRING = getUsers.ContactString/>
                        <cfset item.FirstName = getUsers.FirstName/>
                        <cfset item.LastName = getUsers.LastName/>
                        <cfset item.USERNAME = "#getUsers.FirstName# #getUsers.LastName#"/>
                        <cfset item.REGISTEREDDATE = DateFormat(getUsers.SignUpDate, "yyyy-mm-dd")/>
                        <cfset item.LASTLOGIN = "#DateFormat(getUsers.LastSignInDate, "yyyy-mm-dd")# #TimeFormat(getUsers.LastSignInDate, "HH:mm:ss")#"/>
                        <cfloop array="#userDowngrades#" index="userDowngrade">
                            <cfif userDowngrade.UserId EQ getUsers.UserId>
                                <!--- <cfset item.DOWNGRADES = "#DateFormat(userDowngrade.DowngradeDate, "yyyy-mm-dd")# #TimeFormat(userDowngrade.DowngradeDate, "HH:mm:ss")#"/> --->
                                <cfset item.DOWNGRADES = "#DateFormat(userDowngrade.DowngradeDate, "yyyy-mm-dd")#"/>
                                <cfset item.FROMPLANNAME = "#userDowngrade.UserDowngradePLanName#"/>
                                <cfset item.TOPLANNAME = "#userDowngrade.CurrentPlanName#"/>

                                <cfset item.ENDDATE = "#DateFormat(userDowngrade.EndDate, "yyyy-mm-dd")#"/>
                                <cfif userDowngrade.ApprovedBy NEQ ''>
                                        <cfset item.APPROVEDBY = "#userDowngrade.ApprovedBy#"/>
                                <cfelse>
                                        <cfset item.APPROVEDBY = "System"/>
                                </cfif>


                            </cfif>
                        </cfloop>
                        <!--- <cfset arrayAppend(dataout.RESULT, item)/> --->
                        <cfset ArrayAppend(dataout["datalist"],item)>
                    </cfloop>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="getTotalUserUpgrades"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total new user upgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var userUpgradePlan = ''/>
        <cfset var getUserUpgrades = ''/>

        <cfset var userPlanIds = []>
        <cfset var userIds = []>
        <cfset var userUpgradePlans = []>
        <cfset var userUpgradeIds = []>
        <cfset var filterItem = ''/>
        <cfset var getUsers = ''/>

        <cftry>
            <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    id,
                    userId_int AS userId,
                    COUNT(DISTINCT planId) AS plans
                FROM
                    (SELECT
                        UserPlanId_bi AS id,
                        userId_int,
                        PlanId_int AS planId,
                        StartDate_dt AS created
                    FROM
                        `simplebilling`.`userplans`
                    WHERE
                        StartDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    AND  NumOfRecurringPlan_int = 0
                    AND  NumOfRecurringKeyword_int = 0
                    ORDER BY
                        StartDate_dt DESC
                    ) AS userplans

                GROUP BY
                    userId_int
                HAVING
                    plans > 1
            </cfquery>

            <cfif getUserUpgrades.RECORDCOUNT GT 0>
                <!--- <cfset dataout.RESULT += getUserUpgrades.RECORDCOUNT> --->
                <cfloop query="getUserUpgrades">
                    <cfset arrayAppend(userUpgradeIds,getUserUpgrades.userId)/>
                </cfloop>
            </cfif>

            <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    MAX(IF(
                        created BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        , id, 0)) AS id,
                    userId_int AS userId,
                    COUNT(DISTINCT planId) AS plans,
                    MAX(IF(
                        created BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        , planId, 0)) AS planId
                FROM
                    (SELECT
                        UserPlanId_bi AS id,
                        userId_int,
                        PlanId_int AS planId,
                        StartDate_dt AS created
                    FROM
                        `simplebilling`.`userplans`
                    WHERE
                        <cfif arrayLen(userUpgradeIds) GT 0>
                            userId_int NOT IN (#arrayToList(userUpgradeIds)#) AND
                        </cfif>
                        StartDate_dt < DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    ORDER BY
                        StartDate_dt DESC
                    ) AS userplans

                GROUP BY
                    userId_int
                HAVING
                    plans > 1 AND
                    id > 0
            </cfquery>


            <cfif getUserUpgrades.RECORDCOUNT GT 0>

                <cfloop query="getUserUpgrades">
                    <cfset arrayAppend(userPlanIds,getUserUpgrades.id)/>
                    <cfset arrayAppend(userIds,getUserUpgrades.userId)/>
                    <cfset arrayAppend(userUpgradePlans,{userId: getUserUpgrades.userId, planId: getUserUpgrades.planId})/>
                </cfloop>

                <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                    SELECT
                        id,
                        userId_int AS userId,
                        planId
                    FROM
                        (SELECT
                            UserPlanId_bi AS id,
                            userId_int,
                            PlanId_int AS planId,
                            StartDate_dt AS created
                        FROM
                            `simplebilling`.`userplans`
                        WHERE
                            UserPlanId_bi NOT IN (#arrayToList(userPlanIds)#) AND
                            userId_int IN (#arrayToList(userIds)#) AND
                            StartDate_dt < <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">
                        ORDER BY
                            StartDate_dt DESC
                        ) AS userplans

                    GROUP BY
                        userId_int
                </cfquery>

                <cfif getUserUpgrades.RECORDCOUNT GT 0>
                    <cfloop query="getUserUpgrades">
                        <cfloop array="#userUpgradePlans#" index="userUpgradePlan">
                            <cfif userUpgradePlan.userId EQ getUserUpgrades.userId AND userUpgradePlan.planId NEQ getUserUpgrades.planId>
                                <!--- <cfset dataout.RESULT += 1/> --->
                                <cfset arrayAppend(userUpgradeIds,getUserUpgrades.userId)/>
                            </cfif>
                        </cfloop>
                    </cfloop>
                </cfif>
            </cfif>

            <cfif arrayLen(userUpgradeIds) GT 0>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        COUNT(*) AS TOTAL
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        UserId_int IN (#ArrayToList(userUpgradeIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                </cfquery>

                <cfif getUsers.RECORDCOUNT GT 0>
                    <cfset dataout.RESULT += getUsers.TOTAL/>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>
    <!--- Total upgrades --->
    <cffunction  name="getTotalUserUpgradesNew" access="remote" returntype="struct"  output="false" hint="Return total new user upgrades.">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var userUpgradePlan = ''/>
        <cfset var getUserUpgrades = ''/>

        <cftry>
            <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    count(distinct(pw.userId_int)) as TOTAL
                FROM
                    simplebilling.payment_worldpay pw
                JOIN
                    simpleobjects.useraccount ua
                ON
                    pw.userId_int = ua.UserId_int
                WHERE
                    pw.created BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                  (pw.ModuleName="By Plan" OR pw.ModuleName="Buy Plan")
                AND ua.IsTestAccount_ti = 0
                AND ua.Active_int = 1
            </cfquery>

            <cfif getUserUpgrades.RECORDCOUNT GT 0>
                <cfset dataout.RESULT += getUserUpgrades.TOTAL/>
            </cfif>

            <cfset dataout.RXRESULTCODE = -1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <!--- detail upgrades --->
    <cffunction  name="getUserUpgrades" access="remote" returntype="struct" output="false" hint="Return total new user downgrades.">

        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint="" />

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var userIds = []/>
        <cfset var userDowngrades = []/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />
        <cfset var userDowngrade = ''/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = ''/>
        <cfset var downgradeDate = ''/>

        <cftry>
            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
               SELECT
                    UserId_int, Created
                FROM
                    simplebilling.payment_worldpay
                WHERE
                    created BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                AND
                  (ModuleName="By Plan" OR ModuleName="Buy Plan")
                Group By
                    UserId_int
            </cfquery>

            <cfif getUserDowngrades.RECORDCOUNT GT 0>
                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId_int)/>
                    <cfset arrayAppend(userDowngrades, {UserId: getUserDowngrades.UserId_int, UpgradeDate: Created})/>
                </cfloop>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        ua.UserId_int AS UserId,
                        ua.Active_int AS Activated,
                        ua.EmailAddress_vch AS EmailAddress,
                        ua.FirstName_vch AS FirstName,
                        ua.LastName_vch AS LastName,
                        ua.Created_dt AS SignUpDate,
                        ua.LastLogIn_dt AS LastSignInDate,
                        ua.MFAContactString_vch AS ContactString
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>

                <cfif getUsers.RECORDCOUNT GT 0>
                    <!---<cfset countReport = getTotalUserDowngrades(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>--->
                    <cfset countReport = getTotalUserUpgradesNew(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

                    <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
                    <cfset dataout["iTotalRecords"] = countReport.RESULT/>

                    <cfloop query="getUsers">
                        <cfset var item = {} />
                        <cfset item.USERID = getUsers.UserId/>
                        <cfset item.STATUS = getUsers.Activated/>
                        <cfset item.USEREMAIL = getUsers.EmailAddress/>
                        <cfset item.CONTACTSTRING = getUsers.ContactString/>
                        <cfset item.FirstName = getUsers.FirstName/>
                        <cfset item.LastName = getUsers.LastName/>
                        <cfset item.USERNAME = "#getUsers.FirstName# #getUsers.LastName#"/>
                        <cfset item.REGISTEREDDATE = DateFormat(getUsers.SignUpDate, "yyyy-mm-dd")/>
                        <cfset item.LASTLOGIN = "#DateFormat(getUsers.LastSignInDate, "yyyy-mm-dd")# #TimeFormat(getUsers.LastSignInDate, "HH:mm:ss")#"/>
                        <cfloop array="#userDowngrades#" index="userDowngrade">
                            <cfif userDowngrade.UserId EQ getUsers.UserId>
                                <!--- <cfset item.DOWNGRADES = "#DateFormat(userDowngrade.DowngradeDate, "yyyy-mm-dd")# #TimeFormat(userDowngrade.DowngradeDate, "HH:mm:ss")#"/> --->
                                <cfset item.UPGRADES = "#DateFormat(userDowngrade.UpgradeDate, "yyyy-mm-dd")#"/>

                                <cfset item.USERDOWNGRADEPLANNAME = "0"/>
                                <cfset item.CURRENTPLANNAME = "0"/>
                                <cfset item.ENDDATE = "0"/>
                                <cfset item.APPROVEDBY = "0"/>


                            </cfif>
                        </cfloop>
                        <!--- <cfset arrayAppend(dataout.RESULT, item)/> --->
                        <cfset ArrayAppend(dataout["datalist"],item)>
                    </cfloop>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction
        name="getUserUpgradesOld"
        access="remote"
        returntype="struct"
        output="false"
        hint="Return total new user upgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var item = {}/>
        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var userUpgradePlan = ''/>
        <cfset var filterItem = ''/>
        <cfset var userUpgrade = ''/>
        <cfset var getUserUpgrades = ''/>
        <cfset var getUsers = ''/>


        <cftry>
            <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    id,
                    userId_int AS userId,
                    COUNT(DISTINCT planId) AS plans,
                    Max(created) AS created
                FROM
                    (SELECT
                        UserPlanId_bi AS id,
                        userId_int,
                        PlanId_int AS planId,
                        StartDate_dt AS created
                    FROM
                        `simplebilling`.`userplans`
                    WHERE
                        StartDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    ORDER BY
                        StartDate_dt DESC
                    ) AS userplans

                GROUP BY
                    userId_int
                HAVING
                    plans > 1
            </cfquery>

            <cfset var userUpgradeIds = []>
            <cfset var userUpgrades = []>
            <cfif getUserUpgrades.RECORDCOUNT GT 0>
                <cfloop query="getUserUpgrades">
                    <cfset arrayAppend(userUpgradeIds,getUserUpgrades.userId)/>
                    <cfset arrayAppend(userUpgrades,{userId: getUserUpgrades.userId, UpgradeDate: getUserUpgrades.created})/>
                </cfloop>
            </cfif>

            <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    MAX(IF(
                        created BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        , id, 0)) AS id,
                    userId_int AS userId,
                    COUNT(DISTINCT planId) AS plans,
                    MAX(IF(
                        created BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        , planId, 0)) AS planId,
                        Max(created) as created
                FROM
                    (SELECT
                        UserPlanId_bi AS id,
                        userId_int,
                        PlanId_int AS planId,
                        StartDate_dt AS created
                    FROM
                        `simplebilling`.`userplans`
                    WHERE
                        <cfif arrayLen(userUpgradeIds) GT 0>
                            userId_int NOT IN (#arrayToList(userUpgradeIds)#) AND
                        </cfif>
                        StartDate_dt < DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    ORDER BY
                        StartDate_dt DESC
                    ) AS userplans

                GROUP BY
                    userId_int
                HAVING
                    plans > 1 AND
                    id > 0
            </cfquery>

            <cfif getUserUpgrades.RECORDCOUNT GT 0>
                <cfset var userPlanIds = []>
                <cfset var userIds = []>
                <cfset var userUpgradePlans = []>

                <cfloop query="getUserUpgrades">
                    <cfset arrayAppend(userPlanIds,getUserUpgrades.id)/>
                    <cfset arrayAppend(userIds,getUserUpgrades.userId)/>
                    <cfset arrayAppend(userUpgradePlans,{userId: getUserUpgrades.userId, planId: getUserUpgrades.planId, UpgradeDate: getUserUpgrades.created})/>
                </cfloop>

                <cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
                    SELECT
                        id,
                        userId_int AS userId,
                        planId
                    FROM
                        (SELECT
                            UserPlanId_bi AS id,
                            userId_int,
                            PlanId_int AS planId,
                            StartDate_dt AS created
                        FROM
                            `simplebilling`.`userplans`
                        WHERE
                            UserPlanId_bi NOT IN (#arrayToList(userPlanIds)#) AND
                            userId_int IN (#arrayToList(userIds)#) AND
                            StartDate_dt < <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">
                        ORDER BY
                            StartDate_dt DESC
                        ) AS userplans

                    GROUP BY
                        userId_int
                </cfquery>

                <cfif getUserUpgrades.RECORDCOUNT GT 0>
                    <cfset var userIds = []>
                    <cfloop query="getUserUpgrades">
                        <cfloop array="#userUpgradePlans#" index="userUpgradePlan">
                            <cfif userUpgradePlan.userId EQ getUserUpgrades.userId AND userUpgradePlan.planId NEQ getUserUpgrades.planId>
                                <cfset arrayAppend(userUpgradeIds,getUserUpgrades.userId)/>
                                <cfset arrayAppend(userUpgrades,{userId: getUserUpgrades.userId, UpgradeDate: userUpgradePlan.UpgradeDate})/>
                            </cfif>
                        </cfloop>
                    </cfloop>
                </cfif>
            </cfif>

            <cfif arrayLen(userUpgradeIds) GT 0>

                <cfset countReport = getTotalUserUpgrades(arguments.inpStartDate, arguments.inpEndDate, arguments.customFilter)/>

                <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
                <cfset dataout["iTotalRecords"] = countReport.RESULT/>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        ua.UserId_int AS UserId,
                        ua.Active_int AS Activated,
                        ua.EmailAddress_vch AS EmailAddress,
                        ua.FirstName_vch AS FirstName,
                        ua.LastName_vch AS LastName,
                        ua.Created_dt AS SignUpDate,
                        ua.LastLogIn_dt AS LastSignInDate,
                        ua.MFAContactString_vch AS ContactString
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        UserId_int IN (#ArrayToList(userUpgradeIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>

                <cfif getUsers.RECORDCOUNT GT 0>
                    <cfloop query="getUsers">
                        <cfset var item = {} />
                        <cfset item.USERID = getUsers.UserId/>
                        <cfset item.STATUS = getUsers.Activated/>
                        <cfset item.USEREMAIL = getUsers.EmailAddress/>
                        <cfset item.CONTACTSTRING = getUsers.ContactString/>
                        <cfset item.FirstName = getUsers.FirstName/>
                        <cfset item.LastName = getUsers.LastName/>
                        <cfset item.USERNAME = "#getUsers.FirstName# #getUsers.LastName#"/>
                        <cfset item.REGISTEREDDATE = DateFormat(getUsers.SignUpDate, "yyyy-mm-dd")/>
                        <cfset item.LASTLOGIN = "#DateFormat(getUsers.LastSignInDate, "yyyy-mm-dd")# #TimeFormat(getUsers.LastSignInDate, "HH:mm:ss")#"/>
                        <cfloop array="#userUpgrades#" index="userUpgrade">
                            <cfif userUpgrade.userId EQ getUsers.UserId>
                                <!--- <cfset item.UPGRADES = "#DateFormat(userUpgrade.UpgradeDate, "yyyy-mm-dd")# #TimeFormat(userUpgrade.UpgradeDate, "HH:mm:ss")#"/> --->
                                <cfset item.UPGRADES = "#DateFormat(userUpgrade.UpgradeDate, "yyyy-mm-dd")#"/>
                            </cfif>
                        </cfloop>
                        <!--- <cfset arrayAppend(dataout.RESULT, item)/> --->
                        <cfset ArrayAppend(dataout["datalist"],item)>
                    </cfloop>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>

    <cffunction name="GetMessageReport" access="remote" hint="Get total message sent-received, opt in-out of the system">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGESENT = 0 />
        <cfset dataout.MESSAGERECEIVED = 0 />
        <cfset dataout.TOTALOPTIN = 0 />
        <cfset dataout.TOTALOPTOUT = 0 />

        <cfset var RetVarConsoleSummaryTotalOptInCounts = '' />
        <cfset var RetVarConsoleSummaryTotalOptOutCounts = '' />
        <cfset var countSentMessage = '' />
        <cfset var countReceivedMessage = '' />
        <cftry>
            <cfinvoke method="ConsoleSummaryTotalOptInCounts" component="session.sire.models.cfc.reports.opt" returnvariable="RetVarConsoleSummaryTotalOptInCounts">
                <cfinvokeargument name="isByUser" value="0"/>
                <cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
                <cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
            </cfinvoke>

            <cfset dataout.TOTALOPTIN = RetVarConsoleSummaryTotalOptInCounts.OPTINTOTALCOUNT />

            <cfinvoke method="ConsoleSummaryTotalOptOutCounts" component="session.sire.models.cfc.reports.opt" returnvariable="RetVarConsoleSummaryTotalOptOutCounts">
                <cfinvokeargument name="isByUser" value="0"/>
                <cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
                <cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
            </cfinvoke>

            <cfset dataout.TOTALOPTOUT = RetVarConsoleSummaryTotalOptOutCounts.OPTOUTCOUNT />

            <cfquery name="countSentMessage" datasource="#Session.DBSourceREAD#">
                SELECT
                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                FROM
                    simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = ire.UserId_int
                WHERE
                    ire.IREType_int IN (1,2)
                AND
                    LENGTH(ire.ContactString_vch) < 14
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ire.Created_dt
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>

            <cfset dataout.MESSAGESENT = (countSentMessage.Sent EQ '') ? 0 : countSentMessage.Sent/>
            <cfset dataout.MESSAGERECEIVED =  (countSentMessage.Received EQ '') ? 0 : countSentMessage.Received/>
            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetTotalSentReceivedMessageList" access="remote" hint="Get total sent-received message list">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <!---
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        --->
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserIdInIreResults = '' />
        <cfset var getUserInfo = '' />
        <cfset var getTotalSentReceivedMessageAllUser = '' />
        <cfset var getSentReceivedMessageByUser = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />
        <cfset var totalSent = 0/>
        <cfset var totalReceived = 0/>
        <cfset var MinSummaryDate=''/>
        <cfset var OneDayAfterMaxSummaryDate=''/>
        <cfset var QsCountTotalRows=''/>

        <cfset var CheckSummaryDataByJob = ''/>
        <cfset var MaxSummaryDate = ''/>


        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>
            <cfquery name="CheckSummaryDataByJob" datasource="#Session.DBSourceREAD#">
                SELECT  MAX(SummaryDate_dt) AS MaxSummaryDate , MIN(SummaryDate_dt) as MinSummaryDate
                FROM    simplexresults.summary_total_send_received_by_date
            </cfquery>
            <cfset MaxSummaryDate=CheckSummaryDataByJob.MaxSummaryDate>
            <cfset MinSummaryDate=CheckSummaryDataByJob.MinSummaryDate>
            <cfif MaxSummaryDate EQ "" OR DateDiff( "d", MaxSummaryDate, startDate ) GT 0 OR DateDiff( "d", startDate, MinSummaryDate ) GT 0 >
                <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        ire.UserId_int,
                        ua.EmailAddress_vch,
                        ua.MFAContactString_vch,
                        ua.FirstName_vch,
                        ua.LastName_vch,
                        SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                        SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                    FROM
                        simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                    LEFT JOIN
                        simpleobjects.useraccount as ua
                    ON
                        ire.UserId_int = ua.UserId_int
                    WHERE
                        LENGTH(ire.ContactString_vch) < 14
                    AND
                        ua.IsTestAccount_ti = 0
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.NAME EQ 'ire.UserId_int'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                            </cfif>
                        </cfloop>
                    </cfif>
                    AND
                        ire.Created_dt
                    BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    GROUP BY
                        ire.UserId_int
                    HAVING
                        (
                            Sent > 0
                            OR
                            Received > 0
                        )
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>
                 <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>

                <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
                <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
            <cfelse>
                <cfif DateDiff( "d", MaxSummaryDate, endDate ) GT 0>
			        <cfset OneDayAfterMaxSummaryDate= DateAdd("d",1,MaxSummaryDate)>	 <!--- 1 days after max day--->
                     <cfquery name="QsCountTotalRows" datasource="#Session.DBSourceREAD#">
                        SELECT
                            UserId_int,
                            SUM(Sent) AS Sent,
                            SUM(Received) as Received
                        FROM
                        (
                            SELECT

                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(ire.TotalSent_int) AS Sent,
                                SUM(ire.TotalReceived_int) AS Received
                            FROM
                                simplexresults.summary_total_send_received_by_date as ire
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                ua.IsTestAccount_ti = 0
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.SummaryDate_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#MaxSummaryDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                            UNION ALL
                            SELECT
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                                SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                            FROM
                                simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                LENGTH(ire.ContactString_vch) < 14
                            AND
                                ua.IsTestAccount_ti = 0
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.Created_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#OneDayAfterMaxSummaryDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                        ) as alldata
                        GROUP BY
                                UserId_int
                     </cfquery>

                    <cfset dataout["iTotalDisplayRecords"] = QsCountTotalRows.RECORDCOUNT/>
                    <cfset dataout["iTotalRecords"] = QsCountTotalRows.RECORDCOUNT/>
                    <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                        SELECT
                            UserId_int,
                            EmailAddress_vch,
                            MFAContactString_vch,
                            FirstName_vch,
                            LastName_vch,
                            SUM(Sent) AS Sent,
                            SUM(Received) as Received
                        FROM
                        (
                            SELECT

                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(ire.TotalSent_int) AS Sent,
                                SUM(ire.TotalReceived_int) AS Received
                            FROM
                                simplexresults.summary_total_send_received_by_date as ire
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                ua.IsTestAccount_ti = 0
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.SummaryDate_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#MaxSummaryDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                            UNION ALL
                            SELECT
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                                SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                            FROM
                                simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                LENGTH(ire.ContactString_vch) < 14
                            AND
                                ua.IsTestAccount_ti = 0
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
                                    AND
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.Created_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#OneDayAfterMaxSummaryDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                        ) as alldata
                        GROUP BY
                                UserId_int
                        <cfif arguments.inpSkipPaging EQ 0>
                            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                        </cfif>
                    </cfquery>

                <cfelse>
                    <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                        SELECT
                            SQL_CALC_FOUND_ROWS
                            ire.UserId_int,
                            ua.EmailAddress_vch,
                            ua.MFAContactString_vch,
                            ua.FirstName_vch,
                            ua.LastName_vch,
                            SUM(ire.TotalSent_int) AS Sent,
                            SUM(ire.TotalReceived_int) AS Received
                        FROM
                            simplexresults.summary_total_send_received_by_date as ire
                        LEFT JOIN
                            simpleobjects.useraccount as ua
                        ON
                            ire.UserId_int = ua.UserId_int
                        WHERE
                            ua.IsTestAccount_ti = 0
                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                                </cfif>
                            </cfloop>
                        </cfif>
                        AND
                            ire.SummaryDate_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        GROUP BY
                            ire.UserId_int
                        HAVING
                            (
                                Sent > 0
                                OR
                                Received > 0
                            )
                        <cfif arguments.inpSkipPaging EQ 0>
                            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                        </cfif>
                    </cfquery>
                     <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                        SELECT FOUND_ROWS() AS iTotalRecords
                    </cfquery>

                    <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
                    <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
                </cfif>
            </cfif>

            <cfloop query="getAllUserIdInIreResults">
                <cfset temp = {
                    USERID = '#getAllUserIdInIreResults.UserId_int#',
                    PHONE = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.MFAContactString_vch#',
                    NAME = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.FirstName_vch#' & ' #getAllUserIdInIreResults.LastName_vch#',
                    EMAIL = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.EmailAddress_vch#',
                    SENT = '#Sent#',
                    RECEIVED = '#Received#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>


            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetSummarySentReceivedMessage" access="remote" hint="Get campaign and total sent-received messages">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="numeric" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var temp = {} />
        <cfset var getAllBatch = '' />
        <cfset var getSentMessageByBatch = '' />
        <cfset var getReceivedMessageByBatch = '' />
        <cfset var rowName = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var getSMSChatKeyword = '' />
        <cfset var totalSent = 0/>
        <cfset var totalReceived = 0/>
        <cfset var ireList = []/>

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllBatch" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ire.BatchId_bi as BatchId,
                    ba.Desc_vch as Desc_vch,
                    ire.IREResultsId_bi,
                    ire.UserId_int as UserId,
                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                FROM
                    simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                LEFT JOIN
                    simpleobjects.batch as ba
                ON
                    ba.BatchId_bi = ire.BatchId_bi
                WHERE
                    ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                    LENGTH(ire.ContactString_vch) < 14
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'ire.BatchId_bi'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                AND
                    ire.Created_dt
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                GROUP BY
                    ire.BatchId_bi
                HAVING
                    (
                        Sent > 0
                        OR
                        Received > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllBatch">

                <cfset rowName = '#getAllBatch.Desc_vch#'/>

                <cfif getAllBatch.Desc_vch EQ '' AND getAllBatch.BatchId NEQ 0>
                <!--- SMS chat campaign --->
                    <cfquery name="getSMSChatKeyword" datasource="#Session.DBSourceREAD#">
                        SELECT
                            Keyword_vch
                        FROM
                            sms.keyword
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#getAllBatch.BatchId#'>
                        LIMIT
                            1
                    </cfquery>

                    <cfif getSMSChatKeyword.RECORDCOUNT GT 0 AND getSMSChatKeyword.Keyword_vch NEQ ''>
                        <cfset rowName = 'SMS Chat - Keyword: #getSMSChatKeyword.Keyword_vch#'/>
                    <cfelse>
                        <cfset rowName = 'SMS Chat - ID: #getAllBatch.BatchId#'/>
                    </cfif>
                <cfelseif getAllBatch.Desc_vch EQ '' AND getAllBatch.BatchId EQ 0>
                <!--- System message campaign --->
                    <cfset rowName = 'System message(s)'/>
                </cfif>

                <cfset temp = {
                    NAME = '#rowName#',
                    SENT = '#getAllBatch.Sent#',
                    RECEIVED = '#getAllBatch.Received#',
                    USERID = '#getAllBatch.UserId#',
                    BATCHID = '#getAllBatch.BatchId#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetOptInOutSummaryList" access="remote" hint="Get opt in-out list for admin">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {}/>

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    b.UserId_int,
                    b.BatchId_bi,
                    b.Desc_vch,
                    SUM(IF(oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL, 1, 0)) AS OptIn,
                    SUM(IF(oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL, 1, 0)) AS OptOut
                FROM
                    simplelists.optinout AS oi
                INNER JOIN
                    simpleobjects.batch as b ON
                    (b.BatchId_bi = oi.BatchId_bi)
                WHERE
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpUserId#'>
                    AND
                     LENGTH(oi.ContactString_vch) < 14
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'b.BatchId_bi'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
                    AND
                        CASE WHEN
                            (oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL)
                        THEN
                            oi.OptIn_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                    AND
                        CASE WHEN
                            (oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL)
                        THEN
                            oi.OptOut_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                </cfif>
                GROUP BY
                    b.BatchId_bi
                HAVING
                    (
                        OptIn > 0
                        OR
                        OptOut > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getList">

                <cfset temp = {
                    NAME = '#getList.Desc_vch#',
                    OPTIN = '#getList.OptIn#',
                    OPTOUT = '#getList.OptOut#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetOptInOutList" access="remote" hint="Get opt in-out list for admin">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>

        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {}/>

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    b.UserId_int,
                    ua.EmailAddress_vch,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    SUM(IF(oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL, 1, 0)) AS OptIn,
                    SUM(IF(oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL, 1, 0)) AS OptOut
                FROM
                    simplelists.optinout AS oi
                INNER JOIN
                    simpleobjects.batch as b ON
                    (b.BatchId_bi = oi.BatchId_bi)
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = b.UserId_int
                WHERE
                    ua.UserId_int > 0
                AND
                    LENGTH(oi.ContactString_vch) < 14
                AND
                    ua.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'b.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
                    AND
                        CASE WHEN
                            (oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL)
                        THEN
                            oi.OptIn_dt
                            BETWEEN
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                            AND
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                    AND
                        CASE WHEN
                            (oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL)
                        THEN
                            oi.OptOut_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                </cfif>
                GROUP BY
                    b.UserId_int
                HAVING
                    (
                        OptIn > 0
                        OR
                        OptOut > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>

                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getList">

                <cfset temp = {
                    USERID = '#getList.UserId_int#',
                    PHONE = '#getList.MFAContactString_vch#',
                    NAME = '#getList.FirstName_vch#' & ' #getList.LastName_vch#',
                    EMAIL = '#getList.EmailAddress_vch#',
                    OPTIN = '#getList.OptIn#',
                    OPTOUT = '#getList.OptOut#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetShortCodeList" access="remote" output="false" hint="Get system shortcode list">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

        <cfargument name="iSortCol_0" default="-1"/>
        <cfargument name="sSortDir_0" default=""/>
        <cfargument name="customFilter" default="">

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cfset dataout['DATALIST'] = []/>
        <cfset dataout["iTotalRecords"] = 0/>
        <cfset dataout["iTotalDisplayRecords"] = 0/>

        <cfset var ShortCodeList = '' />
        <cfset var filterItem = '' />
        <cfset var item = '' />
        <cfset var rsCount = '' />

        <cfset var orderField = '' />
        <cfswitch expression="#arguments.iSortCol_0#">
            <cfcase value="0">
                <cfset orderField = 'ShortCodeId_int' />
            </cfcase>
            <cfcase value="1">
                <cfset orderField = 'ShortCode_vch' />
            </cfcase>
        </cfswitch>

        <cftry>
            <cfquery datasource="#session.DBSourceREAD#" name="ShortCodeList">
                SELECT SQL_CALC_FOUND_ROWS
                    ShortCodeId_int,
                    ShortCode_vch,
                    IsDefault_ti
                FROM
                    sms.shortcode
                WHERE
                    1
               <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='cf_sql_varchar' value='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif orderField NEQ "">
                    ORDER BY
                        #orderField# #arguments.sSortDir_0#,
                        ShortCodeId_int DESC
                <cfelse>
                    ORDER BY
                        ShortCodeId_int DESC
                </cfif>
                LIMIT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#"> OFFSET <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>

            <cfif ShortCodeList.RecordCount GT 0>
                <cfloop query="ShortCodeList">
                    <cfset item = {
                        "ShortCodeId": ShortCodeList.ShortCodeId_int,
                        "ShortCode": ShortCodeList.ShortCode_vch,
                        "IsDefault": ShortCodeList.IsDefault_ti
                    } />
                    <cfset arrayAppend(dataout["DATALIST"],item) />
                </cfloop>

                <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>

                <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
                <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="SetDefaultShortCode" access="remote" output="false" hint="Set a shortcode to default">
        <cfargument name="inpShortCodeId" type="numeric" required="true" hint="Shortcode id"/>

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cfset var resetDefault = '' />
        <cfset var setDefaultShortCode = ''/>

        <cftry>
            <cfquery datasource="#session.DBSourceEBM#" result="resetDefault">
                UPDATE
                    sms.shortcode
                SET
                    IsDefault_ti = 0
                AND
                    IsDefault_ti = 1
            </cfquery>

            <cfquery datasource="#session.DBSourceEBM#" result="setDefaultShortCode">
                UPDATE
                    sms.shortcode
                SET
                    IsDefault_ti = 1
                WHERE
                    ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
            </cfquery>

            <cfif setDefaultShortCode.RECORDCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Set default short code successfully!"/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="SystemUsageReportTodayWithShortcode" access="remote" output="false" hint="Get today system usage report with short code">
        <cfargument name="inpShortCodeId" required="true" hint="Shortcode ID"/>

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />
        <cfset var GetTotalBatchesLast1 = '' />
        <cfset var GetTotalMOsLast1 = '' />
        <cfset var GetTotalMTsLast1 = '' />
        <cfset var GetTotalSessionsLast1 = '' />
        <cfset var GetTotalActiveSessionsLast1 = '' />
        <cfset var GetTotalUsageLast1 = '' />
        <cfset var GetContactQueueStatus1 = '' />
        <cfset var GetTotalUsersLast1 = '' />

        <cfset dataout.TOTALUSERLAST1 = 0/>
        <cfset dataout.TOTALBATCHESLAST1 = 0 />
        <cfset dataout.TOTALMOSLAST1 = 0 />
        <cfset dataout.TOTALMTSLAST1 = 0 />
        <cfset dataout.TOTALSESSIONSLAST1 = 0 />
        <cfset dataout.TOTALACTIVESESSIONLAST1 = 0 />
        <cfset dataout.TOTALUSAGELAST1 = [] />
        <cfset dataout.CONTACTQUEUESTATUS1 = [] />

        <cfset var shortcode = {}/>

        <cftry>
            <cfif arguments.inpShortCodeId GT 0>
                <cfset shortcode = GetShortCodeById(arguments.inpShortCodeId)/>
                <cfif shortcode.RXRESULTCODE LT 1>
                    <cfthrow message="Can not get short code information"/>
                </cfif>
            </cfif>

            <cfquery name="GetTotalUsersLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(DISTINCT uc.UserId_int) AS UserCount
                FROM
                    simpleobjects.useraccount uc
                INNER JOIN
                    sms.shortcoderequest scr
                ON
                    uc.UserId_int = scr.RequesterId_int
                WHERE
                    uc.Created_dt > CURDATE()
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        scr.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALUSERLAST1 = GetTotalUsersLast1.UserCount/>

            <cfquery name="GetTotalBatchesLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(BatchId_bi) AS BatchCount
                FROM
                    simpleobjects.batch
                WHERE
                    Created_dt > CURDATE()
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALBATCHESLAST1 = GetTotalBatchesLast1.BatchCount/>

            <cfquery name="GetTotalMOsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(moInboundQueueId_bi) AS MOCount
                FROM
                    simplequeue.moinboundqueue
                WHERE
                    Created_dt > CURDATE()
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMOSLAST1 = GetTotalMOsLast1.MOCount/>

            <cfquery name="GetTotalMTsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(IREResultsId_bi) AS MTCount
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > CURDATE()
                AND
                    IREType_int = 1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMTSLAST1 = GetTotalMTsLast1.MTCount/>

            <cfquery name="GetTotalSessionsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > CURDATE()
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALSESSIONSLAST1 = GetTotalSessionsLast1.SessionCount/>

            <cfquery name="GetTotalActiveSessionsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > CURDATE()
                AND
                    SessionState_int < 4
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALACTIVESESSIONLAST1 = GetTotalActiveSessionsLast1.SessionCount/>

            <cfquery name="GetTotalUsageLast1" datasource="#DBSourceEBM#">
                SELECT
                    UserId_int,
                    COUNT(*) AS TotalCount,
                    IreType_int
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Dateformat(NOW(),'YYYY-MM-DD')#">
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
                GROUP BY
                    UserId_int,IreType_int
                ORDER BY
                    TotalCount DESC
                LIMIT 100
            </cfquery>

            <cfloop query="GetTotalUsageLast1">
                <cfset var item = {}/>
                <cfset item.USERID = GetTotalUsageLast1.UserId_int/>
                <cfset item.TOTALCOUNT = GetTotalUsageLast1.TotalCount/>
                <cfset item.IRETYPE = GetTotalUsageLast1.IreType_int/>
                <cfset arrayAppend(dataout.TOTALUSAGELAST1, item)/>
            </cfloop>

            <cfquery name="GetContactQueueStatus1" datasource="#DBSourceEBM#">
                SELECT
                    COUNT(*) AS TOTALCOUNT,
                    UserId_int,
                    DTSStatusType_ti
                FROM
                    simplequeue.contactqueue
                WHERE
                    Scheduled_dt > <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Dateformat(NOW(),'YYYY-MM-DD')#">
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
                GROUP BY
                    UserId_int, DTSStatusType_ti
                ORDER BY
                    UserId_int, DTSStatusType_ti ASC
                LIMIT 100
            </cfquery>

            <cfloop query="GetContactQueueStatus1">
                <cfset var item = {}/>
                <cfset item.USERID = GetContactQueueStatus1.UserId_int/>
                <cfset item.TOTALCOUNT = GetContactQueueStatus1.TOTALCOUNT/>
                <cfset item.STATUS = GetContactQueueStatus1.DTSStatusType_ti/>
                <cfset arrayAppend(dataout.CONTACTQUEUESTATUS1, item)/>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="SystemUsageReportWithShortcode" access="remote" output="false" hint="Get system usage report with short code">
        <cfargument name="inpShortCodeId" required="true" hint="Shortcode ID"/>

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />
        <cfset var GetTotalBatchesLast1 = '' />
        <cfset var GetTotalMOsLast1 = '' />
        <cfset var GetTotalMTsLast1 = '' />
        <cfset var GetTotalSessionsLast1 = '' />
        <cfset var GetTotalActiveSessionsLast1 = '' />
        <cfset var GetTotalUsageLast1 = '' />
        <cfset var GetContactQueueStatus1 = '' />
        <cfset var GetTotalUsersLast30 = '' />
        <cfset var GetTotalUsers = '' />
        <cfset var GetTotalUsersLast7 = '' />
        <cfset var GetTotalUsersLast1 = '' />
        <cfset var GetTotalBatches = '' />
        <cfset var GetTotalBatchesLast30 = '' />
        <cfset var GetTotalBatchesLast7 = '' />
        <cfset var GetTotalMOs = '' />
        <cfset var GetTotalMOsLast30 = '' />
        <cfset var GetTotalMOsLast7 = '' />
        <cfset var GetTotalMTs = '' />
        <cfset var GetTotalMTsLast30 = '' />
        <cfset var GetTotalMTsLast7 = '' />
        <cfset var GetTotalSessions = '' />
        <cfset var GetTotalSessionsLast30 = '' />
        <cfset var GetTotalSessionsLast7 = '' />
        <cfset var GetTotalActiveSessions = '' />
        <cfset var GetTotalActiveSessionsLast30 = '' />
        <cfset var GetTotalActiveSessionsLast7 = '' />

        <cfset dataout.TOTALUSERS = 0/>
        <cfset dataout.TOTALUSERSLAST30 = 0/>
        <cfset dataout.TOTALUSERSLAST7 = 0/>
        <cfset dataout.TOTALUSERSLAST1 = 0/>

        <cfset dataout.TOTALBATCHES = 0 />
        <cfset dataout.TOTALBATCHESLAST30 = 0 />
        <cfset dataout.TOTALBATCHESLAST7 = 0 />
        <cfset dataout.TOTALBATCHESLAST1 = 0 />

        <cfset dataout.TOTALMOS = 0 />
        <cfset dataout.TOTALMOSLAST30 = 0 />
        <cfset dataout.TOTALMOSLAST7 = 0 />
        <cfset dataout.TOTALMOSLAST1 = 0 />

        <cfset dataout.TOTALMTS = 0 />
        <cfset dataout.TOTALMTSLAST30 = 0 />
        <cfset dataout.TOTALMTSLAST7 = 0 />
        <cfset dataout.TOTALMTSLAST1 = 0 />

        <cfset dataout.TOTALSESSIONS = 0 />
        <cfset dataout.TOTALSESSIONSLAST30 = 0 />
        <cfset dataout.TOTALSESSIONSLAST7 = 0 />
        <cfset dataout.TOTALSESSIONSLAST1 = 0 />

        <cfset dataout.TOTALACTIVESESSIONS = 0 />
        <cfset dataout.TOTALACTIVESESSIONSLAST30 = 0 />
        <cfset dataout.TOTALACTIVESESSIONSLAST7 = 0 />
        <cfset dataout.TOTALACTIVESESSIONSLAST1 = 0 />

        <cfset dataout.TOTALUSAGE = [] />

        <cfset var shortcode = {}/>

        <cftry>
            <cfif arguments.inpShortCodeId GT 0>
                <cfset shortcode = GetShortCodeById(arguments.inpShortCodeId)/>
                <cfif shortcode.RXRESULTCODE LT 1>
                    <cfthrow message="Can not get short code information"/>
                </cfif>
            </cfif>

            <!--- USER QUERY  --->
            <!--- Get Total User Counts--->
            <cfquery name="GetTotalUsers" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(DISTINCT UserId_int) AS UserCount
                FROM
                    simpleobjects.useraccount uc
                INNER JOIN
                    sms.shortcoderequest scr
                ON
                    uc.UserId_int = scr.RequesterId_int
                WHERE
                    1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        scr.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALUSERS = GetTotalUsers.UserCount/>

            <!--- Get New Users Last 30 days --->
            <cfquery name="GetTotalUsersLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(DISTINCT uc.UserId_int) AS UserCount
                FROM
                    simpleobjects.useraccount uc
                INNER JOIN
                    sms.shortcoderequest scr
                ON
                    uc.UserId_int = scr.RequesterId_int
                WHERE
                    uc.Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        scr.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALUSERSLAST30 = GetTotalUsersLast30.UserCount/>

            <!--- Get New Users Last 7 days --->
            <cfquery name="GetTotalUsersLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(DISTINCT UserId_int) AS UserCount
                FROM
                    simpleobjects.useraccount uc
                INNER JOIN
                    sms.shortcoderequest scr
                ON
                    uc.UserId_int = scr.RequesterId_int
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        scr.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALUSERSLAST7 = GetTotalUsersLast7.UserCount/>

            <!--- Get New Users Last 1 days --->
            <cfquery name="GetTotalUsersLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(DISTINCT UserId_int) AS UserCount
                FROM
                    simpleobjects.useraccount uc
                INNER JOIN
                    sms.shortcoderequest scr
                ON
                    uc.UserId_int = scr.RequesterId_int
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        scr.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALUSERSLAST1 = GetTotalUsersLast1.UserCount/>
            <!--- END USER QUERY  --->

            <!--- BATCH QUERY  --->
            <!--- Get Total Batch Counts--->
            <cfquery name="GetTotalBatches" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(BatchId_bi) AS BatchCount
                FROM
                    simpleobjects.batch
                WHERE
                    1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALBATCHES = GetTotalBatches.BatchCount />

            <!--- Get New Batches Last 30 days --->
            <cfquery name="GetTotalBatchesLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(BatchId_bi) AS BatchCount
                FROM
                    simpleobjects.batch
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALBATCHESLAST30 = GetTotalBatchesLast30.BatchCount />

            <!--- Get New Batches Last 7 days --->
            <cfquery name="GetTotalBatchesLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(BatchId_bi) AS BatchCount
                FROM
                    simpleobjects.batch
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALBATCHESLAST7 = GetTotalBatchesLast7.BatchCount />

            <!--- Get New Batches Last 1 days --->
            <cfquery name="GetTotalBatchesLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(BatchId_bi) AS BatchCount
                FROM
                    simpleobjects.batch
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALBATCHESLAST1 = GetTotalBatchesLast1.BatchCount/>
            <!--- END BATCH QUERY  --->

            <!--- MO QUERY --->
            <!--- Get Total MOs Counts--->
            <cfquery name="GetTotalMOs" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(moInboundQueueId_bi) AS MOCount
                FROM
                    simplequeue.moinboundqueue
                WHERE
                    1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMOS = GetTotalMOs.MOCount />

            <!--- Get New MOs Last 30 days --->
            <cfquery name="GetTotalMOsLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(moInboundQueueId_bi) AS MOCount
                FROM
                    simplequeue.moinboundqueue
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMOSLAST30 = GetTotalMOsLast30.MOCount />

            <!--- Get New MOs Last 7 days --->
            <cfquery name="GetTotalMOsLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(moInboundQueueId_bi) AS MOCount
                FROM
                    simplequeue.moinboundqueue
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMOSLAST7 = GetTotalMOsLast7.MOCount />

            <!--- Get New MOs Last 1 days --->
            <cfquery name="GetTotalMOsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(moInboundQueueId_bi) AS MOCount
                FROM
                    simplequeue.moinboundqueue
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMOSLAST1 = GetTotalMOsLast1.MOCount/>
            <!--- END MO QUERY  --->

            <!--- MT QUERY  --->
            <!--- Get Total MTs Counts--->
            <cfquery name="GetTotalMTs" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(IREResultsId_bi) AS MTCount
                FROM
                    simplexresults.ireresults
                WHERE
                    IREType_int = 1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMTS = GetTotalMTs.MTCount/>

            <!--- Get New MTs Last 30 days --->
            <cfquery name="GetTotalMTsLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(IREResultsId_bi) AS MTCount
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                AND
                    IREType_int = 1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMTSLAST30 = GetTotalMTsLast30.MTCount/>

            <!--- Get New MTs Last 7 days --->
            <cfquery name="GetTotalMTsLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(IREResultsId_bi) AS MTCount
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                AND
                    IREType_int = 1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMTSLAST7 = GetTotalMTsLast7.MTCount/>

            <!--- Get New MTs Last 1 days --->
            <cfquery name="GetTotalMTsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(IREResultsId_bi) AS MTCount
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                AND
                    IREType_int = 1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALMTSLAST1 = GetTotalMTsLast1.MTCount/>
            <!--- END MT QUERY  --->

            <!--- SESSION QUERY  --->
            <!--- Get Total Sessions Counts--->
            <cfquery name="GetTotalSessions" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    1
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALSESSIONS = GetTotalSessions.SessionCount/>

            <!--- Get New Sessions Last 30 days --->
            <cfquery name="GetTotalSessionsLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALSESSIONSLAST30 = GetTotalSessionsLast30.SessionCount/>

            <!--- Get New Sessions Last 7 days --->
            <cfquery name="GetTotalSessionsLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
             </cfquery>

             <cfset dataout.TOTALSESSIONSLAST7 = GetTotalSessionsLast7.SessionCount/>

            <!--- Get New Sessions Last 1 days --->
            <cfquery name="GetTotalSessionsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALSESSIONSLAST1 = GetTotalSessionsLast1.SessionCount/>
            <!--- END SESSION QUERY  --->

            <!--- ACTIVE QUERY  --->
            <!--- Get Total Sessions Counts--->
            <cfquery name="GetTotalActiveSessions" datasource="#session.DBSourceREAD#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    SessionState_int < 4
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALACTIVESESSIONS = GetTotalActiveSessions.SessionCount/>

            <!--- Get New Sessions Last 30 days --->
            <cfquery name="GetTotalActiveSessionsLast30" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'
                AND
                    SessionState_int < 4
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALACTIVESESSIONSLAST30 = GetTotalActiveSessionsLast30.SessionCount/>

            <!--- Get New Sessions Last 7 days --->
            <cfquery name="GetTotalActiveSessionsLast7" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                AND
                    SessionState_int < 4
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
             </cfquery>

             <cfset dataout.TOTALACTIVESESSIONSLAST7 = GetTotalActiveSessionsLast7.SessionCount/>

            <!--- Get New Sessions Last 1 days --->
            <cfquery name="GetTotalActiveSessionsLast1" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(SessionId_bi) AS SessionCount
                FROM
                    simplequeue.sessionire
                WHERE
                    Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'
                AND
                    SessionState_int < 4
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
            </cfquery>

            <cfset dataout.TOTALACTIVESESSIONSLAST1 = GetTotalActiveSessionsLast1.SessionCount/>
            <!--- END ACTIVE SESSION QUERY  --->

            <!--- USAGE QUERY --->
            <cfquery name="GetTotalUsageLast1" datasource="#DBSourceEBM#">
                SELECT
                    UserId_int,
                    COUNT(*) AS TotalCount,
                    IreType_int
                FROM
                    simplexresults.ireresults
                WHERE
                    Created_dt > DATE_ADD(NOW(), INTERVAL -1 DAY)
                <cfif arguments.inpShortCodeId GT 0>
                    AND
                        ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortcode.SHORTCODE#"/>
                </cfif>
                GROUP BY
                    UserId_int,IreType_int
                ORDER BY
                    TotalCount DESC
                LIMIT 100
            </cfquery>

            <cfloop query="GetTotalUsageLast1">
                <cfset var item = {}/>
                <cfset item.USERID = GetTotalUsageLast1.UserId_int/>
                <cfset item.TOTALCOUNT = GetTotalUsageLast1.TotalCount/>
                <cfset item.IRETYPE = GetTotalUsageLast1.IreType_int/>
                <cfset arrayAppend(dataout.TOTALUSAGE, item)/>
            </cfloop>
            <!--- END USAGE QUERY --->

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetShortCodeById" access="private" output="false" hint="Get shortcode by id">
        <cfargument name="inpShortCodeId" required="true" hint="shortcode id input"/>

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.SHORTCODE = '' />
        <cfset dataout.SHORTCODEID = arguments.inpShortCodeId/>

        <cfset var getShortCode = '' />

        <cftry>
            <cfquery name="getShortCode" datasource="#session.DBSourceREAD#">
                SELECT
                    ShortCode_vch
                FROM
                    sms.shortcode
                WHERE
                    ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
                LIMIT
                    1
            </cfquery>

            <cfif getShortCode.RECORDCOUNT GT 0>
                <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch/>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = 'Get shortcode successfully!'/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetCreditAddonReport" access="remote" hint="Get total users bought credits">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.BUYCREDIT = 0 />
        <cfset dataout.BUYKEYWORD = 0 />

        <cfset var getAddonCount = '' />

        <cftry>
            <cfquery name="getAddonCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    -- SUM(IF((moduleName = "Buy Credits and Keywords" AND byNumberSMS > 0) OR moduleName = "Buy Credit", 1, 0)) AS BuyCredit
                    DISTINCT p.UserId_int
                FROM
                    simplebilling.payment_worldpay as p
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = p.UserId_int
                WHERE
                    ((p.moduleName = "Buy Credits and Keywords" AND p.byNumberSMS > 0) OR (p.moduleName = "Buy Credit" AND p.byNumberSMS > 0))
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    p.created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>

            <cfset dataout.BUYCREDIT = getAddonCount.RECORDCOUNT/>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="Getgoogletoken" access="remote" hint="Get google token for CRM">
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DATA = '' />
        <cfset var getgoogletokenquery = ''>

        <cftry>
            <cfquery name="getgoogletokenquery" datasource="#Session.DBSourceREAD#">
                SELECT Value_txt
                FROM simpleobjects.sire_setting
                WHERE VariableName_txt = 'googletoken'
            </cfquery>

            <cfif getgoogletokenquery.RECORDCOUNT GT 0>
                <cfset dataout.DATA = getgoogletokenquery.Value_txt/>
                <cfset dataout.RXRESULTCODE = 1/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="Getrevenuewoldpaytotal" access="remote" hint="Get total revenue wold pay">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TOTALREVENUEWP = 0 />
        <cfset dataout.TOTALREFUNDWP = 0 />
        <cfset dataout.TOTALREVENUEWPALL = 0 />

        <cfset var paymentResposeContent = ''>
        <cfset var transactionsdata = ''>
        <cfset var getAddonCount = '' />
        <cfset var getaccounttestquery = ''/>
        <cfset var listaccounttest = ''/>
        <cfset var TotalAmountresult = ''/>
        <cfset var totalamount = 0>
        <cfset var totalrefund = 0>
        <cfset var totalamountall = 0/>
        <cftry>

        <cfquery name="TotalAmountresult" datasource="#Session.DBSourceREAD#" >
            SELECT
                SUM(wp.transactionData_amount) AS AmountPP
            FROM
                simplebilling.payment_worldpay wp
            INNER JOIN
                simpleobjects.useraccount us ON wp.userId_int = us.userId_int
            Where
                wp.responseText = 'Approved'
            AND
                us.IsTestAccount_ti = 0
            AND
                wp.created BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
        </cfquery>
        <cfif TotalAmountresult.RecordCount GT 0>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TOTALREFUNDWP = totalrefund/>
            <cfif TotalAmountresult.AmountPP NEQ "">
                <cfset dataout.TOTALREVENUEWP = TotalAmountresult.AmountPP/>
                <cfset dataout.TOTALREVENUEWPALL = TotalAmountresult.AmountPP/>
            </cfif>
        </cfif>
        <cfcatch type="any">
            <cfset dataout.MESSAGE = cfcatch.MESSAGE />
            <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
            <cfset dataout.TYPE = cfcatch.TYPE />
            <cfset dataout.RXRESULTCODE = -1 />
        </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetKeywordAddonReport" access="remote" hint="Get total users buy keywords">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.BUYCREDIT = 0 />
        <cfset dataout.BUYKEYWORD = 0 />

        <cfset var getAddonCount = '' />

        <cftry>
            <cfquery name="getAddonCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    -- SUM(IF((moduleName = "Buy Credits and Keywords" AND byNumberSMS > 0) OR moduleName = "Buy Credit", 1, 0)) AS BuyCredit,
                    DISTINCT p.UserId_int
                FROM
                    simplebilling.payment_worldpay as p
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = p.UserId_int
                WHERE
                    ((p.moduleName = "Buy Credits and Keywords" AND p.byNumberKeyword > 0) OR (p.moduleName = "By Keyword" AND p.byNumberKeyword > 0))
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    p.created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>

            <cfset dataout.BUYKEYWORD =  getAddonCount.RECORDCOUNT/>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTotalBuyCreditList" access="remote" hint="Get total buy addon list">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserInPaymentLog = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUserInPaymentLog" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    p.UserId_int,
                    ua.EmailAddress_vch,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    SUM(IF((moduleName = "Buy Credits and Keywords" AND byNumberSMS > 0) OR (moduleName = "Buy Credit" AND byNumberSMS > 0), byNumberSMS, 0)) AS BuyCredit
                FROM
                    simplebilling.payment_worldpay as p
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    p.UserId_int = ua.UserId_int
                WHERE
                    ua.UserId_int > 0
                AND
                    ua.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'p.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                AND
                    p.created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                GROUP BY
                    p.UserId_int
                HAVING
                    (
                        BuyCredit > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllUserInPaymentLog">
                <cfset temp = {
                    USERID = '#getAllUserInPaymentLog.UserId_int#',
                    PHONE = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.MFAContactString_vch#',
                    NAME = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.FirstName_vch#' & ' #getAllUserInPaymentLog.LastName_vch#',
                    EMAIL = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.EmailAddress_vch#',
                    CREDIT = '#BuyCredit#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTotalBuyKeywordList" access="remote" hint="Get total buy addon list">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserInPaymentLog = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUserInPaymentLog" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    p.UserId_int,
                    ua.EmailAddress_vch,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    SUM(IF((moduleName = "Buy Credits and Keywords" AND byNumberKeyword > 0) OR (moduleName = "By Keyword" AND byNumberKeyword > 0), byNumberKeyword, 0)) AS BuyKeyword
                FROM
                    simplebilling.payment_worldpay as p
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    p.UserId_int = ua.UserId_int
                WHERE
                    ua.UserId_int > 0
                AND
                    ua.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'p.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                AND
                    p.created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                GROUP BY
                    p.UserId_int
                HAVING
                    (
                        BuyKeyword > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllUserInPaymentLog">
                <cfset temp = {
                    USERID = '#getAllUserInPaymentLog.UserId_int#',
                    PHONE = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.MFAContactString_vch#',
                    NAME = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.FirstName_vch#' & ' #getAllUserInPaymentLog.LastName_vch#',
                    EMAIL = '#getAllUserInPaymentLog.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserInPaymentLog.EmailAddress_vch#',
                    KEYWORD = '#BuyKeyword#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTotalBuyCreditDetails" access="remote" hint="Get buy credit details by user id">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="numeric" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserInPaymentLog = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUserInPaymentLog" datasource="#Session.DBSourceREAD#">
                SELECT
                    byNumberSMS,
                    created
                FROM
                    simplebilling.payment_worldpay
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                    ((moduleName = "Buy Credits and Keywords" AND byNumberSMS > 0) OR (moduleName = "Buy Credit" AND byNumberSMS > 0))
                AND
                    created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        AND
                            DATE(#PreserveSingleQuotes(filterItem.NAME)#) =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                    </cfloop>
                </cfif>
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllUserInPaymentLog">
                <cfset temp = {
                    CREDIT = '#getAllUserInPaymentLog.byNumberSMS#',
                    CREATED = '#DateFormat(getAllUserInPaymentLog.created, "yyyy-mm-dd")#  #TimeFormat(getAllUserInPaymentLog.created, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTotalBuyKeywordDetails" access="remote" hint="Get buy keyword details by user id">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="numeric" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserInPaymentLog = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUserInPaymentLog" datasource="#Session.DBSourceREAD#">
                SELECT
                    byNumberKeyword,
                    created
                FROM
                    simplebilling.payment_worldpay
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                    ((moduleName = "Buy Credits and Keywords" AND byNumberKeyword > 0) OR (moduleName = "By Keyword" AND byNumberKeyword > 0))
                AND
                    created
                BETWEEN
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        AND
                            DATE(#PreserveSingleQuotes(filterItem.NAME)#) =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                    </cfloop>
                </cfif>
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllUserInPaymentLog">
                <cfset temp = {
                    KEYWORD = '#getAllUserInPaymentLog.byNumberKeyword#',
                    CREATED = '#DateFormat(getAllUserInPaymentLog.created, "yyyy-mm-dd")# #TimeFormat(getAllUserInPaymentLog.created, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CountEnterpriseUser" access="remote" hint="Count Enterprise Users">

        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0 />
        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>


        <cfset var getCount = '' />
        <cftry>
            <cfquery name="getCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int
                FROM
                    simplebilling.enterprise_user_billing_plan
                WHERE
                    BalanceThreshold_int > 0
                AND
                    Created_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.RESULT = getCount.RECORDCOUNT />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetEnterpriseUsers" access="remote" hint="Get Enterprise users">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUsers = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var currentUserCredits = '' />
        <cfset var RetGetBalance = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    u.UserId_int,
                    u.FirstName_vch,
                    u.MFAContactString_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch,
                    e.BalanceThreshold_int,
                    e.Created_dt
                FROM
                    simpleobjects.useraccount as u
                INNER JOIN
                    simplebilling.enterprise_user_billing_plan as e
                ON
                    u.UserId_int = e.UserId_int
                AND
                    e.BalanceThreshold_int > 0
                AND
                    e.Created_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'u.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                ORDER BY
                    UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllUsers#">
                <cfinvoke method="GetBalance" component="session.sire.models.cfc.billing" returnvariable="RetGetBalance">
                    <cfinvokeargument name="inpUserId" value="#getAllUsers.UserId_int#"/>
                </cfinvoke>

                <cfif RetGetBalance.RXRESULTCODE GT 0>
                    <cfif RetGetBalance.Balance LT 1000000 >
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance,',')>
                     <cfelseif RetGetBalance.Balance LT 1000000000>
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance \ 1000000,',') & " M">
                     <cfelse>
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance \ 1000000000,',') & " B">
                    </cfif>
                </cfif>

                <cfset temp = {
                    USERID = '#getAllUsers.UserId_int#',
                    PHONE = '#getAllUsers.MFAContactString_vch#',
                    NAME = '#getAllUsers.FirstName_vch#' & ' #getAllUsers.LastName_vch#',
                    EMAIL = '#getAllUsers.EmailAddress_vch#',
                    THRESHOLD = '#getAllUsers.BalanceThreshold_int#',
                    CREDIT = '#currentUserCredits#',
                    CREATED = '#DateFormat(getAllUsers.Created_dt, "yyyy-mm-dd")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetEnterpriseRevenue" access="remote" hint="get enterprise revenue">

        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0 />
        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset var dateDiff = 0 />

        <cfset dateDiff = dateDiff("d", startDate, endDate)/>

        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var getEnterpriseUserBilling = ''/>
        <cfset var listEnterpriseUserBilling = []/>


        <cfset var getRevenue = '' />
        <cfset var totalAll = 0 />
        <cfset var crmp = 0 />
        <cfset var crm_userId = 0 />


        <cftry>

            <cfquery name="getEnterpriseUserBilling" datasource="#Session.DBSourceREAD#">
                SELECT UserId_int, CRMPerMessage_dec FROM simplebilling.enterprise_user_billing_plan;
            </cfquery>

            <cfloop query="getEnterpriseUserBilling">

                 <cfset crmp = getEnterpriseUserBilling.CRMPerMessage_dec/>
                 <cfset crm_userId = getEnterpriseUserBilling.UserId_int/>
                <cfquery name="getRevenue" datasource="#Session.DBSourceREAD#">
                    SELECT
                        (count(si.IREResultsId_bi) * #crmp#) as total
                    FROM
                        simplexresults.ireresults as si USE INDEX (IDX_Created_User_IRETYPE)
                    WHERE
                        (si.IREType_int = 1 OR si.IREType_int = 2)
                    AND
                        si.UserId_int =  #crm_userId#
                    AND
                        LENGTH(si.ContactString_vch ) < 14
                    AND
                        si.Created_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                </cfquery>

                <cfset totalAll = totalAll + getRevenue.total/>
            </cfloop>
            <!---    <cfloop query="getEnterpriseUserBilling">
                <cfset arrayAppend(listEnterpriseUserBilling, UserId_int, CRMPerMessage_dec)/>
            </cfloop>
            --->
            <!--- <cfquery name="getRevenue" datasource="#Session.DBSourceREAD#">
                SELECT
                    count(si.IREResultsId_bi) *  as total
                FROM
                    simplexresults.ireresults as si USE INDEX (IDX_Contact_Created)
                WHERE
                    (si.IREType_int = 1 OR si.IREType_int = 2)
                AND
                    si.UserId_int in (#arrayToList(listEnterpriseUserBilling)#)
                AND
                    LENGTH(si.ContactString_vch ) < 14
                AND
                <cfif dateDiff LTE 7>
                    si.Created_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                                  DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfelse>
                    date(si.Created_dt) >=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND  date(si.Created_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">
                </cfif>

            </cfquery> --->
            <cfif getRevenue.RECORDCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = 1/>
                <!--- <cfset dataout.RESULT = getRevenue.total*0.012 /> --->
                <cfset dataout.RESULT = #totalAll# />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetCarrierInfo" access="remote" hint="We're getting more spammer each day,  we need a tool to see which phone carrier are people signing up with. please create a report for Jeff is today">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <!--- <cfargument name="inpSkipPaging" default="0"/> --->
        <cfargument name="inpBatchId" default="0"/>
        <cfargument name="customFilter" required="false">

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />

        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfif inpBatchId eq "160745MFA">
                <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        si.ContactString_vch,
                        si.Carrier_vch,
                        si.ShortCode_vch,
                        si.Created_dt,
                        api.Referer_vch AS Response_vch,
                        sc.Country_vch,
                        sc.Operator_vch,
                        sc.OperatorAlias_vch,
                        sc.CarrierGroup_vch
                    FROM
                        simplexresults.ireresults as si
                    LEFT JOIN
                        simplexresults.apitracking as api
                    ON
                        si.Created_dt = api.Created_dt
                    LEFT JOIN
                        sms.carrierlist as sc
                    ON
                        sc.OperatorId_int = si.Carrier_vch
                    WHERE
                        si.BatchId_bi = 160745
                    AND
                        si.IREType_int = 1
                    AND
                        si.CPId_int = 1

                    AND
                        api.Subject_vch = 'SendMFA'
                    ORDER BY
                        si.Created_dt DESC
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfquery>
            <cfelse>
                <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        si.ContactString_vch,
                        si.Carrier_vch,
                        si.ShortCode_vch,
                        si.Created_dt,
                        si.Response_vch,
                        sc.Country_vch,
                        sc.Operator_vch,
                        sc.OperatorAlias_vch,
                        sc.CarrierGroup_vch
                    FROM
                        simplexresults.ireresults as si
                    INNER JOIN
                        sms.carrierlist as sc
                    ON
                        sc.OperatorId_int = si.Carrier_vch
                    WHERE
                        si.BatchId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpBatchId#'>
                    AND
                        si.IREType_int = 2
                    ORDER BY
                        si.Created_dt DESC

                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">

                </cfquery>
            </cfif>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfif getList.RECORDCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfloop query="#getList#">

                    <cfset temp = {
                        CONTACTSTRING = '#getList.ContactString_vch#',
                        CARRIER = '#getList.Carrier_vch#',
                        SHORTCODE = '#getList.ShortCode_vch#',
                        RESPONSE = '#getList.Response_vch#',
                        COUNTRY = '#getList.Country_vch#',
                        OPERATOR = '#getList.Operator_vch#',
                        OPERATORALIAS = '#getList.OperatorAlias_vch#',
                        CARRIERGROUP = '#getList.CarrierGroup_vch#',
                        CREATED = '#DateFormat(getList.Created_dt, "yyyy-mm-dd")# #TimeFormat(getList.Created_dt, "HH:mm:ss")#',
                    }/>

                    <cfset ArrayAppend(dataout["DATALIST"],temp)>
                </cfloop>
            </cfif>


            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetBingSpend" access="remote" output="true" hint="get bing spend">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0 />

        <cfset var getSpend = '' />

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cftry>
            <cfquery name="getSpend" datasource="#Session.DBSourceREAD#">
                SELECT
                    SUM(br.Spend) AS Spend
                FROM
                    simplelists.bingreport AS br
                WHERE
                    br.GregorianDate
                    BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfif isNumeric(getSpend.Spend)>
                <cfset dataout.RESULT = "#getSpend.Spend#" />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="getFacebookAdsSpend" access="remote" output="true" hint="get facebook ads spend">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0 />
        <cfset var fbAccountId = '140169069764357'/>
        <cfset var fbApiVersion = 'v2.11'/>
        <cfset var access_token = 'EAAUvlrPyP0QBADhdBs48714V2nDqZBOI36gz3FXaGQ9N01z8kcx5NBZCfI3dN8ZB5eCrkmjo4PN2L1lsJZAblng7H8PuwZACeYNkroT80iufyNaMXyXrTnwiKLuyX225b9UGWPdSHFpmFym0ICIMpYwaXhlMnCakTcs38QcjXrOG4xBKOC8VP04VeJHomaPcZD'/>
        <cfset var fbUrl = 'https://graph.facebook.com/'&fbApiVersion&'/'&'act_'&fbAccountId&"/insights">
        <cfset var timeRange = "">
        <cfset var fbResponse = ''/>
        <cfset var fbResponseContent = ''/>
        <cftry>
            <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
            <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
            <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
            <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
            <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
            <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
            <cfset var timeRange = "{'since':'"&startDate&"','until':'"&endDate&"'}">

            <cfhttp url="#fbUrl#" method="get" result="fbResponse" port="443" timeout="5">
                <cfhttpparam type="url" name="time_range" value="#timeRange#"/>
                <cfhttpparam type="url" name="access_token" value="#access_token#"/>
            </cfhttp>

            <cfif fbResponse.status_code EQ 200>
                <cfif trim(fbResponse.filecontent) NEQ "" AND isJson(fbResponse.filecontent)>
                    <cfset fbResponseContent = DeserializeJSON(fbResponse.filecontent)>
                    <cfif structKeyExists(fbResponseContent, "data")>
                        <cfif structKeyExists(fbResponseContent.data[1], "spend")>
                            <cfset dataout.RESULT = fbResponseContent.data[1]['spend'] />
                            <cfset dataout.RXRESULTCODE = 1 />
                        </cfif>
                    </cfif>
                </cfif>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetFailedMFA" access="remote" hint="Get Failed MFA">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllRecord = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    u.UserId_int,
                    u.FirstName_vch,
                    u.MFAContactString_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch,
                    ul.LogId_int,
                    ul.Timestamp_dt
                FROM
                    simpleobjects.useraccount as u
                RIGHT JOIN
                    simpleobjects.userlogs as ul
                ON
                    u.UserId_int = ul.UserId_int
                WHERE
                    ul.ModuleName_vch = "MFA authentication Failure"
                AND
                    u.IsTestAccount_ti = 0
                AND
                    ul.Timestamp_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'u.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                ORDER BY
                    UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfset dataout.RESULT = rsCount.iTotalRecords />

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    LOGID = '#getAllRecord.LogId_int#',
                    USERID = '#getAllRecord.UserId_int#',
                    PHONE = '#getAllRecord.MFAContactString_vch#',
                    NAME = '#getAllRecord.FirstName_vch#' & ' #getAllRecord.LastName_vch#',
                    EMAIL = '#getAllRecord.EmailAddress_vch#',
                    CREATED = '#DateFormat(getAllRecord.Timestamp_dt, "yyyy-mm-dd")# #TimeFormat(getAllRecord.Timestamp_dt, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction
        name="getTotalNewPaidUsers"
        access="public"
        output="false"
        hint="Return total new paid users."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>

        <cfset var item = {}/>
        <cfset var getTotalNewPaidUser = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var filterItem = '' />
        <cfset var checkUniqueUserPlan = '' />

        <cftry>
            <cfquery name="getTotalNewPaidUser" datasource="#Session.DBSourceREAD#">
                SELECT
                    up.UserPlanId_bi,
                    ua.UserId_int,
                    up.PlanId_int,
                    up.StartDate_dt
                FROM
                    simpleobjects.useraccount as ua
                RIGHT JOIN
                    simplebilling.userplans as up
                ON
                    ua.UserId_int = up.UserId_int
                WHERE
                    up.PlanId_int > 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    up.StartDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>

            </cfquery>

            <cfloop query="getTotalNewPaidUser">

                <cfquery name="checkUniqueUserPlan" datasource="#Session.DBSourceREAD#">
                    SELECT
                        UserPlanId_bi
                    FROM
                        simplebilling.userplans
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#getTotalNewPaidUser.UserId_int#'>
                </cfquery>

                <cfif checkUniqueUserPlan.RECORDCOUNT EQ 1>
                    <cfset dataout.RESULT += 1/>
                </cfif>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetNewPaidUsers" access="remote" hint="Get new paid users">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["datalist"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getNewUserPlanRecord = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var countNewPaidUsers = '' />
        <cfset var getNewUserPlanRecord = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getNewUserPlanRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    ua.UserId_int,
                    ua.FirstName_vch,
                    ua.MFAContactString_vch,
                    ua.LastName_vch,
                    ua.EmailAddress_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    up.PlanId_int,
                    up.StartDate_dt,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount as ua
                RIGHT JOIN
                    (
                        SELECT
                            UserPlanId_bi,
                            PlanId_int,
                            StartDate_dt,
                            UserId_int
                        FROM
                            simplebilling.userplans
                        GROUP BY
                            UserId_int
                        HAVING
                            COUNT(UserPlanId_bi) = 1) as up
                ON
                    ua.UserId_int = up.UserId_int
                WHERE
                    up.PlanId_int > 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    up.StartDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                ORDER BY
                    ua.UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfloop query="getNewUserPlanRecord">

                <cfset temp = {
                    USERID = '#getNewUserPlanRecord.UserId_int#',
                    CONTACTSTRING = '#getNewUserPlanRecord.MFAContactString_vch#',
                    USERNAME = '#getNewUserPlanRecord.FirstName_vch#' & ' #getNewUserPlanRecord.LastName_vch#',
                    USEREMAIL = '#getNewUserPlanRecord.EmailAddress_vch#',
                    REGISTEREDDATE = '#DateFormat(getNewUserPlanRecord.StartDate_dt, "yyyy-mm-dd")# #TimeFormat(getNewUserPlanRecord.StartDate_dt, "HH:mm:ss")#',
                    LASTLOGIN = '#DateFormat(getNewUserPlanRecord.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getNewUserPlanRecord.LastLogIn_dt, "HH:mm:ss")#',
                    SOURCE= MarketingSource_vch
                }/>

                <cfset ArrayAppend(dataout["datalist"],temp)>
            </cfloop>

            <cfset countNewPaidUsers = getTotalNewPaidUsers(arguments.inpStartDate, arguments.inpEndDate)/>
            <cfset dataout["iTotalDisplayRecords"] = countNewPaidUsers.RESULT/>
            <cfset dataout["iTotalRecords"] = countNewPaidUsers.RESULT/>

            <cfset dataout.RESULT = countNewPaidUsers.RESULT />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="SearchMTMessageByWord" access="remote" hint="search MTs for special words used">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset var temp = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllRecord = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {} />

        <cfset var rsCount = ''/>
        <cfset var getAllRecord = '' />

        <cftry>

            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    u.UserId_int,
                    u.FirstName_vch,
                    u.MFAContactString_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch,
                    ire.BatchId_bi,
                    ire.Created_dt,
                    ire.UserId_int,
                    ire.Response_vch
                FROM
                    simpleobjects.useraccount as u
                RIGHT JOIN
                    simplexresults.ireresults as ire
                ON
                    u.UserId_int = ire.UserId_int
                WHERE
                    ire.IREType_int = 1
                <!--- AND
                    u.IsTestAccount_ti = 0 --->
                AND
                    LENGTH(ire.ContactString_vch) < 14
                AND
                    ire.Created_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    AND
                        ire.Response_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.customFilter#%">
                </cfif>
                GROUP BY
                    ire.BatchId_bi,
                    ire.Response_vch
                ORDER BY
                    ire.UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfset dataout.RESULT = rsCount.iTotalRecords />

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    USERID = '#getAllRecord.UserId_int#',
                    PHONE = '#getAllRecord.MFAContactString_vch#',
                    NAME = '#getAllRecord.FirstName_vch#' & ' #getAllRecord.LastName_vch#',
                    EMAIL = '#getAllRecord.EmailAddress_vch#',
                    BATCHID = '#getAllRecord.BatchId_bi#',
                    MESSAGE = '#getAllRecord.Response_vch#',
                    CREATED = '#DateFormat(getAllRecord.Created_dt, "yyyy-mm-dd")# #TimeFormat(getAllRecord.Created_dt, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetUserActivityReport" access="remote" hint="Get User's Activity Report">
        <cfargument name="inpUserId" required="true" hint=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllRecord = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ModuleName_vch,
                    Operator_vch,
                    Timestamp_dt
                FROM
                    simpleobjects.userlogs
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                ORDER BY
                    LogId_int DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    MODULE = '#getAllRecord.ModuleName_vch#',
                    OPERATOR = '#getAllRecord.Operator_vch#',
                    CREATED = '#DateFormat(getAllRecord.Timestamp_dt, "yyyy-mm-dd")# #TimeFormat(getAllRecord.Timestamp_dt, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserApiTrackingReport" access="remote" hint="Get User's Api Tracking Report">
        <cfargument name="inpUserId" required="true" hint=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllRecord = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    Subject_vch,
                    Output_vch,
                    Created_dt
                FROM
                    simplexresults.apitracking
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                ORDER BY
                    APITrackingId_int DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    SUBJECT = '#getAllRecord.Subject_vch#',
                    OUTPUT = '#getAllRecord.Output_vch#',
                    CREATED = '#DateFormat(getAllRecord.Created_dt, "yyyy-mm-dd")# #TimeFormat(getAllRecord.Created_dt, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserBlastCampaignReport" access="remote" hint="Get User's Blast Campaign Report">
        <cfargument name="inpUserId" required="true" hint=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllRecord = '' />
        <cfset var rsCount = '' />
        <cfset var groupIds = '' />
        <cfset var sub = '' />
        <cfset var RetValGetSubscriberInfo = '' />
        <cfset var RetValCountSMS = '' />
        <cfset var temp = {} />
        <cfset var SENT = 0/>
        <cfset var RECEIVED = 0/>

        <cftry>
            <cfquery name="getAllRecord" datasource="#session.DBSourceREAD#">
                SELECT SQL_CALC_FOUND_ROWS
                    sb.BatchId_bi,
                    -- sg.GroupName_vch,
                    -- sg.GroupId_bi,
                    sb.Desc_vch,
                    sbbl.GroupIds_vch,
                    sbbl.AmountLoaded_int
                FROM
                    simpleobjects.batch sb
                INNER JOIN
                    simpleobjects.batch_blast_log sbbl
                ON
                    sb.BatchId_bi = sbbl.BatchId_bi
                -- INNER JOIN
                --  simplelists.grouplist sg
                -- ON
                --  sbbl.GroupIds_vch = sg.GroupId_bi
                WHERE
                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
            </cfquery>

            <cfif getAllRecord.RECORDCOUNT GT 0>
                <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>
                <cfloop query="rsCount">
                    <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                    <cfset dataout["iTotalRecords"] = iTotalRecords>
                </cfloop>

                <cfloop query="getAllRecord">

                    <cfset groupIds = valueList(getAllRecord.GroupIds_vch)>

                    <cfinvoke method="GetSubscriberInfo" component="session.sire.models.cfc.subscribers" returnvariable="RetValGetSubscriberInfo">
                        <cfinvokeargument name="inpSubscriberIds" value="#groupIds#"/>
                    </cfinvoke>

                    <cfif RetValGetSubscriberInfo.RXRESULTCODE LT 1>
                        <cfthrow type="Subscriber" errorcode="-1" message="Can't get subscribers info" detail="#RetValGetSubscriberInfo.MESSAGE#"/>
                    </cfif>

                    <cfloop array="#RetValGetSubscriberInfo.RESULT#" index="sub">

                        <cfset SENT = 0/>
                        <cfset RECEIVED = 0/>
                        <cfinvoke method="CountAllSMSOfSubscriber" component="session.sire.models.cfc.subscribers" returnvariable="RetValCountSMS">
                            <cfinvokeargument name="inpGroupId" value="#sub.GROUPID_BI#"/>
                            <cfinvokeargument name="inpBatchId" value="#getAllRecord.BatchId_bi#"/>
                        </cfinvoke>

                        <cfif RetValCountSMS.RXRESULTCODE GT 0>
                            <cfset SENT = RetValCountSMS.SENT/>
                            <cfset RECEIVED = RetValCountSMS.RECEIVED/>
                        </cfif>

                        <cfset temp = {
                            BATCHID = '#getAllRecord.BatchId_bi#',
                            BATCHNAME = '#getAllRecord.Desc_vch#',
                            SUBSCRIBERLIST = '#sub.GROUPNAME_VCH#',
                            SENT = '#SENT#',
                            QUEUED = '#RetValCountSMS.QUEUED#',
                            OPTIN = '#sub.OPTIN#',
                            OPTOUT = '#sub.OPTOUT#',
                            PERCENT = '#(SENT NEQ 0 ? NumberFormat(RECEIVED/SENT,"0.000") : 0)*100#%',
                            GROUPID = '#sub.GROUPID_BI#'
                        }/>

                        <cfset arrayAppend(dataout["DATALIST"], temp)/>
                    </cfloop>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get report success"/>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>


    <cffunction name="GetUserBlastCampaignReportNew" access="remote" hint="Get list blast campaign report of user">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">

        <cfset var emsLists = '' />
        <cfset var filterItem   = '' />
        <cfset var GetEMSStatus = '' />
        <cfset var getKeywords  = '' />

        <cfset var dataOut = {}>
        <cfset dataOut.DATA = ArrayNew(1)>
        <cfset var GetEMS = 0>
        <cfset var total_pages = 1>
        <cfset var ownerType = 0 >
        <cfset var ownerId = 0 >
        <cfset var RecipientCount = 0>
        <cfset var tempItem  = ArrayNew(1) >
        <cfset var GetNumbersCount  = 0 >
        <cfset var permissionStr  = 0 >
        <cfset var getUserByUserId  = 0 >
        <cfset var order    = "">
        <cfset var PausedCount      = "">
        <cfset var PendingCount         = "">
        <cfset var InProcessCount       = "">
        <cfset var CompleteCount        = "">
        <cfset var ContactResultCount       = "">
        <cfset var IsScheduleInFuture       = "">
        <cfset var IsScheduleInPast         = "">
        <cfset var IsScheduleInCurrent      = "">
        <cfset var LastUpdatedFormatedDateTime      = "">
        <cfset var startDate    = "">
        <cfset var endDate  = "">
        <cfset var ContactTypes         = "">
        <cfset var statusHtml   = "">
        <cfset var ImageStatus  = "">
        <cfset var StatusName       = "">
        <cfset var errorMessage         = "">
        <cfset var reportHtml       = "">
        <cfset var GetCampaignSchedule  = "">
        <cfset var GetPausedCount       = "">
        <cfset var GetPendingCount  = "">
        <cfset var GetInProcessCount        = "">
        <cfset var GetCompleteCount         = "">
        <cfset var GetContactResult         = "">
        <cfset var GetDelivered     = "">
        <cfset var GetListElligableGroupCount_ByType        = "">
        <!---for checking and disabling none method EMS--->
        <cfset var BatchDescLink = '' />
        <cfset var batchListHasKeyword = ''>
        <cfset var KeywordBuff = '' />
        <cfset var DescBuff = '' />
        <cfset var getOptinout = '' />
        <cfset var GetQueueStatus = '' />
        <cfset var emsIds = []>
        <cfset var shortcode = '' />
        <cfset var GetSessionActive = '' />
        <cfset var Incontactque = 0/>
        <cfset var TotalSMS = 0/>
        <cfset var GetNumberContactQue = {}/>
        <cfset var GetNumberContactSend = {}/>
        <cfset var GetScheduleResult = {}/>
        <cfset var campaignSchedule = ''/>
        <cfset var rowSchedule = ''/>
        <cfset var RetVarGetAdminPermission = {}/>

        <cftry>

            <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>
            <cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
                <cfset dataout.RESULT = -1>
                <cfset dataout.MESSAGE = "No Admin Permission."/>
                <cfreturn dataout>
            </cfif>

            <cfinvoke method="BuildSortingParamsForDatatable" component="#Session.SessionCFCPath#.emergencymessages" returnvariable="order">
                <cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
                <cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
                <cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
                <cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
            </cfinvoke>


            <!--- remove old method:
            <cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
            --->

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>

            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>

            <!---Get total EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
                SELECT
                    COUNT(b.BatchId_bi) AS TOTALCOUNT
                FROM
                    simpleobjects.batch AS b
                WHERE
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                    b.Active_int > 0
                AND
                    b.TemplateType_ti = 1
            <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                LIMIT 1
            </cfquery>

            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>


        <!--- LEFT JOIN simpleobjects.batch_blast_log bl ON (bl.BatchId_bi = b.BatchId_bi) --->

                <!--- Get ems data --->
                <cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
                    SELECT
                        b.BatchId_bi,
                        b.RXDSLibrary_int,
                        b.RXDSElement_int,
                        b.RXDSScript_int,
                        b.DESC_VCH,
                        b.Created_dt,
                        b.ContactGroupId_int,
                        b.ContactTypes_vch,
                        b.ContactNote_vch,
                        b.ContactIsApplyFilter,
                        b.ContactFilter_vch,
                        b.EMS_Flag_int,
                        b.TemplateId_int,
                        b.TemplateType_ti,
                        gl.GroupName_vch,
                        tc.Name_vch AS TemplateName,
                        0 AS totalOptin,
                        0 AS totalOptout
                    FROM
                        simpleobjects.batch AS b
                        LEFT JOIN simpleobjects.templatecampaign tc ON b.TemplateId_int = tc.TID_int
                        LEFT JOIN simplelists.subscriberbatch sb ON b.batchid_bi = sb.batchid_bi
                        LEFT JOIN simplelists.grouplist gl ON sb.GroupId_bi = gl.GroupId_bi
                    WHERE
                        b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    AND
                        b.Active_int > 0
                    AND
                        b.TemplateType_ti = 1
                <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                    GROUP BY b.BatchId_bi
                    #order#
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfquery>


                <cfloop query="GetEMS">
                    <cfset arrayAppend(emsIds,BatchId_bi)>
                </cfloop>
                <cfset emsLists = {}>

                <cfif arrayLen(emsIds) GT 0>

                    <cfquery name="GetQueueStatus" datasource="#Session.DBSourceREAD#">
                        SELECT
                            BatchId_bi,
                            count(PKId_int) as total,
                            COUNT(AmountLoaded_int) AS TOTALSMS,
                            COUNT(ActualLoaded_int) AS INCONTACTQUE,
                            PKId_int
                        FROM
                            simpleobjects.batch_blast_log
                        WHERE
                            Status_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_LOADING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PROCESSING#">)
                        GROUP BY
                            BatchId_bi
                        HAVING
                            BatchId_bi IN (#arrayToList(emsIds, ',')#)
                    </cfquery>

                    <cfif GetQueueStatus.RECORDCOUNT GT 0>
                        <cfloop query="GetQueueStatus">
                            <cfif total GT 0>
                                <cfset emsLists[BatchId_bi] = PKId_int>
                            </cfif>
                        </cfloop>
                    </cfif>

                </cfif>

                <cfloop query="GetEMS">

                    <!--- get total msg send --->
                    <cfquery name="GetNumberContactSend" datasource="#Session.DBSourceREAD#">
                        SELECT
                            COUNT(IF(DTSStatusType_ti = 5, 1, NULL)) AS totalSend,
                            COUNT(DTSId_int) AS TotalInQueue
                        FROM
                            simplequeue.contactqueue
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BatchId_bi#">
                        AND
                            UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    </cfquery>

                    <!--- get campaign schedule --->

                    <cfinvoke method="GetSchedule" component="session.sire.models.cfc.schedule" returnvariable="GetScheduleResult">
                        <cfinvokeargument name="INPBATCHID" value="#BatchId_bi#"/>
                    </cfinvoke>
                    <cfif GetScheduleResult.RXRESULTCODE EQ 1>
                        <cfset rowSchedule = GetScheduleResult.ROWS/>
                        <cfset campaignSchedule = rowSchedule[1].START_DT&" to "&rowSchedule[1].STOP_DT&"<br/> Time(hour): "&rowSchedule[1].STARTHOUR_TI&" - "&rowSchedule[1].ENDHOUR_TI>
                    </cfif>

                    <cfset tempItem = {
                    ID = '#BatchId_bi#',
                    NAME =  '#DESC_VCH#',
                    GROUPNAME = "#GetEMS.GroupName_vch#",
                    TOTALSEND =  GetNumberContactSend.totalSend,
                    TOTALINQUEUE =  GetNumberContactSend.TotalInQueue,
                    SCHEDULE = campaignSchedule
                    } />
                    <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
                </cfloop>
            </cfif>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>
    <cffunction
        name="getTotalUserRequestDowngrades"
        access="public"
        returntype="struct"
        output="false"
        hint="Return total user request downgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = '' />
        <cfset var userIds = [] />
        <cfset var filterItem = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


        <!--- Un-Comment this code below when change from last 30 days to depen on Filter of CRM --->
        <!---  <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>--->

        <!--- Comment this code below when change from last 30 days to depen on Filter of CRM --->
        <cfset endDate = DateFormat(Now(), 'yyyy-mm-dd')/>
        <cfset startDate = DateAdd("d", -30, endDate) />
        <cfset startDate = DateFormat(startDate,'yyyy-mm-dd') />

        <cftry>
            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int AS UserId
                FROM
                    simplebilling.userplans
                WHERE
                    UserDowngradeDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)


            </cfquery>

            <cfif getUserDowngrades.RECORDCOUNT GT 0>

                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
                </cfloop>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        COUNT(*) AS TOTAL
                    FROM
                        simpleobjects.useraccount ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>

                    ORDER BY
                        ua.UserId_int DESC
                </cfquery>

                <cfset dataout.RESULT = getUsers.TOTAL/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
    <cffunction
        name="getUserRequestDowngrades"
        access="remote"
        returntype="struct"
        output="false"
        hint="Return total new user downgrades."
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var userIds = []/>
        <cfset var userDowngrades = []/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var countReport = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />

        <!--- Un-Comment this code below when change from last 30 days to depen on Filter of CRM --->
        <!--- <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/> --->

         <!--- Comment this code below when change from last 30 days to depen on Filter of CRM --->
        <cfset endDate = DateFormat(Now(), 'yyyy-mm-dd')/>
        <cfset startDate = DateAdd("d", -30, endDate) />
        <cfset startDate = DateFormat(startDate,'yyyy-mm-dd') />

        <cfset var filterItem = '' />
        <cfset var userDowngrade = ''/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = ''/>
        <cfset var downgradeDate = ''/>

        <cftry>

            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId,
                    DowngradeDate,
                    UserDowngradeDate,

                    UserDowngradePLanName,
                    CurrentPlanName,
                    EndDate,
                    ApprovedBy
                FROM
                    (SELECT
                        up.UserId_int AS UserId,
                        up.DowngradeDate_dt AS DowngradeDate,
                        up.UserDowngradeDate_dt AS UserDowngradeDate,

                        p1.PlanName_vch AS UserDowngradePLanName,
                        p2.PlanName_vch AS CurrentPlanName,
                        up.EndDate_dt AS EndDate,

                        CONCAT(ua.FirstName_vch,' ',ua.LastName_vch) AS ApprovedBy
                    FROM
                        `simplebilling`.`userplans` up
                    INNER JOIN
                        `simplebilling`.`plans` p1
                    ON  p1.PlanId_int = up.UserDowngradePLan_int

                    INNER JOIN
                        `simplebilling`.`plans` p2
                    ON  p2.PlanId_int = up.PlanId_int

                    LEFT JOIN
                        `simpleobjects`.`useraccount` ua
                    ON  up.UserIdDowngrade_int = ua.UserId_int

                    WHERE
                        up.UserDowngradeDate_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                    -- AND
                    -- up.status_int = 1

                    ORDER BY up.UserDowngradeDate_dt DESC
                    ) AS userplans
                GROUP BY
                    UserId
            </cfquery>

            <cfif getUserDowngrades.RECORDCOUNT GT 0>
                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
                    <cfset downgradeDate = getUserDowngrades.UserDowngradeDate/>
                    <cfset arrayAppend(userDowngrades, {UserId: getUserDowngrades.UserId, DowngradeDate: downgradeDate, UserDowngradePLanName: UserDowngradePLanName, CurrentPlanName: CurrentPlanName, EndDate: EndDate,ApprovedBy: ApprovedBy})/>
                </cfloop>

                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        ua.UserId_int AS UserId,
                        ua.Active_int AS Activated,
                        ua.EmailAddress_vch AS EmailAddress,
                        ua.FirstName_vch AS FirstName,
                        ua.LastName_vch AS LastName,
                        ua.Created_dt AS SignUpDate,
                        ua.LastLogIn_dt AS LastSignInDate,
                        ua.MFAContactString_vch AS ContactString
                    FROM
                        `simpleobjects`.`useraccount` ua
                    WHERE
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)

                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                AND
                                    #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                    ORDER BY
                        ua.UserId_int DESC
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>

                <cfif getUsers.RECORDCOUNT GT 0>
                    <cfset countReport = getTotalUserRequestDowngrades("#arguments.inpStartDate#", "#arguments.inpEndDate#", arguments.customFilter )/>

                    <cfset dataout["iTotalDisplayRecords"] = countReport.RESULT/>
                    <cfset dataout["iTotalRecords"] = countReport.RESULT/>


                    <cfloop query="getUsers">
                        <cfset var item = {} />
                        <cfset item.USERID = getUsers.UserId/>
                        <cfset item.STATUS = getUsers.Activated/>
                        <cfset item.USEREMAIL = getUsers.EmailAddress/>
                        <cfset item.CONTACTSTRING = getUsers.ContactString/>
                        <cfset item.FirstName = getUsers.FirstName/>
                        <cfset item.LastName = getUsers.LastName/>
                        <cfset item.USERNAME = "#getUsers.FirstName# #getUsers.LastName#"/>
                        <cfset item.REGISTEREDDATE = DateFormat(getUsers.SignUpDate, "yyyy-mm-dd")/>
                        <cfset item.LASTLOGIN = "#DateFormat(getUsers.LastSignInDate, "yyyy-mm-dd")# #TimeFormat(getUsers.LastSignInDate, "HH:mm:ss")#"/>
                        <cfloop array="#userDowngrades#" index="userDowngrade">
                            <cfif userDowngrade.UserId EQ getUsers.UserId>
                                <cfset item.REQUESTDOWNGRADES = "#DateFormat(userDowngrade.DowngradeDate, "yyyy-mm-dd")#"/>
                                <cfset item.TOPLANNAME = "#userDowngrade.UserDowngradePLanName#"/>
                                <cfset item.FROMPLANNAME = "#userDowngrade.CurrentPlanName#"/>
                                <cfset item.DOWNGRADESDATE = "#DateFormat(userDowngrade.EndDate, "yyyy-mm-dd")#"/>
                                <cfif userDowngrade.ApprovedBy NEQ ''>
                                        <cfset item.APPROVEDBY = "#userDowngrade.ApprovedBy#"/>
                                <cfelse>
                                        <cfset item.APPROVEDBY = "System"/>
                                </cfif>
                            </cfif>

                        </cfloop>
                        <!--- <cfset arrayAppend(dataout.RESULT, item)/> --->
                        <cfset ArrayAppend(dataout["datalist"],item)>
                    </cfloop>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
    <cffunction name="GetOptOutByCampaignNameList" access="remote" hint="Get opt out list for admin by campaign name">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>

        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {}/>

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    b.UserId_int,
                    ua.EmailAddress_vch,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    b.Desc_vch
                    -- oi.OptOut_dt
                    -- SUM(IF(oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL, 1, 0)) AS OptIn,
                    -- SUM(IF(oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL, 1, 0)) AS OptOut
                FROM
                    simplelists.optinout AS oi
                INNER JOIN
                    simpleobjects.batch as b ON
                    (b.BatchId_bi = oi.BatchId_bi)
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = b.UserId_int
                WHERE
                    ua.UserId_int > 0
                AND
                    LENGTH(oi.ContactString_vch) < 14
                AND
                    ua.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'b.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
                    AND
                    oi.OptOut_dt
                                BETWEEN
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                                AND
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                </cfif>
                and oi.OptOut_dt IS NOT NULL

                GROUP BY
                    b.UserId_int
                -- HAVING
                --     (
                --         OptIn > 0
                --         OR
                --         OptOut > 0
                --     )
                <cfif arguments.inpSkipPaging EQ 0>

                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getList">

                <cfset temp = {
                    USERID = '#getList.UserId_int#',
                    PHONE = '#getList.MFAContactString_vch#',
                    NAME = '#getList.FirstName_vch#' & ' #getList.LastName_vch#',
                    EMAIL = '#getList.EmailAddress_vch#'
                    // OPTOUTDATE = '#getList.OptOut_dt#',
                    // OPTIN = '#getList.OptIn#',
                    // OPTOUT = '#getList.OptOut#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
    <cffunction name="GetEnterpriseUserDetails" access="remote" hint="Get Enterprise users">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUsers = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var currentUserCredits = '' />
        <cfset var RetGetBalance = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUsers" datasource="#Session.DBSourceREAD#">

               SELECT
                    SQL_CALC_FOUND_ROWS
                    u.UserId_int,
                    u.FirstName_vch,
                    u.MFAContactString_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch,
                    e.BalanceThreshold_int,
                    e.Created_dt,
                    e.CRMPerMessage_dec,
                    count(si.IREResultsId_bi) as totalMessage
                FROM
                    simplexresults.ireresults as si USE INDEX (IDX_Contact_Created)
                INNER join
                    simpleobjects.useraccount as u
                ON
                    u.UserId_int = si.UserId_int
                INNER JOIN
                    simplebilling.enterprise_user_billing_plan as e
                ON
                    u.UserId_int = e.UserId_int
                WHERE
                    (si.IREType_int = 1 OR si.IREType_int = 2)
                AND
                    LENGTH(si.ContactString_vch ) < 14
                AND
                    date(si.Created_dt) >=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    date(si.Created_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'u.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                GROUP BY
                    UserId_int
                ORDER BY
                    UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
            <cfloop query="#getAllUsers#">
                <!--- <cfinvoke method="GetBalance" component="session.sire.models.cfc.billing" returnvariable="RetGetBalance">
                    <cfinvokeargument name="inpUserId" value="#getAllUsers.UserId_int#"/>
                </cfinvoke>

                <cfif RetGetBalance.RXRESULTCODE GT 0>
                    <cfif RetGetBalance.Balance LT 1000000 >
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance,',')>
                     <cfelseif RetGetBalance.Balance LT 1000000000>
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance \ 1000000,',') & " M">
                     <cfelse>
                        <cfset currentUserCredits = NumberFormat(RetGetBalance.Balance \ 1000000000,',') & " B">
                    </cfif>
                </cfif> --->
                <cfif getAllUsers.RecordCount GT 0>
                    <cfset temp = {
                        USERID = '#getAllUsers.UserId_int#',
                        PHONE = '#getAllUsers.MFAContactString_vch#',
                        NAME = '#getAllUsers.FirstName_vch#' & ' #getAllUsers.LastName_vch#',
                        EMAIL = '#getAllUsers.EmailAddress_vch#',
                        TOTALMESSAGE = '#getAllUsers.totalMessage#',
                        CRMPERMESSAGE = '#getAllUsers.CRMPerMessage_dec#'
                        // CREDIT = '#currentUserCredits#',
                        // CREATED = '#DateFormat(getAllUsers.Created_dt, "yyyy-mm-dd")#'
                    }/>
                     <cfset ArrayAppend(dataout["DATALIST"],temp)>
                </cfif>

            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="GetPaymentRevenueDetails" access="remote" hint="Get Payment revenue details">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUsers = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var currentUserCredits = '' />
        <cfset var RetGetBalance = '' />
        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    us.UserId_int,
                    us.FirstName_vch,
                    us.MFAContactString_vch,
                    us.LastName_vch,
                    us.EmailAddress_vch,
                    wp.transactionData_amount,
                    wp.created
                FROM
                    simplebilling.payment_worldpay wp
                INNER JOIN
                    simpleobjects.useraccount us ON wp.userId_int = us.userId_int
                Where
                    wp.responseText = 'Approved'
                AND
                    us.IsTestAccount_ti = 0
                AND
                    wp.created BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'us.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                ORDER BY
                    UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllUsers#">
                <cfset temp = {
                    USERID = '#getAllUsers.UserId_int#',
                    PHONE = '#getAllUsers.MFAContactString_vch#',
                    NAME = '#getAllUsers.FirstName_vch#' & ' #getAllUsers.LastName_vch#',
                    EMAIL = '#getAllUsers.EmailAddress_vch#',
                    TRANSACTIONAMOUNT = '#getAllUsers.transactionData_amount#',
                    CREATED = '#DateFormat(getAllUsers.created, "yyyy-mm-dd")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <!--- Enterprise Billing --->
    <cffunction name="GetEnterpriseBillingTotal" access="remote" hint="Get Enterprise User billing total">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.ENTERPRISEBILLING = 0 />
        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />

        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var getEnterpriseUserBilling = ''/>
        <cfset var getCountMessage = ''/>
        <cfset var getMOamount = ''/>
        <cfset var getMTamount = ''/>

        <cfset var totalSent = 0 />
        <cfset var totalReceived = 0 />
        <cfset var totalMsg = 0/>
        <cfset var SentAmount = 0 />
        <cfset var ReceivedAmount = 0 />

        <cfset var totalAll = 0 />
        <cfset var levelId = 0 />
        <cfset var crm_userId = 0 />

        <cftry>
            <!--- Get list user is Enterprise --->
            <cfquery name="getEnterpriseUserBilling" datasource="#Session.DBSourceREAD#">
                SELECT
                    si.UserId_int,
                    si.PlanId_int
                FROM
                    simplebilling.enterprise_user_billing_plan as si
            </cfquery>
            <!--- Caculate Enterprise Billing --->
            <cfloop query="getEnterpriseUserBilling">
                <cfset levelId = getEnterpriseUserBilling.PlanId_int/>
                <cfset crm_userId = getEnterpriseUserBilling.UserId_int/>
                <!--- Get total Sent and Received MSG --->
                <cfquery name="getCountMessage" datasource="#Session.DBSourceEBM#">
                    SELECT
                        SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                        SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                    FROM
                        simplexresults.ireresults as ire USE INDEX (IDX_Created_User_IRETYPE)
                    WHERE
                        ire.IREType_int IN (1,2)
                    AND
                        LENGTH(ire.ContactString_vch) < 14
                    AND
                        ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#crm_userId#">
                    AND
                        ire.Created_dt
                    BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                </cfquery>
                <cfif getCountMessage.RecordCount GT 0>
                    <cfif getCountMessage.Sent NEQ ''>
                        <cfset totalSent = getCountMessage.Sent>
                    </cfif>
                    <cfif getCountMessage.Received NEQ ''>
                        <cfset totalReceived = getCountMessage.Received>
                    </cfif>
                    <cfset totalMsg = totalSent + totalReceived>

                    <cfquery name="getMOamount" datasource="#Session.DBSourceREAD#">
                        SELECT
                            (MO1_dec * #totalReceived#) AS ReceivedAmount
                        FROM
                            simplebilling.enterprise_billing_plan
                        WHERE
                            PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#levelId#">
                        AND
                            Volume_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#totalReceived#">
                        ORDER BY Volume_int
                        LIMIT 1
                    </cfquery>
                    <cfif getMOamount.RecordCount GT 0>
                        <cfif getMOamount.ReceivedAmount NEQ "">
                            <cfset ReceivedAmount =  getMOamount.ReceivedAmount/>
                        </cfif>
                    </cfif>

                    <cfquery name="getMTamount" datasource="#Session.DBSourceREAD#">
                        SELECT
                            (MT1_dec * #totalSent#) AS SentAmount
                        FROM
                            simplebilling.enterprise_billing_plan
                        WHERE
                            PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#levelId#">
                        AND
                            Volume_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#totalSent#">
                        ORDER BY Volume_int
                        LIMIT 1
                    </cfquery>
                    <cfif getMTamount.RecordCount GT 0>
                        <cfif getMTamount.SentAmount NEQ ''>
                            <cfset SentAmount = getMTamount.SentAmount>
                        </cfif>
                    </cfif>

                </cfif>

                <cfset totalAll = totalAll + SentAmount + ReceivedAmount/>
                <cfset SentAmount = 0>
                <cfset ReceivedAmount = 0>

            </cfloop>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.ENTERPRISEBILLING = #totalAll# />


            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="GetEnterpriseBillingDetail" access="remote" hint="Get Enterprise User billing total">
         <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUsers = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var currentUserCredits = '' />
        <cfset var RetGetBalance = '' />
        <cfset var temp = {} />

        <cfset var getCountMessage = ''/>
        <cfset var getMOamount = ''/>
        <cfset var getMTamount = ''/>

        <cfset var totalSent = 0 />
        <cfset var totalReceived = 0 />
        <cfset var totalMsg = 0/>
        <cfset var SentAmount = 0 />
        <cfset var ReceivedAmount = 0 />
        <cfset var CostPerMO = 0 />
        <cfset var CostPerMT = 0 />
        <cfset var totalAmountuser = 0 />

        <cfset var totalAll = 0 />
        <cfset var levelId = 0 />
        <cfset var crm_userId = 0 />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUsers" datasource="#Session.DBSourceREAD#">

               SELECT
                    SQL_CALC_FOUND_ROWS
                    u.UserId_int,
                    si.PlanId_int,
                    u.FirstName_vch,
                    u.MFAContactString_vch,
                    u.LastName_vch,
                    u.EmailAddress_vch
                FROM
                    simplebilling.enterprise_user_billing_plan as si
                INNER join
                    simpleobjects.useraccount as u
                ON
                    u.UserId_int = si.UserId_int
                WHERE
                    u.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'u.UserId_int'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                GROUP BY
                    u.UserId_int
                ORDER BY
                    u.UserId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllUsers#">
                <cfset levelId = getAllUsers.PlanId_int/>
                <cfset crm_userId = getAllUsers.UserId_int/>
                <!--- Get total Sent and Received MSG --->
                <cfquery name="getCountMessage" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                        SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                    FROM
                        simplexresults.ireresults as ire
                    WHERE
                        ire.IREType_int IN (1,2)
                    AND
                        LENGTH(ire.ContactString_vch) < 14
                    AND
                        ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#crm_userId#">
                    AND
                        ire.Created_dt
                    BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                </cfquery>
                <cfif getCountMessage.RecordCount GT 0>
                    <cfif getCountMessage.Sent NEQ ''>
                        <cfset totalSent = getCountMessage.Sent>
                    </cfif>
                    <cfif getCountMessage.Received NEQ ''>
                        <cfset totalReceived = getCountMessage.Received>
                    </cfif>
                    <cfset totalMsg = totalSent + totalReceived>

                    <cfquery name="getMOamount" datasource="#Session.DBSourceREAD#">
                        SELECT
                            (MO1_dec * #totalReceived#) AS ReceivedAmount,
                            MO1_dec AS CostPerMO
                        FROM
                            simplebilling.enterprise_billing_plan
                        WHERE
                            PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#levelId#">
                        AND
                            Volume_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#totalReceived#">
                        ORDER BY Volume_int
                        LIMIT 1
                    </cfquery>
                    <cfif getMOamount.RecordCount GT 0>
                        <cfif getMOamount.ReceivedAmount NEQ "">
                            <cfset ReceivedAmount =  getMOamount.ReceivedAmount/>
                        </cfif>
                    </cfif>

                    <cfquery name="getMTamount" datasource="#Session.DBSourceREAD#">
                        SELECT
                            (MT1_dec * #totalSent#) AS SentAmount,
                            MT1_dec AS CostPerMT
                        FROM
                            simplebilling.enterprise_billing_plan
                        WHERE
                            PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#levelId#">
                        AND
                            Volume_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#totalSent#">
                        ORDER BY Volume_int
                        LIMIT 1
                    </cfquery>
                    <cfif getMTamount.RecordCount GT 0>
                        <cfif getMTamount.SentAmount NEQ ''>
                            <cfset SentAmount = getMTamount.SentAmount>
                        </cfif>
                    </cfif>
                    <cfset totalAmountuser = SentAmount + ReceivedAmount>
                </cfif>

                <cfif getAllUsers.RecordCount GT 0 And totalAmountuser GT 0>
                    <cfset temp = {
                        USERID = '#getAllUsers.UserId_int#',
                        PHONE = '#getAllUsers.MFAContactString_vch#',
                        NAME = '#getAllUsers.FirstName_vch#' & ' #getAllUsers.LastName_vch#',
                        EMAIL = '#getAllUsers.EmailAddress_vch#',
                        TOTALMESSAGE = '#totalMsg#',
                        TOTALAMOUNT = '#totalAmountuser#',
                        TOTALMOS = '#totalReceived#',
                        TOTALMTS = '#totalSent#',
                        COSTPERMOS = '#getMOamount.CostPerMO#',
                        COSTPERMTS = '#getMTamount.CostPerMT#'
                    }/>
                    <cfset ArrayAppend(dataout["DATALIST"],temp)>
                </cfif>

            </cfloop>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="getTotalMTCountOfThisMonth" access="remote" hint="Get total MT of this month">
        <cfset var todayDate = Now()>
        <cfset var startDate = CreateDate(#DatePart("yyyy", todayDate)#, #DatePart("m", todayDate)#, 01) />
        <cfset var endDate   = CreateDate(#DatePart("yyyy", todayDate)#, #DatePart("m", todayDate)#, #DatePart("d", todayDate)#) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {}/>
        <cfset dataout.RESULT = 0/>
        <cfset dataout.MESSAGE = '' />

        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllMTs = '' />
        <cfset var RetVarGetAdminPermission	= '' />
        <cfset var getTodayMTs = '' />

        <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif>

        <cftry>
            <cfquery name="getAllMTs" datasource="#Session.DBSourceREAD#">

                SELECT
                   SUM(MessageSent_int)  as totalMTs
                From
                    simplexresults.crm_summary
                WHERE
                    date(DateTime_dt) >=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND
                    date(DateTime_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">

            </cfquery>
            <cfquery name="getTodayMTs" datasource="#Session.DBSourceREAD#">

                SELECT
                    count(si.IREResultsId_bi) as todayMTs
                FROM
                    simplexresults.ireresults as si
                WHERE
                    si.Created_dt >=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">
                AND
                    si.IREType_int = 1

            </cfquery>

            <cfset dataout.MESSAGE = "Get MTs successfully"/>
            <cfset dataout.RESULT = #getAllMTs.totalMTs# + #getTodayMTs.todayMTs#/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetCampainListByUserId" access="remote" output="true" hint="get user list with filter">
        <cfargument name="inpCampaignName" TYPE="string" required="yes" default="" />
        <cfargument name="inpUserIds" TYPE="string" required="yes" default="" />
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {} />
        <cfset var GetCampainList = '' />
        <cfset var rsGetCampainList = '' />
        <cfset var displayInfo = '' />

        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset dataout.CampaignList = arrayNew() />
        <cfset var condtionUserIds = '(#arguments.inpUserIds#)'/>

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="GetCampainList" result="rsGetCampainList">
                    SELECT
                        b.BatchId_bi,
                        b.DESC_VCH
                    FROM
                        simpleobjects.batch AS b
                    WHERE
                        (
                            b.DESC_VCH LIKE <CFQUERYPARAM cfsqltype="cf_sql_varchar" value="%#arguments.inpCampaignName#%">
                            OR
                            b.BatchId_bi = <CFQUERYPARAM cfsqltype="cf_sql_varchar" value="#arguments.inpCampaignName#">
                        )
                    AND
                    b.Userid_int IN #condtionUserIds#
                    AND
                    b.Created_dt BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>
            <cfif GetCampainList.RECORDCOUNT GT 0>

                <cfloop query="GetCampainList" >
                    <cfset displayInfo = '#BatchId_bi# - #DESC_VCH#' >
                    <cfset var tmpCampaign = {
                        'ID':'#BatchId_bi#',
                        'NAME':'#DESC_VCH#',
                        'EMAIL':'#displayInfo#'
                    } />
                    <cfset ArrayAppend(dataout.CampaignList, tmpCampaign) />
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout.CampaignList>
    </cffunction>
    <cffunction
        name="getUserSourceSummary"
        access="remote"
        output="false"
        hint="Get user summary by source"
        >

        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />


        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset dataout["DATALIST"] = arrayNew(1)/>

        <cfset dataout.TOTALFREE = 0/>
        <cfset dataout.TOTALPAID = 0/>
        <cfset dataout.TOTALFREENOMFA = 0/>
        <cfset dataout.TOTALFREEYESMFA = 0/>
        <cfset dataout.TOTALFREEBUYCREDITS = 0/>

        <cfset var temp = {}/>
        <cfset var getUser=''>

        <cfset var getTotalNewUsersBySource = ''/>
        <cfset var getTotalNewUsersByAffiliate=''>
        <cfset var getFreeNoMFA = ''/>
        <cfset var getFreeYesMFA = ''/>
        <cfset var getPaid = ''/>
        <cfset var getFree = ''/>
        <cfset var getFreeBuyCredits = ''/>

        <cfset var listAllUser=''>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>


        <cftry>
            <cfquery name="getUser" datasource="#Session.DBSourceREAD#">
                SELECT
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    upper(ua.MarketingSource_vch ) as MarketingSource_vch,
                    ua.MFAEnabled_ti,
                    ua.MFA_Sent_ti,
                    ua.IsAffiliateSource_ti,
                    up.PlanId_int

                FROM
                    simpleobjects.useraccount ua
                LEFT JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int=up.UserId_int
                AND
                    up.Status_int=1
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                    BETWEEN
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                    AND
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>
            <cfset listAllUser = ValueList(getUser.UserId_int)>
            <cfquery name="getTotalNewUsersBySource" dbtype="query">
                SELECT
                    COUNT(*) AS Total,
                    MarketingSource_vch
                FROM
                    getUser
                WHERE
                    IsAffiliateSource_ti=0
                GROUP BY
                    MarketingSource_vch
            </cfquery>
            <cfif getTotalNewUsersBySource.RECORDCOUNT GT 0>
                <cfloop query="getTotalNewUsersBySource">
                    <cfset temp = {
                        MarketingSource = MarketingSource_vch,
                        ISAFFILIATESOURCE=0,
                        Total= Total
                    }/>

                    <cfset ArrayAppend(dataout["DATALIST"],temp)>
                </cfloop>
            </cfif>

            <cfquery name="getTotalNewUsersByAffiliate" dbtype="query">
                SELECT
                    COUNT(*) AS Total
                FROM
                    getUser
                WHERE
                    IsAffiliateSource_ti=1
            </cfquery>
            <cfif getTotalNewUsersByAffiliate.RECORDCOUNT GT 0>
                <cfloop query="getTotalNewUsersByAffiliate">
                <cfset temp = {
                        MarketingSource = "Affiliate",
                        ISAFFILIATESOURCE=1,
                        Total= Total
                    }/>

                    <cfset ArrayAppend(dataout["DATALIST"],temp)>
                </cfloop>
            </cfif>

            <cfquery name="getPaid" dbtype="query">
                SELECT
                    COUNT(*) AS Total
                FROM
                    getUser

                WHERE
                    PlanId_int> 1
            </cfquery>
            <cfif getPaid.RECORDCOUNT GT 0>
                <cfset dataout.TOTALPAID = getPaid.Total/>
            </cfif>

            <cfquery name="getFree" dbtype="query">
                SELECT
                    COUNT(*) AS Total
                FROM
                    getUser

                WHERE
                    PlanId_int = 1
            </cfquery>
            <cfif getFree.RECORDCOUNT GT 0>
                <cfset dataout.TOTALFREE = getFree.Total/>
            </cfif>

            <cfquery name="getFreeBuyCredits" datasource="#Session.DBSourceREAD#">
                SELECT
                    count(DISTINCT(userId_int)) as Total
                FROM
                    simplebilling.payment_worldpay
                WHERE
                    userId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#listAllUser#" list="yes">)
                AND
                    planId =1
                AND
                    (responseText= 'Approved' OR responseText='SUCCESS')

                AND
                    created BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_UserStartDate#">
                    AND
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>
            <cfif getFreeBuyCredits.RECORDCOUNT GT 0>
                <cfset dataout.TOTALFREEBUYCREDITS = getFreeBuyCredits.Total/>
            </cfif>

            <cfquery name="getFreeNoMFA" dbtype="query">
                SELECT
                    COUNT(*) AS Total
                FROM
                    getUser

                WHERE
                    PlanId_int = 1
                AND
                    MFA_Sent_ti=0
            </cfquery>
            <cfif getFreeNoMFA.RECORDCOUNT GT 0>
                <cfset dataout.TOTALFREENOMFA = getFreeNoMFA.Total/>
            </cfif>

            <cfquery name="getFreeYesMFA" dbtype="query">
                SELECT
                    COUNT(*) AS Total
                FROM
                    getUser

                WHERE
                    PlanId_int = 1
                AND
                    MFA_Sent_ti=1
            </cfquery>
            <cfif getFreeYesMFA.RECORDCOUNT GT 0>
                <cfset dataout.TOTALFREEYESMFA = getFreeYesMFA.Total/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="getUserListBySource" access="remote" hint="Get List user by source signup">

        <cfargument name="inpSource" type="string" required="true" hint="Source signup"/>
        <cfargument name="inpIsAffiliateSource" type="string" required="false" default="0" hint="Affiliate Source or not"/>

        <cfargument name="inpStartDate" type="string" required="true" hint="Start date"/>
        <cfargument name="inpEndDate" type="string" required="true" hint="End date"/>

        <cfargument name="customFilter" default=""/>
        <cfargument name="inpSkipPaging" default="0"/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />


        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = arrayNew(1)/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUsers = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />

        <cfset var temp = {} />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllUsers" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch

                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                <cfif arguments.inpIsAffiliateSource EQ 1>
                AND
                    ua.IsAffiliateSource_ti=1

                <cfelse>
                AND
                    ua.MarketingSource_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSource#">
                AND
                    ua.IsAffiliateSource_ti=0
                </cfif>
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>

            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllUsers">
                <cfset var temp = {
                    USERID = '#UserId_int#',
                    USEREMAIL =  '#EmailAddress_vch#',
                    REGISTEREDDATE = '#DateFormat(Created_dt, "yyyy-mm-dd")#',
                    USERNAME = "#FirstName_vch# #LastName_vch#",
                    FIRSTNAME = "#FirstName_vch#",
                    LASTNAME = "#LastName_vch#",
                    LASTLOGIN = "#DateFormat(LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(LastLogIn_dt, "HH:mm:ss")#",
                    STATUS = "#Active_int#",
                    CONTACTSTRING = "#MFAContactString_vch#",
                    SOURCE= MarketingSource_vch
                } />
                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="getUserListByType" access="remote" hint="Get List By Type">

        <cfargument name="inpType" type="string" required="true" hint="User Type"/>
        <cfargument name="inpStartDate" type="string" required="true" hint="Start date"/>
        <cfargument name="inpEndDate" type="string" required="true" hint="End date"/>

        <cfargument name="customFilter" default=""/>
        <cfargument name="inpSkipPaging" default="0"/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />


        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = arrayNew(1)/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getUser = '' />
        <cfset var getList=''>

        <cfset var getFreeNoMFA = ''/>
        <cfset var getFreeYesMFA = ''/>
        <cfset var getPaid = ''/>
        <cfset var getFree = ''/>
        <cfset var getFreeBuyCredits = ''/>

        <cfset var rsCount = ''/>
        <cfset var filterItem = '' />

        <cfset var temp = {} />
        <cfset var listAllUser=''>
        <cfset var listFilterUser=''>

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getUser" datasource="#Session.DBSourceREAD#">
                SELECT
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.MFAEnabled_ti,
                    ua.MFA_Sent_ti,
                    up.PlanId_int
                FROM
                    simpleobjects.useraccount ua
                LEFT JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int=up.UserId_int
                AND
                    up.Status_int=1
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC

                <!---LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"> --->
            </cfquery>

            <cfset listAllUser = ValueList(getUser.UserId_int)>
            <cfswitch expression="#arguments.inpType#">
                <cfcase value="paid">
                    <cfquery name="getPaid" dbtype="query">
                        SELECT
                            UserId_int
                        FROM
                            getUser

                        WHERE
                            PlanId_int> 1
                    </cfquery>
                    <cfset listFilterUser= ValueList(getPaid.UserId_int)>
                </cfcase>
                <cfcase value="free">
                     <cfquery name="getFree" dbtype="query">
                        SELECT
                            UserId_int
                        FROM
                            getUser

                        WHERE
                            PlanId_int = 1
                    </cfquery>
                    <cfset listFilterUser= ValueList(getFree.UserId_int)>
                </cfcase>
                <cfcase value="freenomfa">
                    <cfquery name="getFreeNoMFA" dbtype="query">
                        SELECT
                            UserId_int
                        FROM
                            getUser

                        WHERE
                            PlanId_int = 1
                        AND
                            MFA_Sent_ti=0
                    </cfquery>
                    <cfset listFilterUser= ValueList(getFreeNoMFA.UserId_int)>

                </cfcase>
                <cfcase value="freeyesmfa">
                    <cfquery name="getFreeYesMFA" dbtype="query">
                        SELECT
                            UserId_int
                        FROM
                            getUser

                        WHERE
                            PlanId_int = 1
                        AND
                            MFA_Sent_ti=1
                    </cfquery>

                    <cfset listFilterUser= ValueList(getFreeYesMFA.UserId_int)>
                </cfcase>
                <cfcase value="freebuycredits">
                    <cfquery name="getFreeBuyCredits" datasource="#Session.DBSourceREAD#">
                        SELECT
                            DISTINCT(userId_int)
                        FROM
                            simplebilling.payment_worldpay
                        WHERE
                            userId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#listAllUser#" list="yes">)
                        AND
                            planId =1
                        AND
                            (responseText= 'Approved' OR responseText='SUCCESS')

                        AND
                            created BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_UserStartDate#">
                            AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    </cfquery>
                    <cfset listFilterUser= ValueList(getFreeBuyCredits.UserId_int)>

                </cfcase>
                <cfdefaultcase>
                </cfdefaultcase>
            </cfswitch>

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.MarketingSource_vch
                FROM
                    simpleobjects.useraccount ua
                WHERE
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0
                AND
                    UserId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#listFilterUser#" list="yes">)
                AND
                    ua.Created_dt
                        BETWEEN
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserStartDate#">
                        AND
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>


            <cfloop query="getList">
                <cfset var temp = {
                    USERID = '#UserId_int#',
                    USEREMAIL =  '#EmailAddress_vch#',
                    REGISTEREDDATE = '#DateFormat(Created_dt, "yyyy-mm-dd")#',
                    USERNAME = "#FirstName_vch# #LastName_vch#",
                    FIRSTNAME = "#FirstName_vch#",
                    LASTNAME = "#LastName_vch#",
                    LASTLOGIN = "#DateFormat(LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(LastLogIn_dt, "HH:mm:ss")#",
                    STATUS = "#Active_int#",
                    CONTACTSTRING = "#MFAContactString_vch#",
                    SOURCE= MarketingSource_vch
                } />
                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

</cfcomponent>
