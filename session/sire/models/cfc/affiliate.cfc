<cfcomponent>
	<cfinclude template="../../../paths.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    
	<cffunction name="GetffiliateList" access="remote" output="false" hint="Get list of affiliate">	
		<cfargument name="inpUserId" TYPE="string" required="no" default="" />
		<cfargument name="customFilter" default="">

		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		
		<cfset var GetListData="">
		<cfset var item="">
		<cfset var rsCount="">
		<cfset var filterData="">
		<cfset var filterItem="">
		<cfset var RetVarGetAdminPermission="">

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'Id_int' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'Id_int' />
		    </cfcase>		    
		    <cfcase value="2">
		        <cfset orderField = 'Id_int' />
		    </cfcase>	
		</cfswitch>

		<cftry>					
            <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfset filterData = DeserializeJSON(arguments.customFilter) />
			<cfquery name="GetListData" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					Id_int,				  	
				  	UserId_int,				  	
                    Affiliate_code_vch,
                    CreateDate_dt,
                    CreateBy_int,
                    Status_ti			
				FROM 
					simpleobjects.affiliate_code_assignment
				WHERE
					1
                <cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR# 
						<cfif filterItem.TYPE EQ 'CF_SQL_VARCHAR'>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'/>						
						<cfelse>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'/>
						</cfif>
					</cfloop>
				</cfif>				
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Id_int  DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListData.RecordCount GT 0>
				<cfloop query="GetListData">
					<cfset item = {
						"ID": Id_int, 
						"AffiliateCode":Affiliate_code_vch,
						"UserID":UserId_int,
						"CreateDate":DATEFORMAT(CreateDate_dt,'mm/dd/yyyy'),
                        "CreateBy":CreateBy_int
												
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="GetAffiliateCode" access="remote" output="false" hint="Get affiliate by userid">	
		<cfargument name="inpUserId" TYPE="string" required="no" default="#Session.UserID#" />
		
				
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />		
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	
        <cfset 	dataout.AFFILIATECODE = '' />	
		<cfset 	dataout.AFFILIATEID=0>
		
		<cfset var GetData="">		
		<cftry>					
            
			<cfquery name="GetData" datasource="#Session.DBSourceREAD#">
				SELECT 				
					Id_int,			  	
                    Affiliate_code_vch                    		
				FROM 
					simpleobjects.affiliate_code_assignment
				WHERE
					UserId_int=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>				
			</cfquery>	
			<cfset 	dataout.AFFILIATEID = GetData.Id_int/>					
            <cfset 	dataout.AFFILIATECODE = GetData.Affiliate_code_vch/>	
			<cfset  dataout.RXRESULTCODE = 1 /> 

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>	
	<cffunction name="GetAppliedAffiliateCodeByUserID" access="remote" output="false" hint="Get Applied Affiliate code by userid">	
		<cfargument name="inpUserId" TYPE="string" required="no" default="#Session.UserID#" />
		
				
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />		
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	
        <cfset 	dataout.AFFILIATECODE = '' />	
		
		<cfset var GetData="">		
		<cftry>					
            
			<cfquery name="GetData" datasource="#Session.DBSourceREAD#">
				SELECT 							  	
                    amc_vch                    		
				FROM 
					simpleobjects.useraccount
				WHERE
					UserId_int=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>				
			</cfquery>	
							
            <cfset 	dataout.AFFILIATECODE = GetData.amc_vch/>	
			<cfset  dataout.RXRESULTCODE = 1 /> 

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>		

	<cffunction name="SaveAffiliate" access="remote" output="false" hint="Save affiliate">
		<cfargument name="inpId" TYPE="string" required="no" default="0" />					
		<cfargument name="inpUserId" TYPE="string" required="no" default="#Session.USERID#" />		
		<cfargument name="inpAffiliateCode" TYPE="string" required="no" default="" />		
        <cfargument name="inpCreateBy" TYPE="string" required="no" default="#Session.USERID#" />		
		 <!--- plz using DBSourceEBM for all query inside because this function wwas call from a transaction from create new accout --->
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		<cfset var UpdateAccountData=''/>
		
		<cfset var CheckAffiliateExist=''/>
		<cfset var CheckUserExistAffiliate=''/>
		<cfset var UpdateUserAccount=''/>
		<cfset var CheckUserEligible=''/>
		<cfset var mailData={}/>
		<cfset var mailTo=""/>
		<cfset var getUserInfo=""/>
        <cftry>			
			<cfif arguments.inpId EQ 0>
				<cfquery name="CheckUserExistAffiliate" datasource="#Session.DBSourceEBM#">
					SELECT 
						Count(*) as Total		
					FROM 
						simpleobjects.affiliate_code_assignment
					WHERE
						UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
					AND
						Id_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>					
				</cfquery>
				<cfif CheckUserExistAffiliate.Total GT 0>
					<cfthrow type="Exists" message="This user have already assigned affiliate code" detail="This user have already assigned affiliate code"/>
				</cfif>
			</cfif>
			<cfquery name="CheckAffiliateExist" datasource="#Session.DBSourceEBM#">
				SELECT 
					Count(*) as Total		
				FROM 
					simpleobjects.affiliate_code_assignment
				WHERE
					Affiliate_code_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpAffiliateCode)#"/>
				AND
					Id_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>		
			</cfquery>
			<cfif CheckAffiliateExist.Total GT 0>
				<cfthrow type="Application" message="Affiliate code (#arguments.inpAffiliateCode#) have already assigned, please try an others one" detail="Affiliate code (#arguments.inpAffiliateCode#) have already assigned, please try an others one"/>
			</cfif>			

			<cfif arguments.inpId GT 0>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					UPDATE
						simpleobjects.affiliate_code_assignment
					SET
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,						
						Affiliate_code_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAffiliateCode#"/>		
					WHERE
						Id_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>											
				</cfquery>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateAccountData">
					UPDATE
						simpleobjects.useraccount
					SET						
						amc_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAffiliateCode#"/>		
					WHERE
						ami_int in(
								SELECT 	UserId_int
								FROM	simpleobjects.affiliate_code_assignment
								WHERE	Id_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>											
						)	
						
				</cfquery>
			<cfelse>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					INSERT INTO
						simpleobjects.affiliate_code_assignment
						(																								
							UserId_int,							
							Affiliate_code_vch,
							CreateDate_dt,
							CreateBy_int,
							Status_ti						
						)
					VALUES
						(							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,													
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAffiliateCode#"/>,																		
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCreateBy#"/>,						
							1							
						)
				</cfquery>
				<cfquery name="getUserInfo" datasource="#Session.DBSourceEBM#">
					SELECT
						EmailAddress_vch,
						SSN_TaxId_vch,
						HomePhoneStr_vch,
						Address1_vch,
						FirstName_vch										
					FROM
						simpleobjects.useraccount
					WHERE                
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
					AND
						Active_int > 0       
				</cfquery>  

				<cfset mailTo=getUserInfo.EmailAddress_vch/>
				
				<cfset mailData.USERNAME=getUserInfo.FirstName_vch/>
				<cfset mailData.NAME=getUserInfo.FirstName_vch/>
				<cfset mailData.ADDRESS=getUserInfo.Address1_vch/>
				<cfset mailData.EMAIL=getUserInfo.EmailAddress_vch/>
				<cfset mailData.PHONE=getUserInfo.HomePhoneStr_vch/>
				<cfset mailData.TAXID=LEFT(getUserInfo.SSN_TaxId_vch,3) & "******"/>
				<cftry>			
					<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
						<cfinvokeargument name="to" value="#mailTo#">
						<cfinvokeargument name="type" value="html">
						<cfinvokeargument name="subject" value="Welcome to the Sire Affiliate Program">
						<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_registration_affiliate.cfm">
						<cfinvokeargument name="data" value="#SerializeJSON(mailData)#">
					</cfinvoke>
				<cfcatch></cfcatch>
				</cftry>
			</cfif>
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="GetMasterInfo" access="remote" output="false" hint="Get master information by affiliate code">	
        <cfargument name="inpAffiliateCode" TYPE="string" required="yes" default="" />				
        <cfset var dataout = StructNew()/>
        <cfset dataout.RXRESULTCODE = 0 />		
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />		
        <cfset dataout.AFFILIATEINFO = '' />
		<cfset dataout.MASTERUSERID = '' />		

        <cfset var GetData="">
        <cftry>
            <cfquery name="GetData" datasource="#Session.DBSourceREAD#">
                SELECT
                    m.UserId_int,                        
                    u.EmailAddress_vch,
					ami_int
                FROM
                    simpleobjects.affiliate_code_assignment m
                INNER JOIN
                    simpleobjects.useraccount u
                    ON	m.UserId_int=u.UserId_int					
                WHERE
                    m.Affiliate_code_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpAffiliateCode)#"/>			  
                    AND	u.Active_int=1
            </cfquery>
			<cfif GetData.RECORDCOUNT GT 0>
				<cfset dataout.AFFILIATEINFO = GetData />
				<cfset dataout.MASTERUSERID = GetData.UserId_int />
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>
            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>
	<cffunction name="GetPaymentAll" access="remote" hint="Get all payment">
		<cfargument name="inpStartDate" type="string" required="true" hint=""/>
		<cfargument name="inpEndDate" type="string" required="true" hint=""/>
		<cfargument name="inpAMI" type="string" required="true" hint=""/>

		<cfargument name="customFilter" default=""/>

		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
		<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
		<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
		
		<cfset var dataout = {}/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset var GetUserList= ''>
		<cfset var GetPaymentData= ''>
		<cfset var amount=0>
		
		<cfset var listUser = ArrayNew(1)/>	
		<cfset var dataout["DATAPAYMENT"] = ArrayNew(1)>
		<cfset var tmpData={}>
		<cftry>			
			<cfquery name="GetUserList" datasource="#Session.DBSourceREAD#">
				SELECT  
					u.UserId_int				
				FROM 					
					simpleobjects.useraccount u											
				WHERE
					u.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAMI#"> 
				AND
					u.IsTestAccount_ti=0
			</cfquery>
			
			<cfloop query="GetUserList">
				<cfset arrayAppend(listUser, UserId_int)/>
			</cfloop>
			<cfset dataout.LISTUSER = listUser />
			<cfquery name="GetPaymentData" datasource="#Session.DBSourceREAD#">
				SELECT 
					userId_int,
					orderId,
					transactionId,
					transactionData_amount,
					moduleName,
					paymentGateway_ti
				FROM 
					simplebilling.payment_worldpay p
				WHERE
					userId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arraytolist(listUser)#" list="yes">)
				AND
				(
					planId > 1 
					OR
					(byNumberKeyword=0 AND byNumberSMS=0 AND planId=1)
				)
				AND
					(responseText= 'Approved' OR responseText='SUCCESS')
				AND
					created BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
					AND 
					DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)			
				AND
                (
					(
						Month(Date(created)) >= (
							SELECT
								Month(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
						AND
						Year(Date(created)) = (
							SELECT
								Year(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
					)
					OR
					(
						Year(Date(created)) > (
							SELECT
								Year(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
                	)
				)	
			</cfquery>
			
			<cfloop query="GetPaymentData">
				<cfset amount = transactionData_amount>
				<cfset tmpData={
					UserID=userId_int,
					Value=amount,
					Notes= moduleName,
					TransactionId=transactionId,
					OrderId=orderId,
					Type=paymentGateway_ti
				}>
				<cfset ArrayAppend(dataout["DATAPAYMENT"], tmpData) />
			</cfloop>			
			
			<cfset dataout.RXRESULTCODE = 1 />
			

			
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.RXRESULTCODE = -1 />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="GetCommissionFromStored" access="remote" hint="Get commission from stored">
		<cfargument name="inpStartDate" type="string" required="true" hint=""/>
		<cfargument name="inpEndDate" type="string" required="true" hint=""/>
		<cfargument name="inpAMI" type="string" required="true" hint=""/>

		<cfargument name="customFilter" default=""/>

		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
		<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
		<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
		
		<cfset var dataout = {}/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TOTALEARNED_BYDATE =0/>
		<cfset dataout.TOTALREVENUE_BYDATE =0/>
		
		<cfset var GetUserList= ''>
		<cfset var GetPaymentData= ''>
		<cfset var amount=0>
		
		<cfset var listUser = ArrayNew(1)/>	
		<cfset var dataout["DATAPAYMENT"] = ArrayNew(1)>
		<cfset var tmpData={}>
		<cftry>			
			<cfquery name="GetUserList" datasource="#Session.DBSourceREAD#">
				SELECT  
					u.UserId_int				
				FROM 					
					simpleobjects.useraccount u											
				WHERE
					u.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAMI#"> 
				AND
					u.IsTestAccount_ti=0
			</cfquery>
			
			<cfloop query="GetUserList">
				<cfset arrayAppend(listUser, UserId_int)/>
			</cfloop>
			<cfset dataout.LISTUSER = listUser />
			<cfquery name="GetPaymentData" datasource="#Session.DBSourceREAD#">
				SELECT 					
					acbp.ReferredPercent_dec,
					acbpd.Value_dec					
				FROM 
					simplebilling.affiliate_commission_by_periods acbp
				INNER JOIN 
					simplebilling.affiliate_commission_by_periods_detail acbpd
				ON
					acbp.Id_bi= acbpd.Commission_int				
				WHERE
					acbpd.UserID_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arraytolist(listUser)#" list="yes">)
				
				AND
					acbpd.TransactionDate_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
					AND 
					DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)			
				
			</cfquery>
			
			<cfloop query="GetPaymentData">						
				<cfset dataout.TOTALREVENUE_BYDATE += Value_dec />
				<cfset dataout.TOTALEARNED_BYDATE += Value_dec*ReferredPercent_dec/100 />				
			</cfloop>						
			<cfset dataout.RXRESULTCODE = 1 />			
			
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.RXRESULTCODE = -1 />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="GetCommissionByUserIdByPeriods" access="remote" hint="Get commission by userid and periods">
		<cfargument name="inpStartDate" type="string" required="true" hint=""/>
		<cfargument name="inpEndDate" type="string" required="true" hint=""/>
		<cfargument name="inpAMI" type="string" required="true" hint=""/>		
		
		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
		
		<cfset var dataout = {}/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />		
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TOTALEARNED_BYDATE =0/>
		<cfset dataout.TOTALREVENUE_BYDATE =0/>

		<cfset var ListUserPaid = ArrayNew(1)>
		<cfset var index=''>
		<cfset var data=''>
		<cfset var TotalCommission=0>
		<cfset var TotalPaymentOfReferred=0>
		<cfset var TotalUserPaid=0>
		<cfset var TotalPaymentOfReferred=0>
		<cfset var ReferredPercent=0>
		<cfset var CommissionSetting=''>
		<cfset var GetPaymentPlan=''>
		<cfset var GetLastDateCommissionStored=''>
		<cfset var MaxDateStored= {}>		
		<cfset var DateTemplate=''>		
		<cfset var MiddleDate= {}>		
		<cfset var AfterMiddleDateOneDay= {}>		
		<cfset var rtGetCommissionFromStored=''>
		<cfset var rtGetTotalActiveReferrals=''>

		<cftry>					
				
			<cfquery name="GetLastDateCommissionStored" datasource="#Session.DBSourceREAD#">
				SELECT MAX(ToDate_dt) AS MaxDateStored
				FROM
					 simplebilling.affiliate_commission_by_periods
				WHERE
					UserID_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAMI#"> 				
			</cfquery>
			
			<cfif GetLastDateCommissionStored.MaxDateStored NEQ "">
				<cfset MaxDateStored={
					day: Dateformat(GetLastDateCommissionStored.MaxDateStored,"dd"),
					month: Dateformat(GetLastDateCommissionStored.MaxDateStored,"mm"),
					year: Dateformat(GetLastDateCommissionStored.MaxDateStored,"yyyy"),
				}>	
			<cfelse>
				<cfset MaxDateStored=inpStartDate>			
			</cfif>

			<cfif DateCompare(Createdate(inpEndDate.year,inpEndDate.month,inpEndDate.day), Createdate(MaxDateStored.year,MaxDateStored.month,MaxDateStored.day)) EQ 1>
				<cfset MiddleDate= MaxDateStored>	
			<cfelse>
				<cfset MiddleDate= inpEndDate>	
			</cfif>
			<cfset DateTemplate= DateAdd("d", 1, Createdate(MiddleDate.year,MiddleDate.month,MiddleDate.day) )>		
			<cfif DateCompare(Createdate(inpStartDate.year,inpStartDate.month,inpStartDate.day),DateTemplate ) EQ 1>
				<cfset AfterMiddleDateOneDay=inpStartDate>
			<cfelse>
				<cfset AfterMiddleDateOneDay={
						day: Dateformat(DateTemplate,"dd"),
						month: Dateformat(DateTemplate,"mm"),
						year: Dateformat(DateTemplate,"yyyy"),
				}>
			</cfif>
			
			
			<!--- commission still MiddleDate--->
			<cfinvoke  method="GetCommissionFromStored" returnvariable="rtGetCommissionFromStored">
				<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(inpStartDate)#">
				<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(MiddleDate)#">
				<cfinvokeargument  name="inpAMI"  value="#arguments.inpAMI#">				
			</cfinvoke>
			
			<cfif rtGetCommissionFromStored.RXRESULTCODE EQ 1>
				<cfset dataout.TOTALEARNED_BYDATE = Numberformat(rtGetCommissionFromStored.TOTALEARNED_BYDATE,'.99')/>
				<cfset dataout.TOTALREVENUE_BYDATE = Numberformat(rtGetCommissionFromStored.TOTALREVENUE_BYDATE,'.99')/>
			</cfif>
			
			<!--- commission real time calc from MiddleDate to end date--->
			<cfinvoke  method="GetPaymentAll" returnvariable="GetPaymentPlan">
				<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(AfterMiddleDateOneDay)#">
				<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(inpEndDate)#">
				<cfinvokeargument  name="inpAMI"  value="#arguments.inpAMI#">				
			</cfinvoke>
			
			<cfif GetPaymentPlan.RXRESULTCODE EQ 1>								
				<cfloop array="#GetPaymentPlan.DATAPAYMENT#" item="data">
					<cfset TotalPaymentOfReferred+=data.VALUE>		
					<cfif data.USERID GT 0 AND ! ArrayFindNoCase(ListUserPaid,data.USERID)>
						<cfset arrayAppend(ListUserPaid,data.USERID) />
					</cfif>
				</cfloop>		
			</cfif>	
			<cfset TotalUserPaid= arrayLen(ListUserPaid)>
			<cfinvoke  method="GetTotalActiveReferrals" returnvariable="rtGetTotalActiveReferrals">				
				<cfinvokeargument  name="inpUserId"  value="#arguments.inpAMI#">				
			</cfinvoke>	
			
			<cfset TotalUserPaid=rtGetTotalActiveReferrals.TOTAL_USER>
			<cfinvoke  method="GetCommissionSetting" returnvariable="CommissionSetting">
				<cfinvokeargument  name="inpUserId"  value="#arguments.inpAMI#">				
			</cfinvoke>			
			<cfif CommissionSetting.RXRESULTCODE EQ 1>
				<cfloop index = "index" array = "#CommissionSetting.SETTING#" >
					<cfif index.VALUE LTE TotalUserPaid AND index.CONDITION EQ 1>
						<cfset ReferredPercent=index.PERCENT>					
					<cfelseif index.VALUE GT TotalUserPaid AND index.CONDITION EQ 0>
						<cfset ReferredPercent=index.PERCENT>					
						 <cfbreak>
					</cfif>					
				</cfloop>
			</cfif>
			<cfset TotalCommission= NumberFormat(TotalPaymentOfReferred*ReferredPercent/100,".99")>
			<cfset dataout.TOTALEARNED_BYDATE += Numberformat(TotalCommission,'.99')/>
			<cfset dataout.TOTALREVENUE_BYDATE += Numberformat(TotalPaymentOfReferred,'.99')/>
			
			<cfset dataout.RXRESULTCODE = 1 />
			
			
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.RXRESULTCODE = -1 />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="GetCommissionByListUserIdByPeriods" access="remote" hint="Get Commission By ListUserIds and ByPeriods">
		<cfargument name="inpStartDate" type="string" required="true" hint=""/>
		<cfargument name="inpEndDate" type="string" required="true" hint=""/>
		<cfargument name="inpListUserId" type="string" required="true" hint=""/>

		<cfargument name="customFilter" default=""/>

		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
		<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
		<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
		
		<cfset var dataout = {}/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset var GetUserList= ''>
		<cfset var GetPaymentData= ''>
		<cfset var amount=0>
		
		<cfset var listUser = ArrayNew(1)/>	
		<cfset var dataout["DATAPAYMENT"] = ArrayNew(1)>
		<cfset var tmpData={}>

		<cfset var ListUserPaid = ArrayNew(1)>
		<cfset var index=''>
		<cfset var data=''>
		<cfset var TotalCommission=0>
		<cfset var TotalPaymentOfReferred=0>
		<cfset var TotalUserPaid=0>
		<cfset var TotalPaymentOfReferred=0>
		<cfset var ReferredPercent=0>
		<cfset var CommissionSetting=''>
		<cfset var GetPaymentPlan=''>

		<cftry>			
			<cfset listUser = listToArray(arguments.inpListUserId)>
			<cfset dataout.LISTUSER = listUser />
			<cfquery name="GetPaymentData" datasource="#Session.DBSourceREAD#">
				SELECT 
					userId_int,
					orderId,
					transactionId,
					transactionData_amount,
					moduleName,
					paymentGateway_ti
				FROM 
					simplebilling.payment_worldpay p
				WHERE
					userId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arraytolist(listUser)#" list="yes">)
				AND
				(
					planId > 1 
					OR
					(byNumberKeyword=0 AND byNumberSMS=0 AND planId=1)
				)
				AND
					(responseText= 'Approved' OR responseText='SUCCESS')
				AND
					created BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
					AND 
					DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)			
				AND
                (
					(
						Month(Date(created)) >= (
							SELECT
								Month(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
						AND
						Year(Date(created)) = (
							SELECT
								Year(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
					)
					OR
					(
						Year(Date(created)) > (
							SELECT
								Year(amd_dt)
							FROM 					
								simpleobjects.useraccount
							WHERE
								UserId_int=p.userId_int
						)
                	)
				)	
			</cfquery>
			
			<cfloop query="GetPaymentData">
				<cfset amount = transactionData_amount>
				<cfset tmpData={
					UserID=userId_int,
					Value=amount,
					Notes= moduleName,
					TransactionId=transactionId,
					OrderId=orderId,
					Type=paymentGateway_ti
				}>
				<cfset ArrayAppend(dataout["DATAPAYMENT"], tmpData) />
			</cfloop>		
			
			<cfloop array="#dataout["DATAPAYMENT"]#" item="data">
				<cfset TotalPaymentOfReferred+=data.VALUE>		
				<cfif data.USERID GT 0 AND ! ArrayFindNoCase(ListUserPaid,data.USERID)>
					<cfset arrayAppend(ListUserPaid,data.USERID) />
				</cfif>
			</cfloop>		
			
			<cfset TotalUserPaid= arrayLen(ListUserPaid)>
			<cfinvoke  method="GetCommissionSetting" returnvariable="CommissionSetting">
				<cfinvokeargument  name="inpUserId"  value="#Session.UserId#">				
			</cfinvoke>			
			<cfif CommissionSetting.RXRESULTCODE EQ 1>
				<cfloop index = "index" array = "#CommissionSetting.SETTING#" >
					<cfif index.VALUE LTE TotalUserPaid AND index.CONDITION EQ 1>
						<cfset ReferredPercent=index.PERCENT>					
					<cfelseif index.VALUE GT TotalUserPaid AND index.CONDITION EQ 0>
						<cfset ReferredPercent=index.PERCENT>					
						 <cfbreak>
					</cfif>					
				</cfloop>
			</cfif>
			<cfset TotalCommission= NumberFormat(TotalPaymentOfReferred*ReferredPercent/100,".99")>
			<cfset dataout.TOTALEARNED_BYDATE = Numberformat(TotalCommission,'.99')/>
			<cfset dataout.TOTALREVENUE_BYDATE = Numberformat(TotalPaymentOfReferred,'.99')/>

			
			<cfset dataout.RXRESULTCODE = 1 />
			

			
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.RXRESULTCODE = -1 />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
    <cffunction name="AffiliateOverView" access="remote" output="true" hint="Get over view of affiliate">
		<cfargument name="inpType" type="string" default="" required="false" >  
		<cfargument name="inpUserId" type="numeric" default="#Session.USERID#" required="false" >    		    		
		<cfargument name="inpListUserId" type="string" default="" required="false" >    
		<cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">	     
		
						    

	    <cfset var dataout = {} />
	    <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cfset dataout.TOTALREVENUE = 0/>
		<cfset dataout.TOTALEARNED = 0/>

		<cfset dataout.TOTALEARNED_BYDATE =0/>
		<cfset dataout.TOTALREVENUE_BYDATE =0/>

		<cfset dataout.TOTALAFFILIATE= 0/>
		<cfset dataout.TOTALNEWAFFILIATE = 0/>

        <cfset dataout.TOTALREFERRED = 0/>
		<cfset dataout.TOTALNEWREFERRED = 0/>
        <cfset dataout.FREEREFERRED = 0/>
        <cfset dataout.PAIDREFERRED = 0/>		
		<!--- lisst--->
		<cfset dataout.LIST_TOTALAFFILIATE= 0/>
		<cfset dataout.LIST_TOTALNEWAFFILIATE = 0/>
		<cfset dataout.LIST_TOTALREFERRED = ''/>
		<cfset dataout.LIST_TOTALNEWREFERRED = 0/>
        <cfset dataout.LIST_FREEREFERRED = ''/>
        <cfset dataout.LIST_PAIDREFERRED = ''/>
		

        <cfset dataout.ERRMESSAGE = 0/>        	   

	    
		<cfset var GetTotalUserTillDate = ''/>	 
        <cfset var GetTotalUser = ''/>	 
		<cfset var getUserUpgrades = ''/>	 
		<cfset var getUserDowngrades = ''/>	
		<cfset var GetTotalEarned = ''/>	
		<cfset var RetVarGetAdminPermission	= '' />		
		 
		<cfset var GetTotalAffiliate = ''/>	
		<cfset var GetTotalAffiliateNew = ''/>	
		<cfset var GetPaymentPlan=''>						

		<cfset var amiList=''>
        <cfset var rtGetCommissionByUserIdByPeriods=''>
		<cfset var rtGetCommissionByListUserIdByPeriods=''>
		<cfset var ami	= '' />

	    <cftry>	    
			<cfif inpType EQ "admin" OR inpType EQ "report">
				<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>    
				<cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
					<cfset dataout.RESULT = -1>
					<cfset dataout.MESSAGE = "No Admin Permission."/>
					<cfreturn dataout>
				</cfif>
			</cfif>
			<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
			<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />

			<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
			<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />	

			<cfif inpType EQ "admin">
				<cfset dataout.TOTALAFFILIATE= 0/>
				<cfset dataout.TOTALNEWAFFILIATE = 0/>
				<cfquery name="GetTotalAffiliate" datasource="#Session.DBSourceREAD#">
					SELECT
						aca.UserId_int,
						aca.CreateDate_dt               
					FROM
						simpleobjects.affiliate_code_assignment aca		
					INNER JOIN
						simpleobjects.useraccount as ua
						ON	aca.UserId_int= ua.UserId_int
					WHERE
						aca.Status_ti=1
					AND	
						ua.IsTestAccount_ti = 0	
				</cfquery>
				<cfloop query="GetTotalAffiliate">
					<!--- total--->
					<cfset dataout.TOTALAFFILIATE +=  1> 
					<cfset dataout.LIST_TOTALAFFILIATE =  listAppend(dataout.LIST_TOTALAFFILIATE, UserId_int)>
				</cfloop>	
				<cfquery name="GetTotalAffiliateNew" datasource="#Session.DBSourceREAD#">
					SELECT
						aca.UserId_int,
						aca.CreateDate_dt               
					FROM
						simpleobjects.affiliate_code_assignment	aca	
					INNER JOIN
						simpleobjects.useraccount as ua
						ON	aca.UserId_int= ua.UserId_int
					WHERE		
						Status_ti=1
					AND	
						ua.IsTestAccount_ti = 0	
					AND			
						CreateDate_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
						AND 
						DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
				</cfquery>
				<cfloop query="GetTotalAffiliateNew">
					<!--- total--->
					<cfset dataout.TOTALNEWAFFILIATE +=  1> 
					<cfset dataout.LIST_TOTALNEWAFFILIATE =  listAppend(dataout.LIST_TOTALNEWAFFILIATE, UserId_int)> 							
				</cfloop>

			</cfif>	   

			<cfquery name="GetTotalEarned" datasource="#Session.DBSourceREAD#">
                SELECT
                    Sum(TotalCommission_dec) as total,
					sum(TotalPaymentOfReferred_dec) as total_revenue                  
                FROM
                    simplebilling.affiliate_commission_by_periods as a                
                WHERE
				1                    
				<cfif inpType EQ "admin">				
                    
				<cfelseif inpListUserId NEQ "">                
                	AND    UserID_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>				
                	AND    UserID_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>                 
				</cfif>
					
            </cfquery> 
			<cfset dataout.TOTALEARNED = Numberformat(GetTotalEarned.total,'.99')/>
			<cfset dataout.TOTALREVENUE = Numberformat(GetTotalEarned.total_revenue,'.99')/>
			<cfif inpType EQ "report" and inpListUserId NEQ "">				
				<cfinvoke  method="GetCommissionByListUserIdByPeriods" returnvariable="rtGetCommissionByListUserIdByPeriods">
					<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(inpStartDate)#">
					<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(inpEndDate)#">
					<cfinvokeargument  name="inpListUserId"  value="#arguments.inpListUserId#">				
				</cfinvoke>
				<cfif rtGetCommissionByListUserIdByPeriods.RXRESULTCODE EQ 1>
					<cfset dataout.TOTALEARNED_BYDATE =rtGetCommissionByListUserIdByPeriods.TOTALEARNED_BYDATE/>
					<cfset dataout.TOTALREVENUE_BYDATE =rtGetCommissionByListUserIdByPeriods.TOTALREVENUE_BYDATE/>
				</cfif> 				
			<cfelse>
				<cfif inpType EQ "admin">				
					<cfset amiList= dataout.LIST_TOTALAFFILIATE>				
				<cfelse>				
					<cfset amiList= arguments.inpUserId>    
				</cfif>
				<cfloop list="#amiList#" delimiters="," index="ami">
					<cfif ami GT 0>
						<cfinvoke  method="GetCommissionByUserIdByPeriods" returnvariable="rtGetCommissionByUserIdByPeriods">
							<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(inpStartDate)#">
							<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(inpEndDate)#">
							<cfinvokeargument  name="inpAMI"  value="#ami#">				
						</cfinvoke>
						
						<cfif rtGetCommissionByUserIdByPeriods.RXRESULTCODE EQ 1>
							<cfset dataout.TOTALEARNED_BYDATE +=rtGetCommissionByUserIdByPeriods.TOTALEARNED_BYDATE/>
							<cfset dataout.TOTALREVENUE_BYDATE +=rtGetCommissionByUserIdByPeriods.TOTALREVENUE_BYDATE/>
						</cfif>  
					</cfif>  
				</cfloop>
			</cfif> 				
			
						
			<cfquery name="GetTotalUserTillDate" datasource="#Session.DBSourceREAD#">
                SELECT
                    count(ua.UserId_int) AS Total,   
					ua.UserId_int,                 
                    up.PlanId_int,
					ua.amd_dt                    
                FROM
                    simpleobjects.useraccount as ua
                LEFT JOIN
                    simplebilling.userplans up               
                ON
                    ua.UserId_int = up.UserId_int					
                AND
                    up.Status_int=1				
                WHERE
                    ua.IsTestAccount_ti = 0				
				<cfif inpType EQ "admin">
				AND
                    ua.ami_int > 0
				<cfelseif inpListUserId NEQ "">
                AND
                    ua.UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
				AND
                    ua.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>                 
				</cfif>
				 AND
				 	ua.amd_dt <= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                GROUP BY
                    ua.UserId_int,up.PlanId_int,ua.Created_dt   
            </cfquery> 
			<cfloop query="GetTotalUserTillDate">
				<!--- total--->
                <cfset dataout.TOTALREFERRED +=  Total> 
				<cfset dataout.LIST_TOTALREFERRED =  listAppend(dataout.LIST_TOTALREFERRED, UserId_int)> 
			</cfloop>

	    	<cfquery name="GetTotalUser" datasource="#Session.DBSourceREAD#">
                SELECT
                    count(ua.UserId_int) AS Total,   
					ua.UserId_int,                 
                    up.PlanId_int,
					ua.amd_dt                    
                FROM
                    simpleobjects.useraccount as ua
                LEFT JOIN
                    simplebilling.userplans up               
                ON
                    ua.UserId_int = up.UserId_int
                AND
                    up.Status_int=1
                WHERE
                    ua.IsTestAccount_ti = 0
				
				<cfif inpType EQ "admin">
				AND
                    ua.ami_int > 0
				<cfelseif inpListUserId NEQ "">
                AND
                    ua.UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
				AND
                    ua.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>                 
				</cfif>
				 AND
				 	ua.amd_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
					AND 
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                GROUP BY
                    ua.UserId_int,up.PlanId_int,ua.Created_dt   
            </cfquery> 
			
			
            <cfloop query="GetTotalUser">
				<!--- total--->
                <cfset dataout.TOTALNEWREFERRED +=  Total> 
				<cfset dataout.LIST_TOTALNEWREFERRED =  listAppend(dataout.LIST_TOTALNEWREFERRED, UserId_int)> 
                <cfif PlanId_int EQ 1>
                    <cfset dataout.FREEREFERRED += Total/>
					<cfset dataout.LIST_FREEREFERRED =  listAppend(dataout.LIST_FREEREFERRED, UserId_int)> 
                <cfelse>
                    <cfset dataout.PAIDREFERRED += Total/>
					<cfset dataout.LIST_PAIDREFERRED =  listAppend(dataout.LIST_PAIDREFERRED, UserId_int)> 
                </cfif>				
            </cfloop>		
			

            <cfset dataout.RXRESULTCODE = 1 />
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>
	<cffunction name="getGroupString" access="private" hint="get Group String">
	    <cfargument name="inpOffsetDate" type="numeric" required="true">

	    <cfset var type = "day">

	   	<cfif arguments.inpOffsetDate GTE 14>
		   	<cfset type = "week">
		</cfif>
			
		<cfif arguments.inpOffsetDate GTE 89>
			<cfset type = "month">
		</cfif>

	   	<cfreturn type />
    </cffunction>
    <cffunction name="GetReferPeriod" access="remote" output="true" hint="">
		<cfargument name="inpType" type="string" default="" required="false" >  
		<cfargument name="inpUserId" type="numeric" default="#Session.USERID#" required="false" >    	
		<cfargument name="inpListUserId" type="string" default="" required="false" >    		    
	    <cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">	    

	    <cfset var dataout = {} />
		<cfset var dataout["DataPeriod"] = '' />
		<cfset var dataout["DataLabel"] = [] />
	    <cfset var dataout["DataListActive"] = [] />
		<cfset var dataout["DataListNew"] = [] />
		<cfset var dataout["DataListUpgrade"] = [] />
		<cfset var dataout["DataListDowngrade"] = [] />
	    <cfset var getStatisticData = [] />	    
		
        <cfset var strGroupType = "" />
	    <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	    <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


	    <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	    <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />	    
		<cfset var dateOffset  = DateDiff("d",startDate, endDate) />

	    <cfset var day = ""/>
	    <cfset var eachDay = ''/>	 
        <cfset var GetTotalActive = ''/>	 
		<cfset var GetTotalNew = ''/>
		<cfset var getUserUpgrades = ''/>	 
		<cfset var getUserDowngrades = ''/>	 
		<cfset var getUsers = ''/>	 
		<cfset var userIds = []/>
		<cfset var startP=''/>
		<cfset var endP=''/>
		<cfset var dayPass=startDate/>
		<cfset var RetVarGetAdminPermission	= '' />
		<cfset var valueItem	= '' />
           

	    <cftry>	    	
			<cfinvoke method="getGroupString" returnvariable="strGroupType">
	    		<cfinvokeargument name="inpOffsetDate" value="#dateOffset#">
	    	</cfinvoke>
			<cfif inpType EQ "admin" OR inpType EQ "report">
				<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>    
				<cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
					<cfset dataout.RESULT = -1>
					<cfset dataout.MESSAGE = "No Admin Permission."/>
					<cfreturn dataout>
				</cfif>
			</cfif>			               			
			

			<cfset var dataout["DataPeriod"] = strGroupType />
	    	<cfswitch expression="#strGroupType#">
		    	<cfcase value="day"> 
					
					<cfset var currDateOut = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="eachDay" step="#CreateTimeSpan(1,0,0,0)#">						
						<cfset startP=eachDay/>
						<cfset endP=eachDay/>
						<cfset dataout["DataLabel"].append(DateFormat(currDateOut, "yyyy-mm-dd")) />
						<cfinvoke method="GetReferByDate" returnvariable="valueItem">
							<cfinvokeargument name="inpType" value="#arguments.inpType#">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inpListUserId" value="#arguments.inpListUserId#">
							<cfinvokeargument name="inpStartDate" value="#startP#">							
							<cfinvokeargument name="inpEndDate" value="#endP#">							
						</cfinvoke>
						<cfset dataout["DataListActive"].append(valueItem.DataListActive) />
						<cfset dataout["DataListNew"].append(valueItem.DataListNew) />
						<cfset dataout["DataListUpgrade"].append(valueItem.DataListUpgrade) />
						<cfset dataout["DataListDowngrade"].append(valueItem.DataListDowngrade) />
						
						
						<cfset currDateOut = currDateOut.add('d', 1) >
					</cfloop>

				</cfcase> 
				<cfcase value="week"> 

					<!--- Create key list for data --->
					<cfset var dateList = [] />
					<cfset var currDate = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(7,0,0,0)#">
						<cfif DateDiff("d",currDate, endDate) GT 0 AND currDate LT endDate>
							<cfset  dateList.append(currDate) />
							<cfset currDate = currDate.add('d', 7) >
						</cfif>
					</cfloop>
					<cfset  dateList.append(endDate) />

					<cfloop array="#dateList#" index="eachDay">	
											
						<cfset startP=dayPass.add('d', 1)/>
						<cfset endP=eachDay/>
						<cfif dayPass EQ eachDay>
							<cfset startP=dayPass/>
						</cfif>
						<cfset dataout["DataLabel"].append(DateFormat(eachDay, "yyyy-mm-dd")) />

						<cfinvoke method="GetReferByDate" returnvariable="valueItem">
							<cfinvokeargument name="inpType" value="#arguments.inpType#">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inpListUserId" value="#arguments.inpListUserId#">
								
							<cfinvokeargument name="inpStartDate" value="#startP#">							
							<cfinvokeargument name="inpEndDate" value="#endP#">						
						</cfinvoke>
						<cfset dataout["DataListActive"].append(valueItem.DataListActive) />
						<cfset dataout["DataListNew"].append(valueItem.DataListNew) />
						<cfset dataout["DataListUpgrade"].append(valueItem.DataListUpgrade) />
						<cfset dataout["DataListDowngrade"].append(valueItem.DataListDowngrade) />	
						<cfset var dayPass=eachDay/>
					</cfloop>

				</cfcase> 
				<cfcase value="month"> 
					<cfset var dateList = [] />
					<cfset var currDate = startDate />
					<cfset dateList.append(currDate) />
					
					<cfloop condition="DateCompare(currDate, endDate) LT 0)">
						<cfset currDate = currDate.add('m', 1) /> 
						<cfif DateCompare(currDate, endDate) LT 0>
							<cfset dateList.append(CreateDate(currDate.year(), currDate.month(), 1)) />
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />

					
					<cfloop array="#dateList#" index="eachDay">
						<cfset startP=dayPass.add('d', 1)/>
						<cfset endP=eachDay/>
						<cfif dayPass EQ eachDay>
							<cfset startP=dayPass/>
						</cfif>
						<cfset dataout["DataLabel"].append(DateFormat(eachDay, "yyyy-mm-dd")) />

						<cfinvoke method="GetReferByDate" returnvariable="valueItem">
							<cfinvokeargument name="inpType" value="#arguments.inpType#">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inpListUserId" value="#arguments.inpListUserId#">
												
							<cfinvokeargument name="inpStartDate" value="#startP#">							
							<cfinvokeargument name="inpEndDate" value="#endP#">	
						</cfinvoke>
						<cfset dataout["DataListActive"].append(valueItem.DataListActive) />
						<cfset dataout["DataListNew"].append(valueItem.DataListNew) />
						<cfset dataout["DataListUpgrade"].append(valueItem.DataListUpgrade) />
						<cfset dataout["DataListDowngrade"].append(valueItem.DataListDowngrade) />

						<cfset var dayPass=eachDay/>
					</cfloop>
				</cfcase> 
				
	    	</cfswitch>		
			
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>
	<cffunction name="GetReferByDate" access="remote" output="true" hint="">
		<cfargument name="inpType" type="string" default="" required="false" >  
		<cfargument name="inpUserId" type="numeric" default="#Session.USERID#" required="false" >    	
		<cfargument name="inpListUserId" type="string" default="" required="false" >    		    
	    <cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">	    

	    <cfset var dataout = {} />
	    <cfset var dataout["DataListActive"] = 0/>
		<cfset var dataout["DataListNew"] = 0 />
		<cfset var dataout["DataListUpgrade"] = 0/>
		<cfset var dataout["DataListDowngrade"] =0 />
	    <cfset var getStatisticData = [] />	    
		       
	    		

	    <cfset var day = ""/>
	    <cfset var eachDay = ''/>	 
        <cfset var GetTotalActive = ''/>	 
		<cfset var GetTotalNew = ''/>
		<cfset var getUserUpgrades = ''/>	 
		<cfset var getUserDowngrades = ''/>	 
		<cfset var getUsers = ''/>	 
		<cfset var userIds = []/>
		<cfset var valueItem = {} />
           

	    <cftry>	    								    	                             
			<!--- active user--->			
			<cfquery name="GetTotalActive" datasource="#Session.DBSourceREAD#">
				SELECT 
					count(ami_int) As Total
				FROM 
					simpleobjects.useraccount
				WHERE
				<cfif inpType EQ "admin">					
					ami_int > 0
				<cfelseif inpListUserId NEQ "">
					UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
					ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 					
				</cfif>
				AND
					IsTestAccount_ti = 0
				AND
					Active_int=1
				AND
										
					LastLogIn_dt
					BETWEEN 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpStartDate#"> 
					AND			DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpEndDate#">, INTERVAL 86399 SECOND)	
					
			</cfquery>                  			
			<cfset dataout["DataListActive"]=GetTotalActive.Total />
			<!--- new user--->
			<cfquery name="GetTotalNew" datasource="#Session.DBSourceREAD#">
				SELECT 
					count(ami_int) As Total
				FROM 
					simpleobjects.useraccount
				WHERE
				<cfif inpType EQ "admin">					
					ami_int > 0
				<cfelseif inpListUserId NEQ "">
					UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
					ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 					
				</cfif>
				AND
					IsTestAccount_ti = 0				
				AND
					amd_dt 
					BETWEEN 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpStartDate#"> 
					AND			DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpEndDate#">, INTERVAL 86399 SECOND)					
					
			</cfquery>      
			
			<cfset dataout["DataListNew"]=(GetTotalNew.Total) />

			<!--- upgrade--->
			<cfquery name="getUserUpgrades" datasource="#Session.DBSourceREAD#">
				SELECT 
					count(distinct(pw.userId_int)) as TOTAL
				FROM 
					simplebilling.payment_worldpay pw
				JOIN
					simpleobjects.useraccount ua
				ON 
					pw.userId_int = ua.UserId_int    
				WHERE 					
					pw.created	
					BETWEEN 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpStartDate#"> 
					AND			DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpEndDate#">, INTERVAL 86399 SECOND)						
				AND     
					(pw.ModuleName="By Plan" OR pw.ModuleName="Buy Plan")
				<cfif inpType EQ "admin">
					AND	ua.ami_int > 0
				<cfelseif inpListUserId NEQ "">
					AND	ua.UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
					AND ua.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 
				</cfif>
				
				AND ua.IsTestAccount_ti = 0  
			</cfquery>
			
			<cfset dataout["DataListUpgrade"]= (getUserUpgrades.TOTAL) />
			<!--- downgrade--->
			<cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
				SELECT 
					UserId_int AS UserId
				FROM 
					`simplebilling`.`userplans` 
				WHERE 
				(
				 DowngradeDate_dt BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpStartDate#"> AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpEndDate#">, INTERVAL 86399 SECOND)
                OR                                        
                    (
                     EndDate_dt BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpStartDate#"> AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#inpEndDate#">, INTERVAL 86399 SECOND)
                     AND  UserDowngradeDate_dt IS NOT NULL  
                    )
				)	
				<cfif inpType EQ "admin">
					AND UserId_int IN(
						SELECT	UserId_int
						FROM	simpleobjects.useraccount 
						WHERE	ami_int > 0

					)    
				
				<cfelseif inpListUserId NEQ "">
					AND	UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				<cfelse>
					AND UserId_int IN(
						SELECT	UserId_int
						FROM	simpleobjects.useraccount 
						WHERE	ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>

					)                                         				                                     						           
				</cfif>
	
			</cfquery>
			
			<cfif getUserDowngrades.RECORDCOUNT GT 0>

				<cfloop query="getUserDowngrades">
					<cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
				</cfloop>
				
				<cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
					SELECT 
						COUNT(*) AS TOTAL
					FROM 
						`simpleobjects`.`useraccount` ua
					WHERE 
						ua.IsTestAccount_ti = 0
					AND
						ua.Active_int = 1
					AND
						ua.UserId_int IN (#ArrayToList(userIds)#) 						
					ORDER BY
						ua.UserId_int DESC
				</cfquery>

				<cfset dataout["DataListDowngrade"]= (getUsers.TOTAL) />
			<cfelse>
				<cfset dataout["DataListDowngrade"]= 0 />
			</cfif>

            
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>
	
	<cffunction name="ApplyAffiliateCode" access="remote" output="true" hint="">
		<cfargument name="inpUserId" type="numeric" required="true" >    	
	    <cfargument name="inpAffiliateCode" type="string" required="true" default="">	    

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var UpdateAffiliateCode=''>
		<cfset var GetMasterInfo=''>
		<cfset var MasterID=0>
		<cfset var getUserCoupon=''>
		<cfset var rtGetCouponFromAffiliateCode=''>
		<cfset var inpPromotionCode=''>
		<cfset var newUserPromoCode=''>
		<cfset var currentCouponApply=''>
		<cfset var disableCoupon=''>
		<cfset var promocode	= '' />
           
	    <cftry>	    	               
            
			<cfinvoke component="public.sire.models.cfc.affiliate"  method="GetMasterInfo" returnvariable="GetMasterInfo">
				<cfinvokeargument  name="inpAffiliateCode"  value="#arguments.inpAffiliateCode#">
			</cfinvoke>
			<cfif GetMasterInfo.RXRESULTCODE EQ 1> 
				<cfset MasterID = GetMasterInfo.AFFILIATEINFO.UserId_int>
			<cfelse>
				<cfthrow type="Application" message="Invalid Affiliate Code" detail="Invalid Affiliate Code"/>
			</cfif>
			<cfif MasterID EQ inpUserId>
				<cfthrow type="Application" message="Cannot apply Affiliate Code for owner" detail="Cannot apply Affiliate Code for owner"/>
			</cfif>

			<cfif GetMasterInfo.AFFILIATEINFO.ami_int EQ arguments.inpUserId>
				<cfthrow type="Application" message="Master Affiliate cannot apply Affiliate Code of sub Affiliate" detail="Master Affiliate cannot apply Affiliate Code of sub Affiliate"/>
			</cfif>		
			<cfquery name="getUserCoupon" datasource="#Session.DBSourceREAD#">
				SELECT
					up.PromotionLastVersionId_int,
					up.UsedTimeByRecurring_int,
					up.RecurringTime_int,
					pc.promotion_code
				FROM
					simplebilling.userpromotions up
				INNER JOIN
					simplebilling.promotion_codes pc
				ON
					up.PromotionId_int= pc.origin_id
				AND
					up.PromotionLastVersionId_int= pc.promotion_id
				WHERE
					up.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				AND
					up.PromotionStatus_int = 1
			</cfquery>	
			<cfset currentCouponApply=getUserCoupon.promotion_code>	
			
			<!--- check linkage coupon code with affiliate code--->
			<cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
				<cfinvokeargument name="inpAffiliateCode" value="#arguments.inpAffiliateCode#"/>
			</cfinvoke>
			<cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>                           
				<cfset inpPromotionCode =rtGetCouponFromAffiliateCode.COUPONCODE >
			</cfif>
			<cfif inpPromotionCode NEQ ''>
				<cfif trim(currentCouponApply) NEQ trim(inpPromotionCode)>
					<cfquery name="disableCoupon" datasource="#Session.DBSourceEBM#">
						UPDATE
							simplebilling.userpromotions
						SET
							PromotionStatus_int = 0
						WHERE
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">						
					</cfquery>
					<cfinvoke component="session.sire.models.cfc.promotion" method="getActivePromoCode" returnvariable="promocode">
						<cfinvokeargument name="inpCode" value="#inpPromotionCode#">				
					</cfinvoke>
					<cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
						<cfinvokeargument name="inpPromoId" value="#promocode.promotion_id#">
						<cfinvokeargument name="inpPromoOriginId" value="#promocode.origin_id#">
						<cfinvokeargument name="inpUsedDate" value="#Now()#">
						<cfinvokeargument name="inpRecurringTime" value="#promocode.recurring_time#">
						<cfinvokeargument name="inpPromotionStatus" value="1">
					</cfinvoke>			
				<cfelse>

				</cfif>
			</cfif>
			
			
            <cfquery name="UpdateAffiliateCode" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simpleobjects.useraccount
				SET
					ami_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MasterID#"/>,
					amc_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAffiliateCode#"/>,
					amd_dt= NOW()
				WHERE
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 									
			</cfquery>  
			<cfset dataout.RXRESULTCODE = 1 />
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>

	<cffunction name="getTotalUserDowngrades" access="public"  returntype="struct" output="false" hint="Return total new user downgrades.">
        
        <cfargument
            name="inpStartDate"
            type="string"
            required="true"
            hint=""
            />
        <cfargument
            name="inpEndDate"
            type="string"
            required="true"
            hint=""
            />        

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = 0/>
        <cfset var getUserDowngrades = ''/>
        <cfset var getUsers = '' />
        <cfset var userIds = [] />
        <cfset var filterItem = ''/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cftry>
            <cfquery name="getUserDowngrades" datasource="#Session.DBSourceREAD#">
                SELECT 
                    UserId_int AS UserId
                FROM 
                    `simplebilling`.`userplans` 
                WHERE 
                    DowngradeDate_dt BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                OR                                        
                    (
                     EndDate_dt BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                     AND  UserDowngradeDate_dt IS NOT NULL  
                    )                 
    
            </cfquery>
            
            <cfif getUserDowngrades.RECORDCOUNT GT 0>

                <cfloop query="getUserDowngrades">
                    <cfset arrayAppend(userIds, getUserDowngrades.UserId)/>
                </cfloop>
                
                <cfquery name="getUsers" datasource="#Session.DBSourceREAD#">
                    SELECT 
                        COUNT(*) AS TOTAL
                    FROM 
                        `simpleobjects`.`useraccount` ua
                    WHERE 
                        ua.Active_int = 1
                    AND
                        ua.IsTestAccount_ti = 0
                    AND
                        ua.UserId_int IN (#ArrayToList(userIds)#)                                            
                    ORDER BY
                        ua.UserId_int DESC
                </cfquery>

                <cfset dataout.RESULT = getUsers.TOTAL/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        
        <cfreturn dataout/>
    </cffunction>
	<cffunction name="GetUserList" access="remote" output="true" hint="get user list with filter">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />				
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="ASC"/>						
		<cfargument name="customFilter" default=""/>
		

		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />		
		<cfset var order = ''/>
		<cfset var filterData = DeserializeJSON(arguments.customFilter) />		
		<cfset var GetUserList = "" />
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>		

		<!--- Build Order --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "UserId_int"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "Full_Name" />
			    </cfcase> 
			    <cfcase value="3"> 
			       <cfset order = "EmailAddress_vch" />
			    </cfcase> 
			    <cfcase value="4"> 
			       <cfset order = "MFAContactString_vch"/>
			    </cfcase> 
			    <cfcase value="5"> 
			       <cfset order = "Active_int"/>
			    </cfcase> 			   
			    <cfdefaultcase> 
			 		<cfset order = "UserId_int"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "UserId_int"/>
		</cfif>

		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["UserList"] = ArrayNew()>		
		<cfquery datasource="#Session.DBSourceREAD#" name="GetUserList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS
					UserId_int,
					Full_Name,
					EmailAddress_vch,
					MFAContactString_vch,					
					Active_int,
					IsTestAccount_ti,
					amd_dt,
					amc_vch					
				FROM (SELECT 
						u.UserId_int,
						CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
						u.EmailAddress_vch,
						u.MFAContactString_vch,
						IF(up.MlpsImageCapacityLimit_bi > 0 , up.MlpsImageCapacityLimit_bi, 0) AS MlpsImageCapacityLimit_bi,
						u.Active_int,
						u.IsTestAccount_ti,
						u.amd_dt,
						u.amc_vch
					FROM 
						simpleobjects.useraccount u
						INNER JOIN simplebilling.userplans up
						ON u.UserId_int = up.UserId_int 
						WHERE up.UserPlanId_bi IN ( SELECT MAX(UserPlanId_bi) FROM simplebilling.userplans GROUP BY userplans.UserId_int  )
						AND  u.ami_int > 0
					ORDER BY 
						up.UserPlanId_bi DESC
					) AS tmpTable
				WHERE
					tmpTable.Active_int = 1
				AND
					IsTestAccount_ti=0				
				<cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						<cfif filterItem.NAME EQ ' u.Status_int ' AND  filterItem.VALUE LT 0>
						<!--- DO nothing --->
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>

				GROUP BY 
					tmpTable.UserId_int
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>

		<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfloop query="GetUserList" >
			<cfset var tmpUser = {
				ID = '#UserId_int#',
				NAME = '#Full_Name#',
				EMAIL = '#EmailAddress_vch#',
				PHONE = '#MFAContactString_vch#',				
				STATUS = '#Active_int#',
				ISTESTACCOUNT = '#IsTestAccount_ti#',
				AMD= Dateformat(amd_dt,'MMM DD,YYYY'),
				AFFILIATE_CODE=amc_vch
			} />
			
			<cfset ArrayAppend(dataout["UserList"], tmpUser) />
		</cfloop>

   		<cfreturn dataout>
	</cffunction>
	<cffunction name="GetCommissionSetting" access="remote" output="false" hint="">	
        <cfargument name="inpUserId" TYPE="string" required="yes" default="" />		
		<cfargument name="inpOrder" TYPE="string" required="no" default="Condition_int DESC, Value_dec  ASC" />		
        <cfset var dataout = StructNew()/>
        <cfset dataout.RXRESULTCODE = 0 />		
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />		        
		<cfset dataout["SETTING"] = ArrayNew()>
        <cfset var GetData="">
		<cfset var tmpSetting = {}>
        <cftry>
            <cfquery name="GetData" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserID_int,                        
					Condition_int,
					Value_dec,
					ReferredPercent_dec
                FROM
                    simpleobjects.commission_setting               			
                WHERE
                    UserID_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 
				ORDER BY 
					Value_dec                      
            </cfquery>
			<cfif GetData.RECORDCOUNT EQ 0>				
				<cfquery name="GetData" datasource="#Session.DBSourceREAD#">
					SELECT
						UserID_int,                        
						Condition_int,
						Value_dec,
						ReferredPercent_dec
					FROM
						simpleobjects.commission_setting               			
					WHERE
						UserID_int=0
					ORDER BY 
						<!---Value_dec --->
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpOrder#"/>			 
				</cfquery>
			</cfif>
			<cfif GetData.RECORDCOUNT GT 0>	
				<cfloop query="GetData">
					<cfset tmpSetting = {
						Condition = '#Condition_int#',
						Value = '#Value_dec#',
						Percent = '#ReferredPercent_dec#'
					} />
					
					<cfset ArrayAppend(dataout["SETTING"], tmpSetting) />
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = "There is no commission setting" />
			</cfif>
			
            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>
	
	<cffunction name="SaveCommissionSetting" access="remote" output="false" hint="">		
		<cfargument name="inpUserId" TYPE="string" required="yes" />		
		<cfargument name="inpSetting" TYPE="string" required="yes" default="" />		
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		<cfset var DeleteOldData=''/>
		<cfset var index=''>
		<cfset var setting = deserializeJSON(arguments.inpSetting) />
		
        <cftry>			
			<cfquery datasource="#Session.DBSourceEBM#" name="DeleteOldData">
				DELETE FROM 
					simpleobjects.commission_setting   
				WHERE 
					UserID_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 
			</cfquery>
			
			<cfloop index = "index" array = "#setting#" >
				
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					INSERT INTO
						simpleobjects.commission_setting 
						(																								
							UserID_int,                        
							Condition_int,
							Value_dec,
							ReferredPercent_dec					
						)
					VALUES
						(							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,													
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LSParseNumber(index.condition)#"/>,																								
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#index.value#"/>,						
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#index.percent#"/>						
						)
				</cfquery>
			</cfloop>		
			
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	
	<cffunction name="GetUserListByListId" access="remote" output="true" hint="get user list with filter">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />				
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="ASC"/>			
		<cfargument name="inpListUserId" type="string" default="" required="true" > 				   		    

		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />		
		<cfset var order = ''/>			
		<cfset var GetUserList = "" />
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>		

		<!--- Build Order --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "u.UserId_int"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "u.Full_Name" />
			    </cfcase> 
			    <cfcase value="3"> 
			       <cfset order = "u.EmailAddress_vch" />
			    </cfcase> 
			    <cfcase value="4"> 
			       <cfset order = "u.MFAContactString_vch"/>
			    </cfcase> 
			    <cfcase value="5"> 
			       <cfset order = "u.Active_int"/>
			    </cfcase> 			   
			    <cfdefaultcase> 
			 		<cfset order = "u.UserId_int"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "UserId_int"/>
		</cfif>

		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["UserList"] = ArrayNew()>		
		<cfquery datasource="#Session.DBSourceREAD#" name="GetUserList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS
						u.UserId_int,
						CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
						u.EmailAddress_vch,
						u.MFAContactString_vch,						
						u.Active_int,
						u.IsTestAccount_ti,
						u.amd_dt as AffiliateDateApply,
						u.amc_vch as AffiliateCodeApply,
						up.PlanId_int,
						p.PlanName_vch,
						a.Affiliate_code_vch as AffiliateCodeRegister,
						a.CreateDate_dt as AffiliateDateRegister
				FROM 
						simpleobjects.useraccount u
				LEFT JOIN
					 simplebilling.userplans up
					 ON		u.UserId_int = up.UserId_int
					 AND	up.Status_int=1
				LEFT JOIN
					simplebilling.plans  p
					ON		p.PlanId_int= up.PlanId_int			
				LEFT JOIN
					simpleobjects.affiliate_code_assignment a
					ON		u.UserId_int = a.UserId_int
					AND		a.Status_ti=1		
				WHERE					
					u.IsTestAccount_ti=0			
				AND
                    u.UserId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListUserId#" list="yes"/>) 
				
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>

		<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfloop query="GetUserList" >
			<cfset var tmpUser = {
				ID = '#UserId_int#',
				NAME = '#Full_Name#',
				EMAIL = '#EmailAddress_vch#',
				PHONE = '#MFAContactString_vch#',				
				STATUS = '#Active_int#',
				ISTESTACCOUNT = '#IsTestAccount_ti#',
				PLAN= #PlanName_vch#,
				AffiliateDateApply= #Dateformat(AffiliateDateApply, "MM/DD/YY")#,			
				AffiliateCodeApply=AffiliateCodeApply,
				AffiliateCodeRegister= AffiliateCodeRegister,
				AffiliateDateRegister=Dateformat(AffiliateDateRegister, "MM/DD/YY")
			} />
			
			<cfset ArrayAppend(dataout["UserList"], tmpUser) />
		</cfloop>

   		<cfreturn dataout>
	</cffunction>
	<cffunction name="GetCommissionReport" access="remote" output="true" hint="">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />				
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="ASC"/>	
		<cfargument name="customFilter" default=""/>		
					   		    
		<cfset var filterData	= '' />
		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />		
		<cfset var order = ''/>			
		<cfset var GetReportList = "" />
		
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>		

		<!--- Build Order --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "Full_Name"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "PaypalEmail_vch" />
			    </cfcase> 
				<cfcase value="3"> 
			       <cfset order = "Affiliate_code_vch" />
			    </cfcase> 
				<cfcase value="4"> 
			       <cfset order = "FromDate_dt" />
			    </cfcase> 
				<cfcase value="5"> 
			       <cfset order = "TotalPaymentOfReferred_dec" />
			    </cfcase> 
				<cfcase value="6"> 
			       <cfset order = "ReferredPercent_dec" />
			    </cfcase> 
				<cfcase value="7"> 
			       <cfset order = "TotalCommission_dec" />
			    </cfcase> 
				<cfcase value="8"> 
			       <cfset order = "PaymentStatus_ti" />
			    </cfcase> 
				<cfcase value="9"> 
			       <cfset order = "PaidDate" />
			    </cfcase> 
				<cfcase value="10"> 
			       <cfset order = "PaidConfirm" />
			    </cfcase> 			    	   
			    <cfdefaultcase> 
			 		<cfset order = "Full_Name"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "Full_Name"/>
		</cfif>
		<cftry>
		<cfset filterData = DeserializeJSON(arguments.customFilter) />
		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["ReportList"] = ArrayNew()>		
		<cfquery datasource="#Session.DBSourceREAD#" name="GetReportList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS
						acbp.UserId_int,
						CONCAT(ua.FirstName_vch ," ", ua.LastName_vch) AS Full_Name,
						ua.PaypalEmail_vch,
						aca.Affiliate_code_vch,
						acbp.TotalPaymentOfReferred_dec,
						acbp.ReferredPercent_dec,
						acbp.TotalCommission_dec,
						acbp.PaymentStatus_ti,
						acbp.Id_bi,
						acbp.FromDate_dt,
						acbp.ToDate_dt,						
						lp.PayoutBatchId_vch as PaidConfirm ,
                    	acbp.PaymentDate_dt as PaidDate	,
						lp.PayoutBatchId_vch					
				FROM 
						simplebilling.affiliate_commission_by_periods acbp
				LEFT JOIN
					 simplebilling.logs_payment_paypal lp
					 ON		acbp.UserId_int = lp.UserId_int	
					 AND	lp.SourceId_int= 	acbp.Id_bi
					 AND	lp.ModuleName_vch='Affiliate Commission'
				LEFT JOIN
					 simpleobjects.useraccount ua
					 ON		acbp.UserId_int = ua.UserId_int			
				LEFT JOIN
					 simpleobjects.affiliate_code_assignment aca		 									
					ON		ua.UserId_int= aca.UserId_int
				WHERE
				acbp.TotalCommission_dec > 0
					<cfif customFilter NEQ "">
						<cfloop array="#filterData#" index="filterItem">
							<cfif filterItem.NAME EQ 'acbp.PaymentStatus_ti' AND  filterItem.VALUE LT 0>
							<!--- DO nothing --->
							<cfelseif TRIM(filterItem.NAME) IS 'acbp.FromDate_dt'>
								AND MONTH(acbp.FromDate_dt) = #MID(filterItem.VALUE,6,2)#
								AND YEAR(acbp.FromDate_dt)= #LEFT(filterItem.VALUE,4)#

							<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>
		
		
		<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfloop query="GetReportList" >
			<cfset var tmpUser = {
				ID= Id_bi,
				AFFILIATE_NAME = '#Full_Name#',				
				EMAIL = '#PaypalEmail_vch#',
				AFFILIATE_CODE = '#Affiliate_code_vch#',				
				REVENUE = '#TotalPaymentOfReferred_dec#',
				COMMISSION_RATE = '#ReferredPercent_dec#',
				COMMISSION_EARNED= #TotalCommission_dec#,
				FROMDATE= dateformat(FromDate_dt,"DD MMM YYYY"),
				TODATE=  dateformat(ToDate_dt,"DD MMM YYYY"), 
				PAID_PERIOD= dateformat(FromDate_dt ,"MMM YYYY"),
				PAID_STATUS= #PaymentStatus_ti#,			
				PAID_DATE= dateformat(PaidDate,"DD MMM YYYY"),
				PAID_CONFIRMATION= PaidConfirm,
				PAYOUT_BATCH_ID=PayoutBatchId_vch				
			} />
			
			<cfset ArrayAppend(dataout["ReportList"], tmpUser) />
		</cfloop>
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 />			
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>

   		<cfreturn dataout>
	</cffunction>
	<cffunction name="GetCommissionReportDetail" access="remote" output="true" hint="">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />				
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="ASC"/>	
		<cfargument name="inpCommissionId" TYPE="numeric" required="yes" default="0" />				
				
					   		    

		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />		
		<cfset var order = ''/>			
		<cfset var GetReportList = "" />
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>		

		<!--- Build Order --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "UserId_int"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "Full_Name" />
			    </cfcase> 	
				<cfcase value="3"> 
			       <cfset order = "EmailAddress_vch" />
			    </cfcase> 	
				<cfcase value="4"> 
			       <cfset order = "HomePhoneStr_vch" />
			    </cfcase> 	
				<cfcase value="5"> 
			       <cfset order = "PlanName_vch" />
			    </cfcase> 	
				<cfcase value="6"> 
			       <cfset order = "Value_dec" />
			    </cfcase> 	
				<cfcase value="7"> 
			       <cfset order = "Value_dec" />
			    </cfcase> 			    	   
			    <cfdefaultcase> 
			 		<cfset order = "Full_Name"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "Full_Name"/>
		</cfif>
		<cftry>
		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["ReportList"] = ArrayNew()>		
		<cfquery datasource="#Session.DBSourceREAD#" name="GetReportList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS						
						CONCAT(ua.FirstName_vch ," ", ua.LastName_vch) AS Full_Name,
						ua.EmailAddress_vch,
						p.PlanName_vch,
						ua.UserId_int,
						ua.HomePhoneStr_vch,
						SUM(acbpd.Value_dec) as Value_dec
						
				FROM 
						simplebilling.affiliate_commission_by_periods_detail acbpd
				LEFT JOIN
					 simpleobjects.useraccount ua
					 ON		acbpd.UserId_int = ua.UserId_int			
				LEFT JOIN
					 simplebilling.userplans  up
					ON		ua.UserId_int= up.UserId_int
					AND		up.Status_int =1
				INNER JOIN
					simplebilling.plans p
					ON		up.PlanId_int= p.PlanId_int
				WHERE
					acbpd.Commission_int= <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCommissionId#">
				GROUP BY
					Full_Name,
					EmailAddress_vch,
					PlanName_vch,
					UserId_int,
					HomePhoneStr_vch	
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>		
		
		<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfloop query="GetReportList" >
			<cfset var tmpUser = {
				ID = '#UserId_int#',				
				NAME = '#Full_Name#',
				EMAIL = '#EmailAddress_vch#',				
				PHONE = '#HomePhoneStr_vch#',
				PLAN = '#PlanName_vch#',
				REVENUE= #Value_dec#										
			} />
			
			<cfset ArrayAppend(dataout["ReportList"], tmpUser) />
		</cfloop>
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 />			
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>

   		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetTotalActiveReferrals" access="remote" output="false" hint="">		
		<cfargument name="inpUserId" TYPE="string" required="no" default="#Session.UserID#" />					
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
		<cfset dataout.TOTAL_USER = 0 />	      
        <cfset var GetData=''/>
		
		
        <cftry>			
			<cfquery datasource="#Session.DBSourceEBM#" name="GetData">
				SELECT 
					count(*) as Total
				FROM
					simpleobjects.useraccount ua		
				INNER JOIN
					simplebilling.userplans up 
					ON 
					ua.UserId_int = up.UserId_int 						
				WHERE 
					ami_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 
				AND
					up.PlanId_int > 1
				AND
					up.Status_int = 1				
				AND 
					ua.Active_int=1
			</cfquery>									
			<cfset dataout.TOTAL_USER = GetData.Total/>	   			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="GetAffiliateLinkageCoupon" access="remote" output="false" hint="">		
		<cfargument name="inpUserId" TYPE="string" required="yes" />				
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
		<cfset dataout.COUPONCODE = '' />	    
        <cfset var GetData=''/>
		
		
        <cftry>	
			<cfquery datasource="#Session.DBSourceREAD#" name="GetData">
				SELECT
					alc.UserId_int,                        
					alc.Affiliate_Id_int,
					alc.Coupon_Id_int,
					alc.CreateDate_dt,
					alc.CreateBy_int,
					alc.Status_ti,
					pc.promotion_code			
				FROM
					simpleobjects.affiliate_linkage_coupon alc		
				INNER JOIN 		
					simplebilling.promotion_codes pc
				ON
					alc.Coupon_Id_int= pc.promotion_id
				WHERE 
					alc.UserID_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 				
				AND
					alc.Status_ti=1
				AND
					pc.promotion_status = 1
			</cfquery>	
			<cfloop query="GetData">				
				<cfset dataout.COUPONCODE = promotion_code />	  
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />

			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="SaveAffiliateLinkageCoupon" access="remote" output="false" hint="">		
		<cfargument name="inpUserId" TYPE="string" required="yes" />		
		<cfargument name="inpAffiliateId" TYPE="string" required="yes" default="" />		
		<cfargument name="inpCouponCode" TYPE="string" required="yes" default="" />		
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		<cfset var DeleteOldData=''/>
		<cfset var CheckValidCoupon=''/>
		<cfset var CheckCouponAvailable=''/>
		<cfset var index=''>
		<cfset var couponId=0>
		
        <cftry>	
			<cfquery datasource="#Session.DBSourceREAD#" name="CheckCouponAvailable">
				SELECT
					alc.UserId_int,                        
					alc.Affiliate_Id_int,
					alc.Coupon_Id_int,
					alc.CreateDate_dt,
					alc.CreateBy_int,
					alc.Status_ti,
					pc.promotion_code,
					aca.Affiliate_code_vch			
				FROM
					simpleobjects.affiliate_linkage_coupon alc		
				INNER JOIN 		
					simplebilling.promotion_codes pc
				ON
					alc.Coupon_Id_int= pc.promotion_id
				INNER JOIN
					simpleobjects.affiliate_code_assignment aca
				ON
					aca.Id_int= alc.Affiliate_Id_int	
				WHERE 
					pc.promotion_code=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpCouponCode)#"/>			 				
				AND
					alc.Status_ti=1
				AND
					pc.promotion_status = 1
			</cfquery>	
			<cfif CheckCouponAvailable.RECORDCOUNT GT 0>
				<cfif CheckCouponAvailable.UserId_int EQ arguments.inpUserId>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfreturn dataout>
				<cfelse>
					<cfthrow type="any" message="This Coupon Code have already assigned for another Affiliate Code" detail="This Coupon Code have already assigned for another Affiliate Code"/>
				</cfif>
			</cfif>
			<cfquery datasource="#Session.DBSourceREAD#" name="CheckValidCoupon">
				SELECT
					promotion_id,
					promotion_desc					
				FROM
					simplebilling.promotion_codes
				WHERE
					latest_version = 1 
					AND promotion_code = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCouponCode#"> 
					AND promotion_status = 1
					AND date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now()#">
					AND IFNULL(expiration_date, date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now()#">)
					AND (( redemption_count_int > 0 AND max_redemption_int > 0) OR max_redemption_int = 0)
					AND promotion_id > 0
					AND origin_id > 0
				ORDER BY
					promotion_id DESC
				LIMIT 1
			</cfquery>	
			<cfif CheckValidCoupon.RECORDCOUNT EQ 1>
				<cfset couponId=CheckValidCoupon.promotion_id>
				<cfquery datasource="#Session.DBSourceEBM#" name="DeleteOldData">
					DELETE FROM 
						simpleobjects.affiliate_linkage_coupon
					WHERE 
						UserID_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 
					AND				
						Affiliate_Id_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpAffiliateId)#"/>			 					
				</cfquery>						
					
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					INSERT INTO 
						simpleobjects.affiliate_linkage_coupon
						(																								
							UserId_int,                        
							Affiliate_Id_int,
							Coupon_Id_int,
							CreateDate_dt,
							CreateBy_int,
							Status_ti
						)
					VALUES
						(							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,													
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAffiliateId#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#couponId#"/>,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"/>,
							1					
						)
				</cfquery>														
				<cfset dataout.RXRESULTCODE = 1 />

			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />	   		    
	   		    <cfset dataout.MESSAGE = "Invalid Coupon Code" />
	   		    <cfset dataout.ERRMESSAGE = "Invalid Coupon Code" />
			</cfif>

			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="GetAffiliateFromCouponCode" access="remote" output="false" hint="">		
		<cfargument name="inpCouponCode" TYPE="string" required="yes" />				
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
		<cfset dataout.COUPONCODE = arguments.inpCouponCode />	    
		<cfset dataout.AFFILIATECODE = "" />	    
		<cfset dataout.AFFILIATEUSERID = "" />	    
        <cfset var GetData=''/>
		
		
        <cftry>	
			<cfquery datasource="#Session.DBSourceREAD#" name="GetData">
				SELECT
					alc.UserId_int,                        
					alc.Affiliate_Id_int,
					alc.Coupon_Id_int,
					alc.CreateDate_dt,
					alc.CreateBy_int,
					alc.Status_ti,
					pc.promotion_code,
					aca.Affiliate_code_vch			
				FROM
					simpleobjects.affiliate_linkage_coupon alc		
				INNER JOIN 		
					simplebilling.promotion_codes pc
				ON
					alc.Coupon_Id_int= pc.promotion_id
				INNER JOIN
					simpleobjects.affiliate_code_assignment aca
				ON
					aca.Id_int= alc.Affiliate_Id_int	
				WHERE 
					pc.promotion_code=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpCouponCode)#"/>			 				
				AND
					alc.Status_ti=1
				AND
					pc.promotion_status = 1
			</cfquery>	
			<cfloop query="GetData">								 
				<cfset dataout.AFFILIATECODE = Affiliate_code_vch />	
				<cfset dataout.AFFILIATEUSERID = UserId_int />	         
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />

			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="GetCouponFromAffiliateCode" access="remote" output="false" hint="Get c">		
		<cfargument name="inpAffiliateCode" TYPE="string" required="yes" />				
        
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
		<cfset dataout.COUPONCODE = "" />	    
		<cfset dataout.AFFILIATECODE = arguments.inpAffiliateCode/>	    
        <cfset var GetData=''/>
		
		
        <cftry>	
			<cfquery datasource="#Session.DBSourceREAD#" name="GetData">
				SELECT
					alc.UserId_int,                        
					alc.Affiliate_Id_int,
					alc.Coupon_Id_int,
					alc.CreateDate_dt,
					alc.CreateBy_int,
					alc.Status_ti,
					pc.promotion_code,
					aca.Affiliate_code_vch			
				FROM
					simpleobjects.affiliate_linkage_coupon alc		
				INNER JOIN 		
					simplebilling.promotion_codes pc
				ON
					alc.Coupon_Id_int= pc.promotion_id
				INNER JOIN
					simpleobjects.affiliate_code_assignment aca
				ON
					aca.Id_int= alc.Affiliate_Id_int	
				WHERE 
					aca.Affiliate_code_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpAffiliateCode)#"/>			 				
				AND
					alc.Status_ti=1
				AND
					pc.promotion_status = 1
			</cfquery>	
			<cfloop query="GetData">								 				
				<cfset dataout.COUPONCODE = promotion_code />	        
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />

			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
</cfcomponent>