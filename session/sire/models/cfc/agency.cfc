<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
     <!--- contains object BasicPermission --->
     <cfinclude template="/public/sire/configs/userConstants.cfm">

	<cfparam name="Session.USERID" default="0"/>
     <cfparam name="Session.EmailAddress" default="0"/>
     <cfparam name="Session.loggedIn" default="0"/>
     <cfparam name="Session.AgencyId" default="0"/>
     <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cfset LOCAL_LOCALE = "English (US)">

     <cffunction name="CheckAgencyPermission" access="public" output="false" hint="Prevent unauthorized access to the agency page.">
          <cfif Session.AgencyId LT 1>
               <cflocation url="/session/sire/pages/dashboard" addToken="no" />
          </cfif>
     </cffunction>

     <!---- Registers a new semi-managed user and adds it to the current agency manager as a client --->
     <cffunction name="AddClientToAgencySemi" access="remote" output="false" hint="Add UserId_int as a semi-managed client to the agency account">

          <cfargument name="inpLName" type="string" required="yes">
          <cfargument name="inpEmailAddress" type="string" required="yes">
          <cfargument name="inpPhoneNumber" type="string" required="yes">
          <cfargument name="inpFName" type="string" required="yes">
          <cfargument name="inpPassword" required="yes" type="string">

          <cfset var AddClient = "" />
		<cfset var DataOut = {} />
          <cfset var GetUserId = "" />
          <cfset var inpTimeZone='31' />

		<cfset DataOut.RXRESULTCODE = 1 />
		<cfset DataOut.TYPE = "" />
		<cfset DataOut.MESSAGE = "An error occurred during the RegisterNewAccount, GetUserId,or AddClient query" />
		<cfset DataOut.ERRMESSAGE = "" />

          <cfif Session.AgencyId GT 0>
               <cfinvoke method="RegisterNewAccountNew" component="public.sire.models.cfc.userstools">
                    <cfinvokeargument name="inpFName" value="#inpFName#">
                    <cfinvokeargument name="inpMainEmail" value="#inpEmailAddress#">
                    <cfinvokeargument name="inpLName" value="#inpLName#">
                    <cfinvokeargument name="inSmsNumber" value="#inpPhoneNumber#">
                    <cfinvokeargument name="inpNaPassword" value="#inpPassword#">
                    <cfinvokeargument name="INPTIMEZONE" value="#inpTimeZone#">
                    <cfinvokeargument name="RegisteredByAgency" value="1">
                    <cfinvokeargument name="inpPlanId" value="1">
               </cfinvoke>

               <!--- May consider using generated key from resultset instead of GetUserId --->
               <cftry>
                    <cfquery name="GetUserId" datasource="#Session.DBSourceEBM#" >
                         SELECT
                              UserId_int
                         FROM
                              simpleobjects.useraccount
                         WHERE
                              EmailAddress_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmailAddress#">
                    </cfquery>

          		<cfquery name="AddClient" datasource="#Session.DBSourceEBM#">
          			INSERT INTO
          				simpleobjects.agencyclients (AgencyId_int, UserId_int, Created_dt)
          			VALUES
                              (
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.AgencyId#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserId.UserId_int#">,
                              NOW()
                              )
          		</cfquery>

          		<cfset DataOut.RXRESULTCODE = "1" />
          		<cfset DataOut.TYPE = "" />
          		<cfset DataOut.MESSAGE = "" />
          		<cfset DataOut.ERRMESSAGE = "" />

          		<cfcatch type="any">
          			<cfset DataOut.RXRESULTCODE = -1 />
          			<cfset DataOut.TYPE = "#cfcatch.TYPE#" />
          			<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
          			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
          		</cfcatch>
     		</cftry>
          </cfif>

          <cfset dataMail = {
              FirstName: "#inpFName#",
              Password: "#inpPassword#"
          }/>

          <cftry>
               <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" >
                    <cfinvokeargument name="to" value="#inpEmailAddress#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Your Sire Account Password">
                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_agency_send_password.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
               </cfinvoke>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch>
                    <cfset DataOut.RXRESULTCODE = -1 />
                    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cfreturn DataOut />
     </cffunction>

     <cffunction name="EditClientDetails" access="remote" output="false" hint="Edit selected client's details such as name, email, etc.">

          <cfargument name="inpLName" type="string" required="yes">
          <cfargument name="inpEmailAddress" default="" type="string" required="no">
          <cfargument name="inpPhoneNumber" type="string" required="yes">
          <cfargument name="inpFName" type="string" required="yes">
          <cfargument name="inpUserId" type="string" required="yes">
          <cfargument name="inpCompanyName" type="string" default="" required="no">

		<cfset var DataOut = {} />

		<cfset DataOut.RXRESULTCODE = 1 />
		<cfset DataOut.TYPE = "" />
		<cfset DataOut.MESSAGE = "EditClient query was unsuccessful" />
		<cfset DataOut.ERRMESSAGE = "" />

          <cftry>
               <cfquery name="EditClient" datasource="#Session.DBSourceEBM#">
                    UPDATE
                         simpleobjects.useraccount
                    SET
                         FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFName#">,
                         LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLName#">,
                         <cfif len(inpEmailAddress)>
                              EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmailAddress#">,
                         </cfif>
                         CompanyName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,
                         HomePhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">
                    WHERE
                         UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
               </cfquery>

     		<cfset DataOut.RXRESULTCODE = "1" />
     		<cfset DataOut.TYPE = "" />
     		<cfset DataOut.MESSAGE = "" />
     		<cfset DataOut.ERRMESSAGE = "" />

     		<cfcatch type="any">
     			<cfset DataOut.RXRESULTCODE = -1 />
     			<cfset DataOut.TYPE = "#cfcatch.TYPE#" />
     			<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
     			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
     		</cfcatch>
		</cftry>
          <cfreturn DataOut />
     </cffunction>

	<cffunction name="GetAgencyClients" access="remote" output="false" hint="Provides data for populating agency datatable">
		<cfargument name="inpStart" required="no" type="string" default="" hint="Start date to filter clients">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="End date to filter clients">
	     <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">

	     <cfargument name="iDisplayStart" required="no" default="0">
	     <cfargument name="iDisplayLength" required="no" default="14">

	     <cfargument name="sSearch" required="no" default="">
	     <cfargument name="iSortingCols" required="no" default="0">

		<cfset var GetNumbersCount = '' />
		<cfset var listColumns  = '' />
		<cfset var tempItem = '' />
		<cfset var LoopIndex = '' />
		<cfset var DataOut = {}>

		<cfset DataOut["aaData"] = ArrayNew(1)>
		<cfset DataOut.RXRESULTCODE = 1 />
		<cfset DataOut.TYPE = "" />
		<cfset DataOut.MESSAGE = "Failed to create dataTable for clients" />
		<cfset DataOut.ERRMESSAGE = "" />
		<cfset DataOut["iTotalRecords"] = 0>
		<cfset DataOut["iTotalDisplayRecords"] = 0>

		<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
			<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
			<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
		</cfif>

		<cftry>
		<!--- List of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
			<cfset listColumns = "UserId_int, CompanyName_vch, Created_dt, FirstName_vch, LastName_vch, HomePhoneStr_vch, EmailAddress_vch, Active_int" />

			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(DISTINCT UserId_int) AS TOTALCOUNT
				FROM
					simpleobjects.agencyclients
				WHERE

					AgencyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.AgencyId#">

					<cfif len(arguments.inpStart) AND len(arguments.inpEnd)>
						AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
					</cfif>
					<cfif len( trim(sSearch) )>
						AND
							UserId_int like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
					</cfif>
			</cfquery>

			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
			</cfif>

			<!--- Get data to display data set after filtering --->
			<cfquery name="GetClients" datasource="#Session.DBSourceEBM#">
                    SELECT
                         simpleobjects.useraccount.UserId_int,
                         simpleobjects.useraccount.Created_dt,
     				simpleobjects.useraccount.HomePhoneStr_vch,
     				simpleobjects.useraccount.EmailAddress_vch,
     				simpleobjects.useraccount.FirstName_vch,
     				simpleobjects.useraccount.LastName_vch,
                         simpleobjects.useraccount.CompanyName_vch,
     				simpleobjects.agencyclients.Active_int
     			FROM
     				simpleobjects.agencyclients
     			LEFT JOIN
     				simpleobjects.useraccount
     			ON
     				simpleobjects.agencyclients.UserId_int = simpleobjects.useraccount.UserId_int

     				<cfif len(arguments.inpStart) AND len(arguments.inpEnd) >
     					AND
     						Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
     					AND
     						Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
     				</cfif>

     				<cfif len( trim(sSearch) )>
     					AND
     						UserId_int like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
     				</cfif>

                    WHERE
                         simpleobjects.agencyclients.AgencyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.AgencyId#">

				<cfif iSortingCols gt 0>
					ORDER BY
				 		<cfloop from="0" to="#iSortingCols-1#" index="LoopIndex">
							<cfif LoopIndex is not 0>, </cfif>
							#listGetAt(listColumns,(form["iSortCol_"&LoopIndex]+1))# <cfif listFindNoCase("asc,desc",form["sSortDir_"&LoopIndex]) gt 0>#form["sSortDir_"&LoopIndex]#</cfif> </cfloop>
				</cfif>

				<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength AND iDisplayLength GT 0>
					LIMIT
					 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">,
					     <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
				<cfelseif iDisplayLength LT 0>
				<!--- No limit records--->
				<cfelse>
					LIMIT
					 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
				</cfif>
			</cfquery>

               <!--- Cfloop over each row of the result set--->
			<cfloop query="GetClients">
				<cfset tempItem = [
                         '#GetClients.UserId_int#',
                         '#GetClients.CompanyName_vch#',
					'#GetClients.FirstName_vch#',
					'#GetClients.LastName_vch#',
					'#GetClients.HomePhoneStr_vch#',
					'#GetClients.EmailAddress_vch#',
					'#GetClients.Created_dt#',
					'#GetClients.Active_int#'
				]>
				<cfset ArrayAppend(DataOut["aaData"],tempItem)>
			</cfloop>

			<cfset DataOut.RXRESULTCODE = "1" />
			<cfset DataOut.TYPE = "" />
			<cfset DataOut.MESSAGE = "" />
			<cfset DataOut.ERRMESSAGE = "" />

			<cfcatch type="any" >
				<cfset DataOut.RXRESULTCODE = -1 />
				<cfset DataOut.TYPE = "#cfcatch.TYPE#" />
				<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset DataOut["iTotalRecords"] = 0>
				<cfset DataOut["iTotalDisplayRecords"] = 0>
				<cfset DataOut["aaData"] = ArrayNew(1)>
			</cfcatch>
		</cftry>

		<cfreturn DataOut>
     </cffunction>

     <cffunction name="ActivateClient" access="remote" output="false" hint="Activate a client's account (both main sire and agency)">

          <cfargument name="UserId" type="string" required="yes" default="">

          <cfset var DataOut = {}>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.TYPE = "" />
          <cfset DataOut.MESSAGE = "Failed to activate client's account" />
          <cfset DataOut.ERRMESSAGE = "" />

          <cftry>
               <cfquery name="ActivateSire" datasource="#Session.DBSourceEBM#">
                    UPDATE
                         simpleobjects.useraccount
                    SET
                         simpleobjects.useraccount.Active_int = 1
                    WHERE
                         simpleobjects.useraccount.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">
               </cfquery>

               <cfquery name="ActivateAgency" datasource="#Session.DBSourceEBM#">
                    UPDATE
                         simpleobjects.agencyclients
                    SET
                         simpleobjects.agencyclients.Active_int = 1
                    WHERE
                         simpleobjects.agencyclients.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">
               </cfquery>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch type="any" >
               	<cfset DataOut.RXRESULTCODE = -1 />
               	<cfset DataOut.TYPE = "#cfcatch.TYPE#" />
               	<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
               	<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cfreturn DataOut>
     </cffunction>

     <cffunction name="DeactivateClient" access="remote" output="false" hint="Deactivate a client's account">

          <cfargument name="UserId" type="string" required="yes" default="">
          <cfargument name="DeactivationType" type="boolean" required="yes" default="0" />

          <cfset var DataOut = {}>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.TYPE = "" />
          <cfset DataOut.MESSAGE = "Failed to deactivate client from agencyclients table" />
          <cfset DataOut.ERRMESSAGE = "" />

          <cfif DeactivationType EQ "1">
               <cftry>
                    <cfquery name="DeactivateSire" datasource="#Session.DBSourceEBM#">
                         UPDATE
                              simpleobjects.useraccount
                         SET
                              simpleobjects.useraccount.Active_int = 0
                         WHERE
                              simpleobjects.useraccount.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">
                    </cfquery>

                    <cfcatch type="any" >
                         <cfset DataOut.RXRESULTCODE = -1 />
                         <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                         <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                         <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
                    </cfcatch>
               </cftry>
          </cfif>

          <cftry>
               <cfquery name="DeactivateAgency" datasource="#Session.DBSourceEBM#">
                    UPDATE
                         simpleobjects.agencyclients
                    SET
                         simpleobjects.agencyclients.Active_int = 0
                    WHERE
                         simpleobjects.agencyclients.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">
               </cfquery>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch type="any" >
                    <cfset DataOut.RXRESULTCODE = -1 />
                    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cfreturn DataOut>
     </cffunction>

     <cffunction name="ResetClientPassword" access="remote" output="false">
		<cfargument name="inpUserId" TYPE="string" required="yes" />
          <cfargument name="inpEmailAddress" type="string" required="yes">

		<cfset var DataOut = {} />
		<cfset var VerifyCode = '' />
		<cfset var DataMail = {}/>
		<cfset var CheckExpire = '' />

          <cfset DataOut.RXRESULTCODE = "0" />
          <cfset DataOut.TYPE = "" />
          <cfset DataOut.MESSAGE = "Failed to update password verification code" />
          <cfset DataOut.ERRMESSAGE = "" />

          <cftry>
               <!--- Generates new VerifyPasswordCode, adds to the useraccount table and sends email to specified address --->
               <cfset VerifyCode = Replace(CreateUUID(), "-", "", "All") />
               <cfquery name="UpdateVerificationCode" datasource="#Session.DBSourceEBM#">
                    UPDATE
                    simpleobjects.useraccount
                    SET
                    VerifyPasswordCode_vch = '#VerifyCode#',
                    validPasswordLink_ti = 0
                    WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
               </cfquery>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "Verification Code Updated" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch type="any">
                    <cfset DataOut.RXRESULTCODE = "-1" />
                    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cftry>
               <!--- Detail - Feature using mail account from BabbleSphere.
                Mail user their welcome aboard email. check account expire (over 30day) --->
               <cfquery name="CheckExpire" datasource="#Session.DBSourceREAD#">
               	SELECT
                         DATEDIFF(NOW(), EndDate_dt) AS ExpireDate FROM simplebilling.userplans
               	WHERE
                         UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
               	AND
                         Status_int = 1
               	HAVING
                         ExpireDate > 30
               </cfquery>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "Check Expire succeeded" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch type="any">
                    <cfset DataOut.RXRESULTCODE = "-1" />
                    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cfif CheckExpire.recordcount EQ 0>
               <cftry>
                    <cfset DataMail =
                    {
                         FirstName: "",
                         verifyCode: VerifyCode,
                         rootUrl: rootUrl,
                         publicPath: publicPath,
                         BrandShort: BrandShort
          		}
                    />

                    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                         <cfinvokeargument name="to" value="#TRIM(inpEmailAddress)#">
                         <cfinvokeargument name="type" value="html">
                         <cfinvokeargument name="subject" value="Requested Password reset for #BrandShort# Account">
                         <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_forgot_password.cfm">
                         <cfinvokeargument name="data" value="#TRIM( serializeJSON(dataMail) )#">
               	</cfinvoke>

                    <cfset DataOut.RXRESULTCODE = "1" />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "Email Sent Successfully" />
                    <cfset DataOut.ERRMESSAGE = "" />

                    <cfcatch type="any">
                         <cfset DataOut.RXRESULTCODE = "-1" />
                         <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                         <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                         <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
                    </cfcatch>
               </cftry>
          </cfif>

          <cfreturn DataOut />
     </cffunction>

     <cffunction name="AddClientToAgencyFully" access="remote" output="false" hint="Add UserId_int as a Fully managed client under the agency account">

          <cfargument name="inpFName" type="string" required="yes">
          <cfargument name="inpLName" type="string" required="yes">
          <cfargument name="inpCompanyName" type="string" default="" required="no">
          <cfargument name="inpPassword" type="string" required="yes">

          <cfset var DataOut = {} />
          <cfset var ClientUserId = ""/>
          <cfset var FreePlanCredits = 100/>
          <cfset var UnlimitedCredit = 0 />
          <cfset var addPromotionID = 0/>
          <cfset var addPromotionLatestVersion = 0/>
          <cfset var addPromotionCredit = 0/>

          <cfset var NewUserPermissions = ArrayToList(BasicPermission, ",") />

          <cfset var TodayDate = NOW()>
          <cfset var StartDay = Day(TodayDate)>

          <cfif StartDay GT 28>
              <cfset StartDay = 28>
          </cfif>

          <cfset var StartMonth = Month(TodayDate)>
          <cfset var StartYear = Year(TodayDate)>
          <cfset var StartDate = CreateDate(StartYear, StartMonth, StartDay)>
          <cfset var EndDate = DateAdd("m", 1, StartDate)>

          <cfset var BillingType = 1/>

          <cfset DataOut.RXRESULTCODE = 1 />
          <cfset DataOut.TYPE = "" />
          <cfset DataOut.MESSAGE = "RegisterNewSimpleAccount query failed to add user to useraccount table" />
          <cfset DataOut.ERRMESSAGE = "" />

          <cfif Session.AgencyId GT 0>
               <cftry>
                    <cfquery name="RegisterNewSimpleAccount" datasource="#Session.DBSourceEBM#" result="ResultSetRegister">
                         INSERT INTO
                              simpleobjects.useraccount
                              (
                              Password_vch,
                              amd_dt,
                              UserLevel_int ,
                              FirstName_vch,
                              LastName_vch,

                              CompanyName_vch,
                              PrimaryPhoneStrVerified_ti,
                              YearlyRenew_ti,
                              Created_dt,
                              Active_int,

                              CompanyUserId_int,
                              timezone_vch,
                              MFAContactType_ti,
                              MFAEnabled_ti,
                              SecurityQuestionEnabled_ti,

                              AllowNotRealPhone_ti,
                              CaptureCCInfo_ti,
                              PreAccountStep_ti,
                              IntegrateHigherUserID,
                              UserType_int,

                              WhiteGloveStatus_ti,
                              PaymentGateway_ti,
                              MarketingSource_vch,
                              IsAffiliateSource_ti
                              )
                         VALUES
                              (
                              AES_ENCRYPT('#inpPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                              NOW(),
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFName#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLName#">,

                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCompanyName#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                              NOW(),
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,

                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="31">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,

                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,

                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="AgencyId: #Session.AgencyId#">,
                              <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
                              )
                    </cfquery>

                    <cfset ClientUserId = ResultSetRegister.GENERATED_KEY />
                    <cfset DataOut.RXRESULTCODE = "1" />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "" />
                    <cfset DataOut.ERRMESSAGE = "" />

                    <cfcatch type="any">
                         <cfset DataOut.RXRESULTCODE = -1 />
                         <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                         <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                         <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
                    </cfcatch>
               </cftry>

               <cfif len(ClientUserId)>
                    <cfinvoke component="session.cfc.administrator.permission" method="setPermission">
                         <cfinvokeargument name="UCID" value="#ClientUserId#">
                         <cfinvokeargument name="RoleType" value="2">
                         <cfinvokeargument name="Permission" value="#NewUserPermissions#">
                    </cfinvoke>

                    <cftry>
                         <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                               INSERT INTO
                                   simplebilling.billing
                                   (
                                       UserId_int,
                                       Balance_int,
                                       UnlimitedBalance_ti,
                                       RateType_int,
                                       Rate1_int,
                                       Rate2_int,
                                       Rate3_int,
                                       Increment1_int,
                                       Increment2_int,
                                       Increment3_int,
                                       PromotionId_int,
                                       PromotionLastVersionId_int,
                                       PromotionCreditBalance_int
                                    )
                                VALUES
                                    (
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ClientUserId#">,
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#FreePlanCredits#"> ,
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UnlimitedCredit#">,
                                       1,
                                       1,
                                       1,
                                       1,
                                       30,
                                       30,
                                       30,
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionID#"> ,
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionLatestVersion#"> ,
                                       <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#addPromotionCredit#">
                                       )
                           </cfquery>

                           <cfset DataOut.RXRESULTCODE = "1" />
                           <cfset DataOut.TYPE = "" />
                           <cfset DataOut.MESSAGE = "" />
                           <cfset DataOut.ERRMESSAGE = "" />

                           <cfcatch type="any">
                                <cfset DataOut.RXRESULTCODE = -1 />
                                <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                                <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                                <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
                           </cfcatch>
                    </cftry>

                    <cfinvoke method="insertUsersPlan" component="session.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                         <cfinvokeargument name="inpPlanId" value="1">
                         <cfinvokeargument name="inpUserId" value="#ClientUserId#">
                         <cfinvokeargument name="inpStarDate" value="#StartDate#">
                         <cfinvokeargument name="inpEndDate" value="#EndDate#">
                         <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#EndDate#">
                         <cfinvokeargument name="inpBillingType" value="1">
                    </cfinvoke>

                    <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                         <cfinvokeargument name="userId" value="#ClientUserId#">
                         <cfinvokeargument name="moduleName" value="Add new Account">
                         <cfinvokeargument name="operator" value="Fully managed agency account was registered by #Session.EmailAddress#">
                    </cfinvoke>

                    <cfinvoke method="AssignSireSharedCSC" component="Session.cfc.csc.csc" returnvariable="RetVarAssignSireSharedCSC">
                         <cfinvokeargument name="inpUserId" value="#ClientUserId#">
                         <cfinvokeargument name="inpKeywordLimit" value="1000">
                    </cfinvoke>

                    <cftry>
                         <cfquery name="AddClient" datasource="#Session.DBSourceEBM#">
                              INSERT INTO
                                   simpleobjects.agencyclients (AgencyId_int, UserId_int, Created_dt)
                              VALUES
                                   (
                                   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.AgencyId#">,
                                   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ClientUserId#">,
                                   NOW()
                                   )
                         </cfquery>

                         <cfset DataOut.RXRESULTCODE = "1" />
                         <cfset DataOut.TYPE = "" />
                         <cfset DataOut.MESSAGE = "" />
                         <cfset DataOut.ERRMESSAGE = "" />

                         <cfcatch type="any">
                              <cfset DataOut.RXRESULTCODE = -1 />
                              <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                              <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                              <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
                         </cfcatch>
                    </cftry>
               </cfif>
          </cfif>

          <cftry>
               <cfset dataMail = {
                   FirstName: "#inpFName#",
                   Password: "#inpPassword#"
               }/>

               <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" >
                    <cfinvokeargument name="to" value="#Session.EmailAddress#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Your Sire Account Password">
                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_agency_send_password.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
               </cfinvoke>

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

               <cfcatch>
                    <cfset DataOut.RXRESULTCODE = -1 />
                    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
                    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
          </cftry>

          <cfreturn DataOut />
     </cffunction>

     <cffunction name="TestMail" access="remote" output="false">

          <cfargument name="inpFName" type="string" required="no" default="monkey">
          <cfargument name="inpPassword" required="no" type="string" default="hellomonkey2">

          <cfset dataMail = {
              FirstName: "#inpFName#",
              password: "#Session.EmailAddress#"
          }/>

               <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" >
                    <cfinvokeargument name="to" value="mmemarian@siremobile.com">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Your Sire Account Password">
                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_agency_send_password.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
               </cfinvoke>
     </cffunction>



</cfcomponent>
