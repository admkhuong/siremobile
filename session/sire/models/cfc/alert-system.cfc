<cfcomponent>
	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cffunction name="webAlertMessages2SignInUser" access="remote" output="false" hint="return web alert messeages for user">
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.MESSAGES = {} />

		<cftry>
			
			<cfif structKeyExists(Session, "webAlertMessages") AND isStruct(Session.webAlertMessages) AND StructCount(Session.webAlertMessages) GT 0>
				<cfset dataout.MESSAGES = Session.webAlertMessages/>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>
	
	<cffunction name="hideWebAlertMessages" access="remote" output="false" hint="hide web alert messeages for user">
		<cfargument name="inpMessageId" type="numeric" required="true" hint="alert message id"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var message = '' />
		
		<cftry>

			<cfquery datasource="#session.DBSourceEBM#">
				UPDATE
					simpleobjects.alert2users
				SET
					WebAlertHide_int = 1,
					Read_int = Read_int + 1,
					Read_dt = NOW()
				WHERE
					PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMessageId#"/>
				AND
					WebAlertHide_int = 0
			</cfquery>

			<cfif structKeyExists(Session, "webAlertMessages") AND isStruct(Session.webAlertMessages) AND StructCount(Session.webAlertMessages) GT 0>
				<cfloop collection="#Session.webAlertMessages#" item="message">
					<cfif Session.webAlertMessages[message][1] EQ arguments.inpMessageId>
						<cfset StructDelete(Session.webAlertMessages, message) />
					</cfif>
				</cfloop>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="sendAlertByCampaign" access="public" output="false" hint="send alert by campaign id">
		<cfargument name="alertCampaignId" type="numeric" required="true" hint="alert campaign id"/>
		<cfargument name="trigger" type="numeric" required="false" default="#ALERT_TRIGGER_SEND_NOW#" hint="trigger type"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var alertMessages = '' />
		<cfset var insertAlertMessages = '' />

		<cftry>

			<cfquery name="alertMessages" datasource="#session.DBSourceEBM#" result="insertAlertMessages">
				INSERT INTO
					simpleobjects.alert2users
					(
						CampaignId_bi,
						CampaignName_vch,
						SentTo_int,
						Trigger_bi,
						MessageId_bi,
						MessageSubject_vch,
						Content_vch,
						SegmentId_bi,
						-- SegmentName_vch,
						UserId_int,
						ReceiverId_int,
						ReceiverEmailAddress_vch,
						ReceiverPhoneStr_vch,
						Created_dt,
						Status_int,
						Expire_dt
					)
				SELECT
					CampaignId_bi,
					CampaignName_vch,
					SentTo_int,
					Trigger_bi,
					MessageId_bi,
					MessageSubject_vch,
					Content_vch,
					SegmentId_bi,
					-- SegmentName_vch,
					UserId_int,
					ReceiverId_int,
					ReceiverEmailAddress_vch,
					ReceiverPhoneStr_vch,
					Created_dt,
					Status_int,
					Expire_dt
				FROM
					(
					SELECT
						T1.CampaignId_bi,
						T1.CampaignName_vch,
						T1.SentTo_int & T2.SentTo_int AS SentTo_int,
						T1.Trigger_bi,
						T1.MessageId_bi,
						T1.MessageSubject_vch,
						CASE 
							WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_WEB_ALERT# THEN TRIM(T1.Web_Alert_Content_vch)
							WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_EMAIL# THEN TRIM(T1.Email_Content_vch)
							WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_SMS# THEN TRIM(T1.SMS_Content_vch)
						END AS Content_vch,
						T1.SegmentId_bi,
						-- T1.SegmentName_vch,
						T1.UserId_int,
						T1.ReceiverId_int,
						T1.ReceiverEmailAddress_vch,
						T1.ReceiverPhoneStr_vch,
						NOW() AS Created_dt,
						1 AS Status_int,
						T1.Expire_dt
					FROM
						(
						SELECT
							T.CampaignId_bi,
							T.CampaignName_vch,
							T.SentTo_int,
							T.Trigger_bi,
							T.MessageId_bi,
							T.MessageSubject_vch,
							T.SMS_Content_vch,
							T.Web_Alert_Content_vch,
							T.Email_Content_vch,
							T.SegmentId_bi,
							-- T.SegmentName_vch,
							T.UserId_int,
							T.ReceiverId_int,
							T.ReceiverEmailAddress_vch,
							T.ReceiverPhoneStr_vch,
							T.Expire_dt,
							L.PKId_bi AS Alert2UserId
						FROM
							(
							SELECT 
								C.PKId_bi AS CampaignId_bi,
								C.CampaignName_vch,
								C.SentTo_int,
								C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> AS Trigger_bi,
								M.PKId_bi AS MessageId_bi,
								M.Subject_vch AS MessageSubject_vch,
								M.SMS_Content_vch,
								M.Web_Alert_Content_vch,
								M.Email_Content_vch,
								C.SegmentId_bi,
								-- S.SegmentName_vch,
								C.UserId_int,
								A.UserId_int AS ReceiverId_int,
								A.EmailAddress_vch AS ReceiverEmailAddress_vch,
								A.HomePhoneStr_vch AS ReceiverPhoneStr_vch,
								-- LEAST(IF(C.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), C.Expire_dt), IF(M.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), M.Expire_dt)) AS Expire_dt
								LEAST(IF(C.Expire_dt IS NULL, M.Expire_dt, C.Expire_dt), M.Expire_dt) AS Expire_dt
							FROM
								simpleobjects.alertcampaign AS C
							INNER JOIN
								simpleobjects.alertmessage AS M
								ON
									C.MessageId_bi = M.PKId_bi
								AND
									C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.alertCampaignId#"/>
								AND
									C.Status_int = 1
								AND
									C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> > 0
								AND
									(C.Expire_dt IS NULL OR C.Expire_dt > NOW())
								AND
									M.Status_int = 1
								AND
									(M.Expire_dt IS NULL OR M.Expire_dt > NOW())
							INNER JOIN
								(
								SELECT 
									a.*
								FROM 
									simpleobjects.useraccount AS a
								INNER JOIN 
									simplebilling.userplans AS up
								ON 
									a.UserId_int = up.UserId_int
								GROUP BY
									a.UserId_int
								HAVING
									(a.CBPID_int = 1 OR a.CBPID_int IS NULL)
								AND 
									a.Active_int = 1
								) AS A
							ON
								(C.ReceiverId_int = A.UserId_int OR ((C.SegmentId_bi = 0 OR C.SegmentId_bi IS NULL) AND (C.ReceiverId_int = 0 OR C.ReceiverId_int IS NULL)))
							
							UNION ALL
							
							SELECT 
								C.PKId_bi AS CampaignId_bi,
								C.CampaignName_vch,
								C.SentTo_int,
								C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> AS Trigger_bi,
								M.PKId_bi AS MessageId_bi,
								M.Subject_vch AS MessageSubject_vch,
								M.SMS_Content_vch,
								M.Web_Alert_Content_vch,
								M.Email_Content_vch,
								C.SegmentId_bi,
								-- S.SegmentName_vch,
								C.UserId_int,
								A.UserId_int AS ReceiverId_int,
								A.EmailAddress_vch AS ReceiverEmailAddress_vch,
								A.HomePhoneStr_vch AS ReceiverPhoneStr_vch,
								-- LEAST(IF(C.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), C.Expire_dt), IF(M.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), M.Expire_dt)) AS Expire_dt
								LEAST(IF(C.Expire_dt IS NULL, M.Expire_dt, C.Expire_dt), M.Expire_dt) AS Expire_dt
							FROM
								simpleobjects.alertcampaign AS C
							INNER JOIN
								simpleobjects.alertmessage AS M
								ON
									C.MessageId_bi = M.PKId_bi
								AND
									C.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.alertCampaignId#"/>
								AND
									C.Status_int = 1
								AND
									C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> > 0
								AND
									(C.Expire_dt IS NULL OR C.Expire_dt > NOW())
								AND
									M.Status_int = 1
								AND
									(M.Expire_dt IS NULL OR M.Expire_dt > NOW())
							INNER JOIN
								simpleobjects.alertsegment AS S
								ON
									C.SegmentId_bi = S.PKId_bi
								AND
									S.Status_int = 1
							INNER JOIN
								simpleobjects.alertsegmentusers AS U
								ON
									S.PKId_bi = U.SegmentId_bi
							INNER JOIN
								(
								SELECT 
									a.*
								FROM 
									simpleobjects.useraccount AS a
								INNER JOIN 
									simplebilling.userplans AS up
								ON 
									a.UserId_int = up.UserId_int
								GROUP BY
									a.UserId_int
								HAVING
									(a.CBPID_int = 1 OR a.CBPID_int IS NULL)
								AND 
									a.Active_int = 1
								) AS A
							ON
								U.UserId_int = A.UserId_int
							) AS T

						LEFT OUTER JOIN 
							simpleobjects.alert2users AS L
							ON
								T.CampaignId_bi = L.CampaignId_bi -- T.MessageId_bi = L.MessageId_bi
							AND 
								T.ReceiverId_int = L.ReceiverId_int
							AND
								L.Status_int = 1
							AND
								(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
							AND 
								T.SentTo_int & L.SentTo_int > 0
						GROUP BY
							MessageId_bi, ReceiverId_int
						HAVING
							Alert2UserId IS NULL
						) AS T1
					INNER JOIN
						(
							SELECT #ALERT_SEND_TO_WEB_ALERT# AS SentTo_int
							UNION ALL
							SELECT #ALERT_SEND_TO_EMAIL# AS SentTo_int
							UNION ALL
							SELECT #ALERT_SEND_TO_SMS# AS SentTo_int
						) AS T2
					ON
						T1.SentTo_int & T2.SentTo_int > 0
					) AS T3
				WHERE
					Content_vch != ''
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>


	<cffunction name="sendAlert2User" access="public" output="false" hint="send alert to user id">
		<cfargument name="userId" type="numeric" required="true" hint="user id"/>
		<cfargument name="trigger" type="numeric" required="false" default="#ALERT_TRIGGER_SEND_NOW#" hint="trigger type"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var userSignIn = '' />
		<cfset var alertMessages = '' />
		<cfset var insertAlertMessages = '' />

		<cftry>

			<cfquery name="userSignIn" datasource="#session.DBSourceREAD#">
				SELECT 
					UserId_int,
					EmailAddress_vch,
					HomePhoneStr_vch
				FROM 
					simpleobjects.useraccount
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#"/>
				AND 
					(CBPID_int = 1 OR CBPID_int IS NULL)
				AND 
					Active_int = 1
			</cfquery>

			<cfif userSignIn.RecordCount GT 0>

				<cfquery name="alertMessages" datasource="#session.DBSourceEBM#" result="insertAlertMessages">
					INSERT INTO
						simpleobjects.alert2users
						(
							CampaignId_bi,
							CampaignName_vch,
							SentTo_int,
							Trigger_bi,
							MessageId_bi,
							MessageSubject_vch,
							Content_vch,
							SegmentId_bi,
							-- SegmentName_vch,
							UserId_int,
							ReceiverId_int,
							ReceiverEmailAddress_vch,
							ReceiverPhoneStr_vch,
							Created_dt,
							Status_int,
							Expire_dt
						)
					SELECT
						CampaignId_bi,
						CampaignName_vch,
						SentTo_int,
						Trigger_bi,
						MessageId_bi,
						MessageSubject_vch,
						Content_vch,
						SegmentId_bi,
						-- SegmentName_vch,
						UserId_int,
						ReceiverId_int,
						ReceiverEmailAddress_vch,
						ReceiverPhoneStr_vch,
						Created_dt,
						Status_int,
						Expire_dt
					FROM
						(
						SELECT
							T1.CampaignId_bi,
							T1.CampaignName_vch,
							T1.SentTo_int & T2.SentTo_int AS SentTo_int,
							T1.Trigger_bi,
							T1.MessageId_bi,
							T1.MessageSubject_vch,
							CASE 
								WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_WEB_ALERT# THEN TRIM(T1.Web_Alert_Content_vch)
								WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_EMAIL# THEN TRIM(T1.Email_Content_vch)
								WHEN T1.SentTo_int & T2.SentTo_int = #ALERT_SEND_TO_SMS# THEN TRIM(T1.SMS_Content_vch)
							END AS Content_vch,
							T1.SegmentId_bi,
							-- T1.SegmentName_vch,
							T1.UserId_int,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"/> AS ReceiverId_int,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userSignIn.EmailAddress_vch#"/> AS ReceiverEmailAddress_vch,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userSignIn.HomePhoneStr_vch#"/> AS ReceiverPhoneStr_vch,
							NOW() AS Created_dt,
							1 AS Status_int,
							T1.Expire_dt
						FROM
							(
							SELECT 
								C.PKId_bi AS CampaignId_bi,
								C.CampaignName_vch,
								C.SentTo_int,
								C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> AS Trigger_bi,
								M.PKId_bi AS MessageId_bi,
								M.Subject_vch AS MessageSubject_vch,
								M.SMS_Content_vch,
								M.Web_Alert_Content_vch,
								M.Email_Content_vch,
								C.SegmentId_bi,
								-- S.SegmentName_vch,
								C.UserId_int,
								-- LEAST(IF(C.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), C.Expire_dt), IF(M.Expire_dt IS NULL, ADDDATE(NOW(), #ALERT_EXPIRE_DEFAULT#), M.Expire_dt)) AS Expire_dt,
								LEAST(IF(C.Expire_dt IS NULL, M.Expire_dt, C.Expire_dt), M.Expire_dt) AS Expire_dt,
								L.PKId_bi AS Alert2UserId
							FROM
								simpleobjects.alertcampaign AS C
							INNER JOIN
								simpleobjects.alertmessage AS M
								ON
									C.MessageId_bi = M.PKId_bi
								AND
									C.Status_int = 1
								AND
									C.Trigger_bi & <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.trigger#"/> > 0
								AND
									(C.Expire_dt IS NULL OR C.Expire_dt > NOW())
								AND
									M.Status_int = 1
								AND
									(M.Expire_dt IS NULL OR M.Expire_dt > NOW())
								AND
									(
										(
											(
												C.SegmentId_bi = 0 
											OR 
												C.SegmentId_bi IS NULL
											)
										AND 
											(
												C.ReceiverId_int = 0 
											OR 
												C.ReceiverId_int IS NULL
											)
										)
									OR
										C.ReceiverId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#"/>
									OR
										C.SegmentId_bi IN (
											SELECT
												S.PKId_bi
											FROM
												simpleobjects.alertsegment AS S
											INNER JOIN
												simpleobjects.alertsegmentusers AS U
												ON
													S.PKId_bi = U.SegmentId_bi
												AND
													S.Status_int = 1
												AND
													U.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#"/>
										)
									)
							<!---/*LEFT JOIN
								simpleobjects.alertsegment AS S
								ON
									C.SegmentId_bi = S.PKId_bi
								AND
									S.Status_int = 1
							LEFT JOIN
								simpleobjects.alertsegmentusers AS U
								ON
									S.PKId_bi = U.SegmentId_bi
								AND 
									U.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"/>*/--->
							LEFT OUTER JOIN 
								simpleobjects.alert2users AS L
								ON
									C.PKId_bi = L.CampaignId_bi -- M.PKId_bi = L.MessageId_bi
								AND 
									L.ReceiverId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#"/>
								AND
									L.Status_int = 1
								AND
									(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
								AND 
									C.SentTo_int & L.SentTo_int > 0
							GROUP BY
								MessageId_bi
							HAVING
								Alert2UserId IS NULL
							) AS T1
						INNER JOIN
							(
								SELECT #ALERT_SEND_TO_WEB_ALERT# AS SentTo_int
								UNION ALL
								SELECT #ALERT_SEND_TO_EMAIL# AS SentTo_int
								UNION ALL
								SELECT #ALERT_SEND_TO_SMS# AS SentTo_int
							) AS T2
						ON
							T1.SentTo_int & T2.SentTo_int > 0
						) AS T3
					WHERE
						Content_vch != ''
				</cfquery>

			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>