<cfcomponent>

	<cfparam name="Session.DBSourceEBM" default="BISHOP"/>
	<cfinclude template="../../configs/credits.cfm">
    <cfinclude template="/session/sire/configs/paths.cfm">

	<cfinclude template="/session/cfc/csc/inc-billing-sire.cfm">
	
    <cffunction name="GetBalance" access="public" output="false" hint="get balance information">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfset var dataout = {} />
        <cfset var GetCurrentBalance = 0 />
        <cfset var StartNewBalance = 0 />
        <cfset var TransLog = 0 />
        <cfset var TotalBlance = 0/>
        <cfoutput>
            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, PLANCREDITBALANCE,BUYCREDITBALANCE,PROMOTIONCREDITBALANCE,RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
            <cfset QuerySetCell(dataout, "BALANCE", "0") />
            <cfset QuerySetCell(dataout, "PLANCREDITBALANCE", "0") />
            <cfset QuerySetCell(dataout, "BUYCREDITBALANCE", "0") />
            <cfset QuerySetCell(dataout, "PROMOTIONCREDITBALANCE", "0") />
            <cfset QuerySetCell(dataout, "RATETYPE", "1") />
            <cfset QuerySetCell(dataout, "RATE1", "100") />
            <cfset QuerySetCell(dataout, "RATE2", "100") />
            <cfset QuerySetCell(dataout, "RATE3", "100") />
            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />
            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />
            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            <cftry>
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif arguments.inpUserId GT 0>
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->
                    <!--- Get description for each group--->
                    <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">
                        SELECT
                            Balance_int,
                            BuyCreditBalance_int,
                            PromotionCreditBalance_int,
                            UnlimitedBalance_ti,
                            RATETYPE_int,
                            RATE1_int,
                            RATE2_int,
                            RATE3_int,
                            INCREMENT1_int,
                            INCREMENT2_int,
                            INCREMENT3_int
                        FROM
                           simplebilling.billing
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    </cfquery>
                    <cfif GetCurrentBalance.RecordCount GT 0>
                        <cfset TotalBlance = GetCurrentBalance.BALANCE_int + GetCurrentBalance.BuyCreditBalance_int + GetCurrentBalance.PromotionCreditBalance_int>
                        <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE,PLANCREDITBALANCE,BUYCREDITBALANCE,PROMOTIONCREDITBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
                        <cfset QuerySetCell(dataout, "BALANCE", "#TotalBlance#") />
                        <cfset QuerySetCell(dataout, "PLANCREDITBALANCE", "#GetCurrentBalance.BALANCE_int#") />
                        <cfset QuerySetCell(dataout, "BUYCREDITBALANCE", "#GetCurrentBalance.BuyCreditBalance_int#") />
                        <cfset QuerySetCell(dataout, "PROMOTIONCREDITBALANCE", "#GetCurrentBalance.PromotionCreditBalance_int#") />
                        <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />
                        <cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />
                        <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />
                        <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />
                        <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />
                        <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />
                        <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                        <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                        <cfreturn dataout />
                    <cfelse>
                        <!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">

                                INSERT INTO
                                simplebilling.billing
                                (
                                    UserId_int,
                                    Balance_int,
                                    RATETYPE_int,
                                    RATE1_int,
                                    RATE2_int,
                                    RATE3_int,
                                    INCREMENT1_int,
                                    INCREMENT2_int,
                                    INCREMENT3_int,
                                    UnlimitedBalance_ti
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="0">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="1">,
                                    1000,
                                    1000,
                                    1000,
                                    30,
                                    30,
                                    30 ,
                                    0
                                )
                        </cfquery>
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="0">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()
                                )
                        </cfquery>
                        <!--- Get current values --->
                        <cfquery name="GetCurrentBalance" datasource="#Session.DBSourceEBM#">
                            SELECT
                                Balance_int,
                                BuyCreditBalance_int,
                                PromotionCreditBalance_int,
                                UnlimitedBalance_ti,
                                RATETYPE_int,
                                RATE1_int,
                                RATE2_int,
                                RATE3_int,
                                INCREMENT1_int,
                                INCREMENT2_int,
                                INCREMENT3_int
                            FROM
                               simplebilling.billing
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                        </cfquery>
                        <cfif GetCurrentBalance.RecordCount GT 0>
                            <cfset TotalBlance = GetCurrentBalance.BALANCE_int + GetCurrentBalance.BuyCreditBalance_int + GetCurrentBalance.PromotionCreditBalance_int>
                            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, PLANCREDITBALANCE,BUYCREDITBALANCE,PROMOTIONCREDITBALANCE,RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "#TotalBlance#") />
                            <cfset QuerySetCell(dataout, "PLANCREDITBALANCE", "#GetCurrentBalance.BALANCE_int#") />
                            <cfset QuerySetCell(dataout, "BUYCREDITBALANCE", "#GetCurrentBalance.BuyCreditBalance_int#") />
                            <cfset QuerySetCell(dataout, "PROMOTIONCREDITBALANCE", "#GetCurrentBalance.PromotionCreditBalance_int#") />
                            <cfset QuerySetCell(dataout, "RATETYPE", "#GetCurrentBalance.RATETYPE_int#") />
                            <cfset QuerySetCell(dataout, "RATE1", "#GetCurrentBalance.RATE1_int#") />
                            <cfset QuerySetCell(dataout, "RATE2", "#GetCurrentBalance.RATE2_int#") />
                            <cfset QuerySetCell(dataout, "RATE3", "#GetCurrentBalance.RATE3_int#") />
                            <cfset QuerySetCell(dataout, "INCREMENT1", "#GetCurrentBalance.INCREMENT1_int#") />
                            <cfset QuerySetCell(dataout, "INCREMENT2", "#GetCurrentBalance.INCREMENT2_int#") />
                            <cfset QuerySetCell(dataout, "INCREMENT3", "#GetCurrentBalance.INCREMENT3_int#") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "#GetCurrentBalance.UnlimitedBalance_ti#") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                        <cfelse>
                            <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
                            <cfset QuerySetCell(dataout, "BALANCE", "0") />
                            <cfset QuerySetCell(dataout, "RATETYPE", "1") />
                            <cfset QuerySetCell(dataout, "RATE1", "100") />
                            <cfset QuerySetCell(dataout, "RATE2", "100") />
                            <cfset QuerySetCell(dataout, "RATE3", "100") />
                            <cfset QuerySetCell(dataout, "INCREMENT1", "100") />
                            <cfset QuerySetCell(dataout, "INCREMENT2", "100") />
                            <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
                            <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, BALANCE, PLANCREDITBALANCE,BUYCREDITBALANCE,PROMOTIONCREDITBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
                    <cfset QuerySetCell(dataout, "BALANCE", "0") />
                    <cfset QuerySetCell(dataout, "PLANCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "BUYCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "PROMOTIONCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />
                    <cfset QuerySetCell(dataout, "RATE1", "100") />
                    <cfset QuerySetCell(dataout, "RATE2", "100") />
                    <cfset QuerySetCell(dataout, "RATE3", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
                    <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                </cfif>
                <cfcatch TYPE="any">
                    <cfset dataout =  QueryNew("RXRESULTCODE, UID, BALANCE, PLANCREDITBALANCE,BUYCREDITBALANCE,PROMOTIONCREDITBALANCE, RATETYPE, RATE1, RATE2, RATE3, INCREMENT1, INCREMENT2, INCREMENT3, UNLIMITEDBALANCE, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                    <cfset QuerySetCell(dataout, "UID", "#arguments.inpUserId#") />
                    <cfset QuerySetCell(dataout, "BALANCE", "0") />
                    <cfset QuerySetCell(dataout, "PLANCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "BUYCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "PROMOTIONCREDITBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "RATETYPE", "1") />
                    <cfset QuerySetCell(dataout, "RATE1", "100") />
                    <cfset QuerySetCell(dataout, "RATE2", "100") />
                    <cfset QuerySetCell(dataout, "RATE3", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT1", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT2", "100") />
                    <cfset QuerySetCell(dataout, "INCREMENT3", "100") />
                    <cfset QuerySetCell(dataout, "UNLIMITEDBALANCE", "0") />
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                </cfcatch>
            </cftry>
        </cfoutput>
        <cfreturn dataout />
    </cffunction>


    <cffunction name="GetBalanceCurrentUser" access="public" output="false" hint="Get simple billing balance to the tenth of a penny">
		<cfset var RetGetBalance = ''/>
		<cfinvoke method="GetBalance" returnvariable="RetGetBalance">
			<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
		</cfinvoke>
		<cfreturn RetGetBalance />
	</cffunction>

    <cffunction name="GetBalanceCurrentUserRemote" access="remote" output="false" hint="Get simple billing balance to the tenth of a penny">
        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.BALANCE = '0 credit' />
        <cfset var RetGetBalance = ''/>
        <cftry>
            <cfinvoke method="GetBalance" returnvariable="RetGetBalance">
                <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
            </cfinvoke>
            <cfif RetGetBalance.RXRESULTCODE GT 0>
                <cfif RetGetBalance.Balance LT 1000000 >
                    <cfset dataout.BALANCE = NumberFormat(RetGetBalance.Balance,',')>
                 <cfelseif RetGetBalance.Balance LT 1000000000>
                    <cfset dataout.BALANCE = NumberFormat(RetGetBalance.Balance \ 1000000,',') & " M">
                 <cfelse>   
                    <cfset dataout.BALANCE = NumberFormat(RetGetBalance.Balance \ 1000000000,',') & " B">
                </cfif>
                <cfif RetGetBalance.Balance LTE 1>
                    <cfset dataout.BALANCE = dataout.BALANCE & " credit"/>
                <cfelse>
                    <cfset dataout.BALANCE = dataout.BALANCE & " credits"/>
                </cfif>
                <cfset dataout.RXRESULTCODE = 1/>
            </cfif>
            <cfcatch>
                <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

	<cffunction name="CreateCustomer" hint="create customer in sercuner vault">
		<cfargument name="campaignName" type="string" required="yes">
	</cffunction>

	<cffunction name="CheckExistsPaymentMethod" access="remote" hint="Check Exits Payment Method">
		<cfargument name="userId" required="no" default="#Session.USERID#" >
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset var CheckExistsPaymentMethod=''/>
		<cfset var rtGetPaymentMethodSetting=''>
		<cfset var paymentGatewayDefault=0>
        <cftry>
			<cfinvoke  method="GetPaymentMethodSettingByUserId" returnvariable="rtGetPaymentMethodSetting">
				<cfinvokeargument name="inpUserId" value="#arguments.userId#"/>
			</cfinvoke>
			<cfset paymentGatewayDefault=rtGetPaymentMethodSetting.RESULT>
			<cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					UserId_int 
				FROM
					simplebilling.authorize_user
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#"> 				
				AND
					PaymentGateway_ti= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#paymentGatewayDefault#"> 	
				AND
					status_ti=1
			</cfquery>			
			<cfif CheckExistsPaymentMethod.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Payment Method Exists" />
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            </cfcatch>
        </cftry>
		<cfreturn dataout />

	</cffunction>
	<cffunction name="RetrieveCustomer" access="remote" hint="Retrieves a customer record from the Vault.">
		<cfargument name="userId" default="#Session.USERID#" >
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.USERID = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.CUSTOMERINFO = "" />
		<cfset dataout.PAYMENTRESULT = "" />
		<cfset var userId = arguments.userId>
		<cfset var customerInfo =  QueryNew("firstName, lastName, emailAddress,primaryPaymentMethodId,expirationDate,maskedNumber,line1, city, state, zip, country, phone,paymentType, cvv, creditCardType, lastFourDigits, method")>
		<cfset QueryAddRow(customerInfo) />
		<cfset var paymentRequestResult = structNew() />

		<cfset var tickBegin = GetTickCount()>
		<cfhttp url="#payment_get_customer_payment##userId#" method="GET" result="paymentRequestResult" >
			<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
		</cfhttp>
		<!--- <cfset paymentRequestResult.filecontent = '{"vaultCustomer":{"customerId":"ERROR","address":{"line1":"","city":"","state":"","zip":"","country":"","company":null,"phone":"","firstName":null,"lastName":null},"firstName":"","lastName":"","phoneNumber":"","emailAddress":"","sendEmailReceipts":false,"company":"","notes":"Customer ID does not exist or invalid.","userDefinedFields":[],"paymentMethods":[],"primaryPaymentMethodId":null,"variablePaymentPlans":[],"recurringPaymentPlans":[],"installmentPaymentPlans":[]},"customerId":"ERROR","success":true,"result":"APPROVED","responseCode":1,"message":"SUCCESS","responseDateTime":"2017-02-16T14:25:01.87Z","rawRequest":null,"rawResponse":null,"jsonRequest":null,"emvResponse":null}'/> --->
		<cfif trim(paymentRequestResult.filecontent) NEQ "" AND IsJSON(paymentRequestResult.filecontent)>
			<cfset var responseData = deserializeJSON(paymentRequestResult.filecontent)>
			<cfif NOT isNUll(responseData.customerId) AND isNumeric(responseData.customerId) AND responseData.customerId GT 0 AND responseData.responseCode EQ 1>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Call RetrieveCustomer Successfully" />
                <cfset dataout.USERID = responseData.customerId />
				<cfset dataout.PAYMENTRESULT = paymentRequestResult.filecontent />

				<cfset var vaultCustomer = responseData.vaultCustomer>
				<cfif !arrayIndexExists(vaultCustomer.paymentMethods, "1")>
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.MESSAGE = "Call RetrieveCustomer Fail" />
					<cfset dataout.ERRMESSAGE = "Call RetrieveCustomer Fail" />
					<cfreturn dataout/>
				</cfif>
				<cfset var dataPaymentMethods = vaultCustomer.paymentMethods[1]>
				<!--- Always get first as primary--->
				<cfif NOT isNUll(dataPaymentMethods.card)>
					<cfset var dataPaymentMethodsType = dataPaymentMethods.card>
					<cfset QuerySetCell(customerInfo, "paymentType", 'card') />
					<cfset QuerySetCell(customerInfo, "maskedNumber", dataPaymentMethodsType.maskedNumber) />
					<cfset QuerySetCell(customerInfo, "expirationDate", dataPaymentMethodsType.expirationDate) />
                    <cfset QuerySetCell(customerInfo, "cvv", '') />
                    <cfset QuerySetCell(customerInfo, "creditCardType", dataPaymentMethods.card.creditCardType) />
					<cfset QuerySetCell(customerInfo, "lastFourDigits", dataPaymentMethods.card.lastFourDigits) />
					<cfif dataPaymentMethodsType.firstName NEQ ''>
						<cfset QuerySetCell(customerInfo, "firstName", dataPaymentMethodsType.firstName) />
					<cfelse>
						<cfset QuerySetCell(customerInfo, "firstName", vaultCustomer.firstName) />
					</cfif>
					<cfif dataPaymentMethodsType.lastName NEQ ''>
						<cfset QuerySetCell(customerInfo, "lastName", dataPaymentMethodsType.lastName) />
					<cfelse>
						<cfset QuerySetCell(customerInfo, "lastName", vaultCustomer.lastName) />
					</cfif>
					<!---
						<cfif dataPaymentMethodsType.email NEQ ''>
						<cfset QuerySetCell(customerInfo, "emailAddress", dataPaymentMethodsType.email) />
					<cfelse>
						<cfset QuerySetCell(customerInfo, "emailAddress", vaultCustomer.emailAddress) />
						</cfif>
						--->
				<cfelseif NOT isNUll(dataPaymentMethods.check)>
					<cfset dataPaymentMethodsType = dataPaymentMethods.check>
					<cfset dataPaymentMethodsType.expirationDate = ''>
					<cfset QuerySetCell(customerInfo, "maskedNumber", dataPaymentMethodsType.routingNumber) />
					<cfset QuerySetCell(customerInfo, "paymentType", 'check') />
					<cfset QuerySetCell(customerInfo, "cvv", dataPaymentMethodsType.lastFourDigits) />
					<cfset QuerySetCell(customerInfo, "firstName", vaultCustomer.firstName) />
					<cfset QuerySetCell(customerInfo, "lastName", vaultCustomer.lastName) />
					<!---
						<cfif dataPaymentMethodsType.email NEQ ''>
						<cfset QuerySetCell(customerInfo, "emailAddress", dataPaymentMethodsType.email) />
					<cfelse>
						<cfset QuerySetCell(customerInfo, "emailAddress", vaultCustomer.emailAddress) />
						</cfif>
						--->
				<cfelse>
					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = "Customer Data not found" />
					<cfreturn dataout>
				</cfif>
				<cfif dataPaymentMethodsType.email NEQ ''>
					<cfset QuerySetCell(customerInfo, "emailAddress", dataPaymentMethodsType.email) />
				<cfelse>
					<cfset QuerySetCell(customerInfo, "emailAddress", vaultCustomer.emailAddress) />
				</cfif>
				<cfset QuerySetCell(customerInfo, "primaryPaymentMethodId", vaultCustomer.primaryPaymentMethodId) />
				<cfset QuerySetCell(customerInfo, "line1",  dataPaymentMethodsType.address.line1) />
				<cfset QuerySetCell(customerInfo, "city", dataPaymentMethodsType.address.city) />
				<cfset QuerySetCell(customerInfo, "state", dataPaymentMethodsType.address.state) />
				<cfset QuerySetCell(customerInfo, "zip", dataPaymentMethodsType.address.zip) />
				<cfset QuerySetCell(customerInfo, "country", dataPaymentMethodsType.address.country) />
				<cfset QuerySetCell(customerInfo, "phone", dataPaymentMethodsType.address.phone) />
				<cfset QuerySetCell(customerInfo, "method", dataPaymentMethods.method) />
				<cfset dataout.CUSTOMERINFO = customerInfo>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Customer Data not found" />
				<cfreturn dataout>
			</cfif>
		<cfelse>
			<cfset dataout.MESSAGE = "Call RetrieveCustomer Fail" />
			<cfset dataout.ERRMESSAGE = "Call RetrieveCustomer Fail" />
		</cfif>
		<cfreturn dataout>
	</cffunction>


	<cffunction name="UpdateCustomer" access="remote" hint="update a customer record from the Vault.">
		<cfargument name="payment_method" default="0" type="number" required="true">
		<cfargument name="line1" default="" required="true">
		<cfargument name="city" default="" required="true">
		<cfargument name="country" default="" required="true">
		<cfargument name="phone" default="" required="true">
		<cfargument name="state" default="" required="true">
		<cfargument name="zip" default="" required="true">
		<cfargument name="email" default="" required="true">
		<cfargument name="cvv" default="">
		<cfargument name="expirationDate" default="">
		<cfargument name="firstName" default="">
		<cfargument name="lastName" default="">
		<cfargument name="number" type="number" default="0">
		<cfargument name="primaryPaymentMethodId" type="number" default="">
		<cfargument name="userId" default="#Session.USERID#" >
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.USERID = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.CUSTOMERINFO = "" />
		<cfset var userId = arguments.userId>
		<!--- --->
		<!--- GET PAYMENTMETHODID --->
		<cfif primaryPaymentMethodId EQ 0>
			<!--- UPDATE --->
			<cfset var RetCustomerInfo =  "">
			<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">
				<cfinvokeargument name="userId" value="#userId#">
			</cfinvoke>
			<cfif RetCustomerInfo.RXRESULTCODE NEQ 1 >
				<cfset dataout.MESSAGE = "#RetCustomerInfo.MESSAGE#.Please contact support" />
				<cfset dataout.ERRMESSAGE = "#RetCustomerInfo.MESSAGE#.Please contact support" />
				<cfreturn dataout>
			</cfif>
			<cfset var paymentMethodId = RetCustomerInfo.CUSTOMERINFO.primaryPaymentMethodId>
			<cfset paymentData = {
				customerId = userId,
				paymentMethodId = paymentMethodId,
				notes = 'update a vault account userId with payment method #paymentMethodId#',
				customerDuplicateCheckIndicator = 1,
				accountDuplicateCheckIndicator = 1,
				primary = true,
				firstName = arguments.firstName,
				lastName = arguments.lastName,
				phoneNumber = arguments.phone,
				emailAddress = arguments.email,
				sendEmailReceipts: true,
				address         = {
				line1   = arguments.line1,
				city    = arguments.city,
				state   = arguments.state,
				zip     = arguments.zip,
				country = arguments.country,
				phone   = arguments.phone
				},
				developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.Version
				}
				}>
			<cfif payment_method EQ 0>
				<!--- CREDIR CARD --->
				<cfset var card = {
					number          = arguments.number,
					expirationDate  = arguments.expirationDate,
					cvv             = arguments.cvv,
					firstName       = arguments.firstName,
					lastName        = arguments.lastName,
					email           = arguments.email,
					address         = {
					line1   = arguments.line1,
					city    = arguments.city,
					state   = arguments.state,
					zip     = arguments.zip,
					country = arguments.country,
					phone   = arguments.phone
					}
					}>
				<!---
					<cfset var paymentData = {
					customerId = userId,
					paymentMethodId = paymentMethodId,
					notes = 'update a vault account #Session.USERID# with payment method #paymentMethodId#',
					accountDuplicateCheckIndicator = 1,
					primary = true,
					developerApplication = {
					developerId: worldpayApplication.developerId,
					version: worldpayApplication.Version
					}
					}>
					--->
				<cfset paymentData.card = card>
			<cfelseif payment_method EQ 1>
				<!--- CHECK --->
				<cfset var check = {
					routingNumber   = arguments.number,
					accountNumber   = arguments.cvv,
					firstName       = arguments.firstName,
					lastName        = arguments.lastName,
					email           = arguments.email,
					address         = {
					line1   = arguments.line1,
					city    = arguments.city,
					state   = arguments.state,
					zip     = arguments.zip,
					country = arguments.country,
					phone   = arguments.phone
					}
					}>
				<!---
					<cfset paymentData = {
					customerId = userId,
					paymentMethodId = paymentMethodId,
					notes = 'update a vault account userId with payment method #paymentMethodId#',
					accountDuplicateCheckIndicator = 1,
					primary = true,
					developerApplication = {
					developerId: worldpayApplication.developerId,
					version: worldpayApplication.Version
					}
					}>
					--->
				<cfset paymentData.check = check>
			</cfif>
			<cfset var paymentRequestResult = "" />
			<!---
				<cfhttp url="#payment_get_customer_payment##userId#/PaymentMethod/#paymentMethodId#" method="PUT" result="paymentRequestResult">
				<cfhttpparam type="header" name="content-type" value="application/json">
				<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
				<cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
				</cfhttp>
				--->
			<cfhttp url="#payment_get_customer_payment##userId#/payments" method="PUT" result="paymentRequestResult">
				<cfhttpparam type="header" name="content-type" value="application/json">
				<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
				<cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
			</cfhttp>

			<cfif trim(paymentRequestResult.filecontent) NEQ "" AND IsJSON(paymentRequestResult.filecontent)>
				<cfset var responseData = deserializeJSON(paymentRequestResult.filecontent)>
				<cfif responseData.responseCode EQ 1>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "Update Successfully" />
					<cfset var RetUserAuthenInfo = "" />
					<cfinvoke component="session.sire.models.cfc.billing" method="updateUserAuthen" returnvariable="RetUserAuthenInfo">
						<cfinvokeargument name="PaymentMethodID" value="#responseData.assignedPaymentId#"/>
						<cfinvokeargument name="PaymentType" value="#responseData.vaultCustomer.paymentMethods[1].method#"/>
						<cfinvokeargument name="userId" value="#userId#">
					</cfinvoke>
				<cfelse>
					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = "Call Update PaymentMethod Fail. #responseData.message#" />
				</cfif>
			<cfelse>
				<cfset dataout.MESSAGE = "Call Update PaymentMethod Fail" />
			</cfif>
		<cfelse>
			<!--- CREATE NEW --->
			<!---
				<cfset paymentMethodId = createUUID()>
				--->
			<!--- CREATE NEW TOKEN --->
			<cfset var RetToken = "" />
			<cfinvoke component="session.sire.models.cfc.billing" method="CreateToken" returnvariable="RetToken">
				<cfinvokeargument name="payment_method" value="#payment_method#"/>
				<cfinvokeargument name="number" value="#arguments.number#"/>
				<cfinvokeargument name="expirationDate" value="#arguments.expirationDate#"/>
				<cfinvokeargument name="cvv" value="#arguments.cvv#"/>
				<cfinvokeargument name="firstName" value="#arguments.firstName#"/>
				<cfinvokeargument name="lastName" value="#arguments.lastName#"/>
				<cfinvokeargument name="email" value="#arguments.email#"/>
				<cfinvokeargument name="line1" value="#arguments.line1#"/>
				<cfinvokeargument name="city" value="#arguments.city#"/>
				<cfinvokeargument name="state" value="#arguments.state#"/>
				<cfinvokeargument name="zip" value="#arguments.zip#"/>
				<cfinvokeargument name="country" value="#arguments.country#"/>
                <cfinvokeargument name="phone" value="#arguments.phone#"/>
				<cfinvokeargument name="customerId" value="#userId#"/>
			</cfinvoke>

			<cfif RetToken.RXRESULTCODE EQ 1>
				<cfset paymentMethodId = RetToken.TOKEN>
				<!--- GET CUSTOMER INFO --->
				<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">
					<cfinvokeargument name="userId" value="#userId#">
				</cfinvoke>

				<cfif RetCustomerInfo.RXRESULTCODE EQ -1>
					<cfset dataout.MESSAGE = "Not found PaymentMethod.Please contact support" />
					<cfreturn dataout>
				<cfelse>
					<!--- UPDATE CUSTOMER INFO --->
					<cfset paymentData = {
						customerId = userId,
						FirstName = arguments.firstName,
						LastName = arguments.lastName,
						PhoneNumber = arguments.phone,
						EmailAddress = arguments.email,
						SendEmailReceipts = true,
						address         = {
						line1   = arguments.line1,
						city    = arguments.city,
						state   = arguments.state,
						zip     = arguments.zip,
						country = arguments.country,
						phone   = arguments.phone
						},
						notes = 'update a vault account #userId#',
						accountDuplicateCheckIndicator = 1,
						developerApplication = {
						developerId: worldpayApplication.developerId,
						version: worldpayApplication.Version
						}
						}>
                    
					<cfhttp url="#payment_get_customer_payment##userId#/" method="PUT" result="paymentRequestResult">
						<cfhttpparam type="header" name="content-type" value="application/json">
						<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
						<cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
					</cfhttp>

					<!--- UPDATE FAIL STILL INSERT DB --->
					<cfinvoke component="session.sire.models.cfc.billing" method="updateUserAuthen" returnvariable="RetUserAuthenInfo">
						<cfinvokeargument name="PaymentMethodID" value="#paymentMethodId#"/>
						<cfinvokeargument name="PaymentType" value="#RetCustomerInfo.CUSTOMERINFO.method#"/>
						<cfinvokeargument name="userId" value="#userId#">
					</cfinvoke>
                    
					<cfif RetUserAuthenInfo.RXRESULTCODE EQ 1>
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = "Update Successfully" />
						<cfreturn dataout>
					<cfelse>
					</cfif>
				</cfif>
			<cfelse>
				<cfset dataout.MESSAGE = RetToken.MESSAGE />
				<cfreturn dataout>
			</cfif>
		</cfif>
		<cfreturn dataout>
	</cffunction>


	<cffunction name='updateUserAuthen' hint="update user authen for future payment - recurring">
		<cfargument name="PaymentMethodID" required="true">
		<cfargument name="PaymentType" required="true">
		<cfargument name="userId" default="#session.USERID#">
        <cfargument name="intPaymentGateway" required="false" default="1">
		<cfargument name="intExtraNote" required="false" default="">

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cftry>
			<cfset var RetUserAuthenInfo = "" />
			<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="RetUserAuthenInfo">
                <cfinvokeargument name="userId" value="#arguments.userId#">
                <cfinvokeargument name="intPaymentGateway" value="#arguments.intPaymentGateway#">
			</cfinvoke>
            
			<cfif RetUserAuthenInfo.RXRESULTCODE EQ 0>
				<!--- INSERT --->
				<cfset var insertUserAuthen = "" />
				<cfquery name="insertUserAuthen" datasource="#Session.DBSourceEBM#" result="insertUserAuthen">
                    INSERT INTO 
                        simplebilling.authorize_user(PaymentMethodID_vch,PaymentType_vch,UserId_int,paymentGateway_ti,extraNote,status_ti)
                    VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PaymentMethodID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PaymentType#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.intPaymentGateway#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.intExtraNote#">,
						1
                    )
                </cfquery>
				<cfif insertUserAuthen.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = "Success" />
				</cfif>
			<cfelse>
				<!--- UPDATE --->
				<cfset var saveUserAuthen = "" />
				<cfquery name="saveUserAuthen" datasource="#Session.DBSourceEBM#" result="saveUserAuthen">
                    UPDATE 
                        simplebilling.authorize_user
                    SET
                        PaymentMethodID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PaymentMethodID#">,
                        PaymentType_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PaymentType#">,
                        updated_dt = #NOW()#,
						extraNote = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.intExtraNote#">,
						status_ti=1
                    WHERE  
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#">
                    AND 
                        PaymentGateway_ti  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.intPaymentGateway#">       
                </cfquery>
                
				<cfif saveUserAuthen.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = "Success" />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>


	<cffunction name='getUserAuthenInfo' hint="get data from simplebilling.authorize_user" access="remote">
		<cfargument name="userId" default="#session.USERID#">
        <cfargument name="intPaymentGateway" required="false" default="1">
		<cfset var dataout	= {} />
		<cfset var userAuthen	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cftry>
			<cfquery name="userAuthen" datasource="#Session.DBSourceEBM#">
                SELECT 
                    PaymentMethodID_vch,PaymentType_vch
                FROM 
                    simplebilling.authorize_user
                WHERE 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#">
                AND 
                    paymentGateway_ti  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.intPaymentGateway#">    
            </cfquery>
			<cfif userAuthen.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.DATA = userAuthen>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>


	<cffunction name="CreateToken" hint="Create token in SercureNet - do not use now" access="remote">
		<cfargument name="payment_method" type="string" required="yes" hint="card = 0, check = 1">
		<cfargument name="number" type="string" required="yes">
		<cfargument name="expirationDate" type="string" required="yes">
		<cfargument name="cvv" type="string" required="yes">
		<cfargument name="firstName" type="string" required="yes">
		<cfargument name="lastName" type="string" required="yes">
		<cfargument name="email" type="string" required="yes">
		<cfargument name="line1" type="string" required="yes">
		<cfargument name="city" type="string" required="yes">
		<cfargument name="state" type="string" required="yes">
		<cfargument name="zip" type="string" required="yes">
		<cfargument name="country" type="string" required="yes">
        <cfargument name="phone" type="string" required="no" default="">
		<cfargument name="customerId" required="no" default="#Session.USERID#">
		<cfset var dataout	= {} />
		<cfset var data	= '' />
		<cfset var card_data	= '' />
		<cfset var create_token_url	= '' />
		<cfset var check_data	= '' />
		<cfset var responseData	= '' />
		<cfset var tokenRequestResult	= '' />
		<cfset dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.USERID = "0" />
		<cfset dataout.TOKEN = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset data = {
			publicKey = payment_public_key,
			addToVault = true,
			customerId = arguments.customerId,
			developerApplication: {
			developerId: worldpayApplication.developerId,
			version: worldpayApplication.Version
			}
			}>
		<cfif arguments.payment_method EQ 0 >
			<cfset card_data = {
				number          = arguments.number,
				expirationDate  = arguments.expirationDate,
				email           = arguments.email,
				cvv             = arguments.cvv,
				firstName       = arguments.firstName,
				lastName        = arguments.lastName,
				address = {
				line1     = arguments.line1,
				city      =  arguments.city,
				state     = arguments.state,
				zip       = arguments.zip,
				country   = arguments.country,
				phone     = arguments.phone
				}
				}>
			<cfset create_token_url = payment_create_token_with_card>
			<cfset data.card = card_data>
		<cfelseif payment_method EQ 1>
			<cfset check_data = {
				routingNumber   = arguments.number,
				accountNumber   = arguments.cvv,
				firstName       = arguments.firstName,
				lastName        = arguments.lastName,
				email           = arguments.email,
				address         = {
				line1   = arguments.line1,
				city    = arguments.city,
				state   = arguments.state,
				zip     = arguments.zip,
				country = arguments.country,
				phone   = arguments.phone
				}
				}>
			<cfset create_token_url = payment_create_token_with_check>
			<cfset data.check = check_data>
		</cfif>
		<cfhttp url="#create_token_url#" method="post" result="tokenRequestResult" port="443">
			<cfhttpparam type="header" name="content-type" value="application/json">
			<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
			<cfhttpparam type="header" name="Origin" value="www.securenet.com=">
			<cfhttpparam type="body" value='#TRIM(serializeJSON(data))#'>
		</cfhttp>
		<cfif trim(tokenRequestResult.filecontent) NEQ "" AND IsJSON(tokenRequestResult.filecontent)>
			<cfset responseData = deserializeJSON(tokenRequestResult.filecontent)>
			<cfif responseData.responseCode EQ 1>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Create Token Success" />
				<cfset dataout.TOKEN = responseData.token />
				<cfset dataout.USERID = arguments.customerId />
			<cfelse>
				<cfset dataout.MESSAGE = "Call getToken Fail. #responseData.message#" />
				<cfset dataout.ERRMESSAGE = "Call getToken Fail. #responseData.message#" />
			</cfif>
		<cfelse>
			<cfset dataout.MESSAGE = "Call Api getToken Fail" />
			<cfset dataout.ERRMESSAGE = "Call Api getToken  Fail." />
		</cfif>
		<cfreturn dataout>
	</cffunction>


	<cffunction name="UpdateBalance" access="public" hint="Update balance value for an user">
		<cfargument name="inpUserId" type="numeric" required="yes" default="0">
		<cfargument name="inpValue" type="numeric" required="yes" default="0">
		<cfset var dataout = {} />
		<cfset var updateRs = {} />

		<cftry>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

			<cfquery result="updateRs" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplebilling.billing
				SET 
					Balance_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpValue#">
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				LIMIT 
					1
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "OK" />
			<cfset dataout.ERRMESSAGE = "" />
			<cfcatch>
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="removeUserPaymentInfo" access="remote" hint="Remove an user's payment info">
		<cfargument name="inpUserID" default="#session.USERID#">
		
		<cfset var dataout	= {} />
		<cfset var apiRs	= {} />
		<cfset var userPayInfo = "" />
		<cfset var removeInfo  = "" />
		
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="userPayInfo">
			</cfinvoke>

			<cfif userPayInfo.RXRESULTCODE EQ 1 >
				<cfset var endpointRemoveApi = "#payment_get_customer_payment#" & "#arguments.inpUserID#/PaymentMethod/" & "#userPayInfo.DATA.PaymentMethodID_vch#"  />
				<cfhttp url="#endpointRemoveApi#" method="DELETE" result="apiRs" timeout="100" >
					<cfhttpparam type="header" name="content-type" value="application/json">
					<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
				</cfhttp>
				<cfset var reponseContent = deserializeJSON(apiRs.filecontent) />
				<!--- Remove user's payment info success from secureNet api --->
				<cfif StructkeyExists(reponseContent, "responseCode")  >

					<cfif reponseContent.responseCode EQ 1>
						<cfquery name="removeInfo" datasource="#Session.DBSourceEBM#">
			                DELETE FROM 
			                	simplebilling.authorize_user
			                WHERE
			                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
			                LIMIT 1
			            </cfquery>
			            <cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.MESSAGE = "Remove Success!">
					<cfelse>
						<cfset dataout.MESSAGE = reponseContent.message>
					</cfif>
				</cfif>
				<cfset dataout["DATA"] = reponseContent />
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout>

	</cffunction>




	<cffunction name="addCredit" access="remote" hint="Add/Update Credit">
		<cfargument name="inpUserID" type="numeric" required="true">
		<cfargument name="inpValue" type="numeric" required="true">
		<cfargument name="inpType" type="numeric" required="true">

		<cfset var fieldName = "">

		<cfif arguments.inpType EQ 1>
			<cfset fieldName = "BuyCreditBalance_int">
		<cfelseif arguments.inpType EQ 2>
			<cfset fieldName = "PromotionCreditBalance_int">
		<cfelse>
			<cfset fieldName = "Balance_int">
		</cfif>
		
		<cfset var dataout	= {} />
		<cfset var rsInsert	= {} />
        <cfset var rxGetAdminUserInfoById = ''/>
        <cfset var rxGetUserInfoById = '' />
        <cfset var rsSaveLog = ''/>
        <cfset var rsGetOld = ''/>
        <cfset var displayFieldName = ''/>


		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery result="rsInsert" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplebilling.billing
				SET 
					#fieldName# =  #fieldName# + <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpValue#">
				WHERE 
					UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				LIMIT 1
			</cfquery>

			<cfif rsInsert.RecordCount GT 0>
				<cfif arguments.inpType EQ 1>
					<cfset displayFieldName = "Purchased Credit" />
					<cfset fieldName = "BuyCreditBalance_int" />
				<cfelseif arguments.inpType EQ 2>
					<cfset displayFieldName = "Promotion Credit" />
					<cfset fieldName = "PromotionCreditBalance_int" />
				<cfelse>
					<cfset displayFieldName = "Current Credit" />
					<cfset fieldName = "Balance_int" />
				</cfif>

				<cfquery name="rsGetOld" datasource="#Session.DBSourceEBM#">
					SELECT #fieldName# 
					FROM simplebilling.billing
					WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
					LIMIT 1
				</cfquery>

				<cfset var newValue = rsGetOld["#fieldName#"]>
				<cfset var oldValue = rsGetOld["#fieldName#"] - arguments.inpValue />

				<cfinvoke method="saveLog" returnvariable="rsSaveLog">
					<cfinvokeargument name="inpUserID" value="#arguments.inpUserID#">
					<cfinvokeargument name="inpOldValue" value="#oldValue#">
					<cfinvokeargument name="inpNewValue" value="#newValue#">
					<cfinvokeargument name="inpFieldName" value="#displayFieldName#">
				</cfinvoke>

                <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="rxGetAdminUserInfoById">
                    <cfinvokeargument name="inpUserId" value="#Session.UserId#">
                </cfinvoke>

                <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="rxGetUserInfoById">
                    <cfinvokeargument name="inpUserId" value="#Session.UserId#">
                </cfinvoke>

                <cfmail from="#SupportEMailFrom#" to="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" 
                server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#" type="html" subject="Adjust Account Credit">

                    <cfoutput>#rxGetAdminUserInfoById.USERNAME# #rxGetAdminUserInfoById.LASTNAME# added #arguments.inpValue# credit to #displayFieldName# for UserID #arguments.inpUserID# (#rxGetUserInfoById.FULLEMAIL#)</cfoutput>
                </cfmail>

				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Add credit success!" />
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>



	<cffunction name="saveLog" access="public" hint="Save log update billing">
		<cfargument name="inpUserID" type="numeric" required="true">
		<cfargument name="inpFieldName" type="string" required="true">
		<cfargument name="inpOldValue" type="numeric" required="true">
		<cfargument name="inpNewValue" type="numeric" required="true">
		
		<cfset var dataout	= {} />
		<cfset var rsInsert	= {} />
		
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery result="rsInsert" datasource="#Session.DBSourceEBM#">
				INSERT INTO 
					simplebilling.billing_update_logs(UserId_int, OwnerID_int, FieldName_vch, OldValue_int, NewValue_int)
				VALUES (
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFieldName#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOldValue#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNewValue#">
				)
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = "" />

			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>



	<cffunction name="GetUpdateLogs" access="remote" hint="Get update logs billing">
		<cfargument name="inpOwnerID" type="numeric" required="true">
		<cfargument name="inpCustomFilter" type="string" required="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />


		<cfset var dataout	= {} />
		<cfset var filterData = DeserializeJSON(arguments.inpCustomFilter) />
		<cfset var rsGetData = {} />
		<cfset var rsCount = {} />
        <cfset var filterItem = ''/>
		

		<cfset dataout["dataList"]	= [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery name="rsGetData" datasource="#Session.DBSourceEBM#">
				SELECT SQL_CALC_FOUND_ROWS
					BUL.ID_bi, 
					BUL.UserId_int,
					BUL.FieldName_vch,
					BUL.OldValue_int,
					BUL.NewValue_int,
					BUL.Created_dt,
					UA.FirstName_vch,
					UA.LastName_vch
				FROM simplebilling.billing_update_logs BUL
					INNER JOIN simpleobjects.useraccount UA ON BUL.UserId_int = UA.UserId_int
				WHERE 
					BUL.OwnerID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOwnerID#">
				<cfif inpCustomFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				ORDER BY Created_dt DESC
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
			</cfquery>

			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>

			<cfloop query="rsGetData" >
				<cfset var item = {
					ID = '#ID_bi#',
					FIELD = '#FieldName_vch#',
					USERID = '#UserId_int#',
					UFIRSTNAME = '#FirstName_vch#',
					ULASTNAME = '#LastName_vch#',
					OLDVALUE = '#OldValue_int#',
					NEWVALUE = '#NewValue_int#',
					CREATED = '#DateFormat(Created_dt, "mm/dd/yyyy")#'
				} />
				<cfset dataout["dataList"].append(item) />
			</cfloop>


			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = "" />

			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>
	<!--- Re-charge Amount count --->
	<cffunction name="RechargeAmountCount" access="remote" hint="Amount count">
		<cfargument name="_nextPage" required="no" default="0">
		<cfargument name="_pageOne_now" required="no" default="">
		<cfargument name="logFileNameIndex" required="no" default="0">
		<cfargument name="logDataStartDate_dt" required="no" default="">
		<cfargument name="logDataAccountNumber_bi" required="no" default="0">
		<cfargument name="logDataErrorNumber_bi" required="no" default="0">
		<cfargument name="UserId" required="yes" default="">

        <cfinclude template="/public/paths.cfm">
        <cfinclude template="/public/sire/configs/paths.cfm">
    
        <cfinclude template="/session/sire/configs/credits.cfm">
		<!--- -1: Error, 0: ----, 1: successfully --->
        <cfset var dataout = {} />
		<cfset dataout.DATA = 0>
		<cfset dataout.RESULT = 0>
        <cfset dataout.MESSAGE = ''>

		<cfset var userlog_vch = ''>
		<cfset var _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
		<cfset var _fcm = left(_now, 7)>
		<cfset var userPlansQuery = ''/>
		<cfset var usersQuery = ''/>
		<cfset var plansQuery = ''/>
		<cfset var customersQuery = ''/>
		<cfset var getDowngradePlan = ''>
		<cfset var getActiveKeyword = ''>
		<cfset var insertUserPlans = ''>
		<cfset var updateBalanceBillings =''>
		<cfset var addPromoCreditBalance =''>
		<cfset var updateUsedTimePromoCode =''>
		<cfset var expiredUserPlans =''>
		<cfset var Rechargeloginfor =''>
		<cfset var userPlansResult =''>
		<cfset var balanceBillingsResult =''>
		<cfset var addResult =''>
		<cfset var updateResult =''>
		<cfset var CouponForUser = ''>
		<cfset var returngetUserPlan = ''>
		<cfset var RetAddReferralCredits = ''>
		<cfset var index = ''>
		<cfset var _balance = ''>
		<cfset var _plan = []>
		<cfset var _amount = 0 >  <!---total amount have to paid --->
		<cfset var _priceMesAfter = ''>
		<cfset var _planStartDate = ''>
		<cfset var _planEndDate = ''>

        <!--- Get all userplan need to run. Except runing on other threead --->
       <cfquery name="userPlansQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserPlanId_bi,Status_int,UserId_int,PlanId_int,BillingType_ti,Amount_dec,UserAccountNumber_int,Controls_int,KeywordsLimitNumber_int,KeywordMinCharLimit_int,FirstSMSIncluded_int,
				UsedSMSIncluded_int,CreditsAddAmount_int,PriceMsgAfter_dec,PriceKeywordAfter_dec,StartDate_dt,EndDate_dt,MonthlyAddBenefitDate_dt,MlpsLimitNumber_int,ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi,BuyKeywordNumber_int,BuyKeywordNumberExpired_int,DowngradeDate_dt,PromotionId_int,PromotionKeywordsLimitNumber_int,PromotionMlpsLimitNumber_int, 
				PromotionShortUrlsLimitNumber_int,PromotionLastVersionId_int,UserDowngradeDate_dt,UserDowngradePlan_int,UserDowngradeKeyword_txt,UserDowngradeMLP_txt,UserDowngradeShortUrl_txt,
				NumOfRecurringKeyword_int,NumOfRecurringPlan_int, DATEDIFF(CURDATE(),MonthlyAddBenefitDate_dt) as expireKeywordDateDiff, DATEDIFF(CURDATE(),EndDate_dt) as expirePlanDateDiff 
			FROM 
				simplebilling.userplans
			WHERE 
				Status_int = 1
				AND (DATE(EndDate_dt) <= CURDATE() OR MonthlyAddBenefitDate_dt <= CURDATE())
				AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
        </cfquery>
		<!--- get all user info --->
		<cfquery name="usersQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch , YearlyRenew_ti
			FROM 
				simpleobjects.useraccount
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
			AND 
				Active_int = 1
		</cfquery>
		<!--- get all active plan info --->
		<cfquery name="plansQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				PlanId_int, PlanName_vch, Amount_dec, YearlyAmount_dec, UserAccountNumber_int, Controls_int, KeywordsLimitNumber_int, KeywordMinCharLimit_int,
				FirstSMSIncluded_int, CreditsAddAmount_int, PriceMsgAfter_dec, PriceKeywordAfter_dec, ByMonthNumber_int, MlpsLimitNumber_int, ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi, Order_int,Status_int
			FROM 
				simplebilling.plans
			WHERE 
				Status_int in (1,8,4,2)
			ORDER BY 
				PlanId_int DESC
		</cfquery>

		<!--- GET FREE PLAN --->
		<cfset var planFree = {}>

		<cfloop query="plansQuery">
			<cfif PlanId_int EQ 1>
				<cfset planFree['Amount_dec'] = Amount_dec >
				<cfset planFree['UserAccountNumber_int'] = UserAccountNumber_int >
				<cfset planFree['KeywordsLimitNumber_int'] = KeywordsLimitNumber_int >
				<cfset planFree['FirstSMSIncluded_int'] = FirstSMSIncluded_int >
				<cfset planFree['CreditsAddAmount_int'] = CreditsAddAmount_int >
				<cfset planFree['PriceMsgAfter_dec'] = PriceMsgAfter_dec >
				<cfset planFree['PriceKeywordAfter_dec'] = PriceKeywordAfter_dec >
				<cfset planFree['PlanId_int'] = PlanId_int >
				<cfset planFree['PlanName_vch'] = PlanName_vch >
				<cfset planFree['Controls_int'] = Controls_int >
				<cfset planFree['MlpsLimitNumber_int'] = MlpsLimitNumber_int >
				<cfset planFree['ShortUrlsLimitNumber_int'] = ShortUrlsLimitNumber_int >
				<cfset planFree['MlpsImageCapacityLimit_bi'] = MlpsImageCapacityLimit_bi >
			</cfif>
		</cfloop>

		<!--- get user authen with sercurenet for auto run payment --->
		<cfquery name="customersQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				id,PaymentMethodID_vch,PaymentType_vch,UserId_int
			FROM 
				simplebilling.authorize_user
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
		</cfquery>

		<cfset var closeBatchesData = {}>
		<cfset closeBatchesData = {
			developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.version
			}
		}>
	
		<cfset var oldUserPlans = []>
		<cfset var newUserPlans = []>
		<cfset var addPromotionCreditBalance = []>
		<cfset var promocodeListToIncreaseUsedTime = []>
		<cfset var promocodeListToIncreaseUsedTimeTemp = []>
		<cfset var errorUserPlans = []>
		<cfset var newBalanceBillings = []>
		<cfset var logPaymentWorldpays = [[],[],[]]>
		<cfset var paymentErrorsPlans = {}>

		<cfset var threadIndex = 1>
		<cfset var emailsend = ''/>
		<cfset var threadId = 1>
		<cfset var planIndex = 0>
		<cfset var userIndex = 0>
		<cfset var customerIndex = 0>
		<cfset var paymentErrorsMonthly = []>
		<cfset var isRemoveCouponBenefit = []>
		<cfset var isDowngrade = []>
		<cfset var TotalSentReceived = ''>
		<cfset var _numberCreditUsing=0>		

        <cfloop query="userPlansQuery">
			<cfset var threadId = threadIndex/>
				
				<cfset planIndex = plansQuery.PlanId_int.IndexOf(JavaCast('int', userPlansQuery.PlanId_int[threadId])) + 1>
				<cfset userIndex = usersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>
				<cfset customerIndex = customersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>
				<cfset 	paymentErrorsPlans[threadId] = 0/>
				<cfset  isDowngrade[threadId] = 0/>
				<cfset  isRemoveCouponBenefit[threadId] = 0/>
				<cfset  promocodeListToIncreaseUsedTimeTemp = []/>
				<cfset  paymentErrorsMonthly[threadId] = 1/>

				<cfif userIndex GT 0 AND planIndex GT 0>

					<cfset var _paymentType = customersQuery.PaymentType_vch[customerIndex]>
					<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
					<cfset var _totalkeywordHaveToPaid = 0/>

					<!--- User Downgrade info --->
					<cfset var _userDowngradePlan = userPlansQuery.UserDowngradePlan_int[threadId]/>
					<cfset var _userDowngradeKeyword = deserializeJSON(userPlansQuery.UserDowngradeKeyword_txt[threadId])/>
					<cfset var _userDowngradeShortUrl = deserializeJSON(userPlansQuery.UserDowngradeShortUrl_txt[threadId])/>
					<cfset var _userDowngradeMLP = deserializeJSON(userPlansQuery.UserDowngradeMLP_txt[threadId])/>
					<cfset var _downgradePlanName = ''/>
					<cfset var _downgradePlanCredits = 0/>
					<cfset var _downgradePlanKeywords = 0 />

					<cfset var _userBillingType = userPlansQuery.BillingType_ti[threadId]/>
					<cfset var _numOfRecurringKeyword = userPlansQuery.NumOfRecurringKeyword_int[threadId]/> 
					<cfset var _numOfRecurringPlan = userPlansQuery.NumOfRecurringPlan_int[threadId]/> 
					<cfset var _expirePlanDateDiff = userPlansQuery.expirePlanDateDiff[threadId]/> 
					<cfset var _expireKeywordDateDiff = userPlansQuery.expireKeywordDateDiff[threadId]/> 
					<cfset var _yearlyRenew = usersQuery.YearlyRenew_ti[userIndex]>

					<cfset _priceMesAfter =userPlansQuery.PriceMsgAfter_dec[threadId]>
					<cfset _planStartDate= userPlansQuery.StartDate_dt[threadId]>
					<cfset _planEndDate= userPlansQuery.EndDate_dt[threadId]>

					<!--- Promotion benefit --->
					<cfset var _userPromotionKeyword = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset var _userPromotionMLP = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset var _userPromotionURL = userPlansQuery.PromotionShortUrlsLimitNumber_int[threadId]/>

					<cfset var percent_dis = 0>
					<cfset var flatrate_dis = 0>
					<cfset var promotion_credit = 0>
					<cfset var promotion_mlp = 0>
					<cfset var promotion_keyword = 0>
					<cfset var promotion_shorturl = 0>
					<cfset var userPromotionId = 0>
					<cfset var promotion_id = 0>
					<cfset var promotion_origin_id = 0>

					<cfset var _monthAmount = 0 /> <!--- use when try to charge monthly of yearly plan --->

					<!--- special cases for postpaid account--->
					<cfif Listfind("4",plansQuery.Status_int[planIndex]) GT 0>

						<cfset _amount=0>
						<cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
							SELECT                        
								SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
								SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
							FROM
								simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
							LEFT JOIN
								simpleobjects.useraccount as ua
							ON
								ire.UserId_int = ua.UserId_int					
							WHERE
								LENGTH(ire.ContactString_vch) < 14
							AND 
								ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.UserId#" >	
							AND
								ua.IsTestAccount_ti = 0                    
							AND
								ire.Created_dt
							BETWEEN 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planStartDate#">
							AND 
								
								DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planEndDate#">, INTERVAL 86399 SECOND)
							GROUP BY
								ire.UserId_int     
						</cfquery>												
						<cfset _numberCreditUsing=0>					
						<cfif TotalSentReceived.RecordCount GT 0>											
							<cfset _numberCreditUsing= LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received)>					
							<cfset _amount= _numberCreditUsing * _priceMesAfter / 100>	
						<cfelse>
							
						</cfif>	
						<!--- add total kw buy for amount--->
						<cfset _amount= _amount+ userPlansQuery.BuyKeywordNumber_int[threadId] * userPlansQuery.PriceKeywordAfter_dec[threadId]>
						<cfset _amount =NumberFormat(_amount,'.99')>
						<cfset dataout.RESULT = 1>
						<cfset dataout.DATA = _amount>
						<cfset dataout.MESSAGE = ''>
						<cfreturn dataout />
					</cfif>

					<!---<cfset var _amount = 0 >  total amount have to paid --->

					<cfset var _makePayment = 1 > <!--- check if user need to run payment --->
					<!---_makePayment 0 : do not run payment but still add new plan --->
					<!---_makePayment 1 : run payment --->
					<!---_makePayment 2 : do not run payment but do not add new plan --->



					<cfset var _recurringType = 0 > 
					<!---_recurringType 0 : renew monthly/yearly --->
					<!---_recurringType 1 : add benefit monthly --->
					<!---_recurringType 2 : payment for buy keyword monthly --->

					<cfinvoke component="public.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
	                  <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
	                </cfinvoke>

					<!--- Check CouponForUser --->
					<cfif CouponForUser.rxresultcode EQ 1>
						<cfloop array="#CouponForUser['DATALIST']#" index="index">
							<cfif index.COUPONDISCOUNTTYPE EQ 1>
								<cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
								<cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								
								<cfset userPromotionId = index.userPromotionId>
							<cfelseif index.COUPONDISCOUNTTYPE EQ 2>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								
								<cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
								<cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
								<cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
								<cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
								<cfset userPromotionId = index.userPromotionId>
							<cfelse>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								<cfset userPromotionId = index.userPromotionId>
								
							</cfif>

							<cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, userPlansQuery.UserId_int[threadId],userPromotionId])/>
						</cfloop>
					</cfif>

					<!--- calc amount when renew plan --->
					<cfif _userBillingType EQ 2 AND _yearlyRenew EQ 1> <!--- yearly and set renew --->
						<cfset _amount = (plansQuery.YearlyAmount_dec[planIndex]*12 - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>	

						<cfset _monthAmount = (plansQuery.Amount_dec[planIndex] - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>
					<cfelse>
						<cfset _amount = (plansQuery.Amount_dec[planIndex] - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>	
					</cfif> 
					
					<!--- if user buy plan yearly recalc _amount, check to see if it run monthly add benefit --->
					<cfif _userBillingType EQ 2 AND isDate(userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
						<!--- if this is recurring monthly EndDate_dt later than MonthlyAddBenefitDate_dt  --->
						<cfif DateCompare(userPlansQuery.EndDate_dt[threadId],userPlansQuery.MonthlyAddBenefitDate_dt[threadId]) EQ 1 AND DateCompare(userPlansQuery.EndDate_dt[threadId], NOW()) EQ 1  >
							<!--- if user buy keyword -> need to pay for these keywords --->
							<cfif userPlansQuery.BuyKeywordNumber_int[threadId] GT 0>
								<cfset _amount = userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex]>	
								<cfset _recurringType = 2 > 
							<cfelse>
								<!--- add benefit monthly --->
								<cfset _amount = 0 >
								<cfset _recurringType = 1 > 
							</cfif>
						</cfif>
					</cfif>

					<!--- check if yearly plan but do not auto renew (_yearlyRenew = 0) --->
					<cfif _recurringType EQ 0 AND _userBillingType EQ 2 AND _yearlyRenew EQ 0>
						<cfset _userBillingType = 1 />
					</cfif>

					<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
					<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="returngetUserPlan">
			            <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
			        </cfinvoke>

			       	<cfset var keywordUsed = returngetUserPlan.KEYWORDUSE/>
			       	<cfset var shorturlUsed = returngetUserPlan.SHORTURLUSED/>
			       	<cfset var mlpUsed = returngetUserPlan.MLPUSED/>
			       	
					<!--- check if user select downgrade and have to paid --->
					<cfif _userDowngradePlan GT 0>
						<cfset isDowngrade[threadId] = 1/>
						<!--- get downgrade plan info --->
						<cfquery name="getDowngradePlan" datasource="#Session.DBSourceREAD#">
							SELECT 
								PlanId_int,
								PlanName_vch,
								Amount_dec,
								YearlyAmount_dec,
								UserAccountNumber_int,
								Controls_int, 
								KeywordsLimitNumber_int, 
								KeywordMinCharLimit_int, 
								FirstSMSIncluded_int,
								CreditsAddAmount_int,
								PriceMsgAfter_dec,
								PriceKeywordAfter_dec,
								ByMonthNumber_int,
								MlpsLimitNumber_int,
								ShortUrlsLimitNumber_int,
								MlpsImageCapacityLimit_bi,
								Order_int
							FROM 
								simplebilling.plans
							WHERE 
								Status_int = 1
							AND
								PlanId_int = #_userDowngradePlan#
						</cfquery>

						<cfif getDowngradePlan.RecordCount GT 0>
							<cfset _downgradePlanName = getDowngradePlan.PlanName_vch/>
							<cfset _downgradePlanCredits = getDowngradePlan.FirstSMSIncluded_int/>
							<cfset _downgradePlanKeywords = getDowngradePlan.KeywordsLimitNumber_int />
							<!--- get user keyword have to paid --->
							<cfset _totalkeywordHaveToPaid = keywordUsed - getDowngradePlan.KeywordsLimitNumber_int - userPlansQuery.BuyKeywordNumber_int[threadId]/>
							
							<!--- if _totalkeywordHaveToPaid greater than 0 then check downgrade keyword --->
							<cfif _totalkeywordHaveToPaid GT 0>
								<cfset var currentRemoveKeyword = 0> 
								<!--- check if keyword user select is still active or not --->
								<cfif arrayLen(_userDowngradeKeyword) GT 0>
									<cfquery name="getActiveKeyword" datasource="#Session.DBSourceREAD#">
										SELECT 
											KeywordId_int
										FROM 
											sms.keyword 
										WHERE 
											Active_int = 1
										AND
											KeywordId_int IN (#arrayToList(_userDowngradeKeyword)#) 
									</cfquery>
									<cfif getActiveKeyword.RecordCount GT 0>
										<cfset currentRemoveKeyword = getActiveKeyword.RecordCount/>
									</cfif>
									<cfset _totalkeywordHaveToPaid = _totalkeywordHaveToPaid - currentRemoveKeyword/>
								</cfif>
							<cfelse>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>	

							<cfif _totalkeywordHaveToPaid LT 0>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>
							
							<cfset _amount = getDowngradePlan.Amount_dec + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex]) + (_totalkeywordHaveToPaid * getDowngradePlan.PriceKeywordAfter_dec) >
						</cfif>
					</cfif>
				</cfif>
			<cfset threadIndex++>
		</cfloop>

        <cfset dataout.RESULT = 1>
		<cfset dataout.DATA = _amount>
        <cfset dataout.MESSAGE = ''>
        <cfreturn dataout />
	</cffunction>
    <!--- Re-charge --->
	<cffunction name="DoRecharge" access="remote" hint="Re-charge">		
		<cfargument name="UserId" required="yes" default="">
		<!--- -1: Error, 0: ----, 1: successfully --->
        <cfset var dataout = {} />
		<cfset dataout.RESULT = 0>
        <cfset dataout.MESSAGE = ''>
		<cfset var GetPlanType=''/>	
		<cfset var RetVarGetAdminPermission=''	/>
		<cfset var PlanID = '0'>
		<cfset var userPlansQuery = ''>
		<cfset var plansQuery = ''>
		<cfset var RechargeVal = ''>
		<cftry>
			<!--- UPDATE: Check admin permission --->
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>	
			<cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
				<cfset dataout.RESULT = -1>
				<cfset dataout.MESSAGE = "No Admin Permission."/>
				<cfreturn dataout>
			</cfif>
			<cfquery name="userPlansQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					PlanId_int
				FROM 
					simplebilling.userplans
				WHERE 
					Status_int = 1
					AND (DATE(EndDate_dt) <= CURDATE() OR MonthlyAddBenefitDate_dt <= CURDATE())
					AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.UserId#">
			</cfquery>			

			<!--- UPDATE: If not found return false --->
			<cfif userPlansQuery.RECORDCOUNT LT 1>
				<cfset dataout.RESULT = -1>
				<cfset dataout.MESSAGE = 'User Plans invalid.'>
				<cfreturn dataout>
			<cfelse>
				<cfset PlanID= userPlansQuery.PlanId_int>
			</cfif>			

			<!--- get all active plan info --->
			<cfquery name="plansQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					PlanId_int,Status_int
				FROM 
					simplebilling.plans
				WHERE 
					Status_int in (1,8,4,2)
				AND
					PlanId_int=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#PlanID#">
				ORDER BY 
					PlanId_int DESC
			</cfquery>
			<cfif plansQuery.RECORDCOUNT GT 0>
				<cfif ListFind("4",plansQuery.Status_int) GT 0>
					<cfinvoke method="Rechargesubaccount" returnvariable="RechargeVal">
						<cfinvokeargument name="UserId" value="#arguments.UserId#">
					</cfinvoke>	
				<cfelse>
					<cfinvoke method="Rechargeaccount" returnvariable="RechargeVal">
						<cfinvokeargument name="UserId" value="#arguments.UserId#">
					</cfinvoke>	
				</cfif>				
				<cfset dataout.RESULT = RechargeVal.RESULT>
        		<cfset dataout.MESSAGE = RechargeVal.MESSAGE>
			<cfelse>
				<cfset dataout.RESULT = -1>
				<cfset dataout.MESSAGE = 'User Plans invalid.'>
				<cfreturn dataout>			
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout.RESULT = -1 /> 
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>				
			</cfcatch>   
        </cftry>
		<cfreturn dataout />
	</cffunction>
	<cffunction name="Rechargesubaccount" access="remote" hint="Re-charge">        
		<cfargument name="_nextPage" required="no" default="0">
		<cfargument name="_pageOne_now" required="no" default="">
		<cfargument name="logFileNameIndex" required="no" default="0">
		<cfargument name="logDataStartDate_dt" required="no" default="">
		<cfargument name="logDataAccountNumber_bi" required="no" default="0">
		<cfargument name="logDataErrorNumber_bi" required="no" default="0">
		<cfargument name="UserId" required="yes" default="">

        
		<!--- -1: Error, 0: ----, 1: successfully --->
        <cfset var dataout = {} />
		<cfset dataout.RESULT = 0>
        <cfset dataout.MESSAGE = ''>
		<cfset var subject = ''>
		<cfset var userlog_vch = ''>
		<cfset var _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
		<cfset var _fcm = left(_now, 7)>
		<cfset var userPlansQuery=''/>

		<cfset var oldUserPlans = []>
		<cfset var newUserPlans = []>
		<cfset var addPromotionCreditBalance = []>
		<cfset var promocodeListToIncreaseUsedTime = []>
		<cfset var promocodeListToIncreaseUsedTimeTemp = []>
		<cfset var userListToSendAlertCCDeclineToAdmin = []>
		<cfset var errorUserPlans = []>
		<cfset var newBalanceBillings = []>
		<cfset var logPaymentWorldpays = [[],[],[]]>
		<cfset var paymentErrorsPlans = {}>

		<cfset var closeBatchesData = {}/>
		<cfset var threadId=0/>
		<cfset var planIndex = 0/>
		<cfset var userIndex = 0/>
		<cfset var customerIndex = 0/>
		<cfset var isDowngrade = {}/>
		<cfset var isRemoveCouponBenefit = {}/>
		<cfset var paymentErrorsMonthly = {}/>
		<cfset var _paymentType = {}/>
		<cfset var _totalkeywordHaveToPaid = ''/>
		<cfset var _userDowngradePlan = ''/>
		<cfset var _downgradePlanName = ''/>
		<cfset var _downgradePlanCredits = ''/>
		<cfset var _downgradePlanKeywords = ''/>
		<cfset var _userPromotionKeyword = ''/>
		<cfset var _userPromotionMLP = ''/>
		<cfset var _userPromotionURL = ''/>
		<cfset var percent_dis = ''/>
		<cfset var flatrate_dis = ''/>
		<cfset var promotion_credit = ''/>
		<cfset var promotion_mlp = ''/>
		<cfset var promotion_keyword = ''/>
		<cfset var promotion_shorturl = ''/>
		<cfset var userPromotionId = ''/>
		<cfset var promotion_id = ''/>
		<cfset var promotion_origin_id = ''/>
		<cfset var _userID = ''/>
		<cfset var _userBillingType = ''/>
		<cfset var _numOfRecurringPlan = ''/>
		<cfset var _expirePlanDateDiff = ''/>
		<cfset var _priceMesAfter = ''/>
		<cfset var _planStartDate = ''/>
		<cfset var _planEndDate = ''/>
		<cfset var _monthAmount = ''/>
		<cfset var _amount = ''/>
		<cfset var _numberCreditUsing = ''/>
		<cfset var _makePayment = ''/>
		<cfset var _recurringType = ''/>
		<cfset var promotion_used_time = ''/>
		<cfset var keywordUsed = ''/>
		<cfset var shorturlUsed = ''/>
		<cfset var mlpUsed = ''/>
		<cfset var paymentData = {}/>
		<cfset var paymentError = ''/>
		<cfset var paymentResponse = {}/>
		<cfset var paymentResponseContent = {}/>
		<cfset var _endDate = ''/>
		<cfset var _monthlyDate = ''/>
		<cfset var declineDataContent = {}/>
		<cfset var declineEmailsend = {}/>
		<cfset var declinePlanTime = ''/>
		<cfset var declineKeywordTime = ''/>
		<cfset var dataMail = {}/>
		<cfset var declinedAmount = ''/>
		<cfset var tempCustomerInfo = {}/>
		<cfset var _comma = ''/>
		<cfset var _plan = ''/>
		<cfset var userIdList = ''/>
		<cfset var userIdListPromotion = ''/>
		<cfset var i = ''/>
		<cfset var datalistacc = {}/>
		<cfset var index = ''/>
		<cfset var _balance = ''/>
		<cfset var usersQuery = ''/>
		<cfset var plansQuery = ''/>
		<cfset var customersQuery = ''/>
		<cfset var TotalSentReceived = ''/>
		<cfset var insertUserPlans = ''/>
		<cfset var updateBalanceBillings = ''/>
		<cfset var addPromoCreditBalance = ''/>
		<cfset var updateUsedTimePromoCode = ''/>
		<cfset var expiredUserPlans = ''/>
		<cfset var CouponForUser = ''/>
		<cfset var getUserPlan = ''/>
		<cfset var RetAddReferralCredits = ''/>
		<cfset var retvalsendmail = ''/>
		<cfset var closeBatchesRequestResult = ''/>
		<cfset var userPlansResult = ''/>
		<cfset var balanceBillingsResult = ''/>
		<cfset var addResult = ''/>
		<cfset var updateResult = ''/>



		<cfset var ___c = 0>
		<cfset var threadIndex = 1>
		<cfset var threadList = ''>
		<cfset var emailsend = ''/>		
		<cfset var _ccBelong= "">
		<cfset var _paymentGateWay = 0/>

        <cfset var transactionInfo = ''/>
        <cfset var DoReferenceTransactionRX = ''/>
        <cfset var rtSaleWithTokenzine = ''/>
        <cfset var rtUpdatePayment = ''/>

		<cfinclude template="/public/paths.cfm">
        <cfinclude template="/public/sire/configs/paths.cfm">
    
        <cfinclude template="/session/sire/configs/credits.cfm">
		<cftry>
			<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
				SELECT 
					UserPlanId_bi,Status_int,UserId_int,PlanId_int,BillingType_ti,Amount_dec,UserAccountNumber_int,Controls_int,KeywordsLimitNumber_int,KeywordMinCharLimit_int,FirstSMSIncluded_int,
					UsedSMSIncluded_int,CreditsAddAmount_int,PriceMsgAfter_dec,PriceKeywordAfter_dec,StartDate_dt,EndDate_dt,MonthlyAddBenefitDate_dt,MlpsLimitNumber_int,ShortUrlsLimitNumber_int,
					MlpsImageCapacityLimit_bi,BuyKeywordNumber_int,BuyKeywordNumberExpired_int,DowngradeDate_dt,PromotionId_int,PromotionKeywordsLimitNumber_int,PromotionMlpsLimitNumber_int, 
					PromotionShortUrlsLimitNumber_int,PromotionLastVersionId_int,UserDowngradeDate_dt,UserDowngradePlan_int,UserDowngradeKeyword_txt,UserDowngradeMLP_txt,UserDowngradeShortUrl_txt,
					NumOfRecurringKeyword_int,NumOfRecurringPlan_int, DATEDIFF(CURDATE(),MonthlyAddBenefitDate_dt) as expireKeywordDateDiff, DATEDIFF(CURDATE(),EndDate_dt) as expirePlanDateDiff 
				FROM 
					simplebilling.userplans
				WHERE 
					Status_int = 1
				AND					
					DATE(EndDate_dt) <= CURDATE() 
				AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
				
				ORDER BY 
					EndDate_dt DESC, UserPlanId_bi DESC
				LIMIT 100
			</cfquery>
			<cfif userPlansQuery.RecordCount GT 0>				
				
				<!--- get all user info --->
				<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
					SELECT 
						ua.UserId_int, ua.FirstName_vch, ua.LastName_vch, ua.EmailAddress_vch, ua.HomePhoneStr_vch , ua.YearlyRenew_ti,
						ua.UserLevel_int,
						uhigher.EmailAddress_vch as HigherUserEmail, 
						uhigher.FirstName_vch as HigherUserFirstName,
						uhigher.LastName_vch as HigherUserLastName,
						uhigher.UserId_int as HigherUserId						
					FROM 
						simpleobjects.useraccount ua
					LEFT JOIN 
						simpleobjects.useraccount uhigher
						ON	ua.HigherUserID_int= uhigher.UserId_int
					WHERE 						
						ua.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
					AND 
						ua.Active_int = 1			
					ORDER BY 
						ua.UserId_int DESC
				</cfquery>
				
				<!--- get all active plan info --->
				<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
					SELECT 
						PlanId_int, PlanName_vch, Amount_dec, YearlyAmount_dec, UserAccountNumber_int, Controls_int, KeywordsLimitNumber_int, KeywordMinCharLimit_int,
						FirstSMSIncluded_int, CreditsAddAmount_int, PriceMsgAfter_dec, PriceKeywordAfter_dec, ByMonthNumber_int, MlpsLimitNumber_int, ShortUrlsLimitNumber_int,
						MlpsImageCapacityLimit_bi, Order_int
					FROM 
						simplebilling.plans
					WHERE 
						Status_int in (4)
					ORDER BY 
						PlanId_int DESC
				</cfquery>
								
				
				<!--- get user authen with sercurenet for auto run payment --->
				<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
					SELECT 
						a.id, a.PaymentMethodID_vch, a.PaymentType_vch, ua.UserId_int, a.UserId_int as CCBelong,
						a.PaymentGateway_ti
					FROM 
						simplebilling.authorize_user a
					LEFT JOIN 
						simpleobjects.useraccount ua
						ON	a.UserId_int= ua.UserId_int						
						OR 	(
							a.UserId_int= ua.HigherUserID_int AND ua.UserLevel_int=3
						)
					WHERE                 
						ua.UserType_int IN(2,3)
					AND
						ua.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
					AND
						a.PaymentGateway_ti = (
							Select  PaymentGateway_ti
							FROM 	simpleobjects.useraccount
							WHERE 	(UserId_int = ua.HigherUserID_int AND ua.UserLevel_int=3)
                            OR		(UserId_int = ua.UserId_int AND ua.UserLevel_int=2)
						)
					
					ORDER BY 
						a.id DESC			
				</cfquery>												
				
				<cfset closeBatchesData = {
					developerApplication = {
						developerId: worldpayApplication.developerId,
						version: worldpayApplication.version
					}
				}>
			
				
				
				<cfloop query="userPlansQuery">
					
					<cfset threadId = threadIndex/>  <!--- need remove when using thread--->																				
						
						<cfset planIndex = plansQuery.PlanId_int.IndexOf(JavaCast('int', userPlansQuery.PlanId_int[threadId])) + 1>
						<cfset userIndex = usersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>
						<cfset customerIndex = customersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>				

						<cfset paymentErrorsPlans[threadId] = 0/>
						<cfset isDowngrade[threadId] = 0/>
						<cfset isRemoveCouponBenefit[threadId] = 0/>
						<cfset promocodeListToIncreaseUsedTimeTemp = []/>
						<cfset paymentErrorsMonthly[threadId] = 1/>
						
						<cfif userIndex GT 0 AND planIndex GT 0>							
							<cfset _paymentGateWay = customersQuery.PaymentGateway_ti[customerIndex]>
							<cfset _paymentType = customersQuery.PaymentType_vch[customerIndex]>
							<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
							<cfset _totalkeywordHaveToPaid = 0/>

							<!--- User Downgrade info --->
							<cfset _userDowngradePlan = userPlansQuery.UserDowngradePlan_int[threadId]/>
							
							<cfset _downgradePlanName = ''/>
							<cfset _downgradePlanCredits = 0/>
							<cfset _downgradePlanKeywords = 0 />

							<!--- Promotion benefit --->
							<cfset _userPromotionKeyword = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
							<cfset _userPromotionMLP = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
							<cfset _userPromotionURL = userPlansQuery.PromotionShortUrlsLimitNumber_int[threadId]/>

							<cfset percent_dis = 0>
							<cfset flatrate_dis = 0>
							<cfset promotion_credit = 0>
							<cfset promotion_mlp = 0>
							<cfset promotion_keyword = 0>
							<cfset promotion_shorturl = 0>
							<cfset userPromotionId = 0>
							<cfset promotion_id = 0>
							<cfset promotion_origin_id = 0>

							<cfset _ccBelong= customersQuery.CCBelong[customerIndex]>
							<cfset _userID= usersQuery.UserId_int[userIndex]>
							<cfset _userBillingType = userPlansQuery.BillingType_ti[threadId]/>					
							<cfset _numOfRecurringPlan = userPlansQuery.NumOfRecurringPlan_int[threadId]/> 
							<cfset _expirePlanDateDiff = userPlansQuery.expirePlanDateDiff[threadId]/> 
							<cfset _priceMesAfter =userPlansQuery.PriceMsgAfter_dec[threadId]>

							<cfset _planStartDate= userPlansQuery.StartDate_dt[threadId]>
							<cfset _planEndDate= userPlansQuery.EndDate_dt[threadId]>

							<cfset _monthAmount = 0 /> <!--- use when try to charge monthly of yearly plan --->

							<cfset _amount = 0 > <!--- total amount have to paid --->
							<!--- get total message sent for calc amount--->
							<cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
								SELECT                        
									SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
									SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
								FROM
									simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
								LEFT JOIN
									simpleobjects.useraccount as ua
								ON
									ire.UserId_int = ua.UserId_int					
								WHERE
									LENGTH(ire.ContactString_vch) < 14
								AND 
									ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#_userID#" >	
								AND
									ua.IsTestAccount_ti = 0                    
								AND
									ire.Created_dt
								BETWEEN 
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planStartDate#">
								AND 
									
									DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planEndDate#">, INTERVAL 86399 SECOND)
								GROUP BY
									ire.UserId_int     
							</cfquery>							
							
							<cfset _numberCreditUsing=0>					
							<cfif TotalSentReceived.RecordCount GT 0>											
								<cfset _numberCreditUsing= LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received)>					
								<cfset _amount= _numberCreditUsing * _priceMesAfter / 100>	
							<cfelse>
								
							</cfif>		
							<!--- add total kw buy for amount--->
							<cfset _amount= _amount+ userPlansQuery.BuyKeywordNumber_int[threadId] * userPlansQuery.PriceKeywordAfter_dec[threadId]>
							<cfset _amount =NumberFormat(_amount,'.99')>
							<cfset _makePayment = 0 > <!--- check if user need to run payment --->
							<!---_makePayment 0 : do not run payment but still add new plan --->
							<!---_makePayment 1 : run payment --->
							<!---_makePayment 2 : do not run payment but do add new plan --->



							<cfset _recurringType = 0 > 
							<!---_recurringType 0 : renew monthly/yearly --->
							<!---_recurringType 1 : add benefit monthly --->
							<!---_recurringType 2 : payment for buy keyword monthly --->	

							<cfinvoke component="public.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
								<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
							</cfinvoke>
							

							<!--- Check CouponForUser --->
							<cfif CouponForUser.rxresultcode EQ 1>
								<cfloop array="#CouponForUser['DATALIST']#" index="index">
									<cfif index.COUPONDISCOUNTTYPE EQ 1>
										<cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
										<cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
										<cfset promotion_id = index.PROMOTIONID>
										<cfset promotion_origin_id = index.ORIGINID>
										<!--- <cfset promotion_used_time = index.USEDTIME> --->
										<cfset userPromotionId = index.userPromotionId>
									<cfelseif index.COUPONDISCOUNTTYPE EQ 2>
										<cfset promotion_id = index.PROMOTIONID>
										<cfset promotion_origin_id = index.ORIGINID>
										<!--- <cfset promotion_used_time = index.USEDTIME> --->
										<cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
										<cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
										<cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
										<cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
										<cfset userPromotionId = index.userPromotionId>
									<cfelse>
										<cfset promotion_id = index.PROMOTIONID>
										<cfset promotion_origin_id = index.ORIGINID>
										<cfset userPromotionId = index.userPromotionId>
										<!--- <cfset promotion_used_time = index.USEDTIME> --->
									</cfif>

									<cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, userPlansQuery.UserId_int[threadId],userPromotionId])/>
								</cfloop>
							</cfif>

							<cfif _numOfRecurringPlan LT 3 AND _amount GT 0>
								<cfset _makePayment = 1>						
							<cfelse>
								<cfset _makePayment = 2>	
							</cfif>


							<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
							<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
								<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
							</cfinvoke>				
							<cfset keywordUsed = getUserPlan.KEYWORDUSE/>
							<cfset shorturlUsed = getUserPlan.SHORTURLUSED/>
							<cfset mlpUsed = getUserPlan.MLPUSED/>
							

							<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
							<cfif ( _userPromotionKeyword GT 0 AND promotion_keyword LT _userPromotionKeyword ) 
								OR ( _userPromotionMLP GT 0 AND promotion_mlp LT _userPromotionMLP )  
								OR ( _userPromotionURL GT 0 AND promotion_shorturl < _userPromotionURL) >
								<cfset isRemoveCouponBenefit[threadId] = 1/>
							</cfif>	  
							
							<cfif getUserPlan.PLANEXPIRED EQ 1> <!--- only check if plan expriry within 30 days--->
								<cfif _makePayment EQ 1>							
									<cfif customerIndex GT 0 > <!--- If find user have token in securenet ---> 
										<cfset paymentError = 1>
										<cfset paymentData = {}>
										<cfif _paymentGateWay EQ 1 >
											<cfset paymentData = {  
												amount = _amount,
												paymentVaultToken = {  
													customerId = customersQuery.CCBelong[customerIndex],
													paymentMethodId = customersQuery.PaymentMethodID_vch[customerIndex],
													paymentType = _paymentType
												},
												developerApplication = {  
													developerId = worldpayApplication.developerId,
													version = worldpayApplication.version
												}
											}>

											<!--- log payment data --->
											
											
											<cfset paymentError = 1>
											<cfset paymentResponse = {
												status_code = '',
												status_text = '',
												errordetail = '',
												filecontent = ''
											}>

											<cftry>
												<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
													<cfhttpparam type="header" name="content-type" value="application/json">
													<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
													<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
												</cfhttp>
												<cfcatch>
													
												</cfcatch>
											</cftry>
												
											<!--- log payment data --->											
											
											<cfif paymentResponse.status_code EQ 200>
												<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
													<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
													<cfset paymentResponseContent.numberOfCredit = _numberCreditUsing/>
													<cfset paymentResponseContent.numberOfKeyword = userPlansQuery.BuyKeywordNumber_int[threadId]/>
													<cfset paymentResponseContent.ROOTURL = rootUrl/>
													<cfset paymentResponseContent.PlanName = plansQuery.PlanName_vch[planIndex]/>
													<cfset paymentResponseContent.SubAccId = userPlansQuery.UserId_int[threadId]/>
													<cfset paymentResponseContent.MasterId = usersQuery.HigherUserId[userIndex]/>
													<cfset paymentResponseContent.MailToMaster = 1/>
													
																								
																																		
													<cfif usersQuery.UserLevel_int[userIndex] EQ 2>
														<cfset emailsend = usersQuery.EmailAddress_vch[userIndex]/>
														<cfset paymentResponseContent.MailToMaster = 0/>
														<cfset subject ="[SIRE][Recurring]Your Account Recurring completed">											
													<cfelse>
														<cfset emailsend = usersQuery.HigherUserEmail[userIndex]/>
														<cfset paymentResponseContent.MailToMaster = 1/>
														<cfset subject ="[SIRE][Recurring]Your Sub Account Recurring completed">
													</cfif>
													
													<cfif paymentResponseContent.success>																						
														<cfset paymentError = 0>	
																																		
													<cfelse> <!---PAYMENT SERCUNET ERROR --->
														<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
														<cfset paymentErrorsPlans[threadId] = 1/>
													</cfif>
												<cfelse> <!---PAYMENT SERCUNET ERROR --->
													<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
													<cfset paymentErrorsPlans[threadId] = 1/>
												</cfif>
											<cfelse> <!---PAYMENT SERCUNET ERROR --->
												<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
												<cfset paymentErrorsPlans[threadId] = 1/>
											</cfif>
											
											<!--- <cfif arrayFind(errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId]) GT 0> --->
											<!--- LOG PAYMENT WORDPAY SUCESS --->
											<cfset arrayAppend(logPaymentWorldpays[1], [
												userPlansQuery.UserId_int[threadId],
												userPlansQuery.PlanId_int[threadId],
												'Recurring',
												userPlansQuery.BuyKeywordNumber_int[threadId],
												0,
												paymentResponseContent,
												_ccBelong
											])>
											<!--- LOG ALL PAYMENT WORDPAY --->
											<cfset arrayAppend(logPaymentWorldpays[2], [
												userPlansQuery.UserId_int[threadId],
												'Recurring',
												paymentResponse.status_code,
												paymentResponse.status_text,
												paymentResponse.errordetail,
												paymentResponse.filecontent,
												paymentData,
												_ccBelong
											])>
										
										<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>
											<cfset paymentData = {  
												amount = _amount,
												inpVaultId = customersQuery.PaymentMethodID_vch[customerIndex]
											}>
											
											
											<cfset paymentError = 1>
											<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>
											<cfif _paymentGateWay EQ 2>
												<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
													<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
													<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
													<cfinvokeargument name="inpModuleName" value="Recurring"/>
												</cfinvoke>		
											<cfelseif _paymentGateWay EQ 3>
												<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
													<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
													<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
													<cfinvokeargument name="inpModuleName" value="Recurring"/>
												</cfinvoke>		
											<cfelseif _paymentGateWay EQ 4>
												<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
													<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
													<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
													<cfinvokeargument name="inpModuleName" value="Recurring"/>
												</cfinvoke>		
											</cfif>						
											
																			
											<cfset paymentResponse.filecontent = rtSaleWithTokenzine.FILECONTENT>
											<cfif rtSaleWithTokenzine.RXRESULTCODE EQ 1>
												<cfset paymentError = 0>										

												<cfset transactionInfo = {
													cardHolder_FirstName: rtSaleWithTokenzine.REPORT.INPFIRSTNAME,
													cardHolder_LastName: rtSaleWithTokenzine.REPORT.INPLASTNAME,
													authorizedAmount: rtSaleWithTokenzine.REPORT.AMT,
													email:'',
													billAddress:{
														line1: rtSaleWithTokenzine.REPORT.INPLINE1,
														city: rtSaleWithTokenzine.REPORT.INPCITY,
														state: rtSaleWithTokenzine.REPORT.INPSTATE,
														zip: rtSaleWithTokenzine.REPORT.INPPOSTALCODE,
														country: '', 
														phone: rtSaleWithTokenzine.REPORT.PHONE												

													}

												}>
												<cfset paymentResponseContent.transaction=transactionInfo>
												<cfset paymentResponseContent.numberOfCredit = _numberCreditUsing/>
												<cfset paymentResponseContent.numberOfKeyword = userPlansQuery.BuyKeywordNumber_int[threadId]/>
												<cfset paymentResponseContent.ROOTURL = rootUrl/>
												<cfset paymentResponseContent.PlanName = plansQuery.PlanName_vch[planIndex]/>
												<cfset paymentResponseContent.SubAccId = userPlansQuery.UserId_int[threadId]/>
												<cfset paymentResponseContent.MasterId = usersQuery.HigherUserId[userIndex]/>
												<cfset paymentResponseContent.MailToMaster = 1/>	
												
																																	
												<cfif usersQuery.UserLevel_int[userIndex] EQ 2>
													<cfset emailsend = usersQuery.EmailAddress_vch[userIndex]/>
													<cfset paymentResponseContent.MailToMaster = 0/>
													<cfset subject ="[SIRE][Recurring]Your Account Recurring completed">											
												<cfelse>
													<cfset emailsend = usersQuery.HigherUserEmail[userIndex]/>
													<cfset paymentResponseContent.MailToMaster = 1/>
													<cfset subject ="[SIRE][Recurring]Your Sub Account Recurring completed">
												</cfif>
												<!--- log payment success only--->
												<cfinvoke component="session.sire.models.cfc.billing" method="UpdatePayment" returnvariable="rtUpdatePayment">
													<cfinvokeargument name="inpPaymentData" value="#SerializeJSON(rtSaleWithTokenzine.REPORT)#"/>
													<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">									
													<cfinvokeargument name="moduleName" value="Recurring"/>											
													<cfinvokeargument name="inpPlanId" value="#userPlansQuery.PlanId_int[threadId]#">
													<cfinvokeargument name="inpPaymentByUserId" value="#_ccBelong#"/>	
													<cfinvokeargument name="inpPaymentGateway" value="#_paymentGateWay#"/>	
												</cfinvoke>	
												
											<cfelse> <!---PAYMENT PAYPAL ERROR --->
												<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
												<cfset paymentErrorsPlans[threadId] = 1/>										
											</cfif>		
										</cfif>
										
										<cfif paymentError EQ 0>									
											
											<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
												<cfinvokeargument name="to" value="#emailsend#">
												<cfinvokeargument name="type" value="html">
												<cfinvokeargument name="subject" value="#subject#">
												<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_integrate_user_completed.cfm">
												<cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
											</cfinvoke>
											
											<!--- Set plan enddate --->
											<cfif _userBillingType EQ 1>
												<cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>	
											<cfelse>	
												<cfset _endDate = DateAdd("yyyy", 1, userPlansQuery.EndDate_dt[threadId])>	
											</cfif>	
											<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>										
											
											<!--- if renew plan -> need to remove old plan --->
											<cfif _recurringType EQ 0>
												<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
											</cfif>	

																						
											
											<cfif _recurringType EQ 0>
												<cfset arrayAppend(newUserPlans,[
													1,
													userPlansQuery.UserId_int[threadId],
													userPlansQuery.PlanId_int[threadId],
													//Variables.plansQuery.Amount_dec[planIndex],
													(
														_userBillingType EQ 1 ? plansQuery.Amount_dec[planIndex] : plansQuery.YearlyAmount_dec[planIndex]*12
													),
													plansQuery.UserAccountNumber_int[planIndex],
													plansQuery.Controls_int[planIndex],
													userPlansQuery.KeywordsLimitNumber_int[threadId],
													userPlansQuery.FirstSMSIncluded_int[threadId],
													(
														plansQuery.Amount_dec[planIndex] GT 0 ?
														0 : 
														userPlansQuery.UsedSMSIncluded_int[threadId]
													),
													plansQuery.CreditsAddAmount_int[planIndex],
													userPlansQuery.PriceMsgAfter_dec[threadId],
													plansQuery.PriceKeywordAfter_dec[planIndex],
													userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] ,
													userPlansQuery.EndDate_dt[threadId],
													_endDate,
													userPlansQuery.MlpsLimitNumber_int[threadId],
													userPlansQuery.ShortUrlsLimitNumber_int[threadId],
													userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
													promotion_origin_id,
													promotion_id,
													promotion_keyword,
													promotion_mlp,
													promotion_shorturl,
													_userBillingType,
													_monthlyDate
												])>

												<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
												<cfif isRemoveCouponBenefit[threadId] EQ 1 >
													<!--- RELEASE USER DATA --->
													<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
														<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
														<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
														<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
														<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
														<cfinvokeargument name="inpLimitKeyword" value="#userPlansQuery.KeywordsLimitNumber_int[threadId]+userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
														<cfinvokeargument name="inpLimitShortUrl" value="#userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
														<cfinvokeargument name="inpLimitMLP" value="#userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
													</cfinvoke>
												</cfif>
											</cfif>

											<!--- RESET CREDIT TO LASTEST USER PLAN --->
											<cfset arrayAppend(newBalanceBillings, [
												userPlansQuery.UserId_int[threadId], 
												userPlansQuery.FirstSMSIncluded_int[threadId], 
											])>
											
											<cfif promotion_credit GT 0>
												<!--- ADD PROMOTION CREDIT BALANCE --->
												<cfset arrayAppend(addPromotionCreditBalance, [
													userPlansQuery.UserId_int[threadId],
													promotion_credit
												])>
											</cfif>

											<!--- count promotion code used --->
											<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
												<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
											</cfloop>

											<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
											<cfinvoke method="AddReferralCredits" component="public.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
												<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
											</cfinvoke>

											
										</cfif>
										<!--- LOG RECURRING --->
										<cfset arrayAppend(logPaymentWorldpays[3], [
											now(),
											userPlansQuery.UserPlanId_bi[threadId],
											userPlansQuery.UserId_int[threadId],
											userPlansQuery.PlanId_int[threadId],
											usersQuery.FirstName_vch[userIndex],
											usersQuery.EmailAddress_vch[userIndex],
											_amount,
											SerializeJSON(paymentData),
											paymentError,
											SerializeJSON(paymentResponse),
											_paymentGateWay,
											_ccBelong
										])>
									<cfelse>
										<cfset paymentErrorsPlans[threadId] = 1/>
										<!--- If no customer in Sercurnet --->																			
										<cfset arrayAppend(errorUserPlans,userPlansQuery.UserPlanId_bi[threadId])>

										<!--- LOG RECURRING --->
										<cfset arrayAppend(logPaymentWorldpays[3], [
											now(),
											userPlansQuery.UserPlanId_bi[threadId],
											userPlansQuery.UserId_int[threadId],
											userPlansQuery.PlanId_int[threadId],
											usersQuery.FirstName_vch[userIndex],
											usersQuery.EmailAddress_vch[userIndex],
											_amount,
											"Account Not Found",
											'1',
											"No data in simplebilling.authorize_user",
											_paymentGateWay,
											_ccBelong
										])>

									</cfif>
																

									<!--- IF PAYMENT NOT SUCCESS --->
									<cfif paymentErrorsPlans[threadId] EQ 1 >
										
										<cfset declineDataContent = {} >								
										<cfset declineDataContent.HIGHERUSERID = usersQuery.HigherUserId[userIndex] >
										<cfset declineDataContent.USERID = userPlansQuery.UserId_int[threadId] >
										<cfset declineDataContent.PLANSNAME = plansQuery.PlanName_vch[planIndex]/>
										<cfset declineDataContent.PLANSTARTDATE = userPlansQuery.StartDate_dt[threadId] >
										<cfset declineDataContent.PLANENDDATE = userPlansQuery.EndDate_dt[threadId] >
										<cfset declineDataContent.PLANKEYWORD = userPlansQuery.KeywordsLimitNumber_int[threadId] >
										<cfset declineDataContent.PROMOKEYWORD = userPlansQuery.PromotionKeywordsLimitNumber_int[threadId] >
										<cfset declineDataContent.PURCHASEDKEYWORD = userPlansQuery.BuyKeywordNumber_int[threadId] >
										<cfset declineDataContent.ACTIVEKEYWORD = keywordUsed >
										<cfset declineDataContent.AVAILABLEKEYWORD = declineDataContent.PLANKEYWORD + declineDataContent.PROMOKEYWORD + declineDataContent.PURCHASEDKEYWORD - declineDataContent.ACTIVEKEYWORD>
										<cfset declineDataContent.MailToMaster = 1/>
										
										<cfset declineEmailsend = usersQuery.HigherUserEmail[userIndex]>
										
										<cfset declinePlanTime = userPlansQuery.NumOfRecurringPlan_int[threadId]>
										<cfset declineKeywordTime = userPlansQuery.NumOfRecurringKeyword_int[threadId]>							
										

										<cfif _recurringType EQ 0>																		
											<cfif paymentErrorsMonthly[threadId] EQ 1>
												<cfif userPlansQuery.NumOfRecurringPlan_int[threadId] EQ 0> <!--- send for 1st time --->
													<cfif usersQuery.UserLevel_int[userIndex] EQ 2>
														<cfset declineEmailsend = usersQuery.EmailAddress_vch[userIndex]>
														<cfset declineDataContent.MailToMaster = 0/>
														<cfset subject ="Do not make a payment for your account">
													<cfelse>
														<cfset declineEmailsend = usersQuery.HigherUserEmail[userIndex]>
														<cfset declineDataContent.MailToMaster = 1/>
														<cfset subject ="Do not make a payment for your sub account">
													</cfif>
													<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
														<cfinvokeargument name="to" value="#declineEmailsend#">
														<cfinvokeargument name="type" value="html">
														<cfinvokeargument name="subject" value="#subject#">
														<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_integrate_user_decline.cfm">
														<cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
													</cfinvoke>	
												<cfelse> <!--- send for the next  --->
													<cfset dataMail = {
														SubAccId=  userPlansQuery.UserId_int[threadId], 
														HIGHERUSERID				= usersQuery.HigherUserId[userIndex] , 												
														PlanName_vch			= plansQuery.PlanName_vch[planIndex],
														StartDate_dt			= userPlansQuery.StartDate_dt[threadId],
														EndDate_dt				= userPlansQuery.EndDate_dt[threadId],
														DownGradeDate_dt		= DateAdd("d", 3, userPlansQuery.EndDate_dt[threadId]),
														MailToMaster=1
													}>
													<cfif usersQuery.UserLevel_int[userIndex] EQ 2>
														<cfset declineEmailsend = usersQuery.EmailAddress_vch[userIndex]>
														<cfset dataMail.MailToMaster=0>
														<cfset subject ="Sire can not make a payment for your account">
													<cfelse>
														<cfset declineEmailsend = usersQuery.HigherUserEmail[userIndex]>
														<cfset dataMail.MailToMaster=1>
														<cfset subject ="Sire can not make a payment for your sub account">
													</cfif>
													<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
														<cfinvokeargument name="to" value="#declineEmailsend#">
														<cfinvokeargument name="type" value="html">
														<cfinvokeargument name="subject" value="#subject#">
														<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_fail_integrate_user.cfm">
														<cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
													</cfinvoke>
												</cfif>
											</cfif>
												
										</cfif>

										<cfif declinePlanTime EQ 2>
											<cfset declinedAmount = _amount/>
											<cfset tempCustomerInfo = {
												ID = "#userPlansQuery.UserId_int[threadId]#",
												EMAIL = "#declineEmailsend#",
												NAME = "#usersQuery.FirstName_vch[userIndex]# #usersQuery.LastName_vch[userIndex]#",
												PLAN = "#declineDataContent.PLANSNAME#",
												KEYWORD = "0",
												AMOUNT = "#declinedAmount#"
											}/>

											<cfset arrayAppend(userListToSendAlertCCDeclineToAdmin, tempCustomerInfo)/>
										</cfif>

										<!--- Update NumOfRecurring --->
										<cfinvoke method="UpdateNumberOfRecurring" component="public.sire.models.cfc.order_plan">
											<cfinvokeargument name="inpUserPlanId" value="#userPlansQuery.UserPlanId_bi[threadId]#">
											<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
											<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
										</cfinvoke>

									</cfif>
								<cfelse> 
									
									<cfif _userBillingType EQ 1>
										<cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>	
									<cfelse>	
										<cfset _endDate = DateAdd("yyyy", 1, userPlansQuery.EndDate_dt[threadId])>	
									</cfif>
									
									<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>

									

									<!--- RESET CREDIT TO LASTEST USER PLAN --->
								
									<cfif _numOfRecurringPlan EQ 3>
										
										<!--- send mail reset credit to 0--->
										
										<cfset dataMail = {
											MasterId = usersQuery.HigherUserId[userIndex],
											SubAccId=  userPlansQuery.UserId_int[threadId], 
											USERID				= userPlansQuery.UserId_int[threadId], 
											PlanName_vch			= plansQuery.PlanName_vch[planIndex],
											StartDate_dt			= userPlansQuery.StartDate_dt[threadId],
											EndDate_dt				= userPlansQuery.EndDate_dt[threadId],
											DownGradeDate_dt		= DateAdd("d", 30, userPlansQuery.EndDate_dt[threadId]),
											MailToMaster=1
										}>
										<cfif usersQuery.UserLevel_int[userIndex] EQ 2>
											<cfset emailsend = usersQuery.EmailAddress_vch[userIndex]>	
											<cfset dataMail.MailToMaster=0>																					
											<cfset subject ="Sire reset your account credit to zero">
										<cfelse>
											<cfset emailsend = usersQuery.HigherUserEmail[userIndex]>		
											<cfset dataMail.MailToMaster=1>																				
											<cfset subject ="Sire reset your sub account credit to zero">
										</cfif>
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
											<cfinvokeargument name="to" value="#emailsend#">
											<cfinvokeargument name="type" value="html">
											<cfinvokeargument name="subject" value="#subject#">
											<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_reset_credit_to_zero_integrate_user.cfm">
											<cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
										</cfinvoke>
										<!--- reset credit to 0--->
										<cfset arrayAppend(newBalanceBillings, [
											userPlansQuery.UserId_int[threadId], 
											0
										])>
									<cfelse>
										<cfif _recurringType EQ 0>
											<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>						    

											<cfset arrayAppend(newUserPlans,[
												1,
												userPlansQuery.UserId_int[threadId],
												userPlansQuery.PlanId_int[threadId],
												// Variables.plansQuery.Amount_dec[planIndex],
												(
													_userBillingType EQ 1 ? plansQuery.Amount_dec[planIndex] : plansQuery.YearlyAmount_dec[planIndex]*12
												),
												plansQuery.UserAccountNumber_int[planIndex],
												plansQuery.Controls_int[planIndex],
												userPlansQuery.KeywordsLimitNumber_int[threadId],
												userPlansQuery.FirstSMSIncluded_int[threadId],
												userPlansQuery.UsedSMSIncluded_int[threadId],
												plansQuery.CreditsAddAmount_int[planIndex],
												plansQuery.PriceMsgAfter_dec[planIndex],
												plansQuery.PriceKeywordAfter_dec[planIndex],
												userPlansQuery.BuyKeywordNumber_int[threadId],
												userPlansQuery.EndDate_dt[threadId],
												_endDate,
												userPlansQuery.MlpsLimitNumber_int[threadId],
												userPlansQuery.ShortUrlsLimitNumber_int[threadId],
												userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
												promotion_origin_id,
												promotion_id,
												promotion_keyword,
												promotion_mlp,
												promotion_shorturl,
												_userBillingType,
												_monthlyDate
											])>

											<cfif promotion_credit GT 0>
												<!--- ADD PROMOTION CREDIT BALANCE --->
												<cfset arrayAppend(addPromotionCreditBalance, [
												userPlansQuery.UserId_int[threadId],
												promotion_credit
												])>
											</cfif>

											<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
												<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
											</cfloop>

											<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
												<cfif isRemoveCouponBenefit[threadId] EQ 1>
												<!--- RELEASE USER DATA --->
												<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
													<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
													<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
													<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
													<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
													<cfinvokeargument name="inpLimitKeyword" value="#userPlansQuery.KeywordsLimitNumber_int[threadId]+userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
													<cfinvokeargument name="inpLimitShortUrl" value="#userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
													<cfinvokeargument name="inpLimitMLP" value="#userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
												</cfinvoke>
											</cfif>	
										
										</cfif> 
										<cfset arrayAppend(newBalanceBillings, [
											userPlansQuery.UserId_int[threadId], 
											userPlansQuery.FirstSMSIncluded_int[threadId]
										])>
									</cfif>
									<!--- Update NumOfRecurring --->
									<cfinvoke method="UpdateNumberOfRecurring" component="public.sire.models.cfc.order_plan">
										<cfinvokeargument name="inpUserPlanId" value="#userPlansQuery.UserPlanId_bi[threadId]#">
										<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
									</cfinvoke>

									<!--- RESET USER REFERRAL CREDITS --->
									<cfinvoke method="ResetUserReferral" component="public.sire.models.cfc.referral">
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
									</cfinvoke>
								</cfif>
							</cfif>

						<cfelseif userIndex GT 0 AND planIndex GT 0>
							
							<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
						<cfelse>
							
							<cfif userIndex LE 0>
								<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
							<cfelse>
								<cfset arrayAppend(oldUserPlans, [userPlansQuery.UserPlanId_bi[threadId], -1])>
							</cfif>
							<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
						</cfif>
						
						<cfset ___c++>															
					<cfset threadIndex++>
				</cfloop>
								
				
				<cfif !arrayIsEmpty(newUserPlans)>
					<!---  Other Discount --->
					<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
						INSERT INTO simplebilling.userplans (
							Status_int,
							UserId_int,
							PlanId_int,
							Amount_dec,
							UserAccountNumber_int,
							Controls_int,
							KeywordsLimitNumber_int,
							FirstSMSIncluded_int,
							UsedSMSIncluded_int,
							CreditsAddAmount_int,
							PriceMsgAfter_dec,
							PriceKeywordAfter_dec,
							BuyKeywordNumber_int,
							StartDate_dt,
							EndDate_dt,
							MlpsLimitNumber_int,
							ShortUrlsLimitNumber_int,
							MlpsImageCapacityLimit_bi,
							PromotionId_int,
							PromotionLastVersionId_int,
							PromotionKeywordsLimitNumber_int,
							PromotionMlpsLimitNumber_int,
							PromotionShortUrlsLimitNumber_int,
							BillingType_ti,
							MonthlyAddBenefitDate_dt,
							BuyKeywordNumberExpired_int,
							DowngradeDate_dt
						) VALUES
						<cfset _comma = ''> 
						<cfloop array="#newUserPlans#" index="_plan">
							<cfif  ArrayLen(_plan) LT 26>
								<cfset _plan[26] = 0 >
								<cfset _plan[27] = '' >
							<cfelseif ArrayLen(_plan) LT 27>
								<cfset _plan[27] = '' >
							</cfif>
							#_comma#(
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[3]#">,
								<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[4]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[5]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[6]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[7]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[8]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[9]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[10]#">,
								<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[11]#">,
								<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[12]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[13]#">,
								<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[14]#">,
								<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[15]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[16]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[17]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[18]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[19]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[20]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[21]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[22]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[23]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[24]#">,
								<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[25]#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[26]#">,
								<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[27]#" null="#NOT len(trim(_plan[27]))#">
							)
							<cfset _comma = ','> 
						</cfloop>
					</cfquery>
				</cfif>
						
				<cfif !arrayIsEmpty(newBalanceBillings)>
					<cfset userIdList = ''>
					<cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
						UPDATE simplebilling.billing 
							SET Balance_int = (CASE
								<cfloop array="#newBalanceBillings#" index="_balance">
									<cfset userIdList = listAppend(userIdList,_balance[1])>
									WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#_balance[1]#"> 
										THEN <cfqueryparam cfsqltype="cf_sql_decimal" value="#_balance[2]#">
								</cfloop>
								ELSE 0
							END)
						WHERE UserId_int IN (#userIdList#)
					</cfquery>			
				</cfif>
				
				<cfif !arrayIsEmpty(addPromotionCreditBalance)>
					<cfset userIdListPromotion = ''>
					<cfquery name="addPromoCreditBalance" datasource="#Session.DBSourceEBM#" result="addResult">
						UPDATE 
							simplebilling.billing
						SET
							PromotionCreditBalance_int = PromotionCreditBalance_int + (CASE
								<cfloop array="#addPromotionCreditBalance#" index="index">
								<cfset userIdListPromotion = listAppend(userIdListPromotion,index[1])>
								WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[1]#"/>
									THEN <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#index[2]#"/>
								</cfloop>
							END)
						WHERE 
							UserId_int IN (#userIdListPromotion#)
					</cfquery>
				</cfif>

				<cfif !arrayIsEmpty(promocodeListToIncreaseUsedTime)>
					<cfloop array="#promocodeListToIncreaseUsedTime#" index="index">
						<!--- Increase coupon code used time --->
						<cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
							<cfinvokeargument name="inpCouponId" value="#index[1]#"/>
							<cfinvokeargument name="inpUsedFor" value="1"/>
						</cfinvoke>
						<!--- End increase coupon code used time --->

						<!--- Insert coupon code used log --->
						<cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
							<cfinvokeargument name="inpCouponId" value="#index[2]#"/>
							<cfinvokeargument name="inpOriginCouponId" value="#index[1]#"/>
							<cfinvokeargument name="inpUserId" value="#index[3]#"/>
							<cfinvokeargument name="inpUsedFor" value="Recurring"/>
						</cfinvoke>    
						<!--- End insert log --->

						<cfquery name="updateUsedTimePromoCode" datasource="#Session.DBSourceEBM#" result="updateResult">
						UPDATE 
							simplebilling.userpromotions
						SET
							UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
						WHERE 
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[3]#">
						AND 
							PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[2]#"> 
						AND 
							UserPromotionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[4]#">
						</cfquery>
					</cfloop>
				</cfif>


				
				<cfif !arrayIsEmpty(oldUserPlans)>
					<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
						UPDATE simplebilling.userplans 
							SET Status_int = (CASE
								<cfset i = 1>
								<cfloop array="#oldUserPlans#" index="_plan">
									<cfif isArray(_plan)>
										WHEN UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#"> 
											THEN <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">
										<cfset oldUserPlans[i] = _plan[1]>
									</cfif>
									<cfset i++>
								</cfloop>
								WHEN 0 = 1 THEN Status_int ELSE 0
							END) 
						WHERE UserPlanId_bi IN (#arrayToList(oldUserPlans, ',')#)
					</cfquery>
				</cfif>
				
				<cfif !arrayIsEmpty(errorUserPlans)>
					<cfset logData.ErrorNumber_bi += arrayLen(errorUserPlans)>
				</cfif>
				<cfif !arrayIsEmpty(userListToSendAlertCCDeclineToAdmin)>
					<cfset datalistacc = {}/>
					<cfset datalistacc.RESULT = 1/>
					<cfset datalistacc['DATALIST'] = userListToSendAlertCCDeclineToAdmin/>

					<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
						<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
						<cfinvokeargument name="type" value="html"/>
						<cfinvokeargument name="subject" value="[SIRE] Customer's credit card declined 3 times"/>
						<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_alert_cc_declined_for_admin.cfm"/>
						<cfinvokeargument name="data" value="#TRIM(serializeJSON(datalistacc))#"/>
					</cfinvoke>
				</cfif>

				<!--- Log payment worldpay response --->
				<cfinvoke method="logPaymentWorldPay" component="public.sire.models.cfc.order_plan">
					<cfinvokeargument name="inplogPaymentWorldpays" value="#logPaymentWorldpays#"/>
					<cfinvokeargument name="inpFcm" value="#_fcm#"/>
					<cfinvokeargument name="inpNow" value="#_now#"/>
				</cfinvoke>    

				<cftry>
					<cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
						<cfhttpparam type="header" name="content-type" value="application/json">
						<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
						<cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
					</cfhttp>
					<cfcatch>

					</cfcatch>
				</cftry>			
			</cfif>
			<cfset dataout.RESULT = 1>
        	<cfset dataout.MESSAGE = 'Re-charge successfully.'>
			<cfcatch>				
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>		
        <cfreturn dataout />

	</cffunction>
    <cffunction name="Rechargeaccount" access="remote" hint="Re-charge">
        <cfargument name="_nextPage" required="no" default="0">
		<cfargument name="_pageOne_now" required="no" default="">
		<cfargument name="logFileNameIndex" required="no" default="0">
		<cfargument name="logDataStartDate_dt" required="no" default="">
		<cfargument name="logDataAccountNumber_bi" required="no" default="0">
		<cfargument name="logDataErrorNumber_bi" required="no" default="0">
		<cfargument name="UserId" required="yes" default="">

        <cfinclude template="/public/paths.cfm">
        <cfinclude template="/public/sire/configs/paths.cfm">
    
        <cfinclude template="/session/sire/configs/credits.cfm">
		<!--- -1: Error, 0: ----, 1: successfully --->
        <cfset var dataout = {} />
		<cfset dataout.RESULT = 0>
        <cfset dataout.MESSAGE = ''>

		<cfset var userlog_vch = ''>
		<cfset var _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
		<cfset var _fcm = left(_now, 7)>
		<cfset var userPlansQuery = ''/>
		<cfset var usersQuery = ''/>
		<cfset var plansQuery = ''/>
		<cfset var customersQuery = ''/>
		<cfset var getDowngradePlan = ''>
		<cfset var getActiveKeyword = ''>
		<cfset var insertUserPlans = ''>
		<cfset var updateBalanceBillings =''>
		<cfset var addPromoCreditBalance =''>
		<cfset var updateUsedTimePromoCode =''>
		<cfset var expiredUserPlans =''>
		<cfset var Rechargeloginfor =''>
		<cfset var userPlansResult =''>
		<cfset var balanceBillingsResult =''>
		<cfset var addResult =''>
		<cfset var updateResult =''>
		<cfset var RetVarGetAdminPermission = ''>
		<cfset var CouponForUser = ''>
		<cfset var returngetUserPlan = ''>
		<cfset var RetAddReferralCredits = ''>
		<cfset var index = ''>
		<cfset var _balance = ''>
		<cfset var _plan = []>
		<cfset var _userFullName =""/>

        <cfset var rtSaleWithTokenzine =""/>
        <cfset var rtUpdatePayment =""/>

		<!--- UPDATE: Check admin permission --->
		<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>	
		<cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
			<cfset dataout.RESULT = -1>
			<cfset dataout.MESSAGE = "No Admin Permission."/>
			<cfreturn dataout>
		</cfif>
		
        <!--- Get all userplan need to run. Except runing on other threead --->
       <cfquery name="userPlansQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserPlanId_bi,Status_int,UserId_int,PlanId_int,BillingType_ti,Amount_dec,UserAccountNumber_int,Controls_int,KeywordsLimitNumber_int,KeywordMinCharLimit_int,FirstSMSIncluded_int,
				UsedSMSIncluded_int,CreditsAddAmount_int,PriceMsgAfter_dec,PriceKeywordAfter_dec,StartDate_dt,EndDate_dt,MonthlyAddBenefitDate_dt,MlpsLimitNumber_int,ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi,BuyKeywordNumber_int,BuyKeywordNumberExpired_int,DowngradeDate_dt,PromotionId_int,PromotionKeywordsLimitNumber_int,PromotionMlpsLimitNumber_int, 
				PromotionShortUrlsLimitNumber_int,PromotionLastVersionId_int,UserDowngradeDate_dt,UserDowngradePlan_int,UserDowngradeKeyword_txt,UserDowngradeMLP_txt,UserDowngradeShortUrl_txt,
				NumOfRecurringKeyword_int,NumOfRecurringPlan_int, DATEDIFF(CURDATE(),MonthlyAddBenefitDate_dt) as expireKeywordDateDiff, DATEDIFF(CURDATE(),EndDate_dt) as expirePlanDateDiff 
			FROM 
				simplebilling.userplans
			WHERE 
				Status_int = 1
				AND (DATE(EndDate_dt) <= CURDATE() OR MonthlyAddBenefitDate_dt <= CURDATE())
				AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
        </cfquery>

        <!--- UPDATE: If not found return false --->
		<cfif userPlansQuery.RECORDCOUNT LT 1>
				<cfset dataout.RESULT = -1>
        		<cfset dataout.MESSAGE = 'User Plans unvalid.'>
				<cfreturn dataout>
		</cfif>

		<!--- get all user info --->
		<cfquery name="usersQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch , YearlyRenew_ti
			FROM 
				simpleobjects.useraccount
			WHERE 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
			AND 
				Active_int = 1
		</cfquery>
		<cfset _userFullName = usersQuery.FirstName_vch & ' ' & usersQuery.LastName_vch/>

        <!--- check usersQuery.RecordCount --->

		<!--- get all active plan info --->
		<cfquery name="plansQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				PlanId_int, PlanName_vch, Amount_dec, YearlyAmount_dec, UserAccountNumber_int, Controls_int, KeywordsLimitNumber_int, KeywordMinCharLimit_int,
				FirstSMSIncluded_int, CreditsAddAmount_int, PriceMsgAfter_dec, PriceKeywordAfter_dec, ByMonthNumber_int, MlpsLimitNumber_int, ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi, Order_int
			FROM 
				simplebilling.plans
			WHERE 
				Status_int in (1,8)
			ORDER BY 
				PlanId_int DESC
		</cfquery>

		<!--- GET FREE PLAN --->
		<cfset var planFree = {}>

		<cfloop query="plansQuery">
			<cfif PlanId_int EQ 1>
				<cfset planFree['Amount_dec'] = Amount_dec >
				<cfset planFree['UserAccountNumber_int'] = UserAccountNumber_int >
				<cfset planFree['KeywordsLimitNumber_int'] = KeywordsLimitNumber_int >
				<cfset planFree['FirstSMSIncluded_int'] = FirstSMSIncluded_int >
				<cfset planFree['CreditsAddAmount_int'] = CreditsAddAmount_int >
				<cfset planFree['PriceMsgAfter_dec'] = PriceMsgAfter_dec >
				<cfset planFree['PriceKeywordAfter_dec'] = PriceKeywordAfter_dec >
				<cfset planFree['PlanId_int'] = PlanId_int >
				<cfset planFree['PlanName_vch'] = PlanName_vch >
				<cfset planFree['Controls_int'] = Controls_int >
				<cfset planFree['MlpsLimitNumber_int'] = MlpsLimitNumber_int >
				<cfset planFree['ShortUrlsLimitNumber_int'] = ShortUrlsLimitNumber_int >
				<cfset planFree['MlpsImageCapacityLimit_bi'] = MlpsImageCapacityLimit_bi >
			</cfif>
		</cfloop>

		<!--- get user authen with sercurenet for auto run payment --->
		<cfquery name="customersQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				uath.id,uath.PaymentMethodID_vch,uath.PaymentType_vch,uath.UserId_int, uath.PaymentGateway_ti
			FROM 
				simplebilling.authorize_user uath
			WHERE 
				uath.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
			AND 
				uath.status_ti = 1	
			AND
				uath.PaymentGateway_ti = (
					Select  PaymentGateway_ti
					FROM 	simpleobjects.useraccount
					WHERE 	UserId_int = uath.UserId_int
				)
			ORDER BY 
				updated_dt desc, id DESC
		</cfquery>

		<cfset var closeBatchesData = {}>
		<cfset closeBatchesData = {
			developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.version
			}
		}>
	
		<cfset var oldUserPlans = []>
		<cfset var newUserPlans = []>
		<cfset var addPromotionCreditBalance = []>
		<cfset var promocodeListToIncreaseUsedTime = []>
		<cfset var promocodeListToIncreaseUsedTimeTemp = []>
		<cfset var errorUserPlans = []>
		<cfset var newBalanceBillings = []>
		<cfset var logPaymentWorldpays = [[],[],[]]>
		<cfset var paymentErrorsPlans = {}>

		<cfset var threadIndex = 1>
		<cfset var emailsend = ''/>
		<cfset var threadId = 1>
		<cfset var planIndex = 0>
		<cfset var userIndex = 0>
		<cfset var customerIndex = 0>
		<cfset var paymentErrorsMonthly = []>
		<cfset var isRemoveCouponBenefit = []>
		<cfset var isDowngrade = []>
		<cfset var paymentResponsePaypal = []/>
		<cfset var _paymentGateWay = 0/>
		<cfif customersQuery.RECORDCOUNT GT 0>
			<cfset _paymentGateWay = customersQuery.PaymentGateway_ti/>
		</cfif>

        <cfloop query="userPlansQuery">
			<cfset var threadId = threadIndex/>
				
				<cfset planIndex = plansQuery.PlanId_int.IndexOf(JavaCast('int', userPlansQuery.PlanId_int[threadId])) + 1>
				<cfset userIndex = usersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>
				<cfset customerIndex = customersQuery.UserId_int.IndexOf(JavaCast('int', userPlansQuery.UserId_int[threadId])) + 1>
				<cfset 	paymentErrorsPlans[threadId] = 0/>
				<cfset  isDowngrade[threadId] = 0/>
				<cfset  isRemoveCouponBenefit[threadId] = 0/>
				<cfset  promocodeListToIncreaseUsedTimeTemp = []/>
				<cfset  paymentErrorsMonthly[threadId] = 1/>

				<cfif userIndex GT 0 AND planIndex GT 0>

					<cfset var _paymentType = customersQuery.PaymentType_vch[customerIndex]>
					<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
					<cfset var _totalkeywordHaveToPaid = 0/>

					<!--- User Downgrade info --->
					<cfset var _userDowngradePlan = userPlansQuery.UserDowngradePlan_int[threadId]/>
					<cfset var _userDowngradeKeyword = deserializeJSON(userPlansQuery.UserDowngradeKeyword_txt[threadId])/>
					<cfset var _userDowngradeShortUrl = deserializeJSON(userPlansQuery.UserDowngradeShortUrl_txt[threadId])/>
					<cfset var _userDowngradeMLP = deserializeJSON(userPlansQuery.UserDowngradeMLP_txt[threadId])/>
					<cfset var _downgradePlanName = ''/>
					<cfset var _downgradePlanCredits = 0/>
					<cfset var _downgradePlanKeywords = 0 />

					<cfset var _userBillingType = userPlansQuery.BillingType_ti[threadId]/>
					<cfset var _numOfRecurringKeyword = userPlansQuery.NumOfRecurringKeyword_int[threadId]/> 
					<cfset var _numOfRecurringPlan = userPlansQuery.NumOfRecurringPlan_int[threadId]/> 
					<cfset var _expirePlanDateDiff = userPlansQuery.expirePlanDateDiff[threadId]/> 
					<cfset var _expireKeywordDateDiff = userPlansQuery.expireKeywordDateDiff[threadId]/> 
					<cfset var _yearlyRenew = usersQuery.YearlyRenew_ti[userIndex]>

					<!--- Promotion benefit --->
					<cfset var _userPromotionKeyword = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset var _userPromotionMLP = userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset var _userPromotionURL = userPlansQuery.PromotionShortUrlsLimitNumber_int[threadId]/>

					<cfset var percent_dis = 0>
					<cfset var flatrate_dis = 0>
					<cfset var promotion_credit = 0>
					<cfset var promotion_mlp = 0>
					<cfset var promotion_keyword = 0>
					<cfset var promotion_shorturl = 0>
					<cfset var userPromotionId = 0>
					<cfset var promotion_id = 0>
					<cfset var promotion_origin_id = 0>

					<cfset var _monthAmount = 0 /> <!--- use when try to charge monthly of yearly plan --->

					<cfset var _amount = 0 > <!--- total amount have to paid --->

					<cfset var _makePayment = 1 > <!--- check if user need to run payment --->
					<!---_makePayment 0 : do not run payment but still add new plan --->
					<!---_makePayment 1 : run payment --->
					<!---_makePayment 2 : do not run payment but do not add new plan --->



					<cfset var _recurringType = 0 > 
					<cfset var paymentResponseContent = []/>
					<!---_recurringType 0 : renew monthly/yearly --->
					<!---_recurringType 1 : add benefit monthly --->
					<!---_recurringType 2 : payment for buy keyword monthly --->

					<cfinvoke component="public.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
	                  <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
	                </cfinvoke>

					<!--- Check CouponForUser --->
					<cfif CouponForUser.rxresultcode EQ 1>
						<cfloop array="#CouponForUser['DATALIST']#" index="index">
							<cfif index.COUPONDISCOUNTTYPE EQ 1>
								<cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
								<cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								
								<cfset userPromotionId = index.userPromotionId>
							<cfelseif index.COUPONDISCOUNTTYPE EQ 2>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								
								<cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
								<cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
								<cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
								<cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
								<cfset userPromotionId = index.userPromotionId>
							<cfelse>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								<cfset userPromotionId = index.userPromotionId>
								
							</cfif>

							<cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, userPlansQuery.UserId_int[threadId],userPromotionId])/>
						</cfloop>
					</cfif>

					<!--- calc amount when renew plan --->
					<cfif _userBillingType EQ 2 AND _yearlyRenew EQ 1> <!--- yearly and set renew --->
						<cfset _amount = (plansQuery.YearlyAmount_dec[planIndex]*12 - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>	

						<cfset _monthAmount = (plansQuery.Amount_dec[planIndex] - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>
					<cfelse>
						<cfset _amount = (plansQuery.Amount_dec[planIndex] - flatrate_dis - ((plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex])>	
					</cfif> 
					
					<!--- if user buy plan yearly recalc _amount, check to see if it run monthly add benefit --->
					<cfif _userBillingType EQ 2 AND isDate(userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
						<!--- if this is recurring monthly EndDate_dt later than MonthlyAddBenefitDate_dt  --->
						<cfif DateCompare(userPlansQuery.EndDate_dt[threadId],userPlansQuery.MonthlyAddBenefitDate_dt[threadId]) EQ 1 AND DateCompare(userPlansQuery.EndDate_dt[threadId], NOW()) EQ 1  >
							<!--- if user buy keyword -> need to pay for these keywords --->
							<cfif userPlansQuery.BuyKeywordNumber_int[threadId] GT 0>
								<cfset _amount = userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex]>	
								<cfset _recurringType = 2 > 
							<cfelse>
								<!--- add benefit monthly --->
								<cfset _amount = 0 >
								<cfset _recurringType = 1 > 
							</cfif>
						</cfif>
					</cfif>

					<!--- check if yearly plan but do not auto renew (_yearlyRenew = 0) --->
					<cfif _recurringType EQ 0 AND _userBillingType EQ 2 AND _yearlyRenew EQ 0>
						<cfset _userBillingType = 1 />
					</cfif>

					<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
					<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="returngetUserPlan">
			            <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
			        </cfinvoke>

			       	<cfset var keywordUsed = returngetUserPlan.KEYWORDUSE/>
			       	<cfset var shorturlUsed = returngetUserPlan.SHORTURLUSED/>
			       	<cfset var mlpUsed = returngetUserPlan.MLPUSED/>
			       	
					<!--- check if user select downgrade and have to paid --->
					<cfif _userDowngradePlan GT 0>
						<cfset isDowngrade[threadId] = 1/>
						<!--- get downgrade plan info --->
						<cfquery name="getDowngradePlan" datasource="#Session.DBSourceREAD#">
							SELECT 
								PlanId_int,
								PlanName_vch,
								Amount_dec,
								YearlyAmount_dec,
								UserAccountNumber_int,
								Controls_int, 
								KeywordsLimitNumber_int, 
								KeywordMinCharLimit_int, 
								FirstSMSIncluded_int,
								CreditsAddAmount_int,
								PriceMsgAfter_dec,
								PriceKeywordAfter_dec,
								ByMonthNumber_int,
								MlpsLimitNumber_int,
								ShortUrlsLimitNumber_int,
								MlpsImageCapacityLimit_bi,
								Order_int
							FROM 
								simplebilling.plans
							WHERE 
								Status_int = 1
							AND
								PlanId_int = #_userDowngradePlan#
						</cfquery>

						<cfif getDowngradePlan.RecordCount GT 0>

							<cfset _downgradePlanName = getDowngradePlan.PlanName_vch/>
							<cfset _downgradePlanCredits = getDowngradePlan.FirstSMSIncluded_int/>
							<cfset _downgradePlanKeywords = getDowngradePlan.KeywordsLimitNumber_int />
							<!--- get user keyword have to paid --->
							<cfset _totalkeywordHaveToPaid = keywordUsed - getDowngradePlan.KeywordsLimitNumber_int - userPlansQuery.BuyKeywordNumber_int[threadId]/>
							
							<!--- if _totalkeywordHaveToPaid greater than 0 then check downgrade keyword --->
							<cfif _totalkeywordHaveToPaid GT 0>
								<cfset var currentRemoveKeyword = 0> 
								<!--- check if keyword user select is still active or not --->
								<cfif arrayLen(_userDowngradeKeyword) GT 0>
									<cfquery name="getActiveKeyword" datasource="#Session.DBSourceREAD#">
										SELECT 
											KeywordId_int
										FROM 
											sms.keyword 
										WHERE 
											Active_int = 1
										AND
											KeywordId_int IN (#arrayToList(_userDowngradeKeyword)#) 
									</cfquery>
									<cfif getActiveKeyword.RecordCount GT 0>
										<cfset currentRemoveKeyword = getActiveKeyword.RecordCount/>
									</cfif>
									<cfset _totalkeywordHaveToPaid = _totalkeywordHaveToPaid - currentRemoveKeyword/>
								</cfif>
							<cfelse>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>	

							<cfif _totalkeywordHaveToPaid LT 0>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>
							
							<cfset _amount = getDowngradePlan.Amount_dec + (userPlansQuery.BuyKeywordNumber_int[threadId] * plansQuery.PriceKeywordAfter_dec[planIndex]) + (_totalkeywordHaveToPaid * getDowngradePlan.PriceKeywordAfter_dec) >
						</cfif>
					</cfif>

					<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
					<cfif ( _userPromotionKeyword GT 0 AND promotion_keyword LT _userPromotionKeyword ) 
						  OR ( _userPromotionMLP GT 0 AND promotion_mlp LT _userPromotionMLP )  
						  OR ( _userPromotionURL GT 0 AND promotion_shorturl < _userPromotionURL) >
						  <cfset isRemoveCouponBenefit[threadId] = 1/>
					</cfif>	  

					<cfif _amount GT 0 AND _makePayment EQ 1> <!--- IF USER HAVE TO PAID $ --->

						<cfif customerIndex GT 0 > <!--- If find user have token in securenet ---> 
							<!--- WorldPay--->
							<cfif _paymentGateWay EQ 1>

									<cfset var paymentData = {}>
									<cfset paymentData = {  
										amount = _amount,
										paymentVaultToken = {  
											customerId = customersQuery.UserId_int[customerIndex],
											paymentMethodId = customersQuery.PaymentMethodID_vch[customerIndex],
											paymentType = _paymentType
										},
										developerApplication = {  
											developerId = worldpayApplication.developerId,
											version = worldpayApplication.version
										}
									}>
									<!--- log payment data --->
									<cfset var paymentError = 1>
									<cfset var paymentResponse = {}>
									<cfset paymentResponse = {
										status_code = '',
										status_text = '',
										errordetail = '',
										filecontent = ''
									}>
									<cftry>
										<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
											<cfhttpparam type="header" name="content-type" value="application/json">
											<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
											<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
										</cfhttp>
										<cfcatch>
											<!--- Recurring aready --->
											<cfset dataout.RESULT = -1>
											<cfset dataout.MESSAGE = 'Can not send request Payment.' /> 
											<cfreturn dataout/>
										</cfcatch>
									</cftry>
									<cfif paymentResponse.status_code EQ 200>
										<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
											<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
											<cfif paymentResponseContent.success>
												<cfset paymentError = 0>
												<!--- LOG PAYMENT WORDPAY SUCESS --->
												<cfset arrayAppend(logPaymentWorldpays[1], [
													userPlansQuery.UserId_int[threadId],
													userPlansQuery.PlanId_int[threadId],
													'Recurring',
													userPlansQuery.BuyKeywordNumber_int[threadId],
													0,
													paymentResponseContent
												])>	
											<cfelse> <!---PAYMENT SERCUNET ERROR --->
												<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
												<cfset paymentErrorsPlans[threadId] = 1/>
											</cfif>
										<cfelse> <!---PAYMENT SERCUNET ERROR --->
											<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
											<cfset paymentErrorsPlans[threadId] = 1/>
										</cfif>
									<cfelse> <!---PAYMENT SERCUNET ERROR --->
										<cfset arrayAppend(errorUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
										<cfset paymentErrorsPlans[threadId] = 1/>
									</cfif>

									<!--- LOG ALL PAYMENT WORDPAY --->
									<cfset arrayAppend(logPaymentWorldpays[2], [
										userPlansQuery.UserId_int[threadId],
										'Recurring',
										paymentResponse.status_code,
										paymentResponse.status_text,
										paymentResponse.errordetail,
										paymentResponse.filecontent,
										paymentData
									])>

							
							<cfelseif _paymentGateWay EQ 2 OR  _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>																														
					        	<cfset paymentData = {  
								   amount = _amount,
								   inpVaultId = customersQuery.PaymentMethodID_vch[customerIndex]
								}>								
								
								<cfset paymentError = 1>
								<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>
								<cfif _paymentGateWay EQ 2>
									<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>								
								<cfelseif _paymentGateWay EQ 3>
									<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>	
								<cfelseif _paymentGateWay EQ 4>
									<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>	
								</cfif>
																
								<cfset paymentResponse.filecontent = rtSaleWithTokenzine.FILECONTENT>
								
								<cfif rtSaleWithTokenzine.RXRESULTCODE EQ 1>
									<cfset paymentError = 0>
									<cfset paymentResponseContent ={}/>																
									<cfset paymentResponseContent.FullName = _userFullName/>
									<!--- log payment success only--->
									<cfinvoke component="session.sire.models.cfc.billing" method="UpdatePayment" returnvariable="rtUpdatePayment">
										<cfinvokeargument name="inpPaymentData" value="#SerializeJSON(rtSaleWithTokenzine.REPORT)#"/>
										<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="moduleName" value="Recurring"/>											
										<cfinvokeargument name="inpPlanId" value="#userPlansQuery.PlanId_int[threadId]#">	
										<cfinvokeargument name="inpPaymentByUserId" value="#userPlansQuery.UserId_int[threadId]#"/>	
										<cfinvokeargument name="inpPaymentGateway" value="#_paymentGateWay#"/>
									</cfinvoke>	
									
								<cfelse> <!---PAYMENT PAYPAL ERROR --->
									<cfset paymentError = 1>
									<cfset dataout.RESULT = -1>
									<cfset dataout.MESSAGE = 'Re-charge fail.'>									
									<cfreturn dataout />								
								</cfif>		

							</cfif>
							
							<!--- log payment data --->
							<cfif paymentError EQ 0>
									<cfset paymentResponseContent.numberOfCredit = userPlansQuery.FirstSMSIncluded_int[threadId]/>
									<cfset paymentResponseContent.numberOfKeyword = userPlansQuery.BuyKeywordNumber_int[threadId]/>
									<cfset paymentResponseContent.ROOTURL = rootUrl/>
									<cfset paymentResponseContent.PlanName = plansQuery.PlanName_vch[planIndex]/>
									<cfset paymentResponseContent.authorizedAmount = _amount/>

									<cfif isDowngrade[threadId] EQ 1>
										<cfset paymentResponseContent.PlanName = _downgradePlanName/>
										<cfset paymentResponseContent.numberOfCredit = _downgradePlanCredits/>
										<cfset paymentResponseContent.numberOfKeyword = _downgradePlanKeywords/>
									</cfif>	

									<!--- log payment data --->
									<cfset emailsend = usersQuery.EmailAddress_vch[userIndex]/>
									<cfif _paymentGateWay EQ 1>
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
											<cfinvokeargument name="to" value="#emailsend#">
											<cfinvokeargument name="type" value="html">
											<cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
											<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed.cfm">
											<cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
										</cfinvoke>									
									<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#emailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed_mojo.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
							        	</cfinvoke>
									</cfif>

									<!--- Set plan enddate --->
									<cfset var _endDate = ''>
									<cfif _userBillingType EQ 1>
										<cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>	
									<cfelse>	
										<cfset _endDate = DateAdd("yyyy", 1, userPlansQuery.EndDate_dt[threadId])>	
									</cfif>
									<cfset var _monthlyDate = ''>
									<cfif _recurringType EQ 2 AND IsDate(userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
										<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
									<cfelse>		
										<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>
									</cfif>
									
									<!--- if renew plan -> need to remove old plan --->
									<cfif _recurringType EQ 0>
										<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
									</cfif>	

									<!--- check if is downgrade then add new plan = downgradeplan --->
									<!--- buy keyword = keyword buy + buy after expired + keyword need to paid when downgrade --->
									<cfif isDowngrade[threadId] EQ 1>

										<cfset arrayAppend(newUserPlans,[
											1,
											userPlansQuery.UserId_int[threadId],
											getDowngradePlan.PlanId_int,
											getDowngradePlan.Amount_dec,
											getDowngradePlan.UserAccountNumber_int,
											getDowngradePlan.Controls_int,
											getDowngradePlan.KeywordsLimitNumber_int,
											getDowngradePlan.FirstSMSIncluded_int,
											(
												getDowngradePlan.Amount_dec GT 0 ?
												0 : 
												userPlansQuery.UsedSMSIncluded_int[threadId]
											),
											getDowngradePlan.CreditsAddAmount_int,
											getDowngradePlan.PriceMsgAfter_dec,
											getDowngradePlan.PriceKeywordAfter_dec,
											userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid,
											userPlansQuery.EndDate_dt[threadId],
											DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId]),
											getDowngradePlan.MlpsLimitNumber_int,
											getDowngradePlan.ShortUrlsLimitNumber_int,
											getDowngradePlan.MlpsImageCapacityLimit_bi,
											0,
											0,
											0,
											0,
											0,
											1,
											DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])
											])>

											<!---DEACTIVE USER DATA IF USER'VE ALREADY SELECT KW,MLP,URL --->
										<cfinvoke method="DeactiveUserData" component="public.sire.models.cfc.order_plan">
											<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
											<cfinvokeargument name="inpDowngradeKeyword" value="#_userDowngradeKeyword#">
											<cfinvokeargument name="inpDowngradeShortUrl" value="#_userDowngradeShortUrl#">
											<cfinvokeargument name="inpDowngradeMLP" value="#_userDowngradeMLP#">
										</cfinvoke>

										<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
										<!--- <cfif isRemoveCouponBenefit[threadId] EQ 1 > --->
											<!--- RELEASE USER DATA --->
											<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
												<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
												<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
												<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
												<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
												<cfinvokeargument name="inpLimitKeyword" value="#getDowngradePlan.KeywordsLimitNumber_int + userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid#">
												<cfinvokeargument name="inpLimitShortUrl" value="#getDowngradePlan.ShortUrlsLimitNumber_int#">
												<cfinvokeargument name="inpLimitMLP" value="#getDowngradePlan.MlpsLimitNumber_int#">
											</cfinvoke>
										<!--- </cfif>	 --->

										<!--- RESET CREDIT TO downgrade plan credit --->
										<cfset arrayAppend(newBalanceBillings, [
											userPlansQuery.UserId_int[threadId], 
											_downgradePlanCredits, 
										])>

										<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>

									<cfelse> <!--- insert new plan or reset credits --->
										<cfif _recurringType EQ 0>
											<cfset arrayAppend(newUserPlans,[
												1,
												userPlansQuery.UserId_int[threadId],
												userPlansQuery.PlanId_int[threadId],
												//plansQuery.Amount_dec[planIndex],
												(
													_userBillingType EQ 1 ? plansQuery.Amount_dec[planIndex] : plansQuery.YearlyAmount_dec[planIndex]*12
												),
												plansQuery.UserAccountNumber_int[planIndex],
												plansQuery.Controls_int[planIndex],
												userPlansQuery.KeywordsLimitNumber_int[threadId],
												userPlansQuery.FirstSMSIncluded_int[threadId],
												(
													plansQuery.Amount_dec[planIndex] GT 0 ?
													0 : 
													userPlansQuery.UsedSMSIncluded_int[threadId]
												),
												plansQuery.CreditsAddAmount_int[planIndex],
												userPlansQuery.PriceMsgAfter_dec[threadId],
												plansQuery.PriceKeywordAfter_dec[planIndex],
												userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] ,
												userPlansQuery.EndDate_dt[threadId],
												_endDate,
												userPlansQuery.MlpsLimitNumber_int[threadId],
												userPlansQuery.ShortUrlsLimitNumber_int[threadId],
												userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
												promotion_origin_id,
												promotion_id,
												promotion_keyword,
												promotion_mlp,
												promotion_shorturl,
												_userBillingType,
												_monthlyDate
												])>

											<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
											<cfif isRemoveCouponBenefit[threadId] EQ 1 >
												<!--- RELEASE USER DATA --->
												<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
													<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
													<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
													<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
													<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
													<cfinvokeargument name="inpLimitKeyword" value="#userPlansQuery.KeywordsLimitNumber_int[threadId]+userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
													<cfinvokeargument name="inpLimitShortUrl" value="#userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
													<cfinvokeargument name="inpLimitMLP" value="#userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
												</cfinvoke>
											</cfif>

										<cfelseif _recurringType EQ 2> <!--- update date to add benefit --->
											<cfinvoke method="UpdateUserPlanAddBenefitDate" component="public.sire.models.cfc.order_plan">
												<cfinvokeargument name="inpUserPlanId" value="#userPlansQuery.UserPlanId_bi[threadId]#">
												<cfinvokeargument name="inpMonthlyAddBenefitDate" value="#_monthlyDate#">
												<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
											</cfinvoke>

											<cfinvoke method="ResetNumberOfRecurring" component="public.sire.models.cfc.order_plan">
												<cfinvokeargument name="inpUserPlanId" value="#userPlansQuery.UserPlanId_bi[threadId]#">
												<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
												<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
											</cfinvoke>
										</cfif>

										<!--- RESET CREDIT TO LASTEST USER PLAN --->
										<cfset arrayAppend(newBalanceBillings, [
											userPlansQuery.UserId_int[threadId], 
											userPlansQuery.FirstSMSIncluded_int[threadId], 
										])>

									</cfif>	

									<cfif _recurringType EQ 0>

										<cfif promotion_credit GT 0>
											<!--- ADD PROMOTION CREDIT BALANCE --->
											<cfset arrayAppend(addPromotionCreditBalance, [
												userPlansQuery.UserId_int[threadId],
												promotion_credit
											])>
										</cfif>

										<!--- count promotion code used --->
										<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
											<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
										</cfloop>

										<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
										<cfinvoke method="AddReferralCredits" component="public.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
											<cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
										</cfinvoke>
									</cfif>	
							</cfif>
							<!--- LOG RECURRING --->
					        <cfset arrayAppend(logPaymentWorldpays[3], [
					        	now(),
					        	userPlansQuery.UserPlanId_bi[threadId],
					        	userPlansQuery.UserId_int[threadId],
					        	userPlansQuery.PlanId_int[threadId],
					        	usersQuery.FirstName_vch[userIndex],
					        	usersQuery.EmailAddress_vch[userIndex],
					        	_amount,
					        	SerializeJSON(paymentData),
					        	paymentError,
					        	SerializeJSON(paymentResponse),
								_paymentGateWay,
								userPlansQuery.UserId_int[threadId]
				        	])>
						<cfelse>
							<cfset paymentErrorsPlans[threadId] = 1/>
							<!--- If no customer in Sercurnet --->
							
							<!--- <cfset arrayAppend(oldUserPlans,[userPlansQuery.UserPlanId_bi[threadId], -2])> --->
							<cfset arrayAppend(errorUserPlans,userPlansQuery.UserPlanId_bi[threadId])>

							<!--- LOG RECURRING --->
					        <cfset arrayAppend(logPaymentWorldpays[3], [
					        	now(),
					        	userPlansQuery.UserPlanId_bi[threadId],
					        	userPlansQuery.UserId_int[threadId],
					        	userPlansQuery.PlanId_int[threadId],
					        	usersQuery.FirstName_vch[userIndex],
					        	usersQuery.EmailAddress_vch[userIndex],
					        	_amount,
					        	"Account Not Found",
					        	'1',
					        	"No data in simplebilling.authorize_user",
								_paymentGateWay,
								userPlansQuery.UserId_int[threadId]
					    	])>

						</cfif>

						<!--- IF PAYMENT NOT SUCCESS --->
						<cfif paymentErrorsPlans[threadId] EQ 1 >
								<cfset dataout.RESULT = -1>
								<cfset dataout.MESSAGE = 'Account has not credit card'>
								<cfinvoke method="logPaymentWorldPay" component="public.sire.models.cfc.order_plan">
									<cfinvokeargument name="inplogPaymentWorldpays" value="#logPaymentWorldpays#"/>
									<cfinvokeargument name="inpFcm" value="#_fcm#"/>
									<cfinvokeargument name="inpNow" value="#_now#"/>
								</cfinvoke>
								<cfreturn dataout />
						</cfif>

					<cfelse> <!--- IF USER DO NOT HAVE TO PAID - INSERT FREE PLAN OR USE COUPON (100%) --->

						<cfif _makePayment EQ 0 OR _makePayment EQ 1 >
						    <cfif _userBillingType EQ 1>
					        	<cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>	
					        <cfelse>	
					        	<cfset _endDate = DateAdd("yyyy", 1, userPlansQuery.EndDate_dt[threadId])>	
					        </cfif>
					        
							<cfif _recurringType EQ 2 AND IsDate(userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
					        	<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
					        <cfelse>		
					        	<cfset _monthlyDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])>
					        </cfif>

						    <cfif _recurringType EQ 0>
						    	<cfset arrayAppend(oldUserPlans, userPlansQuery.UserPlanId_bi[threadId])>
						    </cfif>	

						    <!--- downgrade to free --->
					        <cfif isDowngrade[threadId] EQ 1> 

					        	<cfset arrayAppend(newUserPlans,[
						        	1,
						        	userPlansQuery.UserId_int[threadId],
						        	planFree.PlanId_int,
						        	planFree.Amount_dec,
						        	planFree.UserAccountNumber_int,
						        	planFree.Controls_int,
						        	planFree.KeywordsLimitNumber_int,
						        	planFree.FirstSMSIncluded_int,
						        	0,
						        	planFree.CreditsAddAmount_int,
						        	planFree.PriceMsgAfter_dec,
						        	planFree.PriceKeywordAfter_dec,
						        	0,
						        	userPlansQuery.EndDate_dt[threadId],
						        	DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId]),
						        	planFree.MlpsLimitNumber_int,
						        	planFree.ShortUrlsLimitNumber_int,
						        	planFree.MlpsImageCapacityLimit_bi,
						        	0,
						        	0,
						        	0,
						        	0,
						        	0,
						        	1,
						        	DateAdd("m", 1, userPlansQuery.EndDate_dt[threadId])
						        ])>

						        <!--- RESET CREDIT TO LASTEST USER PLAN --->
					        	<cfset arrayAppend(newBalanceBillings, [
						        	userPlansQuery.UserId_int[threadId], 
						        	_downgradePlanCredits, 
				        		])>

						        <!---DEACTIVE USER DATA IF USER'VE ALREADY SELECT KW,MLP,URL --->
			                    <cfinvoke method="DeactiveUserData" component="public.sire.models.cfc.order_plan">
			                        <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
			                        <cfinvokeargument name="inpDowngradeKeyword" value="#_userDowngradeKeyword#">
			                        <cfinvokeargument name="inpDowngradeShortUrl" value="#_userDowngradeShortUrl#">
			                        <cfinvokeargument name="inpDowngradeMLP" value="#_userDowngradeMLP#">
			                    </cfinvoke>
			                    <!--- <cfif isRemoveCouponBenefit[threadId] EQ 1> --->
			                    	<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
							            <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
							            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
							            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
							            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
							            <cfinvokeargument name="inpLimitKeyword" value="#getDowngradePlan.KeywordsLimitNumber_int + userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid#">
							            <cfinvokeargument name="inpLimitShortUrl" value="#getDowngradePlan.ShortUrlsLimitNumber_int#">
							            <cfinvokeargument name="inpLimitMLP" value="#getDowngradePlan.MlpsLimitNumber_int#">
							        </cfinvoke>
						        <!--- </cfif> --->
					        <cfelse> <!--- if is renew -> insert new plan, if add benefit -> only reset credits to current plan credits --->

					        	<cfif _recurringType EQ 0 > <!--- renew plan --->

					        		<cfset arrayAppend(newUserPlans,[
								    	1,
								    	userPlansQuery.UserId_int[threadId],
								    	userPlansQuery.PlanId_int[threadId],
								    	// plansQuery.Amount_dec[planIndex],
								    	(
											_userBillingType EQ 1 ? plansQuery.Amount_dec[planIndex] : plansQuery.YearlyAmount_dec[planIndex]*12
										),
								    	plansQuery.UserAccountNumber_int[planIndex],
								    	plansQuery.Controls_int[planIndex],
								    	userPlansQuery.KeywordsLimitNumber_int[threadId],
								    	userPlansQuery.FirstSMSIncluded_int[threadId],
								    	userPlansQuery.UsedSMSIncluded_int[threadId],
								    	plansQuery.CreditsAddAmount_int[planIndex],
								    	plansQuery.PriceMsgAfter_dec[planIndex],
								    	plansQuery.PriceKeywordAfter_dec[planIndex],
								    	userPlansQuery.BuyKeywordNumber_int[threadId],
								    	userPlansQuery.EndDate_dt[threadId],
								    	_endDate,
								        userPlansQuery.MlpsLimitNumber_int[threadId],
										userPlansQuery.ShortUrlsLimitNumber_int[threadId],
										userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
								    	promotion_origin_id,
							        	promotion_id,
							        	promotion_keyword,
							        	promotion_mlp,
							        	promotion_shorturl,
							        	_userBillingType,
							        	_monthlyDate
							    	])>

							    	<cfif promotion_credit GT 0>
								    	<!--- ADD PROMOTION CREDIT BALANCE --->
								    	<cfset arrayAppend(addPromotionCreditBalance, [
							        	userPlansQuery.UserId_int[threadId],
							        	promotion_credit
						        		])>
				        			</cfif>

				        			<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
										<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
									</cfloop>

									<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
									 <cfif isRemoveCouponBenefit[threadId] EQ 1>
										<!--- RELEASE USER DATA --->
										<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
								            <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
								            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
								            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
								            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
								           <cfinvokeargument name="inpLimitKeyword" value="#userPlansQuery.KeywordsLimitNumber_int[threadId]+userPlansQuery.BuyKeywordNumber_int[threadId] + userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
											<cfinvokeargument name="inpLimitShortUrl" value="#userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
										    <cfinvokeargument name="inpLimitMLP" value="#userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
								        </cfinvoke>
									</cfif>	
								<cfelseif _recurringType EQ 1>	
									<cfinvoke method="UpdateUserPlanAddBenefitDate" component="public.sire.models.cfc.order_plan">
										<cfinvokeargument name="inpUserPlanId" value="#userPlansQuery.UserPlanId_bi[threadId]#">
									    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#_monthlyDate#">
									</cfinvoke>	
					        	</cfif> 

								<!--- RESET CREDIT TO LASTEST USER PLAN --->
					        	<cfset arrayAppend(newBalanceBillings, [
						        	userPlansQuery.UserId_int[threadId], 
						        	userPlansQuery.FirstSMSIncluded_int[threadId], 
				        		])>

					        </cfif>	

							<!--- RESET USER REFERRAL CREDITS --->
						    <cfinvoke method="ResetUserReferral" component="public.sire.models.cfc.referral">
						        <cfinvokeargument name="inpUserId" value="#userPlansQuery.UserId_int[threadId]#">
						    </cfinvoke>
					    </cfif>
					</cfif>
				</cfif>
			<cfset threadIndex++>
		</cfloop>

		<cfif !arrayIsEmpty(newUserPlans)>
			<!---  Other Discount --->
			<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				INSERT INTO simplebilling.userplans (
					Status_int,
					UserId_int,
					PlanId_int,
					Amount_dec,
					UserAccountNumber_int,
					Controls_int,
					KeywordsLimitNumber_int,
					FirstSMSIncluded_int,
					UsedSMSIncluded_int,
					CreditsAddAmount_int,
					PriceMsgAfter_dec,
					PriceKeywordAfter_dec,
					BuyKeywordNumber_int,
					StartDate_dt,
					EndDate_dt,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int,
					MlpsImageCapacityLimit_bi,
					PromotionId_int,
					PromotionLastVersionId_int,
					PromotionKeywordsLimitNumber_int,
					PromotionMlpsLimitNumber_int,
					PromotionShortUrlsLimitNumber_int,
					BillingType_ti,
					MonthlyAddBenefitDate_dt,
					BuyKeywordNumberExpired_int,
					DowngradeDate_dt
				) VALUES
				<cfset var _comma = ''> 
				<cfloop array="#newUserPlans#" index="_plan">
					<cfif  ArrayLen(_plan) LT 26>
						<cfset _plan[26] = 0 >
						<cfset _plan[27] = '' >
					<cfelseif ArrayLen(_plan) LT 27>
						<cfset _plan[27] = '' >
					</cfif>
					#_comma#(
						<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[3]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[4]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[5]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[6]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[7]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[8]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[9]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[10]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[11]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[12]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[13]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[14]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[15]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[16]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[17]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[18]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[19]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[20]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[21]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[22]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[23]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[24]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[25]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[26]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[27]#" null="#NOT len(trim(_plan[27]))#">
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>
		</cfif>

		<cfif !arrayIsEmpty(newBalanceBillings)>
			<cfset var userIdList = ''>
			<cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
				UPDATE simplebilling.billing 
					SET Balance_int = (CASE
						<cfloop array="#newBalanceBillings#" index="_balance">
							<cfset userIdList = listAppend(userIdList,_balance[1])>
							WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#_balance[1]#"> 
								THEN <cfqueryparam cfsqltype="cf_sql_decimal" value="#_balance[2]#">
						</cfloop>
						ELSE 0
					END)
				WHERE UserId_int IN (#userIdList#)
			</cfquery>
		</cfif>

		<cfif !arrayIsEmpty(addPromotionCreditBalance)>
			<cfset var userIdListPromotion = ''>
			<cfquery name="addPromoCreditBalance" datasource="#Session.DBSourceEBM#" result="addResult">
				UPDATE 
					simplebilling.billing
				SET
					PromotionCreditBalance_int = PromotionCreditBalance_int + (CASE
						<cfloop array="#addPromotionCreditBalance#" index="index">
						<cfset userIdListPromotion = listAppend(userIdListPromotion,index[1])>
						WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[1]#"/>
							THEN <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#index[2]#"/>
						</cfloop>
					END)
				WHERE 
					UserId_int IN (#userIdListPromotion#)
			</cfquery>
		</cfif>

		<cfif !arrayIsEmpty(promocodeListToIncreaseUsedTime)>
			<cfloop array="#promocodeListToIncreaseUsedTime#" index="index">
				<!--- Increase coupon code used time --->
                <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                    <cfinvokeargument name="inpCouponId" value="#index[1]#"/>
                    <cfinvokeargument name="inpUsedFor" value="1"/>
                </cfinvoke>
                <!--- End increase coupon code used time --->

                <!--- Insert coupon code used log --->
                <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                    <cfinvokeargument name="inpCouponId" value="#index[2]#"/>
                    <cfinvokeargument name="inpOriginCouponId" value="#index[1]#"/>
                    <cfinvokeargument name="inpUserId" value="#index[3]#"/>
                    <cfinvokeargument name="inpUsedFor" value="Recurring"/>
                </cfinvoke>    
                <!--- End insert log --->

                <cfquery name="updateUsedTimePromoCode" datasource="#Session.DBSourceEBM#" result="updateResult">
                  UPDATE 
                  	simplebilling.userpromotions
                  SET
                    UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
                  WHERE 
                  	UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[3]#">
                  AND 
                  	PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[2]#"> 
                  AND 
                  	UserPromotionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[4]#">
                </cfquery>
			</cfloop>

		</cfif>

		<cfif !arrayIsEmpty(oldUserPlans)>
			<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				UPDATE simplebilling.userplans 
					SET Status_int = (CASE
						<cfset var i = 1>
						<cfloop array="#oldUserPlans#" index="_plan">
							<cfif isArray(_plan)>
								WHEN UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#"> 
									THEN <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">
								<cfset oldUserPlans[i] = _plan[1]>
							</cfif>
							<cfset i++>
						</cfloop>
						WHEN 0 = 1 THEN Status_int ELSE 0
					END) 
				WHERE UserPlanId_bi IN (#arrayToList(oldUserPlans, ',')#)
			</cfquery>
		</cfif>

		<cfif !arrayIsEmpty(errorUserPlans)>
			<cfset logData.ErrorNumber_bi += arrayLen(errorUserPlans)>
		</cfif>

		 <!--- Log payment worldpay response --->
        <cfinvoke method="logPaymentWorldPay" component="public.sire.models.cfc.order_plan">
            <cfinvokeargument name="inplogPaymentWorldpays" value="#logPaymentWorldpays#"/>
            <cfinvokeargument name="inpFcm" value="#_fcm#"/>
            <cfinvokeargument name="inpNow" value="#_now#"/>
        </cfinvoke> 
		<cfset var closeBatchesRequestResult = ''>


		<!--- Add Userlog --->
		<cfquery name="Rechargeloginfor" datasource="#Session.DBSourceREAD#">
			SELECT 
				UserPlanId_bi,
				UserId_int,
				PlanId_int,
				Amount_dec
			FROM 
				simplebilling.userplans
			WHERE 
				Status_int = 1
			AND 
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">
		</cfquery>
		<cfif Rechargeloginfor.RECORDCOUNT GT 0>
			<cfset userlog_vch = "Re-charge recurring account: " & #arguments.UserId# & ", amount: "& Rechargeloginfor.Amount_dec & ", PlanID: " & Rechargeloginfor.PlanId_int & ", UserPlanID: " & Rechargeloginfor.UserPlanId_bi>
		</cfif>

		<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
			<cfinvokeargument name="userId" value="#session.userid#">
			<cfinvokeargument name="moduleName" value="Re-charge recurring.">
			<cfinvokeargument name="operator" value="#userlog_vch#">
		</cfinvoke>

        <cftry>
            <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                <cfhttpparam type="header" name="content-type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                <cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
            </cfhttp>
            <cfcatch>
                <!--- <cfset dataout.RESULT = -1>
                <cfset dataout.MESSAGE = 'Close batches url fail.'>
                <cfreturn dataout /> --->
            </cfcatch>
        </cftry>

		
        <cfset dataout.RESULT = 1>
        <cfset dataout.MESSAGE = 'Re-charge successfully.'>
        <cfreturn dataout />
    </cffunction>
    <!--- End Re-charge --->
	<!--- Update status transaction purchase need Admin verify ---> 
	<cffunction name="UpdateStatusPaymentTransaction" access="remote" hint="Update status Transaction Purchase Verify">
		<cfargument name="inpPaymentID" type="numeric" required="yes" default="0">
		<cfargument name="inpPaymentStatus" type="numeric" required="yes" default="0">
		<cfset var dataout = {} />
		<cfset var UpdateStatusPaymentTransactionQuery = '' />

		<cftry>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

			<cfquery result="UpdateStatusPaymentTransactionQuery" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplebilling.payment_buykeywords
				SET
					PaymentStatus = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentStatus#">,
                    AdminId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">,
                    VerifyDate = Now()
				WHERE
					PaymentID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentID#">
				AND
					PaymentStatus <> 1
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update status successfully" />
			<cfset dataout.ERRMESSAGE = "" />
			<cfcatch>
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<!--- Add to list transaction purchase need Admin verify ---> 
	<cffunction name="AddPurchaseAmountLimited" access="remote" hint="List Transaction Purchase Verify">
		<cfargument name="inpUserId" type="numeric" required="yes" default="0">
		<cfargument name="inpPaymentAmount" type="numeric" required="yes" default="0">
		<cfargument name="inpPaymentInfor" type="any" required="yes" default="">
		<cfargument name="inpPhoneNumber" type="string" required="yes" default="">
		<cfargument name="inpCompanyName" type="string" required="yes" default="">
		<cfargument name="inpUserName" type="string" required="yes" default="">
        <cfargument name="inpPaymentGateway" type="string" required="yes" default="1">

		<cfset var dataout = {} />
		<cfset var data = {}/>
		<cfset var AddPurchaseAmountLimitedList = '' />
		<cfset var retvalsendmail = ''/>

		<cftry>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfquery result="AddPurchaseAmountLimitedList" datasource="#Session.DBSourceEBM#">
				INSERT INTO
				simplebilling.payment_buykeywords
				(
					UserId_int,
					PaymentInfor_vch,
					UpdateDate,
					PaymentStatus,
					PaymentAmount_dbl,
                    PaymentGateway_ti
				)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPaymentInfor#">,
					NOW(),
					0,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpPaymentAmount#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentGateway#">
				)
			</cfquery>
			<!--- Send Email to Admin --->
			<cfinclude template="/public/sire/configs/paths.cfm"/>
			<cfset data.USERID = arguments.inpUserId/>
			<cfset data.AMOUNT = arguments.inpPaymentAmount/>
			<cfset data.PHONENUMBER = arguments.inpPhoneNumber/>
			<cfset data.COMPANYNAME = arguments.inpCompanyName/>
			<cfset data.USERNAME = arguments.inpUserName/>
			<!--- Send email alert #SupportEMailUserName#--->
			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
				<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
				<cfinvokeargument name="type" value="html"/>
				<cfinvokeargument name="subject" value="Customer Transaction Limit Alert"/>
				<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_payment_above_99.cfm"/>
				<cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert successfully" />
			<cfset dataout.ERRMESSAGE = arguments.inpPaymentInfor />
			<cfcatch>
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<!--- Get list transaction purchase need Admin verify ---> 
	<cffunction name="PurchaseAmountLimitedList" access="remote" output="true" hint="Get list Purchase amount limit"><!--- returnformat="JSON"--->
		<cfargument name="UserIdstatus" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->
		
		
		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'DESC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>
	        			
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
		
			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4"> 
					<cfset order = "pkw.UpdateDate" />
					</cfcase> 
					<cfcase value="5"> 
					<cfset order = "pkw.PaymentAmount_dbl" />
					</cfcase>
					<cfdefaultcase> 
						<cfset order = "pkw.UpdateDate"/>   
						<cfset sSortDir_0 = 'DESC'/>	
					</cfdefaultcase> 	
				</cfswitch>
			<cfelse>
				<cfset order = "pkw.UpdateDate"/>
				<cfset sSortDir_0 = 'DESC'/>
			</cfif>	
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS 
					pkw.PaymentID,
					pkw.UpdateDate,
					pkw.PaymentInfor_vch,
					pkw.PaymentStatus,
					pkw.AdminId_int,
					pkw.VerifyDate,
					pkw.PaymentAmount_dbl,
					u.UserId_int,
					concat(u.FirstName_vch,' ',u.LastName_vch) as Full_Name,
					u.EmailAddress_vch,
					u.MFAContactString_vch,
                    pkw.PaymentGateway_ti
				FROM
					simplebilling.payment_buykeywords pkw
				INNER JOIN 
					simpleobjects.useraccount u
					ON pkw.UserId_int = u.UserId_int
				WHERE u.Active_int = 1
				AND u.IsTestAccount_ti = 0
				<cfif UserIdstatus EQ 1>
				    AND u.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				</cfif>
                
                
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>

		    <cfloop query="GetPMQuery">	
               <cfset Status = "Pending">
                <cfif GetPMQuery.PaymentStatus EQ "0">
                	<cfset Status = "Pending">
                <cfelseif GetPMQuery.PaymentStatus EQ "-1">
                	<cfset Status = "Declined">
                <cfelseif GetPMQuery.PaymentStatus EQ "-2">
                	<cfset Status = "Error">
                <cfelseif GetPMQuery.PaymentStatus EQ "1">
                	<cfset Status = "Approved">
                </cfif>
				<cfset var Item = "">
				<cfset var paymentResponseContent = "">

				<cfset paymentResponseContent = DeserializeJSON(GetPMQuery.PaymentInfor_vch)>
				<cfif paymentResponseContent.numberKeywordToSend NEQ 0 AND paymentResponseContent.numberSMSToSend NEQ 0>
					<cfset Item = paymentResponseContent.numberKeywordToSend & " Keywords, " & paymentResponseContent.numberSMSToSend & " Credits">
				<cfelseif paymentResponseContent.numberKeywordToSend EQ 0>
					<cfset Item = paymentResponseContent.numberSMSToSend & " Credits">
				<cfelseif paymentResponseContent.numberSMSToSend EQ 0>
					<cfset Item = paymentResponseContent.numberKeywordToSend & " Keywords">
				</cfif>

				<cfset tempItem = {
					PAYMENTID = '#PaymentID#',
					USERID = '#UserId_int#',
					NAME = "#Full_Name#",
					EMAIL = "#EmailAddress_vch#",
					PHONE = "#MFAContactString_vch#",
					PAYMENT_DATE = "#DateFormat(UpdateDate,'mm/dd/yyyy')#",
					ITEM = "#Item#",
					PAYMENT_AMOUNT = "$#NumberFormat(PaymentAmount_dbl,'0.00')#",
					STATUS = "#Status#",
					ADMINAPPROVE = '#AdminId_int#',
					VERIFYDATE = '#DateFormat(VerifyDate,'mm/dd/yyyy')#',
                    PAYMENTGATEWAY =  "#PaymentGateway_ti#"
				}>		
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<!--- get purchase payment by ID to payment --->
	<cffunction name="GetPaymentTransactionById" access="remote" output="false" hint="get Payment Transaction by ID">
		<cfargument name="inpPaymentID" type="numeric" required="yes" default="0">
		<cfset var dataout = {}>
		<cfset var GetPaymentTransactionByIdQuery = '' />
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.PAYMENTID = "" />
		<cfset dataout.USERID = ''/>
		<cfset dataout.PAYMENTINFOR = "" />
		<cfset dataout.PAYMENTAMOUNT = "" />
		<cfset dataout.PAYMENTGATEWAY = ""/>
		<cftry>
            <cfquery name="GetPaymentTransactionByIdQuery" datasource="#Session.DBSourceEBM#">              
				SELECT
					PaymentID,
					UserId_int,
					PaymentInfor_vch,
					UpdateDate,
					PaymentStatus,
					PaymentAmount_dbl,
					PaymentGateway_ti
				FROM
					simplebilling.payment_buykeywords
				WHERE                
					PaymentID = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentID#">      
            </cfquery>  
            
			<cfif GetPaymentTransactionByIdQuery.RecordCount gt 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.PAYMENTID = "#GetPaymentTransactionByIdQuery.PaymentID#" />
				<cfset dataout.USERID = "#GetPaymentTransactionByIdQuery.UserId_int#" />
				<cfset dataout.PAYMENTINFOR = "#GetPaymentTransactionByIdQuery.PaymentInfor_vch#" />
				<cfset dataout.PAYMENTAMOUNT = "#GetPaymentTransactionByIdQuery.PaymentAmount_dbl#" />
				<cfset dataout.PAYMENTGATEWAY = "#GetPaymentTransactionByIdQuery.PaymentGateway_ti#"/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
			</cfif>
									
			<cfcatch TYPE="any">
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>   
        </cftry>
		<cfreturn dataout />	
	</cffunction>
	<!--- Get infor for purchase payment transaction --->
	<cffunction name="GetUserInforLimitPurchaseAmount" access="remote" output="false" hint="get user infor for limited purchase amount ">
		<cfargument name="inpUserID" type="numeric" required="no" default="0">
		<cfset var dataout = {}>
		<cfset var GetUserInforLimitPurchaseAmountQuery = '' />
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.USERID = "" />
		<cfset dataout.AMOUNTLIMITED = 0 />
		<cfset dataout.PHONENUMBER = ''/>
		<cfset dataout.COMPANYNAME = ''/>
		<cfset dataout.USERNAME = ''/>
		<cftry>
            <cfquery name="GetUserInforLimitPurchaseAmountQuery" datasource="#Session.DBSourceEBM#">              
				SELECT
					us.userId_int,
					us.LimitedPurchaseAmount_int,
					us.MFAContactString_vch,
					org.OrganizationName_vch,
					CONCAT(us.FirstName_vch,' ', us.LastName_vch) AS UserName
				FROM
					simpleobjects.useraccount us
				LEFT JOIN
					simpleobjects.Organization org
					ON	us.userId_int = org.UserId_int
				WHERE                
					us.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					us.Active_int > 0       
            </cfquery>  
            
			<cfif GetUserInforLimitPurchaseAmountQuery.RecordCount gt 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERID = "#GetUserInforLimitPurchaseAmountQuery.userId_int#" />
				<cfset dataout.AMOUNTLIMITED = "#GetUserInforLimitPurchaseAmountQuery.LimitedPurchaseAmount_int#" />
				<cfset dataout.PHONENUMBER = "#GetUserInforLimitPurchaseAmountQuery.MFAContactString_vch#"/>
				<cfset dataout.COMPANYNAME = "#GetUserInforLimitPurchaseAmountQuery.OrganizationName_vch#"/>
				<cfset dataout.USERNAME = "#GetUserInforLimitPurchaseAmountQuery.UserName#"/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
			</cfif>
									
			<cfcatch TYPE="any">
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>   
		
        </cftry>
		<cfreturn dataout />	
	</cffunction>
	<!--- Save CC --->
	<cffunction name="SaveCCAbove99" access="remote" output="false" hint="Save CC before pending above 99">
		<cfargument name="payment_method" default="">
        <cfargument name="line1" default="">
        <cfargument name="plan" default="">
        <cfargument name="city" default="">
        <cfargument name="country" default="">
        <cfargument name="cvv" default="">
        <cfargument name="email" default="">
        <cfargument name="expirationDate" default="">
        <cfargument name="firstName" default="">
        <cfargument name="lastName" default="">
        <cfargument name="number" default="">
        <cfargument name="phone" default="">
        <cfargument name="state" default="">
        <cfargument name="zip" default="">
        <cfargument name="numberSMSToSend" default="0">
        <cfargument name="numberKeywordToSend" default="0">
        <cfargument name="select_used_card" default="0">
        <cfargument name="save_cc_info" default="">
        <cfargument name="h_email_save" default="">
        
		<cfset var dataout = {}>
		<cfset var GetPaymentTransactionByIdQuery = '' />
		<cfset var primaryPaymentMethodId = ''/>
		<cfset var RetCustomerInfo = ''/>
		<cfset var RetUpdateCustomer = ''/>
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 	
		<cfset dataout.MESSAGE = ''/>
		<cfset dataout.ERRMESSAGE = ''/>
		<cftry>
            
            <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">
			</cfinvoke>    
			<cfif RetCustomerInfo.RXRESULTCODE EQ 1>
				<cfset primaryPaymentMethodId = 0>
			<cfelse>    
				<cfset primaryPaymentMethodId = 1>
			</cfif>
			<cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing" returnvariable="RetUpdateCustomer">
				<cfinvokeargument name="payment_method" value="#arguments.payment_method#">
				<cfinvokeargument name="line1" value="#arguments.line1#">
				<cfinvokeargument name="city" value="#arguments.city#">
				<cfinvokeargument name="country" value="#arguments.country#">
				<cfinvokeargument name="phone" value="#arguments.phone#">
				<cfinvokeargument name="state" value="#arguments.state#">
				<cfinvokeargument name="zip" value="#arguments.zip#">
				<cfinvokeargument name="email" value="#arguments.email#">
				<cfinvokeargument name="cvv" value="#arguments.cvv#">
				<cfinvokeargument name="expirationDate" value="#arguments.expirationDate#">
				<cfinvokeargument name="firstName" value="#arguments.firstName#">
				<cfinvokeargument name="lastName" value="#arguments.lastName#">
				<cfinvokeargument name="number" value="#arguments.number#">
				<cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = RetUpdateCustomer.RXRESULTCODE />					
			<cfcatch TYPE="any">
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>   
        </cftry>
		<cfreturn dataout />	
	</cffunction>

	<!--- Admin Charge buy Keywords above 99--->
	<cffunction name="BuyCreditKeywordAbove99" access="remote" hint="Buy credit or keyword above 99">
        <cfargument name="payment_method" default="">
        <cfargument name="line1" default="">
        <cfargument name="plan" default="">
        <cfargument name="city" default="">
        <cfargument name="country" default="">
        <cfargument name="cvv" default="">
        <cfargument name="email" default="">
        <cfargument name="expirationDate" default="">
        <cfargument name="firstName" default="">
        <cfargument name="lastName" default="">
        <cfargument name="number" default="">
        <cfargument name="phone" default="">
        <cfargument name="state" default="">
        <cfargument name="zip" default="">
        <cfargument name="numberSMSToSend" default="0">
        <cfargument name="numberKeywordToSend" default="0">
        <cfargument name="select_used_card" default="0">
        <cfargument name="save_cc_info" default="">
        <cfargument name="h_email_save" default="">
		<cfargument name="h_email" default="">
		<cfargument name="UserId" default="">

        <cfset var dataout = {} />
        <cfset dataout.ID = ''/>
        <cfset dataout.MSG = '' />

        <cfset var email = '' />
        <cfset var amount = '' />
        <cfset var numberOfSms = '' />
        <cfset var numberOfKeyword = '' />
        <cfset var txtTotalAddon = '' />
        <cfset var updateUsers = '' />
        <cfset var checkUserPlan = '' />
        <cfset var userLogOperator = '' />
        <cfset var RetCustomerInfo = '' />
        <cfset var paymentRespose = '' />
        <cfset var customerId = '' />
        <cfset var paymentMethodId = '' />
        <cfset var paymentTypeShortName = '' />
        <cfset var paymentType = '' />
        <cfset var primaryPaymentMethodId = '' />
        <cfset var catchContent  = '' />
        <cfset var BuyKeyword  = '' />
        <cfset var getUserPlan  = '' />
        <cfset var GetUserAuthen  = '' />
        <cfset var paymentData = {} />
        <cfset var closeBatchesData = {} />
        <cfset var paymentResposeContent  = {} />
        <cfset var closeBatchesRequestResult  = {} />

        <cfset email = arguments.email/>
        <cfif arguments.select_used_card EQ 1>
            <cfif arguments.h_email_save EQ ''>
                <cfset email = arguments.h_email/>
            <cfelse>
                <cfset email = arguments.h_email_save/>
            </cfif>
        <cfelse>
            <cfif arguments.email EQ ''>
                <cfset email = arguments.h_email/>
            </cfif>
        </cfif>
        <cfset numberOfSms = arguments.numberSMSToSend />
        <cfset numberOfKeyword = arguments.numberKeywordToSend />

        <cfif (!structKeyExists(arguments,'number'))>
            <cfabort>
        </cfif>

        <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
			<cfinvokeargument name="InpUserId" value="#arguments.UserId#">
        </cfinvoke>
        <cfset amount = (arguments.numberSMSToSend * getUserPlan.PRICEMSGAFTER)/100 + arguments.numberKeywordToSend * getUserPlan.PRICEKEYWORDAFTER>
        <cfif arguments.numberSMSToSend EQ 0 AND arguments.numberKeywordToSend GT 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberKeywordToSend & ' Keyword(s)'>
        <cfelseif arguments.numberSMSToSend GT 0 AND arguments.numberKeywordToSend EQ 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberSMSToSend & ' Credit(s)'>    
        <cfelseif arguments.numberSMSToSend GT 0 AND arguments.numberKeywordToSend GT 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberSMSToSend & ' Credit(s) and ' & arguments.numberKeywordToSend & ' Keyword(s)'>
        </cfif>

        <cftry>
            <cfinclude template="../../configs/credits.cfm">
            <cfif arguments.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
                <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen">
					<cfinvokeargument name="userId" value="#arguments.UserId#">
				</cfinvoke> 
				
                <cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
                    <cfset customerId = #arguments.UserId#>
                    <cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
                    <cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

                    <cftry>
                        <cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
                    <cfcatch>
                        <cfset paymentType = worldpayPaymentTypes.UNKNOWN>
                    </cfcatch>  
                    </cftry>

                <cfelse>
                        <cfset dataout.ID="-4">
                        <cfset dataout.MSG="We're Sorry! Unable to get your CreditCard info.Please update in [My Plan] page.">
                </cfif>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    paymentVaultToken = {  
                       customerId = customerId,
                       paymentMethodId = paymentMethodId,
                       paymentType = paymentType
                    },
                    developerApplication = worldpayApplication
                }>
            <cfelse>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    developerApplication = {
                        developerId: worldpayApplication.developerId,
                        version: worldpayApplication.version
                  }
                }>

                <cfif arguments.payment_method EQ 1>
                    <cfset paymentData.check = {
                            routingNumber   = arguments.number,
                            accountNumber   = arguments.cvv,
                            firstName       = arguments.firstName,
                            lastName        = arguments.lastName,
                            address         = {
                                line1   = arguments.line1,
                                city    = arguments.city,
                                state   = arguments.state,
                                zip     = arguments.zip,
                                country = arguments.country,
                                phone   = arguments.phone
                            },
                            email       = arguments.email
                        }>
                <cfelse>
                    <cfset paymentData.card = {
                            number          = arguments.number,
                            cvv             = arguments.cvv,
                            expirationDate  = arguments.expirationDate,
                            firstName       = arguments.firstName,
                            lastName        = arguments.lastName,
                            address         = {
                                line1   = arguments.line1,
                                city    = arguments.city,
                                state   = arguments.state,
                                zip     = arguments.zip,
                                country = arguments.country,
                                phone   = arguments.phone
                            },
                            email       = arguments.email
                        }>
                </cfif>
            </cfif>

            <cfset closeBatchesData = {
                            developerApplication = {
                                developerId: worldpayApplication.developerId,
                                version: worldpayApplication.version
                          }
                        }>
                

            <cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
                <cfhttpparam type="header" name="content-type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
            </cfhttp>

            <cfset dataout = {} />
            <cfset dataout.ID = "-10">
            <cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">

            
            <cfif paymentRespose.status_code EQ 200>
                <cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>

                    <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
                        
                        <cfif paymentResposeContent.success>

                            <cfset dataout.ID = "1">
                            <cfset dataout.MSG = "Transaction Complete. Thank you!">
                            
                            <!--- close session--->
                            <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                            <cfhttpparam type="header" name="content-type" value="application/json">
                            <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                            <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
                            </cfhttp>
                            <!--- Add Credit --->
                            <cfif arguments.numberSMSToSend GT 0>
                                <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                    UPDATE  
                                        simplebilling.billing
                                    SET     
                                        BuyCreditBalance_int = BuyCreditBalance_int + #arguments.numberSMSToSend#
                                    WHERE   
                                        userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.UserId#">
                                </cfquery>
                            </cfif>

                            <!--- Add Keyword --->
                            <cfif arguments.numberKeywordToSend GT 0>
                                <!--- CHECK USER PLAN EXPRIED --->
                                <cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
                                    <cfinvokeargument name="userId" value="#arguments.UserId#">
                                </cfinvoke>

                                <!--- IF PLAN EXPRIED in 30 days -> Update BuyKeywordNumberExpired_int --->
                                <cfif checkUserPlan.RXRESULTCODE EQ 1>
                                    <!--- Add keyword After Expried --->
                                    <cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
                                        UPDATE  
                                            simplebilling.userplans
                                        SET     
                                            BuyKeywordNumberExpired_int = BuyKeywordNumberExpired_int + #arguments.numberKeywordToSend#
                                        WHERE   
                                            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.UserId#">
                                        ORDER BY
                                            UserPlanId_bi
                                        DESC
                                        LIMIT 1
                                    </cfquery>
                                <cfelse>
                                    <!--- Add keyword --->
                                    <cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
                                        UPDATE  
                                            simplebilling.userplans
                                        SET     
                                            BuyKeywordNumber_int = BuyKeywordNumber_int + #arguments.numberKeywordToSend#
                                        WHERE   
                                            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.UserId#">
                                        ORDER BY
                                            UserPlanId_bi
                                        DESC
                                        LIMIT 1
                                    </cfquery>
                                </cfif>
                            </cfif>

                            <!--- Sendmail payment --->
                            <cftry>
                                <cfset paymentRespose.filecontent = deserializeJSON(paymentRespose.filecontent)/>
                                <cfset paymentRespose.filecontent.numberSMS = numberOfSms />
                                <cfset paymentRespose.filecontent.numberKeyword = numberOfKeyword />
                                <cfset paymentRespose.filecontent = serializeJSON(paymentRespose.filecontent) />

                                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                    <cfinvokeargument name="to" value="#email#">
                                    <cfinvokeargument name="type" value="html">
                                    <cfinvokeargument name="subject" value="[SIRE][Buy Credits - Keywords] Payment Info">
                                    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                    <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
                                </cfinvoke>
                                <cfcatch type="any">
                                    <cfset dataout.ID = cfcatch.TYPE />
                                    <cfset dataout.MSG = cfcatch.MESSAGE />
                                    <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                                </cfcatch>
                            </cftry>
                            <!--- Add payment --->
                            <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                <cfinvokeargument name="planId" value="#getUserPlan.PLANID#">
                                <cfinvokeargument name="numberKeyword" value="#arguments.numberKeywordToSend#">
                                <cfinvokeargument name="numberSMS" value="#arguments.numberSMSToSend#">
                                <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                                <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
                                <cfinvokeargument name="inpUserId" value="#arguments.UserId#">
                            </cfinvoke>

                            <cfif arguments.numberKeywordToSend EQ 0 AND arguments.numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# for $#amount#" />
                            <cfelseif arguments.numberKeywordToSend GT 0 AND arguments.numberSMSToSend EQ 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Keyword Added = #numberKeywordToSend# for $#amount#" />
                            <cfelseif arguments.numberKeywordToSend GT 0 AND arguments.numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# and Keyword Added = #numberKeywordToSend# for $#amount#" />
                            </cfif>
                            <!--- Add Userlog #session.userid#--->
                            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#arguments.UserId#">
                                <cfinvokeargument name="moduleName" value="Payment Processing">
                                <cfinvokeargument name="operator" value="#userLogOperator#">
                            </cfinvoke>

							 <!--- Log activity--->
							<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
								<cfinvokeargument name="userId" value="#session.userid#">
								<cfinvokeargument name="moduleName" value="Verify Payment Processing Above 99">
								<cfinvokeargument name="operator" value="Verify Payment Processing Above 99: Account = #arguments.UserId# Credit added = #numberSMSToSend# and Keyword Added = #numberKeywordToSend# for $#amount#">
							</cfinvoke>
                            <!--- CREATE ACCOUNT ON SERCUNET --->
                            <cfif arguments.select_used_card EQ 0 OR arguments.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
                                <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">
									<cfinvokeargument name="userId" value="#arguments.UserId#">
								</cfinvoke>    
                                <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
                                    <cfset primaryPaymentMethodId = 0>
                                <cfelse>    
                                    <cfset primaryPaymentMethodId = 1>
                                </cfif>
                                <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
                                    <cfinvokeargument name="payment_method" value="#arguments.payment_method#">
                                    <cfinvokeargument name="line1" value="#arguments.line1#">
                                    <cfinvokeargument name="city" value="#arguments.city#">
                                    <cfinvokeargument name="country" value="#arguments.country#">
                                    <cfinvokeargument name="phone" value="#arguments.phone#">
                                    <cfinvokeargument name="state" value="#arguments.state#">
                                    <cfinvokeargument name="zip" value="#arguments.zip#">
                                    <cfinvokeargument name="email" value="#arguments.email#">
                                    <cfinvokeargument name="cvv" value="#arguments.cvv#">
                                    <cfinvokeargument name="expirationDate" value="#arguments.expirationDate#">
                                    <cfinvokeargument name="firstName" value="#arguments.firstName#">
                                    <cfinvokeargument name="lastName" value="#arguments.lastName#">
                                    <cfinvokeargument name="number" value="#arguments.number#">
                                    <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
									<cfinvokeargument name="userId" value="#arguments.UserId#">
                                </cfinvoke>
                            </cfif>
                        </cfif>
                    
                </cfif>
            <cfelse>

                <!--- Log activity default #session.userid#--->
                <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#arguments.UserId#">
                    <cfinvokeargument name="moduleName" value="Payment Processing">
                    <cfinvokeargument name="operator" value="Securenet Execute Transaction Failed!">
                </cfinvoke>
            </cfif>
                
                <!--- Log payment default #session.userid#--->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                        <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                        <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                        <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
                        <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                        <cfinvokeargument name="paymentdata" value="#paymentData#">
						<cfinvokeargument name="inpUserId" value="#arguments.UserId#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>

            <cfcatch type="any">
                <cftry>
                    <!--- Log bug to file /session/sire/logs/bugs/payment  --->
                    <cfif !isDefined('variables._Response')>
                        <cfset variables._PageContext = GetPageContext()>
                        <!--- Coldfusion --->
                        <!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
                        <cfset variables._Response = variables._PageContext.getOut()>
                    </cfif>
                    <cfset variables._Response.clear()>
                    
                    <cfset catchContent = variables._Response.getString()>
                    <cfset variables._Response.clear()>
                    
                    <cffile action="write" output="#catchContent#"
                    file="../../logs/bugs/payment/securenet_payment_error_#REReplace('#now()#', '\W', '_', 'all')#.txt" 
                    >
                     <cfcatch type="any">
                    </cfcatch>

                </cftry>
                    <cfset dataout.ID="-3">
                    <cfset dataout.MSG="We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="BuyCreditKeyword" access="remote" hint="Buy credit or keyword">
        <cfargument name="payment_method" default="">
        <cfargument name="line1" default="">
        <cfargument name="plan" default="">
        <cfargument name="city" default="">
        <cfargument name="country" default="">
        <cfargument name="cvv" default="">
        <cfargument name="email" default="">
        <cfargument name="expirationDate" default="">
        <cfargument name="firstName" default="">
        <cfargument name="lastName" default="">
        <cfargument name="number" default="">
        <cfargument name="phone" default="">
        <cfargument name="state" default="">
        <cfargument name="zip" default="">
        <cfargument name="numberSMSToSend" default="0">
        <cfargument name="numberKeywordToSend" default="0">
        <cfargument name="select_used_card" default="0">
        <cfargument name="save_cc_info" default="">
        <cfargument name="h_email_save" default="">

        <cfset var dataout = {} />
        <cfset dataout.ID = ''/>
        <cfset dataout.MSG = '' />
        <cfset var email = '' />
        <cfset var amount = '' />
        <cfset var numberOfSms = '' />
        <cfset var numberOfKeyword = '' />
        <cfset var txtTotalAddon = '' />
        <cfset var updateUsers = '' />
        <cfset var checkUserPlan = '' />
        <cfset var userLogOperator = '' />
        <cfset var RetCustomerInfo = '' />
        <cfset var paymentRespose = '' />
        <cfset var customerId = '' />
        <cfset var paymentMethodId = '' />
        <cfset var paymentTypeShortName = '' />
        <cfset var paymentType = '' />
        <cfset var primaryPaymentMethodId = '' />
        <cfset var catchContent  = '' />
        <cfset var BuyKeyword  = '' />
        <cfset var getUserPlan  = '' />
        <cfset var GetUserAuthen  = '' />
        <cfset var paymentData = {} />
        <cfset var closeBatchesData = {} />
        <cfset var paymentResposeContent  = {} />
        <cfset var closeBatchesRequestResult  = {} />

        <cfset email = arguments.email/>
        <cfif arguments.select_used_card EQ 1>
            <cfif arguments.h_email_save EQ ''>
                <cfset email = arguments.h_email/>
            <cfelse>
                <cfset email = arguments.h_email_save/>
            </cfif>
        <cfelse>
            <cfif arguments.email EQ ''>
                <cfset email = arguments.h_email/>
            </cfif>
        </cfif>

        <cfset numberOfSms = arguments.numberSMSToSend />
        <cfset numberOfKeyword = arguments.numberKeywordToSend />

        <cfif (!structKeyExists(arguments,'number'))>
            <cfabort>
        </cfif>

        <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
        </cfinvoke>

        <cfset amount = (arguments.numberSMSToSend * getUserPlan.PRICEMSGAFTER)/100 + arguments.numberKeywordToSend * getUserPlan.PRICEKEYWORDAFTER>

        <cfif arguments.numberSMSToSend EQ 0 AND arguments.numberKeywordToSend GT 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberKeywordToSend & ' Keyword(s)'>
        <cfelseif arguments.numberSMSToSend GT 0 AND arguments.numberKeywordToSend EQ 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberSMSToSend & ' Credit(s)'>    
        <cfelseif arguments.numberSMSToSend GT 0 AND arguments.numberKeywordToSend GT 0>
            <cfset txtTotalAddon = "Buy " & arguments.numberSMSToSend & ' Credit(s) and ' & arguments.numberKeywordToSend & ' Keyword(s)'>
        </cfif>


        <cftry>
            
            <cfinclude template="../../configs/credits.cfm">

            <cfif arguments.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
                <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 

                <cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
                    <cfset customerId = #session.USERID#>
                    <cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
                    <cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

                    <cftry>
                        <cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
                    <cfcatch>
                        <cfset paymentType = worldpayPaymentTypes.UNKNOWN>
                    </cfcatch>  
                    </cftry>

                <cfelse>
                        <cfset dataout.ID="-4">
                        <cfset dataout.MSG="We're Sorry! Unable to get your CreditCard info.Please update in [My Plan] page.">
                </cfif>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    paymentVaultToken = {  
                       customerId = customerId,
                       paymentMethodId = paymentMethodId,
                       paymentType = paymentType
                    },
                    developerApplication = worldpayApplication
                }>
            <cfelse>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    developerApplication = {
                        developerId: worldpayApplication.developerId,
                        version: worldpayApplication.version
                  }
                }>

                <cfif arguments.payment_method EQ 1>
                    <cfset paymentData.check = {
                            routingNumber   = arguments.number,
                            accountNumber   = arguments.cvv,
                            firstName       = arguments.firstName,
                            lastName        = arguments.lastName,
                            address         = {
                                line1   = arguments.line1,
                                city    = arguments.city,
                                state   = arguments.state,
                                zip     = arguments.zip,
                                country = arguments.country,
                                phone   = arguments.phone
                            },
                            email       = arguments.email
                        }>
                <cfelse>
                    <cfset paymentData.card = {
                            number          = arguments.number,
                            cvv             = arguments.cvv,
                            expirationDate  = arguments.expirationDate,
                            firstName       = arguments.firstName,
                            lastName        = arguments.lastName,
                            address         = {
                                line1   = arguments.line1,
                                city    = arguments.city,
                                state   = arguments.state,
                                zip     = arguments.zip,
                                country = arguments.country,
                                phone   = arguments.phone
                            },
                            email       = arguments.email
                        }>
                </cfif>
            </cfif>

            <cfset closeBatchesData = {
                            developerApplication = {
                                developerId: worldpayApplication.developerId,
                                version: worldpayApplication.version
                          }
                        }>
                

            <cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
                <cfhttpparam type="header" name="content-type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
            </cfhttp>

            <cfset dataout = {} />
            <cfset dataout.ID = "-10">
            <cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">

            
            <cfif paymentRespose.status_code EQ 200>
                <cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>

                    <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
                        
                        <cfif paymentResposeContent.success>

                            <cfset dataout.ID = "1">
                            <cfset dataout.MSG = "Transaction Complete. Thank you!">
                            
                            <!--- close session--->
                            <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                            <cfhttpparam type="header" name="content-type" value="application/json">
                            <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                            <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
                            </cfhttp>
                            <!--- Add Credit --->
                            <cfif arguments.numberSMSToSend GT 0>
                                <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                    UPDATE  
                                        simplebilling.billing
                                    SET     
                                        BuyCreditBalance_int = BuyCreditBalance_int + #arguments.numberSMSToSend#
                                    WHERE   
                                        userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                                </cfquery>
                            </cfif>

                            <!--- Add Keyword --->
                            <cfif arguments.numberKeywordToSend GT 0>
                                <!--- CHECK USER PLAN EXPRIED --->
                                <cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
                                    <cfinvokeargument name="userId" value="#SESSION.USERID#">
                                </cfinvoke>

                                <!--- IF PLAN EXPRIED in 30 days -> Update BuyKeywordNumberExpired_int --->
                                <cfif checkUserPlan.RXRESULTCODE EQ 1>
                                    <!--- Add keyword After Expried --->
                                    <cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
                                        UPDATE  
                                            simplebilling.userplans
                                        SET     
                                            BuyKeywordNumberExpired_int = BuyKeywordNumberExpired_int + #arguments.numberKeywordToSend#
                                        WHERE   
                                            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                                        ORDER BY
                                            UserPlanId_bi
                                        DESC
                                        LIMIT 1
                                    </cfquery>
                                <cfelse>
                                    <!--- Add keyword --->
                                    <cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
                                        UPDATE  
                                            simplebilling.userplans
                                        SET     
                                            BuyKeywordNumber_int = BuyKeywordNumber_int + #arguments.numberKeywordToSend#
                                        WHERE   
                                            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                                        ORDER BY
                                            UserPlanId_bi
                                        DESC
                                        LIMIT 1
                                    </cfquery>
                                </cfif>
                            </cfif>

                            <!--- Sendmail payment --->
                            <cftry>
                                <cfset paymentRespose.filecontent = deserializeJSON(paymentRespose.filecontent)/>
                                <cfset paymentRespose.filecontent.numberSMS = numberOfSms />
                                <cfset paymentRespose.filecontent.numberKeyword = numberOfKeyword />
                                <cfset paymentRespose.filecontent = serializeJSON(paymentRespose.filecontent) />

                                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                    <cfinvokeargument name="to" value="#email#">
                                    <cfinvokeargument name="type" value="html">
                                    <cfinvokeargument name="subject" value="[SIRE][Buy Credits - Keywords] Payment Info">
                                    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                    <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
                                </cfinvoke>
                                <cfcatch type="any">
                                    <cfset dataout.ID = cfcatch.TYPE />
                                    <cfset dataout.MSG = cfcatch.MESSAGE />
                                    <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                                </cfcatch>
                            </cftry>
                            <!--- Add payment --->
                            <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                <cfinvokeargument name="planId" value="#getUserPlan.PLANID#">
                                <cfinvokeargument name="numberKeyword" value="#arguments.numberKeywordToSend#">
                                <cfinvokeargument name="numberSMS" value="#arguments.numberSMSToSend#">
                                <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                                <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
                            </cfinvoke>

                            <cfif arguments.numberKeywordToSend EQ 0 AND arguments.numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# for $#amount#" />
                            <cfelseif arguments.numberKeywordToSend GT 0 AND arguments.numberSMSToSend EQ 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Keyword Added = #numberKeywordToSend# for $#amount#" />
                            <cfelseif arguments.numberKeywordToSend GT 0 AND arguments.numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# and Keyword Added = #numberKeywordToSend# for $#amount#" />
                            </cfif>
                            <!--- Add Userlog --->
                            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#session.userid#">
                                <cfinvokeargument name="moduleName" value="Payment Processing">
                                <cfinvokeargument name="operator" value="#userLogOperator#">
                            </cfinvoke>

                            
                            <!--- CREATE ACCOUNT ON SERCUNET --->
                            <cfif arguments.select_used_card EQ 0 OR arguments.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
                                <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>    
                                <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
                                    <cfset primaryPaymentMethodId = 0>
                                <cfelse>    
                                    <cfset primaryPaymentMethodId = 1>
                                </cfif>
                                <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
                                    <cfinvokeargument name="payment_method" value="#arguments.payment_method#">
                                    <cfinvokeargument name="line1" value="#arguments.line1#">
                                    <cfinvokeargument name="city" value="#arguments.city#">
                                    <cfinvokeargument name="country" value="#arguments.country#">
                                    <cfinvokeargument name="phone" value="#arguments.phone#">
                                    <cfinvokeargument name="state" value="#arguments.state#">
                                    <cfinvokeargument name="zip" value="#arguments.zip#">
                                    <cfinvokeargument name="email" value="#arguments.email#">
                                    <cfinvokeargument name="cvv" value="#arguments.cvv#">
                                    <cfinvokeargument name="expirationDate" value="#arguments.expirationDate#">
                                    <cfinvokeargument name="firstName" value="#arguments.firstName#">
                                    <cfinvokeargument name="lastName" value="#arguments.lastName#">
                                    <cfinvokeargument name="number" value="#arguments.number#">
                                    <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
                                </cfinvoke>
                            </cfif>
                            
                        </cfif>
                    
                </cfif>
            <cfelse>

                <!--- Log activity --->
                <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Payment Processing">
                    <cfinvokeargument name="operator" value="Securenet Execute Transaction Failed!">
                </cfinvoke>
            </cfif>
                
                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                        <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                        <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                        <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
                        <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                        <cfinvokeargument name="paymentdata" value="#paymentData#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>

            <cfcatch type="any">
                <cftry>
                    <!--- Log bug to file /session/sire/logs/bugs/payment  --->
                    <cfif !isDefined('variables._Response')>
                        <cfset variables._PageContext = GetPageContext()>
                        <!--- Coldfusion --->
                        <!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
                        <cfset variables._Response = variables._PageContext.getOut()>
                    </cfif>
                    <cfset variables._Response.clear()>
                    
                    <cfset catchContent = variables._Response.getString()>
                    <cfset variables._Response.clear()>
                    
                    <cffile action="write" output="#catchContent#"
                    file="../../logs/bugs/payment/securenet_payment_error_#REReplace('#now()#', '\W', '_', 'all')#.txt" 
                    >
                     <cfcatch type="any">
                    </cfcatch>

                </cftry>
                    <cfset dataout.ID="-3">
                    <cfset dataout.MSG="We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetBillingHistoryList" access="remote" output="true" hint="Get Billing history list">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <!--- <cfargument name="sEcho"> --->
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = '' />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout["BILLINGHISTORYLIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />

        <cfset var getList = '' />
        <cfset var getTotal = '' />
        <cfset var amount = '' />
        <cfset var status = '' />
        <cfset var filecontentParseResult = {} />

        <Cfset var myXMLDocument = ''/>
        <Cfset var processorResponseCode = ''/>
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var paymentdataXML = ''/>
        <Cfset var orderAmount = ''/>

        <cftry>
            <cfif arguments.iDisplayStart < 0>
                <cfset arguments.iDisplayStart = 0>
            </cfif>
			
            <cfquery name="getTotal" datasource="#Session.DBSourceREAD#">
                SELECT
                    id
                FROM
                    simplebilling.log_payment_worldpay
                WHERE
                    (
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						OR						
						PaymentByUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					)
						
                AND
                    moduleName = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Recurring">
            </cfquery>

            <cfset dataout["iTotalRecords"] = (getTotal.RECORDCOUNT GTE 1 ? getTotal.RECORDCOUNT : 0) >
            <cfset dataout["iTotalDisplayRecords"] = (getTotal.RECORDCOUNT GTE 1 ? getTotal.RECORDCOUNT : 0) >

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    id,
                    created,
                    filecontent,
                    moduleName,
                    status_text,
                    PaymentGateway_ti,
		    		userId_int,
					paymentdata
                FROM
                    simplebilling.log_payment_worldpay 				
                WHERE
                    (
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						OR						
						PaymentByUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					)
                AND
                    moduleName = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Recurring">
                ORDER BY created DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
            </cfquery>
			
			
            <cfif getList.RECORDCOUNT GT 0>
                <cfloop query="getList">
                        
                        <cfif PaymentGateway_ti EQ 1>
							<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />						
                            <cfif getList.status_text NEQ 'OK'>
                                <cfset amount = 0/>
                                <cfset status = getList.status_text/>
                            <cfelse>
                                <cfset amount = filecontentParseResult.transaction.authorizedAmount/>
                                <cfset status = filecontentParseResult.result/>
                            </cfif>
                        <cfelseif PaymentGateway_ti EQ 2><!--- Paypal --->
							<cfset status = getList.status_text/>
							<cfif structKeyExists(deserializeJSON(getList.paymentdata), "MONEY")>								
								<cfset amount= deserializeJSON(getList.paymentdata).MONEY>						
							<cfelse>
								<cfset amount = 0/>
							</cfif>
						<cfelseif PaymentGateway_ti EQ 3>										
							<cfset myXMLDocument = XmlParse(getList.filecontent)> 
							<cfset processorResponseCode = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
							<cfset status = getList.status_text/>
							<cfset amount = 0/>
							<cfif ArrayLen(processorResponseCode) GT 0>			
								<cfset processorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
								<cfif ArrayLen(processorResponse) GT 0>			
									<cfset status = processorResponse[1].XmlText/>      
								</cfif>	
							<cfelse>								
								<cfset statusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
								<cfif ArrayLen(statusMessage) GT 0>			
									<cfset status = statusMessage[1].XmlText/>      
								</cfif>
							</cfif>	
							<cfset paymentdataXML = XmlParse(deserializeJSON(getList.paymentdata))> 
							<cfset orderAmount = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/orderAmount")>
							<cfif ArrayLen(orderAmount) GT 0>			
								<cfset amount = orderAmount[1].XmlText/>      
							</cfif>
						<cfelseif PaymentGateway_ti EQ 4>					
							<cfset status = getList.status_text/>
							<cfif structKeyExists(deserializeJSON(getList.paymentdata), "INPAMOUNT")>								
								<cfset amount= deserializeJSON(getList.paymentdata).INPAMOUNT>						
							<cfelse>
								<cfset amount = 0/>
							</cfif>
                        </cfif>
						<cfset amount = numberFormat(replace(amount,"$","","all"),'.99')>
                        <cfset tempItem = 
                            {
                                ID = '#getList.id#',
                                DATE = '#DateFormat(getList.created, 'mm/dd/yyyy')#',
                                AMOUNT = '#amount#',
                                PRODUCT = '#getList.moduleName#',
                                STATUS = '#status#',
								BILLINGUSERID='#userId_int#'
                            }
                        >
                        <cfset ArrayAppend(dataout["BILLINGHISTORYLIST"],tempItem)>
                </cfloop>
                
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get billing history list successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetBillingHistoryById" access="remote" hint="Get Billing history record by id">
        <cfargument name="inpId" required="true" />

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = '' />
        <cfset dataout.MESSAGE = '' />
        <cfset var getRecord = '' />
        <cfset var filecontentParseResult = {} />

        <cfset var xmlParse = '' />
        <cfset var selectedElements = '' />
        <cfset var processorResponse = '' />
        <cfset var statusMessage = '' />
        <cfset var xmlParsePaymentData = '' />
        <cfset var orderAmount = '' />

        <cftry>
            <cfquery name="getRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                    created,
                    filecontent,
                    moduleName,
                    status_text,
					paymentdata,
					PaymentGateway_ti
                FROM
                    simplebilling.log_payment_worldpay
                WHERE
                    (
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						OR
						PaymentByUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					)

                AND
                    id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#">
            </cfquery>

            <cfif getRecord.RECORDCOUNT GT 0>
				
				<cfif getRecord.PaymentGateway_ti EQ 3>
					
					<cfset dataout.CREATED = DateFormat(getRecord.created, 'mm/dd/yyyy')/>
					<cfset dataout.AMOUNT = 0 />
					<cfset dataout.PRODUCT = getRecord.moduleName />
					<cfset dataout.STATUS = "" />
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = 'Get record successfully!' />
										
					
					<cfset xmlParse =  XmlParse(getRecord.fileContent)> 										
					<cfset selectedElements = XmlSearch(xmlParse, "/UAPIResponse/processorResponseCode")> 
					<cfif arrayLen(selectedElements) GT 0>
						<cfset processorResponse = XmlSearch(xmlParse, "/UAPIResponse/processorResponse")>
						<cfif arrayLen(processorResponse) GT 0>									
							<cfset dataout.STATUS = processorResponse[1].XmlText />
						</cfif>   
					<cfelse>
						<cfset statusMessage = XmlSearch(xmlParse, "/UAPIResponse/statusMessage")>
						<cfif arrayLen(statusMessage) GT 0>									
							<cfset dataout.STATUS = statusMessage[1].XmlText />
						</cfif> 						
					</cfif>
									
					<cfset xmlParsePaymentData = XmlParse(deserializeJSON(getRecord.paymentdata))> 
					<cfset orderAmount = XmlSearch(xmlParsePaymentData, "/UAPIRequest/requestData/orderAmount")>
					<cfif arrayLen(orderAmount) GT 0>
						<cfset dataout.AMOUNT = orderAmount[1].XmlText />    
					</cfif>	
				<cfelse>				
					<cfset filecontentParseResult = deserializeJSON(getRecord.filecontent) />
					<cfset dataout.CREATED = DateFormat(getRecord.created, 'mm/dd/yyyy')/>
					<cfset dataout.AMOUNT = filecontentParseResult.transaction.authorizedAmount />
					<cfset dataout.PRODUCT = getRecord.moduleName />
					<cfset dataout.STATUS = filecontentParseResult.result />
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = 'Get record successfully!' />
				</cfif>
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'Get record failed!' />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTransactionList" access="remote" output="true" hint="Get transaction list">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <!--- <cfargument name="sEcho"> --->
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = '' />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout["TRANSACTIONLIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />

        <cfset var getList = '' />
        <cfset var getTotal = '' />
        <cfset var amount = '' />
		<cfset var comment_vch = ''/>
        <cfset var filecontentParseResult = {} />

        <cfset var xmlParse = '' />
        <cfset var selectedElements = '' />
        <cfset var processorResponse = '' />
        <cfset var statusMessage = '' />
        <cfset var xmlParsePaymentData = '' />
        <cfset var orderAmount = '' />

        <cftry>
            <cfif arguments.iDisplayStart < 0>
                <cfset arguments.iDisplayStart = 0>
            </cfif>

            <cfquery name="getTotal" datasource="#Session.DBSourceREAD#">
                SELECT
                    id
                FROM
                    simplebilling.log_payment_worldpay
                WHERE
					(
                    	userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						OR						
						PaymentByUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					)
				AND
					moduleName <> 'Update Vault'
				AND
					moduleName <> 'Create authorization transaction'
				AND
					moduleName <> 'Store Creditcard in vault'
				AND
					moduleName <> 'Create Upload Card transaction'
				AND
					moduleName <> 'Customer Vault Add'
            </cfquery>

            <cfset dataout["iTotalRecords"] = (getTotal.RECORDCOUNT GTE 1 ? getTotal.RECORDCOUNT : 0) >
            <cfset dataout["iTotalDisplayRecords"] = (getTotal.RECORDCOUNT GTE 1 ? getTotal.RECORDCOUNT : 0) >

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    id,
                    created,
                    filecontent,
                    moduleName,
                    status_text,
					PaymentGateway_ti,
                    paymentdata
                FROM
                    simplebilling.log_payment_worldpay
                WHERE                    
					(
                    	userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						OR						
						PaymentByUserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					)
				AND
					moduleName <> 'Update Vault'
				AND
					moduleName <> 'Create authorization transaction'
				AND
					moduleName <> 'Store Creditcard in vault'
				AND
					moduleName <> 'Create Upload Card transaction'
				AND
					moduleName <> 'Customer Vault Add'
                ORDER BY created DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
            </cfquery>
			
            <cfif getList.RECORDCOUNT GT 0>
                <cfloop query="getList">
                    

					<cfif PaymentGateway_ti EQ 1><!--- Worldpay --->
                        <cfset filecontentParseResult = deserializeJSON(getList.filecontent) />
						<cfif getList.status_text NEQ 'OK'>
							<cfset amount = 0/>
						<cfelseif filecontentParseResult.success EQ 'true'>
							<cfset amount = filecontentParseResult.transaction.authorizedAmount/>
						<cfelse>
							<cfset amount = 0/>
						</cfif>
						<cfset comment_vch = filecontentParseResult.message/>

                    <cfelseif PaymentGateway_ti EQ 3>
                        <cfset amount = 0/>
						<cfset comment_vch = "" />
                        <cfif isXML(getList.fileContent)>
                        <cfset xmlParse =  XmlParse(getList.fileContent)>                                                
                            <cfset selectedElements = XmlSearch(xmlParse, "/UAPIResponse/processorResponseCode")> 
                            <cfif arrayLen(selectedElements) GT 0>
                                <cfset processorResponse = XmlSearch(xmlParse, "/UAPIResponse/processorResponse")>
								<cfif arrayLen(processorResponse) GT 0>									
									<cfset comment_vch = processorResponse[1].XmlText />
								</cfif>   
							<cfelse>
								<cfset statusMessage = XmlSearch(xmlParse, "/UAPIResponse/statusMessage")>
								<cfif arrayLen(statusMessage) GT 0>									
									<cfset comment_vch = statusMessage[1].XmlText />
								</cfif> 
								
                            </cfif>
                        </cfif>						
						<cfset xmlParsePaymentData = XmlParse(deserializeJSON(paymentdata))> 
						<cfset orderAmount = XmlSearch(xmlParsePaymentData, "/UAPIRequest/requestData/orderAmount")>
						<cfif arrayLen(orderAmount) GT 0>
							<cfset amount = orderAmount[1].XmlText />    
						</cfif>	
                        
					<cfelseif PaymentGateway_ti EQ 2><!--- Paypal --->
                        <cfset filecontentParseResult = deserializeJSON(getList.filecontent) />						
						<cfif structKeyExists(filecontentParseResult, "AMT")>
							<cfset amount=filecontentParseResult.AMT>	
						<cfelseif structKeyExists(deserializeJSON(paymentdata), "transactions")>			
							<cfset amount=deserializeJSON(paymentdata).transactions[1].amount.total>								
						<cfelse>								
							<cfset amount=0>						
						</cfif>
						
						<cfset comment_vch=getList.status_text>		
					<cfelseif PaymentGateway_ti EQ 4><!---  --->                       						
						
						<cfset amount=  deserializeJSON(paymentdata).INPAMOUNT >													
						<cfset comment_vch=getList.status_text>							
					<cfelse>

					</cfif>
					<cfset amount= numberFormat(replace(amount, "$", "","all"),'.99')>
                    <cfset tempItem = 
                        {
                            ID = '#getList.id#',
                            DATE = '#DateFormat(getList.created, 'mm/dd/yyyy')#',
                            AMOUNT = '#amount#',
                            DUE = '#DateFormat(getList.created, 'mm/dd/yyyy')# ' & '#TimeFormat(getList.created, 'hh:mm:sstt')#',
                            COMMENT = '#comment_vch#'
                        }
                    >
                    
                    <cfset ArrayAppend(dataout["TRANSACTIONLIST"],tempItem)>
                    
                </cfloop>
                
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get transaction list successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

	<!-------------------------------------------------------
    *************************** PAYPAL  **********************
    --------------------------------------------------------->
	<cffunction name="SetPPExpressCheckout" access="remote" hint="Paypal set express checkout">
		<cfargument name="DoCreateBillingAgreement" required="false" default="0"/>
		<cfargument name="inpAmount" required="true" default="0"/>
		<cfargument name="inpPaymentDescription" required="true" default=""/>
        <cfargument name="inpCancelUrl" required="true" default=""/>
        <cfargument name="inpReturnlUrl" required="true" default=""/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.RESULT = "" />
		<cfset var PayPalPaymentRequestResult = ''/>
		<cfset var statuscheckout = 0/>
		<cfset var token = ""/>

		<cftry>
			<cfhttp url="#_PaypalNVPUrl#" method="post" result="PayPalPaymentRequestResult">
			    <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
			    <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
			    <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
			    <cfhttpparam type="formfield" name="METHOD" value="SetExpressCheckout" >
			    <cfhttpparam type="formfield" name="VERSION" value="#_PaypalVersion#" >
			    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_AMT" value="#arguments.inpAmount#" >
			    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_CURRENCYCODE" value="#_PaypalCurrencyCode#" >
			    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_PAYMENTACTION" value="#_PaypalPaymentAction#" >
			    <cfhttpparam type="formfield" name="L_BILLINGTYPE0" value="MerchantInitiatedBillingSingleAgreement" >
			    <!--- <cfhttpparam type="formfield" name="L_BILLINGAGREEMENTDESCRIPTION0" value="#arguments.inpPaymentDescription#" > --->
                <cfhttpparam type="formfield" name="L_BILLINGAGREEMENTDESCRIPTION0" value="" >
                <cfhttpparam type="formfield" name="cancelUrl" value="#inpCancelUrl#" >
                <cfhttpparam type="formfield" name="returnUrl" value="#inpReturnlUrl#" >
			</cfhttp>

            <cfif PayPalPaymentRequestResult.status_code EQ 200>
                <cfif PayPalPaymentRequestResult.FileContent NEQ ''>
                    <cfset statuscheckout = Find("ACK=Success",PayPalPaymentRequestResult.FileContent)>
                    <cfif statuscheckout neq 0>
                        <cfset token = Replace(ListGetAt(PayPalPaymentRequestResult.FileContent,1,"&"),"TOKEN=","","ALL")>
                        <cfset dataout.RXRESULTCODE = 1/>
                        <cfset dataout.RESULT = "#token#"/>
                        <cfset dataout.MESSAGE = ""/>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = 0/>
                        <cfset dataout.MESSAGE = "#PayPalPaymentRequestResult.FileContent#" />
                    </cfif>
                <cfelse>
                    <cfset dataout.MESSAGE = "Call API Failed"/>    
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = "Call API Failed"/>        
            </cfif>
                
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetExpressCheckoutDetails" access="remote" hint="Paypal get info payment">
		<cfargument name="inptoken" required="true" default=""/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.RESULT = "" />
        <cfset dataout.TOKEN = ''/>
        <cfset dataout.PAYERID = ''/>
        <cfset dataout.PAYMENTREQUEST_0_AMT = ''/>

		<cfset var GetExpressCheckoutDetailsResult = ''/>
		<cfset var statuscheckout = 0/>
		<cfset var queryStruct =""/>
		<cftry>
			<cfhttp url="#_PaypalNVPUrl#" method="post" result="GetExpressCheckoutDetailsResult">
			    <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
			    <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
			    <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
			    <cfhttpparam type="formfield" name="METHOD" value="GetExpressCheckoutDetails" >
			    <cfhttpparam type="formfield" name="VERSION" value="#_PaypalVersion#" >
			    <cfhttpparam type="formfield" name="TOKEN" value="#arguments.inptoken#" >
			</cfhttp>

            <cfif GetExpressCheckoutDetailsResult.status_code EQ 200>
                <cfif GetExpressCheckoutDetailsResult.FileContent NEQ ''>
        			<cfset statuscheckout = Find("CHECKOUTSTATUS=PaymentActionNotInitiated",GetExpressCheckoutDetailsResult.FileContent)>
                    
        			<cfif statuscheckout neq 0>

                        <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                            <cfinvokeargument name="QueryString" value="#GetExpressCheckoutDetailsResult.FileContent#">
                        </cfinvoke> 
                        <cfif structKeyExists(queryStruct, "TOKEN")>
            				<cfset dataout.RXRESULTCODE = 1/>
            				<cfset dataout.RESULT = "#GetExpressCheckoutDetailsResult.FileContent#"/>
                            <cfset dataout.TOKEN = queryStruct['TOKEN']/>
                            <cfset dataout.PAYERID = queryStruct['PAYERID']/>
                            <cfset dataout.PAYMENTREQUEST_0_AMT = queryStruct['PAYMENTREQUEST_0_AMT']/>
            				<cfset dataout.MESSAGE = ""/>
                        </cfif>    
        			<cfelse>
        				<cfset dataout.RXRESULTCODE = 0/>
        				<cfset dataout.RESULT = statuscheckout/>
        				<cfset dataout.MESSAGE = "#GetExpressCheckoutDetailsResult.FileContent#" />
        			</cfif>

                <cfelse>
                    <cfset dataout.MESSAGE = "Call API Failed"/>    
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = "Call API Failed"/>        
            </cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

    <cffunction name="DoExpressCheckoutPayment" access="remote" hint="Paypal set express checkout">
        <cfargument name="inpToken" required="true" default=""/>
        <cfargument name="inpPayerId" required="true" default=""/>
        <cfargument name="inpAMT" required="true"  default="0"/>
        <cfargument name="inpModuleName" required="true"  default=""/>
		<cfargument name="numberKeywordToSend" required="no"  default="0"/>
		<cfargument name="numberSMSToSend" required="no"  default="0"/>
		<cfargument name="inpUserId" required="no"  default="0"/>
		<cfargument name="inpPlanId" required="no"  default="0"/>

        <cfif arguments.inpUserId EQ 0>
            <cfset arguments.inpUserId = SESSION.USERID/>
        </cfif>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.ERRCODE = "" />
        <cfset dataout.RESULT = "" />
        <cfset dataout.TRANSACTIONID = "" />
        <cfset dataout.AMOUNT = "" />
        <cfset dataout.BILLINGAGREEMENTID = "" />
		<cfset var userPlanId = ""/>
        <cfset var queryStruct = {}/>

        <cfset var DoExpressCheckoutPaymentResult = {}/>
		<cfset var statuscheckout = ''/>
		<cfset var getUserPlan = ''/>
		<cfset var RevalupdateLogPaymentWorlPay = ''/>
		<cfset var RevalupdatePaymentWorlPay = ''/>

        <cfif arguments.inpToken NEQ '' AND arguments.inpPayerId NEQ '' AND arguments.inpAMT NEQ ''>
            <cftry>
                <cfhttp url="#_PaypalNVPUrl#" method="post" result="DoExpressCheckoutPaymentResult">
                    <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
                    <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
                    <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
                    <cfhttpparam type="formfield" name="METHOD" value="DoExpressCheckoutPayment" >
                    <cfhttpparam type="formfield" name="VERSION" value="#_PaypalVersion#" >
                    <cfhttpparam type="formfield" name="TOKEN" value="#arguments.inpToken#" >
                    <cfhttpparam type="formfield" name="PAYERID" value="#arguments.inpPayerId#" >
                    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_AMT" value="#arguments.inpAMT#" >
                    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_CURRENCYCODE" value="#_PaypalCurrencyCode#" >
                    <cfhttpparam type="formfield" name="PAYMENTREQUEST_0_PAYMENTACTION" value="#_PaypalPaymentAction#" >
                </cfhttp>

                <cfset var paymentdata= {}>
                <cfset paymentdata = {
                    USER: _PaypalUserId,
                    PWD: _PaypalPassword,
                    SIGNATURE:_PaypalSignature,
                    METHOD:"DoExpressCheckoutPayment",
                    VERSION:"_PaypalVersion",
                    TOKEN:arguments.inpToken,
                    PAYERID:arguments.inpPayerId,
                    PAYMENTREQUEST_0_AMT:arguments.inpAMT,
                    PAYMENTREQUEST_0_CURRENCYCODE:_PaypalCurrencyCode,
                    PAYMENTREQUEST_0_PAYMENTACTION:_PaypalPaymentAction
                }>
				<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
					<cfinvokeargument name="QueryString" value="#URLDecode(DoExpressCheckoutPaymentResult.FileContent)#">
				</cfinvoke> 
				
				<cfif arguments.inpPlanId eq 0>
					<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
                    <cfinvokeargument name="InpUserId" value="#arguments.inpUserId#">
        			</cfinvoke>
					<cfset userPlanId = getUserPlan.PLANID>
				<cfelse>
					<cfset userPlanId = arguments.inpPlanId>
				</cfif>
                <!--- Add log payment --->
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdateLogPaymentWorlPay">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
		            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="status_code" value="#DoExpressCheckoutPaymentResult.status_code#">
                    <cfinvokeargument name="status_text" value="#DoExpressCheckoutPaymentResult.status_text#">
                    <cfinvokeargument name="errordetail" value="#DoExpressCheckoutPaymentResult.errordetail#">
                    <cfinvokeargument name="filecontent" value="#SerializeJSON(queryStruct)#">
                    <cfinvokeargument name="paymentdata" value="#paymentData#">
                    <cfinvokeargument name="paymentmethod" value="2">
                </cfinvoke>

                <cfif DoExpressCheckoutPaymentResult.status_code EQ 200>
                    <cfif DoExpressCheckoutPaymentResult.filecontent NEQ ''>
                        <cfset statuscheckout = Find("PAYMENTINFO_0_PAYMENTSTATUS=Completed",DoExpressCheckoutPaymentResult.FileContent)>
                        <cfif statuscheckout neq 0> 
							<!--- Add payment --->
							<cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
								<cfinvokeargument name="planId" value="#userPlanId#">
								<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
								<cfinvokeargument name="numberKeyword" value="#arguments.numberKeywordToSend#">
								<cfinvokeargument name="numberSMS" value="#arguments.numberSMSToSend#">
								<cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
								<cfinvokeargument name="inppaymentmethod" value="2">
								<cfinvokeargument name="paymentRespose" value="#SerializeJSON(queryStruct)#">
							</cfinvoke>

                            <cfset dataout.RESULT = "#DoExpressCheckoutPaymentResult.FileContent#" />

                            <cfif !structKeyExists(queryStruct, "L_ERRORCODE0")>
                                <cfset dataout.RXRESULTCODE = 1 />
                                <cfset dataout.TRANSACTIONID = URLDecode(queryStruct['PAYMENTINFO_0_TRANSACTIONID']) />
                                <cfset dataout.BILLINGAGREEMENTID = URLDecode(queryStruct['BILLINGAGREEMENTID']) />
                                <cfset dataout.AMOUNT = URLDecode(queryStruct['PAYMENTINFO_0_AMT']) />    
                             <cfelse>
                                <cfset dataout.MESSAGE = "Payment Failed"/>        
                                <cfset dataout.RXRESULTCODE = -5 />
                                <cfset dataout.ERRCODE = URLDecode(queryStruct['L_ERRORCODE0']) />
                                <cfset dataout.ERRMESSAGE = URLDecode(queryStruct['L_SHORTMESSAGE0']) />
                            </cfif>
                            
                            
                        <cfelse>
                            <cfset dataout.MESSAGE = "Payment Failed"/>        
                            <cfset dataout.RXRESULTCODE = -2 />
                        </cfif>    
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -3 />
                        <cfset dataout.MESSAGE = "Call API Failed"/>    
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -4 />
                    <cfset dataout.MESSAGE = "Call API Failed"/>        
                </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>

            </cftry>    

        </cfif>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="DoReferenceTransaction" access="public" hint="manual run payment for user (recurring)">
        <cfargument name="inpReferenceId" required="yes"/>
        <cfargument name="inpAMT" required="true"  default="0"/>
        <cfargument name="inpPlanId" required="true"  default="0"/>
        <cfargument name="inpModuleName" required="true"  default="0"/>
        <cfargument name="inpUserId" required="true"  default="0"/>
		<cfargument name="inpNumberSMS" required="false"  default="0"/>
        <cfargument name="inpNumberKeyword" required="false"  default="0"/>

        <cfset var DoReferenceTransactionResult = {}/>
		<cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TRANSACTIONID = ""/>
        <cfset dataout.TRANSACTIONTYPE = ""/>
        <cfset dataout.PAYMENTTYPE = ""/>
        <cfset dataout.RESULT = ""/>
		<cfset var paymentdata = {}/>
		<cfset var statuscheckout = ''/>
		<cfset var queryStruct = []/>
		<cfset var RevalupdateLogPaymentWorlPay = ''/>
		<cfset var RevalupdatePaymentWorlPay = ''/>

        <cftry>
            
            <cfset paymentdata = {
                USER: _PaypalUserId,
                PWD: _PaypalPassword,
                SIGNATURE:_PaypalSignature,
                METHOD:"DoExpressCheckoutPayment",
                VERSION:"_PaypalVersion",
                AMT:arguments.inpAMT,
                CURRENCYCODE:_PaypalCurrencyCode,
                PAYMENTACTION:_PaypalPaymentAction,
                REFERENCEID:arguments.inpReferenceId
            }>

            <cfhttp url="#_PaypalNVPUrl#" method="post" result="DoReferenceTransactionResult">
                <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
                <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
                <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
                <cfhttpparam type="formfield" name="METHOD" value="DoReferenceTransaction" >
                <cfhttpparam type="formfield" name="VERSION" value="#_PaypalVersion#" >
                <cfhttpparam type="formfield" name="AMT" value="#arguments.inpAMT#" >
                <cfhttpparam type="formfield" name="CURRENCYCODE" value="#_PaypalCurrencyCode#" >
                <cfhttpparam type="formfield" name="PAYMENTACTION" value="#_PaypalPaymentAction#" >
                <cfhttpparam type="formfield" name="REFERENCEID" value="#arguments.inpReferenceId#" >
            </cfhttp>

            <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                <cfinvokeargument name="QueryString" value="#URLDecode(DoReferenceTransactionResult.FileContent)#">
            </cfinvoke>

            <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdateLogPaymentWorlPay">
                <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                <cfinvokeargument name="status_code" value="#DoReferenceTransactionResult.status_code#">
                <cfinvokeargument name="status_text" value="#DoReferenceTransactionResult.status_text#">
                <cfinvokeargument name="errordetail" value="#DoReferenceTransactionResult.errordetail#">
                <cfinvokeargument name="filecontent" value="#SerializeJSON(queryStruct)#">
                <cfinvokeargument name="paymentdata" value="#paymentData#">
                <cfinvokeargument name="paymentmethod" value="2">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
            </cfinvoke>

            <cfif DoReferenceTransactionResult.status_code EQ 200>
                <cfif DoReferenceTransactionResult.filecontent NEQ ''>
                    <cfset dataout.RESULT = "#DoReferenceTransactionResult.FileContent#" />
                    <cfset statuscheckout = Find("PAYMENTSTATUS=Completed",DoReferenceTransactionResult.FileContent)>
                    <cfif statuscheckout neq 0> 
                        <!--- Add payment --->
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                            <cfinvokeargument name="planId" value="#arguments.inpPlanId#">
                            <cfinvokeargument name="numberKeyword" value="#arguments.inpNumberKeyword#">
                            <cfinvokeargument name="numberSMS" value="#arguments.inpNumberSMS#">
                            <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                            <cfinvokeargument name="inppaymentmethod" value="2">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(queryStruct)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>
						<cfset dataout.ERRMESSAGE = "#RevalupdatePaymentWorlPay#" />
                        <cfset dataout.TRANSACTIONID = URLDecode(queryStruct['TRANSACTIONID']) /> 
                        <cfset dataout.TRANSACTIONTYPE = URLDecode(queryStruct['TRANSACTIONTYPE'])/>
                        <cfset dataout.PAYMENTTYPE = URLDecode(queryStruct['PAYMENTTYPE'])/>
                        <cfset dataout.RXRESULTCODE = 1 />
                    </cfif> 
                 <cfelse>
                    <cfset dataout.RXRESULTCODE = -3 />
                    <cfset dataout.MESSAGE = "Payment error" />
                </cfif>
            <cfelse>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "Status error (#DoReferenceTransactionResult.status_code#)" />
            </cfif>    
            
        <cfcatch>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>    

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="DoCreateBillingAgreement" access="remote" hint="Create a billing agreement">
        <cfargument name="inptoken" required="true" default=""/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.RESULT = "" />

        <cfset var CreateBillingAgreementResult = ''/>
        <cfset var statuscheckout = 0/>
		<cfset var queryStruct = []/>
        <cftry>
            <cfhttp url="#_PaypalNVPUrl#" method="post" result="CreateBillingAgreementResult">
                <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
                <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
                <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
                <cfhttpparam type="formfield" name="METHOD" value="CreateBillingAgreement" >
                <cfhttpparam type="formfield" name="VERSION" value="#_PaypalVersion#" >
                <cfhttpparam type="formfield" name="TOKEN" value="#arguments.inptoken#" >
            </cfhttp>

            <cfif CreateBillingAgreementResult.status_code EQ 200>
                <cfif CreateBillingAgreementResult.FileContent NEQ ''>
                    <cfset statuscheckout = Find("ACK=Success",CreateBillingAgreementResult.FileContent)>
                    
                    <cfif statuscheckout neq 0>
                        <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                            <cfinvokeargument name="QueryString" value="#CreateBillingAgreementResult.FileContent#">
                        </cfinvoke> 
                        <cfif structKeyExists(queryStruct, "BILLINGAGREEMENTID")>
                            <cfset dataout.RXRESULTCODE = 1/>
                            <cfset dataout.RESULT = "#queryStruct.BILLINGAGREEMENTID#"/>
                            <cfset dataout.MESSAGE = ""/>
                        </cfif>    
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = 0/>
                        <cfset dataout.RESULT = statuscheckout/>
                        <cfset dataout.MESSAGE = "#CreateBillingAgreementResult.FileContent#" />
                    </cfif>

                <cfelse>
                    <cfset dataout.MESSAGE = "Call API Failed"/>    
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = "Call API Failed"/>        
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        
        <cfreturn dataout />
    </cffunction>

	<cffunction name="DoAddPurchasedCreditKeyword" access="remote" hint="Add credit-keyword after payment success">
		<cfargument name="inpNumberSMS" required="true"  default="0"/>
		<cfargument name="inpNumberKeyword" required="true"  default="0"/>
        <cfargument name="inpUserId" required="no"  default="#SESSION.USERID#"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.RESULT = "" />
		<cfset var updateUsers = ''/>
		<cfset var BuyKeyword = ''/>
		<cfset var checkUserPlan = ''/>
		
		<cftry>
            <cfif arguments.inpUserId GT 0>
    			<!--- Add Credit --->
    			<cfif arguments.inpNumberSMS GT 0>
    				<cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
    					UPDATE  
    						simplebilling.billing
    					SET     
    						BuyCreditBalance_int = BuyCreditBalance_int + #arguments.inpNumberSMS#
    					WHERE   
    						userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
    				</cfquery>
    			</cfif>
    			<!--- Add Keyword --->
    			<cfif arguments.inpNumberKeyword GT 0>
    				<!--- CHECK USER PLAN EXPRIED --->
    				<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
    					<cfinvokeargument name="userId" value="#arguments.inpUserId#">
    				</cfinvoke>
    				<!--- IF PLAN EXPRIED in 30 days -> Update BuyKeywordNumberExpired_int --->
    				<cfif checkUserPlan.RXRESULTCODE EQ 1>
    					<!--- Add keyword After Expried --->
    					<cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
    						UPDATE  
    							simplebilling.userplans
    						SET     
    							BuyKeywordNumberExpired_int = BuyKeywordNumberExpired_int + #arguments.inpNumberKeyword#
    						WHERE   
    							userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
    						ORDER BY
    							UserPlanId_bi
    						DESC
    						LIMIT 1
    					</cfquery>
    				<cfelse>
    					<!--- Add keyword --->
    					<cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
    						UPDATE  
    							simplebilling.userplans
    						SET     
    							BuyKeywordNumber_int = BuyKeywordNumber_int + #arguments.inpNumberKeyword#
    						WHERE   
    							userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
    						ORDER BY
    							UserPlanId_bi
    						DESC
    						LIMIT 1
    					</cfquery>
    				</cfif>
    			</cfif>

                <cfset dataout.RXRESULTCODE = 1 />

            </cfif>    
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- Save Setting payment method --->
	<cffunction name="SavePaymentMethodSetting" access="remote" hint="Save payment method.">
		<cfargument name="inppaymentmethod" type="string" default="" >
		<cfset var dataout	= {} />
		<cfset var rsInsert	= {} />
		<cfset var rsDelete = {} />
        <cfset var updateAllUserAuthen = {}/>
        <cfset var updateUserAuthen = {}/>
		<cfset var rsGetCurrent= {}>
		<cfset var updateUserAccount={}>
		<cfset var currentDefault=0>
		
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<!--- Check record exsit then delete --->

        <cftransaction>
		<cftry>
			<cfquery name="rsGetCurrent" datasource="#Session.DBSourceEBM#">
				SELECT Value_txt
				FROM simpleobjects.sire_setting
				WHERE VariableName_txt = 'PaymentMethodSetting'
			</cfquery>
			<cfset currentDefault=rsGetCurrent.Value_txt>
            <cfif arguments.inppaymentmethod GTE 0>

                <cftransaction action="begin">								
    			<cfquery result="rsDelete" datasource="#Session.DBSourceEBM#">
    				DELETE FROM simpleobjects.sire_setting
    				WHERE VariableName_txt = 'PaymentMethodSetting'
    			</cfquery>
    			<cfquery result="rsInsert" datasource="#Session.DBSourceEBM#">
    				INSERT INTO 
    					simpleobjects.sire_setting
    					(
    						VariableName_txt, 
    						Value_txt
    					)
    				VALUES 
    					(
    					'PaymentMethodSetting',
    					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inppaymentmethod#">
    					)
    			</cfquery>

                <cfif arguments.inppaymentmethod EQ 0>

                     <cfquery name="updateUserAuthen" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplebilling.authorize_user
                        SET 
                            status_ti = 1 
                        WHERE
                            status_ti = 0   
                    </cfquery>

                <cfelse>
					<!--- HungNV:remove it because right now we have multi paypal --->
                    <!--- <cfquery name="updateAllUserAuthen" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplebilling.authorize_user
                        SET 
                            status_ti = 0 
                        WHERE
                            status_ti = 1   
                    </cfquery> --->

                     <cfquery name="updateUserAuthen" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplebilling.authorize_user
                        SET 
                            status_ti = 1 
                        WHERE
                            paymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inppaymentmethod#">
                        AND 
                            status_ti = 0   
                    </cfquery>      
					<cfquery name="updateUserAccount" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.useraccount
                        SET 
                            PaymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inppaymentmethod#">
                        WHERE
                            PaymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#currentDefault#">
                    </cfquery>         
                </cfif>
                <cftransaction action="commit">
    			<cfset dataout.RXRESULTCODE = 1>
    			<cfset dataout.MESSAGE = "Save Payment Method Successfully." />
            </cfif>
			<cfcatch>
                <cftransaction action="rollback">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
        </cftransaction>    
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetPaymentMethodSetting" access="remote" hint="Get payment method">
		<cfset var dataout	= {} />
		<cfset var rsgetsettdata	= {} />
		
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.RESULT = ""/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		
		
		<cftry>
			<cfquery name="rsgetsettdata" datasource="#Session.DBSourceREAD#">
				Select VariableName_txt, Value_txt
				FROM simpleobjects.sire_setting
				WHERE VariableName_txt = 'PaymentMethodSetting'
			</cfquery>	
			
			<cfif rsgetsettdata.RECORDCOUNT GT 0>
				<cfset dataout.RESULT = rsgetsettdata.Value_txt>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get Payment Method Successfully." />
			<cfelse>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Get Payment Method Fail." />
			</cfif>
			<cfcatch>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			
		</cftry>		
		<cfreturn dataout>
	</cffunction>
	<cffunction name="GetPaymentMethodSettingByUserId" access="remote" hint="Get payment method setting by userid">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#" >
		<cfset var dataout	= {} />
		<cfset var rsgetsettdata	= {} />
		
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.RESULT = ""/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		
		
		<cftry>
			<cfquery name="rsgetsettdata" datasource="#Session.DBSourceREAD#">
				Select PaymentGateway_ti
				FROM simpleobjects.useraccount
				WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUserId#">
			</cfquery>	
			
			<cfif rsgetsettdata.RECORDCOUNT GT 0>
				<cfset dataout.RESULT = rsgetsettdata.PaymentGateway_ti>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get Payment Method Successfully." />
			<cfelse>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Get Payment Method Fail." />
			</cfif>
			<cfcatch>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			
		</cftry>		
		<cfreturn dataout>
	</cffunction>
    <!--- Get list admin management user cc ---> 
    <cffunction name="ManagementUserCCList" access="remote" output="true" hint="Management list user CC"><!--- returnformat="JSON"--->
        <cfargument name="UserIdstatus" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        
        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetPMQuery = ''>
        <cfset var sSortDir_0 = 'DESC'/>
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var data = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetTemplate  = '' />
        <cfset var Status = ''/>
        <cfset var rsCount = '0'/>
		<cfset var GetListUserHide = ''/>
        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        
            <!--- Order by --->
            <cfif iSortCol_0 GTE 0>
                <cfswitch expression="#Trim(iSortCol_0)#">
                    <cfcase value="4"> 
                    <cfset order = "pkw.Updated_dt" />
                    </cfcase> 
                    <cfcase value="5"> 
                    <cfset order = "pkw.PaymentMethodID_vch" />
                    </cfcase>
                    <cfdefaultcase> 
                        <cfset order = "pkw.Updated_dt"/>   
                        <cfset sSortDir_0 = 'DESC'/>    
                    </cfdefaultcase>    
                </cfswitch>
            <cfelse>
                <cfset order = "pkw.UserId_int"/>
                <cfset sSortDir_0 = 'DESC'/>
            </cfif> 
			
			
            <cfquery name="GetPMQuery" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS 
                    pkw.Id,
                    pkw.PaymentMethodID_vch,
                    pkw.PaymentType_vch,
                    pkw.UserId_int,
                    pkw.paymentGateway_ti,
                    pkw.status_ti,
                    pkw.Updated_dt,
                    pkw.extraNote,                    
                    concat(u.FirstName_vch,' ',u.LastName_vch) as Full_Name,
                    u.EmailAddress_vch,
					u.PaymentGateway_ti DefaultPaymentGW

                FROM
                    simplebilling.authorize_user pkw
                INNER JOIN 
                    simpleobjects.useraccount u
                    ON pkw.UserId_int = u.UserId_int
                WHERE u.Active_int = 1
				<!---AND (
					(pkw.status_ti = 1 AND u.PaymentGateway_ti = pkw.paymentGateway_ti)
					OR
					(pkw.status_ti = 0 AND u.PaymentGateway_ti <> pkw.paymentGateway_ti)
				)--->

                AND u.IsTestAccount_ti = 0
                <cfif UserIdstatus EQ 1>
                AND u.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                </cfif>
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>                          
                            <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
				<!---AND
					pkw.Id NOT IN(
						SELECT 	Id
						FROM	simplebilling.authorize_user
						WHERE	UserId_int IN(
							SELECT	UserId_int
							FROM	simplebilling.authorize_user
							WHERE	paymentGateway_ti=3
						)
						AND		paymentGateway_ti <> 3
					) --->
                ORDER BY 
                        #order# #sSortDir_0#
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetPMQuery"> 
               <cfset Status = "WorldPay">
                <cfif GetPMQuery.paymentGateway_ti EQ "1">
                    <cfset Status = "WorldPay">
                <cfelseif GetPMQuery.paymentGateway_ti EQ "2">
                    <cfset Status = "PayPal">
                <cfelseif GetPMQuery.paymentGateway_ti EQ "3">
                    <cfset Status = "Mojo">                
				<cfelseif GetPMQuery.paymentGateway_ti EQ "4">
                    <cfset Status = "Total Apps">
                </cfif>                
                <cfset tempItem = {
                    ID = '#Id#',
                    PAYMENTMETHODID = '#PaymentMethodID_vch#',
                    PAYMENTTYPEVC = '#PaymentType_vch#',
                    USERID = '#UserId_int#',
                    PAYMENTGATEWAYTI = '#paymentGateway_ti#',   
					DEFAULT_GW= DefaultPaymentGW ,
                    NAME = "#Full_Name#",
                    EMAIL = "#EmailAddress_vch#",                    
                    EXTRANOTE = "#extraNote#",                    
                    STATUS = "#Status#",                    
                    UPDATEDDATE = '#DateFormat(Updated_dt,'mm/dd/yyyy')#'
                }>      
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	<cffunction name="DoAdminPurchasedCreditKeywordWithSaveCC" access="remote" output="true" hint="core buy keyword,credits">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
	    <cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc or new credits">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user">

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
        <cfset var RxMakePayment = {} />
        <cfset var DoAddPurchasedCreditKeywordResult = {} />
        <cfset var RevalupdatePaymentWorlPay = {}/>
        <cfset var RxStoreCreditCardInVault = {}/>
		<cfset var rsSaveLog = {}/>

        <cfset var paymentdata = ''/>
        <cfset var email = ''/>
		<cfset var displayFieldName = ''/>
		<cfset var newValue = ""/>
		<cfset var oldValue = ""/>
		<cfset var fieldName =""/>

        <cfset var moduleName = "Admin Buy Credits and Keywords"/>

        <cfset var EBMAdminEMSList = ''/>
        <cfset var emailData = ''/>
        <cfset var getUserAccountLimit = {}/>
        <cfset var GetEMSAdmins = {}/>

        <cftry>
            <cfif arguments.inpOrderAmount GT 0>
             
                <cfif arguments.inpSelectUsedCard EQ 1>    <!--- MAKE PAYMENT WITH SAVED CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="#moduleName#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>
                <cfelse> <!--- MAKE PAYMENT WITH NEW CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="#moduleName#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>    
                </cfif>

                <cfif RxMakePayment.RXRESULTCODE EQ 1> <!--- IF MAKE PAYMENT SUCCESS THEN ADD CC,KW TO USER --->

                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>
					<cfif paymentdata.numberKeywordToSend EQ 0>
						<cfset fieldName = 'BuyCreditBalance_int'/>    
						<cfset displayFieldName = 'Buy Credits'/>
					<cfelse>
						<cfset fieldName = 'BuyKeywordNumber_int'/>
						<cfset displayFieldName = 'Buy Keywords'/>
					</cfif>

					<cfif paymentdata.numberKeywordToSend EQ 0>
						<cfquery name="getUserAccountLimit" datasource="#Session.DBSourceEBM#">
							SELECT 
								#fieldName#
							FROM 
								simplebilling.billing
							WHERE 
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
							LIMIT 1
						</cfquery>
					<cfelse>
						<cfquery name="getUserAccountLimit" datasource="#Session.DBSourceEBM#">
							SELECT
								#fieldName#
							FROM
								simplebilling.userplans
							WHERE
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
							AND
								Status_int = 1
							ORDER BY 
								UserPlanId_bi DESC
							LIMIT 1
						</cfquery>
					</cfif>

					<cfset oldValue = getUserAccountLimit["#fieldName#"]>
					<cfif paymentdata.numberKeywordToSend EQ 0>
						<cfset newValue = paymentdata.numberSMSToSend>
					<cfelse>
						<cfset newValue = paymentdata.numberKeywordToSend>
					</cfif>
					<cfset newValue = oldValue + newValue/>

                    <cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
                        <cfinvokeargument name="inpNumberSMS" value="#paymentdata.numberSMSToSend#">
                        <cfinvokeargument name="inpNumberKeyword" value="#paymentdata.numberKeywordToSend#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1>
                        <cfset dataout.RESULT = "Success"/>
                        <cfset dataout.MESSAGE = "You have successfully purchased!"/>

                      

                        <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePayment, "REPORT")>
                            <cfset logData =  RxMakePayment['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                            <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#">
                            <cfinvokeargument name="moduleName" value="#moduleName#">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            <cfinvokeargument name="inpAdminUserId" value="#SESSION.USERID#">
                        </cfinvoke>

                        <cfif arguments.inpSelectUsedCard EQ 2 AND inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            </cfinvoke>                             
                        </cfif>


                        <!--- SEND MAIL TO CUSTOMER --->
                        <cfinvoke component="session.sire.models.cfc.users" method="saveUpdateAccountLog" returnvariable="rsSaveLog">
							<cfinvokeargument name="inpUserID" value="#arguments.inpUserId#">
							<cfinvokeargument name="inpOldValue" value="#oldValue#">
							<cfinvokeargument name="inpNewValue" value="#newValue#">
							<cfinvokeargument name="inpFieldName" value="#displayFieldName#">
						</cfinvoke>

						<cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
							SELECT
								ContactAddress_vch                              
							FROM 
								simpleobjects.systemalertscontacts
							WHERE
								ContactType_int = 2
							AND
								EMSNotice_int = 1    
						</cfquery>

						<cfif GetEMSAdmins.RecordCount GT 0>
							<cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                   
						<cfelse>
							<cfset EBMAdminEMSList = SupportEMailFrom>
						</cfif>                
						
						<cfif paymentdata.h_email neq "">
						
							 <cfif arguments.inpSelectUsedCard EQ 1>
								<cfif paymentdata.h_email_save EQ ''>
									<cfset email = paymentdata.h_email/>
								<cfelse>
									<cfset email = paymentdata.h_email_save/>
								</cfif>
							<cfelse>
								<cfif paymentdata.inpEmail EQ ''>
									<cfset email = paymentdata.h_email/>
								<cfelse>
									<cfset email = paymentdata.inpEmail/>
								</cfif>
							</cfif>

							

							<cfset RxMakePayment.REPORT.EMAIL = email/>
							<cfset RxMakePayment.REPORT.AMT = arguments.inpOrderAmount/>
							<cfset RxMakePayment.REPORT.numberSMS = paymentdata.numberSMSToSend />
							<cfset RxMakePayment.REPORT.numberKeyword = paymentdata.numberKeywordToSend/>
							
							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								<cfinvokeargument name="to" value="#email#">
								<cfinvokeargument name="type" value="html">
								<cfinvokeargument name="subject" value="[SIRE][Admin Buy Credits - Keywords] Payment Info">
								<cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
								<cfinvokeargument name="data" value="#SerializeJSON(RxMakePayment)#">
							</cfinvoke>
						
							
						</cfif>               
						
						<!--- Log to userlog --->
						<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
							<cfinvokeargument name="moduleName" value="Admin User Modified Monthly Limits">
							<cfinvokeargument name="operator" value="Changed account limits for #arguments.inpUserId# by #Session.UserId#">
						</cfinvoke>

                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = "#DoAddPurchasedCreditKeywordResult.MESSAGE#" />
                        <cfset dataout.RESULT = "Payment failed"/>        
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = "#RxMakePayment.MESSAGE#" />
                    <cfset dataout.RESULT = "Payment failed"/>    
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

	<cffunction name="DoAdminPurchasedBuyPlanWithSaveCC" access="remote" output="true" hint="core buy plan">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
		<cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc (1) or new credits (2)">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user (1)">

        <cfset var dataout  = {} />
        <cfset var rsgetsettdata    = {} />
        
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
		<cfset var RxMakePaymentWithMojo = ""/>
		<cfset var DoUpgradePlanResult = ""/>
		<cfset var RevalupdatePaymentWorlPay = ""/>
		<cfset var doUpgradePlanResult = ""/>

        <cfset var paymentdata = ''/>
        <cfset var emailData = ''/>
		<cfset var email = ''/>
        <cfset var RxStoreCreditCardInVault = ''/>
        <cfset var moduleName = "Admin Buy Plan" />
		<cfset var displayFieldName = 'User Plan'/>
		<cfset var newValue = ""/>
		<cfset var oldValue = ""/>
        <cfset var getUserAccountLimit = ''/>
        <cfset var rsSaveLog = ''/>

        <cftry>
            <cfif arguments.inpOrderAmount GT 0>
				<cfif inpSelectUsedCard EQ 1>
					<!--- MAKE PAYMENT Current CC --->
					<cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePaymentWithMojo">
						<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
						<cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="#moduleName#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
					</cfinvoke>
				<cfelse>
					  <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePaymentWithMojo">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="#moduleName#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>    
				</cfif>				
                <cfif RxMakePaymentWithMojo.RXRESULTCODE EQ 1> <!--- MAKE PAYMENT SUCCESS THEN BUY PLAN TO USER --->
					<cfquery name="getUserAccountLimit" datasource="#Session.DBSourceEBM#">
						SELECT
							PlanId_int
						FROM
							simplebilling.userplans
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
						AND
							Status_int = 1
						ORDER BY 
							UserPlanId_bi DESC
						LIMIT 1
					</cfquery>

					<cfset oldValue = getUserAccountLimit["PlanId_int"]>
                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>
					<cfset newValue = paymentdata.planId/>
					

					<cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="doUpgradePlanResult">
						<cfinvokeargument name="inpPlanId" value="#paymentdata.planId#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
						<cfinvokeargument name="inpPromotionCode" value="#paymentdata.inpPromotionCode#">
						<cfinvokeargument name="inpMonthYearSwitch" value="#paymentdata.monthYearSwitch#">
						<cfinvokeargument name="inpListKeyword" value="#paymentdata.listKeyword#">
						<cfinvokeargument name="inpListMLP" value="#paymentdata.listMLP#">
						<cfinvokeargument name="inpListShortUrl" value="#paymentdata.listShortUrl#">
						<cfinvokeargument name="inpPaymentGateWay" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpAdminDo" value="1"/>
						<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
					</cfinvoke>

					<cfif doUpgradePlanResult.RXRESULTCODE EQ 1>
					
						 <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePaymentWithMojo, "REPORT")>
                            <cfset logData =  RxMakePaymentWithMojo['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                           <!---  <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#"> --->
                            <cfinvokeargument name="moduleName" value="#moduleName#">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            <cfinvokeargument name="inpAdminUserId" value="#SESSION.USERID#">
                        </cfinvoke>

                        <cfif arguments.inpSelectUsedCard EQ 2 AND inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            </cfinvoke> 
                        </cfif>
						
						<!--- SEND MAIL TO CUSTOMER --->
						<cfif paymentdata.h_email neq "">
							 <cfif arguments.inpSelectUsedCard EQ 1>
								<cfif paymentdata.h_email_save EQ ''>
									<cfset email = paymentdata.h_email/>
								<cfelse>
									<cfset email = paymentdata.h_email_save/>
								</cfif>
							<cfelse>
								<cfif paymentdata.inpEmail EQ ''>
									<cfset email = paymentdata.h_email/>
								<cfelse>
									<cfset email = paymentdata.inpEmail/>
								</cfif>
							</cfif>

							<cfset emailData ={}/>
							<cfset emailData['FullName'] = RxMakePaymentWithMojo.REPORT.INPFIRSTNAME & " " & RxMakePaymentWithMojo.REPORT.INPLASTNAME/>
							<cfset emailData['numberKeywords'] = 0/>
							<cfset emailData['numberSMS'] = 0/>
							<cfset emailData['authorizedAmount'] = arguments.inpOrderAmount/>
							<cfset emailData['transactionId'] = RxMakePaymentWithMojo.REPORT.TRANSACTIONID/>
							<cfset emailData['planName'] = doUpgradePlanResult['PLANNAME']/>
							<!--- Sendmail payment --->

							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							<cfinvokeargument name="to" value="#email#">
							<cfinvokeargument name="type" value="html">
							<cfinvokeargument name="subject" value="[SIRE][Admin Order Plan] Payment Info">
							<cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
							<cfinvokeargument name="data" value="#serializeJSON(emailData)#">
							</cfinvoke>
						</cfif>   

						
						

                        <cfinvoke component="session.sire.models.cfc.users" method="saveUpdateAccountLog" returnvariable="rsSaveLog">
							<cfinvokeargument name="inpUserID" value="#arguments.inpUserId#">
							<cfinvokeargument name="inpOldValue" value="#oldValue#">
							<cfinvokeargument name="inpNewValue" value="#newValue#">
							<cfinvokeargument name="inpFieldName" value="#displayFieldName#">
						</cfinvoke>

						<cfset dataout.RXRESULTCODE = 1/>	
						<cfset dataout.MESSAGE = 'Payment transaction successfully'/>
					<cfelse>
						<cfset dataout.RXRESULTCODE = -3/>	
						<cfset dataout.MESSAGE = 'Payment successfully but can not add credits/keywords.'/>
					</cfif>
				<cfelse>
					<cfset dataout.MESSAGE =  RxMakePaymentWithMojo.MESSAGE/>
					<cfset dataout.ERRMESSAGE = RxMakePaymentWithMojo.MESSAGE />
				</cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="DoPurchasedCreditKeywordWithSaveCC" access="remote" output="true" hint="core buy keyword,credits">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
	    <cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc or new credits">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user">

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
        <cfset var RxMakePayment = {} />
        <cfset var DoAddPurchasedCreditKeywordResult = {} />
        <cfset var RevalupdatePaymentWorlPay = {}/>
        <cfset var RxStoreCreditCardInVault = {}/>

        <cfset var paymentdata = ''/>
        <cfset var email = ''/>

        <cftry>
            <cfif arguments.inpOrderAmount GT 0>
             
                <cfif arguments.inpSelectUsedCard EQ 1>    <!--- MAKE PAYMENT WITH SAVED CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords">
                    </cfinvoke>
                <cfelse> <!--- MAKE PAYMENT WITH NEW CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords">
                    </cfinvoke>    
                </cfif>

                <cfif RxMakePayment.RXRESULTCODE EQ 1> <!--- IF MAKE PAYMENT SUCCESS THEN ADD CC,KW TO USER --->
                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>
                    <cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
                        <cfinvokeargument name="inpNumberSMS" value="#paymentdata.numberSMSToSend#">
                        <cfinvokeargument name="inpNumberKeyword" value="#paymentdata.numberKeywordToSend#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1>
                        <cfset dataout.RESULT = "Success"/>
                        <cfset dataout.MESSAGE = "You have successfully purchased!"/>

                      

                        <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePayment, "REPORT")>
                            <cfset logData =  RxMakePayment['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                            <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#">
                            <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>

                        <cfif arguments.inpSelectUsedCard EQ 2 AND inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            </cfinvoke> 
                        </cfif>

                        <!--- SEND MAIL TO CUSTOMER --->
                        <!--- Sendmail payment --->
						<!--- Sendmail payment --->
                       
                        <cfif arguments.inpSelectUsedCard EQ 1>
                            <cfif paymentdata.h_email_save EQ ''>
                                <cfset email = paymentdata.h_email/>
                            <cfelse>
                                <cfset email = paymentdata.h_email_save/>
                            </cfif>
                        <cfelse>
                            <cfif paymentdata.inpEmail EQ ''>
                                <cfset email = paymentdata.h_email/>
                            <cfelse>
                                <cfset email = paymentdata.inpEmail/>
                            </cfif>
                        </cfif>
                        <cfset RxMakePayment.REPORT.EMAIL = email/>
                        <cfset RxMakePayment.REPORT.AMT = arguments.inpOrderAmount/>
                        <cfset RxMakePayment.REPORT.numberSMS = paymentdata.numberSMSToSend />
                        <cfset RxMakePayment.REPORT.numberKeyword = paymentdata.numberKeywordToSend/>
                        <cftry>
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#email#">
                                <cfinvokeargument name="type" value="html">
                                <cfinvokeargument name="subject" value="[SIRE][Buy Credits - Keywords] Payment Info">
                                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                <cfinvokeargument name="data" value="#SerializeJSON(RxMakePayment)#">
                            </cfinvoke>
                            <cfcatch type="any">
                            </cfcatch>
                        </cftry>

                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = "#DoAddPurchasedCreditKeywordResult.MESSAGE#" />
                        <cfset dataout.RESULT = "Payment failed"/>        
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = "#RxMakePayment.MESSAGE#" />
                    <cfset dataout.RESULT = "Payment failed"/>    
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

	<cffunction name="DoPurchasedBuyPlanWithSaveCC" access="remote" output="true" hint="core buy plan">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
		<cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc (1) or new credits (2)">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user (1)">

        <cfset var dataout  = {} />
        <cfset var rsgetsettdata    = {} />
        
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
		<cfset var RxMakePaymentWithMojo = ""/>
		<cfset var DoUpgradePlanResult = ""/>
		<cfset var RevalupdatePaymentWorlPay = ""/>
		<cfset var doUpgradePlanResult = ""/>

        <cfset var paymentdata = ''/>
        <cfset var emailData = ''/>
        <cfset var RxStoreCreditCardInVault = ''/>
		<cfset var planinfoquery = ''/>
		<cfset var planinfobyuserquery = ''/>

        <cftry>
			<cfset paymentdata =  DeserializeJSON(inpPaymentdata)>

			<cfquery name="planinfoquery" datasource="#Session.DBSourceEBM#">
				SELECT 
					PlanName_vch , PlanId_int, Order_int
				FROM 
					simplebilling.plans
				Where 
					PlanId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#paymentdata.planId#">
				Order by Order_int
			</cfquery>

			<cfquery name="planinfobyuserquery" datasource="#Session.DBSourceEBM#">
				SELECT 
					PlanName_vch , PlanId_int, Order_int
				FROM 
					simplebilling.plans
				WHERE 
					PlanId_int  = (SELECT PlanId_int FROM simplebilling.userplans WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#"> AND Status_int = 1)
				Order by Order_int
			</cfquery>

			<cfif planinfoquery.RecordCount GT 0 AND planinfobyuserquery.RecordCount GT 0>
				<cfif planinfobyuserquery.Order_int GT planinfoquery.Order_int>
					<cfset dataout.RXRESULTCODE = -1>
					<cfset dataout.RESULT = ""/>
					<cfset dataout.MESSAGE = "This action is rejected due to user plan is changed to Pro/SMB before." />
					<cfset dataout.ERRMESSAGE = "This action is rejected due to user plan is changed to Pro/SMB before." />
					<cfreturn dataout>
				</cfif>
			</cfif>

            <cfif arguments.inpOrderAmount GT 0>
				<cfif inpSelectUsedCard EQ 1>
					<!--- MAKE PAYMENT Current CC --->
					<cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePaymentWithMojo">
						<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
						<cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="Buy Plan">
					</cfinvoke>
				<cfelse>
					<!--- payment new CC --->
					  <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePaymentWithMojo">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="Buy Plan">
                    </cfinvoke>    
				</cfif>

                <cfif RxMakePaymentWithMojo.RXRESULTCODE EQ 1> <!--- MAKE PAYMENT SUCCESS THEN BUY PLAN TO USER --->
					<cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="doUpgradePlanResult">
						<cfinvokeargument name="inpPlanId" value="#paymentdata.planId#">
						<cfinvokeargument name="inpPromotionCode" value="#paymentdata.inpPromotionCode#">
						<cfinvokeargument name="inpMonthYearSwitch" value="#paymentdata.monthYearSwitch#">
						<cfinvokeargument name="inpListKeyword" value="#paymentdata.listKeyword#">
						<cfinvokeargument name="inpListMLP" value="#paymentdata.listMLP#">
						<cfinvokeargument name="inpListShortUrl" value="#paymentdata.listShortUrl#">
						<cfinvokeargument name="inpPaymentGateWay" value="#arguments.inpPaymentGateway#">
					</cfinvoke>
					<cfif doUpgradePlanResult.RXRESULTCODE EQ 1>
						<cfif SESSION.EMAILADDRESS NEQ ''>
							<cfset emailData ={}/>
							<cfset emailData['FullName'] = SESSION.FULLNAME/>
							<cfset emailData['numberKeywords'] = 0/>
							<cfset emailData['numberSMS'] = 0/>
							<cfset emailData['authorizedAmount'] = arguments.inpOrderAmount/>
							<cfset emailData['transactionId'] = RxMakePaymentWithMojo.REPORT.TRANSACTIONID/>
							<cfset emailData['planName'] = doUpgradePlanResult['PLANNAME']/>
							<!--- Sendmail payment --->

							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							<cfinvokeargument name="to" value="#SESSION.EMAILADDRESS#">
							<cfinvokeargument name="type" value="html">
							<cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
							<cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
							<cfinvokeargument name="data" value="#serializeJSON(emailData)#">
							</cfinvoke>
						</cfif>

						 <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePaymentWithMojo, "REPORT")>
                            <cfset logData =  RxMakePaymentWithMojo['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                           <!---  <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#"> --->
                            <cfinvokeargument name="moduleName" value="Buy Plan">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>

                        <cfif arguments.inpSelectUsedCard EQ 2 AND inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            </cfinvoke> 
                        </cfif>

						<cfset dataout.RXRESULTCODE = 1/>	
						<cfset dataout.MESSAGE = 'Payment transaction successfully'/>
					<cfelse>
						<cfset dataout.RXRESULTCODE = -3/>	
						<cfset dataout.MESSAGE = 'Payment successfully but can not add credits/keywords.'/>
					</cfif>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -2/>
					<cfset dataout.MESSAGE =  RxMakePaymentWithMojo.MESSAGE/>
					<cfset dataout.ERRMESSAGE = RxMakePaymentWithMojo.MESSAGE />
				</cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <!--- Get list transaction purchase need Admin verify ---> 
    <cffunction name="GetAdminListPaidUsers" access="remote" output="true" hint="Admin get list paid users"><!--- returnformat="JSON"--->
        <cfargument name="UserIdstatus" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        
        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetPMQuery = ''>
        <cfset var sSortDir_0 = 'DESC'/>
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var data = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetTemplate  = '' />
        <cfset var Status = ''/>
        <cfset var rsCount = '0'/>
        <cfset var fullName = ''/>
        <cfset var planName = ''/>
        <cfset var RetPlan = {}/>

        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        
            <!--- Order by --->
           <!---  <cfif iSortCol_0 GTE 0>
                <cfswitch expression="#Trim(iSortCol_0)#">
                    <cfcase value="4"> 
                    <cfset order = "pkw.UpdateDate" />
                    </cfcase> 
                    <cfcase value="5"> 
                    <cfset order = "pkw.PaymentAmount_dbl" />
                    </cfcase>
                    <cfdefaultcase> 
                        <cfset order = "pkw.UpdateDate"/>   
                        <cfset sSortDir_0 = 'DESC'/>    
                    </cfdefaultcase>    
                </cfswitch>
            <cfelse>
                <cfset order = "up.EndDate_dt"/>
                <cfset sSortDir_0 = 'DESC'/>
            </cfif>  --->

            <cfset order = "up.EndDate_dt"/>
            <cfset sSortDir_0 = 'ASC'/>

            <cfquery name="GetPMQuery" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS up.UserPlanId_bi, up.PlanId_int, up.BuyKeywordNumber_int, up.Amount_dec, up.EndDate_dt, up.MonthlyAddBenefitDate_dt, 
                    ua.EmailAddress_vch, ua.FirstName_vch ,ua.LastName_vch, ua.MFAContactString_vch, ua.UserId_int
                FROM 
                    simplebilling.userplans up
                LEFT JOIN
                    simpleobjects.useraccount ua
                ON    
                    up.UserId_int = ua.UserId_int   
                Where 
                    up.Status_int = 1 
                AND 
                    ( up.PlanId_int != 1 OR up.BuyKeywordNumber_int > 0) 
                AND
                    ua.IsTestAccount_ti = 0
                ORDER BY 
                    #order# #sSortDir_0#
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetPMQuery"> 
                <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetOrderPlan"  returnvariable="RetPlan">
                    <cfinvokeargument name="plan" value="#PlanId_int#">
                </cfinvoke>

                <cfif RetPlan.RXRESULTCODE EQ 1  >
                    <cfset planName = RetPlan.PLANNAME/>
                </cfif>
                
                <cfset fullName = FirstName_vch&" "&LastName_vch/>
                <cfset tempItem = {
                    USERID = '#UserId_int#',
                    NAME = "#fullName#",
                    EMAIL = "#EmailAddress_vch#",
                    PHONE = "#MFAContactString_vch#",
                    PAYMENT_DATE = "#DateFormat(EndDate_dt,'mm/dd/yyyy')#",
                    USERPLANID = "#UserPlanId_bi#",
                    BUYKEWORD = "#BuyKeywordNumber_int#",
                    PLANNAME = "#planName#"
                }>      
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="DoAdminPurchasedCreditKeyword" access="remote" output="true" hint="admin buy keyword,credits for user">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
        <cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc or new credits">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user">

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
        <cfset var RxMakePayment = {} />
        <cfset var DoAddPurchasedCreditKeywordResult = {} />
        <cfset var RevalupdatePaymentWorlPay = {}/>
        <cfset var RxStoreCreditCardInVault = {}/>
        <cfset var paymentdata = {}/>
        <cfset var email = {}/>
        <cfset var userLogOperator = {}/>

        <cftry>
			
            <cfif arguments.inpOrderAmount GT 0>
                
                <cfif arguments.inpSelectUsedCard EQ 1>    <!--- MAKE PAYMENT WITH SAVED CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>
                <cfelse> <!--- MAKE PAYMENT WITH NEW CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>    
                </cfif>

                

                <cfif RxMakePayment.RXRESULTCODE EQ 1> <!--- IF MAKE PAYMENT SUCCESS THEN ADD CC,KW TO USER --->
                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>

                    <cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
                        <cfinvokeargument name="inpNumberSMS" value="#paymentdata.numberSMSToSend#">
                        <cfinvokeargument name="inpNumberKeyword" value="#paymentdata.numberKeywordToSend#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1>
                        <cfset dataout.RESULT = "Success"/>
                        <cfset dataout.MESSAGE = "You have successfully purchased!"/>

                        <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePayment, "REPORT")>
                            <cfset logData =  RxMakePayment['REPORT']>
                        </cfif>

                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                            <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#">
                            <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>

    
                        <cfif arguments.inpSelectUsedCard EQ 2 AND arguments.inpSaveCCInfo EQ 1>

                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            </cfinvoke> 
                        </cfif>

                        <!--- SEND MAIL TO CUSTOMER --->
                        <!--- Sendmail payment --->
                        <cfif arguments.inpSelectUsedCard EQ 1>
                            <cfif paymentdata.h_email_save EQ ''>
                                <cfset email = paymentdata.h_email/>
                            <cfelse>
                                <cfset email = paymentdata.h_email_save/>
                            </cfif>
                        <cfelse>
                            <cfif paymentdata.inpEmail EQ ''>
                                <cfset email = paymentdata.h_email/>
                            <cfelse>
                                <cfset email = paymentdata.inpEmail/>
                            </cfif>
                        </cfif>

                        <cfset RxMakePayment.REPORT.EMAIL = email/>
                        <cfset RxMakePayment.REPORT.AMT = arguments.inpOrderAmount/>
                        <cfset RxMakePayment.REPORT.numberSMS = paymentdata.numberSMSToSend/>
                        <cfset RxMakePayment.REPORT.numberKeyword = paymentdata.numberKeywordToSend/>
                        <cftry>
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#email#">
                                <cfinvokeargument name="type" value="html">
                                <cfinvokeargument name="subject" value="[SIRE][Buy Credits - Keywords] Payment Info">
                                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                <cfinvokeargument name="data" value="#SerializeJSON(RxMakePayment)#">
                            </cfinvoke>
                            <cfcatch type="any">
                            </cfcatch>
                        </cftry>

                        <cfset userLogOperator = "Payment Executed OK!"/>
                        <cfif paymentdata.numberKeywordToSend EQ 0 AND paymentdata.numberSMSToSend GT 0>
                             <cfset userLogOperator&= "Credit added = #paymentdata.numberSMSToSend# for $#arguments.inpOrderAmount#" />
                        <cfelseif paymentdata.numberKeywordToSend GT 0 AND paymentdata.numberSMSToSend EQ 0>
                            <cfset userLogOperator&= "Payment Executed OK! Keyword Added = #paymentdata.numberKeywordToSend# for $#arguments.inpOrderAmount#" />
                        <cfelseif paymentdata.numberKeywordToSend GT 0 AND paymentdata.numberSMSToSend GT 0>
                            <cfset userLogOperator&= "Payment Executed OK! Credit added = #paymentdata.numberSMSToSend# and Keyword Added = #paymentdata.numberKeywordToSend# for $#arguments.inpOrderAmount#" />
                        </cfif>
                        <!--- Add Userlog #session.userid#--->
                        <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#arguments.inpUserId#">
                            <cfinvokeargument name="moduleName" value="Payment Processing">
                            <cfinvokeargument name="operator" value="#userLogOperator#">
                        </cfinvoke>

                         <!--- Log activity--->
                        <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#session.userid#">
                            <cfinvokeargument name="moduleName" value="Verify Payment Processing Above 99">
                            <cfinvokeargument name="operator" value="Verify Payment Processing Above 99: Account = #arguments.inpUserId# Credit added = #paymentdata.numberSMSToSend# and Keyword Added = #paymentdata.numberKeywordToSend# for $#arguments.inpOrderAmount#">
                        </cfinvoke>

                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = "#DoAddPurchasedCreditKeywordResult.MESSAGE#" />
                        <cfset dataout.RESULT = "Payment failed"/>        
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = "#RxMakePayment.MESSAGE#" />
                    <cfset dataout.RESULT = "Payment failed"/>    
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	<cffunction name="UpdateTransactionExceededLimit" access="remote" hint="Update transaction Exceeded limit">
		<cfargument name="inpPaymentMethod" type="string" required="true" default="" >
		<cfargument name="inpValue" type="string" required="true" default="" >
		<cfset var dataout	= {} />

        <cfset var UpdateTransactionExceededLimit = {}/>
		<cfset var PaymentTypeString=''>
		
		<cfset dataout.RXRESULTCODE = -1>		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<!--- Check record exsit then delete --->        
		<cftry>
			<cfif arguments.inpPaymentMethod EQ 1>
				<cfset PaymentTypeString='WorldPayTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 2>
				<cfset PaymentTypeString='PayPalTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 3>
				<cfset PaymentTypeString='MoJoTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 4>
				<cfset PaymentTypeString='TotalAppsTransactionExceededLimit'>
			</cfif>
            <cfquery name="UpdateTransactionExceededLimit" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.sire_setting
				SET
					Value_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpValue#">
				WHERE
					VariableName_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PaymentTypeString#">
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update Transaction Exceeded Limit successfully" />

			<cfcatch>                
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>        
		<cfreturn dataout>
	</cffunction> 
	<cffunction name="GetTransactionExceededLimit" access="remote" hint="Get transaction Exceeded limit">
		<cfargument name="inpPaymentMethod" type="string" required="true" default="" >		
		<cfset var dataout	= {} />

        <cfset var GetTransactionExceededLimit = {}/>
		<cfset var PaymentTypeString=''>
		
		<cfset dataout.RXRESULTCODE = -1>		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TRANSACTIONEXCEEDEDLIMIT = 0 />
		<!--- Check record exsit then delete --->        
		<cftry>
			<cfif arguments.inpPaymentMethod EQ 1>
				<cfset PaymentTypeString='WorldPayTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 2>
				<cfset PaymentTypeString='PayPalTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 3>
				<cfset PaymentTypeString='MoJoTransactionExceededLimit'>
			<cfelseif arguments.inpPaymentMethod EQ 4>
				<cfset PaymentTypeString='TotalAppsTransactionExceededLimit'>
			</cfif>
            <cfquery name="GetTransactionExceededLimit" datasource="#Session.DBSourceREAD#">
				SELECT
					Value_txt
				FROM
					simpleobjects.sire_setting				
				WHERE
					VariableName_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#PaymentTypeString#">
			</cfquery>
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TRANSACTIONEXCEEDEDLIMIT = GetTransactionExceededLimit.Value_txt/>
			<cfset dataout.MESSAGE = "Get Transaction Exceeded Limit successfully" />
			<cfif Trim(dataout.TRANSACTIONEXCEEDEDLIMIT) EQ "">
				<cfset dataout.TRANSACTIONEXCEEDEDLIMIT=0>
			</cfif>

			<cfcatch>                
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>        
		<cfreturn dataout>
	</cffunction> 
	<cffunction name="GetAllTransactionExceededLimit" access="remote" hint="Get All transaction Exceeded limit">		
		<cfset var dataout	= {} />

        <cfset var GetTransactionExceededLimit = {}/>
		<cfset var temp=''>
		<cfset var PaymentMethod=''>
		
		<cfset dataout.RXRESULTCODE = -1>		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.DATALIST = arrayNew(1)/>
		<!--- Check record exsit then delete --->        
		<cftry>			
            <cfquery name="GetTransactionExceededLimit" datasource="#Session.DBSourceREAD#">
				SELECT
					Value_txt,
					VariableName_txt
				FROM
					simpleobjects.sire_setting				
				WHERE
					VariableName_txt IN('WorldPayTransactionExceededLimit','PayPalTransactionExceededLimit','MoJoTransactionExceededLimit','TotalAppsTransactionExceededLimit')
			</cfquery>
			<cfloop query="GetTransactionExceededLimit">
				<cfif VariableName_txt EQ 'WorldPayTransactionExceededLimit'>
					<cfset PaymentMethod='1'>
				<cfelseif VariableName_txt EQ 'PayPalTransactionExceededLimit'>
					<cfset PaymentMethod='2'>
				<cfelseif VariableName_txt EQ 'MoJoTransactionExceededLimit'>
					<cfset PaymentMethod='3'>
				<cfelseif VariableName_txt EQ 'TotalAppsTransactionExceededLimit'>
					<cfset PaymentMethod='4'>
				</cfif>
				<cfset temp = {
					PaymentMethod = '#PaymentMethod#',
					Value = '#Value_txt#'
				}>		
				<cfset ArrayAppend(dataout.DATALIST,temp)>
			</cfloop>			
			<cfset dataout.RXRESULTCODE = 1 />			
			<cfset dataout.MESSAGE = "Get Transaction Exceeded Limit successfully" />

			<cfcatch>                
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>        
		<cfreturn dataout>
	</cffunction> 
	<cffunction name="UpdateTransactionExceededLimitLogs" access="remote" hint="update transaction exceeded limit logs">
		<cfargument name="inpPaymentMethod" type="string" required="true" default="" >
		<cfargument name="inpValue" type="string" required="true" default="" >		
		<cfargument name="inpTransaction" type="string" required="true" default="" >
		<cfargument name="inpItem" type="string" required="true" default="" >		
		<cfargument name="inpUserId" type="string" required="false" default="#Session.UserId#" >
		
		<cfset var dataout	= {} />

        <cfset var UpdateTransactionExceededLimitLogs = {}/>
		<cfset var GetUserInfo = {}/>
		<cfset var PaymentTypeString = ''>
		<cfset var rxGetUserInfoById = ''/>
        <cfset var data =''/>
        <cfset var retvalsendmail =''/>
		
		<cfset dataout.RXRESULTCODE = -1>		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<!--- Check record exsit then delete --->       		 
		<cftry>			
			<cfset arguments.inpValue= numberFormat(arguments.inpValue,'.99')>
            <cfquery name="UpdateTransactionExceededLimitLogs" datasource="#Session.DBSourceEBM#">
				INSERT INTO simplebilling.logs_transaction_exceeded_limit
				(
					UserId_int,
					Transaction_vch,
					Item_vch,
					Amount_dec,
					paymentGateway_ti,
					CreateDate_dt
				)
				VALUES
				(
					<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpUserId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTransaction#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpItem#">,
					<cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.inpValue#">,
					<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpPaymentMethod#">,
					NOW()
				)			
			</cfquery>
			<cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
				SELECT
					EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch 
				FROM
					simpleobjects.useraccount
				WHERE                
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">				
			</cfquery>
						
			
			<!--- Send Email to Admin --->
			<cftry>
				<cfinclude template="/public/sire/configs/paths.cfm"/>
				<cfset data.USERID = arguments.inpUserId/>
				<cfset data.AMOUNT = arguments.inpValue/>	
				<cfset data.PHONENUMBER = GetUserInfo.MFAContactString_vch />			
				<cfset data.USERNAME = GetUserInfo.FirstName_vch & " " & GetUserInfo.LastName_vch/>
				<cfset data.TRANSACTION=arguments.inpTransaction>
				<cfset data.GATEWAY="Mojo">	
				<cfif arguments.inpPaymentMethod EQ 1>	
					<cfset data.GATEWAY="WorldPay">	
				<cfelseif arguments.inpPaymentMethod EQ 2>	
					<cfset data.GATEWAY="PayPal">	
				<cfelseif arguments.inpPaymentMethod EQ 3>	
					<cfset data.GATEWAY="Mojo">	
				<cfelseif arguments.inpPaymentMethod EQ 4>	
					<cfset data.GATEWAY="TotalApps">	
				</cfif>
					
				<!--- Send email alert #SupportEMailUserName#--->
				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
					<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
					<cfinvokeargument name="type" value="html"/>
					<cfinvokeargument name="subject" value="Customer Transaction Exceeded Limit By Payment Gateway"/>
					<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_payment_exceeded_limit.cfm"/>
					<cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
				</cfinvoke>
				<cfcatch></cfcatch>

			</cftry>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update Transaction Exceeded Limit successfully" />

			<cfcatch>                
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>        
		<cfreturn dataout>
	</cffunction> 
	<cffunction name="GetListTransactionExceededLimitLogs" access="remote" output="true" hint="Get list transaction exceeded limit log"><!--- returnformat="JSON"--->        
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        
        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetList = ''>        
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
		<cfset var GateWay   = '' />
        <cfset var sSortDir_0= arguments.sSortDir_0>

        <cfset var fullName   = '' />
        <cfset var rsCount   = {} />
        
        
        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListData"] = ArrayNew(1)>
        
            <!--- Order by --->
           <cfif iSortCol_0 GTE 0>
                <cfswitch expression="#Trim(iSortCol_0)#">
                    <cfcase value="0"> 
                    	<cfset order = "ltel.UserId_int" />
                    </cfcase> 
                    <cfcase value="1"> 
                    	<cfset order = "ua.FirstName_vch" />
                    </cfcase>
					<cfcase value="2"> 
                    	<cfset order = "ua.EmailAddress_vch" />
                    </cfcase> 
					<cfcase value="3"> 
                    	<cfset order = "ua.MFAContactString_vch" />
                    </cfcase> 
					<cfcase value="4"> 
                    	<cfset order = "ltel.Transaction_vch" />
                    </cfcase> 
					<cfcase value="6"> 
                    	<cfset order = "ltel.CreateDate_dt" />
                    </cfcase> 
					<cfcase value="7"> 
                    	<cfset order = "ltel.Amount_dec" />
                    </cfcase> 
					<cfcase value="8"> 
                    	<cfset order = "ltel.paymentGateway_ti" />
                    </cfcase> 
                    <cfdefaultcase> 
                        <cfset order = "ltel.CreateDate_dt"/>                            
                    </cfdefaultcase>    
                </cfswitch>
            <cfelse>
                <cfset order = "ltel.CreateDate_dt"/>
                <cfset sSortDir_0 = 'DESC'/>
            </cfif>

			

            <cfquery name="GetList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS 
					ltel.UserId_int,
					ltel.Transaction_vch,
					ltel.Amount_dec,
					ltel.paymentGateway_ti,
					ltel.CreateDate_dt,		
					ltel.Item_vch,			
                    ua.EmailAddress_vch, 
					ua.FirstName_vch ,
					ua.LastName_vch, 
					ua.MFAContactString_vch
                FROM 
                    simplebilling.logs_transaction_exceeded_limit ltel
                INNER JOIN
                    simpleobjects.useraccount ua
                ON    
                    ltel.UserId_int = ua.UserId_int        
				WHERE
				ua.IsTestAccount_ti=0				
				<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>             
                ORDER BY 
                    #order# #sSortDir_0#
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetList">                
                
                <cfset fullName = FirstName_vch&" "&LastName_vch/>
				<cfif PaymentGateway_ti EQ 1>
					<cfset GateWay= "WorldPay">
				<cfelseif PaymentGateway_ti EQ 2>
					<cfset GateWay= "PayPal">
				<cfelseif PaymentGateway_ti EQ 3>
					<cfset GateWay= "Mojo">
				<cfelseif PaymentGateway_ti EQ 4>
					<cfset GateWay= "TotalApps">
				</cfif>
                <cfset tempItem = {					
					USERID = '#UserId_int#',
					NAME = "#fullName#",
					EMAIL = "#EmailAddress_vch#",
					PHONE = "#MFAContactString_vch#",
					ITEM =	"#Item_vch#",
					TRANSACTION = "#Transaction_vch#",
					TRANSACTION_DATE = "#DateFormat(CreateDate_dt,'mm/dd/yyyy')#",					
					TRANSACTION_AMOUNT = "$#NumberFormat(Amount_dec,'0.00')#",
					
                    PAYMENTGATEWAY =  "#GateWay#"
				}>	 
                <cfset ArrayAppend(dataout["ListData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	<cffunction name="AdminMakePayment" access="remote" output="true" hint="admin make payment">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
        <cfargument name="inpSelectUsedCard" TYPE="string" required="true" default="2" hint="used cc or new credits">
        <cfargument name="inpSaveCCInfo" TYPE="string" required="true" default="0" hint="Save cc for user">

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
        <cfset var RxMakePayment = {} />
        <cfset var DoAddPurchasedCreditKeywordResult = {} />
        <cfset var RevalupdatePaymentWorlPay = {}/>
        <cfset var RxStoreCreditCardInVault = {}/>
        <cfset var paymentdata = {}/>
        <cfset var email = {}/>
        <cfset var userLogOperator = {}/>

        <cftry>

            <cfif arguments.inpOrderAmount GT 0>
                
                <cfif arguments.inpSelectUsedCard EQ 1>    <!--- MAKE PAYMENT WITH SAVED CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithUserCardId" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpModuleName" value="Admin Make Payment">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>
                <cfelse> <!--- MAKE PAYMENT WITH NEW CARD --->
                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePayment">
                        <cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#">
                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                        <cfinvokeargument name="inpPaymentdata" value="#arguments.inpPaymentdata#">
                        <cfinvokeargument name="inpModuleName" value="Admin Make Payment">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>    
                </cfif>

                

                <cfif RxMakePayment.RXRESULTCODE EQ 1>                     
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.RESULT = "Success"/>
					<cfset dataout.MESSAGE = "You have successfully purchased!"/>

					<!--- LOG PAYMENT SUCCESS --->
					<cfif structKeyExists(RxMakePayment, "REPORT")>
						<cfset logData =  RxMakePayment['REPORT']>
					</cfif>

					<cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">						
						<cfinvokeargument name="moduleName" value="Admin Make Payment">
						<cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
						<cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
						<cfinvokeargument name="inpAdminUserId" value="#session.userid#">
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
						
					</cfinvoke>


					<cfif arguments.inpSelectUsedCard EQ 2 AND arguments.inpSaveCCInfo EQ 1>

						<cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
							<cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
							<cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
						</cfinvoke> 
					</cfif>
					
					<!--- Log activity--->
					<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Admin Make Payment">
						<cfinvokeargument name="operator" value="Admin Make Payment for #arguments.inpUserId#, Amount: #arguments.inpOrderAmount#">
					</cfinvoke>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = "#RxMakePayment.MESSAGE#" />
                    <cfset dataout.RESULT = "Payment failed"/>    
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	<cffunction name="UpdatePayment" access="remote" output="true" hint="Update payment" >
        <cfargument name="moduleName" type="string" required="yes" default="">
        <cfargument name="inpUserId" type="string" required="yes" default="">
        <cfargument name="inpPaymentData" type="string" required="yes" default="">                
        <cfargument name="inpPlanId" type="string" required="yes" default="">
        
        <cfargument name="numberKeyword" type="string" required="no" default="0">
        <cfargument name="numberSMS" type="string" required="no" default="0">        
        <cfargument name="inpPaymentByUserId" type="string" required="no" default="">
		<cfargument name="inpPaymentGateway" type="string" required="yes" default="">
        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Update Fail">
        

        <cfset var updatePayment	= '' />
        <cftry>                
            <cfset var data=  deserializeJSON(arguments.inpPaymentData)>
            <cfquery name="updatePayment" datasource="#Session.DBSourceEBM#" result="updatePayment">
                INSERT INTO `simplebilling`.`payment_worldpay` 
                (   
                    `userId_int`,
                    `planId`, 
                    `moduleName`,
                    `bynumberKeyword`,
                    `bynumberSMS`,
                    `transactionType`,
                    `orderId`, 
                    `transactionId`, 
                    `authorizedAmount`, 
                    `paymentTypeCode`, 
                    `paymentTypeResult`, 
                    `transactionData_amount`, 
                    `cardNumber`, 
                    `avsCode`, 
                    `cardHolder_FirstName`, 
                    `cardHolder_LastName`, 
                    `billAddress_line1`, 
                    `billAddress_city`, 
                    `billAddress_state`, 
                    `billAddress_zip`, 
                    `billAddress_company`, 
                    `billAddress_phone`, 
                    `email`, 
                    `method`, 
                    `responseText`,
                    `paymentGateway_ti`,
                    `created`,
                    `PaymentByUserId_int`
                    
                )
                VALUES 
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpPlanId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberKeyword#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberSMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONTYPE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.ORDERID#">,   
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPNUMBER#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCVV2#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPFIRSTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLASTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLINE1#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCITY#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPSTATE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPPOSTALCODE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.PHONE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.METHOD#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.RESPONSE#">,                     
					<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpPaymentGateway#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentByUserId#">
                )
            </cfquery>
            <cfset dataout.RESULT = "Success">
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = "Update successfully">
            <cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>
    <cffunction name="AdminRemoveUserCC" access="remote" hint="Admin remove an user's CC">
        <cfargument name="inpUserID" default="0">        
        <cfargument name="inpPaymentGateway" default="0">    
        
        <cfset var dataout  = {} />
        <cfset var apiRs    = {} />        
        <cfset var removeAuthorizeUser  = "" />
        <cfset var removeUserccinfo  = "" />
        <cfset var createUserLog  = "" />
		<cfset var RetVarGetAdminPermission	= '' />
        
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.DATA = ''>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <!--- Check permission first --->
        <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif>

        <cftry>         
            <!--- 1. delete on simplebilling.authorize_user --->
            <cfquery name="removeAuthorizeUser" datasource="#Session.DBSourceEBM#">
                DELETE FROM 
                    simplebilling.authorize_user
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                LIMIT 1
            </cfquery>                        
            <!--- 2. delete on simplebilling.userccinfo--->
            <cfquery name="removeUserccinfo" datasource="#Session.DBSourceEBM#">
                DELETE FROM 
                    simplebilling.userccinfo
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                LIMIT 1
            </cfquery>     
            <!--- 3. log --->
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.userid#">
                    <cfinvokeargument name="moduleName" value="Admin Remove User CC">
                    <cfinvokeargument name="operator" value="Admin #session.userid# has been removed User CC of UserId #arguments.inpUserID# with payment gateway #arguments.inpPaymentGateway#.">
            </cfinvoke>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = "Remove Success!">    
            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                <cfreturn dataout />
            </cfcatch>
        </cftry>
        <cfreturn dataout>

    </cffunction>
</cfcomponent>
