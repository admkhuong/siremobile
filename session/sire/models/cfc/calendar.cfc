<cfcomponent>
	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfset LOCAL_LOCALE = "English (US)">
    <cfinclude template="/session/cfc/csc/constants.cfm">



<cffunction name="AddEventSync" access="remote" output="false" hint="Sync the calender">
        <!--- <cfargument name="inpCalendarId" type="string" required="no" default="1"/> --->
        <cfargument name="inpEvent" type="string" required="true" hint="The structure of the event"/>
        <cfargument name="inpASR" required="false" default="0" hint="If ASR is on - then try to schedule a reminder message. Will only work if reminder message campaign (BatchId) is defined in the calendar's configuration data." />

        <cfset var dataout = {} />
		<cfset var AddEventQuery = '' />
		<cfset var insertResult	= '' />
		<cfset var RetValAddEventReminderToQueue	= '' />
		<cfset var _comma = ''/>
		<cfset var event = ''/>
		<cfset var RetValSetCalendarConfigurationDefault = ''/>
		<cfset var cid = 0/>

        <cftry>

	        <cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(inpEvent)) GT 0 >
				<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
	        </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.DTSID = 0 />
            <cfset dataout.CONFIRMFLAG = 0 />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

<!--- this is pretty dumb - why set defaults on every entry ???!??? --->
			<cfinvoke component="session.sire.models.cfc.calendar" method="SetCalendarConfigurationDefault" returnvariable="RetValSetCalendarConfigurationDefault"></cfinvoke>

			<cfif RetValSetCalendarConfigurationDefault.RXRESULTCODE GT 0>
				<cfset cid = RetValSetCalendarConfigurationDefault.CALENDARID>
			</cfif>

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddEventQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               INSERT IGNORE INTO
               		calendar.events
                    (
                    	PKId_bi,
                    	UserId_int,
                    	CalendarId_int,
                    	Title_vch,
                    	AllDayFlag_int,
                    	Start_dt,
                    	End_dt,
                    	URL_vch,
                    	ClassName_vch,
                    	EditableFlag_int,
                    	StartEditableFlag_int,
                    	DurationEditableFlag_int,
                    	ResourceEditableFlag_int,
                    	Rendering_vch,
                    	OverlapFlag_int,
                    	Constraint_vch,
                    	Color_vch,
                    	BackgroundColor_vch,
                    	BorderColor_vch,
                    	TextColor_vch,
                    	eMailAddress_vch,
                    	SMSNumber_vch,
                    	Created_dt,
                    	Updated_dt,
                    	Notes_vch,
                    	ConfirmationFlag_int,
						CalendarGoogleId_vch,
                    	ConfirmationOneDTSId_int,
                    	Active_int
                     )
               VALUES
                   	<cfset _comma = ''>
					<cfloop array="#inpEvent#" item="event">
						#_comma#(
							NULL,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
							#cid#, <!---inpCalendarId--->
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#event.eventName#">, <!---inpTitle --->
							<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#event.AllDayFlag#">, <!---inpAllDayFlag --->
							<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#event.startDate#">, <!---inpStart --->
							<cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#event.endDate#">, <!---inpEnd --->
							NULL, <!---inpURL --->
							NULL, <!---inpClassName --->
							1, <!---inpEditableFlag --->
							1, <!---inpStartEditableFlag --->
							1, <!---inpDurationEditableFlag --->
							1, <!---inpResourceEditableFlag --->
							NULL, <!---inpRendering --->
							1, <!---inpOverlapFlag --->
							NULL, <!---inpConstraint --->
							NULL, <!---inpColor --->
							NULL, <!---inpBackgroundColor --->
							NULL, <!---inpBorderColor --->
							NULL, <!---inpTextColor --->
							NULL, <!---inpeMail --->
							NULL, <!---inpSMSNumber --->
							NOW(),
							NOW(),
							NULL, <!---inpNotes --->
							6, <!---inpConfirmationFlag --->
							<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#event.id#">, <!---GoogleCalendar Id --->
							0, <!---inpConfirmationOneDTSId --->
							1
						)
						<cfset _comma = ','>
					</cfloop>
            </cfquery>


            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Sync Calendar Event OK.'>

	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create Calendar Event failed. Please try again later'>
	        </cfif>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="GetCalendarEventInfoSync" access="remote" output="false" hint="Get the Google calendar id list">
        <cfset var dataout = {} />
        <cfset var GetEventInfoQuery= '' />
		<cfset var shortCode = {}/>

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.GGIDLIST = {}/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<!---  SHORTCODEID, SHORTCODE--->

			<cfquery name="GetEventInfoQuery" datasource="#Session.DBSourceEBM#">
                SELECT
					CalendarGoogleId_vch
                FROM
                    calendar.events
                WHERE
					Active_int = 1
	        </cfquery>

	  		<cfif GetEventInfoQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.GGIDLIST = ValueList(GetEventInfoQuery.CalendarGoogleId_vch,',')/>
				<cfreturn dataout />
	  		</cfif>

			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>

        </cftry>

	</cffunction>

	<cffunction name="AddEvent" access="remote" output="false" hint="Add a new event to the calender">
        <cfargument name="inpCalendarId" type="string" required="no" default=""/>
        <cfargument name="inpEvent" type="string" required="true" hint="The structure of the event"/>
        <cfargument name="inpASR" required="false" default="0" hint="If ASR is on - then try to schedule a reminder message. Will only work if reminder message campaign (BatchId) is defined in the calendar's configuration data." />

		<!--- structure inpEvent
				inpTitle required="no" type="string" default="Appointment" hint="Event description - defaults to 'Appointment'"
				inpAllDayFlag required="no" type="string" default="0" hint="If set will ignore start and end times"
				inpStart required="yes" type="string"
				inpEnd required="yes" type="string"
				inpURL required="no" type="string" default="" hint="If specified will link to the URL when the event is clicked"
				inpClassName required="no" type="string" default="" hint="Optional CSS Class to apply to the event"
				inpEditableFlag required="no" type="string" default="10" hint="Event - 1= event is ediatble, 0 event is not editable"
				inpStartEditableFlag required="no" type="string" default="10" hint="Start of Event - 1= event is ediatble, 0 event is not editable"
				inpDurationEditableFlag required="no" type="string" default="10" hint="Duration of the Event - 1= event is ediatble, 0 event is not editable"
				inpResourceEditableFlag required="no" type="string" default="10" hint="Resources assigned tot he event - 1= event is ediatble, 0 event is not editable"
				inpRendering required="no" default="" hint="Allows alternate rendering of the event, like background events. Can be empty, 'background', or 'inverse-background' default is empty"
				inpOverlapFlag required="no" type="string" default="0" hint="If 0, prevents this event from being dragged/resized over other events. Also prevents other events from being dragged/resized over this event."
				inpConstraint required="no" type="string" default="" hint="an event ID, 'businessHours', object. Optional"
				inpColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpBackgroundColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpBorderColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpTextColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpSMSNumber required="no" hint="SMS Phone Number to notify / Remind / Change event"
				inpeMail required="no" hint="eMail address to notify / Remind / Change event"
				inpNotes required="no" type="string" default="" hint=""
				inpConfirmationFlag required="no" type="string" default="0" hint="Flag to indicated confirmation / reminder message state"
		--->
        <cfset var dataout = {} />
		<cfset var AddEventQuery = '' />
		<cfset var insertResult	= '' />
		<cfset var RetValAddEventReminderToQueue	= '' />

		<cfset var START_DT	= '' />
		<cfset var STOP_DT	= '' />
		<cfset var STARTHOUR_TI	= '' />
		<cfset var ENDHOUR_TI	= '' />
		<cfset var SUNDAY_TI	= '' />
		<cfset var MONDAY_TI	= '' />
		<cfset var TUESDAY_TI	= '' />
		<cfset var WEDNESDAY_TI	= '' />
		<cfset var THURSDAY_TI	= '' />
		<cfset var FRIDAY_TI	= '' />
		<cfset var SATURDAY_TI	= '' />
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI	= '' />
		<cfset var CheckScheduleOptionExist	= '' />
		<cfset var CreateSchedule	= '' />
		<cfset var UpdateEventQuery	= '' />
		<cfset var RetValGetCalendarConfiguration	= '' />

        <cftry>

	        <cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(inpEvent)) GT 0 >
				<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
	        </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.DTSID = 0 />
            <cfset dataout.CONFIRMFLAG = 0 />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <!--- Check phone number US --->
			<cfif NOT isvalid('telephone', inpEvent.inpSMSNumber)>
				<cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number !'>
				<cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number !'>
				<cfreturn dataout>
			</cfif>

            <cfset arguments.inpEvent.inpStart = "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#">
            <cfset arguments.inpEvent.inpEnd = "#DateFormat(arguments.inpEvent.inpEnd,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpEnd,'HH:mm:ss')#">

			<!--- 	Update scheduleoptions --->
			<!--- Read critical configuration parameter from DB - not as user passed in variables - little slower vs less likely to get hacked --->
	        <cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
				<cfinvokeargument name="inpCalendarId" value="#inpCalendarId#">
			</cfinvoke>
			<cfif inpCalendarId EQ "">
				<cfset arguments.inpCalendarId = RetValGetCalendarConfiguration.ID>
			</cfif>
			<cfif RetValGetCalendarConfiguration.BATCHID NEQ 0 OR  RetValGetCalendarConfiguration.BATCHID NEQ ''>
				<cfquery name="CheckScheduleOptionExist" datasource="#Session.DBSourceEBM#">
					SELECT
						BatchId_bi
					FROM
						simpleobjects.scheduleoptions
					WHERE
						BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#" />
					AND
						Enabled_ti > 0
				</cfquery>
				<cfif CheckScheduleOptionExist.RecordCount LT 1>
						<cfset START_DT = Now()>
						<cfset STOP_DT = DateAdd('yyyy', 100, Now())>
						<cfset STARTHOUR_TI = 9>
						<cfset ENDHOUR_TI = 21>
						<cfset SUNDAY_TI = true>
						<cfset MONDAY_TI = true>
						<cfset TUESDAY_TI = true>
						<cfset WEDNESDAY_TI = true>
						<cfset THURSDAY_TI = true>
						<cfset FRIDAY_TI = true>
						<cfset SATURDAY_TI = true>
						<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = 0>

					<cfquery name="CreateSchedule" datasource="#Session.DBSourceEBM#">
						INSERT INTO simpleobjects.scheduleoptions
						(
							BatchId_bi,
							STARTHOUR_TI,
							ENDHOUR_TI,
							SUNDAY_TI,
							MONDAY_TI,
							TUESDAY_TI,
							WEDNESDAY_TI,
							THURSDAY_TI,
							FRIDAY_TI,
							SATURDAY_TI,
							LOOPLIMIT_INT,
							<cfif STOP_DT NEQ 'null'>
								STOP_DT,
							</cfif>
							<cfif START_DT NEQ 'null'>
								START_DT,
							</cfif>
							STARTMINUTE_TI,
							ENDMINUTE_TI,
							BLACKOUTSTARTHOUR_TI,
							BLACKOUTENDHOUR_TI,
							LASTUPDATED_DT,
							DYNAMICSCHEDULEDAYOFWEEK_TI,
							ENABLED_TI
						)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(RetValGetCalendarConfiguration.BATCHID)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#STARTHOUR_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ENDHOUR_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SUNDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#MONDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#TUESDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#WEDNESDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#THURSDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#FRIDAY_TI#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SATURDAY_TI#">,
							100,
							<cfif STOP_DT NEQ 'null'>
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#STOP_DT#">,
							</cfif>
							<cfif START_DT NEQ 'null'>
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#START_DT#">,
							</cfif>
							0,
							0,
							0,
							0,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DYNAMICSCHEDULEDAYOFWEEK_TI#">,
							1
						)
					</cfquery>
				</cfif>

			</cfif>

            <!---Find and replace all non numerics except P X * #--->
			<cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddEventQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               INSERT INTO
               		calendar.events
                    (
                    	PKId_bi,
                    	UserId_int,
                    	CalendarId_int,
                    	Title_vch,
                    	AllDayFlag_int,
                    	Start_dt,
                    	End_dt,
                    	URL_vch,
                    	ClassName_vch,
                    	EditableFlag_int,
                    	StartEditableFlag_int,
                    	DurationEditableFlag_int,
                    	ResourceEditableFlag_int,
                    	Rendering_vch,
                    	OverlapFlag_int,
                    	Constraint_vch,
                    	Color_vch,
                    	BackgroundColor_vch,
                    	BorderColor_vch,
                    	TextColor_vch,
                    	eMailAddress_vch,
                    	SMSNumber_vch,
                    	Created_dt,
                    	Updated_dt,
                    	Notes_vch,
                    	ConfirmationFlag_int,
                    	ConfirmationOneDTSId_int,
                    	Active_int
                     )
               VALUES
                    (
                    	NULL,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpTitle, 255)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpAllDayFlag#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpStart#" />,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpEnd#" null="#not(len(inpEvent.inpEnd))#" />,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpURL, 2048)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpClassName, 255)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpEditableFlag#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpStartEditableFlag#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpDurationEditableFlag#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpResourceEditableFlag#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpRendering, 25)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpOverlapFlag#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpConstraint#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpColor, 25)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpBackgroundColor, 25)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpBorderColor, 25)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpTextColor, 25)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpeMail, 255)#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpSMSNumber, 45)#">,
	                    NOW(),
	                    NOW(),
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpNotes#">,
	                   	6,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpConfirmationOneDTSId#">,
	                    1
	                )
            </cfquery>
            <!---<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpConfirmationFlag#">,--->

            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Create Event successfully.'>

	        	<cfset dataout.EVENTID = insertResult.GENERATED_KEY />
	        	<cfset arguments.inpEvent.id = insertResult.GENERATED_KEY />


		        <cfif inpASR GT 0 AND LEN(TRIM(arguments.inpEvent.inpSMSNumber)) GTE 10>
							<!--- Add event to queue --->
							<cfinvoke method="AddEventReminderToQueue" returnvariable="RetValAddEventReminderToQueue">
								<cfinvokeargument name="inpCalendarId" value="#inpCalendarId#">
								<cfinvokeargument name="inpEvent" value="#inpEvent#">
							</cfinvoke>
							<cfif RetValAddEventReminderToQueue.RXRESULTCODE EQ -1>
								<cfset dataout.RXRESULTCODE = "-1" />
								<cfset dataout.TYPE = "" />
								<cfset dataout.ERRMESSAGE = "#RetValAddEventReminderToQueue.ERRMESSAGE#" />
								<cfreturn dataout />
							<cfelse>
								<cfif RetValAddEventReminderToQueue.DISTRIBUTIONID GT 0>
									<cfset dataout.DTSID = RetValAddEventReminderToQueue.DISTRIBUTIONID />
									<cfset dataout.CONFIRMFLAG = APPT_REMINDER_QUEUED />
									<cfset arguments.inpEvent.inpConfirmationFlag = APPT_REMINDER_QUEUED />
									<cfset arguments.inpEvent.inpConfirmationOneDTSId = RetValAddEventReminderToQueue.DISTRIBUTIONID />
								</cfif>
								<cfset dataout.DEBUGDATA = "#SerializeJSON(RetValAddEventReminderToQueue)#" />
							</cfif>
	            </cfif>
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create Calendar Event failed. Please try again later'>
	        </cfif>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	 <cffunction name="ReadEvents" access="remote" output="false" hint="Get Calendar Events for Date Range"><!--- returnformat="JSON"--->
	    <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
	    <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">
		<cfset var GetFilterCalendar	= ''>
		<cfset var GetEventChatClose = ''>
		<cfset var listEventUpdate = ''>
		<cfset var UpdateEventChatClose = ''>

	    <cfset var dataOut=ArrayNew(1)>
	    <cfset var returnStruct = StructNew()>
        <cfset var GetEventsQuery = ''>

		<cftry>

        	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
        	<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

            <cfquery name="GetFilterCalendar" datasource="#Session.DBSourceEBM#">
				SELECT
					FilterConfirmationFlag_vch,
					Chat_BatchId_bi
				FROM
					calendar.configuration
				WHERE
					PKID_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
			</cfquery>

			<cfquery name="GetEventChatClose" datasource="#Session.DBSourceEBM#">
				SELECT
					ev.PKId_bi
				FROM
					calendar.events ev INNER JOIN simplequeue.sessionire si ON ev.ChatSessionId_bi = si.SessionId_bi
				WHERE
					ev.CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
				AND
					si.BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetFilterCalendar.Chat_BatchId_bi#" />
				AND
					si.SessionState_int = 4
				AND
					ev.ConfirmationFlag_int = 8
			</cfquery>

			<cfloop query="GetEventChatClose">
				<cfset listEventUpdate = ListAppend(listEventUpdate, GetEventChatClose.PKId_bi, ',')>
			</cfloop>

			<cfif listEventUpdate NEQ "">
				<cfquery name="UpdateEventChatClose" datasource="#Session.DBSourceEBM#">
					UPDATE
						calendar.events
					SET
						ConfirmationFlag_int = 10
					WHERE
						PKId_bi IN (#listEventUpdate#)
				</cfquery>
			</cfif>

            <cfquery name="GetEventsQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    PKId_bi,
                	UserId_int,
                	CalendarId_int,
                	Title_vch,
                	AllDayFlag_int,
               	    Start_dt,
                    End_dt,
                    URL_vch,
                	ClassName_vch,
                	EditableFlag_int,
                	StartEditableFlag_int,
                	DurationEditableFlag_int,
                	ResourceEditableFlag_int,
                	Rendering_vch,
                	OverlapFlag_int,
                	Constraint_vch,
                	Color_vch,
                	BackgroundColor_vch,
                	BorderColor_vch,
                	TextColor_vch,
                	eMailAddress_vch,
                	SMSNumber_vch,
                	Created_dt,
                	Updated_dt,
                	Notes_vch,
                	ConfirmationFlag_int,
                	ConfirmationOneDTSId_int,
                	Active_int
                FROM
                    calendar.events
                WHERE
                    CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
				AND
                	Active_int > 0
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
                	Start_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(inpStart,'YYYY-MM-DD'))#">
                AND
                	Start_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(inpEnd,'YYYY-MM-DD'))#">
				<cfif GetFilterCalendar.FilterConfirmationFlag_vch neq ''>
					AND
						ConfirmationFlag_int in (#GetFilterCalendar.FilterConfirmationFlag_vch#)
				</cfif>
            </cfquery>
            <!--- Need to maintain case in JSON  - use array notaition - not dot notation --->
            <!--- Bad json case will mess up fullcalendar.js processing - particularly on hasTime function --->
            <!--- https://www.bennadel.com/blog/2162-maintaining-key-case-during-json-serialization-in-coldfusion.htm --->

            <!--- Build result array --->
		    <cfloop query="GetEventsQuery">
		        <cfset returnStruct = StructNew() />

		        <cfset returnStruct[ "id" ]  = GetEventsQuery.PKId_bi />
				<cfset returnStruct[ "UserId_int" ] = GetEventsQuery.UserId_int />
		        <cfset returnStruct[ "CalendarId_int" ] = GetEventsQuery.CalendarId_int />
				<cfset returnStruct[ "title" ] = GetEventsQuery.Title_vch />
		        <cfset returnStruct[ "allDay" ] = GetEventsQuery.AllDayFlag_int />
		        <cfset returnStruct[ "start" ] = "#DateFormat(GetEventsQuery.Start_dt,'yyyy-mm-dd')#T#LSTimeFormat(GetEventsQuery.Start_dt,'HH:mm:ss')#" />


		        <cfif TRIM(GetEventsQuery.End_dt) NEQ ''>
		        	<cfset returnStruct[ "end" ] = "#DateFormat(GetEventsQuery.End_dt,'yyyy-mm-dd')#T#LSTimeFormat(GetEventsQuery.End_dt,'HH:mm:ss')#" />
		        </cfif>

		        <cfset returnStruct[ "eMailAddress_vch" ] = GetEventsQuery.eMailAddress_vch />
		        <cfset returnStruct[ "inpSMSNumber" ] = GetEventsQuery.SMSNumber_vch />
		        <cfset returnStruct[ "Created_dt" ] = "#DateFormat(GetEventsQuery.Created_dt,'yyyy-mm-dd')# #LSTimeFormat(GetEventsQuery.Created_dt,'HH:mm:ss')#" />
		        <cfset returnStruct[ "Updated_dt" ] = "#DateFormat(GetEventsQuery.Updated_dt,'yyyy-mm-dd')# #LSTimeFormat(GetEventsQuery.Updated_dt,'HH:mm:ss')#" />
		        <cfset returnStruct[ "Notes_vch" ] = GetEventsQuery.Notes_vch />
		        <cfset returnStruct[ "inpConfirmationFlag" ] = GetEventsQuery.ConfirmationFlag_int />
		        <cfset returnStruct[ "inpConfirmationOneDTSId" ] = GetEventsQuery.ConfirmationOneDTSId_int />
		        <cfset returnStruct[ "Active_int" ] = GetEventsQuery.Active_int />


		        <!--- Appointment Reminder ConfirmationFlag States	--->
				<!---
				<cfset var APPT_REMINDER_NOT_SENT = 0>(Hide)
				<cfset var APPT_REMINDER_QUEUED = 1>
				<cfset var APPT_REMINDER_ACCEPTED = 2>
				<cfset var APPT_REMINDER_DECLINED = 3>
				<cfset var APPT_REMINDER_CHANGE_REQUEST = 4>
				<cfset var APPT_REMINDER_ERROR = 5> (Hide)
				<cfset var APPT_REMINDER_DO_NOT_SEND = 6>
				<cfset var APPT_REMINDER_SENT = 7>
				<cfset var APPT_REMINDER_CHAT_ACTIVE = 8>
				<cfset var APPT_REMINDER_OPT_OUT = 9>
				<cfset var APPT_REMINDER_CHAT_CLOSE = 10>
				--->

		        <cfswitch expression="#GetEventsQuery.ConfirmationFlag_int#">
			        <cfcase value="#APPT_REMINDER_NOT_SENT#">
				        <cfset returnStruct[ "backgroundColor" ] = "##3a87adf" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfcase value="#APPT_REMINDER_QUEUED#">
				        <cfset returnStruct[ "backgroundColor" ] = "##C8BFE7" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>
					<cfcase value="#APPT_REMINDER_ACCEPTED#">
				        <cfset returnStruct[ "backgroundColor" ] = "##83dd8f" />
				        <cfset returnStruct[ "borderColor" ] = "##00aa00" />
			        </cfcase>

			        <cfcase value="#APPT_REMINDER_DECLINED#">
				        <cfset returnStruct[ "backgroundColor" ] = "##ad433a" />
				        <cfset returnStruct[ "borderColor" ] = "##aa0000" />
			        </cfcase>

			        <cfcase value="#APPT_REMINDER_CHANGE_REQUEST#">
				        <cfset returnStruct[ "backgroundColor" ] = "##000000" />
				        <cfset returnStruct[ "borderColor" ] = "##aa0000" />
			        </cfcase>

			        <cfcase value="#APPT_REMINDER_ERROR#">
				        <cfset returnStruct[ "backgroundColor" ] = "##dd8683" />
				        <cfset returnStruct[ "borderColor" ] = "##00aa00" />
			        </cfcase>

			        <cfcase value="#APPT_REMINDER_DO_NOT_SEND#">
				        <cfset returnStruct[ "backgroundColor" ] = "##3a87ad" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfcase value="#APPT_REMINDER_SENT#">
				        <cfset returnStruct[ "backgroundColor" ] = "##008B00" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfcase value="#APPT_REMINDER_CHAT_ACTIVE#">
				        <cfset returnStruct[ "backgroundColor" ] = "##0000ff" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfcase value="#APPT_REMINDER_OPT_OUT#">
				        <cfset returnStruct[ "backgroundColor" ] = "##dd8683" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfcase value="#APPT_REMINDER_CHAT_CLOSE#">
				        <cfset returnStruct[ "backgroundColor" ] = "##999999" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />
			        </cfcase>

					<cfdefaultcase>

				        <cfset returnStruct[ "backgroundColor" ] = "##3a87ad" />
				        <cfset returnStruct[ "borderColor" ] = "##0000aa" />

			        </cfdefaultcase>

		        </cfswitch>
		        <cfset ArrayAppend(dataout,returnStruct) />
		    </cfloop>

        <cfcatch type="Any" >
			<cfset dataOut = {}>
			<cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.RECORDCOUNT = 0 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

	<cffunction name="SaveEvent" access="remote" output="false" hint="Save changes to an event to the calender - pass everything in inpEvent JSON string - create new event if not exists yet">
		<cfargument name="inpCalendarId" type="string" required="no" default=""/>
		<cfargument name="inpEvent" type="string" required="true" hint="The structure of the event"/>

		<cfset var UpdateManualChangeStatusEvent	= '' />
		<cfset var UpdateOutboundQueue	= '' />
		<cfset var CloseSessionReminder	= '' />
		<cfset var RetValGetCalendarConfiguration	= '' />
		<cfset var RetValGetCalendarEventInfo	= '' />
		<cfset var UpdateManualChangeStatusEventResult	= '' />
		<!--- structure inpEvent
						inpTitle required="no" type="string" default="Appointment" hint="Event description - defaults to 'Appointment'"
						inpAllDayFlag required="no" type="string" default="0" hint="If set will ignore start and end times"
						inpStart required="yes" type="string"
						inpEnd required="yes" type="string"
						inpURL required="no" type="string" default="" hint="If specified will link to the URL when the event is clicked"
						inpClassName required="no" type="string" default="" hint="Optional CSS Class to apply to the event"
						inpEditableFlag required="no" type="string" default="10" hint="Event - 1= event is ediatble, 0 event is not editable"
						inpStartEditableFlag required="no" type="string" default="10" hint="Start of Event - 1= event is ediatble, 0 event is not editable"
						inpDurationEditableFlag required="no" type="string" default="10" hint="Duration of the Event - 1= event is ediatble, 0 event is not editable"
						inpResourceEditableFlag required="no" type="string" default="10" hint="Resources assigned tot he event - 1= event is ediatble, 0 event is not editable"
						inpRendering required="no" default="" hint="Allows alternate rendering of the event, like background events. Can be empty, 'background', or 'inverse-background' default is empty"
						inpOverlapFlag required="no" type="string" default="0" hint="If 0, prevents this event from being dragged/resized over other events. Also prevents other events from being dragged/resized over this event."
						inpConstraint required="no" type="string" default="" hint="an event ID, 'businessHours', object. Optional"
						inpColor required="no" type="string" default="" hint="Optional Color to apply to the event"
						inpBackgroundColor required="no" type="string" default="" hint="Optional Color to apply to the event"
						inpBorderColor required="no" type="string" default="" hint="Optional Color to apply to the event"
						inpTextColor required="no" type="string" default="" hint="Optional Color to apply to the event"
						inpSMSNumber required="no" hint="SMS Phone Number to notify / Remind / Change event"
						inpeMail required="no" hint="eMail address to notify / Remind / Change event"
						inpNotes required="no" type="string" default="" hint=""
						inpConfirmationFlag required="no" type="string" default="0" hint="Flag to indicated confirmation / reminder message state"
		--->
		<cfset var dataout = {} />
		<cfset var UpdateEventQuery = '' />
		<cfset var RetValAddEventReminderToQueue = '' />
		<cfset var CreateSchedule = ''/>
		<cfset var CheckScheduleOptionExist = ''/>
		<cfset var CheckEventInvalid = ''/>
		<cfset var GetTZInfo = ''/>

		<cfset var START_DT = Now()>
		<cfset var STOP_DT = DateAdd('yyyy', 100, Now())>
		<cfset var STARTHOUR_TI = 9>
		<cfset var ENDHOUR_TI = 21>
		<cfset var SUNDAY_TI = false>
		<cfset var MONDAY_TI = true>
		<cfset var TUESDAY_TI = true>
		<cfset var WEDNESDAY_TI = true>
		<cfset var THURSDAY_TI = true>
		<cfset var FRIDAY_TI = true>
		<cfset var SATURDAY_TI = false>
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI = 0>

		<cfset var inpSkipNumberValidations = 0/>
		<cfset var inpTimeZone = ''/>
		<cfset var CurrNPA = ''/>
		<cfset var CurrXXX = ''/>
		<cfset var CurrTZ = ''/>

		<cfset var AddEventQuery	= '' />
		<cfset var insertResult	= '' />

		<cftry>


			<!--- Require Session? Validate session is still active and user is logged in and owns this calendar--->


			<cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(inpEvent)) GT 0 >
				<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
			<cfelse>
				<cfthrow message="Invalid Event Specified on Save"/>
			</cfif>

			<cfif !IsNumeric(arguments.inpEvent.id) AND TRIM(arguments.inpEvent.id) NEQ "">
				<cfthrow message="Invalid Event Id Specified on Save"/>
			</cfif>

			<!--- Set default return values --->
			<cfset dataout.RXRESULTCODE = "-1" />
			<cfset dataout.TYPE = "" />
			<cfset dataout.EVENTID = arguments.inpEvent.id />
			<cfset dataout.MESSAGE = "Update event successfully." />
			<cfset dataout.ERRMESSAGE = "" />

			<cfset arguments.inpEvent.inpStart = "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#">
			<cfset arguments.inpEvent.inpEnd = "#DateFormat(arguments.inpEvent.inpEnd,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpEnd,'HH:mm:ss')#">

			<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
				<cfinvokeargument name="inpCalendarId" value="#inpCalendarId#">
			</cfinvoke>




			<!--- 	Remove Event - More bad logic - dont remove an already sent event reminder  --->
				<cfif arguments.inpEvent.inpConfirmationOneDTSId GT 0 >
					<!--- Remove any queue reminders --->
					<cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
						UPDATE
							simplequeue.contactqueue
						SET
							DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#"/>
						WHERE
							DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#"/>
						AND
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					</cfquery>
					<!--- Close session any queue reminders --->
					<cfquery name="CloseSessionReminder" datasource="#Session.DBSourceEBM#" >
						UPDATE
							simplequeue.sessionire
						SET
							SessionState_int = 4
						WHERE
							DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#"/>
						AND
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					</cfquery>
					<cfset arguments.inpEvent.inpConfirmationOneDTSId = 0/>
				</cfif>



			<!--- 	Addnew Event again --->

				<cfset arguments.inpEvent.inpStart = "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#">
				<cfset arguments.inpEvent.inpEnd = "#DateFormat(arguments.inpEvent.inpEnd,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpEnd,'HH:mm:ss')#">

				<!--- Check scheduleoptions Valid--->
				<!--- Read critical configuration parameter from DB - not as user passed in variables - little slower vs less likely to get hacked --->
				<cfif inpCalendarId EQ "">
					<cfset arguments.inpCalendarId = RetValGetCalendarConfiguration.ID>
				</cfif>

				<cfif RetValGetCalendarConfiguration.BATCHID GT 0 >
					<cfquery name="CheckScheduleOptionExist" datasource="#Session.DBSourceEBM#">
						SELECT
							BatchId_bi
						FROM
							simpleobjects.scheduleoptions
						WHERE
							BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#" />
						AND
							Enabled_ti > 0
					</cfquery>
					<cfif CheckScheduleOptionExist.RecordCount LT 1>
							<cfset START_DT = Now()>
							<cfset STOP_DT = DateAdd('yyyy', 100, Now())>
							<cfset STARTHOUR_TI = 9>
							<cfset ENDHOUR_TI = 21>
							<cfset SUNDAY_TI = true>
							<cfset MONDAY_TI = true>
							<cfset TUESDAY_TI = true>
							<cfset WEDNESDAY_TI = true>
							<cfset THURSDAY_TI = true>
							<cfset FRIDAY_TI = true>
							<cfset SATURDAY_TI = true>
							<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = 0>

						<cfquery name="CreateSchedule" datasource="#Session.DBSourceEBM#">
							INSERT INTO simpleobjects.scheduleoptions
							(
								BatchId_bi,
								STARTHOUR_TI,
								ENDHOUR_TI,
								SUNDAY_TI,
								MONDAY_TI,
								TUESDAY_TI,
								WEDNESDAY_TI,
								THURSDAY_TI,
								FRIDAY_TI,
								SATURDAY_TI,
								LOOPLIMIT_INT,
								<cfif STOP_DT NEQ 'null'>
									STOP_DT,
								</cfif>
								<cfif START_DT NEQ 'null'>
									START_DT,
								</cfif>
								STARTMINUTE_TI,
								ENDMINUTE_TI,
								BLACKOUTSTARTHOUR_TI,
								BLACKOUTENDHOUR_TI,
								LASTUPDATED_DT,
								DYNAMICSCHEDULEDAYOFWEEK_TI,
								ENABLED_TI
							)
							VALUES
							(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(RetValGetCalendarConfiguration.BATCHID)#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#STARTHOUR_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ENDHOUR_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SUNDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#MONDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#TUESDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#WEDNESDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#THURSDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#FRIDAY_TI#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SATURDAY_TI#">,
								100,
								<cfif STOP_DT NEQ 'null'>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#STOP_DT#">,
								</cfif>
								<cfif START_DT NEQ 'null'>
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#START_DT#">,
								</cfif>
								0,
								0,
								0,
								0,
								NOW(),
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DYNAMICSCHEDULEDAYOFWEEK_TI#">,
								1
							)
						</cfquery>
					</cfif>

				</cfif>

				<!---Find and replace all non numerics except P X * #--->
				<cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>
				<cfinvoke method="GetCalendarEventInfo" returnvariable="RetValGetCalendarEventInfo">
					<cfinvokeargument name="inpEventId" value="#inpEvent.id#">
				</cfinvoke>

				<cfif !ISNULL(arguments.inpEvent.inpConfirmationFlag)>
					<!--- WTH?!?  <cfif arguments.inpEvent.inpConfirmationFlag EQ APPT_REMINDER_QUEUED>
						<cfset arguments.inpEvent.inpConfirmationFlag = APPT_REMINDER_DO_NOT_SEND>
					</cfif> --->
				<cfelse>
					<cfset arguments.inpEvent.inpConfirmationFlag = RetValGetCalendarEventInfo.EVENTSTATUS>
				</cfif>


				<cfif arguments.inpEvent.id EQ ''>
					<!---Find and replace all non numerics except P X * #--->
					<cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>

					<!--- always get latest entry for operator Id regardless of whose list it is on --->
					<cfquery name="AddEventQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
					   INSERT INTO
							   calendar.events
						   (
							   PKId_bi,
							   UserId_int,
							   CalendarId_int,
							   Title_vch,
							   AllDayFlag_int,
							   Start_dt,
							   End_dt,
							   URL_vch,
							   ClassName_vch,
							   EditableFlag_int,
							   StartEditableFlag_int,
							   DurationEditableFlag_int,
							   ResourceEditableFlag_int,
							   Rendering_vch,
							   OverlapFlag_int,
							   Constraint_vch,
							   Color_vch,
							   BackgroundColor_vch,
							   BorderColor_vch,
							   TextColor_vch,
							   eMailAddress_vch,
							   SMSNumber_vch,
							   Created_dt,
							   Updated_dt,
							   Notes_vch,
							   ConfirmationFlag_int,
							   ConfirmationOneDTSId_int,
							   Active_int
						    )
					   VALUES
						   (
							   NULL,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpTitle, 255)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpAllDayFlag#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpStart#" />,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpEnd#" null="#not(len(inpEvent.inpEnd))#" />,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpURL, 2048)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpClassName, 255)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpEditableFlag#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpStartEditableFlag#">,
								   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpDurationEditableFlag#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpResourceEditableFlag#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpRendering, 25)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpOverlapFlag#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpConstraint#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpColor, 25)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpBackgroundColor, 25)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpBorderColor, 25)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpTextColor, 25)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpeMail, 255)#">,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpEvent.inpSMSNumber, 45)#">,
							   NOW(),
							   NOW(),
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEvent.inpNotes#">,
							   6,
							   <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpConfirmationOneDTSId#">,
							   1
						    )
					</cfquery>

					<cfif insertResult.GENERATED_KEY GT 0>
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = 'Create Event successfully.'>

						<cfset dataout.EVENTID = insertResult.GENERATED_KEY />
						<cfset arguments.inpEvent.id = insertResult.GENERATED_KEY />

					<cfelse>
						<cfthrow message='Create Calendar Event failed. Please try again later'/>
					</cfif>

				<cfelse>

					<!--- always get latest entry for operator Id regardless of whose list it is on --->
					<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							CalendarId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#">,
							Title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpTitle, 255)#">,
							AllDayFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpAllDayFlag#">,
							Start_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEvent.inpStart#" null="#not(len(trim(arguments.inpEvent.inpStart)))#"/>,
							End_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEvent.inpEnd#" null="#not(len(trim(arguments.inpEvent.inpEnd)))#" />,
							URL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpURL, 2048)#">,
							ClassName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpClassName, 255)#">,
							EditableFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpEditableFlag#">,
							StartEditableFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpStartEditableFlag#">,
							DurationEditableFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpDurationEditableFlag#">,
							ResourceEditableFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpResourceEditableFlag#">,
							Rendering_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpRendering, 25)#">,
							OverlapFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpOverlapFlag#">,
							Constraint_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEvent.inpConstraint#">,
							Color_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpColor, 25)#">,
							BackgroundColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpBackgroundColor, 25)#">,
							BorderColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpBorderColor, 25)#">,
							TextColor_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpTextColor, 25)#">,
							eMailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpeMail, 255)#">,
							SMSNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.inpEvent.inpSMSNumber, 45)#">,
							Updated_dt = NOW(),
							Notes_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEvent.inpNotes#">,
							ConfirmationFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationFlag#">,
							ConfirmationOneDTSId_int = 0
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
						AND
							UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					</cfquery>

				</cfif>

				<!--- <cfset dataout.WTF2 = "RetValGetCalendarConfiguration.ASR= #RetValGetCalendarConfiguration.ASR# LEN(TRIM(arguments.inpEvent.inpSMSNumber))=#LEN(TRIM(arguments.inpEvent.inpSMSNumber))# arguments.inpEvent.inpConfirmationFlag = #arguments.inpEvent.inpConfirmationFlag#" /> --->

				<cfif RetValGetCalendarConfiguration.ASR GT 0 AND LEN(TRIM(arguments.inpEvent.inpSMSNumber)) GTE 10 AND arguments.inpEvent.inpConfirmationFlag NEQ 6 AND RetValGetCalendarConfiguration.BATCHID GT 0>

					<!--- Check phone number US --->
					<!--- <cfif NOT isvalid('telephone', arguments.inpEvent.inpSMSNumber)>
						<cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number !'>
						<cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number !'>
						<cfreturn dataout>
					</cfif> --->

					<cfinvoke method="AddEventReminderToQueue" returnvariable="RetValAddEventReminderToQueue">
						<cfinvokeargument name="inpCalendarId" value="#inpCalendarId#">
						<cfinvokeargument name="inpEvent" value="#arguments.inpEvent#">
					</cfinvoke>

					<cfset dataout.WTF = "inpEvent ID=#arguments.inpEvent.ID# RetValAddEventReminderToQueue=#SerializeJSON(RetValAddEventReminderToQueue)#" />


					<cfif RetValAddEventReminderToQueue.RXRESULTCODE EQ -2>
						<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
							UPDATE
								calendar.events
							SET
								ConfirmationFlag_int = 6
							WHERE
								PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
							AND
								UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
						</cfquery>
						<cfset dataout.RXRESULTCODE = "-1" />
						<cfset dataout.TYPE = "" />
						<cfset dataout.ERRMESSAGE = "#RetValAddEventReminderToQueue.ERRMESSAGE#" />
						<cfreturn dataout />
					<cfelse>
						<cfif RetValAddEventReminderToQueue.DISTRIBUTIONID GT 0>
							<cfset arguments.inpEvent.inpConfirmationOneDTSId = RetValAddEventReminderToQueue.DISTRIBUTIONID />
							<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
								UPDATE
									calendar.events
								SET
									ConfirmationFlag_int = 1,
									ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#">
								WHERE
									PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
								AND
									UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
							</cfquery>
						</cfif>
						<cfset dataout.ERRMESSAGE = "#SerializeJSON(RetValAddEventReminderToQueue)#" />
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = 'Update event successfully.'>
						<cfset dataout.TYPE = "" />
					</cfif>
	            </cfif>
			<!---  End Action  --->


        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="DeleteEvent" access="remote" output="false" hint="Remove an event from the calender">
        <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpEvent" type="string" required="true" hint="The structure of the event"/>

		<!--- structure inpEvent
				inpTitle required="no" type="string" default="Appointment" hint="Event description - defaults to 'Appointment'"
				inpAllDayFlag required="no" type="string" default="0" hint="If set will ignore start and end times"
				inpStart required="yes" type="string"
				inpEnd required="yes" type="string"
				inpURL required="no" type="string" default="" hint="If specified will link to the URL when the event is clicked"
				inpClassName required="no" type="string" default="" hint="Optional CSS Class to apply to the event"
				inpEditableFlag required="no" type="string" default="10" hint="Event - 1= event is ediatble, 0 event is not editable"
				inpStartEditableFlag required="no" type="string" default="10" hint="Start of Event - 1= event is ediatble, 0 event is not editable"
				inpDurationEditableFlag required="no" type="string" default="10" hint="Duration of the Event - 1= event is ediatble, 0 event is not editable"
				inpResourceEditableFlag required="no" type="string" default="10" hint="Resources assigned tot he event - 1= event is ediatble, 0 event is not editable"
				inpRendering required="no" default="" hint="Allows alternate rendering of the event, like background events. Can be empty, 'background', or 'inverse-background' default is empty"
				inpOverlapFlag required="no" type="string" default="0" hint="If 0, prevents this event from being dragged/resized over other events. Also prevents other events from being dragged/resized over this event."
				inpConstraint required="no" type="string" default="" hint="an event ID, 'businessHours', object. Optional"
				inpColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpBackgroundColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpBorderColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpTextColor required="no" type="string" default="" hint="Optional Color to apply to the event"
				inpSMSNumber required="no" hint="SMS Phone Number to notify / Remind / Change event"
				inpeMail required="no" hint="eMail address to notify / Remind / Change event"
				inpNotes required="no" type="string" default="" hint=""
				inpConfirmationFlag required="no" type="string" default="0" hint="Flag to indicated confirmation / reminder message state"
		--->
		<cfset var CloseSessionReminder	= '' />
        <cfset var dataout = {} />
		<cfset var UpdateEventQuery = '' />
		<cfset var UpdateOutboundQueue = '' />
		<cfset var CloseSessionChat = ''>
		<cfset var GetEventInfoQuery = ''>
		<cfset var RetValGetCalendarConfiguration = {} />

        <cftry>

	        <cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(inpEvent)) GT 0 >
				<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
			<cfelse>
				<cfthrow message="Invalid Event Specified"/>
	        </cfif>

        	<cfif !IsNumeric(inpEvent.id)>
            	<cfthrow message="Invalid Event Specified"/>
            </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "#inpEvent.id#" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

			<!--- get Calendar config --->
			<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration"></cfinvoke>

			<!--- get event infor --->
			<cfquery name="GetEventInfoQuery" datasource="#Session.DBSourceEBM#">
               	SELECT
				   	PKId_bi,
					UserId_int,
					CalendarId_int,
					Title_vch,
					Start_dt,
					End_dt,
					SMSNumber_vch,
					Active_int,
					ChatSessionId_bi,
					ConfirmationFlag_int,
					ConfirmationOneDTSId_int
				FROM
               		calendar.events
                WHERE
	            	PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEvent.id#">
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					Active_int = 1
            </cfquery>
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.events
			   	SET
			   		Active_int = 0
                WHERE
	            	PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEvent.id#">
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					Active_int = 1
            </cfquery>

            <cfif inpEvent.inpConfirmationOneDTSId GT 0 >
	            <!--- Remove any queue reminders --->
	            <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
	                UPDATE
	                    simplequeue.contactqueue
	                SET
	                    DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#"/>
	                    <!--- Scheduled_dt = Scheduled_dt + INTERVAL 1000 YEAR <!--- Set a scheduled date wayyyyyy in the future - this will allow new events to be schedule in this slot. Use 1000 years by convention, so you can still see when an original was scheduled for debugging---> --->
	                WHERE
	                	DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpConfirmationOneDTSId#"/>
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
	            </cfquery>
				<!--- Close session any queue reminders --->
				<cfquery name="CloseSessionReminder" datasource="#Session.DBSourceEBM#" >
					UPDATE
						simplequeue.sessionire
					SET
						SessionState_int = 4
					WHERE
						DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEvent.inpConfirmationOneDTSId#"/>
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
				</cfquery>
            </cfif>

			<cfquery name="CloseSessionChat" datasource="#Session.DBSourceEBM#" >
				UPDATE
					simplequeue.sessionire
				SET
					SessionState_int = 4
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHCHATID#">
				AND
					SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventInfoQuery.ChatSessionId_bi#">
				AND
					SessionState_int = 1
			</cfquery>

        	<cfset dataout.MESSAGE = 'Delete Calendar Event OK.'>
            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="EventConfirmation" access="remote" output="false" hint="Confirm an event on the calender">
        <cfargument name="inpEventId" type="string" required="true" hint="The Id of the event"/>
        <cfargument name="inpConfirmationFlag" type="string" required="true" hint="0 - No reminder sent, 1 - Reminder Sent, 2 - Reminder Accepted, 3 - Reminder Declined, 4 - Reminder Change Request, 5 - Reminder Error - Bad SMS, eMail or other error, 6 - Dont send Reminder"/>

        <cfset var dataout = {} />
		<cfset var UpdateEventQuery = '' />

        <cftry>

        	<cfif !IsNumeric(inpEventId)>
            	<cfthrow message="Invalid Event Specified for Confirmation"/>
            </cfif>

        	<!---
					ConfirmationFlag States

					0 - No reminder sent
					1 - Reminder in Queue
					2 - Reminder Accepted
					3 - Reminder Declined
					4 - Reminder Change Request
					5 - Reminder Error - Bad SMS, eMail or other error
					6 - Dont send Reminder
					7 - Reminder Sent
					8 - Chat Active
					9 - Opt out
			--->

        	<cfif !IsNumeric(inpConfirmationFlag)>
            	<cfthrow message="Invalid Confirmation Value Specified"/>
            </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "#inpEvent.id#" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.events
			   	SET
			   		Active_int = 0
                WHERE
	            	PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEvent.id#">
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Confirmation Response on Calendar Event Set OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<!---   Calendar config   --->
	<cffunction name="GetCalendarConfiguration" access="public" output="false" hint="Get the configuration options for the specified calendar">
		<!--- <cfargument name="inpCalendarId" type="string" required="no" default=""/> --->

		<cfset var dataout = {} />
		<cfset var GetConfigQuery= '' />
		<cfset var IntervalStr = ""/>
		<cfset var IntervalArray =[]/>
		<cfset var IntervalResendStr = ""/>
		<cfset var IntervalResendArray =[]/>
		<cfset var indexstr = ''/>
		<cfset var itemstr = ''/>
		<cfset var ScheduleByBatchId = '' />
		<cfset var ROW = ''/>
		<cfset var RetValSaveConfigReminderSchedule = '' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.PHONEFORMAT = "1"/>
		<cfset dataout.BATCHID = "0"/>
		<cfset dataout.BATCHCHATID = "0"/>
		<cfset dataout.ASR = "0"/>
		<cfset dataout.SCROLLTIME = "09:00:00"/>
		<cfset dataout.INTERVALONE = "INTERVAL 2 HOUR"/>
		<cfset dataout.INTERVALRESEND = ""/>
		<cfset dataout.DEFAULEVENTTTITLE = ""/>
		<cfset dataout.INTERVALTYPE = "HOUR"/>
		<cfset dataout.INTERVALVALUE = "1"/>
		<cfset dataout.INTERVALRESENDTYPE = "HOUR"/>
		<cfset dataout.INTERVALRESENDVALUE = "4"/>
		<cfset dataout.FILTERCALENDAR = ""/>
		<cfset dataout.NOTIFIMETHOD = ""/>
		<cfset dataout.NOTIFIITEM = ""/>
		<cfset dataout.TIMEZONE = "0"/>
		<cfset dataout.BSTART = "9"/>
		<cfset dataout.BEND = "21"/>

		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<cfquery name="GetConfigQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                	PKId_bi,
                    PhoneNumberFormat_int,
                    Confirmation_BatchId_bi,
                    ScrollTime_vch,
                    AutoSendReminder_int,
				TimeZone_int,
                    ReminderIntervalOne_vch,
                    DefaultEventTitle_vch,
				ReminderIntervalResend_vch,
				FilterConfirmationFlag_vch,
				NotificationMethod_int,
				NotificationItem_vch,
				Chat_BatchId_bi
                FROM
                    calendar.configuration
                WHERE
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        </cfquery>

	  		<cfif GetConfigQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.ID = #GetConfigQuery.PKId_bi#/>
				<cfset dataout.PHONEFORMAT = "#GetConfigQuery.PhoneNumberFormat_int#"/>
				<cfset dataout.BATCHID = "#GetConfigQuery.Confirmation_BatchId_bi#"/>
				<cfset dataout.BATCHCHATID = "#GetConfigQuery.Chat_BatchId_bi#"/>
				<cfset dataout.SCROLLTIME = "#GetConfigQuery.ScrollTime_vch#"/>
				<cfset dataout.ASR = "#GetConfigQuery.AutoSendReminder_int#"/>
				<cfset dataout.INTERVALONE = "#GetConfigQuery.ReminderIntervalOne_vch#"/>
				<cfset dataout.FILTERCALENDAR = "#GetConfigQuery.FilterConfirmationFlag_vch#"/>
				<cfset dataout.NOTIFIMETHOD = "#GetConfigQuery.NotificationMethod_int#"/>
				<cfset dataout.TIMEZONE = "#GetConfigQuery.TimeZone_int#"/>

				<cfif GetConfigQuery.TimeZone_int EQ 0 OR GetConfigQuery.TimeZone_int EQ '' >
					<cfset dataout.TIMEZONE = 31 />
				</cfif>

				<cfset IntervalStr = #GetConfigQuery.ReminderIntervalOne_vch#>
				<cfset IntervalArray = IntervalStr.Split(" ")/>
				<cfloop array="#IntervalArray#" item="itemstr" index="indexstr">
					<cfif indexstr EQ 2>
						<cfset dataout.INTERVALVALUE = "#itemstr#"/>
					<cfelseif indexstr EQ 3>
						<cfset dataout.INTERVALTYPE = "#itemstr#"/>
					</cfif>
				</cfloop>

				<cfset dataout.INTERVALRESEND = "#GetConfigQuery.ReminderIntervalResend_vch#"/>
				<cfset IntervalResendStr = #GetConfigQuery.ReminderIntervalResend_vch#>
				<cfset IntervalResendArray = IntervalResendStr.Split(" ")/>
				<cfloop array="#IntervalResendArray#" item="itemstr" index="indexstr">
					<cfif indexstr EQ 2>
						<cfset dataout.INTERVALRESENDVALUE = "#itemstr#"/>
					<cfelseif indexstr EQ 3>
						<cfset dataout.INTERVALRESENDTYPE = "#itemstr#"/>
					</cfif>
				</cfloop>

				<cfset dataout.DEFAULEVENTTTITLE = "#GetConfigQuery.DefaultEventTitle_vch#"/>

				<!--- Read in Schedule if it exists --->
				<cfif dataout.BATCHID GT 0>

					<cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
						<cfinvokeargument name="INPBATCHID" value="#dataout.BATCHID#">
					</cfinvoke>

					<cfif ScheduleByBatchId.RXRESULTCODE EQ 1>

						<cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">
							<cfset dataout.BSTART = ROW.STARTHOUR_TI/>
							<cfset dataout.BEND = ROW.ENDHOUR_TI/>
						</cfloop>

					<cfelse>
						<!--- Auto repair - create schedule --->
						<cfinvoke component="session.sire.models.cfc.calendar" method="SaveConfigReminderSchedule" returnvariable="RetValSaveConfigReminderSchedule">
							<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
							<cfinvokeargument name="inpStartHour" value="#dataout.BSTART#">
							<cfinvokeargument name="inpEndHour" value="#dataout.BEND#">
						</cfinvoke>

					</cfif>

				<cfelse>

				</cfif>

			<cfelse>

				<!--- Bullet proof Self repair --->

			</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="SetCalendarConfigurationDefault" access="public" output="false" hint="Set Calendar config default for the specified calendar">
		<cfargument name="inpUserId" type="string" required="no" default="#session.USERID#"/>

		<cfset var dataout = {} />
		<cfset var GetConfigQuery= '' />
		<cfset var GetAppointmentTemplate = ''/>
		<cfset var GetAppointmentChatTemplate = ''/>
		<cfset var CreateAppointmentBatchResult = ''/>
		<cfset var CreateAppointmentBatchChatResult = ''/>
		<cfset var InsertCalendarConfigUserResult = ''/>

		<cfset var XMLControlStringCalendar = ''/>
		<cfset var XMLControlStringCalendarChat = ''/>
		<cfset var InsertBatch = {}/>
		<cfset var UpdateConfigQuery = {}/>
		<cfset var shortCode = ''/>
		<cfset var UpdateEventCalendar = ''/>
		<cfset var GetBatchIdValid = ''/>
		<cfset var GetBatchIdChatValid = ''/>
		<cfset var UpdateEventConfig = ''/>
		<cfset var CheckScheduleOptionExist =''/>
		<cfset var CreateSchedule = ''/>

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.PHONEFORMAT = "1"/>
		<cfset dataout.BATCHID = "0"/>
		<cfset dataout.ASR = "0"/>
		<cfset dataout.SCROLLTIME = "09:00:00"/>
		<cfset dataout.INTERVALONE = "INTERVAL 2 HOUR"/>
		<cfset dataout.DEFAULEVENTTTITLE = ""/>
		<cfset dataout.CALENDARID = ""/>
		<cfset dataout.NOTIFIMETHOD = ""/>
		<cfset dataout.NOTIFIITEM = ""/>
		<cfset dataout.CHATBATCHID = ""/>
		<cfset dataout.STARTHOURS = 9/>
		<cfset dataout.ENDHOURS = 21/>

		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var START_DT = Now()>
		<cfset var STOP_DT = DateAdd('yyyy', 100, Now())>
		<cfset var STARTHOUR_TI = 9>
		<cfset var ENDHOUR_TI = 21>
		<cfset var SUNDAY_TI = true>
		<cfset var MONDAY_TI = true>
		<cfset var TUESDAY_TI = true>
		<cfset var WEDNESDAY_TI = true>
		<cfset var THURSDAY_TI = true>
		<cfset var FRIDAY_TI = true>
		<cfset var SATURDAY_TI = true>
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI = 0>

		<cftry>

			<cfquery name="GetConfigQuery" datasource="#Session.DBSourceEBM#">
                SELECT
				PKId_bi,
				CalendarId_int,
				PhoneNumberFormat_int,
				Confirmation_BatchId_bi,
				ScrollTime_vch,
				AutoSendReminder_int,
				ReminderIntervalOne_vch,
				DefaultEventTitle_vch,
				NotificationMethod_int,
				NotificationItem_vch,
				Chat_BatchId_bi
                FROM
                    calendar.configuration
                WHERE
	           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        </cfquery>

	  		<cfif GetConfigQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.CALENDARID = "#GetConfigQuery.CalendarId_int#"/>
				<cfset dataout.PHONEFORMAT = "#GetConfigQuery.PhoneNumberFormat_int#"/>
				<cfset dataout.SCROLLTIME = "#GetConfigQuery.ScrollTime_vch#"/>
				<cfset dataout.ASR = "#GetConfigQuery.AutoSendReminder_int#"/>
				<cfset dataout.INTERVALONE = "#GetConfigQuery.ReminderIntervalOne_vch#"/>
				<cfset dataout.DEFAULEVENTTTITLE = "#GetConfigQuery.DefaultEventTitle_vch#"/>
				<cfset dataout.NOTIFIMETHOD = "#GetConfigQuery.NotificationMethod_int#"/>
				<cfset dataout.NOTIFIITEM = "#GetConfigQuery.NotificationItem_vch#"/>

				<!--- Check Chat BatchId valid --->
				<cfquery name="GetBatchIdChatValid" datasource="#Session.DBSourceEBM#">
					SELECT
						BatchId_bi
					FROM
						simpleobjects.batch
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetConfigQuery.Chat_BatchId_bi#">
					AND
						Active_int = 1
				</cfquery>

				<cfif GetBatchIdChatValid.RecordCount GT 0>
					<cfset dataout.CHATBATCHID = "#GetConfigQuery.Chat_BatchId_bi#"/>
				<cfelse>
					<!--- 		Create campaign chat for user calendar		 --->
					<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
					<!---  SHORTCODEID, SHORTCODE--->

					<cfquery name="GetAppointmentChatTemplate" datasource="#Session.DBSourceEBM#">
						SELECT
							XMLControlString_vch
						FROM
							simpleobjects.templatecampaign
						WHERE
							Name_vch = 'Appointment_Chat'
					</cfquery>

					<cfif GetAppointmentChatTemplate.RecordCount GT 0>
						<cfset XMLControlStringCalendarChat = GetAppointmentChatTemplate.XMLControlString_vch/>
					</cfif>

					<cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="CreateAppointmentBatchChatResult">
						INSERT INTO simpleobjects.batch
						(
							UserId_int,
							rxdsLibrary_int,
							rxdsElement_int,
							rxdsScript_int,
							UserBatchNumber_int,
							Created_dt,
							Desc_vch,
							LastUpdated_dt,
							GroupId_int,
							XMLControlString_vch,
							Active_int,
							ContactGroupId_int,
							ContactTypes_vch,
							TemplateType_ti,
							EMS_Flag_int,
							RealTimeFlag_int,
							TemplateId_int,
							ShortCodeId_int
						)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							0,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Appointment Calendar Chat">,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlStringCalendarChat#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="20">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#">
						)
					</cfquery>
					<cfset dataout.CHATBATCHID = "#CreateAppointmentBatchChatResult.generated_key#"/>

					<cfquery name="UpdateEventConfig" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.configuration
						SET
							Chat_BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CreateAppointmentBatchChatResult.generated_key#" />
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					</cfquery>
				</cfif>
			<cfelse>
		<!--- 		Create campaign for user calendar		 --->
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
				<!---  SHORTCODEID, SHORTCODE--->

					<cfquery name="GetAppointmentChatTemplate" datasource="#Session.DBSourceEBM#">
						SELECT
							XMLControlString_vch
						FROM
							simpleobjects.templatecampaign
						WHERE
							Name_vch = 'Appointment_Chat'
					</cfquery>

					<cfif GetAppointmentChatTemplate.RecordCount GT 0>
						<cfset XMLControlStringCalendarChat = GetAppointmentChatTemplate.XMLControlString_vch/>
					</cfif>

					<cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="CreateAppointmentBatchChatResult">
						INSERT INTO simpleobjects.batch
						(
							UserId_int,
							rxdsLibrary_int,
							rxdsElement_int,
							rxdsScript_int,
							UserBatchNumber_int,
							Created_dt,
							Desc_vch,
							LastUpdated_dt,
							GroupId_int,
							XMLControlString_vch,
							Active_int,
							ContactGroupId_int,
							ContactTypes_vch,
							TemplateType_ti,
							EMS_Flag_int,
							RealTimeFlag_int,
							TemplateId_int,
							ShortCodeId_int
						)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							0,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Appointment Calendar Chat">,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlStringCalendarChat#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="20">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#">
						)
					</cfquery>
					<cfset dataout.CHATBATCHID = "#CreateAppointmentBatchChatResult.generated_key#"/>

					<cfquery name="UpdateEventConfig" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.configuration
						SET
							Chat_BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CreateAppointmentBatchChatResult.generated_key#" />
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					</cfquery>

				<!--- 		Create Congif for user calendar		 --->
				<cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="InsertCalendarConfigUserResult">
					INSERT INTO calendar.configuration
					(
						CalendarId_int,
						UserId_int,
						PhoneNumberFormat_int,
						Confirmation_BatchId_bi,
						ScrollTime_vch,
						AutoSendReminder_int,
						ReminderIntervalOne_vch,
						DefaultEventTitle_vch,
						Chat_BatchId_bi
					)
					VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="09:00:00">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="INTERVAL 2 HOUR">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Special Event">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CreateAppointmentBatchChatResult.generated_key#">
					)
				</cfquery>

				<cfif InsertCalendarConfigUserResult.generated_key GT 0>
					<cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.configuration
						SET
							CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#InsertCalendarConfigUserResult.generated_key#" />
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					</cfquery>

					<cfquery name="UpdateEventCalendar" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#InsertCalendarConfigUserResult.generated_key#" />
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					</cfquery>
				</cfif>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.CALENDARID = "#InsertCalendarConfigUserResult.generated_key#"/>
				<cfset dataout.BATCHID = ""/>
	  		</cfif>



		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetCalendarEventInfo" access="remote" output="false" hint="Get the session chat id for Appointment">
    	<cfargument name="inpEventId" type="string" required="no" default="1" hint="The Id of the event calendar- data_event_obj.id"/>

        <cfset var dataout = {} />
        <cfset var GetEventInfoQuery= '' />
		<cfset var shortCode = ''/>

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.DEFAULEVENTTTITLE = ""/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.CHATSESSIONID = "0"/>
		<cfset dataout.SHORTCODEID = "0"/>
		<cfset dataout.CONTACTSTRING = ""/>
		<cfset dataout.EVENTSTATUS = ""/>
		<cfset dataout.DTSID = ""/>
		<cfset dataout.CONTACTQUEUESTATUS = ""/>

        <cftry>

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<!---  SHORTCODEID, SHORTCODE--->

			<cfquery name="GetEventInfoQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                	ev.PKId_bi,
					ev.UserId_int,
					ev.CalendarId_int,
					ev.Title_vch,
					ev.Start_dt,
					ev.End_dt,
					ev.SMSNumber_vch,
					ev.Active_int,
					ev.ChatSessionId_bi,
					ev.ConfirmationFlag_int,
					ev.ConfirmationOneDTSId_int,
					cq.DTSStatusType_ti
                FROM
                    calendar.events ev
				LEFT JOIN
					simplequeue.contactqueue cq ON ev.ConfirmationOneDTSId_int = cq.DTSId_int
                WHERE
                    ev.PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEventId#" />
	           	AND
	           		ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					ev.Active_int = 1
	        </cfquery>

	  		<cfif GetEventInfoQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.DEFAULEVENTTTITLE = "#GetEventInfoQuery.Title_vch#"/>
				<cfset dataout.SHORTCODEID = "#shortCode.SHORTCODEID#"/>
				<cfset dataout.CHATSESSIONID = "#GetEventInfoQuery.ChatSessionId_bi#"/>
				<cfset dataout.CONTACTSTRING = "#GetEventInfoQuery.SMSNumber_vch#"/>
				<cfset dataout.EVENTSTATUS = "#GetEventInfoQuery.ConfirmationFlag_int#"/>
				<cfset dataout.DTSID = "#GetEventInfoQuery.ConfirmationOneDTSId_int#"/>
				<cfset dataout.CONTACTQUEUESTATUS = "#GetEventInfoQuery.DTSStatusType_ti#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="CheckSessionChatCalendar" access="remote" output="false" hint="Get the session chat id for Appointment">
    	<cfargument name="inpEventId" type="string" required="no" default="1" hint="The Id of the event calendar- data_event_obj.id"/>
    	<cfargument name="inpContactString" required="true" type="string" hint="Customer phone number"/>

        <cfset var dataout = {} />
        <cfset var GetEventInfoQuery= '' />
		<cfset var shortCode = ''/>
		<cfset var checkSession = ''/>

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.DEFAULEVENTTTITLE = ""/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.CHATSESSIONID = "0"/>
		<cfset dataout.SHORTCODEID = "0"/>
		<cfset dataout.CONTACTSTRING = ""/>
		<cfset dataout.EVENTSTATUS = ""/>
		<cfset dataout.DTSID = ""/>
		<cfset dataout.CONTACTQUEUESTATUS = ""/>

        <cftry>

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<!---  SHORTCODEID, SHORTCODE--->

			<cfquery name="GetEventInfoQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					ev.PKId_bi,
					ev.UserId_int,
					ev.CalendarId_int,
					ev.Title_vch,
					ev.Start_dt,
					ev.End_dt,
					ev.SMSNumber_vch,
					ev.Active_int,
					ev.ChatSessionId_bi,
					ev.ConfirmationFlag_int,
					ev.ConfirmationOneDTSId_int,
					cq.DTSStatusType_ti
				FROM
					calendar.events ev
				LEFT JOIN
					simplequeue.contactqueue cq ON ev.ConfirmationOneDTSId_int = cq.DTSId_int
				WHERE
					ev.PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEventId#" />
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					ev.Active_int = 1
			</cfquery>

			<cfif GetEventInfoQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.DEFAULEVENTTTITLE = "#GetEventInfoQuery.Title_vch#"/>
				<cfset dataout.SHORTCODEID = "#shortCode.SHORTCODEID#"/>
				<cfset dataout.CHATSESSIONID = "#GetEventInfoQuery.ChatSessionId_bi#"/>
				<cfset dataout.CONTACTSTRING = "#GetEventInfoQuery.SMSNumber_vch#"/>
				<cfset dataout.EVENTSTATUS = "#GetEventInfoQuery.ConfirmationFlag_int#"/>
				<cfset dataout.DTSID = "#GetEventInfoQuery.ConfirmationOneDTSId_int#"/>
				<cfset dataout.CONTACTQUEUESTATUS = "#GetEventInfoQuery.DTSStatusType_ti#"/>

				<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
					SELECT
						ss.SessionId_bi,
						ss.UserId_int
					FROM
						simplequeue.sessionire ss
					LEFT JOIN
						simpleobjects.batch b
						ON
						ss.BatchId_bi = b.BatchId_bi
					WHERE
					<!---	-- ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
					-- AND --->
						ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
					AND
						ss.SessionState_int = 1
					AND
						ss.CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortCode.SHORTCODE#"/>
					AND
						ss.SessionId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventInfoQuery.ChatSessionId_bi#">
					AND
						b.EMS_Flag_int = 20
					LIMIT 1
				</cfquery>

				<cfif checkSession.RECORDCOUNT GT 0>
					<cfif checkSession.UserId_int EQ #session.USERID#>
						<cfset dataout.AVALABLESESSIONID = checkSession.SessionId_bi/>
						<!--- <cfthrow type="Application" message='You can only have one <a id="end-chat" class="alert-link" data-dismiss="modal">active chat session</a> per subscriber, if you need to create a new session please end the current one.'/> --->
						<cfset dataout.RXRESULTCODE = -3/>
						<cfset dataout.MESSAGE = "There is an active chat session with this subscriber. Do you want to close it to continue?"/>
						<cfset dataout.NEWSESSIONID = checkSession.SessionId_bi/>
						<cfreturn dataout/>
						<!---<cfreturn serializeJSON(dataout)/>--->
					<cfelse>
						<cfset dataout.RXRESULTCODE = -2/>
						<cfset dataout.MESSAGE = "This contact string is available on another chat session. Please end that session before starting new."/>
						<cfset dataout.NEWSESSIONID = 0/>
						<cfreturn dataout/>
						<!---<cfreturn serializeJSON(dataout)/>--->
					</cfif>
				</cfif>
			</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="completeSessionSameContact" access="remote" output="false" hint="Close sms chat session the same phone number">
		<cfargument name="inpContactString" TYPE="string" required="no" default="" />
		<cfargument name="inpUserId" TYPE="numeric" required="no" default="#session.UserId#" />

		<cfset var DataOut	= {} />
		<cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.MESSAGE = "" />
		<cfset DataOut.ERRMESSAGE = "" />

		<cfset var updateCompleteSession = "" />
		<cfset var RetVarCompleteSession = "" />

		<cftry>
			<cfquery name="updateCompleteSession" datasource="#Session.DBSourceEBM#" result="RetVarCompleteSession">
				UPDATE
	                `simplequeue`.`sessionire`
	            SET
	                SessionState_int = 4,
	                LastUpdated_dt = NOW()
	            WHERE
	                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">
				AND
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
				AND
					SessionState_int = 1
			</cfquery>

			<cfset DataOut.RXRESULTCODE = 1 />

		    <cfcatch TYPE="any">

			    <cfset DataOut.RXRESULTCODE = -1 />
	    		<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>

				<cfreturn DataOut>
			</cfcatch>

		</cftry>

		<cfreturn serializeJSON(DataOut)/>

	</cffunction>
	<cffunction name="MakeNewChatSessionForContactStringCalendar" access="remote" hint="Make new chat session with customer">
		<cfargument name="inpContactString" required="true" type="string" hint="Customer phone number"/>
		<cfargument name="inpUserId" required="true" type="numeric" default="#session.UserId#" hint="Sire user id"/>
		<!---<cfargument name="inpKeyword" required="false" type="string" hint="Chat keyword"/>--->
		<cfargument name="inpSkipCheckOPT" required="false" default="0" type="string" hint="check phone number opt in system"/>

		<cfset var shortCode	= '' />
		<cfset var dataout = {} />
		<cfset var makeNewChatSession = ''/>
		<cfset var checkSession = '' />
		<cfset var getBatchIdOfKeyword = '' />
		<cfset var getUserShortCode = '' />
		<cfset var RetVarValidateContactString = '' />
		<cfset var GetChatTemplate = ''/>
		<cfset var XMLControlStringCalendar = ''/>
		<cfset var InsertBatchResult = ''/>
		<cfset var GetConfigQuery = ''/>

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.NEWSESSIONID = 0/>
		<cfset dataout.AVALABLESESSIONID = 0/>

		<cftry>

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<!---  SHORTCODEID, SHORTCODE--->
			<cfquery name="GetConfigQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                	PKId_bi,
					CalendarId_int,
                    PhoneNumberFormat_int,
                    Confirmation_BatchId_bi,
                    ScrollTime_vch,
                    AutoSendReminder_int,
                    ReminderIntervalOne_vch,
                    DefaultEventTitle_vch,
					NotificationMethod_int,
					NotificationItem_vch,
					Chat_BatchId_bi
                FROM
                    calendar.configuration
                WHERE
	           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        </cfquery>

			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
            </cfloop>

			<!--- check session chat old --->
			<!---<cfthrow type="Application" message='You can only have one <a id="end-chat" class="alert-link" data-dismiss="modal">active chat session</a> per subscriber, if you need to create a new session please end the current one.'/>--->

			<cfquery result="makeNewChatSession" datasource="#session.DBSourceEBM#">
				INSERT INTO simplequeue.sessionire
                    (
                        SessionState_int,
                        CSC_vch,
                        ContactString_vch,
                        APIRequestJSON_vch,
                        LastCP_int,
                        XMLResultString_vch,
                        BatchId_bi,
                        UserId_int,
                        Created_dt,
                        LastUpdated_dt,
                        DeliveryReceipt_ti
                    )
                VALUES
                    (
                        1,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#shortCode.SHORTCODE#"/>,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>,
                        '""',
                        1,
                        '',
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#GetConfigQuery.Chat_BatchId_bi#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>,
                        NOW(),
                        NOW(),
                        0
                    )
			</cfquery>

			<cfif makeNewChatSession.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Make new session successfully!"/>
				<cfset dataout.NEWSESSIONID = makeNewChatSession.GENERATEDKEY/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="ValidateContactStringSession" access="public" hint="Check contact string for overlap with other user">
		<cfargument name="inpContactString" required="true" type="string" hint="contact string to check"/>
		<cfargument name="inpBatchId" required="true" type="string" hint="batch id to check"/>
		<cfset var dataout = {} />
		<cfset var getSession = '' />
		<cfset var getUserShortCode = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.ISVALIDATE = 0/>

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>

			<!--- remove old method: SHORTCODE
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="getUserShortCode"></cfinvoke>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getUserShortCode"></cfinvoke>

			<cfquery name="getSession" datasource="#session.DBSourceREAD#">
				SELECT
					s.SessionId_bi,
					s.UserId_int
				FROM
					simplequeue.sessionire s
				WHERE
					s.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					s.SessionState_int = 1
				AND
					s.CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getUserShortCode.SHORTCODE#"/>
				ORDER BY
					s.SessionId_bi DESC
				LIMIT
					1
			</cfquery>

			<cfif getSession.RECORDCOUNT GT 0>
					<cfset dataout.ISVALIDATE = 1/>
			<cfelse>
					<cfset dataout.ISVALIDATE = 1/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1/>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetSessionDetails" access="public" output="false" hint="Get contact string, keyword for chat session">
		<cfargument name="inpSessionId" required="true" type="numeric" hint="Session id" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var getSessionInfo = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cfset dataout.INFO = {} />
		<cfset dataout.INFO.KEYWORD = '' />
		<cfset dataout.INFO.CONTACTSTRING = '' />
		<cfset dataout.INFO.BATCHID = '' />
		<cfset dataout.INFO.SESSIONSTATE = 0/>

		<cftry>
			<cfquery name="getSessionInfo" datasource="#session.DBSourceREAD#">
				SELECT
					s.ContactString_vch,
					s.BatchId_bi,
					s.SessionState_int,
					b.EMS_Flag_int,
					b.TemplateId_int
				FROM
					simplequeue.sessionire s
				INNER JOIN
					simpleobjects.batch b
				ON
					b.BatchId_bi = s.BatchId_bi
				WHERE
					s.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							s.LastCP_int != 3
						)
					)
			</cfquery>

			<cfif getSessionInfo.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.INFO.KEYWORD = ""/>
				<cfset dataout.INFO.CONTACTSTRING = getSessionInfo.ContactString_vch/>
				<cfset dataout.INFO.BATCHID = getSessionInfo.BatchId_bi/>
				<cfset dataout.INFO.SESSIONSTATE = getSessionInfo.SessionState_int/>
				<cfset dataout.INFO.EMSFLAG = getSessionInfo.EMS_Flag_int/>
				<cfset dataout.INFO.TEMPLATEID = getSessionInfo.TemplateId_int/>
				<cfset dataout.MESSAGE = "Get info success"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="SendSMS" access="remote" output="false" hint="Send sms from sire user to user's device">
		<cfargument name="inpSessionId" required="true" type="numeric" hint="Chat session id"/>
		<cfargument name="inpTextToSend" required="true" type="string" hint="Text to send"/>
		<cfargument name="inpSkipPushWS" required="false" default="0"/>
		<cfargument name="inpShortCodeid" required="false" default="0"/>

		<cfset var dataout = {} />
		<cfset var RetVarSendSingleMT = '' />
		<cfset var checkSession = ''/>
		<cfset var shortCode = '' />
		<cfset var getSessionInfo = '' />
		<cfset var validateContactString = ''/>
		<cfset var reg = '' />
		<cfset var pushWS = '' />
		<cfset var RetVarSplit = '' />
		<cfset var text = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<!--- <cfset arguments.inpTextToSend = reReplace(arguments.inpTextToSend, '\n', '<br />', 'ALL')/> --->

			<!--- Check session owner --->
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.UserId_int
				FROM
					simplequeue.sessionire ss
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					ss.SessionState_int = 1
			</cfquery>

			<cfif checkSession.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Session closed! The customer maybe left this conversation!"/>
			</cfif>

			<cfinvoke component="public.sire.models.cfc.shortcode" method="GetShortCodeById" returnvariable="shortCode">
				<cfinvokeargument name="inpShortCodeid" value="#arguments.inpShortCodeid#"/>
			</cfinvoke>

			<!--- Chinhkv --->
			<cfset getSessionInfo = GetSessionDetails(arguments.inpSessionId)/>

			<cfif getSessionInfo.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Session ID not valid!"/>
			</cfif>

			<!--- Validate contact string, to make sure it not on another conversation with different user --->
			<cfset validateContactString = ValidateContactStringSession(getSessionInfo.INFO.CONTACTSTRING, getSessionInfo.INFO.BATCHID)/>

			<!--- Close session if not valid --->
			<cfif validateContactString.ISVALIDATE NEQ 1>

			<!--- 				<cfinvoke method="completeSession"> --->
			<!--- 					<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/> --->
			<!--- 				</cfinvoke> --->
				<cfthrow type="Application" message="Customer is no longer attend this conversation!"/>
			</cfif>

			<!--- Send one single MT --->
			<cfinvoke component="session.cfc.csc.csc" method="SendSingleMT" returnvariable="RetVarSendSingleMT">
				<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#"/>
				<cfinvokeargument name="inpIRESessionId" value="#arguments.inpSessionId#"/>
				<cfinvokeargument name="inpBatchId" value="#getSessionInfo.INFO.BATCHID#"/>
				<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
				<cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/>
			</cfinvoke>

			<cfif RetVarSendSingleMT.PRM NEQ "">
				<cfthrow message="#RetVarSendSingleMT.PRM#"/>
			</cfif>

			<cfif RetVarSendSingleMT.BLOCKEDBYDNC GT 0>
				<cfthrow type="Blocked" message="Message Blocked by Previous DNC request. Until they opt back in you can not message them."/>
			</cfif>

			<cfset dataout.RXRESULTCODE = RetVarSendSingleMT.RXRESULTCODE/>
			<cfset dataout.MESSAGE = RetVarSendSingleMT.MESSAGE/>
			<cfset dataout.ERRMESSAGE = RetVarSendSingleMT.ERRMESSAGE/>

			<cfif dataout.RXRESULTCODE GT 0 AND arguments.inpSkipPushWS EQ 0>
				<cfif len(arguments.inpTextToSend) GT 160>
					<cfinvoke method="SplitMessage" component="session.sire.models.cfc.smschat" returnvariable="RetVarSplit">
					    <cfinvokeargument name="inpText" value="#arguments.inpTextToSend#"/>
					</cfinvoke>
					<cfif RetVarSplit.RXRESULTCODE GT 0>
						<cfloop array="#RetVarSplit.TEXTARRAY#" index="text">
							<cfinvoke component="session.sire.models.cfc.smschat" method="PushWS" returnvariable="pushWS">
								<cfinvokeargument name="inpTextToSend" value="#text#"/>
								<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
								<cfinvokeargument name="inpType" value="1"/>
								<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
							</cfinvoke>
						</cfloop>
					</cfif>
				<cfelse>
					<cfinvoke component="session.sire.models.cfc.smschat" method="PushWS" returnvariable="pushWS">
						<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
						<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
						<cfinvokeargument name="inpType" value="1"/>
						<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
					</cfinvoke>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfif findNoCase("billing" ,dataout.MESSAGE)>
					<cfset dataout.MESSAGE = "You do not have enough credits to send message."/>
				</cfif>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="UpdateEventWithChatSessionID" access="remote" output="false" hint="Update an event on the calender with chatsessionid">
        <cfargument name="inpEventId" type="string" required="true" hint="The Id of the event"/>
        <cfargument name="inpChatSessionId" type="string" required="true" hint="Chatsessionid - simplequeue.sessionire"/>

        <cfset var dataout = {} />
		<cfset var UpdateEventQuery = '' />

        <cftry>
        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.events
			   	SET
			   		ChatSessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpChatSessionId#">
                WHERE
	            	PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEventId#">
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Event With Chat SessionId Successfully.'>

            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="AddEventReminderToQueue" access="public" output="false" hint="Add an event reminder to the queue. Will verify from configuration if it is needed. ">
		<cfargument name="inpCalendarId" type="string" required="no" default=""/>
		<cfargument name="inpEvent" type="struct" required="true" hint="The structure of the event"/>
		<cfargument name="inpResend" type="string" required="no" default="0" hint="Resend or Not"/>
		<cfargument name="inpShortCode" required="false" default="" />

		<cfset var CheckTimeSendEventPastTime	= '' />
		<cfset var UpdateEventQuery	= '' />
		<cfset var dataout = {} />
		<cfset var GetConfigQuery= '' />
		<cfset var GetBatchOwner= '' />
		<cfset var RetValGetCalendarConfiguration= '' />
		<cfset var RetValLoadQueueSMSDynamic= '' />
		<cfset var APIRequestJSON= '' />
		<cfset var inpQueuedScheduledDate = 'NOW()' />
		<cfset var APPTDATE = '' />
		<cfset var APPTTIME = '' />
		<cfset var ScheduleDateBuffer = '' />
		<cfset var UpdateOutboundQueue = '' />
		<cfset var RetValAddEventReminderToQueue = ''/>
		<cfset var GetReminderTodayResult = ''/>
		<cfset var inpContactString = ''/>
		<cfset var GetTimeServer = ''/>
		<cfset var getScheduleOptionInfor = ''/>
		<cfset var StartHourSchedule = 9/>
		<cfset var EndHourSchedule = 21/>
		<cfset var StartHourScheduledis = '9AM'/>
		<cfset var EndHourScheduledis = '9PM'/>
		<cfset var inpQueuedScheduledDate_org = ''>
		<cfset var Currentdatetime = 'NOW()'>
		<cfset var HourCheck = 0/>
		<cfset var HourBuffet = 0/>
		<cfset var GetReminderToday = {}/>
		<cfset var CurrNPA = ''/>
		<cfset var CurrNXX = ''/>
		<cfset var CurrTZ = ''/>
		<cfset var StartHourSchedule_txt = ''/>
		<cfset var EndHourSchedule_txt = ''/>
		<cfset var checkdatecompare = ''/>
		<cfset var UpdateSentOutTime = ''/>
		<cfset var GetTZInfo = '' />
		<cfset var inpXMLControlString = ''/>
		<cfset var RetVarGetAssignedShortCode = '' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.ID = "0"/>
		<cfset dataout.DISTRIBUTIONID = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Read critical configuration parameter from DB - not as user passed in variables - little slower vs less likely to get hacked --->
			<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
				<cfinvokeargument name="inpCalendarId" value="#inpCalendarId#">
			</cfinvoke>

			<cfif inpCalendarId EQ "">
				<cfset arguments.inpCalendarId = RetValGetCalendarConfiguration.ID>
			</cfif>

			<cfset dataout.DEBUGDATA = "#SerializeJSON(RetValGetCalendarConfiguration)#" />

			<!--- Bulletproof: Verify there is actually a reminder campaign defined  --->
			<cfif RetValGetCalendarConfiguration.BATCHID EQ 0 OR  RetValGetCalendarConfiguration.BATCHID EQ ''>

				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.ID = "0"/>
				<cfset dataout.MESSAGE = "There is no campaign configured for this calendar. No reminder has been scheduled."/>
				<cfset dataout.ERRMESSAGE = ""/>

				<cfreturn dataout>
			</cfif>

			<!--- Verify batch is owned by current logged in user --->
			<cfquery name="GetBatchOwner" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TOTALCOUNT
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
				AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			</cfquery>

			<!--- Verify batch is owned by current logged in user - throw error if not the BatchId's owner user Id--->
			<cfif GetBatchOwner.TOTALCOUNT EQ 0>
				<cfthrow message="Security Violation: Current logged in user does not own the campaign they are trying to launch."/>
			</cfif>

			<!--- <cfset arguments.inpEvent.inpStart = "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#"> --->
			<cfset APPTDATE = DateFormat(arguments.inpEvent.inpStart,'mm/dd/yyyy') />

			<!--- Only show hours and minutes here - this is for CDF in message --->
			<cfset APPTTIME =LSTimeFormat(arguments.inpEvent.inpStart, "hh:mm tt") />

			<!--- Build event data into custom JSON --->
			<cfset var APIRequestJSON= "{'APPTDATE':'#APPTDATE#', 'APPTTIME':'#APPTTIME#', 'APPTEVENTID':'#arguments.inpEvent.id#' }" />

			<!--- caculate schedule_dt --->
			<cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>


			<!--- Start initial date as time of appointment --->
			<cfset ScheduleDateBuffer =  "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#" />

			<!--- Compensate for phone numbers that are in different time zones than the calendar Change time by timezone --->
			<!--- All numbers are treated on Schedule (ContactQueue) as if they are in calendar's configuration time zone - RetValGetCalendarConfiguration.TIMEZONE --->
			<cfset ScheduleDateBuffer = "#DateAdd("h",RetValGetCalendarConfiguration.TIMEZONE - 31, ScheduleDateBuffer)#">

			<cfif !ISDATE(ScheduleDateBuffer) >
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "Could not calculate a scheduled reminder time from the provided event start date and time."/>
				<cfset dataout.ERRMESSAGE = ""/>
				<cfreturn dataout>
			</cfif>

			<cfset inpQueuedScheduledDate = "#ScheduleDateBuffer#" />

			<!--- change time by notification configuration option information --->
			<cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ "DAY">
				<cfset inpQueuedScheduledDate = #DATEADD("d", - LSParseNumber(RetValGetCalendarConfiguration.INTERVALVALUE), ScheduleDateBuffer)#>
			<cfelse><!--- Interval type is Hour --->
				<cfset inpQueuedScheduledDate = #DateAdd("h", - LSParseNumber(RetValGetCalendarConfiguration.INTERVALVALUE), ScheduleDateBuffer)#>
			</cfif>

			>
			<!--- ***look for notifications sent out too close to event --->
			<!--- ***Future think - multiple notification options --->

			<!--- Get schedule options information --->
			<cfquery name="getScheduleOptionInfor" datasource="#Session.DBSourceEBM#">
				SELECT
					NOW() AS CURRENTTIME,
					StartHour_ti,
					EndHour_ti
				FROM
					simpleobjects.scheduleoptions
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
			</cfquery>

			<!--- <cfif getScheduleOptionInfor.RecordCount GT 0>
				<cfset StartHourSchedule = getScheduleOptionInfor.StartHour_ti + LSParseNumber(CurrTZ) - 31/>
				<cfif StartHourSchedule LT 10>
					<cfset StartHourSchedule_txt = '0' & StartHourSchedule/>
				<cfelse>
					<cfset StartHourSchedule_txt =  StartHourSchedule/>
				</cfif>

				<cfset EndHourSchedule = getScheduleOptionInfor.EndHour_ti + LSParseNumber(CurrTZ) - 31/>
				<cfif EndHourSchedule LT 10>
					<cfset EndHourSchedule_txt = '0' & EndHourSchedule/>
				<cfelse>
					<cfset EndHourSchedule_txt =  EndHourSchedule/>
				</cfif>
			</cfif>

			<cfset inpQueuedScheduledDate_org = "'#DateFormat(inpQueuedScheduledDate_org,'yyyy-mm-dd')# #LSTimeFormat(inpQueuedScheduledDate_org,'HH:mm:ss')#'">

			<!--- Compare sent out with NOW() --->

				<cfset Currentdatetime = "#DateFormat(getScheduleOptionInfor.CURRENTTIME,'yyyy-mm-dd')# #LSTimeFormat(getScheduleOptionInfor.CURRENTTIME,'HH:mm:ss')#">
				<cfset checkdatecompare = DateCompare(#ScheduleDateBuffer#, getScheduleOptionInfor.CURRENTTIME)>
				<cfif checkdatecompare GTE 0> <!--- sent out later NOW() --->
					<cfset inpQueuedScheduledDate = "'#DateFormat(ScheduleDateBuffer,'yyyy-mm-dd')# #LSTimeFormat(ScheduleDateBuffer,'HH:mm:ss')#'">
				<cfelse><!--- sent out earlier NOW() --->

					<cfset HourCheck = LSParseNumber(LSTimeFormat(Currentdatetime,'H'))>
					<cfif HourCheck GTE EndHourSchedule>
						<!--- if Hour >= schedule option end then set sent out reminder on next day --->
						<cfset ScheduleDateBuffer =  "#DateFormat(DATEADD("d", 1, Currentdatetime),'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
					<cfelseif HourCheck LT StartHourSchedule>
						<!--- if Hour < schedule option start then set time sent out reminder on  today --->
						<cfset ScheduleDateBuffer =  "#DateFormat(Currentdatetime,'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
					<cfelse>
						<!--- StartHourSchedule < sent out valid < EndHourSchedule --->
						<cfset ScheduleDateBuffer =  "#DateFormat(Currentdatetime,'yyyy-mm-dd')# #LSTimeFormat(Currentdatetime,'hh:mm tt')#">
					</cfif>

					<cfset inpQueuedScheduledDate = "'#DateFormat(ScheduleDateBuffer,'yyyy-mm-dd')# #LSTimeFormat(ScheduleDateBuffer,'HH:mm:ss')#'">
				</cfif> --->

			<!--- Look for existing reminder already scheduled - move it if need be - DTSId stored in event --->
			<cfif arguments.inpEvent.inpConfirmationOneDTSId GT 0>

				<!--- allow updates to change target phone number --->
				<cfset var inpContactString = arguments.inpEvent.inpSMSNumber />

				<!---Find and replace all non numerics except P X * #--->
				<cfset inpContactString = REReplaceNoCase(inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

				<!--- Clean up where start character is a 1 --->
				<cfif LEFT(inpContactString, 1) EQ "1">
				    <cfset inpContactString = RemoveChars(inpContactString, 1, 1)>
				</cfif>

				<!--- Move it - update current Scheduled date --->
				<!--- Only if it has not been sent --->
				<cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
					UPDATE
						simplequeue.contactqueue
					SET
						Scheduled_dt = #PreserveSingleQuotes(inpQueuedScheduledDate)#,
						ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					AND
						DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#"/>
					AND
						DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
				</cfquery>

				<!--- Update SentOut_dt of Event  --->
				<cfquery name="UpdateSentOutTime" datasource="#Session.DBSourceEBM#" >
					UPDATE
						calendar.events
					SET
						SentOut_dt = #PreserveSingleQuotes(inpQueuedScheduledDate)#
					WHERE
						ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#"/>
					AND
						PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
				</cfquery>

			<cfelse>

				<!--- Bullet Proof: Validate and verify target short code is still available to user --->
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetAssignedShortCode">
					<cfinvokeargument name="inpBatchId" value="#RetValGetCalendarConfiguration.BATCHID#"/>
					<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				</cfinvoke>

				<!--- Build an XMLControlString to fire batch request at specific time --->
				<!--- RXT='25' is spcial case that just triggeres a new sesssion in contact queue processing logic --->
				<cfset inpXMLControlString = "<RXSS><ELE QID='1' RXT='25' JSON='#XMLFormat(APIRequestJSON)#'>0</ELE></RXSS>" />

				<!--- Create a single request - all the list filtering was done previously  --->
				<cfinvoke component="session.sire.models.cfc.control-point" method="LoadQueueSMSDynamic" returnvariable="RetValLoadQueueSMSDynamic">
					<cfinvokeargument name="inpBatchId" value="#RetValGetCalendarConfiguration.BATCHID#"/>
					<cfinvokeargument name="inpShortCode" value="#RetVarGetAssignedShortCode.SHORTCODE#"/>
					<cfinvokeargument name="inpContactString" value="#arguments.inpEvent.inpSMSNumber#"/>
					<cfinvokeargument name="inpAPIRequestJSON" value="#APIRequestJSON#"/>
					<cfinvokeargument name="inpIRETypeAdd" value="#IREMESSAGETYPE_IRE_CALENDAR_EVENT_TRIGGERED#"/>
					<cfinvokeargument name="inpDeduplicateInterval" value=""/> <!--- INTERVAL 1 DAY  OR  INTERVAL 0 HOUR --->
					<cfinvokeargument name="inpQueuedScheduledDate" value="#PreserveSingleQuotes(inpQueuedScheduledDate)#"/>
					<cfinvokeargument name="inpSkipDNCCheck" value="0"/>
					<cfinvokeargument name="inpXMLControlString" value="#inpXMLControlString#"/>
					<cfinvokeargument name="inpDistributionProcessId" value="0"/> <!--- 9 for testing 0 for production unless we dedicated a CRON for a new ID--->
					<cfinvokeargument name="inpAllowDuplicates" value="1"/>
					<cfinvokeargument name="inpTimeZone" value="#RetValGetCalendarConfiguration.TIMEZONE#"/> <!--- treat all numbers as target time zone inside batch queue  --->
				</cfinvoke>

				<cfset dataout.DISTRIBUTIONID = RetValLoadQueueSMSDynamic.DISTRIBUTIONID />

				<cfif RetValLoadQueueSMSDynamic.DISTRIBUTIONID GT 0>

					<cfset arguments.inpEvent.inpConfirmationOneDTSId = RetValLoadQueueSMSDynamic.DISTRIBUTIONID />
					<cfset arguments.inpEvent.inpConfirmationFlag = APPT_REMINDER_QUEUED />
					<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = 1,
							ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#">,
							SentOut_dt = #PreserveSingleQuotes(inpQueuedScheduledDate)#
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
						AND
							UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					</cfquery>

				<cfelseif RetValLoadQueueSMSDynamic.BLOCKEDBYDNC EQ 1>
					<!--- ConfirmationFlag_int = 9 is blocked by DNC --->
					<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = 9
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
						AND
							UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					</cfquery>
				<cfelse>

					<!--- ConfirmationFlag_int = 5 is Reminder Error - Bad SMS, eMail or other error --->
					<cfquery name="UpdateEventQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = 5
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEvent.id#">
						AND
							UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					</cfquery>

				</cfif>

			</cfif>

			<!--- Verify was able to actually schedule - Check for DNC blockage or duplicate blockage - Update notes/fields in the event itself --->
			<cfset dataout.RXRESULTCODE = "1" />
			<cfset dataout.TYPE = "" />
			<cfset dataout.DEBUGDATA = " ScheduleDateBuffer=#ScheduleDateBuffer# inpQueuedScheduledDate=#inpQueuedScheduledDate# #SerializeJSON(RetValGetCalendarConfiguration)# #SerializeJSON(RetValLoadQueueSMSDynamic)#" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

		<cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>

		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="SaveConfig" access="remote" output="false" hint="Save calendar configuration">
        <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpConfig" type="string" required="true" hint="The structure of the calendar configuration"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var RetValAddEventReminderToQueue = '' />

        <cftry>

	        <cfif !IsStruct(arguments.inpConfig) AND LEN(TRIM(inpConfig)) GT 0 >
				<cfset arguments.inpConfig = DeserializeJSON(arguments.inpConfig) />
			<cfelse>
				<cfthrow message="Invalid Config Specified on Save"/>
	        </cfif>

        	<cfif !IsNumeric(inpEvent.id)>
            	<cfthrow message="Invalid Config Specified on Save"/>
            </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		PhoneNumberFormat_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfig.PhoneNumberFormat#" />,
					Confirmation_BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfig.Confirmation_BatchId#" />,
					ScrollTime_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpConfig.ScrollTime), 45)#" />,
					AutoSendReminder_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfig.AutoSendReminder#" />,
					ReminderIntervalOne_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpConfig.ReminderIntervalOne), 45)#" />
				WHERE
	            		CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveConfigInterval" access="remote" output="false" hint="Save interval setting">
		<cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpInterval" type="string" required="no" default="INTERVAL 1 DAY" hint="default 1 day"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var RetValAddEventReminderToQueue = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		ReminderIntervalOne_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpInterval#" />
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Interval Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveConfigIntervalResend" access="remote" output="false" hint="Save interval resend setting">
		<cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpIntervalResend" type="string" required="no" default="" hint="default 4 hour"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		ReminderIntervalResend_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpInterval#" />
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Interval Resend Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="SaveConfigAutoRemind" access="remote" output="false" hint="Save auto remind setting">
        <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpAutoRemind" type="string" required="false" default="0" hint="ASR setting 0=off 1=on"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var RetValAddEventReminderToQueue = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		AutoSendReminder_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAutoRemind#" />
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveConfigScrollTime" access="remote" output="false" hint="Save scroll time setting">
        <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
        <cfargument name="inpScrollTime" type="string" required="false" default="09:00:00" hint="Default Scroll time is 09:00:00"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var RetValAddEventReminderToQueue = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		ScrollTime_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpScrollTime), 45)#" />
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="SaveConfigTimeZone" access="remote" output="false" hint="Save calendar time zone setting">
	  <cfargument name="inpCalendarId" type="string" required="no" default="1"/>
	  <cfargument name="inpTimeZone" type="string" required="false" default="31" hint="Default calendar time zone setting is Pacific = 31"/>

	  <cfset var dataout = {} />
	    <cfset var UpdateConfigQuery = '' />
	    <cfset var RetValAddEventReminderToQueue = '' />

	  <cftry>

	    <!--- Set default return values --->
		 <cfset dataout.RXRESULTCODE = "-1" />
		 <cfset dataout.TYPE = "" />
		 <cfset dataout.EVENTID = "0" />
		 <cfset dataout.MESSAGE = "" />
		 <cfset dataout.ERRMESSAGE = "" />

		 <!---  --->
		 <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
			    UPDATE
				    calendar.configuration
			    SET
				    TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeZone#" />
			    WHERE
					CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
			    AND
				    UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		 </cfquery>

	    <cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

		 <cfset dataout.RXRESULTCODE = "1" />
		 <cfset dataout.TYPE = "" />
		 <cfset dataout.MESSAGE = "" />
		 <cfset dataout.ERRMESSAGE = "" />

	  <cfcatch TYPE="any">
		 <cfset dataout.RXRESULTCODE = "-2" />
		 <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		 <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	  </cfcatch>

	  </cftry>

	  <cfreturn dataout />
   </cffunction>

   <cffunction name="SaveConfigBatchId" access="remote" output="false" hint="Save calendar reminder Batch Id flow">
	<cfargument name="inpCalendarId" type="string" required="no" default="1"/>
	<cfargument name="inpBatchId" type="string" required="false" default="0" hint="Which BatchId to use for reminder campaigns"/>
	<cfargument name="inpStartHour" type="string" required="false" default="9" hint="Start hour for reminder schedule - default is 9"/>
	<cfargument name="inpEndHour" type="string" required="false" default="21" hint="End hour for reminder schedule - default is 21"/>

	<cfset var dataout = {} />
	<cfset var UpdateConfigQuery = '' />
	<cfset var RetValAddEventReminderToQueue = '' />
	<cfset var RetValSaveConfigReminderSchedule = ''/>
	<cfset var RetVarGetBatchOwner = ''/>

     <cftry>

	  <!--- Set default return values --->
	    <cfset dataout.RXRESULTCODE = "-1" />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.EVENTID = "0" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

	    <!---- Security - Verify user actually owns this batch --->
	    <!--- Bullet Proof: Validate current session owner owns Batch Id --->
	    <!--- Bullet Proof: Verify valid Batch Id - not 0 not blank --->
	    <cfinvoke method="GetBatchOwner" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchOwner">
		    <cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
	    </cfinvoke>

	    <!--- Return error message if user Ids do not match --->
	    <cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>
		    <cfset dataout.RXRESULTCODE = 0 />
		    <cfset dataout.MESSAGE = ""/>
		    <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

		    <cfreturn dataout>
	    </cfif>

	    <!---  --->
	    <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
			  UPDATE
				  calendar.configuration
			  SET
				  Confirmation_BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#" />
			  WHERE
				   CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
			  AND
				  UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	    </cfquery>

		<cfinvoke component="session.sire.models.cfc.calendar" method="SaveConfigReminderSchedule" returnvariable="RetValSaveConfigReminderSchedule">
			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			<cfinvokeargument name="inpStartHour" value="#inpStartHour#">
			<cfinvokeargument name="inpEndHour" value="#inpEndHour#">
		</cfinvoke>

	  <cfset dataout.RXRESULTCODE = 1 />
	  <cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

	    <cfset dataout.RXRESULTCODE = "1" />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

     <cfcatch TYPE="any">
	    <cfset dataout.RXRESULTCODE = "-2" />
	    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
     </cfcatch>

     </cftry>

     <cfreturn dataout />
  </cffunction>

	<cffunction name="SaveConfigReminderSchedule" access="remote" output="false" hint="Save calendar reminder Batch schedule">
		<cfargument name="inpCalendarId" type="string" required="no" default="1"/>
		<cfargument name="inpBatchId" type="string" required="false" default="0" hint="Which BatchId to use for reminder campaigns"/>
		<cfargument name="inpStartHour" type="string" required="false" default="9" hint="Start hour for reminder schedule - default is 9"/>
		<cfargument name="inpEndHour" type="string" required="false" default="21" hint="End hour for reminder schedule - default is 21"/>

		<cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var CheckScheduleOptionExist = '' />
		<cfset var CreateSchedule = '' />
		<cfset var RetVarGetBatchOwner = ''/>
		<cfset var UpdateConfigQuery = ''/>

		<cfset var START_DT	= '' />
		<cfset var STOP_DT	= '' />
		<cfset var STARTHOUR_TI	= '' />
		<cfset var ENDHOUR_TI	= '' />
		<cfset var SUNDAY_TI	= '' />
		<cfset var MONDAY_TI	= '' />
		<cfset var TUESDAY_TI	= '' />
		<cfset var WEDNESDAY_TI	= '' />
		<cfset var THURSDAY_TI	= '' />
		<cfset var FRIDAY_TI	= '' />
		<cfset var SATURDAY_TI	= '' />
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI	= '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = "-1" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.EVENTID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<!---- Security - Verify user actually owns this batch --->
			<!--- Bullet Proof: Validate current session owner owns Batch Id --->
			<!--- Bullet Proof: Verify valid Batch Id - not 0 not blank --->
			<cfinvoke method="GetBatchOwner" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
			</cfinvoke>

			<!--- Return error message if user Ids do not match --->
			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

				<cfreturn dataout>
			</cfif>

			<cfquery name="CheckScheduleOptionExist" datasource="#Session.DBSourceEBM#">
				SELECT
					BatchId_bi, STARTHOUR_TI, ENDHOUR_TI
				FROM
					simpleobjects.scheduleoptions
				WHERE
					BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#" />
				AND
					Enabled_ti > 0
			</cfquery>

			<cfif CheckScheduleOptionExist.RecordCount LT 1>
				<cfset START_DT = Now()>
				<cfset STOP_DT = DateAdd('yyyy', 100, Now())>
				<cfset STARTHOUR_TI = inpStartHour>
				<cfset ENDHOUR_TI = inpEndHour>
				<cfset SUNDAY_TI = true>
				<cfset MONDAY_TI = true>
				<cfset TUESDAY_TI = true>
				<cfset WEDNESDAY_TI = true>
				<cfset THURSDAY_TI = true>
				<cfset FRIDAY_TI = true>
				<cfset SATURDAY_TI = true>
				<cfset DYNAMICSCHEDULEDAYOFWEEK_TI = 0>

				<cfquery name="CreateSchedule" datasource="#Session.DBSourceEBM#">
					INSERT INTO simpleobjects.scheduleoptions
					(
						BatchId_bi,
						STARTHOUR_TI,
						ENDHOUR_TI,
						SUNDAY_TI,
						MONDAY_TI,
						TUESDAY_TI,
						WEDNESDAY_TI,
						THURSDAY_TI,
						FRIDAY_TI,
						SATURDAY_TI,
						LOOPLIMIT_INT,
						<cfif STOP_DT NEQ 'null'>
						STOP_DT,
						</cfif>
						<cfif START_DT NEQ 'null'>
						START_DT,
						</cfif>
						STARTMINUTE_TI,
						ENDMINUTE_TI,
						BLACKOUTSTARTHOUR_TI,
						BLACKOUTENDHOUR_TI,
						LASTUPDATED_DT,
						DYNAMICSCHEDULEDAYOFWEEK_TI,
						ENABLED_TI
					)
					VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(inpBatchId)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#STARTHOUR_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ENDHOUR_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SUNDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#MONDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#TUESDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#WEDNESDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#THURSDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#FRIDAY_TI#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#SATURDAY_TI#">,
						100,
						<cfif STOP_DT NEQ 'null'>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#STOP_DT#">,
						</cfif>
						<cfif START_DT NEQ 'null'>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#START_DT#">,
						</cfif>
						0,
						0,
						0,
						0,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DYNAMICSCHEDULEDAYOFWEEK_TI#">,
						1
					)
				</cfquery>
			<cfelse>
				<cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.scheduleoptions
					SET
						StartHour_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStartHour#" />,
						EndHour_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEndHour#" />
					WHERE
						BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#" />
				</cfquery>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

			<cfset dataout.RXRESULTCODE = "1" />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

		<cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = "-2" />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>

		<cfreturn dataout />
	</cffunction>



    <cffunction name="SaveConfigBatchScheduleTime" access="remote" output="false" hint="Save scroll time setting">
        <cfargument name="inpBatchId" type="string" required="no" default="0"/>
        <cfargument name="inpBatchScheduleTime" type="string" required="yes" default="09:00:00" hint="Default Scroll time is 09:00:00"/>
        <cfargument name="inpType" type="string" required="yes" hint="0 is StartHour, 1 is EndHour."/>

        	<cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var ScheduleByBatchId = '' />
		<cfset var RetValSaveConfigReminderSchedule = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />


		  <!--- Read in Schedule if it exists --->
		  <cfif inpBatchId GT 0>

			  <cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
				  <cfinvokeargument name="INPBATCHID" value="#inpBatchId#">
			  </cfinvoke>

			  <cfif ScheduleByBatchId.RXRESULTCODE NEQ 1>

				  <!--- Auto repair - create schedule --->
				  <cfinvoke component="session.sire.models.cfc.calendar" method="SaveConfigReminderSchedule" returnvariable="RetValSaveConfigReminderSchedule">
					  <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
					  <cfinvokeargument name="inpStartHour" value="9">
					  <cfinvokeargument name="inpEndHour" value="21">
				  </cfinvoke>

			  </cfif>

			  <!--- always get latest entry for operator Id regardless of whose list it is on --->
	            <cfif #arguments.inpType# == '0'>
					<cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.scheduleoptions
						SET
							StartHour_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LEFT(TRIM(inpBatchScheduleTime), 45)#" />
						WHERE
							BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#" />

					</cfquery>
				<cfelse>
					<cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.scheduleoptions
						SET
							EndHour_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#LEFT(TRIM(inpBatchScheduleTime), 45)#" />
						WHERE
							BatchId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#" />
					</cfquery>
	            </cfif>

		  </cfif>





        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update batch schedule is OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveConfigFilterCalendar" access="remote" output="false" hint="Save Filter Calendar">
        <cfargument name="inpCalendarId" type="string" required="no" hint="Calendar Id" default="1"/>
        <cfargument name="inpFilterCalendar" type="string" required="false" hint="List filter" default=""/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />

        <cftry>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		FilterConfirmationFlag_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpFilterCalendar#" />
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

			<!--- save filter in cookie --->
			<cfcookie name="fl_calendar" value="#inpFilterCalendar#" expires="30"/>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveNotificationMethodCalendar" access="remote" output="false" hint="Save Notification Method">
        <cfargument name="inpCalendarId" type="string" required="no" hint="Calendar Id" default="1"/>
        <cfargument name="inpNoticeMethod" type="string" required="false" hint="Notification Method" default="0"/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />

        <cftry>
        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
			   		NotificationMethod_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNoticeMethod#"/>
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<cffunction name="SaveNotificationItemCalendar" access="remote" output="false" hint="Save Notification Method">
        <cfargument name="inpCalendarId" type="string" required="no" hint="Calendar Id" default="1"/>
        <cfargument name="inpNoticeMethod" type="string" required="false" hint="Notification Method" default="0"/>
		<cfargument name="inpNoticeSMS" type="string" required="false" hint="Notification SMS list" default=""/>
		<cfargument name="inpNoticeEMAIL" type="string" required="false" hint="Notification Email list" default=""/>

        <cfset var dataout = {} />
		<cfset var UpdateConfigQuery = '' />
		<cfset var inpNoticeItem = ""/>

        <cftry>
        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.EVENTID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
			<cfif inpNoticeMethod EQ 0>
				<cfset inpNoticeItem = inpNoticeEMAIL>
			<cfelse>
				<cfset inpNoticeItem = inpNoticeSMS>
			</cfif>

            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateConfigQuery" datasource="#Session.DBSourceEBM#">
               	UPDATE
               		calendar.configuration
			   	SET
				    NotificationMethod_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNoticeMethod#" />,
			   		NotificationItem_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNoticeItem#"/>
				WHERE
	            	CalendarId_int = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCalendarId#" />
	           	AND
	           		UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
            </cfquery>

        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Update Calendar Config OK.'>
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

	<!--- CALENDAR REPORT --->
	<cffunction name="EventsUpcomingList" access="remote" output="true" hint="Get list event up comming."><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'DESC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="3">
						<cfset order = "ev.Start_dt" />
					</cfcase>
					<cfcase value="4">
						<cfset order = "ev.End_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Start_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Start_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int
				FROM
					calendar.events ev
				WHERE
					ev.Active_int = 1
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					ev.Start_dt >= NOW()
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="EventsReminderList" access="remote" output="true" hint="Get list event reminder"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "ev.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Created_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int
				FROM
					calendar.events ev
				WHERE
					ev.Active_int = 1
				And
					ev.ConfirmationFlag_int = 1
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				And
					ev.ConfirmationOneDTSId_int <> 0
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="EventsAcceptedList" access="remote" output="true" hint="Get list event accept"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "ev.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Created_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int
				FROM
					calendar.events ev
				WHERE
					ev.Active_int = 1
				And
					ev.ConfirmationFlag_int = 2
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                 <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="EventsDeclinedList" access="remote" output="true" hint="Get list event declined"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "ev.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Created_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int
				FROM
					calendar.events ev
				WHERE
					ev.Active_int = 1
				And
					ev.ConfirmationFlag_int = 3
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                 <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="EventsOpenChatList" access="remote" output="true" hint="Get list event open chat"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "ev.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Created_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int,
					ev.ChatSessionId_bi
				FROM
					calendar.events ev
				LEFT JOIN
					simplequeue.sessionire ses ON ev.ChatSessionId_bi =  ses.SessionId_bi

				WHERE
					ev.Active_int = 1
				And
					ev.ChatSessionId_bi <> 0
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND
					ses.SessionState_int = 1
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                 <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="EventsNoResponeList" access="remote" output="true" hint="Get list event no response"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEventQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "ev.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "ev.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ev.Created_dt"/>
			</cfif>

			<cfquery name="GetEventQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ev.CalendarId_int,
					ev.Title_vch,
					ev.UserId_int,
					ev.Start_dt,
					ev.End_dt,
					ev.Created_dt,
					ev.eMailAddress_vch,
					ev.SMSNumber_vch,
					ev.ConfirmationFlag_int,
					ev.ChatSessionId_bi,
					ev.ConfirmationOneDTSId_int
				FROM
					calendar.events ev
				LEFT JOIN
					simplequeue.contactqueue cq ON ev.ConfirmationOneDTSId_int = cq.DTSId_int
				LEFT JOIN
					simplequeue.sessionire ire ON ev.ConfirmationOneDTSId_int = ire.DTSId_int
				WHERE
					ev.Active_int = 1
				AND
					cq.DTSStatusType_ti = 5
				AND
					ire.SessionState_int = 1
				AND
					ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetEventQuery">
               <cfset Status = "Default">
                <cfif GetEventQuery.ConfirmationFlag_int EQ "1">
                	<cfset Status = "Reminder InQueue">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "2">
                	<cfset Status = "Reminder Accepted">
                <cfelseif GetEventQuery.ConfirmationFlag_int EQ "3">
                	<cfset Status = "Reminder Declined">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "4">
                	<cfset Status = "Reminder Change Request">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "5">
                	<cfset Status = "Reminder Error - Bad SMS, eMail or other error">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "6">
                	<cfset Status = "Don't send Reminder">
				<cfelseif GetEventQuery.ConfirmationFlag_int EQ "7">
                	<cfset Status = "Reminder Sent">
				<cfelse>
					<cfset Status = "Default">
                </cfif>
				<cfset tempItem = {
					EVENTID = '#CalendarId_int#',
					CONTACTQUEID = '#ConfirmationOneDTSId_int#',
					USERID = '#UserId_int#',
					NAME = "#Title_vch#",
					EMAIL = "#eMailAddress_vch#",
					PHONE = "#SMSNumber_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					EVENT_START = "#DateTimeFormat(Start_dt,'yyyy-mm-dd HH:nn:ss')#",
					EVENT_END = "#DateTimeFormat(End_dt,'yyyy-mm-dd HH:nn:ss')#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

	<cffunction name="ContactstringLimited" access="remote" output="false" hint="Check limited reminder sent on day by Contactstring">
		<cfargument name="inpEventId" type="string" required="no" default="0" hint="Event id"/>
    	<cfargument name="inpSMSNumber" type="string" required="no" default="1" hint="Contactstring"/>
		<cfargument name="inpEventStart" type="string" required="no" hint="Event start time"/>

        <cfset var dataout = {} />
        <cfset var GetCountReminderContactstring= '' />
		<cfset var inpQueuedScheduledDate = ''/>
		<cfset var ScheduleDateBuffer = ''/>
		<cfset var RetValGetCalendarConfiguration = {} />
		<cfset var GetCountReminderInqueByPhone	= '' />
        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>
			<cfset arguments.inpSMSNumber = ReReplaceNoCase(arguments.inpSMSNumber,"[^0-9,]","","ALL")>

			<cfquery name="GetCountReminderContactstring" datasource="#Session.DBSourceEBM#">
                SELECT
                	PKId_bi,
					SMSNumber_vch
                FROM
                    calendar.events
                WHERE
                    SMSNumber_vch = <cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSMSNumber#" />
				AND
					Active_int = 1
				AND
					DATE(Start_dt) = DATE('#left(arguments.inpEventStart,10)#')
				<cfif arguments.inpEventId NEQ "">
					AND PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
				</cfif>
	        </cfquery>

			<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
				<cfinvokeargument name="inpCalendarId" value="0">
			</cfinvoke>

			<cfset ScheduleDateBuffer =  "#DateFormat(arguments.inpEventStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEventStart,'HH:mm:ss')#" />
			<cfset inpQueuedScheduledDate = "'#ScheduleDateBuffer#' - #RetValGetCalendarConfiguration.INTERVALONE#" />
		<!---
			<cfquery name="GetCountReminderInqueByPhone" datasource="#Session.DBSourceEBM#">
                SELECT
					DTSId_int
				FROM
					simplequeue.contactqueue
				WHERE
					DTSId_int in (
									SELECT ConfirmationOneDTSId_int FROM calendar.events
								  	WHERE ConfirmationOneDTSId_int > 0
								  	<cfif arguments.inpEventId NEQ "">
										AND PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
									</cfif>)
				AND
					DATE(Scheduled_dt) = DATE(#PreserveSingleQuotes(inpQueuedScheduledDate)#)
				AND
					ContactString_vch = <cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSMSNumber#"/>
	        </cfquery>
		--->
	  		<cfif GetCountReminderContactstring.RecordCount GT 2>
				<cfset dataout.RXRESULTCODE = 2 />
				<cfreturn dataout>
			<!---
			<cfelseif GetCountReminderContactstring.RecordCount GT 0 AND GetCountReminderInqueByPhone.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 3 />
				<cfreturn dataout>
			--->
			<cfelse>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfreturn dataout />
	  		</cfif>

			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>
        </cftry>
	</cffunction>

	<cffunction name="GetUserInfo" access="public" output="false" hint="Get info of user">
		<cfargument name="inpUserId" required="false" type="numeric" hint="User id" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var getUserInfo = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.EMAIL = '' />
		<cfset dataout.CONTACTSTRING = '' />

		<cftry>
			<cfquery name="getUserInfo" datasource="#session.DBSourceEBM#">
				SELECT
                    UserId_int,
               		CONCAT(FirstName_vch ,' ', LastName_vch) as USERNAME,
                    EmailAddress_vch,
                    MFAContactString_vch
			   	FROM
                    simpleobjects.useraccount
                WHERE
	            	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpUserId#">
			</cfquery>

			<cfif getUserInfo.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.EMAIL = getUserInfo.EmailAddress_vch/>
				<cfset dataout.CONTACTSTRING = getUserInfo.MFAContactString_vch/>
				<cfset dataout.MESSAGE = "Get info success"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetInfoChatActiveCalendar" access="remote" output="false" hint="Get header info of Chat pop-up">
		<cfargument name="inpPhonenumber" required="true" type="numeric" hint="Contactstring" default=""/>
		<cfargument name="inpEventId" required="false" type="numeric" hint="Event id" default=""/>

		<cfset var dataout = {} />
		<cfset var getEventInfo = '' />
		<cfset var getCDFInfo = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.FIRSTNAME = '' />
		<cfset dataout.LASTNAME = '' />
		<cfset dataout.EVENT = '' />
		<cfset dataout.EVENTSTART = '' />
		<cfset dataout.EVENTEND = '' />
		<cfset dataout.CHATNOTES = ''/>
		<cfset dataout.CHATSTATUS = ''/>
		<cfset var shortCode	= '' />

		<cftry>
			<!--- Get info chat active--->
			<cfquery name="getEventInfo" datasource="#session.DBSourceEBM#">
				SELECT
                    ev.Title_vch,
					ev.Start_dt,
					ev.End_dt,
					ev.ChatNotes_vch,
					ss.SessionState_int
			   	FROM
                    calendar.events ev
				LEFT JOIN
					simplequeue.sessionire ss ON ev.ChatSessionId_bi = ss.SessionId_bi
                WHERE
	            	ev.PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
			</cfquery>

			<cfif getEventInfo.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.EVENT = getEventInfo.Title_vch/>
				<cfset dataout.EVENTSTART = "#DateFormat(getEventInfo.Start_dt,'mm/dd/yyyy')# #LSTimeFormat(getEventInfo.Start_dt,'hh:mm tt')#"/>
				<cfset dataout.EVENTEND = "#DateFormat(getEventInfo.End_dt,'mm/dd/yyyy')# #LSTimeFormat(getEventInfo.End_dt,'hh:mm tt')#"/>
				<cfset dataout.CHATNOTES = getEventInfo.ChatNotes_vch/>
				<cfset dataout.CHATSTATUS = getEventInfo.SessionState_int/>
			</cfif>

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<cfquery name="getCDFInfo" datasource="#session.DBSourceEBM#">
				Select
					ct.VariableValue_vch,cf.CdfName_vch
				FROM
					simplelists.contactvariable ct
					INNER JOIN simplelists.customdefinedfields cf ON ct.CdfId_int = cf.CdfId_int
				Where
				(
					cf.CdfName_vch like '%Last Name%'
				or
					cf.CdfName_vch like '%First Name%'
				)
				AND ct.ContactId_bi in (
										SELECT
											simplelists.contactstring.ContactId_bi
										FROM
											simplelists.groupcontactlist
											INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
											INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
										WHERE
											simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
										AND
											simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
										AND
											simplelists.groupcontactlist.groupid_bi in (
																						SELECT
																							simplelists.grouplist.GroupId_bi
																						FROM
																							simplelists.grouplist
																						WHERE
																							simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
																							And ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
																						)
										AND
											simplelists.contactstring.ContactType_int =3
										AND
											simplelists.contactstring.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpPhonenumber#">

										)
			</cfquery>
			<cfif getCDFInfo.RecordCount GT 0>
				<cfloop query ="getCDFInfo">
					<cfif getCDFInfo.CdfName_vch EQ "Last Name">
						<cfset dataout.LASTNAME = getCDFInfo.VariableValue_vch />
					<cfelseif getCDFInfo.CdfName_vch EQ "First Name">
						<cfset dataout.FIRSTNAME = getCDFInfo.VariableValue_vch />
					</cfif>
				</cfloop>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="UpdateEventStatus" access="remote" output="false" hint="Update manual Event status">
		<cfargument name="inpEventStatus" required="true" type="numeric" hint="Event status" default=""/>
		<cfargument name="inpEventId" required="false" type="numeric" hint="Event id" default=""/>

		<cfset var dataout = {} />
		<cfset var updateEventstatus = '' />
		<cfset var CheckEventInvalid = ''/>
		<cfset var updateEventstatusResult =''/>
		<cfset var closeAutoreminder = ''/>
		<cfset var CloseSessionReminder	= '' />
		<cfset var GetDTSIdInfor = ''/>
		<cfset var GetEventInfoQuery = ''/>
		<cfset var RetValGetCalendarConfiguration = ''/>

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.RXRESULTSTATUS = arguments.inpEventStatus/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

	 	<cftry>
			<!--- check event valid to edit --->
			<cfif inpEventStatus NEQ 8>
				<cfquery name="CheckEventInvalid" datasource="#Session.DBSourceEBM#">
					SELECT
						cq.DTSStatusType_ti
					FROM
						calendar.events ev
					INNER JOIN
						simplequeue.contactqueue cq ON ev.ConfirmationOneDTSId_int = cq.DTSId_int
					WHERE
						ev.PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEventId#" />
					AND
						ev.ConfirmationOneDTSId_int <> 0
					AND
						cq.DTSStatusType_ti <> 9
				</cfquery>
				<!---
				<cfif CheckEventInvalid.RecordCount GT 0>
					<cfquery name="updateEventstatus" datasource="#session.DBSourceEBM#" result="updateEventstatusResult">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEventStatus#">
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
						AND
							(ConfirmationFlag_int NOT IN (6,7) OR ConfirmationFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEventStatus#">)
					</cfquery>
					<cfif updateEventstatusResult.RecordCount LT 1>
						<cfset dataout.RXRESULTCODE = -1 />
						<cfset dataout.ERRMESSAGE = ''>
						<cfset dataout.MESSAGE = 'This reminder is scheduled. You can not change status'>
						<cfreturn dataout>
					</cfif>
				<cfelse>
				</cfif>
				--->
				<!--- Change Event status not InQue --->
				<cfquery name="updateEventstatus" datasource="#session.DBSourceEBM#" result="updateEventstatusResult">
					UPDATE
						calendar.events
					SET
						ConfirmationFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEventStatus#">
					WHERE
						PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
				</cfquery>


				<cfif inpEventStatus EQ 6 AND arguments.inpEvent.inpConfirmationOneDTSId GT 0>
					<!--- Remove any scheduled reminders--->

					<!--- Remove any queue reminders --->
					<cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
						UPDATE
							simplequeue.contactqueue
						SET
							DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#"/>
						WHERE
							DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEvent.inpConfirmationOneDTSId#"/>
						AND
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					</cfquery>

				</cfif>

			<cfelse>
				<!--- Change to Chat Active when open popup Chat --->
				<cfquery name="GetEventInfoQuery" datasource="#Session.DBSourceEBM#">
					SELECT
						ev.PKId_bi,
						ev.UserId_int,
						ev.CalendarId_int,
						ev.Title_vch,
						ev.Start_dt,
						ev.End_dt,
						ev.SMSNumber_vch,
						ev.Active_int,
						ev.ChatSessionId_bi,
						ev.ConfirmationFlag_int,
						ev.ConfirmationOneDTSId_int,
						ss.SessionState_int
					FROM
						calendar.events ev
					LEFT JOIN
						simplequeue.sessionire ss ON ev.ChatSessionId_bi = ss.SessionId_bi
					WHERE
						ev.PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEventId#" />
					AND
						ev.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					AND
						ev.Active_int = 1
				</cfquery>

				<cfif GetEventInfoQuery.SessionState_int NEQ 4>
					<cfquery name="updateEventstatus" datasource="#session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#APPT_REMINDER_CHAT_ACTIVE#">
						WHERE
							PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
					</cfquery>
				<cfelse>
					<cfset dataout.RXRESULTSTATUS = GetEventInfoQuery.ConfirmationFlag_int>
				</cfif>

				<cfquery name="GetDTSIdInfor" datasource="#session.DBSourceEBM#">
					SELECT
						ConfirmationOneDTSId_int
					FROM
						calendar.events
					WHERE
						PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpEventId#">
					AND
						ConfirmationOneDTSId_int > 0
				</cfquery>

				<cfif GetDTSIdInfor.RecordCount GT 0>
					<!--- get Calendar config --->
					<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration"></cfinvoke>

					<cfquery name="closeAutoreminder" datasource="#session.DBSourceEBM#">
						UPDATE
							simplequeue.contactqueue
						SET
							DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#"/>
						WHERE
							DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetDTSIdInfor.ConfirmationOneDTSId_int#">
						AND
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					</cfquery>

					<cfquery name="CloseSessionReminder" datasource="#Session.DBSourceEBM#" >
						UPDATE
							simplequeue.sessionire
						SET
							SessionState_int = 4
						WHERE
							DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetDTSIdInfor.ConfirmationOneDTSId_int#">
						AND
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
					</cfquery>
				</cfif>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = 'Update successfully.' />
			<cfset dataout.ERRMESSAGE = '' />

			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="UpdatEventChatNotes" access="remote" output="false" hint="Update Event notes chat">
		<cfargument name="inpNotes" required="true" type="string" hint="Notes chat of Event" default=""/>
		<cfargument name="inpEventId" required="false" type="numeric" hint="Event id" default=""/>


		<cfset var dataout = {} />
		<cfset var UpdateChatNotes = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<!--- check event valid to edit --->
			<cfif arguments.inpEventId NEQ "">
				<cfquery name="UpdateChatNotes" datasource="#Session.DBSourceEBM#">
					UPDATE
						calendar.events
					SET
						ChatNotes_vch = <cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNotes#" />
					WHERE
						PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpEventId#" />
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = 'Update successfully.' />
				<cfset dataout.ERRMESSAGE = '' />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>


<!--- remove this pile of crap Seta code  @#$@#$%^%& --->
	<cffunction name="validateEvent" access="remote" hint="Validate Event information Seta AFU - do NOT use ">
		<cfargument name="inpEvent" required="true" type="string" hint="The structure of the event" default=""/>
		<cfargument name="inpType" required="true" type="string" hint="0: check opt-out, 1: check create 3 events in the same day to 1 phone, 2: check with auto schedule setting" default="0"/>
		<cfargument name="inpSkipCheckOptOut" required="true" type="numeric" hint="0 - display message, 1- user select ok, 2- user select cancel" default="0"/>
		<cfargument name="inpSkipCheckSchedule" required="true" type="numeric" hint="0 - display message, 1- user select ok, 2- user select cancel, 3- user select create event but not schedule" default="0"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.TYPE = 0 /> <!--- 1 - check opt-out , 2 - check schedule ON, 3 - check schedule OFF--->
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RELOAD = 0 /> <!--- 0 - no reload, 1 - reload --->

		<cfset var checkOptout = 0/>
		<cfset var checkLimit = 0/>
		<cfset var checkSchedule = 0/>

		<cfset var saveEventStatus = 0/>
		<cfset var autoSchedule = 0/>
		<cfset var RetValcheckContactStringOptout = ''/>

		<cfset var shortCode = ''/>
		<cfset var RetValAddEvent = ''/>
		<cfset var RetValContactstringLimited =''/>

		<cfset var GetReminderTodayResult = ''/>


		<cfset var GetReminderToday = ''/>
		<cfset var RetValUpdateEventStatus = ''/>
		<cfset var RetValValidatescheduleAddEvent = ''/>

		<cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(inpEvent)) GT 0 >
			<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
		</cfif>

		<!--- CHECK OPTOUT --->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

		<cfinvoke component="session.sire.models.cfc.optin-out" method="checkContactStringOptout" returnvariable="RetValcheckContactStringOptout">
			<cfinvokeargument name="inpContactString" value="#inpEvent.inpSMSNumber#">
			<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
		</cfinvoke>
		<cfif RetValcheckContactStringOptout.RXRESULTCODE EQ 1>
			<cfif inpSkipCheckOptOut EQ 0>
				<cfset dataout.RXRESULTCODE = 0/> <!--- return message confirm --->
				<cfset dataout.MESSAGE = '<h4 class="be-modal-title">PHONE NUMBER OPT-OUT</h4><p>Are you sure you want to create this appointment?</p>' />
				<cfset dataout.TYPE = 1 />
				<cfreturn dataout>
			<cfelseif inpSkipCheckOptOut EQ 1>
				<cfset checkOptout = 2/> <!--- create event with status: opt-out --->
			</cfif>
		<cfelse>
			<cfset checkOptout = 1> <!--- user not optout --->
		</cfif>



		<!--- check ContactstringLimited --->
		<cfinvoke component="session.sire.models.cfc.calendar" method="ContactstringLimited" returnvariable="RetValContactstringLimited">
			<cfinvokeargument name="inpEventId" value="#inpEvent.id#">
			<cfinvokeargument name="inpEventStart" value="#inpEvent.inpStart#">
			<cfinvokeargument name="inpSMSNumber" value="#inpEvent.inpSMSNumber#">
		</cfinvoke>

		<cfif RetValContactstringLimited.RXRESULTCODE EQ 2>
			<cfset dataout.RXRESULTCODE = -1/> <!--- return message warring --->
			<cfset dataout.MESSAGE = 'This phone number have exceeded reminder limit.'/>
			<cfset dataout.ERRMESSAGE = 'This phone number have exceeded reminder limit.'/>
			<cfreturn dataout>
		<cfelseif RetValContactstringLimited.RXRESULTCODE EQ -1>
			<cfset dataout.RXRESULTCODE = -1/> <!--- return message warring --->
			<cfset dataout.MESSAGE = 'Error when check 3 events in the same day'/>
			<cfset dataout.ERRMESSAGE = 'Error when check 3 events in the same day'/>
			<cfreturn dataout>
		<cfelseif RetValContactstringLimited.RXRESULTCODE EQ 1>
			<cfset checkLimit = 1 /> <!--- pass rule check ContactstringLimited --->
		</cfif>


		<cfset dataout.WTF = "checkOptout=#checkOptout# checkLimit=#checkLimit# inpEvent.Id=#inpEvent.Id#" />

		<!--- if user optout and < limit - do not check schedule --->
		<cfif checkOptout EQ 2 AND checkLimit EQ 1>
			<!--- call function saveEvent status optout --->
			<cfif inpEvent.Id NEQ ''>
				<cfinvoke component="session.sire.models.cfc.calendar" method="SaveEvent" returnvariable="RetValAddEvent">
					<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
					<cfinvokeargument name="inpASR" value="0">
				</cfinvoke>
			<cfelse>
				<cfinvoke component="session.sire.models.cfc.calendar" method="AddEvent" returnvariable="RetValAddEvent">
					<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
					<cfinvokeargument name="inpASR" value="0">
				</cfinvoke>
				<cfset dataout.RELOAD = 1 />
			</cfif>
			<cfif RetValAddEvent.EVENTID GT 0>
				<cfinvoke component="session.sire.models.cfc.calendar" method="UpdateEventStatus" returnvariable="RetValUpdateEventStatus">
					<cfinvokeargument name="inpEventId" value="#RetValAddEvent.EVENTID#">
					<cfinvokeargument name="inpEventStatus" value="9">
				</cfinvoke>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
			</cfif>
			<cfreturn dataout>
		</cfif>

		<cfif inpSkipCheckSchedule EQ 0>
			<cfinvoke component="session.sire.models.cfc.calendar" method="ValidatescheduleAddEvent" returnvariable="RetValValidatescheduleAddEvent">
				<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
			</cfinvoke>
			<cfif RetValValidatescheduleAddEvent.RXRESULTCODE EQ 1>
				<cfset dataout.RXRESULTCODE = 0/><!--- return message confirm --->
				<cfset dataout.MESSAGE = "#RetValValidatescheduleAddEvent.MESSAGE#"/>
				<cfif RetValValidatescheduleAddEvent.TYPE EQ 1> <!--- Auto schedule ON --->
					<cfset dataout.TYPE = 2 />
				<cfelseif RetValValidatescheduleAddEvent.TYPE EQ 2> <!--- Auto schedule OFF --->
					<cfset dataout.TYPE = 3 />
				</cfif>
				<cfreturn dataout>
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1/><!--- return message warring --->
				<cfset dataout.MESSAGE = "#RetValValidatescheduleAddEvent.MESSAGE#"/>
				<cfreturn dataout>
			</cfif>
		<cfelseif inpSkipCheckSchedule EQ 1>
			<cfset checkSchedule = 1/> <!--- user select ok to create event --->
		<cfelseif inpSkipCheckSchedule EQ 3>
			<cfset checkSchedule = 3/> <!--- user select ok to create event but not schedule --->
		</cfif>

		<cfif checkOptout EQ 1 AND checkLimit EQ 1 >
			<cfif checkSchedule EQ 1>
				<!--- Rule: Just schedule 1 reminder on 1 day with 1 phone --->
				<cfinvoke component="session.sire.models.cfc.calendar" method="ValidatescheduleAddEvent" returnvariable="RetValValidatescheduleAddEvent">
					<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
				</cfinvoke>

				<cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>

				<cfquery name="GetReminderToday" datasource="#Session.DBSourceEBM#" result="GetReminderTodayResult">
					SELECT
						PKId_bi
					FROM
						calendar.events
					WHERE
						ConfirmationOneDTSId_int > 0
					<cfif inpEvent.id NEQ "">
						AND PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEvent.id#">
					</cfif>
					AND
						DATE(SentOut_dt) = DATE('#PreserveSingleQuotes(RetValValidatescheduleAddEvent.RXRESULT)#')
					AND
						SMSNumber_vch = <cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEvent.inpSMSNumber#"/>
					AND
						Active_int = 1
				</cfquery>

				<cfif GetReminderTodayResult.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = -1><!--- return message warring --->
					<cfset dataout.MESSAGE = "Can not create this event. Our system detects more than one appointments with the same phone number in the same day, our reminder message will only go out to the first one."/>
					<cfset dataout.ERRMESSAGE = "Can not create this event. Our system detects more than one appointments with the same phone number in the same day, our reminder message will only go out to the first one."/>
					<cfreturn dataout>
				<cfelse>
					<cfif inpEvent.Id NEQ ''>
						<cfinvoke component="session.sire.models.cfc.calendar" method="SaveEvent" returnvariable="RetValAddEvent">
							<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
							<cfinvokeargument name="inpASR" value="1">
						</cfinvoke>
					<cfelse>
						<cfinvoke component="session.sire.models.cfc.calendar" method="AddEvent" returnvariable="RetValAddEvent">
							<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
							<cfinvokeargument name="inpASR" value="1">
						</cfinvoke>
						<cfset dataout.RELOAD = 1 />
					</cfif>
					<cfif RetValAddEvent.EVENTID GT 0>
						<cfset dataout.RXRESULTCODE = 1/>
						<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
					<cfelse>
						<cfset dataout.RXRESULTCODE = 1/>
						<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
					</cfif>
					<cfreturn dataout>
				</cfif>

				<!--- call function saveEvent status & scheduled --->
			<cfelseif checkSchedule EQ 3 >
				<cfif inpEvent.Id NEQ ''>
					<cfinvoke component="session.sire.models.cfc.calendar" method="SaveEvent" returnvariable="RetValAddEvent">
						<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
						<cfinvokeargument name="inpASR" value="0">
					</cfinvoke>
				<cfelse>
					<cfinvoke component="session.sire.models.cfc.calendar" method="AddEvent" returnvariable="RetValAddEvent">
						<cfinvokeargument name="inpEvent" value="#serializeJSON(inpEvent)#">
						<cfinvokeargument name="inpASR" value="0">
					</cfinvoke>
					<cfset dataout.RELOAD = 1 />
				</cfif>
				<cfif RetValAddEvent.EVENTID GT 0>
					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
				<cfelse>
					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.MESSAGE = "#RetValAddEvent.MESSAGE#" />
				</cfif>
				<cfreturn dataout>
				<!--- call function saveEvent status no scheduled --->
			</cfif>

		</cfif>

	</cffunction>

	<cffunction name="ValidatescheduleAddEvent" access="remote" output="false" hint="Validate schedule_dt">
        <cfargument name="inpEvent" type="string" required="true" hint="The structure of the event"/>

        <cfset var dataout = {} />
		<cfset var ScheduleDateBuffer = ''/>
		<cfset var RetValGetCalendarConfiguration = ''/>
		<cfset var getScheduleOptionInfor = ''/>
		<cfset var StartHourSchedule = ''/>
		<cfset var EndHourSchedule = ''/>
		<cfset var StartHourSchedule_txt = ''/>
		<cfset var EndHourSchedule_txt = ''/>
		<cfset var inpQueuedScheduledDate = ''/> <!--- Sent out time (PST) --->
		<cfset var GetReminderTodayResult = ''/>
		<cfset var GetReminderTodayByPhoneResult = ''/>


		<cfset var CurrTZ = 31>
		<cfset var CurrNPA = "">
		<cfset var CurrNXX = "">
		<cfset var inpQueuedScheduledDate_txt = "">
		<cfset var inpQueuedScheduledDateTimeZone_txt = ''/>
		<cfset var Checkdatecompare = 0>
		<cfset var Confirmquestion = ''/>
		<cfset var HourCheck = '' />
		<cfset var GetTZInfo = '' />

        <cftry>

	        <cfif !IsStruct(arguments.inpEvent) AND LEN(TRIM(arguments.inpEvent)) GT 0 >
				<cfset arguments.inpEvent = DeserializeJSON(arguments.inpEvent) />
				<cfif arguments.inpEvent.id NEQ ''>
					<cfset Confirmquestion = "Do you want to update the event?">
				<cfelse>
					<cfset Confirmquestion = "Do you want to create the event?">
				</cfif>
	        </cfif>

        	<!--- Set default return values --->
            <cfset dataout.RXRESULTCODE = "0">
            <cfset dataout.TYPE = 0> <!--- 0: pass rule 1 schedule/1 day; 1: more than 1 schedule/1day --->
            <cfset dataout.EVENTID = "0">
            <cfset dataout.DTSID = 0>
            <cfset dataout.CONFIRMFLAG = 0>
            <cfset dataout.MESSAGE = "">
            <cfset dataout.ERRMESSAGE = "">
			<cfset dataout.RXRESULT = "">
			<!--- get Calendar config --->
			<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
			</cfinvoke>
            <cfset arguments.inpEvent.inpStart = "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#">
            <cfset arguments.inpEvent.inpEnd = "#DateFormat(arguments.inpEvent.inpEnd,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpEnd,'HH:mm:ss')#">
            <cfset arguments.inpEvent.inpSMSNumber = REReplaceNoCase(arguments.inpEvent.inpSMSNumber, "[^\d^\*^P^X^##]", "", "ALL")>
		<!--- caculate schedule_dt --->

			<!--- Adjust for numers that start with 1 --->
			<cfif LEFT(arguments.inpEvent.inpSMSNumber, 1) EQ 1>
				<cfset CurrNPA = MID(arguments.inpEvent.inpSMSNumber, 2, 3)>
				<cfset CurrNXX = MID(arguments.inpEvent.inpSMSNumber, 5, 3)>

				<!--- Get rid of 1 when looking up in DB --->
				<cfset arguments.inpEvent.inpSMSNumber = RIGHT(arguments.inpEvent.inpSMSNumber, LEN(arguments.inpEvent.inpSMSNumber)-1)>
			<cfelse>
				<cfset CurrNPA = LEFT(arguments.inpEvent.inpSMSNumber, 3) >
				<cfset CurrNXX = MID(arguments.inpEvent.inpSMSNumber, 4, 3)>
			</cfif>

			<cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
				SELECT
					CASE cx.T_Z
							WHEN 14 THEN 37
							WHEN 11 THEN 34
							WHEN 10 THEN 33
							WHEN 9 THEN 32
							WHEN 8 THEN 31
							WHEN 7 THEN 30
							WHEN 6 THEN 29
							WHEN 5 THEN 28
							WHEN 4 THEN 27
							END AS TimeZone,
					CASE
							WHEN fx.Cell IS NOT NULL THEN fx.Cell
							ELSE 0
							END  AS CellFlag
				FROM
					MelissaData.FONE AS fx JOIN
					MelissaData.CNTY AS cx ON
					(fx.FIPS = cx.FIPS)
					WHERE
					fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
				AND
					fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
			</cfquery>

			<cfif GetTZInfo.RecordCount GT 0>
				<cfset CurrTZ = #GetTZInfo.TimeZone#>
			<cfelse>
				<cfset CurrTZ = 31>
			</cfif>

			<cfset ScheduleDateBuffer =  "#DateFormat(arguments.inpEvent.inpStart,'yyyy-mm-dd')# #LSTimeFormat(arguments.inpEvent.inpStart,'HH:mm:ss')#" />
			<!---Change time by timezone --->
			<cfset ScheduleDateBuffer = "#DateAdd("h", LSParseNumber(CurrTZ) - 31, ScheduleDateBuffer)#">

			<cfif !ISDATE(ScheduleDateBuffer) >
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "Could not calculate a scheduled reminder time from the provided event start date and time."/>
				<cfset dataout.ERRMESSAGE = ""/>
				<cfreturn dataout>
			</cfif>
			<cfset inpQueuedScheduledDate = "#ScheduleDateBuffer#" />
            <!--- Get schedule options information --->
			<cfquery name="getScheduleOptionInfor" datasource="#Session.DBSourceEBM#">
				SELECT
					NOW() AS CURRENTTIME,
					StartHour_ti,
					EndHour_ti
				FROM
					simpleobjects.scheduleoptions
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#RetValGetCalendarConfiguration.BATCHID#">
			</cfquery>
			<cfif getScheduleOptionInfor.RecordCount GT 0>
			<!--- - (LSParseNumber(CurrTZ) - 31)--->
				<cfset StartHourSchedule = getScheduleOptionInfor.StartHour_ti  + LSParseNumber(CurrTZ) - 31/>
				<cfif StartHourSchedule LT 10>
					<cfset StartHourSchedule_txt = '0' & StartHourSchedule/>
				<cfelse>
					<cfset StartHourSchedule_txt =  StartHourSchedule/>
				</cfif>

				<cfset EndHourSchedule = getScheduleOptionInfor.EndHour_ti + LSParseNumber(CurrTZ) - 31/>
				<cfif EndHourSchedule LT 10>
					<cfset EndHourSchedule_txt = '0' & EndHourSchedule/>
				<cfelse>
					<cfset EndHourSchedule_txt =  EndHourSchedule/>
				</cfif>
			</cfif>
			<!--- change time by schedule options information --->
			<cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ "DAY">

				<cfset HourCheck = LSParseNumber(LSTimeFormat(ScheduleDateBuffer,'H')) />
				<cfif HourCheck GTE EndHourSchedule>
					<!--- if Hour >= schedule option end then set sent out reminder on next day --->
					<cfset ScheduleDateBuffer =  "#DateFormat(DATEADD("d", 1, ScheduleDateBuffer),'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
				<cfelseif HourCheck LT StartHourSchedule>
					<!--- if Hour < schedule option start then set sent out reminder on  today --->
					<cfset ScheduleDateBuffer =  "#DateFormat(ScheduleDateBuffer,'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00" />
				</cfif>
				<cfset inpQueuedScheduledDate = "#DATEADD("d", - LSParseNumber(RetValGetCalendarConfiguration.INTERVALVALUE), ScheduleDateBuffer)#">

			<cfelse><!--- Interval type is Hour --->

				<cfset ScheduleDateBuffer = "#DateAdd("h", - LSParseNumber(RetValGetCalendarConfiguration.INTERVALVALUE), ScheduleDateBuffer)#">
				<cfset HourCheck = LSParseNumber(LSTimeFormat(ScheduleDateBuffer,'H'))>
				<cfif HourCheck GTE EndHourSchedule>
					<!--- if Hour >= schedule option end then set sent out reminder on next day --->
					<cfset ScheduleDateBuffer =  "#DateFormat(DATEADD("d", 1, ScheduleDateBuffer),'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
				<cfelseif HourCheck LT StartHourSchedule>
					<!--- if Hour < schedule option start then set time sent out reminder on  today --->
					<cfset ScheduleDateBuffer =  "#DateFormat(ScheduleDateBuffer,'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
				</cfif>
				<cfset inpQueuedScheduledDate = ScheduleDateBuffer>
			</cfif>
			<!--- Compare sent out with NOW() --->
			<cfset Checkdatecompare = DateCompare('#inpQueuedScheduledDate#', getScheduleOptionInfor.CURRENTTIME)>
			<cfif Checkdatecompare GTE 0> <!--- sent out later NOW() --->

				<cfset inpQueuedScheduledDate = "#DateFormat(inpQueuedScheduledDate,'yyyy-mm-dd')# #LSTimeFormat(inpQueuedScheduledDate,'HH:mm:ss')#">
				<cfset inpQueuedScheduledDate_txt ="#DateFormat(inpQueuedScheduledDate,'yyyy-mm-dd')# #LSTimeFormat(inpQueuedScheduledDate,'hh:mm tt')# (PST)">
				<cfset inpQueuedScheduledDateTimeZone_txt ="#DateFormat(DateAdd("h", 31 - LSParseNumber(CurrTZ), inpQueuedScheduledDate),'yyyy-mm-dd')# #LSTimeFormat(DateAdd("h", 31 - LSParseNumber(CurrTZ), inpQueuedScheduledDate),'hh:mm tt')# (Customer time)">
			<cfelse><!--- sent out earlier NOW() --->

				<cfset HourCheck = LSParseNumber(LSTimeFormat(getScheduleOptionInfor.CURRENTTIME,'H'))>
				<cfif HourCheck GTE EndHourSchedule>
					<!--- if Hour >= schedule option end then set sent out reminder on next day --->
					<cfset ScheduleDateBuffer =  "#DateFormat(DATEADD("d", 1, getScheduleOptionInfor.CURRENTTIME),'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
				<cfelseif HourCheck LT StartHourSchedule>
					<!--- if Hour < schedule option start then set time sent out reminder on  today --->
					<cfset ScheduleDateBuffer =  "#DateFormat(getScheduleOptionInfor.CURRENTTIME,'yyyy-mm-dd')# #StartHourSchedule_txt#:00:00">
				<cfelse>
					<!--- StartHourSchedule < sent out valid < EndHourSchedule --->
					<cfset ScheduleDateBuffer =  "#DateFormat(getScheduleOptionInfor.CURRENTTIME,'yyyy-mm-dd')# #LSTimeFormat(getScheduleOptionInfor.CURRENTTIME,'hh:mm tt')#">
				</cfif>

				<cfset inpQueuedScheduledDate = "#DateFormat(ScheduleDateBuffer,'yyyy-mm-dd')# #LSTimeFormat(ScheduleDateBuffer,'HH:mm:ss')#">
				<cfset inpQueuedScheduledDate_txt ="#DateFormat(inpQueuedScheduledDate,'yyyy-mm-dd')# #LSTimeFormat(inpQueuedScheduledDate,'hh:mm tt')# (PST)">
				<cfset inpQueuedScheduledDateTimeZone_txt ="#DateFormat(DateAdd("h", 31 - LSParseNumber(CurrTZ), inpQueuedScheduledDate),'yyyy-mm-dd')# #LSTimeFormat(DateAdd("h", 31 - LSParseNumber(CurrTZ), inpQueuedScheduledDate),'hh:mm tt')# (Customer time)">
			</cfif>

			<cfset dataout.RXRESULT = inpQueuedScheduledDate>

			<cfif RetValGetCalendarConfiguration.ASR EQ 0><!--- Auto Schedule OFF --->
				<cfset dataout.TYPE = 2 />
				<cfset dataout.RXRESULTCODE = 1 />
				<cfif CurrTZ EQ '31'>
					<cfset dataout.MESSAGE = '<h4 class="be-modal-title">WARNING REMINDER</h4><p>The reminder will sent out on:</p><p>' & inpQueuedScheduledDate_txt & '<p>Do you want to schedule a reminder?</p>' />
				<cfelse>
					<cfset dataout.MESSAGE = '<h4 class="be-modal-title">WARNING REMINDER</h4><p>The reminder will sent out on:</p><p>' & inpQueuedScheduledDate_txt & '<br>' & inpQueuedScheduledDateTimeZone_txt & '</p><p>Do you want to schedule a reminder?</p>' />
				</cfif>
				<cfreturn dataout />
			<cfelse><!--- Auto Schedule ON --->
				<cfset dataout.TYPE = 1 />
				<cfset dataout.RXRESULTCODE = 1 />
				<cfif CurrTZ EQ '31'>
					<cfset dataout.MESSAGE = '<h4 class="be-modal-title">WARNING REMINDER</h4><p>The reminder will be scheduled and sent out on:</p><p>' & inpQueuedScheduledDate_txt & '</p><p>' & Confirmquestion & '</p>' />
				<cfelse>
					<cfset dataout.MESSAGE = '<h4 class="be-modal-title">WARNING REMINDER</h4><p>The reminder will be scheduled and sent out on:</p><p>' & inpQueuedScheduledDate_txt & '<br>' & inpQueuedScheduledDateTimeZone_txt & '</p><p>' & Confirmquestion & '</p>' />
				</cfif>
				 <cfreturn dataout />
			</cfif>

			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = "-2" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>

        </cftry>
    </cffunction>

	<cffunction name="UpdateChatCloseStatus" access="remote" output="false" hint="Update manual Event status to Chat Close">
		<cfargument name="inpSessionId" required="false" type="numeric" hint="Session Id of Event" default="0"/>

		<cfset var dataout = {} />
		<cfset var UpdateChatNotes = '' />
		<cfset var GetEventByChatSessionId = ''/>
		<cfset var UpdateChatCloseStatus = ''/>

		<cfset dataout.RXRESULTCODE = 0/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<cfif inpSessionId NEQ 0>
				<cfquery name="GetEventByChatSessionId" datasource="#Session.DBSourceEBM#">
					SELECT
						PKId_bi
					FROM
						calendar.events
					WHERE
						ChatSessionId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSessionId#" />
					AND
						Active_int = 1
					AND
						ConfirmationFlag_int = #APPT_REMINDER_CHAT_ACTIVE#
				</cfquery>

				<cfif GetEventByChatSessionId.RecordCount GT 0>
					<cfquery name="UpdateChatCloseStatus" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = #APPT_REMINDER_CHAT_CLOSE#
						WHERE
							PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventByChatSessionId.PKId_bi#" />
						AND
							Active_int = 1
					</cfquery>
					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.MESSAGE = 'Update successfully.' />
					<cfset dataout.ERRMESSAGE = '' />
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="ReactiveChatSession" access="remote" output="false" hint="Update manual Event status">
		<cfargument name="inpSessionId" required="false" type="numeric" hint="Event chat notes" default="0"/>

		<cfset var dataout	= {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var updateReactiveSession = "" />
		<cfset var UpdateChatActiveStatus = "" />
		<cfset var GetEventByChatSessionId = ""/>
		<cfset var rxGetCalendarConfiguration = {} />

		<cftry>

			<!--- Close session chat --->
			<cfif arguments.inpSessionId NEQ 0>
				<cfquery name="updateReactiveSession" datasource="#Session.DBSourceEBM#">
					UPDATE
						`simplequeue`.`sessionire`
					SET
						SessionState_int = 1,
						LastUpdated_dt = NOW(),
						LastCP_int = 1,
						XMLResultString_vch = ''
					WHERE
						SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSessionId#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
					AND
						SessionState_int = 4
				</cfquery>

				<!--- Update event status to Chat Active --->
				<cfquery name="GetEventByChatSessionId" datasource="#Session.DBSourceEBM#">
					SELECT
						PKId_bi
					FROM
						calendar.events
					WHERE
						ChatSessionId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSessionId#" />
					AND
						Active_int = 1
				</cfquery>

				<cfif GetEventByChatSessionId.RecordCount GT 0>
					<cfquery name="UpdateChatActiveStatus" datasource="#Session.DBSourceEBM#">
						UPDATE
							calendar.events
						SET
							ConfirmationFlag_int = #APPT_REMINDER_CHAT_ACTIVE#
						WHERE
							PKId_bi = <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventByChatSessionId.PKId_bi#" />
						AND
							Active_int = 1
					</cfquery>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = 'Update successfully.' />
					<cfset dataout.ERRMESSAGE = '' />
				</cfif>
			</cfif>

		    <cfcatch TYPE="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>
		</cftry>
		<cfreturn dataout/>
	</cffunction>

</cfcomponent>
