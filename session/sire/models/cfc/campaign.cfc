<cfcomponent>
	<cfinclude template="/public/paths.cfm">
	<cfinclude template="/session/administration/constants/userConstants.cfm">
	<cfinclude template="/session/administration/constants/scheduleConstants.cfm">
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/session/sire/configs/paths.cfm">
	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="encodeStringXml" access="public" returntype="string" output="false" hint="Encode several special characters that make xml invalid">
		<cfargument	name="string" type="string" required="true" hint="" />

		<cfset arguments.string = Replace(arguments.string, '&', '&amp;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '"', '&quot;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '<', '&lt;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '>', '&gt;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, "'", '&apos;', 'ALL') />
		<cfreturn string />
	</cffunction>

	<cffunction name="decodeStringXml" access="public" returntype="string" 	output="false" hint="Decode xml string">
		<cfargument name="string" type="any" required="true" hint="" />
		<cfset arguments.string = Replace(arguments.string, '&amp;', '&', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&quot;', '"', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&lt;', '<', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&gt;', '>', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&apos;', "'", 'ALL') />
		<cfreturn string />
	</cffunction>

	<cffunction name="CreateCampaign" access="remote" output="true" hint="Create campaign">
		<cfargument name="campaignName" type="string" required="yes">

		<cfset var INPGROUPID	= '' />
		<cfset var addGroupResult	= '' />
		<cfset var RetVarAddNewBatch	= '' />

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
		<cftry>
			<!---<cfinvoke method="addgroup" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="addGroupResult">
				<cfinvokeargument name="inpGroupDesc" value="#ar campaignName#"/>
			</cfinvoke>

			<cfif addGroupResult.RXRESULTCODE GT 0>
				<cfset INPGROUPID = addGroupResult.INPGROUPID>
			<cfelse>
				<cfset INPGROUPID = 0>
	        </cfif>--->

	        <cfinvoke method="AddNewBatch" component="#Session.SessionCFCPath#.distribution" returnvariable="RetVarAddNewBatch">
	            <cfinvokeargument name="INPBATCHDESC" value="#arguments.campaignName#">
	            <cfinvokeargument name="operator" value="#Keyword_Action_Title#">
	            <cfinvokeargument name="INPALLOWDUPLICATES" value="1">
	            <!---<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">--->
	        </cfinvoke>

	        <cfif RetVarAddNewBatch.RXRESULTCODE LT 0>
	            <cfthrow MESSAGE="#RetVarAddNewBatch.MESSAGE#" TYPE="Any" detail="#RetVarAddNewBatch.ERRMESSAGE#" errorcode="-2">
	        <cfelse>
	        	<cfset dataout.BATCHID = "#RetVarAddNewBatch.NEXTBATCHID#" />
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = "Create successfully" />
	        </cfif>
        <cfcatch>
        	<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.BATCHID = "0" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

	    <cfreturn dataout />
	</cffunction>


	<cffunction name="RenameCampaign" access="remote" output="true" hint="re-name campaign">
		<cfargument name="campaignName" type="string" required="yes">
		<cfargument name="campaignId" type="number" required="yes">

		<cfset var RetVarRenameBatch	= '' />

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
		<cftry>
	        <cfinvoke method="UpdateBatchDesc" component="#Session.SessionCFCPath#.distribution" returnvariable="RetVarRenameBatch">
	            <cfinvokeargument name="INPBATCHID" value="#arguments.campaignId#">
	            <cfinvokeargument name="Desc_vch" value="#arguments.campaignName#">

	        </cfinvoke>

	        <cfif RetVarRenameBatch.RXRESULTCODE LT 0>
	            <cfthrow MESSAGE="#RetVarRenameBatch.MESSAGE#" TYPE="Any" detail="#RetVarRenameBatch.ERRMESSAGE#" errorcode="-2">
	        <cfelse>
	        	<cfset dataout.BATCHID = "#RetVarRenameBatch.INPBATCHID#" />
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = "Rename successfully" />
	        </cfif>
        <cfcatch>
        	<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.BATCHID = "0" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

	    <cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCampaign" access="remote" output="true" hint="Update campaign">
		<cfargument name="campaignId" type="number" required="yes">

		<cfset var deletaQueueQuery	= '' />
		<cfset var deletaBatchQuery	= '' />
		<cfset var keywords	= '' />
		<cfset var deleteKeyword	= '' />
		<cfset var deletaBatchQueryResult	= '' />
		<cfset var deleteAllKeyword	= '' />
		<cfset var deleteAllKeywordResult = ''/>
		<cfset var deleteBlastBatch = ''/>
		<cfset var selectCaptureBatchId = ''/>
		<cfset var updateCaptureBatchId = ''/>
		<cfset var updateCaptureBatchIdResult = ''/>
		<cfset var deleteSubscriberBatch = ''/>
		<cfset var updateDTSStatusIdrefund = ''/>
		<cfset var Getcreditsrefund = ''/>
		<cfset var updaterefundcredits = ''/>
		<cfset var string_log = "Campaign deleted"/>

		<cfset var checkplancredit = 0/>
		<cfset var balance_credits = 0/>
		<cfset var balance_buy_credits  = 0/>
		<cfset var balance_promotion_credits  = 0/>
		<cfset var credits_refund = 0 />
		<cfset var myxmldocResultDoc = ''>
		<cfset var AllWebServicdAndDelays = ''>
		<cfset var GetCurrentBalancecredits = ''>
		<cfset var RetUserPlan = ''>

		<cfset var SMSOUT_RECORD_REMOVED = '9'/>
		<cfset var SMSOUT_QUEUED = '1'/>
		<cfset var SMSOUT_PAUSED = '0'/>

		<cfset var UpdateSMSQueue = ''/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

		<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

	   	<cftransaction action="begin">
		   	<cftry>

				<cfquery name="deleteSubscriberBatch" datasource="#Session.DBSourceEBM#">
					DELETE FROM
						simplelists.subscriberbatch
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
				</cfquery>

				<cfquery name="deleteSubscriberBatch" datasource="#Session.DBSourceEBM#">
					UPDATE simpleobjects.batch_blast_log SET Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PAUSED#">
					WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
						AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfquery>

	            <cfquery name="deletaBatchQuery" datasource="#Session.DBSourceEBM#" result="deletaBatchQueryResult">
	                UPDATE simpleobjects.batch SET Active_int = 0
	                WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.campaignId#">
						AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>
			<!---
	            <cfquery name="selectCaptureBatchId" datasource="#Session.DBSourceEBM#">
	            	SELECT
	            		CaptureBatchId_int
	            	FROM
	            		simpleobjects.waitlist
	            	WHERE
	            		CaptureBatchId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
	            </cfquery>

	            <cfif selectCaptureBatchId.RecordCount GT 0>
	            	<cfquery name="updateCaptureBatchId" datasource="#Session.DBSourceEBM#" result="updateCaptureBatchIdResult">
	            		UPDATE
	            			simpleobjects.waitlist
	            		SET
	            			CaptureBatchId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
	            		WHERE
	            			CaptureBatchId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
	            		AND
	            			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            	</cfquery>

	            	<cfif updateCaptureBatchIdResult.RecordCount GT 0>
	            		<cfset dataout.UPDATECAPTURERESULT = 1 />
	            		<cfset dataout.UPDATECAPTUREMESS = 'Update capture batch id successfully'/>
	            	<cfelse>
	            		<cfset dataout.UPDATECAPTURERESULT = 0 />
	            		<cfset dataout.UPDATECAPTUREMESS = 'Update capture batch id failed'/>
	            	</cfif>
	            </cfif>
			--->
				<cfif deletaBatchQueryResult.RecordCount EQ 1>
					<!--- Delete all inactive keyword --->
					<cfquery name="deleteAllKeyword" datasource="#Session.DBSourceEBM#" result="deleteAllKeywordResult">
						DELETE FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.campaignId#"> AND Active_int = 0
					</cfquery>

		        	<cfinvoke component="session.cfc.csc.csc" method="GetKeywordFromBatchId" returnvariable="keywords" >
						<cfinvokeargument name="inpBatchId" value="#arguments.campaignId#">
					</cfinvoke>

					<cfinvoke component="session.cfc.csc.csc" method="DeleteKeyword" returnvariable="deleteKeyword" >
						<cfinvokeargument name="keywordId" value="#keywords.KEYWORDID#">
					</cfinvoke>

				</cfif>
				<!--- Refund Credits when remove campaign --->
					<!--- Update DTSStatusId = 9--->

				<cfquery name="GetCurrentBalancecredits" datasource="#Session.DBSourceEBM#">
                    SELECT
                        Balance_int,
                        BuyCreditBalance_int,
                        PromotionCreditBalance_int,
                        UnlimitedBalance_ti,
                        RATETYPE_int,
                        RATE1_int,
                        RATE2_int,
                        RATE3_int
                    FROM
                       simplebilling.billing
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                </cfquery>


				<cfquery name="updateDTSStatusIdrefund" datasource="#Session.DBSourceEBM#">
					SET tx_isolation = 'READ-COMMITTED';
					UPDATE
						simplequeue.contactqueue
					SET
						DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						(DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_QUEUED#"> OR DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_PAUSED#">)
				</cfquery>
					<!--- Get Credits refund--->
				<cfquery name="Getcreditsrefund" datasource="#Session.DBSourceEBM#">
					SELECT
						XMLControlString_vch
					FROM
						simplequeue.contactqueue
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#">
				</cfquery>

				<!--- Refund credits for only user limited balance--->
				<cfif GetCurrentBalancecredits.UnlimitedBalance_ti eq 0>
					<cfif Getcreditsrefund.RECORDCOUNT GT 0>
						<cfset credits_refund = 0>

						<cfloop query = "Getcreditsrefund">
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>#Getcreditsrefund.XMLControlString_vch#</XMLControlStringDoc>") />
							<cfset AllWebServicdAndDelays = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='12' or @RXT='19'  ]") />
							<cfset credits_refund = credits_refund +  Arraylen(AllWebServicdAndDelays)>

							<cfset myxmldocResultDoc = "">
							<cfset AllWebServicdAndDelays = "">
						</cfloop>

						<cfif credits_refund GT 0>
							<cfif GetCurrentBalancecredits.RECORDCOUNT GT 0>
								<cfset checkplancredit = RetUserPlan.FIRSTSMSINCLUDED - credits_refund - GetCurrentBalancecredits.Balance_int />
								<cfif checkplancredit lt 0>
									<cfset balance_credits = RetUserPlan.FIRSTSMSINCLUDED - GetCurrentBalancecredits.Balance_int />
									<cfset balance_promotion_credits = credits_refund - RetUserPlan.FIRSTSMSINCLUDED />
								<cfelse>
									<cfset balance_credits = credits_refund />
									<cfset balance_promotion_credits = 0 />
								</cfif>

								<cfquery name="updaterefundcredits" datasource="#Session.DBSourceEBM#">
									UPDATE
										simplebilling.billing
									SET
										Balance_int = Balance_int + #balance_credits#,
										PromotionCreditBalance_int = PromotionCreditBalance_int + #balance_promotion_credits#
									WHERE
										userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#Session.USERID#">
								</cfquery>
							</cfif>
							<cfset string_log = string_log & ", Plan(max): #RetUserPlan.FIRSTSMSINCLUDED# credits, Refund #credits_refund# credits: Plan #balance_credits# credits and Promotion #balance_promotion_credits# credits.">
						</cfif>
					</cfif>
				</cfif>

				<!--- Terminate any intervals queued up --->
				<cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
					UPDATE
						simplequeue.moinboundqueue
					SET
						Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_CAMPAIGN_DELETED#">
					WHERE
					(
						Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_QA_TOOL_READYTOPROCESS#">
						OR
						Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_READYTOPROCESS#">
					)
					AND
						SessionId_bi IN (SELECT SessionId_bi FROM simplequeue.sessionire  WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">)
				</cfquery>

				<!--- AND SessionState_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IRESESSIONSTATE_INTERVAL_HOLD#"> --->

	            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="SMS BatchId = #arguments.campaignId#">
					<cfinvokeargument name="operator" value="#string_log#">
				</cfinvoke>
				<cftransaction action="commit">
	        	<cfset dataout.BATCHID = "#arguments.campaignId#" />
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = "Delete successfully" />
	        <cfcatch>
	        	<cftransaction action="rollback">
	        	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.BATCHID = "0" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	        </cfcatch>
	        </cftry>
        </cftransaction>

	    <cfreturn dataout />
	</cffunction>

	<cffunction access="remote" name="deleteCampaign" >
		<cfargument name="inpBatchIDS" type="string" required="yes">
		<cfset var idlist = DeserializeJSON(arguments.inpBatchIDS) />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset var RetValUpdateCampaign = '' />
	   	<cfset var campaignid = ''/>
		<cfset var UPDATESURVEYSTATE = ''/>


		<cftry>
			<cfif isarray(idlist) AND idlist.len() GT 0>
				<cfloop array="#idlist#" index="campaignid">
					<cfinvoke method="UpdateCampaign" returnvariable="RetValUpdateCampaign">
						<cfinvokeargument name="campaignId" value="#campaignid#">
					</cfinvoke>

					<cfif RetValUpdateCampaign.RXRESULTCODE LT 0>
						<cfthrow MESSAGE="#RetValUpdateCampaign.MESSAGE#" TYPE="Any" detail="#RetValUpdateCampaign.ERRMESSAGE#" errorcode="-2">
					</cfif>

				</cfloop>
				<cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.sessionire
                    SET
                        SessionState_int = 4,
                        LastUpdated_dt = NOW()
                    WHERE
                        BatchId_bi in(<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arraytolist(idlist)#" list="yes" >  )
                </cfquery>
				<cfset dataout.RESULT = RetValUpdateCampaign/>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = "Delete successfully" />
			</cfif>
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction access="remote" name="deleteCampaignSimon" >
		<cfargument name="inpBatchIDS" type="string" required="yes">
		<cfset var idlist = DeserializeJSON(arguments.inpBatchIDS) />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset var RetValUpdateCampaign = '' />

		<cftry>
			<cfif idlist GT 0>
	        	<cfinvoke method="UpdateCampaign" returnvariable="RetValUpdateCampaign">
					<cfinvokeargument name="campaignId" value="#idlist#">
				</cfinvoke>
				<cfset dataout.RESULT = RetValUpdateCampaign/>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = "Delete successfully" />
			</cfif>
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetCampaign" access="remote" output="true" hint="get campaign by campaignid">
		<cfargument name="campaignId" type="number" required="yes">
		<cfargument name="userId" type="number" required="no" default="0">

		<cfset var dataout	= {} />
		<cfset var getbatchDetail	= '' />

		<cfquery name="getbatchDetail" datasource="#session.DBSourceREAD#">
        	SELECT *
            FROM simpleobjects.batch
            WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">
            <cfif arguments.userId GT 0>
            	AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userId#">
            </cfif>
        </cfquery>

		<cfset dataout.RXRESULTCODE = 1>
		<cfset dataout.BatchObject = getbatchDetail>
		<cfset dataout.MESSAGE = "Get detail batch successfully.">

		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetCampaignList" access="remote" output="true" hint="Get list campaign">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">

		<cfset var emsLists	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetEMSStatus	= '' />
		<cfset var getKeywords	= '' />

		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetEMS = 0>
		<cfset var total_pages = 1>
		<cfset var ownerType = 0 >
		<cfset var ownerId = 0 >
		<cfset var RecipientCount = 0>
		<cfset var tempItem  = ArrayNew(1) >
		<cfset var GetNumbersCount  = 0 >
		<cfset var iFlashCount  = 0 >
		<cfset var PlayMyMP3_BD  = 0 >
		<cfset var PlayVoiceFlashHTML5  = 0 >
		<cfset var stopHtml  = 0 >
		<cfset var editHtml  = 0 >
		<cfset var cloneHtml  = 0 >
		<cfset var deleteHtml  = "" >
		<cfset var permissionStr  = 0 >
		<cfset var getUserByUserId  = 0 >
		<cfset var stopPermission  = 0 >
		<cfset var launchPermission  = 0 >
		<cfset var deletePermission  = 0 >
		<cfset var clonePermission  = 0 >
		<cfset var editPermission  = 0 >
		<cfset var order 	= "">
		<cfset var PausedCount 		= "">
		<cfset var PendingCount 		= "">
		<cfset var InProcessCount 		= "">
		<cfset var CompleteCount 		= "">
		<cfset var ContactResultCount 		= "">
		<cfset var IsScheduleInFuture 		= "">
		<cfset var IsScheduleInPast 		= "">
		<cfset var IsScheduleInCurrent 		= "">
		<cfset var LastUpdatedFormatedDateTime 		= "">
		<cfset var startDate 	= "">
		<cfset var endDate 	= "">
		<cfset var ContactTypes 		= "">
		<cfset var statusHtml 	= "">
		<cfset var ImageStatus 	= "">
		<cfset var StatusName 		= "">
		<cfset var errorMessage 		= "">
		<cfset var reportHtml 		= "">
		<cfset var GetCampaignSchedule 	= "">
		<cfset var GetPausedCount 		= "">
		<cfset var GetPendingCount 	= "">
		<cfset var GetInProcessCount 		= "">
		<cfset var GetCompleteCount 		= "">
		<cfset var GetContactResult 		= "">
		<cfset var GetDelivered 	= "">
		<cfset var GetListElligableGroupCount_ByType 		= "">
		<!---for checking and disabling none method EMS--->
		<cfset var disableEmailImage  = "" >
		<cfset var disableVoiceImage  = "" >
		<cfset var disableSmsImage  = "" >
        <cfset var DisplayDeliveredCount = "0" />
        <cfset var DisplayQueuedCount = "0" />
        <cfset var reviewHtml = "" />
        <cfset var LoadHtml = ""/>
        <cfset var BatchDescLink = '' />
        <cfset var batchListHasKeyword = ''>
        <cfset var KeywordBuff = '' />
        <cfset var DescBuff = '' />
        <cfset var getOptinout = '' />
        <cfset var GetQueueStatus = '' />
        <cfset var emsIds = []>
        <cfset var shortcode = '' />
        <cfset var GetSessionActive = '' />
		<cfset var Incontactque = 0/>
		<cfset var TotalSMS = 0/>
		<cfset var GetNumberContactQue = {}/>
		<cfset var GetNumberContactSend = {}/>
		<cfset var GetScheduleResult = {}/>
		<cfset var campaignSchedule = ''/>
		<cfset var rowSchedule = ''/>
		<cfset var totalChat = 0 />

		<cftry>
			<cfinvoke method="BuildSortingParamsForDatatable" component="#Session.SessionCFCPath#.emergencymessages" returnvariable="order">
				<cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
				<cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
				<cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
				<cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
			</cfinvoke>


			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(b.BatchId_bi) AS TOTALCOUNT
				FROM
					simpleobjects.batch AS b
					LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1)
				WHERE
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					b.Active_int > 0
				AND
					b.EMS_Flag_int IN (0,1,2)
				AND
					b.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				LIMIT 1
			</cfquery>

			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>


		<!--- LEFT JOIN simpleobjects.batch_blast_log bl ON (bl.BatchId_bi = b.BatchId_bi) --->

		        <!--- Get ems data --->
				<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
					SELECT
						b.BatchId_bi,
		             	b.RXDSLibrary_int,
		             	b.RXDSElement_int,
		             	b.RXDSScript_int,
		                b.DESC_VCH,
		                b.Created_dt,
		                b.ContactGroupId_int,
					 	b.ContactTypes_vch,
					 	b.ContactNote_vch,
					 	b.ContactIsApplyFilter,
					 	b.ContactFilter_vch,
	                    b.EMS_Flag_int,
	                    b.TemplateId_int,
						b.TemplateType_ti,
						b.IsFinishedCampaign_ti,
	                    k.KeywordId_int,
	                    k.Keyword_vch,
	                    gl.GroupName_vch,
	                    tc.Name_vch AS TemplateName,
	                    0 AS totalOptin,
	                    0 AS totalOptout,
						mlp.ccpxDataId_int,
						mlp.cppxURL_vch
					FROM
						simpleobjects.batch AS b
						LEFT JOIN simpleobjects.templatecampaign tc ON b.TemplateId_int = tc.TID_int
						LEFT JOIN simplelists.subscriberbatch sb ON b.batchid_bi = sb.batchid_bi
						LEFT JOIN simplelists.grouplist gl ON sb.GroupId_bi = gl.GroupId_bi
						LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1)
						LEFT JOIN simplelists.cppx_data AS mlp ON b.batchid_bi = mlp.BatchId_bi
					WHERE
						b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
					AND
						b.Active_int > 0
					AND
						b.EMS_Flag_int IN (0,1,2)
					AND
						b.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
				   <cfif customFilter NEQ "">
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
							AND
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
					GROUP BY b.BatchId_bi
					#order#
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
		        </cfquery>


	        	<cfloop query="GetEMS">
	        		<cfset arrayAppend(emsIds,BatchId_bi)>
	        	</cfloop>

<!--- 	        	<cfif emsIds.len() GT 0>

					<cfquery name="getOptinout" datasource="#Session.DBSourceREAD#">
						SELECT
							OptInSourceBatchId_bi AS Batch_id,
							COUNT(IF(Optin_int > 0, 1, NULL)) AS totalOptin,
	                    	COUNT(IF(Optout_int > 0, 1, NULL)) AS totalOptout
						FROM
							simplelists.contactstring
						WHERE
							simplelists.contactstring.OptInSourceBatchId_bi IN (#arrayToList(emsIds, ',')#)
						GROUP BY OptInSourceBatchId_bi
					</cfquery>

					<cfif getOptinout.RecordCount GT 0>
						<cfloop query="getOptinout">
							<cfloop query="GetEMS">
								<cfif getOptinout.Batch_id EQ GetEMS.BatchId_bi>
									<cfset GetEMS.totalOptin = getOptinout.totalOptin>
									<cfset GetEMS.totalOptout = getOptinout.totalOptout>
								<cfelse>
									<cfset GetEMS.totalOptin = 0>
									<cfset GetEMS.totalOptout = 0>
								</cfif>
							</cfloop>
						</cfloop>
					</cfif>

				</cfif> --->

	        	<cfset emsLists = {}>

	        	<cfif arrayLen(emsIds) GT 0>

					<cfquery name="GetQueueStatus" datasource="#Session.DBSourceREAD#">
						SELECT
							BatchId_bi,
							count(PKId_int) as total,
							COUNT(AmountLoaded_int) AS TOTALSMS,
							COUNT(ActualLoaded_int) AS INCONTACTQUE,
							PKId_int
						FROM
							simpleobjects.batch_blast_log
						WHERE
							Status_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_LOADING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PROCESSING#">)
						GROUP BY
							BatchId_bi
						HAVING
							BatchId_bi IN (#arrayToList(emsIds, ',')#)
					</cfquery>

		        	<cfif GetQueueStatus.RECORDCOUNT GT 0>
						<cfloop query="GetQueueStatus">
			        		<cfif total GT 0>
			        			<cfset emsLists[BatchId_bi] = PKId_int>
			        		</cfif>
			        	</cfloop>
		        	</cfif>

				</cfif>

	        	<cfloop query="GetEMS">
	        		<cfif TemplateId_int EQ 9> <!--- check session state for chat campaign only --->
	        			<!--- get session status of batch id--->
						<cfquery name="GetSessionActive" datasource="#Session.DBSourceREAD#">
							SELECT
								ire.BatchId_bi,
								ss.SessionId_bi,
								ss.SessionState_int,
								ss.ContactString_vch
							FROM
								simplexresults.ireresults ire
								JOIN
								simplequeue.sessionire ss
							ON
									ire.IRESessionId_bi = ss.SessionId_bi
							WHERE
									ire.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BatchId_bi#">
								AND
									ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
								AND
									ire.IREType_int = 2
								AND
									LENGTH(ire.ContactString_vch) < 14
								AND
									ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
								AND
									ss.SessionState_int =1
							GROUP BY
								ire.BatchId_bi,
								ss.SessionId_bi,
								ss.SessionState_int,
								ss.ContactString_vch
						</cfquery>
						<cfset totalChat = GetSessionActive.RECORDCOUNT />
	        		</cfif>

		        	<cfif TRIM(GetEMS.Keyword_vch) NEQ "" >
		        		<cfset KeywordBuff = TRIM(GetEMS.Keyword_vch)/>
		        	<cfelse>
		        		<cfset KeywordBuff = '' />
		        	</cfif>

		        	<cfif TemplateType_ti EQ 1>
						<!--- Get number incontactque/total sms --->
						<cfquery name="GetNumberContactQue" datasource="#Session.DBSourceREAD#">
							SELECT
								SUM(AmountLoaded_int) AS TOTALSMS,
								SUM(ActualLoaded_int) AS INCONTACTQUE
							FROM
								simpleobjects.batch_blast_log
							WHERE
								Status_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_LOADING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PROCESSING#">)
							AND
								BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BatchId_bi#">
						</cfquery>
						<cfif GetNumberContactQue.RECORDCOUNT GT 0>
							<cfset Incontactque = GetNumberContactQue.INCONTACTQUE>
							<cfset TotalSMS = GetNumberContactQue.TOTALSMS/>
						<cfelse>
							<cfset Incontactque = 0>
							<cfset TotalSMS = 0/>
						</cfif>
					</cfif>

					<!--- get total msg send --->
					<cfquery name="GetNumberContactSend" datasource="#Session.DBSourceREAD#">
						SELECT
							COUNT(IF(DTSStatusType_ti = 5, 1, NULL)) AS totalSend,
    						COUNT(DTSId_int) AS TotalInQueue
						FROM
							simplequeue.contactqueue
						WHERE
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BatchId_bi#">
					</cfquery>

					<!--- get campaign schedule --->

					<!--- <cfinvoke method="GetSchedule" component="session.sire.models.cfc.schedule" returnvariable="GetScheduleResult">
						<cfinvokeargument name="INPBATCHID" value="#BatchId_bi#"/>
					</cfinvoke>
					<cfif GetScheduleResult.RXRESULTCODE EQ 1>
						<cfset rowSchedule = GetScheduleResult.ROWS/>
						<cfset campaignSchedule = rowSchedule[1].START_DT&" to "&rowSchedule[1].STOP_DT&"<br/> Time(hour): "&rowSchedule[1].STARTHOUR_TI&" - "&rowSchedule[1].ENDHOUR_TI>
					</cfif> --->

					<cfset tempItem = {
					ID = '#BatchId_bi#',
					NAME =	'#DESC_VCH#',
					ISFINISHED='#IsFinishedCampaign_ti#',
					KEYWORD = '#KeywordBuff#',
					GROUPNAME = "#GetEMS.GroupName_vch#",
					TEMPNAME = "#GetEMS.TemplateName#",
					INCONTACTQUE = '#NUMBERFORMAT(Incontactque)#',
					TOTALSMS = '#NUMBERFORMAT(TotalSMS)#',
					// TOTALOPTIN = "#GetEMS.totalOptin#",
					// TOTALOPTOUT = "#GetEMS.totalOptout#",
					TEMPLATEID = "#GetEMS.TemplateId_int#",
					TEMPLATETYPE="#GetEMS.TemplateType_ti#",
					INPROCESS = "#structKeyExists(emsLists, BatchId_bi) ? emsLists[BatchId_bi] : 0#",
					TOTALCHAT= totalChat,
					TOTALSEND =  GetNumberContactSend.totalSend,
					TOTALINQUEUE =  GetNumberContactSend.TotalInQueue,
					SCHEDULE = '',
					MLPID = '#ccpxDataId_int#',
					MLPURL = '#cppxURL_vch#'
					} />
					<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
				</cfloop>
	        </cfif>
      	<cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataOut>
	</cffunction>

	<cffunction name="checkCampaignStatus" access="remote" output="true" hint="Check campaign status">
		<cfargument name="inpBatchId" type="number" required="yes" default="0">

		<cfset var dataout	= {} />
		<cfset var hasKeyword	= '' />
		<cfset var hasQueue	= '' />
		<cfset var queueStatus	= '' />
		<cfset var campaignStatus	= '' />
		<cfset var GetEMSStatus	= '' />
		<cfset var keyword	= '' />

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.STATUS = ''>

		<cfif arguments.inpBatchId GT 0>
		    <cftry>
		    	<cfset hasKeyword =0>
		    	<cfset hasQueue =0>
		    	<!--- GET KEYWORD --->
		    	<cfinvoke component="session.sire.models.cfc.keywords" method="GetKeywordByBatchId" returnvariable="keyword">
		    		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
				</cfinvoke>

				<cfif keyword.RXRESULTCODE EQ 1>
					<cfif arrayLen(keyword.KEYWORD) GT 0 >
						<cfset hasKeyword =1>
					</cfif>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.MESSAGE = "Get Keyword Errors" />
	   		    	<cfset dataout.ERRMESSAGE = "Get Keyword Errors" />
	   		    	<cfreturn dataout />
				</cfif>

	        	<!--- Get ems Status --->
				<cfquery name="GetEMSStatus" datasource="#Session.DBSourceREAD#">
					SELECT BatchId_bi,
						BIT_OR(IF(DTSStatusType_ti IN (1,2,3), 1, 0)) AS `BatchScheduled`,
						BIT_OR(IF(DTSStatusType_ti = 0, 1, 0)) AS `BatchPause`,
						BIT_AND(IF(DTSStatusType_ti > 3, 1, 0)) AS `BatchFinished`
					FROM simplequeue.contactqueue
					GROUP BY BatchId_bi
					HAVING BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
	        	</cfquery>

	        	<cfif GetEMSStatus.RECORDCOUNT GT 0>
					<cfset hasQueue = 1>
	        	</cfif>

	        	<cfloop query="GetEMSStatus">
		        	<cfif BatchScheduled EQ 1>
		        		<cfset queueStatus = 'Scheduled'>
	        		<cfelseif BatchPause EQ 1>
	        			<cfset queueStatus = 'Pause'>
	        		<cfelseif BatchFinished EQ 1>
	        			<cfset queueStatus = 'Finished'>
		        	</cfif>
		        </cfloop>

		        <cfif hasKeyword EQ 1 AND hasQueue EQ 0>
		        	<cfset campaignStatus = 'Keyword'>
		        <cfelseif hasKeyword EQ 1 AND hasQueue EQ 1>
		        	<cfif queueStatus EQ 'Finished'>
		        		<cfset campaignStatus = 'Running'>
		        	<cfelse>
		        		<cfset campaignStatus = queueStatus>
		        	</cfif>
		        <cfelseif hasKeyword EQ 0 AND hasQueue EQ 1>
		        	<cfset campaignStatus = queueStatus>
		        <cfelse>
		        	<cfset campaignStatus = "InReview">
		        </cfif>

	        	<cfset dataout.RXRESULTCODE = 1>
	        	<cfset dataout.STATUS = campaignStatus>
	        	<cfset dataout.BATCHID = arguments.inpBatchId>
	         <cfcatch>
	        	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	   		    <cfreturn dataout />
	        </cfcatch>
	        </cftry>

        <cfelse>
			<cfset dataout.MESSAGE = "Not Found">
	    </cfif>

	    <cfreturn dataout />
	</cffunction>

	<cffunction name="updateCampaignStatus" access="remote" output="true" hint="Update campaign status">
		<cfargument name="inpBatchId" type="number" required="yes" default="0">
		<cfargument name="inpAction" type="string" required="yes" default="" hint='Stop,Pause,Run'>

		<cfset var dataout = {}>
		<cfset var permissionObject	= '' />
		<cfset var updateCampaignPermission	= '' />
		<cfset var getBatchInfo	= '' />
		<cfset var updateUserInfo	= '' />
		<cfset var result	= '' />

		<cfset dataout.RXRESULTCODE = -1>

		<cfif arguments.inpBatchId GT 0>
			<cftry>
				<!--- CHECK PERMISSION --->
				<cfset permissionObject = CreateObject("component", "#Session.SessionCFCPath#.administrator.permission")>
				<cfset updateCampaignPermission = permissionObject.havePermission(edit_Campaign_Title)>
				<cfif updateCampaignPermission.havePermission>
					<!--- GET BATCH --->
					<cfquery name="getBatchInfo" datasource="#Session.DBSourceREAD#">
						SELECT BatchId_bi
						FROM simpleobjects.batch
						WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND Active_int = 1
					</cfquery>
					<cfif getBatchInfo.RECORDCOUNT GT 0>
						<cfset var _actionText = ''>
						<cfif arguments.inpAction EQ 'Stop'>
							<cfquery name="updateUserInfo" datasource="#Session.DBSourceEBM#" result="result">
								SET tx_isolation = 'READ-COMMITTED';
			                    UPDATE
			                    	simplequeue.contactqueue
			                    SET
			                    	DTSStatusType_ti = 7
			                    WHERE
			                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			                    AND
			                    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
			                    AND
			                    	DTSStatusType_ti IN (0,1,2,3)
			                </cfquery>
			                <cfset _actionText = 'stoped'>
						<cfelseif arguments.inpAction EQ 'Pause'>
							<cfquery name="updateUserInfo" datasource="#Session.DBSourceEBM#" result="result">
								SET tx_isolation = 'READ-COMMITTED';
			                    UPDATE
			                    	simplequeue.contactqueue
			                    SET
			                    	DTSStatusType_ti = 0
			                    WHERE
			                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			                    AND
			                    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
			                    AND
			                    	DTSStatusType_ti IN (0,1,2,3)
			                </cfquery>
			                <cfset _actionText = 'paused'>
						<cfelseif arguments.inpAction EQ 'Run'>
							<cfquery name="updateUserInfo" datasource="#Session.DBSourceEBM#" result="result">
								SET tx_isolation = 'READ-COMMITTED';
			                    UPDATE
			                    	simplequeue.contactqueue
			                    SET
			                    	DTSStatusType_ti = 1
			                    WHERE
			                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			                    AND
			                    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
			                    AND
			                    	DTSStatusType_ti = 0
			                </cfquery>
			                <cfset _actionText = 'running'>
						</cfif>
						<cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.MESSAGE = 'The campaign is #_actionText#!'>
					<cfelse>
						<cfset dataout.MESSAGE = "Campaign not found">
					</cfif>
				<cfelse>
					<cfset dataout.MESSAGE = "You don't have permission.">
				</cfif>
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	   		    <cfreturn dataout />
			</cfcatch>
			</cftry>
		<cfelse>
			<cfset dataout.MESSAGE = "CampaignID Not Exits">
	    </cfif>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="getCampaignTemplateList" access="public" output="true" hint="Get list campaign template by groupid">
		<cfargument name="inpGroupId" type="number" required="yes" default="0">
		<cfset var dataout	= {} />
		<cfset var template_item	= '' />
		<cfset var listTemplate	= '' />
		<cfset var getTemplateCategory	= '' />
		<cfset var listCategory	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.CAMPAIGNTYPE = 0>
		<cftry>
			<!--- CHECK SMS First --->
			<cfquery name = "listTemplate" datasource="#Session.DBSourceREAD#">
				SELECT
                	TID_int,
                    Name_vch,
                    Description_vch,
                    Category_vch,
                    CategoryId_int,
                    Image_vch,
                    Type_int
				FROM
                	simpleobjects.templatesxml
				WHERE
                	Active_int = 1
				ORDER BY
                	Category_vch, Order_int
			</cfquery>

			<cfquery name="getTemplateCategory" result="listCategory" datasource="#Session.DBSourceREAD#">
				SELECT *
				FROM simpleobjects.template_category
				ORDER BY Order_int ASC
			</cfquery>

			<cfif listTemplate.RECORDCOUNT GT 0>
				<cfset dataout.DATA ={}>
				<cfset var i	= '' />
				<cfloop query="getTemplateCategory">
					<cfset dataout.DATA[getTemplateCategory.CID_int] =arrayNew()>
				</cfloop>

				<cfloop query="#listTemplate#">
					<cfset template_item=structNew()>
                    <cfset template_item['ID'] = TID_int>
                    <cfset template_item['NAME'] = Name_vch>
                    <cfset template_item['DESCRIPTION'] = Description_vch>
                    <cfset template_item['IMAGE'] = Image_vch>
                    <cfset template_item['TYPE'] = Type_int>
                   	<cfif structKeyExists(dataout.DATA, "#CategoryId_int#") >
                    	<cfset arrayAppend(dataout.DATA['#CategoryId_int#'], template_item)>
                    </cfif>
				</cfloop>

				<cfset dataout.CAMPAIGNTYPE = _CampaignTypeSMS>
				<cfset dataout.RXRESULTCODE = 1>
				<cfreturn dataout />
			</cfif>
		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>

	<!--- <cffunction name="getUserCampaignTemplateList" access="public" output="true">
		<cfargument name="inpGroupId" type="number" required="yes" default="0">
		<cfset var dataout	= {} />
		<cfset var template_item	= '' />
		<cfset var listTemplate	= '' />
		<cfset var getTemplateCategory	= '' />
		<cfset var listCategory	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.CAMPAIGNTYPE = 0>
		<cftry>
			<!--- CHECK SMS First --->
			<cfquery name = "listTemplate" datasource="#Session.DBSourceREAD#">
				SELECT
                	TID_int,
                    Name_vch,
                    Description_vch,
                    Category_vch,
                    CategoryId_int,
                    Image_vch
				FROM
                	simpleobjects.templatesxml
				WHERE
                	Active_int = 1
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				ORDER BY
                	Category_vch, Order_int
			</cfquery>

			<cfquery name="getTemplateCategory" result="listCategory" datasource="#Session.DBSourceREAD#">
				SELECT *
				FROM simpleobjects.template_category
				ORDER BY Order_int ASC
			</cfquery>

			<cfif listTemplate.RECORDCOUNT GT 0>
				<cfset dataout.DATA ={}>
				<cfset var i	= '' />
				<cfloop query="getTemplateCategory">
					<cfset dataout.DATA[getTemplateCategory.CID_int] =arrayNew()>
				</cfloop>

				<cfloop query="#listTemplate#">
					<cfset template_item=structNew()>
                    <cfset template_item['ID'] = TID_int>
                    <cfset template_item['NAME'] = Name_vch>
                    <cfset template_item['DESCRIPTION'] = Description_vch>
                    <cfset template_item['IMAGE'] = Image_vch>
                   	<cfif structKeyExists(dataout.DATA, "#CategoryId_int#") >
                    	<cfset arrayAppend(dataout.DATA['#CategoryId_int#'], template_item)>
                    </cfif>
				</cfloop>

				<cfset dataout.CAMPAIGNTYPE = _CampaignTypeSMS>
				<cfset dataout.RXRESULTCODE = 1>
				<cfreturn dataout />
			</cfif>
		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction> --->

	<cffunction name="getKeywordQueryByBatchId" access="public" output="false" hint="Get key word query by batchid">
		<cfargument name="BatchId_bi" type="number" required="true" default="0">
		<cfset var keywordQuery	= '' />

		<cfquery name="keywordQuery" datasource="#Session.DBSourceREAD#">
			SELECT KeywordId_int, Keyword_vch, Survey_int, Active_int, EMSFlag_int
			FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.BatchId_bi#">
				AND IsDefault_bit = 0
				AND Active_int = 1
		</cfquery>
		<cfreturn keywordQuery>
	</cffunction>

	<cffunction name="getOptionListStyle" access="public" output="false" hint="Get list option style">
		<cfargument name="AF" type="string" required="true" default="">
		<cfargument name="ID" type="numeric" required="true" default="1">
		<cfswitch expression="#AF#">
			<cfcase value="NUMERICPAR">
				<cfreturn ID & ')'>
			</cfcase>
			<cfcase value="NUMERIC">
				<cfreturn ID & ' for'>
			</cfcase>
			<cfcase value="ALPHAPAR">
				<cfreturn Chr(64+ID) & ')'>
			</cfcase>
			<cfcase value="ALPHA">
				<cfreturn Chr(64+ID) & ' for'>
			</cfcase>
			<cfdefaultcase>
				<cfreturn ''>
			</cfdefaultcase>
		</cfswitch>
	</cffunction>

	<cffunction name="checkKeywordAvailability" access="remote" output="false" hint="check keyword availability">
	    <cfargument name="fieldValue" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="shortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="fieldId" TYPE="string" />

        <cfset var LOCALOUTPUT	= '' />
		<cfset var userPlan	= '' />
		<cfset var extendPlanTK	= '' />
		<cfset var extendPlanUrl	= '' />
		<cfset var checkUserPlan	= '' />

        <cfinvoke component="session.cfc.csc.csc" method="CheckKeywordAvailability" returnvariable="LOCALOUTPUT">
		    <cfinvokeargument name="inpKeyword" value="#trim(arguments.fieldValue)#" />
	        <cfinvokeargument name="inpShortCode" value="#trim(arguments.shortCode)#"/>
        </cfinvoke>

        <cfif LOCALOUTPUT.RXRESULTCODE EQ 1 AND LOCALOUTPUT.EXISTSFLAG EQ 0>
        	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="userPlan"></cfinvoke>
        	<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
				<cfinvokeargument name="userId" value="#SESSION.USERID#">
			</cfinvoke>

    		<cfif userPlan.RXRESULTCODE EQ 1 AND userPlan.KEYWORDAVAILABLE GT 0>
        		<cfreturn [arguments.fieldId, true]>
        	<cfelse>
        		<cfif userPlan.PLANEXPIRED GT 0> <!--- If plan expried --->
        			<cfset var extendPlanEndDate = DateAdd("m", 1, checkUserPlan.ENDDATE)>
					<cfset extendPlanTK = ToBase64(DateFormat(extendPlanEndDate, 'yyyy-mm-dd-') & Session.USERID)>
					<cfset extendPlanUrl = '/public/sire/pages/extend-plan?tk=#extendPlanTK#'>
        		    <cfreturn [arguments.fieldId, false, 'Keyword Limitation.<span id="keyword-limitation"></span> <a href="'&extendPlanUrl&'" target="_blank" class="KeywordAvailable">Extend Plan</a>.']>
        		<cfelse>
        			<cfreturn [arguments.fieldId, false, 'Keyword Limitation.<span id="keyword-limitation"></span> <a href="/session/sire/pages/buy-keyword" target="_blank" class="KeywordAvailable">Buy Keyword</a> or <a href="/session/sire/pages/my-plan" target="_blank" class="KeywordAvailable">Upgrade Plan</a>.']>
        		</cfif>
        	</cfif>

    	<cfelse>
        	<cfreturn [arguments.fieldId, false, 'The keyword in use.']>
        </cfif>
	</cffunction>

	<cffunction name="checkKeywordInUse" access="remote" output="false" hint="Check keyword in use">
	    <cfargument name="fieldValue" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="shortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="fieldId" TYPE="string" />

        <cfset var LOCALOUTPUT	= '' />

        <cfinvoke component="session.cfc.csc.csc" method="CheckKeywordAvailability" returnvariable="LOCALOUTPUT">
		    <cfinvokeargument name="inpKeyword" value="#arguments.fieldValue#" />
	        <cfinvokeargument name="inpShortCode" value="#arguments.shortCode#" />
        </cfinvoke>

        <cfif LOCALOUTPUT.RXRESULTCODE EQ 1 AND LOCALOUTPUT.EXISTSFLAG EQ 0>
    		<cfreturn [arguments.fieldId, true]>
    	<cfelse>
        	<cfreturn [arguments.fieldId, false]>
        </cfif>
	</cffunction>

	<cffunction name="updateNewKeyword" access="remote" output="false" hint="Update new keyword">
		<cfargument name="campaignid" required="false" default="">
		<cfargument name="keyword" required="false" default="">
		<cfparam name="Session.UserId" default="-1" >

		<cfset var campaignDetailQuery	= '' />
		<cfset var shortCode	= '' />
		<cfset var LOCALOUTPUT	= '' />
		<cfset var getShortCodeRequestByUser	= '' />
		<cfset var userOrgInfo	= '' />
		<cfset var RetValAddKeyword	= '' />
		<cfset var keywordResult	= '' />
		<cfset var userPlanQuery = '' />
		<cfset var inpKeywordStripped = '' />

		<cfset var dataout = {
			RXRESULTCODE = 0,
			MESSAGE = '',
			ERRMESSAGE = '',
			KEYWORDID = 0,
			KEYWORD = '',
			CAMPAIGNID = campaignid
		}>

		<!--- Advance Feature - Match keywords with or without quotes and spaces - handle the way users might type or think --->
		<cfset inpKeywordStripped = Replace(TRIM(keyword), '"', "", "All") />
		<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
		<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />

		<cfif isNumeric(campaignid) AND campaignid GT 0>
			<cfquery name="campaignDetailQuery" datasource="#session.DBSourceREAD#">
		    	SELECT BatchId_bi, Desc_vch
		        FROM simpleobjects.batch
		        WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
		        	AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		        	AND EMS_Flag_int = 0
		        	AND Active_int = 1
		    </cfquery>
		    <cfif campaignDetailQuery.RecordCount GT 0>
				<cfinvoke component="session.sire.models.cfc.order_plan" method="getUserPlan" returnvariable="userPlanQuery"></cfinvoke >
				<cfif userPlanQuery.RXRESULTCODE EQ 1 AND userPlanQuery.AMOUNT GT 0>
					<cfset var keywordMinLength = 5>
				<cfelse>
					<cfset var keywordMinLength = 8>
				</cfif>
				<cfif len(inpKeywordStripped) GE keywordMinLength>
					<!--- remove old method:
					<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
					--->
					<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
						<cfinvokeargument name="inpBatchId" value="#campaignDetailQuery.BatchId_bi#">
					</cfinvoke>
			        <cfinvoke component="session.cfc.csc.csc" method="CheckKeywordAvailability" returnvariable="LOCALOUTPUT">
					    <cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#" />
				        <cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#" />
			        </cfinvoke>

					<cfif LOCALOUTPUT.RXRESULTCODE EQ 1 AND LOCALOUTPUT.EXISTSFLAG EQ 0>
						<cfquery datasource="#session.DBSourceEBM#" result="keywordResult">
							UPDATE sms.keyword SET Active_int = 0
							WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignDetailQuery.BatchId_bi#">
								AND IsDefault_bit = 0
								AND EMSFlag_int = 0
								AND Active_int = 1
						</cfquery>
						<!--- keywordResult.RecordCount --->

						<cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
						<cfset var SCMBbyUser = getShortCodeRequestByUser.getSCMB />
						<cfset var ShortCodeRequestId = SCMBbyUser.ShortCodeRequestId_int>
			        	<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
						<cfif userOrgInfo.RXRESULTCODE EQ 1>
							<cfset var orgInfoData = userOrgInfo.ORGINFO>
						<cfelse>
							<cfset var orgInfoData = {CustomHelpMessage_vch = '', CustomStopMessage_vch = ''}>
						</cfif>
			        	<cfset var CustomHELP = orgInfoData.CustomHelpMessage_vch>
			        	<cfset var CustomSTOP = orgInfoData.CustomStopMessage_vch>

						<cfinvoke component="session.sire.models.cfc.optin" method="AddKeyword" returnvariable="RetValAddKeyword">
							<cfinvokeargument name="inpBatchId" value="#campaignid#">
	                        <cfinvokeargument name="ShortCodeRequestId" value="#ShortCodeRequestId#">
	                        <cfinvokeargument name="ShortCode" value="#shortCode.SHORTCODE#">
	                        <cfinvokeargument name="Keyword" value="#inpKeywordStripped#">
	                        <cfinvokeargument name="Response" value="">
	                        <cfinvokeargument name="CustomHELP" value="#CustomHELP#">
	                        <cfinvokeargument name="CustomSTOP" value="#CustomSTOP#">
	                        <cfinvokeargument name="IsSurvey" value="1">
						</cfinvoke>
						<cfif RetValAddKeyword.RXRESULTCODE EQ 1>
							<cfset dataout.KEYWORDID = RetValAddKeyword.KEYWORDID>
							<cfset dataout.KEYWORD = RetValAddKeyword.KEYWORD>
							<cfset dataout.CREATED = '#dateFormat(now(), "medium")# #timeFormat(now(), "short")#'>
							<cfset dataout.RXRESULTCODE = 1>
						<cfelse>
							<cfset dataout.MESSAGE = (RetValAddKeyword.MESSAGE NEQ '' ? RetValAddKeyword.MESSAGE : RetValAddKeyword.ERRMESSAGE)>
							<cfquery datasource="#session.DBSourceEBM#" result="keywordResult">
								UPDATE sms.keyword SET Active_int = 1
								WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignDetailQuery.BatchId_bi#">
									AND IsDefault_bit = 0
									AND EMSFlag_int = 0
									AND Active_int = 0
								ORDER BY Created_dt DESC
								LIMIT 1
							</cfquery>
						</cfif>
					<cfelse>
						<cfset dataout.MESSAGE = 'The keyword in use.'>
					</cfif>
				<cfelse>
					<cfset dataout.MESSAGE = 'Keyword is minimum #keywordMinLength# characters allowed.'>
				</cfif>
		    <cfelse>
		    	<cfset dataout.MESSAGE = 'You have not permission or your session has expired.'>
		    </cfif>
		<cfelse>
			<cfset dataout.MESSAGE = 'There is an error in the saving process. Please contact administrator.'>
		</cfif>

		<cfreturn dataout>
	</cffunction>

	<!---  --->
	<cffunction name="getCampaignStatus" access="public" output="true" hint="get campaign status">
		<cfargument name="BatchId_bi" type="number" required="false" default="0">
		<cfargument name="hasKeyword" type="boolean" required="false" default="false">

	</cffunction>

	<cffunction name="getCampaignAndTypeByXML" access="public" output="false" hint="Get campaign and type by XML">
		<cfargument name="xmlControl" type="xml" required="yes">

		<cfset var _index	= '' />
		<cfset var _Q	= '' />
		<cfset var children	= '' />
		<cfset var _OPTION	= '' />

		<cfset var dataout = {
			type = '',
			question = '',
			answersFormat = '',
			answers = []
		}>
		<cfif (structKeyExists(xmlControl,'XmlChildren') AND isArray(xmlControl.XmlChildren) AND arrayIsDefined(xmlControl.XmlChildren,1)
				AND structKeyExists(xmlControl.XmlChildren[1],'XmlChildren') AND isArray(xmlControl.XmlChildren[1].XmlChildren)
				AND !arrayIsEmpty(xmlControl.XmlChildren[1].XmlChildren))>
			<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
				<cfif children.xmlName EQ 'RXSSCSC'>
					<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
						<cfset _index = 1>
						<cfloop array="#children.XmlChildren#" index="_Q">
							<cfif _index EQ 1>
								<cfif _Q.XmlAttributes.TYPE EQ 'ONESELECTION'>
									<cfset dataout.type = 'VOTE'>
								</cfif>
							<cfelse>
								<cfif arrayFindNoCase(['SHORTANSWER', 'ONESELECTION', 'ONETOTENANSWER', 'OPTIN'],_Q.XmlAttributes.TYPE) GT 0>
									<cfset dataout.type = ''>
									<cfbreak>
								</cfif>
							</cfif>
							<cfset _index++>
						</cfloop>
						<cfif dataout.type EQ 'VOTE'>
							<cfset _Q = children.XmlChildren[1]>
							<cfset dataout.answersFormat = _Q.XmlAttributes.AF>
							<cfset dataout.question = _Q.XmlAttributes.TEXT>
							<cfloop array="#_Q.XmlChildren#" index="_OPTION" >
								<cfset arrayAppend(dataout.answers, _OPTION.XmlAttributes.TEXT)>
							</cfloop>
						</cfif>
					</cfif>
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn dataout>
	</cffunction>


	<!--- No documentation and trying to do too much as well as doing wrong things.

		output="true" on cfc's? - this is only for displaying HTML content back to browser
AddKeywordEMS in optin cfc?  session.sire.models.cfc.optin" method="AddKeywordEMS Weird place to put it
No hints for method name / variables
No documentation
Code is trying to contort and do to much in one method
Create in one method
Update in another method
Send EMS in another method
not all vars decalred at top - threading issues - see requirment on varscoper tool
Even though UI shows Campaign Id we are really dealling with Batch Ids in DB - variables should reflect reality
Not all queires in proper format - coding standards
Need to get keyword length limits from DB not hard coded
Script library/Elements/DataId are only for Voice - dont need for SMS - bad code as a result of copy from EBM
BuildXMLControl string not even usesed!!!! Old EMS logic copied from somewhere - Template defines the XML now.
New Logic From Me: Campaign can now exist in DB WITHOUT a keyword - may be blasted or assigned keyword at any time but still can be saved to BatchID


What is all the nobranch stuff about for single selection? Seems like too much complexity for not a lot of benefit.



Subscriber list counts dont match - how are these calculated - miscalculated


	--->
	<cffunction name="saveCampaign" access="remote" output="true" hint="Save campaign">
		<cfargument name="templateid" required="false" default="">
		<cfargument name="campaignid" required="false" default="">
		<cfargument name="campaignname" required="false" default="">
		<cfargument name="keywordid" required="false" default="">
		<cfargument name="cpUpdated" required="false" default="">
		<cfargument name="EMS_Flag_int" required="false" default="">
		<cfargument name="keyword" required="false" default="">
		<cfargument name="groupid" required="false" default="">
		<cfargument name="scheduleTimeData" required="false" default="">
		<cfargument name="CustomHelpMessage_vch" required="false" default="">
		<cfargument name="CustomStopMessage_vch" required="false" default="">
		<cfargument name="actionSend" required="false" default="">

		<cfparam name="Session.UserId" default="-1" >

		<cfset var keywordid = arguments.keywordid />
		<cfset var keyword	= arguments.keyword />
		<cfset var campaignid	= arguments.campaignid />
		<cfset var item	= '' />
		<cfset var children	= '' />
		<cfset var optionText	= '' />
		<cfset var children	= '' />
		<cfset var cpDataUpdated	= '' />
		<cfset var campaignDetailQuery	= '' />
		<cfset var templateXMLDetailQuery	= '' />
		<cfset var insertCampaignQuery	= '' />
		<cfset var updateCampaignQuery	= '' />
		<cfset var shortCode	= '' />
		<cfset var groupList	= '' />
		<cfset var XMLCONTROLSTRING	= '' />
		<cfset var RetValAddKeywordEMS	= '' />
		<cfset var getShortCodeRequestByUser	= '' />
		<cfset var userOrgInfo	= '' />
		<cfset var RetValAddKeyword	= '' />
		<cfset var RetValAddFilter	= '' />
		<cfset var campaignResult	= '' />
		<cfset var userPlanQuery = '' />
        <cfset var userOrgInfo = '' />
        <cfset var inpFormData = '' />
        <cfset var RetVarDoDynamicTransformations = '' />
        <cfset var inpKeywordStripped = '' />

		<cfset var dataout = {
			RXRESULTCODE = 0,
			MESSAGE = '',
			ERRMESSAGE = '',
			KEYWORDID = 0,
			KEYWORD = '',
			CAMPAIGNID = campaignid
		}>

		<cfif trim(campaignname) EQ ''>
	    	<cfset dataout.MESSAGE = 'Campaign name is required.'>
	    	<cfreturn dataout>
		</cfif>

		<!--- Advance Feature - Match keywords with or without quotes and spaces - handle the way users might type or think --->
		<cfset inpKeywordStripped = Replace(TRIM(keyword), '"', "", "All") />
		<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
		<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />

		<!--- MINHHTN HOT FIX --->
		<cfparam name="Session.AdditionalDNC" default="">

		<cfif isNumeric(campaignid) AND campaignid GT 0>
			<cfquery name="campaignDetailQuery" datasource="#session.DBSourceREAD#">
		    	SELECT BatchId_bi, Desc_vch, GroupId_int, XMLControlString_vch, EMS_Flag_int
		        FROM simpleobjects.batch
		        WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
		        	AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		        	AND Active_int > 0
		    </cfquery>
		    <cfif campaignDetailQuery.RecordCount LE 0>
		    	<cfset dataout.MESSAGE = 'You have not permission or your session has expired.'>
		    	<cfreturn dataout>
		    </cfif>
		    <cfset var mode="edit">
		    <cfset var xmlControl = XmlParse("<XMLControlStringDoc>" & #campaignDetailQuery.XMLControlString_vch# & "</XMLControlStringDoc>") />
	    <cfelseif isNumeric(templateid)>


        	<!--- Why read from template second time? Why not just read once on campaign-edit page and pass through? --->
            <!--- Due to this re-read I need to transform the dynamic XML twice --->
			<cfquery name="templateXMLDetailQuery" datasource="#session.DBSourceREAD#">
		    	SELECT
                	XMLControlString_vch
		        FROM
                	simpleobjects.templatesxml
		        WHERE
                	TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#templateid#">
		    </cfquery>


		    <cfif templateXMLDetailQuery.RecordCount LE 0>
		    	<cfset dataout.MESSAGE = 'This template has been deleted.'>
		    	<cfreturn dataout>
		    </cfif>

<!--- 	<cfset dataout.RAWFROMTEMPLATE = templateXMLDetailQuery.XMLControlString_vch /> --->

			<!--- Read Profile data and put into form --->
            <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>

            <!---Create a data set to run against dynamic data --->
            <cfset inpFormData = StructNew() />

            <cfset inpFormData.inpBrand = userOrgInfo.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
            <cfset inpFormData.inpBrandFull = userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH />
            <cfset inpFormData.inpBrandWebURL = userOrgInfo.ORGINFO.ORGANIZATIONWEBSITE_VCH />
            <cfset inpFormData.inpBrandWebTinyURL = userOrgInfo.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
            <cfset inpFormData.inpBrandPhone = userOrgInfo.ORGINFO.ORGANIZATIONPHONE_VCH />
            <cfset inpFormData.inpBrandeMail = userOrgInfo.ORGINFO.ORGANIZATIONEMAIL_VCH />

            <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
            <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
            <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
            <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
            <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
            <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>

            <!--- Do the transforms here --->
            <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                <cfinvokeargument name="inpResponse_vch" value="#templateXMLDetailQuery.XMLControlString_vch#">
                <cfinvokeargument name="inpContactString" value="">
                <cfinvokeargument name="inpRequesterId_int" value="">
                <cfinvokeargument name="inpFormData" value="#inpFormData#">
                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
                <cfinvokeargument name="inpShortCode" value="">
                <cfinvokeargument name="INPBATCHID" value="">
                <cfinvokeargument name="inpBrandingOnly" value="1">
            </cfinvoke>

            <!--- Update the current template data --->
            <cfset templateXMLDetailQuery.XMLControlString_vch = RetVarDoDynamicTransformations.RESPONSE />


			<cfset var mode="create">
		    <cfset var xmlControl = XmlParse("<XMLControlStringDoc>" & #templateXMLDetailQuery.XMLControlString_vch# & "</XMLControlStringDoc>") />
	    <cfelse>
	    	<cfset dataout.MESSAGE = 'There is an error in the saving process. Please contact administrator.'>
	    	<cfreturn dataout>
		</cfif>

		<cfif !isNumeric(EMS_Flag_int) OR arrayFind([0, 1, 2],EMS_Flag_int) LT 1>
	    	<cfset dataout.MESSAGE = 'There is an error in the saving process. Please contact administrator.'>
	    	<cfreturn dataout>
		</cfif>

		<!--- remove old method:
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
			<cfinvokeargument name="inpBatchId" value="#campaignid#">
		</cfinvoke>

		<cfif shortCode.RXRESULTCODE NEQ 1>
	    	<cfset dataout.MESSAGE = shortCode.MESSAGE>
	    	<cfreturn dataout>
		</cfif>

		<cfswitch expression="#EMS_Flag_int#">
			<cfcase value="1">
				<cfif actionSend EQ true>
					<cfif isNumeric(groupid) && groupid GT 0>
						<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList"></cfinvoke>
						<cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
							<cfset var groupIds = []>
							<cfset var subcriberList = groupList.listData>
							<cfloop array ="#subcriberList#" index="item">
							    <cfset arrayAppend(groupIds, item[1])>
							</cfloop>
							<cfif  arrayFind(groupIds, groupid) LT 1>
						    	<cfset dataout.MESSAGE = 'Your Subscriber List Invalid. Please select another or contact administrator.'>
						    	<cfreturn dataout>
							</cfif>
						<cfelse>
					    	<cfset dataout.MESSAGE = 'Your Subscriber List Invalid. Please select another or contact administrator.'>
					    	<cfreturn dataout>
						</cfif>
					<cfelse>
				    	<cfset dataout.MESSAGE = 'You must choose a subscriber list.'>
				    	<cfreturn dataout>
					</cfif>
				</cfif>
				<cfset keyword = ''>
			</cfcase>
			<cfcase value="2">
				<cfif actionSend EQ true>
					<cfif isNumeric(groupid) && groupid GT 0>
						<!---
						<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList"></cfinvoke>
						--->
						<cfinvoke component="session.sire.models.cfc.subscribers" method="GetGroupDetailsForDatatable" returnvariable="groupList">
							<cfinvokeargument name="INPGROUPID" value="#groupid#">
							<cfinvokeargument name="sendType" value="1">
						</cfinvoke>

						<cfif groupList.RXRESULTCODE NEQ 1 OR groupList.iTotalRecords LTE 0>
						    <cfset dataout.MESSAGE = 'Your Subscriber List Invalid. Please select another or contact administrator.'>
						    <cfreturn dataout>
						</cfif>
					<cfelse>
				    	<cfset dataout.MESSAGE = 'You must choose a subscriber list.'>
				    	<cfreturn dataout>
					</cfif>
				</cfif>
				<cfset keyword = ''>
			</cfcase>
			<cfcase value="0">
				<cfif mode EQ "create" OR !isNumeric(keywordid)>
					<cfinvoke component="session.sire.models.cfc.order_plan" method="getUserPlan" returnvariable="userPlanQuery"></cfinvoke >
					<cfif userPlanQuery.RXRESULTCODE EQ 1 AND userPlanQuery.AMOUNT GT 0>
						<cfset var keywordMinLength = 5>
					<cfelse>
						<cfset var keywordMinLength = 8>
					</cfif>
					<cfif len(inpKeywordStripped) GE keywordMinLength>
						<cfset var checkKeyword = checkKeywordAvailability(inpKeywordStripped, shortCode.SHORTCODE, '')>
						<cfif !checkKeyword[2]>
					    	<cfset dataout.MESSAGE = checkKeyword[3]>
					    	<cfreturn dataout>
						</cfif>
					<cfelse>
				    	<cfset dataout.MESSAGE = 'Keyword is minimum #keywordMinLength# characters allowed.'>
				    	<cfreturn dataout>
					</cfif>
				</cfif>
			</cfcase>
		</cfswitch>

		<cfif IsJSON(cpUpdated)>
			<cfset var controlPointArray = []>
			<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
				<cfif children.xmlName EQ 'RXSSCSC'>
					<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
						<cfset controlPointArray = children.XmlChildren>
					</cfif>
					<cfbreak>
				</cfif>
			</cfloop>

			<cfif !arrayIsEmpty(controlPointArray)>
				<cfset var cpObjUpdated = DeserializeJSON(cpUpdated)>
				<cfloop collection="#cpObjUpdated#" item="cpDataUpdated">
					<cfif arrayIsDefined(controlPointArray,cpObjUpdated[cpDataUpdated].id)>
						<cfswitch expression="#cpObjUpdated[cpDataUpdated].type#">
							<cfcase value="ONESELECTION">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TEXT = cpObjUpdated[cpDataUpdated].text>
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.AF = cpObjUpdated[cpDataUpdated].af>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>

								<cfset var oaOption = controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlChildren[1]>
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlChildren = []>
								<cfset var oaOptionId = 1>
								<cfloop array="#cpObjUpdated[cpDataUpdated].options#" index="optionText">
									<cfset oaOption.XmlAttributes.AVAL = oaOptionId>
									<cfset oaOption.XmlAttributes.ID = oaOptionId>
									<cfset oaOption.XmlAttributes.TEXT = optionText>
									<cfset arrayAppend(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlChildren, oaOption)>
									<cfset oaOptionId++>
								</cfloop>
							</cfcase>
							<cfcase value="SHORTANSWER">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TEXT = cpObjUpdated[cpDataUpdated].text>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>
							</cfcase>
							<cfcase value="STATEMENT">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TEXT = cpObjUpdated[cpDataUpdated].text>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>
							</cfcase>
							<cfcase value="TRAILER">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TEXT = cpObjUpdated[cpDataUpdated].text>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>
							</cfcase>
							<cfcase value="OPTIN">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TEXT = cpObjUpdated[cpDataUpdated].text>
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.OIG = cpObjUpdated[cpDataUpdated].oig>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>
							</cfcase>
							<cfcase value="CDF">
								<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.SCDF = cpObjUpdated[cpDataUpdated].scdf>
								<cfif structKeyExists(controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes, "TDESC")>
									<cfset controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC = (controlPointArray[cpObjUpdated[cpDataUpdated].id].XmlAttributes.TDESC)>
								</cfif>
							</cfcase>
						</cfswitch>
					</cfif>
				</cfloop>
				<cfset var RXSSCSCINDEX = 1>
				<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
					<cfif children.xmlName EQ 'RXSSCSC'>
						<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
							<cfset xmlControl.XmlChildren[1].XmlChildren[RXSSCSCINDEX].XmlChildren = controlPointArray>
						</cfif>
						<cfbreak>
					</cfif>
					<cfset RXSSCSCINDEX++>
				</cfloop>
			</cfif>
		</cfif>


	    <cfset var DefaultCIDVch="">
	    <cfset var CONTACTTYPES_BITFORMAT="100">
	    <cfset var smsData="">

		<cfinvoke component="session.cfc.emergencymessages" method="buildXmlControlString" returnvariable="XMLCONTROLSTRING">
        	<cfinvokeargument name="callerId" value="#DefaultCIDVch#">
        	<cfinvokeargument name="inpContactType" value="#CONTACTTYPES_BITFORMAT#">
        	<cfinvokeargument name="inpGroupId" value="#EMS_Flag_int EQ 0 ? 0 : groupid#">
        	<cfinvokeargument name="smscontent" value="#smsData#">
    	</cfinvoke>

	  	<cfset var INPrxdsSCRIPT="0">
	    <cfset var INPrxdsLIBRARY="0">
	    <cfset var INPrxdsELEMENT="0">

    	<cfif XMLCONTROLSTRING.RXRESULTCODE GT 0>
			<cfset INPrxdsSCRIPT = XMLCONTROLSTRING.INPrxdsSCRIPT>
			<cfset INPrxdsLIBRARY = XMLCONTROLSTRING.INPrxdsLIBRARY>
			<cfset INPrxdsELEMENT = XMLCONTROLSTRING.INPrxdsELEMENT>
    	</cfif>

    	<cfif EMS_Flag_int EQ 2>
    		<cfset var CONTACTTYPES = '4,'>
    	<cfelse>
    		<cfset var CONTACTTYPES = '3,'>
    	</cfif>
		<cfset var CONTACTFILTER = '{"CONTACTFILTER":"","CDFFILTER":[]}'>




		<cfset var XMLControlString_vch = REReplaceNoCase(toString(xmlControl), '<\?[^>]+\?>', '')>
		<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, '<\/?XMLControlStringDoc>', '', "all")>
		<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, "'", '&apos;', "all")>
		<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, '"', "'", "all")>

<!--- 	<cfset dataout.tostringXML = XMLControlString_vch /> --->

	    <!--- Parse and replace SMB Profile CDFs here - user can put back SMB Profile CDFs for transactional SMS if they want them back as a regular CDF  --->

	    <!--- Read Profile data and put into form --->
	    <cfset var userOrgInfo = '' />
	    <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>

	   	<!---Create a data set to run against dynamic data --->
	   	<cfset var inpFormData = StructNew() />

		<cfset inpFormData.inpBrand = userOrgInfo.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
	    <cfset inpFormData.inpBrandFull = userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH />
	    <cfset inpFormData.inpBrandWebURL = userOrgInfo.ORGINFO.ORGANIZATIONWEBSITE_VCH />
	    <cfset inpFormData.inpBrandWebTinyURL = userOrgInfo.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
	    <cfset inpFormData.inpBrandPhone = userOrgInfo.ORGINFO.ORGANIZATIONPHONE_VCH />
	    <cfset inpFormData.inpBrandeMail = userOrgInfo.ORGINFO.ORGANIZATIONEMAIL_VCH />

	    <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
	    <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
	    <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
	    <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
	    <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
	    <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>

	  	<!--- Do the transforms here --->
	  	<cfset var RetVarDoDynamicTransformations = '' />
	    <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
	        <cfinvokeargument name="inpResponse_vch" value="#XMLControlString_vch#">
	        <cfinvokeargument name="inpContactString" value="">
	        <cfinvokeargument name="inpRequesterId_int" value="">
	        <cfinvokeargument name="inpFormData" value="#inpFormData#">
	        <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
	        <cfinvokeargument name="inpShortCode" value="">
	        <cfinvokeargument name="INPBATCHID" value="">
	        <cfinvokeargument name="inpBrandingOnly" value="1">
	    </cfinvoke>

	    <!--- Update the current template data --->
	   	<cfset XMLControlString_vch = RetVarDoDynamicTransformations.RESPONSE />


<!--- <cfset dataout.RetVarDoDynamicTransformations = XMLControlString_vch /> --->


		<cfswitch expression="#mode#">
			<cfcase value="create">
				<cfquery name="insertCampaignQuery" datasource="#Session.DBSourceEBM#" result="campaignResult">
					INSERT INTO simpleobjects.batch (
						UserId_int,
                        rxdsLibrary_int,
                        rxdsElement_int,
                        rxdsScript_int,
                        UserBatchNumber_int,
						Created_dt,
						Desc_vch,
						LastUpdated_dt,
						GroupId_int,
						XMLControlString_vch,
						Active_int,
						ContactGroupId_int,
						ContactTypes_vch,
						EMS_Flag_int,
						RealTimeFlag_int
						<cfif EMS_Flag_int EQ 1 OR EMS_Flag_int EQ 2>
							, ContactIsApplyFilter
							, ContactFilter_vch
						</cfif>
					) VALUE (
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
						0,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#campaignname#">,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#EMS_Flag_int EQ 0 ? 0 : groupid#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#EMS_Flag_int EQ 0 ? 0 : groupid#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTTYPES#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#EMS_Flag_int#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
						<cfif EMS_Flag_int EQ 1 OR EMS_Flag_int EQ 2>
							, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
							, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE='#CONTACTFILTER#'>
						</cfif>
					)
				</cfquery>

				<cfset campaignid = #campaignResult.generated_key#>
				<cfset dataout.CAMPAIGNID = #campaignid#>
			</cfcase>
			<cfcase value="edit">
				<cfquery name="updateCampaignQuery" datasource="#Session.DBSourceEBM#" result="campaignResult">
					UPDATE simpleobjects.batch SET
						Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#campaignname#">,
						LastUpdated_dt = NOW(),
						GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#EMS_Flag_int EQ 0 ? 0 : groupid#">,
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
						ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#EMS_Flag_int EQ 0 ? 0 : groupid#">
					WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
		        		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				</cfquery>
			</cfcase>
		</cfswitch>

		<cfif !isNumeric(keywordid)>
			<cfif keyword EQ ''>
				<cfset keyword = "EMS_#Session.USERID#_#campaignid#">
			</cfif>

			<cfif EMS_Flag_int EQ 1 OR EMS_Flag_int EQ 2>
				<cfinvoke component="session.sire.models.cfc.optin" method="AddKeywordEMS" returnvariable="RetValAddKeywordEMS">
					<cfinvokeargument name="INPBATCHID" value="#campaignid#"/>
					<cfinvokeargument name="ShortCode" value="#shortCode.SHORTCODE#"/>
					<cfinvokeargument name="Keyword" value="#keyword#"/>
					<cfinvokeargument name="Response" value=""/>
					<cfinvokeargument name="IsDefault" value="0"/>
					<cfinvokeargument name="IsSurvey" value="1"/>
					<cfinvokeargument name="CustomHELP" value="#CustomHelpMessage_vch#">
	                <cfinvokeargument name="CustomSTOP" value="#CustomStopMessage_vch#">
				</cfinvoke>

				<cfset dataout.KEYWORDID = RetValAddKeywordEMS.KEYWORDID>
				<cfset dataout.KEYWORD = RetValAddKeywordEMS.KEYWORD>
			<cfelse>
				<cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				<cfset var SCMBbyUser = getShortCodeRequestByUser.getSCMB />
				<cfset var ShortCodeRequestId = SCMBbyUser.ShortCodeRequestId_int>
			<!---
	        	<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
				<cfif userOrgInfo.RXRESULTCODE EQ 1>
					<cfset var orgInfoData = userOrgInfo.ORGINFO>
				<cfelse>
					<cfset var orgInfoData = {CustomHelpMessage_vch = '', CustomStopMessage_vch = ''}>
				</cfif>
	        	<cfset arguments.CustomHelpMessage_vch = orgInfoData.CustomHelpMessage_vch>
	        	<cfset arguments.CustomStopMessage_vch = orgInfoData.CustomStopMessage_vch>
			--->

				<cfinvoke component="session.sire.models.cfc.optin" method="AddKeyword" returnvariable="RetValAddKeyword">
					<cfinvokeargument name="inpBatchId" value="#campaignid#">
	                <cfinvokeargument name="ShortCodeRequestId" value="#ShortCodeRequestId#">
	                <cfinvokeargument name="ShortCode" value="#shortCode.SHORTCODE#">
	                <cfinvokeargument name="Keyword" value="#inpKeywordStripped#">
	                <cfinvokeargument name="Response" value="">
	                <cfinvokeargument name="CustomHELP" value="#CustomHelpMessage_vch#">
	                <cfinvokeargument name="CustomSTOP" value="#CustomStopMessage_vch#">
	                <cfinvokeargument name="IsSurvey" value="1">
				</cfinvoke>
				<cfset dataout.KEYWORDID = RetValAddKeyword.KEYWORDID>
				<cfset dataout.KEYWORD = RetValAddKeyword.KEYWORD>
				<!---<cfset dataout.ERRMESSAGE = RetValAddKeyword>--->
			</cfif>
		<cfelse>

			<cfset var customMessageHelpOld = 0>
			<cfset var customMessageStopOld = 0>
			<cfset var customMessageQuery	= '' />
			<cfset var updateCustomMessageQuery		= '' />
			<cfset var insertCustomMessageQuery		= '' />
			<cfset var updateCustomMessageResult	= '' />
			<cfset var insertCustomMessageResult	= '' />

			<cfquery name="customMessageQuery" datasource="#session.DBSourceREAD#">
				SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
			</cfquery>

			<cfif customMessageQuery.RecordCount GT 0>
				<cfquery name="updateCustomMessageQuery" datasource="#Session.DBSourceEBM#" result="updateCustomMessageResult">
					UPDATE sms.smsshare SET Content_vch = (
						CASE PKId_int
							<cfloop query="#customMessageQuery#">
								<cfswitch expression="#Keyword_vch#" >
								<cfcase value="HELP">
									<cfset customMessageHelpOld = PKId_int>
									WHEN #PKId_int# THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHelpMessage_vch), 640)#">
								</cfcase>
								<cfcase value="STOP">
									<cfset customMessageStopOld = PKId_int>
									WHEN #PKId_int# THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomStopMessage_vch), 640)#">
								</cfcase>
								</cfswitch>
							</cfloop>
						END
					) WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
				</cfquery>
			</cfif>

			<cfif (customMessageHelpOld EQ 0 AND TRIM(CustomHelpMessage_vch) NEQ '') OR (customMessageStopOld EQ 0 AND TRIM(CustomStopMessage_vch) NEQ '')>
				<cfset var comma = ''>
				<cfquery name="insertCustomMessageQuery" datasource="#Session.DBSourceEBM#" result="insertCustomMessageResult">
					INSERT INTO sms.smsshare (
						ShortCode_vch,
						Keyword_vch,
						BatchId_bi,
						Content_vch
					) VALUES
					<cfif customMessageHelpOld EQ 0 AND TRIM(CustomHelpMessage_vch) NEQ ''>
						<cfset comma = ','>
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#shortCode.SHORTCODE#">,
							'HELP',
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHelpMessage_vch), 640)#">
						)
					</cfif>
					<cfif customMessageStopOld EQ 0 AND TRIM(CustomStopMessage_vch) NEQ ''>
						#comma#(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#shortCode.SHORTCODE#">,
							'STOP',
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomStopMessage_vch), 640)#">
						)
					</cfif>
				</cfquery>
			</cfif>

			<cfif customMessageQuery.RecordCount GT 0>
				<cfquery name="updateCustomMessageQuery" datasource="#Session.DBSourceEBM#" result="updateCustomMessageResult">
					DELETE FROM sms.smsshare
					WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
						AND Content_vch = ''
				</cfquery>
			</cfif>
		</cfif>

		<cfif EMS_Flag_int EQ 1 OR EMS_Flag_int EQ 2>

			<cfset var LOOPLIMIT_INT = 100>

			<cfif scheduleTimeData EQ '' OR !isJSON(scheduleTimeData)>
				<!--- Auto create schedule with defaults or specified values--->
				<!---Not enable immediate delivery: 1 is delivery now--->
				<cfset var ScheduleTimeArray = ArrayNew(1)>
				<cfset var scheduleTime = {}>
				<!---Enable immediate delivery: 0 is immediate delivery--->
				<cfscript>
					scheduleTime.scheduleType = SCHEDULETYPE_INT_ENABLE;
					scheduleTime.startDate = START_DT_ENABLE;
					scheduleTime.stopDate = STOP_DT_ENABLE;
					scheduleTime.startHour = STARTHOUR_TI_ENABLE;
					scheduleTime.startMinute = STARTMINUTE_TI_ENABLE;
					scheduleTime.endHour = ENDHOUR_TI_ENABLE;
					scheduleTime.endMinute = ENDMINUTE_TI_ENABLE;
					scheduleTime.enabledBlackout = ENABLEDBLACKOUT_BT_ENABLE;
					scheduleTime.blackoutStartHour = BLACKOUTSTARTHOUR_TI_ENABLE;
					scheduleTime.blackoutStartMinute = BLACKOUTSTARTMINUTE_TI_ENABLE;
					scheduleTime.blackoutEndHour = BLACKOUTENDHOUR_TI_ENABLE;
					scheduleTime.blackoutEndMinute = BLACKOUTENDMINUTE_TI_ENABLE;
					scheduleTime.dayId = DAYID_INT_ENABLE;
					scheduleTime.timeZone = TIMEZONE_ENABLE;
				</cfscript>
				<cfset ArrayAppend(ScheduleTimeArray, scheduleTime)>
				<!---<cfset var RetValSchedule = 0>--->
                <cfinvoke component="session.sire.models.cfc.schedule" method="UpdateSchedule" <!---returnvariable="RetValSchedule"--->>
                    <cfinvokeargument name="INPBATCHID" value="#campaignid#"/>
                    <cfinvokeargument name="SCHEDULETIME" value="#SerializeJSON(ScheduleTimeArray)#"/>
                    <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                </cfinvoke>
			<cfelse>
				<!---<cfset var RetValSchedule = 0>--->
				<!--- use advance schedule --->

				 <cfinvoke component="session.sire.models.cfc.schedule" method="UpdateSchedule" <!---returnvariable="RetValSchedule"--->>
                    <cfinvokeargument name="INPBATCHID" value="#campaignid#"/>
                    <cfinvokeargument name="SCHEDULETIME" value="#SCHEDULETIMEDATA#"/>
                    <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                </cfinvoke>
			</cfif>

			<cfif actionSend EQ true>
				<cfinvoke component="session.cfc.distribution" method="AddFiltersToQueue" returnvariable="RetValAddFilter">
			  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
			        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
			        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
			        <cfinvokeargument name="INPBATCHID" value="#campaignid#">
			        <cfinvokeargument name="INPBATCHDESC" value="#campaignname#">
					<cfinvokeargument name="INPGROUPID" value="#groupid#">
					<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
					<cfinvokeargument name="CONTACTFILTER" value="#CONTACTFILTER#">
					<cfinvokeargument name="Note" value="">
					<cfinvokeargument name="IsApplyFilter" value="1">
				</cfinvoke>
				<!---<cfset dataout.ERRMESSAGE = RetValAddFilter>--->
			</cfif>
		</cfif>

		<cfset dataout.RXRESULTCODE = 1>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="confirmSendCampaign" access="remote" output="true" hint="Confirm send campagin">
		<cfargument name="templateid" required="false" default="">
		<cfargument name="campaignid" required="false" default="">
		<cfargument name="groupid" required="false" default="">
		<cfargument name="sendType" default="ems">

		<cfset var _error	= '' />
		<cfset var campaignDetailQuery	= '' />
		<cfset var templateXMLDetailQuery	= '' />
		<cfset var subscriberTotalQuery	= '' />
		<cfset var queueTotalQuery	= '' />
		<cfset var batchsQuery	= '' />
		<cfset var RetValBillingData	= '' />

		<cfset var dataout = {
			RXRESULTCODE = 0,
			MESSAGE = '',
			ERRMESSAGE = '',
			NUMBERSUBSCRIBER = 0,
			CREDITSNEEDED = 0,
			CREDITSINQUEUE = 0,
			CREDITSAVAILABLE = 0
		}>

		<cfset _error = false>

		<cfif isNumeric(campaignid) AND campaignid GT 0>
			<cfquery name="campaignDetailQuery" datasource="#session.DBSourceREAD#">
		    	SELECT BatchId_bi, Desc_vch, GroupId_int, XMLControlString_vch, EMS_Flag_int
		        FROM simpleobjects.batch
		        WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
		        	AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		        	AND Active_int > 0
		    </cfquery>
		    <cfif campaignDetailQuery.RecordCount LE 0>
		    	<cfset _error=true>
			<cfelse>
				<cfset xmlControl = XmlParse("<XMLControlStringDoc>" & #campaignDetailQuery.XMLControlString_vch# & "</XMLControlStringDoc>") />
		    </cfif>
		<cfelseif isNumeric(templateid)>
			<cfquery name="templateXMLDetailQuery" datasource="#session.DBSourceREAD#">
		    	SELECT XMLControlString_vch
		        FROM simpleobjects.templatesxml
		        WHERE TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#templateid#">
		    </cfquery>
		    <cfif templateXMLDetailQuery.RecordCount LE 0>
		    	<cfset _error=true>
			<cfelse>
			    <cfset xmlControl = XmlParse("<XMLControlStringDoc>" & #templateXMLDetailQuery.XMLControlString_vch# & "</XMLControlStringDoc>") />
		    </cfif>
		<cfelse>
			<cfset _error=true>
		</cfif>

		<cfif _error EQ true>
			<cfset dataout.MESSAGE = 'Error getting preview data, please try again. If problem persists, please contact your system administrator.'>
			<cfreturn dataout>
		</cfif>

		<cfquery name="subscriberTotalQuery" datasource="#Session.DBSourceREAD#">
			SELECT COUNT(*) AS TOTAL
			FROM (
			    SELECT CS.ContactAddressId_bi,CS.OptIn_dt,
				BIT_AND(IF(IO.OptIn_dt IS NULL = TRUE, 0, 1)) AS OptInOut_int,
                MAX(IO.OptOut_dt) AS OptOut_dt
			    FROM
			        simplelists.contactstring CS
					INNER JOIN simplelists.groupcontactlist GCL
						ON GCL.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#groupid#">
							AND GCL.ContactAddressId_bi = CS.ContactAddressId_bi
					INNER JOIN simplelists.grouplist GL
						ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
							AND GL.GroupId_bi = GCL.GroupId_bi
			        LEFT JOIN simplelists.optinout IO
			        	ON IO.ContactString_vch = CS.ContactString_vch
			        		<cfif sendType EQ 'push'>AND CS.ContactType_int = 4<cfelse>AND LENGTH(CS.ContactString_vch) < 14</cfif>
		        GROUP BY
		        	CS.ContactAddressId_bi
		        	HAVING  (OptInOut_int = 1 OR (OptIn_dt IS NULL AND OptOut_dt IS NULL))
        	) AS Contacts
		</cfquery>

		<cfif subscriberTotalQuery.RecordCount GT 0>
			<cfset dataout.NUMBERSUBSCRIBER = subscriberTotalQuery.TOTAL>
		</cfif>

		<cfset dataout.CREDITSNEEDED =  getCreditsNeededByXMLControl(xmlControl) * dataout.NUMBERSUBSCRIBER>


        <cfquery name="queueTotalQuery" datasource="#Session.DBSourceREAD#">
            SELECT
                COUNT(*) AS Total
            FROM
                simplequeue.contactqueue
            WHERE
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                DTSStatusType_ti = 1
        </cfquery>

        <cfset dataout.CREDITSINQUEUE = queueTotalQuery.Total />

        <!--- Why is this looping over Batches too? Is this used somewhere besides launch Campaign? --->

        <!---
			<cfquery name="queueTotalQuery" datasource="#Session.DBSourceREAD#">
				SELECT
					BatchId_bi,
					COUNT(*) AS Total
				FROM
					simplequeue.contactqueue
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					DTSStatusType_ti = 1
				GROUP BY
					BatchId_bi
				ORDER BY
					BatchId_bi
			</cfquery>

			<cfif queueTotalQuery.RecordCount GT 0>
				<cfset var batchIds = valueList(queueTotalQuery.BatchId_bi,',')>
				<cfquery name="batchsQuery" datasource="#Session.DBSourceEBM#">
					SELECT BatchId_bi, XMLControlString_vch
					FROM simpleobjects.batch
					WHERE BatchId_bi IN (#batchIds#)
						AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					ORDER BY BatchId_bi
				</cfquery>

				<cfset batchIds = []>
				<cfloop query="queueTotalQuery">
					<cfset batchIds[queueTotalQuery.BatchId_bi] = queueTotalQuery.Total>
				</cfloop>

				<cfloop query="batchsQuery">
					<cfset var xmlControl = XmlParse("<XMLControlStringDoc>" & #batchsQuery.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfset dataout.CREDITSINQUEUE += (getCreditsNeededByXMLControl(xmlControl) * batchIds[batchsQuery.BatchId_bi])>
				</cfloop>
			</cfif>
		--->


		<cfinvoke component="session.sire.models.cfc.billing" method="GetBalanceCurrentUser" returnvariable="RetValBillingData"></cfinvoke>
		<cfset dataout.CREDITSAVAILABLE = RetValBillingData.Balance>

		<cfset dataout.RXRESULTCODE = 1>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="getCreditsNeededByXMLControl" access="public" output="true" hint="Get credit needed by XML Control">
		<cfargument name="xmlControl" required="true" default="#{}#">
		<cfset var children	= '' />
		<cfset var _Q	= '' />
		<cfset var totalCredit = 1>

		<cfif (structKeyExists(xmlControl,'XmlChildren') AND isArray(xmlControl.XmlChildren) AND arrayIsDefined(xmlControl.XmlChildren,1)
				AND structKeyExists(xmlControl.XmlChildren[1],'XmlChildren') AND isArray(xmlControl.XmlChildren[1].XmlChildren)
				AND !arrayIsEmpty(xmlControl.XmlChildren[1].XmlChildren))>
			<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
				<cfif children.xmlName EQ 'RXSSCSC' AND structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
					<cfset totalCredit = 0>
					<cfloop array="#children.XmlChildren#" index="_Q">
						<cfswitch expression="#_Q.XmlAttributes.TYPE#">
							<cfcase value="ONESELECTION">
								<cfset totalCredit += 2>
							</cfcase>
							<cfcase value="SHORTANSWER">
								<cfset totalCredit += 2>
							</cfcase>
							<cfcase value="STATEMENT">
								<cfset totalCredit += 1>
							</cfcase>
							<cfcase value="TRAILER">
								<cfset totalCredit += 1>
							</cfcase>
						</cfswitch>
					</cfloop>
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn totalCredit>
	</cffunction>

	<cffunction name="cloneCampaign" access="remote" output="true" hint="Clone campaign">
		<cfargument name="INPCAMPAIGNID" default="0">
		<cfargument name="INPCAMPAIGNNAME" default="">
		<cfargument name="INPKEYWORD" default="">

		<cfset var dataout = {
			RXRESULTCODE = -1,
			MESSAGE = '',
			ERRMESSAGE = '',
			NEXTCAMPAINID = 0
		}>

		<cfset var GetCampaign = ''>
		<cfset var NEXTCAMPAINID = 0>
		<cfset var RecipientList = ''>
		<cfset var xmlControlString = ''>
		<cfset var AddBatch = ''>
		<cfset var AddBatchQuery = ''>
		<cfset var RetValGetKeywordFromBatchId = ''>
		<cfset var RetValAddKeywordEMS = ''>
		<cfset var getShortCodeRequestByUser = ''>
		<cfset var userOrgInfo = ''>
		<cfset var AddKeyword = ''>
		<cfset var RetValAddKeyword = ''>
		<cfset var shortCode	= '' />
		<cfset var CustomHELP =''/>
		<cfset var CustomSTOP =''/>
		<cfset var customMessageQuery =''/>

		<cfif arguments.INPCAMPAIGNID GT 0 AND arguments.INPCAMPAIGNNAME NEQ ''>
			<cftransaction action="begin">
			<cftry>
				<!---first we need to select information of mirror campaign --->
				<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
					SELECT
						BATCHID_BI,
						USERID_INT,
						RXDSLIBRARY_INT,
						RXDSELEMENT_INT,
						RXDSSCRIPT_INT,
						USERBATCHNUMBER_INT,
						CREATED_DT,
						DESC_VCH,
						LASTUPDATED_DT,
						GROUPID_INT,
						ALTSYSID_VCH,
						XMLCONTROLSTRING_VCH,
						ACTIVE_INT,
						SERVICEPASSWORD_VCH,
						ALLOWDUPLICATES_TI,
						REDIAL_INT,
						CALLERID_VCH,
						CONTACTGROUPID_INT,
						CONTACTTYPES_VCH,
						CONTACTNOTE_VCH,
						CONTACTISAPPLYFILTER,
						EMS_FLAG_INT,
						DEFAULTCID_VCH
					 FROM
					 	SIMPLEOBJECTS.BATCH
				 	WHERE
				 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.INPCAMPAIGNID#">
				 		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					 LIMIT 1
				</cfquery>

				<cfif GetCampaign.recordCount NEQ 1 >
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.ERRMESSAGE = "Campaign doesn't exist." />
		            <cfreturn dataout>
				</cfif>

				<cfset xmlControlString = #GetCampaign.XMLCONTROLSTRING_VCH#>

				<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
	                INSERT INTO simpleobjects.batch
	                    (
							USERID_INT,
							RXDSLIBRARY_INT,
							RXDSELEMENT_INT,
							RXDSSCRIPT_INT,
							USERBATCHNUMBER_INT,
							CREATED_DT,
							DESC_VCH,
							LASTUPDATED_DT,
							GROUPID_INT,
							ALTSYSID_VCH,
							XMLCONTROLSTRING_VCH,
							ACTIVE_INT,
							SERVICEPASSWORD_VCH,
							ALLOWDUPLICATES_TI,
							REDIAL_INT,
							CALLERID_VCH,
							CONTACTGROUPID_INT,
							CONTACTTYPES_VCH,
							CONTACTNOTE_VCH,
							CONTACTISAPPLYFILTER,
							EMS_FLAG_INT,
							DEFAULTCID_VCH
	                    )
	                VALUES
	                	(
	                      	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSLIBRARY_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSELEMENT_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSSCRIPT_INT#">,
	                        0,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.INPCAMPAIGNNAME#">,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.GROUPID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.ALTSYSID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlControlString#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.ACTIVE_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.SERVICEPASSWORD_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetCampaign.ALLOWDUPLICATES_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.REDIAL_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CALLERID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTGROUPID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTTYPES_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTNOTE_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTISAPPLYFILTER#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.EMS_FLAG_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.DEFAULTCID_VCH#">
	                    )
	            </cfquery>

	            <cfset NEXTCAMPAINID = #AddBatch.generated_key#>

	            <!--- ADD KEYWORD --->
	            <!--- Lee Note: Now that we have a batch id we can add an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->
	            <cfif GetCampaign.EMS_FLAG_INT EQ 1>
					<cfinvoke
					     component="session.cfc.csc.csc"
					     method="GetKeywordFromBatchId"
					     returnvariable="RetValGetKeywordFromBatchId">
					        <cfinvokeargument name="inpBatchId" value="#arguments.INPCAMPAIGNID#"/>
					</cfinvoke>

	    			<cfif RetValGetKeywordFromBatchId.RXRESULTCODE LT 0>
	                    <!--- <cfthrow MESSAGE="#RetValGetKeywordFromBatchId.MESSAGE#" TYPE="Any" detail="#RetValGetKeywordFromBatchId.ERRMESSAGE#" errorcode="-2"> --->
	                    <cftransaction action="rollback" />
	                    <cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = '#RetValGetKeywordFromBatchId.MESSAGE#'>
						<cfreturn dataout>
	                </cfif>

	                <cfquery name="customMessageQuery" datasource="#session.DBSourceEBM#">
						SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPCAMPAIGNID#">
					</cfquery>

					<cfloop query="#customMessageQuery#">
						<cfswitch expression="#Keyword_vch#" >
						<cfcase value="HELP">
							<cfset CustomHELP = Content_vch>
						</cfcase>
						<cfcase value="STOP">
							<cfset CustomSTOP = Content_vch>
						</cfcase>
						</cfswitch>
					</cfloop>

	    			<!---
	                <cfinvoke
	                 component="session.cfc.csc.csc"
	                 method="AddKeywordEMS"
	                 returnvariable="RetValAddKeywordEMS">
	                    <cfinvokeargument name="INPBATCHID" value="#AddBatch.generated_key#"/>
	                    <cfinvokeargument name="ShortCode" value="#RetValGetKeywordFromBatchId.SHORTCODE#"/>
	                    <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTCAMPAINID#"/>
	                    <cfinvokeargument name="Response" value="#RetValGetKeywordFromBatchId.RESPONSE#"/>
	                    <cfinvokeargument name="IsDefault" value="0"/>
	                    <cfinvokeargument name="IsSurvey" value="1"/>
	                </cfinvoke>
	                --->

	                <cfinvoke component="session.sire.models.cfc.optin" method="AddKeywordEMS" returnvariable="RetValAddKeywordEMS">
						 <cfinvokeargument name="INPBATCHID" value="#AddBatch.generated_key#"/>
	                    <cfinvokeargument name="ShortCode" value="#RetValGetKeywordFromBatchId.SHORTCODE#"/>
	                    <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTCAMPAINID#"/>
	                    <cfinvokeargument name="Response" value="#RetValGetKeywordFromBatchId.RESPONSE#"/>
	                    <cfinvokeargument name="IsDefault" value="0"/>
	                    <cfinvokeargument name="IsSurvey" value="1"/>
						<cfinvokeargument name="CustomHELP" value="#CustomHELP#">
		                <cfinvokeargument name="CustomSTOP" value="#CustomSTOP#">
					</cfinvoke>

	                <cfif RetValAddKeywordEMS.RXRESULTCODE LT 0>
	                    <!--- <cfthrow MESSAGE="#RetValAddKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValAddKeywordEMS.ERRMESSAGE#" errorcode="-2"> --->
	                    <cftransaction action="rollback" />
	                    <cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = '#RetValAddKeywordEMS.MESSAGE#'>
						<cfreturn dataout>
	                </cfif>
	            <cfelse>
	            	<!--- remove old method:
					<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
					--->
					<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
						<cfinvokeargument name="inpBatchId" value="#arguments.INPCAMPAIGNID#">
					</cfinvoke>

	                <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
					<cfset var SCMBbyUser = getShortCodeRequestByUser.getSCMB />
					<cfset var ShortCodeRequestId = SCMBbyUser.ShortCodeRequestId_int>

		        	<!---<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
					<cfif userOrgInfo.RXRESULTCODE EQ 1>
						<cfset var orgInfoData = userOrgInfo.ORGINFO>
					<cfelse>
						<cfset var orgInfoData = {CustomHelpMessage_vch = '', CustomStopMessage_vch = ''}>
					</cfif>
		        	<cfset var CustomHELP = orgInfoData.CustomHelpMessage_vch>
		        	<cfset var CustomSTOP = orgInfoData.CustomStopMessage_vch>--->

		        	<cfquery name="customMessageQuery" datasource="#session.DBSourceEBM#">
						SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPCAMPAIGNID#">
					</cfquery>

					<cfloop query="#customMessageQuery#">
						<cfswitch expression="#Keyword_vch#" >
						<cfcase value="HELP">
							<cfset CustomHELP = Content_vch>
						</cfcase>
						<cfcase value="STOP">
							<cfset CustomSTOP = Content_vch>
						</cfcase>
						</cfswitch>
					</cfloop>

					<cfinvoke component="session.sire.models.cfc.optin" method="AddKeyword" returnvariable="RetValAddKeyword">
						<cfinvokeargument name="inpBatchId" value="#NEXTCAMPAINID#">
		                <cfinvokeargument name="ShortCodeRequestId" value="#ShortCodeRequestId#">
		                <cfinvokeargument name="ShortCode" value="#shortCode.SHORTCODE#">
		                <cfinvokeargument name="Keyword" value="#arguments.INPKEYWORD#">
		                <cfinvokeargument name="Response" value="">
		                <cfinvokeargument name="CustomHELP" value="#CustomHELP#">
		                <cfinvokeargument name="CustomSTOP" value="#CustomSTOP#">
		                <cfinvokeargument name="IsSurvey" value="1">
					</cfinvoke>

					 <cfif RetValAddKeyword.RXRESULTCODE LT 0>
	                    <!--- <cfthrow MESSAGE="#RetValAddKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValAddKeywordEMS.ERRMESSAGE#" errorcode="-2"> --->
	                    <cftransaction action="rollback" />
	                    <cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = '#RetValAddKeyword.MESSAGE#'>
						<cfreturn dataout>
	                </cfif>
	            </cfif>

	             <cftransaction action="commit" />
	            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Campaign">
					<cfinvokeargument name="operator" value="Create Campaign">
				</cfinvoke>

				<!---get batchid of recent inserted record --->
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.NEXTCAMPAINID = #AddBatch.generated_key#>
				<cfset dataout.MESSAGE = 'Clone successfully.'>

			<cfcatch>
			 	<cftransaction action="rollback" />
				<cfset dataout.RXRESULTCODE = -1>
				<cfset dataout.MESSAGE = '#cfcatch.MESSAGE# - #cfcatch.detail#'>
			</cfcatch>
			</cftry>

			</cftransaction>
		<cfelse>
			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = 'Can not found this campaign.'>
		</cfif>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="GetCampaignDetail" access="remote" output="true" hint="Get campaign details">
		<cfargument name="INPCAMPAIGNID" type="number" required="yes">
		<cfset var dataout	= {} />
		<cfset var getbatchDetail	= '' />

		<cfquery name="getbatchDetail" datasource="#session.DBSourceREAD#">
        	SELECT
				BATCHID_BI,
				USERID_INT,
				DESC_VCH,
				RXDSLIBRARY_INT,
				RXDSELEMENT_INT,
				RXDSSCRIPT_INT,
				USERBATCHNUMBER_INT,
				CREATED_DT,
				DESC_VCH,
				LASTUPDATED_DT,
				GROUPID_INT,
				ALTSYSID_VCH,
				XMLCONTROLSTRING_VCH,
				ACTIVE_INT,
				SERVICEPASSWORD_VCH,
				ALLOWDUPLICATES_TI,
				REDIAL_INT,
				CALLERID_VCH,
				CONTACTGROUPID_INT,
				CONTACTTYPES_VCH,
				CONTACTNOTE_VCH,
				CONTACTISAPPLYFILTER,
				EMS_FLAG_INT,
				DEFAULTCID_VCH,
				SHORTCODEID_INT
			FROM
			 	SIMPLEOBJECTS.BATCH
		 	WHERE
		 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.INPCAMPAIGNID#">
		 		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			 LIMIT 1

        </cfquery>
		<cfif getbatchDetail.recordCount EQ 1 >
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CAMPAIGNOBJECT = getbatchDetail>
			<cfset dataout.MESSAGE = "Get campaign successfully.">
		<cfelse>
			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.CAMPAIGNOBJECT = ''>
			<cfset dataout.MESSAGE = "Can not find this campaign.">
		</cfif>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="copyCampaign" access="remote" output="true" hint="copy one campaignid from one user to another user">
		<cfargument name="InpSourceUserId" required="true" default="0">
		<cfargument name="InpTargetUserid" required="true" default="0">
		<cfargument name="InpCampaignId" required="true" default="0">
		<cfargument name="InpTicketId" required="false" default="0">


		<cfset var dataout = {
			RXRESULTCODE = -1,
			MESSAGE = '',
			ERRMESSAGE = '',
			NEXTCAMPAINID = 0
		}>

		<cfset var GetCampaign = ''>
		<cfset var NEXTCAMPAINID = 0>
		<cfset var xmlControlString = ''>
		<cfset var toEmail = ''>
		<cfset var dataMail = ''>
		<cfset var AddBatchQuery = ''>
		<cfset var resultUserInfo = ''>
		<cfset var AddBatch = ''>

		<cfif arguments.InpCampaignId GT 0 AND arguments.InpSourceUserId GT 0 AND  arguments.InpTargetUserid GT 0>

			<cftry>
				<!---first we need to select information of mirror campaign --->
				<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
					SELECT
						BATCHID_BI,
						USERID_INT,
						RXDSLIBRARY_INT,
						RXDSELEMENT_INT,
						RXDSSCRIPT_INT,
						USERBATCHNUMBER_INT,
						CREATED_DT,
						DESC_VCH,
						LASTUPDATED_DT,
						GROUPID_INT,
						ALTSYSID_VCH,
						XMLCONTROLSTRING_VCH,
						ACTIVE_INT,
						SERVICEPASSWORD_VCH,
						ALLOWDUPLICATES_TI,
						REDIAL_INT,
						CALLERID_VCH,
						CONTACTGROUPID_INT,
						CONTACTTYPES_VCH,
						CONTACTNOTE_VCH,
						CONTACTISAPPLYFILTER,
						EMS_FLAG_INT,
						DEFAULTCID_VCH,
						TemplateId_int,
						TemplateType_ti
					 FROM
					 	SIMPLEOBJECTS.BATCH
				 	WHERE
				 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.InpCampaignId#">
				 		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.InpSourceUserId#">
					 LIMIT 1
				</cfquery>

				<cfif GetCampaign.recordCount NEQ 1 >
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.ERRMESSAGE = "Campaign doesn't exist." />
		            <cfreturn dataout>
				</cfif>

				<cfset xmlControlString = #GetCampaign.XMLCONTROLSTRING_VCH#>

				<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
	                INSERT INTO simpleobjects.batch
	                    (
							USERID_INT,
							RXDSLIBRARY_INT,
							RXDSELEMENT_INT,
							RXDSSCRIPT_INT,
							USERBATCHNUMBER_INT,
							CREATED_DT,
							DESC_VCH,
							LASTUPDATED_DT,
							GROUPID_INT,
							ALTSYSID_VCH,
							XMLCONTROLSTRING_VCH,
							ACTIVE_INT,
							SERVICEPASSWORD_VCH,
							ALLOWDUPLICATES_TI,
							REDIAL_INT,
							CALLERID_VCH,
							CONTACTGROUPID_INT,
							CONTACTTYPES_VCH,
							CONTACTNOTE_VCH,
							CONTACTISAPPLYFILTER,
							EMS_FLAG_INT,
							DEFAULTCID_VCH,
							TemplateId_int,
							TemplateType_ti
	                    )
	                VALUES
	                	(
	                      	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.InpTargetUserid#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSLIBRARY_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSELEMENT_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSSCRIPT_INT#">,
	                        0,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.DESC_VCH#">,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.GROUPID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.ALTSYSID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlControlString#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.ACTIVE_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.SERVICEPASSWORD_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetCampaign.ALLOWDUPLICATES_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.REDIAL_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CALLERID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTGROUPID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTTYPES_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTNOTE_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTISAPPLYFILTER#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.EMS_FLAG_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.DEFAULTCID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.TemplateId_int#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.TemplateType_ti#">
	                    )
	            </cfquery>

	            <cfset NEXTCAMPAINID = #AddBatch.generated_key#>

	            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="Copy Campaign">
					<cfinvokeargument name="operator" value="Source CampaignId : #arguments.InpCampaignId#, Target CampaignId: #NEXTCAMPAINID#, Source Userid:#arguments.InpSourceUserId#, target UserId:#arguments.InpTargetUserid#, Ticket id:#arguments.InpTicketId# ">
				</cfinvoke>

				<!--- SEND EMAIL --->
				<cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
                    <cfinvokeargument name="inpUserId" value="#arguments.INPTARGETUSERID#">
                </cfinvoke>

                <cfset toEmail =  resultUserInfo.FULLEMAIL/>

				<cfset dataMail = {
                    BATCHID: NEXTCAMPAINID,
                    TICKETID: arguments.InpTicketId,
                    USERNAME: resultUserInfo.USERNAME,
                    ROOTURL_HTTP:rootUrl
                }/>

				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                    <cfinvokeargument name="to" value="#toEmail#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Your request has been completed">
                    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_copy_batch_notification.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                </cfinvoke>

				<!---get batchid of recent inserted record --->
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.NEXTCAMPAINID = #AddBatch.generated_key#>
				<cfset dataout.MESSAGE = 'Copy campaign successfully.'>

			<cfcatch>
			 	<cftransaction action="rollback" />
				<cfset dataout.RXRESULTCODE = -1>
				<cfset dataout.MESSAGE = '#cfcatch.MESSAGE# - #cfcatch.detail#'>
			</cfcatch>
			</cftry>

		<cfelse>
			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = 'Can not found this campaign.'>
		</cfif>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="GetCampaignListById" access="remote" output="true" hint="get user list with filter">
        <cfargument name="inpSourceUserId" TYPE="number" required="yes" default="" />
        <cfargument name="inpBatchId" TYPE="number" required="no" default="0" />


        <cfset var dataout = {} />
        <cfset var GetUserList = '' />
        <cfset var rsGetUserList = '' />

        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset dataout.CampaignList = arrayNew() />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="GetUserList" result="rsGetUserList">
               	SELECT
               		Desc_vch, BatchId_bi
               	FROM simpleobjects.batch
               	WHERE
               		Active_int = 1
                AND
                	UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpSourceUserId#">
                <cfif inpBatchId GT 0>
                AND
                	BatchId_bi LIKE <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
                </cfif>
                AND EMS_Flag_int != 10

            </cfquery>

            <cfif GetUserList.RECORDCOUNT GT 0>
                <cfloop query="GetUserList" >
                    <cfset var tmpUser = {
                        'id':'#BatchId_bi#',
                        'text':'#BatchId_bi# - #Desc_vch#',
                    } />
                    <cfset ArrayAppend(dataout.CampaignList, tmpUser) />
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetTemplateDetailOfCampaign" access="remote" output="false" hint="Get template detail for Save as new template form">
		<cfargument name="inpBatchId" required="true">
		<cfset var dataout = {} />
		<cfset dataout.CATEGORIES = arrayNew(1)>
		<cfset dataout.IMAGES = arrayNew(1)>
		<cfset var UserFiles = '' >
		<cfset var imageItem = {} >
		<cfset var catItem = {} >
		<cfset var getCategory = '' >
		<cfset var getXMLString = '' >
		<cfset var getAllImage = '' >
		<cftry>
			<cfquery name="getCategory" datasource="#Session.DBSourceREAD#">
				SELECT
					CID_int,
					CategoryName_vch,
					DisplayName_vch
				FROM
					simpleobjects.template_category
			</cfquery>

			<cfquery name="getXMLString" datasource="#Session.DBSourceREAD#">
				SELECT
					XMLControlString_vch
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
			</cfquery>

			<cfloop query="getCategory">
				<cfset catItem = {
					id = CID_int,
					name = CategoryName_vch,
					displayname = DisplayName_vch
				}>
				<cfset arrayAppend(dataout.CATEGORIES, catItem)>
			</cfloop>

			<cfset UserFiles = ExpandPath(TEMPLATE_PICKER_PATH)>
			<cfdirectory directory = "#UserFiles#" action = "list" sort = "type asc, name asc" name = "getAllImage">

			<cfloop query="getAllImage">
				<cfset imageItem = {
					name = name
				}>
				<cfset arrayAppend(dataout.IMAGES, imageItem)>
			</cfloop>

			<cfset dataout.XMLCONTROLSTRING = "#getXMLString.XMLControlString_vch#">
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = 'Get template detail successfully!' />
			<cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetMaxOrderByCategoryGroup" access="remote" output="false" hint="Get max order by category group">
		<cfargument name="inpCategoryGroup" required="true">
		<cfset var dataout = {} >
		<cfset var getMaxOrder = ''>

		<cftry>
			<cfquery name="getMaxOrder" datasource="#Session.DBSourceREAD#">
				SELECT
					Order_int
				FROM
					simpleobjects.templatesxml
				WHERE
					CategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCategoryGroup#">
				ORDER BY
					Order_int
				DESC
				LIMIT 1
			</cfquery>

			<cfif getMaxOrder.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 >
				<cfset dataout.MAXORDER = "#getMaxOrder.Order_int#">
				<cfset dataout.MESSAGE = "Get max order successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 1 >
				<cfset dataout.MAXORDER = 1>
				<cfset dataout.MESSAGE = "Max order set to 1">
			</cfif>
			<cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetCampaignsByUserID" access="remote" hint="Get All campaign of specified user">
		<cfargument name="inpUserID" required="true">
		<cfset var dataout = {} />
		<cfset var getCampaigns = {} />

    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.DATALIST = [] />

		<cftry>
			<cfquery name="getCampaigns" datasource="#Session.DBSourceREAD#">
				SELECT
						b.BatchId_bi,
		                b.Desc_vch
					FROM
						simpleobjects.batch AS b
						LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1)
						LEFT JOIN simpleobjects.batch_blast_log bl ON bl.PKId_int = (

																					    SELECT
																					      	MAX(fa.PKId_int)
																					    FROM
																					    	simpleobjects.batch_blast_log fa
																					    WHERE
																					    	fa.BatchId_bi = b.BatchId_bi
																					  )
					WHERE
						b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
					AND
						b.Active_int > 0
					AND
						b.EMS_Flag_int IN (0,1,2)
			</cfquery>

	    	<cfloop query="getCampaigns">
		    	<cfset var item = {"ID" = "#getCampaigns.BatchId_bi#", "NAME" = "#getCampaigns.Desc_vch#" } />
		    	<cfset ArrayAppend(dataout.DATALIST,  item) />
	    	</cfloop>

	    	<cfset dataout.RXRESULTCODE = 1 >
			<cfset dataout.MESSAGE = "Get campaign successfully!">

			<cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="getTemplateList" hint="get List campaign Template ">
		<cfargument name="inpTemplateId" required="false" default="0">
		<cfargument name="inpCheckActive" required="false" default="1">

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.TEMPLATELIST = [] />

	    <cfset var getTemplateList = ""/>
	    <cfset var templateItem = {}>
		<cfset var rxGetTemplateList = ""/>


	    <cftry>
	    	<cfquery name="getTemplateList" datasource="#Session.DBSourceEBM#" result="rxGetTemplateList">
			    SELECT
			    	TID_int, Name_vch, Description_vch, XMLControlString_vch, Order_ti, Type_ti, DisplayOptin_ti,DisplayInteval_ti,DisplaySubcriberList_ti,DisplayPreview_ti
			    FROM
			    	simpleobjects.templatecampaign
			    WHERE
			      1 = 1
			    <cfif arguments.inpCheckActive EQ 1>
			    	AND	Active_ti = 1
			    </cfif>
			    <cfif arguments.inpTemplateId GT 0>
			    	AND TID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTemplateId#">
			    </cfif>
			    ORDER BY
			    	Order_ti, TID_int
			</cfquery>

			<cfif getTemplateList.RECORDCOUNT GT 0>
				<!--- <cfloop query="#getTemplateList#">
					<cfset templateItem = {}>
					<cfset templateItem['Name_vch'] = #getTemplateList.Name_vch#/>
					<cfset templateItem['Description_vch'] = #getTemplateList.Description_vch#/>
					<cfset templateItem['XMLControlString_vch'] = #getTemplateList.XMLControlString_vch#/>
					<cfset templateItem['TID_int'] = #getTemplateList.TID_int#/>
					<cfset arrayAppend(dataout.TEMPLATELIST, templateItem) />
				</cfloop> --->
				<cfset dataout.TEMPLATELIST = #getTemplateList#/>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
	    </cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetCampaignSubscriberReport" access="remote" hint="Get subscriber report for campaign">
		<cfargument name="inpCampaignId" required="true" type="numeric"/>
		<cfargument name="inpType" required="true" type="numeric"/>

		<cfset var dataout = {}/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
				<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetCampaignReportById" access="remote" hint="Get campaign report with blast and keyword">
		<cfargument name="inpBatchId" required="true" type="numeric"/>
		<cfargument name="inpType" required="true" type="numeric"/>
		<cfargument name="inpGroupId" required="false" default="0"/>
		<cfargument name="iDisplayStart" type="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" type="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" type="string" required="no" default="">
		<cfargument name="inpEndDate" type="string" required="no" default="">

	    <cfset var dataout = {}/>
	    <cfset dataout.MESSAGE = ""/>
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

	    <cfset var GetReport = '' />
	    <cfset var RetValGetSubscriberInfo = '' />
	    <cfset var RetValCountSMS = '' />
	    <cfset var sub = '' />
	    <cfset var rsCount = '' />
	    <cfset var groupIds = ''/>

	    <cftry>

	    	<cfif arguments.inpType EQ 1>
		        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
                    SELECT SQL_CALC_FOUND_ROWS
	                    sb.BatchId_bi,
	                    -- sg.GroupName_vch,
	                    -- sg.GroupId_bi,
	                    sbbl.GroupIds_vch,
	                    sbbl.AmountLoaded_int
                    FROM
                    	simpleobjects.batch sb
                    INNER JOIN
                    	simpleobjects.batch_blast_log sbbl
                    ON
                    	sb.BatchId_bi = sbbl.BatchId_bi
                    -- INNER JOIN
                    -- 	simplelists.grouplist sg
                    -- ON
                    -- 	sbbl.GroupIds_vch = sg.GroupId_bi
                    WHERE
                    	sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
                    AND
                    	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
                    <cfif arguments.inpGroupId GT 0>
                    	AND
                    		sg.GroupId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
                    </cfif>
					<cfif arguments.iDisplayStart GT 0 AND arguments.iDisplayLength GT 0>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
					</cfif>
		        </cfquery>

		    <cfelse>
		        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
	                SELECT SQL_CALC_FOUND_ROWS
	                    sb.BatchId_bi,
	                    sg.GroupName_vch,
	                    sg.GroupId_bi
	                FROM
	                    simpleobjects.batch sb
	                INNER JOIN
	                    simplelists.subscriberbatch ss
	                ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                INNER JOIN
	                    simplelists.grouplist sg
	                ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                AND
	                	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
                    <cfif arguments.inpGroupId GT 0>
                    	AND
                    		sg.GroupId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
                    </cfif>
					<cfif arguments.iDisplayStart GT 0 AND arguments.iDisplayLength GT 0>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
					</cfif>
		        </cfquery>
	    	</cfif>

	        <cfif GetReport.RECORDCOUNT GT 0>
				<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>
				<cfif arguments.inpType EQ 1>
					<cfset groupIds = valueList(GetReport.GroupIds_vch)>
				<cfelse>
					<cfset groupIds = GetReport.GroupId_bi>
				</cfif>

	        	<cfinvoke method="GetSubscriberInfo" component="session.sire.models.cfc.subscribers" returnvariable="RetValGetSubscriberInfo">
	        		<cfinvokeargument name="inpSubscriberIds" value="#groupIds#"/>
	        		<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
	        		<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
	        	</cfinvoke>

	        	<cfif RetValGetSubscriberInfo.RXRESULTCODE LT 1>
	        		<cfthrow type="Subscriber" errorcode="-1" message="Can't get subscribers info" detail="#RetValGetSubscriberInfo.MESSAGE#"/>
	        	</cfif>

	        	<cfloop array="#RetValGetSubscriberInfo.RESULT#" index="sub">
	        		<cfset var SENT = 0/>
	        		<cfset var RECEIVED = 0/>
		        	<cfinvoke method="CountAllSMSOfSubscriber" component="session.sire.models.cfc.subscribers" returnvariable="RetValCountSMS">
		        		<cfinvokeargument name="inpGroupId" value="#sub.GROUPID_BI#"/>
		        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		        		<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
	        			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
		        	</cfinvoke>

		        	<cfif RetValCountSMS.RXRESULTCODE GT 0>
	        			<cfset SENT = RetValCountSMS.SENT/>
	        			<cfset RECEIVED = RetValCountSMS.RECEIVED/>
		        	</cfif>

		        	<cfset var item = [
		        		'#sub.GROUPNAME_VCH#',
		        		'#SENT#',
		        		'#RetValCountSMS.QUEUED#',
		        		'#sub.OPTIN#',
		        		'#sub.OPTOUT#',
		        		'#(SENT NEQ 0 ? NumberFormat(RECEIVED/SENT,"0.000") : 0)*100#%',
		        		'#sub.GROUPID_BI#'
		        	]/>

		        	<cfset arrayAppend(dataout["DATALIST"], item)/>
	        	</cfloop>


	        </cfif>

	        <cfset dataout.RXRESULTCODE = 1/>
	        <cfset dataout.MESSAGE = "Get report success"/>

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
	    </cftry>

	    <cfreturn dataout/>
	</cffunction>




	<!--- NEw --->
	<cffunction name="GetCampaignReportByIdNew" access="remote" hint="Get campaign report with blast and keyword">
		<cfargument name="inpBatchId" required="true" type="numeric"/>
		<cfargument name="inpType" required="true" type="numeric"/>
		<cfargument name="inpGroupId" required="false" default="0"/>
		<cfargument name="iDisplayStart" type="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" type="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" type="string" required="no" default="">
		<cfargument name="inpEndDate" type="string" required="no" default="">

	    <cfset var dataout = {}/>
	    <cfset dataout.MESSAGE = ""/>
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

	    <cfset var GetReport = '' />
	    <cfset var RetValGetSubscriberInfo = '' />
	    <cfset var RetValCountSMS = '' />
	    <cfset var sub = '' />
	    <cfset var rsCount = '' />
	    <cfset var groupIds = ''/>

	    <cftry>

	    	<cfif arguments.inpType EQ 1>
		        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
                    SELECT SQL_CALC_FOUND_ROWS
	                    sb.BatchId_bi,
	                    -- sg.GroupName_vch,
	                    sbbl.GroupIds_vch,
	                    sbbl.AmountLoaded_int
                    FROM
                    	simpleobjects.batch sb
                    INNER JOIN
                    	simpleobjects.batch_blast_log sbbl
                    ON
                    	sb.BatchId_bi = sbbl.BatchId_bi
                    -- INNER JOIN
                    -- simplelists.grouplist sg
                    -- ON
                    --	sbbl.GroupId_int = sg.GroupId_bi
                    WHERE
                    	sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
                    AND
                    	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
                    <cfif arguments.inpGroupId GT 0>
                    	AND
                    		sg.GroupId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
                    </cfif>
					<cfif arguments.iDisplayStart GT 0 AND arguments.iDisplayLength GT 0>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
					</cfif>
		        </cfquery>
		    <cfelse>
		        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
	                SELECT SQL_CALC_FOUND_ROWS
	                    sb.BatchId_bi,
	                    sg.GroupName_vch,
	                    sg.GroupId_bi
	                FROM
	                    simpleobjects.batch sb
	                INNER JOIN
	                    simplelists.subscriberbatch ss
	                ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                INNER JOIN
	                    simplelists.grouplist sg
	                ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                AND
	                	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
                    <cfif arguments.inpGroupId GT 0>
                    	AND
                    		sg.GroupId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
                    </cfif>
					<cfif arguments.iDisplayStart GT 0 AND arguments.iDisplayLength GT 0>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
					</cfif>
		        </cfquery>
	    	</cfif>

	        <cfif GetReport.RECORDCOUNT GT 0>
				<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfif arguments.inpType EQ 1>
					<cfset groupIds = valueList(GetReport.GroupIds_vch)>
				<cfelse>
					<cfset groupIds = GetReport.GroupId_bi>
				</cfif>

	        	<cfinvoke method="GetSubscriberInfo" component="session.sire.models.cfc.subscribers" returnvariable="RetValGetSubscriberInfo">
	        		<cfinvokeargument name="inpSubscriberIds" value="#groupIds#"/>
	        		<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
	        		<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
	        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
	        	</cfinvoke>

	        	<cfif RetValGetSubscriberInfo.RXRESULTCODE LT 1>
	        		<cfthrow type="Subscriber" errorcode="-1" message="Can't get subscribers info" detail="#RetValGetSubscriberInfo.MESSAGE#"/>
	        	</cfif>

	        	<cfloop array="#RetValGetSubscriberInfo.RESULT#" index="sub">
	        		<cfset var SENT = 0/>
	        		<cfset var RECEIVED = 0/>
		        	<cfinvoke method="CountSMSOfSubscriber" component="session.sire.models.cfc.subscribers" returnvariable="RetValCountSMS">
		        		<cfinvokeargument name="inpGroupId" value="#sub.GROUPID_BI#"/>
		        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		        		<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
	        			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
		        	</cfinvoke>

		        	<cfif RetValCountSMS.RXRESULTCODE GT 0>
	        			<cfset SENT = RetValCountSMS.SENT/>
	        			<cfset RECEIVED = RetValCountSMS.RECEIVED/>
		        	</cfif>

					<cfset var dataItem = {}/>
					<cfset dataItem = {
						ID = '#sub.GROUPID_BI#',
						NAME = '#sub.GROUPNAME_VCH#',
						optin  = '#sub.OPTIN#',
						optout = '#sub.OPTOUT#'
					}>
					<cfset dataout["DATALIST"].append(dataItem) />

	        	</cfloop>

	        </cfif>

	        <cfset dataout.RXRESULTCODE = 1/>
	        <cfset dataout.MESSAGE = "Get report success"/>

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
	    </cftry>

	    <cfreturn dataout/>
	</cffunction>
	<!--- END NEw --->



    <cffunction name="getGroupString" access="private" hint="Get group string">
	    <cfargument name="inpOffsetDate" type="numeric" required="true">

	    <cfset var type = "day">

	   	<cfif arguments.inpOffsetDate GTE 14>
		   	<cfset type = "week">
		</cfif>

		<cfif arguments.inpOffsetDate GTE 89>
			<cfset type = "month">
		</cfif>

	   	<cfreturn type />
    </cffunction>


    <cffunction name="countDataByDate" access="private" hint="Count contact by date">
	    <cfargument name="inpDate" type="string" required="true">
	    <cfargument name="inpBatchId" type="numeric" required="true">
	    <cfargument name="inpGroupId" type="numeric" default="0">

	    <cfset var value = 0 />
	    <cfset var query =  {} />

    	<cfquery name="query" datasource="#Session.DBSourceEBM#">
    		SELECT
    			COUNT(DISTINCT  CL.ContactId_bi) AS Subcribers
			FROM simplelists.contactlist CL
			INNER JOIN simplelists.contactstring AS CS
				ON CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					<!--- AND CS.OptInSourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">  --->
					AND CS.ContactType_int = 3
					AND CL.ContactId_bi = CS.ContactId_bi
			INNER JOIN simplelists.optinout OP
					ON  OP.OptIn_dt IS NOT NULL
					AND LENGTH(CS.ContactString_vch) < 14
					AND CS.ContactString_vch = OP.ContactString_vch
			INNER JOIN simpleobjects.batch B
				ON  B.BatchId_bi = OP.BatchId_bi
				AND B.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
			<cfif inpType EQ 1 >
				INNER JOIN simpleobjects.batch_blast_log SBL
					ON SBL.BatchId_bi = B.BatchId_bi
					<cfif arguments.inpGroupId GT 0>
						AND SBL.GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
					</cfif>
			<cfelse>
				INNER JOIN simplelists.subscriberbatch SSB
					ON SSB.BatchId_bi = B.BatchId_bi
					<cfif arguments.inpGroupId GT 0>
						AND SSB.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
					</cfif>
			</cfif>
			WHERE DATE(OP.OptIn_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(inpDate, 'yyyy-mm-dd')#">
				  AND (CS.OptOut_dt IS NULL OR CS.OptOut_dt >= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#">, INTERVAL 1 DAY))
    	</cfquery>


    	<cfset value = query.Subcribers>

    	<cfreturn value />
    </cffunction>

    <cffunction name="countResponseByDate" access="private" hint="Count response by Date">
	    <cfargument name="inpDate" type="string" required="true"/>
	    <cfargument name="inpBatchId" type="numeric" required="true"/>

	    <cfset var value = 0 />
	    <cfset var getReportData =  '' />

	    <cftry>
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(ire.IRESessionId_bi) AS Response
				FROM
					simplexresults.ireresults ire
				JOIN
					simplequeue.sessionire ss
				ON
					ire.IRESessionId_bi = ss.SessionId_bi
				WHERE
					ire.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#"/>
				AND
                    ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"/>
                AND
                	ire.IREType_int = 2
                AND
                	LENGTH(ire.ContactString_vch) < 14
                AND
                	ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#"/>
                AND
                	ire.QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="5"/>
                AND
                	ire.CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="4"/>
                AND
                	<!--- ire.Created_dt <= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#"/>, INTERVAL 86399 SECOND) --->
                	DATE(ire.Created_dt) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#"/>
            </cfquery>

            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfset value = getReportData.Response/>
            </cfif>
            <cfcatch>
            	<cfreturn value/>
            </cfcatch>
	    </cftry>

    	<cfreturn value />
    </cffunction>

	<cffunction name="GetStatistic" access="remote" hint="Get statistic">
		<cfargument name="inpBatchId" type="numeric" required="true" >
    	<cfargument name="inpGroupId" type="numeric" required="true" default="0">
    	<cfargument name="inpType" required="true" type="numeric">
	    <cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">
	    <cfargument name="inpTemplateId" type="numeric" required="false" default="0"/>

	    <cfset var dataout = {} />
	    <cfset var dataout["DataList"] = [] />

	    <cfset var getStatisticData = [] />
	    <cfset var strGroupType = "" />

	    <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	    <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


	    <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	    <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	    <cfset var dateOffset  = DateDiff("d",startDate, endDate) />

	    <cfset var day = ""/>
	    <cfset var eachDay = ''/>
	    <cfset var method = 'countDataByDate' />

	    <cftry>

	    	<cfinvoke method="getGroupString" returnvariable="strGroupType">
	    		<cfinvokeargument name="inpOffsetDate" value="#dateOffset#">
	    	</cfinvoke>

	    	<cfif arguments.inpTemplateId EQ 9>
	    		<cfset method = 'countResponseByDate' />
	    	</cfif>

	    	<cfswitch expression="#strGroupType#">
		    	<cfcase value="day">

					<cfset var currDateOut = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(1,0,0,0)#">
						<cfset var valueItem = {
							"Time" 	= "#DateFormat(currDateOut, "yyyy-mm-dd")#",
							"Value" = 0,
							"format" = "#strGroupType#"
						}/>
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#currDateOut#"/>
							<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#"/>
						</cfinvoke>

						<cfset dataout["DataList"].append(valueItem) />
						<cfset currDateOut = currDateOut.add('d', 1) >
					</cfloop>

				</cfcase>
				<cfcase value="week">

					<!--- Create key list for data --->
					<cfset var dateList = [] />
					<cfset var currDate = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(7,0,0,0)#">
						<cfif DateDiff("d",currDate, endDate) GT 0 AND currDate LT endDate>
							<cfset  dateList.append(currDate) />
							<cfset currDate = currDate.add('d', 7) >
						</cfif>
					</cfloop>
					<cfset  dateList.append(endDate) />

					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>
				</cfcase>
				<cfcase value="month">
					<cfset var dateList = [] />
					<cfset var currDate = startDate />
					<cfset dateList.append(currDate) />

					<cfloop condition="DateCompare(currDate, endDate) LT 0)">
						<cfset currDate = currDate.add('m', 1) />
						<cfif DateCompare(currDate, endDate) LT 0>
							<cfset dateList.append(CreateDate(currDate.year(), currDate.month(), 1)) />
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />


					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>
				</cfcase>

	    	</cfswitch>
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>

    <cffunction name="GetLatestBlastGroup" access="public" hint="Get most recent subscriber blast of campaign">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>

    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.GROUPID = 0/>

    	<cfset var GetGroup = '' />

    	<cftry>
    		<cfquery name="GetGroup" datasource="#Session.DBSourceREAD#">
    			SELECT
    				GroupIds_vch
    			FROM
    				simpleobjects.batch_blast_log
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"/>
    			ORDER BY
    				Created_dt DESC
    			LIMIT 1
    		</cfquery>

    		<cfif GetGroup.RECORDCOUNT GT 0>
    			<cfset dataout.GROUPID = GetGroup.GroupIds_vch/>
    			<cfset dataout.MESSAGE = "Get subscriber list successfully!"/>
    		<cfelse>
    			<cfset dataout.MESSAGE = "This campaign doesn't have any blast!"/>
    		</cfif>

    		<cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout/>
    </cffunction>
	<cffunction name="GetCampaignBlastScheduleInfo" access="public" hint="Get Campaign Blast Schedule">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>

    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
		<cfset var GetCampaignBlast="">
		<cfset var scheduleDate="">
		<cfset var scheduleTime="">
		<cfset var scheduleDateTime="">
		<cfset var realSchedule="">


		<cfset var dataout.campaignBlastScheduleInfo=""/>

    	<cftry>
    		<cfquery name="GetCampaignBlast" datasource="#Session.DBSourceREAD#">
    			SELECT
    				c.DTSStatusType_ti,
					c.GroupId_int,
					c.Scheduled_dt,
					c.LastUpdated_dt,
					g.GroupName_vch,
					s.Start_dt,
					s.STARTHOUR_TI,
					s.STARTMINUTE_TI
    			FROM
					simpleobjects.scheduleoptions s
				INNER JOIN
    				simplequeue.contactqueue c
					ON	s.BatchId_bi=c.BatchId_bi
				LEFT JOIN simplelists.grouplist g
					ON c.GroupId_int=g.GroupId_bi
    			WHERE
    				c.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				c.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"/>
				AND
					g.Active_int=1
				GROUP BY c.DTSStatusType_ti, c.GroupId_int
    			ORDER BY
    				c.GroupId_int DESC
    		</cfquery>

    		<cfloop query="GetCampaignBlast">
				<cfset scheduleDate = dateFormat(Start_dt, 'mm/dd/yyyy')>
				<cfset scheduleTime = LSTimeFormat(STARTHOUR_TI&":"&STARTMINUTE_TI, 'hh:mm tt')>
				<cfset scheduleDateTime = scheduleDate&" "& scheduleTime/>

				<cfset realSchedule=dateTimeFormat (Scheduled_dt,"mm/dd/yyyy hh:mm tt")>
				<cfif DTSStatusType_ti is 5>
					<cfset dataout.campaignBlastScheduleInfo= dataout.campaignBlastScheduleInfo & "<br>You have previously blasted to subscriber list <b>#GroupName_vch#</b> on <b>#realSchedule#</b>"/>
				<cfelseif DTSStatusType_ti is 1>
					<cfset dataout.campaignBlastScheduleInfo= dataout.campaignBlastScheduleInfo & "<br>You have a blast scheduled to go out <b>#scheduleDateTime#</b> on subscriber list <b>#GroupName_vch#</b>"/>
				</cfif>
			</cfloop>
			<cfset dataout.campaignBlastScheduleInfo= replace(dataout.campaignBlastScheduleInfo,"<br>","","one")/>
    		<cfset dataout.MESSAGE = "Get Campaign Schedule successfully!"/>
    		<cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout/>
    </cffunction>

	<cffunction name="SaveCampaignName" access="remote" hint="Save campaign name">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>
    	<cfargument name="inpBatchName" required="true"/>


    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />

    	<cfset var updateCampaignNameResult = '' />

    	<cftry>
    		<cfquery result="updateCampaignNameResult" datasource="#Session.DBSourceEBM#">
    			UPDATE
    				simpleobjects.batch
    			SET
    				Desc_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpBatchName#"/>
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"/>
    		</cfquery>

    		<cfif updateCampaignNameResult.RECORDCOUNT GT 0>
    			<cfset dataout.RXRESULTCODE = 1/>
    			<cfset dataout.MESSAGE = 'Update campaign name successfully!'/>
    		<cfelse>
    			<cfset dataout.RXRESULTCODE = 0/>
    			<cfset dataout.MESSAGE = 'Update failed!'/>
    		</cfif>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
	<cffunction name="UpdateCampaignTemplateType" access="remote" hint="Update campaign template type">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>
    	<cfargument name="inpTemplateType" type="numeric" required="true"/>
		<cfargument name="inpTemplateTypeID" required="true" type="numeric"/>

    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset var UpdateCampaignTemplateType = '' />

    	<cftry>

    		<cfquery result="UpdateCampaignTemplateType" datasource="#Session.DBSourceEBM#">
    			UPDATE
    				simpleobjects.batch
    			SET
    				TemplateType_ti = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpTemplateType#"/>
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"/>
    		</cfquery>

    		<cfif UpdateCampaignTemplateType.RECORDCOUNT GT 0>
    			<cfset dataout.RXRESULTCODE = 1/>
    			<cfset dataout.MESSAGE = 'Update campaign template type successfully!'/>
    		<cfelse>
    			<cfset dataout.RXRESULTCODE = 0/>
    			<cfset dataout.MESSAGE = 'Update failed!'/>
    		</cfif>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>


    <cffunction name="GetBlastListByCampaignId" access="remote" hint="Get blast list by campaign id">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>
    	<cfargument name="inpGroupId" required="true" type="numeric"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="customFilter" default="">

    	<cfset var dataout = {} />
    	<cfset var getList = '' />
    	<cfset var rsCount = '' />
    	<cfset var tmp = {} />
    	<cfset dataout['DATALIST'] = [] />
    	<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset var filterItem = ''/>

    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				DTSId_int,
    				ContactString_vch,
    				Scheduled_dt
    			FROM
    				simplequeue.contactqueue
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				LENGTH(ContactString_vch) < 14
    			<cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
               	AND GroupId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
               	AND DTSStatusType_ti = 5
                ORDER BY
					DTSId_int
				DESC
    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
				</cfif>

    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
    			<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

	    		<cfloop query="#getList#">
	    			<cfset tmp = {
	    				// ID = Id_int,
	    				PHONE = ContactString_vch,
	    				CREATED = "#DateFormat(Scheduled_dt, "yyyy-mm-dd")# #TimeFormat(Scheduled_dt, "HH:mm:ss")#"
	    			}/>

	    			<cfset arrayAppend(dataout['DATALIST'], tmp)/>
	    		</cfloop>

	    		<cfset dataout.RXRESULTCODE = 1/>
    		</cfif>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetQueuedBlastListByCampaignId" access="remote" hint="Get queued blast list by campaign id">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>
    	<cfargument name="inpGroupId" required="true" type="numeric"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="customFilter" default="">

    	<cfset var dataout = {} />
    	<cfset var getList = '' />
    	<cfset var rsCount = '' />
    	<cfset var tmp = {} />
    	<cfset dataout['DATALIST'] = [] />
    	<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset var filterItem = ''/>

    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				DTSId_int,
    				ContactString_vch,
    				Scheduled_dt
    			FROM
    				simplequeue.contactqueue
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
    			AND
    				LENGTH(ContactString_vch) < 14
    			<cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
               	AND GroupId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
               	AND DTSStatusType_ti = 1
                ORDER BY
					DTSId_int
				DESC
    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
				</cfif>

    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
    			<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

	    		<cfloop query="#getList#">
	    			<cfset tmp = {
	    				// ID = Id_int,
	    				PHONE = ContactString_vch,
	    				CREATED = "#DateFormat(Scheduled_dt, "yyyy-mm-dd")# #TimeFormat(Scheduled_dt, "HH:mm:ss")#"
	    			}/>

	    			<cfset arrayAppend(dataout['DATALIST'], tmp)/>
	    		</cfloop>

	    		<cfset dataout.RXRESULTCODE = 1/>
    		</cfif>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
	<!--- Blast list --->
	<cffunction name="AdminBlastList" access="remote" output="true" hint="Admin get list blast"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cfset var SortOrderNum = 0/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4">
						<cfset order = "bll.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "bll.Status_int,bll.PriorityStatus_int, bll.UpdateDate_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "bll.Status_int,bll.PriorityStatus_int, bll.UpdateDate_dt"/>
			</cfif>
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					bll.PKId_int,
					bll.Created_dt,
					bll.AmountLoaded_int,
					bll.ActualLoaded_int,
					bll.Status_int,
					bll.BatchId_bi,
					bll.PriorityStatus_int,
					b.Desc_vch,
					u.UserId_int,
					concat('UserId: ',u.UserId_int,'<br>Name: ',u.FirstName_vch,' ',u.LastName_vch, '<br>Email: ',u.EmailAddress_vch,'<br>Phone: ',u.MFAContactString_vch) as Full_Name,
					u.EmailAddress_vch,
					u.MFAContactString_vch
				FROM
					simpleobjects.batch_blast_log bll
				INNER JOIN
					simpleobjects.useraccount u	ON bll.UserId_int = u.UserId_int
				INNER JOIN
					simpleobjects.batch b ON bll.BatchId_bi = b.BatchId_bi
				WHERE
					u.Active_int = 1
				AND
					u.IsTestAccount_ti = 0
				AND
					(DATE(bll.Created_dt) = DATE(NOW()) OR bll.Status_int IN (20, 30))
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
			<cfset SortOrderNum = 1/>
		    <cfloop query="GetPMQuery">
               <cfset Status = "READY">
                <cfif GetPMQuery.Status_int EQ "20">
                	<cfset Status = "READY">
                <cfelseif GetPMQuery.Status_int EQ "30">
                	<cfset Status = "PROCESSING">
                <cfelseif GetPMQuery.Status_int EQ "40">
                	<cfset Status = "COMPLETED">
                </cfif>
				<cfset tempItem = {
					SORTORDERNUM = '#SortOrderNum#',
					PKIDS = '#PKId_int#',
					USERID = '#UserId_int#',
					NAME = "#Full_Name#",
					EMAIL = "#EmailAddress_vch#",
					PHONE = "#MFAContactString_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",
					COMPLETED = "#NumberFormat(ActualLoaded_int)#",
					TOTAL = "#NumberFormat(AmountLoaded_int)#",
					STATUS = "#Status#",
					BATCH_ID = "#BatchId_bi#",
					BATCH_NAME = "#Desc_vch#",
					PRIORITY = #PriorityStatus_int#
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
				<cfset SortOrderNum = SortOrderNum + 1/>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<!--- Set Priority for Blast --- UpdateStatusBlastNow --->
	<cffunction name="UpdateStatusBlastNow" access="remote" hint="Update Campaign Priority">
    	<cfargument name="inpPKIds" required="true" type="numeric"/>

    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = ""/>

		<cfset var CheckCampaignPriority = '' />
		<cfset var UpdatePriority = '' />

    	<cftry>
			<cfquery name="CheckCampaignPriority" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.batch_blast_log
				SET
					PriorityStatus_int = 1
				WHERE
					Status_int IN (20,30)
			</cfquery>

    		<cfquery result="UpdatePriority" datasource="#Session.DBSourceEBM#">
    			UPDATE
    				simpleobjects.batch_blast_log
    			SET
    				PriorityStatus_int = 0
    			WHERE
    				PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKIds#"/>
    		</cfquery>
			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = 'Set priority for this campaign successfully!'/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
				<cfset dataout.RXRESULTCODE = -1/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
	<cffunction name="CountTotalCampaign" access="remote" hint="Count total campaign">
    	<cfargument name="inpUserID" required="false" type="numeric" default="#Session.UserID#"/>

    	<cfset var dataout = {} />
    	<cfset var getList = '' />
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TOTALCAMPAIGN = 0 />


    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				count(*) as Total
    			FROM
    				 simpleobjects.batch
    			WHERE
    				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserID#"/>
    			AND
    				Active_int=1
    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
				<cfset dataout.TOTALCAMPAIGN = getList.Total />
			</cfif>
			<cfset dataout.RXRESULTCODE = 1 />
    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
	<!--- Phone number lookup list report --->
	<cffunction name="PhoneNumberLookupList1" access="remote" output="true" hint="Get list phone number lookup 1"><!--- returnformat="JSON"--->
		<cfargument name="inpPhoneNumber" TYPE="string" required="no" default="" />
		<cfargument name="inpUserId" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->

		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'DESC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cfset var GetAdminPermission  = ''/>
		<cfset var Contactstring = ''/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="0">
						<cfset order = "sire.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "IREResultsId_bi"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "sire.Created_dt"/>
			</cfif>
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					DISTINCT
					ire.BatchId_bi,
					b.Desc_vch,
					DATE_FORMAT(sire.Created_dt,'%b %d %Y %T') AS Created_dt,
					DATE_FORMAT(sire.LastUpdated_dt,'%b %d %Y %T') AS LastUpdated_dt,
					b.Active_int,
					sire.SessionState_int,
					ire.IRESessionId_bi
				FROM
					simplexresults.ireresults ire
				INNER JOIN
					simpleobjects.batch b ON ire.BatchId_bi = b.BatchId_bi
				INNER JOIN
					simplequeue.sessionire sire ON ire.IRESessionId_bi = sire.SessionId_bi
				WHERE
					ire.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">
				<cfif GetAdminPermission.ISADMINOK NEQ "1">
					AND
						ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfif>
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.NAME EQ ' sire.SessionState_int ' AND filterItem.VALUE EQ 99>
								AND 1 = 1
							<cfelse>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
									AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
									OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
								<cfelse>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetPMQuery">
				<cfswitch expression="#SessionState_int#">
					<cfcase value= "0">
						<cfset Status = "Not a Survey">
					</cfcase>
					<cfcase value= "1">
						<cfset Status = "Open">
					</cfcase>
					<cfcase value="2">
						<cfset Status = "Interval Hold">
					</cfcase>
					<cfcase value="3">
						<cfset Status = "Response Interval">
					</cfcase>
					<cfcase value="4">
						<cfset Status = "Close">
					</cfcase>
					<cfcase value="5">
						<cfset Status = "Cancelled">
					</cfcase>
					<cfcase value="6">
						<cfset Status = "Expired">
					</cfcase>
					<cfcase value="7">
						<cfset Status = "Terminated">
					</cfcase>
					<cfcase value="8">
						<cfset Status = "Stop">
					</cfcase>
					<cfcase value="9">
						<cfset Status = "Too Many Sessions">
					</cfcase>
					<cfdefaultcase>
						<cfset Status = "Close">
					</cfdefaultcase>
				</cfswitch>
				<cfset tempItem = {
					BATCH_ID = "#BatchId_bi#",
					BATCH_NAME = "#Desc_vch#",
					CREATE_DATE = "#Created_dt#",
					UPDATE_DATE = "#LastUpdated_dt#",
					SESSIONID = "#IRESessionId_bi#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<!--- Phone number lookup list report --->
	<cffunction name="PhoneNumberLookupList2" access="remote" output="true" hint="Get list phone number lookup 2"><!--- returnformat="JSON"--->
		<cfargument name="inpPhoneNumber" TYPE="string" required="no" default="" />
		<cfargument name="inpUserId" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'DESC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var DateTime_Opt = ''/>
		<cfset var rsCount = '0'/>
		<cfset var GetAdminPermission = ''/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfdefaultcase>
						<cfset order = "ire.Created_dt"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "ire.Created_dt"/>
			</cfif>
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ire.BatchId_bi,
					b.UserId_int,
					ire.ShortCode_vch,
					ire.IREType_int,
					ire.Response_vch,
					ire.IRESessionId_bi,
					DATE_FORMAT(ire.Created_dt,'%b %d %Y %T') AS Created_dt,
					b.Desc_vch
				FROM
					simplexresults.ireresults ire
				INNER JOIN
					simpleobjects.batch b ON ire.BatchId_bi = b.BatchId_bi
				WHERE
				1=1
				AND
					(
						(ire.IREType_int = 5 AND UPPER(trim(ire.Response_vch)) = 'IRE - OPTIN' AND ire.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">)
					OR
						(ire.IREType_int = 2 AND UPPER(REPLACE(ire.Response_vch,' ','')) like 'STOP%' AND ire.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">)
					)
				<cfif GetAdminPermission.ISADMINOK NEQ "1">
					AND
						b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfif>
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetPMQuery">
               <cfset Status = "">
                <cfif GetPMQuery.IREType_int EQ "2">
                	<cfset Status = "OPT OUT">
                <cfelse>
                	<cfset Status = "OPT IN">
                </cfif>
				<cfset tempItem = {
					SESSIONID = '#IRESessionId_bi#',
					USERID = '#UserId_int#',
					BATCH_ID = "#BatchId_bi#",
					BATCH_NAME = "#Desc_vch#",
					DATETIME_OPT = "#Created_dt#",
					STATUS = "#Status#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<!--- Phone number lookup list report --->
	<cffunction name="PhoneNumberLookupList3" access="remote" output="true" hint="Get list Phone number lookup list report 3"><!--- returnformat="JSON"--->
		<cfargument name="inpPhoneNumber" TYPE="string" required="no" default="" />
		<cfargument name="inpUserId" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->

		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'DESC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var Status = ''/>
		<cfset var rsCount = '0'/>
		<cfset var GetAdminPermission = ''/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfdefaultcase>
						<cfset order = "IREResultsId_bi"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "IREResultsId_bi"/>
			</cfif>
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					IREResultsId_bi,
					BatchId_bi,
					UserId_int,
					CPId_int,
					QID_int,
					IREType_int,
					CarrierId_int,
					InitialSendResult_int,
					Increments_ti,
					IRESessionId_bi,
					MasterRXCallDetailId_bi,
					moInboundQueueId_bi,
					DATE_FORMAT(Created_dt,'%b %d %Y %T') AS Created_dt,
					ContactString_vch,
					NPA_vch,
					NXX_vch,
					Response_vch,
					ShortCode_vch
				FROM
					simplexresults.ireresults
				WHERE
					ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">
				AND
					IREType_int = 2
				<cfif GetAdminPermission.ISADMINOK NEQ "1">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfif>
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				ORDER BY
						#order# #sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetPMQuery">

				<cfset tempItem = {
					IRE_ID = '#IREResultsId_bi#',
					USERID = '#UserId_int#',
					BATCH_ID = "#BatchId_bi#",
					CREATE_DATE = "#Created_dt#",
					MESSAGE_DESC = "#Response_vch#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<!--- Close Batch ID --->
	<cffunction access="remote" name="CloseBatch" >
		<cfargument name="inpSessionId" type="string" required="yes">
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset var campaignid = ''/>
		<cfset var UPDATESURVEYSTATE = ''/>

		<cftry>
			<cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#">
				UPDATE
	                `simplequeue`.`sessionire`
	            SET
	                SessionState_int = 4,
	                LastUpdated_dt = NOW()
	            WHERE
	                SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSessionId#">
			</cfquery>
			<cfset dataout.RESULT = ''/>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Session is closed successfully" />
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	<!--- Phone conversation --->
	<cffunction name="PhoneConversationList" access="remote" output="true" hint="get list Phone conversation"><!--- returnformat="JSON"--->
		<cfargument name="inpPhoneNumber" TYPE="string" required="no" default="" />
		<cfargument name="inpBatchId" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpSessionId" TYPE="numeric" required="no" default="0"/>
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="ASC"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->

		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetPMQuery = ''>
		<cfset var sSortDir_0 = 'ASC'/>
		<cfset var order 	= "">
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		<cfset var message_type = ''/>
		<cfset var message_desc = ''/>
		<cfset var rsCount = '0'/>
		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
					SELECT
						SQL_CALC_FOUND_ROWS
						IREResultsId_bi,
						BatchId_bi,
						UserId_int,
						CPId_int,
						QID_int,
						IREType_int,
						CarrierId_int,
						InitialSendResult_int,
						Increments_ti,
						IRESessionId_bi,
						MasterRXCallDetailId_bi,
						moInboundQueueId_bi,
						DATE_FORMAT(Created_dt,'%b %d %Y %T') AS Created_dt,
						ContactString_vch,
						NPA_vch,
						NXX_vch,
						Response_vch,
						ShortCode_vch
					FROM
						simplexresults.ireresults
					WHERE
						ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPhoneNumber#">
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
					AND
						IRESessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSessionId#">
					ORDER BY
						IREResultsId_bi
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
		    <cfloop query="GetPMQuery">
               <cfset message_type = "">
                <cfif GetPMQuery.IREType_int EQ 1>
                	<cfset message_type = "Sent to Device (MT)">
					<cfset message_desc = #Response_vch#/>
                <cfelseif GetPMQuery.IREType_int EQ 2>
                	<cfset message_type = "Received from Device (MO)">
					<cfset message_desc = #Response_vch#/>
                <cfelse>
                	<cfset message_type = "System Logic("& #IREType_int# &")">
					<cfset message_desc = #Response_vch#/>
                </cfif>
				<cfset tempItem = {
					SHORT_CODE = '#ShortCode_vch#',
					USERID = '#UserId_int#',
					IRE_ID = "#IREResultsId_bi#",
					DATE_TIME = "#Created_dt#",
					CP_ID = "#CPId_int#",
					MESSAGE_TYPE = "#message_type#",
					MESSAGE_DESC = "#message_desc#"
				}>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>

		<cffunction name="AdminBlastListReport" access="remote" output="false" hint="Admin get list blast report"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="DESC">
		<cfargument name="customFilter" default="">
		<cfargument name="searchByFilter" default="">



		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var AdminBlastListReport = ''>
		<cfset var rsCount=""/>
		<cfset var tempItem={}/>
		<cfset var order	= '' />
		<cfset var filterItem	= '' />

		<cftry>

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["aoData"] = ArrayNew(1)>

			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="1">
						<cfset order = "b.BatchId_bi" />
					</cfcase>
					<cfcase value="2">
						<cfset order = "b.Desc_vch" />
					</cfcase>
					<cfcase value="3">
						<cfset order = "b.Created_dt" />
					</cfcase>
					<cfdefaultcase>
						<cfset order = "b.BatchId_bi"/>
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "b.BatchId_bi"/>
			</cfif>
			<cfquery name="AdminBlastListReport" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					b.Created_dt,
					b.BatchId_bi,
					b.Desc_vch,
					u.UserId_int,
					concat('UserId: ',u.UserId_int,'<br>Name: ',u.FirstName_vch,' ',u.LastName_vch, '<br>Email: ',u.EmailAddress_vch,'<br>Phone: ',u.MFAContactString_vch) as Full_Name,
					u.EmailAddress_vch,
					u.MFAContactString_vch
				FROM
					simpleobjects.batch b
				INNER JOIN
					simpleobjects.useraccount u	ON b.UserId_int = u.UserId_int
				WHERE
					u.Active_int = 1
				AND
					u.IsTestAccount_ti = 0
				AND
					b.TemplateType_ti=1
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
				<cfif searchByFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(searchByFilter)#" index="filterItem">
							AND #PreserveSingleQuotes(filterItem.NAME)# = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
						</cfloop>
					</cfoutput>
				</cfif>

				ORDER BY
						#order# #arguments.sSortDir_0#
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
	        </cfquery>

			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>

			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>

		    <cfloop query="AdminBlastListReport">
				<cfset tempItem = {
					USERID = '#UserId_int#',
					NAME = "#Full_Name#",
					EMAIL = "#EmailAddress_vch#",
					PHONE = "#MFAContactString_vch#",
					CREATE_DATE = "#DateFormat(Created_dt,'mm/dd/yyyy')#",

					BATCH_ID = "#BatchId_bi#",
					BATCH_NAME = "#Desc_vch#"

				}>
				<cfset ArrayAppend(dataout["aoData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<cffunction name="GetReportBlast" access="remote" output="false" hint="Get blast report">
		<cfargument name="inpBatchId" TYPE="numeric" required="yes" />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout["SCHEDULEOPTIONS"] = ArrayNew(1)>
		<cfset dataout.TOTALSUBSCRIBERS = 0 />
		<cfset dataout.MBLOXSENT = 0 />
		<cfset dataout.MBLOXFAIL = 0 />
		<cfset dataout.INPROGRESS = 0 />

		<cfset dataout.DELIVERED = 0 />
		<cfset dataout.NONDELIVERED = 0 />
		<cfset dataout.LOSTNOTI = 0 />
		<cfset var GetScheduleOptions=""/>
		<cfset var GetBatchInfo=""/>
		<cfset var GetProcessStatus=""/>
		<cfset var GetDeliveryReport=""/>
		<cfset var GetSubscriberCounts=""/>

		<cfset var rsCount=""/>
		<cfset var tempItem={}/>
		<cfset var GroupIdList=0>
		<cfset var ShortCode=0>
		<cfset var BatchUserID=0>

		<cftry>
			<cfquery name="GetScheduleOptions" datasource="#Session.DBSourceREAD#">
				SELECT
					StartHour_ti,
					EndHour_ti,
					StartMinute_ti,
					EndMinute_ti,

					DATE_FORMAT(Start_dt, "%M %d %Y") AS Start_dt,
					DATE_FORMAT(Stop_dt, "%M %d %Y") AS Stop_dt

				FROM
					simpleobjects.scheduleoptions
				WHERE
					BatchId_bi= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
			</cfquery>
			 <cfloop query="GetScheduleOptions">
				<cfset tempItem = {
					STARTDATE = '#Start_dt#',
					ENDDATE = "#Stop_dt#",
					STARTTIME = LSTimeFormat(CreateTime(StartHour_ti, StartMinute_ti, 0), "HH:mm:ss"),
					ENTTIME =  LSTimeFormat(CreateTime(EndHour_ti, EndMinute_ti, 0), "HH:mm:ss")


				}>
				<cfset ArrayAppend(dataout["SCHEDULEOPTIONS"],tempItem)>
		    </cfloop>
			<cfquery name="GetBatchInfo" datasource="#Session.DBSourceREAD#">
				SELECT
					b.ShortCodeId_int,
					bbl.GroupIds_vch,
					bbl.UserId_int
				FROM
					simpleobjects.batch b
				INNER JOIN
					simpleobjects.batch_blast_log bbl  ON b.BatchId_bi= bbl.BatchId_bi
				WHERE
					b.BatchId_bi=<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
			</cfquery>
			<cfloop query="GetBatchInfo">
				<cfset GroupIdList=ListAppend(GroupIdList, GroupIds_vch)>
				<cfset ShortCode=ShortCodeId_int>
				<cfset BatchUserID=UserId_int>
			</cfloop>

			<cfquery name="GetSubscriberCounts" datasource="#Session.DBSourceREAD#">
				SELECT
					count(*) as Total
				FROM
					simplequeue.batch_load_queue
				WHERE
					BatchId_bi=<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				UNION ALL
				SELECT
					count(*) as Total
				FROM
					simplequeue.contactqueue
				WHERE
					BatchId_bi=<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
			</cfquery>
			<cfloop query="GetSubscriberCounts">
				<cfset dataout.TOTALSUBSCRIBERS += Total>
			</cfloop>

			<!--- --->
			<cfquery name="GetProcessStatus" datasource="#Session.DBSourceREAD#">
				SELECT
					BatchId_bi,
					DTSStatusType_ti,
					count(DTSStatusType_ti) as Total
				FROM
					simplequeue.contactqueue
				WHERE
					BatchId_bi= <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				GROUP BY
					BatchId_bi,DTSStatusType_ti;
			</cfquery>
			<cfloop query="GetProcessStatus">
				<cfif listfind("3,5,8",DTSStatusType_ti) GT 0>
					<cfset dataout.MBLOXSENT += Total />
				<cfelseif listfind("4,6,7,100",DTSStatusType_ti) GT 0>
					<cfset dataout.MBLOXFAIL += Total />
				</cfif>
			</cfloop>
			<cfset dataout.INPROGRESS = dataout.TOTALSUBSCRIBERS - dataout.MBLOXSENT - dataout.MBLOXFAIL/>
			<!--- delivery report--->
			<cfquery name="GetDeliveryReport" datasource="#Session.DBSourceREAD#">
				SELECT	s.Status_vch ,count(Status_vch) as Total
				FROM
					simplexresults.smsdisposition s
				INNER JOIN
					simplexresults.contactresults c
					ON
						c.ContactString_vch=s.SubscriberNumber_vch
					AND
						c.SMSTrackingOne_bi = s.SecondarySequenceNumber_vch

				WHERE
					c.BatchId_bi=<cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				GROUP BY
				 	s.Status_vch
			</cfquery>

			<cfloop query="GetDeliveryReport">
				<cfif lcase(Status_vch) EQ lcase("Delivered")>
					<cfset dataout.DELIVERED = Total />
				<cfelseif lcase(Status_vch) EQ lcase("Non Delivered")>
					<cfset dataout.NONDELIVERED = Total />
				<cfelseif lcase(Status_vch) EQ lcase("Lost Notification")>
					<cfset dataout.LOSTNOTI = Total />
				</cfif>
			</cfloop>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch type="Any" >
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	<cffunction access="remote" name="GetTotalOptOutByBatch" >
		<cfargument name="inpBatchId" type="string" required="yes">
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.OPTOUT = 0 />

		<cfset var GetTotal = ''/>

		<cftry>
			<cfquery name="GetTotal" datasource="#Session.DBSourceREAD#">
					SELECT
						count(*) as Total
					FROM
						simplelists.optinout
					WHERE
						Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
					AND
						OptOut_dt IS NOT NULL
					AND
						OptIn_dt IS NULL
			</cfquery>
			<cfset dataout.OPTOUT = GetTotal.Total />
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Get data successfully" />
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- Function get list response blast by campaignid --->
	<cffunction name="GetResponseBlastListByCampaignId" access="remote" hint="Get response blast list by campaign id">
    	<cfargument name="inpBatchId" required="true" type="numeric"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="customFilter" default="">
		<cfargument name="inpSkipPaging" default="0"/>
		<cfargument name="sSortDir_0" default="DESC">

    	<cfset var dataout = {} />
    	<cfset var getList = '' />
    	<cfset var rsCount = '' />
    	<cfset var tmp = {} />
    	<cfset dataout['DATALIST'] = [] />
    	<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset var filterItem = ''/>

    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				-- IREResultsId_bi,
					BatchId_bi,
					Response_vch,
					Created_dt,
					ContactString_vch
    			FROM
    				simplexresults.ireresults
    			WHERE
    				Batchid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>

    			<cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
               	AND IREType_int = 2 -- Type 2 is response from user

                ORDER BY
					Created_dt #sSortDir_0#

				 <cfif arguments.inpSkipPaging EQ 0>
                    <cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
					</cfif>
                </cfif>


    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
    			<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

	    		<cfloop query="#getList#">
	    			<cfset tmp = {
	    				//ID = IREResultsId_bi,
	    				BATCHID = BatchId_bi,
						RESPONSE = Trim(#REPLACE(Response_vch,"...",'')#),
						PHONE = ContactString_vch,
	    				CREATED = "#DateFormat(Created_dt, "yyyy-mm-dd")# #TimeFormat(Created_dt, "HH:mm:ss")#"
	    			}/>

	    			<cfset arrayAppend(dataout['DATALIST'], tmp)/>
	    		</cfloop>

	    		<cfset dataout.RXRESULTCODE = 1/>
    		</cfif>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
</cfcomponent>
