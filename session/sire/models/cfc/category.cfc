<cfcomponent>


	<cffunction name="GetCategoryGroup" access="remote" hint="Get All Group of Category">
		<cfset var dataout = {} />
		<cfset var queryCTGList	= '' />

		<cfset dataout["DATALIST"] = ArrayNew() />

		<cftry>
			<cfset dataout.RXRESULTCODE = "0" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
			<cfquery datasource="#Session.DBSourceEBM#" name="queryCTGList">
				SELECT 
					CGID_int AS ID,
					CGNAME_vch AS NAME,
					CGSTATUS_int AS STATUS
				FROM 
					simpleobjects.templatecategorygroups
				WHERE 
					1
			</cfquery>
			<cfloop query="queryCTGList">
				<cfset var item = {id = "#ID#", name = "#NAME#"} />
				<cfset ArrayAppend(dataout["DATALIST"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = "1" />
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = "-1" />
	            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />         
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetCategory" access="remote" hint="Get categories">
		<cfargument name="inpGroupID" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">

		<cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var	dataout = StructNew() />
		<cfset var queryCTList	= '' />
		<cfset var rsCount = '' />
		<cfset var filterItem = {} />
		<cfset dataout["DATALIST"] = ArrayNew() />

		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
			<!--- <cfdump var="#filterData#" abort="true"/>	 --->
		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">  
		    <cfcase value="0">  
		        <cfset orderField = 'tc.CategoryName_vch' />
		    </cfcase>   
		    <cfcase value="1">  
		        <cfset orderField = 'tc.DisplayName_vch' />
		    </cfcase> 
			<cfcase value="3">  
		        <cfset orderField = 'tc.Order_int' />
		    </cfcase>
		    <cfdefaultcase>
			    <cfset orderField = 'tc.CategoryName_vch, tc.Order_int' />
		    </cfdefaultcase> 
		</cfswitch>
		<cftry>
			<cfset dataout.RXRESULTCODE = "0" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
			<cfquery datasource="#Session.DBSourceEBM#" name="queryCTList">
				SELECT SQL_CALC_FOUND_ROWS
					tc.CID_int AS ID,
					tc.CategoryName_vch AS NAME,
					tc.DisplayName_vch AS DISPLAY_NAME,
					tc.Description_txt AS DESCRIPTION,
					tc.Order_int AS ORDER_NUMBER,
					tcg.CGNAME_vch AS GROUPNAME,
					tcg.CGID_int AS GROUPID
				FROM 
					simpleobjects.template_category tc
					LEFT JOIN  simpleobjects.templatecategorygroups tcg ON tc.GroupId_int = tcg.CGID_int
				WHERE 
					1
				<cfif arguments.inpGroupID GT 0>
					AND tc.GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpGroupID#">
				</cfif>
				<cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						AND #PreserveSingleQuotes(filterItem.field)# #filterItem.operator# <cfif filterItem.type EQ 'varchar'>
							<CFQUERYPARAM CFSQLTYPE='cf_sql_#filterItem.type#' VALUE='%#filterItem.VALUE#%'>
						<cfelse>
							<CFQUERYPARAM CFSQLTYPE='cf_sql_#filterItem.type#' VALUE='#filterItem.VALUE#'>
						</cfif>
					</cfloop>
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				</cfif>
				<cfif arguments.iDisplayStart GT 0 AND arguments.iDisplayLength GT 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>
			</cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="queryCTList">
				<cfset var item = {id = "#ID#", name = "#NAME#", display_name = "#DISPLAY_NAME#", description = "#DESCRIPTION#", order_number = "#ORDER_NUMBER#", group_name = "#GROUPNAME#", group_id = "#GROUPID#"} />
				<cfset ArrayAppend(dataout["DATALIST"], item) />
			</cfloop>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
			<cfset dataout.RXRESULTCODE = "1" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
	            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>



	<cffunction name="GetItemBySubCatID" access="remote" hint="Get Item by subCatId">
		<cfargument name="inpCatId" TYPE="numeric" required="no" default="0" />

		<cfset var	dataout = StructNew() />
		<cfset var queryCTList	= '' />
		<cfset dataout["DATALIST"] = ArrayNew() />

		<cftry>
			<cfset dataout.RXRESULTCODE = "0" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
			<cfquery datasource="#Session.DBSourceEBM#" name="queryCTList">
				SELECT 
					tx.TID_int AS ID,
					tx.Name_vch AS DISPLAY_NAME
				FROM 
					simpleobjects.template_category tc
					INNER JOIN simpleobjects.templatexmlcategory txc ON tc.CID_int = txc.template_categoryID_int
					INNER JOIN simpleobjects.templatesxml tx ON txc.template_xmlID_int = tx.TID_int
				WHERE
					1
					<cfif arguments.inpCatId GT 0>
						AND txc.template_categoryID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCatId#">
					</cfif>
				ORDER BY 
					txc.CreatedAt_dt
			</cfquery>
			<cfloop query="queryCTList">
				<cfset var item = {id = "#ID#", name = "#DISPLAY_NAME#"} />
				<cfset ArrayAppend(dataout["DATALIST"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = "1" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
	            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />


	</cffunction>


	<cffunction name="GetDataList" access="remote" hint="Get admin category list">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">
		<cfargument name="paginate" TYPE="string" default="yes">

		<cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var dataout = {}/>
		<cfset var queryCtList = "" />
		<cfset var rsGetUserList = "" />
		<cfset var categories = [] />
		<cfset var filterItem	= "" />
		<cfset var rsCount	= "" />
		<cfset var rsQueryGetList	= "" />

		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["DATALIST"] = ArrayNew()>

		<cftry>

			<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList" result="rsQueryGetList">
				SELECT SQL_CALC_FOUND_ROWS
					tcg.CGNAME_vch AS CatGroupName,
					tc.CategoryName_vch AS CatName,
					tx.Name_vch AS TemplateName,
					txc.xmlctgID_int AS CatTemplateID,
					tcg.CGID_int AS CatGroupID,
					tc.CID_int AS CatID,
					tx.TID_int AS TemplateID,
					txc.status_int AS ItemStatus
				FROM simpleobjects.templatecategorygroups tcg
					INNER JOIN simpleobjects.template_category tc ON tc.GroupId_int = tcg.CGID_int
					INNER JOIN simpleobjects.templatexmlcategory txc ON tc.CID_int = txc.template_categoryID_int
					INNER JOIN simpleobjects.templatesxml tx ON txc.template_xmlID_int = tx.TID_int
				WHERE 
					1
					<cfif customFilter NEQ "">
						<cfloop array="#filterData#" index="filterItem">
							AND #PreserveSingleQuotes(filterItem.field)# = <CFQUERYPARAM CFSQLTYPE='cf_sql_integer' VALUE='#filterItem.VALUE#'>
						</cfloop>
					</cfif>
				ORDER BY 
					txc.template_categoryID_int,
					txc.Order_int 
				<cfif arguments.paginate EQ "yes">
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>
				
			</cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] 	   = iTotalRecords>
			</cfloop>

			<cfloop query="#queryCtList#" >
				<cfset var dataItem = [
					"#CatGroupName#",
					"#CatName#",
					"#TemplateName#",
					{
						CatTemplateID = "#CatTemplateID#",
						CatGroupID = "#CatGroupID#",
						CatID = "#CatID#",
						TemplateID = "#TemplateID#",
						ItemStatus = "#ItemStatus#"
					}
				] />
				<cfset ArrayAppend(dataout["DATALIST"], dataItem) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataout) />
	</cffunction>







	<cffunction name="ChangeState" access="remote" hint="Change status (active/deaction) an item">
		<cfargument name="inpItemID" required="yes"  type="numeric" default="0">
		<cfset var dataout = {} />
		<cfset var queryCtList	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsQueryGetList	= "" />

		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList" result="rsQueryGetList">
					UPDATE 
						simpleobjects.templatexmlcategory
					SET 
						Status_int = ABS(Status_int -1) 
					WHERE 
						xmlctgID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#"> 
					LIMIT
						1
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif> 
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>


		<cfreturn serializeJSON(dataout) />

	</cffunction>



	<cffunction name="GetItemByID" access="remote" hint="Get an item by item ID">
		<cfargument name="inpItemID" required="yes"  type="numeric" default="0">

		<cfset var dataout = {} />
		<cfset var queryCtList	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsQueryGetList	= "" />
		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />


		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList" result="rsQueryGetList">
					UPDATE 
						simpleobjects.templatexmlcategory
					SET 
						Status_int = ABS(Status_int -1) 
					WHERE 
						xmlctgID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#"> 
					LIMIT
						1
				</cfquery>
			</cfif>
			<cfset dataout.RXRESULTCODE = 1 />


			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>


	<cffunction name="UpdateItem" access="remote" hint="Get an item by item ID">
		<cfargument name="inpItemID" required="yes"  type="numeric" default="0">
		<cfargument name="inpTxID" required="yes"  type="numeric">
		<cfargument name="inpTcID" required="yes"  type="numeric">

		<cfset var dataout = {} />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsCheckExist	= "" />
		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfinvoke method="isExistsItem" returnvariable="rsCheckExist">
					<cfinvokeargument name="inpCatID" value="#arguments.inpTcID#">
					<cfinvokeargument name="inpTemID" value="#arguments.inpTxID#">
				</cfinvoke>
				<cfif rsCheckExist EQ false>
					<cfquery datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.templatexmlcategory
						SET 
							template_categoryID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTcID#"> ,
							template_xmlID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTxID#">
						WHERE 
							xmlctgID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#"> 
						LIMIT
							1
					</cfquery>

					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "Insert successfully!" />
				<cfelse>
					<cfset dataout.MESSAGE = "This item already exists!" />
    				<cfset dataout.ERRMESSAGE = "This item already exists!" />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>



	<cffunction name="GetAllTemplate" access="remote" hint="Get All Template XML">
		<cfset var	dataout = StructNew() />
		<cfset var queryTxList	= "" />
		<cfset dataout["DATALIST"] = ArrayNew() />
		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfquery datasource="#Session.DBSourceEBM#" name="queryTxList">
				SELECT 
					TID_int AS ID,
					Name_vch AS NAME
				FROM
					simpleobjects.templatesxml
				WHERE 
					Active_int = 1
				ORDER BY 
					TID_int, Name_vch
			</cfquery>
			<cfloop query="queryTxList">
				<cfset var item = {id = "#ID#", name = "#NAME#"} />
				<cfset ArrayAppend(dataout["DATALIST"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetCategoryByID" access="remote" hint="Get categories">
		<cfargument name="inpCatID" required="yes"  type="numeric" default="0">

		<cfset var	dataout = StructNew() />
		<cfset var queryCtList	= "" />
		<cfset dataout["DATALIST"] = StructNew()/>


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />


		<cftry>

			<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList">
				SELECT 
					tc.CID_int AS ID,
					tc.CategoryName_vch AS NAME,
					tc.DisplayName_vch AS DISPLAY_NAME,
					tc.Description_txt AS DESCRIPTION,
					tcg.CID_int AS CGID
				FROM 
					simpleobjects.template_category tc
					INNER JOIN  simpleobjects.templatecategorygroups tcg.CGID_int ON tc.GroupId_int
				WHERE 
					tc.CID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCatID#"> 
				ORDER BY
					CID_int, DisplayName_vch 
			</cfquery>

			<cfloop query="queryCtList">
				<cfset dataout["DATALIST"] = {id = "#ID#", name = "#DISPLAY_NAME#", cgid = "#CGID#"} />
			</cfloop>


			<cfset dataout.RXRESULTCODE = 1 />


			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>



	<cffunction name="RemoveItem" access="remote" hint="Remove a new template from category">
		<cfargument name="inpItemID" required="yes"  type="numeric" default="0">
		<cfset var dataout = {} />
		<cfset var queryCtList	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsQueryGetList	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList" result="rsQueryGetList">
					DELETE FROM 
						simpleobjects.templatexmlcategory
					WHERE 
						xmlctgID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#"> 
					LIMIT
						1
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif> 
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="AddItem" access="remote" hint="An a new template to category">
		<cfargument name="inpCatID" required="yes"  type="numeric" default="0">
		<cfargument name="inpTemID" required="yes"  type="numeric" default="0">


		<cfset var dataout = {} />
		<cfset var queryInsert	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsCheckExist	= "" />
		<cfset var lastOrder = 0 />
		<cfset var newItem = {} />

		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>


				<cfinvoke method="isExistsItem" returnvariable="rsCheckExist">
					<cfinvokeargument name="inpCatID" value="#arguments.inpCatID#">
					<cfinvokeargument name="inpTemID" value="#arguments.inpTemID#">
				</cfinvoke>
				<cfif rsCheckExist EQ false>
					<cfquery datasource="#Session.DBSourceEBM#" result="queryInsert">
						INSERT INTO
							simpleobjects.templatexmlcategory(template_categoryID_int, template_xmlID_int)
						VALUES
						(
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCatID#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTemID#">
						)
					</cfquery>

					<cfif queryInsert.RecordCount GT 0 >
						<cfinvoke method="getTXCByID" returnvariable="newItem" >
							<cfinvokeargument name="inpID" value="#queryInsert.generatedKey#" />
						</cfinvoke>

						<cfinvoke method="getLastOrderInGroup" returnvariable="lastOrder">
							<cfinvokeargument name="inpCatID" value="#newItem.template_categoryID_int#" />
						</cfinvoke>
						<cfif lastOrder GT 0>
							<cfset lastOrder += 1 />
							<cfquery datasource="#Session.DBSourceEBM#" result="queryInsert">
								UPDATE 
									simpleobjects.templatexmlcategory
								SET 
									Order_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#lastOrder#">
								WHERE 
									xmlctgID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#queryInsert.generatedKey#">
								LIMIT 1
							</cfquery>
						</cfif>
					</cfif>

					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "Insert successfully!" />
				<cfelse>
					<cfset dataout.MESSAGE = "This item already exists!" />
    				<cfset dataout.ERRMESSAGE = "This item already exists!" />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction> 


	<cffunction access="private" name="isExistsItem" hint="Check if an item has existed">
		<cfargument name="inpCatID" required="yes"  type="numeric" default="0">
		<cfargument name="inpTemID" required="yes"  type="numeric" default="0">
		<cfset var rs = false />
		<cfset var queryCtList	= '' />

		<cftry>

			<cfquery datasource="#Session.DBSourceEBM#" name="queryCtList">
				SELECT  
					COUNT(1) AS COUNT
				FROM
					simpleobjects.templatexmlcategory
				WHERE 
					template_categoryID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCatID#"> 
					AND 
					template_xmlID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTemID#">
				LIMIT
					1
			</cfquery>
			<cfloop query="queryCtList">
				<cfif COUNT GT 0>
					<cfset rs = true />
				</cfif>
			</cfloop>
			<cfcatch>
			</cfcatch>
		</cftry>

		<cfreturn rs />
	</cffunction>



	<cffunction access="remote" name="addNewCategory" hint="Add a new template category">
		<cfargument name="inpName" required="yes"  type="string">
		<cfargument name="inpDisplayName" required="yes"  type="string">
		<cfargument name="inpDescription" required="yes"  type="string">
		<cfargument name="inpGroupID" required="yes"  type="numeric">
		<cfargument name="inpOrder" required="no"  type="numeric" default="0">


		<cfset var dataout = {} />
		<cfset var queryInsert	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsOrderValidation = false />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfinvoke method="ValidateOrderValue" returnvariable="rsOrderValidation">
					<cfinvokeargument name="inpGroupID" value="#arguments.inpGroupID#">
					<cfinvokeargument name="inpOrder" value="#arguments.inpOrder#">
				</cfinvoke>
				<cfif rsOrderValidation EQ true >
					<cfquery datasource="#Session.DBSourceEBM#" name="queryInsert">
						INSERT IGNORE INTO 
							 simpleobjects.template_category
							 (
							 	CategoryName_vch,
							 	DisplayName_vch,
							 	Description_txt,
							 	GroupId_int,
							 	Order_int
							 )
						VALUES
							(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpName#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDisplayName#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#arguments.inpDescription#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupID#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOrder#">
							)
					</cfquery>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "Insert successfully!" />
				<cfelse>
					<cfset dataout.RXRESULTCODE = -2 />
					<cfset dataout.MESSAGE = "Order value must unique in each category!" />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction access="remote" name="updateCategory" hint="update an existed template category">
		<cfargument name="inpID" required="yes"  type="numeric">
		<cfargument name="inpName" required="yes"  type="string">
		<cfargument name="inpDisplayName" required="yes"  type="string">
		<cfargument name="inpDescriptionEdit" required="yes"  type="string">
		<cfargument name="inpGroupID" required="yes"  type="numeric">
		<cfargument name="inpOrder" required="no"  type="numeric" default="0">

		<cfset var dataout = {} />
		<cfset var queryUpdate	= "" />
		<cfset var RetVarGetAdminPermission	= "" />
		<cfset var rsOrderValidation	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfinvoke method="ValidateOrderValue" returnvariable="rsOrderValidation">
					<cfinvokeargument name="inpGroupID" value="#arguments.inpGroupID#">
					<cfinvokeargument name="inpOrder" value="#arguments.inpOrder#">
					<cfinvokeargument name="inpID" value="#arguments.inpID#">
				</cfinvoke>
				<cfif rsOrderValidation EQ true >
					<cfquery datasource="#Session.DBSourceEBM#" name="queryUpdate">
						UPDATE simpleobjects.template_category
						SET CategoryName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpName#">,
						 	DisplayName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDisplayName#">,
						 	Description_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#arguments.inpDescriptionEdit#">,
						 	GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupID#">,
						 	Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOrder#">
						WHERE 
							CID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#">
						LIMIT 
							1
					</cfquery>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.MESSAGE = "Update successfully!" />
				<cfelse>
					<cfset dataout.RXRESULTCODE = -2 />
					<cfset dataout.MESSAGE = "Order value must unique in each category!" />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>




	<cffunction access="remote" name="deleteCategory" hint="update an existed template category">
		<cfargument name="inpID" required="yes"  type="numeric">

		<cfset var dataout = {} />
		<cfset var queryUpdate	= "" />
		<cfset var RetVarGetAdminPermission	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1">
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>
				<cfquery datasource="#Session.DBSourceEBM#" name="queryUpdate">
					DELETE FROM simpleobjects.template_category
				
					WHERE 
						CID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#">
					LIMIT 
						1
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Delete successfully!" />
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction access="private" name="ValidateOrderValue" hint="Check exists cate item">
		<cfargument name="inpGroupID" required="yes"  type="numeric">
		<cfargument name="inpOrder" required="yes"  type="numeric" default="0">
		<cfargument name="inpID" required="no"  type="numeric" default="0">
		<cfset var rs = false />
		<cfset var rsQuery = "" />
		<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
			SELECT 
				COUNT(1) AS COUNT
			FROM 
				simpleobjects.template_category
			WHERE 
				Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOrder#">
				AND 
				GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupID#">
				<cfif  arguments.inpID GT 0>
					AND CID_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#">
				</cfif>
		</cfquery>
		<cfif rsQuery.COUNT EQ 0>
			<cfset rs = true />
		</cfif>
		<cfreturn rs />
	</cffunction>




	<cffunction name="moveUp" access="remote" hint="Push a templdate order to up 1">
		<cfargument name="inpID" required="yes"  type="numeric">

		<cfset var dataout = {} />
		<cfset var rsQuery = {} />
		<cfset var item	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

	    <cftry>
			<cfinvoke method="getPreviousItem" returnvariable="item" >
				<cfinvokeargument name="inpID" value="#arguments.inpID#" />
			</cfinvoke>

			<cfif item.RecordCount GT 0>
				<cfset var order = item.Order_int>
				<cfset var itemOrder = item.Order_int + 1 />
				<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
					UPDATE 
						simpleobjects.templatexmlcategory
					SET 
						Order_int = CASE xmlctgID_int
								    WHEN 
								    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#"> THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">
								    WHEN
								    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.xmlctgID_int#"> THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itemOrder#">
								    ELSE 
								    	Order_int
								    END
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Move successfully!" />
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
			
		</cftry>
		

		<cfreturn dataout />
	</cffunction>


	<cffunction name="moveDown" access="remote" hint="Push a templdate order to down 1">
		<cfargument name="inpID" required="yes"  type="numeric">


		<cfset var dataout = {} />
		<cfset var rsQuery = {} />
		<cfset var item	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />

	    <cftry>
			<cfinvoke method="getNextItem" returnvariable="item" >
				<cfinvokeargument name="inpID" value="#arguments.inpID#" />
			</cfinvoke>

			<cfif item.RecordCount GT 0>
				<cfset var order = item.Order_int>
				<cfset var itemOrder = item.Order_int - 1 />
				<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
					UPDATE 
						simpleobjects.templatexmlcategory
					SET 
						Order_int = CASE xmlctgID_int
								    WHEN 
								    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#"> THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#order#">
								    WHEN
								    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.xmlctgID_int#"> THEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itemOrder#">
								    ELSE 
								    	Order_int
								    END
				</cfquery>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Move successfully!" />
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
			
		</cftry>
		

		<cfreturn dataout />
	</cffunction>


	<cffunction name="moveToFirst" access="remote" hint="Push a templdate order to first position">
		<cfargument name="inpID" required="yes"  type="numeric">

		<cfset var dataout = {} />
		<cfset var rsQuery = {} />
		<cfset var item	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
 
	    <cftry>
			<cfinvoke method="getTXCByID" returnvariable="item" >
				<cfinvokeargument name="inpID" value="#arguments.inpID#" />
			</cfinvoke>

			<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
				UPDATE 
					simpleobjects.templatexmlcategory
				SET 
					Order_int = CASE xmlctgID_int
							    WHEN 
							    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#"> THEN 1
							    ELSE 
							    	Order_int + 1
							    END
				WHERE 
					template_categoryID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.template_categoryID_int#"> 
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Move successfully!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>	

		<cfreturn dataout />
	</cffunction>


	<cffunction name="moveToLast" access="remote" hint="Push a templdate order to last position">
		<cfargument name="inpID" required="yes"  type="numeric">

		<cfset var dataout = {} />
		<cfset var rsQuery = {} />
		<cfset var item	= "" />
		<cfset var lastOrder	= "" />


		<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />


		<cftry>

			<cfinvoke method="getTXCByID" returnvariable="item" >
				<cfinvokeargument name="inpID" value="#arguments.inpID#" />
			</cfinvoke>


			<cfinvoke method="getLastOrderInGroup" returnvariable="lastOrder">
				<cfinvokeargument name="inpCatID" value="#item.template_categoryID_int#" />
			</cfinvoke>
			<cfset lastOrder += 1 />

			<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
				UPDATE 
					simpleobjects.templatexmlcategory
				SET 
					Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lastOrder#"> 
				WHERE 
					xmlctgID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#"> 
			</cfquery>	
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Move successfully!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		

		<cfreturn dataout />
	</cffunction>







	<cffunction access="private" name="getTXCByID">
		<cfargument name="inpID" required="yes"  type="numeric">
		<cfset var rsQuery = {} />
		<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
			SELECT 
				xmlctgID_int,
				template_categoryID_int,
				template_xmlID_int,
				order_int,
				status_int,
				createdAt_dt
			FROM 
				simpleobjects.templatexmlcategory
			WHERE 
				xmlctgID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpID#">
			LIMIT 1
		</cfquery>
		<cfreturn rsQuery />
	</cffunction>



	<cffunction access="private" name="getLastOrderInGroup">
		<cfargument name="inpCatID" required="yes"  type="numeric">
		<cfset var rsQuery = {} />
		<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
			SELECT 
				MAX(Order_int) AS LastOrder
			FROM 
				simpleobjects.templatexmlcategory
			WHERE 
				template_categoryID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCatID#">
			LIMIT 1
		</cfquery>
		<cfreturn rsQuery.LastOrder />
	</cffunction>



	<cffunction access="private" name="getPreviousItem">
		<cfargument name="inpID" required="yes"  type="numeric">
		<cfset var rsQuery = {} />
		<cfset var item	= "" />
		<cfinvoke method="getTXCByID" returnvariable="item" >
			<cfinvokeargument name="inpID" value="#arguments.inpID#" />
		</cfinvoke>

		<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
			SELECT 
				xmlctgID_int,
				template_categoryID_int,
				template_xmlID_int,
				order_int,
				status_int,
				createdAt_dt
			FROM 
				simpleobjects.templatexmlcategory
			WHERE 
				Order_int < <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.order_int#">
				AND 
				template_categoryID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.template_categoryID_int#">
			ORDER BY
				Order_int DESC
			LIMIT 1
		</cfquery>
		<cfreturn rsQuery />
	</cffunction>




	<cffunction access="private" name="getNextItem">
		<cfargument name="inpID" required="yes"  type="numeric">
		<cfset var rsQuery = {} />
		<cfset var item	= "" />
		<cfinvoke method="getTXCByID" returnvariable="item" >
			<cfinvokeargument name="inpID" value="#arguments.inpID#" />
		</cfinvoke>

		<cfquery datasource="#Session.DBSourceEBM#" name="rsQuery">
			SELECT 
				xmlctgID_int,
				template_categoryID_int,
				template_xmlID_int,
				order_int,
				status_int,
				createdAt_dt
			FROM 
				simpleobjects.templatexmlcategory
			WHERE 
				Order_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.order_int#">
				AND 
				template_categoryID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item.template_categoryID_int#">
			ORDER BY
				Order_int
			LIMIT 1
		</cfquery>
		<cfreturn rsQuery />
	</cffunction>

	<cffunction name="RenameTemplate" access="remote" hint="Rename Template by Template ID">
		<cfargument name="inpTemplateId" required="true" type="numeric">
		<cfargument name="inpTemplateName" required="true" type="string">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var renameTemplate = '' />
		<cfset var updateResult = '' />
		<cfset var validationResult = '' />
		<cfset var checkDuplicatedTemplateName = '' />

		<cftry>
			<cfif arguments.inpTemplateName EQ ''>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please enter new Template Name!'>
				<cfreturn dataout/>
			<cfelse>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpTemplateName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>

				<cfquery name="checkDuplicatedTemplateName" datasource="#Session.DBSourceEBM#">
					SELECT
						Name_vch, TID_int
					FROM
						simpleobjects.templatesxml
					WHERE
						Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTemplateName#">
					AND
						TID_int != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTemplateId#">
					LIMIT
						1
				</cfquery>

				<cfif checkDuplicatedTemplateName.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = -4>
				    <cfset dataout.MESSAGE = 'Oops! Duplicated template name, please input another name.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfquery name="renameTemplate" datasource="#Session.DBSourceEBM#" result="updateResult">
				UPDATE
					simpleobjects.templatesxml
				SET
					Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTemplateName#">
				WHERE
					TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTemplateId#">
			</cfquery>

			<cfif updateResult.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = 'Rename Template successfully!'/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = 'No record done'/>
			</cfif>
 			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" /> 
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

</cfcomponent>

