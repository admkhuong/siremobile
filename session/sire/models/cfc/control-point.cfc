<cfcomponent>
	<cfinclude template="/session/administration/constants/userConstants.cfm">
	<cfinclude template="/session/administration/constants/scheduleConstants.cfm">
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/session/sire/configs/paths.cfm">
	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="encodeStringXml" access="public" returntype="string" output="false" hint="Encode several special characters that make xml invalid">
		<cfargument	name="string" type="string" required="true" hint="" />

		<cfset arguments.string = Replace(arguments.string, '&', '&amp;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '"', '&quot;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '<', '&lt;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '>', '&gt;', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, "'", '&apos;', 'ALL') />
		<cfreturn string />
	</cffunction>

	<cffunction name="decodeStringXml" access="public" returntype="string" 	output="false" hint="Decode xml string">
		<cfargument name="string" type="any" required="true" hint="" />
		<cfset arguments.string = Replace(arguments.string, '&amp;', '&', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&quot;', '"', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&lt;', '<', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&gt;', '>', 'ALL') />
		<cfset arguments.string = Replace(arguments.string, '&apos;', "'", 'ALL') />
		<cfreturn string />
	</cffunction>

	<cffunction	name="XmlDeleteNodes" access="public" returntype="void"	output="false" hint="I remove a node or an array of nodes from the given XML document.">
		<!--- Define arugments. --->
		<cfargument	name="XmlDocument" type="any" required="true" hint="I am a ColdFusion XML document object."/>

		<cfargument	name="Nodes" type="any"	required="false" hint="I am the node or an array of nodes being removed from the given document."/>

		<!--- Define the local scope. --->
		<cfset var LOCAL = StructNew() />

		<!---
			Check to see if we have a node or array of nodes. If we
			only have one node passed in, let's create an array of
			it so we can assume an array going forward.
		--->
		<cfif NOT IsArray( ARGUMENTS.Nodes )>

			<!--- Get a reference to the single node. --->
			<cfset LOCAL.Node = ARGUMENTS.Nodes />

			<!--- Convert single node to array. --->
			<cfset ARGUMENTS.Nodes = [ LOCAL.Node ] />

		</cfif>


		<!---
			Flag nodes for deletion. We are going to need to delete
			these via the XmlChildren array of the parent, so we
			need to be able to differentiate them from siblings.
			Also, we only want to work with actual ELEMENT nodes,
			not attributes or anything, so let's remove any nodes
			that are not element nodes.
		--->
		<cfloop	index="LOCAL.NodeIndex"	from="#ArrayLen( ARGUMENTS.Nodes )#" to="1"	step="-1">

			<!--- Get a node short-hand. --->
			<cfset LOCAL.Node = ARGUMENTS.Nodes[ LOCAL.NodeIndex ] />

			<!---
				Check to make sure that this node has an XmlChildren
				element. If it does, then it is an element node. If
				not, then we want to get rid of it.
			--->
			<cfif StructKeyExists( LOCAL.Node, "XmlChildren" )>

				<!--- Set delet flag. --->
				<cfset LOCAL.Node.XmlAttributes[ "delete-me-flag" ] = "true" />

			<cfelse>

				<!---
					This is not an element node. Delete it from out
					list of nodes to delete.
				--->
				<cfset ArrayDeleteAt(ARGUMENTS.Nodes, LOCAL.NodeIndex) />

			</cfif>

		</cfloop>


		<!---
			Now that we have flagged the nodes that need to be
			deleted, we can loop over them to find their parents.
			All nodes should have a parent, except for the root
			node, which we cannot delete.
		--->
		<cfloop	index="LOCAL.Node" array="#ARGUMENTS.Nodes#">
			<!--- Get the parent node. --->
			<cfset LOCAL.ParentNodes = XmlSearch( LOCAL.Node, "../" ) />

			<!---
				Check to see if we have a parent node. We can't
				delete the root node, and we also be deleting other
				elements as well - make sure it is all playing
				nicely together. As a final check, make sure that
				out parent has children (only happens if we are
				dealing with the root document element).
			--->
			<cfif (
				ArrayLen( LOCAL.ParentNodes ) AND
				StructKeyExists( LOCAL.ParentNodes[ 1 ], "XmlChildren" )
				)>

				<!--- Get the parent node short-hand. --->
				<cfset LOCAL.ParentNode = LOCAL.ParentNodes[ 1 ] />

				<!---
					Now that we have a parent node, we want to loop
					over it's children to one the nodes flagged as
					deleted (and delete them). As we do this, we
					want to loop over the children backwards so that
					we don't go out of bounds as we start to remove
					child nodes.
				--->
				<cfloop
					index="LOCAL.NodeIndex"
					from="#ArrayLen( LOCAL.ParentNode.XmlChildren )#"
					to="1"
					step="-1">

					<!--- Get the current node shorthand. --->
					<cfset LOCAL.Node = LOCAL.ParentNode.XmlChildren[ LOCAL.NodeIndex ] />

					<!---
						Check to see if this node has been flagged
						for deletion.
					--->
					<cfif StructKeyExists( LOCAL.Node.XmlAttributes, "delete-me-flag" )>

						<!--- Delete this node from parent. --->
						<cfset ArrayDeleteAt(
							LOCAL.ParentNode.XmlChildren,
							LOCAL.NodeIndex
							) />

						<!---
							Clean up the node by removing the
							deletion flag. This node might still be
							used by another part of the program.
						--->
						<cfset StructDelete(
							LOCAL.Node.XmlAttributes,
							"delete-me-flag"
							) />

					</cfif>

				</cfloop>

			</cfif>

		</cfloop>
		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<!--- --------------------------------------------------------------------------------------- ----



	Blog Entry:

	Ask Ben: Getting XML Values And Aggregates In ColdFusion



	Author:

	Ben Nadel / Kinky Solutions



	Link:

	http://www.bennadel.com/index.cfm?event=blog.view&id=925



	Date Posted:

	Aug 27, 2007 at 7:00 AM



		hint="Provides some XML utilities that live on top of XmlSearch() and XPath.">



---- --------------------------------------------------------------------------------------- --->
	<cffunction

		name="Init"

		access="public"

		returntype="any"

		output="false"

		hint="Returns an initialized component instance.">



		<!--- Return This reference. --->

		<cfreturn THIS />

	</cffunction>





	<cffunction

		name="GetValueAggregate"

		access="public"

		returntype="numeric"

		output="false"

		hint="Gets the given aggregate of the matched attribute or element text values. Available aggregates are SUM, MIN, MAX, AVG.">



		<!--- Define arguments. --->

		<cfargument

			name="XML"

			type="any"

			required="true"

			hint="The ColdFusion XML document we are searching."

			/>



		<cfargument

			name="XPath"

			type="string"

			required="true"

			hint="The XPAth that will return the XML nodes from which we will be getting the values for our array."

			/>



		<cfargument

			name="Aggregate"

			type="string"

			required="true"

			hint="The SUM, MIN, MAX, AVG aggregate run on the returned values."

			/>



		<!--- Define the local scope. --->

		<cfset var LOCAL = StructNew() />



		<!--- Get the value array. --->

		<cfset LOCAL.Values = THIS.GetValueArray(

			XML = ARGUMENTS.XML,

			XPath = ARGUMENTS.XPath,

			NumericOnly = true

			) />



		<!--- Check to see which aggregate we are running. --->

		<cfswitch expression="#ARGUMENTS.Aggregate#">

			<cfcase value="MIN">

				<cfreturn ArrayMin( LOCAL.Values ) />

			</cfcase>

			<cfcase value="MAX">

				<cfreturn ArrayMax( LOCAL.Values ) />

			</cfcase>

			<cfcase value="AVG">

				<cfreturn ArrayAvg( LOCAL.Values ) />

			</cfcase>



			<!--- By default, return SUM. --->

			<cfdefaultcase>

				<cfreturn ArraySum( LOCAL.Values ) />

			</cfdefaultcase>

		</cfswitch>

	</cffunction>





	<cffunction

		name="GetValueArray"

		access="public"

		returntype="array"

		output="false"

		hint="Returns an array of of either attribute values or node text values.">



		<!--- Define arguments. --->

		<cfargument

			name="XML"

			type="any"

			required="true"

			hint="The ColdFusion XML document we are searching."

			/>



		<cfargument

			name="XPath"

			type="string"

			required="true"

			hint="The XPAth that will return the XML nodes from which we will be getting the values for our array."

			/>



		<cfargument

			name="NumericOnly"

			type="boolean"

			required="false"

			default="false"

			hint="Flags whether only numeric values will be selected."

			/>



		<!--- Define the local scope. --->

		<cfset var LOCAL = StructNew() />



		<!---

			Get the matching XML nodes based on the

			given XPath.

		--->

		<cfset LOCAL.Nodes = XmlSearch(

			ARGUMENTS.XML,

			ARGUMENTS.XPath

			) />





		<!--- Set up an array to hold the returned values. --->

		<cfset LOCAL.Return = ArrayNew( 1 ) />



		<!--- Loop over the matched nodes. --->

		<cfloop

			index="LOCAL.NodeIndex"

			from="1"

			to="#ArrayLen( LOCAL.Nodes )#"

			step="1">



			<!--- Get a short hand to the current node. --->

			<cfset LOCAL.Node = LOCAL.Nodes[ LOCAL.NodeIndex ] />



			<!---

				Check to see what kind of value we are getting -

				different nodes will have different values. When

				getting the value, we must also check to see if

				only numeric values are being returned.

			--->

			<cfif (

				StructKeyExists( LOCAL.Node, "XmlText" ) AND

				(

					(NOT ARGUMENTS.NumericOnly) OR

					IsNumeric( LOCAL.Node.XmlText )

				))>



				<!--- Add the element node text. --->

				<cfset ArrayAppend(

					LOCAL.Return,

					LOCAL.Node.XmlText

					) />



			<cfelseif (

				StructKeyExists( LOCAL.Node, "XmlValue" ) AND

				(

					(NOT ARGUMENTS.NumericOnly) OR

					IsNumeric( LOCAL.Node.XmlValue )

				))>



				<!--- Add the attribute node value. --->

				<cfset ArrayAppend(

					LOCAL.Return,

					LOCAL.Node.XmlValue

					) />



			</cfif>



		</cfloop>





		<!--- Return value array. --->

		<cfreturn LOCAL.Return />

	</cffunction>

	<cffunction name="AddNewBatchFromTemplate" access="public" output="false" hint="Simple create Batch from Template Id">
		<cfargument name="inpTemplateId" required="true" hint="The template Id">
		<cfargument name="inpDesc" required="false" default="" hint="The name of the Batch">
		<cfargument name="inpKeyword" required="false" default="" hint="A keyword to associate with the Batch">
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to Set HELP / STOP for." />
		<cfargument name="inpAddList" TYPE="string" default="0" required="false" hint="Automatically Add a new Subscriber list and link to Opt Ins in this flow" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = 0 />
		<cfset dataout.DESC = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.TEMPLATETYPE = 0 />
		<cfset dataout.CustomHelpMessage_vch = "" />
		<cfset dataout.CustomStopMessage_vch = "" />
		<cfset dataout.XMLControlString = "" />
		<cfset dataout.CPPID = "0" />

		<cfset var DebugStr	= '' />
		<cfset var templateXMLDetailQuery	= '' />
		<cfset var inpFormData = ''/>
		<cfset var RetVarDoDynamicTransformations = '' />
		<cfset var XMLControlString = ''/>
		<cfset var InsertBatch = '' />
		<cfset var InsertBatchResult = '' />
		<cfset var RetVarGetUserOrganization = ''/>
		<cfset var RetVarSetCustomStandardReply = ''/>
		<cfset var AddSubscriberBatchRecord = ''/>
		<cfset var AddSubscriberBatchRecordResult = ''/>
		<cfset var RetCPPXData = '' />
		<cfset var RetAddMLP = '' />
		<cfset var getShortCodeId = '' />

		<cftry>

			<!--- Read template data --->
			<cfquery name="templateXMLDetailQuery" datasource="#Session.DBSourceEBM#">
		    	SELECT
			    	Name_vch,
                	XMLControlString_vch,
                	Type_int,
                	MLPTemplatedId_int
		        FROM
                	simpleobjects.templatesxml
		        WHERE
                	TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTemplateId#">
		    </cfquery>

		    <cfif templateXMLDetailQuery.RecordCount EQ 0>
		    	<cfset dataout.MESSAGE = 'This template is no longer available or has been deleted.'>
		    	<cfreturn dataout>
		    </cfif>

			<cfset dataout.TEMPLATETYPE = templateXMLDetailQuery.Type_int />

			<cfif LEN(TRIM(arguments.inpDesc)) EQ 0>
				<cfset arguments.inpDesc = ReplaceNoCase(templateXMLDetailQuery.Name_vch, '<br/>', ' ', 'all') & ' ' & LSDateFormat(now(), 'yyyy-mm-dd') & ' ' & LSTimeFormat(now(), 'HH:mm:ss')>
			</cfif>

			<cfset dataout.DESC = arguments.inpDesc />

			<!--- Read Profile data and put into form --->
            <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

            <!---Create a data set to run against dynamic data --->
            <cfset inpFormData = StructNew() />

            <cfset inpFormData.inpBrand = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
            <cfset inpFormData.inpBrandFull = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
            <cfset inpFormData.inpBrandWebURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
            <cfset inpFormData.inpBrandWebTinyURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
            <cfset inpFormData.inpBrandPhone = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
            <cfset inpFormData.inpBrandeMail = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />

            <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
            <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
            <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
            <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
            <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
            <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>

            <cfset inpFormData.CompanyNameShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
            <cfset inpFormData.CompanyName = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
            <cfset inpFormData.CompanyWebSite = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
            <cfset inpFormData.CompanyWebSiteShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
            <cfset inpFormData.CompanyPhoneNumber = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
            <cfset inpFormData.CompanyEMailAddress = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />

<!---
            <cfif inpFormData.CompanyNameShort EQ ""><cfset inpFormData.CompanyNameShort = "CompanyNameShort"></cfif>
            <cfif inpFormData.CompanyName EQ ""><cfset inpFormData.CompanyName = "CompanyName"></cfif>
            <cfif inpFormData.CompanyWebSite EQ ""><cfset inpFormData.CompanyWebSite = "CompanyWebSite"></cfif>
            <cfif inpFormData.CompanyWebSiteShort EQ ""><cfset inpFormData.CompanyWebSiteShort = "CompanyWebSiteShort"></cfif>
            <cfif inpFormData.CompanyPhoneNumber EQ ""><cfset inpFormData.CompanyPhoneNumber = "CompanyPhoneNumber"></cfif>
            <cfif inpFormData.CompanyEMailAddress EQ ""><cfset inpFormData.CompanyEMailAddress = "CompanyEMailAddress"></cfif>
--->

            <!--- Do the transforms here --->
            <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                <cfinvokeargument name="inpResponse_vch" value="#templateXMLDetailQuery.XMLControlString_vch#">
                <cfinvokeargument name="inpContactString" value="">
                <cfinvokeargument name="inpRequesterId_int" value="">
                <cfinvokeargument name="inpFormData" value="#inpFormData#">
                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
                <cfinvokeargument name="inpShortCode" value="">
                <cfinvokeargument name="inpBatchId" value="">
                <cfinvokeargument name="inpBrandingOnly" value="1">
            </cfinvoke>

            <!--- Update the current template data --->
            <cfset XMLControlString = RetVarDoDynamicTransformations.RESPONSE />

            <cfif templateXMLDetailQuery.MLPTemplatedId_int GT 0>

				<!--- Create an MLP --->
				<!--- Read exisitng template data --->
				<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
					 <cfinvokeargument name="ccpxDataId" value="#templateXMLDetailQuery.MLPTemplatedId_int#">
				</cfinvoke>

				<!--- Do the MLP transforms here --->
	            <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
	                <cfinvokeargument name="inpResponse_vch" value="#RetCPPXData.RAWCONTENT#">
	                <cfinvokeargument name="inpContactString" value="">
	                <cfinvokeargument name="inpRequesterId_int" value="">
	                <cfinvokeargument name="inpFormData" value="#inpFormData#">
	                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
	                <cfinvokeargument name="inpShortCode" value="">
	                <cfinvokeargument name="inpBatchId" value="">
	                <cfinvokeargument name="inpBrandingOnly" value="1">
	            </cfinvoke>

	            <!--- Update the current template data --->
				<cfset RetCPPXData.RAWCONTENT = RetVarDoDynamicTransformations.RESPONSE />

				<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
					<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
					<cfinvokeargument name="inpDesc" value="#RetCPPXData.CPPNAME#">
					<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
				</cfinvoke>

				<cfset dataout.ADDMLP = "#SerializeJSON(RetAddMLP)#" />

				<cfset dataout.CPPID = "#RetAddMLP.CPPID#" />

			</cfif>

			<cfquery datasource="#session.DBSourceREAD#" name="getShortCodeId">
            	SELECT
            		ShortCodeId_int
            	FROM
            		sms.shortcode
            	WHERE
            		ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
            	LIMIT 1
            </cfquery>

            <cfif getShortCodeId.RECORDCOUNT LT 1>
            	<cfthrow message="Shortcode not exists!"/>
            </cfif>

		 	<!--- Write basic batch to DB --->
		    <cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="InsertBatchResult">
				INSERT INTO simpleobjects.batch
				(
					UserId_int,
                    rxdsLibrary_int,
                    rxdsElement_int,
                    rxdsScript_int,
                    UserBatchNumber_int,
					Created_dt,
					Desc_vch,
					LastUpdated_dt,
					GroupId_int,
					XMLControlString_vch,
					Active_int,
					ContactGroupId_int,
					ContactTypes_vch,
					EMS_Flag_int,
					RealTimeFlag_int,
					MLPId_int,
					ShortCodeId_int
				)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataout.CPPID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getShortCodeId.ShortCodeId_int#">
				)
			</cfquery>

			<!--- <cfdump var="#XMLControlString#" abort="true" /> --->

			<cfquery name="AddSubscriberBatchRecord" datasource="#Session.DBSourceEBM#" result="AddSubscriberBatchRecordResult">
                INSERT INTO simplelists.subscriberbatch
                    (
                        GroupId_bi,
                        BatchId_bi,
                        UserId_int,
                        Created_dt
                        )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#InsertBatchResult.generated_key#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        NOW()
                        )
            </cfquery>

			<cfset dataout.BATCHID = #InsertBatchResult.generated_key#>

			<cfset dataout.CustomHelpMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch>
			<cfset dataout.CustomStopMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch>
			<cfset dataout.XMLControlString = XMLControlString>

			<!--- Optionally add subscriber list and link it to this XMLControlString in the OPTIN TYPEs --->
			<cfif inpAddList GT 0>
            	<!--- Add any lists as needed --->
                <cfinvoke method="CreateAndLinkSubscriberList">
					<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				</cfinvoke>
            </cfif>

			<!--- Write generic keyword HELP STOP   --->
			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="HELP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>

			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="STOP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>

			<cfset dataout.DebugStr = DebugStr>

		<cfcatch type="any">

			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>

		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="CreateAndLinkSubscriberList" access="remote" output="false" hint="Create a new blank subscriber list and then assign it to Opt In CPs.">
		<cfargument name="inpBatchId" TYPE="string" hint="The Batch Id to change"/>

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.NEWLISTID = 0 />
		<cfset dataout.NEWNAME = "Default Subscriber List for Campaign Id #inpBatchId#" />
		<cfset dataout.COUNT = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

        <cfset var isTimeOut = '' />
        <cfset var GetBatchQuestion = '' />
        <cfset var myxmldocResultDoc = '' />
        <cfset var xmlRXSSCSCElement = '' />
        <cfset var questionOPTIN = '' />
        <cfset var item = '' />
        <cfset var xmlRxss = '' />
        <cfset var rxtValue = '' />
        <cfset var nextQIDCKOfDelete = '' />
        <cfset var nextQuestionID = '' />
        <cfset var nextLink = '' />
        <cfset var previousQuestion = '' />
        <cfset var rXSSElements = '' />
        <cfset var firstMCID = '' />
        <cfset var WriteBatchOptions = '' />
        <cfset var i = '' />
        <cfset var DebugStr = '' />
        <cfset var OutToDBXMLBuff = '' />
		<cfset var QElements	= '' />
		<cfset var Q2Item	= '' />
		<cfset var selectedElementsII	= '' />
		<cfset var listQuestion	= '' />
		<cfset var RetVarAddGroup	= '' />

		<cftry>

            <!--- Secured to Session.User by query --->

			<!--- Read from DB Batch Options --->
            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                SELECT
	                desc_vch,
                    XMLControlString_vch
                FROM
                    simpleobjects.batch
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            </cfquery>

			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

        	<cfset xmlRXSSCSCElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
			<cfif ArrayLen(xmlRXSSCSCElement) GT 0 >

				<!---Search element which is deleted--->
				<cfset questionOPTIN = XmlSearch(xmlRXSSCSCElement[1], "//Q[ @TYPE = 'OPTIN' ]") />
				<cfif  ArrayLen(questionOPTIN) GT 0>

					<!--- Create new subscriber list --->
					<!--- The naming scheme will prevent duplicate lists for the same batch --->
					<cfinvoke method="AddGroup" returnVariable="RetVarAddGroup"  >
						<cfinvokeargument name="INPGROUPDESC" value="#dataout.NEWNAME#">
                    </cfinvoke>

					<cfif RetVarAddGroup.INPGROUPID GT 0>
						<cfset dataout.NEWLISTID = "#RetVarAddGroup.INPGROUPID#" />
					</cfif>
				</cfif>

				<cfif RetVarAddGroup.INPGROUPID GT 0>
					<!--- Loop over each matching question --->
					<cfloop array="#questionOPTIN#" index="listQuestion">

						<!--- ID --->
						<cfset selectedElementsII = XmlSearch(listQuestion, "@OIG")>
		                <cfif ArrayLen(selectedElementsII) GT 0>
		                    <cfset selectedElementsII[ArrayLen(selectedElementsII)].XmlValue = dataout.NEWLISTID>

		                    <cfset dataout.COUNT = dataout.COUNT + 1 />

		                </cfif>

					</cfloop>
				</cfif>
			</cfif>

        	<!--- Clean up XML for DB storage --->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

	        <!--- Secured to Session.User by query --->

			<!--- Write to DB Batch Options --->
			<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.batch
				SET
					XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>

            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.BATCHID = INPBATCHID />
			<cfset dataout.ERRMESSAGE = DebugStr />

           <cfinvoke method="AddHistory" component="session.sire.models.cfc.history">
			<cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
			<cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
			<cfinvokeargument name="EVENT" value="Auto Create and Link Subscriber List NewListId=#dataout.NEWLISTID#">
		</cfinvoke>

            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS BatchId = #INPBATCHID#">
				<cfinvokeargument name="operator" value="Auto Create and Link Subscriber List NewListId=#dataout.NEWLISTID#">
			</cfinvoke>

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="DeleteControlPoint" access="remote" output="false" hint="Delete XML Question by BatchID and the Physical ID of the CP.">
		<cfargument name="inpBatchId" TYPE="string" hint="The Batch Id to change"/>
		<cfargument name="INPPID" TYPE="string" hint="The Physical ID of the Control Point to Delete" />
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.PID = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

        <cfset var isTimeOut = '' />
        <cfset var GetBatchQuestion = '' />
        <cfset var myxmldocResultDoc = '' />
        <cfset var xmlRXSSCSCElement = '' />
        <cfset var questionDelete = '' />
        <cfset var item = '' />
        <cfset var xmlRxss = '' />
        <cfset var rxtValue = '' />
        <cfset var nextQIDCKOfDelete = '' />
        <cfset var nextQuestionID = '' />
        <cfset var nextLink = '' />
        <cfset var previousQuestion = '' />
        <cfset var rXSSElements = '' />
        <cfset var firstMCID = '' />
        <cfset var WriteBatchOptions = '' />
        <cfset var i = '' />
        <cfset var DebugStr = '' />
        <cfset var OutToDBXMLBuff = '' />
		<cfset var QElements	= '' />
		<cfset var Q2Item	= '' />

		<cftry>

			<cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>


			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

        	<cfset xmlRXSSCSCElement =  XmlSearch(myxmldocResultDoc,"//RXSSCSC") />
			<cfif ArrayLen(xmlRXSSCSCElement) GT 0 >

				<!---Search element which is deleted--->
				<cfset questionDelete = XmlSearch(xmlRXSSCSCElement[1], "//Q[ @ID = #INPPID# ]") />
				<cfif  ArrayLen(questionDelete) GT 0>
					<cfset XmlDeleteNodes(myxmldocResultDoc, questionDelete[1]) />
				</cfif>

			</cfif>

			<!--- Re-order all control points - RQ --->
	        <cfset QElements = XmlSearch(myxmldocResultDoc, "//Q") />
	        <cfset i = 0>
	        <cfloop array="#QElements#" index="Q2Item">
	            <cfset i = i + 1>
	            <cfset Q2Item.XmlAttributes["RQ"] ="#i#"/>
	        </cfloop>

			<cfset dataout.COUNTERLIMIT = i />

        	<!--- Clean up XML for DB storage --->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

			<cfif inpTemplateFlag EQ 1>

	           <!--- Write to DB Batch Options --->
	           <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.templatesxml
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
				</cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Write to DB Batch Options --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.batch
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>

			</cfif>

            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.BATCHID = INPBATCHID />
			<cfset dataout.PID = INPPID />
			<cfset dataout.ERRMESSAGE = DebugStr />

            <cfinvoke method="AddHistory" component="session.sire.models.cfc.history">
					<cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
					<cfinvokeargument name="EVENT" value="Delete Question PID = #INPPID#">
			</cfinvoke>

            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS BatchId = #INPBATCHID#">
				<cfinvokeargument name="operator" value="Delete Question PID = #INPPID#">
			</cfinvoke>

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>


	<cffunction name="Add_ControlPoint" access="remote" output="false" hint="Add a CP to the XML by BatchID">
		<cfargument name="inpBatchId" TYPE="string" required="true" hint="The Batch Id to change"/>
		<cfargument name="controlPoint" type="any" required="true" hint="The JSON structure of the Control Point"/>
		<cfargument name="STARTNUMBERQ" TYPE="numeric" required="false" default="1" hint="The current page for supporting only showing a sub group of Control Point (CP) in the flow - 1 based - default to 1 if no paging used">
		<cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
		<cfargument name="inpHFE" TYPE="any" required="no" default="1" hint="Hide flow editor - Flag to enabel or disable flow editor" />
		<cfargument name="inpAFE" type="any" required="false" default="0" hint="Advanced Flow Editor Options"/>


		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.PID = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

        <cfset var isTimeOut = '' />
        <cfset var GetBatchQuestion = '' />
        <cfset var myxmldocResultDoc = '' />
        <cfset var xmlRXSSCSCElement = '' />
        <cfset var questionDelete = '' />
        <cfset var item = '' />
        <cfset var xmlRxss = '' />
        <cfset var rxtValue = '' />
        <cfset var nextQIDCKOfDelete = '' />
        <cfset var nextQuestionID = '' />
        <cfset var nextLink = '' />
        <cfset var previousQuestion = '' />
        <cfset var rXSSElements = '' />
        <cfset var firstMCID = '' />
        <cfset var WriteBatchOptions = '' />
        <cfset var i = '' />
        <cfset var DebugStr = '' />
        <cfset var RXSSCSC = '' />
        <cfset var RetVarGetValueAggregate = ''/>
        <cfset var NextPID = '' />
        <cfset var genEleArr = '' />
        <cfset var generatedElement = '' />
        <cfset var _options = []>
        <cfset var subcriberArray = []>
        <cfset var StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
        <cfset var Counter = 1 />
        <cfset var NewRXSSCSC = '' />
        <cfset var QItem = '' />
        <cfset var Q2Item = '' />
        <cfset var OutToDBXMLBuff = '' />
        <cfset var FinalDoc = ''/>
        <cfset var QElements = ''/>
        <cfset var Qs = '' />
        <cfset var RetVarRenderCP = ''/>
        <cfset var inpXML = '' />
        <cfset var SelectedCP = ''/>
		<cfset var arrQuestion = ''/>
		<cfset var AttributeBuff = ''/>
		<cfset var _conditions = []>
		<cfset var POSITION = 0 />
		<cfset var OPTIONSXML = '' />
		<cfset var OPTIONITEM = '' />
		<cfset var CONDITIONITEM	= '' />
		<cfset var CONDITIONSXML	= '' />
		<cfset var APIXML = ''/>
		<cfset var CDFXML = '' />
		<cfset var OPTINXML = '' />
		<cfset var RTEXTITEM	= '' />
		<cfset var RTEXTXML	= '' />
		<cfset var TRNITEM	= '' />
		<cfset var TRNXML	= '' />
		<cfset var INTERVALXML = '' />
		<cfset var ESOPTIONITEM	= '' />
		<cfset var ESOPTIONSXML	= '' />
		<cfset var ANSTEMPXML=''>
		<cfset var LOOPLIMITSXML=''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = ''/>
		<cfset var debugInpXML = '' />

		<cftry>

 			<cfset arguments.controlPoint = DeSerializeJSON(arguments.controlPoint) />


 			<cfif !StructKeyExists(arguments.controlPoint, "LMAX") >
	 			<cfset arguments.controlPoint.LMAX = "0" />
 			</cfif>

 			<cfif !StructKeyExists(arguments.controlPoint, "LDIR") >
	 			<cfset arguments.controlPoint.LDIR = "-1" />
 			</cfif>

 			<cfif !StructKeyExists(arguments.controlPoint, "LMSG") >
	 			<cfset arguments.controlPoint.LMSG = "" />
 			</cfif>

			<cfif !StructKeyExists(arguments.controlPoint, "RTF") >
	 			<cfset arguments.controlPoint.RTF = "0" />
 			</cfif>

			<cfif !StructKeyExists(arguments.controlPoint, "RTEXT") >
				<cfset arguments.controlPoint.RTEXT = ArrayNew() />
			</cfif>

 			<!---
			<!--- All top level CPs (or Q elememnts) will have some form of default value for all of these XML --->
			<!--- Not all CP types use all values - leave it up to the display to allow users to edit the important ones --->
			<cfset arguments.controlPoint = 	{
										AF = "NOFORMAT",
										API_ACT = "",
										API_DATA = "",
										API_DIR = "",
										API_DOM = "somwhere.com",
										API_PORT = "80",
										API_HEAD = "",
										API_FORM = "",
										API_RA = "",
										API_TYPE = "GET",
										BOFNQ = "0",
										CONDITIONS = _conditions,
										COND_XML = "",
										ERRMSGTXT = "",
										GID = controlPoint.GID,
										ID = NextPID,
										IENQID = "0",
										IHOUR = "0",
										IMIN = "0",
										IMRNR = "4",
										INOON = "0",
										INRMO = "END",
										ITYPE = "MINUTES",
										IVALUE = "0",
										LMAX = "0",
										LDIR = "-1",
										LMSG = "",
										OIG = "0",
										OPTION_XML = "",
										OPTIONS = _options,
										REQUIREDANS = "undefined",
										RQ = POSITION,
										SCDF = "",
										TDESC = StrEscUtils.unescapeHTML(inpDesc),
										TEXT = StrEscUtils.unescapeHTML(inpText),
										TYPE = "STATEMENT"
									} />

--->

			<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "#chr(13)##chr(10)#", "\n", "ALL")>
           	<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "#chr(10)#", "\n", "ALL")>


			<cfset POSITION = controlPoint.RQ />

			<cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>


            <cfset DebugStr = DebugStr & "GetBatchQuestion.RecordCount(#GetBatchQuestion.RecordCount#)" />

			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Get next available physical ID - All Physical Ids must be unique --->
            <!--- Loop through and get max Q ID--->
            <cfinvoke method="GetValueAggregate" returnvariable="RetVarGetValueAggregate">
                <cfinvokeargument name="XML" value="#myxmldocResultDoc#">
                <cfinvokeargument name="XPath" value="//Q/@ID">
                <cfinvokeargument name="Aggregate" value="MAX">
            </cfinvoke>

            <cfset NextPID = RetVarGetValueAggregate + 1 />

            <cfset arguments.controlPoint.ID = NextPID />

            <cfset arguments.controlPoint.TEXT = XMLFormat(arguments.controlPoint.TEXT) />
            <cfset arguments.controlPoint.TDESC = XMLFormat(arguments.controlPoint.TDESC) />

            <!--- Build OPTIONS if any --->

			<cfloop array="#controlPoint.OPTIONS#" index="OPTIONITEM">
				<cfset OPTIONITEM.TEXT = XMLFormat(OPTIONITEM.TEXT) />
				<cfset OPTIONITEM.AVAL = XMLFormat(OPTIONITEM.AVAL) />
				<cfset OPTIONITEM.AVALREG = XMLFormat(OPTIONITEM.AVALREG) />
				<cfset OPTIONITEM.AVALRESP = XMLFormat(OPTIONITEM.AVALRESP) />
				<cfset OPTIONITEM.AVALREDIR = XMLFormat(OPTIONITEM.AVALREDIR) />
				<cfset OPTIONITEM.AVALNEXT = XMLFormat(OPTIONITEM.AVALNEXT) />

				<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				<!--- default is plain text storage --->
				<cfset OPTIONITEM.T64 = 0 />

				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
				<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", OPTIONITEM.TEXT ) ) />

				<!--- Look for character higher than 255 ASCII --->

				<cfif UniSearchMatcher.Find() >

					<!--- UnicodeCP Save_cp Formatting processing --->
					<cfset OPTIONITEM.T64 = 1 />

				</cfif>

				<!--- now check optional response AVALRESP --->
				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
				<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", OPTIONITEM.AVALRESP ) ) />

				<!--- Look for character higher than 255 ASCII --->

				<cfif UniSearchMatcher.Find() >

					<!--- UnicodeCP Save_cp Formatting processing --->
					<cfset OPTIONITEM.T64 = 1 />

				</cfif>

				<cfif OPTIONITEM.T64 EQ 1>
					<!--- IF any of the items are unicode - format all text --->
					<cfset OPTIONITEM.TEXT = toBase64(OPTIONITEM.TEXT) />
					<cfset OPTIONITEM.AVALRESP = toBase64(OPTIONITEM.AVALRESP) />
				</cfif>

				<!--- AI Training Text for each option --->
			     <cfif !StructKeyExists(OPTIONITEM, "TRN") >
			    	<cfset OPTIONITEM.TRN = ArrayNew() />
			    </cfif>

			    <cfset TRNXML = "" />
			    <cfloop array="#OPTIONITEM.TRN#" index="TRNITEM">
			    	   <cfset TRNITEM.TEXT = XMLFormat(TRNITEM.TEXT) />

			    	   <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			    	    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

			    	    <!--- default is plain text storage --->
			    	    <cfset TRNITEM.T64 = 0 />

			    	    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
			    	    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TRNITEM.TEXT ) ) />

			    	    <!--- Look for character higher than 255 ASCII --->

			    	    <cfif UniSearchMatcher.Find() >

			    		    <!--- UnicodeCP Save_cp Formatting processing --->
			    		    <cfset TRNITEM.T64 = 1 />

			    	    </cfif>

			    	    <cfif TRNITEM.T64 EQ 1>
			    		    <!--- IF any of the items are unicode - format all text --->
			    		    <cfset TRNITEM.TEXT = toBase64(TRNITEM.TEXT) />
			    	    </cfif>

			    	  <cfset TRNXML = TRNXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<TRN T64='#TRNITEM.T64#'  TEXT='#TRNITEM.TEXT#' TID='#TRNITEM.TID#'>0</TRN>" />

			    </cfloop>

			    <cfset OPTIONSXML = OPTIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<OPTION T64='#OPTIONITEM.T64#'  AVAL='#OPTIONITEM.AVAL#' ID='#OPTIONITEM.ID#' TEXT='#OPTIONITEM.TEXT#' AVALREG='#OPTIONITEM.AVALREG#' AVALRESP='#OPTIONITEM.AVALRESP#' AVALREDIR='#OPTIONITEM.AVALREDIR#' AVALNEXT='#OPTIONITEM.AVALNEXT#'>#TRNXML#</OPTION>" />

			</cfloop>


			<cfloop array="#controlPoint.RTEXT#" index="RTEXTITEM">
				   <cfset RTEXTITEM.TEXT = XMLFormat(RTEXTITEM.TEXT) />


				   <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				    <!--- default is plain text storage --->
				    <cfset RTEXTITEM.T64 = 0 />

				    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
				    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", RTEXTITEM.TEXT ) ) />

				    <!--- Look for character higher than 255 ASCII --->

				    <cfif UniSearchMatcher.Find() >

					    <!--- UnicodeCP Save_cp Formatting processing --->
					    <cfset RTEXTITEM.T64 = 1 />

				    </cfif>


				    <cfif RTEXTITEM.T64 EQ 1>
					    <!--- IF any of the items are unicode - format all text --->
					    <cfset RTEXTITEM.TEXT = toBase64(RTEXTITEM.TEXT) />
				    </cfif>

				  <cfset RTEXTXML = RTEXTXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<RTEXT T64='#RTEXTITEM.T64#'  TEXT='#RTEXTITEM.TEXT#' TID='#RTEXTITEM.TID#'>0</RTEXT>" />

			</cfloop>


            <!--- Build Expert System ESOPTIONS if any --->
            <cfloop array="#controlPoint.ESOPTIONS#" index="ESOPTIONITEM">
	            <cfset ESOPTIONITEM.ESTEXT = XMLFormat(ESOPTIONITEM.ESTEXT) />
	            <cfset ESOPTIONITEM.ESID = XMLFormat(ESOPTIONITEM.ESID) />
	            <cfset ESOPTIONITEM.ESREG = XMLFormat(ESOPTIONITEM.ESREG) />

			    <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", ESOPTIONITEM.ESTEXT ) ) />

	            <!--- Look for character higher than 255 ASCII --->

	          	<cfif UniSearchMatcher.Find() >

	            	<cfset ESOPTIONITEM.ESTEXT = toBase64(ESOPTIONITEM.ESTEXT) />

	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset ESOPTIONITEM.T64 = 1 />

	            <cfelse>
	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset ESOPTIONITEM.T64 = 0 />
	            </cfif>


			    <cfset ESOPTIONSXML = ESOPTIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<ESOPTION T64='#ESOPTIONITEM.T64#' ESID='#ESOPTIONITEM.ESID#' ESTEXT='#ESOPTIONITEM.ESTEXT#' ESREG='#ESOPTIONITEM.ESREG#'>0</ESOPTION>" />

			</cfloop>


            <!--- Build CONDITIONS if any --->
            <cfloop array="#controlPoint.CONDITIONS#" index="CONDITIONITEM">
	            <cfset CONDITIONITEM.DESC = XMLFormat(CONDITIONITEM.DESC) />
			    <cfset CONDITIONSXML = CONDITIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<COND BOAV='#CONDITIONITEM.BOAV#' BOC='#CONDITIONITEM.BOC#' BOCDV='#CONDITIONITEM.BOCDV#' BOQ='#CONDITIONITEM.BOQ#' BOTNQ='#CONDITIONITEM.BOTNQ#' BOV='#CONDITIONITEM.BOV#' CID='#CONDITIONITEM.CID#' TYPE='#CONDITIONITEM.TYPE#' DESC='#CONDITIONITEM.DESC#'></COND>" />
			</cfloop>

            <!--- Default is empty string - smaller kinder XML --->
            <cfif arguments.controlPoint.TYPE EQ "API">
	            <!--- Build API Options if any --->

			  <!--- Legacy fix --->
			  <cfif !StructKeyExists(arguments.controlPoint, "API_HEAD") >
				  <cfset arguments.controlPoint.API_HEAD = "" />
			  </cfif>

			  <cfif !StructKeyExists(arguments.controlPoint, "API_FORM") >
				  <cfset arguments.controlPoint.API_FORM = "" />
			  </cfif>

                <cfset arguments.controlPoint.API_ACT = XMLFormat(arguments.controlPoint.API_ACT) />
	            <cfset arguments.controlPoint.API_DATA = XMLFormat(arguments.controlPoint.API_DATA) />
	            <cfset arguments.controlPoint.API_DIR = XMLFormat(arguments.controlPoint.API_DIR) />
	            <cfset arguments.controlPoint.API_DOM = XMLFormat(arguments.controlPoint.API_DOM) />
	            <cfset arguments.controlPoint.API_PORT = XMLFormat(arguments.controlPoint.API_PORT) />
	            <cfset arguments.controlPoint.API_HEAD = XMLFormat(arguments.controlPoint.API_HEAD) />
	            <cfset arguments.controlPoint.API_FORM = XMLFormat(arguments.controlPoint.API_FORM) />
	            <cfset arguments.controlPoint.API_RA = XMLFormat(arguments.controlPoint.API_RA) />
	            <cfset arguments.controlPoint.API_TYPE = XMLFormat(arguments.controlPoint.API_TYPE) />
			    <cfset APIXML = "API_ACT='#controlPoint.API_ACT#' API_DATA='#controlPoint.API_DATA#' API_DIR='#controlPoint.API_DIR#' API_DOM='#controlPoint.API_DOM#' API_PORT='#controlPoint.API_PORT#' API_HEAD='#controlPoint.API_HEAD#' API_FORM='#controlPoint.API_FORM#' API_RA='#controlPoint.API_RA#' API_TYPE='#controlPoint.API_TYPE#'" />

            </cfif>

            <!--- Default is empty string - smaller kinder XML --->
            <cfif arguments.controlPoint.TYPE EQ "CDF">
	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.SCDF = XMLFormat(arguments.controlPoint.SCDF) />
			    <cfset CDFXML = "SCDF='#controlPoint.SCDF#'" />
            </cfif>

            <!--- Default is empty string - smaller kinder XML --->
            <cfif arguments.controlPoint.TYPE EQ "OPTIN">
	            <!--- Build API Options if any --->
	            <cfset arguments.controlPoint.OIG = XMLFormat(arguments.controlPoint.OIG) />
			    <cfset OPTINXML = "OIG='#controlPoint.OIG#'" />
		    </cfif>

            <!--- Default is empty string - smaller kinder XML --->
            <cfif ListContains('ONESELECTION,SHORTANSWER,INTERVAL', arguments.controlPoint.TYPE, ',') GT 0>
	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.ITYPE = XMLFormat(arguments.controlPoint.ITYPE) />
                <cfset arguments.controlPoint.IVALUE = XMLFormat(arguments.controlPoint.IVALUE) />
                <cfset arguments.controlPoint.IHOUR = XMLFormat(arguments.controlPoint.IHOUR) />
                <cfset arguments.controlPoint.IMIN = XMLFormat(arguments.controlPoint.IMIN) />
                <cfset arguments.controlPoint.INOON = XMLFormat(arguments.controlPoint.INOON) />
                <cfset arguments.controlPoint.IENQID = XMLFormat(arguments.controlPoint.IENQID) />
                <cfset arguments.controlPoint.IMRNR = XMLFormat(arguments.controlPoint.IMRNR) />
                <cfset arguments.controlPoint.INRMO = XMLFormat(arguments.controlPoint.INRMO) />
                <cfset INTERVALXML = "ITYPE='#controlPoint.ITYPE#' IVALUE='#controlPoint.IVALUE#' IHOUR='#controlPoint.IHOUR#' IMIN='#controlPoint.IMIN#' INOON='#controlPoint.INOON#' IENQID='#controlPoint.IENQID#' IMRNR='#controlPoint.IMRNR#' INRMO='#controlPoint.INRMO#'" />
		    </cfif>

			<cfif ListContains('ONESELECTION,SHORTANSWER', arguments.controlPoint.TYPE, ',') GT 0>
				<cfset ANSTEMPXML = "ANSTEMP= '#controlPoint.ANSTEMP#' "/>
			</cfif>

            <!--- Build Loop Limit Options if any --->
            <cfset arguments.controlPoint.LMAX = XMLFormat(arguments.controlPoint.LMAX) />
            <cfset arguments.controlPoint.LDIR = XMLFormat(arguments.controlPoint.LDIR) />
            <cfset arguments.controlPoint.LMSG = XMLFormat(arguments.controlPoint.LMSG) />
            <cfset LOOPLIMITSXML = "LMAX='#controlPoint.LMAX#' LDIR='#controlPoint.LDIR#' LMSG='#controlPoint.LMSG#'" />

			<cfif arguments.controlPoint.T64 EQ 0 >
				<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
				<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

				<!--- Look for character higher than 255 ASCII --->

				<cfif UniSearchMatcher.Find() >

					<cfset arguments.controlPoint.TEXT = toBase64(arguments.controlPoint.TEXT) />

					<!--- UnicodeCP Save_cp Formatting processing --->
					<cfset arguments.controlPoint.T64 = 1 />

				<cfelse>
					<!--- UnicodeCP Save_cp Formatting processing --->
					<cfset arguments.controlPoint.T64 = 0 />
				</cfif>
			</cfif>

            <!--- Build XML for a statement --->
            <!--- Must be valid XML --->
            <!--- RQ is not defined here since it is absolutely positioned in the XML - RQ is generated after XML is generated --->
            <cfset inpXML = "#chr(13)##chr(10)##chr(9)#<Q ID='#controlPoint.ID#' TYPE='#controlPoint.TYPE#' T64='#controlPoint.T64#' TEXT='#arguments.controlPoint.TEXT#' RTF='#arguments.controlPoint.RTF#' TDESC='#controlPoint.TDESC#' SWT='#controlPoint.SWT#' GID='#controlPoint.GID#' MDF='0' AF='#controlPoint.AF#' #ANSTEMPXML# #INTERVALXML# #LOOPLIMITSXML# REQANS='undefined' errMsgTxt='undefined' BOFNQ='#controlPoint.BOFNQ#' #APIXML# #CDFXML# #OPTINXML#>#RTEXTXML##OPTIONSXML##CONDITIONSXML##ESOPTIONSXML# #Chr(13)##Chr(10)##chr(9)#</Q>" />

            <!--- Undo for Rendering response - UnicodeCP ReadCP Formatting --->
			<cfif arguments.controlPoint.T64 EQ 1>
				<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
				<cfset arguments.controlPoint.TEXT = ToString( ToBinary( arguments.controlPoint.TEXT ), "UTF-8" ) />
			</cfif>

			<cfset Counter = 1 />

			<cfset NewRXSSCSC = "" />
			<cfset NewRXSSCSC &= "<RXSSCSC>#Chr(13)##Chr(10)#" />

			<!--- Insert First if first count --->
			<cfif POSITION LTE Counter>
				<cfset NewRXSSCSC &= "#inpXML##Chr(13)##Chr(10)#"/>
			</cfif>

			<cfset Qs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC/Q") />

			<cfloop array="#Qs#" index="QItem">

				<cfset Counter = Counter + 1 />

			    <!--- Clean up XML for DB storage --->
				<cfset OutToDBXMLBuff = ToString(QItem, "UTF-8") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
				<cfset NewRXSSCSC &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/>

				<cfif POSITION EQ Counter>
					<cfset NewRXSSCSC &= "#inpXML##Chr(13)##Chr(10)#"/>
				</cfif>

			</cfloop>

			<!--- Insert last if beyond current counter --->
			<cfif POSITION GT Counter>
				<cfset NewRXSSCSC &= "#inpXML##Chr(13)##Chr(10)#"/>
			</cfif>

			<cfset NewRXSSCSC &= "</RXSSCSC>"/>


			 <cfset dataout.NewRXSSCSC = NewRXSSCSC />

			<!--- Parse new XML --->
			<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #NewRXSSCSC# & "</XMLControlStringDoc>") />

			 <!--- make sure to update all RQ POSITIONs --->
		    <cfset FinalDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

			<!--- Order (or Re-Order) all questions by RQ - starting point is at 1 --->
		    <cfif ArrayLen(FinalDoc) GT 0>

		        <cfset QElements = XmlSearch(FinalDoc[1], "//Q") />
		        <cfset i = 0>
		        <cfloop array="#QElements#" index="Q2Item">
		            <cfset i = i + 1>
		            <cfset Q2Item.XmlAttributes["RQ"] ="#i#"/>
		        </cfloop>
		    </cfif>

		    <!--- Read the new postion (RQ) from the PID - reset POSITION for render--->
			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @ID = #NextPID#]") />

            <cfif ArrayLen(SelectedCP) GT 0>

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@RQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
	                <cfset POSITION = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfset arguments.controlPoint.RQ =  POSITION />
                </cfif>

            </cfif>

        	<!--- Clean up XML for DB storage --->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

            <cfset dataout.OutToDBXMLBuff = OutToDBXMLBuff />

			<cfif inpTemplateFlag EQ 1>

	           <!--- Write to DB Batch Options --->
	           <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.templatesxml
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
				</cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Write to DB Batch Options --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.batch
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>

			</cfif>

            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.BATCHID = INPBATCHID />
			<cfset dataout.PID = NextPID />
			<cfset dataout.ERRMESSAGE = DebugStr />

			<!--- Store all changes in history table - this allows user to undo as well as protects us from someone sending bad message and then changing it and claiming wrong message sent --->
            <cfinvoke method="AddHistory" component="session.sire.models.cfc.history">
					<cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
					<cfinvokeargument name="EVENT" value="Add Control Point PID = #NextPID#">
			</cfinvoke>

            <!--- What has this user been up to - good metrics on who is using Sire for what - also good for security tracking  --->
            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS BatchId = #INPBATCHID#">
				<cfinvokeargument name="operator" value="Add Control Point PID = #NextPID#">
			</cfinvoke>

			<cfset subcriberArray = []>
			<cfset _options = []>
			<cfset _conditions = []>

			<cfinvoke method="RenderCP" returnvariable="RetVarRenderCP">
				<cfinvokeargument name="controlPoint" value="#controlPoint#">
				<cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
				<cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#">
				<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
				<cfinvokeargument name="inpHFE" value="#inpHFE#">
				<cfinvokeargument name="inpAFE" value="#inpAFE#">
			</cfinvoke>

			<cfset dataout.HTML = RetVarRenderCP.HTML />

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<!--- Render via cfc for primarily for being able to add on the fly without having to refresh and reparse data --->
	<cffunction name="RenderCP" access="remote" output="true" hint="Render a Control Point in HTML ">
		<cfargument name="controlPoint" type="any" required="true" hint="The structure of the Control Point"/>
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
		<cfargument name="inpCPList" type="any" required="false" hint="Read all the CP items once so they can be used for selection boxes without having to re-query for each control point"/>
		<cfargument name="inpPreview" type="any" required="false" default="0" hint="Just preview - no edit options"/>
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
		<cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
		<cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />
		<cfargument name="inpHFE" TYPE="any" required="no" default="1" hint="Hide flow editor - Flag to enable or disable flow editor" />
		<cfargument name="inpAFE" TYPE="any" required="no" default="0" hint="Advanced flow editor - Flag to enable or disable flow editor" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.PID = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cfset var DebugStr = '' />
		<cfset var CP_HTML = "" />
		<cfset var StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
		<cfset var subcriberArray = ''/>
		<cfset var subcriberList = ''/>
		<cfset var _OPTION = ''/>
		<cfset var listStyleIndex = ''/>
		<cfset var item = ''/>
		<cfset var groupList = ''/>
		<cfset var TypeListValue = "" />
		<cfset var _CONDITIONS	= '' />
		<cfset var CONDITIONITEM	= '' />
		<cfset var RetVarGetGroupInfo	= '' />
		<cfset var RetVarReadCPDataById	= '' />
		<cfset var CONDITIONSXML	= '' />
		<cfset var NoConditionsSpecified = '1' />
		<cfset var BuilderOptionTypeVal = "" />
		<cfset var ConvertedAlphaOptionList = "" />
		<cfset var iBOV = '' />
		<cfset var SendAsUnicode = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var UniSearchPattern = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var shortcode = '' />
		<cfset var NewLinesSearchPattern = '' />
		<cfset var NewLinesSearchMatcher = '' />
		<cfset var RetVarReadXMLQuestions	= '' />
		<cfset var RetVarWFHH = ''/>
		<cfset var RetVarDrip = '' />

		<cftry>

			<cfif isJson(arguments.controlPoint)>
		        <cfset arguments.controlPoint = deserializeJSON(arguments.controlPoint) />
		    </cfif>

			<cfif NOT isStruct(arguments.controlPoint)>
		        <cfthrow MESSAGE="controlPoint argument must be structure or a json string" />
		    </cfif>

			<!--- What if there are no CPs yet?--->
			<cfif isJson(arguments.inpCPList) AND TRIM(arguments.inpCPList) NEQ "">
		        <cfset arguments.inpCPList = deserializeJSON(arguments.inpCPList) />
		    </cfif>

		    <cfif NOT isStruct(arguments.inpCPList)>

		    	<!--- Read all the CP items once so they can be used for selection boxes without having to re-query for each control point --->
				<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestions" returnvariable="RetVarReadXMLQuestions">
	                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
					<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	            </cfinvoke>

		        <cfset arguments.inpCPList = RetVarReadXMLQuestions/>
		    </cfif>

			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortcode"></cfinvoke>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

			<cfswitch expression="#controlPoint.TYPE#" >

				<cfcase value="STATEMENT">
					<cfset TypeListValue = "Simple Message" />
				</cfcase>

				<cfcase value="SHORTANSWER">
					<cfset TypeListValue = "Question & Wait for Answer" />
				</cfcase>

				<cfcase value="ONESELECTION">
					<cfset TypeListValue = "Question & Wait for Answer" />
				</cfcase>

				<cfcase value="TRAILER">
					<cfset TypeListValue = "Final Message - This will end the conversation" />
				</cfcase>

				<cfcase value="BRANCH">
					<cfset TypeListValue = "Rules Engine" />
				</cfcase>

				<cfcase value="INTERVAL">
					<cfset TypeListValue = "Wait Interval" />
				</cfcase>

				<cfcase value="API">
					<cfset TypeListValue = "API Call" />
				</cfcase>

				<cfcase value="OPTIN">
					<cfset TypeListValue = "Capture Opt In" />
				</cfcase>

				<cfcase value="CDF">
					<cfset TypeListValue = "Capture Custom Data Field" />
				</cfcase>

				<cfcase value="RESET">
					<cfset TypeListValue = "Drip Marketing Reset" />
				</cfcase>

			</cfswitch>


			<cfset var unicodeInfo = 'When unicode text has been detected in your message, it will count that as 2 characters instead of 1. <a href="https://www.siremobile.com/blog/support/unicode-text-mean-detected-message/" _target-"blank"> Learn More. </a>'/>



			<cfsavecontent variable="CP_HTML">
				<cfoutput>

					<cfset BuilderOptionTypeVal = "BuilderOption" />


					<!--- Stop hiding these - only hide them in SWT flows --->
					<cfset BuilderOptionTypeVal = "" />

<!---
					<!--- Show Interval for easy to edit settings? INTERVAL --->
					<cfif ListFindNoCase("BRANCH,API,RESET", controlPoint.TYPE) GT 0>
						<cfset BuilderOptionTypeVal = "BuilderOption" />
					<cfelse>
						<cfset BuilderOptionTypeVal = "" />
					</cfif>
--->

					<!--- Preserve possible newlines in display but format all other tags --->
					<cfset arguments.controlPoint.TDESC = ReplaceNoCase(arguments.controlPoint.TDESC, "newlinex", "<br>", "ALL")>
					<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "\n", "<br>", "ALL")>
					<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "#chr(10)#", "<br>", "ALL")>
					<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
<!--- 					<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "\n", "<br>", "ALL")> --->

					<div class="form-group clearfix control-point cpobj-drop-zone #BuilderOptionTypeVal#" id="cp#controlPoint.RQ#" data-control-point-physical-id="#controlPoint.ID#" data-control-rq-id="#controlPoint.RQ#" data-control-point-type="#controlPoint.TYPE#" >

						<div class="CPDisplaySection">

							<input type="hidden" id="ACTION" value="EDIT" />
							<input type="hidden" id="RQ" value="#controlPoint.RQ#" />
							<input type="hidden" id="ID" value="#controlPoint.ID#" />
							<input type="hidden" id="TYPE" value="#controlPoint.TYPE#" />

				            <cfif inpTemplateFlag NEQ 0>


							  <cfif inpPreview EQ 0>
								<div id="SWTSection" class="row" style="margin-bottom: 1em;">

									<div class="col-sm-12">
								        <label for="SWT" class="bc-title mbTight" id="SWT_Label">Show this section on Simplified Walk Through:</label>

							        	<select id="SWT" class="Select2" size="1" data-width="100%">
										    <option value="0" <cfif controlPoint.SWT EQ 0>selected </cfif>>No</option>
										    <option value="1" <cfif controlPoint.SWT EQ 1>selected </cfif>>Yes</option>
										</select>
							    		</div>

								</div>

							</cfif>

					    	<cfelse>
					    		<input type="hidden" id="SWT" value="#controlPoint.SWT#" />
							</cfif>


<!---

							<!--- Builder option to add more control points --->
							<cfif inpPreview EQ 0 and inpSimpleViewFlag NEQ 0>
			    				<div class="col-sm-12 cpobj-drop-zone" style="text-align: center;">

				    				<button type="button" class="BuilderOption builder-btn btn btn-link control-point-btn-add" style="margin-bottom: .8em; opacity: 0.6; filter: alpha(opacity=60);"
									 	data-control-point-id="#controlPoint.RQ#"
										data-control-point-type="#controlPoint.TYPE#"
									>&nbsp;</button>

			    				</div>
							</cfif>
--->



							<div class="">
								<div class="control-point-border clearfix">

									<div class="clearfix">


										<cfif inpHFE EQ 0>
											<div style="float: right; display: inline; margin-left: .6em; margin-right: 1em; height: 32px; line-height: 32px;">

												<cfif inpAFE EQ 1>
													<span class="hover-edit-options">
														<span class="copy-cp cp-edit-icon"><i class="far fa-copy"></i></span>
														<!--- <span class="paste-cp cp-edit-icon"><i class="fas fa-paste"></i></span> --->
													</span>
												</cfif>

												<!--- Keep the display clean - hover delete option --->
												<span class="hover-delete-cp BuilderOption glyphicon glyphicon-remove-sign cp-edit-icon control-point-btn-delete" aria-hidden="true" style=""
													data-control-point-id="#controlPoint.RQ#"
												 	data-control-point-physical-id="#controlPoint.ID#"
													data-control-point-type="#controlPoint.TYPE#">
												</span>
											</div>
										</cfif>

										<div class="col-xs-8 handle">
											<cfif inpSimpleViewFlag EQ 1 AND inpPreview EQ 0>
												<label class="control-point-label"><span class="CPNumberOnPage">#controlPoint.RQ#</span> <span class="">#TypeListValue#</span></label>
											<cfelse>
												<label class="control-point-label">&nbsp; <span class="CPNumberOnPage">&nbsp;</span>&nbsp; <span class="">&nbsp;</span></label>
				    						</cfif>
										</div>

									</div>
<!---
									<cfif inpPreview EQ 0>

										<div class="col-xs-12 col-sm-12 cp-obj-header handle">

											<div style="float: right; display: inline; margin-left: .6em; height: 32px; line-height: 32px;">

												<!--- background-color: ##578ca5; padding: 0px 2px 0px 2px; margin-left: 1em; margin-top: 6px;; border-width: 3px; font-size: 12px; border-color:##fafafa; height: 21px; opacity: 0.9; filter: alpha(opacity=90);" --->

<!---
													<span class="glyphicon glyphicon-edit cp-edit-icon control-point-btn-edit" style=""
														data-control-point-id="#controlPoint.RQ#"
														data-control-point-type="#controlPoint.TYPE#"
														data-control-point-af="#controlPoint.AF#"
														data-control-point-oig="#controlPoint.OIG#"
														data-control-point-scdf="#controlPoint.SCDF#">
													</span>
--->

													<span class="BuilderOption glyphicon glyphicon-remove-sign cp-edit-icon control-point-btn-delete" aria-hidden="true" style=""
														data-control-point-id="#controlPoint.RQ#"
													 	data-control-point-physical-id="#controlPoint.ID#"
														data-control-point-type="#controlPoint.TYPE#">
													</span>

											</div>

											<cfif inpSimpleViewFlag EQ 1 AND inpPreview EQ 0>

													<label class="control-point-label">## <span class="CPNumberOnPage">#controlPoint.RQ#</span>. <span class="">#TypeListValue#</span></label>
											<cfelse>

													<label class="control-point-label">&nbsp; <span class="CPNumberOnPage">&nbsp;</span>&nbsp; <span class="">&nbsp;</span></label>
				    						</cfif>

										</div>

										<div class="clearfix"></div>
										<hr class="hr0">

									</cfif>
--->

									<div class="col-sm-12 clearfix">
										<div class="control-point-body">

											<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
											<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
											<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


            								<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
					                        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

								      		<!--- Look for character higher than 255 ASCII --->
					                        <cfif UniSearchMatcher.Find() >

					                            <cfset SendAsUnicode = 1 />
					                            <cfset MaxPerMessage = 66 />
					                            <cfset UpperCharacterLimit = 70 />

					                        <cfelse>

						                        <cfset SendAsUnicode = 0 />
						                        <cfset MaxPerMessage = 153 />
						                        <cfset UpperCharacterLimit = 160 />

					                        </cfif>

											<cfset arguments.controlPoint.length = len(ReReplaceNoCase(arguments.controlPoint.TEXT, '<[^>]*>', '', 'all'))>
											<!--- count newlines as 2 character--->
											<cfset NewLinesSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "(\r\n|\r|\n)" ) ) />
					                        <cfset NewLinesSearchMatcher = REMatch(NewLinesSearchPattern, arguments.controlPoint.TEXT) />
							 				<cfset arguments.controlPoint.length +=Arraylen(NewLinesSearchMatcher)>

											<!--- Preserve possible newlines in display but format all other tags --->
<!---
											<cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "<br>", "newlinex", "ALL")>
											<cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "\n", "newlinex", "ALL")>
											<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, chr(10), "newlinex", "ALL")>


											<cfset arguments.controlPoint.TEXT = HTMLEditFormat(arguments.controlPoint.TEXT) />

											<!--- Allow newline in text messages - reformat for display --->
											<cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "newlinex", "<br>", "ALL")>
											<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "\n", "<br>", "ALL")>
											<!--- Display ' but store as &apos; in the XML --->
											<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "&apos;", "'", "ALL")>
--->

											<!--- Syntax highlight CDFs --->
											<!--- <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, '{%', '<code class="mceNonEditable">{%', 'all')>
											<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, '%}', '%}</code>', 'all')> --->

						    				<cfswitch expression="#controlPoint.TYPE#">

						    					<cfcase value="ONESELECTION,SHORTANSWER">

		                                        	<!--- Get Adv UI Here --->
										            <cfset var RetVarQuestion = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.question" method="GetAdvEditUI" returnvariable="RetVarQuestion">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
													 <cfinvokeargument name="inpAFE" value="#inpAFE#">
												  </cfinvoke>
										            #RetVarQuestion#

						    					</cfcase>

						    					<cfcase value="OPTIN">

							    					<!--- Get Adv UI Here --->
										            <cfset var RetVarOptIn = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.optin" method="GetAdvEditUI" returnvariable="RetVarOptIn">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
										            </cfinvoke>
										            #RetVarOptIn#

					    						</cfcase>

					    						<cfcase value="CDF">

					    							<!--- Get Adv UI Here --->
										            <cfset var RetVarCDF = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.cdf" method="GetAdvEditUI" returnvariable="RetVarCDF">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
										            </cfinvoke>
										            #RetVarCDF#

					    						</cfcase>

					    						<cfcase value="RESET">
												<!--- Get Adv UI Here --->
												  <cfset var RetVarStatement = '' />
												  <cfinvoke component="session.sire.models.cfc.control-point.drip-reset" method="GetAdvEditUI" returnvariable="RetVarDrip">
													 <cfinvokeargument name="controlPoint" value="#controlPoint#">
												  </cfinvoke>

												  #RetVarDrip#
					    						</cfcase>

					    						<cfcase value="INTERVAL">

					    							<!--- Get Adv UI Here --->
										            <cfset var RetVarInterval = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.interval" method="GetAdvEditUI" returnvariable="RetVarInterval">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
										            </cfinvoke>
										            #RetVarInterval#

					    						</cfcase>

					    						<cfcase value="API">

													<!--- Get Adv UI Here --->
										            <cfset var RetVarAPI = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.api" method="GetAdvEditUI" returnvariable="RetVarAPI">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
										            </cfinvoke>
										            #RetVarAPI#

					    						</cfcase>

												<cfcase value="ES">
													<div>Expert System ("Chat Bot")</div>
					    						</cfcase>

											<cfcase value="STATEMENT">

					    							<!--- Get Adv UI Here --->
										            <cfset var RetVarStatement = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.statement" method="GetAdvEditUI" returnvariable="RetVarStatement">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
													 <cfinvokeargument name="inpAFE" value="#inpAFE#">
										            </cfinvoke>



										            #RetVarStatement#

					    						</cfcase>

<!---
												<cfcase value="SHORTANSWER">

					    							<div class="control-point-text Preview-Bubble Preview-Me">#controlPoint.TEXT#</div>
					    							<br>
					    							<cfif controlPoint.length GT UpperCharacterLimit>
								    						Character Count {#controlPoint.length#}
								    						<span class="text-danger">#ceiling(controlPoint.length / MaxPerMessage)# Increments</span><br>
								    						<a class="see-unicode-mess <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
								    					<cfelse>
								    						<span>#UpperCharacterLimit - controlPoint.length# characters available</span>
								    						<a class="see-unicode-mess <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
								    					</cfif>

					    						</cfcase>
--->

											<cfcase value="TRAILER">

													<!--- Get Adv UI Here --->
										            <cfset var RetVarTrailer = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.trailer" method="GetAdvEditUI" returnvariable="RetVarTrailer">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
													 <cfinvokeargument name="inpAFE" value="#inpAFE#">
												  </cfinvoke>
										            #RetVarTrailer#

					    						</cfcase>

											<cfcase value="BATCHCP">

													<!--- Get Adv UI Here --->
										            <cfset var RetVarTrailer = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.batch-cp" method="GetAdvEditUI" returnvariable="RetVarTrailer">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										            </cfinvoke>
										            #RetVarTrailer#

					    						</cfcase>

											<cfcase value="WFHH">

												<!--- Get Adv UI Here --->
												  <cfset var RetVarStatement = '' />
												  <cfinvoke component="session.sire.models.cfc.control-point.wfhh" method="GetAdvEditUI" returnvariable="RetVarWFHH">
													 <cfinvokeargument name="controlPoint" value="#controlPoint#">
												  </cfinvoke>

												  #RetVarWFHH#

											</cfcase>

					    						<cfcase value="BRANCH">

						    						<!--- Get Adv UI Here --->
										            <cfset var RetVarRulesEngine = '' />
										            <cfinvoke component="session.sire.models.cfc.control-point.rulesengine" method="GetAdvEditUI" returnvariable="RetVarRulesEngine">
										                <cfinvokeargument name="controlPoint" value="#controlPoint#">
										                <cfinvokeargument name="inpCPList" value="#inpCPList#">
										                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
										            </cfinvoke>
										            #RetVarRulesEngine#

					    						</cfcase>

						    					<cfdefaultcase>

						    						<div class="control-point-text Preview-Bubble Preview-Me">#controlPoint.TEXT#</div>
						    					</cfdefaultcase>

						    				</cfswitch>

				    					</div> <!--- End control-point-body --->
				    				</div> <!-- End outer container --->

								</div> <!--- End control-point-border --->
							</div>	<!-- End full width column --->
						</div> <!--- End CP display Section --->
					</div> <!--- End outer Container --->

				</cfoutput>
			</cfsavecontent>

			<cfset dataout.HTML  = CP_HTML />
			<cfset dataout.RXRESULTCODE = 1 />

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.HTML  = SerializeJSON(cfcatch) />

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadCPDataById" access="remote" output="false" hint="Get Control Point data from XMLControlString in specified BatchID. Can either be by physical ID (ID) or Position ID (RQ) controled by inpIDKey. Default is RQ">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
        <cfargument name="inpQID" TYPE="string" required="yes" hint="The question ID to look up - inpIDKey controls which key to use" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpShowOPTIONS" required="no" default="0" hint="Hint include Options XML in response - optional" />
        <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
        <cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />
        <cfargument name="RenderHTML" TYPE="string" required="no" hint="Render for HTML or TextArea" default="1" />

		<cfset var inpRXSSCSCLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var SelectedCP = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var RXSSCSC = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var CPObj = StructNew() />
		<cfset var AttributeBuff = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetBatchQuestion = '' />
		<cfset var DebugStr = '' />
		<cfset var userId = Session.USERID />

		<cfset var arrES	= '' />
		<cfset var ESOPTION_XML	= '' />
		<cfset var ESOPTION_XMLBUFF	= '' />
		<cfset var GetScheduleQuestion	= '' />

		<cfset var arrRTexts = '' />
		<cfset var selectedRText = '' />
		<cfset var RTEXT_XML = '' />
		<cfset var RTextIndex = '' />
		<cfset var RTexts = '' />
		<cfset var RTEXT_XMLBUFF = '' />

		<cfset var arrTRNs = '' />
		<cfset var selectedTRN = '' />
		<cfset var TRN_XML = '' />
		<cfset var TRNIndex = '' />
		<cfset var TRNs = '' />
		<cfset var TRN_XMLBUFF = '' />

		<cfset var dataout = StructNew()/>

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.INPBATCHID = "#INPBATCHID#">
		<cfset dataout.TYPE = "">
		<cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout.STRUCTCOUNT = 0 />
		<cfset dataout.CPSOObj = {
			StartHour_ti = 0,
            EndHour_ti = 0
		} />

		<cftry>

            <!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <!--- Validate inpQID Id--->
			<cfif !isnumeric(inpQID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>


            <cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>
            <cfelse>

				<!--- Secured to Session.User by query --->
				 <cfif arguments.inpUserId GT 0>
	            	<cfset userId = arguments.inpUserId/>
	            </cfif>
				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
	            </cfquery>


	        </cfif>


	        <!--- WTF?!?! This does not belong here !!!!!!!!!!! Schedule is a property of the Batch - not a property of an individual Control Point--->

	        <!--- Read StartHour_ti & EndHour_ti from DB schedule options --->
	        <cfquery name="GetScheduleQuestion" datasource="#Session.DBSourceEBM#">
                SELECT
                    StartHour_ti,
                    EndHour_ti
                FROM
                    simpleobjects.scheduleoptions
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#"> limit 1
	        </cfquery>


            <cfset inpRXSSCSCLocalBuff = GetBatchQuestion.XMLControlString_vch />

            <!--- Watch out for formated unicade --->
            <cfset inpRXSSCSCLocalBuff = Replace(inpRXSSCSCLocalBuff, "&##","&amp;##","ALL") />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSCSCLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset RXSSCSC = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

			<cfset GROUPCOUNT = 0/>

			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />

            <cfif ArrayLen(SelectedCP) GT 0>

                <cfset CPObj = StructNew() />

                <!--- Dont assume all attributes are there - set defaults --->

                <!--- Get text and type of question --->

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@RQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.RQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- ID --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>


				<!--- T64 Flag for is Base64 Encoded Text --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@T64")>
                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.T64 = "0">
                </cfif>

                <!--- Text --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TEXT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <!--- UnicodeCP ReadCP Formatting --->
					<cfif CPObj.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset CPObj.TEXT = ToString( ToBinary( CPObj.TEXT ), "UTF-8" ) />
					</cfif>

                    <!--- Allow newline in text messages - reformat for display --->
					<cfset CPObj.TEXT = Replace(CPObj.TEXT, "\n", "#chr(10)#", "ALL")>

                <cfelse>
                    <cfset CPObj.TEXT = "">
                </cfif>

                <!--- Text Description--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TDESC")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TDESC = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
                    <!--- Allow newline in text desc messages - reformat for display --->
					<cfif RenderHTML EQ 1>
						<cfset CPObj.TDESC = ReplaceNoCase(CPObj.TDESC, "newlinex", "<br>", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "\n", "<br>", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "#chr(10)#", "<br>", "ALL")>
					<cfelse>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "\n", "#chr(10)#", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "<br>", "#chr(10)#", "ALL")>
					</cfif>

                <cfelse>
                    <cfset CPObj.TDESC = "">
                </cfif>

                <!--- Text Guild --->

                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TGUIDE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TGUIDE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
                    <!--- Allow newline in text desc messages - reformat for display --->
					<cfif RenderHTML EQ 1>
						<cfset CPObj.TGUIDE = ReplaceNoCase(CPObj.TGUIDE, "newlinex", "<br>", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "\n", "<br>", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "#chr(10)#", "<br>", "ALL")>
					<cfelse>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "\n", "#chr(10)#", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "<br>", "#chr(10)#", "ALL")>
					</cfif>

                <cfelse>
                    <cfset CPObj.TGUIDE = "">
                </cfif>

                <!--- Walk Through - Flag to enable edit of this control point during siplified walk through--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@SWT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SWT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.SWT = "1">
                </cfif>

				<!--- ANSTEMP--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ANSTEMP")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ANSTEMP = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ANSTEMP = "">
                </cfif>

                <!--- type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.TYPE = "0">
                </cfif>

                <!--- GID --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@GID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.GID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.GID = "0">
                </cfif>

				<!--- ValidateSMSResponse --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@VALIDATESMSRESPONSE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.VALIDATESMSRESPONSE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.VALIDATESMSRESPONSE = "0">
                </cfif>

                <!--- requiredAns --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@REQANS")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.REQANS = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.REQANS = "0">
                </cfif>

                <!--- Answer Format --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@AF")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.AF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.AF = "HIDDEN">
                </cfif>

                <!--- errMsgTxt --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ERRMSGTXT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ERRMSGTXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ERRMSGTXT = "">
                </cfif>

                <!--- OIG - Opt In Group --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@OIG")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.OIG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.OIG = "0">
                </cfif>

                <!---API Type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_TYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_TYPE = "GET">
                </cfif>

                <!---API Domain --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DOM")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DOM = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DOM = "somwhere.com">
                </cfif>

                <!---API Directory --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DIR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DIR = "">
                </cfif>

                <!---API Port --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_PORT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_PORT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_PORT = "80">
                </cfif>

                <!---API Head --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_HEAD")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_HEAD = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_HEAD = "">
                </cfif>

                <!---API Post --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_FORM")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_FORM = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_FORM = "">
                </cfif>

                <!---API Data --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DATA")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DATA = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DATA = "">
                </cfif>

                <!---API Result Action --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_RA")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_RA = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_RA = "">
                </cfif>

                <!---API Additional Content Type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_ACT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_ACT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_ACT = "">
                </cfif>

                <!---Store CDF  --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@SCDF")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SCDF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.SCDF = "">
                </cfif>

                <!--- Interval Data --->

                <!--- ITYPE - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ITYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ITYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ITYPE = "MINUTES">
                </cfif>

                <!--- IVALUE - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IVALUE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IVALUE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IVALUE = "0">
                </cfif>

                <!--- IHOUR - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IHOUR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IHOUR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IHOUR = "0">
                </cfif>

                <!--- IMIN - The value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IMIN")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IMIN = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IMIN = "0">
                </cfif>

                <!--- INOON - The value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@INOON")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.INOON = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.INOON = "0">
                </cfif>

                <!--- IENQID - The RQ value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IENQID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IENQID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IENQID = "0">
                </cfif>

                <!--- IMRNR - MRNR = Max Repeat No Response --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IMRNR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IMRNR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IMRNR = "4">
                </cfif>

                <!--- INRMO - NRM = No Response Max Option END or NEXT--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@INRMO")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.INRMO = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.INRMO = "END">
                </cfif>


                <!--- Looping limits - before security is violated --->
                <!--- LMAX - Loop Max for this CP - use drip clean to remove--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LMAX")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LMAX = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LMAX = "0">
                </cfif>

                <!--- LDIR - Loop Max default path to move on to: 0 is end GT 0 is PID of next CP --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LDIR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LDIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LDIR = "-1">  <!--- Default is to terminate -1, 0 is to move on to next step, and GT 0 is to move to that point in flow --->
                </cfif>

                <!--- Message for user if LDIR is 0 and conversation is terminated - default is blank --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LMSG")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LMSG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LMSG = "">
                </cfif>


                <!--- Branch Logic Data --->

                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@BOFNQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.BOFNQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.BOFNQ = "0">
                </cfif>

				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@OPT_NUM")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.OPT_NUM = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.OPT_NUM = "">
                </cfif>

				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@OPT_TYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.OPT_TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.OPT_TYPE = "">
                </cfif>



                <!--- Get answers --->
                <cfset arrAnswer = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./OPTION") />

                <cfset OPTION_XML = "">

                <cfloop array="#selectedAnswer#" index="ans">

                	<cfif inpShowOPTIONS GT 0>
						<cfset OPTION_XMLBUFF = ToString(ans, "UTF-8")/>
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset OPTION_XMLBUFF = TRIM(OPTION_XMLBUFF) />

                        <cfset OPTION_XML = OPTION_XML & OPTION_XMLBUFF/>
                    </cfif>

                    <cfset answer = StructNew() />

				<!--- Get TRN(s) --->
				<cfset arrTRNs = ArrayNew(1) />
				<cfset selectedTRN = XmlSearch(ans, "./TRN") />

				<cfset TRN_XML = "">

				<cfloop array="#selectedTRN#" index="TRNIndex">

					<cfset TRN_XMLBUFF = ToString(TRNIndex, "UTF-8")/>
					<cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, '"', "'", "ALL") />
					<cfset TRN_XMLBUFF = TRIM(TRN_XMLBUFF) />

					<cfset TRN_XML = TRN_XML & TRN_XMLBUFF/>

					<cfset TRNs = StructNew() />

					<!--- id --->
					<cfset AttributeBuff = XmlSearch(TRNIndex, "@TID")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.TID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
					<cfelse>
						<cfset TRNs.TID = "0">
					</cfif>

					<!--- TYPE - RESPONSE, CDF, ... --->
					<cfset AttributeBuff = XmlSearch(TRNIndex, "@T64")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
					<cfelse>
						<cfset TRNs.T64 = "0">
					</cfif>

					<cfset AttributeBuff = XmlSearch(TRNIndex, "@TEXT")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

						<!--- UnicodeCP ReadCP Formatting --->
						<cfif TRNs.T64 EQ 1>
						    <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						    <cfset TRNs.TEXT = ToString( ToBinary( TRNs.TEXT ), "UTF-8" ) />
						</cfif>

						<!--- Allow newline in text messages - reformat for display --->
						<cfset TRNs.TEXT = Replace(TRNs.TEXT, "\n", "#chr(10)#", "ALL")>

					<cfelse>
						<cfset TRNs.TEXT = "">
					</cfif>

					<cfset ArrayAppend(arrTRNs, TRNs) />

				</cfloop>

				<cfset answer.TRN_XML = TRN_XML>

				<!--- add all conditions to current question --->
				<cfset answer.TRN = arrTRNs />

                    <!--- id --->
					<cfset AttributeBuff = XmlSearch(ans, "@ID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.id = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">
                    </cfif>

                    <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

                    <!--- errMsgTxt --->
					<cfset AttributeBuff = XmlSearch(ans, "@TEXT")>

                     <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.text = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.text = ToString( ToBinary( answer.text ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.text = Replace(answer.text, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.text = "">
	                </cfif>

                    <!--- Assigned Answer Value --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVAL")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.AVAL = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.AVAL = "">
                    </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVALREG")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.AVALREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.AVALREG = "">
                    </cfif>

				<!--- Optional statement response --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALRESP")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALRESP = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- UnicodeCP ReadCP Formatting --->
					<cfif answer.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset answer.AVALRESP = ToString( ToBinary( answer.AVALRESP ), "UTF-8" ) />
					</cfif>

					<!--- Allow newline in text messages - reformat for display --->
					<cfset answer.AVALRESP = Replace(answer.AVALRESP, "\n", "#chr(10)#", "ALL")>

				<cfelse>
				    <cfset answer.AVALRESP = "">
				</cfif>

				<!--- Optional redirect to another flow --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALREDIR")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALREDIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
				    <cfset answer.AVALREDIR = "">
				</cfif>

				<!--- Where to go next CP - default 0 is just move on --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALNEXT")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALNEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
				    <cfset answer.AVALNEXT = "0">
				</cfif>

				<!--- Let Render know this has already been decoded --->
				<cfset answer.T64 = 0/>

                    <cfset ArrayAppend(arrAnswer, answer) />
                </cfloop>
                <cfset CPObj.answers = arrAnswer />
                <cfset CPObj.OPTIONS = arrAnswer />

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.OPTION_XML = OPTION_XML>
                <cfelse>
	                <cfset CPObj.OPTION_XML = "">
                </cfif>


                <!--- Get ESOptions --->
                <cfset arrES = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./ESOPTION") />

                <cfset ESOPTION_XML = "">

                <cfloop array="#selectedAnswer#" index="ans">

                	<cfif inpShowOPTIONS GT 0>
						<cfset ESOPTION_XMLBUFF = ToString(ans, "UTF-8")/>
                        <cfset ESOPTION_XMLBUFF = Replace(ESOPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset ESOPTION_XMLBUFF = Replace(ESOPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset ESOPTION_XMLBUFF = TRIM(ESOPTION_XMLBUFF) />

                        <cfset ESOPTION_XML = ESOPTION_XML & ESOPTION_XMLBUFF/>
                    </cfif>

                    <cfset answer = StructNew() />

                    <!--- id --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.ESID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.ESID = "0">
                    </cfif>

                     <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

	                <!--- Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESTEXT")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.ESTEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.ESTEXT = ToString( ToBinary( answer.ESTEXT ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.ESTEXT = Replace(answer.ESTEXT, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.ESTEXT = "">
	                </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESREG")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.ESREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.ESREG = "">
                    </cfif>

                    <cfset ArrayAppend(arrES, answer) />
                </cfloop>

                <cfset CPObj.ESOPTIONS = arrES />

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.ESOPTION_XML = ESOPTION_XML>
                <cfelse>
	                <cfset CPObj.ESOPTION_XML = "">
                </cfif>


				<!--- Get conditions --->
                <cfset arrConditions = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./COND") />

                <cfset COND_XML = "">

                <cfloop array="#selectedAnswer#" index="conditionsIndex">

					<cfif inpShowOPTIONS GT 0>

                        <cfset COND_XMLBUFF = ToString(conditionsIndex, "UTF-8")/>
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '"', "'", "ALL") />
                        <cfset COND_XMLBUFF = TRIM(COND_XMLBUFF) />

                        <cfset COND_XML = COND_XML & COND_XMLBUFF/>

                    </cfif>

                    <cfset conditions = StructNew() />

                    <!--- id --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@CID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.CID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.CID = "0">
                    </cfif>

                    <!--- Branch Logic Data --->

        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@TYPE")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">
                    </cfif>

                    <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOQ")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">
                    </cfif>

                    <!--- BOC The comparison of the question to value to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOC")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOC = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">
                    </cfif>

                    <!--- BOV - The value of the question to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">
                    </cfif>

                    <!--- BOAV - The value of the question to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOAV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOAV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">
                    </cfif>

                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOCDV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOCDV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">
                    </cfif>

                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOTNQ")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOTNQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">
                    </cfif>

                    <!--- Notes --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@DESC")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.text = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">
                    </cfif>

                    <cfset ArrayAppend(arrConditions, conditions) />

                </cfloop>

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.COND_XML = COND_XML>
                <cfelse>
	                <cfset CPObj.COND_XML = "">
                </cfif>

                <cfset CPObj.COND_XML = COND_XML>

                <!--- add all conditions to current question --->
                <cfset CPObj.CONDITIONS = arrConditions />



			<!---Randon Text Flag (RTF) Whether to display or enable random texts as defined in RTEXT --->
			<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@RTF")>

			<cfif ArrayLen(AttributeBuff) GT 0>
				<cfset CPObj.RTF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
			<cfelse>
				<cfset CPObj.RTF = "0">
			</cfif>



			<!--- Get RText(s) --->
			<cfset arrRTexts = ArrayNew(1) />
			<cfset selectedRText = XmlSearch(SelectedCP[1], "./RTEXT") />

			<cfset RTEXT_XML = "">

			<cfloop array="#selectedRText#" index="RTextIndex">

				<cfset RTEXT_XMLBUFF = ToString(RTextIndex, "UTF-8")/>
				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, '"', "'", "ALL") />
				<cfset RTEXT_XMLBUFF = TRIM(RTEXT_XMLBUFF) />

				<cfset RTEXT_XML = RTEXT_XML & RTEXT_XMLBUFF/>

				<cfset RTexts = StructNew() />

				<!--- id --->
				<cfset AttributeBuff = XmlSearch(RTextIndex, "@TID")>

				<cfif ArrayLen(AttributeBuff) GT 0>
					<cfset RTexts.TID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
					<cfset RTexts.TID = "0">
				</cfif>

				<!--- TYPE - RESPONSE, CDF, ... --->
				<cfset AttributeBuff = XmlSearch(RTextIndex, "@T64")>

				<cfif ArrayLen(AttributeBuff) GT 0>
					<cfset RTexts.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
					<cfset RTexts.T64 = "0">
				</cfif>

				<!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
				<cfset AttributeBuff = XmlSearch(RTextIndex, "@TEXT")>

				<cfif ArrayLen(AttributeBuff) GT 0>
					<cfset RTexts.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- UnicodeCP ReadCP Formatting --->
					<cfif RTexts.T64 EQ 1>
					    <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
					    <cfset RTexts.TEXT = ToString( ToBinary( RTexts.TEXT ), "UTF-8" ) />
					</cfif>

					<!--- Allow newline in text messages - reformat for display --->
					<cfset RTexts.TEXT = Replace(RTexts.TEXT, "\n", "#chr(10)#", "ALL")>

				<cfelse>
					<cfset RTexts.TEXT = "">
				</cfif>

				<cfset ArrayAppend(arrRTexts, RTexts) />

			</cfloop>

			<cfset CPObj.RTEXT_XML = RTEXT_XML>

			<!--- add all conditions to current question --->
			<cfset CPObj.RTEXT = arrRTexts />

		 </cfif> <!--- CPObj is populated --->


            <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CPOBJ = CPObj>
			<cfset dataout.CPSOObj = GetScheduleQuestion />
			<cfset dataout.STRUCTCOUNT = StructCount(CPObj) />

        <cfcatch TYPE="any">

            <cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataout.TYPE = "#cfcatch.TYPE#">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>

	<cffunction name="ReadCPTempalteDataById" access="remote" output="false" hint="">
		<cfargument name="inpTemplateID" TYPE="string" required="yes" hint="" />
		<cfargument name="inpSelectedTemplateType" TYPE="string" required="no" default="0" hint="Flag to use Template - Optional" />


        <cfargument name="inpQID" TYPE="string" required="yes" hint="The question ID to look up - inpIDKey controls which key to use" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpShowOPTIONS" required="no" default="0" hint="Hint include Options XML in response - optional" />

        <cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />
        <cfargument name="RenderHTML" TYPE="string" required="no" hint="Render for HTML or TextArea" default="1" />

		<cfset var inpRXSSCSCLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var SelectedCP = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var RXSSCSC = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var CPObj = StructNew() />
		<cfset var AttributeBuff = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetXMLString = '' />
		<cfset var DebugStr = '' />
		<cfset var userId = Session.USERID />

		<cfset var arrES	= '' />
		<cfset var ESOPTION_XML	= '' />
		<cfset var ESOPTION_XMLBUFF	= '' />

		<cfset var arrRTexts = '' />
		<cfset var selectedRText = '' />
		<cfset var RTEXT_XML = '' />
		<cfset var RTextIndex = '' />
		<cfset var RTexts = '' />
		<cfset var RTEXT_XMLBUFF = '' />

		<cfset var arrTRNs = '' />
		<cfset var selectedTRN = '' />
		<cfset var TRN_XML = '' />
		<cfset var TRNIndex = '' />
		<cfset var TRNs = '' />
		<cfset var TRN_XMLBUFF = '' />

		<cfset var dataout = StructNew()/>

		<cfset dataout.RXRESULTCODE = -1>

		<cfset dataout.TYPE = "">
		<cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout.STRUCTCOUNT = 0 />


		<cftry>
            <!--- Validate inpQID Id--->
			<cfif !isnumeric(inpQID) >
            	<cfthrow MESSAGE="Invalid Question Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfquery name="GetXMLString" datasource="#Session.DBSourceEBM#">
				SELECT
					<cfif arguments.inpSelectedTemplateType EQ 0 OR arguments.inpSelectedTemplateType EQ 9>
						XMLControlString_vch	as XMLControlString_vch
					<cfelse>
						XMLControlStringBlast_vch as XMLControlString_vch
					</cfif>
				FROM
					simpleobjects.templatecampaign
				WHERE
					TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpTemplateID#">
			</cfquery>

            <cfset inpRXSSCSCLocalBuff = GetXMLString.XMLControlString_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSCSCLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset RXSSCSC = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

			<cfset GROUPCOUNT = 0/>

			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />

            <cfif ArrayLen(SelectedCP) GT 0>

                <cfset CPObj = StructNew() />

                <!--- Dont assume all attributes are there - set defaults --->

                <!--- Get text and type of question --->

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@RQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.RQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- ID --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>

                <!--- T64 Flag for is Base64 Encoded Text --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@T64")>
                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.T64 = "0">
                </cfif>

                <!--- Text --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TEXT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <!--- UnicodeCP ReadCP Formatting --->
					<cfif CPObj.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset CPObj.TEXT = ToString( ToBinary( CPObj.TEXT ), "UTF-8" ) />
					</cfif>

                    <!--- Allow newline in text messages - reformat for display --->
					<cfset CPObj.TEXT = Replace(CPObj.TEXT, "\n", "#chr(10)#", "ALL")>

                <cfelse>
                    <cfset CPObj.TEXT = "">
                </cfif>

                <!--- Text Description--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TDESC")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TDESC = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
                    <!--- Allow newline in text desc messages - reformat for display --->
					<cfif RenderHTML EQ 1>
						<cfset CPObj.TDESC = ReplaceNoCase(CPObj.TDESC, "newlinex", "<br>", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "\n", "<br>", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "#chr(10)#", "<br>", "ALL")>
					<cfelse>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "\n", "#chr(10)#", "ALL")>
						<cfset CPObj.TDESC = Replace(CPObj.TDESC, "<br>", "#chr(10)#", "ALL")>
					</cfif>

                <cfelse>
                    <cfset CPObj.TDESC = "">
                </cfif>

                <!--- Text Guild --->

                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TGUIDE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TGUIDE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
                    <!--- Allow newline in text desc messages - reformat for display --->
					<cfif RenderHTML EQ 1>
						<cfset CPObj.TGUIDE = ReplaceNoCase(CPObj.TGUIDE, "newlinex", "<br>", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "\n", "<br>", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "#chr(10)#", "<br>", "ALL")>
					<cfelse>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "\n", "#chr(10)#", "ALL")>
						<cfset CPObj.TGUIDE = Replace(CPObj.TGUIDE, "<br>", "#chr(10)#", "ALL")>
					</cfif>

                <cfelse>
                    <cfset CPObj.TGUIDE = "">
                </cfif>

                <!--- Walk Through - Flag to enable edit of this control point during siplified walk through--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@SWT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SWT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.SWT = "1">
                </cfif>

				<!--- ANSTEMP--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ANSTEMP")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ANSTEMP = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ANSTEMP = "">
                </cfif>

                <!--- type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@TYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.TYPE = "0">
                </cfif>

                <!--- GID --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@GID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.GID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.GID = "0">
                </cfif>

				<!--- ValidateSMSResponse --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@VALIDATESMSRESPONSE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.VALIDATESMSRESPONSE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.VALIDATESMSRESPONSE = "0">
                </cfif>

                <!--- requiredAns --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@REQANS")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.REQANS = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.REQANS = "0">
                </cfif>

                <!--- Answer Format --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@AF")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.AF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.AF = "HIDDEN">
                </cfif>

                <!--- errMsgTxt --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ERRMSGTXT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ERRMSGTXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ERRMSGTXT = "">
                </cfif>

                <!--- OIG - Opt In Group --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@OIG")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.OIG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.OIG = "0">
                </cfif>

                <!---API Type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_TYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_TYPE = "GET">
                </cfif>

                <!---API Domain --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DOM")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DOM = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DOM = "somwhere.com">
                </cfif>

                <!---API Directory --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DIR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DIR = "">
                </cfif>

                <!---API Port --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_PORT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_PORT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_PORT = "80">
                </cfif>

                <!---API Head --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_HEAD")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_HEAD = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_HEAD = "">
                </cfif>

                <!---API Post --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_FORM")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_FORM = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_FORM = "">
                </cfif>

                <!---API Data --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_DATA")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_DATA = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_DATA = "">
                </cfif>

                <!---API Result Action --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_RA")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_RA = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_RA = "">
                </cfif>

                <!---API Additional Content Type --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@API_ACT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.API_ACT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.API_ACT = "">
                </cfif>

                <!---Store CDF  --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@SCDF")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SCDF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.SCDF = "">
                </cfif>

                <!--- Interval Data --->

                <!--- ITYPE - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ITYPE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ITYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ITYPE = "MINUTES">
                </cfif>

                <!--- IVALUE - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IVALUE")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IVALUE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IVALUE = "0">
                </cfif>

                <!--- IHOUR - The value of the question to check --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IHOUR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IHOUR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IHOUR = "0">
                </cfif>

                <!--- IMIN - The value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IMIN")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IMIN = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IMIN = "0">
                </cfif>

                <!--- INOON - The value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@INOON")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.INOON = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.INOON = "0">
                </cfif>

                <!--- IENQID - The RQ value of the question to check --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IENQID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IENQID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IENQID = "0">
                </cfif>

                <!--- IMRNR - MRNR = Max Repeat No Response --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@IMRNR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.IMRNR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.IMRNR = "4">
                </cfif>

                <!--- INRMO - NRM = No Response Max Option END or NEXT--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@INRMO")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.INRMO = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.INRMO = "END">
                </cfif>


                <!--- Looping limits - before security is violated --->
                <!--- LMAX - Loop Max for this CP - use drip clean to remove--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LMAX")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LMAX = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LMAX = "0">
                </cfif>

                <!--- LDIR - Loop Max default path to move on to: 0 is end GT 0 is PID of next CP --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LDIR")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LDIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LDIR = "-1"> <!--- Default is to terminate -1, 0 is to move on to next step, and GT 0 is to move to that point in flow --->
                </cfif>

                <!--- Message for user if LDIR is 0 and conversation is terminated - default is blank --->
                <cfset AttributeBuff = XmlSearch(SelectedCP[1], "@LMSG")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.LMSG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.LMSG = "">
                </cfif>

                <!--- Branch Logic Data --->

                <!--- BOFNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@BOFNQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.BOFNQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.BOFNQ = "0">
                </cfif>

                <!--- Get answers --->
                <cfset arrAnswer = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./OPTION") />

                <cfset OPTION_XML = "">

                <cfloop array="#selectedAnswer#" index="ans">

                	<cfif inpShowOPTIONS GT 0>
						<cfset OPTION_XMLBUFF = ToString(ans, "UTF-8")/>
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset OPTION_XMLBUFF = Replace(OPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset OPTION_XMLBUFF = TRIM(OPTION_XMLBUFF) />

                        <cfset OPTION_XML = OPTION_XML & OPTION_XMLBUFF/>
                    </cfif>

                    <cfset answer = StructNew() />

				<!--- Get TRN(s) --->
				<cfset arrTRNs = ArrayNew(1) />
				<cfset selectedTRN = XmlSearch(ans, "./TRN") />

				<cfset TRN_XML = "">

				<cfloop array="#selectedTRN#" index="TRNIndex">

					<cfset TRN_XMLBUFF = ToString(TRNIndex, "UTF-8")/>
					<cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset TRN_XMLBUFF = Replace(TRN_XMLBUFF, '"', "'", "ALL") />
					<cfset TRN_XMLBUFF = TRIM(TRN_XMLBUFF) />

					<cfset TRN_XML = TRN_XML & TRN_XMLBUFF/>

					<cfset TRNs = StructNew() />

					<!--- id --->
					<cfset AttributeBuff = XmlSearch(TRNIndex, "@TID")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.TID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
					<cfelse>
						<cfset TRNs.TID = "0">
					</cfif>

					<!--- TYPE - RESPONSE, CDF, ... --->
					<cfset AttributeBuff = XmlSearch(TRNIndex, "@T64")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
					<cfelse>
						<cfset TRNs.T64 = "0">
					</cfif>

					<cfset AttributeBuff = XmlSearch(TRNIndex, "@TEXT")>

					<cfif ArrayLen(AttributeBuff) GT 0>
						<cfset TRNs.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

						<!--- UnicodeCP ReadCP Formatting --->
						<cfif TRNs.T64 EQ 1>
						    <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						    <cfset TRNs.TEXT = ToString( ToBinary( TRNs.TEXT ), "UTF-8" ) />
						</cfif>

						<!--- Allow newline in text messages - reformat for display --->
						<cfset TRNs.TEXT = Replace(TRNs.TEXT, "\n", "#chr(10)#", "ALL")>

					<cfelse>
						<cfset TRNs.TEXT = "">
					</cfif>

					<cfset ArrayAppend(arrTRNs, TRNs) />

				</cfloop>

				<cfset answer.TRN_XML = TRN_XML>

				<!--- add all conditions to current question --->
				<cfset answer.TRN = arrTRNs />


                    <!--- id --->
				<cfset AttributeBuff = XmlSearch(ans, "@ID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.id = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.id = "0">
                    </cfif>

                    <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

                    <!--- errMsgTxt --->
					<cfset AttributeBuff = XmlSearch(ans, "@TEXT")>

                     <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.text = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.text = ToString( ToBinary( answer.text ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.text = Replace(answer.text, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.text = "">
	                </cfif>

                    <!--- Assigned Answer Value --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVAL")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.AVAL = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.AVAL = "">
                    </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVALREG")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.AVALREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.AVALREG = "">
                    </cfif>

				<!--- Optional statement response --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALRESP")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALRESP = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

				    <!--- UnicodeCP ReadCP Formatting --->
				    <cfif answer.T64 EQ 1>
					    	<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
					    	<cfset answer.AVALRESP = ToString( ToBinary( answer.AVALRESP ), "UTF-8" ) />
				    </cfif>

				    <!--- Allow newline in text messages - reformat for display --->
				    <cfset answer.AVALRESP = Replace(answer.AVALRESP, "\n", "#chr(10)#", "ALL")>


				<cfelse>
				    <cfset answer.AVALRESP = "">
				</cfif>

				<!--- Optional redirect to another flow --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALREDIR")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALREDIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
				    <cfset answer.AVALREDIR = "">
				</cfif>

				<!--- Optional move on to another CP - default 0 is just next CP --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALNEXT")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALNEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
				    <cfset answer.AVALNEXT = "0">
				</cfif>

                    <cfset ArrayAppend(arrAnswer, answer) />
                </cfloop>

                <cfset CPObj.answers = arrAnswer />
                <cfset CPObj.OPTIONS = arrAnswer />

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.OPTION_XML = OPTION_XML>
                <cfelse>
	                <cfset CPObj.OPTION_XML = "">
                </cfif>


                <!--- Get ESOptions --->
                <cfset arrES = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./ESOPTION") />

                <cfset ESOPTION_XML = "">

                <cfloop array="#selectedAnswer#" index="ans">

                	<cfif inpShowOPTIONS GT 0>
						<cfset ESOPTION_XMLBUFF = ToString(ans, "UTF-8")/>
                        <cfset ESOPTION_XMLBUFF = Replace(ESOPTION_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset ESOPTION_XMLBUFF = Replace(ESOPTION_XMLBUFF, '"', "'", "ALL") />
                        <cfset ESOPTION_XMLBUFF = TRIM(ESOPTION_XMLBUFF) />

                        <cfset ESOPTION_XML = ESOPTION_XML & ESOPTION_XMLBUFF/>
                    </cfif>

                    <cfset answer = StructNew() />

                    <!--- id --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.ESID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.ESID = "0">
                    </cfif>

                     <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

	                <!--- Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESTEXT")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.ESTEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.ESTEXT = ToString( ToBinary( answer.ESTEXT ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.ESTEXT = Replace(answer.ESTEXT, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.ESTEXT = "">
	                </cfif>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.ESTEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.ESTEXT = "">
                    </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESREG")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset answer.ESREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset answer.ESREG = "">
                    </cfif>

                    <cfset ArrayAppend(arrES, answer) />
                </cfloop>

                <cfset CPObj.ESOPTIONS = arrES />

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.ESOPTION_XML = ESOPTION_XML>
                <cfelse>
	                <cfset CPObj.ESOPTION_XML = "">
                </cfif>

			 <!--- Get conditions --->
                <cfset arrConditions = ArrayNew(1) />
                <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./COND") />

                <cfset COND_XML = "">

                <cfloop array="#selectedAnswer#" index="conditionsIndex">

					<cfif inpShowOPTIONS GT 0>

                        <cfset COND_XMLBUFF = ToString(conditionsIndex, "UTF-8")/>
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
                        <cfset COND_XMLBUFF = Replace(COND_XMLBUFF, '"', "'", "ALL") />
                        <cfset COND_XMLBUFF = TRIM(COND_XMLBUFF) />

                        <cfset COND_XML = COND_XML & COND_XMLBUFF/>

                    </cfif>

                    <cfset conditions = StructNew() />

                    <!--- id --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@CID")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.CID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.CID = "0">
                    </cfif>

                    <!--- Branch Logic Data --->

        			<!--- TYPE - RESPONSE, CDF, ... --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@TYPE")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.TYPE = "0">
                    </cfif>

                    <!--- BOQ - The physical ID of the question to check - Not the RQ which is the question order and may change --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOQ")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOQ = "0">
                    </cfif>

                    <!--- BOC The comparison of the question to value to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOC")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOC = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOC = "0">
                    </cfif>

                    <!--- BOV - The value of the question to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOV = "0">
                    </cfif>

                    <!--- BOAV - The value of the question to check --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOAV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOAV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOAV = "0">
                    </cfif>

                    <!--- BOCDV - The value of the question to check Branch Options Client Data Value --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOCDV")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOCDV = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOCDV = "0">
                    </cfif>

                    <!--- BOTNQ - The physical ID of the question to move to - Not the RQ which is the question order and may change --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@BOTNQ")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.BOTNQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.BOTNQ = "0">
                    </cfif>

                    <!--- Notes --->
                    <cfset AttributeBuff = XmlSearch(conditionsIndex, "@DESC")>

                    <cfif ArrayLen(AttributeBuff) GT 0>
                        <cfset conditions.text = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                    <cfelse>
                        <cfset conditions.text = "">
                    </cfif>

                    <cfset ArrayAppend(arrConditions, conditions) />

                </cfloop>

                <cfif inpShowOPTIONS GT 0>
                	<cfset CPObj.COND_XML = COND_XML>
                <cfelse>
	                <cfset CPObj.COND_XML = "">
                </cfif>

                <cfset CPObj.COND_XML = COND_XML>

                <!--- add all conditions to current question --->
                <cfset CPObj.CONDITIONS = arrConditions />

			 <!---Randon Text Flag (RTF) Whether to display or enable random texts as defined in RTEXT --->
 			<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@RTF")>

 			<cfif ArrayLen(AttributeBuff) GT 0>
 				<cfset CPObj.RTF = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
 			<cfelse>
 				<cfset CPObj.RTF = "0">
 			</cfif>

 			<!--- Get RText(s) --->
 			<cfset arrRTexts = ArrayNew(1) />
 			<cfset selectedRText = XmlSearch(SelectedCP[1], "./RTEXT") />

 			<cfset RTEXT_XML = "">

 			<cfloop array="#selectedRText#" index="RTextIndex">

 				<cfset RTEXT_XMLBUFF = ToString(RTextIndex, "UTF-8")/>
 				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
 				<cfset RTEXT_XMLBUFF = Replace(RTEXT_XMLBUFF, '"', "'", "ALL") />
 				<cfset RTEXT_XMLBUFF = TRIM(RTEXT_XMLBUFF) />

 				<cfset RTEXT_XML = RTEXT_XML & RTEXT_XMLBUFF/>

 				<cfset RTexts = StructNew() />

 				<!--- id --->
 				<cfset AttributeBuff = XmlSearch(RTextIndex, "@TID")>

 				<cfif ArrayLen(AttributeBuff) GT 0>
 					<cfset RTexts.TID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
 				<cfelse>
 					<cfset RTexts.TID = "0">
 				</cfif>

 				<!--- TYPE - RESPONSE, CDF, ... --->
 				<cfset AttributeBuff = XmlSearch(RTextIndex, "@T64")>

 				<cfif ArrayLen(AttributeBuff) GT 0>
 					<cfset RTexts.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
 				<cfelse>
 					<cfset RTexts.T64 = "0">
 				</cfif>

				<cfset AttributeBuff = XmlSearch(RTextIndex, "@TEXT")>

				<cfif ArrayLen(AttributeBuff) GT 0>
					<cfset RTexts.TEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

					<!--- UnicodeCP ReadCP Formatting --->
					<cfif RTexts.T64 EQ 1>
					    <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
					    <cfset RTexts.TEXT = ToString( ToBinary( RTexts.TEXT ), "UTF-8" ) />
					</cfif>

					<!--- Allow newline in text messages - reformat for display --->
					<cfset RTexts.TEXT = Replace(RTexts.TEXT, "\n", "#chr(10)#", "ALL")>

				<cfelse>
					<cfset RTexts.TEXT = "">
				</cfif>

 				<cfset ArrayAppend(arrRTexts, RTexts) />

 			</cfloop>

 			<cfset CPObj.RTEXT_XML = RTEXT_XML>

 			<!--- add all conditions to current question --->
 			<cfset CPObj.RTEXT = arrRTexts />

            </cfif>

            <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CPOBJ = CPObj>
			<cfset dataout.STRUCTCOUNT = StructCount(CPObj) />

        <cfcatch TYPE="any">

            <cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataout.TYPE = "#cfcatch.TYPE#">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>

	<cffunction name="ReadCPs" access="remote" output="false" hint="Get list of all Control Points from XMLControlString in specified BatchID. Can either be by physical ID (ID) or Position ID (RQ) controled by inpIDKey. Default is RQ">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
		<cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />

		<cfset var inpRXSSCSCLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var SelectedCP = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var RXSSCSC = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var CPObj = StructNew() />
		<cfset var CPObjects = StructNew() />
		<cfset var AttributeBuff = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetBatchQuestion = '' />
		<cfset var CurrCP	= '' />
		<cfset var DebugStr = '' />
		<cfset var CPCounter = 0 />
		<cfset var SWTCounter = 0 />
		<cfset var userId = Session.USERID />

		<cfset var dataout = StructNew()/>

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.INPBATCHID = "#INPBATCHID#">
		<cfset dataout.CPCOUNT = 0>
		<cfset dataout.SWTCOUNT = 0>
		<cfset dataout.CPObj = StructNew() />
		<cfset dataout.TYPE = "">
		<cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">

		<cftry>

            <!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

	            <cfif arguments.inpUserId GT 0>
	            	<cfset userId = arguments.inpUserId/>
	            </cfif>

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
	            </cfquery>

			</cfif>

            <cfset inpRXSSCSCLocalBuff = GetBatchQuestion.XMLControlString_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSCSCLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset CPObjects = ArrayNew(1) />

			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q") />

            <cfloop array="#SelectedCP#" index="CurrCP">

                <cfset CPObj = StructNew() />

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@RQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.RQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <cfset CPCounter = CPCounter + 1 />

                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- ID --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@ID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>

                <!--- WT --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@SWT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SWT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <cfif CPObj.SWT EQ "1">
                        <cfset SWTCounter = SWTCounter + 1 />
                    </cfif>
                <cfelse>
                    <cfset CPObj.SWT = "1">

                    <cfset SWTCounter = SWTCounter + 1 />
                </cfif>

			 <!--- type --->
			 <cfset AttributeBuff = XmlSearch(CurrCP, "@TYPE")>

			 <cfif ArrayLen(AttributeBuff) GT 0>
				<cfset CPObj.TYPE = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
			 <cfelse>
				<cfset CPObj.TYPE = "0">
			 </cfif>

                <cfset ArrayAppend(CPObjects, CPObj) />

            </cfloop>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.CPCOUNT = CPCounter>
            <cfset dataout.SWTCOUNT = SWTCounter>
	  	  <cfset dataout.CPOBJ = CPObjects>

        <cfcatch TYPE="any">

            <cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataout.TYPE = "#cfcatch.TYPE#">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>
	<cffunction name="ReadCPsFromTemplateID" access="remote" output="false" hint="get xml string by template id">
		<cfargument name="inpTemplateID" TYPE="string" required="yes" hint="" />
		<cfargument name="inpSelectedTemplateType" TYPE="string" required="no" default="0" />

		<cfset var inpRXSSCSCLocalBuff = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var SelectedCP = '' />
		<cfset var CURREXP = '' />
		<cfset var DTime = '' />
		<cfset var RXSSCSC = '' />
		<cfset var ISDISABLEBACKBUTTON = '' />
		<cfset var Configs = '' />
		<cfset var GROUPCOUNT = '' />
		<cfset var CPObj = StructNew() />
		<cfset var CPObjects = StructNew() />
		<cfset var AttributeBuff = '' />
		<cfset var arrAnswer = '' />
		<cfset var selectedAnswer = '' />
		<cfset var OPTION_XML = '' />
		<cfset var OPTION_XMLBUFF = '' />
		<cfset var answer = '' />
		<cfset var arrConditions = '' />
		<cfset var COND_XML = '' />
		<cfset var COND_XMLBUFF = '' />
		<cfset var conditions = '' />
		<cfset var ans = '' />
		<cfset var conditionsIndex = '' />
		<cfset var GetXMLString = '' />
		<cfset var CurrCP	= '' />
		<cfset var DebugStr = '' />
		<cfset var CPCounter = 0 />
		<cfset var SWTCounter = 0 />

		<cfset var dataout = StructNew()/>

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.CPCOUNT = 0>
		<cfset dataout.SWTCOUNT = 0>
		<cfset dataout.CPObj = StructNew() />
		<cfset dataout.TYPE = "">
		<cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">

		<cftry>


			<cfquery name="GetXMLString" datasource="#Session.DBSourceEBM#">
				SELECT
					<cfif arguments.inpSelectedTemplateType EQ 0 OR arguments.inpSelectedTemplateType EQ 9 >
						XMLControlString_vch	as XMLControlString_vch
					<cfelse>
						XMLControlStringBlast_vch as XMLControlString_vch
					</cfif>
				FROM
					simpleobjects.templatecampaign
				WHERE
					TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpTemplateID#">
			</cfquery>

            <cfset inpRXSSCSCLocalBuff = GetXMLString.XMLControlString_vch />

			<!--- Parse XML Control String--->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSCSCLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset CPObjects = ArrayNew(1) />

			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q") />

            <cfloop array="#SelectedCP#" index="CurrCP">

                <cfset CPObj = StructNew() />

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@RQ")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.RQ = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <cfset CPCounter = CPCounter + 1 />

                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- ID --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@ID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.ID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>

                <!--- WT --->
				<cfset AttributeBuff = XmlSearch(CurrCP, "@SWT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SWT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

                    <cfif CPObj.SWT EQ "1">
                        <cfset SWTCounter = SWTCounter + 1 />
                    </cfif>
                <cfelse>
                    <cfset CPObj.SWT = "1">

                    <cfset SWTCounter = SWTCounter + 1 />
                </cfif>

                <cfset ArrayAppend(CPObjects, CPObj) />

            </cfloop>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.CPCOUNT = CPCounter>
            <cfset dataout.SWTCOUNT = SWTCounter>
			<cfset dataout.CPOBJ = CPObjects>

        <cfcatch TYPE="any">

            <cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataout.TYPE = "#cfcatch.TYPE#">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

        </cfcatch>
		</cftry>

		<!--- Return a RecordSet--->
		<cfreturn dataout />

	</cffunction>

	<cffunction name="CP_Save" access="remote" output="false" hint="Save a CP to the XML by BatchID">
		<cfargument name="inpBatchId" TYPE="string" required="true" hint="The Batch Id to change"/>
		<cfargument name="controlPoint" type="any" required="true" hint="The JSON structure of the Control Point"/>
		<cfargument name="STARTNUMBERQ" TYPE="numeric" required="false" default="1" hint="The current page for supporting only showing a sub group of Control Point (CP) in the flow - 1 based - default to 1 if no paging used">
	    <cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
	    <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.PID = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />


		<cfset var CurrPID = 0 />
		<cfset var DebugStr	= '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var SelectedCP	= '' />
		<cfset var AttributeBuff	= '' />
		<cfset var Counter	= '' />
		<cfset var NewRXSSCSC	= '' />
		<cfset var Qs	= '' />
		<cfset var OutToDBXMLBuff	= '' />
		<cfset var FinalDoc	= '' />
		<cfset var subcriberArray	= '' />
		<cfset var _options	= '' />
		<cfset var _conditions	= '' />
		<cfset var QItem	= '' />
		<cfset var GetBatchQuestion	= '' />
		<cfset var WriteBatchOptions	= '' />
		<cfset var RetVarRenderCP	= '' />
		<cfset var POSITION = 0 />
		<cfset var inpXML	= '' />
		<cfset var OPTIONSXML = '' />
		<cfset var OPTIONITEM = '' />
		<cfset var CONDITIONITEM	= '' />
		<cfset var CONDITIONSXML	= '' />
		<cfset var APIXML = ''/>
		<cfset var CDFXML = '' />
		<cfset var OPTINXML = '' />
		<cfset var INTERVALXML = '' />
		<cfset var AddSubscriberBatchRecord = '' />
		<cfset var AddSubscriberBatchRecordResult = '' />
		<cfset var ESOPTIONITEM	= '' />
		<cfset var ESOPTIONSXML	= '' />
		<cfset var ANSTEMPXML=''>
		<cfset var LOOPLIMITSXML=''>
		<cfset var RTEXTITEM	= '' />
		<cfset var RTEXTXML	= '' />

		<cfset var TRNITEM	= '' />
		<cfset var TRNXML	= '' />


		<cfset var OPT_NUM_XML=''>
		<cfset var OPT_TYPE_XML=''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var debugInpXML = ''/>
		<cfset var RetVarRunCheckBlackList = '' />

		<cftry>

			<cfset arguments.controlPoint = DeSerializeJSON(arguments.controlPoint) />

			<cfif !StructKeyExists(arguments.controlPoint, "LMAX") >
	 			<cfset arguments.controlPoint.LMAX = "0" />
 			</cfif>

 			<cfif !StructKeyExists(arguments.controlPoint, "LDIR") >
	 			<cfset arguments.controlPoint.LDIR = "-1" />
 			</cfif>

 			<cfif !StructKeyExists(arguments.controlPoint, "LMSG") >
	 			<cfset arguments.controlPoint.LMSG = "" />
 			</cfif>

			<cfif !StructKeyExists(arguments.controlPoint, "OPT_NUM") >
	 			<cfset arguments.controlPoint.OPT_NUM = "" />
 			</cfif>
			<cfif !StructKeyExists(arguments.controlPoint, "OPT_TYPE") >
	 			<cfset arguments.controlPoint.OPT_TYPE = "" />
 			</cfif>

			<cfif !StructKeyExists(arguments.controlPoint, "RTF") >
	 			<cfset arguments.controlPoint.RTF = "0" />
 			</cfif>

			<cfif !StructKeyExists(arguments.controlPoint, "RTEXT") >
				<cfset arguments.controlPoint.RTEXT = ArrayNew() />
			</cfif>

			<!---
			<!--- All top level CPs (or Q elememnts) will have some form of default value for all of these XML --->
			<!--- Not all CP types use all values - leave it up to the display to allow users to edit the important ones --->
			<cfset arguments.controlPoint = 	{
										AF = "NOFORMAT",
										API_ACT = "",
										API_DATA = "",
										API_DIR = "",
										API_DOM = "somwhere.com",
										API_PORT = "80",
										API_HEAD = "",
										API_FORM = "",
										API_RA = "",
										API_TYPE = "GET",
										BOFNQ = "0",
										CONDITIONS = _conditions,
										COND_XML = "",
										ERRMSGTXT = "",
										GID = controlPoint.GID,
										ID = NextPID,
										IENQID = "0",
										IHOUR = "0",
										IMIN = "0",
										IMRNR = "4",
										INOON = "0",
										INRMO = "END",
										ITYPE = "MINUTES",
										IVALUE = "0",
										LMAX = "0",
										LDIR = "-1",
										LMSG = "",
										OIG = "0",
										OPTION_XML = "",
										OPTIONS = _options,
										REQUIREDANS = "undefined",
										RQ = POSITION,
										SCDF = "",
										TDESC = StrEscUtils.unescapeHTML(inpDesc),
										TEXT = StrEscUtils.unescapeHTML(inpText),
										TYPE = "STATEMENT"
									} />

--->
			<cfset POSITION = arguments.controlPoint.RQ />

			<cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>

            <cfset DebugStr = DebugStr & "GetBatchQuestion.RecordCount(#GetBatchQuestion.RecordCount#)" />

			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Read the physical ID (this does not change on edits even if RQ is moved!) --->
			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @RQ = #controlPoint.RQ#]") />

            <cfif ArrayLen(SelectedCP) GT 0>

                <!--- RQ - the order of the question --->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@ID")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CurrPID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                </cfif>

            </cfif>

			<cfset arguments.controlPoint.ID = CurrPID />

			<cfinvoke method="RunCheckBlackList" returnvariable="RetVarRunCheckBlackList">
				<cfinvokeargument name="inpText" value="#arguments.controlPoint.TEXT#">
			</cfinvoke>

			<cfif RetVarRunCheckBlackList.RXRESULTCODE NEQ 1 >

				<cfset dataout.RXRESULTCODE = -5 >
				<cfset dataout.TYPE = "Error">
				<cfset dataout.MESSAGE = "Blacklist Rules Violation<br/><br/>">
				<cfset dataout.ERRMESSAGE = 'One or more words in your ad copy are in violation of CITA SHAFT policies. You will not be able to save or send a message containing these keywords: <br/> <b>*<b/>#Replace(RetVarRunCheckBlackList.BLACKLISTWORD, ",", "<br/><b>*<b/>", "ALL")#' >

				<cfreturn dataout />
			</cfif>

            	<!--- XMLFormat screws with line breaks - need to store as \n in actual string for end sending logic to work  --->
			<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "#chr(13)##chr(10)#", "\n", "ALL")>
			<cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "#chr(10)#", "\n", "ALL")>

			<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "#chr(13)##chr(10)#", "\n", "ALL")>
			<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "#chr(10)#", "\n", "ALL")>

			<cfset arguments.controlPoint.TEXT = XMLFormat(arguments.controlPoint.TEXT) />
			<cfset arguments.controlPoint.TDESC = XMLFormat(arguments.controlPoint.TDESC) />

           	<cfif structKeyExists(arguments.controlPoint, "TGUIDE")>
            	<cfset arguments.controlPoint.TGUIDE = Replace(arguments.controlPoint.TGUIDE, "#chr(13)##chr(10)#", "\n", "ALL")>
           		<cfset arguments.controlPoint.TGUIDE = Replace(arguments.controlPoint.TGUIDE, "#chr(10)#", "\n", "ALL")>
           	<cfelse>
           		<cfset arguments.controlPoint.TGUIDE = ""/>
           	</cfif>


           	<!--- Build OPTIONS if any --->

            <cfloop array="#controlPoint.OPTIONS#" index="OPTIONITEM">
				<cfset OPTIONITEM.TEXT = XMLFormat(OPTIONITEM.TEXT) />
				<cfset OPTIONITEM.AVAL = XMLFormat(OPTIONITEM.AVAL) />
				<cfset OPTIONITEM.AVALREG = XMLFormat(OPTIONITEM.AVALREG) />
				<cfset OPTIONITEM.AVALRESP = XMLFormat(OPTIONITEM.AVALRESP) />
				<cfset OPTIONITEM.AVALREDIR = XMLFormat(OPTIONITEM.AVALREDIR) />
				<cfset OPTIONITEM.AVALNEXT = XMLFormat(OPTIONITEM.AVALNEXT) />

				<cfif controlPoint.AF NEQ "HIDDEN">
					<cfinvoke method="RunCheckBlackList" returnvariable="RetVarRunCheckBlackList">
						<cfinvokeargument name="inpText" value="#OPTIONITEM.TEXT#">
					</cfinvoke>

					<cfif RetVarRunCheckBlackList.RXRESULTCODE NEQ 1 >

						<cfset dataout.RXRESULTCODE = -5 >
						<cfset dataout.TYPE = "Error">
						<cfset dataout.MESSAGE = "Blacklist Rules Violation<br/><br/>">
						<cfset dataout.ERRMESSAGE = 'One or more words in your ad copy are in violation of CITA SHAFT policies. You will not be able to save or send a message containing these keywords: <br/> <b>*<b/>#Replace(RetVarRunCheckBlackList.BLACKLISTWORD, ",", "<br/><b>*<b/>", "ALL")#' >

						<cfreturn dataout />
					</cfif>
				</cfif>

				<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
	    			 <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

	    			 <!--- default is plain text storage --->
	    			 <cfset OPTIONITEM.T64 = 0 />

	    			 <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	    			 <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", OPTIONITEM.TEXT ) ) />

	    			 <!--- Look for character higher than 255 ASCII --->

	    			 <cfif UniSearchMatcher.Find() >

	    				 <!--- UnicodeCP Save_cp Formatting processing --->
	    				 <cfset OPTIONITEM.T64 = 1 />

	    			 </cfif>

	    			 <!--- now check optional response AVALRESP --->
	    			 <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	    			 <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", OPTIONITEM.AVALRESP ) ) />

	    			 <!--- Look for character higher than 255 ASCII --->

	    			 <cfif UniSearchMatcher.Find() >

	    				 <!--- UnicodeCP Save_cp Formatting processing --->
	    				 <cfset OPTIONITEM.T64 = 1 />

	    			 </cfif>

	    			 <cfif OPTIONITEM.T64 EQ 1>
	    				 <!--- IF any of the items are unicode - format all text --->
	    				 <cfset OPTIONITEM.TEXT = toBase64(OPTIONITEM.TEXT) />
	    				 <cfset OPTIONITEM.AVALRESP = toBase64(OPTIONITEM.AVALRESP) />
	    			 </cfif>

				 <!--- AI Training Text for each option --->
				 <cfif !StructKeyExists(OPTIONITEM, "TRN") >
 					<cfset OPTIONITEM.TRN = ArrayNew() />
 				</cfif>

				<cfset TRNXML = "" />
				<cfloop array="#OPTIONITEM.TRN#" index="TRNITEM">
	 				   <cfset TRNITEM.TEXT = XMLFormat(TRNITEM.TEXT) />

	 				   <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
	 				    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

	 				    <!--- default is plain text storage --->
	 				    <cfset TRNITEM.T64 = 0 />

	 				    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	 				    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TRNITEM.TEXT ) ) />

	 				    <!--- Look for character higher than 255 ASCII --->

	 				    <cfif UniSearchMatcher.Find() >

	 					    <!--- UnicodeCP Save_cp Formatting processing --->
	 					    <cfset TRNITEM.T64 = 1 />

	 				    </cfif>

	 				    <cfif TRNITEM.T64 EQ 1>
	 					    <!--- IF any of the items are unicode - format all text --->
	 					    <cfset TRNITEM.TEXT = toBase64(TRNITEM.TEXT) />
	 				    </cfif>

	 				  <cfset TRNXML = TRNXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<TRN T64='#TRNITEM.T64#'  TEXT='#TRNITEM.TEXT#' TID='#TRNITEM.TID#'>0</TRN>" />

	 			</cfloop>

			    <cfset OPTIONSXML = OPTIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<OPTION T64='#OPTIONITEM.T64#'  AVAL='#OPTIONITEM.AVAL#' ID='#OPTIONITEM.ID#' TEXT='#OPTIONITEM.TEXT#' AVALREG='#OPTIONITEM.AVALREG#' AVALRESP='#OPTIONITEM.AVALRESP#' AVALREDIR='#OPTIONITEM.AVALREDIR#' AVALNEXT='#OPTIONITEM.AVALNEXT#'>#TRNXML#</OPTION>" />

			</cfloop>

			<cfloop array="#controlPoint.RTEXT#" index="RTEXTITEM">
				   <cfset RTEXTITEM.TEXT = XMLFormat(RTEXTITEM.TEXT) />


				   <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				    <!--- default is plain text storage --->
				    <cfset RTEXTITEM.T64 = 0 />

				    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
				    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", RTEXTITEM.TEXT ) ) />

				    <!--- Look for character higher than 255 ASCII --->

				    <cfif UniSearchMatcher.Find() >

					    <!--- UnicodeCP Save_cp Formatting processing --->
					    <cfset RTEXTITEM.T64 = 1 />

				    </cfif>


				    <cfif RTEXTITEM.T64 EQ 1>
					    <!--- IF any of the items are unicode - format all text --->
					    <cfset RTEXTITEM.TEXT = toBase64(RTEXTITEM.TEXT) />
				    </cfif>

				  <cfset RTEXTXML = RTEXTXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<RTEXT T64='#RTEXTITEM.T64#'  TEXT='#RTEXTITEM.TEXT#' TID='#RTEXTITEM.TID#'>0</RTEXT>" />

			</cfloop>



            <!--- Build Expert System ESOPTIONS if any --->
            <cfloop array="#controlPoint.ESOPTIONS#" index="ESOPTIONITEM">
	            <cfset ESOPTIONITEM.ESTEXT = XMLFormat(ESOPTIONITEM.ESTEXT) />
	            <cfset ESOPTIONITEM.ESID = XMLFormat(ESOPTIONITEM.ESID) />
	            <cfset ESOPTIONITEM.ESREG = XMLFormat(ESOPTIONITEM.ESREG) />

			    <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
				<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

				<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", ESOPTIONITEM.ESTEXT ) ) />

	            <!--- Look for character higher than 255 ASCII --->

	          	<cfif UniSearchMatcher.Find() >

	            	<cfset ESOPTIONITEM.ESTEXT = toBase64(ESOPTIONITEM.ESTEXT) />

	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset ESOPTIONITEM.T64 = 1 />

	            <cfelse>
	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset ESOPTIONITEM.T64 = 0 />
	            </cfif>

			    <cfset ESOPTIONSXML = ESOPTIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<ESOPTION T64='#ESOPTIONITEM.T64#' ESID='#ESOPTIONITEM.ESID#' ESTEXT='#ESOPTIONITEM.ESTEXT#' ESREG='#ESOPTIONITEM.ESREG#'>0</ESOPTION>" />
			</cfloop>

            <!--- Build CONDITIONS if any --->
            <cfloop array="#controlPoint.CONDITIONS#" index="CONDITIONITEM">
	            <cfset CONDITIONITEM.DESC = XMLFormat(CONDITIONITEM.DESC) />
	            <cfset CONDITIONITEM.BOAV = XMLFormat(CONDITIONITEM.BOAV) />
	            <cfset CONDITIONITEM.BOC = XMLFormat(CONDITIONITEM.BOC) />
	            <cfset CONDITIONITEM.BOCDV = XMLFormat(CONDITIONITEM.BOCDV) />
	            <cfset CONDITIONITEM.BOV = XMLFormat(CONDITIONITEM.BOV) />
			    <cfset CONDITIONSXML = CONDITIONSXML & "#chr(13)##chr(10)##chr(9)##chr(9)#<COND BOAV='#CONDITIONITEM.BOAV#' BOC='#CONDITIONITEM.BOC#' BOCDV='#CONDITIONITEM.BOCDV#' BOQ='#CONDITIONITEM.BOQ#' BOTNQ='#CONDITIONITEM.BOTNQ#' BOV='#CONDITIONITEM.BOV#' CID='#CONDITIONITEM.CID#' TYPE='#CONDITIONITEM.TYPE#' DESC='#CONDITIONITEM.DESC#'></COND>" />
			</cfloop>

            <!--- Default is empty string --->
            <cfif arguments.controlPoint.TYPE EQ "API">

				<!--- Legacy fix --->
				<cfif !StructKeyExists(arguments.controlPoint, "API_HEAD") >
					<cfset arguments.controlPoint.API_HEAD = "" />
				</cfif>

				<cfif !StructKeyExists(arguments.controlPoint, "API_FORM") >
					<cfset arguments.controlPoint.API_FORM = "" />
				</cfif>

	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.API_ACT = XMLFormat(arguments.controlPoint.API_ACT) />
	            <cfset arguments.controlPoint.API_DATA = XMLFormat(arguments.controlPoint.API_DATA) />
	            <cfset arguments.controlPoint.API_DIR = XMLFormat(arguments.controlPoint.API_DIR) />
	            <cfset arguments.controlPoint.API_DOM = XMLFormat(arguments.controlPoint.API_DOM) />
	            <cfset arguments.controlPoint.API_PORT = XMLFormat(arguments.controlPoint.API_PORT) />
	            <cfset arguments.controlPoint.API_HEAD = XMLFormat(arguments.controlPoint.API_HEAD) />
	            <cfset arguments.controlPoint.API_FORM = XMLFormat(arguments.controlPoint.API_FORM) />
	            <cfset arguments.controlPoint.API_RA = XMLFormat(arguments.controlPoint.API_RA) />
	            <cfset arguments.controlPoint.API_TYPE = XMLFormat(arguments.controlPoint.API_TYPE) />
			    <cfset APIXML = "API_ACT='#controlPoint.API_ACT#' API_DATA='#controlPoint.API_DATA#' API_DIR='#controlPoint.API_DIR#' API_DOM='#controlPoint.API_DOM#' API_PORT='#controlPoint.API_PORT#' API_HEAD='#controlPoint.API_HEAD#' API_FORM='#controlPoint.API_FORM#' API_RA='#controlPoint.API_RA#' API_TYPE='#controlPoint.API_TYPE#'" />
		    </cfif>

            <!--- Default is empty string --->
            <cfif arguments.controlPoint.TYPE EQ "CDF">
	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.SCDF = XMLFormat(arguments.controlPoint.SCDF) />
			    <cfset CDFXML = "SCDF='#controlPoint.SCDF#'" />
            </cfif>

            <!--- Default is empty string --->
            <cfif arguments.controlPoint.TYPE EQ "OPTIN">
	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.OIG = XMLFormat(arguments.controlPoint.OIG) />
			    <cfset OPTINXML = "OIG='#controlPoint.OIG#'" />

				<cfquery name="AddSubscriberBatchRecord" datasource="#Session.DBSourceEBM#" result="AddSubscriberBatchRecordResult">
	                UPDATE
	                	simplelists.subscriberbatch
	                SET
	                	GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#controlPoint.OIG#">
	                WHERE
	                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>
            </cfif>

            <!--- Default is empty string - smaller kinder XML --->
            <cfif ListContains('ONESELECTION,SHORTANSWER,INTERVAL', arguments.controlPoint.TYPE, ',') GT 0>
	            <!--- Build API Options if any --->
                <cfset arguments.controlPoint.ITYPE = XMLFormat(arguments.controlPoint.ITYPE) />
                <cfset arguments.controlPoint.IVALUE = XMLFormat(arguments.controlPoint.IVALUE) />
                <cfset arguments.controlPoint.IHOUR = XMLFormat(arguments.controlPoint.IHOUR) />
                <cfset arguments.controlPoint.IMIN = XMLFormat(arguments.controlPoint.IMIN) />
                <cfset arguments.controlPoint.INOON = XMLFormat(arguments.controlPoint.INOON) />
                <cfset arguments.controlPoint.IENQID = XMLFormat(arguments.controlPoint.IENQID) />
                <cfset arguments.controlPoint.IMRNR = XMLFormat(arguments.controlPoint.IMRNR) />
                <cfset arguments.controlPoint.INRMO = XMLFormat(arguments.controlPoint.INRMO) />
                <cfset INTERVALXML = "ITYPE='#controlPoint.ITYPE#' IVALUE='#controlPoint.IVALUE#' IHOUR='#controlPoint.IHOUR#' IMIN='#controlPoint.IMIN#' INOON='#controlPoint.INOON#' IENQID='#controlPoint.IENQID#' IMRNR='#controlPoint.IMRNR#' INRMO='#controlPoint.INRMO#'" />
		    </cfif>

            <cfif ListContains('ONESELECTION,SHORTANSWER', arguments.controlPoint.TYPE, ',') GT 0>
				<cfset ANSTEMPXML = "ANSTEMP='#controlPoint.ANSTEMP#' " />
			</cfif>

			<!--- Build Loop Limit Options if any --->
            <cfset arguments.controlPoint.LMAX = XMLFormat(arguments.controlPoint.LMAX) />
            <cfset arguments.controlPoint.LDIR = XMLFormat(arguments.controlPoint.LDIR) />
            <cfset arguments.controlPoint.LMSG = XMLFormat(arguments.controlPoint.LMSG) />
            <cfset LOOPLIMITSXML = "LMAX='#controlPoint.LMAX#' LDIR='#controlPoint.LDIR#' LMSG='#controlPoint.LMSG#'" />

            <cfset OPT_NUM_XML = "OPT_NUM='#controlPoint.OPT_NUM#'" />

            <cfset OPT_TYPE_XML = "OPT_TYPE='#controlPoint.OPT_TYPE#'" />

			<!--- Only convert if passed in value has not already been converted --->
			<cfif arguments.controlPoint.T64 EQ 0 >

				<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
	            <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

	            <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

				<!--- Look for character higher than 255 ASCII --->

	          	<cfif UniSearchMatcher.Find() >

	            	<cfset arguments.controlPoint.TEXT = toBase64(arguments.controlPoint.TEXT) />

	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset arguments.controlPoint.T64 = 1 />

	            <cfelse>
	                <!--- UnicodeCP Save_cp Formatting processing --->
					<cfset arguments.controlPoint.T64 = 0 />
	            </cfif>

			</cfif>

            <!--- Build XML for a statement --->
            <!--- Must be valid XML --->
            <!--- RQ is not defined here since it is absolutely positioned in the XML - RQ is generated after XML is generated --->
            <cfset inpXML = "#Chr(13)##Chr(10)##chr(9)#<Q RQ='#controlPoint.RQ#' ID='#CurrPID#' TYPE='#controlPoint.TYPE#' T64='#controlPoint.T64#' TEXT='#arguments.controlPoint.TEXT#' RTF='#arguments.controlPoint.RTF#' TDESC='#controlPoint.TDESC#' SWT='#controlPoint.SWT#' GID='#controlPoint.GID#' MDF='0' AF='#controlPoint.AF#' #INTERVALXML# #LOOPLIMITSXML# REQANS='undefined' errMsgTxt='undefined' BOFNQ='#controlPoint.BOFNQ#' TGUIDE='#controlPoint.TGUIDE#' #OPT_NUM_XML# #OPT_TYPE_XML# #ANSTEMPXML# #APIXML# #CDFXML# #OPTINXML# >#RTEXTXML##OPTIONSXML##CONDITIONSXML##ESOPTIONSXML# #Chr(13)##Chr(10)##chr(9)#</Q>" />

           	<!--- Undo for Rendering response - UnicodeCP ReadCP Formatting --->

			<cfif arguments.controlPoint.T64 EQ 1>
				<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
				<cfset arguments.controlPoint.TEXT = ToString( ToBinary( arguments.controlPoint.TEXT ), "UTF-8" ) />
			</cfif>

           	<cfset dataout.DebugUni7 = inpXML>

           	<cfset debugInpXML = Replace(inpXML, "&amp;","&","ALL") />
           	<cfset dataout.DebugUni8 = debugInpXML>

			<cfset Counter = 1 />

			<cfset NewRXSSCSC = "" />
			<cfset NewRXSSCSC &= "<RXSSCSC>#Chr(13)##Chr(10)#" />

			<cfset Qs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC/Q") />


			<cfloop array="#Qs#" index="QItem">

				<!--- Replace with new XML if this is the one that has been edited --->
				<cfif POSITION EQ Counter>
					<cfset NewRXSSCSC &= "#inpXML##Chr(13)##Chr(10)#"/>

				<cfelse>

					<!--- Clean up XML for DB storage --->
					<cfset OutToDBXMLBuff = ToString(QItem, "UTF-8") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset NewRXSSCSC &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/>

				</cfif>

				<cfset Counter = Counter + 1 />

			</cfloop>


			<cfset NewRXSSCSC &= "</RXSSCSC>"/>



			<!--- Parse new XML --->
			<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #NewRXSSCSC# & "</XMLControlStringDoc>") />

			 <!--- make sure to update all RQ POSITIONs --->
		    <cfset FinalDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

        	<!--- Clean up XML for DB storage --->
			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
			<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
			<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

		    <cfset dataout.DebugUni9 = OutToDBXMLBuff>

			<cfif inpTemplateFlag EQ 1>

	           <!--- Write to DB Batch Options --->
	           <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.templatesxml
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
				</cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Write to DB Batch Options --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.batch
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>

			</cfif>

            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.BATCHID = INPBATCHID />
			<cfset dataout.PID = CurrPID />
			<cfset dataout.ERRMESSAGE = DebugStr />

			<!--- Store all changes in history table - this allows user to undo as well as protects us from someone sending bad message and then changing it and claiming wrong message sent --->
            <cfinvoke method="AddHistory" component="session.sire.models.cfc.history">
					<cfinvokeargument name="inpBatchId" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
					<cfinvokeargument name="EVENT" value="Add Control Point PID = #CurrPID#">
			</cfinvoke>

            <!--- What has this user been up to - good metrics on who is using Sire for what - also good for security tracking  --->
            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS BatchId = #INPBATCHID#">
				<cfinvokeargument name="operator" value="Add Control Point PID = #CurrPID#">
			</cfinvoke>

			<cfset subcriberArray = []>
			<cfset _options = []>
			<cfset _conditions = []>

			<cfinvoke method="RenderCP" returnvariable="RetVarRenderCP">
				<cfinvokeargument name="controlPoint" value="#controlPoint#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#">
				<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
			</cfinvoke>

<!--- 			<cfset dataout.DEBUGSTUFF = "DEBUGSTUFF=" & SerialIzeJSON(RetVarRenderCP) /> --->

			<cfset dataout.HTML = RetVarRenderCP.HTML />


			<!--- Decode goofy XML special character as they are stored in question strings so they can redisplay via HTML via JSON return structure --->
			<cfif Find("&", dataout.HTML) GT 0>
				<cfset dataout.HTML = Replace(dataout.HTML, "&amp;","&","ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&gt;",">","ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&lt;","<","ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&apos;","'","ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&quot;",'"',"ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&##63;","?","ALL") />
                <cfset dataout.HTML = Replace(dataout.HTML, "&amp;","&","ALL") />
            </cfif>


			<cfset dataout.controlPoint = controlPoint />

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#" >

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadXMLQuestions" access="remote" output="false" hint="Get List of Questions for given BatchID.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpDefaultOption" TYPE="string" required="false" default="Default to next Control Point" hint="Set the default option in SELECT box"  />
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />
		<cfargument name="inpTypeFilter" TYPE="string" required="false" hint="Optional TYPE of question filter" default="" />


		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "#INPBATCHID#" />
		<cfset dataout.QUESTIONS = []>
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cfset var inpRXSSEMLocalBuff	= '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var TITLE	= '' />
		<cfset var LSTQIDBOTH	= '' />
		<cfset var selectedElements	= '' />
		<cfset var previousQuestions	= '' />
		<cfset var arrQuestion	= '' />
		<cfset var questionObj	= '' />
		<cfset var selectedElementsII	= '' />
		<cfset var listQuestion	= '' />
		<cfset var GetBatchQuestion	= '' />
		<cfset var TypeFilterString = '' />
		<cfset var CurrentFilter = '' />

		<cftry>

        	<!--- Set default to 0 for just move to next CP --->
        	<cfset arrQuestion = ArrayNew(1) />

        	<cfif TRIM(inpDefaultOption) NEQ "" >
	        	<cfset questionObj = StructNew() />
				<cfset questionObj.ID = "0">
				<cfset questionObj.RQ = "0">
				<cfset questionObj.TEXT = "#inpDefaultOption#">
				<cfset questionObj.TDESC = "#inpDefaultOption#">
				<cfset questionObj.TYPE = "0">

				<cfset ArrayAppend(arrQuestion, questionObj) />
        	</cfif>

        	<!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS Desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>

            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset TITLE = GetBatchQuestion.desc_vch>

			<!--- read all Q questions inside RXSSTXT tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />

			<!---<cfset previousQuestions = XmlSearch(myxmldocResultDoc, "//Q[ @GID<'#GROUPID#']") />--->
			<!---<cfset previousQuestions = (myxmldocResultDoc, "//Q[ @TYPE='#ONESELECTION#']") />--->

			<cfif TRIM(inpTypeFilter) NEQ "">
				<cfset TypeFilterString = "[" />

				<cfloop list="#TRIM(inpTypeFilter)#" index="CurrentFilter">

					<!--- Check if first time in loop - just check TypeFilterString instead of a dedicated loop conter variable--->
					<cfif TypeFilterString EQ "[">
						<cfset TypeFilterString = TypeFilterString & "@TYPE='#TRIM(CurrentFilter)#'" />
					<cfelse>
						<cfset TypeFilterString = TypeFilterString & " or @TYPE='#TRIM(CurrentFilter)#'" />
					</cfif>

				</cfloop>

				<cfset TypeFilterString = TypeFilterString & "]" />

				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q#TypeFilterString#") />

			<cfelse>
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
			</cfif>


			<cfloop array="#selectedElements#" index="listQuestion">

				<cfset questionObj = StructNew() />

				<!--- Get text and type of question --->

                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@ID")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.ID = "0">
                </cfif>

                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@RQ")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.RQ = "0">
                </cfif>

                <!--- T64 Flag for is Base64 Encoded Text --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@T64")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.T64 = "0">
                </cfif>

                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TEXT")>

                 <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                    <!--- UnicodeCP ReadCP Formatting --->
					<cfif questionObj.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset questionObj.TEXT = ToString( ToBinary( questionObj.TEXT ), "UTF-8" ) />
					</cfif>

                    <!--- Allow newline in text messages - reformat for display --->
					<cfset questionObj.TEXT = Replace(questionObj.TEXT, "\n", "#chr(10)#", "ALL")>

                <cfelse>
                    <cfset questionObj.TEXT = "">
                </cfif>

                <!--- TEMPLATE_DESC --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TDESC")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.TDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.TDESC = "">
                </cfif>

                 <!--- Walk Through - Flag to enable edit of this control point during simplified walk through--->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@SWT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.SWT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.SWT = "1">
                </cfif>

                <!--- type --->
				<cfset selectedElementsII = XmlSearch(listQuestion, "@TYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset questionObj.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset questionObj.TYPE = "0">
                </cfif>

				<!--- This is so if API is in the list it will show up so it can be selected --->
                <cfif questionObj.TYPE EQ "API">
	                <cfif TRIM(questionObj.TEXT) EQ "">

	                	<cfif questionObj.TDESC NEQ "">
		                	<cfset questionObj.TEXT = "API (#questionObj.TDESC#)">
		                <cfelse>
	                		<cfset questionObj.TEXT = "API (#questionObj.RQ#)">
	                	</cfif>

	                </cfif>
                </cfif>

               <!--- This is so select boxes can pick this control point from the list instead of it being blank --->
                <cfif questionObj.TYPE EQ "OPTIN">

	                <cfif questionObj.TEXT EQ "">
		                <cfset questionObj.TEXT = "###questionObj.RQ# Capture Opt In" />
	                </cfif>

                </cfif>

                <!--- Output the whole question as a single object--->
				<cfset ArrayAppend(arrQuestion, questionObj) />

			</cfloop>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.QUESTIONS = arrQuestion>

			<cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">


			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadXMLQuestionAnswers" access="remote" output="false" hint="Get List of Answers for the single given Question for given BatchID.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpQID" TYPE="string" required="yes" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "#INPBATCHID#" />
		<cfset dataout.QUESTIONS = []>
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cfset var inpRXSSEMLocalBuff	= '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var TITLE	= '' />
		<cfset var LSTQIDBOTH	= '' />
		<cfset var selectedElementsII	= '' />
		<cfset var GetBatchQuestion	= '' />
		<cfset var CPObj = StructNew() />
		<cfset var SelectedCP	= '' />
		<cfset var arrAnswer	= '' />
		<cfset var selectedAnswer	= '' />
		<cfset var answer	= '' />
		<cfset var AttributeBuff	= '' />
		<cfset var ans	= '' />

		<cfset CPObj.OPTIONS = StructNew() />

		<cftry>

        	<!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

			<cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS Desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>

            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset TITLE = GetBatchQuestion.desc_vch>

			<!--- read the Q question inside RXSSCSC tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />

			<cfif ArrayLen(SelectedCP) GT 0>

				<cfset CPObj = StructNew() />
				<cfset CPObj.OPTIONS = StructNew() />

				<!--- Get text and type of question --->

                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@ID")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>

                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@RQ")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- T64 Flag for is Base64 Encoded Text --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@T64")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.T64 = "0">
                </cfif>

                <!--- errMsgTxt --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TEXT")>

                 <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                    <!--- UnicodeCP ReadCP Formatting --->
					<cfif CPObj.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset CPObj.TEXT = ToString( ToBinary( CPObj.TEXT ), "UTF-8" ) />
					</cfif>

                    <!--- Allow newline in text messages - reformat for display --->
					<cfset CPObj.TEXT = Replace(CPObj.TEXT, "\n", "#chr(10)#", "ALL")>

                <cfelse>
                    <cfset CPObj.TEXT = "">
	            </cfif>

                <!--- TEMPLATE_DESC --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TDESC")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.TDESC = "">
                </cfif>

                 <!--- Walk Through - Flag to enable edit of this control point during siplified walk through--->
				<cfset AttributeBuff = XmlSearch(SelectedCP[1], "@SWT")>

                <cfif ArrayLen(AttributeBuff) GT 0>
                    <cfset CPObj.SWT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
                <cfelse>
                    <cfset CPObj.SWT = "1">
                </cfif>

                <!--- type --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.TYPE = "0">
                </cfif>

	            <!--- Get answers --->
	            <cfset arrAnswer = ArrayNew(1) />
	            <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./OPTION") />

	            <cfloop array="#selectedAnswer#" index="ans">

	                <cfset answer = StructNew() />

	                <!--- id --->
					<cfset AttributeBuff = XmlSearch(ans, "@ID")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.id = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.id = "0">
	                </cfif>

	                <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

                    <!--- errMsgTxt --->
					<cfset AttributeBuff = XmlSearch(ans, "@TEXT")>

                     <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.text = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.text = ToString( ToBinary( answer.text ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.text = Replace(answer.text, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.text = "">
	                </cfif>

	                <!--- Assigned Answer Value --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVAL")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.AVAL = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.AVAL = "">
	                </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@AVALREG")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.AVALREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.AVALREG = "">
	                </cfif>

				 <!--- Default response statement --->
 				<cfset AttributeBuff = XmlSearch(ans, "@AVALRESP")>

 				<cfif ArrayLen(AttributeBuff) GT 0>
 				    <cfset answer.AVALRESP = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

				    <!--- UnicodeCP ReadCP Formatting --->
				   	 <cfif answer.T64 EQ 1>
				   		 <!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
				   		 <cfset answer.AVALRESP = ToString( ToBinary( answer.AVALRESP ), "UTF-8" ) />
				   	 </cfif>

				    <!--- Allow newline in text messages - reformat for display --->
				   	 <cfset answer.AVALRESP = Replace(answer.AVALRESP, "\n", "#chr(10)#", "ALL")>


				<cfelse>
 				    <cfset answer.AVALRESP = "">
 				</cfif>

 				<!--- Redirect to another flow --->
 				<cfset AttributeBuff = XmlSearch(ans, "@AVALREDIR")>

 				<cfif ArrayLen(AttributeBuff) GT 0>
 				    <cfset answer.AVALREDIR = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
 				<cfelse>
 				    <cfset answer.AVALREDIR = "">
 				</cfif>

				<!--- Where to go next --->
				<cfset AttributeBuff = XmlSearch(ans, "@AVALNEXT")>

				<cfif ArrayLen(AttributeBuff) GT 0>
				    <cfset answer.AVALNEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
				<cfelse>
				    <cfset answer.AVALNEXT = "0">
				</cfif>

	                <cfset ArrayAppend(arrAnswer, answer) />
	            </cfloop>

	            <cfset CPObj.OPTIONS = arrAnswer />

			</cfif>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CPOBJ = CPOBJ>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">


			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="ReadXMLExpertSystemOptions" access="remote" output="false" hint="Get List of Options for the single given Expert system Control Point for given BatchID.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpQID" TYPE="string" required="yes" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>
        <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "#INPBATCHID#" />
		<cfset dataout.QUESTIONS = []>
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cfset var inpRXSSEMLocalBuff	= '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var TITLE	= '' />
		<cfset var LSTQIDBOTH	= '' />
		<cfset var selectedElementsII	= '' />
		<cfset var GetBatchQuestion	= '' />
		<cfset var CPObj = StructNew() />
		<cfset var SelectedCP	= '' />
		<cfset var arrAnswer	= '' />
		<cfset var selectedAnswer	= '' />
		<cfset var answer	= '' />
		<cfset var AttributeBuff	= '' />
		<cfset var ans	= '' />

		<cfset CPObj.ESOPTIONS = StructNew() />

		<cftry>

        	<!--- Validate Batch Id--->
			<cfif !isnumeric(INPBATCHID) >
            	<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

			<cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS Desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->

				<!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                desc_vch,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>

            <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

			<cfset TITLE = GetBatchQuestion.desc_vch>

			<!--- read the Q question inside RXSSCSC tag --->
			<cfset LSTQIDBOTH = ArrayNew(1) />
			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @#inpIDKey# = #inpQID#]") />

			<cfif ArrayLen(SelectedCP) GT 0>

				<cfset CPObj = StructNew() />
				<cfset CPObj.ESOPTIONS = StructNew() />

				<!--- Get text and type of question --->

                <!--- ID --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@ID")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.ID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.ID = "0">
                </cfif>

                <!--- RQ - the order of the question --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@RQ")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.RQ = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.RQ = "0">
                </cfif>

                <!--- T64 Flag for is Base64 Encoded Text --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@T64")>
                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.T64 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.T64 = "0">
                </cfif>

                <!--- Text --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TEXT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TEXT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>

                    <!--- UnicodeCP ReadCP Formatting --->
					<cfif CPObj.T64 EQ 1>
						<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
						<cfset CPObj.TEXT = ToString( ToBinary( CPObj.TEXT ), "UTF-8" ) />
					</cfif>

                    <!--- Allow newline in text messages - reformat for display --->
					<cfset CPObj.TEXT = Replace(CPObj.TEXT, "\n", "#chr(10)#", "ALL")>

                <cfelse>
                    <cfset CPObj.TEXT = "">
                </cfif>

                <!--- TEMPLATE_DESC --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TDESC")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TDESC = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.TDESC = "">
                </cfif>

                 <!--- Walk Through - Flag to enable edit of this control point during siplified walk through--->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@SWT")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.SWT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.SWT = "1">
                </cfif>

                <!--- type --->
				<cfset selectedElementsII = XmlSearch(SelectedCP[1], "@TYPE")>

                <cfif ArrayLen(selectedElementsII) GT 0>
                    <cfset CPObj.TYPE = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                <cfelse>
                    <cfset CPObj.TYPE = "0">
                </cfif>

	            <!--- Get answers --->
	            <cfset arrAnswer = ArrayNew(1) />
	            <cfset selectedAnswer = XmlSearch(SelectedCP[1], "./ESOPTION") />

	            <cfloop array="#selectedAnswer#" index="ans">

	                <cfset answer = StructNew() />

	                <!--- id --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESID")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.ESID = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.ESID = "0">
	                </cfif>

	                <!--- T64 Flag for is Base64 Encoded Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@T64")>
	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.T64 = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.T64 = "0">
	                </cfif>

	                <!--- Text --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESTEXT")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.ESTEXT = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>

	                    <!--- UnicodeCP ReadCP Formatting --->
						<cfif answer.T64 EQ 1>
							<!--- All CP Main Text may now be stored in base64 format so unicode is stored in DB properly - T64 EQ 1 --->
							<cfset answer.ESTEXT = ToString( ToBinary( answer.ESTEXT ), "UTF-8" ) />
						</cfif>

	                    <!--- Allow newline in text messages - reformat for display --->
						<cfset answer.ESTEXT = Replace(answer.ESTEXT, "\n", "#chr(10)#", "ALL")>

	                <cfelse>
	                    <cfset answer.ESTEXT = "">
	                </cfif>

					<!--- Assigned Answer Value - Regular Expression Matcher --->
					<cfset AttributeBuff = XmlSearch(ans, "@ESREG")>

	                <cfif ArrayLen(AttributeBuff) GT 0>
	                    <cfset answer.ESREG = AttributeBuff[ArrayLen(AttributeBuff)].XmlValue>
	                <cfelse>
	                    <cfset answer.ESREG = "">
	                </cfif>

	                <cfset ArrayAppend(arrAnswer, answer) />
	            </cfloop>

	            <cfset CPObj.ESOPTIONS = arrAnswer />

			</cfif>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CPOBJ = CPOBJ>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">


			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="CheckKeywordAvailability" access="remote" output="false" hint="Check if Keyword is available for use on specified shortcode.">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = '' />


       	<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.EXISTSFLAG = 1 />

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

        	<!--- Get Keyword information --->
            <cfquery name="GetKeywordCount" datasource="#Session.DBSourceEBM#">
	                SELECT
                        COUNT(k.Keyword_vch) AS TOTALCOUNT
                    FROM
                        sms.keyword AS k
                    JOIN
                        sms.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        sms.shortcode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE
                        k.Active_int = 1
                    AND
                    	( k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeyword)#"> OR k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeywordStripped)#"> )
                    AND
                        sc.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                    	k.BatchId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">
            </cfquery>

            <cfif GetKeywordCount.TOTALCOUNT GT 0>

              	<!--- Enhancement: Look up user information on who has this Keyword reserved --->

       			<cfset dataout.EXISTSFLAG = 1 />

            <cfelse>

              	<cfset dataout.EXISTSFLAG = 0 />

            </cfif>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset dataout.MESSAGE = "Invalid Keyword"/>
			<cfset dataout.ERRMESSAGE = "Invalid Keyword"/>


			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="SaveKeyword" access="remote" output="false" hint="Save Keyword if Keyword is available for use on specified shortcode.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" required="false" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = arguments.inpKeyword />
		<cfset var inpKeywordquotes = ''/>
		<cfset var checkKeywordquotes = ''/>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var Pattern = '' />
		<cfset var Matcher = '' />
		<cfset var CSVInvalidChar = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetShortAccessData = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarGetKeywordId = ''/>
		<cfset var RetVarAddKeyword = ''/>
		<cfset var RetVarUpdateKeyword	= '' />

       	<cfset dataout.RXRESULTCODE = 0 />
       	<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->

   			<!--- <cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

			<cfset inpKeywordquotes = Replace(TRIM(arguments.inpKeyword), '"', ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "'", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), " ", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "-", ",@@@,", "All") />

			<cfset checkKeywordquotes = ListFind(inpKeywordquotes, '@@@')>

			<cfif checkKeywordquotes gt 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "This Keyword contains not allow spaces, dashes, or quotes."/>

				<cfreturn dataout>
			</cfif> --->

			<!--- Optional ISO-8859-1 (Latin-1) Char validation --->
			<!--- <cfif inpISOLatin EQ 1 >
	   			<!--- Validate characters are within - ISO-8859-1 (Latin-1) --->
	   			<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
	   			<cfset Matcher = Pattern.Matcher( JavaCast( "string", inpKeywordStripped ) ) />

	   			<!--- loop through the matches --->
				<cfloop condition="matcher.find()">
					<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
				</cfloop>

				<!--- If any are found return with error message  --->
				<cfif LEN(CSVInvalidChar) GT 0>

					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "This Keyword contains the following comma seperated list of invalid characters (#CSVInvalidChar#)"/>

		   			<cfreturn dataout>

				</cfif>

			</cfif> --->

			<!--- Validate current session owner owns Batch Id --->
   			<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

	   			<cfreturn dataout>
   			</cfif>

   			<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

   			<!--- Return error message if it exists --->
			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

   			<!--- Validate user keyword length limits from DB --->
   			<cfinvoke method="GetCurrentUserPlanKeywordLimits" returnvariable="RetVarGetCurrentUserPlanKeywordLimits"></cfinvoke>

   			<cfif LEN(inpKeywordStripped) LT RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH >

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword (without spaces) is not long enough. Your current account requires #RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH# number of characters"/>

	   			<cfreturn dataout>

   			</cfif>

   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>

   			</cfif>

   			<!--- Check if this Batch already has a keyword set  - update if it does -- add a new one if it does not--->
   			<cfinvoke method="GetKeywordId" returnvariable="RetVarGetKeywordId">
	   			<cfinvokeargument name="inpShortCodeRequestId" value="#RetVarGetShortAccessData.SHORCODEREQUESTID#">
	   			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	   		</cfinvoke>

	   		<cfif RetVarGetKeywordId.KEYWORDID EQ 0>
	   		<!--- Add keyword --->

		   		<!--- Validate user does not have too many keywords --->
	   			<cfinvoke method="GetKeywordsCountInUse" returnvariable="RetVarGetKeywordsCountInUse"></cfinvoke>

	   			<cfif RetVarGetKeywordsCountInUse.KEYWORDSINUSE GTE RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT >

		   			<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "You are using more keywords (#RetVarGetKeywordsCountInUse.KEYWORDSINUSE#) than your current plan allows (#RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT#).  "/>

		   			<cfreturn dataout>

	   			</cfif>

	   			<cfinvoke method="AddKeyword" returnvariable="RetVarAddKeyword">
		   			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
		   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
		   			<cfinvokeargument name="inpShortCodeRequestId" value="#RetVarGetShortAccessData.SHORCODEREQUESTID#">
		   			<cfinvokeargument name="inpShortCodeId" value="#RetVarGetShortAccessData.SHORCODEID#">
		   			<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#">
		   			<cfinvokeargument name="inpDefaultFlag" value="0">
		   		</cfinvoke>

		   		<cfif RetVarAddKeyword.KEYWORDID EQ 0 OR RetVarAddKeyword.RXRESULTCODE NEQ 1>

			   		<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "Problem adding keyword. (#RetVarAddKeyword.RXRESULTCODE#) #RetVarAddKeyword.MESSAGE# #RetVarAddKeyword.MESSAGE#  "/>

		   			<cfreturn dataout>

		   		</cfif>

	   			<cfset dataout.KEYWORDID = RetVarAddKeyword.KEYWORDID />


	   		<cfelse>
	   		<!--- Update existing - does not check if keyword limit reach to allow update of existing --->

				<cfinvoke method="UpdateKeyword" returnvariable="RetVarUpdateKeyword">
		   			<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#">
		   			<cfinvokeargument name="inpKeywordId" value="#RetVarGetKeywordId.KEYWORDID#">
		   			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
		   		</cfinvoke>

		   		<cfif RetVarUpdateKeyword.RXRESULTCODE NEQ 1>

			   		<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "Problem updating keyword. (#RetVarUpdateKeyword.RXRESULTCODE#) #RetVarUpdateKeyword.MESSAGE# #RetVarUpdateKeyword.MESSAGE#  "/>

		   			<cfreturn dataout>

		   		</cfif>

	   			<cfset dataout.KEYWORDID = RetVarGetKeywordId.KEYWORDID />

   			</cfif>

   			<!--- What has this user been up to - good metrics on who is using Sire for what - also good for security tracking  --->
            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS">
				<cfinvokeargument name="operator" value="Set Keyword = #inpKeyword# BatchId = #INPBATCHID# inpShortCode = #inpShortCode#">
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

	    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="ValidateKeyword" access="remote" output="false" hint="Validate Keyword if Keyword is available for use on specified shortcode.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = arguments.inpKeyword />
		<cfset var inpKeywordquotes = ''/>
		<cfset var checkKeywordquotes = ''/>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var Pattern = '' />
		<cfset var Matcher = '' />
		<cfset var CSVInvalidChar = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetShortAccessData = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarGetKeywordId = ''/>
		<cfset var RetVarAddKeyword = ''/>
		<cfset var RetVarUpdateKeyword	= '' />

       	<cfset dataout.RXRESULTCODE = 0 />
       	<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<!--- <cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

			<cfset inpKeywordquotes = Replace(TRIM(arguments.inpKeyword), '"', ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "'", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), " ", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "-", ",@@@,", "All") />

			<cfset checkKeywordquotes = ListFind(inpKeywordquotes, '@@@')>

			<cfif checkKeywordquotes GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = inpKeywordquotes/>
				<cfset dataout.ERRMESSAGE = "This Keyword contains not allow spaces, dashes, or quotes."/>
				<cfreturn dataout>
			</cfif> --->

			<!--- Optional ISO-8859-1 (Latin-1) Char validation --->
			<!--- <cfif inpISOLatin EQ 1 >
	   			<!--- Validate characters are within - ISO-8859-1 (Latin-1) --->
	   			<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
	   			<cfset Matcher = Pattern.Matcher( JavaCast( "string", inpKeywordStripped ) ) />

	   			<!--- loop through the matches --->
				<cfloop condition="matcher.find()">
					<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
				</cfloop>

				<!--- If any are found return with error message  --->
				<cfif LEN(CSVInvalidChar) GT 0>

					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "This Keyword contains the following comma seperated list of invalid characters (#CSVInvalidChar#)"/>

		   			<cfreturn dataout>

				</cfif>

			</cfif> --->

			<!--- Validate current session owner owns Batch Id --->
			<cfif inpBatchId GT 0>
				<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				</cfinvoke>

	            <!--- Return error message if user Ids do not match --->
	   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

		   			<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

		   			<cfreturn dataout>
	   			</cfif>
			</cfif>

   			<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

   			<!--- Return error message if it exists --->

   			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

   			<!--- Validate user keyword length limits from DB --->
   			<cfinvoke method="GetCurrentUserPlanKeywordLimits" returnvariable="RetVarGetCurrentUserPlanKeywordLimits"></cfinvoke>

   			<cfif LEN(inpKeywordStripped) LT RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH >

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is not long enough. Your current account requires #RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH# number of characters"/>

	   			<cfreturn dataout>

   			</cfif>

   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>

   			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Keyword is available."/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

	    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>
	<cffunction name="ValidateKeywordNew" access="remote" output="false" hint="Validate Keyword if Keyword is available for use on specified shortcode.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = arguments.inpKeyword />
		<cfset var inpKeywordquotes = ''/>
		<cfset var checkKeywordquotes = ''/>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var Pattern = '' />
		<cfset var Matcher = '' />
		<cfset var CSVInvalidChar = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetShortAccessData = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarGetKeywordId = ''/>
		<cfset var RetVarAddKeyword = ''/>
		<cfset var RetVarUpdateKeyword	= '' />

       	<cfset dataout.RXRESULTCODE = 0 />
       	<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<!--- <cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

			<cfset inpKeywordquotes = Replace(TRIM(arguments.inpKeyword), '"', ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "'", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), " ", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "-", ",@@@,", "All") />

			<cfset checkKeywordquotes = ListFind(inpKeywordquotes, '@@@')>

			<cfif checkKeywordquotes GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = inpKeywordquotes/>
				<cfset dataout.ERRMESSAGE = "This Keyword contains not allow spaces, dashes, or quotes."/>
				<cfreturn dataout>
			</cfif> --->

			<!--- Optional ISO-8859-1 (Latin-1) Char validation --->
			<!--- <cfif inpISOLatin EQ 1 >
	   			<!--- Validate characters are within - ISO-8859-1 (Latin-1) --->
	   			<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
	   			<cfset Matcher = Pattern.Matcher( JavaCast( "string", inpKeywordStripped ) ) />

	   			<!--- loop through the matches --->
				<cfloop condition="matcher.find()">
					<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
				</cfloop>

				<!--- If any are found return with error message  --->
				<cfif LEN(CSVInvalidChar) GT 0>

					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "This Keyword contains the following comma seperated list of invalid characters (#CSVInvalidChar#)"/>

		   			<cfreturn dataout>

				</cfif>

			</cfif> --->

			<!--- Validate current session owner owns Batch Id --->
			<cfif inpBatchId GT 0>
				<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				</cfinvoke>

	            <!--- Return error message if user Ids do not match --->
	   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

		   			<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

		   			<cfreturn dataout>
	   			</cfif>
			</cfif>

   			<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>
   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

   			<!--- Validate user keyword length limits from DB --->
   			<cfinvoke method="GetCurrentUserPlanKeywordLimits" returnvariable="RetVarGetCurrentUserPlanKeywordLimits"></cfinvoke>

   			<cfif LEN(inpKeywordStripped) LT RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH >

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is not long enough. Your current account requires #RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH# number of characters"/>

	   			<cfreturn dataout>

   			</cfif>
   			<!--- --->
			<cfinvoke method="GetKeywordsCountInUse" returnvariable="RetVarGetKeywordsCountInUse"></cfinvoke>

			<cfif RetVarGetKeywordsCountInUse.KEYWORDSINUSE GTE RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT >

				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "You are using more keywords (#RetVarGetKeywordsCountInUse.KEYWORDSINUSE#) than your current plan allows (#RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT#).  "/>

				<cfreturn dataout>

			</cfif>
   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>

   			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Keyword is available."/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

	    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="ValidateKeywordWithoutBatchId" access="remote" output="false" hint="Validate Keyword if Keyword is available for use on specified shortcode.">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = arguments.inpKeyword />
		<cfset var inpKeywordquotes = ''/>
		<cfset var checkKeywordquotes = ''/>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var Pattern = '' />
		<cfset var Matcher = '' />
		<cfset var CSVInvalidChar = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetShortAccessData = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarGetKeywordId = ''/>
		<cfset var RetVarAddKeyword = ''/>
		<cfset var RetVarUpdateKeyword	= '' />

       	<cfset dataout.RXRESULTCODE = 0 />
       	<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<!--- <cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

			<cfset inpKeywordquotes = Replace(TRIM(arguments.inpKeyword), '"', ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "'", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), " ", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "-", ",@@@,", "All") />

			<cfset checkKeywordquotes = ListFind(inpKeywordquotes, '@@@')>

			<cfif checkKeywordquotes gt 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "This Keyword contains not allow spaces, dashes, or quotes."/>

				<cfreturn dataout>
			</cfif> --->

			<!--- Optional ISO-8859-1 (Latin-1) Char validation --->
			<!--- <cfif inpISOLatin EQ 1 >
	   			<!--- Validate characters are within - ISO-8859-1 (Latin-1) --->
	   			<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
	   			<cfset Matcher = Pattern.Matcher( JavaCast( "string", inpKeywordStripped ) ) />

	   			<!--- loop through the matches --->
				<cfloop condition="matcher.find()">
					<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
				</cfloop>

				<!--- If any are found return with error message  --->
				<cfif LEN(CSVInvalidChar) GT 0>

					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "This Keyword contains the following comma seperated list of invalid characters (#CSVInvalidChar#)"/>

		   			<cfreturn dataout>

				</cfif>

			</cfif> --->

			<!--- Validate current session owner owns Batch Id --->
   			<!--- <cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

	   			<cfreturn dataout>
   			</cfif>	 --->

   			<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				<cfinvokeargument name="inpBatchId" value="0"/>
			</cfinvoke>

   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

   			<!--- Validate user keyword length limits from DB --->
   			<cfinvoke method="GetCurrentUserPlanKeywordLimits" returnvariable="RetVarGetCurrentUserPlanKeywordLimits"></cfinvoke>

   			<cfif LEN(inpKeywordStripped) LT RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH >

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is not long enough. Your current account requires #RetVarGetCurrentUserPlanKeywordLimits.KEYWORDMINLENGTH# number of characters"/>

	   			<cfreturn dataout>

   			</cfif>

   			<cfinvoke method="GetKeywordsCountInUse" returnvariable="RetVarGetKeywordsCountInUse"></cfinvoke>

   			<cfif RetVarGetKeywordsCountInUse.KEYWORDSINUSE GTE RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT >

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "You are using more keywords (#RetVarGetKeywordsCountInUse.KEYWORDSINUSE#) than your current plan allows (#RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT#).  "/>

	   			<cfreturn dataout>

   			</cfif>

   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>

   			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Keyword is available."/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

	    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<!--- This is only concerned with attempts to add new Keywords - this is not to cleanup or turn off an old keywords  --->
	<cffunction name="GetCurrentUserPlanKeywordLimits" access="public" output="false" hint="Get current user plan Keyword limits. Will default to free plan limits if account is expired.">

        <cfset var dataout = {} />
        <cfset var GetUserPlan	= '' />
        <cfset var GetFreePlan	= '' />
        <cfset var DefaultToFreePlan = 0 />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDLIMIT = "0"/>
		<cfset dataout.KEYWORDMINLENGTH = "8"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetUserPlan" datasource="#Session.DBSourceEBM#">
	            SELECT
	            	u.UserPlanId_bi,
	            	u.Status_int,
	            	u.PlanId_int,
	            	u.StartDate_dt,
	            	u.EndDate_dt,
	            	DATEDIFF(u.EndDate_dt,CURDATE()) AS DaysUntilExpires,
	            	p.PlanName_vch,
	            	p.Amount_dec,
	            	p.UserAccountNumber_int,
	            	p.Order_int,
	            	u.KeywordsLimitNumber_int,
	                p.KeywordsLimitNumber_int as PlanKeywordsLimitNumber_int,
	                u.KeywordMinCharLimit_int,
	                p.KeywordMinCharLimit_int as PlanKeywordMinCharLimit_int,
	            	p.FirstSMSIncluded_int,
	            	u.PriceMsgAfter_dec,
	            	u.PriceKeywordAfter_dec,
	            	u.BuyKeywordNumber_int,
	                u.BuyKeywordNumberExpired_int,
	                u.DowngradeDate_dt,
	                u.PromotionKeywordsLimitNumber_int
	            FROM
	            	simplebilling.userplans u INNER JOIN simplebilling.plans p ON u.PlanId_int = p.PlanId_int
	            WHERE
	                u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            AND
	               	u.Status_int = 1
	            ORDER BY UserPlanId_bi DESC
	            LIMIT 1
	        </cfquery>

	        <!--- 10.04.2018 New code bellow --->
	        <cfif GetUserPlan.RecordCount GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
					<!--- Keyword Count Limits - Allow user plan override of default values --->
					<cfif GetUserPlan.KeywordsLimitNumber_int GT 0>
						<cfset dataout.KEYWORDLIMIT = "#GetUserPlan.KeywordsLimitNumber_int + GetUserPlan.BuyKeywordNumber_int + GetUserPlan.PromotionKeywordsLimitNumber_int#"/>
					<cfelse>
						<!--- Default to Plan limits --->
						<cfset dataout.KEYWORDLIMIT = "#GetUserPlan.PlanKeywordsLimitNumber_int + GetUserPlan.BuyKeywordNumber_int + GetUserPlan.PromotionKeywordsLimitNumber_int#"/>
					</cfif>

					<!--- Leyword length limits - Allow user plan override of default values --->
					<cfif GetUserPlan.KeywordMinCharLimit_int GT 0>
						<cfset dataout.KEYWORDMINLENGTH = "#GetUserPlan.KeywordMinCharLimit_int#"/>
					<cfelse>
						<!--- Default to Plan limits --->
						<cfset dataout.KEYWORDMINLENGTH = "#GetUserPlan.PlanKeywordMinCharLimit_int#"/>
				</cfif>
	        </cfif>
	        <!--- 10.04.2018 Old code bellow --->

	        <!--- If there is an active plan (Status_int = 1) for this Session.USERID .... --->
	       <!---  <cfif GetUserPlan.RecordCount GT 0>

		        <!--- Check if plan has expired - anyhting less than 0 is past expireation date - no grace period --->
		        <cfif GetUserPlan.DaysUntilExpires LT 0 >

			        <cfset DefaultToFreePlan = 1 />

			    <cfelse>

			        <cfset dataout.RXRESULTCODE = 1 />

					<!--- Keyword Count Limits - Allow user plan override of default values --->
					<cfif GetUserPlan.KeywordsLimitNumber_int GT 0>
						<cfset dataout.KEYWORDLIMIT = "#GetUserPlan.KeywordsLimitNumber_int + GetUserPlan.BuyKeywordNumber_int + GetUserPlan.PromotionKeywordsLimitNumber_int#"/>
					<cfelse>
						<!--- Default to Plan limits --->
						<cfset dataout.KEYWORDLIMIT = "#GetUserPlan.PlanKeywordsLimitNumber_int + GetUserPlan.BuyKeywordNumber_int + GetUserPlan.PromotionKeywordsLimitNumber_int#"/>
					</cfif>

					<!--- Leyword length limits - Allow user plan override of default values --->
					<cfif GetUserPlan.KeywordMinCharLimit_int GT 0>
						<cfset dataout.KEYWORDMINLENGTH = "#GetUserPlan.KeywordMinCharLimit_int#"/>
					<cfelse>
						<!--- Default to Plan limits --->
						<cfset dataout.KEYWORDMINLENGTH = "#GetUserPlan.PlanKeywordMinCharLimit_int#"/>
					</cfif>

					<cfset DefaultToFreePlan = 0 />

		        </cfif>

		    <cfelse>
		    <!--- Default to free plan limits - assumes plan 1 is always the free plan --->

			    <cfset DefaultToFreePlan = 1 />

		    </cfif>

	        <!--- Look up free plan limits if no active user plan limits available --->
	        <cfif DefaultToFreePlan EQ 1>

		         <cfquery name="GetFreePlan" datasource="#Session.DBSourceEBM#">
		            SELECT
		            	p.PlanId_int,
		            	p.PlanName_vch,
		            	p.Status_int,
		            	p.Amount_dec,
		            	p.UserAccountNumber_int,
		            	p.KeywordsLimitNumber_int,
		            	p.KeywordMinCharLimit_int,
		            	p.FirstSMSIncluded_int,
		            	p.CreditsAddAmount_int,
		            	p.PriceMsgAfter_dec,
		            	p.PriceKeywordAfter_dec,
		            	p.Order_int
		            FROM
		            	simplebilling.plans p
		            WHERE
		            	PlanId_int = 1

	        	</cfquery>

				<!--- Somthing seriously wrong if this fails but better safe than sorry --->
				<cfif GetFreePlan.RecordCount GT 0>

					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.KEYWORDLIMIT = "#GetFreePlan.KeywordsLimitNumber_int#"/>
					<cfset dataout.KEYWORDMINLENGTH = "#GetFreePlan.KeywordMinCharLimit_int#"/>

				</cfif>

	        </cfif> --->
	        <!--- 10.04.2018 End Old code bellow --->

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="GetKeywordsCountInUse" access="remote" output="false" hint="Get current user's number of keywords in use (exclude EMS blasts from count) for specfied short code.">

        <cfset var dataout = {} />
        <cfset var GetKeywordsCountInUseQuery	= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDSINUSE = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetKeywordsCountInUseQuery" datasource="#Session.DBSourceEBM#">
	        	SELECT
	        		COUNT(k.KeywordId_int) AS KeywordsInUse
	        	FROM
	        		sms.keyword k
	        		INNER JOIN simpleobjects.batch b ON k.BatchId_bi = b.BatchId_bi
	        	WHERE
	        		k.Active_int = 1
	        	AND
	        		k.IsDefault_bit = 0
	        	AND
	        		EMSFlag_int = 0
	        	AND
	        		b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	        </cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.KEYWORDSINUSE = "#GetKeywordsCountInUseQuery.KeywordsInUse#"/>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="GetShortAccessData" access="public" output="false" hint="Look up the Approval ID and Short Code ID for the current session user to access this short code">
		<cfargument name="inpShortCode" TYPE="any" hint="Shortcode to check if user has approved access to." />
		<cfargument name="inpAccountType" TYPE="string" default="1" required="false" hint="Only used for advanced company accounts - lets company account admins assign rights to short code resources DEFAULT_ACCOUNT_ID = 1; OWNER_TYPE_USER = 1; OWNER_TYPE_COMPANY = 2;" />

		<cfset var dataout = {} />
		<cfset var GetSCData = '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.SHORCODE = "#inpShortCode#"/>
		<cfset dataout.SHORCODEID = "0"/>
		<cfset dataout.SHORCODEREQUESTID = "0"/>
		<cfset dataout.SHORCODECLASS = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>
				<!---
					Defined in /session/cfc/csc/constants.cfm

					DEFAULT_ACCOUNT_ID = 1;
					OWNER_TYPE_USER = 1;
					OWNER_TYPE_COMPANY = 2;

					SHORT_CODE_APPROVED = 1;
					SHORT_CODE_REJECT = 2;
					SHORT_CODE_PENDING = 3;
					SHORT_CODE_TERMINATED = 4;
				--->

				<!--- SCR.KeywordQuantity_int, is legacy EBM - now ignored on Sire - Sire uses User Plan data limits instead --->

				<cfquery name="GetSCData" datasource="#Session.DBSourceEBM#">
					SELECT
						SCR.ShortCodeRequestId_int,
						SC.ShortCode_vch,
						SC.ShortCodeId_int,
						SC.Classification_int
					FROM
		                sms.shortcoderequest as SCR
		            LEFT JOIN
		             	sms.shortcode as SC
		            ON
		             	SC.ShortCodeId_int = SCR.ShortCodeId_int
		            WHERE
		            	SC.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
					AND
	             		SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
		            AND
		             	SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                	AND
                		SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAccountType#">
				</cfquery>

				<cfif GetSCData.RecordCount GT 0>

					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.SHORCODE = "#inpShortCode#"/>
					<cfset dataout.SHORCODEID = "#GetSCData.ShortCodeId_int#"/>
					<cfset dataout.SHORCODEREQUESTID = "#GetSCData.ShortCodeRequestId_int#"/>
					<cfset dataout.SHORCODECLASS = "#GetSCData.Classification_int#"/>

				<cfelse>

					<cfset dataout.RXRESULTCODE = 0 />
		    		<cfset dataout.MESSAGE = ""/>
					<cfset dataout.ERRMESSAGE = "No short code data found for this user Id (#Session.USERID#)"/>

				</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="GetKeywordId" access="public" output="false" hint="Get the current keyword ID used for this Batch.">
    	<cfargument name="inpShortCodeRequestId" TYPE="string" hint="The short code request Id this user is authorized to use" />
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />


        <cfset var dataout = {} />
        <cfset var GetKeywordIdQuery= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetKeywordIdQuery" datasource="#Session.DBSourceEBM#">
	        	SELECT
	        		k.KeywordId_int
	        	FROM
	        		sms.keyword k
	        	WHERE
	        		k.Active_int =1
	        	AND
	        		k.EMSFlag_int = 0
	       		AND
	       			k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	        </cfquery>

	  		<cfif GetKeywordIdQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.KEYWORDID = "#GetKeywordIdQuery.KeywordId_int#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="GetBatchOwner" access="public" output="false" hint="Get the current batch owners's User Id">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

        <cfset var dataout = {} />
        <cfset var GetBatchOwnerQuery= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.USERID = "-1"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetBatchOwnerQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserId_int
                FROM
                    simpleobjects.batch
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
            </cfquery>

	  		<cfif GetBatchOwnerQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERID = "#GetBatchOwnerQuery.UserId_int#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetBatchDetails" access="public" output="false" hint="Get the current batch owners's User Id">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
    	<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

        <cfset var dataout = {} />
        <cfset var GetBatchQuestion= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.RECORDCOUNT = 0/>
		<cfset dataout.BATCHID = "#inpBatchId#"/>
		<cfset dataout.DESC = ""/>
		<cfset dataout.GROUPID = "0"/>
		<cfset dataout.XMLCONTROLSTRING = ""/>
		<cfset dataout.EMSFLAG = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

            <cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS Desc_vch,
		                0 AS GroupId_int,
		                0 AS EMS_Flag_int,
	                    XMLControlString_vch,
	                    0 AS MLPId_int,
						0 AS ScheduleOption
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    Desc_vch,
			    		GroupId_int,
			    		XMLControlString_vch,
			    		EMS_Flag_int,
			    		MLPId_int,
			    		TemplateId_int,
			    		TemplateType_ti,
			    		ShortCodeId_int,
						ScheduleOption_ti as ScheduleOption
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

			</cfif>

	  		<cfif GetBatchQuestion.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.RECORDCOUNT = "#GetBatchQuestion.RecordCount#" />
				<cfset dataout.BATCHID = "#inpBatchId#"/>
				<cfset dataout.DESC = "#GetBatchQuestion.Desc_vch#"/>
				<cfset dataout.GROUPID = "#GetBatchQuestion.GroupId_int#"/>
				<cfset dataout.MLPID = "#GetBatchQuestion.MLPId_int#"/>
				<cfset dataout.XMLCONTROLSTRING = "#GetBatchQuestion.XMLControlString_vch#"/>
				<cfset dataout.EMSFLAG = "#GetBatchQuestion.EMS_Flag_int#"/>
				<cfset dataout.TEMPLATEID = "#GetBatchQuestion.TemplateId_int#"/>
				<cfset dataout.TemplateType = "#GetBatchQuestion.TemplateType_ti#"/>
				<cfset dataout.SHORTCODEID = "#GetBatchQuestion.ShortCodeId_int#"/>
				<cfset dataout.ScheduleOption = "#GetBatchQuestion.ScheduleOption#"/>

	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetBatchDesc" access="public" output="false" hint="Get the current batch description">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
    	<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

        <cfset var dataout = {} />
        <cfset var GetBatchQuestion= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.RECORDCOUNT = 0/>
		<cfset dataout.BATCHID = "#inpBatchId#"/>
		<cfset dataout.DESC = ""/>
		<cfset dataout.GROUPID = "0"/>
		<cfset dataout.XMLCONTROLSTRING = ""/>
		<cfset dataout.EMSFLAG = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

            <cfif inpTemplateFlag EQ 1>

	            <!--- Read from DB Batch Options --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
		                Description_vch AS Desc_vch,
		                0 AS GroupId_int,
		                0 AS EMS_Flag_int,
	                    XMLControlString_vch,
	                    0 AS MLPId_int
						0 AS ScheduleOption
	                FROM
	                    simpleobjects.templatesxml
	                WHERE
	                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	            </cfquery>

	        <cfelse>

	            <!--- Secured to Session.User by query --->
	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                    Desc_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	            </cfquery>

			</cfif>

	  		<cfif GetBatchQuestion.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.RECORDCOUNT = "#GetBatchQuestion.RecordCount#" />
				<cfset dataout.BATCHID = "#inpBatchId#"/>
				<cfset dataout.DESC = "#GetBatchQuestion.Desc_vch#"/>

	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="AddKeyword" access="public" output="false" hint="Add a keyword to the DB for the specified Short Code Request ID.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to to add." />
		<cfargument name="inpShortCodeId" TYPE="string" hint="Short code Id to add." />
		<cfargument name="inpShortCodeRequestId" TYPE="string" hint="The short code request Id this user is authorized to use" />
		<cfargument name="inpKeyword" TYPE="string" hint="Keyword to use." />
		<cfargument name="inpDefaultFlag" TYPE="string" required="false" default="0" hint="Use this keyword as the default response for all unknown keywords - default is 0" />

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var AddKeywordQuery = '' />
		<cfset var AddKeywordResult	= '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "0" />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Double check! - Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeyword#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

			<!--- Add keyword into db --->
            <cfquery name="AddKeywordQuery" datasource="#Session.DBSourceEBM#" result="AddKeywordResult">
                INSERT INTO sms.keyword
                    (
                        Keyword_vch,
                        ShortCodeRequestId_int,
                        ShortCodeId_int,
                        Response_vch,
                        Created_dt,
                        IsDefault_bit,
                        BatchId_bi,
                        Survey_int,
                        EMSFlag_int
                     )
                VALUES
                	(
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeRequestId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDefaultFlag#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
                    )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.KEYWORDID = "#AddKeywordResult.GENERATED_KEY#" />

			<cfcatch TYPE="any">

			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="UpdateKeyword" access="public" output="false" hint="Update a keyword in the DB for the specified Keyword ID.">
		<cfargument name="inpKeyword" TYPE="string" hint="Keyword to use." />
		<cfargument name="inpKeywordId" TYPE="string" hint="Keyword to use." />
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var UpdateKeywordQuery = '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Double check! - Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeyword#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.RXRESULTCODE NEQ 1>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = RetVarCheckKeywordAvailability.MESSAGE />
		        <cfset dataout.ERRMESSAGE = RetVarCheckKeywordAvailability.ERRMESSAGE />
				<cfreturn dataout>
   			<cfelseif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "This Keyword is already in use on this Short Code. Please try another."/>

	   			<cfreturn dataout>
   			</cfif>

			<!--- Update keyword in db --->
            <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">
                UPDATE sms.keyword SET
                    Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">
                WHERE
                    KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpKeywordId#">
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">

			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="ClearKeyword" access="remote" output="false" hint="Remove all keywords from a Batch">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check availability against." />

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var UpdateKeywordQuery = '' />
		<cfset var RetVarGetBatchOwner	= '' />
		<cfset var RetVarGetShortAccessData	= '' />
		<cfset var RetVarGetKeywordId	= '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>


			<!--- Validate current session owner owns Batch Id --->
   			<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

	   			<cfreturn dataout>
   			</cfif>


   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>

   			</cfif>

   			<!--- Check if this Batch already has a keyword set  - update if it does --->
   			<cfinvoke method="GetKeywordId" returnvariable="RetVarGetKeywordId">
	   			<cfinvokeargument name="inpShortCodeRequestId" value="#RetVarGetShortAccessData.SHORCODEREQUESTID#">
	   			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	   		</cfinvoke>

	   		<cfif RetVarGetKeywordId.KEYWORDID GT 0>

   				<cfset dataout.KEYWORDID = "#RetVarGetKeywordId.KEYWORDID#" />

				<!--- Clear keyword from db --->
	            <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">
	                UPDATE
	                	sms.keyword
	                SET
	                    Active_int = 0
	                WHERE
	                    KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetKeywordId.KEYWORDID#">
	            </cfquery>

			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">

			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="SetCustomStandardReplyHELPSTOP" access="remote" output="false" hint="Set both HELP and STOP in one call">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpKeyword" TYPE="string" hint="Keyword to use. HELP or STOP" />
		<cfargument name="inpHELPResponse" TYPE="string" default="" required="false" hint="Response to use. If blank will default to short code defaults"  />
		<cfargument name="inpSTOPResponse" TYPE="string" default="" required="false" hint="Response to use. If blank will default to short code defaults" />
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to Set this for." />

		<cfset var dataout = {}>
		<cfset var RetVarSetCustomStandardReply = '' />
		<cfset var RetVarSetCustomStandardReply = '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Write generic keyword HELP STOP   --->
			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpKeyword" value="HELP">
				<cfinvokeargument name="inpResponse" value="#inpHELPResponse#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>

			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpKeyword" value="STOP">
				<cfinvokeargument name="inpResponse" value="#inpSTOPResponse#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>


			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">

			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="SetCustomStandardReply" access="public" output="false" hint="Set the values for HELP or STOP standard responses - Compliance Tool">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpKeyword" TYPE="string" hint="Keyword to use. HELP or STOP" />
		<cfargument name="inpResponse" TYPE="string" hint="Response to use. If blank will default to short code defaults" />
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to Set this for." />

		<cfset var dataout = {}>
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var GetCustomResponse = '' />
		<cfset var UpdateCustomResponse = '' />
		<cfset var DeleteCustomResponse = '' />
		<cfset var AddCustomResponse = '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

            <!--- Validate current session owner owns Batch Id --->
   			<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

	   			<cfreturn dataout>
   			</cfif>


   			<!--- Only allowed standard responses are HELP or STOP --->
   			<cfif inpKeyword NEQ "HELP" AND inpKeyword NEQ "STOP">

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Sanity Check Failure. Only allowed standard responses are HELP or STOP"/>

	   			<cfreturn dataout>

   			</cfif>

			<!--- Check for existing Custom Help/Stop --->
            <cfquery name="GetCustomResponse" datasource="#Session.DBSourceEBM#">
                 SELECT
                    Content_vch
                 FROM
                    sms.smsshare
                 WHERE
                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                 AND
                    Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">
                 AND
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
            </cfquery>

            <cfif GetCustomResponse.RecordCount GT 0>

                <cfif TRIM(inpResponse) NEQ "">

					<!--- Update it --->
                    <cfquery name="UpdateCustomResponse" datasource="#Session.DBSourceEBM#">
                         UPDATE
                         	sms.smsshare
                         SET
                         	Content_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpResponse), 640)#">
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                         AND
                            Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
                    </cfquery>

                <cfelse>
                	<!--- Delete it --->
                	<cfquery name="DeleteCustomResponse" datasource="#Session.DBSourceEBM#">
                         DELETE FROM
                         	sms.smsshare
                         WHERE
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                         AND
                            Keyword_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">
                         AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
                    </cfquery>

                </cfif>

            <cfelse>

            	<cfif TRIM(inpResponse) NEQ "">

                    <!--- ...OR Add it --->
                    <cfquery name="AddCustomResponse" datasource="#Session.DBSourceEBM#">
                         INSERT INTO
                           	sms.smsshare
                         (
                            ShortCode_vch,
                         	Keyword_vch,
                            BatchId_bi,
                            Content_vch
                         )
                         VALUES
                         (
                         	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeyword#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpResponse), 640)#">
                         )
                  	</cfquery>

                </cfif>

            </cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="UpdateBatchData" access="remote" output="false" hint="Update Batch Data - Cleaner UI - all in one call for this section">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
    	<cfargument name="inpDesc" TYPE="string" hint="The new Description for this Batch" />
    	<cfargument name="inpKeyword" required="false" default="" TYPE="string" hint="Keyword to use or lookup if it is in use yet." />
    	<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check availability against." />
    	<cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" />
    	<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />


        <cfset var dataout = {} />
        <cfset var RetVarUpdateBatchDesc = StructNew()/>
        <cfset var RetVarClearKeyword = StructNew()/>
        <cfset var RetVarSaveKeyword = StructNew()/>

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

        	<cfset arguments.inpDesc = Replace(arguments.inpDesc, "#chr(10)#", "\n", "ALL")>
			<cfset arguments.inpDesc = Replace(arguments.inpDesc, "#chr(10)#", "<br>", "ALL")>

   			<cfinvoke method="UpdateBatchDesc" returnvariable="RetVarUpdateBatchDesc">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpDesc" value="#inpDesc#">
				<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
			</cfinvoke>

        	<cfif RetVarUpdateBatchDesc.RXRESULTCODE NEQ 1>
	        	<cfthrow detail="#RetVarUpdateBatchDesc.ERRMESSAGE#" message="#RetVarUpdateBatchDesc.MESSAGE#" />
        	</cfif>


        	<cfif inpTemplateFlag EQ 0>
	        	<cfif TRIM(inpKeyword) EQ "">

		        	<!--- If Keyword is blank then call remove keyword --->
		        	<cfinvoke method="ClearKeyword" returnvariable="RetVarClearKeyword">
						<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
						<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
					</cfinvoke>

					<cfset dataout.CLEARKEYWORDRESPONSE = RetVarClearKeyword/>

					<cfif RetVarClearKeyword.RXRESULTCODE NEQ 1>
			        	<cfthrow detail="#RetVarClearKeyword.ERRMESSAGE#" message="#RetVarClearKeyword.MESSAGE#" />
		        	</cfif>

	        	<cfelse>

		        	<!--- If Keyword is specified then set/update it --->
	        		<cfinvoke method="SaveKeyword" returnvariable="RetVarSaveKeyword">
						<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
						<cfinvokeargument name="inpKeyword" value="#inpKeyword#">
						<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
						<cfinvokeargument name="inpISOLatin" value="#inpISOLatin#">
					</cfinvoke>

					<cfset dataout.SAVEKEYWORDRESPONSE = RetVarSaveKeyword/>

					<cfif RetVarSaveKeyword.RXRESULTCODE NEQ 1>
			        	<cfthrow detail="#RetVarSaveKeyword.ERRMESSAGE#" message="#RetVarSaveKeyword.MESSAGE#" />
		        	</cfif>

	        	</cfif>
        	</cfif>

        	<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="UpdateBatchDesc" access="remote" output="false" hint="Update Batch Desc">
    	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
    	<cfargument name="inpDesc" TYPE="string" hint="The new Description for this Batch" />
    	<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" />

        <cfset var UpdateBatchQuery = '' />
        <cfset var dataout = {} />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

        	<cfif LEN(TRIM(inpDesc)) GT 0>

	        	<cfif inpTemplateFlag EQ 1>

		      		<cfquery name="UpdateBatchQuery" datasource="#Session.DBSourceEBM#">
		                UPDATE
		                	simpleobjects.templatesxml
		                SET
		                	Description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpDesc)#">
		                WHERE
		                    TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
		            </cfquery>

		        <cfelse>

		            <!--- Secured to Session.User by query --->
		    		<cfquery name="UpdateBatchQuery" datasource="#Session.DBSourceEBM#">
		                UPDATE
		                	simpleobjects.batch
		                SET
		                	Desc_vch = 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpDesc), 255)#">
		                WHERE
		                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
		                AND
		                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
		            </cfquery>

				</cfif>

				<cfset dataout.RXRESULTCODE = 1 />

	        <cfelse>

	       		<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Campaign name can not be blank."/>

	        </cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetBlastToListStats" access="remote" output="false" hint="Preview a Blast a Batch to a List of Subscribers - treturn numbers to confirm">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpGroupId" TYPE="string" required="true" default="0" hint="The List to send to" />
		<cfargument name="inpShortCode" TYPE="string" required="true" hint="Shortcode to Set this for." />
		<cfargument name="inpContactTypeId" TYPE="string" default="3" required="false" hint="1=Voice, 2=eMAil, 3=SMS, 4=OTP DEvice" />
		<cfargument name="inpSkipDNCCheck" TYPE="string" default="0" required="false" hint="1 = Count those on list and on DNC - 0 = count those on list AND not on DNC" />
		<cfargument name="ABTestingBatches" required="no" default="" hint="Comma sperated list of Batches. If Specified and duplicates are not allowed will limit distribution to those not already distributed in list of Batches" />
		<cfargument name="inpCdffilter" TYPE="string" default="" />
		<cfargument name="inpContactFilter" TYPE="string" default="" />
		<cfargument name="inpTimeZone" type="string" required="false" default="31" hint="Default calendar time zone setting is Pacific = 31"/>

		<cfset var dataout = {} />
		<cfset var RetVarGetBatchOwner= '' />
		<cfset var RetVarGetContactQueueCount = ''/>
		<cfset var RetValBillingData = '' />
		<cfset var RetVarGetSubscriberCounts	= '' />
		<cfset var RetVarGetContactQueueCountOnList = ''/>
		<cfset var RetValCheckActiveBlastQueue = '' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.LISTCOUNT = "0"/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.HASACTIVEQUEUE = 1 />

		<cftry>

			<!--- Validate inpGroupId is list of groups --->
			<cfif !REFind("^\d+(,\d+)*$", inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is valid"/>

				<cfreturn dataout>
			</cfif>

			<!--- Return error message if user Ids do not match --->
			<cfif listLen(inpGroupId) EQ 1 AND inpGroupId EQ 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Please select a subscriber list to send to first. If you do not have one, you will need to create one and use the List Building tools to get people to 'Subscribe' to your list."/>

				<cfreturn dataout>
			</cfif>


			<!--- Validate current session owner owns Batch Id --->
			<cfinvoke method="GetBatchOwner" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
			</cfinvoke>

			<!--- Return error message if user Ids do not match --->
			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

				<cfreturn dataout>
			</cfif>

			<!--- Get count of opt in from Subscriber list - currently only way to get on list is to opt in --->
			<cfinvoke method="GetSubscriberCounts" returnvariable="RetVarGetSubscriberCounts">
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				<cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
				<cfinvokeargument name="inpSkipDNCCheck" value="#inpSkipDNCCheck#"/>
				<cfinvokeargument name="inpCdffilter" value="#inpCdffilter#"/>
				<cfinvokeargument name="inpContactFilter" value="#inpContactFilter#"/>
			</cfinvoke>
			<cfset dataout.LISTCOUNT = "#RetVarGetSubscriberCounts.LISTCOUNT#"/>

			<!--- Get count of opt in from Subscriber list - currently only way to get on list is to opt in --->
			<cfinvoke method="GetContactQueueCount" returnvariable="RetVarGetContactQueueCount">
				<cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
			</cfinvoke>

			<cfset dataout.QUEUECOUNT = "#RetVarGetContactQueueCount.QUEUECOUNT#"/>

			<cfinvoke component="session.sire.models.cfc.billing" method="GetBalanceCurrentUser" returnvariable="RetValBillingData"></cfinvoke>

			<cfset dataout.BALANCE = "#NumberFormat(RetValBillingData.Balance,',')#"/>
			<cfset dataout.BALANCENUMBER = RetValBillingData.Balance/>

			<!--- Get count of opt in from Subscriber list - currently only way to get on list is to opt in --->
			<cfinvoke method="GetContactQueueCountOnList" returnvariable="RetVarGetContactQueueCountOnList">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				<cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
				<cfinvokeargument name="inpSkipDNCCheck" value="#inpSkipDNCCheck#"/>
				<cfinvokeargument name="ABTestingBatches" value="#ABTestingBatches#"/>
			</cfinvoke>

			<cfset dataout.LISTCOUNTDUPLICATE = "#RetVarGetContactQueueCountOnList.LISTCOUNTDUPLICATE#"/>


			<!---
			Scenario:
			User builds a list
			User sends blast to list
			More subscribers join list
			User wants to blast any new subscribers only


			Prior to HASACTIVEQUEUE this was possible - just duplicates would be blocked
			I am disabling HASACTIVEQUEUE for now - always will be 0 unless we put it back


			<!--- Check if user has active blast queue with this batch and group --->
			<cfinvoke method="GetActiveBlastQueue" returnvariable="RetValCheckActiveBlastQueue">
				<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
				<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#"/>
			</cfinvoke>

			<cfset dataout.HASACTIVEQUEUE = "#RetValCheckActiveBlastQueue.HASACTIVEQUEUE#"/>
			--->

			<cfset dataout.HASACTIVEQUEUE = 0 />

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>

			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

				<cfreturn dataout>
			</cfcatch>

		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetActiveBlastQueue" access="public" output="false" hint="Get current active blast queue">
		<cfargument name="inpBatchId" type="numeric" required="true" hint="The BatchId to check blast queue on"/>
		<cfargument name="inpGroupId" type="numeric" required="true" hint="The list to send to"/>

		<cfset var dataout = {} />
		<cfset var blastLogQuery = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.HASACTIVEQUEUE = -1 />

		<cftry>
			<cfquery name="blastLogQuery" datasource="#Session.DBSourceREAD#">
				SELECT
					Id_int
				FROM
					simplequeue.batch_load_queue
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#"/>
				AND
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					GroupId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
				AND
					Status_ti IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_READY#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_PROCESSING#">)
			</cfquery>
			<!--- Status_ti IN (1,2) -- Processing --->

			<cfif blastLogQuery.RECORDCOUNT GT 0>
				<cfset dataout.HASACTIVEQUEUE = 1/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

				<cfreturn dataout />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetSubscriberCounts" access="remote" output="false" hint="Get count of elligable subscribers in list and NOT IN (or IN depending on inpSkipDNCCheck) DNC">
		<cfargument name="inpGroupId" TYPE="string" required="true" hint="The Subscriber List Id" />
		<cfargument name="inpShortCode" TYPE="string" required="true" hint="Shortcode to Set this for." />
		<cfargument name="inpContactTypeId" TYPE="string" default="3" required="false" hint="1=Voice, 2=eMAil, 3=SMS, 4=OTP DEvice" />
		<cfargument name="inpSkipDNCCheck" TYPE="string" default="0" required="false" hint="1 = Count those on list and on DNC - 0 = count those on list AND not on DNC" />
		<cfargument name="inpCdffilter" TYPE="string" default="">
		<cfargument name="inpContactFilter" TYPE="string" default="">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#Session.USERID#">
		<cfset var dataout = {} />
		<cfset var GetSelGroupCount= '' />
		<cfset var cdfLoopIndex = 1 />
		<cfset var cdfItem = ''/>
		<cfset var cdfRowItem = ''/>
		<cfset var cdValuefLoopIndex = 1 />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.LISTCOUNT = "0"/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<cfif !REFind("^\d+(,\d+)*$", inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is valid"/>

				<cfreturn dataout>
			</cfif>

			<cfif arguments.inpCdffilter NEQ ''>
				<cfset arguments.inpCdffilter = DeSerializeJSON(arguments.inpCdffilter) />
			<cfelse>
				<cfset arguments.inpCdffilter = arrayNew() />
			</cfif>

			<!--- <cfdump var="#arguments.inpCdffilter#" abort="true"/> --->
			<!--- Secured to Session.User by query --->
			<!--- <cfdump var="inpGroupId" abort="true"> --->
			<!--- Get group counts --->

			<cfquery name="GetSelGroupCount" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(Record) AS TotalGroupCount
				FROM
					(
					SELECT
						1 AS Record
					FROM
						simplelists.groupcontactlist
					INNER JOIN
						simplelists.contactstring
						ON
							simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					INNER JOIN
						simplelists.contactlist
						ON
							simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
					LEFT JOIN
						simplelists.contactvariable
						ON
							simplelists.contactstring.ContactId_bi = simplelists.contactvariable.ContactId_bi
					WHERE
						simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
					AND
						simplelists.groupcontactlist.groupid_bi IN (#inpGroupId#)
					AND
						ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#"/>
						<!--- Use = "equal" rerun for other contact types - using IN really slows down query --->
						<!--- Exclude master DNC --->
						<!--- Master DNC is any contact in user Id 50's lists --->
					AND
						ContactString_vch NOT IN (
							SELECT
								ContactString_vch
							FROM
								simplelists.contactstring
							INNER JOIN
								simplelists.contactlist
								ON
									simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
							WHERE
								UserId_int = 50
						)

					<!--- Session User DNC --->
					<cfif inpSkipDNCCheck EQ 0 >
						AND	0 = (
							SELECT
								COUNT(oi.ContactString_vch)
							FROM
								simplelists.optinout AS oi
							WHERE
								OptOut_dt IS NOT NULL
							AND
								OptIn_dt IS NULL
							AND
								ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"/>
							AND
								oi.ContactString_vch = simplelists.contactstring.ContactString_vch
							)
					<cfelse>
						AND	0 < (
							SELECT
								COUNT(oi.ContactString_vch)
							FROM
								simplelists.optinout AS oi
							WHERE
								OptOut_dt IS NOT NULL
							AND
								OptIn_dt IS NULL
							AND
								ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"/>
							AND
								oi.ContactString_vch = simplelists.contactstring.ContactString_vch
							)
					</cfif>
					<cfif trim(arguments.inpContactFilter) NEQ "">
						AND
							simplelists.contactstring.ContactString_vch like  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactFilter#%"/>
					</cfif>
					<cfif arraylen(arguments.inpCdffilter) GT 0>
						AND (
							<cfset cdfLoopIndex = 1 />
							<cfloop array="#arguments.inpCdffilter#" index="cdfItem">

								<!--- Each filter is an OR by default --->
								<cfif cdfLoopIndex  GT 1>
									OR
								</cfif>

								<!--- Each filter is a type ID AND a series of possible vaules (OR) clauses  --->
								(
									simplelists.contactvariable.CdfId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#"/>

									AND (

										<!--- Reset for each Filter type (cdfItem.CDFID) --->
										<cfset cdValuefLoopIndex = 1 />
										<cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">

											<cfif cdValuefLoopIndex  GT 1>
												OR
											</cfif>

											simplelists.contactvariable.VariableValue_vch

											<cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
												#cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#URLDecode(cdfRowItem.VALUE)#%"/>
											<cfelseif lcase(cdfRowItem.OPERATOR) eq '<>'>
												#cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#URLDecode(cdfRowItem.VALUE)#"/>
											<cfelse>
												#cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#URLDecode(cdfRowItem.VALUE)#"/>
											</cfif>

											<cfset cdValuefLoopIndex++/>
										</cfloop>
										)
									)
								<cfset cdfLoopIndex++/>
							</cfloop>
							)
					</cfif>

					GROUP BY
						simplelists.contactstring.ContactId_bi
					<cfif arraylen(arguments.inpCdffilter) GT 0>
						HAVING
							COUNT(simplelists.contactvariable.ContactId_bi) = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" value="#arraylen(arguments.inpCdffilter)#"/>
					</cfif>
					) tmpTable

			</cfquery>


			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.LISTCOUNT = "#GetSelGroupCount.TotalGroupCount#"/>

			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

				<cfreturn dataout />
			</cfcatch>

		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetContactQueueCount" access="remote" output="false" hint="Get count of elligable subscribers already in the contact queue for current user.">
     	<cfargument name="inpContactTypeId" TYPE="string" default="3" required="false" hint="1=Voice, 2=eMAil, 3=SMS, 4=OTP DEvice" />
     	<cfargument name="inpCdffilter" TYPE="string" default="">
		<cfargument name="inpContactFilter" TYPE="string" default="">

        <cfset var dataout = {} />
        <cfset var GetTotalQueueCount= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.QUEUECOUNT = "0"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

        	<!--- Secured to Session.User by query --->

        	<!--- See constants.cfm for current list of possible values --->
        	<!---
	         	ContactQueue_Queued = 1 />
				ContactQueue_InProgress = 2 />
				ContactQueue_InFulfillment = 3 />
				ContactQueue_Dupe = 4 />
				ContactQueue_Extracted = 5 />
				ContactQueue_BlockByBR = 6 />
				ContactQueue_SMSStop = 7 />
				ContactQueue_RealTimeFulfilled = 8 />
				ContactQueue_Hold = 100 />
	         --->

			<!--- Get group counts --->
            <cfquery name="GetTotalQueueCount" datasource="#Session.DBSourceEBM#">
                SELECT
                	COUNT(*) AS TotalCount
                FROM
					simplequeue.contactqueue
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
					DTSStatusType_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#ContactQueue_Queued#">
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.QUEUECOUNT = "#GetTotalQueueCount.TotalCount#"/>


		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetContactQueueCountOnList" access="remote" output="false" hint="Get count of non-elligable duplicate subscribers already in the contact queue for current user and current batch and current list.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpGroupId" TYPE="string" required="true" default="0" hint="The List to send to" />
		<cfargument name="inpShortCode" TYPE="string" required="true" hint="Shortcode to Set this for." />
		<cfargument name="inpContactTypeId" TYPE="string" default="3" required="false" hint="1=Voice, 2=eMAil, 3=SMS, 4=OTP DEvice" />
		<cfargument name="inpSkipDNCCheck" TYPE="string" default="0" required="false" hint="1 = Count those on list and on DNC - 0 = count those on list AND not on DNC" />
		<cfargument name="ABTestingBatches" required="no" default="" hint="Comma sperated list of Batches. If Specified and duplicates are not allowed will limit distribution to those not already distributed in list of Batches" />

		<cfset var dataout = {} />
		<cfset var GetTotalQueueCount= '' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.LISTCOUNTDUPLICATE = "0"/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<cfif !REFind("^\d+(,\d+)*$", inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is valid"/>

				<cfreturn dataout>
			</cfif>

			<!--- Secured to Session.User by query --->

			<!--- Get group counts --->
			<cfquery name="GetTotalQueueCount" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TotalGroupCount
				FROM
					simplelists.groupcontactlist
				INNER JOIN
					simplelists.contactstring
					ON
						simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
				INNER JOIN
					simplelists.contactlist
					ON
						simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
				WHERE
					simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"/>
				AND
					simplelists.groupcontactlist.groupid_bi IN (#inpGroupId#)
				AND
					ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#"/>
				<!--- Use = "equal" rerun for other contact types - using IN really slows down query --->
				<!--- Session User DNC --->
				<cfif inpSkipDNCCheck EQ 0 >
					AND	0 = (
						SELECT
							COUNT(oi.ContactString_vch)
						FROM
							simplelists.optinout AS oi
						WHERE
							OptOut_dt IS NOT NULL
						AND
							OptIn_dt IS NULL
						AND
							ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"/>
						AND
							oi.ContactString_vch = simplelists.contactstring.ContactString_vch
						)
				<cfelse>
					AND	0 < (
						SELECT
							COUNT(oi.ContactString_vch)
						FROM
							simplelists.optinout AS oi
						WHERE
							OptOut_dt IS NOT NULL
						AND
							OptIn_dt IS NULL
						AND
							ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"/>
						AND
							oi.ContactString_vch = simplelists.contactstring.ContactString_vch
						)
				</cfif>

				AND
					ContactString_vch IN
					(
					SELECT
						simplequeue.contactqueue.contactstring_vch
					FROM
						simplelists.groupcontactlist
					INNER JOIN
						simplelists.contactstring
						ON
							simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					INNER JOIN
						simplelists.contactlist
						ON
							simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
					INNER JOIN
						simplequeue.contactqueue
						ON
							simplequeue.contactqueue.contactstring_vch = simplelists.contactstring.contactstring_vch
						AND
							simplequeue.contactqueue.typemask_ti = 3
					<cfif ABTestingBatches NEQ "">
						AND
							simplequeue.contactqueue.batchid_bi IN (<cfqueryparam value="#ABTestingBatches#" cfsqltype="CF_SQL_INTEGER" list="yes">)
					<cfelse>
						AND
							simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
					</cfif>
					WHERE
						simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						simplelists.groupcontactlist.groupid_bi IN (#inpGroupId#)
					)
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.LISTCOUNTDUPLICATE = "#GetTotalQueueCount.TotalGroupCount#"/>


			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

				<cfreturn dataout>
			</cfcatch>

		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetBatchList" access="remote" output="false" hint="Get List of all Batches for Select Box for current user.">
     	<cfargument name="inpActive" TYPE="string" required="false" default="0" hint="If Postitive, Limit to Active Batches only." />

        <cfset var dataout = {} />
        <cfset var GetBatches= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHARRAY = {}/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

        	<!--- Secured to Session.User by query --->

			<!--- Get GetBatches --->
            <cfquery name="GetBatches" datasource="#Session.DBSourceEBM#">
                SELECT
                	BatchId_bi,
                	Desc_vch
                FROM
					simpleobjects.batch
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            <cfif inpActive GT 0>
	                AND
	                	Active_int = 1
	            </cfif>

            </cfquery>

			<cfset dataout.BATCHARRAY = GetBatches/>
			<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetBatchFilteredList" access="remote" output="false" hint="Get List of all Batches for Select Box for current user.">
		<cfargument name="inpActive" TYPE="string" required="false" default="1" hint="If Postitive, Limit to Active Batches only." />

		<cfset var dataout = {} />
		<cfset var GetBatches= '' />
		<cfset var RetVarGetAssignedShortCode = '' />
		<cfset var defaultShortCodeId= '0' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHARRAY = {}/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Secured to Session.User by query --->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetAssignedShortCode"></cfinvoke>
			<cfif RetVarGetAssignedShortCode.RXRESULTCODE EQ 1>
				<cfset defaultShortCodeId = RetVarGetAssignedShortCode.SHORTCODEID />
			</cfif>

			<!--- Get GetBatches --->
			<cfquery name="GetBatches" datasource="#Session.DBSourceEBM#">
				SELECT
					BatchId_bi,
					Desc_vch
				FROM
					simpleobjects.batch
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfif inpActive GT 0>
					AND
						Active_int = 1
				</cfif>
					AND
						EMS_Flag_int IN (0,1,2)
					AND
						ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCodeId#">
			</cfquery>

			<cfset dataout.BATCHARRAY = GetBatches/>
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetControlPointsForBatch" access="remote" output="true" hint="Get Control Points for a given Batch - limit to user owned.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

		<cfset var dataout = '' />
		<cfset var inpRXSSEMLocalBuff	= '' />
		<cfset var myxmldocResultDoc	= '' />
        <cfset var selectedElements	= '' />
        <cfset var arrQuestion	= '' />
        <cfset var questionObj	= '' />
        <cfset var listQuestion	= '' />
        <cfset var GetBatchQuestion	= {} />
        <cfset GetBatchQuestion.UserId_int	= 0 />


		<cfset dataout = {}>
        <cfset dataout["CPDATA"] = ArrayNew(1) />


			<cftry>

				<!--- Validate Batch Id--->
				<cfif !isnumeric(INPBATCHID) >
                    <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
                </cfif>

                <!--- Secured to Session.User by query --->

                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                    SELECT
                        XMLControlString_vch,
                        UserId_int
                    FROM
                        simpleobjects.batch
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                    AND
                    	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                </cfquery>

                <cfset inpRXSSEMLocalBuff = GetBatchQuestion.XMLControlString_vch />

                <!--- Parse XML Control String--->
                <cftry>
                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #inpRXSSEMLocalBuff# & "</XMLControlStringDoc>") />
                    <cfcatch TYPE="any">
                        <!--- Squash bad data  --->
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
                    </cfcatch>
                </cftry>

                <cfset selectedElements = XmlSearch(myxmldocResultDoc, "//Q") />
                <cfset arrQuestion = ArrayNew(1) />
                <cfloop array="#selectedElements#" index="listQuestion">
                    <cfset questionObj = StructNew() />
                    <!--- Get text and type of question --->
                    <cfset questionObj.RQ = listQuestion.XmlAttributes.RQ />
                    <cfset questionObj.ID= listQuestion.XmlAttributes.ID />
                    <cfset questionObj.TEXT = listQuestion.XmlAttributes.Text />
                    <cfset questionObj.TYPE = listQuestion.XmlAttributes.TYPE />

                    <cfset ArrayAppend(dataout["CPDATA"],questionObj)>

                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />

                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout["CPDATA"] = ArrayNew(1) />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetGroups" access="remote" output="false" hint="Get the current list of subscribers for the current user.">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.GROUPS = []>
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetGroupsQuery= {} />
		<cfset var defaultShortCode = '' />
		<cfset var defaultShortCodeId = 0 />

		<cftry>

        	<!--- Secured to Session.User by query --->
        	<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
        	<cfif defaultShortCode.RXRESULTCODE EQ 1>
        		<cfset defaultShortCodeId = defaultShortCode.SHORTCODEID />
    		</cfif>

			<!--- Read from DB Group Details - only allow current user to view their own groups --->
            <cfquery name="GetGroupsQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    GroupId_bi,
                    GroupName_vch
                FROM
                    simplelists.grouplist
                WHERE
                   	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
               	<cfif defaultShortCodeId GT 0>
                AND
                	ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCodeId#">
            	</cfif>
                AND
                	Active_int =1
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.GROUPS = GetGroupsQuery>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetGroupInfo" access="remote" output="false" hint="Get the information for the supplied group">
		<cfargument name="inpGroupId" required="yes" hint="The subscriber lists ID">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.GROUPNAME = "None Selected">
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetGroupsQuery= {} />

		<cftry>

        	<!--- Secured to Session.User by query --->

			<!--- Read from DB Group Details - only allow current user to view their own groups --->
            <cfquery name="GetGroupsQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    GroupId_bi,
                    GroupName_vch
                FROM
                    simplelists.grouplist
                WHERE
                   	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND
	                GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpGroupId#">
                AND
                	Active_int = 1
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.GROUPNAME = GetGroupsQuery.GroupName_vch>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="AddGroup" access="remote" output="false" hint="Add a new Group (Subscriber List) to this user's account" >
       	<cfargument name="INPGROUPDESC" required="yes" hint="The new subscriber list's unique (per user Id) name">

        <!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var NextGroupId	= '' />
		<cfset var VerifyUnique	= '' />
		<cfset var AddGroupQuery	= '' />
		<cfset var AddGroupResult	= '' />
		<cfset var shortcode = '' />

        <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	    --->


		<cfoutput>

            <cfset NextGroupId = -1>

            <!--- Set default return values --->
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.INPGROUPDESC = "#INPGROUPDESC#">
			<cfset dataout.INPGROUPID = "#NextGroupId#">
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />


            <cftry>

			  	<!--- Set default to -1 --->
			   	<cfset NextGroupId = -1>

                <!--- Verify does not already exist--->
                <!--- Get next Lib ID for current user --->
                <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                    SELECT
                        GroupId_bi
                    FROM
                        simplelists.grouplist
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND
                        GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">
                </cfquery>

                <cfif VerifyUnique.RECORDCOUNT GT 0>

                    <cfset dataout.RXRESULTCODE = -2 />
					<cfset dataout.INPGROUPDESC = "#INPGROUPDESC#">
					<cfset dataout.INPGROUPID = "#VerifyUnique.GroupId_bi#">
					<cfset dataout.TYPE = "" />
					<cfset dataout.MESSAGE = "Subscriber List Name already in use! Try a different Subscriber List Name." />
					<cfset dataout.ERRMESSAGE = "" />
					<cfreturn dataout />

                </cfif>


                <!--- remove old method:
				<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
				--->
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

                <cfquery name="AddGroupQuery" datasource="#Session.DBSourceEBM#" result="AddGroupResult">
                    INSERT INTO simplelists.grouplist
                        (UserId_int, GroupName_vch, Created_dt, ShortCodeId_int)
                    VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortcode.SHORTCODEID#"/>
                    )
                </cfquery>

                <cfset NextGroupId = #AddGroupResult.GENERATEDKEY#>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.INPGROUPDESC = "#INPGROUPDESC#">
				<cfset dataout.INPGROUPID = "#NextGroupId#">
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />



            <cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">


            </cfcatch>

            </cftry>

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


	<cffunction name="GetCDFs" access="remote" output="false" hint="Get the current list of subscribers for the current user.">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.GROUPS = []>
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetCDFsQuery= {} />

		<cftry>

        	<!--- Secured to Session.User by query --->

			<!--- Read from DB Group Details - only allow current user to view their own groups --->
            <cfquery name="GetCDFsQuery" datasource="#Session.DBSourceEBM#">

               SELECT
			        CDFId_int,
			        CDFName_vch,
			        CDFDefaultValue_vch as DEFAULTVALUE
			    FROM
			        simplelists.customdefinedfields
			    WHERE
			        simplelists.customdefinedfields.userid_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.UserId#">
			    ORDER BY
			        CDFName_vch ASC

            </cfquery>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.CDFS = GetCDFsQuery>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="AddCDF" access="remote" output="false" hint="Add a Custom Data Field (CDF) to this users account." >
       	<cfargument name="inpCDFName" required="yes" hint="The new CDF's name unique (per user Id) name">
		<cfargument name="inpOptions" TYPE="string" required="no" default="{}" hint="The array of default values a user can choose for this CDF">
		<cfargument name="inpCheckOption" TYPE="string" required="no" default=0 hint="This defines whether to use the array of default values or not ">
		<cfargument name="inpDefaultValue" TYPE="string" required="no" default="" hint="Single CDF default value">


		<cfset var AddNewCDF = "">


		<cfset var dataout	= {} />
		<cfset var NextGroupId	= '' />
		<cfset var VerifyUnique	= '' />
		<cfset var AddGroupQuery	= '' />
		<cfset var AddGroupResult	= '' />
		<cfset var defaultValueData	= '' />
		<cfset var value	= '' />
		<cfset var item	= '' />
		<cfset var AddNewCDFQuery	= '' />
		<cfset var AddDefaultValueCustomDefinedFields	= '' />
		<cfset var UpdateCustomField	= '' />
		<cfset var AddNewCDFResult	= '' />
		<cfset var AddDefaultValues	= '' />


		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CDFID = "0">
		<cfset dataout.CDFName = "#LEFT(TRIM(arguments.inpCDFName),100)#">
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>



        <!---

		Special CDF Creation:

		Insert a new CDF - get the CDFID


		INSERT INTO simplelists.default_value_custom_defined_fields
		(
			CdfId_int,
			default_value
		)
		SELECT DISTINCT #CDFID#, STATE FROM melissadata.cnty

		then set the data based on melissa in main contact if need be - let user change to whatever later
update simplelists.contactvariable cv
left join simplelists.contactlist cl on
    cl.ContactId_bi = cv.ContactId_bi
set
    VariableValue_vch = cl.State_vch
WHERE
	cv.UserId_int	 = 389
and cv.CDFID_int = 11

...

and


update simplelists.contactvariable cv
LEFT Join simplelists.default_value_custom_defined_fields df ON df.default_value = cv.VariableValue_vch
set
    VariableValue_vch = df.id
WHERE
	cv.UserId_int = 389
and cv.CDFID_int = 11
AND df.cdfid_int = 11




		--->

	        <!--- make sure you arin simplelists DB

				-- --------------------------------------------------------------------------------
				-- Routine DDL
				-- Note: comments before and after the routine body will not be stored by the server
				-- --------------------------------------------------------------------------------
				DELIMITER $$
				CREATE DEFINER=`EBMSQLAdmin`@`%` PROCEDURE `sp_addcdf`( IN inpDesc Varchar(100),
				IN inpDefault Varchar(5000),
				IN inpUserId INTEGER(11),
				IN inpCdfId INTEGER(11)
				)
				BEGIN

				INSERT INTO
				simplelists.contactvariable
				SELECT
				NULL,
				UserId_int,
				ContactId_bi,
				LEFT(inpDesc,100),
				LEFT(inpDefaultValue, 5000),
				NOW(),
				inpCdfId
				FROM
				simplelists.contactlist
				WHERE
				UserId_int = inpUserId;
				END$$
				DELIMITER ;

			--->

			<!--- Verify name does not already exist for this user --->
		    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
	            SELECT
	                COUNT(CDFName_vch) AS TOTALCOUNT
	            FROM
	                simplelists.customdefinedfields
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            AND
	                CDFName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(arguments.inpCDFName),100)#">
	        </cfquery>

	        <cfif VerifyUnique.TOTALCOUNT GT 0>

	            <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.CDFID = "0">
				<cfset dataout.CDFName = "#LEFT(TRIM(arguments.inpCDFName),100)#">
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "CDF Name already in use for this user account! Try a different CDF Name." />
				<cfset dataout.ERRMESSAGE = "" />
				<cfreturn dataout />

	        </cfif>

			<!--- Insert new record to get CDFId_int  --->
			<cfquery name="AddNewCDFQuery" result="AddNewCDFResult" datasource="#Session.DBSourceEBM#">
	        	insert into
					simplelists.customdefinedfields
	                (
						CDFName_vch,
						UserId_int,
						CdfDefaultValue_vch,
						Created_dt,
						type_field
					)
				Values(
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(arguments.inpCDFName),100)#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDefaultValue,5000)#">,
					 NOW(),
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCheckOption#">
				)
	        </cfquery>

			<!--- Add default values for UI MLP and other interfaces where a set list of optional values is specified --->
	        <cfif inpOptions NEQ '{}' AND inpOptions NEQ '' AND inpCheckOption EQ 1>
	            <cfset defaultValueData = deserializeJSON(inpOptions)>
	            <cfloop collection="#defaultValueData#" item="item">
	                <cfset value = StructFind(defaultValueData,item)/>
	                <cfquery name="AddDefaultValueCustomDefinedFields" result="AddDefaultValues" datasource="#Session.DBSourceEBM#">
	                    insert into
	                        simplelists.default_value_custom_defined_fields
	                        (
	                            CdfId_int,
	                            default_value
	                        )
	                    Values(
	                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AddNewCDFResult.generatedkey#">,
	                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">
	                    )
	                </cfquery>
	            </cfloop>
	        </cfif>
	        <!---now we have id of recent inserted record --->

	    	<!--- This will work much faster as a stored procedure ...--->
			<!--- update custom data field --->
			<cfquery name="UpdateCustomField" datasource="#Session.DBSourceEBM#">
	           <!--- INSERT INTO
	                simplelists.contactvariable
	            SELECT
	                NULL,
	                UserId_int,
	                ContactId_bi,
	                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
	                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDefaultValue#">,
	                NOW()
	            FROM
	                simplelists.contactlist
	            WHERE
	                UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">    --->

	                <!--- Replace query with stored procedure --->

	                CALL simplelists.sp_addcdf(<cfqueryparam CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(arguments.inpCDFName),100)#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDefaultValue,5000)#">,<cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AddNewCDFResult.GENERATED_KEY#">);

	        </cfquery>

		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.CDFID = "#AddNewCDFResult.generatedkey#">
			<cfset dataout.CDFName = "#LEFT(TRIM(arguments.inpCDFName),100)#">
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "" />
   		    <cfset dataout.ERRMESSAGE = "" />
	    <cfcatch TYPE="any">
		    <cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.CDFID = "0">
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetPreviousBlasts" access="remote" output="false" hint="Get the current list of subscribers for the current user.">
	  	<cfargument name="inpBatchId" TYPE="any" required="true" hint="The BatchId to read the XMLControlString from" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BLASTFOUND = 0 />
		<cfset dataout.QUEUED = 0 />
		<cfset dataout.COMPLETED = 0 />
		<cfset dataout.PAUSED = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetTotalQueueCount= {} />

		<cftry>

        	<!--- Secured to Session.User by query --->

            <!--- Get group counts --->
            <cfquery name="GetTotalQueueCount" datasource="#Session.DBSourceEBM#">
                SELECT
                	COUNT(*) AS TotalCount,
                	DTSStatusType_ti
                FROM
					simplequeue.contactqueue
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                GROUP BY
                	DTSStatusType_ti
            </cfquery>


            <cfif GetTotalQueueCount.RECORDCOUNT GT 0>


	            <cfloop query="GetTotalQueueCount">


		            <cfset dataout.BLASTFOUND = 1 />

		            <!---
			            See contstanst file for possible values
			            < SMSOUT_PAUSED = 0>
						< SMSOUT_QUEUED = 1>
						< SMSOUT_QUEUEDTOGO = 2>
						< SMSOUT_INPROCESSONDIALER = 3>
						< SMSOUT_DUPLICATE = 4>
						< SMSOUT_EXTRACTED = 5>
						< SMSOUT_BADXML = 6>
						< SMSOUT_SMSSTOP = 7>
						< SMSOUT_HOLDAUDIOERROR = 100>
			        --->

		            <!--- Keep UI simple for now - In Queue still or Sent only --->
		            <cfswitch expression="#GetTotalQueueCount.DTSStatusType_ti#">

			            <cfcase value="#SMSOUT_PAUSED#">
			            	<cfset dataout.PAUSED = dataout.PAUSED + GetTotalQueueCount.TotalCount />
		            	</cfcase>

		            	<cfcase value="#SMSOUT_QUEUED#">
			            	<cfset dataout.QUEUED = dataout.QUEUED + GetTotalQueueCount.TotalCount />
		            	</cfcase>

						<cfdefaultcase>
							<cfset dataout.COMPLETED = dataout.COMPLETED + GetTotalQueueCount.TotalCount />
						</cfdefaultcase>

		            </cfswitch>

	            </cfloop>


		    <cfelse>

			    <cfset dataout.BLASTFOUND = 0 />
			    <cfset dataout.QUEUED = 0 />
				<cfset dataout.COMPLETED = 0 />
				<cfset dataout.PAUSED = 0 />

		    </cfif>

			<cfset dataout.RXRESULTCODE = 1>

			<cfcatch TYPE="any">


				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="AddBlastLog" access="remote" output="false" hint="Add a new log entry for this blast" >
		<cfargument name="inpStatus" TYPE="string" required="false" default="#BLAST_LOG_LOADING#" hint="The Status of this Batch Blast" />
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpSourceBatchId" TYPE="string" required="false" default="0" hint="The BatchId that the cloned campaign is being created from." />
		<cfargument name="inpGroupId" TYPE="string" required="true" default="0" hint="The List to send to" />
		<cfargument name="inpAmountLoaded" TYPE="string" required="false" default="0" hint="The amount actually loaded into queue." />
		<cfargument name="inpDesc" TYPE="string" required="true" default="0" hint="Blast" />
		<cfargument name="inpAPIRequestJSON" TYPE="string" required="no" default="" />
		<cfargument name="inpTimeZone" required="false" default="0" hint="used by the schedule to send messages local to the target devices time zone">
		<cfargument name="inpDistributionProcessId" type="string" required="false" default="0" hint="Use this Id to allow multiple CRON/Thread processes to run against same tables"/>


        <!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var NextGroupId	= '' />
		<cfset var VerifyUnique	= '' />
		<cfset var AddBlastLogQuery	= '' />
		<cfset var AddBlastLogQueryResult	= '' />

        <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

    	Used just for loggin in beginning but
		JLP TODO: *** need to return gui fast and then process loads in back ground instead of making user wait
		MAybe adjust based on volume which way to load?
	    --->


		<cfoutput>

            <!--- Set default return values --->
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.BLASTID = "0">
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

            <cftry>

			  <cfif TRIM(arguments.inpTimeZone) EQ "" OR !IsNumeric(arguments.inpTimeZone)>
			  	<cfset arguments.inpTimeZone = 0 />
			  </cfif>

			  <cfif TRIM(arguments.inpDistributionProcessId) EQ "" OR !IsNumeric(arguments.inpDistributionProcessId)>
			  	<cfset arguments.inpDistributionProcessId = 0 />
			  </cfif>

                <cfquery name="AddBlastLogQuery" datasource="#Session.DBSourceEBM#" result="AddBlastLogQueryResult">
                    INSERT INTO simpleobjects.batch_blast_log
                    (
	                    Status_int,
                    	UserId_int,
                        BatchId_bi,
                        SourceBatchId_bi,
                        GroupIds_vch,
                        AmountLoaded_int,
                        Desc_vch,
                        APIRequestJSON_vch,
                        Created_dt,
				    TimeZone_int,
				    DistributionProcessId_int
                    )
                    VALUES
                    (
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSourceBatchId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpGroupId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAmountLoaded#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(arguments.inpDesc),255)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#inpAPIRequestJSON#">,
                        NOW(),
				    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeZone#">,
				    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDistributionProcessId#">
                    )
                </cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.BLASTID = "#AddBlastLogQueryResult.GENERATEDKEY#">
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />

            <cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">


            </cfcatch>

            </cftry>

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateBlastLogStatus" access="remote" output="false" hint="Update the Blast Log Status" >
       	<cfargument name="inpBlastId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
    	<cfargument name="inpStatus" TYPE="string" required="true" hint="The Status of this Batch Blast" />

        <!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var NextGroupId	= '' />
		<cfset var VerifyUnique	= '' />
		<cfset var UpdatedBlastLogQuery	= '' />

        <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	    --->

		<cfoutput>

            <!--- Set default return values --->
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

            <cftry>

                <cfquery name="UpdatedBlastLogQuery" datasource="#Session.DBSourceEBM#">
                    UPDATE
                    	simpleobjects.batch_blast_log
                    SET
                    	Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">
                    WHERE
                    	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND
                    	PKId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBlastId#">
                </cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />

            <cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

            </cfcatch>

            </cftry>

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


    <cffunction name="GetBatchBlasts" access="remote" output="false" hint="Get list of all blasts for given Batch .">
     	<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

        <cfset var dataout = {} />
        <cfset var GetBlastsQuery= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BLASTS = {}/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

        	<!--- Secured to Session.User by query --->

			<!--- Get GetBatches --->
            <cfquery name="GetBlastsQuery" datasource="#Session.DBSourceEBM#">
                SELECT
	                PKId_int,
                	Status_int,
                	UserId_int,
                    BatchId_bi,
                    SourceBatchId_bi,
                    GroupIds_vch,
                    AmountLoaded_int,
                    Desc_vch,
                    Created_dt
                FROM
					simpleobjects.batch_blast_log
	            WHERE
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            AND
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
            </cfquery>

			<cfset dataout.BLASTS = GetBlastsQuery/>
			<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="CloneCampaign" access="remote" output="false" hint="Clone a copy of this Batch">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpShortCode" TYPE="string" required="false" default="" hint="Shortcode to Set this for. IF the Batch has one defined it will be used if this is blank." />
		<cfargument name="inpDesc" required="false" default="" hint="Name of the blast - cloned campaing option only">

		<cfset var dataout = {} />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.NEXTCAMPAINID = 0>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>


		<cfset var GetCampaign = ''>
		<cfset var NEXTCAMPAINID = 0>
		<cfset var RecipientList = ''>
		<cfset var xmlControlString = ''>
		<cfset var AddBatch = ''>
		<cfset var AddBatchQuery = ''>
		<cfset var RetValGetKeywordFromBatchId = ''>
		<cfset var RetValAddKeywordEMS = ''>
		<cfset var getShortCodeRequestByUser = ''>
		<cfset var userOrgInfo = ''>
		<cfset var AddKeyword = ''>
		<cfset var RetValAddKeyword = ''>
		<cfset var shortCode	= '' />
		<cfset var CustomHELP =''/>
		<cfset var CustomSTOP =''/>
		<cfset var customMessageQuery =''/>
		<cfset var GetCustomResponse	= '' />
		<cfset var RetVarSetCustomStandardReply	= '' />
		<cfset var xmlControlStringParsed	= '' />
		<cfset var _index	= '' />
		<cfset var OPTINGROUP	= 0 />
		<cfset var children	= {} />
		<cfset var _Q	= {} />
		<cfset var AddSubscriberBatchRecord	= '' />
		<cfset var AddSubscriberBatchRecordResult	= '' />
		<cfset var campaignGroupId	= '' />
		<cfset var RetVarGetAssignedShortCode = '' />
		<cfset var NewCloneDesc = "" />

		<cfif arguments.inpBatchId GT 0 >

			<cftry>
				<!---first we need to select information of mirror campaign --->
				<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
					SELECT
						BATCHID_BI,
						USERID_INT,
						RXDSLIBRARY_INT,
						RXDSELEMENT_INT,
						RXDSSCRIPT_INT,
						USERBATCHNUMBER_INT,
						CREATED_DT,
						DESC_VCH,
						LASTUPDATED_DT,
						GROUPID_INT,
						ALTSYSID_VCH,
						XMLCONTROLSTRING_VCH,
						ACTIVE_INT,
						SERVICEPASSWORD_VCH,
						ALLOWDUPLICATES_TI,
						REDIAL_INT,
						CALLERID_VCH,
						CONTACTGROUPID_INT,
						CONTACTTYPES_VCH,
						CONTACTNOTE_VCH,
						CONTACTISAPPLYFILTER,
						EMS_FLAG_INT,
						DEFAULTCID_VCH,
						TEMPLATEID_INT,
						TEMPLATETYPE_TI,
						ShortCodeId_int
					 FROM
					 	SIMPLEOBJECTS.BATCH
				 	WHERE
				 		BATCHID_BI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpBatchId#">
				 		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					 LIMIT 1
				</cfquery>

				<cfif GetCampaign.recordCount NEQ 1 >
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.ERRMESSAGE = "Campaign doesn't exist." />
		            <cfreturn dataout>
				</cfif>

				<cfset xmlControlString = #GetCampaign.XMLCONTROLSTRING_VCH#>

				<cfset xmlControlStringParsed = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & "#xmlControlString#</XMLControlStringDoc>") />

				<cfif (structKeyExists(xmlControlStringParsed,'XmlChildren') AND isArray(xmlControlStringParsed.XmlChildren) AND arrayIsDefined(xmlControlStringParsed.XmlChildren,1)
					AND structKeyExists(xmlControlStringParsed.XmlChildren[1],'XmlChildren') AND isArray(xmlControlStringParsed.XmlChildren[1].XmlChildren)
					AND !arrayIsEmpty(xmlControlStringParsed.XmlChildren[1].XmlChildren))>
					<cfloop array="#xmlControlStringParsed.XmlChildren[1].XmlChildren#" index="children">
						<cfif children.xmlName EQ 'RXSSCSC'>
							<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
								<cfset _index = 1>
								<cfloop array="#children.XmlChildren#" index="_Q">
										<cfif arrayFindNoCase(['OPTIN'],_Q.XmlAttributes.TYPE) GT 0>
											<cfset OPTINGROUP = _Q.XmlAttributes.OIG>
											<cfbreak>
										</cfif>
									<cfset _index++>
								</cfloop>
							</cfif>
							<cfbreak>
						</cfif>
					</cfloop>
				</cfif>
				<cfif IsNumeric(GetCampaign.GROUPID_INT)>
					<cfset campaignGroupId = GetCampaign.GROUPID_INT>
				<cfelse>
					<cfset campaignGroupId = 0>
				</cfif>


				<!--- Validate and verify target short code is still available to user --->
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetAssignedShortCode">
		 			<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
		 			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	 			</cfinvoke>

	 			<!--- Allow UI or API to specify new description - limit 255 characters --->
	 			<cfif LEN(TRIM(inpDesc)) EQ 0 >
		 			<cfset NewCloneDesc = #LEFT('CLONE: #LSDateFormat(NOW(), 'yyyy-mm-dd')# : ' & GetCampaign.Desc_vch, 255)# />
		 		<cfelse>
		 			<cfset NewCloneDesc = #LEFT(inpDesc, 255)# />
	 			</cfif>

				<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
	                INSERT INTO simpleobjects.batch
	                    (
							USERID_INT,
							RXDSLIBRARY_INT,
							RXDSELEMENT_INT,
							RXDSSCRIPT_INT,
							USERBATCHNUMBER_INT,
							CREATED_DT,
							DESC_VCH,
							LASTUPDATED_DT,
							GROUPID_INT,
							ALTSYSID_VCH,
							XMLCONTROLSTRING_VCH,
							ACTIVE_INT,
							SERVICEPASSWORD_VCH,
							ALLOWDUPLICATES_TI,
							REDIAL_INT,
							CALLERID_VCH,
							CONTACTGROUPID_INT,
							CONTACTTYPES_VCH,
							CONTACTNOTE_VCH,
							CONTACTISAPPLYFILTER,
							EMS_FLAG_INT,
							DEFAULTCID_VCH,
							TEMPLATEID_INT,
							TEMPLATETYPE_TI,
							ShortCodeId_int
	                    )
	                VALUES
	                	(
	                      	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSLIBRARY_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSELEMENT_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.RXDSSCRIPT_INT#">,
	                        0,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(NewCloneDesc, 255)#">,
	                        NOW(),
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignGroupId#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.ALTSYSID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlControlString#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.ACTIVE_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.SERVICEPASSWORD_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetCampaign.ALLOWDUPLICATES_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.REDIAL_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CALLERID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTGROUPID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTTYPES_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.CONTACTNOTE_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.CONTACTISAPPLYFILTER#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.EMS_FLAG_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCampaign.DEFAULTCID_VCH#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.TEMPLATEID_INT#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.TEMPLATETYPE_TI#">,
	                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetAssignedShortCode.SHORTCODEID#">

	                    )
	            </cfquery>

	            <cfquery name="AddSubscriberBatchRecord" datasource="#Session.DBSourceEBM#" result="AddSubscriberBatchRecordResult">
	                INSERT INTO
	                	simplelists.subscriberbatch
	                (
	                	GroupId_bi,
	                	BatchId_bi,
	                	UserId_int,
	                	Created_dt
	                )
	                VALUES
	                (
	                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OPTINGROUP#">,
	                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#AddBatch.generated_key#">,
	                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
	                	NOW()
	                )

	            </cfquery>

	            <cfset NEXTCAMPAINID = #AddBatch.generated_key#>


	            <!--- Biullet PRoof: Only set this if TRIM(RetVarGetAssignedShortCode.SHORTCODE) is available and is still valid --->
	            <cfif LEN(TRIM(RetVarGetAssignedShortCode.SHORTCODE)) GT 0>

					<!--- Check for existing Custom Help/Stop --->
		            <cfquery name="GetCustomResponse" datasource="#Session.DBSourceEBM#">
		                 SELECT
		                    Content_vch
		                 FROM
		                    sms.smsshare
		                 WHERE
		                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(RetVarGetAssignedShortCode.SHORTCODE)#">
		                 AND
		                    Keyword_vch =<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="HELP">
		                 AND
		                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
		            </cfquery>

		            <cfif GetCustomResponse.RecordCount GT 0>

						<!--- Write generic keyword HELP STOP   --->
						<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
							<cfinvokeargument name="inpBatchId" value="#NEXTCAMPAINID#">
							<cfinvokeargument name="inpKeyword" value="HELP">
							<cfinvokeargument name="inpResponse" value="#GetCustomResponse.Content_vch#">
							<cfinvokeargument name="inpShortCode" value="#TRIM(RetVarGetAssignedShortCode.SHORTCODE)#">
						</cfinvoke>

					</cfif>

					<!--- Check for existing Custom Help/Stop --->
		            <cfquery name="GetCustomResponse" datasource="#Session.DBSourceEBM#">
		                 SELECT
		                    Content_vch
		                 FROM
		                    sms.smsshare
		                 WHERE
		                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(RetVarGetAssignedShortCode.SHORTCODE)#">
		                 AND
		                    Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="STOP">
		                 AND
		                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
		            </cfquery>

		            <cfif GetCustomResponse.RecordCount GT 0>

						<!--- Write generic keyword HELP STOP   --->
						<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
							<cfinvokeargument name="inpBatchId" value="#NEXTCAMPAINID#">
							<cfinvokeargument name="inpKeyword" value="STOP">
							<cfinvokeargument name="inpResponse" value="#GetCustomResponse.Content_vch#">
							<cfinvokeargument name="inpShortCode" value="#TRIM(RetVarGetAssignedShortCode.SHORTCODE)#">
						</cfinvoke>

					</cfif>

				</cfif>

				<!--- What has this user been up to - good metrics on who is using Sire for what - also good for security tracking  --->
	            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="Campaign Cloned">
					<cfinvokeargument name="operator" value="inpBatchId = #inpBatchId#">
				</cfinvoke>

				<!---get batchid of recent inserted record --->
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.NEXTCAMPAINID = #AddBatch.generated_key#>
				<cfset dataout.MESSAGE = 'Cloned successfully.'>

			<cfcatch TYPE="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

				<cfreturn dataout>
			</cfcatch>

			</cftry>

		<cfelse>
			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = 'Can not find this campaign.'>
		</cfif>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="ConsoleSummaryKeywords" access="remote" output="false" returntype="any" hint="Get Keyword Stats - Combo available total , and what is activly used. ">
		<cfargument name="inpLimit" TYPE="any" hint="Limit how many to get" required="false" default="3" />

	   	<cfset var dataout = {} />
		<cfset var GetSMSResults = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarConsoleSummaryMostActiveKeywords = '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.KEYWORDSINUSE = 0 />
    	<cfset dataout.KEYWORDLIMIT = 0 />
    	<cfset dataout.TOPACTIVE = ArrayNew(1) />

		<cftry>

			<!--- Validate user keywords counts in use from DB --->
			<cfinvoke method="GetKeywordsCountInUse" returnvariable="RetVarGetKeywordsCountInUse"></cfinvoke>

			<cfset dataout.KEYWORDSINUSE = RetVarGetKeywordsCountInUse.KEYWORDSINUSE />

			<!--- Validate user keyword length limits from DB --->
			<cfinvoke method="GetCurrentUserPlanKeywordLimits" returnvariable="RetVarGetCurrentUserPlanKeywordLimits"></cfinvoke>

			<cfset dataout.KEYWORDLIMIT = RetVarGetCurrentUserPlanKeywordLimits.KEYWORDLIMIT />

	        <cfinvoke method="ConsoleSummaryMostActiveKeywords" returnvariable="RetVarConsoleSummaryMostActiveKeywords">
	        	<cfinvokeargument name="inpLimit" value="#arguments.inpLimit#">
	        </cfinvoke>

			<cfset dataout.TOPACTIVE = RetVarConsoleSummaryMostActiveKeywords.TOPACTIVE />

	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />

		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />

	</cffunction>

	<cffunction name="ConsoleSummaryMostActiveKeywords" access="remote" output="false" returntype="any" hint="Get Most Active Keywords Stats ">
		<cfargument name="inpLimit" TYPE="any" hint="Limit how many to get" required="false" default="3" />

	   	<cfset var dataout = {} />
		<cfset var GetResults = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var ObjBuff = '' />

		<!--- Set default return values --->
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.TOPACTIVE = ArrayNew(1) />

		<cftry>

			<!--- Not filtering for active - summary will display even if keyword has been removed - if need to filter active than need to add WHERE k.Active_int = 1  --->
			<cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
		    	SELECT
					COUNT(*) AS TotalCount,
					k.Keyword_vch,
					b.Desc_vch,
					ire.BatchId_bi
				FROM
					simplexresults.ireresults AS ire
				INNER JOIN
					simpleobjects.batch as b
				ON
					(b.batchid_bi = ire.BatchId_bi AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
				INNER JOIN
					sms.keyword k
				ON
					k.BatchId_bi = b.BatchId_bi
				WHERE
					ire.Created_dt > DATE_ADD(NOW(), INTERVAL -30 DAY)
				GROUP BY
					k.Keyword_vch, b.Desc_vch, ire.BatchId_bi
				ORDER BY
					TotalCount DESC
				LIMIT
					 <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpLimit#">
		    </cfquery>

			<cfif GetResults.RecordCount EQ 0>

				<cfset ObjBuff = StructNew() />

				<cfset ObjBuff.BATCHID = 0 />
				<cfset ObjBuff.COUNT = 0 />
				<cfset ObjBuff.KEYWORD = "N/A"/>
				<cfset ObjBuff.DESC = "No active keywords found in the last 30 days." />

				<cfset ArrayAppend(dataout.TOPACTIVE,ObjBuff)>

			</cfif>

			<cfloop query="GetResults">

				<cfset ObjBuff = StructNew() />

				<cfset ObjBuff.BATCHID = #GetResults.BatchId_bi# />
				<cfset ObjBuff.COUNT = #GetResults.TotalCount# />
				<cfset ObjBuff.KEYWORD = #GetResults.Keyword_vch# />
				<cfset ObjBuff.DESC = #HTMLEditFormat(GetResults.Desc_vch)# />

				<cfset ArrayAppend(dataout.TOPACTIVE,ObjBuff)>

			</cfloop>

	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />

		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

        </cftry>

        <cfreturn dataout />

	</cffunction>

	<cffunction name="GetAssignedShortCode" access="public" output="false" hint="Get short code to that is assigned or is to be assigned based on input Keyword and Batch. Will check Batch first, then any keywords assigned to this Batch, Then any existing short code passed in, then users default. In that order.">
		<cfargument name="inpBatchId" TYPE="string" required="false" default="0" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpShortCode" TYPE="string" default="" required="no" hint="Shortcode to check if specified - default is empty string - not specified by default" />
		<cfargument name="inpUserIDRequest" TYPE="string" default="" required="no" hint="for cases call funtion from cronjob with out batchid, shortcode code" />
		<cfargument name="inpUserId" required="false" default="#Session.USERID#"/>
		<!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var getShortCode = structNew() />
		<cfset var GetCampaign = '' />
		<cfset var getShortCode = '' />
		<cfset var getUserShortCode = '' />
		<cfset var VerifyShortCodeId = '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.SHORTCODE = "" />
		<cfset dataout.SHORTCODEID = "0" />

		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cfset dataout.QUERYOUTPUT = '' />

		<cfset var inpUserIDRequest = arguments.inpUserIDRequest />
		<cfif inpUserIDRequest EQ "">
			<cfset var inpUserIDRequest = session.USERID />
		</cfif>


		<cftry>

			<!--- Verify valid session --->
			<cfif Session.USERID LT 1 and inpUserIDRequest LT 0>
				<cfthrow type="Application" message="Session is expired."/>
			</cfif>

			<!--- Check batchID first --->
			<cfif inpBatchId GT 0>

	            	<!--- Get the existing campaign info -- also makes sure current user owns the batch--->
				<cfquery name="GetCampaign" datasource="#Session.DBSourceEBM#">
					SELECT
						ShortCodeId_int
					 FROM
					 	simpleobjects.batch
				 	WHERE
				 		BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpBatchId#">
				 	AND
				 		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				</cfquery>

				<cfif GetCampaign.ShortCodeId_int GT 0>

					<!--- Bullet Proof: Now verify user still has access to this short code Id --->
					<cfquery datasource="#Session.DBSourceEBM#" name="VerifyShortCodeId">
	                    SELECT
	                        sc.ShortCodeId_int,
	                        sc.ShortCode_vch,
	                        scr.RequesterId_int,
	                        sc.Classification_int
	                    FROM
	                        sms.shortcode sc
	                    JOIN
	                        sms.shortcoderequest scr
	                    ON
	                        sc.ShortCodeId_int = scr.ShortCodeId_int
	                    WHERE
	                        scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
	                    AND
	                    	sc.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCampaign.ShortCodeId_int#">
	                    ORDER BY
	                    	scr.IsDefault_ti DESC,
	                    	scr.ShortCodeRequestId_int ASC
		            </cfquery>

		            <cfif VerifyShortCodeId.RECORDCOUNT GT 0>

			            <!--- First level check is successful  --->
			            <cfset dataout.RXRESULTCODE = 1 />
			            <cfset dataout.SHORTCODE = VerifyShortCodeId.ShortCode_vch />
			            <cfset dataout.SHORTCODEID = VerifyShortCodeId.ShortCodeId_int />

					  <!--- used for variouse short code display options --->
					  <cfset session.Shortcode = VerifyShortCodeId.ShortCode_vch />

			            <cfreturn dataout />

		            </cfif>

				<cfelse>

					<!--- Check in batch via keyword --->
					<!--- Only should ever be called in session - no need for session check here --->
		            <cfquery datasource="#Session.DBSourceEBM#" name="getShortCode">
		                    SELECT
								sc.ShortCodeId_int,
								sc.ShortCode_vch,
								scr.RequesterId_int,
								sc.Classification_int
							FROM
								sms.keyword k
							JOIN
								sms.shortcoderequest scr
							ON
								scr.ShortCodeRequestId_int = k.ShortCodeRequestId_int
							INNER JOIN
								sms.shortcode sc
							ON
								sc.ShortCodeId_int = scr.ShortCodeId_int
							WHERE
		                    	k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
		                    AND
		                    	k.Active_int = 1
		                    AND
		                        scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
		                    ORDER BY
		                    	scr.IsDefault_ti DESC,
		                    	scr.ShortCodeRequestId_int ASC
		            </cfquery>

		            <cfif getShortCode.RecordCount GT 0>

			            <!--- Bullet Proof: Now verify user still has access to this short code Id --->
						<cfquery datasource="#Session.DBSourceEBM#" name="VerifyShortCodeId">
		                    SELECT
		                        sc.ShortCodeId_int,
		                        sc.ShortCode_vch,
		                        scr.RequesterId_int,
		                        sc.Classification_int
		                    FROM
		                        sms.shortcode sc
		                    JOIN
		                        sms.shortcoderequest scr
		                    ON
		                        sc.ShortCodeId_int = scr.ShortCodeId_int
		                    WHERE
		                        scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
		                    AND
		                    	sc.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getShortCode.ShortCodeId_int#">
		                    ORDER BY
		                    	scr.IsDefault_ti DESC,
		                    	scr.ShortCodeRequestId_int ASC
			            </cfquery>

			            <cfif VerifyShortCodeId.RECORDCOUNT GT 0>

				            <!--- First level check is successful  --->
				            <cfset dataout.RXRESULTCODE = 1 />
				            <cfset dataout.SHORTCODE = VerifyShortCodeId.ShortCode_vch />
				            <cfset dataout.SHORTCODEID = VerifyShortCodeId.ShortCodeId_int />

						  <!--- used for variouse short code display options --->
						  <cfset session.Shortcode = VerifyShortCodeId.ShortCode_vch />

				            <cfreturn dataout />

			            </cfif>

		            </cfif>

				</cfif>

			</cfif>

			<cfif LEN(TRIM(arguments.inpShortCode)) GT 0>

				<!--- Verify specified short code is actually still available to the user --->
				<cfquery datasource="#Session.DBSourceEBM#" name="getShortCode">
					SELECT
					    sc.ShortCodeId_int,
					    sc.ShortCode_vch,
					    scr.RequesterId_int,
					    sc.Classification_int
					FROM
					    sms.shortcode sc
					JOIN
					    sms.shortcoderequest scr
					ON
					    sc.ShortCodeId_int = scr.ShortCodeId_int
					WHERE
					    scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
					AND
						sc.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpShortCode)#">
					AND
						scr.Status_int = 1
					ORDER BY
						scr.IsDefault_ti DESC,
						scr.ShortCodeRequestId_int ASC
				</cfquery>

				<cfif getShortCode.RECORDCOUNT GT 0>
				  	<!--- Found in active shortcode by name success --->
				      <cfset dataout.RXRESULTCODE = 1 />
				      <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
				      <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
					 <!--- used for variouse short code display options --->
 					<cfset session.Shortcode = getShortCode.ShortCode_vch />

					<cfreturn dataout />

				</cfif>

			</cfif>

			<!--- If not found in any of the above methods then default to account short code --->
	        <!--- Get the oldest assigned short code on this account and treat it as the default --->
        	<cfquery datasource="#Session.DBSourceEBM#" name="getShortCode">
                    SELECT
                        sc.ShortCodeId_int,
                        sc.ShortCode_vch,
                        scr.RequesterId_int
                    FROM
                        sms.shortcode sc
                    JOIN
                        sms.shortcoderequest scr
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE
                        scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserIDRequest#">
                    ORDER BY
                    	scr.IsDefault_ti DESC,
                    	scr.ShortCodeRequestId_int ASC
                    LIMIT 1
            </cfquery>

			<cfif getShortCode.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
				<cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>

				<!--- used for variouse short code display options --->
				<cfset session.Shortcode = getShortCode.ShortCode_vch />

				<cfreturn dataout />
			</cfif>

			<!--- If everthing fails.... then no short code available --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "No shortcode found." />
            <cfset dataout.ERRMESSAGE = "" />

			<cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetBasicCampaignTemplateList" access="public" output="false" hint="Get Basic Top Section Templates">
		<cfargument name="inpGroupId" type="number" required="no" default="0">

		<cfset var dataout	= {} />
		<cfset var GetBasicTemplates	= '' />
		<cfset var templateList	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>

		<cftry>

			<!--- Filter on Basic Flag is 1 only --->
			<cfquery name="GetBasicTemplates" result="templateList" datasource="#Session.DBSourceREAD#">
			  SELECT
			    GROUP_CONCAT(DISTINCT txc.template_categoryID_int ORDER BY txc.template_categoryID_int ASC ) AS CatListID,
			    tx.TID_int,
			    tx.Name_vch,
			    tx.Description_vch,
			    txc.template_categoryID_int,
			    tx.Image_vch,
			    tc.GroupId_int,
			    tx.Type_int
			  FROM
			    simpleobjects.templatexmlcategory txc
			  INNER JOIN
			    simpleobjects.templatesxml tx
			  ON
			    tx.TID_int = txc.template_xmlID_int
			  INNER JOIN
			    simpleobjects.template_category tc
			  ON
			    tc.CID_int = txc.template_categoryID_int
			  WHERE
			  	tx.BasicFlag_ti = 1
			  AND
			    tx.Active_int = 1
			  GROUP BY
			    tx.TID_int
			  ORDER BY
			  	tx.Order_int
			</cfquery>

			<cfset dataout.QUERYRES = GetBasicTemplates>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />

		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>

	<cffunction name="GetTemplateCategoriesList" access="public" output="false" hint="Get Set of Top Level Categories for Templates">

		<cfset var dataout	= {} />
		<cfset var GetCategories	= '' />
		<cfset var templateList	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>

		<cftry>

			<!--- Filter on Basic Flag is 1 only --->
			<cfquery name="GetCategories" result="templateList" datasource="#Session.DBSourceREAD#">
			  SELECT
			  	CGID_int,
			    CGNAME_vch,
			    Desc_vch
			  FROM
			    simpleobjects.templatecategorygroups
			  WHERE
			  	Active_int = 1
			  ORDER BY
			   	Order_int ASC
			</cfquery>

			<cfset dataout.QUERYRES = GetCategories>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />

		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>

	<cffunction name="GetTemplateCategoriesSubCategoriesList" access="public" output="false" hint="Get Set of Sub Level Categories for Top Level Templates Categories">
		<cfargument name="inpCategoryId" type="number" required="yes">

		<cfset var dataout	= {} />
		<cfset var GetSubCategories	= '' />
		<cfset var templateList	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>

		<cftry>

			<!--- Filter on Basic Flag is 1 only --->
			<cfquery name="GetSubCategories" result="templateList" datasource="#Session.DBSourceREAD#">
			  SELECT
					tc.CID_int,
					DisplayName_vch,
					Description_txt,
					CategoryName_vch,
					GroupId_int,
					tc.ImgSource_vch
				FROM
					simpleobjects.template_category tc
				INNER JOIN
					simpleobjects.templatexmlcategory txc
				ON
					tc.CID_int = txc.template_categoryID_int
				WHERE
					tc.GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCategoryId#">
				GROUP BY
					tc.CID_int
				ORDER BY
					tc.Order_int ASC
			</cfquery>

			<cfset dataout.QUERYRES = GetSubCategories>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />

		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>


	<cffunction name="GetCampaignSubCategoryTemplateList" access="public" output="false" hint="Get Templates foir the specified Category and Sub category">
		<cfargument name="inpCategoryId" type="number" required="yes">
		<cfargument name="inpSubCategoryId" type="number" required="yes">

		<cfset var dataout	= {} />
		<cfset var GetSubCategoryTemplates	= '' />
		<cfset var templateList	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>

		<cftry>

			<!--- Filter on Basic Flag is 1 only --->
			<cfquery name="GetSubCategoryTemplates" result="templateList" datasource="#Session.DBSourceREAD#">
			  SELECT
			    GROUP_CONCAT(DISTINCT txc.template_categoryID_int ORDER BY txc.template_categoryID_int ASC ) AS CatListID,
			    tx.TID_int,
			    tx.Name_vch,
			    tx.Description_vch,
			    txc.template_categoryID_int,
			    tx.Image_vch,
			    tc.GroupId_int,
			    tx.Type_int,
			    txc.Order_int
			  FROM
			    simpleobjects.templatexmlcategory txc
			  INNER JOIN
			    simpleobjects.templatesxml tx
			  ON
			    tx.TID_int = txc.template_xmlID_int
			  INNER JOIN
			    simpleobjects.template_category tc
			  ON
			    tc.CID_int = txc.template_categoryID_int
			  WHERE
			  	tc.CID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpSubCategoryId#">
			  AND
			    tx.Active_int = 1
			  GROUP BY
			    tx.TID_int
			  ORDER BY
			  	  txc.Order_int
			</cfquery>

			<cfset dataout.QUERYRES = GetSubCategoryTemplates>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />

		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>

	<cffunction name="GetAllActiveCampaignTemplates" access="public" output="false" hint="Get All Active Templates">

		<cfset var dataout	= {} />
		<cfset var GetAllTemplates	= '' />
		<cfset var templateList	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>

		<cftry>

			<!--- Filter on Basic Flag is 1 only --->
			<cfquery name="GetAllTemplates" result="templateList" datasource="#Session.DBSourceREAD#">
				SELECT
				    GROUP_CONCAT(DISTINCT txc.template_categoryID_int ORDER BY txc.template_categoryID_int ASC ) AS CatListID,
				    tx.TID_int,
				    tx.Name_vch,
				    tx.Description_vch,
				    txc.template_categoryID_int,
				    tx.Image_vch,
				    tc.GroupId_int,
				    tx.Type_int
				  FROM
				    simpleobjects.templatexmlcategory txc
				  INNER JOIN
				    simpleobjects.templatesxml tx
				  ON
				    tx.TID_int = txc.template_xmlID_int
				  INNER JOIN
				    simpleobjects.template_category tc
				  ON
				    tc.CID_int = txc.template_categoryID_int
				  WHERE
				    tx.Active_int = 1
				  GROUP BY
				    tx.TID_int
			</cfquery>

			<cfset dataout.QUERYRES = GetAllTemplates>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />

		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>
	 	<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCompanyName" access="remote" output="false" hint="Update Organizational Information - Company Name Desc">
		<cfargument name="OrganizationId_int" type="string" required="false" default="0">
    	<cfargument name="OrganizationName_vch" TYPE="string" hint="The new company name for this account" required="no" default="" />

        <cfset var dataout = {} />
        <cfset var UpdateOrginazation= '' />
        <cfset var AddOrginazation= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

	        <!--- Secured to Session.User by query --->
	    	<cfif OrganizationId_int GT 0>
				<!--- UPDATE --->
                <cfquery name="UpdateOrginazation" datasource="#Session.DBSourceEBM#">
                    UPDATE
                    	simpleobjects.organization
                    SET
                        OrganizationName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationName_vch#">
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    AND
                        OrganizationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OrganizationId_int#">
                </cfquery>
            <cfelse>
            	<!--- INSERT --->
                <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#">
                    INSERT INTO
                    	simpleobjects.organization
                        (
                            OrganizationName_vch,
                            UserId_int
                        )
                    VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationName_vch#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                        )
                </cfquery>

            </cfif>

			<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="getOptionListStyle" access="public" output="false">
		<cfargument name="AF" type="string" required="true" default="">
		<cfargument name="ID" type="numeric" required="true" default="1">
		<cfswitch expression="#AF#">
			<cfcase value="NUMERICPAR">
				<cfreturn ID & ')'>
			</cfcase>
			<cfcase value="NUMERIC">
				<cfreturn ID & ' for'>
			</cfcase>
			<cfcase value="ALPHAPAR">
				<cfreturn Chr(64+ID) & ')'>
			</cfcase>
			<cfcase value="ALPHA">
				<cfreturn Chr(64+ID) & ' for'>
			</cfcase>
			<cfcase value="HIDDEN">
				<cfreturn ''>
			</cfcase>
			<cfdefaultcase>
				<cfreturn ''>
			</cfdefaultcase>
		</cfswitch>
	</cffunction>


	<cffunction name="UpdateMLPWithKeywordAndShortCode" access="remote" output="false" hint="Update MLP Content with current keyowrd and short code data">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId that owns this MLP" />
		<cfargument name="inpKeyword" required="false" default="" hint="A keyword to associate with the Batch">
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to Set HELP / STOP for." />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var VerifyUser	= '' />
		<cfset var RetCPPXData	= '' />
		<cfset var RetVarDoDynamicTransformations	= '' />
		<cfset var RetSaveMLP	= '' />
		<cfset var inpFormData	= '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />


		<cfset dataout.QUERYOUTPUT = '' />


		<cftry>

            <!--- Read in Current Short Code --->
	    	<!--- Read in Current Keyword --->

	    	<!--- Secure by session User Id - dont let other users web tools hack data they dont own --->
	    	<cfquery name="VerifyUser" datasource="#Session.DBSourceEBM#">
		    	SELECT
			    	MLPId_int
		        FROM
                	simpleobjects.batch
		        WHERE
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		    </cfquery>


		    <cfif VerifyUser.RecordCount GT 0>

		    	<cfif VerifyUser.MLPId_int GT 0>

					<!--- Create an MLP --->
					<!--- Read exisitng template data --->
					<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
						 <cfinvokeargument name="ccpxDataId" value="#VerifyUser.MLPId_int#">
					</cfinvoke>

					<!---Create a data set to run against dynamic data --->
		            <cfset inpFormData = StructNew() />

		            <cfset inpFormData.Keyword = #inpKeyword# />
		            <cfset inpFormData.ShortCode = #inpShortCode# />

					<!--- Do the MLP transforms here --->
		            <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
		                <cfinvokeargument name="inpResponse_vch" value="#RetCPPXData.RAWCONTENT#">
		                <cfinvokeargument name="inpContactString" value="">
		                <cfinvokeargument name="inpRequesterId_int" value="">
		                <cfinvokeargument name="inpFormData" value="#inpFormData#">
		                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
		                <cfinvokeargument name="inpShortCode" value="">
		                <cfinvokeargument name="inpBatchId" value="">
		                <cfinvokeargument name="inpBrandingOnly" value="1">
		            </cfinvoke>

		            <!--- Update the current template data --->
					<cfset RetCPPXData.RAWCONTENT = RetVarDoDynamicTransformations.RESPONSE />

					<cfinvoke component="session.sire.models.cfc.mlp" method="SaveMLP" returnvariable="RetSaveMLP">
						<cfinvokeargument name="ccpxDataId" value="#VerifyUser.MLPId_int#">
						<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
						<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
					</cfinvoke>

					<!---
						<cfset dataout.READMLP = "#SerializeJSON(RetCPPXData)#" />
						<cfset dataout.SAVEMLP = "#SerializeJSON(RetSaveMLP)#" />
						<cfset dataout.inpFormData = "#SerializeJSON(inpFormData)#" />


						<cfset dataout.RAWCONTENT = "#RetCPPXData.RAWCONTENT#" />
					--->

				</cfif>

	    	</cfif>

	    	<cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction access="public" name="updateBatchXMLCS" hint="">
		<cfargument name="inpBatchID" TYPE="numeric" required="true" />
		<cfargument name="inpXMLCS" TYPE="string" required="true" />

		<cfset var updateBatch = {} />
		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
	    	<cfquery name="updateBatch" datasource="#Session.DBSourceEBM#">
		    	UPDATE
		    		simpleobjects.batch
			   	SET
			   		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#arguments.inpXMLCS#">
		        WHERE
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpBatchID#">
                LIMIT 1
		    </cfquery>
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfcatch TYPE="any">
				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="AddListToQueueForBlast" access="public" output="false" hint="Add a contact list to the queue for blast">
		<cfargument name="inpBatchId" required="true" />
		<cfargument name="inpShortCode" required="true" />
		<cfargument name="INPGROUPID" required="true" />
		<cfargument name="CONTACTTYPES" required="true" />
		<cfargument name="CONTACTFILTER" required="true" />
		<cfargument name="IsApplyFilter" required="false" default="0"/>
		<cfargument name="inpBlastLogId" required="true"/>
		<cfargument name="inpTimeZone" type="string" required="false" default="0" hint="Default calendar time zone setting is none - local to device = 0"/>


		<cfset var dataout = {}/>
		<cfset var GetContacts = '' />
		<cfset var inpContactTypeId = '3' />
        <cfset var contacStringFilter = "">
        <cfset var contactIdListByCdfFilter = "">
        <cfset var cdfAndContactStringFilterObj = '' />
        <cfset var retValCDF = ""/>
        <cfset var InsertQueue	= '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cfset var cdfLoopIndex = 1>
		<cfset var cdfFilter = [] />
		<cfset var cdfItem = '' />
		<cfset var cdfRowItem = '' />

		<cftry>

			<cfif TRIM(arguments.inpTimeZone) EQ "" OR !IsNumeric(arguments.inpTimeZone)>
				<cfset arguments.inpTimeZone = 0 />
			</cfif>

			<cfif isNumeric(arguments.INPGROUPID)>
				<cfset arguments.INPGROUPID = 0/>
			</cfif>

            <cfif arguments.CONTACTFILTER NEQ ''>
				<!---now build contact string and contact list by cdf --->
                <cfset cdfAndContactStringFilterObj = deserializeJson(arguments.CONTACTFILTER)>
                <cfset contacStringFilter  = cdfAndContactStringFilterObj.CONTACTFILTER>
                <cfset cdfFilter = cdfAndContactStringFilterObj.CDFFILTER>

                <!--- <cfinvoke method="GetContactListByCDF" returnvariable="retValCDF" component="session.sire.models.cfc.import-contact">
                    <cfinvokeargument name="CDFData" value="#cdfAndContactStringFilterObj.CDFFILTER#">
                    <cfinvokeargument name="INPGROUPID" value="#arguments.INPGROUPID#" >
                </cfinvoke>
                <cfif retValCDF.RXRESULTCODE NEQ 1>
                    <cfthrow MESSAGE="Error getting CDF data" TYPE="Any" detail="#retValCDF.MESSAGE# & #retValCDF.ERRMESSAGE#" errorcode="-2">
                </cfif>
                <cfset contactIdListByCdfFilter = retValCDF.CONTACTLISTBYCDF> --->
            </cfif>

    		<cfquery result="InsertQueue" datasource="#Session.DBSourceEBM#">
    			INSERT INTO
    				simplequeue.batch_load_queue
    				(
    					UserId_int,
    					BatchId_bi,
    					GroupId_int,
    					ContactString_vch,
    					Status_ti,
    					ContactType_int,
    					ShortCode_vch,
    					BlastLogId_int,
					TimeZone_int
    				)
	                SELECT
	                	<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
    					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>,
    					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.INPGROUPID#"/>,
    					ContactString_vch,
    					<cfqueryparam cfsqltype="cf_sql_integer" value="#BLAST_LOG_QUEUE_READY#"/>,
    					<cfqueryparam cfsqltype="cf_sql_integer" value="3"/>,
    					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>,
    					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBlastLogId#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeZone#"/>
	                FROM
	                    simplelists.groupcontactlist
                    INNER JOIN
	                    simplelists.contactstring
                    ON
                    	simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
                    INNER JOIN
                    	simplelists.contactlist
                	ON
                		simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi

					<cfif arraylen(cdfFilter) GT 0 >
	                    LEFT JOIN
	                        simplelists.contactvariable cv
	                    ON
	                        simplelists.contactstring.ContactId_bi = cv.ContactId_bi

	                    GROUP BY
	                    	ContactString_vch

                        HAVING
                        (
                            <cfloop array="#cdfFilter#" index="cdfItem">
                                <cfif cdfLoopIndex GT 1>
                                     OR
                                </cfif>
                                 (cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#"/>
                                  <cfloop array="#cdfItem.ROWS#" index="cdfRowItem">
                                      And cv.VariableValue_vch
                                    <cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#cdfRowItem.VALUE#%"/>
                                    <cfelse>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#cdfRowItem.VALUE#"/>
                                    </cfif>
                                  </cfloop>
                                 )
                                 <cfset cdfLoopIndex++/>
                            </cfloop>
                        )
                        AND
                        	COUNT(cv.ContactId_bi) = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" value="#arraylen(cdfFilter)#"/>
                    	AND
                    <cfelse>
		                WHERE
                    </cfif>

	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	                AND
	                    ContactType_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#">
                    AND
                        simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPGROUPID#">
	                <cfif contacStringFilter NEQ "">
	                       AND  simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#contacStringFilter#%">
	                </cfif>
	                <cfif contactIdListByCdfFilter NEQ "">
	                     AND
	                         simplelists.contactstring.contactid_bi IN  (<CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#contactIdListByCdfFilter#"  list="yes">)
	                </cfif>

	                AND
	                    ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)


					<!--- This is OLD code that soes not apply to Sire - I am commenting it out, but it should never have made it here  --->
<!---
	                <cfif Session.AdditionalDNC NEQ "" AND isnumeric(Session.AdditionalDNC)>
	                AND
	                    ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = #Session.AdditionalDNC#)
	                </cfif>
--->

	                <!--- Session User DNC --->
                    <cfif inpSkipDNCCheck EQ 0 >
                        AND 0 = (SELECT COUNT(oi.ContactString_vch) FROM simplelists.optinout AS oi WHERE OptOut_dt IS NOT NULL AND OptIn_dt IS NULL AND ShortCode_vch = '#GetShortCodeData.ShortCode_vch#' AND oi.ContactString_vch = simplelists.contactstring.ContactString_vch )
                    </cfif>


<!--- How are duplicates being removed? Dont let them get on list in the first place? --->




    		</cfquery>

            <cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
				<cfset dataout.TYPE = cfcatch.type />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<!--- Render via cfc for primarily for being able to add on the fly without having to refresh and reparse data --->
	<cffunction name="RenderCPNew" access="remote" output="false" hint="Render a Control Point in HTML ">
	    <cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>
	    <cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
	    <cfargument name="inpPreview" type="any" required="false" default="0" hint="Just preview - no edit options"/>
	    <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
	    <cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
	    <cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />
	    <cfargument name="inpdisplayIntervalType" TYPE="any" required="no" default="0" hint="Input display interval - Optional" />
	    <cfargument name="inpTemplateId" TYPE="any" required="no" default="0" hint="Input template id - Optional" />

	    <!--- Default Return Structure --->
	    <cfset var dataout  = {} />

	    <!--- Set default return values --->
	    <cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.BATCHID = "" />
	    <cfset dataout.PID = "" />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.HTML  = "" />

	    <cfset var DebugStr = '' />
	    <cfset var CP_HTML = "" />
	    <cfset var StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
	    <cfset var subcriberArray = ''/>
	    <cfset var subcriberList = ''/>
	    <cfset var _OPTION = ''/>
	    <cfset var _ESOPTION = ''/>
	    <cfset var listStyleIndex = ''/>
	    <cfset var item = ''/>
	    <cfset var groupList = ''/>
	    <cfset var TypeListValue = "" />
	    <cfset var _CONDITIONS  = '' />
	    <cfset var CONDITIONITEM    = '' />
	    <cfset var RetVarGetGroupInfo   = '' />
	    <cfset var RetVarReadCPDataById = '' />
	    <cfset var CONDITIONSXML    = '' />
	    <cfset var NoConditionsSpecified = '1' />
	    <cfset var BuilderOptionTypeVal = "" />
	    <cfset var ConvertedAlphaOptionList = "" />
	    <cfset var iBOV = '' />
	    <cfset var SendAsUnicode = '' />
	    <cfset var MaxPerMessage = '' />
	    <cfset var UniSearchMatcher = '' />
	    <cfset var UniSearchPattern = '' />
	    <cfset var UpperCharacterLimit = '' />

	    <cfset var htmlId = ''/>
	    <cfset var afSeclected = ''/>
	    <cfset var seletedIType = '' />
	    <cfset var seletedIValue = '' />
	    <cfset var seletedIMRNR = '' />
	    <cfset var currentYear = '' />
	    <cfset var iAF = '' />
	    <cfset var i = ''/>
	    <cfset var iType = '' />
	    <cfset var iValue = ''/>
	    <cfset var iMRNR = '' />
	    <cfset var hour = '' />
	    <cfset var minute = '' />
	    <cfset var index = ''/>
	    <cfset var year = ''/>
	    <cfset var cpTextWithoutCDF = ''/>
	    <cfset var shortcode = '' />
	    <cfset var stringDateFormat=''>
	    <cfset var step=''>

	    <cftry>


	        <!--- remove old method:
	        <cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortcode"></cfinvoke>
	        --->
	        <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode">
	            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	        </cfinvoke>

	        <cfif arguments.inpTemplateId EQ 9 AND arguments.controlPoint.RQ GTE 4>
	            <cfset dataout.HTML = ""/>
	            <cfreturn dataout />
	        </cfif>

	        <cfswitch expression="#controlPoint.TYPE#" >

	            <cfcase value="STATEMENT">
	                <cfset TypeListValue = "Simple Message" />
	            </cfcase>

	            <cfcase value="SHORTANSWER">
	                <cfset TypeListValue = "Question & Wait for Answer" />
	            </cfcase>

	            <cfcase value="ONESELECTION">
	                <cfset TypeListValue = "Question & Wait for Answer" />
	            </cfcase>

	            <cfcase value="TRAILER">
	                <cfset TypeListValue = "Final Message - This will end the conversation" />
	            </cfcase>

	            <cfcase value="BRANCH">
	                <cfset TypeListValue = "Rules Engine" />
	            </cfcase>

	            <cfcase value="INTERVAL">
	                <cfset TypeListValue = "Step Action Interval" />
	            </cfcase>

	            <cfcase value="API">
	                <cfset TypeListValue = "API Call" />
	            </cfcase>

	            <cfcase value="OPTIN">
	                <cfset TypeListValue = "Capture Opt In" />
	            </cfcase>

	            <cfcase value="CDF">
	                <cfset TypeListValue = "Capture Custom Data Field" />
	            </cfcase>

	            <cfcase value="RESET">
	                <cfset TypeListValue = "Drip Marketing Reset" />
	            </cfcase>

	        </cfswitch>

	        <cfsavecontent variable="CP_HTML">
	            <cfoutput>

	                <cfset BuilderOptionTypeVal = "BuilderOption" />


	                <!--- Stop hiding these - only hide them in SWT flows --->
	                <cfset BuilderOptionTypeVal = "" />

	<!---
	                <!--- Show Interval for easy to edit settings? INTERVAL --->
	                <cfif ListFindNoCase("BRANCH,API,RESET", controlPoint.TYPE) GT 0>
	                    <cfset BuilderOptionTypeVal = "BuilderOption" />
	                <cfelse>
	                    <cfset BuilderOptionTypeVal = "" />
	                </cfif>
	--->

	                <!--- Preserve possible newlines in display but format all other tags --->
	<!--- Reserve newlines for display message in textbox
	                <cfset arguments.controlPoint.TDESC = ReplaceNoCase(arguments.controlPoint.TDESC, "newlinex", "<br>", "ALL")>
	                <cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "\n", "<br>", "ALL")>
	                <cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "#chr(10)#", "<br>", "ALL")>
	                <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "\n", "<br>", "ALL")>
	--->
	                <cfset htmlId = arguments.controlPoint.ID>

	                <div class="form-group clearfix control-point #BuilderOptionTypeVal#" id="cp#controlPoint.RQ#" data-control-point-physical-id="#controlPoint.ID#" data-control-rq-id="#controlPoint.RQ#" data-control-point-type="#controlPoint.TYPE#" data-add-new="Edit" data-template-id="#arguments.inpTemplateId#" >

	                                        <cfset arguments.controlPoint.length = len(ReReplaceNoCase(arguments.controlPoint.TEXT, '<[^>]*>', '', 'all'))>

	<!--- Reserve newlines for display message in textbox
	                                        <!--- Preserve possible newlines in display but format all other tags --->
	                                        <cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "<br>", "newlinex", "ALL")>
	                                        <cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "\n", "newlinex", "ALL")>
	                                        <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, chr(10), "newlinex", "ALL")>

	                                        <cfset arguments.controlPoint.TEXT = HTMLEditFormat(arguments.controlPoint.TEXT) />

	                                        <!--- Allow newline in text messages - reformat for display --->
	                                        <cfset arguments.controlPoint.TEXT = ReplaceNoCase(arguments.controlPoint.TEXT, "newlinex", "<br>", "ALL")>
	                                        <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "\n", "<br>", "ALL")>
	                                        <!--- Display ' but store as &apos; in the XML --->
	                                        <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, "&apos;", "'", "ALL")>
	--->
	                                        <!--- Syntax highlight CDFs --->
	                                        <!--- <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, '{%', '<code class="mceNonEditable">{%', 'all')>
	                                        <cfset arguments.controlPoint.TEXT = Replace(arguments.controlPoint.TEXT, '%}', '%}</code>', 'all')> --->

	                                        <div class="control-point-body">
	                                        <!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
	                                        <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />

	                                        <cfset cpTextWithoutCDF = REReplace(arguments.controlPoint.TEXT, "\{\%.+\%\}", "")/>

	                                        <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
	                                        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", cpTextWithoutCDF ) ) />
	                                        <!--- Look for character higher than 255 ASCII --->
	                                        <cfif UniSearchMatcher.Find() >

	                                            <cfset SendAsUnicode = 1 />
	                                            <cfset MaxPerMessage = 66 />
	                                            <cfset UpperCharacterLimit = 70 />

	                                        <cfelse>

	                                            <cfset SendAsUnicode = 0 />
	                                            <cfset MaxPerMessage = 153 />
	                                            <cfset UpperCharacterLimit = 160 />

	                                        </cfif>

	                                        <cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
	                                        <cfset arguments.controlPoint.TGUIDE = Replace(arguments.controlPoint.TGUIDE, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>

	                                        <cfswitch expression="#controlPoint.TYPE#">

	                                            <cfcase value="ONESELECTION">
	                                            <form class="frm-answer" name="frm-answer">
	                                                <cfif arguments.inpTemplateId EQ 11>
	                                                    <div class="row clearfix">
	                                                        <div class="col-sm-6 col-lg-2">
	                                                            <select class="form-control cp_type" data-cp-type='#controlPoint.TYPE#' data-anstemp="#controlPoint.ANSTEMP#">
	                                                                <option value="ONESELECTION" data-anstemp="1" <cfif controlPoint.ANSTEMP is 1>selected</cfif>>Yes or No?</option>
	                                                                <option value="ONESELECTION" data-anstemp="2" <cfif controlPoint.ANSTEMP is 2>selected</cfif>>True or False?</option>
	                                                                <option value="ONESELECTION" data-anstemp="3" <cfif controlPoint.ANSTEMP is 3>selected</cfif>>Agree or Disagree?</option>
	                                                                <option value="ONESELECTION" data-anstemp="4" <cfif controlPoint.ANSTEMP is 4>selected</cfif>>Useful or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="5" <cfif controlPoint.ANSTEMP is 5>selected</cfif>>Easy or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="6" <cfif controlPoint.ANSTEMP is 6>selected</cfif>>Likely or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="7" <cfif controlPoint.ANSTEMP is 7>selected</cfif>>Important or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="8" <cfif controlPoint.ANSTEMP is 8>selected</cfif>>Satisfied or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="9" <cfif controlPoint.ANSTEMP is 9>selected</cfif>>Net Promoter Score</option>
	                                                                <option value="SHORTANSWER" >Open Ended</option>
	                                                                <option value="ONESELECTION" data-anstemp="0" <cfif controlPoint.ANSTEMP is 0>selected</cfif>>Custom</option>
	                                                            </select>
	                                                        </div>
	                                                    </div>
	                                                </cfif>
	                                                <!--- <div class="Preview-Bubble Preview-Me">
	                                                    <div class="control-point-text">#controlPoint.TEXT#</div>

	                                                    <cfif controlPoint.AF NEQ "HIDDEN">
	                                                        <div class="control-point-options">
	                                                            <cfloop array="#controlPoint.OPTIONS#" index="_OPTION">
	                                                                <cfinvoke method="getOptionListStyle" returnvariable="listStyleIndex">
	                                                                    <cfinvokeargument name="AF" value="#controlPoint.AF#">
	                                                                    <cfinvokeargument name="ID" value="#_OPTION.ID#">
	                                                                </cfinvoke>
	                                                                <cfset controlPoint.length += 1 + len('#listStyleIndex# #_OPTION.TEXT#')>
	                                                                <div class="control-point-option">
	                                                                    #listStyleIndex# <span class="control-point-option-text">#_OPTION.TEXT#</span>
	                                                                </div>
	                                                            </cfloop>
	                                                        </div>
	                                                    </cfif>
	                                                </div>

	                                                <cfif controlPoint.length GT UpperCharacterLimit>
	                                                        Character Count {#controlPoint.length#}
	                                                        <span class="text-danger">#ceiling(controlPoint.length / MaxPerMessage)# Increments</span><br>
	                                                        <cfif SendAsUnicode EQ 1>
	                                                            <a class="see-unicode-mess pull-right">You have Unicode characters in your message</a>
	                                                        </cfif>
	                                                    <cfelse>
	                                                        <span>#UpperCharacterLimit - controlPoint.length# characters available</span>
	                                                        <cfif SendAsUnicode EQ 1>
	                                                            <a class="see-unicode-mess pull-right">You have Unicode characters in your message</a>
	                                                        </cfif>
	                                                    </cfif> --->
	                                                <hr/>
	                                                <h4 class="portlet-heading2 questionNumber"></h4>
	                                                <div class="cp-content">
	                                                    <p class="portlet-subheading tguide ">#controlPoint.TGUIDE#</p>
	                                                    <div class="row">
	                                                        <div class="col-md-11">
	                                                            <div class="form-gd">
	                                                                <div class="form-group">
	                                                                    <textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control validate[required] text">#controlPoint.TEXT#</textarea>
	                                                                    <div class="row">
	                                                                        <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
	                                                                            <span class="help-block tdesc">#controlPoint.TDESC#</span>
	                                                                        </div>
	                                                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
	                                                                            <h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
	                                                                                Character Count {#len(controlPoint.TEXT)#}
	                                                                            </h5>
	                                                                            <a class="see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>

	                                                    <div class="row">
	                                                        <div class="col-lg-11">
	                                                            <div class="row">
	                                                                <div class="col-lg-9">
	                                                                    <h4 class="portlet-heading2">Answer Choices List</h4>

	                                                                    <div class="answer-wrap clearfix">
	                                                                        <label for="">Format Answers:</label>
	                                                                        <div class="answer-wrap-select">
	                                                                            <select id="AF" class="form-control AF-select AF_#htmlId# Select2">
	                                                                                <cfloop array="#_CPONESELECTAF#" index="iAF">
	                                                                                    <cfif iAF[1] EQ arguments.controlPoint.AF>
	                                                                                        <cfset afSeclected = 'selected="selected"'/>
	                                                                                    <cfelse>
	                                                                                        <cfset afSeclected = ""/>
	                                                                                    </cfif>
	                                                                                    <option value="#iAF[1]#" #afSeclected# >#iAF[2]#</option>
	                                                                                </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>

	                                                                    <div class="row">
	                                                                        <div class="col-lg-11">
	                                                                            <div class="wrapper-answer-text">
	                                                                                <div class="form-gd field-anser-holder">
	                                                                                    <div class="row row-small">
	                                                                                        <div class="col-xs-8 col-sm-10">
	                                                                                            <div class="form-group mb-0">
	                                                                                                <label>Answer Text</label>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                        <div class="col-xs-2 col-sm-1 hidden">
	                                                                                            <div class="form-group text-center mb-0">
	                                                                                                <label>S-Val</label>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                        <div class="col-xs-2 col-sm-1">
	                                                                                            <div class="form-group text-center mb-0">
	                                                                                                <label>Delete</label>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                    </div>
	                                                                                    <div class="answers_list">
	                                                                                        <cfif arrayLen(controlPoint.ANSWERS) GT 0>
	                                                                                            <cfloop array="#controlPoint.ANSWERS#" index="i">
	                                                                                                <div class="AnswerItem row row-small">
	                                                                                                    <div class="col-xs-8 col-sm-10">
	                                                                                                        <div class="form-group">
	                                                                                                            <input id="OPTION" type="text" value="#i['TEXT']#" class="form-control" />
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                    <div class="col-xs-2 col-sm-1 hidden">
	                                                                                                        <div class="form-group">
	                                                                                                            <input type="text" id="AVAL" class="form-control readonly-val text-center" value="#i['AVAL']#" />
	                                                                                                            <input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="#i['AVALREG']#" />
																						   <input type="hidden" id="AVALRESP" class="form-control readonly-val text-center" value="#i['AVALRESP']#" />
																						   <input type="hidden" id="AVALREDIR" class="form-control readonly-val text-center" value="#i['AVALREDIR']#" />
																						   <input type="hidden" id="AVALNEXT" class="form-control readonly-val text-center" value="#i['AVALNEXT']#" />
																					    </div>
	                                                                                                    </div>
	                                                                                                    <div class="col-xs-2 col-sm-1">
	                                                                                                        <div class="form-group">
	                                                                                                            <button class="form-control delete-answer">
	                                                                                                                <i class="fa fa-minus"></i>
	                                                                                                            </button>
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                </div>
	                                                                                            </cfloop>
	                                                                                        </cfif>

	                                                                                    </div>
	                                                                                </div>

	                                                                                <div class="row row-small">
	                                                                                    <div class="col-xs-4 col-xs-offset-7 col-sm-2 col-sm-offset-9">
	                                                                                        <a href="##" id="add-more-answer" class="btn btn-block grey-mint add-more-answer">ADD MORE</a>
	                                                                                    </div>
	                                                                                </div>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>

	                                                                <div class="col-lg-3 hidden">
	                                                                    <h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
	                                                                    <h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>

	                                                                    <div class="row">
	                                                                        <div class="col-lg-10">
	                                                                            <div class="list-answer-template">
	                                                                                <ul class="nav">
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-1-#htmlId#" class="ans-group" value='1'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-1-#htmlId#">Yes or No?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-2-#htmlId#" class="ans-group" value='2'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-2-#htmlId#">True or False?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-3-#htmlId#" class="ans-group" value='3'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-3-#htmlId#">Agree or Disagree?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-4-#htmlId#" class="ans-group" value='4'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-4-#htmlId#">Useful or Not?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-5-#htmlId#" class="ans-group" value='5'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-5-#htmlId#">Easy or Not?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-6-#htmlId#" class="ans-group" value='6'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-6-#htmlId#">Likely or Not?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-7-#htmlId#" class="ans-group" value='7'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-7-#htmlId#">Important or Not?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-8-#htmlId#" class="ans-group" value='8'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-8-#htmlId#">Satisfied or Not?</label>
	                                                                                    </li>
	                                                                                    <li>
	                                                                                        <input type="radio" name="ans-group" id="ans-temp-9-#htmlId#" class="ans-group" value='9'>
	                                                                                        <label class="lbl-ans-group" for="ans-temp-9-#htmlId#">Net Promoter Score</label>
	                                                                                    </li>
	                                                                                </ul>
	                                                                            </div>

	                                                                            <a href="##" class="btn green-gd btn-add-answer-list-template" data-answer-list="answers_list_#htmlId#">apply</a>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>

	                                                <cfif arguments.inpdisplayIntervalType EQ 1 >
	                                                    <!--- <cfif arguments.inpTemplateId EQ 7 > --->
	                                                        <div class="row">
	                                                            <div class="col-lg-12">
	                                                                <h4 class="portlet-heading2"><a class="toogle-inteval">Response Interval Settings (Optional)<span class="glyphicon glyphicon-chevron-down set-margin"></span></a></h4>
	                                                                <!--- <a class="toogle-inteval">Hide</a> --->
	                                                                <div class="div-toogle-inteval hidden">
	                                                                    <p class="des-for-heading">
	                                                                        Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on
	                                                                        how long you wish to wait before moving on to the next specified Action if no response is received.
	                                                                    </p>

	                                                                    <div class="form-gd">
	                                                                        <div class="row row-small">
	                                                                            <div class="col-lg-2 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Interval Time</label>
	                                                                                    <select name="ITYPE" class="form-control ITYPE Select2">
	                                                                                        <cfloop array="#_CPINTEVALTYPE_1#" index="iType">
	                                                                                            <cfset seletedIType = ''>
	                                                                                            <cfif arguments.controlPoint.ITYPE EQ iType[1]>
	                                                                                                <cfset seletedIType = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>

	                                                                            <div class="col-lg-3 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">&nbsp;</label>
	                                                                                    <select id="IVALUE" class="form-control Select2 IVALUE">
	                                                                                        <cfloop from="0" to="60" index="iValue">
	                                                                                            <cfset seletedIValue = ''>
	                                                                                            <cfif arguments.controlPoint.IVALUE EQ iValue>
	                                                                                                <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iValue#" #seletedIValue#> #iValue# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                            <div class="col-lg-3 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Retry on No Response?</label>
	                                                                                    <select id="IMRNR" class="form-control Select2 IMRNR">
	                                                                                         <cfloop array="#_IMRNR#" index="iMRNR">
	                                                                                            <cfset seletedIMRNR = ''>
	                                                                                            <cfif arguments.controlPoint.IMRNR EQ iMRNR[1]>
	                                                                                                <cfset seletedIMRNR = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iMRNR[1]#" #seletedIMRNR#> #iMRNR[2]# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>

	                                                                            <div class="col-lg-4 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Still No Response?</label>
	                                                                                    <select id="INRMO" class="form-control Select2 INRMO">
	                                                                                        <option value="END" <cfif arguments.controlPoint.INRMO EQ 'END'>SELECTED="SELECTED"</cfif> >End Conversation</option>
	                                                                                        <option value="NEXT" <cfif arguments.controlPoint.INRMO EQ 'NEXT'>SELECTED="SELECTED"</cfif> > Proceed to Next Action</option>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>

	                                                        </div>
	                                                        </form>
	                                                        <div class="row row-small mt-30 mb-30 add-another-question-block">
	                                                            <div class="row col-lg-6">
	                                                                <div class="add-remove-question">
	                                                                    <span class="add-another-question add-another-question1">
	                                                                        <a href="##" class="btn green-gd btn-add-another">add another question</a>
	                                                                    </span>
	                                                                    <span class="add-another-question add-another-simon">
	                                                                        <a href="##" class="btn blue-gd btn-remove-cp btn-simon-remove">REMOVE</a>
	                                                                    </span>
	                                                                </div>
	                                                            </div>

	                                                            <div class="col-lg-6 paging-content add-question-paging" >
	                                                                <!--- <div class="dataTables_paginate paging_input"><span class="paginate_button first"><img class="paginate-arrow" src="/session/sire/images/double-left.png"></span><span class="paginate_button previous" id="tblCampaignList_previous"><img class="paginate-arrow" src="/session/sire/images/left.png"></span><span class="paginate_page">Page </span><input type="text" class="paginate_text" style="display: inline;"><span class="paginate_of"> of 2</span><span class="paginate_button next" id="tblCampaignList_next"><img class="paginate-arrow" src="/session/sire/images/right.png"></span><span class="paginate_button last" id="tblCampaignList_last"><img class="paginate-arrow" src="/session/sire/images/double-right.png"></span></div> --->
	                                                            </div>
	                                                        </div>
	                                                        <hr/>
	                                                    <!--- </cfif>    --->
	                                                </cfif>

	                                            </cfcase>

	                                            <cfcase value="OPTIN">

	                                                <cfinvoke method="GetGroupInfo" returnvariable="RetVarGetGroupInfo">
	                                                    <cfinvokeargument name="inpGroupId" value="#controlPoint.OIG#">
	                                                </cfinvoke>

	                                            <!---   <div class="control-point-optin" data-control-point-OIG="#controlPoint.OIG#">

	                                                    <label>Subscriber list to add this phone number to:</label>
	                                                    <div class="cp-oig-text">
	                                                        #RetVarGetGroupInfo.GROUPNAME#
	                                                    </div>

	                                                </div> --->
	                                                <cfif arguments.inpTemplateId NEQ 1>
	                                                    <p class="portlet-subheading tguide">#controlPoint.TGUIDE#</p>
	                                                    <div class="row">
	                                                        <div class="col-md-11">
	                                                            <div class="form-gd">
	                                                                <div class="form-group">
	                                                                    <textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required]">#controlPoint.TEXT#</textarea>
	                                                                <div class="row">
	                                                                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
	                                                                        <span class="help-block tdesc">#controlPoint.TDESC#</span>
	                                                                    </div>
	                                                                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
	                                                                        <h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
	                                                                            Character Count {#len(controlPoint.TEXT)#}
	                                                                        </h5>
	                                                                        <a class="see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
	                                                                    </div>
	                                                                </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </cfif>

	                                            </cfcase>

	                                            <cfcase value="CDF">
	                                                <div class="control-point-cdf">

	                                                    Selected Custom Field:
	                                                    <span class="cp-cdf-name">
	                                                        #controlPoint.SCDF NEQ '0' ? controlPoint.SCDF : 'Select CDF To Use'#
	                                                    </span>
	                                                </div>
	                                            </cfcase>

	                                            <cfcase value="RESET">

	                                            </cfcase>

	                                            <cfcase value="INTERVAL">

	                                                <cfset var selectDate = ''>
	                                                <cfset var selectYear = ''>
	                                                <cfset var selectMonth = ''>
	                                                <cfset var selectDay = ''>

	                                                    <!--- Look for special types of interval logic selection --->
	                                                <!---

	                                                    Today at xx:xx


	                                                --->


	                                                <!---  Today at xx:xx

	                                                    Only one Condition
	                                                    ITYPE = TODAY


	                                                    Render an easier to understand time picker
	                                                    Adjust editor on campaign-edit page - hide and show certain fields


	                                                --->

	                                                <cfif controlPoint.ITYPE EQ "TODAY">

	                                                    <div class="">Hold Action Flow until Today at <cfif controlPoint.IHOUR EQ "00">12<cfelse>#controlPoint.IHOUR#</cfif>:#controlPoint.IMIN# <cfif controlPoint.INOON EQ "01">PM<cfelse>AM</cfif></div>

	                                                        <cfif controlPoint.IENQID GT 0>
	                                                            <!--- Only display message if a real question is found OK --->
	                                                            <!--- BOQ stores the Physical "ID" of the question --->
	                                                            <cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	                                                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                                                                <cfinvokeargument name="inpQID" value="#controlPoint.IENQID#">
	                                                                <cfinvokeargument name="inpIDKey" value="ID">
	                                                                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	                                                                <cfinvokeargument name="inpUserId" value="#inpUserId#">
	                                                            </cfinvoke>

	                                                            <cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

	                                                                <div class="">
	                                                                    <h5>Then move on to Question at Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>

	                                                                    <!--- Used TEXT if available - or use TDESC if not --->
	                                                                    <cfif LEN(RetVarReadCPDataById.CPObj.TEXT) GT 0>
	                                                                        <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
	                                                                    <cfelse>
	                                                                        <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TDESC#</span>
	                                                                    </cfif>

	                                                                </div>

	                                                            <cfelse>

	                                                                <div class="">
	                                                                    <h5>Then move on to the next Action</h5>
	                                                                </div>

	                                                            </cfif>
	                                                        <cfelse>

	                                                            <div class="">
	                                                                <h5>Then move on to the next Action</h5>
	                                                            </div>
	                                                        </cfif>
	                                                </cfif>
	                                                <cfset stringDateFormat="">
	                                                <cfif arguments.controlPoint.ITYPE EQ 'DATE'>
	                                                    <cfset selectDate = listToArray(arguments.controlPoint.IVALUE,'/') >
	                                                    <cfset selectYear = selectDate[1]>
	                                                    <cfset selectMonth = selectDate[2]>
	                                                    <cfset selectDay = selectDate[3]>
	                                                    <cfset stringDateFormat=selectMonth & "/"&  selectDay & "/" & selectYear>
	                                                </cfif>

	                                                <cfif arguments.inpTemplateId EQ 8>
	                                                    <div class="item-another-mesage">
	                                                        <div class="row row-small">

	                                                            <div class="col-lg-11 col-md-11">
	                                                                <div class="row row-small form-gd">
	                                                                    <div class="col-sm-12">
	                                                                        <div class="form-group mb-0">
	                                                                            <label for="">Delay</label>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-2">
	                                                                        <div class="form-group">
	                                                                            <select name="ITYPE" class="form-control itype-select-new Select2 ITYPE">
	                                                                                <option value="SECONDS" <cfif arguments.controlPoint.ITYPE is "SECONDS">selected</cfif> >Immediately</option>
	                                                                                <option value="HOURS" <cfif arguments.controlPoint.ITYPE is "HOURS">selected</cfif>>Hour(s)</option>
	                                                                                <option value="DAYS" <cfif arguments.controlPoint.ITYPE is "DAYS">selected</cfif>>Day(s)</option>
	                                                                                <option value="WEEKS" <cfif arguments.controlPoint.ITYPE is "WEEKS">selected</cfif>>Week(s)</option>
	                                                                                <option value="DATE" <cfif arguments.controlPoint.ITYPE is "DATE">selected</cfif>>Select Date</option>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-2">
	                                                                        <div class="form-group">
	                                                                            <div class="selected-interval #(arguments.controlPoint.ITYPE EQ "SECONDS" OR arguments.controlPoint.ITYPE EQ "DATE") ? "hidden" : ""#">
	                                                                                <div class="form-group">
	                                                                                    <select name="selected-hour-select" id="selected-hour-select-#controlPoint.RQ#" class="form-control Select2 IVALUE">
	                                                                                        <cfloop from="0" to="60" index="step">
	                                                                                            <option value="#step#" <cfif arguments.controlPoint.IVALUE EQ step>selected</cfif> >#step#</option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                            <div class="selected-date #(arguments.controlPoint.ITYPE NEQ "DATE") ? "hidden" : ""#">
	                                                                                <div class="form-group">
	                                                                                    <div class="input-group datetimepicker date set-z-index" id="selected-date-select-#controlPoint.RQ#">
	                                                                                        <input type="text" class="form-control" value="#stringDateFormat#">
	                                                                                        <span class="input-group-addon">
	                                                                                            <span class="glyphicon glyphicon-calendar"></span>
	                                                                                        </span>
	                                                                                    </div>
	                                                                                </div>
	                                                                            </div>
	                                                                            <div class="hidden">
	                                                                                <div class="form-group">
	                                                                                    <select name="" id="" class="form-control ITIME Select2">
	                                                                                    <cfloop from="0" to="23" index="hour">
	                                                                                        <cfloop from="0" to="59" index="minute" step="15">
	                                                                                            <cfset var time = '#(hour LT 10) ? '0'&hour : hour#:#(minute LT 10) ? '0'&minute : minute#'/>
	                                                                                            <cfset var checked = 0>
	                                                                                            <cfset var checkHour = int(arguments.controlPoint.INOON) EQ 0 ? arguments.controlPoint.IHOUR : int(arguments.controlPoint.IHOUR) + 12/>
	                                                                                            <cfif int(arguments.controlPoint.IHOUR) EQ 0>
	                                                                                                <cfset checkHour = int(arguments.controlPoint.INOON) EQ 0 ? 0 : 12/>
	                                                                                            </cfif>
	                                                                                            <cfif 0 EQ int(minute) AND 9 EQ int(hour) >
	                                                                                                <cfset checked = 1/>
	                                                                                            </cfif>
	                                                                                            <option value="#time#" <cfif checked EQ 1>selected</cfif> >#time#</option>
	                                                                                        </cfloop>
	                                                                                    </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-8">

	                                                                    </div>

	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                <cfelse>
	                                                    <div class="item-another-mesage">

	                                                        <div class="row row-small">

	                                                            <div class="col-lg-4 col-md-11">
	                                                                <div class="row row-small form-gd">
	                                                                    <div class="col-sm-12">
	                                                                        <div class="form-group mb-0">
	                                                                            <label for="">Interval Time</label>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select id="" class="form-control IVALUE Select2">
	                                                                                <cfloop from="0" to="60" index="iValue">
	                                                                                    <cfset seletedIValue = ''>
	                                                                                    <cfif arguments.controlPoint.IVALUE EQ iValue  AND arguments.controlPoint.ITYPE NEQ 'DATE'>
	                                                                                        <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                    </cfif>
	                                                                                    <option value="#iValue#" #seletedIValue#> #iValue# </option>
	                                                                                </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select name="ITYPE" class="form-control ITYPE Select2">
	                                                                                <cfloop array="#_CPINTEVALTYPE_2#" index="iType">
	                                                                                    <cfset seletedIType = ''>
	                                                                                    <cfif arguments.controlPoint.ITYPE EQ iType[1] AND arguments.controlPoint.ITYPE NEQ 'DATE'>
	                                                                                        <cfset seletedIType = 'SELECTED="SELECTED"'>
	                                                                                    </cfif>
	                                                                                    <option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
	                                                                                </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select name="" id="" class="form-control ITIME Select2">
	                                                                            <cfloop from="0" to="23" index="hour">
	                                                                                <cfloop from="0" to="59" index="minute" step="15">
	                                                                                    <cfset var time = '#(hour LT 10) ? '0'&hour : hour#:#(minute LT 10) ? '0'&minute : minute#'/>
	                                                                                    <cfset var checked = 0>
	                                                                                    <cfset var checkHour = int(arguments.controlPoint.INOON) EQ 0 ? arguments.controlPoint.IHOUR : int(arguments.controlPoint.IHOUR) + 12/>
	                                                                                    <cfif int(arguments.controlPoint.IHOUR) EQ 0>
	                                                                                        <cfset checkHour = int(arguments.controlPoint.INOON) EQ 0 ? 0 : 12/>
	                                                                                    </cfif>
	                                                                                    <cfif int(arguments.controlPoint.IMIN) EQ int(minute) AND int(checkHour) EQ int(hour) AND arguments.controlPoint.ITYPE NEQ 'DATE'>
	                                                                                        <cfset checked = 1/>
	                                                                                    </cfif>
	                                                                                    <option value="#time#" <cfif checked EQ 1>selected</cfif> >#time#</option>
	                                                                                </cfloop>
	                                                                            </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>

	                                                            <div class="col-lg-1 col-md-12">
	                                                                <div class="text-center or-text">
	                                                                    <span>OR</span>
	                                                                </div>
	                                                            </div>
	                                                            <div class="col-lg-4 col-md-11">
	                                                                <div class="row row-small form-gd">
	                                                                    <div class="col-sm-12">
	                                                                        <div class="form-group mb-0">
	                                                                            <label for="">When to Send this Message</label>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select name="" id="" class="form-control IMONTH Select2">
	                                                                                <option value="" selected>Month</option>
	                                                                                <cfloop from="1" to="12" index="index">
	                                                                                    <cfoutput>
	                                                                                        <cfset seletedIValue = ''>
	                                                                                        <cfif selectMonth EQ index  AND arguments.controlPoint.ITYPE EQ 'DATE'>
	                                                                                            <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                        </cfif>
	                                                                                        <option value="#index#" #seletedIValue# >#index#</option>
	                                                                                    </cfoutput>
	                                                                                </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select name="" id="" class="form-control IDAY Select2">
	                                                                                <option value="" selected>Day</option>
	                                                                                <cfloop from="1" to="31" index="index">
	                                                                                    <cfoutput>
	                                                                                        <cfset seletedIValue = ''>
	                                                                                        <cfif selectDay EQ index  AND arguments.controlPoint.ITYPE EQ 'DATE'>
	                                                                                            <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                        </cfif>
	                                                                                        <option value="#index#" #seletedIValue# >#index#</option>
	                                                                                    </cfoutput>
	                                                                                </cfloop>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-sm-4">
	                                                                        <div class="form-group">
	                                                                            <select name="" id="" class="form-control IYEAR Select2">
	                                                                                <option value="" selected>Year</option>
	                                                                                <cfset currentYear = year(now())/>
	                                                                                <cfoutput>
	                                                                                    <cfloop from="#currentYear#" to="#(currentYear+10)#" index="year">
	                                                                                        <cfset seletedIValue = ''>
	                                                                                        <cfif selectYear EQ year  AND arguments.controlPoint.ITYPE EQ 'DATE'>
	                                                                                            <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                        </cfif>

	                                                                                        <option value="#year#" #seletedIValue#>#year#</option>
	                                                                                    </cfloop>
	                                                                                </cfoutput>
	                                                                            </select>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </cfif>


	                                            </cfcase>

	                                            <cfcase value="API">
	                                                <!--- <div>API Call</div> --->
	                                                <cfif arguments.inpTemplateId EQ 9>
	                                                    <cfset var apiData = DeSerializeJSON(controlPoint.API_DATA)/>
	                                                    <cfif isDefined("apiData.inpListOfPhones")>
	                                                        <cfset var listOfPhones = listToArray(apiData.inpListOfPhones)/>
	                                                    <cfelse>
	                                                        <cfset var listOfPhones = ArrayNew(1)/>
	                                                    </cfif>
	                                                    <cfif isDefined("apiData.inpListOfEmails")>
	                                                        <cfset var listOfEmails = listToArray(apiData.inpListOfEmails)/>
	                                                    <cfelse>
	                                                        <cfset var listOfEmails =  ArrayNew(1)/>
	                                                    </cfif>


	                                                    <!--- New Update Html, Tell The Manager Template --->
	                                                    <div class="uk-grid uk-grid-small">
	                                                        <div class="uk-width-1-1">
	                                                            <span class="des-for-heading uk-dislay-block"> Customers feedback will be forwarded to these contact addresses in real time.</span>
	                                                        </div>
	                                                    </div>
	                                                    <p class="portlet-subheading tguide">Please check the contact method you prefer to be contacted for notification.</p>
	                                                    <div class="uk-grid uk-grid-small block-contact-method">
	                                                        <div class="uk-width-1-2@l">
	                                                            <div class="uk-grid uk-grid-small sms-number-wrap #(len(listOfPhones) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
	                                                                <div class="uk-width-auto@s col-for-label">
	                                                                    <label class="check-method"><input id="tellmgr-sms-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfPhones) GT 0 ? "checked" : "")#> SMS Number</label>
	                                                                </div>
	                                                                <div class="uk-width-expand@s hold-more-content">
	                                                                    <cfif len(listOfPhones) GT 0>
	                                                                        <cfset var phone = '' />
	                                                                        <cfloop array="#listOfPhones#" index="phone">
	                                                                            <div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
	                                                                                <div class="uk-width-expand">
	                                                                                    <input type="text" class="form-control phone-style" value="#phone#"/>
	                                                                                </div>
	                                                                                <div class="uk-width-auto col-for-more">
	                                                                                    <cfif len(listOfPhones) GT 1>
	                                                                                        <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
	                                                                                    </cfif>
	                                                                                    <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
	                                                                                </div>
	                                                                            </div>
	                                                                        </cfloop>
	                                                                    <cfelse>
	                                                                        <div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
	                                                                            <div class="uk-width-expand">
	                                                                                <input type="text" class="form-control phone-style" value="" disabled/>
	                                                                            </div>
	                                                                            <div class="uk-width-auto col-for-more">
	                                                                                <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
	                                                                                <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
	                                                                            </div>
	                                                                        </div>
	                                                                    </cfif>
	                                                                </div>
	                                                            </div>

	                                                            <div class="uk-grid uk-grid-small email-add-wrap #(len(listOfEmails) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
	                                                                <div class="uk-width-auto@s col-for-label">
	                                                                    <label class="check-method"><input id="tellmgr-email-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfEmails) GT 0 ? "checked" : "")#> Email Address</label>
	                                                                </div>
	                                                                <div class="uk-width-expand@s hold-more-content">
	                                                                    <cfif len(listOfEmails) GT 0>
	                                                                        <cfset var email = '' />
	                                                                        <cfloop array="#listOfEmails#" index="email">
	                                                                            <div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
	                                                                                <div class="uk-width-expand">
	                                                                                    <input type="text" class="form-control" value="#email#"/>
	                                                                                </div>
	                                                                                <div class="uk-width-auto col-for-more">
	                                                                                    <cfif len(listOfEmails) GT 1>
	                                                                                        <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
	                                                                                    </cfif>
	                                                                                    <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
	                                                                                </div>
	                                                                            </div>
	                                                                        </cfloop>
	                                                                    <cfelse>
	                                                                        <div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
	                                                                            <div class="uk-width-expand">
	                                                                                <input type="text" class="form-control" value="" disabled/>
	                                                                            </div>
	                                                                            <div class="uk-width-auto col-for-more">
	                                                                                <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
	                                                                                <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
	                                                                            </div>
	                                                                        </div>
	                                                                    </cfif>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                    <!--- End New Update Html --->
	                                                </cfif>
	                                            </cfcase>

	                                            <cfcase value="STATEMENT">
	                                                <div class="cp-update-cpn hidden">
	                                                    <p>
	                                                        Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name needs to appear before your message.
	                                                        <a href="##" id="update-profile-from-cp">Change Profile Name</a>
	                                                    </p>
	                                                </div>
	                                                <p class="portlet-subheading tguide">#controlPoint.TGUIDE#</p>
	                                                <div class="row">
	                                                    <div class="col-md-11">
	                                                        <div class="form-gd">
	                                                            <div class="form-group">
	                                                                <textarea data-emojiable="true" maxLength="459" name="" id="" cols="30" data-control="text-message" rows="5" class="form-control text validate[required]">#controlPoint.TEXT#</textarea>
	                                                                <div class="row">
	                                                                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
	                                                                        <span class="help-block tdesc">#controlPoint.TDESC#</span>
	                                                                    </div>
	                                                                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
	                                                                        <h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
	                                                                            Character Count {#len(controlPoint.TEXT)#}
	                                                                        </h5>
	                                                                        <a class="see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
	                                                                    </div>
	                                                                </div>
	                                                                <!--- <PRE>
	                                                                    <cfdump var="#controlPoint#"/>
	                                                                </PRE>   --->
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>

	                                            </cfcase>

	                                            <cfcase value="SHORTANSWER">
	                                                <div class="cp-update-cpn hidden">
	                                                    <p>
	                                                        Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name needs to appear before your message.
	                                                        <a href="##" id="update-profile-from-cp">Change Profile Name</a>
	                                                    </p>
	                                                </div>
	                                                <cfif arguments.inpTemplateId EQ 11>
	                                                    <div class="row clearfix">
	                                                        <div class="col-sm-6 col-lg-2">
	                                                            <select class="form-control cp_type" data-cp-type="#controlPoint.TYPE#" data-anstemp="">
	                                                                <option value="ONESELECTION" data-anstemp="1">Yes or No?</option>
	                                                                <option value="ONESELECTION" data-anstemp="2">True or False?</option>
	                                                                <option value="ONESELECTION" data-anstemp="3">Agree or Disagree?</option>
	                                                                <option value="ONESELECTION" data-anstemp="4">Useful or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="5">Easy or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="6">Likely or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="7">Important or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="8">Satisfied or Not?</option>
	                                                                <option value="ONESELECTION" data-anstemp="9">Net Promoter Score</option>
	                                                                <option value="SHORTANSWER" selected>Open Ended</option>
	                                                                <option value="ONESELECTION" data-anstemp="0">Custom</option>
	                                                            </select>
	                                                        </div>
	                                                    </div>
	                                                    <h4 class="portlet-heading2 questionNumber"></h4>
	                                                </cfif>
	                                                <div class="cp-content">
	                                                    <p class="portlet-subheading tguide">#controlPoint.TGUIDE#</p>
	                                                    <div class="row">
	                                                        <div class="col-md-11">
	                                                            <div class="form-gd">
	                                                                <div class="form-group">
	                                                                    <textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required] need-rule-message">#controlPoint.TEXT#</textarea>
	                                                                    <div class="row">
	                                                                        <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
	                                                                            <div class="uk-flex uk-flex-middle has-error-message-block">
	                                                                                <div class="has-error-message hidden"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
	                                                                                <div class="help-block need-rule-tdesc tdesc">#controlPoint.TDESC#</div>
	                                                                            </div>
	                                                                        </div>
	                                                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
	                                                                            <h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
	                                                                                Character Count {#len(controlPoint.TEXT)#}
	                                                                            </h5>
	                                                                            <a class="see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
	                                                                        </div>
	                                                                    </div>
	                                                                    <!--- <PRE>
	                                                                        <cfdump var="#controlPoint#"/>
	                                                                    </PRE>   --->
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>

	                                                <cfif arguments.inpdisplayIntervalType EQ 1 >
	                                                        <div class="row">
	                                                            <div class="col-lg-12">
	                                                                <h4 class="portlet-heading2"><a class="toogle-inteval">Response Interval Settings (Optional)<span class="glyphicon glyphicon-chevron-down set-margin"></span></a></h4>
	                                                                <!--- <a class="toogle-inteval">Hide</a> --->
	                                                                <div class="div-toogle-inteval hidden">
	                                                                    <p class="des-for-heading">
	                                                                        Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on
	                                                                        how long you wish to wait before moving on to the next specified Action if no response is received.
	                                                                    </p>

	                                                                    <div class="form-gd">
	                                                                        <div class="row row-small">
	                                                                            <div class="col-lg-2 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Interval Time</label>
	                                                                                    <select name="ITYPE" class="form-control ITYPE Select2">
	                                                                                        <cfloop array="#_CPINTEVALTYPE_1#" index="iType">
	                                                                                            <cfset seletedIType = ''>
	                                                                                            <cfif arguments.controlPoint.ITYPE EQ iType[1]>
	                                                                                                <cfset seletedIType = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>

	                                                                            <div class="col-lg-3 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">&nbsp;</label>
	                                                                                    <select id="IVALUE" class="form-control IVALUE Select2">
	                                                                                        <cfloop from="0" to="60" index="iValue">
	                                                                                            <cfset seletedIValue = ''>
	                                                                                            <cfif arguments.controlPoint.IVALUE EQ iValue>
	                                                                                                <cfset seletedIValue = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iValue#" #seletedIValue#> #iValue# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                            <div class="col-lg-3 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Retry on No Response?</label>
	                                                                                    <select id="IMRNR" class="form-control Select2 IMRNR">
	                                                                                         <cfloop array="#_IMRNR#" index="iMRNR">
	                                                                                            <cfset seletedIMRNR = ''>
	                                                                                            <cfif arguments.controlPoint.IMRNR EQ iMRNR[1]>
	                                                                                                <cfset seletedIMRNR = 'SELECTED="SELECTED"'>
	                                                                                            </cfif>
	                                                                                            <option value="#iMRNR[1]#" #seletedIMRNR#> #iMRNR[2]# </option>
	                                                                                        </cfloop>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>

	                                                                            <div class="col-lg-4 col-sm-6">
	                                                                                <div class="form-group">
	                                                                                    <label for="">Still No Response?</label>
	                                                                                    <select id="INRMO" class="form-control Select2 INRMO">
	                                                                                        <option value="END" <cfif arguments.controlPoint.INRMO EQ 'END'>SELECTED="SELECTED"</cfif> >End Conversation</option>
	                                                                                        <option value="NEXT" <cfif arguments.controlPoint.INRMO EQ 'NEXT'>SELECTED="SELECTED"</cfif> > Proceed to Next Action</option>
	                                                                                    </select>
	                                                                                </div>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>

	                                                        </div>
	                                                        </form>

	                                                        <div class="row row-small mt-30 mb-30 add-another-question-block">
	                                                            <div class="row col-lg-6">
	                                                                <div class="add-remove-question">
	                                                                    <span class="add-another-question add-another-question1">
	                                                                        <a href="##" class="btn green-gd btn-add-another">add another question</a>
	                                                                    </span>
	                                                                    <span class="add-another-question add-another-simon">
	                                                                        <a href="##" class="btn blue-gd btn-remove-cp btn-simon-remove">REMOVE</a>
	                                                                    </span>
	                                                                </div>
	                                                            </div>

	                                                            <div class="col-lg-6 paging-content add-question-paging" >
	                                                                <!--- <div class="dataTables_paginate paging_input"><span class="paginate_button first"><img class="paginate-arrow" src="/session/sire/images/double-left.png"></span><span class="paginate_button previous" id="tblCampaignList_previous"><img class="paginate-arrow" src="/session/sire/images/left.png"></span><span class="paginate_page">Page </span><input type="text" class="paginate_text" style="display: inline;"><span class="paginate_of"> of 2</span><span class="paginate_button next" id="tblCampaignList_next"><img class="paginate-arrow" src="/session/sire/images/right.png"></span><span class="paginate_button last" id="tblCampaignList_last"><img class="paginate-arrow" src="/session/sire/images/double-right.png"></span></div> --->
	                                                            </div>
	                                                        </div>
	                                                        <hr/>
	                                                </cfif>
	                                            </cfcase>

	                                            <cfcase value="TRAILER">
	                                                <cfif arguments.inpTemplateId EQ 3>
	                                                    <div class="cp-update-cpn hidden">
	                                                        <p>
	                                                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name needs to appear before your message.
	                                                            <a href="##" id="update-profile-from-cp">Change Profile Name</a>
	                                                        </p>
	                                                    </div>
	                                                </cfif>

	                                                <p class="portlet-subheading tguide">#controlPoint.TGUIDE#</p>
	                                                <div class="row">
	                                                    <div class="col-md-11">
	                                                        <div class="form-gd">
	                                                            <div class="form-group">
	                                                                <textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="text form-control validate[required]">#controlPoint.TEXT#</textarea>
	                                                                <div class="row">
	                                                                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
	                                                                        <span class="help-block tdesc">#controlPoint.TDESC#</span>
	                                                                    </div>
	                                                                    <div class="col-xs-12">
	                                                                        <h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
	                                                                            Character Count {#len(controlPoint.TEXT)#}
	                                                                        </h5>
	                                                                        <a class="see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message</a>
	                                                                    </div>
	                                                                </div>
	                                                                <!--- <PRE>
	                                                                    <cfdump var="#controlPoint#"/>
	                                                                </PRE>   --->
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </cfcase>

	                                            <cfcase value="BRANCH">


	                                                <!--- Look for special types of branch logic selection --->
	                                                <!---




	                                                --->

	                                                <!--- Single condition Check --->
	                                                <cfif ArrayLen(controlPoint.CONDITIONS) EQ 1>
	                                                    <div class="hidden">Single Condition!</div>
	                                                </cfif>

	                                                <!--- Reset this flag for each Branch --->
	                                                <cfset NoConditionsSpecified = '1' />

	<!---                                                   <p>#SerializeJSON(controlPoint)#</p> --->

	                                                <cfloop array="#controlPoint.CONDITIONS#" index="_CONDITIONS">

	                                                    <!--- Flag the fact that at least one condition is defined for this branch --->
	                                                    <cfset NoConditionsSpecified = '0' />

	                                                    <div class="hidden">
	                                                        <span class="">Condition ###_CONDITIONS.CID# - #_CONDITIONS.TYPE#</span>
	                                                    </div>

	                                                    <!--- This Rule will look at question X and move to Question Y if the answer to Question X is Z --->

	                                                    <!--- Look up Question Text --->

	                                                    <cfif _CONDITIONS.TYPE EQ "RESPONSE" >

	                                                        <cfif _CONDITIONS.BOQ GT 0>

	                                                            <!--- Only display message if a real question is found OK --->
	                                                            <!--- BOQ stores the Physical "ID" of the question --->
	                                                            <cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	                                                                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                                                                <cfinvokeargument name="inpQID" value="#_CONDITIONS.BOQ#">
	                                                                <cfinvokeargument name="inpIDKey" value="ID">
	                                                                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	                                                                <cfinvokeargument name="inpUserId" value="#inpUserId#">
	                                                            </cfinvoke>

	                                                            <cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

	                                                                <div class="hidden">
	                                                                    <h5>For Question at Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
	                                                                    <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
	                                                                </div>

	                                                                <cfset ConvertedAlphaOptionList = "">

	                                                                <cfif _CONDITIONS.BOV NEQ ''>

	                                                                    <cfloop list="#_CONDITIONS.BOV#" delimiters="," index="iBOV">

	                                                                        <cfif LEN(ConvertedAlphaOptionList) EQ 0>
	                                                                            <cfset ConvertedAlphaOptionList = "#CHR(iBOV +64)#">
	                                                                        <cfelse>
	                                                                            <cfset ConvertedAlphaOptionList = ConvertedAlphaOptionList & ",#CHR(iBOV + 64)#">
	                                                                        </cfif>

	                                                                    </cfloop>

	                                                                    <div class="hidden">
	                                                                        <h5 style="margin-bottom: 0;">If this Question is Answered with: </h5>
	                                                                        <cfswitch expression="#RetVarReadCPDataById.CPObj.AF#">
	                                                                            <cfcase value="NOFORMAT">
	                                                                                <span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfcase>

	                                                                            <cfcase value="NUMERIC">
	                                                                                <span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfcase>

	                                                                             <cfcase value="NUMERICPAR">
	                                                                                <span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfcase>

	                                                                            <cfcase value="ALPHA">
	                                                                                <span style="font-size:.7em;">#ConvertedAlphaOptionList# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfcase>

	                                                                            <cfcase value="ALPHAPAR">
	                                                                                <span style="font-size:.7em;">#ConvertedAlphaOptionList# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfcase>

	                                                                            <cfdefaultcase>
	                                                                                <span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
	                                                                            </cfdefaultcase>
	                                                                        </cfswitch>

	                                                                    </div>

	                                                                </cfif>


	                                                                <cfif _CONDITIONS.BOAV NEQ '' AND _CONDITIONS.BOV EQ ''>

	                                                                    <div class="hidden">
	                                                                        <h5>If this Question is Answered with anything in the following list: </h5>
	                                                                        <span style="font-size:.7em; word-break: break-word;">#_CONDITIONS.BOAV#</span>
	                                                                    </div>

	                                                                </cfif>

	                                                                <cfif _CONDITIONS.BOTNQ GT 0>

	                                                                    <!--- BOTNQ stores the Physical "ID" of the question --->
	                                                                    <cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	                                                                        <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                                                                        <cfinvokeargument name="inpQID" value="#_CONDITIONS.BOTNQ#">
	                                                                        <cfinvokeargument name="inpIDKey" value="ID">
	                                                                        <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	                                                                        <cfinvokeargument name="inpUserId" value="#inpUserId#">
	                                                                    </cfinvoke>

	                                                                    <div class="hidden">
	                                                                        <h5>The conversation will move to Action ## #RetVarReadCPDataById.CPObj.RQ# </h5>
	                                                                        <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
	                                                                    </div>
	                                                                <cfelse>

	                                                                    <div class="hidden">
	                                                                        <h5>The next Action is to Jump to the next Action in the sequence.</h5>
	                                                                        <span style="font-size:.7em; font-style: italic;"></span>
	                                                                    </div>

	                                                                </cfif>

	                                                            </cfif>


	                                                        </cfif>


	                                                    </cfif>

	                                                </cfloop>

	                                                <!--- Display message for default next action --->
	                                                <cfif controlPoint.BOFNQ GT 0 >

	                                                    <!--- BOFNQ stores the Physical "ID" of the question --->
	                                                    <cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	                                                        <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                                                        <cfinvokeargument name="inpQID" value="#controlPoint.BOFNQ#">
	                                                        <cfinvokeargument name="inpIDKey" value="ID">
	                                                        <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	                                                        <cfinvokeargument name="inpUserId" value="#inpUserId#">
	                                                    </cfinvoke>

	                                                    <cfif NoConditionsSpecified EQ '1'>

	                                                        <!--- No Conditions defined so  --->

	                                                        <!--- Only display message if a real question is found OK --->
	                                                        <cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

	                                                            <div class="hidden">
	                                                                <h5>The next Action is to Jump to Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
	                                                                <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
	                                                            </div>

	                                                        <cfelse>

	                                                            <div class="hidden">
	                                                                <h5>The next Action is to Jump to the next Action in the sequence.</h5>
	                                                                <span style="font-size:.7em; font-style: italic;"></span>
	                                                            </div>

	                                                        </cfif>

	                                                    <cfelse>

	                                                        <!--- Conditions defined so  --->

	                                                        <!--- Only display message if a real question is found OK --->
	                                                        <cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

	                                                            <div class="hidden">
	                                                                <h5>The default next Action if no conditions specified were met:<BR/>
	                                                                    Jump to Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
	                                                                <span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
	                                                            </div>

	                                                        <cfelse>

	                                                            <div class="hidden">
	                                                                <h5>The default next Action if no conditions specified were met:<BR/>
	                                                                    Jump to next Action in the sequence.</h5>
	                                                                <span style="font-size:.7em; font-style: italic;"></span>
	                                                            </div>

	                                                        </cfif>

	                                                    </cfif>

	                                                <cfelse>

	                                                    <cfif NoConditionsSpecified EQ '1'>

	                                                        <!--- No Conditions defined so  --->
	                                                        <div class="hidden">
	                                                            <h5>The next Action is to Jump to the next Action in the sequence.</h5>
	                                                            <span style="font-size:.7em; font-style: italic;"></span>
	                                                        </div>

	                                                    <cfelse>
	                                                        <!--- Conditions defined so  --->
	                                                        <div class="hidden">
	                                                            <h5>The default next Action if no conditions specified were met:<BR/>
	                                                                Jump to next Action in the sequence.</h5>
	                                                            <span style="font-size:.7em; font-style: italic;"></span>
	                                                        </div>

	                                                    </cfif>

	                                                </cfif>

	                                            </cfcase>

	                                            <cfdefaultcase>

	                                                <div class="control-point-text Preview-Bubble Preview-Me">#controlPoint.TEXT#</div>
	                                            </cfdefaultcase>

	                                        </cfswitch>
	                    </div>
	                </div>
	            </cfoutput>
	        </cfsavecontent>

	        <cfset dataout.HTML  = CP_HTML />

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.HTML  = SerializeJSON(cfcatch) />

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#">
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

	        </cfcatch>
	    </cftry>

	    <cfreturn dataout />
	</cffunction>
	<!--- Render via cfc for primarily for being able to add on the fly without having to refresh and reparse data --->
	<cffunction name="RenderCPNewNew" access="remote" output="false" hint="Render a Control Point in HTML ">
	    <cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>
	    <cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
	    <cfargument name="inpPreview" type="any" required="false" default="0" hint="Just preview - no edit options"/>
	    <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional" />
	    <cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
	    <cfargument name="inpUserId" TYPE="any" required="no" default="0" hint="Input user ID for admin preview - Optional" />
	    <cfargument name="inpdisplayIntervalType" TYPE="any" required="no" default="0" hint="Input display interval - Optional" />
	    <cfargument name="inpTemplateId" TYPE="any" required="no" default="0" hint="Input template id - Optional" />

	    <!--- Default Return Structure --->
	    <cfset var dataout  = {} />

	    <!--- Set default return values --->
	    <cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.BATCHID = "" />
	    <cfset dataout.PID = "" />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.HTML  = "" />

	    <cfset var DebugStr = '' />
	    <cfset var CP_HTML = "" />
	    <cfset var StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
	    <cfset var subcriberArray = ''/>
	    <cfset var subcriberList = ''/>
	    <cfset var _OPTION = ''/>
	    <cfset var _ESOPTION = ''/>
	    <cfset var listStyleIndex = ''/>
	    <cfset var item = ''/>
	    <cfset var groupList = ''/>
	    <cfset var TypeListValue = "" />
	    <cfset var _CONDITIONS  = '' />
	    <cfset var CONDITIONITEM    = '' />
	    <cfset var RetVarGetGroupInfo   = '' />
	    <cfset var RetVarReadCPDataById = '' />
	    <cfset var CONDITIONSXML    = '' />
	    <cfset var NoConditionsSpecified = '1' />
	    <cfset var BuilderOptionTypeVal = "" />
	    <cfset var ConvertedAlphaOptionList = "" />
	    <cfset var iBOV = '' />
	    <cfset var SendAsUnicode = '' />
	    <cfset var MaxPerMessage = '' />
	    <cfset var UniSearchMatcher = '' />
	    <cfset var UniSearchPattern = '' />
	    <cfset var UpperCharacterLimit = '' />

	    <cfset var htmlId = ''/>
	    <cfset var afSeclected = ''/>
	    <cfset var seletedIType = '' />
	    <cfset var seletedIValue = '' />
	    <cfset var seletedIMRNR = '' />
	    <cfset var currentYear = '' />
	    <cfset var iAF = '' />
	    <cfset var i = ''/>
	    <cfset var iType = '' />
	    <cfset var iValue = ''/>
	    <cfset var iMRNR = '' />
	    <cfset var hour = '' />
	    <cfset var minute = '' />
	    <cfset var index = ''/>
	    <cfset var year = ''/>
	    <cfset var cpTextWithoutCDF = ''/>
	    <cfset var shortcode = '' />
	    <cfset var stringDateFormat=''>
	    <cfset var step=''>
		<cfset var textfull	= '' />
		<cfset var unicodeReg = ''/>

		<cfset var unicodeInfo = 'When unicode text has been detected in your message, it will count that as 2 characters instead of 1. <a target="_blank" href="https://www.siremobile.com/blog/support/unicode-text-mean-detected-message/" _target-"blank"> Learn More. </a>'/>

	    <cftry>

	        <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode">
	            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	        </cfinvoke>

	        <cfif arguments.inpTemplateId EQ 9 AND arguments.controlPoint.RQ GTE 4>
	            <cfset dataout.HTML = ""/>
	            <cfreturn dataout />
	        </cfif>
	        <cfswitch expression="#controlPoint.TYPE#" >

	            <cfcase value="STATEMENT">
	                <cfset TypeListValue = "Simple Message" />
	            </cfcase>

	            <cfcase value="SHORTANSWER">
	                <cfset TypeListValue = "Question & Wait for Answer" />
	            </cfcase>

	            <cfcase value="ONESELECTION">
	                <cfset TypeListValue = "Question & Wait for Answer" />
	            </cfcase>

	            <cfcase value="TRAILER">
	                <cfset TypeListValue = "Final Message - This will end the conversation" />
	            </cfcase>

	            <cfcase value="BRANCH">
	                <cfset TypeListValue = "Rules Engine" />
	            </cfcase>

	            <cfcase value="INTERVAL">
	                <cfset TypeListValue = "Step Action Interval" />
	            </cfcase>

	            <cfcase value="API">
	                <cfset TypeListValue = "API Call" />
	            </cfcase>

	            <cfcase value="OPTIN">
	                <cfset TypeListValue = "Capture Opt In" />
	            </cfcase>

	            <cfcase value="CDF">
	                <cfset TypeListValue = "Capture Custom Data Field" />
	            </cfcase>

	            <cfcase value="RESET">
	                <cfset TypeListValue = "Drip Marketing Reset" />
	            </cfcase>

	        </cfswitch>
	        <cfif !StructKeyExists(arguments.controlPoint, "OPT_NUM") >
	 			<cfset arguments.controlPoint.OPT_NUM = "4" />
 			</cfif>
			<cfif !StructKeyExists(arguments.controlPoint, "OPT_TYPE") >
	 			<cfset arguments.controlPoint.OPT_TYPE = "mo" />
 			</cfif>


	        <cfsavecontent variable="CP_HTML">
	            <cfoutput>
	                <cfset BuilderOptionTypeVal = "BuilderOption" />


	                <!--- Stop hiding these - only hide them in SWT flows --->
	                <cfset BuilderOptionTypeVal = "" />

	                <cfset htmlId = arguments.controlPoint.ID>

	                <div class="clearfix control-point #BuilderOptionTypeVal#" id="cp#controlPoint.RQ#" data-control-point-physical-id="#controlPoint.ID#" data-control-rq-id="#controlPoint.RQ#" data-control-point-type="#controlPoint.TYPE#" data-add-new="Edit" data-template-id="#arguments.inpTemplateId#" >

						<cfset arguments.controlPoint.length = len(ReReplaceNoCase(arguments.controlPoint.TEXT, '<[^>]*>', '', 'all'))>

						<div class="control-point-body">
						<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE) ) />

						<cfset cpTextWithoutCDF = REReplace(arguments.controlPoint.TEXT, "\{\%.+\%\}", "")/>

						<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
						<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", cpTextWithoutCDF ) ) />
						<!--- Look for character higher than 255 ASCII --->
						<cfif UniSearchMatcher.Find()>
							<cfset SendAsUnicode = 1 />
							<cfset MaxPerMessage = 66 />
							<cfset UpperCharacterLimit = 70 />
						<cfelse>
							<cfset SendAsUnicode = 0 />
							<cfset MaxPerMessage = 153 />
							<cfset UpperCharacterLimit = 160 />
						</cfif>

						<cfset arguments.controlPoint.TDESC = Replace(arguments.controlPoint.TDESC, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
						<cfset arguments.controlPoint.TGUIDE = Replace(arguments.controlPoint.TGUIDE, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
						<cfset textfull= controlPoint.TEXT>
						<cfset arguments.controlPoint.TEXT =REReplaceNoCase(arguments.controlPoint.TEXT,"\Reply YES to confirm, HELP for help or STOP to cancel.*$","")/>

						<cfswitch expression="#controlPoint.TYPE#">
							<cfcase value="API">
								<!--- <div>API Call</div> --->
								<cfif arguments.inpTemplateId EQ 9>
									<cfset var apiData = DeSerializeJSON(controlPoint.API_DATA)/>
									<cfif isDefined("apiData.inpListOfPhones")>
										<cfset var listOfPhones = listToArray(apiData.inpListOfPhones)/>
									<cfelse>
										<cfset var listOfPhones = ArrayNew(1)/>
									</cfif>
									<cfif isDefined("apiData.inpListOfEmails")>
										<cfset var listOfEmails = listToArray(apiData.inpListOfEmails)/>
									<cfelse>
										<cfset var listOfEmails =  ArrayNew(1)/>
									</cfif>

									<div class="cp-content">
									<div class="row">
										<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
											<h4 class="portlet-heading-new">

												<span class="tdesc">How do you want to be notified of new chat responses?</span>
												<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='All inbound messages will be forwarded to the selected contact info in real time.'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
											</h4>
											<div class="form-gd">
												<div class="form-group">
													<div class="uk-grid uk-grid-small block-contact-method">
														<div class="uk-width-1-1@l">
															<div class="uk-grid uk-grid-small sms-number-wrap #(len(listOfPhones) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
																<div class="uk-width-auto@s col-for-label">
																	<label class="check-method"><input id="tellmgr-sms-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfPhones) GT 0 ? "checked" : "")#> SMS Number</label>
																</div>
																<div class="uk-width-expand@s hold-more-content">
																	<cfif len(listOfPhones) GT 0>
																		<cfset var phone = '' />
																		<cfloop array="#listOfPhones#" index="phone">
																			<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
																				<div class="uk-width-expand">
																					<input type="text" class="form-control phone-style input-list-phone" value="#phone#"/>
																				</div>
																				<div class="uk-width-auto col-for-more">
																					<cfif len(listOfPhones) GT 1>
																						<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
																					</cfif>
																					<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
																				</div>
																			</div>
																		</cfloop>
																	<cfelse>
																		<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
																			<div class="uk-width-expand">
																				<input type="text" class="form-control phone-style input-list-phone" value=""/>
																			</div>
																			<div class="uk-width-auto col-for-more">
																				<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
																				<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
																			</div>
																		</div>
																	</cfif>
																</div>
															</div>

															<div class="uk-grid uk-grid-small email-add-wrap #(len(listOfEmails) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
																<div class="uk-width-auto@s col-for-label">
																	<label class="check-method"><input id="tellmgr-email-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfEmails) GT 0 ? "checked" : "")#> Email Address</label>
																</div>
																<div class="uk-width-expand@s hold-more-content">
																	<cfif len(listOfEmails) GT 0>
																		<cfset var email = '' />
																		<cfloop array="#listOfEmails#" index="email">
																			<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
																				<div class="uk-width-expand">
																					<input type="text" class="form-control validate[custom[email]] input-list-mail" value="#email#"/>
																				</div>
																				<div class="uk-width-auto col-for-more">
																					<cfif len(listOfEmails) GT 1>
																						<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
																					</cfif>
																					<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
																				</div>
																			</div>
																		</cfloop>
																	<cfelse>
																		<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
																			<div class="uk-width-expand">
																				<input type="text" class="form-control validate[custom[email]] input-list-mail" value=""/>
																			</div>
																			<div class="uk-width-auto col-for-more">
																				<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
																				<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
																			</div>
																		</div>
																	</cfif>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos">Next <i class="fa fa-caret-down"></i></a>
									</div>
								</cfif>
							</cfcase>
							<cfcase value="ONESELECTION">
								<div class="cp-content">
									<div class="row">
										<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
											<cfif arguments.inpTemplateId EQ 11>
												<h4 class="portlet-heading-new questionNumber"></h4>
												<p class="portlet-subheading">What type of question do you want to ask?</p>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
															<select class="form-control cp_type" data-cp-type='#controlPoint.TYPE#' data-anstemp="#controlPoint.ANSTEMP#">
																<option value="ONESELECTION" data-anstemp="1" <cfif controlPoint.ANSTEMP is 1>selected</cfif>>Yes or No?</option>
																<option value="ONESELECTION" data-anstemp="2" <cfif controlPoint.ANSTEMP is 2>selected</cfif>>True or False?</option>
																<option value="ONESELECTION" data-anstemp="3" <cfif controlPoint.ANSTEMP is 3>selected</cfif>>Agree or Disagree?</option>
																<option value="ONESELECTION" data-anstemp="4" <cfif controlPoint.ANSTEMP is 4>selected</cfif>>Useful or Not?</option>
																<option value="ONESELECTION" data-anstemp="5" <cfif controlPoint.ANSTEMP is 5>selected</cfif>>Easy or Not?</option>
																<option value="ONESELECTION" data-anstemp="6" <cfif controlPoint.ANSTEMP is 6>selected</cfif>>Likely or Not?</option>
																<option value="ONESELECTION" data-anstemp="7" <cfif controlPoint.ANSTEMP is 7>selected</cfif>>Important or Not?</option>
																<option value="ONESELECTION" data-anstemp="8" <cfif controlPoint.ANSTEMP is 8>selected</cfif>>Satisfied or Not?</option>
																<option value="ONESELECTION" data-anstemp="9" <cfif controlPoint.ANSTEMP is 9>selected</cfif>>Net Promoter Score</option>
																<option value="SHORTANSWER" >Open Ended</option>
																<option value="ONESELECTION" data-anstemp="0" <cfif controlPoint.ANSTEMP is 0>selected</cfif>>Custom</option>
															</select>
													</div>
												</div>
											</cfif>
											<h4 class="#arguments.inpTemplateId EQ 11 ? 'portlet-subheading' : 'portlet-heading-new' #">

												<span class="tdesc">#controlPoint.TDESC#</span>
												<cfif trim(#controlPoint.TDESC#) NEQ ''>
													<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
												</cfif>
											</h4>
											<div class="tguide hidden">#controlPoint.TGUIDE#</div>
											<div class="form-gd">
												<div class="form-group">
													<textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required] ">#controlPoint.TEXT#</textarea>
													<div class="row">
														<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
															<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
																Character Count {#len(controlPoint.TEXT)#}
															</h5>
															<span>
																<a tabindex="0" class="info-popover pull-right see-unicode-mess  <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#unicodeInfo#'><img  src='../assets/layouts/layout4/img/info.png '/></a>
																<a class="see-unicode-mess see-unicode-mess-edit pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message &nbsp</a>
															</span>
														</div>
													</div>

												</div>
											</div>
										</div>
										<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
											<div class="movi-1">
												<cfif ListFind("11",arguments.inpTemplateId) EQ 0>
												<div class="movi-short-code">
													<button class=" movi__btn">Short Code</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-keyword">
													<button class=" movi__btn">Keyword</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-message">
													<button class=" movi__btn">Message</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												</cfif>
												<div class="movi-heading">
													<h3 class="movi-heading__number">#session.Shortcode#</h3>
												</div>
												<div class="movi-body">
													<cfif ListFind("11",arguments.inpTemplateId) EQ 0>
													<div class="media chat__item chat--sent">
														<div class="media-body media-bottom">
															<div class="chat__text" style="min-width:100px; min-height:35px; max-width:100px;">
																<span class="span_keyword">Localdinner</span>
															</div>
														</div>
														<div class="media-right media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-women.png" />
															</div>
														</div>
													</div>
													</cfif>
													<div class="media chat__item chat--receive">
														<div class="media-left media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-men.png" />
															</div>
														</div>
														<div class="media-body media-bottom ">
															<div class="chat__text">
																<span class="span_text">
																	<cfif Len(controlPoint.TEXT) GT 85>
																		#left(controlPoint.TEXT,82)#...
																	<cfelse>
																		#controlPoint.TEXT#
																	</cfif>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div><!--End Mobile View -->

										</div>
									</div>
									<div class="row">
										<div class="col-lg-11">
											<div class="row">
												<div class="col-lg-9">
													<h4 class="portlet-heading2">Answer Choices List</h4>

													<div class="answer-wrap clearfix">
														<label for="">Select an answer format:</label>
														<div class="answer-wrap-select">
															<select id="AF" class="form-control AF-select AF_#htmlId# Select2">
																<cfloop array="#_CPONESELECTAF#" index="iAF">
																	<cfif iAF[1] EQ arguments.controlPoint.AF>
																		<cfset afSeclected = 'selected="selected"'/>
																	<cfelse>
																		<cfset afSeclected = ""/>
																	</cfif>
																	<option value="#iAF[1]#" #afSeclected# >#iAF[2]#</option>
																</cfloop>
															</select>
														</div>
													</div>

													<div class="row">
														<div class="col-lg-11">
															<div class="wrapper-answer-text">
																<div class="form-gd field-anser-holder">
																	<div class="row row-small">
																		<div class="col-xs-8 col-sm-10">
																			<div class="form-group mb-0">
																				<label>Enter your answer choices</label>
																			</div>
																		</div>
																		<div class="col-xs-2 col-sm-1 hidden">
																			<div class="form-group text-center mb-0">
																				<label>S-Val</label>
																			</div>
																		</div>
																		<div class="col-xs-2 col-sm-1">
																			<div class="form-group text-center mb-0">
																				<label>Delete</label>
																			</div>
																		</div>
																	</div>
																	<div class="answers_list">
																		<cfif arrayLen(controlPoint.ANSWERS) GT 0>
																			<cfloop array="#controlPoint.ANSWERS#" index="i">
																				<div class="AnswerItem row row-small">
																					<div class="col-xs-8 col-sm-10">
																						<div class="form-group">
																							<input id="OPTION" type="text" value="#i['TEXT']#" class="form-control" />
																						</div>
																					</div>
																					<div class="col-xs-2 col-sm-1 hidden">
																						<div class="form-group">
																							<input type="text" id="AVAL" class="form-control readonly-val text-center" value="#i['AVAL']#" />
																							<input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="#i['AVALREG']#" />
																							<input type="hidden" id="AVALRESP" class="form-control readonly-val text-center" value="#i['AVALRESP']#" />
																							<input type="hidden" id="AVALREDIR" class="form-control readonly-val text-center" value="#i['AVALREDIR']#" />
																							<input type="hidden" id="AVALNEXT" class="form-control readonly-val text-center" value="#i['AVALNEXT']#" />
																						</div>
																					</div>
																					<div class="col-xs-2 col-sm-1">
																						<div class="form-group">
																							<button class="form-control delete-answer">
																								<i class="fa fa-minus"></i>
																							</button>
																						</div>
																					</div>
																				</div>
																			</cfloop>
																		</cfif>

																	</div>
																</div>

																<div class="row row-small">
																	<div class="col-xs-4 col-xs-offset-7 col-sm-2 col-sm-offset-9">
																		<a href="##" id="add-more-answer" class="btn btn-block grey-mint add-more-answer">ADD MORE</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="col-lg-3 hidden">
													<h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
													<h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>

													<div class="row">
														<div class="col-lg-10">
															<div class="list-answer-template">
																<ul class="nav">
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-1-#htmlId#" class="ans-group" value='1'>
																		<label class="lbl-ans-group" for="ans-temp-1-#htmlId#">Yes or No?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-2-#htmlId#" class="ans-group" value='2'>
																		<label class="lbl-ans-group" for="ans-temp-2-#htmlId#">True or False?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-3-#htmlId#" class="ans-group" value='3'>
																		<label class="lbl-ans-group" for="ans-temp-3-#htmlId#">Agree or Disagree?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-4-#htmlId#" class="ans-group" value='4'>
																		<label class="lbl-ans-group" for="ans-temp-4-#htmlId#">Useful or Not?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-5-#htmlId#" class="ans-group" value='5'>
																		<label class="lbl-ans-group" for="ans-temp-5-#htmlId#">Easy or Not?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-6-#htmlId#" class="ans-group" value='6'>
																		<label class="lbl-ans-group" for="ans-temp-6-#htmlId#">Likely or Not?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-7-#htmlId#" class="ans-group" value='7'>
																		<label class="lbl-ans-group" for="ans-temp-7-#htmlId#">Important or Not?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-8-#htmlId#" class="ans-group" value='8'>
																		<label class="lbl-ans-group" for="ans-temp-8-#htmlId#">Satisfied or Not?</label>
																	</li>
																	<li>
																		<input type="radio" name="ans-group" id="ans-temp-9-#htmlId#" class="ans-group" value='9'>
																		<label class="lbl-ans-group" for="ans-temp-9-#htmlId#">Net Promoter Score</label>
																	</li>
																</ul>
															</div>

															<a href="##" class="btn green-gd btn-add-answer-list-template" data-answer-list="answers_list_#htmlId#">apply</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<cfif arguments.inpdisplayIntervalType EQ 1 >
									<!--- <cfif arguments.inpTemplateId EQ 7 > --->
										<div class="row">
											<div class="col-lg-12">
												<h4 class="portlet-heading2"><a class="toogle-inteval">Response Interval Settings (Optional)<span class="glyphicon glyphicon-chevron-down set-margin"></span></a></h4>
												<!--- <a class="toogle-inteval">Hide</a> --->
												<div class="div-toogle-inteval hidden">
													<p class="des-for-heading">
														Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on
														how long you wish to wait before moving on to the next specified Action if no response is received.
													</p>

													<div class="form-gd">
														<div class="row row-small">
															<div class="col-lg-2 col-sm-6">
																<div class="form-group">
																	<label for="">Interval Time</label>
																	<select name="ITYPE" class="form-control ITYPE Select2">
																		<cfloop array="#_CPINTEVALTYPE_1#" index="iType">
																			<cfset seletedIType = ''>
																			<cfif arguments.controlPoint.ITYPE EQ iType[1]>
																				<cfset seletedIType = 'SELECTED="SELECTED"'>
																			</cfif>
																			<option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
																		</cfloop>
																	</select>
																</div>
															</div>

															<div class="col-lg-3 col-sm-6">
																<div class="form-group">
																	<label for="">&nbsp;</label>
																	<select id="IVALUE" class="form-control Select2 IVALUE">
																		<cfloop from="0" to="60" index="iValue">
																			<cfset seletedIValue = ''>
																			<cfif arguments.controlPoint.IVALUE EQ iValue>
																				<cfset seletedIValue = 'SELECTED="SELECTED"'>
																			</cfif>
																			<option value="#iValue#" #seletedIValue#> #iValue# </option>
																		</cfloop>
																	</select>
																</div>
															</div>
															<div class="col-lg-3 col-sm-6">
																<div class="form-group">
																	<label for="">Retry on No Response?</label>
																	<select id="IMRNR" class="form-control Select2 IMRNR">
																			<cfloop array="#_IMRNR#" index="iMRNR">
																			<cfset seletedIMRNR = ''>
																			<cfif arguments.controlPoint.IMRNR EQ iMRNR[1]>
																				<cfset seletedIMRNR = 'SELECTED="SELECTED"'>
																			</cfif>
																			<option value="#iMRNR[1]#" #seletedIMRNR#> #iMRNR[2]# </option>
																		</cfloop>
																	</select>
																</div>
															</div>

															<div class="col-lg-4 col-sm-6">
																<div class="form-group">
																	<label for="">Still No Response?</label>
																	<select id="INRMO" class="form-control Select2 INRMO">
																		<option value="END" <cfif arguments.controlPoint.INRMO EQ 'END'>SELECTED="SELECTED"</cfif> >End Conversation</option>
																		<option value="NEXT" <cfif arguments.controlPoint.INRMO EQ 'NEXT'>SELECTED="SELECTED"</cfif> > Proceed to Next Action</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<div class="row row-small mt-30 mb-30 add-another-question-block">
											<div class="row col-lg-6">
												<div class="add-remove-question">
													<span class="add-another-question add-another-question1">
														<a href="##" class="btn green-gd btn-add-another">add another question</a>
													</span>
													<span class="add-another-question add-another-simon">
														<a href="##" class="btn blue-gd btn-remove-cp btn-simon-remove">REMOVE</a>
													</span>
												</div>
											</div>

											<div class="col-lg-6 paging-content add-question-paging" >

											</div>
										</div>

								</cfif>

							</cfcase>
							<cfcase value="SHORTANSWER">
								<div class="cp-update-cpn hidden">
									<p>
										Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name needs to appear before your message.
										<a href="##" id="update-profile-from-cp">Change Profile Name</a>
									</p>
								</div>

								<div class="cp-content">
									<div class="row">
										<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
											<cfif arguments.inpTemplateId EQ 11>
												<h4 class="portlet-heading-new questionNumber"></h4>
												<p class="portlet-subheading">What type of question do you want to ask?</p>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														<select class="form-control cp_type" data-cp-type="#controlPoint.TYPE#" data-anstemp="">
															<option value="ONESELECTION" data-anstemp="1">Yes or No?</option>
															<option value="ONESELECTION" data-anstemp="2">True or False?</option>
															<option value="ONESELECTION" data-anstemp="3">Agree or Disagree?</option>
															<option value="ONESELECTION" data-anstemp="4">Useful or Not?</option>
															<option value="ONESELECTION" data-anstemp="5">Easy or Not?</option>
															<option value="ONESELECTION" data-anstemp="6">Likely or Not?</option>
															<option value="ONESELECTION" data-anstemp="7">Important or Not?</option>
															<option value="ONESELECTION" data-anstemp="8">Satisfied or Not?</option>
															<option value="ONESELECTION" data-anstemp="9">Net Promoter Score</option>
															<option value="SHORTANSWER" selected>Open Ended</option>
															<option value="ONESELECTION" data-anstemp="0">Custom</option>
														</select>
													</div>
												</div>
											</cfif>

											<h4 class="#arguments.inpTemplateId EQ 11 ? 'portlet-subheading' : 'portlet-heading-new' #">

												<span class="tdesc">#controlPoint.TDESC#</span>
												<cfif arguments.inpTemplateId EQ 1>
													<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#, reply YES to confirm, HELP for help or STOP to cancel. Up to #arguments.controlPoint.OPT_NUM# msg/#arguments.controlPoint.OPT_TYPE#. Msg&data rates may apply'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
												<cfelse>
													<cfif trim(#controlPoint.TDESC#) NEQ ''>
														<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
													</cfif>
												</cfif>
											</h4>
											<div class="tguide hidden">#controlPoint.TGUIDE#</div>
											<div class="form-gd">
												<div class="form-group">
													<textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required] ">#controlPoint.TEXT#</textarea>

													<div class="row">
														<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12 pull-right">

															<div class="pull-right" style="width:100%">
																<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
																	Character Count {#len(controlPoint.TEXT)#}
																</h5>
															</div>
															<div class="pull-right" style="display: flex; justify-content: space-between;">
																<a class="see-unicode-mess see-unicode-mess-edit see-unicode-mess <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message &nbsp</a>
																<a tabindex="0" class="info-popover see-unicode-mess pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#unicodeInfo#'><img  src='../assets/layouts/layout4/img/info.png '/></a>
															</div>
														</div>
														<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
															<cfif ListFind("1,8", arguments.inpTemplateId)  GT 0>
																<p class="yes-help-stop">
																<i>
																	Reply YES to confirm, HELP for help or STOP to cancel.</br>
																	Up to
																	<span id='opt-select-val'>
																		<select id="select-select-val" class="form-control">
																			<cfloop from="1" to="9" index="i">
																				<option value="#i#" <cfif arguments.controlPoint.OPT_NUM EQ i> selected</cfif>>#i#</option>
																			</cfloop>
																		</select>
																	</span>msg/
																	<span id='opt-select-type'>
																		<select id="select-select-type" class="form-control">
																			<option value="mo" <cfif arguments.controlPoint.OPT_TYPE EQ 'mo'> selected</cfif> >mo</option>
																			<option value="wk" <cfif arguments.controlPoint.OPT_TYPE EQ 'wk'> selected</cfif>>wk</option>
																			<option value="day" <cfif arguments.controlPoint.OPT_TYPE EQ 'day'> selected</cfif>>day</option>
																			<option value="qtr" <cfif arguments.controlPoint.OPT_TYPE EQ 'qtr'> selected</cfif>>qtr</option>
																		</select>
																	</span>. Msg&Data rates may apply.
																	<span>
																		<a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content="
																			In order to stay compliant with FCC Regulations and the Telephone Consumer Protection Act (TCPA) along with CAN-SPAM Act, all text marketers must include the following in their opt-in messages.

																			</br></br>a.	A clear opt-in confirmation
																			</br>b.	A way for customers to obtain help
																			</br>c.	A clear option to stop receiving messages
																			</br>d.	The message frequency
																			</br>e.	A disclaimer that message & data rates may apply

																			</br></br>We make this easy on you by automatically including it in your campaign.  Just adjust the message frequency and you’re done!"
																			data-original-title=""><img src="../assets/layouts/layout4/img/info.png"></a>
																	</span>
																</i>
																</p>
															<cfelse>

															</cfif>
														</div>


													</div>

												</div>
											</div>

										</div>
										<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
											<div class="movi-1">
												<cfif ListFind("11",arguments.inpTemplateId) EQ 0>
												<div class="movi-short-code">
													<button class=" movi__btn">Short Code</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-keyword">
													<button class=" movi__btn">Keyword</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-message">
													<button class=" movi__btn">Message</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												</cfif>
												<div class="movi-heading">
													<h3 class="movi-heading__number">#session.Shortcode#</h3>
												</div>
												<div class="movi-body">
													<cfif ListFind("11",arguments.inpTemplateId) EQ 0>
													<div class="media chat__item chat--sent">
														<div class="media-body media-bottom">
															<div class="chat__text" style="min-width:100px; min-height:35px; max-width:100px;">
																<span class="span_keyword">Localdinner</span>
															</div>
														</div>
														<div class="media-right media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-women.png" />
															</div>
														</div>
													</div>
													</cfif>
													<div class="media chat__item chat--receive">
														<div class="media-left media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-men.png" />
															</div>
														</div>
														<div class="media-body media-bottom ">
															<div class="chat__text">
																<span class="span_text">
																	<cfif Len(textfull) GT 80>
																		#left(textfull,80)#...
																	<cfelse>
																		#textfull#
																	</cfif>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div><!--End Mobile View -->

										</div>
									</div>
									<cfif arguments.inpTemplateId EQ 11 AND controlPoint.TYPE EQ "SHORTANSWER">
									<cfelse>
										<a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos">Next <i class="fa fa-caret-down"></i></a>
									</cfif>
								</div>
								<cfif arguments.inpdisplayIntervalType EQ 1 >
									<div class="row">
										<div class="col-lg-12">
											<h4 class="portlet-heading2"><a class="toogle-inteval">Response Interval Settings (Optional)<span class="glyphicon glyphicon-chevron-down set-margin"></span></a></h4>
											<!--- <a class="toogle-inteval">Hide</a> --->
											<div class="div-toogle-inteval hidden">
												<p class="des-for-heading">
													Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on
													how long you wish to wait before moving on to the next specified Action if no response is received.
												</p>

												<div class="form-gd">
													<div class="row row-small">
														<div class="col-lg-2 col-sm-6">
															<div class="form-group">
																<label for="">Interval Time</label>
																<select name="ITYPE" class="form-control ITYPE Select2">
																	<cfloop array="#_CPINTEVALTYPE_1#" index="iType">
																		<cfset seletedIType = ''>
																		<cfif arguments.controlPoint.ITYPE EQ iType[1]>
																			<cfset seletedIType = 'SELECTED="SELECTED"'>
																		</cfif>
																		<option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
																	</cfloop>
																</select>
															</div>
														</div>

														<div class="col-lg-3 col-sm-6">
															<div class="form-group">
																<label for="">&nbsp;</label>
																<select id="IVALUE" class="form-control IVALUE Select2">
																	<cfloop from="0" to="60" index="iValue">
																		<cfset seletedIValue = ''>
																		<cfif arguments.controlPoint.IVALUE EQ iValue>
																			<cfset seletedIValue = 'SELECTED="SELECTED"'>
																		</cfif>
																		<option value="#iValue#" #seletedIValue#> #iValue# </option>
																	</cfloop>
																</select>
															</div>
														</div>
														<div class="col-lg-3 col-sm-6">
															<div class="form-group">
																<label for="">Retry on No Response?</label>
																<select id="IMRNR" class="form-control Select2 IMRNR">
																		<cfloop array="#_IMRNR#" index="iMRNR">
																		<cfset seletedIMRNR = ''>
																		<cfif arguments.controlPoint.IMRNR EQ iMRNR[1]>
																			<cfset seletedIMRNR = 'SELECTED="SELECTED"'>
																		</cfif>
																		<option value="#iMRNR[1]#" #seletedIMRNR#> #iMRNR[2]# </option>
																	</cfloop>
																</select>
															</div>
														</div>

														<div class="col-lg-4 col-sm-6">
															<div class="form-group">
																<label for="">Still No Response?</label>
																<select id="INRMO" class="form-control Select2 INRMO">
																	<option value="END" <cfif arguments.controlPoint.INRMO EQ 'END'>SELECTED="SELECTED"</cfif> >End Conversation</option>
																	<option value="NEXT" <cfif arguments.controlPoint.INRMO EQ 'NEXT'>SELECTED="SELECTED"</cfif> > Proceed to Next Action</option>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="row row-small mt-30 mb-30 add-another-question-block">
										<div class="row col-lg-6">
											<div class="add-remove-question">
												<span class="add-another-question add-another-question1">
													<a href="##" class="btn green-gd btn-add-another">add another question</a>
												</span>
												<span class="add-another-question add-another-simon">
													<a href="##" class="btn blue-gd btn-remove-cp btn-simon-remove">REMOVE</a>
												</span>
											</div>
										</div>

										<div class="col-lg-6 paging-content add-question-paging" >
											<!--- <div class="dataTables_paginate paging_input"><span class="paginate_button first"><img class="paginate-arrow" src="/session/sire/images/double-left.png"></span><span class="paginate_button previous" id="tblCampaignList_previous"><img class="paginate-arrow" src="/session/sire/images/left.png"></span><span class="paginate_page">Page </span><input type="text" class="paginate_text" style="display: inline;"><span class="paginate_of"> of 2</span><span class="paginate_button next" id="tblCampaignList_next"><img class="paginate-arrow" src="/session/sire/images/right.png"></span><span class="paginate_button last" id="tblCampaignList_last"><img class="paginate-arrow" src="/session/sire/images/double-right.png"></span></div> --->
										</div>
									</div>
							</cfif>
							</cfcase>
							<cfcase value="INTERVAL">

								<cfset var selectDate = ''>
								<cfset var selectYear = ''>
								<cfset var selectMonth = ''>
								<cfset var selectDay = ''>


								<cfset stringDateFormat="">
								<cfif arguments.controlPoint.ITYPE EQ 'DATE'>
									<cfset selectDate = listToArray(arguments.controlPoint.IVALUE,'/') >
									<cfset selectYear = selectDate[1]>
									<cfset selectMonth = selectDate[2]>
									<cfset selectDay = selectDate[3]>
									<cfset stringDateFormat=selectMonth & "/"&  selectDay & "/" & selectYear>
								</cfif>

								<cfif arguments.inpTemplateId EQ 8>
									<div class="item-another-mesage">
										<div class="row row-small">

											<div class="col-lg-11 col-md-11">
												<div class="row row-small form-gd">
													<div class="col-sm-12">
														<div class="form-group mb-0">

															<h4 class="portlet-subheading">
															When do you want this to send?
															<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right" data-html="true" title="" data-content="Set the interval at which you would like this to send." data-original-title=""><img src="../assets/layouts/layout4/img/info.png "></a></span>
															</h4>
														</div>
													</div>
													<div class="col-sm-2">
														<div class="form-group">
															<select name="ITYPE" class="form-control itype-select-new Select2 ITYPE">
																<option value="SECONDS" <cfif arguments.controlPoint.ITYPE is "SECONDS">selected</cfif> >Immediately</option>
																<option value="HOURS" <cfif arguments.controlPoint.ITYPE is "HOURS">selected</cfif>>Hour(s)</option>
																<option value="DAYS" <cfif arguments.controlPoint.ITYPE is "DAYS">selected</cfif>>Day(s)</option>
																<option value="WEEKS" <cfif arguments.controlPoint.ITYPE is "WEEKS">selected</cfif>>Week(s)</option>
																<option value="DATE" <cfif arguments.controlPoint.ITYPE is "DATE">selected</cfif>>Select Date</option>
															</select>
														</div>
													</div>
													<div class="col-sm-2">
														<div class="form-group">
															<div class="selected-interval #(arguments.controlPoint.ITYPE EQ "SECONDS" OR arguments.controlPoint.ITYPE EQ "DATE") ? "hidden" : ""#">
																<div class="form-group">
																	<select name="selected-hour-select" id="selected-hour-select-#controlPoint.RQ#" class="form-control Select2 IVALUE">
																		<cfloop from="0" to="60" index="step">
																			<option value="#step#" <cfif arguments.controlPoint.IVALUE EQ step>selected</cfif> >#step#</option>
																		</cfloop>
																	</select>
																</div>
															</div>
															<div class="selected-date #(arguments.controlPoint.ITYPE NEQ "DATE") ? "hidden" : ""#">
																<div class="form-group">
																	<div class="input-group datetimepicker date set-z-index" id="selected-date-select-#controlPoint.RQ#">
																		<input type="text" class="form-control" value="#stringDateFormat#">
																		<span class="input-group-addon">
																			<span class="glyphicon glyphicon-calendar"></span>
																		</span>
																	</div>
																</div>
															</div>
															<div class="hidden">
																<div class="form-group">
																	<select name="" id="" class="form-control ITIME Select2">
																	<cfloop from="0" to="23" index="hour">
																		<cfloop from="0" to="59" index="minute" step="15">
																			<cfset var time = '#(hour LT 10) ? '0'&hour : hour#:#(minute LT 10) ? '0'&minute : minute#'/>
																			<cfset var checked = 0>
																			<cfset var checkHour = int(arguments.controlPoint.INOON) EQ 0 ? arguments.controlPoint.IHOUR : int(arguments.controlPoint.IHOUR) + 12/>
																			<cfif int(arguments.controlPoint.IHOUR) EQ 0>
																				<cfset checkHour = int(arguments.controlPoint.INOON) EQ 0 ? 0 : 12/>
																			</cfif>
																			<cfif 0 EQ int(minute) AND 9 EQ int(hour) >
																				<cfset checked = 1/>
																			</cfif>
																			<option value="#time#" <cfif checked EQ 1>selected</cfif> >#time#</option>
																		</cfloop>
																	</cfloop>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-8">

													</div>

												</div>
											</div>
										</div>
									</div>
								</cfif>


							</cfcase>
							<cfcase value="STATEMENT">
								<div class="cp-update-cpn hidden">
									<p>
										Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name needs to appear before your message.
										<a href="##" id="update-profile-from-cp">Change Profile Name</a>
									</p>
								</div>
								<div class="cp-content">
									<div class="row">
										<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
											<cfif  arguments.inpTemplateId EQ 8>
												<h4 class="portlet-subheading">
													<span class="tdesc">#controlPoint.TDESC#</span>
												</h4>
											<cfelse>
												<h4 class="portlet-heading-new">
													<span class="tdesc">#controlPoint.TDESC#</span>
													<cfif trim(#controlPoint.TDESC#) NEQ ''>
														<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
													</cfif>
												</h4>

											</cfif>

											<div class="tguide hidden">#controlPoint.TGUIDE#</div>
											<div class="form-gd">
												<div class="form-group">
													<textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required]">#controlPoint.TEXT#</textarea>
													<div class="row">
														<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
															<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
																Character Count {#len(controlPoint.TEXT)#}
															</h5>
															<span>
																<a tabindex="0" class="info-popover pull-right see-unicode-mess  <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#unicodeInfo#'><img  src='../assets/layouts/layout4/img/info.png '/></a>
																<a class="see-unicode-mess see-unicode-mess-edit pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message &nbsp</a>
															</span>
														</div>

													</div>

												</div>
											</div>

										</div>
										<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
											<div class="movi-1">
												<cfif arguments.inpTemplateId NEQ 8>
												<div class="movi-short-code">
													<button class=" movi__btn">Short Code</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-keyword">
													<button class=" movi__btn">Keyword</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												<div class="movi-message">
													<button class=" movi__btn">Message</button>
													<div class="line"></div>
													<span class="dot"></span>
												</div>
												</cfif>
												<div class="movi-heading">
													<h3 class="movi-heading__number">#session.Shortcode#</h3>
												</div>
												<div class="movi-body">
													<cfif arguments.inpTemplateId NEQ 8>
													<div class="media chat__item chat--sent">
														<div class="media-body media-bottom">
															<div class="chat__text" style="min-width:100px; min-height:35px; max-width:100px;">
																<span class="span_keyword">Localdinner</span>
															</div>
														</div>
														<div class="media-right media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-women.png" />
															</div>
														</div>
													</div>
													</cfif>
													<div class="media chat__item chat--receive">
														<div class="media-left media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-men.png" />
															</div>
														</div>
														<div class="media-body media-bottom ">
															<div class="chat__text">
																<span class="span_text">
																	<cfif Len(controlPoint.TEXT) GT 85>
																		#left(controlPoint.TEXT,82)#...
																	<cfelse>
																		#controlPoint.TEXT#
																	</cfif>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div><!--End Mobile View -->

										</div>
									</div>
									<a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos">Next <i class="fa fa-caret-down"></i></a>
								</div>

							</cfcase>
							<cfcase value="TRAILER">

								<div class="row">
									<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
										<h4 class="portlet-heading-new">
											<span class="tdesc">#controlPoint.TDESC#</span>
											<cfif trim(#controlPoint.TDESC#) NEQ '' AND arguments.inpTemplateId NEQ 3>
												<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#'><img  src='../assets/layouts/layout4/img/info.png'/></a></span>
											</cfif>
										</h4>
										<div class="tguide hidden">#controlPoint.TGUIDE#</div>

										<div class="form-gd">
											<div class="form-group">
												<textarea data-emojiable="true" maxLength="459" name="" id="" data-control="text-message" cols="30" rows="5" class="text form-control validate[required]">#controlPoint.TEXT#</textarea>
												<div class="row">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
															Character Count {#len(controlPoint.TEXT)#}
														</h5>
														<span>
															<a tabindex="0" class="info-popover pull-right see-unicode-mess  <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#unicodeInfo#'><img  src='../assets/layouts/layout4/img/info.png '/></a>
															<a class="see-unicode-mess see-unicode-mess-edit pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">You have Unicode characters in your message &nbsp</a>
														</span>
													</div>

												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
										<div class="movi-1">
											<div class="movi-heading">
												<h3 class="movi-heading__number">#session.Shortcode#</h3>
											</div>
											<div class="movi-body">
												<cfif ListFind("3,11",arguments.inpTemplateId) EQ 0>
													<div class="media chat__item chat--sent">
														<div class="media-body media-bottom">
															<div class="chat__text">
																<!--- HARD CODE YES/NO TEXT FOR TRAILAER ONLY--->
																<cfif arguments.inpTemplateId EQ 1>
																	<cfif htmlId EQ 13>
																		<span >YES</span>
																	<cfelseif htmlId EQ 12>
																		<span >NO</span>
																	</cfif>
																<cfelseif arguments.inpTemplateId EQ 8>
																	<span >NO</span>
																<cfelse>
																	<span class="span_keyword">Localdinner</span>
																</cfif>
															</div>
														</div>
														<div class="media-right media-bottom">
															<div class="chat__user">
																<img src="../assets/layouts/layout4/img/avatar-women.png" />
															</div>
														</div>
													</div>
												</cfif>
												<div class="media chat__item chat--receive">
													<div class="media-left media-bottom">
														<div class="chat__user">
															<img src="../assets/layouts/layout4/img/avatar-men.png" />
														</div>
													</div>
													<div class="media-body media-bottom ">
														<div class="chat__text">
															<span class="span_text">
																<cfif Len(controlPoint.TEXT) GT 85>
																	#left(controlPoint.TEXT,82)#...
																<cfelse>
																	#controlPoint.TEXT#
																</cfif>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div><!--End Mobile View -->

									</div>
								</div>
								<cfif arguments.inpTemplateId EQ 8>
									<a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos save-mlp-cpg">Next <i class="fa fa-caret-down"></i></a>
								<cfelse>
									<a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos">Next <i class="fa fa-caret-down"></i></a>
								</cfif>
							</cfcase>
							<cfcase value="BRANCH">

								<!--- Single condition Check --->
								<cfif ArrayLen(controlPoint.CONDITIONS) EQ 1>
									<div class="hidden">Single Condition!</div>
								</cfif>

								<!--- Reset this flag for each Branch --->
								<cfset NoConditionsSpecified = '1' />


								<cfloop array="#controlPoint.CONDITIONS#" index="_CONDITIONS">

									<!--- Flag the fact that at least one condition is defined for this branch --->
									<cfset NoConditionsSpecified = '0' />

									<div class="hidden">
										<span class="">Condition ###_CONDITIONS.CID# - #_CONDITIONS.TYPE#</span>
									</div>

									<!--- This Rule will look at question X and move to Question Y if the answer to Question X is Z --->

									<!--- Look up Question Text --->

									<cfif _CONDITIONS.TYPE EQ "RESPONSE" >

										<cfif _CONDITIONS.BOQ GT 0>

											<!--- Only display message if a real question is found OK --->
											<!--- BOQ stores the Physical "ID" of the question --->
											<cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
												<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
												<cfinvokeargument name="inpQID" value="#_CONDITIONS.BOQ#">
												<cfinvokeargument name="inpIDKey" value="ID">
												<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
												<cfinvokeargument name="inpUserId" value="#inpUserId#">
											</cfinvoke>

											<cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

												<div class="hidden">
													<h5>For Question at Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
													<span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
												</div>

												<cfset ConvertedAlphaOptionList = "">

												<cfif _CONDITIONS.BOV NEQ ''>

													<cfloop list="#_CONDITIONS.BOV#" delimiters="," index="iBOV">

														<cfif LEN(ConvertedAlphaOptionList) EQ 0>
															<cfset ConvertedAlphaOptionList = "#CHR(iBOV +64)#">
														<cfelse>
															<cfset ConvertedAlphaOptionList = ConvertedAlphaOptionList & ",#CHR(iBOV + 64)#">
														</cfif>

													</cfloop>

													<div class="hidden">
														<h5 style="margin-bottom: 0;">If this Question is Answered with: </h5>
														<cfswitch expression="#RetVarReadCPDataById.CPObj.AF#">
															<cfcase value="NOFORMAT">
																<span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfcase>

															<cfcase value="NUMERIC">
																<span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfcase>

																<cfcase value="NUMERICPAR">
																<span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfcase>

															<cfcase value="ALPHA">
																<span style="font-size:.7em;">#ConvertedAlphaOptionList# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfcase>

															<cfcase value="ALPHAPAR">
																<span style="font-size:.7em;">#ConvertedAlphaOptionList# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfcase>

															<cfdefaultcase>
																<span style="font-size:.7em;">#_CONDITIONS.BOV# <cfif _CONDITIONS.BOAV NEQ "">(OR) #_CONDITIONS.BOAV#</cfif> </span>
															</cfdefaultcase>
														</cfswitch>

													</div>

												</cfif>


												<cfif _CONDITIONS.BOAV NEQ '' AND _CONDITIONS.BOV EQ ''>

													<div class="hidden">
														<h5>If this Question is Answered with anything in the following list: </h5>
														<span style="font-size:.7em; word-break: break-word;">#_CONDITIONS.BOAV#</span>
													</div>

												</cfif>

												<cfif _CONDITIONS.BOTNQ GT 0>

													<!--- BOTNQ stores the Physical "ID" of the question --->
													<cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
														<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
														<cfinvokeargument name="inpQID" value="#_CONDITIONS.BOTNQ#">
														<cfinvokeargument name="inpIDKey" value="ID">
														<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
														<cfinvokeargument name="inpUserId" value="#inpUserId#">
													</cfinvoke>

													<div class="hidden">
														<h5>The conversation will move to Action ## #RetVarReadCPDataById.CPObj.RQ# </h5>
														<span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
													</div>
												<cfelse>

													<div class="hidden">
														<h5>The next Action is to Jump to the next Action in the sequence.</h5>
														<span style="font-size:.7em; font-style: italic;"></span>
													</div>

												</cfif>

											</cfif>
										</cfif>
									</cfif>

								</cfloop>

								<!--- Display message for default next action --->
								<cfif controlPoint.BOFNQ GT 0 >

									<!--- BOFNQ stores the Physical "ID" of the question --->
									<cfinvoke method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
										<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
										<cfinvokeargument name="inpQID" value="#controlPoint.BOFNQ#">
										<cfinvokeargument name="inpIDKey" value="ID">
										<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
										<cfinvokeargument name="inpUserId" value="#inpUserId#">
									</cfinvoke>

									<cfif NoConditionsSpecified EQ '1'>

										<!--- No Conditions defined so  --->

										<!--- Only display message if a real question is found OK --->
										<cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

											<div class="hidden">
												<h5>The next Action is to Jump to Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
												<span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
											</div>

										<cfelse>

											<div class="hidden">
												<h5>The next Action is to Jump to the next Action in the sequence.</h5>
												<span style="font-size:.7em; font-style: italic;"></span>
											</div>

										</cfif>

									<cfelse>

										<!--- Conditions defined so  --->

										<!--- Only display message if a real question is found OK --->
										<cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

											<div class="hidden">
												<h5>The default next Action if no conditions specified were met:<BR/>
													Jump to Action ## #RetVarReadCPDataById.CPObj.RQ#</h5>
												<span style="font-size:.7em; font-style: italic;">#RetVarReadCPDataById.CPObj.TEXT#</span>
											</div>

										<cfelse>

											<div class="hidden">
												<h5>The default next Action if no conditions specified were met:<BR/>
													Jump to next Action in the sequence.</h5>
												<span style="font-size:.7em; font-style: italic;"></span>
											</div>

										</cfif>

									</cfif>

								<cfelse>

									<cfif NoConditionsSpecified EQ '1'>

										<!--- No Conditions defined so  --->
										<div class="hidden">
											<h5>The next Action is to Jump to the next Action in the sequence.</h5>
											<span style="font-size:.7em; font-style: italic;"></span>
										</div>

									<cfelse>
										<!--- Conditions defined so  --->
										<div class="hidden">
											<h5>The default next Action if no conditions specified were met:<BR/>
												Jump to next Action in the sequence.</h5>
											<span style="font-size:.7em; font-style: italic;"></span>
										</div>

									</cfif>

								</cfif>

							</cfcase>
							<cfcase value="RESET">

	                        </cfcase>
							<cfdefaultcase>
								<div class="control-point-text Preview-Bubble Preview-Me">#controlPoint.TEXT#</div>
							</cfdefaultcase>

						</cfswitch>
	                    </div>
	                </div>
	            </cfoutput>
	        </cfsavecontent>

	        <cfset dataout.HTML  = CP_HTML />

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.HTML  = SerializeJSON(cfcatch) />

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#">
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#">

	        </cfcatch>
	    </cftry>

	    <cfreturn dataout />
	</cffunction>


	<cffunction name="AddNewBatchFromTemplateNew" access="remote" output="false" hint="Simple create Batch from Template Id">
		<cfargument name="inpTemplateId" required="true" hint="The template Id">
		<cfargument name="inpTemplateType" required="true" hint="The template Type">
		<cfargument name="inpDesc" required="false" default="" hint="The name of the Batch">
		<cfargument name="inpKeyword" required="false" default="" hint="A keyword to associate with the Batch">
		<cfargument name="inpShortCode" TYPE="string" hint="Shortcode to Set HELP / STOP for." />
		<cfargument name="inpAddList" TYPE="string" default="0" required="false" hint="Automatically Add a new Subscriber list and link to Opt Ins in this flow" />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = 0 />
		<cfset dataout.DESC = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.TEMPLATETYPE = 0 />
		<cfset dataout.CustomHelpMessage_vch = "" />
		<cfset dataout.CustomStopMessage_vch = "" />
		<cfset dataout.XMLControlString = "" />
		<cfset dataout.CPPID = "0" />

		<cfset var DebugStr	= '' />
		<cfset var templateXMLDetailQuery	= '' />
		<cfset var inpFormData = ''/>
		<cfset var RetVarDoDynamicTransformations = '' />
		<cfset var XMLControlString = ''/>
		<cfset var InsertBatch = '' />
		<cfset var InsertBatchResult = '' />
		<cfset var RetVarGetUserOrganization = ''/>
		<cfset var RetVarSetCustomStandardReply = ''/>
		<cfset var AddSubscriberBatchRecord = ''/>
		<cfset var AddSubscriberBatchRecordResult = ''/>
		<cfset var RetCPPXData = '' />
		<cfset var RetAddMLP = '' />
		<cfset var getShortCodeId = ''/>
		<cfset var InsertKeyWord=''/>

		<cftry>

			<!--- Read template data --->
			<cfquery name="templateXMLDetailQuery" datasource="#Session.DBSourceEBM#">
		    	SELECT
			    	Name_vch,
                	XMLControlString_vch,
                	XMLControlStringBlast_vch,
                	Type_ti
		        FROM
                	simpleobjects.templatecampaign
		        WHERE
                	TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTemplateId#">
		    </cfquery>

		    <cfif templateXMLDetailQuery.RecordCount EQ 0>
		    	<cfset dataout.MESSAGE = 'This template is no longer available or has been deleted.'>
		    	<cfreturn dataout>
		    </cfif>

			<cfset dataout.TEMPLATETYPE = templateXMLDetailQuery.Type_ti />

<!--- 			<cfif LEN(TRIM(arguments.inpDesc)) EQ 0>
				<cfset arguments.inpDesc = ReplaceNoCase(templateXMLDetailQuery.Name_vch, '<br/>', ' ', 'all') & ' ' & LSDateFormat(now(), 'yyyy-mm-dd') & ' ' & LSTimeFormat(now(), 'HH:mm:ss')>
			</cfif>

			<cfset dataout.DESC = arguments.inpDesc /> --->

			<!--- Read Profile data and put into form --->
            <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

            <!---Create a data set to run against dynamic data --->
            <cfset inpFormData = StructNew() />

            <cfset inpFormData.inpBrand = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH) />
            <cfset inpFormData.inpBrandFull = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH) />
            <cfset inpFormData.inpBrandWebURL = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH) />
            <cfset inpFormData.inpBrandWebTinyURL = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH)/>
            <cfset inpFormData.inpBrandPhone = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH) />
            <cfset inpFormData.inpBrandeMail = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH) />

            <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
            <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
            <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
            <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
            <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
            <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>

            <cfset inpFormData.CompanyNameShort = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH) />
            <cfset inpFormData.CompanyName = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH) />
            <cfset inpFormData.CompanyWebSite = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH) />
            <cfset inpFormData.CompanyWebSiteShort = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH)/>
            <cfset inpFormData.CompanyPhoneNumber = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH) />
            <cfset inpFormData.CompanyEMailAddress = strippedXMLSpecialCharacter(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH) />

<!---
            <cfif inpFormData.CompanyNameShort EQ ""><cfset inpFormData.CompanyNameShort = "CompanyNameShort"></cfif>
            <cfif inpFormData.CompanyName EQ ""><cfset inpFormData.CompanyName = "CompanyName"></cfif>
            <cfif inpFormData.CompanyWebSite EQ ""><cfset inpFormData.CompanyWebSite = "CompanyWebSite"></cfif>
            <cfif inpFormData.CompanyWebSiteShort EQ ""><cfset inpFormData.CompanyWebSiteShort = "CompanyWebSiteShort"></cfif>
            <cfif inpFormData.CompanyPhoneNumber EQ ""><cfset inpFormData.CompanyPhoneNumber = "CompanyPhoneNumber"></cfif>
            <cfif inpFormData.CompanyEMailAddress EQ ""><cfset inpFormData.CompanyEMailAddress = "CompanyEMailAddress"></cfif>
--->

            <!--- Do the transforms here --->

            <cfif inpTemplateType EQ 0 OR inpTemplateType EQ 9 >
            	<cfset XMLControlString = templateXMLDetailQuery.XMLControlString_vch>
            <cfelse>
            	<cfset XMLControlString = templateXMLDetailQuery.XMLControlStringBlast_vch>

            </cfif>

            <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
                <cfinvokeargument name="inpResponse_vch" value="#XMLControlString#">
                <cfinvokeargument name="inpContactString" value="">
                <cfinvokeargument name="inpRequesterId_int" value="">
                <cfinvokeargument name="inpFormData" value="#inpFormData#">
                <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
                <cfinvokeargument name="inpShortCode" value="">
                <cfinvokeargument name="inpBatchId" value="">
                <cfinvokeargument name="inpBrandingOnly" value="1">
            </cfinvoke>

            <!--- Update the current template data --->
            <cfset XMLControlString = RetVarDoDynamicTransformations.RESPONSE />

            <cfquery datasource="#session.DBSourceREAD#" name="getShortCodeId">
            	SELECT
            		ShortCodeId_int
            	FROM
            		sms.shortcode
            	WHERE
            		ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
            	LIMIT 1
            </cfquery>

            <cfif getShortCodeId.RECORDCOUNT LT 1>
            	<cfthrow message="Shortcode not exists!"/>
            </cfif>

		 	<!--- Write basic batch to DB --->
		    <cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="InsertBatchResult">
				INSERT INTO simpleobjects.batch
				(
					UserId_int,
                    rxdsLibrary_int,
                    rxdsElement_int,
                    rxdsScript_int,
                    UserBatchNumber_int,
					Created_dt,
					 Desc_vch,
					LastUpdated_dt,
					GroupId_int,
					XMLControlString_vch,
					Active_int,
					ContactGroupId_int,
					ContactTypes_vch,
					TemplateType_ti,
					RealTimeFlag_int,
					TemplateId_int,
					ShortCodeId_int
				)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTemplateType#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTemplateId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getShortCodeId.ShortCodeId_int#"/>
				)
			</cfquery>


			<!--- <cfdump var="#XMLControlString#" abort="true" /> --->

			<cfquery name="AddSubscriberBatchRecord" datasource="#Session.DBSourceEBM#" result="AddSubscriberBatchRecordResult">
                INSERT INTO simplelists.subscriberbatch
                    (
                        GroupId_bi,
                        BatchId_bi,
                        UserId_int,
                        Created_dt
                        )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#InsertBatchResult.generated_key#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        NOW()
                        )
            </cfquery>

			<cfset dataout.BATCHID = #InsertBatchResult.generated_key#>

			<cfset dataout.CustomHelpMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch>
			<cfset dataout.CustomStopMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch>
			<cfset dataout.XMLControlString = XMLControlString>

			<!--- Optionally add subscriber list and link it to this XMLControlString in the OPTIN TYPEs --->
			<cfif inpAddList GT 0>
            	<!--- Add any lists as needed --->
                <cfinvoke method="CreateAndLinkSubscriberList">
					<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				</cfinvoke>
            </cfif>

			<!--- Write generic keyword HELP STOP   --->
			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="HELP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>

			<cfinvoke method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#dataout.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="STOP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
			</cfinvoke>

			<cfset dataout.DebugStr = DebugStr>

		<cfcatch type="any">

			<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>

		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="UpdateTellMgrAddress" access="private" output="true" hint="Update tell the manager campaign email and phone number list">
		<cfargument name="inpBatchId" required="true" hint="Batch id to update"/>
		<cfargument name="inpListOfEmails" required="true" default="" hint="List of email to send notification"/>
		<cfargument name="inpListOfPhones" required="true" default="" hint="List of phone number to send notification"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.RQID = 0 />

		<cfset var GetBatchQuestion = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var SelectedCP = '' />
		<cfset var apiData = '' />
		<cfset var RetValUpdateCP = '' />
		<cfset var CP = '' />

		<cftry>
			<cfquery name="GetBatchQuestion" datasource="#session.DBSourceREAD#">
				SELECT
					XMLControlString_vch
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.Userid#"/>
			</cfquery>

			<cfif GetBatchQuestion.RECORDCOUNT LT 1>
				<cfthrow message="Batch ID not found!"/>
			</cfif>

			<!--- Parse for data --->
			<cftry>
				<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
				<cfcatch TYPE="any">
					<!--- Squash bad data  --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
				</cfcatch>
			</cftry>

            <!--- Read the physical ID (this does not change on edits even if RQ is moved!) --->
			<cfset SelectedCP = XmlSearch(myxmldocResultDoc, "//Q[ @TYPE = 'API']") />

			<cfset apiData = DeSerializeJSON(SelectedCP[1].XmlAttributes.API_DATA)/>

			<cfset apiData.inpListOfEmails = trim(arguments.inpListOfEmails)/>
			<cfset apiData.inpListOfPhones = trim(arguments.inpListOfPhones)/>

			<cfset SelectedCP[1].XmlAttributes.API_DATA = SerializeJSON(apiData)/>

			<cfset CP = DeSerializeJSON(SerializeJSON(SelectedCP[1].XmlAttributes))/>
			<cfset CP.OPTIONS = []/>
			<cfset CP.CONDITIONS = []/>
			<cfset CP.ESOPTIONS = []/>
			<cfset CP.ANSTEMP = ''/>
			<cfset CP.T64=0>
			<cfset CP.OIG=0>
			<cfset CP.SCDF=''/>

			<cfinvoke method="CP_Save" returnvariable="RetValUpdateCP">
				<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
				<cfinvokeargument name="controlPoint" value="#SerializeJSON(CP)#"/>
				<cfinvokeargument name="inpTemplateFlag" value="0"/>
				<cfinvokeargument name="inpSimpleViewFlag" value="0"/>
			</cfinvoke>

			<cfset dataout.RQID = CP.RQ />

			<cfset dataout.RXRESULTCODE = RetValUpdateCP.RXRESULTCODE/>
			<cfset dataout.MESSAGE = RetValUpdateCP.MESSAGE/>
			<cfset dataout.ERRMESSAGE = RetValUpdateCP.ERRMESSAGE/>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="CP_Save_Simple" access="remote" output="false" hint="Save a CP to the XML by BatchID">
		<cfargument name="inpBatchId" TYPE="string" required="true" hint="The Batch Id to change"/>
		<cfargument name="controlPoints" type="any" required="true" hint="The JSON structure of the Control Point"/>
		<cfargument name="inpGroupId" type="string" required="true" hint="The subscriber user will opt in to"/>
		<cfargument name="inpBatchDesc" type="string" required="true" hint="Campaign name"/>
		<cfargument name="inpKeyword" type="string" required="false" default="" hint="Campaign keyword"/>
		<cfargument name="inpOrganizationName" type="string" required="false" default="" hint="Organization name"/>
		<cfargument name="inpShortCode" type="string" required="false" default="" hint="User shortcode"/>
		<cfargument name="inpScheduleTime" type="string" required="false" default="" hint="Schedued time for blast"/>
		<cfargument name="inpCampaignType" type="numeric" required="false" default="1"/>
		<cfargument name="intListDeleteRID" type="string" required="false" default=""/>
		<cfargument name="inpIsTellTheManagerTemplate" type="numeric" required="false" default="0"/>
		<cfargument name="inpListOfEmails" type="string" required="false" default=""/>
		<cfargument name="inpListOfPhones" type="string" required="false" default=""/>
		<cfargument name="inpSchdeduleOption" type="string" required="false" default="0" hint="0 is simple schedule, 1 is advance schedule"/>
		<cfargument name="inpIsFinishedCampaign" type="string" required="false" default="1" hint="0 is draft campaign, 1 is fully finish campaign"/>

		<!--- <cfargument name="STARTNUMBERQ" TYPE="numeric" required="false" default="1" hint="The current page for supporting only showing a sub group of Control Point (CP) in the flow - 1 based - default to 1 if no paging used">
	    <cfargument name="inpSimpleViewFlag" TYPE="any" required="no" default="1" hint="Flag to use simplified view - Optional - default is 1=on" />
	    <cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0" hint="Flag to use Template - Optional - If inpTemplateFlag=1 then inpBatchId is used as Template Id" /> --->

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BATCHID = "" />
		<cfset dataout.PID = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />
		<cfset dataout.BLASTRESULT = "" />
		<cfset dataout.BLASTNAME = arguments.inpBatchDesc />
		<cfset dataout.LISTIDNEW  = "" />
		<cfset dataout.BLACKLISTWORD = "" />

		<cfset var CheckBlackList	= '' />
		<cfset var CurrPID = 0 />
		<cfset var DebugStr	= '' />
		<cfset var myxmldocResultDoc	= '' />
		<cfset var SelectedCP	= '' />
		<cfset var AttributeBuff	= '' />
		<cfset var Counter	= '' />
		<cfset var NewRXSSCSC	= '' />
		<cfset var Qs	= '' />
		<cfset var OutToDBXMLBuff	= '' />
		<cfset var FinalDoc	= '' />
		<cfset var subcriberArray	= '' />
		<cfset var _options	= '' />
		<cfset var _conditions	= '' />
		<cfset var QItem	= '' />
		<cfset var GetBatchQuestion	= '' />
		<cfset var WriteBatchOptions	= '' />
		<cfset var RetVarRenderCP	= '' />
		<cfset var POSITION = 0 />
		<cfset var inpXML	= '' />
		<cfset var OPTIONSXML = '' />
		<cfset var OPTIONITEM = '' />
		<cfset var CONDITIONITEM	= '' />
		<cfset var CONDITIONSXML	= '' />
		<cfset var APIXML = ''/>
		<cfset var CDFXML = '' />
		<cfset var OPTINXML = '' />
		<cfset var INTERVALXML = '' />
		<cfset var AddSubscriberBatchRecord = '' />
		<cfset var AddSubscriberBatchRecordResult = '' />
		<cfset var parsedItem = '' />
		<cfset var UpdateBatch = '' />
		<cfset var RetValUpdateBatchDesc = '' />
		<cfset var RetVarGetUserOrganization = '' />
		<cfset var RetValUpdateCompanyName = '' />
		<cfset var RetValUpdateKeyword = '' />
		<cfset var RetValUpdateSchedule = '' />
		<cfset var RetValSendBlast = '' />

		<cfset var RID = '' />
		<cfset var cp = '' />
		<cfset var GetUserGroup = '' />
		<cfset var RetValDeleteCP = '' />
		<cfset var RetValUpdateCP = '' />
		<cfset var i = '' />
		<cfset var GetControlPoints = '' />
		<cfset var RetVarUpdateTellMgr = '' />
		<cfset var UpdateMoreForBatch = ''/>
		<cfset var BlackWords_str = 'One or more words in your ad copy are in violation of CITA SHAFT policies.  You will not be able to save or send a message containing these keywords:'>
		<cfset var checkblackwords = 0/>

		<cfset var UniSearchMatcher = '' />
	    <cfset var UniSearchPattern = '' />

		<cfset var ChatWithCustomerAPIRQ=0>
		<cfset var blackword=''>

		<cftry>
			<cfset arguments.controlPoints = DeSerializeJSON(arguments.controlPoints) />
			<cfset arguments.intListDeleteRID = DeSerializeJSON(arguments.intListDeleteRID)/>
			<!--- Check black list words --->
			<cfloop array="#arguments.controlPoints#" index="cp">
				<cfif isDefined("cp.CP.TEXT")>


					<!--- Check if text is unicode --->
					<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		            <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

		            <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
		            <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", cp.CP.TEXT ) ) />

					<!--- Look for character higher than 255 ASCII --->

		          	<cfif UniSearchMatcher.Find() >

		            	<cfset cp.CP.TEXT = toBase64(cp.CP.TEXT) />

		                <!--- UnicodeCP Save_cp Formatting processing --->
 						<cfset cp.CP.T64 = 1 />

		            <cfelse>
		                <!--- UnicodeCP Save_cp Formatting processing --->
 						<cfset cp.CP.T64 = 0 />
		            </cfif>


					<cfquery name ="CheckBlackList" datasource="#Session.DBSourceEBM#">
						SELECT 	WordId,
								Words_vch
						FROM
								simpleobjects.black_list_words
						Where
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LCase(cp.CP.TEXT)#"> REGEXP  CONCAT('[[:<:]]', Words_vch, '[[:>:]]')
					</cfquery>

					<cfif CheckBlackList.RECORDCOUNT GT 0>
						<cfset checkblackwords = 1/>
						<cfloop query = "CheckBlackList">
							<!--- <cfset BlackWords_str = Replace(BlackWords_str, ",@@@,"&CheckBlackList.Words_vch,"","ALL")>
							<cfset BlackWords_str = BlackWords_str &",@@@,"& CheckBlackList.Words_vch > --->
							<cfif ListFindNoCase(dataout.BLACKLISTWORD, TRIM(Words_vch) ) EQ 0>
								<cfset dataout.BLACKLISTWORD = listAppend(dataout.BLACKLISTWORD, Words_vch) />
							</cfif>
						</cfloop>
					</cfif>

				</cfif>
			</cfloop>


			<cfif checkblackwords eq 1>
				<!--- <cfset BlackWords_str = Replace(BlackWords_str, ",@@@,","<br>&nbsp;&nbsp;*&nbsp;","ALL")> --->
				<cfloop index="blackword" list="#dataout.BLACKLISTWORD#">
					<cfset BlackWords_str = BlackWords_str & "<br/>&nbsp;&nbsp;*&nbsp;" & blackword>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = BlackWords_str/>
				<cfset dataout.ERRMESSAGE = "BlackWords"/>
				<cfreturn dataout>
			</cfif>

	        <cfif arguments.inpBatchDesc EQ "">
	        	<cfthrow type="Batch" message="Campaign name not valid"/>
	        </cfif>

	        <!--- Validate inpGroupId is list of groups --->
			<cfif !REFind("^\d+(,\d+)*$", arguments.inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is valid"/>
				<cfreturn dataout>
			</cfif>

	        <!--- <cfif arguments.inpGroupId LT 0>
	        	<cfthrow type="Subscriber list" message="Subscriber list id not valid"/>
	        </cfif> --->

	        <cfif arguments.inpScheduleTime NEQ "">
	        	<cfset DebugStr = DebugStr & " Update Schedule."/>

	        	<cfinvoke component="session.sire.models.cfc.schedule" method="UpdateSchedule" returnvariable="RetValUpdateSchedule">
	        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
	        		<cfinvokeargument name="SCHEDULETIME" value="#arguments.inpScheduleTime#"/>
	        	</cfinvoke>

	        	<cfif RetValUpdateSchedule.RXRESULTCODE LT 1>
	        		<cfthrow type="Schedule" message="#RetValUpdateSchedule.MESSAGE#"/>
	        	</cfif>
	        </cfif>

	        <cfif arguments.inpShortCode NEQ "">
		        <cfif arguments.inpKeyword EQ "">
		        	<cfset DebugStr = DebugStr & " Clear Keyword."/>

		        	<cfinvoke method="ClearKeyword" returnvariable="RetValUpdateKeyword">
		        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		        		<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
		        	</cfinvoke>
		        <cfelse>
		        	<cfset DebugStr = DebugStr & " Update Keyword."/>

		        	<cfinvoke method="SaveKeyword" returnvariable="RetValUpdateKeyword">
		        		<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		        		<cfinvokeargument name="inpKeyword" value="#arguments.inpKeyword#"/>
		        		<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
		        	</cfinvoke>
		        </cfif>

		        <cfif RetValUpdateKeyword.RXRESULTCODE LT 1>
		        	<cfthrow type="Keyword" message="#RetValUpdateKeyword.ERRMESSAGE#" detail="#RetValUpdateKeyword.ERRMESSAGE#"/>
		        </cfif>
	        </cfif>

	        <cfif arguments.inpGroupId GT 0>
	        	<cfset DebugStr = DebugStr & " Check subscriber."/>

	        	<cfquery name="GetUserGroup" datasource="#session.DBSourceREAD#">
	        		SELECT
	        			GroupId_bi
	        		FROM
	        			simplelists.grouplist
	        		WHERE
	        			GroupId_bi in (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpGroupId#"/>)
	        		AND
	        			UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	        	</cfquery>

		        <cfif GetUserGroup.RECORDCOUNT LT 1>
		        	<cfthrow type="Application" message="Subscriber list id not valid"/>
		        </cfif>
	        </cfif>

            <!--- Secured to Session.User by query --->

            <cfset DebugStr = DebugStr & " Get batch."/>

			<!--- Read from DB Batch Options --->
            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
                SELECT
                	BatchId_bi,
                    XMLControlString_vch,
                    Desc_vch
                FROM
                    simpleobjects.batch
                WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
                AND
                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            </cfquery>

			<cfif GetBatchQuestion.RECORDCOUNT NEQ 1>
				<cfthrow type="Batch" message="Campaign id not valid"/>
			</cfif>

			<cfset DebugStr = DebugStr & " Update batch desc."/>

			<cfif GetBatchQuestion.Desc_vch NEQ arguments.inpBatchDesc>
				<cfinvoke method="UpdateBatchDesc" returnvariable="RetValUpdateBatchDesc">
					<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
					<cfinvokeargument name="inpDesc" value="#arguments.inpBatchDesc#"/>
					<cfinvokeargument name="inpTemplateFlag" value="0"/>
				</cfinvoke>
				<cfif RetValUpdateBatchDesc.RXRESULTCODE LT 1>
					<cfthrow type="Batch" message="Can not save campaign name! Please check your input" detail="#RetValUpdateBatchDesc.ERRMESSAGE#"/>
				</cfif>
			</cfif>

			<cfif arguments.inpOrganizationName NEQ "">
				<cfset DebugStr = DebugStr & " Update organization."/>

				<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>
				<cfif RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH NEQ arguments.inpOrganizationName>
					<cfinvoke method="UpdateCompanyName" returnvariable="RetValUpdateCompanyName">
						<cfinvokeargument name="OrganizationId_int" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONID_INT#"/>
						<cfinvokeargument name="OrganizationName_vch" value="#arguments.inpOrganizationName#"/>
					</cfinvoke>

					<cfif RetValUpdateCompanyName.RXRESULTCODE LT 1>
						<cfthrow type="Company" message="Can not save company name! Please check your input" detail="#RetValUpdateCompanyName.ERRMESSAGE#"/>
					</cfif>
				</cfif>
			</cfif>

			<cfif arguments.inpIsTellTheManagerTemplate GT 0>
				<cfinvoke method="UpdateTellMgrAddress" returnvariable="RetVarUpdateTellMgr">
					<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
					<cfinvokeargument name="inpListOfEmails" value="#arguments.inpListOfEmails#"/>
					<cfinvokeargument name="inpListOfPhones" value="#arguments.inpListOfPhones#"/>
				</cfinvoke>
				<cfset ChatWithCustomerAPIRQ=RetVarUpdateTellMgr.RQID>
				<cfset DebugStr &= " Update Tell Manager Results = " & RetVarUpdateTellMgr.RXRESULTCODE & ". MESSAGE=#RetVarUpdateTellMgr.MESSAGE# & MESSAGE=#RetVarUpdateTellMgr.ERRMESSAGE# & "/>
			</cfif>

			<cfset DebugStr = DebugStr & " Delete CP."/>

			<cfloop array="#arguments.intListDeleteRID#" index="RID">
				<cfinvoke method="DeleteControlPoint" returnvariable="RetValDeleteCP">
					<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
					<cfinvokeargument name="INPPID" value="#RID#"/>
				</cfinvoke>
			</cfloop>

			<cfset DebugStr = DebugStr & " Add CP."/>

			<cfloop array="#arguments.controlPoints#" index="cp">
				<cfif cp.ACTION EQ "New">
					<cfinvoke method="Add_ControlPoint" returnvariable="RetValUpdateCP">
						<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
						<cfinvokeargument name="controlPoint" value="#SerializeJSON(cp.CP)#"/>
						<cfinvokeargument name="inpTemplateFlag" value="0"/>
						<cfinvokeargument name="inpSimpleViewFlag" value="1"/>
					</cfinvoke>

					<cfset dataout.LISTIDNEW  = listAppend(dataout.LISTIDNEW, RetValUpdateCP.PID) />
				</cfif>
			</cfloop>

			<cfset DebugStr = DebugStr & " Edit CP."/>

			<cfloop array="#arguments.controlPoints#" index="cp">

				<cfif cp.ACTION EQ "Edit" AND cp.CP.RQ NEQ ChatWithCustomerAPIRQ>

					<cfinvoke method="CP_Save" returnvariable="RetValUpdateCP">
						<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
						<cfinvokeargument name="controlPoint" value="#SerializeJSON(cp.CP)#"/>
						<cfinvokeargument name="inpTemplateFlag" value="0"/>
						<cfinvokeargument name="inpSimpleViewFlag" value="0"/>
					</cfinvoke>

					<cfset dataout.CP_Save = SerializeJSON(RetValUpdateCP) />

				</cfif>

			</cfloop>

            <cfif arguments.inpGroupId GT 0 AND arguments.inpCampaignType EQ 0>
            	<cfset DebugStr = DebugStr & " Update CP Group."/>

	            <!--- Build API Options if any --->
				<cfquery name="AddSubscriberBatchRecord" datasource="#Session.DBSourceEBM#" result="AddSubscriberBatchRecordResult">
	                UPDATE
	                	simplelists.subscriberbatch
	                SET
	                	GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
	                WHERE
	                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

	            <cfquery name="GetBatchQuestion" datasource="#Session.DBSourceEBM#">
	                SELECT
	                	BatchId_bi,
	                    XMLControlString_vch,
	                    Desc_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	            </cfquery>

				<cfset NewRXSSCSC = "" />
				<cfset NewRXSSCSC &= "<RXSSCSC>#Chr(13)##Chr(10)#" />

				<!--- Parse for data --->
				<cftry>
					<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #GetBatchQuestion.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>

				<cfset Qs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC/Q") />

				<cfloop array="#Qs#" index="QItem">
					<cfif QItem.XmlAttributes.TYPE EQ "OPTIN">
						<cfset QItem.XmlAttributes.OIG = "#arguments.inpGroupId#"/>
					</cfif>

					<!--- <cfloop array="#arguments.controlPoint#" index="CP">
						<cfif QItem.XmlAttributes.RQ EQ CP.RQ AND (QItem.XmlAttributes.TYPE EQ 'SHORTANSWER' OR QItem.XmlAttributes.TYPE EQ 'OPTIN' OR QItem.XmlAttributes.TYPE EQ 'STATEMENT' )>
							<cfset QItem.XmlAttributes.TEXT = CP.TEXT/>
						</cfif>
					</cfloop> --->

					<cfset OutToDBXMLBuff = ToString(QItem, "UTF-8") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset NewRXSSCSC &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/>
				</cfloop>

				<cfset NewRXSSCSC &= "</RXSSCSC>"/>

				<!--- Parse new XML --->
				<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #NewRXSSCSC# & "</XMLControlStringDoc>") />

				 <!--- make sure to update all RQ POSITIONs --->
			    <cfset FinalDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

	        	<!--- Clean up XML for DB storage --->
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />


				<!--- Write to DB Batch Options --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.batch
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				</cfquery>
            </cfif>
			<!--- update shedule option--->
			<cfquery name="UpdateMoreForBatch" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.batch
				SET
					ScheduleOption_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSchdeduleOption#">,
					IsFinishedCampaign_ti= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIsFinishedCampaign#">

				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpBatchId#">
			</cfquery>


			<cfif arguments.inpCampaignType EQ 1 AND arguments.inpGroupId GT 0>
				<cfset DebugStr = DebugStr & " Blast."/>

				<cfinvoke method="BlastToList" returnvariable="RetValSendBlast">
					<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
					<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#"/>
					<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
					<cfinvokeargument name="inpScheduledTimeData" value="#arguments.inpScheduleTime#"/>
				</cfinvoke>

				<cfif RetValSendBlast.RXRESULTCODE EQ 1>
					<cfset dataout.RXRESULTCODE = 1 />
				<cfelse>
					<cfset dataout.MESSAGE = RetValSendBlast.MESSAGE>
				</cfif>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

			<!---  <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
                <cfinvokeargument name="InpEmailString" value="#Session.EmailAddress#">
                <cfinvokeargument name="InpLastActiveDate" value="#dateFormat(now(), 'mm/dd/yyyy')#">
            </cfinvoke> --->

			<cfset dataout.BATCHID = INPBATCHID />
			<cfset dataout.PID = CurrPID />
			<cfset dataout.ERRMESSAGE = DebugStr />
			<cfset dataout.BLASTRESULT = RetValSendBlast/>
			<cfset dataout.ERRMESSAGE = "DebugStr=#DebugStr#" >

            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#" >

			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<!--- Make this "public" access: available to a locally executing page or component method only - no AJAX. --->
	<cffunction name="ClearKeywordById" access="remote" output="true" hint="Remove all keywords from  keyword id list">
		<cfargument name="inpKeywordId" TYPE="array" hint="" />
		<cfargument name="inpShortCode" TYPE="string" hint="" />
		<cfargument name="inpDisable" TYPE="numeric" hint="" default="0" />
		<cfargument name="inpEndDate" TYPE="any" hint="" default="0" />

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var UpdateKeywordQuery = '' />
		<cfset var RetVarGetKeywordOwner	= '' />
		<cfset var RetVarGetShortAccessData	= '' />
		<cfset var RetVarGetKeywordId	= '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Validate current session owner owns Batch Id --->
   			<cfinvoke method="GetKeywordOwner" returnvariable="RetVarGetKeywordOwner">
				<cfinvokeargument name="inpKeywordId" value="#inpKeywordId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetKeywordOwner.USERID NEQ Session.USERID>
	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this keywords"/>
	   			<cfreturn dataout>
   			</cfif>

   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>
	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>
   			</cfif>

			<cfset dataout.KEYWORDID = "#arguments.inpKeywordId#" />

			<!--- Clear keyword from db --->
            <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">
                UPDATE
                	sms.keyword
                SET
                    <!--- Active_int = 2, --->
                    UserDowngradeDate_dt = <cfqueryparam cfsqltype="cf_sql_date" value="#inpEndDate#"/>
                WHERE
                    KeywordId_int IN (#arrayToList(arguments.inpKeywordId)#)
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="ActiveKeywordById" access="public" output="true" hint="Remove all keywords from  keyword id list">
		<cfargument name="inpKeywordId" TYPE="array" hint="" />
		<cfargument name="inpShortCode" TYPE="string" hint="" />

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var UpdateKeywordQuery = '' />
		<cfset var RetVarGetKeywordOwner	= '' />
		<cfset var RetVarGetShortAccessData	= '' />
		<cfset var RetVarGetKeywordId	= '' />

		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Validate current session owner owns Batch Id --->
   			<cfinvoke method="GetKeywordOwner" returnvariable="RetVarGetKeywordOwner">
				<cfinvokeargument name="inpKeywordId" value="#inpKeywordId#">
			</cfinvoke>

            <!--- Return error message if user Ids do not match --->
   			<cfif RetVarGetKeywordOwner.USERID NEQ Session.USERID>
	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this keywords"/>
	   			<cfreturn dataout>
   			</cfif>

   			<!--- Get active Short Code Request Id for current user - makes sure current user is allowed to use this Short Code --->
   			<cfinvoke method="GetShortAccessData" returnvariable="RetVarGetShortAccessData">
	   			<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
	   		</cfinvoke>

   			<cfif RetVarGetShortAccessData.SHORCODEREQUESTID EQ 0>
	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Security Check Failure - Your account does not currently have access to the CSC (#inpShortCode#) "/>

	   			<cfreturn dataout>
   			</cfif>

			<cfset dataout.KEYWORDID = "#arguments.inpKeywordId#" />

			<!--- Clear keyword from db --->
            <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">
                UPDATE
                	sms.keyword
                SET
                    Active_int = 1
                WHERE
                    KeywordId_int IN (#arrayToList(arguments.inpKeywordId)#)
                AND
                	Active_int = 2
            </cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetKeywordOwner" access="public" output="true" hint="Get the current keyword owners's User Id">
    	<cfargument name="inpKeywordId" TYPE="array" hint="The KeywordId_int to read the XMLControlString from" />

        <cfset var dataout = {} />
        <cfset var GetKeywordOwnerQuery= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.USERID = "-1"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>
			<cfquery name="GetKeywordOwnerQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    sb.UserId_int
                FROM
                    simpleobjects.batch sb
                JOIN
                	sms.keyword sk
                ON
                	sb.Batchid_bi = sk.Batchid_bi
                WHERE
                    sk.KeywordId_int IN (#arrayToList(arguments.inpKeywordId)#)
                GROUP BY
                	sb.UserId_int
            </cfquery>

	  		<cfif GetKeywordOwnerQuery.RecordCount EQ 1>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERID = "#GetKeywordOwnerQuery.UserId_int#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="GetListOwner" access="public" output="false" hint="Get the current group owners's User Id">
    	<cfargument name="inpGroupId" TYPE="string" hint="The Group Id to check owner of " />

        <cfset var dataout = {} />
        <cfset var GetListOwnerQuery= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.USERID = "-1"/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

			<cfquery name="GetListOwnerQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserId_int
                FROM
                    simplelists.grouplist
                WHERE
                    GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpGroupId#">
            </cfquery>

	  		<cfif GetListOwnerQuery.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERID = "#GetListOwnerQuery.UserId_int#"/>
	  		</cfif>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="BlastToList" access="remote" output="false" hint="Blast a Batch to a List of Subscribers">
		<cfargument name="inpBatchId" TYPE="string" required="true" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpGroupId" TYPE="string" required="true" hint="The List to send to or Optionally a CSV list of List Ids" />
		<cfargument name="inpShortCode" TYPE="string" required="false" hint="Shortcode to send this from. User must have access to specified short code or default will be used intsead." default="" />
		<cfargument name="inpContactTypeId" TYPE="string" default="3" required="false" hint="1=Voice, 2=eMAil, 3=SMS, 4=OTP DEvice" />
		<cfargument name="inpMinBR" required="false" default="60" hint="Business rule - block new campaign blasts if duplicate in last (inpMinBR) minutes. Default is 60 minutes. Shit happens. When you automate systems, shit happens automatically."/>
		<cfargument name="inpClone" required="false" default="0" hint="Default will just blast to specified BatchId. A value of 1 will create a new Batch (Clone) for the blast - Clone based on on passed in inpBatchId. This allows seperate tracking and multiple reuse of given campaigns. Subject to inpMinBR limitations">
		<cfargument name="inpSkipDNCCheck" TYPE="string" default="0" required="false" hint="1 = Count those on list and on DNC - 0 = count those on list AND not on DNC" />
		<cfargument name="inpScheduledTimeData" required="false" default="" hint="Legacy field - will be removed"/>
		<cfargument name="inpSchedule" TYPE="string" default="" hint="A JSON string of schedule options you wish to override. Each variable name must match expected list of possibilities. See docs for further info." />
		<cfargument name="inpCDFFilter" TYPE="string" default="" />
		<cfargument name="inpContactFilter" TYPE="string" default="" />
		<cfargument name="inpAPIRequestJSON" required="false" hint="Custom fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default=""/>
		<cfargument name="inpDesc" required="false" default="" hint="Name of the blast - cloned campaing option only">
		<cfargument name="inpTimeZone" type="string" required="false" default="0" hint="Default calendar time zone setting is none - local to device = 0"/>
		<cfargument name="inpDistributionProcessId" type="string" required="false" default="0" hint="Use this Id to allow multiple CRON/Thread processes to run against same tables"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.SOURCEBATCHID = "" />
		<cfset dataout.TARGETBATCHID = "" />
		<cfset dataout.LISTCOUNT = "0" />
		<cfset dataout.QUEUEDCOUNT = 0 />
		<cfset dataout.BALANCE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var TotalListCount = '' />
		<cfset var TotalListCountDuplicates = '' />
		<cfset var BillingBalance = '' />
		<cfset var inpSourceBatchId = '' />
		<cfset var LOOPLIMIT_INT = '' />
		<cfset var inpFormData = '' />
		<cfset var isCDFFilter = false />
		<cfset var cdfArray = [] />
		<cfset var cdfItem = '' />
		<cfset var cdfRowItem = '' />
		<cfset var cdfLoopIndex = 0 />
		<cfset var GetGroupListOwnerQuery = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarVerifyBlastNotDuplicateBR = '' />
		<cfset var RetVarGetAssignedShortCode = '' />
		<cfset var RetVarGetBlastToListStats = '' />
		<cfset var RetVarCloneCampaign = '' />
		<cfset var RetValUpdateSchedule = '' />
		<cfset var RetVarAddBlastLog = '' />
		<cfset var RetVarUpdateBlastLogStatus = '' />
		<cfset var InsertQueue = '' />
		<cfset var DeleteQueue = '' />

		<cftry>

			<!--- Bulletproof checks --->
			<cfif TRIM(arguments.inpTimeZone) EQ "" OR !IsNumeric(arguments.inpTimeZone)>
				<cfset arguments.inpTimeZone = 0 />
			</cfif>

			<!--- Bulletproof checks --->
			<cfif TRIM(arguments.inpDistributionProcessId) EQ "" OR !IsNumeric(arguments.inpDistributionProcessId)>
				<cfset arguments.inpDistributionProcessId = 0 />
			</cfif>

			<!--- Validate inpGroupId is list of groups --->
			<cfif !REFind("^\d+(,\d+)*$", inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is not valid"/>

				<cfreturn dataout>
			</cfif>

			<!--- Bullet Proof: Validate current session owner owns Batch Id --->
			<!--- Bullet Proof: Verify valid Batch Id - not 0 not blank --->
			<cfinvoke method="GetBatchOwner" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchOwner">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
			</cfinvoke>

			<!--- Return error message if user Ids do not match --->
			<cfif RetVarGetBatchOwner.USERID NEQ Session.USERID>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this Batch Id (#inpBatchId#)"/>

				<cfreturn dataout>
			</cfif>

			<!--- Bullet Proof: Validate current list owner owns Group Id --->
			<!--- Bullet Proof: Verify valid List Id - not 0 not blank --->
			<cfquery name="GetGroupListOwnerQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS Total
				FROM
					simplelists.grouplist
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#Session.USERID#">
				AND
					GroupId_bi IN (#inpGroupId#)
			</cfquery>

			<cfif GetGroupListOwnerQuery.Total EQ 0 OR GetGroupListOwnerQuery.Total NEQ listLen(inpGroupId)>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Security Check Failure. The currently logged in account (#Session.USERID#) does not have access to this List Id in (#inpGroupId#)"/>

				<cfreturn dataout>
			</cfif>


			<!--- Bullet Proof: Verify no duplicate blast requests in time range --->
			<cfif inpMinBR GT 0>

				<!--- Look for SourceBatchId_bi too close to inpMinBR --->
				<cfinvoke method="VerifyBlastNotDuplicateBR" returnvariable="RetVarVerifyBlastNotDuplicateBR">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
					<cfinvokeargument name="inpGroupId" value="#inpGroupId#"/>
					<cfinvokeargument name="inpMinBR" value="#inpMinBR#"/>
				</cfinvoke>

				<cfif RetVarVerifyBlastNotDuplicateBR.COUNT GT 0>

					<!--- SH Violation - Shit happens. When you automate systems, shit happens automatically.  --->
					<cfset dataout.RXRESULTCODE = -3 />
					<cfset dataout.TYPE = "SH Violation" />
					<cfset dataout.MESSAGE = "This Batch has been sent to this Subscriber List in the last #inpMinBR# minutes. " />
					<cfset dataout.ERRMESSAGE = "inpBatchId (#inpBatchId#) has been sent to Subscriber List Id (#inpGroupId#) in the last inpMinBR (#inpMinBR#) minutes. " />

					<cfreturn dataout>

				</cfif>

			</cfif>

			<!--- Bullet Proof: Validate and verify target short code is still available to user --->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetAssignedShortCode">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
			</cfinvoke>

			<!--- Need to lookup stats for each list --->
			<!--- Look up numbers to be used for further validation --->
			<cfinvoke method="GetBlastToListStats" returnvariable="RetVarGetBlastToListStats">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#"/>
				<cfinvokeargument name="inpShortCode" value="#RetVarGetAssignedShortCode.SHORTCODE#"/>
				<cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
				<cfinvokeargument name="inpSkipDNCCheck" value="#inpSkipDNCCheck#"/>
				<cfinvokeargument name="inpContactFilter" value="#inpContactFilter#"/>
			</cfinvoke>

			<!--- Add counts to global totals --->
			<cfset TotalListCount = LSParseNumber(RetVarGetBlastToListStats.LISTCOUNT)/>

			<!--- Duplicate counts are only within each list - not the total duplicates of a merge purge --->
			<cfset TotalListCountDuplicates = LSParseNumber(RetVarGetBlastToListStats.LISTCOUNTDUPLICATE)/>

			<!--- There only is one balance - not an aggregate --->
			<cfset BillingBalance = LSParseNumber(RetVarGetBlastToListStats.BALANCE) />

			<!--- Blast using inpBatchId specified or create new blast BatchId based on clone --->
			<cfif inpClone GT 0>

				<!--- Clone inpBatchId  - call clone campaign method --->
				<cfinvoke method="CloneCampaign" returnvariable="RetVarCloneCampaign">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
					<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
					<cfinvokeargument name="inpDesc" value="#inpDesc#">
				</cfinvoke>

				<cfset inpSourceBatchId = inpBatchId/>
				<cfset arguments.inpBatchId = RetVarCloneCampaign.NEXTCAMPAINID />

				<!--- For cloned campaign ignore in contact queue duplicates counts --->
				<cfset TotalListCountDuplicates = 0 />

			<cfelse>

				<cfset inpSourceBatchId = inpBatchId />

			</cfif>

			<cfset dataout.SOURCEBATCHID = "#inpSourceBatchId#" />
			<cfset dataout.TARGETBATCHID = "#arguments.inpBatchId#" />
			<cfset dataout.LISTCOUNT = "#TotalListCount#" />
			<cfset dataout.BALANCE = "#BillingBalance#" />

			<!--- Return error message if user does not have enough credits --->
			<cfif ( TotalListCount - TotalListCountDuplicates ) GT BillingBalance>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Blast verification Failure. The list count (#TotalListCount#) is greater than currently avialable credits (#BillingBalance#)."/>
				<cfreturn dataout>
			</cfif>

			<!--- Return error message if user does not have enough credits --->
			<cfif RetVarGetBlastToListStats.HASACTIVEQUEUE GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "This campaign has already processed a blast with this subscriber list."/>

				<cfreturn dataout>
			</cfif>

			<!--- How many to send per loop of processing - this allows multiple users and batches to share the same distribution service and not block each other from going out --->
			<cfset LOOPLIMIT_INT = 100>

			<!--- I am only leaving this bad logic so templates dont break, but.... inpScheduledTimeData method needs removal  --->
			<cfif LEN(TRIM(inpScheduledTimeData)) GT 0>
				<cfinvoke component="session.sire.models.cfc.schedule" method="UpdateSchedule" returnvariable="RetValUpdateSchedule">
					<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
					<cfinvokeargument name="SCHEDULETIME" value="#inpScheduledTimeData#"/>
					<cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
				</cfinvoke>

			<cfelse>

				<!--- Create / Validate schedule - change end date to today plus 30 if the source batchid is already expired --->
				<cfinvoke method="BlastValidateScheduleActive" component="session.sire.models.cfc.control-point" returnvariable="RetValUpdateSchedule">
					<cfinvokeargument name="inpSourceBatchId" value="#inpSourceBatchId#"/>
					<cfinvokeargument name="inpTargetBatchId" value="#inpBatchId#"/>
					<cfinvokeargument name="inpSchedule" value="#inpSchedule#"/>
				</cfinvoke>

			</cfif>

			<!--- Return error message if schedule is not set OK --->
			<cfif RetValUpdateSchedule.RXRESULTCODE LT 1>

				<cfset dataout.RETVALUPDATESCHEDULE = SerializeJSON(RetValUpdateSchedule) />

				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Error - Schedule failed to Set OK."/>

				<cfreturn dataout>
			</cfif>

			<!--- Append FORM and URL scope with that of the inpFormDataJSON --->

			<Cfif structKeyExists(form, "controlPoints")>
				<Cfset form['controlPoints'] = toBase64(form['controlPoints'])/>
			</Cfif>

			<cfif !IsStruct(arguments.inpAPIRequestJSON) AND LEN(TRIM(inpAPIRequestJSON)) GT 0 >
				<cfset inpFormData = DeserializeJSON(arguments.inpAPIRequestJSON) />

				<cfset StructAppend(inpFormData, URL, true) />

				<!--- Need to load any dynamic URL variables associated with this request --->
				<cfset StructAppend(inpFormData, FORM, true) />

				<cfset arguments.inpAPIRequestJSON = SerializeJSON(inpFormData) />

			<cfelse>

				<!--- Need to load any dynamic URL variables associated with this request --->
				<cfset StructAppend(FORM, URL, true) />
				<cfset arguments.inpAPIRequestJSON = SerializeJSON(FORM) />

			</cfif>

			<!--- Insert blast - set ID so process wont race to shut off --->
			<!--- Just log it for now - maybe later make the loading asyncronous from UI --->

			<cfinvoke method="AddBlastLog" returnvariable="RetVarAddBlastLog">
				<cfinvokeargument name="inpStatus" value="#BLAST_LOG_LOADING#"/>
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#"/>
				<cfinvokeargument name="inpSourceBatchId" value="#inpSourceBatchId#"/>
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#"/>
				<cfinvokeargument name="inpAmountLoaded" value="#TotalListCount - TotalListCountDuplicates#"/>
				<cfinvokeargument name="inpAPIRequestJSON" value="#inpAPIRequestJSON#"/>
				<cfinvokeargument name="inpDesc" value="BLAST LOADED"/>
				<cfinvokeargument name="inpTimeZone" value="#inpTimeZone#"/>
				<cfinvokeargument name="inpDistributionProcessId" value="#inpDistributionProcessId#"/>
			</cfinvoke>

			<!--- Bullet Proof: Make sure log entry is created OK --->
			<cfif RetVarAddBlastLog.BLASTID EQ 0>
				<cfthrow type="Application" message="Unable to create Blast Log" detail="#RetVarAddBlastLog.ERRMESSAGE#"/>
			</cfif>

			<cfif inpCdffilter NEQ '' AND (isValid("String", inpCdffilter) AND isJSON(inpCdffilter))>
				<cfset cdfArray = DeSerializeJSON(inpCdffilter) />
			<cfelse>
				<cfset cdfArray = arrayNew() />
			</cfif>
			<cfset isCDFFilter = (arrayLen(cdfArray) GT 0) />

			<!--- Load Blast to queue - Asyncronous message generation done by CRON server --->
			<cfquery result="InsertQueue" datasource="#Session.DBSourceEBM#">
				INSERT INTO
					simplequeue.batch_load_queue
					(
						UserId_int,
						BatchId_bi,
						GroupId_int,
						ContactString_vch,
						Status_ti,
						ContactType_int,
						ShortCode_vch,
						BlastLogId_int,
						OperatorId_int,
						TimeZone_int,
						DistributionProcessId_int
					)
				SELECT
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"/>,
					simplelists.groupcontactlist.groupid_bi,
					simplelists.contactstring.ContactString_vch,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_READY#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGetAssignedShortCode.SHORTCODE#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarAddBlastLog.BLASTID#"/>,
					simplelists.contactstring.OperatorId_int,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTimeZone#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDistributionProcessId#"/>
				FROM
					simplelists.groupcontactlist
				INNER JOIN
					simplelists.contactstring
					ON
						simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
				INNER JOIN
					simplelists.contactlist
					ON
						simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi

				<!--- This is the deduplication part of the query https://stackoverflow.com/questions/13838415/inserting-millions-of-records-with-deduplication-sql --->
				<!--- Prevent the same number from getting blasted twice within the same blast - whether it is on the same list or a differnt list - one message per blast --->
				LEFT OUTER JOIN
					simplequeue.batch_load_queue s2
					ON
						simplelists.contactstring.ContactString_vch = s2.ContactString_vch
					AND
						s2.BlastLogId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarAddBlastLog.BLASTID#"/>

				WHERE
					s2.ContactString_vch IS NULL
				AND
					simplelists.contactlist.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					simplelists.contactstring.ContactType_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#">
				AND
					simplelists.groupcontactlist.groupid_bi IN (#inpGroupId#)

				<cfif arguments.inpContactFilter NEQ "">
					AND
						simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactFilter#%">
				</cfif>

				<!--- New Logic - Need a better way to filter on CDFs --->
				<cfif isCDFFilter>
					AND
					(
					SELECT
						COUNT(cv.ContactVariableId_bi)
					FROM
						simplelists.contactvariable cv
					WHERE
						simplelists.contactstring.ContactId_bi = cv.ContactId_bi
					AND
						(
						<cfloop array="#cdfArray#" index="cdfItem">
							<cfif ++cdfLoopIndex GT 1>
								OR
							</cfif>
							(cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER"  VALUE="#cdfItem.CDFID#"/>
								<cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
									And cv.VariableValue_vch
									<cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
										#cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#cdfRowItem.VALUE#%"/>
									<cfelse>
										#cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cdfRowItem.VALUE#"/>
									</cfif>
								</cfloop>
								)
						</cfloop>
						)
					) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arrayLen(cdfArray)#"/>
				</cfif>

				<!--- Make sure to Block DNCs --->
				<cfif inpSkipDNCCheck EQ 0 >
					AND 0 = (
						SELECT
							COUNT(oi.ContactString_vch)
						FROM
							simplelists.optinout AS oi
						WHERE
							OptOut_dt IS NOT NULL
						AND
							OptIn_dt IS NULL
						AND
							ShortCode_vch = '#RetVarGetAssignedShortCode.SHORTCODE#'
						AND
							oi.ContactString_vch = simplelists.contactstring.ContactString_vch
						)
				</cfif>

				GROUP BY
					simplelists.contactstring.ContactString_vch
			</cfquery>

			<cfset dataout.QUEUEDCOUNT = InsertQueue.RECORDCOUNT />

 			<cfif dataout.QUEUEDCOUNT GT 0>
	 			<!--- Tell the Blast queue this job is ready --->
				<cfinvoke method="UpdateBlastLogStatus" component="session.sire.models.cfc.control-point" returnvariable="RetVarUpdateBlastLogStatus" >
					<cfinvokeargument name="inpBlastId" value="#RetVarAddBlastLog.BLASTID#"/>
					<cfinvokeargument name="inpStatus" value="#BLAST_LOG_PROCESSING#"/>
				</cfinvoke>
			<cfelse>
				<!--- Tell the Blast queue this job is complete when queue empty --->
				<cfinvoke method="UpdateBlastLogStatus" component="session.sire.models.cfc.control-point" returnvariable="RetVarUpdateBlastLogStatus" >
					<cfinvokeargument name="inpBlastId" value="#RetVarAddBlastLog.BLASTID#"/>
					<cfinvokeargument name="inpStatus" value="#BLAST_LOG_COMPLETE#"/>
				</cfinvoke>
			</cfif>

			<!--- Log the Blast --->
			<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="SMS Blast Sent"/>
				<cfinvokeargument name="operator" value="inpBatchId = #inpBatchId# inpGroupId = #inpGroupId# inpShortCode=#RetVarGetAssignedShortCode.SHORTCODE#"/>
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1 />
			<!--- <cfset dataout.MESSAGE = "DebugStr=#DebugStr#"/> --->

			<cfcatch TYPE="any">
				<!--- <cfdump var="#cfcatch#" abort="true"> --->
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE# Error - #cfcatch.MESSAGE# #cfcatch.detail#"/>
				<cfset dataout.ERRMESSAGE = "Service Error, unable to send at this time."/>

				<cfreturn dataout>
			</cfcatch>

		</cftry>


		<cfreturn dataout />

	</cffunction>


    <!--- Just the essentials needed to insert a single request


    inpPostToQueueForWebServiceDeviceFulfillment
    into the queue or to send right away


    session.UserId is set by the CRON jop based on which user is blasting at a given loop

    --->
    <cffunction name="LoadQueueSMSDynamic" access="public" output="true" hint="Load a single SMS request into the queue. SMS queue requires one at a time due to the highly customized nature of sending SMS requests." >
		<cfargument name="inpBatchId" required="true" />
		<cfargument name="inpShortCode" required="false" default="" />
		<cfargument name="inpContactString" required="true" hint="The contact string - Phone number, eMail, SMS number, OTT Device/Account Number">
		<cfargument name="inpGroupId" required="false" default="0">
		<cfargument name="inpTimeZone" required="false" default="" hint="used by the schedule to send messages local to the target devices time zone">
		<cfargument name="inpCarrier" required="false" default="0" hint="used by some aggregators to specify which carrier the number is on - provide it if its there for cheaper faster service calls">

		<!--- Less commonly used params - safe to leave default --->
		<cfargument name="inpPostToQueueForWebServiceDeviceFulfillment" required="false" default="1" hint="1 (Default) forces the request to go through queue, 0 allows to force processing message in real time - see Real Time Trigger API for example">
		<cfargument name="inpLimitDistribution" required="false" default="1">
		<cfargument name="ESIID" required="false" default="-15" hint="used in higher volume installations to set which servers sends which data - default to -15">
		<cfargument name="inpOTT" required="false" default="0" hint="Allow for queueuing of Over the Top (OTT) request">
		<cfargument name="inpInternational" required="false" default="0" hint="Allow for queueuing of International SMS requests">
		<cfargument name="inpSkipNumberValidations" required="false" default="0" hint="option to skip phone number validations - leave this off unless you need high speed and are sure data is clean">
		<cfargument name="inpSkipDNCCheck" required="false" default="0" hint="option to skip DNC checks">
		<cfargument name="inpDistributionProcessId" required="false" default="0" hint="used in higher volume installations to set which servers control which data">
		<cfargument name="inpAPIRequestJSON" required="false" hint="Custom fields to be used in dynamic data - above what might be in Contact custom data fields CDF's " default="">
		<cfargument name="inpRegisteredDeliverySMS" required="false" default="0" hint="Allow each request to require SMS delivery receipts. 0 – no delivery receipt requested, 1 - return delivery receipt on final state (i.e. delivered, expired, or rejected), 2 - only return delivery receipt when final state is failed (expired or rejected)"/>
		<cfargument name="inpIRETypeAdd" required="false" default="#IREMESSAGETYPE_IRE_BLAST_TRIGGERED#">
		<cfargument name="inpQueuedScheduledDate" required="false" default="NOW()" hint="Queue wont attemp to process until after this date - this date is local to the web server/DB and NOT the phone number target. Default is DB function NOW(). If you include a date it needs to be surrounded by single quotes - ex '2017-08-15 09:00:00' ">
		<cfargument name="inpDeduplicateInterval" required="false" default="" hint="OPTIONAL - advanced uses - Needs to be a MySQL interval - Ex: INTERVAL 1 DAY Allow multiple request of the same batch outside a given date range. Example - no more than once per day/hour/week/. The interval given here is how far back from new item's scheduled_dt you want to look. Must be a valid Date or option will be ignored.">
		<cfargument name="inpXMLControlString" required="false" default="" hint="Allow calling section to specify XMLControlString - this will by pass process next response. Used for queuing up Batch start requests such as @RXT='25'">
		<cfargument name="inpAllowDuplicates" required="false" default="0" hint="Allow calling section to specify duplicates OK  - this will by passde-dupe checks. Used for queuing up Batch start requests such as @RXT='25'">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var DebugStr	= '' />
		<cfset var NEXTBATCHID = inpBatchId>
		<cfset var tickBegin	= '' />
		<cfset var GetShortCodeData	= '' />
		<cfset var SMSINIT	= 0 />
		<cfset var SMSINITMESSAGE	= '' />
		<cfset var COUNTQUEUEDUPSMS	= 0 />
		<cfset var UserLocalDNCBlocked	= 0 />
		<cfset var inpCarrier	= '' />
		<cfset var LASTQUEUEDUPID	= 0 />
		<cfset var inpKeyword	= '' />
		<cfset var inpTransactionId	= '' />
		<cfset var inpServiceId	= '' />
		<cfset var inpXMLDATA	= '' />
		<cfset var inpOverRideInterval	= 0 />
		<cfset var inpTimeOutNextQID	= 0 />
		<cfset var inpQAToolRequest	= 0 />
		<cfset var tickEnd	= '' />
		<cfset var testTime	= '' />
		<cfset var NEWUUID	= '' />
		<cfset var RetValGetXML	= '' />
		<cfset var GetUserDNCFroServiceRequest	= '' />
		<cfset var InsertIntocontactqueue	= '' />
		<cfset var InsertIntocontactqueueResult	= '' />
		<cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />
		<cfset var CurrTZ = '31'> <!--- Default to PST--->
		<cfset var GetTimeZoneInfoData	= '' />
		<cfset var CurrNPA	= '' />
		<cfset var CurrNXX	= '' />
		<cfset var ServiceRequestdataout	= '' />
		<cfset var VariableNamesArray	= '' />
		<cfset var whichField	= '' />
		<cfset var GetTZInfo	= '' />
		<cfset var inpContactTypeId	= 3  /> <!--- This is the SMS only method --->
		<cfset var inpFormDataJSONString = '' />
		<cfset var SMSONLYXMLCONTROLSTRING_VCH = '' />
		<cfset var inpFormData = {} />
		<cfset var RetValProcessNextResponseSMS = '' />
		<cfset var RetVarUpdateIRESessionDTS = '' />

		<!--- Un-used features but still need to decalre as empty so they are ignored --->
		<cfset var ABTestingBatches = '' />
		<cfset var contacStringFilter = ''>
		<cfset var contactIdListByCdfFilter = ''>
		<cfset var MCCONTACT_MASK   = '' />
		<cfset var notes_mask   = '' />
		<cfset var inpSourceMask    = '' />
		<cfset var RulesgeneratedWhereClause    = '' />
		<cfset var EstimatedCost = 0.00 />

		<cfset var NextSessionId = 0 />

		<cfset dataout.inpContactString = "#inpContactString#" />
		<cfset dataout.inpBatchId = "#inpBatchId#" />
		<cfset dataout.inpTimeZone = "#inpTimeZone#" />
		<cfset dataout.CURRTZ = "#CURRTZ#" />
		<cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
		<cfset dataout.REQUESTUUID = "#NEWUUID#" />
		<cfset dataout.SMSINIT = "#SMSINIT#" />
		<cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
		<cfset dataout.SMSDEST = "#inpShortCode#" />


		<cftry>

	        <cfif inpContactString NEQ "" >

	            <cfif inpSkipNumberValidations EQ 0 >

	                <!---Find and replace all non numerics except P X * #--->
	                <cfset arguments.inpContactString = REReplaceNoCase(arguments.inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

	                <!--- Clean up where start character is a 1 --->
	                <cfif LEFT(arguments.inpContactString, 1) EQ "1">
	                    <cfset arguments.inpContactString = RemoveChars(arguments.inpContactString, 1, 1)>
	                </cfif>

	            </cfif>

	            <!--- Is International number--->
	            <cfif inpInternational GT 0>
	                <!--- Validate allowed to / Security--->

	                <!--- Validate 011 / Country code --->
	                <cfif LEFT(arguments.inpContactString, 3) NEQ "011" >
	                    <cfthrow MESSAGE="International number must specify 011 and country code " TYPE="Any" detail="" errorcode="-4">
	                </cfif>

	                <cfset CurrTZ = "0" >

				<!--- Allow Over-The-Top integrations - number is just a Device/Account number - special aggregator types are used to handle these --->
				<cfelseif inpOTT GT 0>

					<!--- No more validations - just assume Time Zone is US Pacific --->
					<cfset CurrTZ = "31" >

	            <cfelse>
	                <!--- Validate proper 10 digit North American phone number--->

	                <cfif LEN(LEFT(arguments.inpContactString, 10)) LT 10 OR !ISNUMERIC(LEFT(arguments.inpContactString, 10))>
	                    <cfthrow MESSAGE="Not a valid 10 digit North American phone number" TYPE="Any" detail="" errorcode="-3">
	                </cfif>

	                <!--- Check for supplied Time Zone --->
	                <cfswitch expression="#UCASE(inpTimeZone)#">

	                    <cfcase value="UNK"><cfset CurrTZ = "0"></cfcase>
	                    <cfcase value="GUAM"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "38"> <cfelse> <cfset CurrTZ = "37"> </cfif></cfcase>
	                    <cfcase value="SAMOA"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "35"> <cfelse> <cfset CurrTZ = "34"> </cfif></cfcase>
	                    <cfcase value="HAWAII"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
	                    <cfcase value="HAST"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
	                    <cfcase value="ALASKA"><cfset CurrTZ = "32"></cfcase>
	                    <cfcase value="PACIFIC"><cfset CurrTZ = "31"></cfcase>
	                    <cfcase value="MOUNTAIN"><cfset CurrTZ = "30"></cfcase>
	                    <cfcase value="CENTRAL"><cfset CurrTZ = "29"></cfcase>
	                    <cfcase value="EASTERN"><cfset CurrTZ = "28"></cfcase>
	                    <cfcase value="PDT"><cfset CurrTZ = "31"></cfcase>
	                    <cfcase value="PST"><cfset CurrTZ = "31"></cfcase>
	                    <cfcase value="MDT"><cfset CurrTZ = "30"></cfcase>
	                    <cfcase value="MST"><cfset CurrTZ = "30"></cfcase>
	                    <cfcase value="CDT"><cfset CurrTZ = "29"></cfcase>
	                    <cfcase value="CST"><cfset CurrTZ = "29"></cfcase>
	                    <cfcase value="EDT"><cfset CurrTZ = "28"></cfcase>
	                    <cfcase value="EST"><cfset CurrTZ = "28"></cfcase>

	                    <!--- In case a valid value is passed in alreay - use it instead of another lookup of the phone number --->
	                    <cfcase value="36"><cfset CurrTZ = "36"></cfcase>
	                    <cfcase value="35"><cfset CurrTZ = "35"></cfcase>
	                    <cfcase value="34"><cfset CurrTZ = "34"></cfcase>
	                    <cfcase value="33"><cfset CurrTZ = "33"></cfcase>
	                    <cfcase value="32"><cfset CurrTZ = "32"></cfcase>
	                    <cfcase value="31"><cfset CurrTZ = "31"></cfcase>
	                    <cfcase value="30"><cfset CurrTZ = "30"></cfcase>
	                    <cfcase value="29"><cfset CurrTZ = "29"></cfcase>
	                    <cfcase value="28"><cfset CurrTZ = "28"></cfcase>
	                    <cfcase value="27"><cfset CurrTZ = "27"></cfcase>

	                    <cfdefaultcase>
	                        <!--- DO lookup--->
	                        <cfset CurrTZ = "-1">

	                        <!--- Adjust for numers that start with 1 --->
				            <cfif LEFT(inpContactString, 1) EQ 1>
				                <cfset CurrNPA = MID(arguments.inpContactString, 2, 3)>
				                <cfset CurrNXX = MID(arguments.inpContactString, 5, 3)>

				                <!--- Get rid of 1 when looking up in DB --->
				                <cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString)-1)>
				            <cfelse>
				                <cfset CurrNPA = LEFT(arguments.inpContactString, 3) >
				                <cfset CurrNXX = MID(arguments.inpContactString, 4, 3)>
				            </cfif>

	                        <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
	                            SELECT
	                                CASE cx.T_Z
	                                      WHEN 14 THEN 37
	                                      WHEN 11 THEN 34
	                                      WHEN 10 THEN 33
	                                      WHEN 9 THEN 32
	                                      WHEN 8 THEN 31
	                                      WHEN 7 THEN 30
	                                      WHEN 6 THEN 29
	                                      WHEN 5 THEN 28
	                                      WHEN 4 THEN 27
	                                     END AS TimeZone,
	                                CASE
	                                      WHEN fx.Cell IS NOT NULL THEN fx.Cell
	                                      ELSE 0
	                                      END  AS CellFlag
	                            FROM
	                                MelissaData.FONE AS fx JOIN
	                                MelissaData.CNTY AS cx ON
	                                (fx.FIPS = cx.FIPS)
	                             WHERE
	                                fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
	                            AND
	                            	fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
	                         </cfquery>

	                         <cfif GetTZInfo.RecordCount GT 0>
	                            <cfset CurrTZ = "#GetTZInfo.TimeZone#">
	                         <cfelse>
	                            <cfset CurrTZ = "-1">
	                         </cfif>

	                    </cfdefaultcase>

	                </cfswitch>

	                <cfif CurrTZ EQ "-1" >
	                    <!---<cfthrow MESSAGE="Could not find time zone and no valid default value provided. Possible inpTimeZone values are UNK,GUAM,SAMOA,HAWAII,ALASKA,PACIFIC,PDT,PST,MOUNTAIN,MDT,MST,CENTRAL,CDT,CST,EASTERN,EDT,EST" TYPE="Any" detail="" errorcode="-5">--->
	                    <!--- For real time triggered SMS TimeZone is not required --->
	                    <cfset CurrTZ = "31">
	                </cfif>

	            </cfif>

	        </cfif>

			<!--- Append FORM scope with that of the inpFormDataJSON --->
			<cfif !IsStruct(arguments.inpAPIRequestJSON) AND LEN(TRIM(arguments.inpAPIRequestJSON)) GT 0 >
				<cfset inpFormData = DeserializeJSON(arguments.inpAPIRequestJSON) />
				<cfset StructAppend(FORM, inpFormData, true) />
			</cfif>

			<cfset DebugStr = DebugStr & " Load SMS Dynamic Start">

	        <!--- Start timing test --->
	        <cfset tickBegin = GetTickCount()>

	        <!--- Set default so if query does not find anything we get RecordCount = 0 OK --->
	        <cfset GetShortCodeData = {}>
	        <cfset GetShortCodeData.RecordCount = 0>


	        <!--- Look if short code is defined by passed in value --->
	        <!--- First look at batch id for pre-defined short code --->
	        <!--- Then look if keyword is defined --->
	        <!--- Then look if user has default short code --->

	        <cfquery name="GetShortCodeData" datasource="#Session.DBSourceEBM#">
	            SELECT
	                k.Response_vch,
	                k.Survey_int,
	                scr.RequesterId_int,
	                k.Keyword_vch,
	                sc.ShortCode_vch
	            FROM
	                SMS.Keyword AS k
	            LEFT OUTER JOIN
	                SMS.shortcoderequest AS scr
	            ON
	                k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
	            JOIN
	                SMS.ShortCode AS sc
	            ON
	                sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
	            WHERE
	                k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	            AND
	                k.Active_int IN (1,-3)
	            ORDER BY
	                k.BatchId_bi
	            LIMIT 1
	        </cfquery>

	        <cfif GetShortCodeData.RecordCount EQ 0 AND inpBatchId GT 0>

	           <!--- Preview overide by specifying short code --->
	            <cfif TRIM(inpShortCode) NEQ "">

	                <cfset GetShortCodeData = {}>
	                <cfset GetShortCodeData.RecordCount = 1>
	                <cfset GetShortCodeData.Survey_int = 1>
	                <!--- This Keyword_vch must be blank to trigger by Batch Id and Short Code--->
	                <cfset GetShortCodeData.Keyword_vch = "">
	                <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
	                <cfset GetShortCodeData.Response_vch = "">
	                <cfset GetShortCodeData.RequesterId_int = Session.USERID>

	                <cfset DebugStr = DebugStr & " Batch Id Triggered: #inpBatchId#" />
	            <cfelse>

	                <cfset DebugStr = DebugStr & " inpShortCode not found: #inpBatchId#" />

	                <!--- No need to Get User Id from Batch Id - the Session.UserId should be good enough - this is only called via authenticated API or logged in user account --->
	                <cfquery datasource="#Session.DBSourceEBM#" name="GetShortCodeData">
	                        SELECT
	                            sc.ShortCodeId_int,
	                            sc.ShortCode_vch,
	                            scr.RequesterId_int,
	                            scr.ShortCodeRequestId_int
	                        FROM
	                            sms.shortcode sc
	                        JOIN
	                            sms.shortcoderequest scr
	                        ON
	                            sc.ShortCodeId_int = scr.ShortCodeId_int
	                        WHERE
	                            scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	                        ORDER BY
	                            scr.ShortCodeRequestId_int ASC
	                        LIMIT 1
	                </cfquery>

	                <cfif GetShortCodeData.RECORDCOUNT EQ 0>

	                    <cfset SMSINIT = 0 />
	                    <cfset SMSINITMESSAGE = "No Keyword Defined For This Batch Id"/>
	                <cfelse>

	                    <cfset arguments.inpShortCode = GetShortCodeData.ShortCode_vch />

	                    <cfset GetShortCodeData = {}>
	                    <cfset GetShortCodeData.RecordCount = 1>
	                    <cfset GetShortCodeData.Survey_int = 1>
	                    <!--- This Keyword_vch must be blank to trigger by Batch Id and Short Code --->
	                    <cfset GetShortCodeData.Keyword_vch = "">
	                    <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
	                    <cfset GetShortCodeData.Response_vch = "">
	                    <cfset GetShortCodeData.RequesterId_int = Session.USERID>
	                </cfif>

	            </cfif>

	            <cfset GetShortCodeData = {}>
	            <cfset GetShortCodeData.RecordCount = 1>
	            <cfset GetShortCodeData.Survey_int = 1>
	            <!--- This Keyword_vch must be blank to trigger by Batch Id --->
	            <cfset GetShortCodeData.Keyword_vch = "">
	            <cfset GetShortCodeData.ShortCode_vch = inpShortCode>
	            <cfset GetShortCodeData.Response_vch = "">
	            <cfset GetShortCodeData.RequesterId_int = Session.USERID>

	            <cfset DebugStr = DebugStr & " Batch Id Triggered: #inpBatchId#" />

	        </cfif>

	<!---<cfset DebugStr = DebugStr & " GetShortCodeData = #SerializeJSON(GetShortCodeData)#"> --->

	        <cfset UserLocalDNCBlocked = 0 />

	        <cfif GetShortCodeData.RecordCount GT 0>

	        	<!--- Set short code data here for API requests even if on DNC  --->
	        	<cfset arguments.inpShortCode = "#GetShortCodeData.ShortCode_vch#"/>

	        	<!--- The keyword is no longer required - Batch Id is enough. By leaving this blank we can allow API calls to override running campaigns --->
				<!---  <cfset inpKeyword = "#GetShortCodeData.Keyword_vch#"/> --->

				<cfif inpSkipDNCCheck EQ 0>

				    <!--- Session User DNC --->
				    <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
				    <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
				        SELECT
				            COUNT(oi.ContactString_vch) AS TotalCount
				        FROM
				            simplelists.optinout AS oi
				        WHERE
				            OptOut_dt IS NOT NULL
				        AND
				            OptIn_dt IS NULL
				        AND
				            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetShortCodeData.ShortCode_vch#">
				        AND
				            oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
				    </cfquery>

				    <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>
				        <cfset UserLocalDNCBlocked = 1 />

				        <cfset LASTQUEUEDUPID = "DNC">
				        <cfset DebugStr = DebugStr & " Blocked by User DNC">

				    <cfelse>
				        <cfset UserLocalDNCBlocked = 0 />
				    </cfif>

				</cfif>

	            <!--- Not blocked by Service Request DNC Check--->
	            <cfif UserLocalDNCBlocked EQ 0 >

	                <cfif CurrTZ EQ 0 OR CurrTZ EQ "">
	                    <cfset CurrTZ = 31>
	                </cfif>

	                <!--- Build XMLControlString for webservice calls with CCD PTL=13--->
	                <cfset SMSONLYXMLCONTROLSTRING_VCH = "">
	                <cfset SMSONLYXMLCONTROLSTRING_VCH = SMSONLYXMLCONTROLSTRING_VCH & "<DM LIB='0' MT='1' PT='13' EMPT='Dynamic'><ELE ID='0'>0</ELE></DM>">
	                <cfset SMSONLYXMLCONTROLSTRING_VCH = SMSONLYXMLCONTROLSTRING_VCH & "<RXSS>">

	                <cfset inpTransactionId = ""/>
	                <cfset inpServiceId = ""/>
	                <cfset inpXMLDATA = ""/>
	                <cfset inpOverRideInterval = "0"/>
	                <cfset inpTimeOutNextQID = "0"/>
	                <cfset inpQAToolRequest = "0"/>

	                <!--- Calculate final result --->
	                <cfset tickEnd = GetTickCount()>
	                <cfset testTime = tickEnd - tickBegin>

	                <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>

	                    <cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />


	                    <!--- Bullet Proof: Additonal SH Violation Prevention Check: Look for most efficient way to block duplicates here -  --->
	                    <!--- trying.... https://stackoverflow.com/questions/15622121/mysql-query-insert-where-not-exists --->

	                    <!--- This query is defined in two places so most of the same messaging logic applies - process now(inpPostToQueueForWebServiceDeviceFulfillment=0) or process later(inpPostToQueueForWebServiceDeviceFulfillment=1) --->
	                    <!--- In this case add entry to queue for processing --->
	                    <!--- Insert customized message --->
	                    <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
	                    	SET tx_isolation = 'READ-COMMITTED';
	                        INSERT INTO
	                            simplequeue.contactqueue
	                        (
	                            BatchId_bi,
	                            DTSStatusType_ti,
	                            DTS_UUID_vch,
	                            TypeMask_ti,
	                            TimeZone_ti,
	                            CurrentRedialCount_ti,
	                            UserId_int,
	                            PushLibrary_int,
	                            PushElement_int,
	                            PushScript_int,
	                            EstimatedCost_int,
	                            ActualCost_int,
	                            CampaignTypeId_int,
	                            GroupId_int,
	                            Scheduled_dt,
	                            Queue_dt,
	                            Queued_DialerIP_vch,
	                            ContactString_vch,
	                            Sender_vch,
	                            XMLCONTROLSTRING_VCH,
	                            ShortCode_vch,
	                            ProcTime_int,
	                            DistributionProcessId_int
	                        )
	                        SELECT
	                        	a.BatchId_bi,
	                            a.DTSStatusType_ti,
	                            a.DTS_UUID_vch,
	                            a.TypeMask_ti,
	                            a.TimeZone_ti,
	                            a.CurrentRedialCount_ti,
	                            a.UserId_int,
	                            a.PushLibrary_int,
	                            a.PushElement_int,
	                            a.PushScript_int,
	                            a.EstimatedCost_int,
	                            a.ActualCost_int,
	                            a.CampaignTypeId_int,
	                            a.GroupId_int,
	                            a.Scheduled_dt,
	                            a.Queue_dt,
	                            a.Queued_DialerIP_vch,
	                            a.ContactString_vch,
	                            a.Sender_vch,
	                            a.XMLCONTROLSTRING_VCH,
	                            a.ShortCode_vch,
	                            a.ProcTime_int,
	                            a.DistributionProcessId_int
	                        FROM
	                        (SELECT
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#"> AS BatchId_bi,
	                            #EBMQueue_InFulfillment# AS DTSStatusType_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#"> AS DTS_UUID_vch,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactTypeId#"> AS TypeMask_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#"> AS TimeZone_ti,
	                            0 AS CurrentRedialCount_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"> AS UserId_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushLibrary_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushElement_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushScript_int,
	                            #EstimatedCost# AS EstimatedCost_int,
	                            0.000 AS ActualCost_int,
	                            30 AS CampaignTypeId_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#"> AS GroupId_int,
	                            #PreserveSingleQuotes(inpQueuedScheduledDate)# AS Scheduled_dt, <!---  #inpQueuedScheduledDate# AS Scheduled_dt,  ---> <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
	                            NOW() AS Queue_dt,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="RealTimeService"> AS Queued_DialerIP_vch,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#"> AS ContactString_vch,
	                            'LoadQueueSMSDynamic' AS Sender_vch,
	                            'Posted Directly to Fulfillment' AS XMLCONTROLSTRING_VCH,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"> AS ShortCode_vch,
	                            #testTime# AS ProcTime_int,
	                            #inpDistributionProcessIdLocal# AS DistributionProcessId_int
	                        ) a

					    <cfif inpAllowDuplicates EQ 0>
						    <!--- inpDeduplicateInterval is special case that allows duplicate  batch/phonenumbers, but only outside a given interval - not too many messages in the same day from same campaign in queue  --->
		                        <!--- If inpDeduplicateInterval is '' or not specified - will default to block all duplicates batch/phonenumber from loading into queue --->
		                        WHERE NOT EXISTS (
						    					SELECT
												1
											FROM
												simplequeue.contactqueue
											WHERE
												BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
											AND
												ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
											<cfif LEN(TRIM(inpDeduplicateInterval)) GT 0>
												AND Scheduled_dt >= #PreserveSingleQuotes(inpQueuedScheduledDate)# - #inpDeduplicateInterval#
												AND Scheduled_dt < #PreserveSingleQuotes(inpQueuedScheduledDate)# + #inpDeduplicateInterval#
											</cfif>
										 )
						</cfif>
	                    </cfquery>

	                    <cfset COUNTQUEUEDUPSMS = COUNTQUEUEDUPSMS + InsertIntocontactqueueResult.RecordCount>
	                    <cfif InsertIntocontactqueueResult.RecordCount GT 0>
	                    	<cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>
	                    </cfif>

	                    <!--- Use only for debugging --->
						<!--- <cfset dataout.QUERYRESULT = SerializeJSON(InsertIntocontactqueueResult) /> --->

	                </cfif>


				<cfif inpXMLControlString NEQ "" >

					<cfset SMSINIT = 0 />
					<cfset SMSONLYXMLCONTROLSTRING_VCH = inpXMLControlString />
					<cfset NextSessionId = 0 />

				<cfelse>

					<cfinvoke
						 component="session.cfc.csc.csc"
						 method="ProcessNextResponseSMS"
						 returnvariable="RetValProcessNextResponseSMS">
						    <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
						    <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
						    <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
						    <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
						    <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
						    <cfinvokeargument name="inpServiceId" value="#inpServiceId#"/>
						    <cfinvokeargument name="inpXMLDATA" value="#inpXMLDATA#"/>
						    <cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>
						    <cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>
						    <cfinvokeargument name="inpQAToolRequest" value="0"/>
						    <cfif inpPostToQueueForWebServiceDeviceFulfillment EQ 0>
							   <cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#"/>
						    </cfif>
						    <cfinvokeargument name="inpPostToQueueForWebServiceDeviceFulfillment" value="#inpPostToQueueForWebServiceDeviceFulfillment#"/>
						    <cfinvokeargument name="inpFormData" value="#FORM#"/>
						    <cfinvokeargument name="inpSessionUserId" value="#Session.USERID#"/>
						    <cfinvokeargument name="inpRegisteredDeliverySMS" value="#inpRegisteredDeliverySMS#"/>
						    <cfinvokeargument name="inpIREType" value="#inpIRETypeAdd#">
						    <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
					 </cfinvoke>

					 <cfset SMSINIT = RetValProcessNextResponseSMS.PROKF />
					 <cfset SMSINITMESSAGE = SMSINITMESSAGE & RetValProcessNextResponseSMS.PRM />

					 <!--- Let API return a value of 0 if not defined or found --->
					 <cfif SMSINIT EQ "">
						<cfset SMSINIT = 0 />
					 </cfif>

					 <!---   <cfset DebugStr = DebugStr & " SMSINIT = #SMSINIT# SMSINITMESSAGE=#SMSINITMESSAGE# RetValProcessNextResponseSMS =  #SerializeJSON(RetValProcessNextResponseSMS)#"> --->

					 <cfset SMSONLYXMLCONTROLSTRING_VCH = SMSONLYXMLCONTROLSTRING_VCH & RetValProcessNextResponseSMS.RSSSDATA />

					 <cfset SMSONLYXMLCONTROLSTRING_VCH = SMSONLYXMLCONTROLSTRING_VCH & "</RXSS>">

					 <!--- Use Current CCD String for File Sequence and User Specified Data--->
					 <cfset SMSONLYXMLCONTROLSTRING_VCH = SMSONLYXMLCONTROLSTRING_VCH & "<CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='0' RMin='0' PTL='1' PTM='1' RDWS='76' ESI='#ESIID#'>0</CCD>">

					<cfset NextSessionId = RetValProcessNextResponseSMS.GETRESPONSE.IRESESSIONID />

				</cfif>


	                <!--- Post to Queue After processing --->
	                <cfif inpPostToQueueForWebServiceDeviceFulfillment GT 0>

	                    <!--- Calculate final result --->
	                    <cfset tickEnd = GetTickCount()>
	                    <cfset testTime = tickEnd - tickBegin>

	                    <cfset NEWUUID = createobject("java", "java.util.UUID").randomUUID().toString() />

	                    <!--- Bullet Proof: Additonal SH Violation Prevention Check: Look for most efficient way to block duplicates here -  --->
						<!--- trying.... https://stackoverflow.com/questions/15622121/mysql-query-insert-where-not-exists --->

	                    <!--- This query is defined in two places so most of the same messaging logic applies - process now(inpPostToQueueForWebServiceDeviceFulfillment=0) or process later(inpPostToQueueForWebServiceDeviceFulfillment=1) --->
	                    <!--- Depending on the processing type - different values are send to the message generation ProcessNextResponseSMS can reference its own unique queue id --->
	                    <!--- Still add entry to queue even if only for tracking purposes--->
	                    <!--- Insert customized message --->
	                    <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
	                    	SET tx_isolation = 'READ-COMMITTED';
	                        INSERT INTO
	                            simplequeue.contactqueue
	                        (
	                            BatchId_bi,
	                            DTSStatusType_ti,
	                            DTS_UUID_vch,
	                            TypeMask_ti,
	                            TimeZone_ti,
	                            CurrentRedialCount_ti,
	                            UserId_int,
	                            PushLibrary_int,
	                            PushElement_int,
	                            PushScript_int,
	                            EstimatedCost_int,
	                            ActualCost_int,
	                            CampaignTypeId_int,
	                            GroupId_int,
	                            Scheduled_dt,
	                            Queue_dt,
	                            Queued_DialerIP_vch,
	                            ContactString_vch,
	                            Sender_vch,
	                            XMLCONTROLSTRING_VCH,
	                            ShortCode_vch,
	                            ProcTime_int,
	                            DistributionProcessId_int
	                        )
	                        SELECT
	                        	a.BatchId_bi,
	                            a.DTSStatusType_ti,
	                            a.DTS_UUID_vch,
	                            a.TypeMask_ti,
	                            a.TimeZone_ti,
	                            a.CurrentRedialCount_ti,
	                            a.UserId_int,
	                            a.PushLibrary_int,
	                            a.PushElement_int,
	                            a.PushScript_int,
	                            a.EstimatedCost_int,
	                            a.ActualCost_int,
	                            a.CampaignTypeId_int,
	                            a.GroupId_int,
	                            a.Scheduled_dt,
	                            a.Queue_dt,
	                            a.Queued_DialerIP_vch,
	                            a.ContactString_vch,
	                            a.Sender_vch,
	                            a.XMLCONTROLSTRING_VCH,
	                            a.ShortCode_vch,
	                            a.ProcTime_int,
	                            a.DistributionProcessId_int
	                        FROM
	                        (SELECT
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#"> AS BatchId_bi,
	                            #EBMQueue_Queued# AS DTSStatusType_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#NEWUUID#"> AS DTS_UUID_vch,
	                            3 AS TypeMask_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#"> AS TimeZone_ti,
	                            0 AS CurrentRedialCount_ti,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"> AS UserId_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushLibrary_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushElement_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0"> AS PushScript_int,
	                            #EstimatedCost# AS EstimatedCost_int,
	                            0.000 AS ActualCost_int,
	                            30 AS CampaignTypeId_int,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#"> AS GroupId_int,
	                            #PreserveSingleQuotes(inpQueuedScheduledDate)# AS Scheduled_dt, <!---  #inpQueuedScheduledDate# AS Scheduled_dt,  ---> <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
	                            NULL AS Queue_dt,
	                            NULL AS Queued_DialerIP_vch,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#"> AS ContactString_vch,
	                            'LoadQueueSMSDynamic' AS Sender_vch,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSONLYXMLCONTROLSTRING_VCH#"> AS XMLCONTROLSTRING_VCH,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#"> AS ShortCode_vch,
	                            #testTime# AS ProcTime_int,
	                            #inpDistributionProcessIdLocal# AS DistributionProcessId_int
	                        ) a

					    <cfif inpAllowDuplicates EQ 0>
		                        <!--- inpDeduplicateInterval is special case that allows duplicate  batch/phonenumbers, but only outside a given interval - not too many messages in the same day from same campaign in queue  --->
		                        <!--- If inpDeduplicateInterval is '' or not specified - will default to block all duplicates batch/phonenumber from loading into queue --->
		                        WHERE NOT EXISTS (
						    					SELECT
												1
											FROM
												simplequeue.contactqueue
											WHERE
												BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
											AND
												ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">

							    				<cfif LEN(TRIM(inpDeduplicateInterval)) GT 0>
												AND Scheduled_dt >= #PreserveSingleQuotes(inpQueuedScheduledDate)# - #inpDeduplicateInterval#
												AND Scheduled_dt < #PreserveSingleQuotes(inpQueuedScheduledDate)# + #inpDeduplicateInterval#
											</cfif>
										)
						</cfif>

	                    </cfquery>

	                    <cfset COUNTQUEUEDUPSMS = COUNTQUEUEDUPSMS + InsertIntocontactqueueResult.RecordCount>

	                    <cfif InsertIntocontactqueueResult.RecordCount GT 0>
	                    	<cfset LASTQUEUEDUPID = InsertIntocontactqueueResult.GENERATED_KEY>

	                    	<!--- Only for queue requests do we need to do this - faster for API requests - Add the DTSId_int to the session to distiguish requests --->
	                    	<!--- Need a non-blocking way to pass this to new session --->
	                    	<cfif LASTQUEUEDUPID GT 0 AND NextSessionId GT 0>

							<cfinvoke method="UpdateIRESessionDTS" component="session.sire.models.cfc.control-point" returnvariable="RetVarUpdateIRESessionDTS" >
								<cfinvokeargument name="inpIRESEssionId" value="#RetValProcessNextResponseSMS.GETRESPONSE.IRESESSIONID#"/>
								<cfinvokeargument name="inpDTSId" value="#LASTQUEUEDUPID#"/>
							</cfinvoke>

	                    	</cfif>

	                    </cfif>

						<cfset DebugStr = DebugStr & " InsertIntocontactqueueResult.RecordCount = #InsertIntocontactqueueResult.RecordCount#">

						<!--- Use only for debugging --->
						<!--- <cfset dataout.QUERYRESULT = SerializeJSON(InsertIntocontactqueueResult) /> --->

	                </cfif>

	             </cfif><!--- Not blocked by Service Request DNC Check--->

	        </cfif>

	        <cfset dataout.RXRESULTCODE = 1 />
	   		<cfset dataout.inpContactString = "#inpContactString#" />
			<cfset dataout.inpBatchId = "#inpBatchId#" />
			<cfset dataout.inpTimeZone = "#inpTimeZone#" />
			<cfset dataout.CURRTZ = "#CURRTZ#" />
			<cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
			<cfset dataout.REQUESTUUID = "#NEWUUID#" />
			<cfset dataout.SMSINIT = "#SMSINIT#" />
			<cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
			<cfset dataout.SMSDEST = "#inpShortCode#" />

	        <cfset dataout.TYPE = "" />
	        <cfset dataout.MESSAGE = "#DebugStr#" />
	        <cfset dataout.ERRMESSAGE = "" />



		<cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>



	<cffunction name="BlastValidateScheduleActive" access="public" output="false" hint="Setup a campaigns schedule based on copy or default schedule.">
		<cfargument name="inpSourceBatchId" TYPE="string" hint="The BatchId to read the schedule from" />
		<cfargument name="inpTargetBatchId" TYPE="string" hint="The BatchId to set the schedule for" />
		<cfargument name="inpSchedule" TYPE="string" default="" hint="A JSON string of schedule options you wish to override. Each variable name must match expected list of possibilities. See docs for further info." />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var getShortCode = structNew() />
		<cfset var NewScheduleObj = {} />
		<cfset var GetSchedule	= '' />
		<cfset var CreateSchedule	= '' />
		<cfset var ScheduleOverride = '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.COPY = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.HTML  = "" />

		<cftry>

			<!--- This allows self refernece checks and updates --->
			<cfif arguments.inpTargetBatchId EQ "">
				<cfset arguments.inpTargetBatchId = arguments.inpSourceBatchId />
			</cfif>

			<cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">
				SELECT
					ENABLED_TI,
					DYNAMICSCHEDULEDAYOFWEEK_TI,
	                STARTHOUR_TI,
	                ENDHOUR_TI,
	                SUNDAY_TI,
	                MONDAY_TI,
	                TUESDAY_TI,
	                WEDNESDAY_TI,
	                THURSDAY_TI,
	                FRIDAY_TI,
	                SATURDAY_TI,
	                LOOPLIMIT_INT,
	                STOP_DT,
	                START_DT,
	                STARTMINUTE_TI,
	                ENDMINUTE_TI,
	                BLACKOUTSTARTHOUR_TI,
	                BLACKOUTENDHOUR_TI,
	                LASTUPDATED_DT
            	FROM
            		simpleobjects.scheduleoptions
            	WHERE
                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSourceBatchId#">
                AND
                    DYNAMICSCHEDULEDAYOFWEEK_TI = 0
			</cfquery>

			<cfset NewScheduleObj = {} />

			<cfif GetSchedule.RecordCount GT 0 >

				<cfset NewScheduleObj.ENABLED_TI = GetSchedule.ENABLED_TI />
				<cfset NewScheduleObj.STARTHOUR_TI = GetSchedule.STARTHOUR_TI />
				<cfset NewScheduleObj.ENDHOUR_TI = GetSchedule.ENDHOUR_TI />
				<cfset NewScheduleObj.SUNDAY_TI = GetSchedule.SUNDAY_TI />
				<cfset NewScheduleObj.MONDAY_TI = GetSchedule.MONDAY_TI />
				<cfset NewScheduleObj.TUESDAY_TI = GetSchedule.TUESDAY_TI />
				<cfset NewScheduleObj.WEDNESDAY_TI = GetSchedule.WEDNESDAY_TI />
				<cfset NewScheduleObj.THURSDAY_TI = GetSchedule.THURSDAY_TI />
				<cfset NewScheduleObj.FRIDAY_TI = GetSchedule.FRIDAY_TI />
				<cfset NewScheduleObj.SATURDAY_TI = GetSchedule.SATURDAY_TI />
				<cfset NewScheduleObj.LOOPLIMIT_INT = GetSchedule.LOOPLIMIT_INT />
				<cfset NewScheduleObj.STOP_DT = GetSchedule.STOP_DT     />
				<cfset NewScheduleObj.START_DT = GetSchedule.START_DT />
				<cfset NewScheduleObj.STARTMINUTE_TI = GetSchedule.STARTMINUTE_TI />
				<cfset NewScheduleObj.ENDMINUTE_TI = GetSchedule.ENDMINUTE_TI />
				<cfset NewScheduleObj.BLACKOUTSTARTHOUR_TI = GetSchedule.BLACKOUTSTARTHOUR_TI />
				<cfset NewScheduleObj.BLACKOUTENDHOUR_TI = GetSchedule.BLACKOUTENDHOUR_TI />
				<cfset NewScheduleObj.DYNAMICSCHEDULEDAYOFWEEK_TI = GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI />

				<!--- Atart date can be in future but Stop date MUST be in future or no sense in blasting --->
				<!--- If the stop date is less than Now() --->
				<cfif DateCompare(NewScheduleObj.STOP_DT, NOW()) GT 0 >
					<cfset NewScheduleObj.STOP_DT = "#LSDateFormat(DateAdd('d', 30, NOW()), 'yyyy-mm-dd 00:00:00')#" />
				</cfif>

				<cfset dataout.COPY = 1 />

			<cfelse>

				<cfset NewScheduleObj.STARTHOUR_TI = 9 />
				<cfset NewScheduleObj.ENDHOUR_TI = 7 />
				<cfset NewScheduleObj.SUNDAY_TI = 1 />
				<cfset NewScheduleObj.MONDAY_TI = 1 />
				<cfset NewScheduleObj.TUESDAY_TI = 1 />
				<cfset NewScheduleObj.WEDNESDAY_TI = 1 />
				<cfset NewScheduleObj.THURSDAY_TI = 1 />
				<cfset NewScheduleObj.FRIDAY_TI = 1 />
				<cfset NewScheduleObj.SATURDAY_TI = 1 />
				<cfset NewScheduleObj.LOOPLIMIT_INT = 100 />
				<cfset NewScheduleObj.STOP_DT = "#LSDateFormat(DateAdd('d', 30, NOW()), 'yyyy-mm-dd 00:00:00')#" />
				<cfset NewScheduleObj.START_DT = "#LSDateFormat(NOW(), 'yyyy-mm-dd 00:00:00')#" />
				<cfset NewScheduleObj.STARTMINUTE_TI = 0 />
				<cfset NewScheduleObj.ENDMINUTE_TI = 0 />
				<cfset NewScheduleObj.BLACKOUTSTARTHOUR_TI = 0 />
				<cfset NewScheduleObj.BLACKOUTENDHOUR_TI = 0 />
				<cfset NewScheduleObj.DYNAMICSCHEDULEDAYOFWEEK_TI = 0 />

			</cfif>


			<!--- Check for overrides --->
			<cfif LEN(TRIM(inpSchedule)) GT 0 >

				<cfset ScheduleOverride = DeSerializeJSON(arguments.inpSchedule) />

				<!--- Any matching overrides will replace (true) the values in the target structure - aything named incorrectly will not be used --->
				<cfset StructAppend(NewScheduleObj, ScheduleOverride, true) />

			</cfif>

			<cfquery name="CreateSchedule" datasource="#Session.DBSourceEBM#">

				<!--- Update with create if not exists --->
		       	INSERT INTO simpleobjects.scheduleoptions
		            (
		                BatchId_bi,
		                STARTHOUR_TI,
		                ENDHOUR_TI,
		                SUNDAY_TI,
		                MONDAY_TI,
		                TUESDAY_TI,
		                WEDNESDAY_TI,
		                THURSDAY_TI,
		                FRIDAY_TI,
		                SATURDAY_TI,
		                LOOPLIMIT_INT,
		                STOP_DT,
		                START_DT,
		                STARTMINUTE_TI,
		                ENDMINUTE_TI,
		                BLACKOUTSTARTHOUR_TI,
		                BLACKOUTENDHOUR_TI,
		                LASTUPDATED_DT,
		                DYNAMICSCHEDULEDAYOFWEEK_TI,
		                ENABLED_TI
		            )
		            VALUES
		            (
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(inpTargetBatchId)#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.STARTHOUR_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.ENDHOUR_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.SUNDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.MONDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.TUESDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.WEDNESDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.THURSDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.FRIDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.SATURDAY_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NewScheduleObj.LOOPLIMIT_INT#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(NewScheduleObj.STOP_DT,'YYYY-MM-DD'))#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(NewScheduleObj.START_DT,'YYYY-MM-DD'))#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.STARTMINUTE_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.ENDMINUTE_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.BLACKOUTSTARTHOUR_TI#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.BLACKOUTENDHOUR_TI#">,
		                NOW(),
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
		                1
		            )

		            ON DUPLICATE KEY UPDATE
                        STARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.STARTHOUR_TI#">,
	                    ENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.ENDHOUR_TI#">,
	                    SUNDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.SUNDAY_TI#">,
	                    MONDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.MONDAY_TI#">,
	                    TUESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.TUESDAY_TI#">,
	                    WEDNESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.WEDNESDAY_TI#">,
	                    THURSDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.THURSDAY_TI#">,
	                    FRIDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.FRIDAY_TI#">,
	                    SATURDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.SATURDAY_TI#">,
	                    LOOPLIMIT_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NewScheduleObj.LOOPLIMIT_INT#">,
	                    STOP_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(NewScheduleObj.STOP_DT,'YYYY-MM-DD'))#">,
	                    START_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(NewScheduleObj.START_DT,'YYYY-MM-DD'))#">,
	                    STARTMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.STARTMINUTE_TI#">,
	                    ENDMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.ENDMINUTE_TI#">,
	                    BLACKOUTSTARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.BLACKOUTSTARTHOUR_TI#">,
	                    BLACKOUTENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.BLACKOUTENDHOUR_TI#">,
	                    LASTUPDATED_DT = NOW(),
	                    ENABLED_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#NewScheduleObj.ENABLED_TI#">

			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="VerifyBlastNotDuplicateBR" access="remote" output="false" hint="Verify that the blast request is not a duplicate within given time range." >
		<cfargument name="inpBatchId" required="true" hint="The pre-defined campaign Id you created under your account.">
		<cfargument name="inpGroupId" required="true" TYPE="string" hint="The List to send to or Optionally a CSV list of List Ids" />
		<cfargument name="inpMinBR" required="no" default="60" hint="Business rule - block new campaign blasts if duplicate in last (inpMinBR) minutes. Default is 60 minutes. ">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />
		<cfset var NextGroupId	= '' />
		<cfset var VerifyUnique	= '' />
		<cfset var GetQueueQuery	= '' />
		<cfset var groupId = 0 />
		<cfset var groupIndex = 0 />

		<!---
		Positive is success
		1 = OK
		2 =
		3 =
		4 =

		Negative is failure
		-1 = general failure
		-2 = S
		-3 =

		--->

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.COUNT = 100 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<!--- Validate inpGroupId is list of groups --->
			<cfif !REFind("^\d+(,\d+)*$", inpGroupId)>
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "Group Id (#inpGroupId#) is valid"/>

				<cfreturn dataout>
			</cfif>

			<cfquery name="GetQueueQuery" datasource="#Session.DBSourceEBM#">
				SELECT
					PKId_int AS PKId
				FROM
					simpleobjects.batch_blast_log
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					(SourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#"> OR BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">)
				AND
					Created_dt > DATE_ADD(NOW(), INTERVAL -<CFQUERYPARAM cfsqltype="CF_SQL_INTEGER" value="#inpMinBR#"/> MINUTE)
				AND
					(
					<cfloop list="#inpGroupId#" delimiters="," index="groupId">
						<cfif groupIndex++ GTE 1>
							OR
						</cfif>
						FIND_IN_SET(#groupId#, GroupIds_vch)
					</cfloop>
					)
				LIMIT 1
			</cfquery>

			<cfif GetQueueQuery.PKId EQ 0>
				<cfquery name="GetQueueQuery" datasource="#Session.DBSourceEBM#">
					SELECT
						DTSId_int AS PKId
					FROM
						simplequeue.contactqueue
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
					AND
						GroupId_int IN (#inpGroupId#)
					AND
						Queue_dt > DATE_ADD(NOW(), INTERVAL -<CFQUERYPARAM cfsqltype="CF_SQL_INTEGER" value="#inpMinBR#"/> MINUTE)
					LIMIT 1
				</cfquery>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.COUNT = GetQueueQuery.PKId />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />

			<cfcatch TYPE="any">

				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataout.COUNT = 100 />
				<cfset dataout.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataout.TYPE = "#cfcatch.TYPE#">
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>

		</cftry>

		<cfreturn dataout />
	</cffunction>


	<cffunction access="public" name="UpdateBatchLogQueueStatus" hint="Can only be called from within CF server - no REMOTE access">
		<cfargument name="inpBlastLogId" TYPE="numeric" required="true" hint="The Batch Log Id that is running" />
		<cfargument name="inpBlastLogQueueId" TYPE="numeric" required="true" hint="The Batch Log Queue Id that is updated" />
		<cfargument name="inpStatus" TYPE="string" required="true" hint="The Batch Log Queue Status - See/Use constants file for possible values" />
		<cfargument name="inpNotes" TYPE="string" required="false" default="" hint="Error messages or other info you wish to pass through when record is not deleted" />

		<cfset var UpdateSimpleXCallQueueData = {} />
		<cfset var UpdateBlastLog = {} />
		<cfset var UpdateSimpleXCallQueueData = {} />

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<cfset var DeadlockIndex = 0 />
		<cfset var DeadlockCheckThread = 0 />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<!--- Squash deadlocking issues? --->
		<cfloop from="1" to="3" step="1" index="DeadlockIndex">
			<cftry>

				<cfif inpStatus EQ BLAST_LOG_QUEUE_COMPLETE>

					<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
						DELETE FROM
							simplequeue.batch_load_queue
						WHERE
							Id_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpBlastLogQueueId#"/>
					</cfquery>

					<cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.batch_blast_log
						SET
							ActualLoaded_int = ActualLoaded_int + 1
						WHERE
							PKId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpBlastLogId#"/>
					</cfquery>
				<cfelse>
					<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
						UPDATE
							simplequeue.batch_load_queue
						SET
							Status_ti = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpStatus#"/>,
							Note_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LEFT(inpNotes,255)#"/>
						WHERE
							Id_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpBlastLogQueueId#"/>
					</cfquery>
				</cfif>

				<!--- UPdate Status OK --->
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />

				<cfbreak>

				<cfcatch type="any">

					<!--- Look for DB deadloack issues - try to recover by looping --->
					<cfif findnocase("Deadlock", cfcatch.detail) GT 0 >

						<cfscript>
							DeadlockCheckThread = CreateObject("java","java.lang.Thread");
							DeadlockCheckThread.sleep(3000); // CF will now sleep for 3 seconds to give deadlocks time to clear
						</cfscript>

					<cfelse>

						<cfset dataout.RXRESULTCODE = -1>
						<cfset dataout.TYPE = "#cfcatch.TYPE#">
						<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
						<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">

						<!--- other errors - break from loop--->
						<cfbreak>

					</cfif>

				</cfcatch>

			</cftry>

		</cfloop>

		<cfreturn dataout />
	</cffunction>


	<cffunction name="UpdateIRESessionDTS" access="public" output="true" hint="Set the DTS of a Session - Used to track whether Session is Active or not (Has Session been sent to user yet)">
	    <cfargument name="inpIRESEssionId" TYPE="string" required="yes"/>
        <cfargument name="inpDTSId" TYPE="string" required="no" default="0"/>

       	<cfset var LOCALOUTPUT = {} />
		<cfset var UPDATESURVEYSTATE = '' />
		<cfset var DebugStr = '' />

        <cftry>

			<!--- Validate inpMasterRXCallDetailId Id--->
			<cfif !isnumeric(inpIRESEssionId) >
            	<cfthrow MESSAGE="Invalid inpIRESEssionId Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfquery name="UPDATESURVEYSTATE" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplequeue.sessionire
                SET
                    DTSId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpDTSId#"/>
                WHERE
                    SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpIRESEssionId#">
            </cfquery>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 />
     		<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn LOCALOUTPUT>
		</cfcatch>
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>

	<cffunction name="strippedXMLSpecialCharacter" hint="">
		<cfargument name="inpString" required="true"/>

		<cfset  arguments.inpString = Replace( arguments.inpString, "&","&amp;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, ">","&gt;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, "<","&lt;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, "'","&apos;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, '"',"&quot;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, "?","&##63;","ALL") />
        <cfset  arguments.inpString = Replace( arguments.inpString, "&","&amp;","ALL") />
		<cfreturn arguments.inpString>
	</cffunction>


    <!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->
    <cffunction name="MoveQuestion" access="remote" output="false" hint="Update Control Point position by BatchID, QuestionId and new postion..">
		<cfargument name="INPBATCHID" TYPE="string" required="yes" />
		<cfargument name="INPPID" TYPE="string" required="yes" />
        <cfargument name="INPNEWPOS" TYPE="string" required="yes" />
        <cfargument name="STARTNUMBERQ" TYPE="string" required="no" default="1" hint="Allows for multiple pages - question position on page is related to how many questions into the survey that the STARTNUMBERQ is."/>

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<cfset var DebugStr = '' />
		<cfset var myxmldocResultDoc = '' />
		<cfset var RXSSQObj = '' />
		<cfset var CopyOfRXSSQObj = '' />
		<cfset var RXSSQs = '' />
		<cfset var i = '' />
		<cfset var item = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var GetBatchOptions = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var POSITION = INPNEWPOS />
		<cfset var Counter = 1 />
		<cfset var NewRXSSCSC	= '' />
		<cfset var Qs	= '' />
		<cfset var FinalDoc	= '' />
		<cfset var QElements	= '' />
		<cfset var Q2Item	= '' />
		<cfset var SelectedCP	= '' />
		<cfset var AttributeBuff	= '' />
		<cfset var QItem	= '' />
		<cfset var CopyOfRXSSQObjOutToDBXMLBuff = '' />


		<!---
			Positive is success
			1 = OK
			2 =
			3 =
			4 =
			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 =
			--->

        <cfset DebugStr = "Start">

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

        <cftry>

				<!--- Cleanup SQL injection --->
				<!--- Verify all numbers are actual numbers --->
				<!--- Clean up phone string --->
				<!--- Update list --->
				<!--- Read from DB Batch Options --->
				<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
					SELECT
	                	BatchId_bi,
	                    XMLControlString_vch
	                FROM
	                    simpleobjects.batch
	                WHERE
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	                AND
	                	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfquery>

				<!--- Parse for data --->
				<cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>") />
					<cfcatch TYPE="any">
						<!--- Squash bad data  --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
					</cfcatch>
				</cftry>

                <cfset RXSSQObj = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q[ @ID = #INPPID# ]") />

				<cfif ArrayLen(RXSSQObj) GT 0 >

                    <!--- Copy object from Old position --->
                    <cfset CopyOfRXSSQObj = Duplicate(RXSSQObj[1])>

                    <!--- Clean up XML for DB storage --->
					<cfset OutToDBXMLBuff = ToString(CopyOfRXSSQObj, "UTF-8") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
					<cfset CopyOfRXSSQObjOutToDBXMLBuff &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/>


                    <!--- Read all Qs --->
                	<cfset RXSSQs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//Q") />


                	<!---  Delete existing object from main XML all Qs --->
					<cfset XmlDeleteNodes(RXSSQs[1], RXSSQObj[1]) />


					<cfset Counter = 1 />

					<cfset NewRXSSCSC = "" />
					<cfset NewRXSSCSC &= "<RXSSCSC>#Chr(13)##Chr(10)#" />

					<!--- Insert First if first count --->
					<cfif POSITION LTE Counter>
						<cfset NewRXSSCSC &= "#CopyOfRXSSQObjOutToDBXMLBuff##Chr(13)##Chr(10)#"/>
					</cfif>

					<cfset Qs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC/Q") />

					<cfloop array="#Qs#" index="QItem">

						<cfset Counter = Counter + 1 />

					    <!--- Clean up XML for DB storage --->
						<cfset OutToDBXMLBuff = ToString(QItem, "UTF-8") />
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
						<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
						<cfset NewRXSSCSC &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/>

						<cfif POSITION EQ Counter>
							<cfset NewRXSSCSC &= "#CopyOfRXSSQObjOutToDBXMLBuff##Chr(13)##Chr(10)#"/>
						</cfif>

					</cfloop>

					<!--- Insert last if beyond current counter --->
					<cfif POSITION GT Counter>
						<cfset NewRXSSCSC &= "#CopyOfRXSSQObjOutToDBXMLBuff##Chr(13)##Chr(10)#"/>
					</cfif>

					<cfset NewRXSSCSC &= "</RXSSCSC>"/>


					<cfset dataout.NewRXSSCSC = NewRXSSCSC />

					<!--- Parse new XML --->
					<cfset myxmldocResultDoc = XmlParse('<?xml version="1.0" encoding="utf-8"?><XMLControlStringDoc>' & #NewRXSSCSC# & "</XMLControlStringDoc>") />

					 <!--- make sure to update all RQ POSITIONs --->
				    <cfset FinalDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

					<!--- Order (or Re-Order) all questions by RQ - starting point is at 1 --->
				    <cfif ArrayLen(FinalDoc) GT 0>

				        <cfset QElements = XmlSearch(FinalDoc[1], "//Q") />
				        <cfset i = 0>
				        <cfloop array="#QElements#" index="Q2Item">
				            <cfset i = i + 1>
				            <cfset Q2Item.XmlAttributes["RQ"] ="#i#"/>
				        </cfloop>
				    </cfif>

				</cfif>

				<!--- Clean up XML for DB storage --->
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc, "UTF-8") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />

				<!--- Set default return values --->
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />

				<!--- Save Local Query to DB --->
				<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.batch
					SET
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfquery>

                <cfinvoke method="AddHistory" component="#Session.SessionCFCPath#.history">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                    <cfinvokeargument name="XMLControlString_vch" value="#OutToDBXMLBuff#">
					<cfinvokeargument name="EVENT" value="Move Question #INPPID#">
				</cfinvoke>

                <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#">
					<cfinvokeargument name="moduleName" value="SMS #INPBATCHID#">
					<cfinvokeargument name="operator" value="Edit CP Flow">
				</cfinvoke>



		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="RunCheckBlackList" access="public" output="false" hint="Check if text contains black listed words">
		<cfargument name="inpText" TYPE="string" required="no" default="" hint="Text to search for blacklisted keyw rods or phrases"  />

		<!--- Default Return Structure --->
		<cfset var dataout = {} />
		<cfset var CheckBlackListDB = '' />
		<cfset var BlackWords_str = ''/>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.BLACKLISTWORD = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfset dataout.RXRESULTCODE = 1>

			<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

			<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
			<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.inpText ) ) />

			<!--- Look for and replace character higher than 255 ASCII -- DB wont store or allow regex check against unicode - not really needed so just squash for now --->
			<cfset arguments.inpText = UniSearchMatcher.replaceAll('') />

			<cfquery name ="CheckBlackListDB" datasource="#Session.DBSourceEBM#">
				SELECT
					WordId,
					Words_vch
				FROM
					simpleobjects.black_list_words
				Where
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LCase(inpText)#"> REGEXP  CONCAT('[[:<:]]', Words_vch, '[[:>:]]')
			</cfquery>

			<cfif CheckBlackListDB.RECORDCOUNT GT 0>
				<cfloop query = "CheckBlackListDB">
					<!--- <cfset BlackWords_str = Replace(BlackWords_str, ",@@@,"&CheckBlackListDB.Words_vch,"","ALL")>
					<cfset BlackWords_str = BlackWords_str &",@@@,"& CheckBlackListDB.Words_vch > --->
					<cfif ListFindNoCase(dataout.BLACKLISTWORD, TRIM(CheckBlackListDB.Words_vch) ) EQ 0>
						<cfset dataout.BLACKLISTWORD = listAppend(dataout.BLACKLISTWORD, CheckBlackListDB.Words_vch) />
					</cfif>
				</cfloop>

				<cfset dataout.RXRESULTCODE = 0>

			</cfif>

		<cfcatch TYPE="any">
			  <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

			  <cfif cfcatch.errorcode EQ "">
				  <cfset cfcatch.errorcode = -1 />
			  </cfif>

			  <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
			  <cfset dataout.TYPE = "#cfcatch.TYPE#">
			  <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			  <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

		</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

</cfcomponent>
