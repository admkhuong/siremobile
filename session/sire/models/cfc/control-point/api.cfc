<cfcomponent>

	<!---
		Better manage the UI for Control Points


		Coding Standard Reminder:
		Dont' use relative paths - you do not know where this output might be included

		Bad:
			<img src="../assets/layouts/layout4/img/avatar-men.png" />

		Good:
			<img src="/session/sire/assets/layouts/layout4/img/avatar-men.png" />

	--->


	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="remote" returntype="string" output="false" hint="Get the raw HTML for an API CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var SendAsUnicode	= '' />
		<cfset var HeaderName	= '' />

		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>


				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<div class="row APIContainer">
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
						<h4 class="portlet-heading-new">
							<span id="TDESC" style="" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>


						</h4>
						<div class="tguide hidden">#controlPoint.TGUIDE#</div>

						<!---

							<Q AF='NOFORMAT' API_ACT='JSON' API_DATA='sd;lafsaldjasjkd' API_DIR='someplace/something' API_DOM='somwhere.com' API_PORT='80' API_RA='STORE' API_TYPE='GET' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='1' TDESC='' TEXT='Reset' TYPE='API' errMsgTxt='undefined'></Q>

						--->


            			<!--- Set default controlPoint.API_TYPE --->
					 	<cfif TRIM(arguments.controlPoint.API_TYPE) EQ "" >
						 	<cfset arguments.controlPoint.API_TYPE = "GET" />
					 	</cfif>
            			<label for="API_TYPE" class="bc-title mbTight">REST API Verb</label>
						<select id="API_TYPE" class="Select2" data-width="100%">
                            <option value="GET" <cfif controlPoint.API_TYPE EQ "GET">selected="selected"</cfif>>GET</option>
							<option value="POST" <cfif controlPoint.API_TYPE EQ "POST">selected="selected"</cfif>>POST</option>
                        </select>


            			<div class="">
		                    <label class="bc-title mbTight">URL</label>
							<input id="API_DOM" value="#controlPoint.API_DOM#" class="form-control" maxlength="1000" style="width:100%;">
					 	</div>

					 	<!--- Legacy Field - everthing in DOM field going forward --->
						<input type="hidden" id="API_DIR" value="#controlPoint.API_DIR#">

					 	<div class="">
		                    <label class="bc-title mbTight">PORT</label>
							<input id="API_PORT" value="#controlPoint.API_PORT#" class="form-control" maxlength="1000" style="width:100%;">
					 	</div>

					 	<!--- Set default controlPoint.API_ACT --->
					 	<cfif TRIM(arguments.controlPoint.API_ACT) EQ "" >
						 	<cfset arguments.controlPoint.API_ACT = "TEXT" />
					 	</cfif>

						<div class="">
		                    <label class="bc-title mbTight">Headers (JSON list of variable pairs)</label>
							<textarea id="API_HEAD" value="#controlPoint.API_HEAD#" class="form-control" maxlength="1000" style="width:100%;" rows="3">#controlPoint.API_HEAD#</textarea>
					 	</div>

					 	<!--- Sample loop to get at the header data in the JSON
					 	<div>
						 	<label class="bc-title mbTight">Headers RAW</label>

							<cfif ISJSON(arguments.controlPoint.API_HEAD) AND TRIM(arguments.controlPoint.API_HEAD) NEQ "">
						        <cfset arguments.controlPoint.API_HEAD = deserializeJSON(arguments.controlPoint.API_HEAD) />
						    </cfif>

						    <cfif isStruct(arguments.controlPoint.API_HEAD)>

							 	<cfloop collection=#arguments.controlPoint.API_HEAD# item="HeaderName">
									 <!--- The Key --->
									 <cfdump var="#HeaderName#" />
									 <!--- The value --->
									 <cfdump var="#arguments.controlPoint.API_HEAD[HeaderName]#" />

								</cfloop>

							<cfelse>

								#controlPoint.API_HEAD#
						    </cfif>

					 	</div>
						--->

            			<label for="API_ACT" class="bc-title mbTight">Additional Content to Send Type</label>
						<select id="API_ACT" class="Select2">
                            <option value="TEXT" <cfif controlPoint.API_ACT EQ "TEXT">selected="selected"</cfif>>Plain Text</option>
                            <option value="JSON" <cfif controlPoint.API_ACT EQ "JSON">selected="selected"</cfif>>JSON</option>
                            <option value="XML" <cfif controlPoint.API_ACT EQ "XML">selected="selected"</cfif>>XML</option>
                        </select>

                    	<div class="">
		                    <label class="bc-title mbTight">Body - Additional Content To Send</label>
							<textarea id="API_DATA" value="#controlPoint.API_DATA#" class="form-control" maxlength="1000" style="width:100%;" rows="3">#controlPoint.API_DATA#</textarea>
					 	</div>

					 	<div class="">
		                    <label class="bc-title mbTight">Form Content To Post - JSON list of variable pairs - POST only</label>
							<textarea id="API_FORM" value="#controlPoint.API_FORM#" class="form-control" maxlength="1000" style="width:100%;" rows="3">#controlPoint.API_FORM#</textarea>
					 	</div>


					 	<div style="margin-top: 2em;">
						 	<!--- Set default controlPoint.API_TYPE --->
						 	<cfif TRIM(arguments.controlPoint.API_RA) EQ "" >
							 	<cfset arguments.controlPoint.API_RA = "SEND" />
						 	</cfif>
							<label for="API_RA" class="bc-title mbTight">Action with API Results</label>
							<select id="API_RA" class="Select2" data-width="100%">
	                            <option value="SEND" <cfif controlPoint.API_RA EQ "SEND">selected="selected"</cfif>>Send result data to user</option>
	                            <option value="STORE" <cfif controlPoint.API_RA EQ "STORE">selected="selected"</cfif>>Just Store Results</option>
	                        </select>
						</div>


					</div>

					<div class="col-lg-5 col-md-5 hidden-md hidden-sm hidden-xs">

						<img src="/public/sire/images/learning/hero-rest-md.png" class="img-responsive" style="float: right; opacity: .3;">

					</div>

				</div>

				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
				<div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp">Save</button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
		        </div>

			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

</cfcomponent>
