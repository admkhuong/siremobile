<!---
	*** Experimental *** 
	
	
	Centralized location for UI messages related to control point editor
	Instead of load a lot of uneeded constants - just load what is needed locally
	
	ToDo:
		Look at internationalizing the strings
	
--->
<!--- CHECK UNICODE REGEX --->
<cfset REGEX_UNICODE  = '[^A-Za-z0-9 \u00A0\r\n\t@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"$%&()~*+,\\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC\u2013\u2014õç`’´‘’′″“–‑−—一«»”！：•®οκ\[\]\/'&"'##]">
<cfset REGEX_DOUBLECHAR = ['^','{','}','\\','[','~',']','|','€'] />
	

<cfparam name="Session.UILanguage" default="ENGLISH"/> 


<cfswitch expression="#Session.UILanguage#">

	
	<!--- https://www.google.com/search?q=english+to+spanish&oq=english+to+spanish&aqs=chrome..69i57j0l5.6102j1j4&sourceid=chrome&ie=UTF-8 --->
	<cfcase value="SPANISH">
		<cfset _CPONESELECTAF=[]/>
		<cfset _CPONESELECTAF[1]=['NOFORMAT','No Formatting']/> 
		<cfset _CPONESELECTAF[2]=['NUMERIC','1 for ..., 2 for ..., 3 for... ']/>
		<cfset _CPONESELECTAF[3]=['NUMERICPAR','1) ..., 2) ..., 3) ... ']/>
		<cfset _CPONESELECTAF[4]=['ALPHA','A for ..., B for ..., C for ...']/>
		<cfset _CPONESELECTAF[5]=['ALPHAPAR','A) ..., B) ..., C) ...']/>
		<cfset _CPONESELECTAF[6]=['HIDDEN','Hidden']/>
		
				
		<cfset _CPINTEVALTYPE_1=[]>
		<cfset _CPINTEVALTYPE_1[1]=['SECONDS','Seconds(s)']/> 
		<cfset _CPINTEVALTYPE_1[2]=['MINUTES','Minute(s)']/> 
		<cfset _CPINTEVALTYPE_1[3]=['HOURS','Hour(s)']/> 
				
		<cfset _CPINTEVALTYPE_2=[]>
		<cfset _CPINTEVALTYPE_2[1]=['DAYS','Day(s)']/> 
		<cfset _CPINTEVALTYPE_2[2]=['WEEKS','Week(s)']/> 
		<cfset _CPINTEVALTYPE_2[3]=['MONTHS','Month(s)']/> 
		
		<cfset _IMRNR = []/>
		<cfset _IMRNR[1]=[0,'No Retry']/> 
		<cfset _IMRNR[2]=[1,'Retry 1 time']/> 
		<cfset _IMRNR[3]=[2,'Retry 2 times']/> 
		<cfset _IMRNR[4]=[3,'Retry 3 times']/>
		
		<cfset UnicodeCPWarning = 'Tienes caracteres Unicode en tu mensaje'/>
		<cfset UnicodePopupInfo = 'Cuando se haya detectado texto unicode en su mensaje, todos los caracteres contarán como 2 caracteres en lugar de 1. <a href="https://www.siremobile.com/blog/support/unicode-text-mean-detected-message/" target="_blank"> Aprende más. </a>'/>
	
		<!--- Start all language phrases names with UIL and then include a good description of what it is in the name - short stuff include all words 5 or less --->
		<cfscript>
			
			// SMS Section
			UIL_ShortCode = "Código corto";
			
			// Keyword Section
			UIL_Keyword = "Palabra clave";			
			UIL_ChooseAKeyword = "Elija una palabra clave";
			UIL_KeywordInfo = "Una palabra clave es un término o frase, exclusivo de su empresa, que se utiliza para activar una comunicación SMS entre usted y su cliente. Un ejemplo es si ve un anuncio que dice: Texto &##34;Café &##34; a 39492. El café es la palabra clave que un cliente usará para aceptar o comenzar una conversación de texto con usted.";
			UIL_KeywordInfoTip = "Haga que su palabra clave sea breve y memorable.";
			
			// Campaign Section
			UIL_CampaignName = "Nombre de campaña";
			
			// Subscriber List Section
			UIL_SubscriberListInfo = "Una Lista de suscriptores es una lista de personas que han optado por su campaña. Capturamos automáticamente todos los números de teléfono de sus suscriptores y los guardamos para que pueda enviarlos de manera rápida y fácil en el futuro. </ Br> </ br> Si esta es su primera campaña, puede crear su primera lista haciendo clic en &##34;Crear nueva lista&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Haga clic aquí </a> para aprender cómo ver y administrar sus listas.";
									
			// CDF Section
			UIL_CDFInfo = "Campaign Name";
			
		</cfscript>	
	
		<cfsavecontent variable="UIL_CDFInfo">
			<p> Herramientas para administrar sus datos personalizados. Su cuenta. Tu información. ¡Sin limites! </ p>
			<p> Utilizado dentro de una Acción como parte del flujo de una conversación, puede almacenar una respuesta de los usuarios a la pregunta anterior y vincularla al número de teléfono actual. </ p>
			<p> Las fortalezas únicas de su empresa son su ventaja competitiva. ¿Por qué debería comprometerse con las implementaciones de aplicaciones de vanilla u otros límites de proveedores? Con los campos de datos personalizados de Sire, usted no tiene demasiado. </ P>
			<p> Cualquier CDF en el flujo de mensajes se reemplazará por la información que envíe como parte de la llamada de API triggerSMS. Envíe estos valores CDF adicionales como parte de la cadena de consulta URL en solicitudes GET y POST o como parte de los datos JSON en las solicitudes POST. </ p>
			
			<ul>
			<li> <b> Personalización: </ b> personalice diferentes partes de cada mensaje para cada destinatario individual. </ li>
			<li> <b> Personalización: </ b> personaliza partes del mensaje para que se relacionen con el historial de transacciones de cada destinatario individual. Se debe poner en práctica el conocimiento que una empresa tiene sobre un cliente y se debe tener en cuenta la información que se tiene para poder darle al cliente exactamente lo que quiere </ li>
			<li> <b> Motor de reglas: </ b> puede cambiar el flujo de una conversación según la entrada capturada previamente. </ li>
			</ ul>
		</cfsavecontent>
		
	</cfcase>
	
	<cfdefaultcase>
		
		<cfset _CPONESELECTAF=[]/>
		<cfset _CPONESELECTAF[1]=['NOFORMAT','No Formatting']/> 
		<cfset _CPONESELECTAF[2]=['NUMERIC','1 for ..., 2 for ..., 3 for... ']/>
		<cfset _CPONESELECTAF[3]=['NUMERICPAR','1) ..., 2) ..., 3) ... ']/>
		<cfset _CPONESELECTAF[4]=['ALPHA','A for ..., B for ..., C for ...']/>
		<cfset _CPONESELECTAF[5]=['ALPHAPAR','A) ..., B) ..., C) ...']/>
		<cfset _CPONESELECTAF[6]=['HIDDEN','Hidden']/>		
				
		<cfset _CPINTEVALTYPE_1=[]>
		<cfset _CPINTEVALTYPE_1[1]=['SECONDS','Seconds(s)']/> 
		<cfset _CPINTEVALTYPE_1[2]=['MINUTES','Minute(s)']/> 
		<cfset _CPINTEVALTYPE_1[3]=['HOURS','Hour(s)']/> 		
		
		<cfset _CPINTEVALTYPE_2=[]>
		<cfset _CPINTEVALTYPE_2[1]=['DAYS','Day(s)']/> 
		<cfset _CPINTEVALTYPE_2[2]=['WEEKS','Week(s)']/> 
		<cfset _CPINTEVALTYPE_2[3]=['MONTHS','Month(s)']/> 
		
		<cfset _IMRNR = []/>
		<cfset _IMRNR[1]=[0,'No Retry']/> 
		<cfset _IMRNR[2]=[1,'Retry 1 time']/> 
		<cfset _IMRNR[3]=[2,'Retry 2 times']/> 
		<cfset _IMRNR[4]=[3,'Retry 3 times']/>
		
		<cfset UnicodeCPWarning = 'You have Unicode characters in your message'/>
		<cfset UnicodePopupInfo = 'When unicode text has been detected in your message, all characters will count as 2 characters instead of 1. <a href="https://www.siremobile.com/blog/support/unicode-text-mean-detected-message/" target="_blank"> Learn More. </a>'/>
	
		<!--- Start all language specific phrases with UIL and then include a good description of what it is in the name - short stuff include all words 5 or less --->
		<cfscript>
			
			// SMS Section
			UIL_ShortCode = "Short Code";
			
			// Keyword Section
			UIL_Keyword = "Keyword";
			UIL_ChooseAKeyword = "Choose a Keyword";
			UIL_KeywordInfo = "A Keyword is a term or phrase, unique to your business, which is used to trigger a SMS communication between you and your customer.  An example is if you see an ad that says:  Text &##34;Coffee&##34; to 39492.  Coffee is the keyword a customer will use to opt-in or begin a text conversation with you.";
			UIL_KeywordInfoTip = "Make your keyword short and memorable.";
						
			// Campaign Section
			UIL_CampaignName = "Campaign Name";
			
			// Subscriber List Section
			UIL_SubscriberListInfo = "A Subscriber List is a list of people who have opted in to your campaign.  We automatically capture all of your subscribers phone numbers and save them so you can quickly and easily send them messages in the future.</br></br>If this is your first campaign, you can create your first list by clicking &##34;Create New List&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Click Here</a> to learn how to view and manage your lists.";
			
			
		</cfscript>	
	
	
		<cfsavecontent variable="UIL_CDFInfo">
			<p>Tools to manage your custom data. Your account. Your data. No Limits! </p>
	      	<p>Used within an Action as part of the flow of a conversation, you can store a users response to the previous question and link it to the current phone number.</p>
	      	<p>Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With Sire's Custom Data Fields, you don't have too.</p>
	      	<p>Any CDFs in the message flow will be replaced with the data you send as part of the triggerSMS API call. Send these additional CDF values as part of the URL query string in GET and POST requests or as part of the JSON data in the POST requests. </p>
	      
		  	<ul>
			  	<li><b>Personalization:</b> Personalize different parts of each message for each individual recipient.</li>
			  	<li><b>Customization:</b> Customize parts of the message to relate to each individual recipient's transaction history. The knowledge that a company has about a customer needs to be put into practice and the information held has to be taken into account in order to be able to give the client exactly what he wants</li>
			  	<li><b>Rules Engine:</b> You can change the flow of a conversation based on previously captured input.</li>
		  	</ul>
		</cfsavecontent>
	
	
	</cfdefaultcase>

</cfswitch>