<cfcomponent>

	<!---
		Better manage the UI for Control Points


		Coding Standard Reminder:
		Dont' use relative paths - you do not know where this output might be included

		Bad:
			<img src="../assets/layouts/layout4/img/avatar-men.png" />

		Good:
			<img src="/session/sire/assets/layouts/layout4/img/avatar-men.png" />

	--->


	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="remote" returntype="string" output="false" hint="Get the raw HTML for a Interval CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var SendAsUnicode	= '' />
		<cfset var QUESTIONITEM	= '' />


		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>

				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<div class="row">
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
						<h4 class="portlet-heading-new">
							<span id="TDESC" style="" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>
						</h4>
						<div class="tguide hidden">#controlPoint.TGUIDE#</div>

						<div id="IntervalOptions" class="row IntervalContainer">

							<fieldset>

								<div id="IntervalSettingSection" class="col-sm-12 mb15" >

									<div class="mb15 ControlPointInterval" style="width:100%;">
										<a data-toggle="modal" href="##" data-target="##InfoModalCPInterval">What is this?</a>
									</div>

									<label for="ITYPE" class="bc-title mbTight">Select Interval Type:</label>
									<select id="ITYPE" class="Select2" data-width="100%">
										<option value="SECONDS" <cfif controlPoint.ITYPE EQ "SECONDS">selected="selected"</cfif>>Seconds(s)</option>
										<option value="MINUTES" <cfif controlPoint.ITYPE EQ "MINUTES">selected="selected"</cfif>>Minute(s)</option>
										<option value="HOURS" <cfif controlPoint.ITYPE EQ "HOURS">selected="selected"</cfif>>Hour(s)</option>
										<option value="DAYS" <cfif controlPoint.ITYPE EQ "DAYS">selected="selected"</cfif>>Day(s)</option>
										<option value="WEEKS" <cfif controlPoint.ITYPE EQ "WEEKS">selected="selected"</cfif>>Week(s)</option>
										<option value="MONTHS" <cfif controlPoint.ITYPE EQ "MONTHS">selected="selected"</cfif>>Month(s)</option>
										<option value="TODAY" <cfif controlPoint.ITYPE EQ "TODAY">selected="selected"</cfif>>Today at xx:xx</option>
										<option value="WEEKDAY" <cfif controlPoint.ITYPE EQ "WEEKDAY">selected="selected"</cfif>>Weekday(s) M-F</option>
										<option value="DATE" <cfif controlPoint.ITYPE EQ "DATE">selected="selected"</cfif>>Specific Date</option>
										<option value="SUN" <cfif controlPoint.ITYPE EQ "SUN">selected="selected"</cfif>>Sunday</option>
										<option value="MON" <cfif controlPoint.ITYPE EQ "MON">selected="selected"</cfif>>Monday</option>
										<option value="TUE" <cfif controlPoint.ITYPE EQ "TUE">selected="selected"</cfif>>Tuesday</option>
										<option value="WED" <cfif controlPoint.ITYPE EQ "WED">selected="selected"</cfif>>Wednesday</option>
										<option value="THU" <cfif controlPoint.ITYPE EQ "THU">selected="selected"</cfif>>Thursday</option>
										<option value="FRI" <cfif controlPoint.ITYPE EQ "FRI">selected="selected"</cfif>>Friday</option>
										<option value="SAT" <cfif controlPoint.ITYPE EQ "SAT">selected="selected"</cfif>>Saturday</option>
									</select>

									<div id="IVALUE_Container">
										<label for="IVALUE" class="bc-title mbTight" id="IVALUE_Label">How many:</label>
			                            <select id="IVALUE" class="Select2" data-width="100%">
			                                <option value="0" <cfif controlPoint.IVALUE EQ "0">selected="selected"</cfif>>00</option>
		                                    <option value="1" <cfif controlPoint.IVALUE EQ "1">selected="selected"</cfif>>01</option>
		                                    <option value="2" <cfif controlPoint.IVALUE EQ "2">selected="selected"</cfif>>02</option>
		                                    <option value="3" <cfif controlPoint.IVALUE EQ "3">selected="selected"</cfif>>03</option>
		                                    <option value="4" <cfif controlPoint.IVALUE EQ "4">selected="selected"</cfif>>04</option>
		                                    <option value="5" <cfif controlPoint.IVALUE EQ "5">selected="selected"</cfif>>05</option>
		                                    <option value="6" <cfif controlPoint.IVALUE EQ "6">selected="selected"</cfif>>06</option>
		                                    <option value="7" <cfif controlPoint.IVALUE EQ "7">selected="selected"</cfif>>07</option>
		                                    <option value="8" <cfif controlPoint.IVALUE EQ "8">selected="selected"</cfif>>08</option>
		                                    <option value="9" <cfif controlPoint.IVALUE EQ "9">selected="selected"</cfif>>09</option>
		                                    <option value="10" <cfif controlPoint.IVALUE EQ "10">selected="selected"</cfif>>10</option>
		                                    <option value="11" <cfif controlPoint.IVALUE EQ "11">selected="selected"</cfif>>11</option>
		                                    <option value="12" <cfif controlPoint.IVALUE EQ "12">selected="selected"</cfif>>12</option>
		                                    <option value="13" <cfif controlPoint.IVALUE EQ "13">selected="selected"</cfif>>13</option>
		                                    <option value="14" <cfif controlPoint.IVALUE EQ "14">selected="selected"</cfif>>14</option>
		                                    <option value="15" <cfif controlPoint.IVALUE EQ "15">selected="selected"</cfif>>15</option>
		                                    <option value="16" <cfif controlPoint.IVALUE EQ "16">selected="selected"</cfif>>16</option>
		                                    <option value="17" <cfif controlPoint.IVALUE EQ "17">selected="selected"</cfif>>17</option>
		                                    <option value="18" <cfif controlPoint.IVALUE EQ "18">selected="selected"</cfif>>18</option>
		                                    <option value="19" <cfif controlPoint.IVALUE EQ "19">selected="selected"</cfif>>19</option>
		                                    <option value="20" <cfif controlPoint.IVALUE EQ "20">selected="selected"</cfif>>20</option>
		                                    <option value="21" <cfif controlPoint.IVALUE EQ "21">selected="selected"</cfif>>21</option>
		                                    <option value="22" <cfif controlPoint.IVALUE EQ "22">selected="selected"</cfif>>22</option>
		                                    <option value="23" <cfif controlPoint.IVALUE EQ "23">selected="selected"</cfif>>23</option>
		                                    <option value="24" <cfif controlPoint.IVALUE EQ "24">selected="selected"</cfif>>24</option>
		                                    <option value="25" <cfif controlPoint.IVALUE EQ "25">selected="selected"</cfif>>25</option>
		                                    <option value="26" <cfif controlPoint.IVALUE EQ "26">selected="selected"</cfif>>26</option>
		                                    <option value="27" <cfif controlPoint.IVALUE EQ "27">selected="selected"</cfif>>27</option>
		                                    <option value="28" <cfif controlPoint.IVALUE EQ "28">selected="selected"</cfif>>28</option>
		                                    <option value="29" <cfif controlPoint.IVALUE EQ "29">selected="selected"</cfif>>29</option>
		                                    <option value="30" <cfif controlPoint.IVALUE EQ "30">selected="selected"</cfif>>30</option>
		                                    <option value="31" <cfif controlPoint.IVALUE EQ "31">selected="selected"</cfif>>31</option>
		                                    <option value="32" <cfif controlPoint.IVALUE EQ "32">selected="selected"</cfif>>32</option>
		                                    <option value="33" <cfif controlPoint.IVALUE EQ "33">selected="selected"</cfif>>33</option>
		                                    <option value="34" <cfif controlPoint.IVALUE EQ "34">selected="selected"</cfif>>34</option>
		                                    <option value="35" <cfif controlPoint.IVALUE EQ "35">selected="selected"</cfif>>35</option>
		                                    <option value="36" <cfif controlPoint.IVALUE EQ "36">selected="selected"</cfif>>36</option>
		                                    <option value="37" <cfif controlPoint.IVALUE EQ "37">selected="selected"</cfif>>37</option>
		                                    <option value="38" <cfif controlPoint.IVALUE EQ "38">selected="selected"</cfif>>38</option>
		                                    <option value="39" <cfif controlPoint.IVALUE EQ "39">selected="selected"</cfif>>39</option>
		                                    <option value="40" <cfif controlPoint.IVALUE EQ "40">selected="selected"</cfif>>40</option>
		                                    <option value="41" <cfif controlPoint.IVALUE EQ "41">selected="selected"</cfif>>41</option>
		                                    <option value="42" <cfif controlPoint.IVALUE EQ "42">selected="selected"</cfif>>42</option>
		                                    <option value="43" <cfif controlPoint.IVALUE EQ "43">selected="selected"</cfif>>43</option>
		                                    <option value="44" <cfif controlPoint.IVALUE EQ "44">selected="selected"</cfif>>44</option>
		                                    <option value="45" <cfif controlPoint.IVALUE EQ "45">selected="selected"</cfif>>45</option>
		                                    <option value="46" <cfif controlPoint.IVALUE EQ "46">selected="selected"</cfif>>46</option>
		                                    <option value="47" <cfif controlPoint.IVALUE EQ "47">selected="selected"</cfif>>47</option>
		                                    <option value="48" <cfif controlPoint.IVALUE EQ "48">selected="selected"</cfif>>48</option>
		                                    <option value="49" <cfif controlPoint.IVALUE EQ "49">selected="selected"</cfif>>49</option>
		                                    <option value="50" <cfif controlPoint.IVALUE EQ "50">selected="selected"</cfif>>50</option>
		                                    <option value="51" <cfif controlPoint.IVALUE EQ "51">selected="selected"</cfif>>51</option>
		                                    <option value="52" <cfif controlPoint.IVALUE EQ "52">selected="selected"</cfif>>52</option>
		                                    <option value="53" <cfif controlPoint.IVALUE EQ "53">selected="selected"</cfif>>53</option>
		                                    <option value="54" <cfif controlPoint.IVALUE EQ "54">selected="selected"</cfif>>54</option>
		                                    <option value="55" <cfif controlPoint.IVALUE EQ "55">selected="selected"</cfif>>55</option>
		                                    <option value="56" <cfif controlPoint.IVALUE EQ "56">selected="selected"</cfif>>56</option>
		                                    <option value="57" <cfif controlPoint.IVALUE EQ "57">selected="selected"</cfif>>57</option>
		                                    <option value="58" <cfif controlPoint.IVALUE EQ "58">selected="selected"</cfif>>58</option>
		                                    <option value="59" <cfif controlPoint.IVALUE EQ "59">selected="selected"</cfif>>59</option>
		                                    <option value="60" <cfif controlPoint.IVALUE EQ "60">selected="selected"</cfif>>60</option>
			                            </select>
		                        	</div>


		                        	<div id="IntervalTimeContainer">

				                    	<label class="bc-title mbTight">Interval Timeout Time: Time zone Specific to Phone Number</label>

				                        <div style="width: 100%;">
					                        <div style="display: inline-block; clear: both;">
				                                <select id="IHOUR" class="Select2" style="width:5em;" data-width="100%">
				                                    <option value="0" <cfif controlPoint.IHOUR EQ "0">selected="selected"</cfif>>Hour</option>
				                                    <option value="00" <cfif controlPoint.IHOUR EQ "00">selected="selected"</cfif>>12</option>
				                                    <option value="01" <cfif controlPoint.IHOUR EQ "01">selected="selected"</cfif>>01</option>
				                                    <option value="02" <cfif controlPoint.IHOUR EQ "02">selected="selected"</cfif>>02</option>
				                                    <option value="03" <cfif controlPoint.IHOUR EQ "03">selected="selected"</cfif>>03</option>
				                                    <option value="04" <cfif controlPoint.IHOUR EQ "04">selected="selected"</cfif>>04</option>
				                                    <option value="05" <cfif controlPoint.IHOUR EQ "05">selected="selected"</cfif>>05</option>
				                                    <option value="06" <cfif controlPoint.IHOUR EQ "06">selected="selected"</cfif>>06</option>
				                                    <option value="07" <cfif controlPoint.IHOUR EQ "07">selected="selected"</cfif>>07</option>
				                                    <option value="08" <cfif controlPoint.IHOUR EQ "08">selected="selected"</cfif>>08</option>
				                                    <option value="09" <cfif controlPoint.IHOUR EQ "09">selected="selected"</cfif>>09</option>
				                                    <option value="10" <cfif controlPoint.IHOUR EQ "10">selected="selected"</cfif>>10</option>
				                                    <option value="11" <cfif controlPoint.IHOUR EQ "11">selected="selected"</cfif>>11</option>
				                                </select>
					                        </div>

				                            <div style="display: inline-block; clear: both;">
					                                <select id="IMIN" class="Select2" style="width:5em;" data-width="100%">
				                                    <option value="0" <cfif controlPoint.IMIN EQ "0">selected="selected"</cfif>>Min</option>
				                                    <option value="00" <cfif controlPoint.IMIN EQ "00">selected="selected"</cfif>>00</option>
				                                    <option value="01" <cfif controlPoint.IMIN EQ "01">selected="selected"</cfif>>01</option>
				                                    <option value="02" <cfif controlPoint.IMIN EQ "02">selected="selected"</cfif>>02</option>
				                                    <option value="03" <cfif controlPoint.IMIN EQ "03">selected="selected"</cfif>>03</option>
				                                    <option value="04" <cfif controlPoint.IMIN EQ "04">selected="selected"</cfif>>04</option>
				                                    <option value="05" <cfif controlPoint.IMIN EQ "05">selected="selected"</cfif>>05</option>
				                                    <option value="06" <cfif controlPoint.IMIN EQ "06">selected="selected"</cfif>>06</option>
				                                    <option value="07" <cfif controlPoint.IMIN EQ "07">selected="selected"</cfif>>07</option>
				                                    <option value="08" <cfif controlPoint.IMIN EQ "08">selected="selected"</cfif>>08</option>
				                                    <option value="09" <cfif controlPoint.IMIN EQ "09">selected="selected"</cfif>>09</option>
				                                    <option value="10" <cfif controlPoint.IMIN EQ "10">selected="selected"</cfif>>10</option>
				                                    <option value="11" <cfif controlPoint.IMIN EQ "11">selected="selected"</cfif>>11</option>
				                                    <option value="12" <cfif controlPoint.IMIN EQ "12">selected="selected"</cfif>>12</option>
				                                    <option value="13" <cfif controlPoint.IMIN EQ "13">selected="selected"</cfif>>13</option>
				                                    <option value="14" <cfif controlPoint.IMIN EQ "14">selected="selected"</cfif>>14</option>
				                                    <option value="15" <cfif controlPoint.IMIN EQ "15">selected="selected"</cfif>>15</option>
				                                    <option value="16" <cfif controlPoint.IMIN EQ "16">selected="selected"</cfif>>16</option>
				                                    <option value="17" <cfif controlPoint.IMIN EQ "17">selected="selected"</cfif>>17</option>
				                                    <option value="18" <cfif controlPoint.IMIN EQ "18">selected="selected"</cfif>>18</option>
				                                    <option value="19" <cfif controlPoint.IMIN EQ "19">selected="selected"</cfif>>19</option>
				                                    <option value="20" <cfif controlPoint.IMIN EQ "20">selected="selected"</cfif>>20</option>
				                                    <option value="21" <cfif controlPoint.IMIN EQ "21">selected="selected"</cfif>>21</option>
				                                    <option value="22" <cfif controlPoint.IMIN EQ "22">selected="selected"</cfif>>22</option>
				                                    <option value="23" <cfif controlPoint.IMIN EQ "23">selected="selected"</cfif>>23</option>
				                                    <option value="24" <cfif controlPoint.IMIN EQ "24">selected="selected"</cfif>>24</option>
				                                    <option value="25" <cfif controlPoint.IMIN EQ "25">selected="selected"</cfif>>25</option>
				                                    <option value="26" <cfif controlPoint.IMIN EQ "26">selected="selected"</cfif>>26</option>
				                                    <option value="27" <cfif controlPoint.IMIN EQ "27">selected="selected"</cfif>>27</option>
				                                    <option value="28" <cfif controlPoint.IMIN EQ "28">selected="selected"</cfif>>28</option>
				                                    <option value="29" <cfif controlPoint.IMIN EQ "29">selected="selected"</cfif>>29</option>
				                                    <option value="30" <cfif controlPoint.IMIN EQ "30">selected="selected"</cfif>>30</option>
				                                    <option value="31" <cfif controlPoint.IMIN EQ "31">selected="selected"</cfif>>31</option>
				                                    <option value="32" <cfif controlPoint.IMIN EQ "32">selected="selected"</cfif>>32</option>
				                                    <option value="33" <cfif controlPoint.IMIN EQ "33">selected="selected"</cfif>>33</option>
				                                    <option value="34" <cfif controlPoint.IMIN EQ "34">selected="selected"</cfif>>34</option>
				                                    <option value="35" <cfif controlPoint.IMIN EQ "35">selected="selected"</cfif>>35</option>
				                                    <option value="36" <cfif controlPoint.IMIN EQ "36">selected="selected"</cfif>>36</option>
				                                    <option value="37" <cfif controlPoint.IMIN EQ "37">selected="selected"</cfif>>37</option>
				                                    <option value="38" <cfif controlPoint.IMIN EQ "38">selected="selected"</cfif>>38</option>
				                                    <option value="39" <cfif controlPoint.IMIN EQ "39">selected="selected"</cfif>>39</option>
				                                    <option value="40" <cfif controlPoint.IMIN EQ "40">selected="selected"</cfif>>40</option>
				                                    <option value="41" <cfif controlPoint.IMIN EQ "41">selected="selected"</cfif>>41</option>
				                                    <option value="42" <cfif controlPoint.IMIN EQ "42">selected="selected"</cfif>>42</option>
				                                    <option value="43" <cfif controlPoint.IMIN EQ "43">selected="selected"</cfif>>43</option>
				                                    <option value="44" <cfif controlPoint.IMIN EQ "44">selected="selected"</cfif>>44</option>
				                                    <option value="45" <cfif controlPoint.IMIN EQ "45">selected="selected"</cfif>>45</option>
				                                    <option value="46" <cfif controlPoint.IMIN EQ "46">selected="selected"</cfif>>46</option>
				                                    <option value="47" <cfif controlPoint.IMIN EQ "47">selected="selected"</cfif>>47</option>
				                                    <option value="48" <cfif controlPoint.IMIN EQ "48">selected="selected"</cfif>>48</option>
				                                    <option value="49" <cfif controlPoint.IMIN EQ "49">selected="selected"</cfif>>49</option>
				                                    <option value="50" <cfif controlPoint.IMIN EQ "50">selected="selected"</cfif>>50</option>
				                                    <option value="51" <cfif controlPoint.IMIN EQ "51">selected="selected"</cfif>>51</option>
				                                    <option value="52" <cfif controlPoint.IMIN EQ "52">selected="selected"</cfif>>52</option>
				                                    <option value="53" <cfif controlPoint.IMIN EQ "53">selected="selected"</cfif>>53</option>
				                                    <option value="54" <cfif controlPoint.IMIN EQ "54">selected="selected"</cfif>>54</option>
				                                    <option value="55" <cfif controlPoint.IMIN EQ "55">selected="selected"</cfif>>55</option>
				                                    <option value="56" <cfif controlPoint.IMIN EQ "56">selected="selected"</cfif>>56</option>
				                                    <option value="57" <cfif controlPoint.IMIN EQ "57">selected="selected"</cfif>>57</option>
				                                    <option value="58" <cfif controlPoint.IMIN EQ "58">selected="selected"</cfif>>58</option>
				                                    <option value="59" <cfif controlPoint.IMIN EQ "59">selected="selected"</cfif>>59</option>
				                                </select>
				                            </div>

				                            <div style="display: inline-block; clear: both;">
				                                <select id="INOON" class="Select2" style="width:5em;" data-width="100%">
				                                    <option value="01" <cfif controlPoint.INOON EQ "01">selected="selected"</cfif>>PM</option>
				                                    <option value="00" <cfif controlPoint.INOON EQ "00">selected="selected"</cfif>>AM</option>
				                                </select>
				                            </div>

				                        </div>

				                    </div>

									<label class="bc-title mbTight">Interval Expired: Next Action</label>

		                            <select id="IENQID" class="Select2 StepQuestionList" data-width="100%">

										<cfif ARRAYLEN(inpCPList.QUESTIONS) GT 0>

											<cfloop array="#inpCPList.QUESTIONS#" index="QUESTIONITEM">

												<!--- Display different CP types differently in the list ListContainsNoCase('"ONESELECTION","SHORTANSWER","API"', "QQQ")--->
												<cfif QUESTIONITEM.RQ EQ 0  >
													<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>#QUESTIONITEM.TEXT#</option>
												<cfelse>

													<cfif LEN(TRIM(QUESTIONITEM.TEXT)) EQ 0>

														<cfif LEN(TRIM(QUESTIONITEM.TDESC)) EQ 0>
															<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TYPE#</option>
														<cfelse>
															<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TDESC#</option>
														</cfif>

													<cfelse>
														<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TEXT#</option>
													</cfif>

												</cfif>

											</cfloop>

										<cfelse>
											<!--- Handle case where there is no data yet --->
											<option value="0" selected>-- Loading Data ... --</option>
										</cfif>

		                    		</select>

		                    		<!--- These only make sense for questions so hide them for plain CP Intervals --->
		                    		<input type="hidden" id="IMRNR" value="#controlPoint.IMRNR#">
		                    		<input type="hidden" id="INRMO" value="#controlPoint.INRMO#">
<!---

		                        	<label class="bc-title mbTight">Retry on No Response Option</label>
									<select id="IMRNR" class="Select2" data-width="100%">
		                                    <option value="0" <cfif controlPoint.IMRNR EQ "0">selected="selected"</cfif>>No Retry</option>
		                                    <option value="1" <cfif controlPoint.IMRNR EQ "1">selected="selected"</cfif>>Retry 1 time</option>
		                                    <option value="2" <cfif controlPoint.IMRNR EQ "2">selected="selected"</cfif>>Retry 2 times</option>
		                                    <option value="3" <cfif controlPoint.IMRNR EQ "3">selected="selected"</cfif>>Retry 3 times</option>
		                            </select>

		                            <label class="bc-title mbTight">Still No Response After Maximum Retries Option</label>
									<select id="INRMO" class="Select2" data-width="100%">
		                                <option value="END" <cfif controlPoint.INRMO EQ "END">selected="selected"</cfif>>End Conversation</option>
		                                <option value="NEXT" <cfif controlPoint.INRMO EQ "NEXT">selected="selected"</cfif>>Proceed to Next Action</option>
		                            </select>
--->

								</div>
							</fieldset>


						</div>


					</div>

					<div class="col-lg-5 col-md-5 hidden-md hidden-sm hidden-xs" style="min-height: 100px; text-align: right;">

						<!--- https://loading.io/spinner/custom/164047/ --->
						<svg width="50%"  height="50%"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-lds-clock" style="background: none;"><g transform="translate(50 50)"><g ng-attr-transform="scale({{config.scale}})" transform="scale(0.8)"><g transform="translate(-50 -50)"><path ng-attr-fill="{{config.c1}}" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-width="{{config.width}}" d="M50,14c19.85,0,36,16.15,36,36S69.85,86,50,86S14,69.85,14,50S30.15,14,50,14 M50,10c-22.091,0-40,17.909-40,40 s17.909,40,40,40s40-17.909,40-40S72.091,10,50,10L50,10z" fill="##274e60" stroke="##274e60" stroke-width="0"></path><path ng-attr-fill="{{config.c3}}" d="M52.78,42.506c-0.247-0.092-0.415-0.329-0.428-0.603L52.269,40l-0.931-21.225C51.304,18.06,50.716,17.5,50,17.5 s-1.303,0.56-1.338,1.277L47.731,40l-0.083,1.901c-0.013,0.276-0.181,0.513-0.428,0.604c-0.075,0.028-0.146,0.063-0.22,0.093V44h6 v-1.392C52.925,42.577,52.857,42.535,52.78,42.506z" fill="##dedede" transform="rotate(330.107 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="2.5s" begin="0s" repeatCount="indefinite"></animateTransform></path><path ng-attr-fill="{{config.c2}}" d="M58.001,48.362c-0.634-3.244-3.251-5.812-6.514-6.391c-3.846-0.681-7.565,1.35-9.034,4.941 c-0.176,0.432-0.564,0.717-1.013,0.744l-15.149,0.97c-0.72,0.043-1.285,0.642-1.285,1.383c0,0.722,0.564,1.321,1.283,1.363 l15.153,0.971c0.447,0.027,0.834,0.312,1.011,0.744c1.261,3.081,4.223,5.073,7.547,5.073c2.447,0,4.744-1.084,6.301-2.975 C57.858,53.296,58.478,50.808,58.001,48.362z M50,53.06c-1.688,0-3.06-1.373-3.06-3.06s1.373-3.06,3.06-3.06s3.06,1.373,3.06,3.06 S51.688,53.06,50,53.06z" fill="##4a798f" transform="rotate(352.527 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="10s" begin="0s" repeatCount="indefinite"></animateTransform></path></g></g></g></svg>

					</div>
				</div>

				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
				<div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp">Save</button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
		        </div>

			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

</cfcomponent>
