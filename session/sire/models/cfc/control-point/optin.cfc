<cfcomponent>

	<!---
		Better manage the UI for Control Points


		Coding Standard Reminder:
		Dont' use relative paths - you do not know where this output might be included

		Bad:
			<img src="../assets/layouts/layout4/img/avatar-men.png" />

		Good:
			<img src="/session/sire/assets/layouts/layout4/img/avatar-men.png" />

	--->


	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="remote" returntype="string" output="false" hint="Get the raw HTML for a Opt In CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var SendAsUnicode	= '' />


		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>

				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<div class="row OptInContainer">
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
						<h4 class="portlet-heading-new">
							<span id="TDESC" style="" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>
						</h4>
						<div class="tguide hidden">#controlPoint.TGUIDE#</div>

						<!--- Use this hidden field to pass in default - to be used when populating select box --->
						<input type="hidden" id="OPTIN_OIG" value="#controlPoint.OIG#">


						<!--- BEGIN : SELECT SUBCRIBER LIST --->

	                    <h4 class="portlet-heading-new">Choose a Subscriber List <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="#UIL_SubscriberListInfo#"><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>

	                    <div class="row">
	                        <div class="col-md-5 col-xs-12">
	                            <div class="form-gd form-lb-large">
	                                <div class="form-group">
	                                    <select class="form-control Select2" id="OIG">

	                                    </select>
	                                </div>
	                            </div>
	                        </div>

	                        <div class="col-md-2 col-xs-12">
	                            <a href="javascript:;" id="btn-add-group" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
	                        </div>
	                    </div>

			            <!--- END : SELECT SUBCRIBER LIST --->


					</div>
					<div class="col-lg-5 col-md-5 hidden-md hidden-sm hidden-xs">

						<div style="float: right; opacity: .3; text-align: center; margin-right: 9em;">

							<h1>Join</h1>
							<div>the</div>
							<h1>List</h1>

						</div>

					</div>
				</div>

				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
				<div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp">Save</button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
		        </div>

			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

</cfcomponent>
