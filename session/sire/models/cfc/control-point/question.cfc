<cfcomponent>

	<!---
		Better manage the UI for Control Points



	--->


	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    	<cfparam name="Session.loggedIn" default="0"/>
    	<cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="public" returntype="string" output="false" hint="Get the raw HTML for a Question CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>
		<cfargument name="inpCPList" type="struct" required="false" hint="Read all the CP items once so they can be used for selection boxes without having to re-query for each control point"/>
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0"/>
		<cfargument name="inpAFE" type="any" required="false" default="0" hint="Advanced Flow Editor Options"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var CIIndex = 0>
		<cfset var SendAsUnicode	= '' />

		<cfset var CONDITIONITEM = '' />
		<cfset var QUESTIONITEM = '' />
		<cfset var RetVarReadXMLQuestions = '' />
		<cfset var afSeclected	= '' />
		<cfset var seletedIType	= '' />
		<cfset var seletedIValue	= '' />
		<cfset var seletedIMRNR	= '' />
		<cfset var iAF	= '' />
		<cfset var i	= '' />
		<cfset var iTRN	= '' />
		<cfset var iType	= '' />
		<cfset var iValue	= '' />
		<cfset var iMRNR	= '' />


		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>



				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

                <div class="form-gd form-lb-large">
                    <div class="form-group">
                        <div class="col-xs-8">

							<span id="TDESC" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>

                       	</div>
                    </div>
                </div>

				<div class="cp-content QuestionContainer">
					<div class="row">
						<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">

								<input type="hidden" id="ANSTEMP" value="#controlPoint.ANSTEMP#" />

								<h4 class="portlet-subheading">

								<!--- <span class="tdesc">#controlPoint.TDESC#</span>
								<cfif trim(#controlPoint.TDESC#) NEQ ''>
									<span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right"  data-html="true"   title="" data-content='#controlPoint.TGUIDE#'><img  src='../assets/layouts/layout4/img/info.png '/></a></span>
								</cfif> --->
							</h4>
							<div class="tguide hidden">#controlPoint.TGUIDE#</div>
							<div class="form-gd">
								<div class="xform-group">
									<textarea data-emojiable="true" maxLength="459" name="CPText" id="CPText" data-control="text-message" cols="30" rows="5" class="form-control text validate[required] ">#controlPoint.TEXT#</textarea>
									<div class="row">
										<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
											<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
												Character Count {#len(controlPoint.TEXT)#}
											</h5>
											<span>
												<a tabindex="0" class="info-popover pull-right see-unicode-mess  <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#UnicodePopupInfo#'><img  src='../assets/layouts/layout4/img/info.png '/></a>
												<span class="see-unicode-mess see-unicode-mess-edit pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">#UnicodeCPWarning# &nbsp</span>
											</span>
										</div>
									</div>

								</div>

								<cfif inpAFE EQ 1>

									<!-- Rounded switch -->
									<label class="switch">
									  <input id="RTF" class="RTF" type="checkbox" <cfif controlPoint.RTF GT 0>checked</cfif> >
									  <span class="slider round"></span>

									  <span class="slabel">Random</span>

									</label>
									<!--- <span class="random">Random</span> --->



									<div class="wrapper-random-text" <cfif controlPoint.RTF EQ 0>style="display:none;"</cfif>>
										<div class="form-gd">

											<div class="rtext_list">
												<cfif arrayLen(controlPoint.RTEXT) GT 0>
													<cfloop array="#controlPoint.RTEXT#" index="i">
														<div class="RandomTextArrayItem row row-small">
															<div class="col-xs-12">
																<div class="form-group">

																	<input type="hidden" id="TID" class="form-control readonly-val text-center" value="#i['TID']#" />
																	<input type="hidden" id="T64" class="form-control readonly-val text-center" value="#i['T64']#" />

																	<span  class="RandomInput">
																		<span style="display:block; overflow:hidden;"><input data-emojiable="true" data-type="input" type="text" value="#i['TEXT']#" class="form-control RTEXT" /></span>

																		<!--- Keep the display clean - hover delete option --->
																		<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-text hover-delete-text" aria-hidden="true"></span>

																		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
																	   <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", i['TEXT'] ) ) />

																		<!--- Look for character higher than 255 ASCII --->
																	   <cfif UniSearchMatcher.Find() >

																		  <cfset SendAsUnicode = 1 />
																		  <cfset MaxPerMessage = 66 />
																		  <cfset UpperCharacterLimit = 70 />

																	   <cfelse>

																		  <cfset SendAsUnicode = 0 />
																		  <cfset MaxPerMessage = 153 />
																		  <cfset UpperCharacterLimit = 160 />

																	   </cfif>

																		<div class="col-xs-12" style="margin-bottom: 1em;">
																			<h5 id="cp-char-count-#i['TID']#" class="text-right control-point-char text-color-gray">
																				Character Count {#len(i['TEXT'])#}
																			</h5>
																			<span>
																				<a <cfif SendAsUnicode NEQ 1>style="display:none;"</cfif> tabindex="0" class="info-popover pull-right see-unicode-mess " data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#UnicodePopupInfo#'><img  src='/session/sire/assets/layouts/layout4/img/info.png '/></a>
																				<span <cfif SendAsUnicode NEQ 1>style="display:none;"</cfif> class="see-unicode-mess see-unicode-mess-edit pull-right ">#UnicodeCPWarning# &nbsp</span>
																			</span>
																		</div>
																	</span>
																</div>

															</div>

														</div>
													</cfloop>
												</cfif>

											</div>
										</div>

										<div class="row row-small" style="margin-bottom: 2em;">
											<div class="col-xs-12 new-response add-more-text" id="add-more-text">
												<span class="response-plus">+</span> Add Text
											</div>
										</div>
									</div>
								<cfelse>

									<!--- keep options - even if they are hidden --->
									<div class="rtext_list">
										<cfif arrayLen(controlPoint.RTEXT) GT 0>
											<cfloop array="#controlPoint.RTEXT#" index="i">
												<div class="RandomTextArrayItem row row-small">
													<div class="col-xs-12">
														<div class="form-group">

															<input type="hidden" id="TID" class="form-control readonly-val text-center" value="#i['TID']#" />
															<input type="hidden" id="T64" class="form-control readonly-val text-center" value="#i['T64']#" />


															<span  class="RandomInput">

																<span style="display:block; overflow:hidden;"><input id="RTEXT" type="hidden" value="#i['TEXT']#" class="form-control RTEXT" /></span>

																<!--- Keep the display clean - hover delete option --->
																<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-text hover-delete-text" aria-hidden="true"></span>

															</span>


														</div>
													</div>

												</div>
											</cfloop>
										</cfif>

									</div>

								</cfif>

							</div>



							<div class="toggle-answer-intents">Response Flow <i class="fa fa-angle-down"></i></div>

							<!--- new logic - everthing is hidden - up to user to type answer prompts in message now --->
							<input type="hidden" id="AF" class="form-control readonly-val text-center" value="HIDDEN" />


							<div class="wrapper-answer-text">
								<div class="form-gd field-anser-holder">


									<div class="answers_list">
										<cfif arrayLen(controlPoint.Options) GT 0>
											<cfloop array="#controlPoint.Options#" index="i">
												<div class="AnswerItem row row-small">
													<div class="col-xs-12">
														<div class="form-group AnswerItemInput">

															<i class="fa fa-angle-down toggle-ai-opt"></i>

															<span class="AVALREGLINK" id="AVALREGLINK">

																<i class="fas fa-bars"></i>


																<input type="hidden" id="AVAL" class="form-control readonly-val text-center" value="#i['AVAL']#" />
																<input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="#i['AVALREG']#" />
																<input type="hidden" id="AVALRESP" class="form-control readonly-val text-center" value="#i['AVALRESP']#" />
																<input type="hidden" id="AVALREDIR" class="form-control readonly-val text-center" value="#i['AVALREDIR']#" />
																<input type="hidden" id="AVALNEXT" class="form-control readonly-val text-center" value="#i['AVALNEXT']#" />

															</span>

															<span  class="IntentInput">

																<span><input id="OPTION" type="text" value="#i['TEXT']#" class="form-control" /></span>

																<!--- Keep the display clean - hover delete option --->
																<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-intent hover-delete-intent" aria-hidden="true"></span>
															</span>
														</div>
													</div>

													<!--- https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch05s06.html --->

													<div class="wrapper-training-text col-xs-12">

														<div class="training-phrases-header">Training Phrases</div>

														<div class="form-gd">

															<div class="training-text-list">
																<cfif arrayLen(i.TRN) GT 0>
																	<cfloop array="#i.TRN#" index="iTRN">
																		<div class="TrainingItem row row-small">
																			<div class="col-xs-12">
																				<div class="form-group">

																					<input type="hidden" id="TID" class="form-control readonly-val text-center" value="#iTRN['TID']#" />
																					<input type="hidden" id="T64" class="form-control readonly-val text-center" value="#iTRN['T64']#" />

																					<span style="display:block; overflow:hidden;"><input type="text" value="#iTRN['TEXT']#" class="form-control TEXT" /></span>

																					<!--- Keep the display clean - hover delete option --->
																					<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-training hover-delete-training" aria-hidden="true"></span>

																				</div>

																			</div>

																		</div>
																	</cfloop>
																</cfif>

															</div>
														</div>

														<div class="row row-small" style="margin-bottom: 2em;">
															<div class="col-xs-12 new-training add-more-training">
																<span class="training-plus">+</span> Add Training Phrase
															</div>
														</div>
													</div>


												</div>
											</cfloop>
										</cfif>

									</div>
								</div>

								<div class="row row-small" style="margin-bottom: 2em;">
									<div class="col-xs-12">
										<span class="AVALREGLINK"></span>
										<span class="new-intent add-more-answer" id="add-more-answer" style="display:block; overflow:hidden;margin-right:22px;">
											<span class="intent-plus">+</span> New Response Intent
										</span>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-lg-12">

									<div class="toggle-inteval-question">Interval Options <i class="fa fa-angle-down" aria-hidden="true"></i></div>

									<!--- <a class="toggle-inteval">Hide</a> --->
									<div class="div-toggle-inteval">

										<p class="question-options">
											How long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite.
										</p>

										<div class="col-xs-12 col-sm-10 col-lg-8">

											<div class="form-gd">
												<div class="row row-small">

													<div class="col-lg-6 col-sm-6">
														<div class="form-group">
															<label for="">Wait Time</label>
															<select id="IVALUE" class="form-control Select2 IVALUE" style="width:100%;">
																<cfloop from="0" to="60" index="iValue">
																	<cfset seletedIValue = ''>
																	<cfif arguments.controlPoint.IVALUE EQ iValue>
																		<cfset seletedIValue = 'SELECTED="SELECTED"'>
																	</cfif>
																	<option value="#iValue#" #seletedIValue#> #iValue# </option>
																</cfloop>
															</select>
														</div>
													</div>

													<div class="col-lg-6 col-sm-6">
														<div class="form-group">
															<label for="">Resolution</label>
															<select id="ITYPE" name="ITYPE" class="form-control ITYPE Select2" style="width:100%;">
																<cfloop array="#_CPINTEVALTYPE_1#" index="iType">
																	<cfset seletedIType = ''>
																	<cfif arguments.controlPoint.ITYPE EQ iType[1]>
																		<cfset seletedIType = 'SELECTED="SELECTED"'>
																	</cfif>
																	<option value="#iType[1]#" #seletedIType#> #iType[2]# </option>
																</cfloop>
															</select>
														</div>
													</div>

												</div>

												<div class="row row-small">

													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<label for="">Retry on Invalid/No Response?</label>
															<select id="IMRNR" class="form-control Select2 IMRNR" style="width:100%;">
																	<cfloop array="#_IMRNR#" index="iMRNR">
																	<cfset seletedIMRNR = ''>
																	<cfif arguments.controlPoint.IMRNR EQ iMRNR[1]>
																		<cfset seletedIMRNR = 'SELECTED="SELECTED"'>
																	</cfif>
																	<option value="#iMRNR[1]#" #seletedIMRNR#> #iMRNR[2]# </option>
																</cfloop>
															</select>
														</div>
													</div>
												</div>

												<div class="row row-small">

													<div class="col-lg-12 col-sm-12">
														<div class="form-group">
															<label for="">Still No Valid Response?</label>
															<select id="INRMO" class="form-control Select2 INRMO" style="width:100%;">
																<option value="END" <cfif arguments.controlPoint.INRMO EQ 'END'>SELECTED="SELECTED"</cfif> >End Conversation</option>
																<option value="NEXT" <cfif arguments.controlPoint.INRMO EQ 'NEXT'>SELECTED="SELECTED"</cfif> >Proceed to Next Action</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="hidden-xs col-sm-2 col-lg-4" style="text-align: right;">

											<!--- https://loading.io/spinner/custom/164047/ --->
											<svg width="50%"  height="50%"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-lds-clock" style="background: none;"><g transform="translate(50 50)"><g ng-attr-transform="scale({{config.scale}})" transform="scale(0.8)"><g transform="translate(-50 -50)"><path ng-attr-fill="{{config.c1}}" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-width="{{config.width}}" d="M50,14c19.85,0,36,16.15,36,36S69.85,86,50,86S14,69.85,14,50S30.15,14,50,14 M50,10c-22.091,0-40,17.909-40,40 s17.909,40,40,40s40-17.909,40-40S72.091,10,50,10L50,10z" fill="##274e60" stroke="##274e60" stroke-width="0"></path><path ng-attr-fill="{{config.c3}}" d="M52.78,42.506c-0.247-0.092-0.415-0.329-0.428-0.603L52.269,40l-0.931-21.225C51.304,18.06,50.716,17.5,50,17.5 s-1.303,0.56-1.338,1.277L47.731,40l-0.083,1.901c-0.013,0.276-0.181,0.513-0.428,0.604c-0.075,0.028-0.146,0.063-0.22,0.093V44h6 v-1.392C52.925,42.577,52.857,42.535,52.78,42.506z" fill="##dedede" transform="rotate(330.107 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="2.5s" begin="0s" repeatCount="indefinite"></animateTransform></path><path ng-attr-fill="{{config.c2}}" d="M58.001,48.362c-0.634-3.244-3.251-5.812-6.514-6.391c-3.846-0.681-7.565,1.35-9.034,4.941 c-0.176,0.432-0.564,0.717-1.013,0.744l-15.149,0.97c-0.72,0.043-1.285,0.642-1.285,1.383c0,0.722,0.564,1.321,1.283,1.363 l15.153,0.971c0.447,0.027,0.834,0.312,1.011,0.744c1.261,3.081,4.223,5.073,7.547,5.073c2.447,0,4.744-1.084,6.301-2.975 C57.858,53.296,58.478,50.808,58.001,48.362z M50,53.06c-1.688,0-3.06-1.373-3.06-3.06s1.373-3.06,3.06-3.06s3.06,1.373,3.06,3.06 S51.688,53.06,50,53.06z" fill="##4a798f" transform="rotate(352.527 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="10s" begin="0s" repeatCount="indefinite"></animateTransform></path></g></g></g></svg>
										</div>

									</div>

								</div>

							</div>




						</div>

						<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
							<div class="movi-1">

								<div class="movi-heading">
									<h3 class="movi-heading__number">#session.Shortcode#</h3>
								</div>
								<div class="movi-body">

									<div class="media chat__item chat--receive">
										<div class="media-left media-bottom">
											<div class="chat__user">
												<img src="../assets/layouts/layout4/img/avatar-men.png" />
											</div>
										</div>
										<div class="media-body media-bottom ">
											<div class="chat__text">
												<span class="span_text">
													<cfif Len(controlPoint.TEXT) GT 85>
														#left(controlPoint.TEXT,82)#...
													<cfelse>
														#controlPoint.TEXT#
													</cfif>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div><!--End Mobile View -->

						</div>
					</div>

				</div>




				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
                <div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp"> Save </button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
	            </div>


	            <!---


		            Loop Limits

					Applies only to intervals, and questions - anything that waits for next user response

					Set in XMLControlString
					LMAX - default is 0 - won’t apply if 0 - does not impact processing speed if 0
					LDIR - -1 will terminate and plat LMSG, 0 will move on to next CP, GT 0 will go to specified CP
					LMSG - Message to send to user when flow is terminated - can use this OR jump to another trailer message

					Uses counts from XMLResultString in Session - NOT what is in IREResults
					DRIP reset can reset the counts - still subject to infinite loop restrictions in processing

					IS set in XMLControlString directly until UI catches up with this one


	            --->



			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

</cfcomponent>
toggle
