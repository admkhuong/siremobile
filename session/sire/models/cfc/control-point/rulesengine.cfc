<cfcomponent>

	<!---
		Better manage the UI for Control Points



	--->


	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    	<cfparam name="Session.loggedIn" default="0"/>
    	<cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="public" returntype="string" output="false" hint="Get the raw HTML for a Rules Engine CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>
		<cfargument name="inpBatchId" TYPE="string" required="true" hint="The Batch Id where the XMLControlString is stored" />
		<cfargument name="inpCPList" type="struct" required="false" hint="Read all the CP items once so they can be used for selection boxes without having to re-query for each control point"/>
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var CIIndex = 0>
		<cfset var SendAsUnicode	= '' />

		<cfset var CONDITIONITEM = '' />
		<cfset var QUESTIONITEM = '' />
		<cfset var RetVarReadXMLQuestions = '' />

		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>



				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>


				<div id="BranchOptionsContainer" class="row BuilderOption BranchOptionsContainer">
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">

	                    <h4 class="portlet-heading-new">
							<span id="TDESC" style="" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>
		                </h4>
						<div class="tguide hidden">#controlPoint.TGUIDE#</div>


						<!---
						<p class="col-sm-12 alert-message"><a data-toggle="modal" href="##" data-target="##InfoModalBusinessRules">What are Business Logic Conditions?</a></p>
						--->

						<div id="ConditionsList" class="col-xs-12 col-sm-12 mb15" style="padding-left: 5px; padding-right: 5px;">

							<cfset CIIndex = 0>
							<cfloop array="#controlPoint.CONDITIONS#" index="CONDITIONITEM">

								<cfset CIIndex = CIIndex + 1>

								<!--- This is broken out in to seperate method so we can dynamically add more in the UI--->
								<!--- Get Adv UI Here --->
					            <cfset var RetVarRulesEngineCondition = '' />
					            <cfinvoke method="GetAdvEditUICondition" returnvariable="RetVarRulesEngineCondition">
					                <cfinvokeargument name="CONDITIONITEM" value="#CONDITIONITEM#">
					                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
					                <cfinvokeargument name="inpCPList" value="#inpCPList#">
					                <cfinvokeargument name="CIIndex" value="#CIIndex#">
					            </cfinvoke>
					            #RetVarRulesEngineCondition#


							</cfloop>


						</div>

						<div class="col-xs-12 col-sm-12">
							 <a href="javascript:;" class="AddCondition btn grey-mint"><i class="fa fa-plus"></i> Add Another Rule</a>
						</div>

						<!--- Default Action --->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left; padding: .5em; margin-top: 1.2em;">

							<label class="bc-title mbTight">Where to go if no business rule is met (DEFAULT)</label>
							<div class="rulesbox Select2NoBorder">
								<select id="BOFNQ" class="Select2 StepQuestionList" data-width="100%">

									<cfif ARRAYLEN(inpCPList.QUESTIONS) GT 0>

										<cfloop array="#inpCPList.QUESTIONS#" index="QUESTIONITEM">

											<!--- Display different CP types differently in the list ListContainsNoCase('"ONESELECTION","SHORTANSWER","API"', "QQQ")--->
											<cfif QUESTIONITEM.RQ EQ 0  >
												<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>#QUESTIONITEM.TEXT#</option>
											<cfelse>

												<cfif LEN(TRIM(QUESTIONITEM.TEXT)) EQ 0>

													<cfif LEN(TRIM(QUESTIONITEM.TDESC)) EQ 0>
														<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TYPE#</option>
													<cfelse>
														<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TDESC#</option>
													</cfif>

												<cfelse>
													<option value="#QUESTIONITEM.ID#" <cfif controlPoint.BOFNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TEXT#</option>
												</cfif>

											</cfif>

										</cfloop>

									<cfelse>
										<!--- Handle case where there is no data yet --->
										<option value="0" selected>-- Loading Data ... --</option>
									</cfif>

	                    		</select>

							</div>
						</div>



					</div>

					<div class="col-lg-5 hidden-md hidden-sm hidden-xs">


						<div class="GearContainer">
						  <div class="gearbox">
						  <div class="overlay"></div>
						    <div class="gear one">
						      <div class="gear-inner">
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						      </div>
						    </div>
						    <div class="gear two">
						      <div class="gear-inner">
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						      </div>
						    </div>
						    <div class="gear three">
						      <div class="gear-inner">
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						      </div>
						    </div>
						    <div class="gear four large">
						      <div class="gear-inner">
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						        <div class="bar"></div>
						      </div>
						    </div>
						  </div>
						</div>


					</div>



				</div>

				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
                <div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp"> Save </button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
	            </div>


			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

	<cffunction name="GetAdvEditUICondition" access="public" returntype="string" output="false" hint="Get the HTML for a new Condition">
		<cfargument name="CONDITIONITEM" type="struct" required="true" hint="The structure of the CONDITIONITEM"/>
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
		<cfargument name="inpCPList" type="struct" required="false" hint="Read all the CP items once so they can be used for selection boxes without having to re-query for each control point"/>
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0"/>
		<cfargument name="CIIndex" TYPE="any" required="no" default="0"/>

		<cfset var ConditionItem_HTML = ''>
		<cfset var QUESTIONITEM = '' />
		<cfset var BUFFA = '' />
		<cfset var RetVarReadCPDataById = '' />

		<cfsavecontent variable="ConditionItem_HTML">
			<cfoutput>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb5 ConditionItem" style="position:relative;">

					<input type="hidden" id="CID" value="#arguments.CONDITIONITEM.CID#" />

					<cfif CONDITIONITEM.TYPE EQ "CDF">
						<h3 class="RulesHeader HandleCond">Rule<span id="CIDLabel"> ## #CIIndex#</span></h3>
					<cfelse>
						<h3 class="RulesHeader HandleCond">Step Rule<span id="CIDLabel"> ## #CIIndex#</span></h3>
					</cfif>

					<!--- Because the delete button is absolutely positions - make sure other objects dont block it via z-index --->
					<div style="position:absolute; top:0; right:0px; margin: 0 .2em 0 0; z-index: 6;">

						<button type="button" class="btn btn-default RemoveCondition" style="padding: 3px 6px;">
							<span class="glyphicon glyphicon-minus" aria-hidden="true" ></span>
						</button>

					</div>

					<div class="row" style="">


						<div class="col-xs-12" style="text-align: left; padding: .5em; position: relative;">

							<!---
								Two possibilities here
								The rules engine is either comparing the results of a previous control point - RESPONSE														Or
								The result of a data value - CDF
							--->
							<cfif CONDITIONITEM.TYPE EQ "CDF">

								<label class="bc-title mbTight">Data to look at (IF)</label>
								<div class="rulesbox BOCDVBox">

									<!--- Search for question answer matches--->

									<cfif FindNoCase("{%INPPA=", CONDITIONITEM.BOCDV) GT 0 >

										<cfset BUFFA = ReplaceNoCase(CONDITIONITEM.BOCDV, "{%INPPA=", "") />
										<cfset BUFFA = ReplaceNoCase(BUFFA, "%}", "") />

                                    <cfelseif FindNoCase("{%INPPAV=", CONDITIONITEM.BOCDV) GT 0>

                                    	<cfset BUFFA = ReplaceNoCase(CONDITIONITEM.BOCDV, "{%INPPAV=", "") />
										<cfset BUFFA = ReplaceNoCase(BUFFA, "%}", "") />

									</cfif>

									<cfif IsNumeric(BUFFA) >
										<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById" >
                                            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                                            <cfinvokeargument name="inpQID" value="#BUFFA#">
                                            <cfinvokeargument name="inpIDKey" value="ID">
                                            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
                                        </cfinvoke>

	                                    <cfif RetVarReadCPDataById.RXRESULTCODE NEQ 1>
	                                    	<!--- Reset BUFFA to blank if no question data is found --->
	                                        <cfset BUFFA = '' />
                                        </cfif>

                                    <cfelse>
                                    	<!--- Reset BUFFA to blank if not numeric --->
                                    	<cfset BUFFA = '' />
                                    </cfif>


									<cfif TRIM(BUFFA) NEQ "">
										<div>Step ## #RetVarReadCPDataById.CPObj.RQ#</div>
										<div>
											<cfif TRIM(RetVarReadCPDataById.CPObj.TEXT) NEQ "">
												#RetVarReadCPDataById.CPObj.TEXT#
											<cfelseif TRIM(RetVarReadCPDataById.CPObj.TDESC) NEQ "">
												#RetVarReadCPDataById.CPObj.TDESC#
											<cfelse>
												#RetVarReadCPDataById.CPObj.TYPE#
											</cfif>
										</div>

										<input id="BOCDV" type="hidden" value="#CONDITIONITEM.BOCDV#" class="form-control" maxlength="1000" style="width:100%; border: none; text-align: left;">
									<cfelse>
										<cfif TRIM(CONDITIONITEM.BOCDV) EQ "">
											<cfset arguments.CONDITIONITEM.BOCDV = "Not Defined Yet" />
										</cfif>

										<input id="BOCDV" value="#CONDITIONITEM.BOCDV#" class="form-control" maxlength="1000" style="width:100%; border: none; text-align: left;">
									</cfif>

								</div>

							<cfelse>
								<!--- Legacy use only everyhting but CDF type is now deprecated --->
								<div class="rulesbox">

									<select id="BOQ" class="Select2 StepQuestionList" data-width="100%">

										<cfif ARRAYLEN(inpCPList.QUESTIONS) GT 0>

											<cfloop array="#inpCPList.QUESTIONS#" index="QUESTIONITEM">

												<!--- Display different CP types differently in the list ListContainsNoCase('"ONESELECTION","SHORTANSWER","API"', "QQQ")--->
												<cfif QUESTIONITEM.RQ EQ 0  >
													<!--- Dont show the default --->
													<!---
														Custom on change handeler for this? Allow switch between CDF and Question? How to switch back?
														Popup?

													--->
													<!--- <option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOQ EQ QUESTIONITEM.ID>selected</cfif>>#QUESTIONITEM.TEXT#</option> --->
												<cfelse>

													<cfif LEN(TRIM(QUESTIONITEM.TEXT)) EQ 0>

														<cfif LEN(TRIM(QUESTIONITEM.TDESC)) EQ 0>
															<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TYPE#</option>
														<cfelse>
															<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TDESC#</option>
														</cfif>

													<cfelse>
														<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TEXT#</option>
													</cfif>

												</cfif>

											</cfloop>

										<cfelse>
											<!--- Handle case where there is no data yet --->
											<option value="0" selected>-- Loading Data ... --</option>
										</cfif>

		                    		</select>

								</div>

							</cfif>
						</div>


						<div class="col-xs-12" style="text-align: left; padding: .5em;">
							<label class="bc-title mbTight">How do we want to compare (Operator)</label>
							<div class="rulesbox">
								<select id="BOC" class="Select2" data-width="100%" style="">
									<option value="=" <cfif CONDITIONITEM.BOC EQ "=">selected</cfif> >Equals<!---- Use Answer Choice, Freeform CSV, or Freeform Regular expression. Note: NOT CASE sensitive---></option>
									<option value="LIKE" <cfif CONDITIONITEM.BOC EQ "LIKE">selected</cfif>>Similar<!--- - Specify Static value, CSV, or Regular expression---></option>
									<option value="<" <cfif CONDITIONITEM.BOC EQ "<">selected</cfif>>Less Than</option>
									<option value=">" <cfif CONDITIONITEM.BOC EQ ">">selected</cfif>>Greater Than</option>
									<option value="<=" <cfif CONDITIONITEM.BOC EQ "<=">selected</cfif>>Less Than or Equal To</option>
									<option value=">=" <cfif CONDITIONITEM.BOC EQ ">=">selected</cfif>>Greater Than or Equal To</option>
								</select>
							</div>
						</div>

						<div class="col-xs-12" style="text-align: left; padding: .5em;">

							<label class="bc-title mbTight">What are we looking for the data to be</label>
							<div class="rulesbox">
								<textarea id="BOAV" value="#CONDITIONITEM.BOAV#" class="form-control" maxlength="1000">#CONDITIONITEM.BOAV#</textarea>
							</div>
						</div>

						<div class="col-xs-12" style="text-align: left; padding: .5em;">

							<label class="bc-title mbTight">Where to go if business rule is met (THEN)</label>
							<div class="rulesbox">

								<select id="BOTNQ" class="Select2 StepQuestionList" style="width: 100%;">

									<cfif ARRAYLEN(inpCPList.QUESTIONS) GT 0>

										<cfloop array="#inpCPList.QUESTIONS#" index="QUESTIONITEM">

											<!--- Display different CP types differently in the list ListContainsNoCase('"ONESELECTION","SHORTANSWER","API"', "QQQ")--->
											<cfif QUESTIONITEM.RQ EQ 0  >
												<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOTNQ EQ QUESTIONITEM.ID>selected</cfif>>#QUESTIONITEM.TEXT#</option>
											<cfelse>

												<cfif LEN(TRIM(QUESTIONITEM.TEXT)) EQ 0>

													<cfif LEN(TRIM(QUESTIONITEM.TDESC)) EQ 0>
														<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOTNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TYPE#</option>
													<cfelse>
														<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOTNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TDESC#</option>
													</cfif>

												<cfelse>
													<option value="#QUESTIONITEM.ID#" <cfif CONDITIONITEM.BOTNQ EQ QUESTIONITEM.ID>selected</cfif>>Step ###QUESTIONITEM.RQ# #QUESTIONITEM.TEXT#</option>
												</cfif>

											</cfif>

										</cfloop>

									<cfelse>
										<!--- Handle case where there is no data yet --->
										<option value="0" selected>-- Loading Data ... --</option>
									</cfif>

	                    		</select>

							</div>
						</div>

					</div>

<!---

					<!--- Because the delete button is absolutely positions - make sure other objects dont block it via z-index --->
					<div class="col-xs-12 col-sm-12 col-left-align" style="z-index: 5;">
					 	<label class="bc-title mbTight" >Rule Type</label>

						<select id="CTYPE" class="Select2" data-width="100%">
	                        <option value="RESPONSE" <cfif CONDITIONITEM.TYPE EQ "RESPONSE">selected</cfif>>Single Selection Compare</option>
	                        <option value="RESPONSE2" <cfif CONDITIONITEM.TYPE EQ "RESPONSE2">selected</cfif>>Response Compare</option>
	                        <option value="CDF" <cfif CONDITIONITEM.TYPE EQ "CDF">selected</cfif>>Data Compare</option>
	                    </select>
					</div>
--->


			 		<input type="hidden" id="TYPE" value="#CONDITIONITEM.TYPE#" />
			 		<input type="hidden" id="BOV" value="#CONDITIONITEM.BOV#" />

					<!---
					<!--- Legacy option for rules engine - tries to match actual answer - new logic just says to list possibilities in BOAV --->
					<div class="col-xs-12 col-sm-12">
						<label class="bc-title mbTight">Answer List</label>
						<select id="BOV" class="Select2" data-width="100%">
	                            <option value="0" selected>-- Loading Data ... --</option>
	                    </select>
					</div>
					--->

				</div>

			</cfoutput>
		</cfsavecontent>

		<cfreturn ConditionItem_HTML />
	</cffunction>


	<cffunction name="GetAdvEditUIConditionRemote" access="remote" output="false" hint="Get the HTML for a new Condition">
		<cfargument name="CONDITIONITEM" type="any" required="true" hint="The structure of the CONDITIONITEM"/>
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="The Batch Id where the XMLControlString is stored" />
		<cfargument name="inpCPList" type="any" required="false" hint="Read all the CP items once so they can be used for selection boxes without having to re-query for each control point"/>
		<cfargument name="inpTemplateFlag" TYPE="any" required="no" default="0"/>
		<cfargument name="CIIndex" TYPE="any" required="no" default="0"/>


		<cfset var DataOut = StructNew()/>
		<cfset DataOut.RXRESULTCODE = -1>
		<cfset DataOut.TYPE = "">
		<cfset DataOut.MESSAGE = "">
		<cfset DataOut.ERRMESSAGE = "">
		<cfset DataOut.COND_HTML = '' />

		<cftry>

			<cfif isJson(arguments.CONDITIONITEM)>
		        <cfset arguments.CONDITIONITEM = deserializeJSON(arguments.CONDITIONITEM) />
		    </cfif>

			<cfif NOT isStruct(arguments.CONDITIONITEM)>
		        <cfthrow MESSAGE="CONDITIONITEM argument must be structure or a json string" />
		    </cfif>

		    <!--- What if there are no CPs yet?--->
			<cfif isJson(arguments.inpCPList)>
		        <cfset arguments.inpCPList = deserializeJSON(arguments.inpCPList) />
		    </cfif>

		    <cfif NOT isStruct(arguments.inpCPList)>
		        <cfthrow MESSAGE="inpCPList argument must be structure or a json string" />
		    </cfif>

			<!--- This is broken out in to seperate method so we can dynamically add more in the UI--->
			<!--- Get Adv UI Here --->
	        <cfset var RetVarRulesEngineCondition = '' />
	        <cfinvoke method="GetAdvEditUICondition" returnvariable="RetVarRulesEngineCondition">
	            <cfinvokeargument name="CONDITIONITEM" value="#arguments.CONDITIONITEM#">
	            <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	            <cfinvokeargument name="inpCPList" value="#arguments.inpCPList#">
	            <cfinvokeargument name="CIIndex" value="#CIIndex#">
	        </cfinvoke>

			<cfset DataOut.RXRESULTCODE = 1>
			<cfset DataOut.COND_HTML = RetVarRulesEngineCondition />

		<cfcatch TYPE="any">

            <cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset DataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset DataOut.TYPE = "#cfcatch.TYPE#">
			<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#">

        </cfcatch>
		</cftry>

		<cfreturn DataOut />

	</cffunction>

</cfcomponent>
