<cfcomponent>

	<!---
		Better manage the UI for Control Points


		Coding Standard Reminder:
		Dont' use relative paths - you do not know where this output might be included

		Bad:
			<img src="../assets/layouts/layout4/img/avatar-men.png" />

		Good:
			<img src="/session/sire/assets/layouts/layout4/img/avatar-men.png" />


	--->

	<!--- Wait For Human Help (WFHH) --->

	<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

	<cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<cffunction name="GetAdvEditUI" access="remote" returntype="string" output="false" hint="Get the raw HTML for a WFHH CP">
		<cfargument name="controlPoint" type="struct" required="true" hint="The structure of the Control Point"/>

		<cfset var CP_HTML = ''>
		<cfset var UniSearchPattern = '' />
		<cfset var UniSearchMatcher = '' />
		<cfset var MaxPerMessage = '' />
		<cfset var UpperCharacterLimit = '' />
		<cfset var SendAsUnicode	= '' />


		<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
		<!--- <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) /> --->
		<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", REGEX_UNICODE ) ) />


		<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", arguments.controlPoint.TEXT ) ) />

  		<!--- Look for character higher than 255 ASCII --->
        <cfif UniSearchMatcher.Find() >

            <cfset SendAsUnicode = 1 />
            <cfset MaxPerMessage = 66 />
            <cfset UpperCharacterLimit = 70 />

        <cfelse>

            <cfset SendAsUnicode = 0 />
            <cfset MaxPerMessage = 153 />
            <cfset UpperCharacterLimit = 160 />

        </cfif>

		<cfsavecontent variable="CP_HTML">
			<cfoutput>

				<!--- Mask jumpy CSS, loading select2 boxes, etc -- No FOUC in the UI --->
				<div class="LoadingCPs">
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<div class="row">
					<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-heading-new">
							<span id="TDESC" style="" class="cp-desc-input text-color-gray" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Step Description">#controlPoint.TDESC#</span>
						</div>
						<div class="tguide hidden">#controlPoint.TGUIDE#</div>

						<div class="form-gd">
							<div class="form-group">
								<textarea data-emojiable="true" maxLength="459" name="CPText" id="CPText" data-control="text-message" cols="30" rows="5" class="text form-control validate[required]">#controlPoint.TEXT#</textarea>

								<div class="row">
									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
										<h5 id="cp-char-count-#controlPoint.RQ#" class="text-right control-point-char text-color-gray">
											Character Count {#len(controlPoint.TEXT)#}
										</h5>
										<span>
											<a tabindex="0" class="info-popover pull-right see-unicode-mess  <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>" data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#UnicodePopupInfo#'><img  src='/session/sire/assets/layouts/layout4/img/info.png '/></a>
											<span class="see-unicode-mess see-unicode-mess-edit pull-right <cfif SendAsUnicode NEQ 1><cfoutput>hidden</cfoutput></cfif>">#UnicodeCPWarning# &nbsp</span>
										</span>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
						<div class="movi">
							<div class="movi-heading">
								<h3 class="movi-heading__number">#session.Shortcode#</h3>
							</div>
							<div class="movi-body">
<!---
								<cfif ListFind("3,11",arguments.inpTemplateId) EQ 0>
									<div class="media chat__item chat--sent">
										<div class="media-body media-bottom">
											<div class="chat__text">
												<!--- HARD CODE YES/NO TEXT FOR TRAILAER ONLY--->
<!---
												<cfif arguments.inpTemplateId EQ 1>
													<cfif htmlId EQ 13>
														<span >YES</span>
													<cfelseif htmlId EQ 12>
														<span >NO</span>
													</cfif>
												<cfelseif arguments.inpTemplateId EQ 8>
													<span >NO</span>
												<cfelse>
													<span class="span_keyword">Localdinner</span>
												</cfif>
--->
												<span class="span_keyword">Localdinner</span>
											</div>
										</div>
										<div class="media-right media-bottom">
											<div class="chat__user">
												<img src="/session/sire/assets/layouts/layout4/img/avatar-women.png" />
											</div>
										</div>
									</div>
								</cfif>
--->
								<div class="media chat__item chat--receive">
									<div class="media-left media-bottom">
										<div class="chat__user">
											<img src="/session/sire/assets/layouts/layout4/img/avatar-men.png" />
										</div>
									</div>
									<div class="media-body media-bottom ">
										<div class="chat__text">
											<span class="span_text">
												<cfif Len(controlPoint.TEXT) GT 85>
													#left(controlPoint.TEXT,82)#...
												<cfelse>
													#controlPoint.TEXT#
												</cfif>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div><!--End Mobile View -->

					</div>
				</div>

				<!--- Use visibility:hidden vs diaplay:none so CSS does not get jumpy--->
				<div class="CPEFooter" style="visibility:hidden;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-cp">Save</button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-cancel-cpe">Cancel</button>
		        </div>

			</cfoutput>
		</cfsavecontent>

		<cfreturn CP_HTML />
	</cffunction>

</cfcomponent>
