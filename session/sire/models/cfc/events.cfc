<cfcomponent name="events">

	<cffunction name="trigger" access="public" output="false" hint="trigger a event">
		<cfargument name="event" type="string" required="true" hint="event name"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.EVENT = arguments.event />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.EVENTDATAOUT = [] />

		<cfset var coms = '' />
		<cfset var RetVarEvent = '' />
		<cfset var eventPath = '' />
		<cfset var com = '' />

		<cftry>

			<cfset eventPath = ExpandPath("/session/sire/models/cfc/events/" & arguments.event)/>
			<cfif DirectoryExists(eventPath)>
				<cfset coms = DirectoryList(eventPath, false, "name" , "*.cfc")/> 

				<cfif arrayLen(coms) GT 0>
					<cfloop array="#coms#" index="com">
						<cfset com = left(com, len(com) - 4)/>
						<cftry>
							<cfinvoke method="callback" component="session.sire.models.cfc.events.#arguments.event#.#com#" returnvariable="RetVarEvent">
								<cfinvokeargument name="args" value="#arguments#"/>
								<cfinvokeargument name="caller" value="events"/>
							</cfinvoke>
							<cfset RetVarEvent.CALLBACK = com/>
							<cfset arrayAppend(dataout.EVENTDATAOUT, RetVarEvent)/>
							
							<cfcatch>
								<cfset arrayAppend(dataout.EVENTDATAOUT, {
									RXRESULTCODE = -1,
									TYPE = "#cfcatch.TYPE#",
									MESSAGE = "#cfcatch.MESSAGE#",
									ERRMESSAGE = "#cfcatch.detail#"
								})/>
							</cfcatch>
						</cftry>
					</cfloop>
					<cfset dataout.RXRESULTCODE = 1 />
				<cfelse>
					<cfset dataout.ERRCODE = "CallbackNotExists"/>
				</cfif>
			<cfelse>
				<cfset dataout.ERRCODE = "DirectoryNotExists"/>
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>

</cfcomponent>