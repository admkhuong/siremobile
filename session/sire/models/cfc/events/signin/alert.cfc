<cfcomponent>
	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cffunction name="callback" access="public" output="false" hint="trigger a event">
		<cfargument name="args" type="struct" required="true" hint="event arguments"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var userId = '' />
		<cfset var userTriggers = ALERT_TRIGGER_SIGN_IN />
		<cfset var webAlertMessages = '' />
		<cfset var getCreditPercentage = '' />

		<cftry>

			<cfif structKeyExists(arguments.args, "userId")>
				<cfset userId = arguments.args.userId/>

				<cfquery name="getCreditPercentage" datasource="#session.DBSourceREAD#">
					SELECT
						ROUND((bl.Balance_int + bl.BuyCreditBalance_int + bl.PromotionCreditBalance_int)/(up.FirstSMSIncluded_int)*100, 2) AS CreditPercentage,
						up.EndDate_dt < NOW() AS Expired,
						NumOfRecurringPlan_int
					FROM
						simplebilling.billing bl
					JOIN
						simplebilling.userplans up
					ON
						bl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"/>
					AND
						up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"/>
					AND
						up.Status_int = 1
				</cfquery>

				<cfif getCreditPercentage.RECORDCOUNT GT 0>
					<cfif getCreditPercentage.CreditPercentage EQ 0>
						<cfset userTriggers += ALERT_TRIGGER_NO_CREDIT />
					<cfelseif getCreditPercentage.CreditPercentage LT 25>
						<cfset userTriggers += ALERT_TRIGGER_LOW_CREDIT />
					</cfif>
				</cfif>

				<cfif getCreditPercentage.Expired GT 0 AND getCreditPercentage.NumOfRecurringPlan_int GT 0>
					<cfset userTriggers += ALERT_TRIGGER_DECLINED_CREDIT />
				</cfif>

				<cfinvoke method="sendAlert2User" component="session.sire.models.cfc.alert-system">
					<cfinvokeargument name="userId" value="#userId#"/>
					<cfinvokeargument name="trigger" value="#userTriggers#"/>
				</cfinvoke>

				<!--- Web Alert --->
				<cfquery name="webAlertMessages" datasource="#session.DBSourceREAD#">
					SELECT
						L.PKId_bi,
						L.MessageId_bi,
						L.Content_vch
					FROM
						simpleobjects.alert2users AS L
					WHERE
						L.ReceiverId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"/>
					AND
						L.SentTo_int = #ALERT_SEND_TO_WEB_ALERT#
					AND
						L.Status_int = 1
					AND
						(L.WebAlertHide_int = 0 OR L.WebAlertHide_int IS NULL)
					AND
						(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
				</cfquery>

				<cfset Session.webAlertMessages = {} />
				<cfif webAlertMessages.RECORDCOUNT GT 0>
					<cfloop query="#webAlertMessages#">
						<cfset Session.webAlertMessages[webAlertMessages.MessageId_bi] = [webAlertMessages.PKId_bi, webAlertMessages.Content_vch, webAlertMessages.MessageId_bi] />
					</cfloop>
				</cfif>

				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

			<cfset dataout.ARGS = arguments.args/>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>