<cfcomponent>
	<cffunction name="callback" access="public" output="false" hint="trigger a event">
		<cfargument name="args" type="struct" required="true" hint="event arguments"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />


		<cfset dataout.ARGS = arguments.args/>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>