<cfcomponent>
	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cffunction name="callback" access="public" output="false" hint="trigger a event">
		<cfargument name="args" type="struct" required="true" hint="event arguments"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRCODE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var userId = 0 />

		<cftry>

			<cfif structKeyExists(arguments.args, "userId")>
				<cfset userId = arguments.args.userId/>

				<cfinvoke method="sendAlert2User" component="session.sire.models.cfc.alert-system">
					<cfinvokeargument name="userId" value="#userId#"/>
					<cfinvokeargument name="trigger" value="#ALERT_TRIGGER_SIGN_UP#"/>
				</cfinvoke>

				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

			<cfset dataout.ARGS = arguments.args/>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>