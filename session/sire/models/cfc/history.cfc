<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off DEBUGging --->
    <cfsetting showDEBUGoutput="no" />
	<cfinclude template="/session/sire/configs/paths.cfm">

    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.UserID" default="0"/>

    <!---
		Flag = 1 - somthing is elligible for redo
		Flag = 0 - somthin is elligible for undo further
	--->


	<!--- Store to database --->
	<cffunction name="AddHistory" access="remote" output="false" hint="Insert latest changes to history for later undo/redo">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetHistory = '' />
		<cfset var RemoveHistory = '' />
		<cfset var UpdateHistory = '' />


		<cftry>
			<!--- Remove history when add event --->
			<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
				SELECT
					id
				FROM
					simpleobjects.history
				WHERE
					flag = 1
				AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				ORDER BY id ASC LIMIT 0,1
	       </cfquery>

			<cfif GetHistory.recordCount GT 0>
				<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
					DELETE FROM
						simpleobjects.history
					WHERE
						flag = 1
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
						UserId_int = #SESSION.UserID#
					AND
						id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
				</cfquery>

				<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.history
					SET
						flag = 0
					WHERE
						id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
				</cfquery>
			</cfif>

			<!--- Update history --->
	 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
	            INSERT INTO
	              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
	            VALUES
	            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
					#SESSION.UserID#,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
					)
	       </cfquery>

		   <cfset dataout.RXRESULTCODE = 1 />

	   <cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataOut.TYPE = "#cfcatch.TYPE#">
			<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>


    <cffunction name="ClearHistory" access="remote" output="false" hint="clear history">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var DeleteHistory = '' />

		<cftry>
			<cfquery name="DeleteHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataOut.TYPE = "#cfcatch.TYPE#">
			<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="UndoMCID" access="remote" output="true" hint="Undo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetHistory = '' />
		<cfset var GetHistoryRedo = '' />
		<cfset var UpdateHistory = '' />
		<cfset var GetBatchOptions = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var OutToDBXMLBuff = '' />

		<cftry>

            <!---	<cfquery name="GetHistoryRedo" datasource="#Session.DBSourceEBM#">
					SELECT
						MAX(id) AS MAXID
					FROM
						simpleobjects.history
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
							flag = 1
				</cfquery>--->


				<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
					SELECT
						id, XMLControlString_vch, event
					FROM
						simpleobjects.history
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
							flag = 0

						<!---<cfif GetHistoryRedo.MAXID NEQ "">
                            AND
                                id < #GetHistoryRedo.MAXID#
                        </cfif>     --->
 					ORDER BY
						`id` DESC
					LIMIT 0,2
				</cfquery>

				<cfif GetHistory.recordCount neq 0>

			        <!--- Read from DB Batch Options --->
			        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
			            SELECT
			              XMLControlString_vch
			            FROM
			              simpleobjects.batch
			            WHERE
			              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			        </cfquery>

			        <cfif trim(GetBatchOptions.XMLControlString_vch) eq trim(GetHistory.XMLControlString_vch)>
				        <cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>

                    	<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 1
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetHistory.id[2]#">
						</cfquery>

			        <cfelse>

                    	<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
			        </cfif>

					<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.history
						SET
							flag = 1
						WHERE
							id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
					</cfquery>

			        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
			             UPDATE
			                 simpleobjects.batch
			             SET
			                 XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
			             WHERE
			                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			         </cfquery>

			         <cfif GetHistory.recordCount eq 1>
				         <cfset dataOut.RXRESULTCODE = 1>
				     </cfif>
				<cfelse>
					<cfset dataOut.RXRESULTCODE = 0>
				</cfif>

				<cfset dataOut.RXRESULTCODE = 2>

			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataOut.TYPE = "#cfcatch.TYPE#">
				<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">

			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="RedoMCID" access="remote" output="false" hint="Redo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetHistory = '' />
		<cfset var UpdateHistory = '' />
		<cfset var GetBatchOptions = '' />
		<cfset var WriteBatchOptions = '' />
		<cfset var OutToDBXMLBuff = '' />
		<cfset var ID = '' />
		<cfset var ID2 = '' />

		<cftry>
			<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
				AND
					flag = 1
				ORDER BY
					`id` ASC
				LIMIT 0,2
			</cfquery>

			<cfif GetHistory.recordcount neq 0>
		        <!--- Read from DB Batch Options --->
		        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
		            SELECT
		              XMLControlString_vch
		            FROM
		              simpleobjects.batch
		            WHERE
		              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		        </cfquery>


				<cfset ID = GetHistory.id>
				<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
				<cfif GetBatchOptions.XMLControlString_vch eq GetHistory.XMLControlString_vch>
					<cfset ID2 = GetHistory.id[2]>
					<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>
					<cfif ID2 neq 0>
						<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 0
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID2#">
						</cfquery>
					</cfif>
				</cfif>
		        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
		             UPDATE
		                 simpleobjects.batch
		             SET
		                 XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#OutToDBXMLBuff#">
		             WHERE
		                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		         </cfquery>

				<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.history
					SET
						flag = 0
					WHERE
						id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID#">
				</cfquery>

		         <cfif GetHistory.recordCount eq 1>
			         <cfset dataOut.RXRESULTCODE = 1>
			     </cfif>
			<cfelse>
				<cfset dataOut.RXRESULTCODE = 0>
			</cfif>


		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataOut.TYPE = "#cfcatch.TYPE#">
			<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>


    <cffunction name="CheckHistory" access="remote" output="false" hint="Not really sure what the heck this is for?!? Get history event types for XML Control String">
		<cfargument name="INPBATCHID" Type="string" default="0">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetHistoryUnDo = '' />
		<cfset var GetHistoryUnDone = '' />

		<cftry>
			<cfquery name="GetHistoryUnDo" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 1
				ORDER BY
					`id` ASC
				LIMIT 0,2
			</cfquery>

            <cfquery name="GetHistoryUnDone" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 0
				ORDER BY
					`id` DESC
				LIMIT 0,2
			</cfquery>

			<cfif GetHistoryUnDo.recordcount GT 0 AND GetHistoryUnDone.recordcount GT 0>
		        <cfset dataOut.RXRESULTCODE = 3>
            <cfelseif GetHistoryUnDo.recordcount GT 0 AND GetHistoryUnDone.recordcount EQ 0>
		        <cfset dataOut.RXRESULTCODE = 2>
            <cfelseif GetHistoryUnDo.recordcount EQ 0 AND GetHistoryUnDone.recordcount GT 0>
		       <cfset dataOut.RXRESULTCODE = 1>
			<cfelse>
				<cfset dataOut.RXRESULTCODE = 0>
			</cfif>

            <cfset dataOut.RXRESULTCODE = GetHistory.recordcount>

		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>

			<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset dataOut.TYPE = "#cfcatch.TYPE#">
			<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">

		</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>


    <cffunction name="createUserLog" access="remote">
		<cfargument name="moduleName" required="true">
		<cfargument name="operator" required="true">

		<!--- Default Return Structure --->
		<cfset var dataout	= {} />

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var insertUserLog = '' />

		<cftry>

			<cfif Session.CompanyUserId GT 0>
                <cfset arguments.userId = "#Session.CompanyUserId#">
            <cfelse>
                <cfset arguments.userId = "#Session.UserId#">
            </cfif>

			<cfquery name="insertUserLog" datasource="#Session.DBSourceEBM#">
				INSERT INTO
					simpleobjects.userlogs
				(
					UserId_int,
					ModuleName_vch,
					Operator_vch,
					Timestamp_dt
				)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#" />,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(moduleName, 45)#" />, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(operator, 255)#" />,
					NOW()
				)

			</cfquery>

			<cfset dataOut.RXRESULTCODE = 1>

			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>

				<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataOut.TYPE = "#cfcatch.TYPE#">
				<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>



</cfcomponent>
