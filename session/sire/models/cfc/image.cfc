<cfcomponent>
	<cfinclude template="/session/sire/configs/paths.cfm">

	<cffunction name="SaveImage" access="remote" output="false" hint="Save image to server for temporary process">

		<cfset var dataout = {} />
		<cfset var retValUploadImage = '' />
		<cfset var uploadResult = '' />
		<cfset var userStorageInfo = '' />
		<cfset var checkFilename = '' />
		<cfset var uploadPath = _USERUPLOADFOLDER />
		
		<cfset var CheckDirExists = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.DEBUGSTR = '' />

		<cftry>
	
			<!--- Self repair here - is there some other process thats been creating these on the user leve?!? - really no need if code is self repairing on dir structures --->
			<cfif !DirectoryExists(uploadPath)> 
				<cfdirectory action="create" directory="#uploadPath#" mode="755">
			</cfif>	
			
			<!--- Upload file to server --->
			<cffile action="upload" fileField="upload" destination="#uploadPath#" accept="image/*" nameconflict="overwrite" result="uploadResult" mode="755">

			<cfif !arrayContains(_ACCEPTEDIMAGETYPE, lCase(uploadResult.clientfileext))>
				<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
				<cfthrow message="File type not accepted!" />
			</cfif>

			<!--- Check filesize, client already handle this but check again for security --->
			<cfif uploadResult.filesize GT _IMGMAXSIZE>
				<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
				<cfthrow message="This file is too large! Max file size is 10MB" />
			</cfif>

			<cfinvoke method="GetUserStorageInfo" returnvariable="userStorageInfo">
			</cfinvoke>

			<cfif userStorageInfo.RXRESULTCODE LT 1>
				<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
				<cfthrow message="Internal Server Error! Please try again later" />
			</cfif>

			<cfif (userStorageInfo.USERUSAGE + uploadResult.filesize) GT userStorageInfo.USERSTORAGEPLAN>
				<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
				<cfthrow message="You have running out of storage! Please remove unused images or upgrade your plan!" />
			</cfif>
			<cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#uploadResult.serverdirectory#/#uploadResult.serverfilename#.#lCase(uploadResult.clientfileext)#" attributes="normal">

			<cfset uploadResult.serverfile = uploadResult.serverfilename & '.' & lCase(uploadResult.clientfileext) />

			<!--- Upload to S3 --->
			<cfinvoke method="UploadImageToS3" returnvariable="retValUploadImage">
				<cfinvokeargument name="inpFullFileName" value="#uploadResult.serverfile#">
				<cfinvokeargument name="inpLocalPath" value="#uploadResult.serverdirectory#/">
				<cfinvokeargument name="inpFileSize" value="#uploadResult.filesize#">
				<cfinvokeargument name="inpImageType" value="#uploadResult.clientfileext#">
				<cfinvokeargument name="inpPlainFileName" value="#uploadResult.serverfilename#">
			</cfinvoke>

			<cfreturn retValUploadImage>

			<cfcatch type="any">
				
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
				
				<!--- This wont work - uploadResult is not avialable --->
				<!--- <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" /> --->
				
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="ClearTempFile" access="remote" output="false" hint="Delete temporary file">

		<cfargument name="inpUserId" required="false" default="#SESSION.UserId#" hint="user id">
		<cfargument name="inpFilename" required="true" type="string" hint="file name need delete">

		<cfset var dataout = {} />
		<cfset var uploadPath = _USERUPLOADFOLDER />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cffile action="delete" file="#uploadPath#/#arguments.inpFilename#" />

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="UploadImageToS3" access="remote" output="false" hint="Upload image to s3">

		<cfargument name="inpFullFileName" required="true" type="string">
		<cfargument name="inpLocalPath" required="true" type="string" hint="">
		<cfargument name="inpFileSize" required="true" type="numeric">
		<cfargument name="inpImageType" required="true" type="string">
		<cfargument name="inpPlainFileName" required="true" type="string">
		<cfargument name="inpUserId" required="false" default="#SESSION.UserId#" hint="user Id">

		<cfset var dataout = {} />
		<cfset var retValUploadS3 = '' />
		<cfset var retValUploadThumbS3 = '' />
		<cfset var insertUserImage = '' />
		<cfset var checkImageExist = '' />
		<cfset var updateImageExist = '' />
		<cfset var bucketName = _USERS3BUCKET />
		<cfset var thumbImage = '' />
		<cfset var retValInsertImage = '' />
		<cfset var imageType = lCase(arguments.inpImageType) />
		<cfset var thumbCreated = 0 />
		<cfset var myImage = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.IMAGEURL = '' />
		<cfset dataout.IMAGETHUMBURL = '' />
		<cfset dataout.TYPE = '' />

		<!--- Thumb image path --->
		<cfset thumbImage = arguments.inpPlainFileName & "_#_IMGTHUMBWIDTH#x#_IMGTHUMBHEIGHT#_thumb." & imageType >

		<cftry>
			<cfif arrayIsEmpty(REmatch(_NAMEREG, arguments.inpPlainFileName))>
				<cfthrow type="Object" message="Filename syntax error" detail="Filename syntax error! Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only">
			</cfif>
			<cfif FileExists(arguments.inpLocalPath & arguments.inpFullFileName)>

				<!--- Create thumb image --->
				<cfif structKeyExists(FORM, 'thumbnail')>
					<cfset myImage = ImageReadBase64("#FORM.thumbnail#")>
					<cfimage action="convert" destination = "#arguments.inpLocalPath##thumbImage#" source = "#myImage#" overwrite = "yes">
					<cfset thumbCreated = 1 />
				<cfelse>
					<cftry>
						<cfimage source="#arguments.inpLocalPath##arguments.inpFullFileName#" name="myImage">

						<cfinvoke method="SquareThumbnail">
							<cfinvokeargument name="image" value="#myImage#">
							<cfinvokeargument name="size" value="#_IMGTHUMBWIDTH#">
							<cfinvokeargument name="destination" value="#arguments.inpLocalPath##thumbImage#">
						</cfinvoke>

						<cfset thumbCreated = 1 />
						<cfcatch type="any">
							<cfset thumbCreated = 0 />
						</cfcatch>
					</cftry>
				</cfif>

				<!--- Upload image and thumb image to S3 --->
				<cfinvoke component="session.sire.models.cfc.upload" method="uploadFileS3" returnvariable="retValUploadS3">
					<cfinvokeargument name="name" value="#arguments.inpFullFileName#">
					<cfinvokeargument name="local_path" value="#arguments.inpLocalPath#">
					<cfinvokeargument name="contentType" value="image">
					<cfinvokeargument name="bucketName" value="#bucketName#">
				</cfinvoke>

				<cfif thumbCreated GT 0>
					<cfinvoke component="session.sire.models.cfc.upload" method="uploadFileS3" returnvariable="retValUploadThumbS3">
						<cfinvokeargument name="name" value="#thumbImage#">
						<cfinvokeargument name="local_path" value="#arguments.inpLocalPath#">
						<cfinvokeargument name="contentType" value="image">
						<cfinvokeargument name="bucketName" value="#bucketName#">
					</cfinvoke>
				</cfif>

				<!--- If upload success, insert tracking image record --->
				<cfif retValUploadS3.STATUS EQ 1>

					<cfset dataout.IMAGEURL = _S3URL & "/" & bucketName & "/" & arguments.inpPlainFileName & '.' & imageType />
					<cfset dataout.IMAGENAME = arguments.inpPlainFileName />

					<cfif thumbCreated GT 0>
						<cfset dataout.IMAGETHUMBURL = _S3URL & "/" & bucketName & "/" & thumbImage />
					<cfelse>
						<cfset dataout.IMAGETHUMBURL = dataout.IMAGEURL />
					</cfif>

					<!--- If this image name existed in db, just return its url --->
					<cfquery name="checkImageExist" datasource="#SESSION.DBSourceREAD#">
						SELECT
							ImageName_vch,
							ImageType_vch,
							ImageId_bi,
							ImageUrl_vch
						FROM
							simpleobjects.userimages
						WHERE
							ImageName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlainFileName#">
						AND
							ImageType_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#imageType#">
						AND
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
						LIMIT 1
					</cfquery>

					<cfif checkImageExist.RECORDCOUNT GT 0>
						<cfquery name="updateImageExist" datasource="#SESSION.DBSourceEBM#">
							UPDATE
								simpleobjects.userimages
							SET
								ImageSize_bi = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpFileSize#">,
								ThumbCreated_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#thumbCreated#">
							WHERE
								ImageName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlainFileName#">
							AND
								ImageType_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#imageType#">
							AND
								UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
						</cfquery>

						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.IMAGEID = checkImageExist.ImageId_bi />
					<cfelse>
						<!--- Insert new image record --->
						<cfquery name="insertUserImage" datasource="#SESSION.DBSourceEBM#" result="retValInsertImage">
							INSERT INTO
								simpleobjects.userimages
								(
									UserId_int,
									ImageName_vch,
									ImageUrl_vch,
									ImageSize_bi,
									ImageType_vch,
									ThumbCreated_int
								)
							VALUEs
								(
									<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
									<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlainFileName#">,
									<cfqueryparam cfsqltype="cf_sql_varchar" value="#dataout.IMAGEURL#">,
									<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpFileSize#">,
									<cfqueryparam cfsqltype="cf_sql_varchar" value="#imageType#">,
									<cfqueryparam cfsqltype="cf_sql_integer" value="#thumbCreated#">
								)
						</cfquery>

						<cfif retValInsertImage.RECORDCOUNT GT 0>
							<cfset dataout.RXRESULTCODE = 1 />
							<cfset dataout.IMAGEID = retValInsertImage.GENERATED_KEY />
						</cfif>
					</cfif>
				<cfelse>
					<cfset dataout.MESSAGE = "Failed to upload image to storage!" />
				</cfif>

				<!--- Delete temp file --->
				<cffile action="delete" file="#arguments.inpLocalPath##arguments.inpFullFileName#" />
				<cfif thumbCreated GT 0>
					<cffile action="delete" file="#arguments.inpLocalPath##thumbImage#" />
				</cfif>
			<cfelse>
				<cfset dataout.MESSAGE = "File not exist!" />
			</cfif>
			<cfcatch type="any">
				<cffile action="delete" file="#arguments.inpLocalPath##arguments.inpFullFileName#" />
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="CheckImageName" access="remote" output="false" hint="Check if file name already exist, return 0 if not exist - 1 if already exist - 2 if syntax error">

		<cfargument name="inpPlainFileName" required="true" type="string">
		<cfargument name="inpFileType" required="true" type="string">
		<cfargument name="inpUserId" required="false" default="#SESSION.UserId#">

		<cfset var dataout = {} />
		<cfset var retValCheckFileName = '' />
		<cfset var fileType = lCase(arguments.inpFileType) />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.CHECKRESULT = 0 />
		<cfset dataout.IMAGEURL = '' />
		<!--- 0 - not exist, 1 - existed, 2 - syntax error --->

		<cftry>
			<cfif arrayIsEmpty(REmatch(_NAMEREG, arguments.inpPlainFileName))>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.CHECKRESULT = 2 />
				<cfset dataout.MESSAGE = "Filename syntax error! Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only" />
				<cfreturn dataout />
			</cfif>
			<cfquery name="retValCheckFileName" datasource="#SESSION.DBSourceREAD#">

				SELECT
					ImageId_bi,
					ImageUrl_vch
				FROM
					simpleobjects.userimages
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#" />
				AND
					ImageName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpPlainFileName#" />
				AND
					ImageType_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#fileType#" />
				LIMIT 1
			</cfquery>

			<cfif retValCheckFileName.RECORDCOUNT GT 0>
				<cfset dataout.CHECKRESULT = 1>
				<cfset dataout.IMAGEURL = retValCheckFileName.ImageUrl_vch />
			</cfif> 

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="BrowseImage" access="public" output="false" hint="Browse user images">

		<cfargument name="inpUserId" required="false" default="#SESSION.UserId#">

		<cfset var dataout = {} />
		<cfset var userImages = '' />
		<cfset var bucketName = _USERS3BUCKET />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE  = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.USERIMAGES = [] />

		<cftry>

			<cfquery name="userImages" datasource="#SESSION.DBSourceREAD#">
				SELECT
					ImageId_bi,
					ImageUrl_vch,
					ImageName_vch,
					ImageType_vch,
					ImageSize_bi,
					ThumbCreated_int
				FROM
					simpleobjects.userimages
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				ORDER BY
					ImageId_bi DESC
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfif userImages.RECORDCOUNT GT 0>
				<cfloop query="userImages">
					<cfset var temp = {} />

					<cfset temp.IMAGEURL = ImageUrl_vch />
					<cfset temp.IMAGEID = ImageId_bi />
					<cfset temp.IMAGETYPE = ImageType_vch />
					<cfset temp.IMAGESIZE = ImageSize_bi />
					<cfset temp.IMAGENAME = ImageName_vch />

					<!--- Prepare thumb image to display --->
					<cfif ThumbCreated_int GT 0>
						<cfset temp.IMAGETHUMB = _S3IMAGEROOTURL & "/" & bucketName & "/" & ImageName_vch & "_#_IMGTHUMBWIDTH#x#_IMGTHUMBHEIGHT#_thumb." & ImageType_vch />
					<cfelse>
						<cfset temp.IMAGETHUMB = ImageUrl_vch />
					</cfif>

					<cfset arrayAppend(dataout.USERIMAGES, temp) />
				</cfloop>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="DeleteImage" access="remote" output="false" hint="Delete image">

		<cfargument name="inpImageId" required="true" type="numeric">

		<cfset var dataout = {} />
		<cfset var getImage = '' />
		<cfset var deleteImage = '' />
		<cfset var bucketName = _USERS3BUCKET />
		<cfset var retValDeleteS3 = '' />
		<cfset var retValDeleteThumbS3 = '' />
		<cfset var retValDeleteImage = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE  = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
		<!--- Check if image existed --->
			<cfquery name="getImage" datasource="#SESSION.DBSourceREAD#">
				SELECT
					ImageName_vch,
					ImageType_vch
				FROM
					simpleobjects.userimages
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#SESSION.UserId#">
				AND
					ImageId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpImageId#">
				LIMIT 1
			</cfquery>

			<cfif getImage.RECORDCOUNT GT 0>
				<!--- Delete image record --->
				<cfquery name="deleteImage" datasource="#SESSION.DBSourceEBM#" result="retValDeleteImage">
					DELETE FROM
						simpleobjects.userimages
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#SESSION.UserId#">
					AND
						ImageId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpImageId#">
				</cfquery>

				<cfif retValDeleteImage.RECORDCOUNT GT 0>
					<!--- Delete image on S3, thumb need to be deleted as well --->
					<cfinvoke component="session.sire.models.cfc.upload" method="DeleteS3" returnvariable="retValDeleteS3">
						<cfinvokeargument name="inpFileName" value="#getImage.ImageName_vch#.#getImage.ImageType_vch#">
						<cfinvokeargument name="inpBucketName" value="#bucketName#">
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.upload" method="DeleteS3" returnvariable="retValDeleteThumbS3">
						<cfinvokeargument name="inpFileName" value="#getImage.ImageName_vch#_#_IMGTHUMBWIDTH#x#_IMGTHUMBHEIGHT#_thumb.#getImage.ImageType_vch#">
						<cfinvokeargument name="inpBucketName" value="#bucketName#">
					</cfinvoke>

					<cfif retValDeleteS3 EQ true AND retValDeleteThumbS3 EQ true>
						<cfset dataout.RXRESULTCODE = 1 />
					</cfif>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetUserStorageInfo" access="remote" output="false" hint="Get user image storage information">

		<cfargument name="inpUserId" required="false" default="#SESSION.UserId#">

		<cfset var dataout = {} />
		<cfset var userUsage = '' />
		<cfset var userStoragePlan = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE  = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.USERUSAGE = '' />
		<cfset dataout.USERSTORAGEPLAN = '' />

		<cftry>
			<!--- Get user storage usage --->
			<cfquery name="userUsage" datasource="#SESSION.DBSourceREAD#">
				SELECT
					SUM(ImageSize_bi) as UserUsage
				FROM
					simpleobjects.userimages
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
			</cfquery>

			<!--- Get user storage plan --->
			<cfquery name="userStoragePlan" datasource="#SESSION.DBSourceREAD#">
				SELECT
					MlpsImageCapacityLimit_bi
				FROM
					simplebilling.userplans
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				AND
					Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
				LIMIT
					1
			</cfquery>

			<cfif userUsage.RECORDCOUNT GT 0 AND userStoragePlan.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERUSAGE = iIf(userUsage.UserUsage EQ '', 0, userUsage.UserUsage) />
				<cfset dataout.USERSTORAGEPLAN = userStoragePlan.MlpsImageCapacityLimit_bi />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.DETAIL />
				<cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<!--- http://www.learncf.com/tutorial/26 --->
	<!--- Create a square thumbnail --->
	<cffunction name="SquareThumbnail" access="private" output="false" hint="Create a square thumbnail">
		<cfargument name="image" required="true" />
		<cfargument name="size" required="true" />
		<cfargument name="destination" required="true" />

		<cfset var half = int(arguments.size / 2) />
		<cfset var fromX = 0 />
		<cfset var fromY = 0 />

		<!--- Resize image by height if width is greater then height --->
		<cfif arguments.image.height gt arguments.image.width>

			<cfset imageResize(arguments.image, arguments.size, '') />
			<cfset fromX = arguments.image.Height / 2 - half />
			<cfset imageCrop(arguments.image, 0, fromX, arguments.size, arguments.size) />

		<!--- Resize image width if height is greater then width --->
		<cfelseif image.width gt image.height>

			<cfset imageResize(arguments.image, '', arguments.size) />
			<cfset fromY = arguments.image.Width / 2 - half />
			<cfset imageCrop(arguments.image, fromY, 0, arguments.size, arguments.size) />

		<!--- Already Square, can do a resize with no cropping --->
		<cfelse>

			<cfset imageResize(arguments.image, '', arguments.size)>
			<cfset imageCrop(arguments.image, 0, 0, arguments.size, arguments.size)>
		</cfif>

		<cfimage action="write" source="#arguments.image#" destination="#arguments.destination#"/>
	</cffunction>

</cfcomponent>
