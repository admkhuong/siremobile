<cfcomponent>

	<cffunction name="searchFileName" access="remote" hint="Search file by file name">
		<cfargument name="inpFileName" type="string" required="true">        

        <cfset var dataout = {} />
        <cfset var GetFileList = '' />
        <cfset var rsGetFileList = '' />
        <cfset var displayInfo = '' />
        <cfset var rsGetUserList = '' />
            
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout.FILELIST = [] />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="GetFileList" result="rsGetUserList">
                    SELECT 
                       PKId_bi,
                       OriginFileName_vch
                    FROM 
                        simplexuploadstage.uploads
                    WHERE
                        (
                            OriginFileName_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpFileName#%"> 
                            OR 
                            SourceFileName_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpFileName#%">
                             OR 
                            PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpFileName#">
                        )   
            </cfquery>
         
            <cfif GetFileList.RECORDCOUNT GT 0>
                <cfloop query="GetFileList" >
                    <cfset var fileItem = {
                        ID = "#GetFileList.PKId_bi#",
                        NAME = "#GetFileList.OriginFileName_vch#"
                    } />
                    <cfset dataout.FILELIST.append(fileItem) />
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>    

        <cfreturn dataout.FILELIST>

	</cffunction>


    <cffunction name="getUpdateHistory" access="remote" hint="get list update history">
        <cfargument name="inpFileRecordID" type="numeric" required="no" default="0">
        <cfargument name="inpDateStart" type="string" required="no" default="">
        <cfargument name="inpDateEnd" type="string" required="no" default="">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        
        <cfset var dataout = {}>
        <cfset var getHts = ''/>
        <cfset var tableName = "simplexuploadstage.importupdatehistories" />
        <cfset var rsCount  = {} />

        <cfset dataout.DATALIST = []>  
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["iTotalRecords"] = 0>

        <cftry>
            <cfquery name="getHts" datasource="#Session.DBSourceEBM#" >
                SELECT SQL_CALC_FOUND_ROWS 
                    iuh.ID_bi,
                    up.OriginFileName_vch,
                    up.ProcessType_ti,
                    iuh.Field_vch,
                    iuh.OldValue_vch,
                    iuh.NewValue_vch,
                    iuh.AdminUserID_int,
                    up.Created_dt AS fileDate,
                    DATE_FORMAT(iuh.Created_dt, "%Y-%m-%d %r") AS Updated,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS UserName
                FROM 
                    simplexuploadstage.uploads up
                    INNER JOIN #tableName# iuh ON up.PKId_bi = iuh.FileImportID_bi
                    LEFT JOIN simpleobjects.useraccount ua ON ua.UserID_int = iuh.AdminUserID_int
                WHERE
                    1
                    <cfif arguments.inpFileRecordID GT 0>
                        AND iuh.FileImportID_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpFileRecordID#"> 
                    </cfif> 
                    <cfif arguments.inpDateStart NEQ "" AND arguments.inpDateEnd NEQ "">
                        AND iuh.Created_dt  BETWEEN <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.inpDateStart#"> 
                        AND DATE_ADD(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.inpDateEnd#">,INTERVAL 1 DAY)   
                    </cfif>
                ORDER BY 
                    up.Created_dt DESC, iuh.Created_dt DESC
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">  
            </cfquery>
            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

            <cfloop query="#getHts#">
                <cfset var item = {
                    ID = "#getHts.ID_bi#", 
                    FILENAME = "#getHts.OriginFileName_vch#", 
                    LISTTYPE = "#getHts.ProcessType_ti#",
                    FIELD    =  "#getHts.Field_vch#",
                    OLDVALUE = "#getHts.OldValue_vch#",
                    NEWVALUE = "#getHts.NewValue_vch#",
                    FILEDATE = "#getHts.fileDate#",
                    CREATED  = "#getHts.Updated#",
                    USERNAME  = "#getHts.UserName#"

                } />
                <cfset dataout.DATALIST.append(item) />
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Get history success!'>


            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>

         <cfreturn dataout/>

    </cffunction>






    <cffunction name="logAction" access="public" hint="save the action of admin">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="inpAdminID" type="numeric" required="true">
        <cfargument name="inpTime" type="string" required="true">
        <cfargument name="inpFieldType" type="string" required="true">
        <cfargument name="inpOldValue" type="string" required="true" default="">
        <cfargument name="inpNewValue" type="string" required="true">

        <!--- <cfdump var="#arguments#" abort="true" /> --->
        <cfset var dataout = {}>
        <cfset var exeSql = {} />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="exeSql">
                INSERT INTO 
                    simplexuploadstage.importupdatehistories 
                    (
                    FileImportID_bi, 
                    Field_vch, 
                    OldValue_vch, 
                    NewValue_vch, 
                    AdminUserID_int, 
                    Created_dt
                )
                VALUES(
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.inpFileRecordID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpFieldType#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpOldValue#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpNewValue#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpAdminID#">,
                    <cfqueryparam cfsqltype="cf_sql_string" value="#arguments.inpTime#">
                )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Log history success!'>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>







</cfcomponent>



