<cfcomponent>


    <cfinclude template="/public/paths.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/bulk_insert_constants.cfm">
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/session/sire/configs/env_paths.cfm">
    <cfinclude template="/session/sire/configs/userConstants.cfm">

    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="/session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="/session/cfc/csc/inc-billing-sire.cfm">

    <cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>

    <cfparam name="DeliveryServiceComponentPath" default="" >
    <cfparam name="DeliveryServiceMCIDDMsComponentPath" default="">

    <cffunction name="ImportDataFromCSV" access="public" output="false" hint="read contact string from csv then import to db">
        <cfargument name="inpCSVFileName" TYPE="string" required="true" default="" hint="path to csv file">
        <cfargument name="inpStageTable" TYPE="string" required="true" default="" hint="table Name to import data">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="0"/>
        <cfargument name="inpUploadIdPK" type="numeric" default="0" required="true"/>

        <cfset var uploadPath = ExpandPath(UPLOAD_CSV_PATH) />


        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "invalid Input">
        <cfset dataout.ERRMESSAGE = "">
        <cfset var filePath = '' />
        <cfset var dataFile = '' />
        <cfset var insertContactString = '' />
        <cfset var checkFileContent = -1 />
        <cfset var line = '' />
        <cfset var getContactStringFromStageTable = ''/>
        <cfset var stageTableName = ''/>
        <cfset var rxInsertContactString = ''/>

        <cfset var fieldsArray = '' />
        <cfset var i = ''/>

        <cfset dataout["duplicate"] = 0/>
        <cfset var getDuplicateData = ''/>
        <cfset var arrDuplicateData = []/>
        <cfset var arrData = '' />
        <cfset var getData = '' />
        <cfset var updateDuplicateData = '' />
        <cfset var updateUploadData = '' />

        <!---
        RXRESULTCODE :
        1  - success
        -1 - errors
        -2 - can not find file or file errors
        -3 - stage table already have data
        --->

        <cfif arguments.inpCSVFileName NEQ '' AND arguments.inpStageTable NEQ ''>

        <cftry>
                <cfset stageTableName = '#arguments.inpStageTable#'/>

                <!--- Check if table is empty or not --->
                <cfquery name="getContactStringFromStageTable" datasource="#Session.DBSourceEBM#">
                    SELECT count(ContactString_vch) as TotalContactString
                    FROM #stageTableName#;
                </cfquery>

                <!--- If already have data, ignore process..
                Todo: should ask user to replace --->
                <cfif getContactStringFromStageTable.TotalContactString GT 0>
                    <cfset dataout.RXRESULTCODE = -3>
                    <cfset dataout.MESSAGE = 'Stage table already have data'>
                    <cfreturn dataout />
                </cfif>

                <cfset filePath = expandPath(UPLOAD_CSV_PATH&'/'&arguments.inpCSVFileName) />

                <!--- CHECK IF File exits on local. If not, download from S3 --->
                <!--- <cfset var s3FileUrl = _UPLOAD_S3_CSV_URL&'/'&'#arguments.inpCSVFileName#'/>


                <cfif !fileExists( filePath )>
                    <cfhttp url="#s3FileUrl#" method="get" getAsBinary="yes" path="#expandPath(UPLOAD_CSV_PATH)#" file="#arguments.inpCSVFileName#" />
                </cfif> --->

                <!--- check if file is valid --->
                <cfset dataFile = fileOpen( filePath, "read" ) />

               <cfloop condition="!fileIsEOF( dataFile )">
                    <cfset line = fileReadLine( dataFile ) />
                    <cfif line NEQ ''>
                        <cfset checkFileContent = 1 />
                    </cfif>
                    <cfbreak>
                </cfloop>

                <!--- Close the file stream to prevent locking. --->
                <cfset fileClose( dataFile ) />

                <!--- CHECK IF FILE CONTENT IS OK --->
                <cfif checkFileContent EQ -1>
                    <cfset dataout.RXRESULTCODE = -2 />
                    <cfset dataout.MESSAGE = "File content errors"/>
                    <cfreturn dataout />
                </cfif>

                <cfquery name="insertContactString" datasource="#Session.DBSourceEBM#" result="rxInsertContactString">
                    LOAD DATA LOCAL INFILE '#filePath#'
                    INTO TABLE #stageTableName#
                    CHARACTER SET UTF8
                    <!--- FIELDS TERMINATED BY ';' ENCLOSED BY '' --->
                    FIELDS TERMINATED BY <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpSeparator#"/>
                    OPTIONALLY ENCLOSED BY '"'
                    LINES TERMINATED BY '\n'
                    IGNORE <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSkipLine#"/> LINES
                    (<cfoutput>#arguments.inpColumnNames#</cfoutput>);
                </cfquery>

                <!--- --->
                <cfquery name="getData" datasource="#SESSION.DBSourceEBM#">
                    SELECT
                        COUNT(1) AS COUNT
                    FROM
                        #stageTableName#
                </cfquery>
                <cfset dataout["total"] = getData.COUNT />



                <!--- REMOVE ALL NON-NUMBERIC CHARACTER --->
                <!---REMOVE 1 in contactstring --->
                <cfstoredproc procedure="simplexuploadstage.RemoveNonNumberic" datasource="#SESSION.DBSourceEBM#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#stageTableName#">
                </cfstoredproc>

                <!--- GET DUPLICATE DATA --->
                <cfquery name="getDuplicateData" datasource="#SESSION.DBSourceEBM#">
                    SELECT PKId_bi, ContactStringCorrect_vch
                    FROM #stageTableName#
                    WHERE ContactStringCorrect_vch IN (
                            SELECT ContactStringCorrect_vch
                            FROM #stageTableName#
                            Where Status_ti = 1
                            GROUP BY ContactStringCorrect_vch
                            HAVING COUNT(PKId_bi) > 1
                        )
                    ORDER BY ContactStringCorrect_vch desc
                </cfquery>

                <!--- KEEP THE 1st duplicate item, update from 2nd --->
                <cfif getDuplicateData.RECORDCOUNT GT 0>
                    <cfset arrData = []/>
                    <cfloop query="#getDuplicateData#">
                        <cfif ArrayFind(arrData,getDuplicateData.ContactStringCorrect_vch) GT 0>
                            <cfset arrayAppend(arrDuplicateData, getDuplicateData.PKId_bi)>
                        <cfelse>
                            <cfset arrayAppend(arrData, getDuplicateData.ContactStringCorrect_vch)>
                        </cfif>
                    </cfloop>
                </cfif>

                <!--- Update Duplicate Data STATUS --->
                <cfif !arrayIsEmpty(arrDuplicateData)>
                    <cfset dataout["duplicate"] = arrayLen(arrDuplicateData) />
                    <cfquery name="updateDuplicateData" datasource="#Session.DBSourceEBM#">
                        UPDATE #stageTableName#
                        SET Status_ti =  10
                        WHERE PKId_bi IN (#ArrayToList(arrDuplicateData)#)
                    </cfquery>
                </cfif>

                <!--- UPDATE TOTAL RECORD --->
                <cfquery name="updateUploadData" datasource="#SESSION.DBSourceEBM#">
                    UPDATE
                        `simplexuploadstage`.`uploads`
                    SET
                        Total_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["total"]#"/>,
                        Duplicates_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["duplicate"]#"/>
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadIdPK#"/>
                </cfquery>

                <cfif rxInsertContactString.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = 'Load data success'/>
                </cfif>

                <!--- delete file --->
                <cffile action="delete" file="#filePath#" />

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
            </cftry>

        </cfif>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CreateContactlistStageTable" access="public" output="true" hint="Create Stage File in DB">
        <cfargument name="inpUploadIdPK" TYPE="numeric" required="true" default="" hint="PKID from 'simplexuploadstage'.'uploads' ">

        <cfargument name="inpColumnNumber" default="1"/>
        <cfargument name="inpColumnNames" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" default="1"/>
        <cfargument name="inpSeparator" default=","/>

        <cfargument name="inpSendToQueue" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.TABLENAME = "">
        <cfset var createStageTable = ''/>
        <cfset var dropStageTable = ''/>
        <cfset var tableName = "simplexuploadstage.Contactlist_stage_"&#arguments.inpUploadIdPK#>
        <cfset var createTable = '' />

        <cfset var fieldsArray = '' />
        <cfset var i = ''/>

        <cftry>

            <!---
            <cfquery name="dropStageTable" datasource="#Session.DBSourceEBM#">
                DROP TABLE IF EXISTS #tableName#
            </cfquery>
            --->

            <cfif arguments.inpColumnNumber GT 0>
                <cfset fieldsArray = listToArray(arguments.inpColumnNames, ',')/>
            </cfif>

            <cfquery name="createStageTable" datasource="#Session.DBSourceEBM#" result="createTable">
                CREATE TABLE #tableName#
                (
                    PKId_bi INT NOT NULL AUTO_INCREMENT,
                    Status_ti TINYINT(4) NULL DEFAULT 1,
                    <!---ContactString_vch VARCHAR(200) NULL,--->
                    ContactStringCorrect_vch VARCHAR(200) NULL,
                    Note_vch VARCHAR(255) NULL,
                    TimeZone_int INT(11) NULL DEFAULT 31,
                    City_vch VARCHAR(50) NULL DEFAULT "N/A",
                    State_vch VARCHAR(50) NULL DEFAULT "N/A",
                    Zip1_vch VARCHAR(20) NULL DEFAULT "N/A",
                    Zip2_vch VARCHAR(20) NULL DEFAULT "N/A",
                    Zip3_vch VARCHAR(20) NULL DEFAULT "N/A",
                    BatchId_bi BIGINT(11) NULL DEFAULT 0,
                    SendToQueue_ti TINYINT(4) NULL DEFAULT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendToQueue#"/>,
                    <!--- NPA CHAR(3) NULL DEFAULT NULL,
                    NXX CHAR(3) NOT NULL DEFAULT '',--->
                    <cfif arguments.inpColumnNumber GT 0>
                        <cfloop from="1" to="#arrayLen(fieldsArray)#" index="i">
                            <!---<cfif i NEQ arguments.inpContactStringCol>--->
                                <cfoutput>
                                    #fieldsArray[i]# VARCHAR(255) NULL,
                                </cfoutput>
                            <!---</cfif>--->
                        </cfloop>
                    </cfif>
                    PRIMARY KEY (PKId_bi)
                )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.TABLENAME = #tableName#>
            <cfset dataout.MESSAGE = 'Created table #tableName#'>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="AddToUploadTable" access="public" output="true" hint="Add new row to upload table">
        <cfargument name="inpUserId" TYPE="numeric" required="true" default="" hint="">
        <cfargument name="inpSourceFileName" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpProcessType" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpCampaignID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpSubcriberListID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpImportID" TYPE="string" required="true" default="" hint="">

        <cfargument name="inpColumnNames" default="ContactString_vch"/>
        <cfargument name="inpColumnNumber" default="1"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.PKID = 0>
        <cfset var insertToUploadTable = ''/>
        <cfset var rxInsertToUploadTable = ''/>

        <cftry>

            <cfquery name="insertToUploadTable" datasource="#Session.DBSourceEBM#" result="rxInsertToUploadTable" >
                INSERT INTO `simplexuploadstage`.`uploads`
                (
                    UserId_int,
                    SourceFileName_vch,
                    ProcessType_ti,
                    Created_dt,
                    BatchId_bi,
                    SubcriberList_bi,
                    ProcessId_vch,
                    AdminUserID_int,
                    ColumnNumber_int,
                    ColumnNames_vch,
                    ErrorsIgnore_int,
                    SendToQueue_ti
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSourceFileName#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpProcessType#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSubcriberListID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpImportID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpColumnNumber#"/>,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpColumnNames#"/>,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpErrorsIgnore#"/>,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendToQueue#"/>
                )
            </cfquery>

            <cfif rxInsertToUploadTable.GENERATED_KEY GT 0>
                <cfset dataout.RXRESULTCODE = 1>
                <cfset dataout.MESSAGE = 'Created table success'>
                <cfset dataout.PKID = rxInsertToUploadTable.GENERATED_KEY >
            </cfif>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="ProcessUpload" access="public" output="true" hint="Process upload file">
        <cfargument name="inpUserId" TYPE="numeric" required="true" default="" hint="">
        <cfargument name="inpSourceFileName" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpProcessType" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpCampaignID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpSubcriberListID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpImportID" TYPE="string" required="true" default="" hint="">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="0"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.UPLOADPKID = "0">
        <cfset var pKID = ''/>
        <cfset var AddToUploadTable = ''/>
        <cfset var rxAddToUploadTable = ''/>
        <cfset var CreateContactlistStageTable = ''/>
        <cfset var rxCreateContactlistStageTable = ''/>
        <cfset var rxImportDataFromCSV = ''/>
        <cfset var ImportDataFromCSV = ''/>

        <cftry>
            <!--- add new row to upload table --->
            <cfinvoke method="AddToUploadTable" component="session.sire.models.cfc.import-contact" returnvariable="rxAddToUploadTable">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
                <cfinvokeargument name="inpSourceFileName" value="#arguments.inpSourceFileName#"/>
                <cfinvokeargument name="inpProcessType" value="#arguments.inpProcessType#"/>
                <cfinvokeargument name="inpCampaignID" value="#arguments.inpCampaignID#"/>
                <cfinvokeargument name="inpSubcriberListID" value="#arguments.inpSubcriberListID#"/>
                <cfinvokeargument name="inpImportID" value="#arguments.inpImportID#"/>

                <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                <cfinvokeargument name="inpErrorsIgnore" value="#arguments.inpErrorsIgnore#"/>

                <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
            </cfinvoke>

            <!--- If Add success --->
            <cfif rxAddToUploadTable.RXRESULTCODE EQ 1>
                <cfset pKID = rxAddToUploadTable.PKID>
                <cfset dataout.UPLOADPKID = pKID>
                <!--- create stage table --->
                <cfinvoke method="CreateContactlistStageTable" component="session.sire.models.cfc.import-contact" returnvariable="rxCreateContactlistStageTable">
                    <cfinvokeargument name="inpUploadIdPK" value="#pKID#"/>

                    <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                    <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                    <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                    <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>

                    <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
                </cfinvoke>

                <cfif rxCreateContactlistStageTable.RXRESULTCODE GT 0>
                    <!--- Import data from csv --->
                    <cfinvoke method="ImportDataFromCSV" component="session.sire.models.cfc.import-contact" returnvariable="rxImportDataFromCSV">
                        <cfinvokeargument name="inpCSVFileName" value="#arguments.inpSourceFileName#"/>
                        <cfinvokeargument name="inpStageTable" value="#rxCreateContactlistStageTable.TABLENAME#"/>

                        <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                        <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                        <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                        <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>
                        <cfinvokeargument name="inpSkipLine" value="#arguments.inpSkipLine#"/>
                        <cfinvokeargument name="inpUploadIdPK" value="#pKID#"/>

                    </cfinvoke>

                    <cfif rxImportDataFromCSV.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1 />
                    <cfelse>
                        <cfset dataout.MESSAGE = rxImportDataFromCSV.MESSAGE />
                        <cfset dataout.ERRMESSAGE = rxImportDataFromCSV.ERRMESSAGE />
                    </cfif>
                <cfelse>
                    <cfset dataout.MESSAGE = 'Can not create stage table (#pKID#)'/>
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = 'Can not insert to upload table'/>
            </cfif>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="SaveCSVContact" access="remote" hint="Save CSV contact">
        <cfargument name="inpUserId" required="true">
        <cfargument name="inpSubcriberListID" default="0">
        <cfargument name="inpCampaignID" default="0">
        <cfargument name="inptProcessType" default="1">
        <cfargument name="inpImportID" default="">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="0"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>

        <cfset var dataout = {} />
        <cfset var retValUploadCSV = '' />
        <cfset var uploadResult = '' />
        <cfset var userStorageInfo = '' />
        <cfset var checkFilename = '' />
        <cfset var uploadPath = ExpandPath(UPLOAD_CSV_PATH) />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.FILENAME = ''/>
        <cfset var processType = 1>
        <cfset var RXProcessUpload = ''/>
        <cfset var uploadPKID = ''/>
        <cfset var RXCleanData = ''/>
        <cfset var RXImportContactToSubcriberList = ''/>

        <cfset dataout.DUPLICATE =  0/>
        <cfset dataout.INVALID =  0/>
        <cfset dataout.ERRORS =  0/>
        <cfset dataout.TOTAL =  0/>
        <cfset dataout.UNIQUE =  0/>
        <cfset var updateUploadStatus = ''/>
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var loopTime = 0 />
        <cfset var retValUploadFile = '' />
        <cfset var rxUpdateUploadStatus = '' />

        <cfif arguments.inptProcessType EQ PROCESSTYPE_SUBSCRIBER_LIST>
            <cfset arguments.inpSendToQueue = 1/>
        </cfif>

        <cftry>

            <cfset tickBegin = GetTickCount()>

            <cfset processType = arguments.inptProcessType>

            <cfif processType EQ 1>
            <cfelseif arguments.inpCampaignID GT 2>
                <cfset arguments.inpSubcriberListID = 0/>
            </cfif>

            <cfif NOT findNoCase('ContactString_vch', arguments.inpColumnNames)>
                <cfthrow message="Column names much contain ContactString_vch" detail="Column names much contain ContactString_vch" />
            </cfif>

            <cfif arrayLen(REMatchNoCase('[^,a-zA-Z0-9_]|[,_]$', arguments.inpColumnNames)) GT 0>
                <cfthrow message="Column names much not contain spaces or any special characters except underscore(_)." detail="Column name much not contain spaces or any special characters except underscore (_)." />
            </cfif>

            <cfif NOT DirectoryExists(uploadPath)>
                <cfdirectory action = "create" directory="#uploadPath#" mode="770" />
            </cfif>
            <!--- Upload file to server --->
            <cffile action="upload" fileField="file" destination="#uploadPath#" accept="text/csv,application/vnd.ms-excel,application/csv,text/plain" nameconflict="MakeUnique" result="uploadResult" mode="770">
            <cfif !arrayContains(_ACCEPTEDCSVTYPE, lCase(uploadResult.clientfileext))>
                <!--- <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" /> --->
                <cfthrow message="File type not accepted!" detail="File type not accepted!" />
            </cfif>

            <!--- Check filesize, client already handle this but check again for security --->
            <cfif uploadResult.filesize GT _CSVMAXSIZE>
                <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
                <cfthrow message="This file is too large! Max file size is 300MB" />
            </cfif>

            <!--- <cfdump var="#uploadResult.serverdirectory#/contact-string-rwerwerwer.#lCase(uploadResult.clientfileext)#" abort="true"> --->
            <cfset var newName = "contact-string#arguments.inpUserId#-#DateFormat(NOW(), "mmm-dd-yyyy")#-#TimeFormat(now(), "HH-mm-ss")#.#lCase(uploadResult.clientfileext)#" />
            <cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#newName#" attributes="normal" >

            <cfexecute name="mac2unix"
                arguments="#uploadPath#/#newName#"
                timeout="10">
            </cfexecute>

            <!--- Upload to S3 --->
<!---             <cfinvoke method="UploadCSVToS3" returnvariable="retValUploadFile">
                <cfinvokeargument name="inpFullFileName" value="#newName#">
                <cfinvokeargument name="inpLocalPath" value="#uploadResult.serverdirectory#/">
                <cfinvokeargument name="inpFileSize" value="#uploadResult.filesize#">
                <cfinvokeargument name="inpFileType" value="#uploadResult.clientfileext#">
                <cfinvokeargument name="inpPlainFileName" value="#uploadResult.serverfilename#">
            </cfinvoke>
            <cfif retValUploadFile.RXRESULTCODE NEQ 1>
                <cfthrow MESSAGE="#retValUploadFile.MESSAGE#" TYPE="Any" detail="#retValUploadFile.ERRMESSAGE#" errorcode="-1">
            </cfif>  --->

            <!--- LOAD DATA TO TABLE --->
            <cfinvoke component="session.sire.models.cfc.import-contact" method="ProcessUpload" returnvariable="RXProcessUpload">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                <cfinvokeargument name="inpSourceFileName" value="#newName#">
                <cfinvokeargument name="inpProcessType" value="#processType#">
                <cfinvokeargument name="inpCampaignID" value="#arguments.inpCampaignID#">
                <cfinvokeargument name="inpSubcriberListID" value="#arguments.inpSubcriberListID#">
                <cfinvokeargument name="inpImportID" value="#arguments.inpImportID#">

                <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>
                <cfinvokeargument name="inpSkipLine" value="#arguments.inpSkipLine#"/>
                <cfinvokeargument name="inpErrorsIgnore" value="#arguments.inpErrorsIgnore#"/>

                <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
            </cfinvoke>

            <cfset uploadPKID = RXProcessUpload.UPLOADPKID/>

            <cfif RXProcessUpload.RXRESULTCODE NEQ 1>

                <cfquery name="updateUploadStatus" datasource="#SESSION.DBSourceEBM#" result="rxUpdateUploadStatus">
                    UPDATE
                        simplexuploadstage.uploads
                    SET
                        Status_ti = -99,
                        ErrorDetails_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RXProcessUpload.MESSAGE#"/>
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#uploadPKID#"/>
                </cfquery>

                <cfthrow MESSAGE="#RXProcessUpload.MESSAGE#" TYPE="Any" detail="#RXProcessUpload.ERRMESSAGE#" errorcode="-2">
            </cfif>

            <cfquery name="updateUploadStatus" datasource="#SESSION.DBSourceEBM#">
                UPDATE simplexuploadstage.uploads
                SET Status_ti = 1,
                OriginFileName_vch =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#uploadResult.serverfile#"/>
                WHERE PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#uploadPKID#"/>
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>



    <cffunction name="AddToQueue" access="public" output="true" hint="Add a single request to the dial queue">
        <cfargument name="inpBatchId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpUserId" required="yes" default="0">

        <cfargument name="inpContactTypeId" required="no" default="3" hint="1=Phone number, 2=eMail, 3=SMS">
        <cfargument name="inpInternational" required="no" default="0" hint=" 1 will flag this number as international">
        <cfargument name="inpValidateScheduleActive" required="no" default="0" hint="1 will halt message if time is currently outside of the pre-defined campaigns's scheduled range">
        <cfargument name="inpTimeZone" required="no" default="PST">
        <cfargument name="inpCompany" required="no" default="">
        <cfargument name="inpFirstName" required="no" default="">
        <cfargument name="inpLastName" required="no" default="">
        <cfargument name="inpAddress" required="no" default="">
        <cfargument name="inpAddress1" required="no" default="">
        <cfargument name="inpCity" required="no" default="">
        <cfargument name="inpState" required="no" default="">
        <cfargument name="inpZipCode" required="no" default="">
        <cfargument name="inpCountry" required="no" default="">
        <cfargument name="inpUserDefinedKey" required="no" default="">
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">
        <cfargument name="inpBlockDuplicates" required="no" default="0">
        <cfargument name="inpSkipNumberValidations" required="no" default="0">
        <cfargument name="inpSkipLocalUserDNCCheck" required="no" default="0">
        <cfargument name="inpDistributionProcessId" required="no" default="0">
        <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts"/>
        <cfargument name="inpXMLControlString" required="no" default="" hint="Allow custom XMLControlString to be passed in - still must be valid user/batch but will override the default XMLControlString in the Batch">
        <cfargument name="DebugAPI" required="no" default="0">
        <cfargument name="inpShortCode" required="no" default=""/>

        <!--- Change flags for special processing --->
        <cfset var inpPostToQueueForWebServiceDeviceFulfillment = 1 />

        <cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />

        <!--- SOAP, REST, FORM, or URL --->
        <cfset var dataout = {} />
        <cfset var ServiceRequestdataout = '0' />
        <cfset var INPSCRIPTID = "-1" />
        <cfset var inpLibId = "-1" />
        <cfset var inpEleId = "-1" />
        <cfset var inpLimitDistribution = 0 />
        <cfset var ABTestingBatches = "" />
        <cfset var BlockGroupMembersAlreadyInQueue = 1 />
        <cfset var QueuedScheduledDate = "NOW()" />
        <cfset var DebugStr = "Start" />
        <cfset var NEXTBATCHID = -1 />
        <cfset var CurrUserId = -1 />
        <cfset var CurrTZ = "31" />
        <cfset var inpLibId = "0" />
        <cfset var inpEleId = "0" />
        <cfset var inpScriptId = "0" />
        <cfset var INPGROUPID = "0" />
        <cfset var CURRTZVOICE = "-1" />
        <cfset var COUNTQUEUEDUPVOICE = 0 />
        <cfset var COUNTQUEUEDUPEMAIL = 0 />
        <cfset var COUNTQUEUEDUPSMS = 0 />
        <cfset var LASTQUEUEDUPID = 0 />
        <cfset var EstimatedCost = 0.00 />
        <cfset var SMSONLYXMLCONTROLSTRING_VCH = "" />
        <cfset var DBSourceEBM = "BishopDev"/>
        <cfset var ESIID = "-15"/>
        <cfset var VariableNamesArray = ArrayNew(1) />
        <cfset var LocalGMTRelative = -8 />
        <cfset var AfterHoursBlocked = 0 />
        <cfset var CurrGMTRelative = -8 />
        <cfset var LocalRelative = 0 />
        <cfset var CurrTime = NOW() />
        <cfset var ServiceInputFlag = 1 />
        <cfset var ENA_Message = "pdc-AddToQueue Debugging Info" />
        <cfset var SubjectLine = "SimpleX SMS API Result - pdc-AddToQueue" />
        <cfset var TroubleShootingTips="See CatchMessage_vch in this log for details" />
        <cfset var ErrorNumber="1114" />
        <cfset var AlertType="1" />
        <cfset var InsertToAPILog = '' />
        <cfset arguments.inpContactString = TRIM(arguments.inpContactString) />

        <cfset var BLOCKEDBYDNC = 0 />
        <cfset var SMSINIT = 0 />
        <cfset var SMSINITMESSAGE = "" />

        <cfset var whichField = 0 />
        <cfset var GetTZInfo = 0 />
        <cfset var GetRawDataTZs = 0 />
        <cfset var GetRawDataCount = 0 />
        <cfset var InsertToErrorLog = 0 />

        <cfset var temp = '' />
        <cfset var RowCountVar = 0>
        <cfset var VOICEONLYXMLCONTROLSTRING_VCH = "">
        <cfset var GetUserDNCFroServiceRequest = ''/>
        <cfset var UserLocalDNCBlocked = 0 />

        <cfset var inpIRETypeAdd = IREMESSAGETYPE_IRE_BULK_TRIGGERED />

        <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
        <cfset var InvalidMCIDXML   = '' />
        <cfset var DDMBuffA = '' />
        <cfset var DDMBuffB = '' />
        <cfset var DDMBuffC = '' />
        <cfset var DDMPos1  = '' />
        <cfset var DDMPos2  = '' />
        <cfset var DDMReultsArray   = '' />
        <cfset var RawDataFromDB    = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var DebugStr = '' />
        <cfset var selectedElements = '' />
        <cfset var CURRVAL  = '' />
        <cfset var DDMSWITCHReultsArray = '' />
        <cfset var CurrSwitchValue  = '' />
        <cfset var CurrSwitchQIDValue   = '' />
        <cfset var CurrSwitchBSValue    = '' />
        <cfset var CaseMatchFound   = '' />
        <cfset var CurrCaseReplaceString    = '' />
        <cfset var XMLFDDoc = '' />
        <cfset var selectedElementsII   = '' />
        <cfset var OutToDBXMLBuff   = '' />
        <cfset var XMLDefaultDoc    = '' />
        <cfset var DDMCONVReultsArray   = '' />
        <cfset var CurrXMLConversionType    = '' />
        <cfset var AccountnumberTwoAtATimeBuffer    = '' />
        <cfset var CurrAccountNum   = '' />
        <cfset var TwoDigitLibrary  = '' />
        <cfset var SingleDigitLibrary   = '' />
        <cfset var PausesLibrary    = '' />
        <cfset var PausesStyle  = '' />
        <cfset var ISDollars    = '' />
        <cfset var ISDecimalPlace   = '' />
        <cfset var DecimalLibrary   = '' />
        <cfset var MoneyLibrary = '' />
        <cfset var HundredsLibrary  = '' />
        <cfset var CurrDynamicAmount    = '' />
        <cfset var IncludeDayOfWeek = '' />
        <cfset var IncludeTimeOfWeek    = '' />
        <cfset var DayofWeekElement = '' />
        <cfset var MonthElement = '' />
        <cfset var DayofMonthElement    = '' />
        <cfset var YearElement  = '' />
        <cfset var DSTimeElement    = '' />
        <cfset var TwoDigitElement  = '' />
        <cfset var CurrMessageXMLTransactionDate    = '' />
        <cfset var CurrTransactionDate  = '' />
        <cfset var CurrXMLConvDesc  = '' />
        <cfset var CurrTransactionVar   = '' />
        <cfset var CurrXMLConversionMessage = '' />
        <cfset var CurrDSHour   = '' />
        <cfset var CurrDSMinute = '' />
        <cfset var CurrCentAmountBuff   = '' />
        <cfset var CurrMessageXMLDecimal    = '' />
        <cfset var CurrDynamicAmountBuff    = '' />
        <cfset var BuffStr  = '' />
        <cfset var IsDecimal    = '' />
        <cfset var CurrDDMVar   = '' />
        <cfset var CurrDDMSWITCHVar = '' />
        <cfset var CurrFDXML    = '' />
        <cfset var CurrWildcardXML  = '' />
        <cfset var CurrDefaultXML   = '' />
        <cfset var CurrDDMCONVVar   = '' />
        <cfset var AccountnumberIndex   = '' />
        <cfset var ixi  = '' />
        <cfset var iixi = '' />
        <cfset var GetCustomFields = '' />
        <cfset var GetShortCodeData = '' />
        <cfset var GetRawData = '' />
        <cfset var inpCarrier = ""/>
        <cfset var inpShortCode = arguments.inpShortCode/>
        <cfset var inpKeyword = ""/>
        <cfset var inpTransactionId = ""/>
        <cfset var inpServiceId = ""/>
        <cfset var inpXMLDATA = ""/>
        <cfset var inpOverRideInterval = "0"/>
        <cfset var inpTimeOutNextQID = "0"/>
        <cfset var inpQAToolRequest = "0"/>
        <cfset var PassOnFormData = ''>
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var loopTime  = 0 />
        <cfset var RetValGetXML = "">
        <cfset var NEWUUID = "" />
        <cfset var GetTimeZoneInfoData = "">
        <cfset var CheckDuplicateContactStringInQueue = '' />

        <!--- Then include main processing --->

        <cfset var inpeMailHTMLTransformed = '' />
        <cfset var PreviewFileName = '' />
        <cfset var ScriptProcessedOutput = ''/>
        <cfset var inpSkipLocalUserDNCCheck = arguments.inpSkipLocalUserDNCCheck/>

         <!---
        Positive is success
        1 = OK
        2 =
        3 =
        4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

         --->

        <cfoutput>

            <!--- Start timing test --->
            <!--- <cfset tickBegin = GetTickCount()> --->

            <cfif TRIM(inpScheduleOffsetSeconds) NEQ 0 AND ISNUMERIC(TRIM(inpScheduleOffsetSeconds))>
                <cfset QueuedScheduledDate = "DATE_ADD(NOW(), INTERVAL #inpScheduleOffsetSeconds# SECOND)">
            </cfif>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, inpBatchId, CURRTZVOICE, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, CURRTZ, LASTQUEUEDUPID, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpBatchId", "#inpBatchId#") />
            <cfset QuerySetCell(dataout, "CURRTZ", "#inpTimeZone#") />
            <cfset QuerySetCell(dataout, "CURRTZVOICE", "#CURRTZVOICE#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
            <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") />
            <cfset QuerySetCell(dataout, "LASTQUEUEDUPID", "#LASTQUEUEDUPID#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />


            <cftry>

                <cfif inpContactString EQ "">
                    <cfthrow MESSAGE="Invalid inpContactString Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

                <!--- Cleanup SQL injection --->

                <cfif !isnumeric(inpBatchId) OR inpBatchId EQ 0 OR inpBatchId EQ "">
                    <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-6">
                </cfif>

                <cfset NEXTBATCHID = inpBatchId>

                <cfset CurrUserId = inpUserId>

                <cfif CurrUserId EQ "" OR CurrUserId LT 1>
                    <cfthrow MESSAGE="Invalid Authentication Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

                <!--- Even though no group is specified the default group of 0 is used.--->
                <cfif inpBlockDuplicates GT 0>
                    <cfquery name="CheckDuplicateContactStringInQueue" datasource="#session.DBSourceREAD#">
                        SELECT
                            simplequeue.contactqueue.contactstring_vch,
                            simplequeue.contactqueue.DTSStatusType_ti
                        FROM
                            simplequeue.contactqueue
                        WHERE
                            simplequeue.contactqueue.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                        AND
                            simplequeue.contactqueue.batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                        AND
                            simplequeue.contactqueue.groupid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
                        AND
                            simplequeue.contactqueue.contactstring_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
                        AND
                            simplequeue.contactqueue.DTSStatusType_ti != #CONTACTQUEUE_PAUSED#
                    </cfquery>
                    <cfif CheckDuplicateContactStringInQueue.RecordCount GT 0>
                        <cfthrow type="Application" errorcode="#STAGE_ERROR_DUPLICATED_QUEUE#" message="ContactString already in queue. Status - #CheckDuplicateContactStringInQueue.DTSStatusType_ti#"/>
                    </cfif>
                </cfif>

                <cfset BlockGroupMembersAlreadyInQueue = 0>

                <cfif inpSkipLocalUserDNCCheck EQ 0>
                    <!--- Session User DNC --->
                    <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
                    <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
                        SELECT
                            COUNT(oi.ContactString_vch) AS TotalCount
                        FROM
                            simplelists.optinout AS oi
                        WHERE
                            OptOut_dt IS NOT NULL
                        AND
                            OptIn_dt IS NULL
                        AND
                            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                        AND
                            oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                    </cfquery>

                    <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>
                        <cfthrow type="Application" errorcode="#STAGE_ERROR_BLOCKED#" message="Blocked by User DNC" detail="Blocked by User DNC"/>
                    </cfif>

                    <cfset inpSkipLocalUserDNCCheck = 1/>
                </cfif>

                    <!--- Other security checks --->
                    <cfinvoke method="GetXMLControlString" returnvariable="RetValGetXML">
                        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
                        <cfinvokeargument name="REQSESSION" value="0">
                        <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">
                    </cfinvoke>

                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <!--- Verify all numbers are actual numbers --->
                    <cfif !isnumeric(inpBatchId)>
                        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>


                    <!--- Allow users to create their own control strings --->
                    <cfif LEN(arguments.inpXMLControlString) GT 0>
                        <cfset RetValGetXML.XMLCONTROLSTRING = arguments.inpXMLControlString />
                    </cfif>

                    <cfset NEXTBATCHID = "#inpBatchId#">


                   <!--- Auto create schedule with defaults or specified values--->
                   <!--- Validate Schedule is elligible --->

                    <cfif arguments.inpUseInputTimeZone EQ 0>
                        <cfset CurrTZ = "31"> <!--- Default to PST--->

                        <cfswitch expression="#inpContactTypeId#">

                            <cfcase value="3"> <!--- SMS --->
                                <cfif inpContactString NEQ "" >

                                    <cfif inpSkipNumberValidations EQ 0 >

                                        <!---Find and replace all non numerics except P X * #--->
                                        <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

                                        <!--- Clean up where start character is a 1 --->
                                        <cfif LEFT(inpContactString, 1) EQ "1">
                                            <cfset arguments.inpContactString = RemoveChars(inpContactString, 1, 1)>
                                        </cfif>

                                    </cfif>

                                    <!--- Is International number--->
                                    <cfif inpInternational GT 0>
                                        <!--- Validate allowed to / Security--->

                                        <!--- Validate 011 / Country code --->
                                        <cfif LEFT(inpContactString, 3) NEQ "011" >
                                            <cfthrow MESSAGE="International number must specify 011 and country code " TYPE="Any" detail="" errorcode="-4">
                                        </cfif>

                                        <cfset CurrTZ = "0" >

                                    <cfelse>
                                        <!--- Validate proper 10 digit North American phone number--->

                                        <cfif LEN(LEFT(inpContactString, 10)) LT 10 OR !ISNUMERIC(LEFT(inpContactString, 10))>
                                            <cfthrow MESSAGE="Not a valid 10 digit North American phone number" TYPE="Any" detail="" errorcode="-3">
                                        </cfif>

                                        <!--- Check for supplied Time Zone --->
                                        <cfswitch expression="#UCASE(inpTimeZone)#">

                                            <cfcase value="UNK"><cfset CurrTZ = "0"></cfcase>
                                            <cfcase value="GUAM"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "38"> <cfelse> <cfset CurrTZ = "37"> </cfif></cfcase>
                                            <cfcase value="SAMOA"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "35"> <cfelse> <cfset CurrTZ = "34"> </cfif></cfcase>
                                            <cfcase value="HAWAII"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                            <cfcase value="HAST"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                            <cfcase value="ALASKA"><cfset CurrTZ = "32"></cfcase>
                                            <cfcase value="PACIFIC"><cfset CurrTZ = "31"></cfcase>
                                            <cfcase value="MOUNTAIN"><cfset CurrTZ = "30"></cfcase>
                                            <cfcase value="CENTRAL"><cfset CurrTZ = "29"></cfcase>
                                            <cfcase value="EASTERN"><cfset CurrTZ = "28"></cfcase>
                                            <cfcase value="PDT"><cfset CurrTZ = "31"></cfcase>
                                            <cfcase value="PST"><cfset CurrTZ = "31"></cfcase>
                                            <cfcase value="MDT"><cfset CurrTZ = "30"></cfcase>
                                            <cfcase value="MST"><cfset CurrTZ = "30"></cfcase>
                                            <cfcase value="CDT"><cfset CurrTZ = "29"></cfcase>
                                            <cfcase value="CST"><cfset CurrTZ = "29"></cfcase>
                                            <cfcase value="EDT"><cfset CurrTZ = "28"></cfcase>
                                            <cfcase value="EST"><cfset CurrTZ = "28"></cfcase>

                                            <cfdefaultcase>
                                                <!--- DO lookup--->
                                                <cfset CurrTZ = "-1">

                                                <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                                    SELECT
                                                        CASE cx.T_Z
                                                              WHEN 14 THEN 37
                                                              WHEN 11 THEN 34
                                                              WHEN 10 THEN 33
                                                              WHEN 9 THEN 32
                                                              WHEN 8 THEN 31
                                                              WHEN 7 THEN 30
                                                              WHEN 6 THEN 29
                                                              WHEN 5 THEN 28
                                                              WHEN 4 THEN 27
                                                             END AS TimeZone,
                                                        CASE
                                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                                              ELSE 0
                                                              END  AS CellFlag
                                                    FROM
                                                        MelissaData.FONE AS fx JOIN
                                                        MelissaData.CNTY AS cx ON
                                                        (fx.FIPS = cx.FIPS)
                                                     WHERE
                                                        CONCAT(fx.NPA,fx.NXX) = '#LEFT(inpContactString,6)#'
                                                </cfquery>

                                                <cfif GetTZInfo.RecordCount GT 0>
                                                    <cfset CurrTZ = "#GetTZInfo.TimeZone#">
                                                <cfelse>
                                                   <cfset CurrTZ = "-1">
                                                </cfif>

                                            </cfdefaultcase>

                                        </cfswitch>

                                        <cfif CurrTZ EQ "-1" >
                                            <cfthrow MESSAGE="Could not find time zone and no valid default value provided" TYPE="Any" detail="" errorcode="-5">
                                        </cfif>

                                    </cfif>

                                </cfif>
                            </cfcase>

                        </cfswitch>
                    <cfelse>
                        <cfset CurrTZ = arguments.inpTimeZone/>
                    </cfif>

                        <!--- Add contact to list - group and update data if GroupId specified belongs to current userid --->

                        <cfif inpContactString NEQ "">

                            <cfset ServiceRequestdataout =  QueryNew("  UserId_int,
                                                                        ContactTypeId_int,
                                                                        ContactString_vch,
                                                                        TimeZone_int,
                                                                        Company_vch,
                                                                        FirstName_vch,
                                                                        LastName_vch,
                                                                        Address_vch,
                                                                        Address1_vch,
                                                                        City_vch,
                                                                        State_vch,
                                                                        ZipCode_vch,
                                                                        Country_vch,
                                                                        UserDefinedKey_vch
                                                                         ", "VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar")>

                            <cfset QueryAddRow(ServiceRequestdataout) />


                            <cfloop collection="#FORM#" item="whichField">

                               <!--- Only add fields not already on list of standard fields --->
                               <cfif NOT structKeyExists(ServiceRequestdataout, '#whichField#')  >

                                    <!---<cfoutput>#whichField# = #FORM[whichField]#</cfoutput><br>--->

                                    <cfset VariableNamesArray = ArrayNew(1)>
                                    <cfset ArraySet(VariableNamesArray, 1, 1, "#FORM[whichField]#")>
                                    <cfset QueryAddColumn(ServiceRequestdataout, whichField, "VarChar", VariableNamesArray)>

                                </cfif>

                            </cfloop>



                            <cfset QuerySetCell(ServiceRequestdataout, "UserId_int", "#Session.UserId#") />
                            <cfset QuerySetCell(ServiceRequestdataout, "ContactString_vch", "#inpContactString#") />
                            <cfset QuerySetCell(ServiceRequestdataout, "ContactTypeId_int", "#inpContactTypeId#") />
                            <cfset QuerySetCell(ServiceRequestdataout, "TimeZone_int", "#CurrTZ#") />

                            <cfset QuerySetCell(ServiceRequestdataout, "Company_vch", '#LEFT(inpCompany, 500)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "FirstName_vch", '#LEFT(inpFirstName, 90)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "LastName_vch", '#LEFT(inpLastName, 90)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "Address_vch", '#LEFT(inpAddress, 250)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "Address1_vch", '#LEFT(inpAddress1, 100)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "City_vch", '#LEFT(inpCity, 100)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "State_vch", '#LEFT(inpState, 50)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "ZipCode_vch", '#LEFT(inpZipCode, 20)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "Country_vch", '#LEFT(inpCountry, 100)#') />
                            <cfset QuerySetCell(ServiceRequestdataout, "UserDefinedKey_vch", '#LEFT(inpUserDefinedKey, 2048)#') />

                        </cfif>

                    <cfset DebugStr = DebugStr & " GetRawDataCount">

                    <!--- Verify all numbers are actual numbers --->
                    <cfquery name="GetRawDataCount" dbTYPE="query">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            ServiceRequestdataout
                    </cfquery>

                    <cfif GetRawDataCount.TOTALCOUNT EQ "">

                       <cfthrow MESSAGE="Distribution Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) No data elligable to send - check your contact strings." errorcode="-5">

                    </cfif>

                    <cfset var TotalUnitCost = GetRawDataCount.TOTALCOUNT/>

                    <!--- Voice Credit surcharge --->
                    <cfif inpContactTypeId EQ 1>
                        <cfset TotalUnitCost = TotalUnitCost * 3 />
                    </cfif>

                    <!--- SMS Credit surcharge --->
                    <cfif inpContactTypeId EQ 3>
                        <cfset TotalUnitCost = TotalUnitCost * 2 />
                    </cfif>

                    <!--- Make sure it is ok with billing - only charge when dial complete --->
                    <!---component="billing"--->
                    <!--- MINHHTN COMMENT                     <cfinvoke
                     method="ValidateBilling"
                     returnvariable="local.RetValBillingData">
                        <cfinvokeargument name="inpCount" value="#TotalUnitCost#"/>
                        <cfinvokeargument name="inpUserId" value="#CurrUserId#"/>
                        <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                    </cfinvoke>

                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) #RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                    </cfif>
                     --->

                    <!---
                    <!--- This is inserted into Queue --->
                    <cfset EstimatedCost = RetValBillingData.ESTIMATEDCOSTPERUNIT>
                     --->

                    <!--- Changes which data source to process for each channel--->
                    <cfset ServiceInputFlag = 1>


                    <!--- Insert Voice, eMAil, SMS into dial queue--->


                    <!--- <cfthrow MESSAGE="Debug Error" TYPE="Any" detail="Made It Here" errorcode="-5">         --->

                    <cfset DebugStr = DebugStr & " Load Start">

                    <cfinclude template="/session/cfc/Includes/LoadQueueSMS_Dynamic.cfm">

                    <cfset DebugStr = DebugStr & " Load Finished">

                   <!--- <cfset tickEnd = GetTickCount()>
                   <cfset loopTime = tickEnd - tickBegin>  --->

                    <cfset dataout =  QueryNew("RXRESULTCODE, inpBatchId, CURRTZVOICE, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, CURRTZ, LASTQUEUEDUPID, REQUESTUUID, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpBatchId", "#inpBatchId#") />
                    <cfset QuerySetCell(dataout, "CURRTZ", "#CurrTZ#") />
                    <cfset QuerySetCell(dataout, "CURRTZVOICE", "#CURRTZVOICE#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
                    <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") />
                    <cfset QuerySetCell(dataout, "LASTQUEUEDUPID", "#LASTQUEUEDUPID#") />
                    <cfset QuerySetCell(dataout, "REQUESTUUID", "#NEWUUID#") />
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfif DebugAPI GT 0>
                        <cfset QuerySetCell(dataout, "MESSAGE", "DebugAPI=#DebugAPI# #DebugStr#") />
                    <cfelse>
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                    </cfif>
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

            <cfcatch TYPE="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout =  QueryNew("RXRESULTCODE, inpContactString, inpBatchId, inpTimeZone, CURRTZVOICE, COUNTQUEUEDUPVOICE, COUNTQUEUEDUPEMAIL, COUNTQUEUEDUPSMS, CURRTZ, LASTQUEUEDUPID, REQUESTUUID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "inpContactString", "#inpContactString#") />
                <cfset QuerySetCell(dataout, "inpBatchId", "#inpBatchId#") />
                <cfset QuerySetCell(dataout, "inpTimeZone", "#inpTimeZone#") />
                <cfset QuerySetCell(dataout, "CURRTZ", "#CurrTZ#") />
                <cfset QuerySetCell(dataout, "CURRTZVOICE", "#CURRTZVOICE#") />
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPVOICE", "#COUNTQUEUEDUPVOICE#") />
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPSMS", "#COUNTQUEUEDUPSMS#") />
                <cfset QuerySetCell(dataout, "COUNTQUEUEDUPEMAIL", "#COUNTQUEUEDUPEMAIL#") />
                <cfset QuerySetCell(dataout, "LASTQUEUEDUPID", "#LASTQUEUEDUPID#") />
                <cfset QuerySetCell(dataout, "REQUESTUUID", "#NEWUUID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfif DebugAPI GT 0>
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#") />
                <cfelse>
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                </cfif>
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

            </cfcatch>

            </cftry>

        </cfoutput>


        <cfreturn dataout />


    </cffunction>



    <cffunction name="ImportContactToSubcriberList" access="public" hint="Read and Import data to subcriber or contact queue">
        <cfargument name="inpFileRecordID" type="string" required="true" >
        <cfargument name="inpSubcriberListID" type="numeric">
        <cfargument name="inpUserId" type="numeric">


        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cfset var checkTableExists = false />
        <cfset var getData = {} />

        <cfset var inpTableName = "simplexuploadstage.Contactlist_stage_#arguments.inpFileRecordID#" />

        <cftry>
            <cfquery name="getData" datasource="#SESSION.DBSourceEBM#">
                SELECT
                    PKId_bi,
                    Status_ti,
                    ContactString_vch
                FROM
                    #inpTableName#
                WHERE
                    Status_ti = 1
                LIMIT 10
            </cfquery>

            <cfloop query="getData">

                <cfset var contactString = getData.ContactString_vch />
                    <cfinvoke component="session.sire.models.cfc.import-contact" method="AddContactStringToList">
                        <cfinvokeargument name="INPCONTACTSTRING" value="#contactString#" />
                        <cfinvokeargument name="INPCONTACTTYPEID" value="3" />
                        <cfinvokeargument name="INPGROUPID" value="#arguments.inpSubcriberListID#" />
                        <cfinvokeargument name="INPUSERID" value="#arguments.inpUserId#" />
                    </cfinvoke>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>



    <cffunction name="CleanData" access="public" output="true" hint="Clearn up data">
        <cfargument name="inpFileRecordID" type="string" required="true" >
        <cfargument name="inpDoModValue" type="numeric" default="0" required="true" >
        <cfargument name="inpMod" type="numeric" default="1" required="true" >
        <cfargument name="inpNumberOfDialsToRead" type="numeric" default="1" required="true" >

        <cfset var dataout = {} />
        <cfset var getData = {} />
        <cfset var getUniqueData = {}/>
        <cfset var getInvalidData = {}/>
        <cfset var updateUploadData = {}/>
        <cfset var updateDuplicateData = {}/>
        <cfset var arrInvalidData = []/>
        <cfset var updateInvalidData = ''/>
        <cfset var arrData = []/>
        <cfset var arrDuplicateData = []/>
        <cfset var arrCorrectData = []/>
        <cfset var itemCorrectData = {}/>
        <cfset var updateCorrectData = ''/>
        <cfset var getDuplicateData = ''/>
        <cfset var listTZData = []/>
        <cfset var itemData = ''/>
        <cfset var rxCheckValidPhoneNumber = ''/>

        <cfset var currNPA = ''>
        <cfset var currNXX = ''>
        <cfset var getTZInfo = ''/>
        <cfset var tZInfoData = ''/>
        <cfset var currTZInfo = 0>

        <cfset var getDataValid = '' />
        <cfset var HoldProcessingRecord = ''/>
        <cfset var UpdateResults = ''/>
        <cfset var getDuplicateData=''>
        <cfset var updateDuplicateData=''>



        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout["duplicate"] = 0 />
        <cfset dataout["unique"] = 0 />
        <cfset dataout["total"] = 0 />
        <cfset dataout["errors"] = 0 />
        <cfset dataout["invalid"] = 0 />

        <cfset var tableName = "simplexuploadstage.Contactlist_stage_#arguments.inpFileRecordID#" />

        <cftry>

            <!--- Get invalid data to arrInvalidData --->
            <cfquery name="getInvalidData" datasource="#SESSION.DBSourceEBM#">
                SELECT
                    ContactString_vch,ContactStringCorrect_vch,PKId_bi
                FROM
                    #tableName#
                WHERE
                    PKId_bi MOD #inpMod# = #inpDoModValue#
                AND
                    Status_ti = 1 or ContactStringCorrect_vch='' or ContactStringCorrect_vch is null
                LIMIT #inpNumberOfDialsToRead#
            </cfquery>

            <cfif getInvalidData.RECORDCOUNT GT 0>

                <!--- Update CSV Queue to in process (Status=2) --->
                <cfquery name="HoldProcessingRecord" datasource="#session.DBSourceEBM#" result="UpdateResults">
                    UPDATE
                        #tableName#
                    SET
                        Status_ti = 2
                    WHERE
                        PKId_bi IN
                        (
                            #valueList(getInvalidData.PKId_bi, ',')#
                        )
                </cfquery>

                <cfloop query="#getInvalidData#" >


                    <cfset itemCorrectData = {}/>
                    <cfset itemCorrectData['PKId'] = getInvalidData.PKId_bi/>
                    <cfset itemCorrectData['ContactString'] = '' />
                    <cfset itemCorrectData['NPA'] = '' />
                    <cfset itemCorrectData['NXX'] = '' />
                    <cfset currTZInfo = 0 />

                    <!--- <cfset getInvalidData.ContactString_vch = rereplace(getInvalidData.ContactString_vch, '[^0-9]+', '', 'all')> --->

                    <!--- CHECK VALID PHONE NUMBER --->
                    <cfinvoke method="validUsPhoneNumber" returnvariable="rxCheckValidPhoneNumber">
                        <cfinvokeargument name="inpContactString" value="#getInvalidData.ContactStringCorrect_vch#">
                    </cfinvoke>

                    <cfif !(rxCheckValidPhoneNumber)>
                        <cfset dataout["invalid"] += 1  />
                        <cfset arrayAppend(arrInvalidData, getInvalidData.PKId_bi)>
                    <cfelse>
                        <cfset arrayAppend(arrData, getInvalidData.ContactStringCorrect_vch)>

                        <!--- Get time zones, Localities, Cellular data --->
                        <cfset currNPA = LEFT(getInvalidData.ContactStringCorrect_vch, 3) >
                        <cfset currNXX = MID(getInvalidData.ContactStringCorrect_vch, 4, 3)>

                       <cfquery name="getTZInfo" datasource="#Session.DBSourceEBM#">
                            SELECT
                                CASE cx.T_Z
                                      WHEN 14 THEN 37
                                      WHEN 11 THEN 34
                                      WHEN 10 THEN 33
                                      WHEN 9 THEN 32
                                      WHEN 8 THEN 31
                                      WHEN 7 THEN 30
                                      WHEN 6 THEN 29
                                      WHEN 5 THEN 28
                                      WHEN 4 THEN 27
                                     END AS TimeZone,
                                CASE
                                      WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                      ELSE 0
                                      END  AS CellFlag,
                                CASE
                                      WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                      ELSE ''
                                      END AS State,
                                CASE
                                      WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                      ELSE ''
                                      END AS City,
                                CASE
                                      WHEN fx.ZIP1 IS NOT NULL THEN fx.ZIP1
                                      ELSE ''
                                      END AS Zip1,
                                CASE
                                      WHEN fx.ZIP2 IS NOT NULL THEN fx.ZIP2
                                      ELSE ''
                                      END AS Zip2,
                                CASE
                                      WHEN fx.ZIP3 IS NOT NULL THEN fx.ZIP3
                                      ELSE ''
                                      END AS Zip3
                            FROM
                                MelissaData.FONE AS fx JOIN
                                MelissaData.CNTY AS cx ON
                                (fx.FIPS = cx.FIPS)
                            WHERE
                                cx.T_Z IS NOT NULL
                            AND
                                fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                            AND
                                fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                        </cfquery>
                        <cfif getTZInfo.RECORDCOUNT GT 0>
                            <cfset currTZInfo = GetTZInfo.TimeZone/>
                           <!---  <cfif currTZInfo NEQ 31> --->
                                <cfquery name="updateCorrectData" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        #tableName#
                                    SET
                                        TimeZone_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#currTZInfo#"/>
                                        <cfif GetTZInfo.City NEQ "">
                                            ,
                                            City_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getTZInfo.City#"/>
                                        </cfif>
                                        <cfif GetTZInfo.State NEQ "">
                                            ,
                                            State_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getTZInfo.State#"/>
                                        </cfif>
                                        <cfif GetTZInfo.Zip1 NEQ "">
                                            ,
                                            Zip1_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getTZInfo.Zip1#"/>
                                        </cfif>
                                        <cfif GetTZInfo.Zip2 NEQ "">
                                            ,
                                            Zip2_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getTZInfo.Zip2#"/>
                                        </cfif>
                                        <cfif GetTZInfo.Zip3 NEQ "">
                                            ,
                                            Zip3_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getTZInfo.Zip3#"/>
                                        </cfif>
                                    WHERE
                                        PKId_bi = #getInvalidData.PKId_bi#
                                </cfquery>
                           <!---  </cfif> --->
                        </cfif>
                    </cfif>
                </cfloop>

            </cfif>

            <cfset dataout["unique"] = arrayLen(arrData) />

            <!--- Update Invalid Data STATUS --->
            <cfif !arrayIsEmpty(arrInvalidData)>
                <cfquery name="updateInvalidData" datasource="#Session.DBSourceEBM#">
                    UPDATE #tableName#
                    SET Status_ti =  11
                    WHERE PKId_bi IN (#ArrayToList(arrInvalidData)#)
                </cfquery>
            </cfif>
            <!--- duplicate data--->

            <cfquery name="getDuplicateData" datasource="#SESSION.DBSourceEBM#">
                SELECT
                    COUNT(*) as TotalDup
                FROM
                    #tableName#
                WHERE
                    Status_ti = '#STAGE_ERROR_DUPLICATED_DATA#'
            </cfquery>

            <cfquery name="updateDuplicateData" datasource="#Session.DBSourceEBM#">
                UPDATE simplexuploadstage.uploads
                SET Duplicates_int =  '#getDuplicateData.TotalDup#'
                WHERE PKId_bi =<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpFileRecordID#">
            </cfquery>
            <!--- Update CSV Queue to in process (Status=2) --->
            <cfquery name="HoldProcessingRecord" datasource="#session.DBSourceEBM#" result="UpdateResults">
                UPDATE
                    #tableName#
                SET
                    Status_ti = 3
                WHERE
                    PKId_bi IN
                    (
                        #valueList(getInvalidData.PKId_bi, ',')#
                    )
                AND Status_ti = 2
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="UploadCSVToS3" access="remote" output="false" hint="Upload image to s3">

        <cfargument name="inpFullFileName" required="true" type="string">
        <cfargument name="inpLocalPath" required="true" type="string">
        <cfargument name="inpFileSize" required="true" type="numeric">
        <cfargument name="inpFileType" required="true" type="string">
        <cfargument name="inpPlainFileName" required="true" type="string">
        <cfargument name="inpUserId" required="false" default="#SESSION.UserId#">

        <cfset var dataout = {} />
        <cfset var retValUploadS3 = '' />
        <cfset var fileType = lCase(arguments.inpFileType) />
        <cfset var bucketName = _S3CSVCONTACTFOLDERPATH />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cftry>
            <cfif arrayIsEmpty(REmatch(_NAMEREG, arguments.inpPlainFileName))>
                <cfthrow type="Object" message="Filename syntax error" detail="Filename syntax error! Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only">
            </cfif>

            <cfif FileExists(arguments.inpLocalPath & arguments.inpFullFileName)>

                <!--- Upload image and thumb image to S3 --->
                <cfinvoke component="session.sire.models.cfc.upload" method="uploadFileS3" returnvariable="retValUploadS3">
                    <cfinvokeargument name="name" value="#arguments.inpFullFileName#">
                    <cfinvokeargument name="local_path" value="#arguments.inpLocalPath#">
                    <cfinvokeargument name="contentType" value="application">
                    <cfinvokeargument name="bucketName" value="#bucketName#">
                </cfinvoke>
                <!--- If upload success, insert tracking image record --->
                <cfif retValUploadS3.STATUS EQ 1>
                    <cfset dataout.MESSAGE = "Upload CSV contact successfully!" />
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.ERRMESSAGE = '' />
                <cfelse>
                    <cfset dataout.MESSAGE = "Failed to upload CSV contact to storage!" />
                </cfif>

                <!--- Delete temp file --->
                <!--- <cffile action="delete" file="#arguments.inpLocalPath##arguments.inpFullFileName#" /> --->
            <cfelse>
                <cfset dataout.MESSAGE = "File not exist!" />
            </cfif>
            <cfcatch type="any">
                <cffile action="delete" file="#arguments.inpLocalPath##arguments.inpFullFileName#" />
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>



    <cffunction name="getReport" access="remote" hint="get list upload contact report">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default="">

        <cfset var dataout = {} />
        <cfset var getData = {} />
        <cfset var getSummary = {} />

        <cfset dataout["summary"]["duplicate"] = 0 />
        <cfset dataout["summary"]["unique"] = 0 />
        <cfset dataout["summary"]["invalid"] = 0 />
        <cfset dataout["summary"]["loaded"] = 0 />
        <cfset dataout["datalist"] = [] />

        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var rsCount  = {} />
        <cfset var customFilterStr = DeserializeJson(arguments.customFilter) />

        <!--- <cfdump var="#customFilterStr#" abort="true" /> --->

        <cftry>
            <cfquery name="getSummary" datasource="#SESSION.DBSourceEBM#">
                SELECT
                    IF(SUM(Total_int) > 0, SUM(Total_int),0)    AS Loaded,
                    IF(SUM(Duplicates_int) > 0, SUM(Duplicates_int),0)  AS duplicated,
                    IF(SUM(CountErrors_int) > 0, SUM(CountErrors_int),0)    AS invalided,
                    IF(SUM(Valid_int) > 0, SUM(Valid_int),0)  AS uniqued
                FROM
                    simplexuploadstage.uploads up
                WHERE
                    up.UploadType_ti = 1
                    <cfif IsStruct(customFilterStr) AND structKeyExists(customFilterStr, "USERID")>
                        <cfif customFilterStr.userID GT 0 >
                            AND up.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#customFilterStr.userID#">
                        </cfif>
                        <cfif customFilterStr.startDate NEQ "" >
                            AND up.Created_dt
                                BETWEEN <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.startDate#">
                                AND DATE_ADD(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.endDate#">,INTERVAL 1 DAY)
                        </cfif>

                    </cfif>
                -- AND
                    -- Status_ti != #UPLOADS_DELETED#
            </cfquery>


            <cfset dataout["summary"]["duplicate"] = getSummary.duplicated />
            <cfset dataout["summary"]["unique"] = getSummary.uniqued />
            <cfset dataout["summary"]["invalid"] = getSummary.invalided />
            <cfset dataout["summary"]["loaded"] = getSummary.Loaded />


            <cfquery name="getData" datasource="#SESSION.DBSourceEBM#">

                SELECT SQL_CALC_FOUND_ROWS
                    PKID_BI,
                    OriginFileName_vch,
                    IF(Total_int > 0, Total_int, 0) AS TotalContactString_int,
                    IF(CountErrors_int > 0, CountErrors_int, 0) AS CountErrors_int,
                    IF(Duplicates_int > 0, Duplicates_int, 0) AS Duplicates_int,
                    IF(Valid_int > 0, Valid_int, 0) AS Valid_int,
                    IF(ProcessingTime_int > 0, ProcessingTime_int, 0) AS ProcessingTime_int,
                    IF(Deleted_int > 0, Deleted_int, 0) AS Deleted_int,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS UserName,
                    CONCAT(SUBSTRING(ua1.FirstName_vch, 1, 1), ".", ua1.LastName_vch) AS ImportedBy,
                    CONCAT(SUBSTRING(ua2.FirstName_vch, 1, 1), ".", ua2.LastName_vch) AS UpdatedBy,
                    Status_ti,
                    ProcessType_ti,
                    up.UserId_int AS UserID,
                    DATE_FORMAT(Updated_dt, '%Y-%m-%d %r') AS UpdatedAt,
                    DATE_FORMAT(up.Created_dt, '%Y-%m-%d %r') AS CreatedAt,
                    up.SendToQueue_ti
                FROM
                    simplexuploadstage.uploads AS up
                    LEFT JOIN simpleobjects.useraccount AS ua ON up.UserId_int = ua.UserId_int
                    LEFT JOIN simpleobjects.useraccount AS ua1 ON up.AdminUserID_int = ua1.UserId_int
                    LEFT JOIN simpleobjects.useraccount AS ua2 ON up.LastUpdatedAUID_int = ua2.UserId_int
                WHERE
                    up.UploadType_ti = 1
                    <cfif IsStruct(customFilterStr) AND structKeyExists(customFilterStr, "USERID")>
                        <cfif customFilterStr.userID GT 0 >
                            AND up.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#customFilterStr.userID#">
                        </cfif>
                        <cfif customFilterStr.startDate NEQ "" >
                            AND up.Created_dt
                                BETWEEN <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.startDate#">
                                AND DATE_ADD(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.endDate#">,INTERVAL 1 DAY)
                        </cfif>

                    </cfif>
                -- AND
                    -- Status_ti != #UPLOADS_DELETED#
                ORDER BY
                    up.Created_dt DESC
                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>
            <!--- <cfdump var="#getData#" abort="true"/> --->

            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

            <cfloop query="getData" >
                <cfset var tmpItem = {
                    id = "#getData.PKId_bi#",
                    filename = "#getData.OriginFileName_vch#",
                    Loaded = "#getData.TotalContactString_int#",
                    Invalid = "#getData.CountErrors_int#",
                    Duplicate = "#getData.Duplicates_int#",
                    Unique = "#getData.Valid_int#",
                    Deleted = "#getData.Deleted_int#",
                    ProcessingTime = "#getData.ProcessingTime_int#",
                    UserName = "#getData.UserName#",
                    UserID = "#getData.UserID#",
                    Status = "#getData.Status_ti#",
                    ImportedBy = "#ImportedBy#",
                    UpdatedAt = "#UpdatedAt#",
                    UpdatedBy = "#UpdatedBy#",
                    ListType  = "#ProcessType_ti#",
                    CreatedAt = "#CreatedAt#",
                    SendToQueue = "#SendToQueue_ti#"
                }/>

                <cfset ArrayAppend(dataout["datalist"], tmpItem) />
            </cfloop>


            <cfset dataout.MESSAGE = "Get data successfully!" />
            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>


    <cffunction name="AddContactStringToList" access="public" output="true" hint="Add phone number to list if not already on it - Assumes no existing unique top level contact already linked - this is stand alone - Only can be called by another method inside this local CFC">
        <cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPUSERID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        <cfargument name="INP_CPPID" required="no" default="">
        <cfargument name="INP_CPP_UUID" required="no" default="">
        <cfargument name="INP_SOCIALTOKEN" required="no" default="">
        <cfargument name="INPFIRSTNAME" required="no" default="">
        <cfargument name="INPLASTNAME" required="no" default="">
        <cfargument name="LANGUAGEPREFERENCE" required="no" default="">
        <cfargument name="INPCDFDATA" required="no" default="">
        <cfargument name="INPSTAGETABLE" required="no" default="">
        <cfargument name="inpTimeZone" required="no" default="31"/>
        <cfargument name="inpUseInputTimeZone" required="no" default="0"/>
        <cfargument name="inpSkipLocalUserDNCCheck" required="no" default="0"/>
        <cfargument name="inpShortCode" required="no" default=""/>
        <cfargument name="inpUploadTableRecordID" required="no" default=""/>


        <cfset var inpDesc = '' />
        <cfset var CurrTZ = '' />
        <cfset var CurrLOC = '' />
        <cfset var CurrCity = '' />
        <cfset var CurrCellFlag = '' />
        <cfset var NextContactListId = '' />
        <cfset var NextContactAddressId = '' />
        <cfset var NextContactId = '' />
        <cfset var Fields = '' />
        <cfset var value = '' />
        <cfset var item = '' />
        <cfset var VerifyUnique = '' />
        <cfset var CheckForContactString = '' />
        <cfset var DeleteContactVariable = '' />
        <cfset var AddToContactVariable = '' />
        <cfset var AddToContactList = '' />
        <cfset var AddToContactString = '' />
        <cfset var CheckForGroupLink = '' />
        <cfset var AddToGroup = '' />
        <cfset var GetTZInfo = '' />
        <cfset var shortCode = '' />
        <cfset var optinResult = '' />
        <cfset var AddContactVariable = '' />
        <cfset var AddContactListResult = '' />
        <cfset var AddContactResult = '' />
        <cfset var AddToGroupResult = '' />
        <cfset var GetCDFData = '' />

        <cfset var INPCONTACTSTRING = arguments.INPCONTACTSTRING/>
        <cfset var CurrNPA = ''/>
        <cfset var CurrNXX = ''/>
        <cfset var CurrState = ''/>

        <cfset var inpSkipLocalUserDNCCheck = arguments.inpSkipLocalUserDNCCheck/>
        <cfset var GetUserDNCFroServiceRequest = '' />
        <cfset var UpdateValidRecordWhenBlockPhone = '' />


        <cfset var dataout = '0' />

         <!---
        Positive is success
        1 = OK
        2 = Duplicate
        3 =
        4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

         --->


        <cfoutput>

            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE,ERRORCODE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRORCODE", "0") />


            <cftry>


                <!--- Validate user is specified - private methods onlylay - handle gracefully if not --->
                <cfif INPUSERID GT 0>

                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->

                    <!--- SMS validation--->
                    <cfif INPCONTACTTYPEID EQ 3>
                        <!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d]", "", "ALL")>
                    </cfif>

                    <!--- Get time zone info --->
                    <cfset CurrTZ = 31>

                    <!--- Get Locality info --->
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrState = ""/>
                    <cfset CurrCellFlag = "0">

                    <!--- Set default to -1 --->
                    <cfset NextContactListId = -1>

                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                        <cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>
                    </cfif>

                    <!--- Add record --->
                   <!---  <cftry> --->



                        <!--- Create generic contact first--->
                        <!--- Add Address to contact --->
                         <cfif (INPGROUPID EQ "" OR INPGROUPID EQ 0) >
                            <cfthrow MESSAGE="Valid Group ID for this user account required! Please specify a group ID." TYPE="Any" detail="" errorcode="-6">
                         </cfif>


                        <!--- Only add to group if greater than 0 --->
                        <cfif INPGROUPID GT 0>
                            <!--- Verify user is group id owner--->
                            <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                                SELECT
                                    COUNT(GroupName_vch) AS TOTALCOUNT
                                FROM
                                    simplelists.grouplist
                                WHERE
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                                    AND
                                    GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">
                            </cfquery>

                            <cfif VerifyUnique.TOTALCOUNT EQ 0>
                                <cfthrow MESSAGE="Group ID does not exists for this user account! Try a different group." TYPE="Any" detail="" errorcode="-6">
                            </cfif>

                        </cfif>

                    <cfif inpSkipLocalUserDNCCheck EQ 0>
                        <!--- Session User DNC --->
                        <!--- Cant sub-select from external DB in query of query service request so check one at a time --->
                        <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
                            SELECT
                                COUNT(oi.ContactString_vch) AS TotalCount
                            FROM
                                simplelists.optinout AS oi
                            WHERE
                                OptOut_dt IS NOT NULL
                            AND
                                OptIn_dt IS NULL
                            AND
                                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                            AND
                                oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                        </cfquery>

                        <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>

                            <cfset QuerySetCell(dataout, "ERRORCODE", "#STAGE_ERROR_BLOCKED#") />
                            <cfthrow type="Application" errorcode="#STAGE_ERROR_BLOCKED#" message="Blocked by User DNC" detail="Blocked by User DNC"/>
                        </cfif>

                        <cfset inpSkipLocalUserDNCCheck = 1/>
                    </cfif>

                        <cfset NextContactAddressId = "">
                        <cfset NextContactId = "">

                        <!--- Check if contact string exists in main list --->
                        <cfquery name="CheckForContactString" datasource="#Session.DBSourceREAD#">
                            SELECT
                                   simplelists.contactlist.contactid_bi,
                                   simplelists.contactstring.contactaddressid_bi
                            FROM
                               simplelists.contactstring
                               INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                            WHERE
                               simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                            AND
                               simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                            AND
                               simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                            <!---
                            <cfif TRIM(INP_CPPID) NEQ "" AND TRIM(INP_CPP_UUID) NEQ "" >
                                AND
                                    simplelists.contactlist.cppid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPPID)#">
                                AND
                                    simplelists.contactlist.cpp_uuid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPP_UUID)#">
                            </cfif>
                            --->

                        </cfquery>

                        <cfif CheckForContactString.RecordCount GT 0>

                            <cfset NextContactId = "#CheckForContactString.ContactId_bi#">
                            <cfset NextContactAddressId = "#CheckForContactString.ContactAddressId_bi#">
                            <cfquery name="DeleteContactVariable" datasource="#Session.DBSourceEBM#">
                                DELETE FROM simplelists.contactvariable
                                WHERE
                                    ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">
                                AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                            </cfquery>

                            <!--- insert CDF --->
                            <cfif IsStruct(INPCDFDATA)>
                                <cfloop collection="#INPCDFDATA#" item="item">
                                    <cfset value = INPCDFDATA['#item#']/>
                                    <cfif find("_", item) GT 0>
                                        <cfset item = replace(item, "_", " ",'all')/>
                                    </cfif>
                                    <cfquery name="GetCDFData" datasource="#Session.DBSourceEBM#">
                                        SELECT
                                            CdfId_int,
                                            CDFName_vch
                                        FROM
                                            simplelists.customdefinedfields
                                        WHERE
                                            CDFName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#item#">
                                        AND
                                            userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                                        LIMIT
                                            1
                                    </cfquery>

                                    <cfif GetCDFData.RecordCount GT 0 AND Trim(value) NEQ ''>
                                         <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                           INSERT INTO simplelists.contactvariable
                                                (
                                                    UserId_int,
                                                    ContactId_bi,
                                                    VariableName_vch,
                                                    VariableValue_vch,
                                                    Created_dt,
                                                    CdfId_int
                                                 )
                                           VALUES
                                                (
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCDFData.CDFName_vch#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCDFData.CdfId_int#">
                                                )
                                            </cfquery>
                                    </cfif>
                                </cfloop>
                            </cfif>


                        <cfelse> <!--- ContactList for this user does not exist yet --->

                            <!--- By default just add new unique contact during this method - add method later on to search for existing contact--->
                            <cfquery name="AddToContactList" datasource="#Session.DBSourceEBM#" result="AddContactListResult">
                                    INSERT INTO simplelists.contactlist
                                        (
                                            UserId_int,
                                            Created_dt,
                                            LASTUPDATED_DT,
                                            socialToken_vch,
                                            FirstName_vch,
                                            LastName_vch,
                                            LanguagePreference_vch
                                        )
                                    VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                            NOW(),
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_SOCIALTOKEN)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPFIRSTNAME)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPLASTNAME)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LANGUAGEPREFERENCE)#">
                                         )
                            </cfquery>

                            <cfset NextContactId = "#AddContactListResult.GENERATEDKEY#">

                                <!--- Get time zones, Localities, Cellular data --->
                                <!--- Adjust for numers that start with 1 --->
                                <cfif LEFT(INPCONTACTSTRING, 1) EQ 1>
                                    <cfset CurrNPA = MID(INPCONTACTSTRING, 2, 3)>
                                    <cfset CurrNXX = MID(INPCONTACTSTRING, 5, 3)>

                                    <!--- Get rid of 1 when looking up in DB --->
                                    <cfset INPCONTACTSTRING = RIGHT(INPCONTACTSTRING, LEN(INPCONTACTSTRING)-1)>
                                <cfelse>
                                    <cfset CurrNPA = LEFT(INPCONTACTSTRING, 3) >
                                    <cfset CurrNXX = MID(INPCONTACTSTRING, 4, 3)>
                                </cfif>

                                <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                    SELECT
                                        CASE cx.T_Z
                                              WHEN 14 THEN 37
                                              WHEN 11 THEN 34
                                              WHEN 10 THEN 33
                                              WHEN 9 THEN 32
                                              WHEN 8 THEN 31
                                              WHEN 7 THEN 30
                                              WHEN 6 THEN 29
                                              WHEN 5 THEN 28
                                              WHEN 4 THEN 27
                                             END AS TimeZone,
                                        CASE
                                              WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                              ELSE 0
                                              END  AS CellFlag,
                                        CASE
                                              WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                              ELSE ''
                                              END AS State,
                                        CASE
                                              WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                              ELSE ''
                                              END AS City
                                    FROM
                                        MelissaData.FONE AS fx JOIN
                                        MelissaData.CNTY AS cx ON
                                        (fx.FIPS = cx.FIPS)
                                    WHERE
                                        cx.T_Z IS NOT NULL
                                    AND
                                        fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                                    AND
                                        fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                                </cfquery>

                                <cfif GetTZInfo.RECORDCOUNT GT 0>
                                    <cfset CurrTZ = GetTZInfo.TimeZone/>
                                    <cfset CurrState = GetTZInfo.State/>
                                    <cfset CurrCity = GetTZInfo.City/>
                                    <cfset CurrCellFlag = GetTZInfo.CellFlag/>
                                </cfif>

                                <cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#" result="AddContactResult">
                                   INSERT INTO simplelists.contactstring
                                        (
                                            ContactId_bi,
                                            Created_dt,
                                            LASTUPDATED_DT,
                                            ContactType_int,
                                            ContactString_vch,
                                            TimeZone_int,
                                            CellFlag_int,
                                            UserSpecifiedData_vch,
                                            OptIn_int,
                                            State_vch,
                                            City_vch,
                                            NPA_vch,
                                            NXX_vch,
                                            OptIn_dt,
                                            OptInVerify_dt
                                         )
                                   VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                            NOW(),
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">,
                                            1,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrState#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrCity#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">,
                                            NOW(),
                                            NOW()
                                        )
                                </cfquery>

                                <!--- insert CDF --->
                                <cfif IsStruct(INPCDFDATA)>
                                    <cfloop collection="#INPCDFDATA#" item="item">
                                        <cfset value = INPCDFDATA['#item#']/>
                                        <cfif find("_", item) GT 0>
                                            <cfset item = replace(item, "_", " ",'all')/>
                                        </cfif>

                                        <cfquery name="GetCDFData" datasource="#Session.DBSourceEBM#">
                                            SELECT
                                                CdfId_int,
                                                CDFName_vch
                                            FROM
                                                simplelists.customdefinedfields
                                            WHERE
                                                CDFName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#item#">
                                            AND
                                                userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                                            LIMIT
                                                1
                                        </cfquery>
                                        <cfif GetCDFData.RecordCount GT 0 AND Trim(value) NEQ ''>
                                             <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                               INSERT INTO simplelists.contactvariable
                                                    (
                                                        UserId_int,
                                                        ContactId_bi,
                                                        VariableName_vch,
                                                        VariableValue_vch,
                                                        Created_dt,
                                                        CdfId_int
                                                     )
                                               VALUES
                                                    (
                                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCDFData.CDFName_vch#">,
                                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                        NOW(),
                                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCDFData.CdfId_int#">
                                                    )
                                                </cfquery>
                                        </cfif>
                                    </cfloop>
                                </cfif>

                              <cfset NextContactAddressId = #AddContactResult.GENERATEDKEY#>

                        </cfif>


                       <!--- Only add to group if greater than 0 --->
                       <cfif INPGROUPID GT 0>

                           <cfquery name="CheckForGroupLink" datasource="#Session.DBSourceREAD#">
                               SELECT
                                   ContactAddressId_bi
                               FROM
                                   simplelists.groupcontactlist
                               WHERE
                                   ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
                               AND
                                   GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                           </cfquery>

                            <cfif CheckForGroupLink.RecordCount EQ 0>

                             <cfquery name="AddToGroup" datasource="#Session.DBSourceEBM#" result="AddToGroupResult">
                                    INSERT INTO
                                       simplelists.groupcontactlist
                                       (ContactAddressId_bi, GroupId_bi)
                                    VALUES
                                       (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">)
                               </cfquery>
                           </cfif>

                      </cfif>


                        <!---

                        Time Zone · The number of hours past Greenwich Mean Time. The time zones are:

                           Hours Time Zone
                                   27 4 Atlantic
                                   28 5 Eastern
                                   29 6 Central
                                   30 7 Mountain
                                   31 8 Pacific
                                   32 9 Alaska
                                   33 10 Hawaii-Aleutian
                                   34 11 Samoa
                                   37 14 Guam


                        --->


                        <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE,ERRORCODE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRORCODE", "0") />


                  <!---
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->

                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>

                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

                        <cfelse>

                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

                        </cfif>

                    </cfcatch>

                    </cftry> --->


                <cfelse>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE,ERRORCODE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                    <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Invalid UserId sepecified!") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    <cfset QuerySetCell(dataout, "ERRORCODE", "0") />

                </cfif>

            <cfcatch TYPE="any">

                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE,ERRORCODE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "ERRORCODE", "#cfcatch.ErrorCode#") />

            </cfcatch>

            </cftry>

        </cfoutput>

        <cfreturn dataout />
    </cffunction>



    <cffunction access="remote" name="getProcessStatus" hint="">
        <cfargument name="inpImportID" type="string" >

        <cfset var dataout = {}>
        <cfset var getStatus = ''/>
        <cfset var tableName = "simplexuploadstage.uploads" />

        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.TABLENAME = "">

        <cfset dataout.STATUS = 0>

        <cftry>

            <cfquery name="getStatus" datasource="#Session.DBSourceEBM#" >
                SELECT
                    Status_ti
                FROM
                    #tableName#
                WHERE
                    ProcessId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpImportID#">
                LIMIT 1
            </cfquery>

            <cfif getStatus.RECORDCOUNT EQ 1>
                <cfset dataout.STATUS = getStatus.Status_ti />
            </cfif>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Created table #tableName#'>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>



    <cffunction access="remote" name="getContact" hint="">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="inpType" type="numeric" default="10">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />

        <cfset var dataout = {}>
        <cfset var getContact = ''/>
        <cfset var ContactStringRT = ''/>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpFileRecordID />

        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">

        <cfset dataout.DATALIST = []>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset var rsCount  = {} />

        <cftry>

            <cfquery name="getContact" datasource="#Session.DBSourceEBM#" >
                SELECT SQL_CALC_FOUND_ROWS
                    PKId_bi,
                    Status_ti,
                    TimeZone_int AS TimeZone,
                    ContactStringCorrect_vch
                FROM
                    #tableName#
                WHERE
                    <cfif arguments.inpType is "3">
                        Status_ti IN(3,5)
                    <cfelse>
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpType#"/>
                    </cfif>
                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">

            </cfquery>

             <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

            <cfloop query="#getContact#">
                <cfset ContactStringRT=getContact.ContactStringCorrect_vch>
                <cfif ContactStringRT EQ "">
                    <cfset ContactStringRT="Blank">
                </cfif>
                <cfset var item = {ID = "#getContact.PKId_bi#", CONTACT = "#ContactStringRT#", TIMEZONE = '#TimeZone#', STATUS = "#getContact.Status_ti#" } />
                <cfset dataout.DATALIST.append(item) />
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Get contact success!'>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction access="public" name="validUsPhoneNumber" hint="" output="false">
        <cfargument name="inpContactString" required="true" type="string" >
       <!---  <cfset var arrCheck = arrayNew()/> --->

        <cfif trim(arguments.inpContactString) EQ ''>
            <cfreturn false>
        </cfif>

        <cfif LEN(arguments.inpContactString) NEQ 10 >
            <cfreturn false>
        </cfif>

       <cfset var arrCheck = REMatchNoCase("[^\d^\*^P^X^##]",trim(arguments.inpContactString)) >

        <cfif arrayLen(arrCheck) GT 0>
            <cfreturn false>
        </cfif>

        <cfreturn true>
    </cffunction>

    <cffunction name="deleteContacts" access="remote" hint="Delete contacts">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="inpItemIDs" TYPE="string" required="true">
        <cfargument name="inpType" required="no" default="10"/>
        <cfargument name="inpDeleteAll" required="no" default="0"/>

        <cfset var dataout = {}>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpFileRecordID />
        <cfset var arrItemId = DeserializeJson(arguments.inpItemIDs) />

        <cfset var exeSql = {}>
        <cfset var oldRecords = {}>
        <cfset var currentTime = datetimeformat(NOW(), "yyyy-mm-dd HH:mm:ss") />
        <cfset var rsUpdateFileRecord = '' />
        <cfset var typeString = '' />

        <cfif structKeyExists(STAGE_STATUS_TXT, "#arguments.inpType#")>
            <cfset typeString = STAGE_STATUS_TXT["#arguments.inpType#"]/>
        <cfelse>
            <cfset typeString = "UNKNOWN"/>
        </cfif>

        <cftransaction>
            <cftry>
                <cfif isArray(arrItemId) AND arrItemId.len() GT 0 AND arguments.inpDeleteAll EQ 0>
                    <cfquery name="oldRecords" datasource="#Session.DBSourceEBM#" >
                        SELECT ContactString_vch
                        FROM #tableName#
                        WHERE PkID_bi IN(#arrItemId.toList()#)
                    </cfquery>

                    <cfquery name="exeSql" datasource="#Session.DBSourceEBM#" >
                       UPDATE #tableName#
                       SET Status_ti = #STAGE_DELETED#
                       WHERE PkID_bi IN(#arrItemId.toList()#)
                       LIMIT #arrItemId.len()#
                    </cfquery>

                    <cfinvoke method="updateFileRecord" returnvariable="rsUpdateFileRecord">
                        <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#">
                    </cfinvoke>

                    <cfloop query="oldRecords">
                        <cfinvoke component="session.sire.models.cfc.import-contact-history" method="logAction">
                            <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#" >
                            <cfinvokeargument name="inpAdminID" value="#Session.USERID#" >
                            <cfinvokeargument name="inpTime" value="#currentTime#" >
                            <cfinvokeargument name="inpFieldType" value="#typeString#" >
                            <cfinvokeargument name="inpOldValue" value="#oldRecords.ContactString_vch#" >
                            <cfinvokeargument name="inpNewValue" value="DELETED" >
                        </cfinvoke>
                    </cfloop>

                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.MESSAGE = 'Delete contacts success!'>
                <cfelseif arguments.inpDeleteAll GT 0>
                    <cfquery name="oldRecords" datasource="#Session.DBSourceEBM#" >
                        SELECT ContactString_vch
                        FROM #tableName#
                        WHERE Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpType#"/>
                    </cfquery>

                    <cfquery name="exeSql" datasource="#Session.DBSourceEBM#" >
                       UPDATE #tableName#
                       SET Status_ti = #STAGE_DELETED#
                       WHERE Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpType#"/>
                    </cfquery>

                    <cfinvoke method="updateFileRecord" returnvariable="rsUpdateFileRecord">
                        <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#">
                    </cfinvoke>

                    <cfloop query="oldRecords">
                        <cfinvoke component="session.sire.models.cfc.import-contact-history" method="logAction">
                            <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#" >
                            <cfinvokeargument name="inpAdminID" value="#Session.USERID#" >
                            <cfinvokeargument name="inpTime" value="#currentTime#" >
                            <cfinvokeargument name="inpFieldType" value="#typeString#" >
                            <cfinvokeargument name="inpOldValue" value="#oldRecords.ContactString_vch#" >
                            <cfinvokeargument name="inpNewValue" value="DELETED" >
                        </cfinvoke>
                    </cfloop>

                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.MESSAGE = 'Delete contacts success!'>
                <cfelse>
                    <cfthrow type="any" message="inpItemIDs argument is invalid!">
                </cfif>
                <cfcatch type="any">
                    <cftransaction action="rollback">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
                </cfcatch>
            </cftry>
        </cftransaction>
        <cfreturn dataout/>
    </cffunction>


    <cffunction name="updateContact" access="remote" hint="Upload contact">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="inpItemID" TYPE="numeric" required="true">
        <cfargument name="inpContact" TYPE="string" required="true">
        <cfargument name="inpTimezone" TYPE="numeric" required="true">
        <cfargument name="inpType" TYPE="numeric" required="true">

        <cfset var dataout = {}>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpFileRecordID />
        <cfset var exeSql = {}>
        <cfset var oldRecord = {}>
        <cfset var isValidContact = '' />
        <cfset var currentTime = datetimeformat(NOW(), "yyyy-mm-dd HH:mm:ss") />
        <cfset var rsUpdateFileRecord = '' />

        <cfif structKeyExists(STAGE_STATUS_TXT, "#arguments.inpType#")>
            <cfset arguments.inpType = STAGE_STATUS_TXT["#arguments.inpType#"]/>
        <cfelse>
            <cfset arguments.inpType = "UNKNOWN"/>
        </cfif>

        <cftry>

            <cfinvoke method="validateRecord" returnvariable="isValidContact">
                <cfinvokeargument name="inpTablename" value="#tableName#">
                <cfinvokeargument name="inpItemID" value="#arguments.inpItemID#">
                <cfinvokeargument name="inpContact" value="#arguments.inpContact#">
            </cfinvoke>

            <cfif isValidContact>
                <cfquery name="oldRecord" datasource="#Session.DBSourceEBM#" >
                    SELECT ContactString_vch
                    FROM #tableName#
                    WHERE PkID_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#">
                    LIMIT 1
                </cfquery>

                <cfquery name="exeSql" datasource="#Session.DBSourceEBM#" >
                    UPDATE #tableName#
                    SET ContactStringCorrect_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContact#">,
                        TimeZone_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpTimezone#">,
                        Status_ti = #STAGE_READY#
                    WHERE PkID_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#">
                    LIMIT 1
                </cfquery>

                <cfinvoke method="updateFileRecord" returnvariable="rsUpdateFileRecord">
                    <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#">
                    <cfinvokeargument name="inpTime" value="#currentTime#">
                </cfinvoke>

                <cfinvoke component="session.sire.models.cfc.import-contact-history" method="logAction">
                    <cfinvokeargument name="inpFileRecordID" value="#arguments.inpFileRecordID#" >
                    <cfinvokeargument name="inpAdminID" value="#Session.USERID#" >
                    <cfinvokeargument name="inpTime" value="#currentTime#" >
                    <cfinvokeargument name="inpFieldType" value="#arguments.inpType#" >
                    <cfinvokeargument name="inpOldValue" value="#oldRecord.ContactString_vch#" >
                    <cfinvokeargument name="inpNewValue" value="#arguments.inpContact#" >
                </cfinvoke>

                <cfset dataout.RXRESULTCODE = 1>
                <cfset dataout.MESSAGE = 'Update contacts success!'>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>



    <cffunction name="validateRecord" access="private" hint="private validate record">
        <cfargument name="inpTablename" type="string" required="true">
        <cfargument name="inpItemID" TYPE="numeric" required="true">
        <cfargument name="inpContact" TYPE="string" required="true">
        <cfset var rs = false>
        <cfset var exeSql = {}>
        <cfset var checkUsPhone = ''/>

        <cfinvoke method="validUsPhoneNumber" returnvariable="checkUsPhone">
            <cfinvokeargument name="inpContactString" value="#arguments.inpContact#">
        </cfinvoke>

        <cfif checkUsPhone>
            <cfquery name="exeSql" datasource="#Session.DBSourceEBM#" >
                SELECT PKId_bi
                FROM #arguments.inpTablename#
                WHERE
                    PKId_bi <> <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpItemID#">
                    AND ContactStringCorrect_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContact#">
                AND
                    Status_ti = #STAGE_READY#
                LIMIT 1
            </cfquery>
            <cfif exeSql.RECORDCOUNT GT 0>
                <cfthrow errorcode="-1" type="any" message="Contact already existed!">
            <cfelse>
                <cfset rs = true>
            </cfif>
        <cfelse>
             <cfthrow errorcode="-2" type="any" message="Contact is not a US phone!">
        </cfif>
        <cfreturn rs />
    </cffunction>




    <cffunction name="getTZList" access="public" hint="get List time zone">
        <cfreturn [
            { name = "UNK", value = 0},
            { name = "EASTERN|EDT|EST", value = 28},
            { name = "CENTRAL|CDT|CST", value = 29},
            { name = "MOUNTAIN|MDT|MST", value = 30},
            { name = "PACIFIC|PDT|PST", value = 31},
            { name = "ALASKA", value = 32},
            { name = "HAWAII|HAST", value = (GetTimeZoneInfo().isDSTOn ? 34 : 33)},
            { name = "SAMOA", value = (GetTimeZoneInfo().isDSTOn ? 35 : 34)},
            { name = "GUAM", value = (GetTimeZoneInfo().isDSTOn ? 38 : 37)}
        ]/>
    </cffunction>



    <cffunction name="updateFileRecord" access="private" hint="Update file record">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="inpTime" type="string" required="false" default="">

        <cfset var dataout = {}>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpFileRecordID />
        <cfset var uploadStatus = 1>
        <cfset var rxGetUserInfoById = ''/>
        <cfset var rxNotifyToUser = ''/>
        <cfset var inputData = {} />
        <cfset var GetActiveUpload = {} />
        <cfset var getSumaryInfo = '' />
        <cfset var exeSql = '' />
        <cfset var RXGetActiveUpload = '' />

        <cfquery name="getSumaryInfo" datasource="#Session.DBSourceEBM#" >
            SELECT
                COUNT(IF(Status_ti = #STAGE_DELETED#, 1, NULL)) AS DELETED,
                COUNT(IF((Status_ti = #STAGE_READY#) OR (Status_ti=#STAGE_SUCCESS#) , 1, NULL))  AS VALID,
                COUNT(IF(Status_ti = #STAGE_ERROR_DUPLICATED_DATA#, 1, NULL)) AS DUPLICATED,
                COUNT(IF(Status_ti = #STAGE_ERROR_INVALID#, 1, NULL)) AS INVALID,
                COUNT(PKId_bi) AS TOTAL
            FROM
                #tableName#
        </cfquery>


        <cfif getSumaryInfo.DUPLICATED GT 0 OR getSumaryInfo.INVALID GT 0>
            <cfset uploadStatus = #UPLOADS_PENDING#>
        <cfelse>

            <!--- SEND EMAIL TO ADMIN --->

            <cfquery name="GetActiveUpload" datasource="#DBSourceEBM#" result="RXGetActiveUpload">
                SELECT
                    su.PKId_bi,
                    su.ProcessType_ti,
                    su.Status_ti,
                    su.BatchId_bi,
                    su.SubcriberList_bi,
                    su.UserId_int,
                    su.SendToQueue_ti,
                    su.AdminUserID_int
                FROM
                    simplexuploadstage.uploads su
                WHERE
                    su.PKId_bi = #inpFileRecordID#
            </cfquery>

            <cfif GetActiveUpload.RECORDCOUNT GT 0>

                <cfset inputData={}/>
                <cfset inputData.UPLOADID=inpFileRecordID/>

                <cfif GetActiveUpload.ProcessType_ti EQ 1>
                        <cfset inputData.STATUS= "Ready to send contact to subcriber list (#GetActiveUpload.SubcriberList_bi#)"/>
                    <cfelse>
                        <cfset inputData.STATUS= "ready to send blast (#GetActiveUpload.BatchId_bi#) "/>
                </cfif>

                <cfset inputData.DATETIME= datetimeformat(NOW(), "yyyy-mm-dd HH:mm:ss") />

                <!--- GET ADMIN UPLOAD EMAIL --->
                <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="rxGetUserInfoById">
                    <cfinvokeargument name="inpUserId" value="#GetActiveUpload.AdminUserID_int#">
                </cfinvoke>

                <cfinvoke component="session.sire.models.cfc.import-contact" method="notifyToUser" returnvariable="rxNotifyToUser">
                    <cfinvokeargument name="inpTo" value="#rxGetUserInfoById.FULLEMAIL#">
                    <cfinvokeargument name="inpSubject" value="Your upload file are complete.">
                    <cfinvokeargument name="inpData" value="#inputData#">
                </cfinvoke>

                <cfif GetActiveUpload.SendToQueue_ti EQ 1>
                    <cfset uploadStatus = #UPLOADS_READY#>
                 <cfelse>
                    <cfset uploadStatus = #UPLOADS_WAITING#>
                </cfif>


            </cfif>
        </cfif>

        <cfquery name="exeSql" datasource="#Session.DBSourceEBM#" >
            UPDATE
                simplexuploadstage.uploads
            SET
                Total_int = #getSumaryInfo.TOTAL#,
                Valid_int = #getSumaryInfo.VALID#,
                Duplicates_int = #getSumaryInfo.DUPLICATED#,
                CountErrors_int = #getSumaryInfo.INVALID#,
                Deleted_int = #getSumaryInfo.DELETED#,
                Status_ti = #uploadStatus#,
                updated_dt = NOW(),
                LastUpdatedAUID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#SESSION.USERID#"/>
            WHERE
                PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpFileRecordID#">
            LIMIT 1
        </cfquery>


        <cfset dataout.RXRESULTCODE = 1>
        <cfset dataout.MESSAGE = 'Update FileRecord success!'>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="getReportByUploadId" access="remote" hint="get Report for upload">
        <cfargument name="inpUploadIdPK" type="numeric" required="true">
        <cfargument name="inpType" type="numeric" default="0">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="20" />
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="ASC">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_3" default="-1">
        <cfargument name="sSortDir_3" default="">

        <cfset var dataout = {}>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpUploadIdPK />

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cfset dataout.DATALIST = []/>
        <cfset var getContact =''/>
        <cfset var order = '' />
        <cfset var statusTXT = '' />
        <cfset var getUploadInfo = '' />
        <cfset var getBlocked = '' />
        <cfset var getDuplicated = '' />
        <cfset var getTotalIreResult = '' />
        <cfset var getTotalContactqueue = '' />
        <cfset var getTotalContactResult = '' />
        <cfset var rsCount = '' />
        <cfset var getCampaignName = '' />
        <cfset var getOverAllContactInQueue = '' />

        <cftry>
            <cfif arguments.inpUploadIdPK GT 0>
                <!--- GET UPLOAD INFO --->
                <cfquery name="getUploadInfo" datasource="#Session.DBSourceEBM#">
                    SELECT
                         CountErrors_int,
                         Duplicates_int,
                         Deleted_int,
                         Total_int,
                         Valid_int,
                         SubcriberList_bi,
                         BatchId_bi,
                         LoadSuccess_int,
                         LoadError_int,
                         ErrorDetails_vch,
                         ProcessType_ti,
                         DATE_FORMAT(Created_dt, '%Y-%m-%d %r') AS CreatedAt,
                         OriginFileName_vch
                    FROM
                         simplexuploadstage.uploads
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadIdPK#"/>
                </cfquery>

                <cfif getUploadInfo.RECORDCOUNT GT 0>
                    <cfset dataout['loadSuccess'] = getUploadInfo.LoadSuccess_int>
                    <cfset dataout['loadError'] = getUploadInfo.LoadError_int>
                    <cfset dataout['Processtype'] = getUploadInfo.ProcessType_ti>
                    <cfset dataout['createdAt'] = getUploadInfo.CreatedAt/>
                    <cfset dataout['fileName'] = getUploadInfo.OriginFileName_vch/>

                    <cfif getUploadInfo.ProcessType_ti EQ 1>

                    <cfelse>
                        <!--- Get latest campaign name --->
                        <cfquery name="getCampaignName" datasource="#Session.DBSourceREAD#">
                            SELECT
                                Desc_vch
                            FROM
                                simpleobjects.batch
                            WHERE
                                BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUploadInfo.BatchId_bi#"/>
                            LIMIT
                                1
                        </cfquery>
                        <cfif getCampaignName.RECORDCOUNT GT 0>
                            <cfset dataout['CampaignName'] = getCampaignName.Desc_vch/>
                        <cfelse>
                            <cfset dataout['CampaignName'] = 'N/A'/>
                        </cfif>

                        <!--- Get total contact in queue --->
                        <cfquery name="getOverAllContactInQueue" datasource="#Session.DBSourceREAD#">
                            SELECT
                                COUNT(*) AS TOTALCOUNT
                            FROM
                                simplequeue.contactqueue
                            WHERE
                                DTSStatusType_ti = 1
                        </cfquery>
                        <cfset dataout["totalContactQueueOverall"] = getOverAllContactInQueue.TOTALCOUNT/>

                        <!--- Total STAGE_ERROR_BLOCKED --->
                         <cfquery name="getBlocked" datasource="#Session.DBSourceEBM#" >
                            SELECT
                                count(PKId_bi) as TOTALCOUNT
                            FROM
                                #tableName#
                            WHERE
                                Status_ti = #STAGE_ERROR_BLOCKED#
                        </cfquery>

                        <cfset dataout['Blocked'] =  getBlocked.TOTALCOUNT/>

                        <!--- Total Duplicate in queue --->
                         <cfquery name="getDuplicated" datasource="#Session.DBSourceEBM#" >
                            SELECT
                                count(PKId_bi) as TOTALCOUNT
                            FROM
                                #tableName#
                            WHERE
                                Status_ti = #STAGE_ERROR_DUPLICATED_QUEUE#
                        </cfquery>

                        <cfset dataout['DuplicatedInQueue'] =  getDuplicated.TOTALCOUNT/>

                        <cfif getUploadInfo.BatchId_bi GT 0>
                            <!--- Sanity Check Queries --->
                            <cfquery name="getTotalIreResult" datasource="#Session.DBSourceEBM#">
                                SELECT
                                     COUNT(*)  AS TOTALCOUNT
                                FROM
                                     simplexresults.ireresults
                                WHERE
                                     BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUploadInfo.BatchId_bi#"/>
                                AND
                                     IREType_int = 1
                                AND
                                    LENGTH(ContactString_vch) <= 10
                             </cfquery>
                             <cfset dataout['totalIreResult'] =  getTotalIreResult.TOTALCOUNT/>

                            <cfquery name="getTotalContactqueue" datasource="#Session.DBSourceEBM#">
                                SELECT
                                     COUNT(*) AS TOTALCOUNT
                                FROM
                                     simplequeue.contactqueue
                                WHERE
                                    BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUploadInfo.BatchId_bi#"/>
                            </cfquery>
                            <cfset dataout['totalContactQueue'] =  getTotalContactqueue.TOTALCOUNT/>

                            <cfquery name="getTotalContactResult" datasource="#Session.DBSourceEBM#">
                                SELECT
                                     COUNT(*)  AS TOTALCOUNT
                                FROM
                                     simplexresults.contactresults
                                WHERE
                                    BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUploadInfo.BatchId_bi#"/>
                                AND
                                    LENGTH(ContactString_vch) <= 10
                            </cfquery>
                            <cfset dataout['totalContactResults'] =  getTotalContactResult.TOTALCOUNT/>
                        </cfif>
                    </cfif>

                    <!--- Build Order --->
                    <cfif iSortCol_0 GTE 0>
                        <cfswitch expression="#Trim(iSortCol_0)#">
                            <cfcase value="1">
                               <cfset order = "ContactStringCorrect_vch"/>
                            </cfcase>
                            <cfcase value="2">
                               <cfset order = "TimeZone_int" />
                            </cfcase>
                            <cfcase value="3">
                               <cfset order = "Status_ti"/>
                            </cfcase>
                            <cfdefaultcase>
                                <cfset order = "PKId_bi"/>
                            </cfdefaultcase>
                        </cfswitch>
                    <cfelse>
                         <cfset order = "PKId_bi"/>
                    </cfif>

                    <cfquery name="getContact" datasource="#Session.DBSourceEBM#" >
                        SELECT SQL_CALC_FOUND_ROWS
                            PKId_bi,
                            Status_ti,
                            TimeZone_int AS TimeZone,
                            ContactStringCorrect_vch,
                            Note_vch
                        FROM
                            #tableName#
                        WHERE 1
                            <cfif arguments.inpType NEQ 0>
                                AND Status_ti = #arguments.inpType#
                            </cfif>
                        ORDER BY
                            #order# #sSortDir_0#
                        LIMIT
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                        OFFSET
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">

                    </cfquery>

                    <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                        SELECT FOUND_ROWS() AS iTotalRecords
                    </cfquery>
                    <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
                    <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

                    <cfloop query="#getContact#">
                        <cfset statusTXT = STAGE_STATUS_TXT['#getContact.Status_ti#']>
                        <cfset var item = {ID = "#getContact.PKId_bi#", CONTACT = "#getContact.ContactStringCorrect_vch#", TIMEZONE = '#TimeZone#', STATUS = "#statusTXT#", NOTE = "#Note_vch#" } />
                        <cfset dataout.DATALIST.append(item) />
                    </cfloop>

                 </cfif>

                 <cfset dataout.RXRESULTCODE = 1>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>


    <cffunction name="StopImportById" access="remote" hint="Stop import by id">
        <cfargument name="inpPKId" type="numeric" required="true"/>
        <cfargument name="inpType" type="numeric" required="true"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = ''/>
        <cfset dataout.RXRESULTCODE = -1/>

        <cfset var tableName = "simplexuploadstage.Contactlist_stage_"&#arguments.inpPKId#>

        <cfset var stopUploadQuery = '' />
        <cfset var stopQueueQuery = ''/>
        <cfset var activeQueue = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="stopUploadQuery">
                UPDATE
                    simplexuploadstage.uploads
                SET
                    Status_ti = #UPLOADS_PAUSED#
                WHERE
                    PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKId#"/>
            </cfquery>

            <cfif stopUploadQuery.RECORDCOUNT LT 1>
                <cfset dataout.MESSAGE = "Import ID not found. Stop failed"/>
            <cfelse>
                <cfif arguments.inpType EQ PROCESSTYPE_CAMPAIGN_BLAST>

                    <cfquery datasource="#Session.DBSourceEBM#" name="activeQueue">
                        SELECT
                            scq.DTSId_int
                        FROM
                            #tableName# scl
                        INNER JOIN
                            simplequeue.contactqueue scq
                        ON
                            scl.ContactStringCorrect_vch = scq.ContactString_vch
                        WHERE
                            scq.DTSStatusType_ti = 1
                        AND
                            scq.GroupId_int = 0
                        AND
                            scq.BatchId_bi = (
                                SELECT
                                    su.BatchId_bi
                                FROM
                                    simplexuploadstage.uploads su
                                WHERE
                                    su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKId#"/>
                            )
                    </cfquery>

                    <cfif activeQueue.RECORDCOUNT GT 0>
                        <cfquery datasource="#Session.DBSourceEBM#" result="stopQueueQuery">
                            UPDATE
                                simplequeue.contactqueue
                            SET
                                DTSStatusType_ti = #CONTACTQUEUE_PAUSED#
                            WHERE
                                DTSId_int IN (
                                    #valueList(activeQueue.DTSId_int, ',')#
                                )
                        </cfquery>
                    </cfif>

                </cfif>

                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Import stopped"/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Unexpected error happened. Cannot stop"/>
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="ResumeImportById" access="remote" hint="Resume import by id">
        <cfargument name="inpPKId" type="numeric" required="true"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = ''/>
        <cfset dataout.RXRESULTCODE = -1/>

        <cfset var resumeUploadQuery = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="resumeUploadQuery">
                UPDATE
                    simplexuploadstage.uploads
                SET
                    Status_ti = #UPLOADS_READY#
                WHERE
                    PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKId#"/>
            </cfquery>

            <cfif resumeUploadQuery.RECORDCOUNT LT 1>
                <cfset dataout.MESSAGE = "Import ID not found. Resume failed"/>
            <cfelse>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Import resumed"/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Unexpected error happened. Cannot resume"/>
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="DeleteImportById" access="remote" hint="Delete import by id">
        <cfargument name="inpPKId" type="numeric" required="true"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = ''/>
        <cfset dataout.RXRESULTCODE = -1/>

        <cfset var deleteUploadQuery = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="deleteUploadQuery">
                UPDATE
                    simplexuploadstage.uploads
                SET
                    Status_ti = #UPLOADS_DELETED#
                WHERE
                    PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKId#"/>
            </cfquery>

            <cfif deleteUploadQuery.RECORDCOUNT LT 1>
                <cfset dataout.MESSAGE = "Import ID not found. Delete failed"/>
            <cfelse>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Import deleted"/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Unexpected error happened. Cannot delete"/>
                <cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction access="remote" name="GetContactToSend" hint="">
        <cfargument name="inpFileRecordID" type="numeric" required="true">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />

        <cfset var dataout = {}>
        <cfset var getContact = ''/>
        <cfset var tableName = "simplexuploadstage.contactlist_stage_"&arguments.inpFileRecordID />

        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">

        <cfset dataout.DATALIST = []>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout.TOTALSENT = 0/>
        <cfset dataout.TOTALREMAIN = 0/>
        <cfset var rsCount  = {} />
        <cfset var getStats = '' />

        <cftry>

            <cfquery name="getContact" datasource="#Session.DBSourceEBM#" >
                SELECT SQL_CALC_FOUND_ROWS
                    PKId_bi,
                    Status_ti,
                    ContactStringCorrect_vch
                FROM
                    #tableName#
                WHERE
                    Status_ti = #STAGE_READY#
                AND
                    SendToQueue_ti = 0
                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>

            <cfquery name="getStats" datasource="#Session.DBSourceREAD#">
                SELECT
                    SUM(IF(SendToQueue_ti = 1, 1, 0)) AS TotalSent
                FROM
                    #tableName#
            </cfquery>

            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

            <cfif getStats.RECORDCOUNT GT 0>
                <cfset dataout.TOTALSENT = getStats.TotalSent/>
                <cfset dataout.TOTALREMAIN = rsCount.iTotalRecords/>
            </cfif>

            <cfloop query="#getContact#">
                <cfset var item = {ID = "#getContact.PKId_bi#", CONTACT = "#getContact.ContactStringCorrect_vch#", STATUS = "#getContact.Status_ti#" } />
                <cfset dataout.DATALIST.append(item) />
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Get contact success!'>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="SendToQueue" access="remote" hint="Send to queue">
        <cfargument name="inpPKId" required="true" type="numeric"/>
        <cfargument name="inpSendAll" required="false" type="numeric" default="0"/>
        <cfargument name="inpNumberToSend" required="false" type="numeric" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var inpTableName = "simplexuploadstage.Contactlist_stage_#arguments.inpPKId#" />

        <cfset var selectContactArray = ""/>
        <cfset var updateStageTable = '' />
        <cfset var updateUploadStatus = '' />

        <cftry>
            <cfif inpSendAll GT 0>
                <cfquery datasource="#Session.DBSourceEBM#" result="updateStageTable">
                    UPDATE
                        #inpTablename#
                    SET
                        SendToQueue_ti = 1
                    WHERE
                        Status_ti = #STAGE_READY#
                </cfquery>
            <cfelse>

<!---                 <cfif arrayLen(REMatchNoCase('[^,0-9]|[,]$', arguments.inpSelectedContact)) GT 0>
                    <cfthrow message="Input has wrong format. Please check and try again" detail="Input has wrong format. Please check and try again" />
                </cfif> --->

                <cfquery datasource="#Session.DBSourceEBM#" result="updateStageTable">
                    UPDATE
                        #inpTablename#
                    SET
                        SendToQueue_ti = 1
                    WHERE
                        Status_ti = #STAGE_READY#
                    AND
                        SendToQueue_ti = 0
                    ORDER BY
                        PkID_bi ASC
                    LIMIT
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpNumberToSend#"/>
                </cfquery>
            </cfif>

            <cfquery datasource="#Session.DBSourceEBM#" result="updateUploadStatus">
                UPDATE
                    simplexuploadstage.uploads
                SET
                    Status_ti = #UPLOADS_READY#,
                    SendToQueue_ti = 1
                WHERE
                    PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPKId#"/>
            </cfquery>

            <cfset dataout.MESSAGE = "Contacts sent!"/>
            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Unexpected error occurred"/>
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>

    </cffunction>

     <cffunction name="notifyToUser" access="public">
        <cfargument name="inpFrom" type="string" required="false" default="#SupportEMailFrom#">
        <cfargument name="inpTo" type="string" required="true">
        <cfargument name="inpSubject" type="string" required="true">
        <cfargument name="inpData" type="any" required="true">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset var longUrl=''>
        <cfset var GetSubscriberList=''>

        <cftry>

            <cfquery name="GetSubscriberList" datasource="#Session.DBSourceREAD#">
                SELECT
                    GroupName_vch
                FROM
                    simplelists.grouplist
                WHERE
                    GroupId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpData.SUBSCRIBERLISTID#"/>
            </cfquery>
            <cfset longUrl = "https://#(findNoCase("cron.siremobile.com", CGI.SERVER_NAME) ? "www.siremobile.com" : CGI.SERVER_NAME)#/session/sire/pages/subscriber" />
            <cfif arguments.inpData.STATUS EQ UPLOADS_SUCCESS OR arguments.inpData.STATUS EQ UPLOADS_OUTSTANDING>
                <cfmail from="#arguments.inpFrom#" to="#arguments.inpTo#" username="#SupportEMailUserName#" password="#SupportEMailPassword#"
                server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#" type="html" subject="#arguments.inpSubject#">

                    <cfoutput>
                        <div>Good News! Your upload is complete.</div>

                        <div>This list is now available in the subscribers (<a href="#longUrl#">#GetSubscriberList.GroupName_vch#</a>) of your account.</div>

                        <div style="margin-top:20px;">Reminder: All uploaded contacts must have expressly consented or “opted-in” to receive text marketing messages as part of your campaign.</div>
                        <div>You will be held liable for any violation of _FCC or TCPA_ ( https://apps.fcc.gov/edocs_public/attachmatch/FCC-15-72A1_Rcd.pdf) policy.</div>
                    </cfoutput>
                </cfmail>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Send to user success!'>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetUploadInfo" access="public" hint="Get all information about upload id">
        <cfargument name="inpUploadId" required="true" type="numeric"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.UPLOAD = {} />
        <cfset var GetUpload = '' />

        <cftry>
            <cfquery name="GetUpload" datasource="#session.DBSourceEBM#">
                SELECT
                    PKId_bi,
                    UserId_int,
                    OriginFileName_vch,
                    AdminUserID_int,
                    Total_int,
                    CountErrors_int,
                    Duplicates_int,
                    Valid_int,
                    Deleted_int,
                    Created_dt,
                    Status_ti,
                    ProcessType_ti,
                    BatchId_bi,
                    SubcriberList_bi,
                    LoadSuccess_int,
                    LoadError_int,
                    RemoveStageTable_ti,
                    ColumnNumber_int,
                    ColumnNames_vch,
                    ErrorsIgnore_int,
                    SendToQueue_ti
                FROM
                    simplexuploadstage.uploads
                WHERE
                    PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadId#"/>
                LIMIT
                    1
            </cfquery>
            <cfif GetUpload.RECORDCOUNT GT 0>
                <cfset dataout.UPLOAD = GetUpload/>

                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Get upload information successfully!"/>
            <cfelse>
                <cfset dataout.MESSAGE = "Can't get upload information!"/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetUploadBlastReport" access="remote" hint="Get all blast log for upload id">
        <cfargument name="inpUploadId" required="true" type="numeric"/>
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default=""/>

        <cfset var dataout = {} />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = ""/>

        <cfset dataout["BlastReport"] = [] />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var GetUploadBlastReport = "" />
        <cfset var rsCount  = {} />
        <cfset var filterItem = ""/>

        <cftry>
            <cfquery name="GetUploadBlastReport" datasource="#Session.DBSourceREAD#">
                SELECT SQL_CALC_FOUND_ROWS
                    ubl.PKId_bi,
                    ubl.BatchId_bi,
                    ubl.TotalSend_int,
                    ubl.TotalRemain_int,
                    ubl.Created_dt,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS AdminUserName,
                    b.Desc_vch AS CampaignName
                FROM
                    simplexuploadstage.upload_blast_log ubl
                INNER JOIN
                    simpleobjects.useraccount ua
                ON
                    ubl.AdminId_int = ua.UserId_int
                INNER JOIN
                    simpleobjects.batch b
                ON
                    ubl.BatchId_bi = b.BatchId_bi
                WHERE
                    ubl.UploadId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadId#"/>
               <cfif arguments.customFilter NEQ "">
                    <cfloop array="#deserializeJSON(arguments.customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>

            <cfif GetUploadBlastReport.RECORDCOUNT GT 0>
                <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>
                <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
                <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

                <cfloop query="GetUploadBlastReport" >
                    <cfset var tmpItem = {
                        ID = "#GetUploadBlastReport.PKId_bi#",
                        CAMPAIGNNAME = "#GetUploadBlastReport.CampaignName#",
                        BATCHID = "#GetUploadBlastReport.BatchId_bi#",
                        TOTALSEND = "#GetUploadBlastReport.TotalSend_int#",
                        BALANCE = "#GetUploadBlastReport.TotalRemain_int#",
                        CREATED = "#dateTimeFormat(GetUploadBlastReport.Created_dt,'mm-dd-yyyy')#",
                        //CREATED = "#GetUploadBlastReport.Created_dt)#",
                        ADMINUSERNAME = "#GetUploadBlastReport.AdminUserName#"
                    }/>

                    <cfset ArrayAppend(dataout["BlastReport"], tmpItem) />
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get data successfully!"/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn (dataout)/>
    </cffunction>

    <cffunction name="GetEligibleCount" access="remote" hint="count eligible records from stage table">
        <cfargument name="inpUploadId" required="true" type="numeric"/>
        <cfargument name="customFilter" required="false" type="string" default=""/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.SENT = 0/>
        <cfset dataout.ELIGIBLE = 0/>
        <cfset dataout.SENDING = 0/>
        <cfset dataout.OTHER = 0/>

        <cfset var tablename = "simplexuploadstage.contactlist_stage_" & arguments.inpUploadId/>
        <cfset var GetData = "" />
        <cfset var filterItem = ""/>

        <cftry>
            <cfquery name="GetData" datasource="#Session.DBSourceREAD#">
                SELECT
                    SUM(IF(Status_ti = 3 AND SendToQueue_ti = 0, 1, 0)) AS ELIGIBLE,
                    SUM(IF(SendToQueue_ti = 1, 1, 0)) AS SENT,
                    SUM(IF(Status_ti = 3 AND SendToQueue_ti = 1, 1, 0)) AS SENDING,
                    SUM(IF(Status_ti != 3 AND SendToQueue_ti != 5, 1, 0)) AS OTHER
                FROM
                    #tablename#
                WHERE
                    1
               <cfif arguments.customFilter NEQ "">
                    <cfloop array="#deserializeJSON(arguments.customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ "ContactString_vch">
                            <cfset filterItem.NAME = 'ContactStringCorrect_vch'>
                        </cfif>

                        <cfif findNoCase("zipcode", filterItem.NAME)>
                        AND (
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                <cfset var AC = filterItem.OPERATOR EQ "=" ? "or" : "and"/>
                                Zip1_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                                #AC#
                                Zip2_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                                #AC#
                                Zip3_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                            <cfelse>
                                <cfset var AC = filterItem.OPERATOR EQ "LIKE" ? "or" : "and"/>
                                Zip1_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                                #AC#
                                Zip2_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                                #AC#
                                Zip3_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                            </cfif>
                        )
                        <cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfquery>
            <!--- <cfdump var="#GetData#" abort="true"/> --->

            <cfif GetData.RECORDCOUNT GT 0>
                <cfset dataout.SENT = GetData.SENT NEQ "" ? GetData.SENT : 0/>
                <cfset dataout.ELIGIBLE = GetData.ELIGIBLE NEQ "" ? GetData.ELIGIBLE : 0/>
                <cfset dataout.SENDING = GetData.SENDING NEQ "" ? GetData.SENDING : 0/>
                <cfset dataout.OTHER = GetData.OTHER NEQ "" ? GetData.OTHER : 0/>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = "Get data successfully!"/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="SendPartial" access="remote" hint="Send blast for imported contact partially">
        <cfargument name="inpUploadId" required="true" type="numeric"/>
        <cfargument name="customFilter" required="false" type="string" default=""/>
        <cfargument name="inpSendNumber" required="true" type="numeric"/>
        <cfargument name="inpBatchId" required="true" type="numeric"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cfset var tablename = "simplexuploadstage.contactlist_stage_" & arguments.inpUploadId/>
        <cfset var UpdateStageTable = ""/>
        <cfset var GetUpload = "" />
        <cfset var UpdateUploadInfo = "" />
        <cfset var InsertUploadBlastLog = "" />
        <cfset var UpdateStageRecord = "" />
        <cfset var CountRemainContact = ""/>
        <cfset var filterItem = '' />

        <cfset GetUpload = GetUploadInfo(arguments.inpUploadId)/>
        <cfif GetUpload.RXRESULTCODE LT 1>
            <cfthrow type="Application" message="Can't get upload information" detail="Can't get upload information" />
        </cfif>

        <cfif GetUpload.UPLOAD.Status_ti NEQ UPLOADS_WAITING>
            <cfthrow type="Application" message="Upload is not eligible to send partially!" detail="Upload status is not valid" />
        </cfif>

        <cftransaction>
            <cftry>

                <cfquery result="UpdateStageRecord" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        #tablename#
                    SET
                        SendToQueue_ti = 1
                    WHERE
                        Status_ti = 3
                    AND
                        SendToQueue_ti = 0
                   <cfif arguments.customFilter NEQ "">
                        <cfloop array="#deserializeJSON(arguments.customFilter)#" index="filterItem">
                            <cfif findNoCase("zipcode", filterItem.NAME)>
                            AND (
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                    <cfset var AC = filterItem.OPERATOR EQ "=" ? "or" : "and"/>
                                    Zip1_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                                    #AC#
                                    Zip2_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                                    #AC#
                                    Zip3_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'/>
                                <cfelse>
                                    <cfset var AC = filterItem.OPERATOR EQ "LIKE" ? "or" : "and"/>
                                    Zip1_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                                    #AC#
                                    Zip2_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                                    #AC#
                                    Zip3_vch #filterItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'/>
                                </cfif>
                            )
                            <cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                    LIMIT
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendNumber#"/>
                </cfquery>

                <cfif UpdateStageRecord.RECORDCOUNT LT arguments.inpSendNumber>
                    <cfthrow type="Application" message="Can not send eligible number. Please try again later!" detail="Error on update stage record"/>
                </cfif>

                <cfquery name="CountRemainContact" datasource="#Session.DBSourceEBM#">
                    SELECT
                        COUNT(PKId_bi) AS REMAIN
                    FROM
                        #tablename#
                    WHERE
                        SendToQueue_ti = 0
                    AND
                        Status_ti = 3
                </cfquery>

                <cfif CountRemainContact.RECORDCOUNT LT 1>
                    <cfthrow type="Application" message="Can not send to eligible number. Please try again later!" detail="Failed to count remain eligible number"/>
                </cfif>

                <cfquery datasource="#Session.DBSourceEBM#" result="InsertUploadBlastLog">
                    INSERT INTO
                        simplexuploadstage.upload_blast_log
                    (
                        UploadId_bi,
                        BatchId_bi,
                        UserId_int,
                        AdminId_int,
                        TotalSend_int,
                        TotalRemain_int
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#GetUpload.UPLOAD.PKId_bi#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#GetUpload.UPLOAD.UserId_int#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#GetUpload.UPLOAD.AdminUserID_int#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendNumber#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#CountRemainContact.REMAIN#"/>
                    )
                </cfquery>

                <cfif InsertUploadBlastLog.RECORDCOUNT LT 1>
                    <cfthrow type="Application" message="Can't create new blast log" detail="Can't create new blast log"/>
                </cfif>

                <cfquery result="UpdateUploadInfo" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplexuploadstage.uploads
                    SET
                        Status_ti = #UPLOADS_READY#,
                        BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadId#"/>
                </cfquery>

                <cfif UpdateUploadInfo.RECORDCOUNT LT 1>
                    <cfthrow type="Application" message="Can not send eligible number. Please try again later!" detail="Failed to update upload record"/>
                </cfif>

                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Send successfully!"/>

                <cfcatch type="any">
                    <cftransaction action="rollback">
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                    <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                    <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
                </cfcatch>
            </cftry>
        </cftransaction>
        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="AddFiltersToQueue" access="remote" output="false" hint="Add a group to the dial queue">
        <cfargument name="INPSCRIPTID" required="no" default="-1">
        <cfargument name="inpLibId" required="no" default="-1">
        <cfargument name="inpEleId" required="no" default="-1">
        <cfargument name="INPBATCHID" required="no" default="">
        <cfargument name="INPBATCHDESC" required="no" default="">
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENABLED_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="inpSocialmediaFlag" required="no" default="0">
        <cfargument name="INPGROUPID" required="yes">
        <cfargument name="CONTACTTYPES" required="yes" default="">
        <cfargument name="Note" required="yes" default="">
        <cfargument name="IsApplyFilter" required="yes" default="0">
        <cfargument name="CONTACTFILTER" required="no" default="">
        <cfargument name="inpDoRules" required="no" default="1">
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">
        <cfargument name="inpLimitDistribution" required="no" default="0" hint="Limits total allow to load. Does not work if duplicates are allowed in advance options">
        <cfargument name="ABTestingBatches" required="no" default="" hint="Comma sperated list of Batches. If Specified and duplicates are not allowed will limit distribution to those not already distributed in list of Batches">
        <cfargument name="inpDistributionProcessId" required="no" default="0">
        <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts"/>
        <cfargument name="inpShortCode" TYPE="string" required="false" default="" hint="Shortcode to trigger this for - used only with Batch Id if it is specified." />
        <cfargument name="inpSkipBillingCheck" required="no" default="0" hint="Skip billing check for process large blast">

        <cfset var dataout = {} />
        <cfset var MESSAGE  = '' />
        <cfset var MCCONTACT_MASK   = '' />
        <cfset var notes_mask   = '' />
        <cfset var inpSourceMask    = '' />
        <cfset var BlockGroupMembersAlreadyInQueue  = '' />
        <cfset var QueuedScheduledDate  = '' />
        <cfset var NEXTBATCHID  = '' />
        <cfset var COUNTQUEUEDUPVOICE   = '' />
        <cfset var COUNTQUEUEDUPEMAIL   = '' />
        <cfset var COUNTQUEUEDUPSMS = '' />
        <cfset var MainLibFile  = '' />
        <cfset var RulesgeneratedWhereClause    = '' />
        <cfset var RetValGroupCount = '' />
        <cfset var EstimatedCost    = '' />
        <cfset var RetValGetXML = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var CCDElement   = '' />
        <cfset var isValidXMLControlString  = '' />
        <cfset var ServiceInputFlag = '' />
        <cfset var RUNNING_CAMPAIGN = '' />
        <cfset var STOP_CAMPAIGN    = '' />
        <cfset var getDistinctBatchIdsDupeProcessingFlag    = '' />
        <cfset var StopBatchInQueue = '' />
        <cfset var RetValBillingData    = '' />

        <cfset var BLOCKEDBYDNC = 0 />
        <cfset var SMSINIT = 0 />
        <cfset var SMSINITMESSAGE = "" />

        <cfset var whichField = 0 />
        <cfset var GetTZInfo = 0 />
        <cfset var GetRawDataTZs = 0 />
        <cfset var GetRawDataCount = 0 />
        <cfset var InsertToErrorLog = 0 />

        <cfset var temp = '' />
        <cfset var RowCountVar = 0>
        <cfset var VOICEONLYXMLCONTROLSTRING_VCH = "">
        <cfset var GetUserDNCFroServiceRequest = ''/>
        <cfset var UserLocalDNCBlocked = 0 />
        <cfset var inpSkipLocalUserDNCCheck = 0 />

        <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
        <cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />
        <cfset var InvalidMCIDXML   = '' />
        <cfset var DDMBuffA = '' />
        <cfset var DDMBuffB = '' />
        <cfset var DDMBuffC = '' />
        <cfset var DDMPos1  = '' />
        <cfset var DDMPos2  = '' />
        <cfset var DDMReultsArray   = '' />
        <cfset var RawDataFromDB    = '' />
        <cfset var myxmldocResultDoc    = '' />
        <cfset var DebugStr = '' />
        <cfset var selectedElements = '' />
        <cfset var CURRVAL  = '' />
        <cfset var DDMSWITCHReultsArray = '' />
        <cfset var CurrSwitchValue  = '' />
        <cfset var CurrSwitchQIDValue   = '' />
        <cfset var CurrSwitchBSValue    = '' />
        <cfset var CaseMatchFound   = '' />
        <cfset var CurrCaseReplaceString    = '' />
        <cfset var XMLFDDoc = '' />
        <cfset var selectedElementsII   = '' />
        <cfset var OutToDBXMLBuff   = '' />
        <cfset var XMLDefaultDoc    = '' />
        <cfset var DDMCONVReultsArray   = '' />
        <cfset var CurrXMLConversionType    = '' />
        <cfset var AccountnumberTwoAtATimeBuffer    = '' />
        <cfset var CurrAccountNum   = '' />
        <cfset var TwoDigitLibrary  = '' />
        <cfset var SingleDigitLibrary   = '' />
        <cfset var PausesLibrary    = '' />
        <cfset var PausesStyle  = '' />
        <cfset var ISDollars    = '' />
        <cfset var ISDecimalPlace   = '' />
        <cfset var DecimalLibrary   = '' />
        <cfset var MoneyLibrary = '' />
        <cfset var HundredsLibrary  = '' />
        <cfset var CurrDynamicAmount    = '' />
        <cfset var IncludeDayOfWeek = '' />
        <cfset var IncludeTimeOfWeek    = '' />
        <cfset var DayofWeekElement = '' />
        <cfset var MonthElement = '' />
        <cfset var DayofMonthElement    = '' />
        <cfset var YearElement  = '' />
        <cfset var DSTimeElement    = '' />
        <cfset var TwoDigitElement  = '' />
        <cfset var CurrMessageXMLTransactionDate    = '' />
        <cfset var CurrTransactionDate  = '' />
        <cfset var CurrXMLConvDesc  = '' />
        <cfset var CurrTransactionVar   = '' />
        <cfset var CurrXMLConversionMessage = '' />
        <cfset var CurrDSHour   = '' />
        <cfset var CurrDSMinute = '' />
        <cfset var CurrCentAmountBuff   = '' />
        <cfset var CurrMessageXMLDecimal    = '' />
        <cfset var CurrDynamicAmountBuff    = '' />
        <cfset var BuffStr  = '' />
        <cfset var IsDecimal    = '' />
        <cfset var CurrDDMVar   = '' />
        <cfset var CurrDDMSWITCHVar = '' />
        <cfset var CurrFDXML    = '' />
        <cfset var CurrWildcardXML  = '' />
        <cfset var CurrDefaultXML   = '' />
        <cfset var CurrDDMCONVVar   = '' />
        <cfset var AccountnumberIndex   = '' />
        <cfset var ixi  = '' />
        <cfset var iixi = '' />
        <cfset var GetCustomFields = '' />
        <cfset var GetShortCodeData = {} />
        <cfset var GetRawData = '' />
        <cfset var inpContactTypeId = '3' />




        <cfset var RetValGetCurrentUserEmailSubaccountInfo = '' />

        <cfset var eMailSubUserAccountName = '' />
        <cfset var eMailSubUserAccountPassword = '' />
        <cfset var eMailSubUserAccountRemoteAddress = '' />
        <cfset var eMailSubUserAccountRemotePort = '' />
        <cfset var eMailSubUserAccountTLSFlag = '' />
        <cfset var eMailSubUserAccountSSLFlag = '' />

        <cfset var inpeMailHTMLTransformed = '' />
        <cfset var PreviewFileName = '' />
        <cfset var ScriptProcessedOutput = ''/ >
        <cfset var NEWUUID = "" />


        <cfset var DeliveryServiceComponentPath = "#Session.SessionCFCPath#.csc.csc" />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />

        <!--- 1 is for add to queue 0 will force real time processing --->
        <cfset var INPPOSTTOQUEUEFORWEBSERVICEDEVICEFULFILLMENT = 1 />

        <cfset var inpIRETypeAdd = IREMESSAGETYPE_IRE_BLAST_TRIGGERED />

        <cfset var retValCDF = ''/>

         <!---
        Positive is success
        1 = OK
        2 =
        3 =
        4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

         --->

        <cfoutput>

            <cfset MESSAGE = "START">

            <cfset arguments.Note = URLDECODE(Note)>

            <!--- Filters that need to be added back in  --->
            <cfset MCCONTACT_MASK = "">
            <cfset notes_mask = "">
            <cfset inpSourceMask = "">
            <cfset var contacStringFilter = "">
            <cfset var contactIdListByCdfFilter = "">


            <!--- Don't use for anything
            <cfif CONTACTFILTER NEQ ''>
            <!---now build contact string and contact list by cdf --->
                <cfset var cdfAndContactStringFilterObj = deserializeJson(CONTACTFILTER)>
                <!---<cfdump var="#cdfAndContactStringFilterObj#">  --->
                <cfset contacStringFilter  = cdfAndContactStringFilterObj.CONTACTFILTER>
                <cfinvoke method="GetContactListByCDF" returnvariable="retValCDF">
                    <cfinvokeargument name="CDFData" value="#cdfAndContactStringFilterObj.CDFFILTER#">
                    <cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
                </cfinvoke>
                <cfif retValCDF.RXRESULTCODE NEQ 1>
                    <cfthrow MESSAGE="Error getting CDF data" TYPE="Any" detail="#retValCDF.MESSAGE# & #retValCDF.ERRMESSAGE#" errorcode="-2">
                </cfif>
                <cfset contactIdListByCdfFilter = retValCDF.CONTACTLISTBYCDF>
            </cfif> --->

            <cfset BlockGroupMembersAlreadyInQueue = 1>

            <cfset QueuedScheduledDate = "NOW()">

            <cfif TRIM(inpScheduleOffsetSeconds) NEQ 0 AND ISNUMERIC(TRIM(inpScheduleOffsetSeconds))>
                <cfset QueuedScheduledDate = "DATE_ADD(NOW(), INTERVAL #inpScheduleOffsetSeconds# SECOND)">
            </cfif>

            <cfset NEXTBATCHID = -1>

            <cfset COUNTQUEUEDUPVOICE = 0>
            <cfset COUNTQUEUEDUPEMAIL = 0>
            <cfset COUNTQUEUEDUPSMS = 0>

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
            <cfset dataout.INPSCRIPTID = "#INPSCRIPTID#"/>
            <cfset dataout.INPBATCHID = "#INPBATCHID#"/>
            <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#"/>
            <cfset dataout.INPBATCHDESC = "#INPBATCHDESC#"/>
            <cfset dataout.COUNTQUEUEDUPVOICE = "#COUNTQUEUEDUPVOICE#"/>
            <cfset dataout.COUNTQUEUEDUPSMS = "#COUNTQUEUEDUPSMS#"/>
            <cfset dataout.COUNTQUEUEDUPEMAIL = "#COUNTQUEUEDUPEMAIL#"/>
            <cfset dataout.TYPE = ""/>
            <cfset dataout.MESSAGE = ""/>
            <cfset dataout.ERRMESSAGE = ""/>

            <cftry>

                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>


                  <cfif !isnumeric(INPBATCHID) OR INPBATCHID LT 1 >
                        <cfthrow MESSAGE="Invalid BatchId Specified" TYPE="Any" detail="" errorcode="-2">
                  </cfif>

                    <cfif inpDoRules EQ 0 OR INPBATCHID EQ 0>

                        <cfset RulesgeneratedWhereClause = "">

                    <cfelse>

                        <cfset NEXTBATCHID = INPBATCHID>

                        <cfinclude template="/session/cfc/includes/act_ProcessDistributionRules.cfm">

                    </cfif>

                    <!--- Check for wether duplicates allowed or other BR's here--->
                    <cfquery name="getDistinctBatchIdsDupeProcessingFlag" datasource="#Session.DBSourceEBM#">
                        SELECT
                            AllowDuplicates_ti
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>

                    <cfif getDistinctBatchIdsDupeProcessingFlag.AllowDuplicates_ti GT 0>
                        <cfset BlockGroupMembersAlreadyInQueue = 0>
                    </cfif>

                    <!--- Turn list of CONTACTTYPES into valid in clause list - remove trailing comma --->
                    <cfset arguments.CONTACTTYPES = TRIM(arguments.CONTACTTYPES)>
                    <cfif Right(arguments.CONTACTTYPES, 1) EQ ",">
                        <cfset arguments.CONTACTTYPES = left(arguments.CONTACTTYPES,len(arguments.CONTACTTYPES)-1)>
                    </cfif>

                    <cfset MESSAGE = MESSAGE & " Debug Point A #ListLen(arguments.CONTACTTYPES)# (#INPBATCHID#) (#INPGROUPID#) (#arguments.CONTACTTYPES#) (#Note#) (#RulesgeneratedWhereClause#) ">

                    <cfset RetValGroupCount = GetListElligableGroupCount_ByType("#INPBATCHID#", "#INPGROUPID#", "#MCCONTACT_MASK#", "#notes_mask#", "#arguments.CONTACTTYPES#", "#Note#", "#RulesgeneratedWhereClause#" )>
                    <cfif RetValGroupCount.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting elligible counts." TYPE="Any" detail="#RetValGroupCount.MESSAGE# - #RetValGroupCount.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <cfset MESSAGE = MESSAGE & " Debug Point AII">


                    <cfif arguments.inpSkipBillingCheck EQ 0>
                        <!--- Make sure it is ok with billing - only charge when dial complete --->
                        <cfinvoke
                         component="session.sire.models.cfc.billing"
                         method="ValidateBilling"
                         returnvariable="RetValBillingData">
                            <cfinvokeargument name="inpCount" value="#RetValGroupCount.TOTALUNITCOUNT#"/>
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                            <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke
                         component="session.sire.models.cfc.billing"
                         method="ValidateBilling"
                         returnvariable="RetValBillingData">
                            <cfinvokeargument name="inpCount" value="1"/>
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                            <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                        </cfinvoke>
                    </cfif>

                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <!--- This is inserted into Queue  --->
                    <cfset EstimatedCost = RetValBillingData.ESTIMATEDCOSTPERUNIT>

                    <!--- Limit to 1000 calls per distribution? --->
                    <!--- Other security checks --->


                    <cfset RetValGetXML = "">


                    <!--- BuildXMLControlString --->
                    <cfset RetValGetXML = GetXMLControlString(INPBATCHID)>

                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <!--- Verify all numbers are actual numbers --->
                    <cfif !isnumeric(INPBATCHID)>
                        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>

                    <!--- Only validate if phone call --->
                    <cfif ListContains(arguments.CONTACTTYPES, 1) >

                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #RetValGetXML.XMLCONTROLSTRING# & "</XMLControlStringDoc>") />
                        <cfset CCDElement =  XmlSearch(myxmldocResultDoc,"/XMLControlStringDoc//CCD") />
                        <cfset isValidXMLControlString = false>
                        <cfif ArrayLen(CCDElement) EQ 1>
                            <cfif Trim(CCDElement[1].XmlAttributes["CID"]) NEQ "">
                                <cfset isValidXMLControlString = true>
                            </cfif>
                        </cfif>
                        <cfif NOT isValidXMLControlString >
                            <cfthrow MESSAGE="Invalid CID Specified - Make sure your account has one specified in administrator settings" TYPE="Any" detail="" errorcode="-5">
                        </cfif>

                    </cfif>


                    <cfset NEXTBATCHID = "#INPBATCHID#">


                    <cfset MESSAGE = MESSAGE & " Debug Point B">


                    <!--- Changes which data source to process for each channel--->
                    <cfset ServiceInputFlag = 0>

                    <!--- Insert Voice, eMail, SMS into dial queue--->
                    <cfset COUNTQUEUEDUPVOICE = 0>
                    <cfset COUNTQUEUEDUPEMAIL = 0>
                    <cfset COUNTQUEUEDUPSMS = 0>

                    <!--- Load current user's email sub account information if any --->
                    <cfif ListContains(arguments.CONTACTTYPES, 2)>

                        <cfinvoke
                            method="GetCurrentUserEmailSubaccountInfo"
                            returnvariable="RetValGetCurrentUserEmailSubaccountInfo">
                            <cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
                        </cfinvoke>

                        <cfset eMailSubUserAccountName = "#RetValGetCurrentUserEmailSubaccountInfo.USERNAME#" />
                        <cfset eMailSubUserAccountPassword = "#RetValGetCurrentUserEmailSubaccountInfo.PASSWORD#" />
                        <cfset eMailSubUserAccountRemoteAddress = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEADDRESS#" />
                        <cfset eMailSubUserAccountRemotePort = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEPORT#" />
                        <cfset eMailSubUserAccountTLSFlag = "#RetValGetCurrentUserEmailSubaccountInfo.TLSFLAG#" />
                        <cfset eMailSubUserAccountSSLFlag = "#RetValGetCurrentUserEmailSubaccountInfo.SSLFLAG#" />

                    </cfif>

                    <!--- Check if XML control string has customized processing --->

                    <cfif REFIND("{%[^%]*%}", RetValGetXML.XMLCONTROLSTRING)  GT 0 OR  REFIND( "(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", RetValGetXML.XMLCONTROLSTRING)  GT 0  OR REFIND( "(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", RetValGetXML.XMLCONTROLSTRING)  GT 0  >

                        <cfset MESSAGE = MESSAGE & " Dynamic Message">

                        <cfif ListContains(arguments.CONTACTTYPES, 1)>
                            <cfinclude template="/session/cfc/includes/loadqueuevoice_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 2)>
                            <cfinclude template="/session/cfc/includes/loadqueueemail_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 3)>
                            <cfset inpContactTypeId = 3 />
                            <cfinclude template="/session/cfc/includes/loadqueuesms_dynamic.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 4)>
                            <cfinclude template="/session/cfc/includes/loadqueuepush_dynamic.cfm">
                        </cfif>
                    <cfelse>
                        <!---- Update status stop if exist---------->
                        <cfset RUNNING_CAMPAIGN = 1>
                        <cfset STOP_CAMPAIGN = 0>

                        <cfset MESSAGE = MESSAGE & " Static Message">

                        <cfquery name="StopBatchInQueue" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                                simplequeue.contactqueue
                            SET
                                DTSStatusType_ti = #RUNNING_CAMPAIGN#
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                            AND
                                DTSStatusType_ti = #STOP_CAMPAIGN#
                         </cfquery>


                        <!--- Static Content - Loads faster --->
                         <cfif ListContains(arguments.CONTACTTYPES, 1)>
                            <cfinclude template="/session/cfc/includes/loadqueuevoice_static.cfm">
                            <cfset MESSAGE = MESSAGE & " Debug Point BV VOICEONLYXMLCONTROLSTRING_VCH=#VOICEONLYXMLCONTROLSTRING_VCH#">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 2)>
                            <cfinclude template="/session/cfc/includes/loadqueueemail_static.cfm">
                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 3)>
                            <!---<cfinclude template="/session/cfc/includes/LoadQueueSMS_Static.cfm">--->
                            <!--- No such thing as static - MBLOX adds to many variables dependent on short code and target--->
                            <cfset inpContactTypeId = 3 />
                            <cfinclude template="/session/cfc/includes/loadqueuesms_dynamic.cfm">

                        </cfif>
                        <cfif ListContains(arguments.CONTACTTYPES, 4)>
                            <cfinclude template="/session/cfc/includes/loadqueuepush_dynamic.cfm">
                        </cfif>
                    </cfif>

                    <cfset MESSAGE = MESSAGE & " Debug Point C #Session.DBSourceEBM#">

                    <cfset MESSAGE = MESSAGE & " Debug Point D #DebugStr#">

                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
                    <cfset dataout.INPSCRIPTID = "#INPSCRIPTID#"/>
                    <cfset dataout.INPBATCHID = "#INPBATCHID#"/>
                    <cfset dataout.COUNTQUEUEDUPVOICE = "#COUNTQUEUEDUPVOICE#"/>
                    <cfset dataout.COUNTQUEUEDUPSMS = "#COUNTQUEUEDUPSMS#"/>
                    <cfset dataout.COUNTQUEUEDUPEMAIL = "#COUNTQUEUEDUPEMAIL#"/>
                    <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#"/>
                    <cfset dataout.INPBATCHDESC = "#INPBATCHDESC#"/>
                    <cfset dataout.TYPE = ""/>
                    <cfset dataout.MESSAGE = "#MESSAGE#"/>
                    <cfset dataout.ERRMESSAGE = ""/>

                    <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                        <cfinvokeargument name="userId" value="#session.userid#">
                        <cfinvokeargument name="moduleName" value="Campaign #INPBATCHDESC#">
                        <cfinvokeargument name="operator" value="Run Campaign ">
                    </cfinvoke>

                <cfelse>

                    <cfset dataout.RXRESULTCODE = -2/>
                    <cfset dataout.INPGROUPID = "#INPGROUPID#"/>
                    <cfset dataout.INPSCRIPTID = "#INPSCRIPTID#"/>
                    <cfset dataout.INPBATCHID = "#INPBATCHID#"/>
                    <cfset dataout.COUNTQUEUEDUPVOICE = "#COUNTQUEUEDUPVOICE#"/>
                    <cfset dataout.COUNTQUEUEDUPSMS = "#COUNTQUEUEDUPSMS#"/>
                    <cfset dataout.COUNTQUEUEDUPEMAIL = "#COUNTQUEUEDUPEMAIL#"/>
                    <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#"/>
                    <cfset dataout.INPBATCHDESC = "#INPBATCHDESC#"/>
                    <cfset dataout.TYPE = "-2"/>
                    <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in."/>
                    <cfset dataout.ERRMESSAGE = ""/>

                </cfif>

            <cfcatch TYPE="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                <cfset dataout.RXRESULTCODE = "#cfcatch.errorcode#"/>
                <cfset dataout.INPGROUPID =  "#INPGROUPID#"/>
                <cfset dataout.INPSCRIPTID =  "#INPSCRIPTID#"/>
                <cfset dataout.INPBATCHID =  "#INPBATCHID#"/>
                <cfset dataout.COUNTQUEUEDUPVOICE =  "#COUNTQUEUEDUPVOICE#"/>
                <cfset dataout.COUNTQUEUEDUPSMS =  "#COUNTQUEUEDUPSMS#"/>
                <cfset dataout.COUNTQUEUEDUPEMAIL =  "#COUNTQUEUEDUPEMAIL#"/>
                <cfset dataout.NEXTBATCHID =  "#NEXTBATCHID#"/>
                <cfset dataout.INPBATCHDESC =  "#INPBATCHDESC#"/>
                <cfset dataout.TYPE =  "#cfcatch.TYPE#"/>
                <cfset dataout.MESSAGE =  "#cfcatch.MESSAGE# #MESSAGE# #SerializeJSON(cfcatch)#"/>
                <cfset dataout.ERRMESSAGE =  "#cfcatch.detail#"/>

            </cfcatch>

            </cftry>

        </cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name = "GetContactListByCDF" access="remote" output="true" hint="Get list contact by CDF">
        <cfargument name="CDFData" required="no">
        <cfargument name="INPGROUPID" required="yes" >
        <cftry>
            <cfset var dataOut = {}>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfset var cdfLoopIndex = 1>
            <cfset var GetContactListByCDF = "">
            <cfset var cdfArray = CDFData>

            <cfset var cdfItem = ''/>
            <cfset var cdfRowItem = ''/>

            <!---get contact id by cdf --->
            <!---CDF is an array of structs which looks like [
                                                                {
                                                                    "CDFID":"12",
                                                                    "ROWS":[{"OPERATOR":"=","VALUE":"abc"},{"OPERATOR":"<>","VALUE":"abc"}]
                                                                },
                                                                {
                                                                    "CDFID":"13",
                                                                    "ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
                                                                }
                                                            ] --->
            <!---CDFID is used to query cdfId_int field in simplelists.contactvariable--->
            <!---ROWS is array of OPERATOR and VALUE --->
            <!---OPERATOR is operator used in query, it could be '=' (is), '<>'(is not), 'like'(is similar to) --->
            <!---VALUE is value of VariableValue_vch in simplelists.contactvariable--->
            <!---presume we have an array CDF with 2 elements --->
            <!---so we need to query contacts which match all condition element --->
            <!---that means the desired contacts must have exactly 2(2 is length of cdf array) records in simplelists.contactvariable,
                one has cdfid_int = 12 (12 comes from CDFID) and   VariableValue_vch    '='(equal sign comes from OPERATOR)    '123' (123 comes from VALUE) AND  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'abc' (abc comes from VALUE)
                and the left record has cdfid_int = 13 (13 comes from CDFID) and  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'def' (def comes from VALUE) AND   VariableValue_vch    '='(equal sign comes from OPERATOR)    '345' (345 comes from VALUE)
             --->
            <!---below query should be used when we need to make a complex query --->
            <!---we can use return list contact ids to pass into other query as a list param--->
            <cfquery name="GetContactListByCDF" datasource="#Session.DBSourceEBM#">
                SELECT
                    CDF1.ContactId_bi,CDF1.ContactVariableId_bi,CDF1.CdfId_int,VariableValue_vch
                FROM
                    (
                        SELECT cs.ContactId_bi,cv.ContactVariableId_bi,cv.CdfId_int,cv.VariableValue_vch
                        FROM
                            simplelists.grouplist gl
                        inner join
                            simplelists.groupcontactlist gcl
                        on
                            gl.GroupId_bi = gcl.GroupId_bi

                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                            AND
                                gl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                        </cfif>
                        and gl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        inner join simplelists.contactstring cs
                        on
                            gcl.ContactAddressId_bi = cs.ContactAddressId_bi

                        left join
                        simplelists.contactvariable cv
                            on cs.ContactId_bi = cv.ContactId_bi

                        GROUP BY ContactId_bi, CdfId_int

                        <cfif arraylen(cdfArray) GT 0 >
                        HAVING
                        (
                            <cfloop array="#cdfArray#" index="cdfItem">
                                <cfif cdfLoopIndex  GT 1>
                                     Or
                                </cfif>
                                 (cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#">
                                  <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
                                      And cv.VariableValue_vch
                                    <cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#cdfRowItem.VALUE#%">
                                    <cfelse>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#cdfRowItem.VALUE#">
                                    </cfif>
                                  </cfloop>
                                 )
                                 <cfset cdfLoopIndex++>
                            </cfloop>
                        )
                        </cfif>
                    ) AS CDF1
                 GROUP BY CDF1.ContactId_bi
                 <cfif  arraylen(cdfArray) GT 0>
                 having count(CDF1.ContactVariableId_bi) = <cfqueryparam cfsqltype="cf_sql_integer" value="#arraylen(cdfArray)#">
                 </cfif>
            </cfquery>

            <cfset var contactListByCdf = valuelist(GetContactListByCDF.ContactId_bi)>
            <cfset dataout.CONTACTLISTBYCDF = contactListByCdf NEQ ""? contactListByCdf : "-1"/>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

</cfcomponent>
