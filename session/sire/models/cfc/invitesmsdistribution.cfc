<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/public/sire/configs/paths.cfm">
	<cffunction name="AddInvitesToQueue" access="remote" output="false" hint="Add sms invites to queue">
		<cfargument name="inpContactStrings" type="string" required="true">
		<cfargument name="inpBatchId" type="numeric" required="true">

		<cfset var inpContactStringArray = DeserializeJSON(arguments.inpContactStrings) />

		<cfset var dataout = '0' />
		<cfset var nextBatchId = '' />
		<cfset var retValGetXML = '' />
		<cfset var countQueueDupSms	= 0 />
		<cfset var timezone = 0 />
		<cfset var queuedScheduledDate = "NOW()">
		<cfset var deliveryServiceComponentPath = "#Session.SessionCFCPath#.csc.csc" />
		<cfset var RUNNING_CAMPAIGN = 1>
		<cfset var STOP_CAMPAIGN = 0>
		<cfset var stopBatchInQueue = '' />

		<cftry>
			<cfif Session.USERID GT 0>
				<cfif !isnumeric(arguments.inpBatchId) OR arguments.inpBatchId LT 1 >
					<cfthrow MESSAGE="Invalid BatchId Specified" TYPE="Any" detail="" errorcode="-2">
				</cfif>
			</cfif>

			<!--- Verify all numbers are actual numbers --->
			<cfif !isnumeric(arguments.inpBatchId)>
				<cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
			</cfif>

			<!--- BuildXMLControlString --->
			<cfset retValGetXML = GetXMLControlString(arguments.inpBatchId)>

			<cfif retValGetXML.RXRESULTCODE LT 1>
				<cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#retValGetXML.MESSAGE# - #retValGetXML.ERRMESSAGE#" errorcode="-5">
			</cfif>

			<cfset nextBatchId = arguments.inpBatchId />

			<cfset timezone = GetUserTimezone() />

			<cfif REFIND("{%[^%]*%}", retValGetXML.XMLCONTROLSTRING)  GT 0 OR  REFIND( "(?i)<SWITCH[^>]+[^>]*>(.+?)</SWITCH>", retValGetXML.XMLCONTROLSTRING)  GT 0  OR REFIND( "(?i)<CONV[^>]+[^>]*>(.+?)</CONV>", retValGetXML.XMLCONTROLSTRING)  GT 0 >
				<cfinclude template="../includes/loadqueuesms_dynamic.cfm">
			<cfelse>
				<!---- Update status stop if exist---------->

				<cfquery name="stopBatchInQueue" datasource="#Session.DBSourceEBM#" >
					UPDATE
						simplequeue.contactqueue
					SET
						DTSStatusType_ti = #RUNNING_CAMPAIGN#
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpBatchId#">
					AND
						DTSStatusType_ti = #STOP_CAMPAIGN#
				</cfquery>

				<cfinclude template="../includes/loadqueuesms_dynamic.cfm">
			</cfif>

			<cfset dataout =  QueryNew("RXResultCode, inpBatchId, inpContactStrings, type, message, errMessage")>
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXResultCode", 1) />
			<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
			<cfset QuerySetCell(dataout, "inpContactStrings", "#inpContactStringArray#") />

			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXResultCode, inpBatchId, inpContactStrings, type, message, errMessage")>
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXResultCode","#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
				<cfset QuerySetCell(dataout, "inpContactStrings", "#inpContactStringArray#") />
				<cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />
				<cfset QuerySetCell(dataout, "errMessage", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetXMLControlString" access="remote" output="false" hint="Get existing XMLControlString">
		<cfargument name="inpBatchId" required="yes" default="0">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">

		<cfset var dataout = '0' />
		<cfset var XMLControlString = "">
		<cfset var getBatchOptions	= '' />

		<!---
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 

			Negative is failure
			-1 = general failure
			-2 = Session Expired
			-3 = 

		--->

		<cfoutput>

			<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXResultCode, inpBatchId, XMLControlString, type, message, errMessage")>
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXResultCode", -1) />
			<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
			<cfset QuerySetCell(dataout, "XMLControlString", "#XMLControlString#") />
			<cfset QuerySetCell(dataout, "type", "") />
			<cfset QuerySetCell(dataout, "message", "") />
			<cfset QuerySetCell(dataout, "errMessage", "") />

			<cftry>

				<!--- Validate session still in play - handle gracefully if not --->
				<cfif inpUserId GT 0>

					<!--- Cleanup SQL injection --->

					<!--- Verify all numbers are actual numbers --->

					<cfif !isnumeric(arguments.inpBatchId) OR !isnumeric(arguments.inpBatchId) >
						<cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-2">
					</cfif>                                        

					<!--- Read from DB Batch Options --->
					<cfquery name="getBatchOptions" datasource="#Session.DBSourceEBM#">
						SELECT                
							XMLControlString_VCH
						FROM
							simpleobjects.batch
						WHERE
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpBatchId#">
					</cfquery>     


					<!--- Pre distribution cleanup goes here ***JLP--->



					<cfset dataout =  QueryNew("RXResultCode, inpBatchId, XMLControlString, type, message, errMessage")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXResultCode", 1) />
					<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
					<cfset QuerySetCell(dataout, "XMLControlString", "#getBatchOptions.XMLControlString_VCH#") />
					<cfset QuerySetCell(dataout, "type", "") />
					<cfset QuerySetCell(dataout, "message", "") />
					<cfset QuerySetCell(dataout, "errMessage", "") />

				<cfelse>

					<cfset dataout =  QueryNew("RXResultCode, inpBatchId, XMLControlString, type, message, errMessage")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXResultCode", -2) />
					<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
					<cfset QuerySetCell(dataout, "XMLControlString", "#XMLControlString_VCH#") />
					<cfset QuerySetCell(dataout, "type", "-2") />
					<cfset QuerySetCell(dataout, "message", "Session Expired! Refresh page after logging back in.") />
					<cfset QuerySetCell(dataout, "errMessage", "") /> 

				</cfif>

				<cfcatch TYPE="any">

					<cfset dataout =  QueryNew("RXResultCode, inpBatchId, XMLControlString, type, message, errMessage")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXResultCode", -1) />
					<cfset QuerySetCell(dataout, "inpBatchId", "#arguments.inpBatchId#") />
					<cfset QuerySetCell(dataout, "XMLControlString", "#XMLControlString_VCH#") />
					<cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
					<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />
					<cfset QuerySetCell(dataout, "errMessage", "#cfcatch.detail#") />

				</cfcatch>

			</cftry>
		</cfoutput>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetUserTimezone" access="private" output="false" hint="Get current user timezone">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">

		<cfset var dataout = {} />
		<cfset var getUserTimezone = '' />
        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.timezone = 0 />

        <cfset dataout.message = "" />
        <cfset dataout.type = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
			<cfquery name="getUserTimezone" datasource="#Session.DBSourceEBM#">
				SELECT
					timezone_vch
				FROM
					simpleobjects.useraccount
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
			</cfquery>

			<cfset dataout.RXResultCode = 1 />

			<cfif getUserTimezone.RECORDCOUNT GT 0>
				<cfset dataout.timezone = getUserTimezone.timezone_vch />
			</cfif>

        	<cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
        	</cfcatch>
        </cftry>

        <cfreturn dataout />
	</cffunction>
</cfcomponent>