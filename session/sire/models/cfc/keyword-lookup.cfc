<cfcomponent hint="SMS Chat Feature">

	<cfinclude template="/public/sire/configs/paths.cfm"/>

	<cffunction name="getListKeyword" access="remote" hint="Get list key word with info">
		<cfargument name="inpUserId" TYPE="string" required="yes" default="#session.UserId#" />
		<cfargument name="inpKeyword" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var listKeyWord = "" />
		<cfset var countKeyWord = "" />
		<cfset var rsCount = "" />
		<cfset var item = {} />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
			<cfcase value="0">
				<cfset orderField = 'ir.Response_vch' />
			</cfcase>
		</cfswitch>

		<cftry>
			<cfif arguments.inpKeyword eq ''>
				<cfreturn dataout/>
			</cfif>
			<cfquery name="listKeyWord" datasource="#Session.DBSourceREAD#">
				SELECT SQL_CALC_FOUND_ROWS
					ir.IREResultsId_bi 	as KeywordId_int,
					ir.Response_vch 	as Response_vch,
					ua.EmailAddress_vch as UserName_vch,
					ua.UserId_int 		as UserId_int,
					ir.BatchId_bi 		as BatchId_bi,
					ir.Created_dt 		as Created_dt
				FROM
					simplexresults.ireresults ir
				INNER JOIN
					simpleobjects.useraccount ua
				ON
					ir.UserId_int = ua.UserId_int
				WHERE
					ir.IREType_int = 2
				AND
					UPPER(ir.Response_vch) LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UCase(arguments.inpKeyword)#%">
				ORDER BY
					ir.Created_dt DESC
				LIMIT
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>

			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>

			<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
			<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

			<cfif listKeyWord.RecordCount GT 0>
				<cfloop query="listKeyWord">
					<cfset item = {
					"UserId_int": listKeyWord.UserId_int,
					"UserName_vch": listKeyWord.UserName_vch,
					"KeywordId_int": listKeyWord.KeywordId_int,
					"Keyword_vch": HTMLEditFormat(listKeyWord.Response_vch),
					"BatchId_bi": listKeyWord.BatchId_bi,
					"Created_dt": DateFormat(listKeyWord.Created_dt, 'mm/dd/yyyy ') & TimeFormat(listKeyWord.Created_dt , "HH:mm:ss")
				} />
				<cfset arrayAppend(dataout["aaData"],item) />
			</cfloop>
		</cfif>

		<cfset dataout.RXRESULTCODE = 1 />

		<cfcatch>
			<cfset dataout = {}>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
	</cftry>

	<cfreturn dataout>

</cffunction>

</cfcomponent>