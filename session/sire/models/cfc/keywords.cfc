<cfcomponent>
	<cfinclude template="/session/administration/constants/userConstants.cfm">
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/public/paths.cfm" >
	

	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	
	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="GetKeywordByBatchId" access="remote" output="true" hint="Get list keyword by batchid">
		<cfargument name="inpHideEMS" type="string" required="no" default="1">
		<cfargument name="inpBatchId" type="string" required="no" default="0">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BATCHID = "#inpBatchId#" />
		<cfset dataout.KEYWORD =  arrayNew(1) />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

	   	<cfset var SCMBbyUser	= '' />
		<cfset var getKeywordByShortCodeAndUser	= '' />
		<cfset var getShortCodeRequestByUser	= '' />
		<cfset var tmpResult	= '' />

		<cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
		<cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />

		<cftry>
			<cfif getShortCodeRequestByUser.RXRESULTCODE GT 0>
				<cfquery name="getKeywordByShortCodeAndUser" datasource="#Session.DBSourceREAD#" result="tmpResult">
					SELECT 
						kw.KeywordId_int,
						kw.Keyword_vch AS Keyword,
						1 AS Triggered,
						kw.shortcoderequestID_INT,
						sc.ShortCode_vch,
						kw.IsDefault_bit As IsDefault,
						kw.ToolTip_vch,
	                    kw.Survey_int,
	                    kw.BatchId_bi
					FROM 
						sms.keyword as kw
					LEFT JOIN 
						sms.shortcode As SC
					ON kw.ShortCodeId_int = SC.ShortCodeId_int
					WHERE 
						SC.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SCMBbyUser.ShortCodeRequestId_int#">
						AND kw.IsDefault_bit = 1
						AND kw.Active_int = 1
	                    <cfif inpHideEMS GT 0>
	                        AND kw.EMSFlag_int = 0                    
	                    </cfif>
	                    <cfif inpBatchId GT 0>
	                    	AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	                    </cfif>
					UNION	
					SELECT 
						kw.KeywordId_int,
						kw.Keyword_vch AS Keyword,
						1 AS Triggered,
						kw.shortcoderequestID_INT,
						sc.ShortCode_vch,
						kw.IsDefault_bit As IsDefault,
						kw.ToolTip_vch,
	                    kw.Survey_int,
	                    kw.BatchId_bi
					FROM
						sms.keyword AS kw
					LEFT JOIN 
						sms.shortcoderequest AS scr
					ON 
						kw.shortcoderequestID_INT = scr.shortcoderequestID_INT
					LEFT JOIN 
						sms.shortcode As SC
					On 
						scr.ShortCodeId_int = sc.ShortCodeId_int
					WHERE 
						kw.shortcoderequestID_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SCMBbyUser.ShortCodeRequestId_int#">
						AND kw.Active_int = 1
	                    <cfif inpHideEMS GT 0>
	                        AND kw.EMSFlag_int = 0                    
	                    </cfif>
	                    <cfif inpBatchId GT 0>
	                    	AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	                    </cfif>
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.BATCHID = "#inpBatchId#" />
				<cfset dataout.MESSAGE = "Success" />


				<cfif getKeywordByShortCodeAndUser.RECORDCOUNT GT 0>
					<cfloop query="#getKeywordByShortCodeAndUser#">
						<cfset ArrayAppend(dataout.KEYWORD, KeywordId_int)>
					</cfloop>
				</cfif>
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.BATCHID = "#inpBatchId#" />
				<cfset dataout.MESSAGE = "No ShortCode Request" />
			</cfif>	
		<cfcatch>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>


		<cfreturn dataout />
	</cffunction>








	<cffunction name="GetKeywordByUserId" access="remote" output="true" hint="Get keyword by UserId and Short code for Datatable">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<!--- <cfargument name="sEcho"> --->
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->

		<cfargument name="inpShortCode" required="false">
		<cfargument name="isDashboard" required="false">
		<cfargument name="inpKeyword" required="true" default="">
		<cfargument name="inpAction" required="false" default="downgrade">
		<cfargument name="inpUserId" required="false" default="#Session.USERID#">

		<cfargument name="inpAdminSearch" required="false" default="0">


		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TOTALKWHAVETOPAID = 0 />
		<cfset dataout["LISTKEYWORD"] = ArrayNew(1) />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var tempItem = {} />

		<cfset var getKeyword = '' />
		<cfset var getTotalUsed = '' />
		<cfset var getTimesUsedEach = '' />
		<cfset var shortCode = '' />
		<cfset var active = 1>
		<cfset var rsCount = ''/>
		<cfset var inpKeywordStripped = ''/>
		<cfset var defaultShortCode = ''/>
		<cfset var getKeywordHaveToPaid = ''/>
		

		<cfif inpAction == 'upgrade'>
			<cfset active = 2>
		</cfif>

		<cftry>
			<cfif arguments.iDisplayStart < 0>
				<cfset arguments.iDisplayStart = 0>
			</cfif>
			
			<!--- remove old method: 
				<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
			--->
			<cfif arguments.inpAdminSearch EQ 0>
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode">
					<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
				</cfinvoke>	
				<cfset arguments.inpShortCode = defaultShortCode.SHORTCODEID />
			</cfif>
			
			

			<cfif arguments.inpKeyword NEQ ''>
				<cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />  
	   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />  
	   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />  
	   		</cfif>
	   		
			<cfquery name="getKeywordHaveToPaid" datasource="#Session.DBSourceREAD#">
				SELECT
					TotalKeywordHaveToPaid_int
				FROM
					simplebilling.userplans 
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					Status_int = 1
			</cfquery>

			<cfif getKeywordHaveToPaid.RECORDCOUNT EQ 1>
				<cfset dataout.TOTALKWHAVETOPAID = getKeywordHaveToPaid.TotalKeywordHaveToPaid_int />
			</cfif>

			<cfquery name="getKeyword" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					kw.Keyword_vch,
					kw.Created_dt,
					kw.KeywordId_int,
					kw.BatchId_bi
				FROM sms.keyword AS kw
				INNER JOIN simpleobjects.batch AS ba 
				ON kw.BatchId_bi = ba.BatchId_bi 
				LEFT JOIN simplexresults.ireresults as ir
				ON ba.BatchId_bi = ir.BatchId_bi
				WHERE 
				kw.Active_int = #active#
				AND ba.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND kw.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpShortCode#">
				<!--- AND kw.Keyword_vch = ir.Response_vch --->
				<cfif inpKeywordStripped NEQ ''>
					AND REPLACE(UPPER(kw.Keyword_vch)," ","") = UPPER(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeywordStripped#">)
				</cfif>

				GROUP BY 
					kw.Keyword_vch
				ORDER BY
					kw.Created_dt
				DESC
				<cfif arguments.isDashboard EQ 1>
					LIMIT 10
				<cfelse>
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfif>
				
			</cfquery>
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>

			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] =  (iTotalRecords GTE 1 ? iTotalRecords : 0)>
				<cfset dataout["iTotalRecords"] = (iTotalRecords GTE 1 ? iTotalRecords : 0)>
			</cfloop>

			<!--- <cfset dataout["iTotalRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) >
			<cfset dataout["iTotalDisplayRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) > --->

			<cfif getKeyword.RECORDCOUNT GT 0>
				<cfloop query="getKeyword">
					<cfset inpKeywordStripped = getKeyword.Keyword_vch/>
					<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), '"', "", "All") />  
	   				<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />  
	   				<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />  

					<cfquery name="getTotalUsed" datasource="#Session.DBSourceREAD#">
						SELECT 
							COUNT(ir.IREResultsId_bi) as total
						FROM
							simplexresults.ireresults as ir
						JOIN
							sms.keyword as k
						ON
							ir.BatchId_bi = k.BatchId_bi
						WHERE
							REPLACE(UPPER(ir.Response_vch)," ","") = UPPER(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeywordStripped#">)
						AND 
							k.Active_int = #active#
						AND
							ir.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getKeyword.BatchId_bi#">
						AND
							LENGTH(ir.ContactString_vch) < 14
					</cfquery>

					<cfset getTimesUsedEach = 0>
					<!--- <cfloop query="getTotalUsed">
						<cfif isvalid('telephone', getTotalUsed.ContactString_vch)>
							<cfset getTimesUsedEach = getTimesUsedEach + 1>
						</cfif>
					</cfloop> --->
					<cfif getTotalUsed.RECORDCOUNT GT 0>
						<cfset getTimesUsedEach = getTotalUsed.total/>
					</cfif>
					

					<cfset tempItem = 
						{
							KEYWORD = '#getKeyword.Keyword_vch#',
							TIMEUSED = '#getTimesUsedEach#', 
							CREATED = '#DateFormat(getKeyword.Created_dt, 'mm/dd/yyyy ')#',
							KEYWORDID = '#getKeyword.KeywordId_int#'
						}
					>
					<cfset ArrayAppend(dataout["LISTKEYWORD"],tempItem)>
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Get Keyword successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No keyword found' />
			</cfif>
			<cfcatch type="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetKeywordByUserIdForSelect" access="remote" output="true" hint="Get keyword by User id and Short code for Select box">
		<cfargument name="inpShortCode" required="false" default="">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset var getKeywordList = '' />
		<cfset var getTotalUsed = '' />
		<cfset dataout["LISTKEYWORD"] = ArrayNew(1) />

		<cfset var shortCode = '' />
		<cfset var inpKeywordStripped = ''/>
		<cfset var defaultShortCode = ''/>

		<cftry>
			<cfif arguments.inpShortCode EQ "">
				<!--- remove old method: 
				<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
				--->
				<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
				<cfset arguments.inpShortCode = defaultShortCode.SHORTCODEID />
			</cfif>

			<cfquery name="getKeywordList" datasource="#Session.DBSourceREAD#">
				SELECT
					kw.Keyword_vch,
					kw.Created_dt,
					kw.KeywordId_int,
					kw.BatchId_bi
				FROM sms.keyword AS kw
				INNER JOIN simpleobjects.batch AS ba 
				ON kw.BatchId_bi = ba.BatchId_bi 
				LEFT JOIN simplexresults.ireresults as ir
				ON ba.BatchId_bi = ir.BatchId_bi
				WHERE kw.Active_int = 1
				AND ba.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND kw.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpShortCode#">
				GROUP BY 
					kw.Keyword_vch
				ORDER BY
					kw.Created_dt
				DESC
				
			</cfquery>
			<cfif getKeywordList.RECORDCOUNT GT 0>
				<cfloop query="getKeywordList">
					<cfset inpKeywordStripped = getKeywordList.Keyword_vch/>
					<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), '"', "", "All") />  
   					<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />  
   					<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />  

					<cfquery name="getTotalUsed" datasource="#Session.DBSourceREAD#">
						SELECT 
							ir.Response_vch
						FROM
							simplexresults.ireresults as ir
						JOIN
							sms.keyword as k
						ON
							ir.BatchId_bi = k.BatchId_bi
						WHERE
							REPLACE(UPPER(ir.Response_vch)," ","") = UPPER(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeywordStripped#">)
						AND 
							k.Active_int = 1
						AND
							ir.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getKeywordList.BatchId_bi#">
					</cfquery>

					<cfset var tempItem = {} />
					
					<cfset tempItem = 
						{
							KEYWORD = '#getKeywordList.Keyword_vch#',
							TIMEUSED = '#getTotalUsed.RECORDCOUNT#', 
							CREATED = '#DateFormat(getKeywordList.Created_dt, 'mm/dd/yyyy ')#',
							KEYWORDID = '#getKeywordList.KeywordId_int#'
						}
					>
					<cfset ArrayAppend(dataout["LISTKEYWORD"],tempItem)>
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Get Keyword successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No keyword found' />
			</cfif>

			<cfcatch type="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="DeleteKeyword" access="remote" output="true" hint="Delete (deactive) Keyword by KeywordId">
		<cfargument name="inpKeywordId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var deactiveKeyword = '' />
		<cfset var result = '' />
		<cfset var userlog_vch = ''/>

		<cftry>
			<cfquery name="deactiveKeyword" datasource="#Session.DBSourceEBM#" result="result">
				UPDATE
					sms.keyword
				SET 
					Active_int = 0
				WHERE
					KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpKeywordId#">
			</cfquery>

			<cfif result.RECORDCOUNT GT 0>
				<!--- check if user have un-paid keyword then reduce it --->
				<cfquery name="deactiveKeyword" datasource="#Session.DBSourceEBM#" result="result">
					UPDATE 
						simplebilling.userplans up 
					SET 
						up.TotalKeywordHaveToPaid_int = if( up.TotalKeywordHaveToPaid_int > 0, up.TotalKeywordHaveToPaid_int - 1, TotalKeywordHaveToPaid_int )  
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
					AND
						Status_int = 1
				</cfquery>

				<!--- log action of user --->
				<cfset userlog_vch = 'User #session.USERID# Deleted Keyword id (#arguments.inpKeywordId#)' />
				<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.USERID#">
					<cfinvokeargument name="moduleName" value="User Deleted Keyword.">
					<cfinvokeargument name="operator" value="#userlog_vch#">
				</cfinvoke>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Delete Keyword successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Delete Keyword failed!' />
			</cfif>
			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="AdminDeleteKeyword" access="remote" output="true" hint="Delete (deactive) Keyword by KeywordId">
		<cfargument name="inpKeywordId" required="true">
		<cfargument name="inpUserId" required="true">
		<cfargument name="inpKeyword" required="true">
		<cfargument name="inpAdminId" required="false" default="#Session.USERID#">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var deactiveKeyword = '' />		
		<cfset var result = '' />
		<cfset var userlog_vch = 'Admin with User ID #Session.USERID# has been deleted keyword "#arguments.inpKeyword#" of User ID #arguments.inpUserId#.'/>
		<cftry>
			<cfquery name="deactiveKeyword" datasource="#Session.DBSourceEBM#" result="result">
				UPDATE
					sms.keyword
				SET 
					Active_int = 0
				WHERE
					KeywordId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpKeywordId#">
			</cfquery>

			<cfif result.RECORDCOUNT GT 0>
				<!--- check if user have un-paid keyword then reduce it --->
				<cfquery name="deactiveKeyword" datasource="#Session.DBSourceEBM#" result="result">
					UPDATE 
						simplebilling.userplans up 
					SET 
						up.TotalKeywordHaveToPaid_int = if( up.TotalKeywordHaveToPaid_int > 0, up.TotalKeywordHaveToPaid_int - 1, TotalKeywordHaveToPaid_int )  
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
					AND
						Status_int = 1
				</cfquery>

				<!--- log action of admin --->
				<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#arguments.inpUserId#">
					<cfinvokeargument name="moduleName" value="Admin Deleted Keyword.">
					<cfinvokeargument name="operator" value="#userlog_vch#">
				</cfinvoke>

				<cfset dataout.RXRESULTCODE = 1 />	
				<cfset dataout.MESSAGE = 'Delete Keyword successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Delete Keyword failed!' />
			</cfif>
			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	
    <cffunction name="getGroupString" access="private" hint="Get group string">
	    <cfargument name="inpOffsetDate" type="numeric" required="true">

	    <cfset var type = "day">

	   	<cfif arguments.inpOffsetDate GTE 14>
		   	<cfset type = "week">
		</cfif>
			
		<cfif arguments.inpOffsetDate GTE 89>
			<cfset type = "month">
		</cfif>

	   	<cfreturn type />
    </cffunction>


    <cffunction name="countDataByDate" access="private" hint="Count keyword by date">
	    <cfargument name="inpDate" type="string" required="true">
	    <cfargument name="inpShortCode" type="string" default="">
	    <cfargument name="inpKeyword" type="string" default="">
	    <cfset var inpKeywordStripped = ''/>

	    <cfif arguments.inpKeyword NEQ ''>
	    	<cfset inpKeywordStripped = arguments.inpKeyword/>
			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), '"', "", "All") />  
			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />  
			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />  	
	    </cfif>	
	    

	    <cfset var value = 0 />
	    <cfset var query =  {} />

    	<cfquery name="query" datasource="#Session.DBSourceEBM#">
			SELECT 
				COUNT(1) AS NumOfTimeUsed
			FROM 
				sms.keyword K 
			INNER JOIN simpleobjects.batch B
			ON K.BatchId_bi = B.BatchId_bi
			AND K.Active_int = 1
			AND K.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
			INNER JOIN simplexresults.ireresults I
			ON B.BatchId_bi = I.BatchId_bi
			AND K.BatchId_bi = I.BatchId_bi AND UPPER(K.Keyword_vch) = REPLACE(UPPER(I.Response_vch)," ","")
			WHERE 
				B.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND 
				I.Created_dt <= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#">, INTERVAL 86399 SECOND)
			AND
				LENGTH(I.ContactString_vch) < 14
			<cfif arguments.inpKeyword NEQ "">
				AND UPPER(K.Keyword_vch) = UPPER(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeywordStripped#">)
			</cfif>
    	</cfquery>

    	<cfset value = query.NumOfTimeUsed>
    	<cfreturn value />
    </cffunction>



	<cffunction name="GetStatistic" access="remote" hint="Get Statistic">
    	<cfargument name="inpKeyword" type="string" required="true" default="">
	    <cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">


	    <cfset var dataout = {} />
	    <cfset var dataout["DataList"] = [] />

	    <cfset var getStatisticData = [] />
	    <cfset var strGroupType = "" />
	    <cfset var defaultShortCode = "" />

	    <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	    <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


	    <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	    <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	    <cfset var dateOffset  = DateDiff("d",startDate, endDate) />


	    <cfset var shortCode = {} />
	    <cfset var day = '' />
	    <cfset var eachDay = '' />

	    <cftry>

	    	<cfinvoke method="getGroupString" returnvariable="strGroupType">
	    		<cfinvokeargument name="inpOffsetDate" value="#dateOffset#">
	    	</cfinvoke>

			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>

	    	<cfswitch expression="#strGroupType#">
		    	<cfcase value="day"> 
					
					<cfset var currDateOut = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(1,0,0,0)#">
						<cfset var valueItem = {
							"Time" 	= "#DateFormat(currDateOut, "yyyy-mm-dd")#",
							"Value" = 0,
							"format" = "#strGroupType#"
						}/>

						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#currDateOut#">
							<cfinvokeargument name="inpShortCode" value="#defaultShortCode.SHORTCODEID#">
							<cfinvokeargument name="inpKeyword" value="#arguments.inpKeyword#">
						</cfinvoke>

						<cfset dataout["DataList"].append(valueItem) />
						<cfset currDateOut = currDateOut.add('d', 1) >
					</cfloop>

				</cfcase> 
				<cfcase value="week"> 

					<!--- Create key list for data --->
					<cfset var dateList = [] />
					<cfset var currDate = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(7,0,0,0)#">
						<cfif DateDiff("d",currDate, endDate) GT 0 AND currDate LT endDate>
							<cfset  dateList.append(currDate) />
							<cfset currDate = currDate.add('d', 7) >
						</cfif>
					</cfloop>
					<cfset  dateList.append(endDate) />

					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpShortCode" value="#defaultShortCode.SHORTCODEID#">
							<cfinvokeargument name="inpKeyword" value="#arguments.inpKeyword#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>

				</cfcase> 
				<cfcase value="month"> 
					<cfset var dateList = [] />
					<cfset var currDate = startDate />
					<cfset dateList.append(currDate) />
					
					<cfloop condition="DateCompare(currDate, endDate) LT 0)">
						<cfset currDate = currDate.add('m', 1) /> 
						<cfif DateCompare(currDate, endDate) LT 0>
							<cfset dateList.append(CreateDate(currDate.year(), currDate.month(), 1)) />
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />

					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = {"Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpShortCode" value="#defaultShortCode.SHORTCODEID#">
							<cfinvokeargument name="inpKeyword" value="#arguments.inpKeyword#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>
				</cfcase> 
				
	    	</cfswitch>
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>

    <cffunction name="GetKeywordByListId" access="remote" output="true" hint="get keyword info by list ID">
    	<cfargument name="inpListKeywordId" type="array" required="true" default="">
	    <cfset var dataout = {} />
	    <cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.LIST = {}/>
		<cfset dataout.NAMELIST = []/>
	    <cfset var listKeyword = {} />

		<cfset var defaultShortCode = '' />

	    <cfif arrayLen(inpListKeywordId) GT 0>
		    <cftry>

		    	<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>

		    	<cfquery name="listKeyword" datasource="#Session.DBSourceEBM#">
					SELECT 
						K.Keyword_vch, K.KeywordId_int
					FROM 
						sms.keyword K 
					WHERE
						K.Active_int = 1
					AND 
						K.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultShortCode.SHORTCODEID#">
					AND
						K.KeywordId_int	IN (#ArrayToList(inpListKeywordId)#)
		    	</cfquery>

		    	<cfif listKeyword. RECORDCOUNT GT 0>
	    			<cfloop query="#listKeyword#">
	    				<cfset dataout.LIST["#KeywordId_int#"] = #Keyword_vch#/>
	    				<cfset arrayAppend(dataout.NAMELIST, Keyword_vch)/> 
	    			</cfloop>
	    			<cfset dataout.RXRESULTCODE = 1 />
		    	</cfif>

			    <cfcatch type="any">
			    	<cfset dataout.RXRESULTCODE = -1 />
				    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			    </cfcatch>
		    </cftry>
		<cfelse>
			 <cfset dataout.MESSAGE = "Invalid input."/>    
	    </cfif>
	    <cfreturn dataout />
    </cffunction>

</cfcomponent>