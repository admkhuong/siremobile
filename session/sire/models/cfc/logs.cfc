<cfcomponent>
	<cffunction name="createSireUserLog" access="remote" hint="create users log">
		<cfargument name="userId" required="false" default="#Session.UserId#" hint="user id">
		<cfargument name="moduleName" required="true" hint="">
		<cfargument name="operator" required="true">
		<cfset var dataout = {} />
		<cfset var insertUserLog=''>
		<cftry>
			<cfquery name="insertUserLog" datasource="#Session.DBSourceEBM#">
				insert into simpleobjects.userlogs(
					UserId_int,
					ModuleName_vch,
					Operator_vch,
					Timestamp_dt
				)value(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#" />,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#moduleName#" />, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#operator#" />,
					NOW()
				)
			
			</cfquery>
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "TYPE", "1") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
</cfcomponent>	