<cfcomponent>
	<cfinclude template="/public/paths.cfm"/>    
	<cffunction name="GetRequestCouponList" access="remote" output="false" hint="Get list Request Coupon">	
		<cfargument name="inpUserId" TYPE="string" required="no" default="" />
		<cfargument name="customFilter" default="">

		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		
		<cfset var GetListData="">
		<cfset var item="">
		<cfset var rsCount="">
		<cfset var filterData="">
		<cfset var filterItem="">
		<cfset var RetVarGetAdminPermission="">

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'Id_int' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'Id_int' />
		    </cfcase>		    
		    <cfcase value="2">
		        <cfset orderField = 'Id_int' />
		    </cfcase>	
		</cfswitch>

		<cftry>					
            <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfset filterData = DeserializeJSON(arguments.customFilter) />
			<cfquery name="GetListData" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					Id_int,
				  	RequestDetail_vch,
				  	UserId_int,
                    ResponseBy_int,
                    CreateDate_dt,
                    ResponseDate_dt,
                    Status_ti			
				FROM 
					simpleobjects.master_user_coupon_request
				WHERE
					1
                <cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR# 
						<cfif filterItem.TYPE EQ 'CF_SQL_VARCHAR'>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'/>
						<cfelse>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'/>
						</cfif>
					</cfloop>
				</cfif>				
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Id_int  DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListData.RecordCount GT 0>
				<cfloop query="GetListData">
					<cfset item = {
						"ID": Id_int, 						
						"UserID":UserId_int,
						"CreateDate":DATEFORMAT(CreateDate_dt,'mm/dd/yyyy'),
                        "Status":Status_ti,
                        "ResponseBy":ResponseBy_int,
                        "ResponseDate":DATEFORMAT(ResponseDate_dt,'mm/dd/yyyy')
												
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	<cffunction name="GetMasterCouponList" access="remote" output="false" hint="Get list Master Coupon">	
		<cfargument name="inpUserId" TYPE="string" required="no" default="" />
		<cfargument name="customFilter" default="">

		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		
		<cfset var GetListData="">
		<cfset var item="">
		<cfset var rsCount="">
		<cfset var filterData="">
		<cfset var filterItem="">
		<cfset var RetVarGetAdminPermission="">

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'Id_int' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'Id_int' />
		    </cfcase>		    
		    <cfcase value="2">
		        <cfset orderField = 'Id_int' />
		    </cfcase>	
		</cfswitch>

		<cftry>					
            <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfset filterData = DeserializeJSON(arguments.customFilter) />
			<cfquery name="GetListData" datasource="#Session.DBSourceREAD#">
				SELECT 
					SQL_CALC_FOUND_ROWS
					Id_int,				  	
				  	UserId_int,
				  	PlanId_int,
                    CouponCode_vch,
                    CreateDate_dt,
                    CreateBy_int,
                    Status_ti			
				FROM 
					simpleobjects.master_user_coupon_assignment
				WHERE
					1
                <cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR# 
						<cfif filterItem.TYPE EQ 'CF_SQL_VARCHAR'>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'/>						
						<cfelse>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'/>
						</cfif>
					</cfloop>
				</cfif>				
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Id_int  DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListData.RecordCount GT 0>
				<cfloop query="GetListData">
					<cfset item = {
						"ID": Id_int, 
						"CouponCode":CouponCode_vch,
						"UserID":UserId_int,
						"CreateDate":DATEFORMAT(CreateDate_dt,'mm/dd/yyyy'),
                        "CreateBy":CreateBy_int
												
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	

	<cffunction name="SaveCoupon" access="remote" output="false" hint="Save coupon">
		<cfargument name="inpId" TYPE="string" required="no" default="0" />			
		<cfargument name="inpPlanId" TYPE="string" required="no" default="" />		
		<cfargument name="inpUserId" TYPE="string" required="no" default="" />		
		<cfargument name="inpCouponCode" TYPE="string" required="no" default="" />		
		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		<cfset var CheckCouponExist=''/>
		<cfset var CheckUserExistCoupon=''/>
		<cfset var UpdateUserAccount=''/>
		<cfset var CheckUserEligible=''/>
		<cfset var mailData={}/>
		<cfset var mailTo=""/>
		<cfset var getUserInfo=""/>
        <cftry>
			<cfquery name="CheckUserExistCoupon" datasource="#Session.DBSourceREAD#">
				SELECT 
					Count(*) as Total		
				FROM 
					simpleobjects.master_user_coupon_assignment
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
				AND
					Id_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>					
			</cfquery>
			<cfif CheckUserExistCoupon.Total GT 0>
				<cfthrow type="Application" message="This user have already assigned coupon" detail="This user have already assigned coupon"/>
			</cfif>
			<cfquery name="CheckCouponExist" datasource="#Session.DBSourceREAD#">
				SELECT 
					Count(*) as Total		
				FROM 
					simpleobjects.master_user_coupon_assignment
				WHERE
					CouponCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(arguments.inpCouponCode)#"/>
				AND
					Id_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>		
			</cfquery>
			<cfif CheckCouponExist.Total GT 0>
				<cfthrow type="Application" message="Coupon code (#arguments.inpCouponCode#) have already assigned, please try an others one" detail="Coupon code (#arguments.inpCouponCode#) have already assigned, please try an others one"/>
			</cfif>
			<cfquery name="CheckUserEligible" datasource="#Session.DBSourceREAD#">
				SELECT 
					Count(*) as Total		
				FROM 
					simpleobjects.useraccount
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
				AND
					UserType_int=1						
			</cfquery>
			<cfif CheckUserEligible.Total EQ 0>
				<cfthrow type="Application" message="We cannot create coupon code for this type of user" detail="We cannot create coupon code for this type of user"/>
			</cfif>

			<cfif arguments.inpId GT 0>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					UPDATE
						simpleobjects.master_user_coupon_assignment
					SET
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,
						PlanId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#"/>,
						CouponCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCouponCode#"/>		
					WHERE
						Id_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>											
				</cfquery>
			<cfelse>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
					INSERT INTO
						simpleobjects.master_user_coupon_assignment
						(																								
							UserId_int,
							PlanId_int,
							CouponCode_vch,
							CreateDate_dt,
							CreateBy_int,
							Status_ti						
						)
					VALUES
						(							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,						
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCouponCode#"/>,																		
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"/>,						
							1							
						)
				</cfquery>
			</cfif>
			
			<cfquery datasource="#Session.DBSourceEBM#" name="UpdateUserAccount">
				UPDATE
					simpleobjects.useraccount
				SET
					UserType_int = 3,
					UserLevel_int= 1
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>											
			</cfquery>
			<cfquery name="getUserInfo" datasource="#Session.DBSourceREAD#">
				SELECT
					EmailAddress_vch
				FROM
					simpleobjects.useraccount
				WHERE                
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					Active_int > 0       
			</cfquery>  

			<cfset mailTo=getUserInfo.EmailAddress_vch/>
			<cfset mailData.CouponCode=arguments.inpCouponCode/>
			<cfset mailData.UserID=arguments.inpUserId/>
			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
				<cfinvokeargument name="to" value="#mailTo#">
				<cfinvokeargument name="type" value="html">
				<cfinvokeargument name="subject" value="[SIRE][Your Master Coupon was assigned]">
				<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_master_coupon_assigned.cfm">
				<cfinvokeargument name="data" value="#SerializeJSON(mailData)#">
			</cfinvoke>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
</cfcomponent>