<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/session/sire/configs/paths.cfm">
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfset LOCAL_LOCALE = "English (US)">

   <cffunction name="AddTOC" access="remote" output="false" hint="Add a new Top Level TOC for the current User">
        <cfargument name="inpDesc" TYPE="string" required="no" default="New TOC"/>
                                       
        <cfset var dataout = {} />    
		<cfset var AddTOCQuery = '' />
		<cfset var insertResult	= '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.MLPID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                                                       
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddTOCQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               INSERT INTO 
               		simplelists.mlp_toc
                    (
                    	PKId_bi, 
                    	UserId_int, 
                    	Created_dt, 
                    	Updated_dt, 
                    	Desc_vch,
                    	Active_int
                     )
               VALUES
                    (	
                    	NULL,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
	                    NOW(), 
	                    NOW(),
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
	                    1
	                )                                        
            </cfquery>         
            
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Create TOC OK.'>
	        	<cfset dataout.MLPID = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create TOC failed. Please try again later'>	
	        </cfif>
	                                 
            <cfset dataout.RXRESULTCODE = "1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTOCs" access="remote" output="false" hint="Get All Active TOC(s)">
				
		<cfset var dataout	= {} />
		<cfset var GetAllTOCs	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>
		
		<cftry>		
			
			<cfquery name="GetAllTOCs" datasource="#Session.DBSourceREAD#">
				SELECT 
				   	PKId_bi,
				   	Desc_vch				   
				FROM 
					simplelists.mlp_toc				  
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND  
					Active_int = 1							
			</cfquery>

			<cfset dataout.QUERYRES = GetAllTOCs>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />
			
		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>	
	 	<cfreturn dataout />		
	</cffunction>
	
	 <cffunction name="AddTopic" access="remote" output="false" hint="Add a new topic to the menu">
        <cfargument name="inpDesc" TYPE="string" required="no" default="New Topic"/>
        <cfargument name="inpParentId" TYPE="string" required="no" default="New Topics Parent"/>
        <cfargument name="inpParentIdType" TYPE="string" required="no" default="New Topics Parent Type 1=TOC 2=Sub Topic"/>
        <cfargument name="inpMLPId" TYPE="string" required="no" default="New Topics Parent"/>
                                       
        <cfset var dataout = {} />    
		<cfset var AddTopicQuery = '' />
		<cfset var insertResult	= '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.NEWMLPTOPICID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <!--- Validation Parent --->
            <cfif !isNumeric(inpParentId) || inpParentId EQ "0">
	            <cfset dataout.RXRESULTCODE = "-5" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWMLPTOPICID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Parent Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif>    
            
            <!--- Validation Parent Type --->
            <cfif !isNumeric(inpParentIdType) || inpParentIdType EQ "0">
	            <cfset dataout.RXRESULTCODE = "-6" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWMLPTOPICID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Parent Type Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
           
            <!--- Validation MLP Id --->
            <cfif !isNumeric(inpMLPId) >
	            <cfset dataout.RXRESULTCODE = "-7" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWMLPTOPICID = "0" />
	            <cfset dataout.MESSAGE = "Invalid MLP Id Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
                                                        
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddTopicQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               INSERT INTO 
               		simplelists.mlp_topic
                    (
                    	PKId_bi, 
                    	UserId_int, 
                    	Created_dt, 
                    	Updated_dt, 
                    	Desc_vch,
                    	Active_int,
                    	ParentId_bi,
                    	ParentType_int,
                    	MLPId_int
                     )
               VALUES
                    (	
                    	NULL,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
	                    NOW(), 
	                    NOW(),
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
	                    1,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpParentId#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpParentIdType#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMLPId#">	                    
	                )                                        
            </cfquery>         
            
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Create TOC OK.'>
	        	<cfset dataout.NEWMLPTOPICID = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create TOC failed. Please try again later'>	
	        </cfif>
	                 
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetTopics" access="remote" output="false" hint="Get Active Topic(s)">
		<cfargument name="inpParentId" TYPE="string" required="no" default="New Topics Parent"/>
        <cfargument name="inpParentIdType" TYPE="string" required="no" default="New Topics Parent Type 1=TOC 2=Sub Topic"/>
        
		<cfset var dataout	= {} />
		<cfset var GetAllTopics	= '' />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.QUERYRES = QueryNew('')>
				
		<cftry>		
			
			<!--- Validation Parent --->
            <cfif !isNumeric(inpParentId) || inpParentId EQ "0">
	            <cfset dataout.RXRESULTCODE = "-5" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.MLPID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Parent Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif>    
            
            <!--- Validation Parent Type --->
            <cfif !isNumeric(inpParentIdType) || inpParentIdType EQ "0">
	            <cfset dataout.RXRESULTCODE = "-6" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.MLPID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Parent Type Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
            
			<cfquery name="GetAllTopics" datasource="#Session.DBSourceREAD#">
				SELECT 
				   	PKId_bi,
				   	Desc_vch,
				   	MLPId_int,
				   	Order_int				   
				FROM 
					simplelists.mlp_topic				  
				WHERE 
					ParentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpParentId#">
				AND			
					ParentType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpParentIdType#">
				AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				AND  
					Active_int = 1	
				ORDER BY
					Order_int ASC, Desc_vch ASC, PKId_bi ASC							
			</cfquery>

			<cfset dataout.QUERYRES = GetAllTopics>
			<cfset dataout.RXRESULTCODE = 1>
			<cfreturn dataout />
			
		<cfcatch>
		       	<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
		   	    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		   	    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		   	    <cfreturn dataout />
		</cfcatch>
		</cftry>	
	 	<cfreturn dataout />		
	</cffunction>
	
	
	<cffunction name="CreateMLPAndLinkToTopic" access="remote" output="false" hint="Add a new MLP and link ot a Topic for the current User">
        <cfargument name="inpMLPTemplateId" TYPE="string" required="no" default="0" hint="The template Id to start new MLP off of"/>
        <cfargument name="inpTopicId" TYPE="string" required="no" default="0" hint="The Topic Id to Link the MLP to"/>
             
                                                                 
        <cfset var dataout = {} />    
		<cfset var AddTOCQuery = '' />
		<cfset var inpFormData = '' />
		<cfset var RetVarDoDynamicTransformations = '' />
		<cfset var RetVarGetUserOrganization = '' />
		<cfset var RetAddMLP = '' />
		<cfset var RetVarLinkMLPToTopic = '' />
		<cfset var RetCPPXData	= '' />
		
		     
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.NEWMLPID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
        
        
			<cfif inpMLPTemplateId GT 0 >		
	
				<!--- Read exisitng template data --->
				<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
					 <cfinvokeargument name="ccpxDataId" value="#inpMLPTemplateId#">
				</cfinvoke> 
				
				<!--- Read Profile data and put into form --->
		        <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>
		      
		        <!---Create a data set to run against dynamic data --->
		        <cfset inpFormData = StructNew() />
		        
		        <cfset inpFormData.inpBrand = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
		        <cfset inpFormData.inpBrandFull = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
		        <cfset inpFormData.inpBrandWebURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
		        <cfset inpFormData.inpBrandWebTinyURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
		        <cfset inpFormData.inpBrandPhone = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
		        <cfset inpFormData.inpBrandeMail = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />
		        
		        <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
		        <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
		        <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
		        <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
		        <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
		        <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>
		                    
		        <cfset inpFormData.CompanyNameShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
		        <cfset inpFormData.CompanyName = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
		        <cfset inpFormData.CompanyWebSite = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
		        <cfset inpFormData.CompanyWebSiteShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
		        <cfset inpFormData.CompanyPhoneNumber = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
		        <cfset inpFormData.CompanyEMailAddress = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />
		
				
				<!--- Do the MLP transforms here --->  
		        <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
		            <cfinvokeargument name="inpResponse_vch" value="#RetCPPXData.RAWCONTENT#"> 
		            <cfinvokeargument name="inpContactString" value="">
		            <cfinvokeargument name="inpRequesterId_int" value=""> 
		            <cfinvokeargument name="inpFormData" value="#inpFormData#"> 
		            <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
		            <cfinvokeargument name="inpShortCode" value="">
		            <cfinvokeargument name="inpBatchId" value="">
		            <cfinvokeargument name="inpBrandingOnly" value="1">
		        </cfinvoke>   
		        
		        <!--- Update the current template data --->           
				<cfset RetCPPXData.RAWCONTENT = RetVarDoDynamicTransformations.RESPONSE />  
										
				<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
					<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
					<cfinvokeargument name="inpDesc" value="New MLP based on #RetCPPXData.CPPNAME#">
					<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
				</cfinvoke> 		
					
			<cfelse>
			
				<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
					<cfinvokeargument name="inpRawContent" value="">
					<cfinvokeargument name="inpDesc" value="New Empty MLP">
				</cfinvoke> 		
			
			</cfif>	                                              
            
        	<cfinvoke method="LinkMLPToTopic" returnvariable="RetVarLinkMLPToTopic">
                <cfinvokeargument name="inpTopicId" value="#inpTopicId#">  
                <cfinvokeargument name="inpMLPId" value="#RetAddMLP.CPPID#">                   
            </cfinvoke>          
                            	                                 
            <cfset dataout.RXRESULTCODE = "1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.NEWMLPID = "#RetAddMLP.CPPID#" />
            <cfset dataout.MESSAGE = "#SerializeJSON(RetVarLinkMLPToTopic)#" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>

	<cffunction name="LinkMLPToTopic" access="remote" output="false" hint="Add a new MLP and link ot a Topic for the current User">
        <cfargument name="inpTopicId" TYPE="string" required="no" default="0" hint="The Topic Id to Link the MLP to"/>
        <cfargument name="inpMLPId" TYPE="string" required="no" default="0" hint="The MLP Id to Link to the Topic"/>
                                            
        <cfset var dataout = {} />    
		<cfset var UpdateMLPForTopic = '' />
		<cfset var insertResult	= '' />
        
         <!--- Validation Topic Id Type --->
        <cfif !isNumeric(inpTopicId) || inpTopicId EQ "0">
            <cfset dataout.RXRESULTCODE = "-6" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.MLPID = "0" />
            <cfset dataout.MESSAGE = "Invalid Topic ID Specified" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfreturn dataout />
        </cfif>      
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.MLPID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                         
            <!--- Secure update to logged in user! --->                                           
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateMLPForTopic" datasource="#Session.DBSourceEBM#">
               	UPDATE 
               		simplelists.mlp_topic
			   	SET	
			   		MLPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMLPId#">
			   	WHERE
			   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
			   	AND	
			   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		    </cfquery>         
                        
        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Updated Topic MLP Id OK.'>
        	<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>

	<cffunction name="UpdateTopicDesc" access="remote" output="false" hint="Update The Topics Description">
    	<cfargument name="inpTopicId" TYPE="string" required="true" default="0" />
    	<cfargument name="inpDesc" TYPE="string" required="true" />

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

	   	<cfset var UpdateTopicDescQuery = ''>
	   	<cfset var result = ''>
	   	<cfset var xmlOldData = ''>
	   	<cfset var cppData	= '' />
		
	   	<cftry>
	   		
	   		 <!--- Validation Topic Id Type --->
		        <cfif !isNumeric(arguments.inpTopicId) || arguments.inpTopicId EQ "0">
		            <cfset dataout.RXRESULTCODE = "-6" />           
		            <cfset dataout.TYPE = "" />
		            <cfset dataout.MLPID = "0" />
		            <cfset dataout.MESSAGE = "Invalid Topic ID Specified" />
		            <cfset dataout.ERRMESSAGE = "" />
		            
		            <cfreturn dataout />
		        </cfif>      

				<cfif TRIM(arguments.inpDesc) EQ "">
					<cfset arguments.inpDesc = "Blank Topic Desc" />
				</cfif>
	   		
			   	<cfquery name="UpdateTopicDescQuery" datasource="#Session.DBSourceEBM#" >
		            UPDATE 
               			simplelists.mlp_topic
				   	SET	
				   		Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">
				   	WHERE
				   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
				   	AND	
				   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			    </cfquery>         

		        <cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Updated Topic Desc OK.'>
	        	<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />
	            <cfset dataout.ERRMESSAGE = "" />
            
	       
		    <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Update Topic Description Failed. Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry>  
	    	

	    <cfreturn dataout>
    </cffunction>    
    
    <cffunction name="DeleteMLPTopic" access="remote" output="false" hint="Logically delete The Topic">
    	<cfargument name="inpTopicId" TYPE="string" required="true" default="0" />
    
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

	   	<cfset var UpdateTopicQuery = ''>
	   	<cfset var result = ''>
	   	<cfset var xmlOldData = ''>
	   	<cfset var cppData	= '' />
		
	   	<cftry>
	   		
	   		 	<!--- Validation Topic Id Type --->
		        <cfif !isNumeric(arguments.inpTopicId) || arguments.inpTopicId EQ "0">
		            <cfset dataout.RXRESULTCODE = "-6" />           
		            <cfset dataout.TYPE = "" />
		            <cfset dataout.MLPID = "0" />
		            <cfset dataout.MESSAGE = "Invalid Topic ID Specified" />
		            <cfset dataout.ERRMESSAGE = "" />
		            
		            <cfreturn dataout />
		        </cfif>      
					   		
			   	<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
		            UPDATE 
               			simplelists.mlp_topic
				   	SET	
				   		Active_int = 0
				   	WHERE
				   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
				   	AND	
				   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			    </cfquery>         

		        <cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Logically Deleted the Topic OK.'>
	        	<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />
	            <cfset dataout.ERRMESSAGE = "" />
            
	       
		    <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Logically Delete Topic Failed. Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry>  
	    	

	    <cfreturn dataout>
    </cffunction>

 <cffunction name="ReadMLPTOCDescription" access="remote" output="false" hint="Read the TOC Description">
    	<cfargument name="inpTOCId" TYPE="string" required="true" default="0" />
    
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.DESC = "" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

	   	<cfset var ReadTOCQuery = ''>
	   	<cfset var result = ''>
	   	<cfset var xmlOldData = ''>
	   	<cfset var cppData	= '' />
		
	   	<cftry>
	   		
	   		 	<!--- Validation Topic Id Type --->
		        <cfif !isNumeric(arguments.inpTOCId) || arguments.inpTOCId EQ "0">
		            <cfset dataout.RXRESULTCODE = "-6" />           
		            <cfset dataout.TYPE = "" />
		            <cfset dataout.MLPID = "0" />
		            <cfset dataout.MESSAGE = "Invalid inpTOCId Specified" />
		            <cfset dataout.ERRMESSAGE = "" />
		            
		            <cfreturn dataout />
		        </cfif>      
					   		
			   	<cfquery name="ReadTOCQuery" datasource="#Session.DBSourceEBM#" >
		            SELECT
		            	Desc_vch 
               		FROM
               			simplelists.mlp_toc
				   	WHERE
				   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTOCId#">
				   	AND	
				   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				   	AND	
				   		Active_int = 1	
			    </cfquery>         

		        <cfset dataout.RXRESULTCODE = 1 />
		        <cfset dataout.DESC = "#ReadTOCQuery.Desc_vch#" />
	        	<cfset dataout.MESSAGE = ''>
	        	<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />
	            <cfset dataout.ERRMESSAGE = "" />
            
	       
		    <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Read TOC Description Failed. Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry>  
	    	

	    <cfreturn dataout>
    </cffunction>


	<cffunction name="MoveMLPTopic" access="remote" output="false" hint="Move The Topic">
    	<cfargument name="inpTopicId" TYPE="string" required="true" default="0" />
    	<cfargument name="inpParentId" TYPE="string" required="no" hint="New Topics Parent"/>
        <cfargument name="inpParentIdType" TYPE="string" required="no" hint="New Topics Parent Type 1=TOC 2=Sub Topic"/>
        <cfargument name="inpTargetParentId" TYPE="string" required="no" hint="New Topics Parent"/>
        <cfargument name="inpTargetParentIdType" TYPE="string" required="no" hint="New Topics Parent Type 1=TOC 2=Sub Topic"/>
        <cfargument name="inpTargetPos" TYPE="string" required="no" default="0"/>
    
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />

	   	<cfset var UpdateTopicQuery = ''>
	   	<cfset var GetAllTargetTopics = '' />
	   	<cfset var result = ''>
	   	<cfset var xmlOldData = ''>
	   	<cfset var cppData	= '' />
	   	<cfset var NewOrder	= 0 />
		
	   	<cftry>
	   	
	   		<!--- Validation Topic Id  --->
	        <cfif !isNumeric(arguments.inpTopicId) || arguments.inpTopicId EQ "0">
	            <cfset dataout.RXRESULTCODE = "-6" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.MLPID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Topic ID Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
	        </cfif>   
		        	   	
	   		<!--- New parent --->
	   		<cfif inpParentId NEQ inpTargetParentId OR inpParentIdType NEQ inpTargetParentIdType> 
		   		
		   		<!--- Select everything so we can re-position --->
		   		<cfquery name="GetAllTargetTopics" datasource="#Session.DBSourceREAD#">
					SELECT 
					   	PKId_bi,
					   	Desc_vch,
					   	MLPId_int,
					   	Order_int				   
					FROM 
						simplelists.mlp_topic				  
					WHERE 
						ParentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentId#">
					AND			
						ParentType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentIdType#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					AND  
						Active_int = 1	
					ORDER BY
						Order_int ASC, Desc_vch ASC, PKId_bi ASC	
						
					LIMIT 1000 <!--- Security limit - not Topic allowed over 1000 long or we need to page through these queries--->	
				</cfquery>

				<!--- Order is 0 based in DB to match Javascript position --->
				<cfset  NewOrder = 0 />
				
				<cfloop query="GetAllTargetTopics">
					
					<cfif NewOrder GTE inpTargetPos>
						
						<!--- skip a number for new item --->
						<cfset NewOrder	= NewOrder + 1 />
						
					</cfif>	
					
					<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
			            UPDATE 
	               			simplelists.mlp_topic
					   	SET	
					   		Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NewOrder#">
					   	WHERE
					   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAllTargetTopics.PKId_bi#">
					   	AND	
					   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				    </cfquery>   
				
					<cfset NewOrder	= NewOrder + 1 />
					
				</cfloop>
				
				<!--- Now move the Topic to the new Parent and set its order to the Target Pos --->	
		   		<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
		            UPDATE 
               			simplelists.mlp_topic
				   	SET	
				   		ParentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentId#">, 
				   		ParentType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentIdType#">,
				   		Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetPos#">			   		
				   	WHERE
				   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
				   	AND	
				   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			    </cfquery>   
				    
			<cfelse>
		   	
		   		<!--- same parent just new position --->	  
		   		<!--- Select everything so we can re-position --->
		   		<cfquery name="GetAllTargetTopics" datasource="#Session.DBSourceREAD#">
					SELECT 
					   	PKId_bi,
					   	Desc_vch,
					   	MLPId_int,
					   	Order_int				   
					FROM 
						simplelists.mlp_topic				  
					WHERE 
						ParentId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentId#">
					AND			
						ParentType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetParentIdType#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					AND  
						Active_int = 1	
					AND
						PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
					ORDER BY
						Order_int ASC, Desc_vch ASC, PKId_bi ASC	
					
					LIMIT 1000 <!--- Security limit - not Topic allowed over 1000 long or we need to page through these queries--->	
						
				</cfquery>

				<!--- Order is 0 based in DB to match Javascript position --->
				<cfset  NewOrder = 0 />
				
				<cfloop query="GetAllTargetTopics">
					
					<cfif NewOrder EQ inpTargetPos>
												
						<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
				            UPDATE 
		               			simplelists.mlp_topic
						   	SET	
						   		Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetPos#">
						   	WHERE
						   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
						   	AND	
						   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					    </cfquery>
				    
						<cfset NewOrder	= NewOrder + 1 />
						
					</cfif>
					
					<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
			            UPDATE 
	               			simplelists.mlp_topic
					   	SET	
					   		Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NewOrder#">
					   	WHERE
					   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAllTargetTopics.PKId_bi#">
					   	AND	
					   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
				    </cfquery>
				    
					<cfset NewOrder	= NewOrder + 1 />
					
					<cfif NewOrder GTE GetAllTargetTopics.RecordCount>
												
						<cfquery name="UpdateTopicQuery" datasource="#Session.DBSourceEBM#" >
				            UPDATE 
		               			simplelists.mlp_topic
						   	SET	
						   		Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTargetPos#">
						   	WHERE
						   		PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpTopicId#">
						   	AND	
						   		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
					    </cfquery>
					    						
					</cfif>
										
				</cfloop>
						   	
		   	</cfif>
			   
	        <cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Moved the Topic OK.'>
        	<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
		    <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Move Topic Failed. Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry>  	    	

	    <cfreturn dataout>
    </cffunction>




	
</cfcomponent>