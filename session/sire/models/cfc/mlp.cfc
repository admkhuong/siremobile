<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/session/sire/configs/paths.cfm">
	<cfparam name="Session.USERID" default="0"/> 
	<cfparam name="Session.loggedIn" default="0"/>
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="SetupCPP" access="remote" output="true" hint="Create MLP">
		<cfargument name="cppxName" type="string" required="yes">
		<cfargument name="cppxURL" type="string" required="yes">
		<cfargument name="checkTerms" type="string" required="no">
		<cfargument name="cppxTermOfService" type="string" required="no">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var MyDoc = '' />

		<cfset var insertResult = ''>
		<cfset var insertCPP = ''>
		<cfset var XMLDataString = ''>
		<cfset var CreateUUID = ''/>
		<cfset var URLName = ''/>
		<cfset var MyDoc	= '' />

		<cfset CreateUUID = CreateUUID()/>

		<cfif arguments.cppxURL NEQ ''>
			<cfset URLName = arguments.cppxURL/>
		<cfelse>
			<cfset URLName = CreateUUID/>
		</cfif>

		<cftry>
			<cfxml variable="MyDoc"> 
				<CPPX MULTISUBLIST="0">
					<cfif arguments.checkTerms EQ 1 AND arguments.cppxTermOfService NEQ '' >
						<OBJ TYPE="term-of-service">#arguments.cppxTermOfService#</OBJ> 
					</cfif>
				</CPPX>
			</cfxml>

			<cfset XMLDataString = REReplaceNoCase(toString(MyDoc), '<\?[^>]+\?>', '')>
			
			<cfquery name="insertCPP" datasource="#Session.DBSourceEBM#" result="insertResult">
				INSERT INTO
				simplelists.cppx_data(
					cppxUUID_vch,
					XMLDataString,
					cppxName_vch,
					cppxURL_vch,
					created_dt,
					UserId_int,
					Status_int
					)
				VALUES 
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CreateUUID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLDataString#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.cppxName#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#URLName#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">		                
					)   
			</cfquery>

			<cfif insertResult.GENERATED_KEY GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Create MLP successfully.'>
				<cfset dataout.CPPID = insertResult.GENERATED_KEY />
			<cfelse>
				<cfset dataout.MESSAGE = 'Update fail. Please try again later'>	
			</cfif>
			<cfcatch>
				<cfset dataout.MESSAGE = 'Update fail. Please try again later'>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

    <cffunction name="ReadCPP" access="remote" output="true" hint="Read MLP">
		<cfargument name="ccpxDataId" type="string" required="no" default="0">
		<cfargument name="ccpxURL" type="string" required="no" default="">


		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.CPPUUID = "0" />
		<cfset dataout.CPPNAME = "" />
		<cfset dataout.CPPURL = "" />
		<cfset dataout.STATUS = "0" />
		<cfset dataout.CUSTOMCSS = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.DATA = "" />
		<cfset dataout.EXPIRE_DT = "" />
		<cfset dataout.RAWCONTENT = "Error Reading Content" />
		<cfset dataout.AUTOPUBLISH = 1 />

		<cfset var readCPP = ''>
		<cftry>
			<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
				SELECT 
				XMLDataString,
				cppxUUID_vch,
				cppxName_vch,
				cppxURL_vch, 
				css_custom, 
				css_external_link,
				css_type,
				RawContent_vch,
				CustomCSS_vch,
				Status_int,
				userId_int,
				AutoPublish_int,
				Expire_dt
				FROM	
				simplelists.cppx_data
				WHERE  

				<cfif ccpxDataId EQ 0 >
					cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.ccpxURL#">							                
				<cfelse>		            
					ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
				</cfif>
				AND    
				status_int >= 0    
			</cfquery>

			<cfif readCPP.RecordCount GT 0>

				<cfset dataout.DATA = readCPP.XMLDataString>
				<cfset dataout.CPPNAME = readCPP.cppxName_vch>
				<cfset dataout.CPPURL = readCPP.cppxURL_vch>
				<cfset dataout.CPPID = arguments.ccpxDataId />
				<cfset dataout.CPPUUID = readCPP.cppxUUID_vch />
				<cfset dataout.CSS_CUSTOM = readCPP.css_custom />
				<cfset dataout.CSS_EXTERNAL_LINK = readCPP.css_external_link />
				<cfset dataout.CSS_TYPE = readCPP.css_type />
				<cfset dataout.RAWCONTENT = readCPP.RawContent_vch />
				<cfset dataout.CUSTOMCSS = readCPP.CustomCSS_vch />
				<cfset dataout.STATUS = readCPP.Status_int />
				<cfset dataout.USERID = readCPP.userId_int />
				<cfset dataout.AUTOPUBLISH = readCPP.AutoPublish_int />
				<cfset dataout.EXPIRE_DT = LSDateFormat(readCPP.Expire_dt, 'yyyy-mm-dd') />				
				<cfset dataout.RXRESULTCODE = 1 />


			<cfelse>
				<cfset dataout.MESSAGE = 'Can not get this MLP data'>
			</cfif>	

			<cfcatch>
				<cfset dataout.MESSAGE = 'Can not get this MLP data'>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>    	
		</cftry>    	

		<cfreturn dataout>
	</cffunction>


	<cffunction name="UpdateCPP" access="remote" output="true" hint="update CPP">
		<cfargument name="ccpxXMLDataString" type="string" required="yes">
		<cfargument name="checkMultiSubList" type="string" required="yes">
		<cfargument name="inpRawContent" type="string" required="yes">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var updateCPP = ''>
		<cfset var result = ''>
		<cfset var xmlOldData = ''>

		<cfset var XMLDataString	= '' />
		<cfset var ccpxDataId	= '' />
		<cfset var structTermOfService	= '' />
		<cfset var i	= '' />
		<cfset var cppData	= '' />
		<cfset var xmlDataControl	= '' />


		<cfset var XMLDataString = deserializeJSON(ccpxXMLDataString)>

		<cfset var ccpxDataId = XMLDataString[1].ID>

		<!--- REMOVE ID the 1st item in list --->
		<cfset ArrayDeleteAt(XMLDataString,1)>
		
		<!---<cftry>--->

			<!--- GET CURRENT DATA --->
			<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="cppData">
				<cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
			</cfinvoke>

			<cfif cppData.RXRESULTCODE NEQ 1>
				<cfset dataout.MESSAGE = "This MLP not found." />
				<cfset dataout.ERRMESSAGE = "This MLP not found." />
				<cfreturn dataout>
			</cfif>


	   		<!--- READ OLD DATA TO GET TERM OF SERVICE 
	   		<cfinvoke component="session.sire.models.cfc.mlp" method="ParseCPPXXmlData" returnvariable="xmlOldData">
	   		 	<cfinvokeargument name="xmlData" value="#cppData.DATA#">
	   		</cfinvoke>

	   		<cfset var termOfService = ''>
	   		<cfif isArray(xmlOldData) && arrayLen(xmlOldData) GT 0>
	   			<cfloop array="#xmlOldData#" index="i">
	   				<cfif structKeyExists(i,"term-of-service")>
	   					<cfset termOfService = i['term-of-service']>
	   					<cfbreak>
	   				</cfif>
	   			</cfloop>
	   		</cfif>
	   		
	   		<cfif len(trim(termOfService)) GT 0>
	   			<cfset var structTermOfService = {'TYPE':'term-of-service','VALUE':'termOfService'}>
	   			<cfset var arrayPrepend(XMLDataString, structTermOfService)>
	   		</cfif>
	   	--->


	   	<cfinvoke component="session.sire.models.cfc.mlp" method="BuildCPPXXmlData" returnvariable="xmlDataControl">
	   		<cfinvokeargument name="xmlData" value="#XMLDataString#">
	   		<cfinvokeargument name="checkMultiSubList" value="#arguments.checkMultiSubList#">
	   	</cfinvoke>

	   	<cfif cppxName NEQ '' AND cppxURL NEQ ''>
	   		<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
	   			UPDATE 
	   			simplelists.cppx_data
	   			SET 
	   			XMLDataString  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">,
	   			css_custom = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssCustom#">,
	   			css_external_link = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssExternalLink#">,
	   			css_type = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssType#">,
	   			cppxName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppxName#">,
	   			cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppxURL#">,
	   			RawContent_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRawContent#">
	   			WHERE  
	   			ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
	   			AND        
	   			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	   		</cfquery>		
	   	<cfelse>
	   		<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
	   			UPDATE 
	   			simplelists.cppx_data
	   			SET 
	   			XMLDataString  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">,
	   			css_custom = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssCustom#">,
	   			css_external_link = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssExternalLink#">,
	   			css_type = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssType#">,
	   			RawContent_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpRawContent#">
	   			WHERE  
	   			ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
	   			AND        
	   			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	   		</cfquery>		
	   	</cfif>


	   	<cfif result.RecordCount GT 0>
	   		<cfset dataout.MESSAGE = 'Update MLP successfully.'>
	   		<cfset dataout.CPPID = ccpxDataId />
	   		<cfset dataout.RXRESULTCODE = 1 />
	   	<cfelse>
	   		<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
	   	</cfif>	


		   <!--- <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Update CPP fail.Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
		</cftry> ---> 


		<cfreturn dataout>
	</cffunction>

	<cffunction name="checkCPPNameAvailability" access="remote" output="false" hint="check unique MLP name">
		<cfargument name="fieldValue" TYPE="string" hint="Name to lookup if it is in use yet." />
		<cfargument name="fieldId" TYPE="string" />
		<cfargument name="ccpxDataId" type="numeric" default="0"/>

		<cfset var readCPP	= '' />

		<cftry>
			<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
				SELECT 
				XMLDataString
				FROM	
				simplelists.cppx_data AS c
				WHERE  
				c.cppxName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldValue#">
				AND        
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">    
				AND 
				c.status_int >= 0
				<cfif ccpxDataId GT 0>
					AND c.ccpxDataId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
				</cfif>
				LIMIT 1
			</cfquery>

			<cfif readCPP.RecordCount GT 0>
				<cfreturn [arguments.fieldId, false]>
			<cfelse>
				<cfreturn [arguments.fieldId, true]>
			</cfif>	

			<cfcatch>
				<cfreturn [arguments.fieldId, true]>
			</cfcatch>    	
		</cftry>    	

	</cffunction>

	<cffunction name="checkCPPUrlAvailability" access="remote" output="false" hint="check unique MLP url">
		<cfargument name="fieldValue" TYPE="string" hint="Url to lookup if it is in use yet." />
		<cfargument name="fieldId" TYPE="string" />
		<cfargument name="ccpxDataId" type="numeric" default="0"/>

		<cfset var readCPP	= '' />

		<cftry>
			<cfif fieldValue NEQ "">
				<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
					SELECT 
					XMLDataString
					FROM	
					simplelists.cppx_data AS c
					WHERE  
					c.cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldValue#">
					AND 
					c.status_int >= 0
					<cfif ccpxDataId GT 0>
						AND c.ccpxDataId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
					</cfif>    
					LIMIT 1
				</cfquery>

				<cfif readCPP.RecordCount GT 0>
					<cfreturn [arguments.fieldId, false]>
				<cfelse>
					<cfreturn [arguments.fieldId, true]>
				</cfif>	
			<cfelse>
				<cfreturn [arguments.fieldId, true]>
			</cfif>

			<cfcatch>
				<cfreturn [arguments.fieldId, true]>
			</cfcatch>    	
		</cftry>    	

	</cffunction>

	<cffunction name="checkMLPUrlAvailability" access="remote" output="false" hint="check unique MLP url - jquery validation with ajax is broken or crap - check directly">
		<cfargument name="inpURL" TYPE="string" hint="Url to lookup if it is in use yet." />
		<cfargument name="MLPId" type="numeric" default="0"/>

		<cfset var readCPP	= '' />
		<cfset var CheckDuplicatedShortKey = '' />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />	   	

		<cftry>
			<cfif inpURL NEQ "">
				<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
					SELECT 
					c.status_int
					FROM	
					simplelists.cppx_data AS c
					WHERE  
					c.cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpURL#">
					AND 
					c.status_int >= 0
					<cfif MLPId GT 0>
						AND c.ccpxDataId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MLPId#">
					</cfif>    
					LIMIT 1
				</cfquery>

				<cfif readCPP.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = -3 />
					<cfset dataout.MESSAGE = "URL is Already in use">
				<cfelse>
					
					<!--- Merge check short URL also --->
					<!--- Verify this short key is unique --->
					<cfquery name="CheckDuplicatedShortKey" datasource="#Session.DBSourceEBM#">
						SELECT
						ShortURL_vch
						FROM 
						simplelists.url_shortner
						WHERE
						ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpURL#">
						AND 
						Active_int = 1 
					</cfquery>

					<cfif CheckDuplicatedShortKey.RECORDCOUNT GT 0>
						<cfset dataout.RXRESULTCODE = -3 />
						<cfset dataout.MESSAGE = "URL is Already in use">		            
					<cfelse>		            
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = "URL Available for Use">  	                
					</cfif>

				</cfif>	
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'URL can not be blank'>
			</cfif>

			<cfcatch>

				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Error while looking up URL'>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>

			</cfcatch>    	
		</cftry>    	

		<cfreturn dataout>

	</cffunction>


	<cffunction name="updateCPPStatus" access="remote" output="false" hint="update MLP status">
		<cfargument name="inpCppId" TYPE="string" required="true" />
		<cfargument name="inpStatus" TYPE="string" required="true" />
		<cfargument name="inpAction" TYPE="string" required="true" />

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset var updateCPP = '' />
		<cfset var result = '' />
		<cfset var status =	0>	
		<cfset var successMessage =	''>	
		<cfset var errorsMessage =	''>	
		<cfset var result	= '' />

		<cfif arguments.inpAction EQ 'changeStatus'>
			<cfset status = (arguments.inpStatus EQ 0 ? 1 : 0)>
			<cfset successMessage =	'Update MLP successfully.'>
			<cfset errorsMessage =	'Update MLP fail.Please try again later'>

		<cfelseif arguments.inpAction EQ 'delete'>
			<cfset status =	-1>	
			<cfset successMessage =	'Remove MLP successfully.'>
			<cfset errorsMessage =	'Remove MLP fail.Please try again later'>
		</cfif>

		<cfset var updateCPP = ''>

		<cftry>

			<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
				UPDATE 
				simplelists.cppx_data
				SET 
				status_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#status#">
				WHERE  
				ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCppId#">
				AND        
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			</cfquery>

			<cfif result.RecordCount GT 0>
				<cfset dataout.MESSAGE = successMessage />
				<cfset dataout.CPPID = inpCppId />
				<cfset dataout.RXRESULTCODE = 1 />

				<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="MLPID = #arguments.inpCppId#">
					<cfinvokeargument name="operator" value="MLP status updated. Status = #arguments.inpStatus#. Action = #arguments.inpAction#">
				</cfinvoke>
			<cfelse>
				<cfset dataout.MESSAGE = errorsMessage>
			</cfif>

			<cfcatch>	
				<cfset dataout.MESSAGE = errorsMessage>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>    	
		</cftry>         
		<cfreturn dataout>
	</cffunction>

	<cffunction name="getCPPList" access="remote" output="false" hint="get list MLP">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">

		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetNumbersCount = ''>
		<cfset var order = ''>
		<cfset var actionHtml = ''/>
		<cfset var DisplaySection = ''/>
		<cfset var cppStatusTxt	= '' />
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetEMS	= '' />
		<cfset var allowedMlpAdd = false />
		<cfset var mlpCount = 1 />

		<!--- <cftry>--->
			<cfinvoke method="BuildSortingParamsForDatatable" returnvariable="order">
				<cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
				<cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
				<cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
				<cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
			</cfinvoke>
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			<cfset dataout.LOCKBUTTON = false />


			<!--- Check Add MLP for current user to set Locked button state --->
			<cfinvoke method="CheckAddPerm" returnvariable="allowedMlpAdd"></cfinvoke>
			<cfif !allowedMlpAdd >
				<cfset dataout.LOCKBUTTON = true />
			</cfif> 


			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
				COUNT(*) AS TOTALCOUNT
				FROM
				simplelists.cppx_data AS c
				WHERE        
				c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND 
					-- c.status_int <> -1
					c.status_int = 1	
				AND 
				c.MlpType_ti = 1
				<cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
			</cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>

				<!--- Get ems data --->		
				<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
					SELECT 
					c.ccpxDataId_int,
					c.cppxUUID_vch,
					c.cppxName_vch,
					c.cppxURL_vch,
					c.created_dt,
					c.status_int
					FROM
					simplelists.cppx_data c
					WHERE 
					-- c.status_int <> -1	   
					c.status_int = 1	
					AND
					c.MlpType_ti = 1
					AND 
					c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
					AND
						c.MlpType_ti=1
					<cfif customFilter NEQ "">
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
					#order#
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfquery>





				<!--- Use cfcontent to gennerate readable and easier to edit HTML for returning in datatables --->
				<cfsavecontent variable="DisplaySection">
						<!--- <cfoutput>
							<div>								
								<div><b>Description:</b></div>								
								<p>#GetEMS.cppxName_vch#</p>
							</div>							
							
							<div class="dont-break-out">
								<div><b>Public URL:</b></div>	
								<p>								
				        			<a class="" href="https://mlp-x.com/lz/#GetEMS.cppxUUID_vch#" target="_blank">https://mlp-x.com/lz/#GetEMS.cppxUUID_vch#</a>
								</p>
							</div>
																												
							<div class="">
								<div><b>Preview:</b></div>	
			                	<div class="Frame-Wrapper"><iframe class="scaled-frame" src="/mlp-x/lz/#GetEMS.cppxUUID_vch#" scrolling="auto"></iframe></div>
				            </div>   	            
		            
							<!--- (status_int EQ 0 ? 'Off' : 'On'), --->						
						</cfoutput> --->
						<cfoutput>
							<div class="row" style="margin-left: -25px; margin-right: -25px;">



								<cfloop query="GetEMS">
									<cfset cppStatusTxt = (status_int EQ 0 ? 'TURN ON' : 'TURN OFF')>

									<div class="col-xs-12 col-sm-6 col-md-4" style="height: 480px; overflow-y: auto;">


								<!--- 
									.mlp-list-count {
    background: rgb(222,222,222);
    height: 60px;
    margin-left: -6px;
    margin-right: -6px;
    margin-top: -16px !important;
    display: -webkit-flex;
    -webkit-align-items: center;
    display: flex;
    align-items: center;
}

<div class="col-xs-7" style="line-height: 100%; vertical-align: middle;">
											<p class=""><b class="mlp-numb">MLP</b></p>
										</div>
	
										<div class="col-xs-5">
		<!---
											<div class="text-right">
		<!---
			                                	<a class="btn-re-xs btn-change-cpp-status btn-report btn-actions" data-cpp-action="changeStatus" data-cpp-id="#ccpxDataId_int#" data-cpp-status="#status_int#">#cppStatusTxt#</a>
			                                	<div class="btn-re-xs btn-embed-cpp btn-report btn-actions" data-cpp-url="#urlEncodedFormat(GetEMS.cppxURL_vch)#" data-cpp-id="#GetEMS.ccpxDataId_int#" data-cpp-uuid="#GetEMS.cppxUUID_vch#">EMBED CODE</div>
		--->
			                                </div>
		--->
			                            	<div class="text-right">
			                            		<a class="btn-edit btn-re-edit mr-15" href="/session/sire/pages/mlpx-edit?ccpxDataId=#ccpxDataId_int#&action=Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			                            		<a class="btn-delete-cpp btn-re-delete" data-cpp-action="delete" data-cpp-name="#cppxName_vch#" data-cpp-status="#status_int#" data-cpp-id="#ccpxDataId_int#"><i class="fa fa-times" aria-hidden="true"></i></a>
			                            	</div>
										</div>
									
									--->

									<div class="row" style="background: rgb(222,222,222); margin: 0em; border-radius: .5em .5em 0 0 ">
										<!--- 										<p class="mlp-list-count"><b class="mlp-numb">MLP#mlpCount#</b></p>	 --->


										<div class="col-xs-6" style="text-align: left;">
											<b class="" style="font-size: 18px;">MLP</b>
										</div>

													<!---
												<div class="text-right">
			<!---
				                                	<a class="btn-re-xs btn-change-cpp-status btn-report btn-actions" data-cpp-action="changeStatus" data-cpp-id="#ccpxDataId_int#" data-cpp-status="#status_int#">#cppStatusTxt#</a>
				                                	<div class="btn-re-xs btn-embed-cpp btn-report btn-actions" data-cpp-url="#urlEncodedFormat(GetEMS.cppxURL_vch)#" data-cpp-id="#GetEMS.ccpxDataId_int#" data-cpp-uuid="#GetEMS.cppxUUID_vch#">EMBED CODE</div>
			--->
				                                </div>
				                            --->
				                            <div class="col-xs-6" style="text-align: right; line-height: 25px;">
				                            	<a class="btn-edit xbtn-re-edit" href="/session/sire/pages/mlpx-edit?ccpxDataId=#ccpxDataId_int#&action=Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
				                            	<a class="btn-delete-cpp xbtn-re-delete" data-cpp-action="delete" data-cpp-name="#cppxName_vch#" data-cpp-status="#status_int#" data-cpp-id="#ccpxDataId_int#"><i class="fa fa-times" aria-hidden="true" style="position:relative; top: -1px;"></i></a>
				                            </div>


				                        </div>


			                            <!--- <div class="mlp-desc pull-left" style="width: 65%">
			                            	<h3>Description:</h3>
			                            	<p>#GetEMS.cppxName_vch#</p>
			                            </div> --->
			                            <!--- <div class="row">
				                            <div class="mlp-action pull-right" style="padding-right:10px;width:35%;">
				                                <div class="text-right">
				                                	<a class="btn-re-xs btn-change-cpp-status btn-report btn-actions" data-cpp-action="changeStatus" data-cpp-id="#ccpxDataId_int#" data-cpp-status="#status_int#">#cppStatusTxt#</a>
				                                	<div class="btn-re-xs btn-embed-cpp btn-report btn-actions" data-cpp-url="#urlEncodedFormat(GetEMS.cppxURL_vch)#" data-cpp-id="#GetEMS.ccpxDataId_int#" data-cpp-uuid="#GetEMS.cppxUUID_vch#">EMBED CODE</div>
				                                </div>
				                            	<div class="text-right">
				                            		<a class="btn-edit btn-re-edit" href="/session/sire/pages/mlp-edit?ccpxDataId=#ccpxDataId_int#&action=Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
				                            		<a class="btn-delete-cpp btn-re-delete" data-cpp-action="delete" data-cpp-name="#cppxName_vch#" data-cpp-status="#status_int#" data-cpp-id="#ccpxDataId_int#"><i class="fa fa-times" aria-hidden="true"></i></a>
				                            	</div>
				                            </div>
		
		 								</div> 
		 								
		 								style="box-shadow: 0 0px 10px ##777;"
		 								
		 							--->

		 							<div class="row" >                   

		 								<div class="col-md-12" style="padding-top: 1em;">
		 									<h3>Description:</h3>
		 									<p>#GetEMS.cppxName_vch#</p>
		 								</div>

		 								<div class="col-md-12">
		 									<div class="mlp-pub-url">
		 										<h3>Public URL:</h3>
		 										<p><a class="" href="https://mlp-x.com/lz/#GetEMS.cppxUUID_vch#" target="_blank">https://mlp-x.com/lz/#GetEMS.cppxUUID_vch#</a></p>		                            	
		 										<p><img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-url="https://mlp-x.com/lz/#GetEMS.cppxUUID_vch#" alt="Google"></p>

		 									</div>
		 								</div>

		 							</div>                            	                       



		 						</div>


		 						<cfset mlpCount = mlpCount + 1 />

		 					</cfloop>

		 				</div>
		 			</cfoutput>
		 		</cfsavecontent>

<!---
					<cfsavecontent variable="actionHtml">
						<cfoutput>
							<!--- <a class="btn btn-success-custom btn-edit btn-report btn-actions" href="/session/sire/pages/mlp-edit?ccpxDataId=#ccpxDataId_int#&action=Edit">Edit</a>
							<a class="btn btn-success-custom btn-change-cpp-status btn-report btn-actions" data-cpp-action="changeStatus" data-cpp-id="#ccpxDataId_int#" data-cpp-status="#status_int#">#cppStatusTxt#</a>
							<button class="btn btn-success-custom btn-embed-cpp btn-report btn-actions" data-cpp-url="#urlEncodedFormat(GetEMS.cppxURL_vch)#" data-cpp-id="#GetEMS.ccpxDataId_int#" data-cpp-uuid="#GetEMS.cppxUUID_vch#">Embed Code</button>
	        				<button class="btn btn-back-custom btn-delete-cpp btn-report btn-actions" data-cpp-action="delete" data-cpp-name="#cppxName_vch#" data-cpp-status="#status_int#" data-cpp-id="#ccpxDataId_int#">Delete</button> --->
	        				<div class="row" style="margin-left: -25px; margin-right: -25px;">
	        					<div class="col-xs-12">
	        						<div class="mlp-action">
<!---
		                                <div class="text-right">
		                                	<a class="btn-re-xs btn-change-cpp-status btn-report btn-actions" data-cpp-action="changeStatus" data-cpp-id="#ccpxDataId_int#" data-cpp-status="#status_int#">#cppStatusTxt#</a>
		                                	<div class="btn-re-xs btn-embed-cpp btn-report btn-actions" data-cpp-url="#urlEncodedFormat(GetEMS.cppxURL_vch)#" data-cpp-id="#GetEMS.ccpxDataId_int#" data-cpp-uuid="#GetEMS.cppxUUID_vch#">Embed Code</div>
		                                </div>
--->
		                            	<div class="text-right">
		                            		<a class="btn-edit btn-re-dowload" href="/session/sire/pages/mlpx-edit?ccpxDataId=#ccpxDataId_int#&action=Edit"></a>
		                            		<a class="btn-delete-cpp btn-re-delete" data-cpp-action="delete" data-cpp-name="#cppxName_vch#" data-cpp-status="#status_int#" data-cpp-id="#ccpxDataId_int#"></a>
		                            	</div>
		                            </div>
	        					</div>
	        				</div>
						</cfoutput>
					</cfsavecontent>
				--->					

				<cfset tempItem = [
				DisplaySection,
				actionHtml
				]>
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>



				
			</cfif>
	    <!---    
        <cfcatch type="Any">
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
    --->
    <cfreturn serializeJSON(dataOut)>
</cffunction>

<cffunction name="BuildSortingParamsForDatatable" access="public" hint="Build Sorting Params For Datatable">
	<cfargument name="iSortCol_0" default="-1">
	<cfargument name="iSortCol_1" default="-1">
		<!---these are the index of colums that need to be sorted, their value could be  
			1 is Desc, 
			2 is ClickCount_int, 
		--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfset var dataout= "">
		<cftry>
			<!---set order param for query--->

			<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
				<cfset var order_0 = sSortDir_0>
			<cfelse>
				<cfset order_0 = "">	
			</cfif>

			<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
				<cfset var order_1 = sSortDir_1>
			<cfelse>
				<cfset order_1 = "">	
			</cfif>
			<cfif iSortCol_0 neq -1 and order_0 NEQ "">
				<cfset dataout = dataout &" order by ">
				<cfif iSortCol_0 EQ 0> 
					<cfset dataout = dataout &" c.cppxName_vch ">
				<cfelseif iSortCol_0 EQ 1>
					<cfset dataout = dataout &" c.ccpxDataId_int ">
				<cfelse>
					<cfset dataout = dataout &" c.ccpxDataId_int ">	
				</cfif>
				<cfset dataout = dataout &"#order_0#">
			</cfif>
			<cfif iSortCol_1 neq -1 and order_1 NEQ "">
				<cfset dataout = dataout &" , ">
				<cfif iSortCol_1 EQ 0> 
					<cfset dataout = dataout &" c.cppxName_vch ">	
					<cfset dataout = dataout &"desc">
				<cfelseif iSortCol_1 EQ 1>
					<cfset dataout = dataout &" c.ccpxDataId_int ">
				<cfelse>
					<cfset dataout = dataout & " c.ccpxDataId_int ">
					<cfset dataout = dataout & "#order_0#">						
				</cfif>
			</cfif>      
			<cfcatch type="Any" >
				<cfset dataout = "">
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>


	<cffunction name="getCPPListTemplate" access="remote" output="false" hint="get list MLP template">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">

		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetNumbersCount = ''>
		<cfset var order = ''>
		<cfset var cppStatusTxt	= '' />
		<cfset var changeStatus	= '' />
		<cfset var actionHtml	= '' />
		<cfset var tempItem	= '' />
		<cfset var GetEMS	= '' />

		<!---<cftry>--->

			<cfinvoke method="BuildSortingParamsForDatatable" returnvariable="order">
				<cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
				<cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
				<cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
				<cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
			</cfinvoke>
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
				COUNT(*) AS TOTALCOUNT
				FROM
				simplelists.cppx_template AS c
				
				ORDER BY tempalteId_int DESC
			</cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>

				<!--- Get ems data --->		
				<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
					SELECT 
					c.tempalteId_int,
					c.title_vch,
					c.description_vch,
					c.status_int
					FROM
					simplelists.cppx_template c
					ORDER BY tempalteId_int DESC					
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfquery>

				<!---<cfdump var="#GetEMS#" >--->
				<cfloop query="GetEMS">
					<cfset cppStatusTxt = (status_int EQ 0 ? 'Enable' : 'Disable')>
					<cfset changeStatus = (status_int EQ 0 ? 1 : 0)>
					<cfset actionHtml  = '<a class="btn btn-success-custom btn-edit btn-report" href="/session/sire/pages/mlpx-edit?ccpxDataId='&tempalteId_int&'&action=template">Manage</a>'>
					<cfset actionHtml  &= '<a class="btn btn-success-custom btn-change-cpp-status btn-report" data-cpp-action="changeStatus" data-cpp-id="'&tempalteId_int&'" data-cpp-status="'&changeStatus&'">'&cppStatusTxt&'</a>'>
					<cfset actionHtml  &= '<a class="btn btn-success-custom btn-preview-cpp btn-report" href="/session/sire/pages/admin-mlp-template-preview-page?ccpxDataId='&tempalteId_int&'" target="_blank">Preview</a>'>
					<cfset actionHtml  &= '<button class="btn btn-back-custom btn-delete-cpp btn-report" data-cpp-action="delete"  data-cpp-id="'&tempalteId_int&'"  data-cpp-status="'&status_int&'">Delete</button>'> 

					<cfset tempItem = [
					'#tempalteId_int#',
					'#title_vch#',
					(status_int EQ 0 ? 'Disable' : 'Enable'),
					actionHtml
					]>
					<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
				</cfloop>
			</cfif>

        <!---<cfcatch type="Any">
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
    </cftry>--->

    <cfreturn serializeJSON(dataOut)>
</cffunction>

<cffunction name="UpdateCPPTemplate" access="remote" output="true" hint="update MLP">
	<cfargument name="ccpxXMLDataString" type="string" required="yes">
	<cfargument name="templateName" type="string" required="yes">
	<cfargument name="Description" type="string" required="yes">

	<cfset var dataout = {}>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.CPPID = "0" />
	<cfset dataout.MESSAGE = "" />
	<cfset dataout.ERRMESSAGE = "" />

	<cfset var updateCPP = ''>
	<cfset var result = ''>
	<cfset var xmlOldData = ''>
	<cfset var XMLDataString	= '' />
	<cfset var ccpxDataId	= '' />
	<cfset var insertCPPTemplate	= '' />
	<cfset var cppData	= '' />
	<cfset var xmlDataControl	= '' />
	<cfset var insertResult	= '' />

	<cfset var XMLDataString = deserializeJSON(ccpxXMLDataString)>
	<cfset var ccpxDataId = XMLDataString[1].ID>
	<cfset var insertCPPTemplate = '' />
	<cfset var xmlDataControl = '' />
	<cfset var insertResult = '' />
	<cfset var cppData = '' />
	<!--- REMOVE ID the 1st item in list --->
	<cfset ArrayDeleteAt(XMLDataString,1)>

	<!---<cftry>--->

		<!--- GET CURRENT DATA --->
		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPPTemplate" returnvariable="cppData">
			<cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
		</cfinvoke>

		<cfinvoke component="session.sire.models.cfc.mlp" method="BuildCPPXXmlData" returnvariable="xmlDataControl">
			<cfinvokeargument name="xmlData" value="#XMLDataString#">
			<cfinvokeargument name="checkMultiSubList" value="#arguments.checkMultiSubList#">
		</cfinvoke>

		<cfif cppData.RXRESULTCODE NEQ 1>
			<cfquery name="insertCPPTemplate" datasource="#Session.DBSourceEBM#" result="insertResult" >
				INSERT INTO `simplelists`.`cppx_template`
				(
					`title_vch`,
					`description_vch`,
					`image_vch`,
					`XML_txt`
					)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#templateName#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Description#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">
					);

			</cfquery>

			<cfif insertResult.GENERATED_KEY GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Create MLP Template successfully.'>
				<cfset dataout.CPPID = insertResult.GENERATED_KEY />
			<cfelse>
				<cfset dataout.MESSAGE = 'Update fail. Please try again later'>	
			</cfif>

			<cfreturn dataout>
		</cfif>



		<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
			UPDATE 
			simplelists.cppx_template
			SET 
			title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#templateName#">,
			description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Description#">,
			XML_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">
			WHERE  
			tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
		</cfquery>

		<cfif result.RecordCount GT 0>
			<cfset dataout.MESSAGE = 'Update MLP successfully.'>
			<cfset dataout.CPPID = ccpxDataId />
			<cfset dataout.RXRESULTCODE = 1 />
		<cfelse>
			<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
		</cfif>	


		   <!--- <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Update CPP fail.Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
		</cftry> ---> 


		<cfreturn dataout>
	</cffunction>

	<cffunction name="deleteCPPTempate" access="remote" output="false" hint="delete MLP template">
		<cfargument name="inpCppId" TYPE="string" required="true" />


		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var status =	0>	
		<cfset var successMessage =	''>	
		<cfset var errorsMessage =	''>	
		<cfset var deleteCPPTempate	= '' />
		<cfset var result	= '' />

		<cfset var deleteCPPTempate = '' />
		<cfset var result = '' />

		<cfset status =	-1>	
		<cfset successMessage =	'Remove MLP Template successfully.'>
		<cfset errorsMessage =	'Remove MLP Template fail.Please try again later'>

		<cfset var updateCPP = ''>

		<cftry>
			<cfquery name="deleteCPPTempate" datasource="#Session.DBSourceEBM#" result="result" >
				DELETE FROM
				simplelists.cppx_template
				WHERE  
				tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCppId#">

			</cfquery>

			<cfif result.RecordCount GT 0>
				<cfset dataout.MESSAGE = successMessage />
				<cfset dataout.CPPID = inpCppId />
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = errorsMessage>
			</cfif>	
			<cfcatch>	
				<cfset dataout.MESSAGE = errorsMessage>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>    	
		</cftry>         
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="ReadCPPTemplate" access="public" output="true" hint="Read MLP">
		<cfargument name="ccpxDataId" type="string" required="yes">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TEMPALTEID = "0" />
		<cfset dataout.TITLE = "" />
		<cfset dataout.DESCRIPTION = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.DATA = "" />

		<cfset var readCPP = ''>
		<cftry>
			<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
				SELECT 
				tempalteId_int,title_vch,description_vch,image_vch,XML_txt
				FROM	
				simplelists.cppx_template
				WHERE  
				tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
				LIMIT 1

			</cfquery>

			<cfif readCPP.RecordCount GT 0>
				<cfloop query="#readCPP#">
					<cfset dataout.DATA = XML_txt>
					<cfset dataout.TITLE = title_vch>
					<cfset dataout.TEMPALTEID = tempalteId_int />
					<cfset dataout.DESCRIPTION = description_vch />
				</cfloop>	
				<cfset dataout.RXRESULTCODE = 1 />

			<cfelse>
				<cfset dataout.MESSAGE = 'Can not get this MLP Template data'>
			</cfif>	

			<cfcatch>
				<cfset dataout.MESSAGE = 'Can not get this MLP Template data'>
				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
			</cfcatch>    	
		</cftry>    	

		<cfreturn dataout>
	</cffunction>
	
	
	<cffunction name="getCPPTemplateList" access="public" output="true" hint="get list CPPTemplate">
		<cfset var dataout	= {} />
		<cfset var template_item	= '' />
		<cfset var listTemplate	= '' />

		<cfset dataout.RXRESULTCODE = -1>


		<cftry>
			<!--- CHECK SMS First --->
			<cfquery name = "listTemplate" datasource="#Session.DBSourceREAD#">
				SELECT 
				tempalteId_int,
				title_vch,
				description_vch,
				image_vch,
				Order_int
				FROM 
				simplelists.cppx_template
				WHERE status_int = 1
				ORDER BY 
				order_int, tempalteId_int ASC
			</cfquery>
			
			<cfif listTemplate.RECORDCOUNT GT 0>
				<cfset dataout.DATA ={}>
				<cfset var i = 0  />

				<cfloop query="#listTemplate#">
					
					<cfset i = i + 1 />
					
					<cfset template_item=structNew()> 
					<cfset template_item['ID'] = tempalteId_int>
					<cfset template_item['NAME'] = title_vch>
					<cfset template_item['DESCRIPTION'] = title_vch>
					<cfset template_item['IMAGE'] = Image_vch>
					<cfset template_item['ORDER'] = Order_int>

					<!--- Bad code here!  --->
					<!--- <cfset dataout.DATA[tempalteId_int] =template_item>	 --->
					<cfset dataout.DATA[i] =template_item>	
				</cfloop>

				<cfset dataout.RXRESULTCODE = 1>
				<cfreturn dataout />
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>	
		<cfreturn dataout />		
	</cffunction>
	
	
	<cffunction name="AddCPPFromTemplate" access="remote" output="true" hint="Add MLP From Template">
		<cfargument name="cppxName" type="string" required="yes">
		<cfargument name="cppxURL" type="string" required="yes">
		<cfargument name="ccpxXMLDataString" type="string" required="yes">
		<cfargument name="checkMultiSubList" type="string" required="yes">


		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var updateCPP = ''>
		<cfset var result = ''>
		<cfset var xmlOldData = ''>
		<cfset var XMLDataString	= '' />
		<cfset var ccpxDataId	= '' />
		<cfset var insertCPP	= '' />
		<cfset var xmlDataControl	= '' />
		<cfset var insertResult	= '' />


		<cfset var CreateUUID = ''/>
		<cfset var URLName = ''/>

		<cfset CreateUUID = CreateUUID()/>

		<cfif arguments.cppxURL NEQ ''>
			<cfset URLName = arguments.cppxURL/>
		<cfelse>
			<cfset URLName = CreateUUID/>
		</cfif>

		<cfset var XMLDataString = deserializeJSON(ccpxXMLDataString)>
		<cfset var ccpxDataId = XMLDataString[1].ID>
		<cfset var insertCPP = '' />
		<cfset var xmlDataControl = '' />
		<cfset var insertResult = '' />
		<cfset var allowedMlpAdd = false />
		<!--- REMOVE ID the 1st item in list --->
		<cfset ArrayDeleteAt(XMLDataString,1)>
		
		<cftry>


			
			<cfinvoke component="session.sire.models.cfc.mlp" method="BuildCPPXXmlData" returnvariable="xmlDataControl">
				<cfinvokeargument name="xmlData" value="#XMLDataString#">
				<cfinvokeargument name="checkMultiSubList" value="#arguments.checkMultiSubList#">
			</cfinvoke>

		   	<!---<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
	            UPDATE 
	                simplelists.cppx_data
	            SET 
	            	XMLDataString  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">,
	            	css_custom = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssCustom#">,
	            	css_external_link = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssExternalLink#">,
	            	css_type = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssType#">
	            WHERE  
	                ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
		        AND        
		            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        </cfquery>

	        <cfif result.RecordCount GT 0>
	        	<cfset dataout.MESSAGE = 'Update MLP successfully.'>
	        	<cfset dataout.CPPID = ccpxDataId />
	        	<cfset dataout.RXRESULTCODE = 1 />
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
	        </cfif>	--->
	        <!--- Check Add mlp condition of current user true then insert new mlp to database --->
	        <cfinvoke method="CheckAddPerm"	 returnvariable="allowedMlpAdd"></cfinvoke>      
	        <cfif allowedMlpAdd>	
	        	<cfquery name="insertCPP" datasource="#Session.DBSourceEBM#" result="insertResult">
	        		INSERT INTO
	        		simplelists.cppx_data(
	        			cppxUUID_vch,
	        			XMLDataString,
	        			cppxName_vch,
	        			cppxURL_vch,
	        			created_dt,
	        			updated_dt,
	        			UserId_int,
	        			css_custom,
	        			css_external_link,
	        			css_type
	        			)
	        		VALUES 
	        		(
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CreateUUID#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlDataControl#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.cppxName#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#URLName#">,
	        			NOW(),
	        			NOW(),
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssCustom#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssExternalLink#">,
	        			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cssType#">
	        			)   
	        	</cfquery>
	        </cfif>
	        <cfif allowedMlpAdd AND structKeyExists(insertResult, 'GENERATED_KEY') AND insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Create MLP successfully.'>
	        	<cfset dataout.CPPID = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Update fail. Please try again later'>	
	        </cfif>


	        <cfcatch>	
	        	<cfset dataout.MESSAGE = 'Update MLP fail. Please try again later'>
	        	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
	        </cfcatch>   	
	    </cftry>


	    <cfreturn dataout>
	</cffunction>


	<cffunction name="getAdminCPPList" access="remote" output="false" hint="get list admin MLP">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">

		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetNumbersCount = ''>
		<cfset var order = ''>
		<cfset var actionHtml	= '' />
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetEMS	= '' />        

		<!--- <cftry>--->
			<cfinvoke method="BuildSortingParamsForDatatable" returnvariable="order">
				<cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
				<cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
				<cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
				<cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
			</cfinvoke>
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
				COUNT(*) AS TOTALCOUNT
				FROM
				simplelists.cppx_data AS c
				WHERE        
				c.status_int >= 0	
				<cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
			</cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>

				<!--- Get ems data --->		
				<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
					SELECT 
					c.ccpxDataId_int,
					c.UserId_int,
					c.cppxUUID_vch,
					c.cppxName_vch,
					c.cppxURL_vch,
					c.created_dt,
					c.status_int
					FROM
					simplelists.cppx_data c
					WHERE 
					c.status_int >= 0	   

					<cfif customFilter NEQ "">
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
					#order#
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfquery>


				<cfloop query="GetEMS">
					<cfset actionHtml  = '<a class="btn btn-success-custom btn-edit btn-report" href="/session/sire/pages/mlpx-edit?ccpxDataIdSource='&ccpxDataId_int&'&action=template">Create MLP From Template</a>'>
					<cfset actionHtml  &= '<a class="btn btn-success-custom btn-preview btn-report" target="_blank" href="/session/sire/pages/mlp-preview?inpMLPXURL='&cppxURL_vch&'&preview=1" ">Preview</a>'>
					<cfset tempItem = [
					'#cppxUUID_vch#',
					'#cppxName_vch#',
					'#cppxURL_vch#',
					actionHtml
					]>
					<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
				</cfloop>
			</cfif>
	    <!---    
        <cfcatch type="Any">
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
    --->
    <cfreturn serializeJSON(dataOut)>
</cffunction>

<cffunction name="AdminReadCPP" access="public" output="true" hint="Read MLP">
	<cfargument name="ccpxDataId" type="string" required="yes">


	<cfset var dataout = {}>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.TEMPALTEID = "0" />
	<cfset dataout.TITLE = "" />
	<cfset dataout.DESCRIPTION = "" />
	<cfset dataout.MESSAGE = "" />
	<cfset dataout.ERRMESSAGE = "" />
	<cfset dataout.DATA = "" />


	<cfset var readCPP = ''>
	<cftry>
		<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
			SELECT 
			XMLDataString,cppxUUID_vch,cppxName_vch,cppxURL_vch, css_custom, css_external_link,css_type
			FROM	
			simplelists.cppx_data
			WHERE  
			ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
			AND    
			status_int >= 0    
			LIMIT 1

		</cfquery>

		<cfif readCPP.RecordCount GT 0>
			<cfloop query="#readCPP#">
				<cfset dataout.DATA = XMLDataString>
			</cfloop>	
			<cfset dataout.RXRESULTCODE = 1 />

		<cfelse>
			<cfset dataout.MESSAGE = 'Can not get this MLP data'>
		</cfif>	

		<cfcatch>
			<cfset dataout.MESSAGE = 'Can not get this MLP data'>
			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		</cfcatch>    	
	</cftry>    	

	<cfreturn dataout>
</cffunction>


<cffunction name="updateStatusCPPTempate" access="remote" output="true" hint="update status MLP template">
	<cfargument name="inpCppId" TYPE="string" required="true" />
	<cfargument name="inpStatus" TYPE="string" required="true" />

	<cfset var dataout = {}>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.CPPID = "0" />
	<cfset dataout.MESSAGE = "" />
	<cfset dataout.ERRMESSAGE = "" />

	<cfset var updateCPP = ''>
	<cfset var result = ''>
	<cfset var xmlOldData = ''>
	<cfset var cppData	= '' />

	<!---<cftry>--->

		<!--- GET CURRENT DATA --->
		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPPTemplate" returnvariable="cppData">
			<cfinvokeargument name="ccpxDataId" value="#inpCppId#">
		</cfinvoke>


		<cfif cppData.RXRESULTCODE GT 0>
			<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
				UPDATE 
				simplelists.cppx_template
				SET 
				status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">
				WHERE  
				tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCppId#">
			</cfquery>

			<cfif result.RecordCount GT 0>
				<cfset dataout.MESSAGE = 'Update MLP successfully.'>
				<cfset dataout.CPPID = inpCppId />
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
			</cfif>	
		</cfif>

		   <!--- <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
		</cftry> ---> 


		<cfreturn dataout>
	</cffunction>


	<cffunction name="SaveMLP" access="remote" output="true" hint="update CPP">
    	<cfargument name="ccpxDataId" type="string" required="yes">
		<cfargument name="cppxName" type="string" required="no" default="">
		<cfargument name="cppxURL" type="string" required="no" default="">
		<cfargument name="inpRawContent" type="string" required="yes">
		<cfargument name="inpCustomCSS" type="string" required="no" default="">
		<cfargument name="inpAutoPublish" type="string" required="no" default="1">
		<cfargument name="inpExpire" type="string" required="no" default="">
		<cfargument name="isForCampaignMLP" type="numeric" required="no" default="0">
		<cfargument name="inpStatus" type="numeric" required="no" default="1">

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   
	   	<cfset var updateCPP = '' />
	   	<cfset var ValidateURL = ''/>
	   	<cfset var SelectCurrent = '' />
	   	<cfset var GetNOWFromDB = '' />
	   	<cfset var MLPHIstory = '' />
	   	<cfset var result = '' />
	   	<cfset var xmlOldData = '' />
	   	<cfset var cppData = '' />
   	
	   	<cftry>
	   		
	   		<!--- GET CURRENT DATA --->
	   		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="cppData">
	   		 	<cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	   		</cfinvoke>

	   		<cfif cppData.RXRESULTCODE NEQ 1>
	   			<cfset dataout.MESSAGE = "This MLP not found." />
	   			<cfset dataout.ERRMESSAGE = "This MLP not found." />
	   			<cfreturn dataout>
	   		</cfif>
	   		
	   		<!--- ***Todo apply security - remove certain tags like cfquery --->
	   		
	   		<cfif arguments.isForCampaignMLP EQ 0>
		   		<!--- GET CURRENT DATA --->
		   		<cfinvoke method="checkMLPUrlAvailability" returnvariable="ValidateURL">
		   		 	<cfinvokeargument name="inpURL" value="#cppxURL#">
		   		 	<cfinvokeargument name="MLPId" value="#ccpxDataId#">
		   		</cfinvoke>
		   		
		   		<cfif ValidateURL.RXRESULTCODE NEQ 1>
			   		
			   		<cfset dataout.RXRESULTCODE = -3 />
			   		<cfset dataout.MESSAGE = "URL is Already in use" />
		   			<cfset dataout.ERRMESSAGE = "" />
		   			<cfreturn dataout>
		   			
		   		</cfif>
		   	</cfif>
     
	   		<!--- Convert from Boolean --->
	   		<cfif arguments.inpAutoPublish EQ 'true'>		   	
		   		<cfset arguments.inpAutoPublish = "1" />	
		   	<cfelse>
		   		<cfset arguments.inpAutoPublish = "0" />
	   		</cfif>
	   			
	   		<!--- Get current values --->
	   		<cfquery name="SelectCurrent" datasource="#Session.DBSourceEBM#">
	            SELECT
	            	RawContent_vch,
	            	CustomCSS_vch,
	            	cppxName_vch,
	            	cppxURL_vch,
	            	AutoPublish_int,
	            	Expire_dt	            	
	            FROM
	            	simplelists.cppx_data		            	
	            WHERE  
	                ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
	            AND        
		            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	      	</cfquery>			            
		    
		    <!--- Un-Escape Special Tags --->
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<mlp-meta", "<meta", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "mlp-meta/>", "meta/>", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<mlp-script", "<script", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "mlp-script/>", "script/>", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<mlp-iframe", "<iframe", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "mlp-iframe/>", "iframe/>", "All") />
	   		
	   		<!--- Remove bad coldfusion tags  cfdelete, cfquery, cfabort, etc...... --->
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfquery", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfdelete", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfinclude", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfinvoke", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfdelete", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<cf", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfexecute", "INVALID", "All") />
	   		<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfscript", "INVALID", "All") />
	      	
	      	<!--- Un-Escape Special Tags --->
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<mlp-meta", "<meta", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "mlp-meta/>", "meta/>", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<mlp-script", "<script", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "mlp-script/>", "script/>", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<mlp-iframe", "<iframe", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "mlp-iframe/>", "iframe/>", "All") />
	   		
	   		<!--- Remove bad coldfusion tags  cfdelete, cfquery, cfabort, etc...... --->
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfquery", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfdelete", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfinclude", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfinvoke", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfdelete", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<cf", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfexecute", "INVALID", "All") />
	   		<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfscript", "INVALID", "All") />
        
	   		<!--- Make sure somthing has actually changed - no need to fill DB with old history of no changes --->
	   		<cfif SelectCurrent.RawContent_vch NEQ arguments.inpRawContent OR SelectCurrent.CustomCSS_vch NEQ arguments.inpCustomCSS OR SelectCurrent.cppxName_vch NEQ cppxName OR SelectCurrent.cppxURL_vch NEQ cppxURL OR SelectCurrent.AutoPublish_int NEQ inpAutoPublish OR LSDateFormat(SelectCurrent.Expire_dt, 'yyyy-mm-dd') NEQ TRIM(inpExpire)>
	   			   		  			      		      		   		
		   		<!--- write to history table so old versions can be retrieved  --->	   		
		   		<cfquery name="MLPHIstory" datasource="#Session.DBSourceEBM#">
		            INSERT INTO
		            	simplelists.mlp_history
		            SELECT
		            	null, 
		            	ccpxDataId_int,
		            	0,
		            	RawContent_vch,
		            	CustomCSS_vch,
		            	NOW()		            	
		            FROM
		            	simplelists.cppx_data		            	
		             WHERE  
		                ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
		            AND        
			            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		      	</cfquery>	
		      			   		 
			   	<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
		            UPDATE 
		                simplelists.cppx_data
		            SET
		            <cfif cppxName NEQ ''>
		            	cppxName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cppxName)#">,
		            </cfif> 		           	
		           	<!--- UUID and URL are one and the same - dont need two fields for this - old bad logic got copied --->           
		            <cfif cppxURL NEQ ''>
		            	cppxUUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cppxURL)#">,
		            	cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cppxURL)#">,
		            </cfif> 	            
		             	RawContent_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpRawContent#">,
		            	CustomCSS_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpCustomCSS#">,
		            	Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpStatus#">,
		            	Updated_dt = NOW(),
		            	
		            	<!--- Check if auto publish is on --->
		            	<cfif arguments.inpAutoPublish GT 0>
			            	PublishedRawContent_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpRawContent#">, 
							PublishedCustomCSS_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpCustomCSS#">,
							LastPublished_dt = NOW(),			            	
		            	</cfif>	
		            	
		            	AutoPublish_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAutoPublish#">,
		            	Expire_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpExpire)#" null="#IIF(TRIM(inpExpire) EQ "", true, false)#">
		            WHERE  
		                ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
			        AND        
			            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
	        	</cfquery>		
			
		        <cfif result.RecordCount GT 0>
		        	<cfset dataout.MESSAGE = "Update MLP successfully.">
		        	<cfset dataout.CPPID = ccpxDataId />
		        	<cfset dataout.RXRESULTCODE = 1 />
	
	                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
	                    <cfinvokeargument name="moduleName" value="MLPID = #arguments.ccpxDataId#">
	                    <cfinvokeargument name="operator" value="MLP updated">
	                </cfinvoke>
		        <cfelse>
		        	<cfset dataout.MESSAGE = 'Update MLP failed.'>
		        	<cfset dataout.RXRESULTCODE = 0>
		        </cfif>	
   
			<cfelse>
				<cfset dataout.MESSAGE = "MLP has not changed.">
	        	<cfset dataout.RXRESULTCODE = 2 />				
			</cfif>	
   
		    <cfcatch>	
		    	<cfset dataout.RAW = arguments.inpRawContent />
		    			    	
		    	<cfif findnocase("UC_UUID", cfcatch.detail) GT 0>
		    		<cfset dataout.MESSAGE = 'Update MLP fail. URL Key is already in use by another MLP. This key must be unique. Please try a different key'>
		    	<cfelse>
			    	<cfset dataout.MESSAGE = 'Update MLP failed.'>
		    	</cfif>

		    	<cfset dataout.RXRESULTCODE = -2>
		    			    	
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry> 
	    
	    <cfreturn dataout>
    </cffunction>

	<cffunction name="ReadMLPTemplate" access="public" output="true" hint="Read MLP Template">
		<cfargument name="ccpxDataId" type="string" required="no" default="0">
		<cfargument name="inpTemplateUser" required="no" default="0">

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.DESC = "" />
		<cfset dataout.CPPNAME = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.RAWCONTENT = "Error Reading Content" />
		<cfset dataout.CUSTOMCSS = "" />
		<cfset dataout.AUTOPUBLISH = 1 />

<!--- 
	SELECT `cppx_template`.`tempalteId_int`,
    `cppx_template`.`title_vch`,
    `cppx_template`.`description_vch`,
    `cppx_template`.`image_vch`,
    `cppx_template`.`customHtml`,
    `cppx_template`.`XML_txt`,
    `cppx_template`.`status_int`,
    `cppx_template`.`RawContent_vch`,
    `cppx_template`.`UserId_int`
FROM `simplelists`.`cppx_template`;
--->

<cfset var readCPP = ''>
<cftry>
	<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
		SELECT 
		description_vch,
		RawContent_vch,
		CustomCSS_vch
		FROM	
		simplelists.cppx_template
		WHERE	               	            
		tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ccpxDataId#">
		AND 

		<cfif inpTemplateUser GT 0>       
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		<cfelse>
			<!--- Admin templates are UserId_int = 0 and are visible to all users --->
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">		        
		</cfif>    

	</cfquery>

	<cfif readCPP.RecordCount GT 0>

		<cfset dataout.CPPID = arguments.ccpxDataId />
		<cfset dataout.DESC = readCPP.description_vch />
		<cfset dataout.CPPNAME = readCPP.description_vch />
		<cfset dataout.RAWCONTENT = readCPP.RawContent_vch />
		<cfset dataout.CUSTOMCSS = readCPP.CustomCSS_vch />

		<cfset dataout.RXRESULTCODE = 1 />

	<cfelse>
		<cfset dataout.MESSAGE = 'Can not get this MLP data'>
		<cfset dataout.RAWCONTENT = '' />
	</cfif>	

	<cfcatch>
		<cfset dataout.MESSAGE = 'Can not get this MLP data'>
		<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
	</cfcatch>    	
</cftry>    	

<cfreturn dataout>
</cffunction>

<cffunction name="AddMLPTemplate" access="public" output="false" hint="Create MLP Template">
	<cfargument name="inpRawContent" type="string" required="no" default="">
	<cfargument name="inpDesc" type="string" required="no" default="New MLP Template">
	<cfargument name="inpCustomCSS" type="string" required="no" default="">

	<cfset var dataout = {}>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.CPPID = "0" />
	<cfset dataout.MESSAGE = "" />
	<cfset dataout.ERRMESSAGE = "" />
	<cfset dataout.AUTOPUBLISH = 0 />

	<cfset var insertMLPTemplate = ''>
	<cfset var insertResult = ''>
	<cfset var allowedMlpAdd =  false />

	<cftry>
	   		<!---
	   		<cfinvoke method="CheckAddPerm"	 returnvariable="allowedMlpAdd"></cfinvoke>      
		 	<cfif allowedMlpAdd>	
		 	--->	
		 	<cfquery name="insertMLPTemplate" datasource="#Session.DBSourceEBM#" result="insertResult" >
		 		INSERT INTO 
		 		simplelists.cppx_template
		 		(					
		 			description_vch,
		 			title_vch,
		 			RawContent_vch,
		 			CustomCSS_vch
		 			)
		 		VALUES
		 		(				
		 			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
		 			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
		 			<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpRawContent#">,
		 			<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpCustomCSS#">
		 			)

		 	</cfquery>
	        <!---	
   			</cfif>
   		--->
   		<!--- <cfif allowedMlpAdd AND structKeyExists(insertResult, 'GENERATED_KEY') AND insertResult.GENERATED_KEY GT 0> --->
   			<cfif structKeyExists(insertResult, 'GENERATED_KEY') AND insertResult.GENERATED_KEY GT 0>
   				<cfset dataout.RXRESULTCODE = 1 />
   				<cfset dataout.MESSAGE = 'Created MLP Template successfully.'>
   				<cfset dataout.CPPID = insertResult.GENERATED_KEY />
   			<cfelse>
   				<cfset dataout.MESSAGE = 'Create MLP Template failed. Please try again later'>	
   			</cfif>

   			<cfcatch>	
   				<cfset dataout.MESSAGE = 'Error - Create MLP Template failed. Please try again later'>
   				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
   			</cfcatch>   	
   		</cftry> 


   		<cfreturn dataout>
   	</cffunction>

   	<cffunction name="SaveMLPTemplate" access="remote" output="true" hint="update CPP">
   		<cfargument name="ccpxDataId" type="string" required="yes">
   		<cfargument name="cppxName" type="string" required="no" default="Create MLP Template">
   		<cfargument name="inpRawContent" type="string" required="yes">
   		<cfargument name="inpTemplateUser" required="no" default="0">
   		<cfargument name="inpCustomCSS" type="string" required="no" default="">

   		<cfset var dataout = {}>
   		<cfset dataout.RXRESULTCODE = -1 />
   		<cfset dataout.CPPID = "0" />
   		<cfset dataout.MESSAGE = "" />
   		<cfset dataout.ERRMESSAGE = "" />

   		<cfset var updateCPP = ''>
   		<cfset var result = ''>
   		<cfset var xmlOldData = ''>
   		<cfset var cppData = '' />


   		<cftry>

   			<!--- GET CURRENT DATA --->
   			<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="cppData">
   				<cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
   			</cfinvoke>

   			<cfif cppData.RXRESULTCODE NEQ 1>
   				<cfset dataout.MESSAGE = "This MLP not found." />
   				<cfset dataout.ERRMESSAGE = "This MLP not found." />
   				<cfreturn dataout>
   			</cfif>

   			<!--- TODO: write to history table so old versions can be retrieved  --->

   			<!--- Un-Escape Special Tags --->
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<mlp-meta", "<meta", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "mlp-meta/>", "meta/>", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<mlp-script", "<script", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "mlp-script/>", "script/>", "All") />

   			<!--- Remove bad coldfusion tags  cfdelete, cfquery, cfabort, etc...... --->
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfquery", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfdelete", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfinclude", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfinvoke", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfdelete", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "<cf", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfexecute", "INVALID", "All") />
   			<cfset arguments.inpRawContent = Replace(arguments.inpRawContent, "cfscript", "INVALID", "All") />

   			<!--- Un-Escape Special Tags --->
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<mlp-meta", "<meta", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "mlp-meta/>", "meta/>", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<mlp-script", "<script", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "mlp-script/>", "script/>", "All") />

   			<!--- Remove bad coldfusion tags  cfdelete, cfquery, cfabort, etc...... --->
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfquery", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfdelete", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfinclude", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfinvoke", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfdelete", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "<cf", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfexecute", "INVALID", "All") />
   			<cfset arguments.inpCustomCSS = Replace(arguments.inpCustomCSS, "cfscript", "INVALID", "All") />


   			<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
   				UPDATE 
   				simplelists.cppx_template
   				SET
   				<cfif cppxName NEQ ''>
   					description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppxName#">,
   				</cfif>

   				<cfif cppxName NEQ ''>
   					title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cppxName#">,
   				</cfif> 	
   				RawContent_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpRawContent#">,
   				CustomCSS_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#arguments.inpCustomCSS#">
   				WHERE  
   				tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ccpxDataId#">
   				AND        
   				<cfif inpTemplateUser GT 0>       
   					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
   				<cfelse>
   					<!--- Admin templates are UserId_int = 0 and are visible to all users --->
   					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">		        
   				</cfif>  
   			</cfquery>		

   			<cfif result.RecordCount GT 0>
   				<cfset dataout.MESSAGE = 'Update MLP successfully.'>
   				<cfset dataout.CPPID = ccpxDataId />
   				<cfset dataout.RXRESULTCODE = 1 />
   			<cfelse>
   				<cfset dataout.MESSAGE = 'Update MLP fail.Please try again later'>
   			</cfif>	

   			<cfcatch>	
   				<cfset dataout.RAW = inpRawContent />
   				<cfset dataout.MESSAGE = 'Update CPP fail.Please try again later'>
   				<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
   			</cfcatch>   	
   		</cftry> 

   		<cfreturn dataout>
   	</cffunction>

   	<cffunction name="AddMLP" access="remote" output="false" hint="Create MLP">
    	<cfargument name="inpRawContent" type="string" required="no" default="">
    	<cfargument name="inpDesc" type="string" required="no" default="New MLP Template">
		<cfargument name="cppxURL" type="string" required="no" default="">
		<cfargument name="inpCustomCSS" type="string" required="no" default="">
		<cfargument name="inpMLPType" type="numeric" required="no" default="1">
		<cfargument name="inpBatchId" type="string" required="no" default="">
		<cfargument name="inpLength" type="Numeric" required="no" default="6" hint="The length of the short URL key - 6 by default" />

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.AUTOPUBLISH = 0 />
	   	<cfset dataout.URLNAME = '' />

	   	<cfset var insertMLPTemplate = ''>
	   	<cfset var insertResult = ''>
	   	
	   	<cfset var CreateUUID = ''/>
	   	<cfset var URLName = ''/>
	   	
	   	<cfset var RetVarGenerateLinkCode = '' />
	   	<cfset var currentIndex = '' />
		<cfset var CheckForCollisions = '' />
		<cfset var GetUniqueTargetId = '' />
		<cfset var UniqueTargetId = '0' />
		<cfset var allowedMlpAdd = false />


	   	<cftry>
	   		
	   	   				   		
	   		<cfif LEN(TRIM(arguments.cppxURL)) EQ 0> 
		        <cfloop index = "currentIndex" from = "1" to = "100" step = "1">
					
					<!--- Generate a randon key --->
					<cfinvoke method="GenerateLinkCode" returnvariable="RetVarGenerateLinkCode">
					 	<cfinvokeargument name="inpLength" value="#inpLength#">                                   
					</cfinvoke>   	
	
					<!--- Lookup in DB for Collisions - key must be unique - dont rely on error handling do an actual lookup --->
					<!--- always get latest entry for operator Id regardless of whose list it is on --->
		            <cfquery name="CheckForCollisions" datasource="#Session.DBSourceEBM#">
		                SELECT 
		                    COUNT(*) AS TOTALCOUNT
		                FROM
		                    simplelists.cppx_data
		                WHERE
		                    cppxUUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGenerateLinkCode.SHORTURLKEY#"> 
		                AND
							status_int = 1	
		            </cfquery>  
	
					<cfif CheckForCollisions.TOTALCOUNT EQ 0> 
						<!--- Exit loop if no collision found--->	
						
						<cfset URLName = RetVarGenerateLinkCode.SHORTURLKEY />
										
						<cfbreak />
					</cfif>            	
	
				</cfloop>	
				
			<cfelse>
			
				<cfset URLName = arguments.cppxURL/>						       
				
			</cfif>

			<cfif arguments.inpMLPType EQ 2>
				<cfset allowedMlpAdd = true/>
			<cfelse>
				<cfinvoke method="CheckAddPerm"	 returnvariable="allowedMlpAdd"></cfinvoke>
			</cfif> 

		   	<cfif allowedMlpAdd>		


	   			<cfquery name="insertMLPTemplate" datasource="#Session.DBSourceEBM#" result="insertResult" >
		            INSERT INTO 
		            	simplelists.cppx_data
					(	
						cppxUUID_vch,
						cppxName_vch,
						cppxURL_vch,
						UserId_int,
						Status_int,
						RawContent_vch,
						CustomCSS_vch,
						PublishedRawContent_vch,
						PublishedCustomCSS_vch,
						LastPublished_dt,
						Created_dt,
						Updated_dt,
						<cfif arguments.inpBatchId NEQ ''>
						BatchId_bi,
						</cfif>
						MlpType_ti
					)
					VALUES
					(	
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#URLName#">,	
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDesc#">,		
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#URLName#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpRawContent#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpCustomCSS#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpRawContent#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpCustomCSS#">,
						NOW(),
						NOW(),
						NOW(),
						<cfif arguments.inpBatchId NEQ ''>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpBatchId#">,
						</cfif>
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMLPType#">
					)
	        	</cfquery>
	        </cfif>	

        	<cfif allowedMlpAdd AND structKeyExists(insertResult, 'GENERATED_KEY') AND insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Created MLP successfully.'>
	        	<cfset dataout.CPPID = insertResult.GENERATED_KEY />
	        	<cfset dataout.URLNAME = '#URLName#' />

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="MLPID = #insertResult.GENERATED_KEY#">
                    <cfinvokeargument name="operator" value="MLP created">
                </cfinvoke>
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create MLP failed. Please try again later'>	
	        </cfif>
			
	       
		    <cfcatch>	
		    	<cfset dataout.MESSAGE = 'Error - Create MLP failed. Please try again later'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>   	
	    </cftry> 
	    	
	    <cfreturn dataout>
    </cffunction>

   	<cffunction name="GenerateLinkCode" access="public" output="false" hint="Generate a alphanumberic key - non-case sensitive">   
   		<cfargument name="inpLength" type="Numeric" required="true" default="6">   

   		<cfset var i	= '' />
   		<!--- create a list of all allowable characters for our short URL link - not case sensitive --->   
   		<cfset var chars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,-">   
   		<cfset var i = '' />

   		<!--- our radix is the total number of possible characters --->   
   		<cfset var radix=listlen(local.chars)>

   		<cfset var randnum = 1>  

   		<!---   Then, after setting our return variable to an empty string, we're going to add random characters from that list in a loop: --->
   		<cfset var dataout=StructNew()>

   		<!--- initialise our return variable --->   
   		<cfset dataout.SHORTURLKEY=""> 

		<!--- loop from 1 until the number of characters our URL should be,   
			adding a random character from our master list on each iteration  --->   
			<cfloop from="1" to="#arguments.inpLength#" index="i">   

				<!--- generate a random number in the range of 1 to the total number of possible characters we have defined --->   
				<cfset randnum = RandRange(1, radix)>   

				<!--- add the character from a random position in our list to our short link --->   
				<cfset dataout.SHORTURLKEY=dataout.SHORTURLKEY & listGetAt(chars, randnum)>   

			</cfloop>

			<!--- Finally, return the newly generated short link to the calling code: --->	
			<!--- return the generated random short link --->   
			<cfreturn dataout>   

		</cffunction> 


		<cffunction name="CheckAddPerm" access="public" hint="Check validation for current user's actived plan to active MLP">
			<cfset var totalActivedMLPs = 0 />
			<cfset var allowed = false />
			<cfset var curUserPlan = {} />

			<cftry>
				<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="curUserPlan">
				</cfinvoke>
				<cfinvoke method="GetTotalCCPActived" returnvariable="totalActivedMLPs" >
				</cfinvoke>
				<!--- Determines if Add MLP condition of current user is true --->
				<cfif curUserPlan.mlpsLimitNumber EQ 0 OR (curUserPlan.mlpsLimitNumber GT 0 AND totalActivedMLPs LT curUserPlan.mlpsLimitNumber) >
					<cfset allowed = true />
				</cfif>
				<cfcatch type="any">

				</cfcatch>
			</cftry>
			<cfreturn allowed />
		</cffunction>



		<cffunction name="GetTotalCCPActived" access="private" hint="Get total MLP actived of current user">
			<cfset var rsCount = {} />
			<cfset var countMLP = 0 />

			<cftry>
				<!---Get total actived MLP --->
				<cfquery name="rsCount" datasource="#Session.DBSourceREAD#">
					SELECT
					COUNT(1) AS totalActived
					FROM
					simplelists.cppx_data AS c
					WHERE        
					c.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
					c.status_int <> -1
			    	AND
			    	c.MlpType_ti = 1
				</cfquery>
				<cfset countMLP = rsCount.totalActived />
				<cfcatch type="any">
					<cfset countMLP = -1 />
				</cfcatch>
			</cftry>
			<cfreturn countMLP />
		</cffunction>


		<cffunction name="getMLPHistoryList" access="remote" output="false" hint="get list MLP">
			<cfargument name="MLPId" default="0" required="true" type="numeric">
			<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
			<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
			<cfargument name="sColumns" default="" />
			<cfargument name="sEcho">
			<cfargument name="iSortCol_1" default="-1">
			<cfargument name="sSortDir_1" default="">
			<cfargument name="iSortCol_2" default="-1">
			<cfargument name="sSortDir_2" default="">
			<cfargument name="iSortCol_0" default="-1">
			<cfargument name="sSortDir_0" default="">
			<cfargument name="customFilter" default="">
		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
			--->
			<!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
			<cfargument name="sSearch" required="no"  default="">
			<cfargument name="iSortingCols" required="no"  default="0">

			<cfset var dataOut = {}>
			<cfset dataOut.DATA = ArrayNew(1)>
			<cfset var GetNumbersCount = ''>
			<cfset var order = ''>
			<cfset var actionHtml = ''/>
			<cfset var DisplaySection = ''/>
			<cfset var cppStatusTxt	= '' />
			<cfset var tempItem	= '' />
			<cfset var filterItem	= '' />
			<cfset var Created_dt	= '' />
			<cfset var allowedMlpAdd = false />
			<cfset var mlpCount = 1 />
			<cfset var listColumns = "" />
			<cfset var n = "" />
			<cfset var thisS = "" />
			<cfset var GetHistory = "" />

			<!--- <cftry>--->

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
				<cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />

				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>

				<!---ListEMSData is key of DataTable control--->
				<cfset dataout["ListHistoryData"] = ArrayNew(1)>

				<!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
				<cfargument name="sSearch" required="no"  default="">

				<cfset listColumns = "PKId_bi,Created_dt" />

				<!---Get total EMS for paginate --->
				<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
					SELECT
					COUNT(*) AS TOTALCOUNT
					FROM
					simplelists.mlp_history AS c
					WHERE        
					c.MLPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
					<cfif customFilter NEQ "">
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
				</cfquery>

				<cfif GetNumbersCount.TOTALCOUNT GT 0>
					<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
					<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>

					<!--- Get ems data --->		
					<cfquery name="GetHistory" datasource="#Session.DBSourceREAD#">
						SELECT 
						c.PKId_bi,
						c.Created_dt
						FROM
						simplelists.mlp_history c
						WHERE 
						c.MLPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
						<cfif customFilter NEQ "">
							<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
						<cfif iSortingCols gt 0>
							ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>c.#listGetAt(listColumns,(arguments["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",arguments["sSortDir_"&thisS]) gt 0>#arguments["sSortDir_"&thisS]#</cfif> </cfloop>
						</cfif>
						LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
					</cfquery>

					<cfloop query="GetHistory">

						<cfset tempItem = {
							PKID = "#GetHistory.PKId_bi#",
							CREATED_DT = "<div class='mlp-historical-date' data-height='720' data-width='1080' data-hdt='#GetHistory.PKId_bi#'>#LSDateFormat(GetHistory.Created_dt, 'yyyy-mm-dd') & ' <br/>' & LSTimeFormat(GetHistory.Created_dt, 'HH:mm:ss z')#</div>"				
						}/>

						<cfset dataout["ListHistoryData"].append(tempItem) />
					</cfloop>

			<!--- ***TODO
				Use javascript to set local time specific 
				
				<cfset n = dateconvert("local2utc",now())>
				<script>
				 d = new Date();
				 d.setUTCFullYear(#dateformat(n, "yyyy")#);
				 d.setUTCMonth(#dateformat(n, "m")#);
				 d.setUTCDate(#dateformat(n, "d")#);
				 d.setUTCHours(#timeformat(n, "H")#);
				 d.setUTCMinutes(#timeformat(n, "m")#);
				 document.write(d.toLocaleString());
				</script>

				
			--->
			
			
		</cfif>
	    <!---    
        <cfcatch type="Any">
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
    --->
    <cfreturn serializeJSON(dataOut)>
</cffunction>


<cffunction name="PublishMLP" access="remote" hint="Publish Staged MLP Content">
	<cfargument name="MLPId" type="string" required="yes">

	<cfset var UpdatePublished = {} />
	<cfset var LOCALOUTPUT = {} />

	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	<cfset LOCALOUTPUT.MESSAGE = ""/>
	<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 

	<cftry>
		<!---Copy raw data to published data --->
		<cfquery name="UpdatePublished" datasource="#Session.DBSourceEBM#">
			UPDATE 
			simplelists.cppx_data 
			SET 
			PublishedRawContent_vch = RawContent_vch, 
			PublishedCustomCSS_vch = CustomCSS_vch, 
			LastPublished_dt = NOW()
			WHERE  
			ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
			AND        
			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		</cfquery>

		<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
		<cfset LOCALOUTPUT.MESSAGE = ""/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 


		<cfcatch type="any">
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
	</cftry>
	<cfreturn LOCALOUTPUT />
</cffunction>

<cffunction name="GetCountSavesSinceLastPublishMLP" access="remote" hint="Get the number of saves since the last time user published staged MLP content">
	<cfargument name="MLPId" type="string" required="yes">

	<cfset var GetCounts = {} />
	<cfset var GetLastSavedDate = {} />
	<cfset var GetLastPublishedDate = {} />
	<cfset var LOCALOUTPUT = {} />

	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	<cfset LOCALOUTPUT.COUNT = "0"/>
	<cfset LOCALOUTPUT.LASTSAVED = "0"/>
	<cfset LOCALOUTPUT.LASTPUBLISHED = "0"/>
	<cfset LOCALOUTPUT.MESSAGE = ""/>
	<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 

	<cftry>
		
		<cfquery name="GetLastPublishedDate" datasource="#Session.DBSourceEBM#">
			SELECT 
			d.LastPublished_dt 
			FROM 
			simplelists.cppx_data as d
			WHERE  
			d.ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
			AND        
			d.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		</cfquery>


		<cfquery name="GetLastSavedDate" datasource="#Session.DBSourceEBM#">
			SELECT 
			d.Updated_dt 
			FROM 
			simplelists.cppx_data as d
			WHERE  
			d.ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
			AND        
			d.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
		</cfquery>

		<cfif GetLastPublishedDate.LastPublished_dt NEQ "">
			
			<!---Copy raw data to published data --->
			<cfquery name="GetCounts" datasource="#Session.DBSourceEBM#">
				SELECT
				COUNT(*) AS TOTALCOUNT
				FROM
				simplelists.mlp_history AS c
				WHERE        
				c.MLPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
				AND
				c.Created_dt > <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#LSDateFormat(GetLastPublishedDate.LastPublished_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetLastPublishedDate.LastPublished_dt, 'HH:mm:ss')#">						
			</cfquery>
			
			<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.COUNT =  GetCounts.TOTALCOUNT/>

			<cfset LOCALOUTPUT.LASTSAVED = "#LSDateFormat(GetLastSavedDate.Updated_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetLastSavedDate.Updated_dt, 'HH:mm:ss z')#" />
			<cfset LOCALOUTPUT.LASTPUBLISHED = "#LSDateFormat(GetLastPublishedDate.LastPublished_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetLastPublishedDate.LastPublished_dt, 'HH:mm:ss z')#" />

			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 

		<cfelse>

			<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
			<cfset LOCALOUTPUT.COUNT = 0/>
			<cfset LOCALOUTPUT.LASTSAVED = "#LSDateFormat(GetLastSavedDate.Updated_dt, 'yyyy-mm-dd')# #LSTimeFormat(GetLastSavedDate.Updated_dt, 'HH:mm:ss z')#" /> 
			<cfset LOCALOUTPUT.LASTPUBLISHED = "Never" />
			<cfset LOCALOUTPUT.MESSAGE = ""/>
			<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 
			
		</cfif>        

		<cfcatch type="any">
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
	</cftry>
	<cfreturn LOCALOUTPUT />
</cffunction>

<cffunction name="RevertMLP" access="remote" hint="Revert Saved Staged MLP Content to Historical Data - user will still need to publish after review">
	<cfargument name="MLPId" type="string" required="yes">
	<cfargument name="inpHDT" type="string" required="yes">

	<cfset var UpdatePublished = {} />
	<cfset var MLPHIstory = {} />
	<cfset var SelectCurrent = {} />
	<cfset var LOCALOUTPUT = {} />
	<cfset var CUSTOMCSS = "" />

	<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
	<cfset LOCALOUTPUT.RAW = ""/>
	<cfset LOCALOUTPUT.RAWCSS = ""/>		
	<cfset LOCALOUTPUT.MESSAGE = ""/>
	<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 

	<cftry>			

		<cfif inpHDT GT 0> 

			<!--- Get historys values --->
			<cfquery name="SelectCurrent" datasource="#Session.DBSourceEBM#">
				SELECT
				RawContent_vch,
				CustomCSS_vch
				FROM
				simplelists.mlp_history		            	
				WHERE  
				PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpHDT#">	            
			</cfquery>			            

			<!--- Write current to history --->
			<cfquery name="MLPHIstory" datasource="#Session.DBSourceEBM#">
				INSERT INTO
				simplelists.mlp_history
				SELECT
				null, 
				ccpxDataId_int,
				0,
				RawContent_vch,
				CustomCSS_vch,
				NOW()
				FROM
				simplelists.cppx_data		            	
				WHERE  
				ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
				AND        
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			</cfquery>	

			<!---Copy raw data to published data --->
			<cfquery name="UpdatePublished" datasource="#Session.DBSourceEBM#">
				UPDATE 
				simplelists.cppx_data 
				SET 
				RawContent_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#SelectCurrent.RawContent_vch#">,
				CustomCSS_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#SelectCurrent.CustomCSS_vch#">, 
				Updated_dt = NOW()
				WHERE  
				ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MLPId#">
				AND        
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
			</cfquery>

			<cfset LOCALOUTPUT.RAW = SelectCurrent.RawContent_vch/>		
			<cfset LOCALOUTPUT.RAWCSS = SelectCurrent.CustomCSS_vch/>

		</cfif>

		<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
		<cfset LOCALOUTPUT.MESSAGE = ""/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/> 


		<cfcatch type="any">
			<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
		</cfcatch>
	</cftry>
	<cfreturn LOCALOUTPUT />
</cffunction>

<cffunction name="GetMLPList" access="remote" output="true" hint="Get mlp by UserId ">
	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
	<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
	<cfargument name="sColumns" default="" />
	<!--- <cfargument name="sEcho"> --->
	<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
	<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
	<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
	<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
	<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
	<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
	<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
	<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
	<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
	<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
	<cfargument name="inpAction" required="false" default="downgrade">

	<cfset var dataout = {} />
	<cfset dataout.RXRESULTCODE = '' />
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout["ListMLPData"] = ArrayNew(1) />
	<cfset dataout["iTotalRecords"] = 0>
	<cfset dataout["iTotalDisplayRecords"] = 0>
	<cfset var tempItem = {} />

	<cfset var getMLP = '' />
	<cfset var status = 1 >
	<cfset var rsCount = ''/>

	<cfif arguments.inpAction EQ 'upgrade'>
		<cfset status = 2 >
	</cfif>

	<cftry>
		<cfif arguments.iDisplayStart < 0>
			<cfset arguments.iDisplayStart = 0>
		</cfif>

		<cfquery name="getMLP" datasource="#Session.DBSourceREAD#">
			SELECT 
			SQL_CALC_FOUND_ROWS
			c.ccpxDataId_int,
			c.cppxUUID_vch,
			c.cppxName_vch,
			c.cppxURL_vch,
			c.created_dt,
			c.status_int
			FROM
			simplelists.cppx_data c
			WHERE 
			c.status_int = #status#    
			AND 
			c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
			AND 
			c.MlpType_ti = 1
			ORDER BY 
			c.ccpxDataId_int desc
			LIMIT 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">

		</cfquery>

		<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] =  (iTotalRecords GTE 1 ? iTotalRecords : 0)>
			<cfset dataout["iTotalRecords"] = (iTotalRecords GTE 1 ? iTotalRecords : 0)>
		</cfloop>

            <!--- <cfset dataout["iTotalRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) >
            	<cfset dataout["iTotalDisplayRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) > --->

            	<cfif getMLP.RECORDCOUNT GT 0>
            		<cfloop query="getMLP">
            			<cfset tempItem = 
            			{
            				NAME = '#getMLP.cppxName_vch#',
            				URL = '#getMLP.cppxURL_vch#', 
            				CREATED = '#DateFormat(getMLP.created_dt, 'mm/dd/yyyy ')#',
            				ID = '#getMLP.ccpxDataId_int#'
            			}
            			>
            			<cfset ArrayAppend(dataout["ListMLPData"],tempItem)>
            		</cfloop>

            		<cfset dataout.RXRESULTCODE = 1 />
            		<cfset dataout.MESSAGE = 'Get mlp successfully!' />
            	<cfelse>
            		<cfset dataout.RXRESULTCODE = 0 />
            		<cfset dataout.MESSAGE = 'No mlp found' />
            	</cfif>
            	<cfcatch type="any">
            		<cfset dataout.RXRESULTCODE = -1 />
            		<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            		<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            	</cfcatch>
            </cftry>

            <cfreturn dataout />
        </cffunction>


        <cffunction name="GetMLPByListId" access="remote" output="true" hint="get keyword info by list ID">
        	<cfargument name="inpListMLPId" type="array" required="true" default="">
        	<cfset var dataout = {} />
        	<cfset dataout.RXRESULTCODE = -1 />
        	<cfset dataout.MESSAGE = ""/>
        	<cfset dataout.ERRMESSAGE = ""/>
        	<cfset dataout.LIST = {}/>
        	<cfset dataout.NAMELIST = []/>
        	<cfset var listMLP = {} />

        	<cfset var defaultShortCode = '' />

        	<cfif arrayLen(inpListMLPId) GT 0>
        		<cftry>

        			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>

        			<cfquery name="listMLP" datasource="#Session.DBSourceEBM#">
        				SELECT 
        				ccpxDataId_int,cppxName_vch 
        				FROM 
        				simplelists.cppx_data
        				WHERE 
        				status_int = 1
        				AND 
        				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
        				AND
        				ccpxDataId_int IN (#ArrayToList(inpListMLPId)#)
        			</cfquery>

        			<cfif listMLP. RECORDCOUNT GT 0>
        				<cfloop query="#listMLP#">
        					<cfset dataout.LIST["#ccpxDataId_int#"] = #cppxName_vch#/>
        					<cfset arrayAppend(dataout.NAMELIST, cppxName_vch)/> 
        				</cfloop>
        				<cfset dataout.RXRESULTCODE = 1 />
        			</cfif>

        			<cfcatch type="any">
        				<cfset dataout.RXRESULTCODE = -1 />
        				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
        				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        			</cfcatch>
        		</cftry>
        	<cfelse>
        		<cfset dataout.MESSAGE = "Invalid input."/>    
        	</cfif>
        	<cfreturn dataout />
        </cffunction>

        <cffunction name="GetMLPByCampaignId" access="remote" hint="Get MLP by Campaign ID">
        	<cfargument name="inpBatchId" required="true"/>

        	<cfset var dataout = {} />
        	<cfset dataout.RXRESULTCODE = 0 />
        	<cfset dataout.MESSAGE = ""/>
        	<cfset dataout.ERRMESSAGE = ""/>
        	<cfset dataout.CPMLPID = 0/>
        	<cfset dataout.STATUS = 0/>
        	<cfset dataout.MLPURL = ''/>
        	<cfset var getMLP = ""/>

        	<cftry>
        		<cfquery name="getMLP" datasource="#Session.DBSourceEBM#">
        			SELECT
        			ccpxDataId_int,
        			Status_int,
        			cppxURL_vch
        			FROM
        			simplelists.cppx_data
        			WHERE
        			userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
        			AND
        			BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> 
        		</cfquery>

        		<cfif getMLP.RECORDCOUNT GT 0>
        			<cfset dataout.RXRESULTCODE = 1/>
        			<cfset dataout.MESSAGE = 'This batch already had a MLP'/>
        			<cfset dataout.CPMLPID = getMLP.ccpxDataId_int/>
        			<cfset dataout.STATUS = getMLP.Status_int/>
        			<cfset dataout.MLPURL = getMLP.cppxURL_vch/>
        		<cfelse>
        			<cfset dataout.RXRESULTCODE = 0/>
        			<cfset dataout.MESSAGE = 'Please click on Finished Button before you share it!'/>
        		</cfif>

        		<cfcatch type="any">
        			<cfset dataout.RXRESULTCODE = -1 />
        			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
        			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        		</cfcatch>
        	</cftry>

        	<cfreturn dataout />
        </cffunction>
		<cffunction name="DeleteMLPByCampaignId" access="remote" hint="Delete MLP by Campaign ID">
        	<cfargument name="inpBatchId" required="true"/>

        	<cfset var dataout = {} />
        	<cfset dataout.RXRESULTCODE = 0 />
        	<cfset dataout.MESSAGE = ""/>
        	<cfset dataout.ERRMESSAGE = ""/>        	
        	<cfset var deleteMLP = ""/>

        	<cftry>
        		<cfquery name="deleteMLP" datasource="#Session.DBSourceEBM#">
        			DELETE FROM simplelists.cppx_data        			
        			WHERE
        			userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
        			AND
        			BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> 
        		</cfquery>

        		<cfset dataout.RXRESULTCODE = 1/>
        		<cfset dataout.MESSAGE = 'Delete MLP Successfully'/>

        		<cfcatch type="any">
        			<cfset dataout.RXRESULTCODE = -1 />
        			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
        			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        		</cfcatch>
        	</cftry>

        	<cfreturn dataout />
        </cffunction>

        <cffunction name="GetCampaignMLPTemplate" access="remote" hint="Get Campaign MLP Template ID">
        	<cfset var dataout = {} />
        	<cfset dataout.RXRESULTCODE = 0 />
        	<cfset dataout.MESSAGE = ""/>
        	<cfset dataout.ERRMESSAGE = ""/>
        	<cfset dataout.TEMPLATEID = 0/>
        	<cfset var getMLP = ""/>

        	<cftry>
        		<cfquery name="getMLP" datasource="#Session.DBSourceREAD#">
        			SELECT
        			tempalteId_int
        			FROM
        			simplelists.cppx_template
        			WHERE
        			title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Campaign MLP"> 
        		</cfquery>

        		<cfif getMLP.RECORDCOUNT GT 0>
        			<cfset dataout.RXRESULTCODE = 1/>
        			<cfset dataout.MESSAGE = 'Get template successfully'/>
        			<cfset dataout.TEMPLATEID = getMLP.tempalteId_int/>
        		</cfif>

        		<cfcatch type="any">
        			<cfset dataout.RXRESULTCODE = -1 />
        			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
        			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        		</cfcatch>
        	</cftry>

        	<cfreturn dataout />
        </cffunction>
        

    <cffunction name="GetCampaignMLPTemplate" access="remote" hint="Get Campaign MLP Template ID">
    	<cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TEMPLATEID = 0/>
        <cfset var getMLP = ""/>

       	<cftry>
       		<cfquery name="getMLP" datasource="#Session.DBSourceREAD#">
       			SELECT
       				tempalteId_int
       			FROM
       				simplelists.cppx_template
       			WHERE
       				title_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Campaign MLP"> 
       		</cfquery>

       		<cfif getMLP.RECORDCOUNT GT 0>
       			<cfset dataout.RXRESULTCODE = 1/>
       			<cfset dataout.MESSAGE = 'Get template successfully'/>
       			<cfset dataout.TEMPLATEID = getMLP.tempalteId_int/>
       		</cfif>

       		<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
       	</cftry>

       	<cfreturn dataout />
    </cffunction>

    <cffunction name="ConvertMLPImageToBase64" access="remote" hint="Convert MLP Image To Base 64">
    	<cfargument name="inpURL" required="true"/>

    	<cfset var resp = {}>
    	<cfset var dataout = {}/>
    	<cfset var JsonResult = ''/>
    	<cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.URL = ""/>

    	<cftry>
    		<cfif !find('data:image/png;base64,', arguments.inpURL)>
	    		<cfhttp  url="#arguments.inpURL#" method="get" timeout="3" result="resp">
	    		</cfhttp>

	    		<cfif resp.status_code EQ 200>
	    			<cfset JsonResult=ToBase64(resp.fileContent)/>

	    			<cfset dataout.RXRESULTCODE = 1 />
			        <cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = ""/>
			        <cfset dataout.URL = "data:image/png;base64,#JsonResult#"/>
	    		</cfif>
	    	</cfif>
    		<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
       	</cftry>

       	<cfreturn dataout />
    </cffunction>
    </cfcomponent>