<cfcomponent>
	<cfsetting showdebugoutput="true">
	<cfinclude template="/session/sire/configs/paths.cfm">
    <cfinclude template="/session/sire/configs/env_paths.cfm">
	<cfinclude template="/session/sire/configs/userConstants.cfm">

	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	
	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="UpdateSecurityQuestionStatus" access="remote" output="true" hint="Get user info by session user id">
    	<cfargument name="SecurityQuestionEnabled" type="number" required="yes">

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>

        <cfset var updateUserInfo   = '' />
        <cfset var listQuestion = '' />
        <cfset var result   = '' />

        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>
            <cftry>

                <cfif arguments.SecurityQuestionEnabled EQ 1>
                    <cfinvoke method="GetUserListQuestion" returnvariable="listQuestion">
                        <cfinvokeargument name="detail_question" value=0>
                    </cfinvoke>

                    <cfif listQuestion.RESULT EQ 'FAIL'>
                        <cfset dataout.MESSAGE = 'Please set your security questions first.'>
                        <cfreturn dataout>
                    </cfif>
                </cfif>

                <cfquery name="updateUserInfo" datasource="#Session.DBSourceEBM#" result="result">
                   UPDATE
                        simpleobjects.useraccount
                    SET SecurityQuestionEnabled_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.SecurityQuestionEnabled#">    
                    WHERE                
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>  
                
                <cfif result.RecordCount GT 0>
                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.MESSAGE = 'Updated successfully.'>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = 'Update fail.'>
                    <cfreturn dataout>
                </cfif>
                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>                    
                    <cfreturn dataout>                             
                </cfcatch>
            </cftry>
        <cfelse>
    		<cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="getSecurityQuestionList" access="public" output="true" hint="get all security questions">
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.DATA = ''>
        <cfset var question_item    = '' />
        <cfset var getList  = '' />

        <cftry>
            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT 
                    QuestionId_int,GroupId_int,Question_vch
                FROM 
                    simpleobjects.security_questions_list 
            </cfquery>

            <cfif getList.RecordCount GT 0>

                <cfset dataout.DATA ={}>
                <cfset dataout.DATA['list_1'] = arrayNew()>
                <cfset dataout.DATA['list_2'] = arrayNew()>
                <cfset dataout.DATA['list_3'] = arrayNew()>

                <cfloop query="#getList#">
                    <cfset question_item=structNew()> 
                    <cfset question_item['ID'] = QuestionId_int>
                    <cfset question_item['QUESTION'] = Question_vch>
                    <cfset arrayAppend(dataout.DATA['list_#GroupId_int#'], question_item)>

                </cfloop>

                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = 'Get success.'>
                <cfreturn dataout>
            <cfelse>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.MESSAGE = 'Get fail.'>
                <cfreturn dataout>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>                    
                <cfreturn dataout>                             
            </cfcatch>  
        </cftry>
        
    </cffunction>

    <cffunction name="AddUserSecurityQuestion" access="remote" output="true" hint="update user security questions">
        <cfargument name="question_id" type="array" required="yes">
        <cfargument name="answer_txt" type="array" required="yes">

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.DATA = ''>

        <cfset var answer = '' />
        <cfset var i = '' />
        <cfset var deleteOldAnswer  = '' />
        <cfset var updateNewAnswer  = '' />
        <cfset var updateNewAnswer  = '' />
        <cfset var deleteResult = '' />
        <cfset var insertResult = '' />

        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>
            <cftransaction action="begin">
            <cftry>
                <!--- DELETE OLD ANSWER 
                <cfquery name="deleteOldAnswer" datasource="#Session.DBSourceEBM#" result="deleteResult">
                    DELETE 
                    FROM 
                        simpleobjects.secrity_user_answers
                    WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">    
                </cfquery>

                <cfquery name="updateNewAnswer" datasource="#Session.DBSourceEBM#" result="insertResult">
                    INSERT INTO
                        simpleobjects.secrity_user_answers(QuestionId_int,QuestionGroupId_int,Answer_txt,UserId_int)
                    VALUES 
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_1#">,1,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_1, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">),    
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_2#">,2,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_2, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">),
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_3#">,3,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_3, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">)
                </cfquery>
                --->
                <cfloop from="1" to="#arrayLen(arguments.question_id)#" index="i">
                    <cfset answer = Hash(arguments.answer_txt[i], "MD5")>
                    <cfquery name="updateNewAnswer" datasource="#Session.DBSourceEBM#" result="insertResult">
                        INSERT INTO
                            simpleobjects.secrity_user_answers(QuestionId_int,QuestionGroupId_int,Answer_txt,UserId_int)
                        VALUES 
                            (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.question_id[i]#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#i#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#answer#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">)   
                    </cfquery>
                </cfloop>
                <cftransaction action="commit" />
                <cfset dataout.MESSAGE = 'Create successfully'>
                <cfset dataout.RESULT = 'SUCCESS'>

                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                </cfcatch>    
            </cftry>
            </cftransaction>
            <cfreturn dataout>
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
    </cffunction>

    <cffunction name="EditUserSecurityQuestion" access="remote" output="true" hint="update user security questions">
        <cfargument name="question_id" type="array" required="yes">
        <cfargument name="answer_txt" type="array" required="yes">
        <cfargument name="hidden_answer_id" type="array" required="yes">
       
        <cfset var isUpdate  = '' />
        <cfset var answer   = '' />
        <cfset var isUpdate = '' />
        <cfset var i    = '' />
        <cfset var deleteOldAnswer  = '' />
        <cfset var updateNewAnswer  = '' />
        <cfset var deleteResult = '' />
        <cfset var insertResult = '' />

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.DATA = ''>
        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>
            <cftransaction action="begin">
            <cftry>
                <!--- DELETE OLD ANSWER 
                <cfquery name="deleteOldAnswer" datasource="#Session.DBSourceEBM#" result="deleteResult">
                    DELETE 
                    FROM 
                        simpleobjects.secrity_user_answers
                    WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">    
                </cfquery>

                <cfquery name="updateNewAnswer" datasource="#Session.DBSourceEBM#" result="insertResult">
                    INSERT INTO
                        simpleobjects.secrity_user_answers(QuestionId_int,QuestionGroupId_int,Answer_txt,UserId_int)
                    VALUES 
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_1#">,1,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_1, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">),    
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_2#">,2,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_2, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">),
                        (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#question_3#">,3,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Hash(answer_3, "MD5")#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">)
                </cfquery>
                --->
                <cfloop from="1" to="#arrayLen(arguments.question_id)#" index="i">
                    <cfset isUpdate = false>
                    <cfset answer = ''>
                    <cfif arguments.answer_txt[i] NEQ ''> <!--- CHANGE ANSWER --->
                        <cfset answer = Hash(arguments.answer_txt[i], "MD5")>
                        <cfset isUpdate = true>
                    </cfif>

                    <cfif isUpdate>
                         <cfquery name="updateNewAnswer" datasource="#Session.DBSourceEBM#" result="insertResult">
                            UPDATE
                                simpleobjects.secrity_user_answers
                            SET 
                                QuestionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.question_id[i]#">,
                                Answer_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#answer#">
                            WHERE
                                AnswerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.hidden_answer_id[i]#">
                            AND 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">    
                        </cfquery>    
                    </cfif>
                </cfloop>

                <cftransaction action="commit" />
                <cfset dataout.MESSAGE = 'Update success.'>
                <cfset dataout.RESULT = 'SUCCESS'>

                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                </cfcatch>    
            </cftry>
            </cftransaction>
            <cfreturn dataout>
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
    </cffunction>

    <cffunction name="GetUserListQuestion" access="public" output="true" hint="Get user info by session user id">
        <cfargument name="detail_question" type="numeric" required="no" default=0>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.LISTQUESTION = ''>
        <cfset dataout.LISTQUESTIONID = ''>

        <cfset var list_question    = '' />
        <cfset var obj  = '' />
        <cfset var getUserQuestionId    = '' />

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfquery name="getUserQuestionId" datasource="#Session.DBSourceREAD#">
                    SELECT 
                        sua.AnswerId_int,Answer_txt,sua.UserId_int,sqli.QuestionId_int,sqli.Question_vch,sqli.GroupId_int 
                    FROM 
                        simpleobjects.secrity_user_answers sua 
                    JOIN 
                        simpleobjects.security_questions_list sqli 
                    ON 
                        sua.QuestionId_int = sqli.QuestionId_int
                    WHERE    
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>  
                
                <cfif getUserQuestionId.RecordCount GTE 3>

                    <cfset list_question = [] />
                    <cfoutput query="getUserQuestionId">
                        <cfset obj = {
                            "QUESTIONID" = QuestionId_int,
                            "QUESTION" = Question_vch,
                            "ANSWERID" = AnswerId_int,
                            "GROUPID" = GroupId_int,
                            "ANSWER" = Answer_txt,
                         } />
                        <cfset dataout.LISTQUESTIONID = listAppend(dataout.LISTQUESTIONID, QuestionId_int) >
                        <cfset arrayAppend(list_question, obj) />
                    </cfoutput>
                    
                    <cfset dataout.LISTQUESTION = list_question>
                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.MESSAGE = 'SUCCESS'>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = 'User not set security question'>
                    <cfreturn dataout>
                </cfif>
                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>                    
                    <cfreturn dataout>                             
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction>


    <cffunction name="UpdateUserSettingAccount" access="remote" output="true" hint="Get user info by session user id">
        <cfargument name="fname" type="string" required="yes" default="">
        <cfargument name="lname" type="string" required="yes" default="">
        <cfargument name="sms_number" type="string" required="yes" default="">
        <cfargument name="zip_code" type="string" required="no" default="">
        <cfargument name="sendType" required="no" default="" type="string">
        <cfargument name="Address1_vch" required="no" default="" type="string">
        <cfargument name="City_vch" required="no" default="" type="string">
        <cfargument name="State_vch" required="no" default="" type="string">

        <cfset var AddUser  = '' />
        <cfset var updateUserSetting    = '' />
        <cfset var MFAContactType    = '' />
        
        <cfset var UpdateLastUpdate=''>
        <cfset var GetUserInfo=''>
        <cfset var ResetFraund=''>
        <cfset var insertUserLog=''>
        

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>

        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>
            <!--- CHECK US PHONE NUMBER --->
            <cfif NOT isvalid('telephone', sms_number)>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number!'> 
                <cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number!'>
                <cfreturn dataout>
            </cfif>

            <cfif arguments.sendType EQ ''>
                <cfset MFAContactType = 1/>
            <cfelse>
                <cfset MFAContactType = arguments.sendType/>
            </cfif>
            
            <cftransaction action="begin">
            <cftry>
                <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        firstName_vch,
                        lastName_vch,
                        HomePhoneStr_vch,
                        MFAContactString_vch,
                        MFAContactType_ti,
                        Address1_vch,
                        City_vch,
                        PostalCode_vch,
                        State_vch
                    FROM
                        simpleobjects.useraccount                   
                    WHERE UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>
                <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="updateUserSetting">
                    UPDATE simpleobjects.useraccount
                    SET 
                        firstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fname#">,
                        lastName_vch  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lname#">,
                        HomePhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sms_number#">,
                        MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sms_number#">,
                        MFAContactType_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#MFAContactType#">,   
                        Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Address1_vch#">,
                        City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#City_vch#">,
                        PostalCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#zip_code#">,
                        State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#State_vch#">                        
                    WHERE UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>
                <cfif   TRIM(GetUserInfo.firstName_vch) NEQ TRIM(fname)
                    OR  TRIM(GetUserInfo.lastName_vch) NEQ TRIM(lname)
                    OR  TRIM(GetUserInfo.MFAContactString_vch) NEQ TRIM(sms_number)
                    OR  TRIM(GetUserInfo.Address1_vch) NEQ TRIM(Address1_vch)
                    OR  TRIM(GetUserInfo.City_vch) NEQ TRIM(City_vch)
                    OR  TRIM(GetUserInfo.PostalCode_vch) NEQ TRIM(zip_code)
                    OR  TRIM(GetUserInfo.State_vch) NEQ TRIM(State_vch)>     
                    <cfquery name="UpdateLastUpdate" datasource="#Session.DBSourceEBM#">
                        UPDATE simpleobjects.useraccount
                        SET                                                         
                            LastProfileUpdate_dt= NOW()
                        WHERE UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    </cfquery>           
                    <cfquery name="ResetFraund" datasource="#Session.DBSourceEBM#">
                        UPDATE simplebilling.list_cc_review
                        SET 
                            Status_ti=0
                        WHERE 
                            UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    </cfquery>
                    
                    <cfquery name="insertUserLog" datasource="#Session.DBSourceEBM#">
                        insert into simpleobjects.userlogs(
                            UserId_int,
                            ModuleName_vch,
                            Operator_vch,
                            Timestamp_dt
                        )value(
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#" />,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Fraud check reset" />, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Reset All Vault status when user update profile" />,
                            NOW()
                        )
                    
                    </cfquery>
                </cfif>

                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = 'Your Profile Settings have been saved.'>       
                <cfreturn dataout>

                <cftransaction action="commit" />
                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.RESULT = 'FAIL'>
                    <!--- <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail> --->
                    <cfset dataout.MESSAGE = "Unable to update your profile settings at this time. An error occurred.">
                    <cfreturn dataout>                             
                </cfcatch>
            </cftry>
        </cftransaction> 
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateUserSetting" access="remote" output="true" hint="Get user info by session user id">


        <cfargument name="fname" type="string" required="yes" default="">
        <cfargument name="lname" type="string" required="yes" default="">
        <cfargument name="sms_number" type="string" required="yes" default="">
        <!---
        <cfargument name="emailadd" type="string" required="yes" default="">
        --->
        <cfargument name="address" type="string" required="no" default="">
        <cfargument name="zip_code" type="string" required="no" default="">
        <cfargument name="sendType" required="no" default="" type="string">
        <cfargument name="makeDefault" required="no" default="" type="string">
        <cfargument name="SecurityQuestionEnabled" required="yes" default="" type="string">

        <cfargument name="OrganizationId_int" type="string">
        <cfargument name="OrganizationName_vch" type="string" required="no" default="">
        <cfargument name="OrganizationBusinessName_vch" type="string" required="no" default="">
        <cfargument name="OrganizationPhone_vch" type="string" required="no" default="">
        <cfargument name="OrganizationEmail_vch" type="string" required="no" default="">
        <cfargument name="OrganizationCity_vch" type="string" required="no" default="">
        <cfargument name="OrganizationState_vch" type="string" required="no" default="">
        <cfargument name="OrganizationTagline_vch" type="string" required="no" default="">
        <cfargument name="OrganizationHOO_vch" type="string" required="no" default="">
        <cfargument name="OrganizationAdd_vch" type="string" required="no" default="">
        <cfargument name="OrganizationZipcode_vch" type="string" required="no" default="">
        <cfargument name="OrganizationWebsite_vch" type="string" required="no" default="">
        <cfargument name="OrganizationWebsiteTiny_vch" type="string" required="no" default="">
        <cfargument name="OrganizationLogo_vch" type="string" required="no" default="">
        <cfargument name="CustomHelpMessage_vch" type="string" required="no" default="">
        <cfargument name="CustomStopMessage_vch" type="string" required="no" default="">
        <cfargument name="UpdateOrganizationLogo" type="string" required="no" default="0">
        <cfargument name="Old_OrganizationLogo_vch" type="string" required="no" default="0">
        <cfargument name="TimezoneId_int" type="string" required="no" default="0">

        <cfset var sendType = '' />
        <cfset var AddUser  = '' />
        <cfset var AddOrginazation  = '' />
        <cfset var updateUserSetting    = '' />
        <cfset var insert   = '' />
        <cfset var ResUploadS3   = '' />
        <cfset var RemoveLogoS3   = '' />
        
        <cfif arguments.SecurityQuestionEnabled NEQ ''>
            <cfset var SecurityQuestionEnabled = 1>
        <cfelse>
            <cfset var SecurityQuestionEnabled = 0>
        </cfif>

        <cfif arguments.sendType EQ ''>
            <cfset sendType = 1>
            <cfset arguments.makeDefault = ''>
        <cfelse>
            <cfset sendType = arguments.sendType>
            <cfset arguments.makeDefault = 1>
        </cfif>

        

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>

        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>
            <!--- CHECK US PHONE NUMBER --->
          <!---   <cfif NOT isvalid('telephone', sms_number)>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number!'> 
                <cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number!'>
                <cfreturn dataout>
            </cfif> --->
            
            <cfif OrganizationPhone_vch NEQ ''>
            	 <cfif NOT isvalid('telephone', OrganizationPhone_vch)>
	                <cfset dataout.RESULT = 'FAIL'>
	                <cfset dataout.ERRMESSAGE = 'Phone Number is not a valid US telephone number!'> 
	                <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number!'>
	                <cfreturn dataout>
            	</cfif>
            </cfif>

            <cftransaction action="begin">
            <cftry>
                <!--- <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="updateUserSetting">
                    UPDATE simpleobjects.useraccount
                    SET 
                        firstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fname#">,
                        lastName_vch  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lname#">,
                        HomePhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sms_number#">,
                        MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sms_number#">,
                        MFAContactType_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#sendType#">,   
                        SecurityQuestionEnabled_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SecurityQuestionEnabled#">,
                        Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#address#">,
                        PostalCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#zip_code#">

                    WHERE UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>  --->


                <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
                    <cfinvokeargument name="InpEmailString" value="#Session.EmailAddress#">
                    <cfinvokeargument name="InpFirstName" value="#fname#">   
                    <cfinvokeargument name="InpLastName" value="#lname#">                             
                </cfinvoke>
                
                

                <cfif OrganizationId_int GT 0> <!--- UPDATE --->
                 
                        <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#" result="insert">
                            UPDATE simpleobjects.organization
                            SET
                                OrganizationName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationName_vch#">, 
                                OrganizationBusinessName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationBusinessName_vch#">, 
                                OrganizationPhone_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationPhone_vch#">,
                                OrganizationEmail_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationEmail_vch#">,
                                OrganizationTagline_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationTagline_vch#">,
                                OrganizationHOO_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationHOO_vch#">,
                                OrganizationAdd_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationAdd_vch#">,
                                OrganizationZipcode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationZipcode_vch#">,
                                OrganizationWebsite_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationWebsite_vch#">,
                                OrganizationWebsiteTiny_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationWebsiteTiny_vch#">,
                                OrganizationLogo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationLogo_vch#">,
                                CustomHelpMessage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomHelpMessage_vch#">,
                                CustomStopMessage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomStopMessage_vch#">,
                                TimezoneId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TimezoneId_int#">,
                                OrganizationCity_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationCity_vch#">,
                                OrganizationState_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationState_vch#">
                            WHERE 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                            AND   
                                OrganizationId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#OrganizationId_int#">                                      
                        </cfquery>   

                    <cfelse> <!--- INSERT --->
                        <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#" result="insert">
                            INSERT INTO simpleobjects.organization
                                (
                                    OrganizationName_vch,
                                    OrganizationBusinessName_vch,
                                    OrganizationPhone_vch,
                                    OrganizationEmail_vch,
                                    OrganizationTagline_vch,
                                    OrganizationHOO_vch,
                                    OrganizationAdd_vch,
                                    OrganizationZipcode_vch,
                                    OrganizationWebsite_vch,
                                    OrganizationWebsiteTiny_vch,
                                    CustomHelpMessage_vch,
                                    CustomStopMessage_vch,
                                    OrganizationLogo_vch,
                                    UserId_int, 
                                    TimezoneId_int,
                                    OrganizationCity_vch ,
                                    OrganizationState_vch
                                )
                            VALUES 
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationName_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationBusinessName_vch#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationPhone_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationEmail_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationTagline_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationHOO_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationAdd_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationZipcode_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationWebsite_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationWebsiteTiny_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomHelpMessage_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CustomStopMessage_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationLogo_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TimezoneId_int#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationCity_vch#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OrganizationState_vch#">
                                )                                        
                        </cfquery> 

                </cfif>

                <cfset var local_logo_file = ExpandPath(UPLOAD_ORG_LOGO_PATH&OrganizationLogo_vch)/>

                <cfif OrganizationLogo_vch NEQ '' AND UpdateOrganizationLogo EQ 1>
                    <!--- UPLOAD IMAGE TO S3 --->
                    <cfinvoke  component="/session/sire/models/cfc/upload" method="uploadS3" returnvariable="ResUploadS3">
                        <cfinvokeargument name="name" value="#OrganizationLogo_vch#"/>
                        <cfinvokeargument name="local_path" value="#ExpandPath(UPLOAD_ORG_LOGO_PATH)#"/>
                    </cfinvoke>

                    <cfif ResUploadS3.STATUS NEQ '1'> <!--- IF FAIL --->

                         <!--- DELETE LOCAL FILES --->
                        <cfif FileExists(local_logo_file)>
                            <cffile action = "delete" file = "#local_logo_file#">
                        </cfif>    

                        <cftransaction action="rollback" />
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.MESSAGE = 'Unable to upload image to S3 at this time. An error occurred.'>                    
                        <cfset dataout.ERRMESSAGE = 'Unable to upload image to S3 at this time.Please try again.<br/> Status: '&ResUploadS3.STATUS & ".<br/> AMAZONEID : " & ResUploadS3.AMAZONEID> 
                        <cfreturn dataout>    
                    </cfif>
                </cfif>

                <!--- REMOVE OLD IMAGE --->
                <cfif UpdateOrganizationLogo EQ 1 AND Old_OrganizationLogo_vch NEQ ''> 
                    <cfinvoke  component="/session/sire/models/cfc/myaccountinfo" method="RemoveOrgLogo" returnvariable="RemoveLogoS3">
                        <cfinvokeargument name="logo_name" value="#Old_OrganizationLogo_vch#"/>
                    </cfinvoke>
                </cfif>

                <!--- DELETE LOCAL FILES --->
                <cfif FileExists(local_logo_file)>
                    <cffile action = "delete" file = "#local_logo_file#">
                </cfif>    

                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = 'Your Profile Settings have been saved.'>       
                <cfreturn dataout>

                <cftransaction action="commit" />
                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.RESULT = 'FAIL'>
                    <!--- <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail> --->
                    <cfset dataout.MESSAGE = "Unable to update your profile settings at this time. An error occurred.">
                    <cfreturn dataout>                             
                </cfcatch>
            </cftry>
        </cftransaction> 
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction> 

    <cffunction name="UpdateUserCompanyName" access="remote" output="true" hint="Update User Company Only">
        <cfargument name="inpCompanyName" type="string" required="yes" default="">
        <cfset var CheckExitsBlank = '' />      
        <cfset var UpdateCompanyName = '' />   
        <cfset var dataout = {} />      
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cftry>
            <cfquery name="CheckExitsBlank" datasource="#Session.DBSourceREAD#">
                SELECT
                    OrganizationName_vch
                FROM
                    simpleobjects.organization                
                WHERE 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">                                                   
            </cfquery>   
            <cfif CheckExitsBlank.RECORDCOUNT GT 0>
                <cfquery name="UpdateCompanyName" datasource="#Session.DBSourceEBM#">
                    UPDATE simpleobjects.organization
                    SET
                        OrganizationName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCompanyName#">                   
                    WHERE 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">                                                   
                </cfquery>     
            <cfelse>
                <cfquery name="UpdateCompanyName" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simpleobjects.organization
                        (
                            OrganizationName_vch,                            
                            UserId_int                            
                        )
                    VALUES 
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCompanyName#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#"> 
                        )                                        
                </cfquery>  
            </cfif>
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Update Company Name Successfully'>
            <cfcatch type="any">
                <cfset dataout = {} />    
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "Unable to Update Company Name info at this time. An error occurred." />
                <cfset dataout.ERRMESSAGE = "#cfcatch.MESSAGE# - #cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction> 
    <cffunction name="GetUserOrganization" access="remote" output="true" hint="Get User Organization">
        <cfset var orgInfo  = '' />
        <cfset var ListItem = '' />
        <cfset var GetUserOrginazation  = '' />

        <cfset var dataout = {} />        
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cfset dataout.ORGINFO = {
			ORGANIZATIONID_INT = 0, 
			ORGANIZATIONNAME_VCH = '', 
			ORGANIZATIONLOGO_VCH = '', 
			ORGANIZATIONBUSINESSNAME_VCH = '', 
			ORGANIZATIONPHONE_VCH = '', 
			ORGANIZATIONEMAIL_VCH = '', 
			ORGANIZATIONTAGLINE_VCH = '', 
			ORGANIZATIONHOO_VCH = '', 
			ORGANIZATIONADD_VCH = '', 
			ORGANIZATIONZIPCODE_VCH = '',
			ORGANIZATIONWEBSITE_VCH = '', 
			ORGANIZATIONWEBSITETINY_VCH = '', 
			USERID_INT = 0,
			CUSTOMHELPMESSAGE_VCH = '',
			CUSTOMSTOPMESSAGE_VCH = ''
    	}>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfquery datasource="#Session.DBSourceREAD#" name="GetUserOrginazation">
                        SELECT
                            so.OrganizationId_int, 
                            so.OrganizationName_vch, 
                            so.OrganizationLogo_vch, 
                            so.OrganizationBusinessName_vch, 
                            so.OrganizationPhone_vch, 
                            so.OrganizationEmail_vch, 
                            so.OrganizationTagline_vch, 
                            so.OrganizationHOO_vch, 
                            so.OrganizationAdd_vch, 
                            so.OrganizationZipcode_vch,
                            so.OrganizationWebsite_vch, 
                            so.OrganizationWebsiteTiny_vch, 
                            so.UserId_int,
                            so.CustomHelpMessage_vch,
                            so.CustomStopMessage_vch,
                            so.TimezoneId_int,
                            so.OrganizationCity_vch,
                            so.OrganizationState_vch
                        FROM 
                            simpleobjects.Organization so
                        WHERE        
                            so.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>

                <cfif GetUserOrginazation.RecordCount GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfinvoke  component="/public/sire/models/helpers/helper" method="QueryToStruct" returnvariable="orgInfo">
                        <cfinvokeargument name="Query" value="#GetUserOrginazation#"/>
                        <cfinvokeargument name="Row" value="1"/>
                    </cfinvoke>
                    <cfset dataout.ORGINFO = orgInfo>
                <cfelse>
                    <cfset orgInfo= structNew()>
                    <cfloop list="#GetUserOrginazation.ColumnList#" index="ListItem" delimiters=",;">
                        <cfset orgInfo[#ListItem#] = ''>
                    </cfloop>
                    <cfset dataout.ORGINFO = orgInfo>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = "No organization found." />
                    <cfset dataout.ERRMESSAGE = "" />   
                </cfif>

                <cfcatch type="any">
                    <cfset dataout = {} />    
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "Unable to get Organization info at this time. An error occurred." />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.MESSAGE# - #cfcatch.detail#" />
                </cfcatch>
            </cftry>    
            
         <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfset dataout.ERRMESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>

        <cfreturn dataout />
    </cffunction> 

    <cffunction name="RemoveOrgLogo" access="remote" output="true" hint="Remove Orginazation Logo">
        <cfargument name="logo_name" type="string" required="yes" default="">

        <cfset var dataout = {} /> 
        <cfset var removeLogo = '' /> 
        <cfset var logo_file = ExpandPath(UPLOAD_ORG_LOGO_PATH&arguments.logo_name)/>
        <cfset var AddOrginazation = '' />
        <cfset var orgInfo= '' />
        <cftry>
            
            <!---    
            <cfinvoke method="GetUserOrganization" returnvariable="orgInfo"></cfinvoke>

            <cfif orgInfo.RXRESULTCODE EQ 1 AND orgInfo.ORGINFO.ORGANIZATIONID_INT GT 0>
                <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#" result="removeLogo">
                    UPDATE simpleobjects.organization
                    SET
                        OrganizationLogo_vch = ''
                    WHERE 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>     
            </cfif>
            --->
            <!--- DELETE LOCAL FILES --->
            <cfif FileExists(logo_file)>
                <cffile action = "delete" file = "#logo_file#">
            </cfif>    

            <!--- DELETE S3 FILE --->
            <cfset var accessKeyId = "#_S3ACCESSKEYID#"> 
            <cfset var secretAccessKey = "#_S3SECRETACCESSKEY#">
            <cfset var bucketName = '#_S3IMAGEFOLDERPATCH#'>

            <cfset var amazoneS3 = CreateObject("component","session.sire.models.cfc.s3_v1_9").init(accessKeyId,secretAccessKey)>
            <cfset var result = amazoneS3.deleteObject(_S3IMAGEROOTURL,bucketName,arguments.logo_name)>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Your Orginazation logo has been removed." />

        <cfcatch>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.MESSAGE# - #cfcatch.detail#" />
        </cfcatch>    
        </cftry>

        <cfreturn dataout>
    </cffunction>    

	<cffunction name="GetWelcomePreference" access="public" output="false" hint="Get Preference to show welcome page on log in. 1=yes 0=no - default is 1">
                                
        <cfset var dataout = {} />    
        <cfset var GetPrefOption = '' />
		 
		<cftry>      
                    
            <!--- Read from DB --->
            <cfquery name="GetPrefOption" datasource="#Session.DBSourceREAD#">
                SELECT
                	WelcomePref_ti
                FROM	 
                    simpleobjects.useraccount 
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#">
            </cfquery>
      	               
         	<cfif GetPrefOption.RecordCount GT 0>
            
	            <cfset dataout.RXRESULTCODE = "1" />
				<cfset dataout.PREF = "#GetPrefOption.WelcomePref_ti#" />
				<cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
             
            <cfelse>
            
	            <cfset dataout.RXRESULTCODE = "0" />
	            <cfset dataout.PREF = "1" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "Not logged in or current session has timed out." />
            	 
         	</cfif>             	  	                        
                     
        <cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = "-1" />
			<cfset dataout.PREF = "1" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        </cftry>     
		
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="SetWelcomePreference" access="remote" output="false" hint="Set Preference to show welcome page on log in. 1=yes 0=no - default is 1">
        <cfargument name="inpPref" TYPE="numeric" required="yes" />
                        
        <cfset var dataout = {} /> 
        <cfset var SetPrefOption = '' />
		       
        <cftry>      
                        	                                                                
            <!--- Save Local Query to DB --->
            <cfquery name="SetPrefOption" datasource="#Session.DBSourceEBM#">
                UPDATE 
                        simpleobjects.useraccount
                SET 
                        WelcomePref_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpPref#">
                WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserID#">
            </cfquery>                
                                       
            <cfset dataout.RXRESULTCODE = "1" />               
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        </cftry>     
		
        <cfreturn dataout />
    </cffunction>  
    <cffunction name="CheckHelpStopSetup" access="remote" output="false" hint="Check help, stop message is setup in profile">
        <cfargument name="inpUserID" TYPE="numeric" required="no" default="#Session.UserID#" />
        <cfset var dataout = {} />    
        <cfset dataout.RXRESULTCODE = "-1" />        
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />           
        <cfset dataout.NOTSETUP = 0 />           
        <cfset var CheckHelpStop = '' />
		 
		<cftry>      
                    
            <!--- Read from DB --->
            <cfquery name="CheckHelpStop" datasource="#Session.DBSourceREAD#">
                SELECT
                    OrganizationName_vch,
                	OrganizationId_int,
                    CustomHelpMessage_vch,
                    CustomStopMessage_vch,                    
                    UserId_int
                FROM	 
                    simpleobjects.organization
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                AND
                    CustomHelpMessage_vch IS NOT NULL AND LENGTH(CustomHelpMessage_vch) > 9
                AND
                    CustomStopMessage_vch IS NOT NULL AND LENGTH(CustomStopMessage_vch) > 9
                AND
                    OrganizationName_vch IS NOT NULL AND OrganizationName_vch <> ''
            </cfquery>
      	               
         	<cfif CheckHelpStop.RecordCount GT 0>            
	            <cfset dataout.NOTSETUP = 0 />     
            <cfelse>            	
                <cfset dataout.NOTSETUP = 1 />                       	 
         	</cfif>    
             <cfset dataout.RXRESULTCODE = "1" />                 	  	                        
                     
        <cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = "-1" />			
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        </cftry>     
		
        <cfreturn dataout />
    </cffunction>

</cfcomponent>	