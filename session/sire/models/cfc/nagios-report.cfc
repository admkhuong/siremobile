<cfcomponent>

	<cfinclude template="/session/sire/configs/paths.cfm">
    
	<cffunction name="GetNagiosStatus" access="remote" output="true" hint="get status with all host">
		<cfset var content	= '' />
		<cfset var RetVarGetAdminPermission	= '' />
		<cfset var localDate	= '' />
	    <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif>

		<cfset var dataout = {} />
			
	 	<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = arrayLen(_NAGIOSERVERLIST)>
		<cfset dataout["iTotalDisplayRecords"] = arrayLen(_NAGIOSERVERLIST)>
		<cfset dataout["ListServerStatus"] = [] />
		<cfset var serverStatus = {}/>

		<cfset var nagiosUrl = ''/>
		<cfset var checkCallApi = 0/>
		<cfset var hostList = {}/>

		<cfset var nagiosBaseUrl = "http://10.0.0.101/nagios/cgi-bin/statusjson.cgi?query=hostlist&details=true"/>
		<cfset var nagiosStatusResult = ''/>
		<cfset var host = '' />

		<cfhttp url="#nagiosBaseUrl#" method="GET" username="#_NAGIOSUSERNAME#" password="#_NAGIOSPASSWORD#" result="nagiosStatusResult" charset="utf-8">
			<cfhttpparam type="header" name="content-type" value="application/json">
		</cfhttp>

		<cfif nagiosStatusResult.status_code EQ 200>
			<cfif isJson(nagiosStatusResult.filecontent)>
				<cfset content = deserializeJSON(nagiosStatusResult.filecontent)/>
				<cfif content['result']['type_code'] EQ 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset hostList = content['data']['hostlist'] />
					<cfloop collection="#hostList#" index="host">
						<cfif arrayFind(_ARRNAGIOSERVERLIST,hostList[host]['name']) >
							<cfinvoke component="public.sire.models.cfc.common" method="EpochTimeToLocalDate" returnvariable="localDate">
							    <cfinvokeargument name="inpEpoch" value="#hostList[host]['last_check']#">
							</cfinvoke>
							
							<cfset serverStatus = {
									SERVERNAME = hostList[host]['name'],
									STATUS = _NAGIOSSTATUS[hostList[host]['status']],
									LASTCHECK = dateTimeFormat(localDate,'yyyy-mm-dd hh:mm:ss'),
									STATUSINFORMATION = hostList[host]['plugin_output']
							} />	
							<cfset ArrayAppend(dataout["ListServerStatus"], serverStatus) />
						</cfif>
					</cfloop>
				</cfif>
			</cfif> 
		</cfif>

   		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetNagiosDetailHost" access="remote" output="true" hint="get service status with 1 host ">
		<cfargument name="inpServerName" required="true" type="string" hint="host/server name"/>

		<cfset var serviceList	= '' />
		<cfset var service	= '' />
		<cfset var RetVarGetAdminPermission	= '' />
		<cfset var nagiosStatusResult	= '' />

		<cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif>

		<cfset var dataout = {} />
			
	 	<cfset dataout.RXRESULTCODE = 0 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />	    
	    <cfset dataout.SERVICEDETAIL = "" />
	    <cfset var itemServiceDetail = {} />
	    <cfset var serviceDetail = {} />
	    <cfset var content = {} />

	    <cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout["ListServerSetail"] = [] />

		
		<cfset var nagiosBaseUrl = "http://10.0.0.101/nagios/cgi-bin/statusjson.cgi?query=servicelist&details=true&hostname="&arguments.inpServerName/>
		
		<cfhttp url="#nagiosBaseUrl#" method="GET" username="#_NAGIOSUSERNAME#" password="#_NAGIOSPASSWORD#" result="nagiosStatusResult" charset="utf-8">
			<cfhttpparam type="header" name="content-type" value="application/json">
		</cfhttp>

		<cfif nagiosStatusResult.status_code EQ 200>
			<cfif isJson(nagiosStatusResult.filecontent)>
				<cfset content = deserializeJSON(nagiosStatusResult.filecontent)/>
				
				<cfif content['result']['type_code'] EQ 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset serviceList = content['data']['servicelist'][arguments.inpServerName] />
					<cfloop collection="#serviceList#" index="service">
						<cfset itemServiceDetail = serviceList[service]/>
						<cfset dataout["iTotalRecords"] ++>
						<cfset dataout["iTotalDisplayRecords"] ++>

						<cfset serviceDetail = {
								SERVERNAME = itemServiceDetail['description'],
								STATUS = _NAGIOSSTATUS[itemServiceDetail['status']],
								STATUSINFORMATION = itemServiceDetail['plugin_output']
							} />	

						<cfset ArrayAppend(dataout["ListServerSetail"], serviceDetail) />
					</cfloop>
				</cfif>
			</cfif> 
		</cfif>

   		<cfreturn dataout>
	</cffunction>

</cfcomponent>