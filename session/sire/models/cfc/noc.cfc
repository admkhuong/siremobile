<cfcomponent>
	<cfinclude template="/session/sire/configs/noc_constants.cfm"/>

	<cffunction name="GetNOCList" access="remote" output="false" hint="Get all noc list">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfargument name="customFilter" default="">
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset dataout['DATALIST'] = []/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>

		<cfset var NOCList = '' />
		<cfset var filterItem = '' />
		<cfset var item = '' />
		<cfset var rsCount = '' />
		<cfset var description_noc = ''/>
		<cfset var listaddition = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
			<cfcase value="0">
				<cfset orderField = 'Name_vch' />
			</cfcase>
			<cfcase value="1">
				<cfset orderField = 'URL_vch' />
			</cfcase>
			<cfcase value="2">
				<cfset orderField = 'IntervalType_vch' />
			</cfcase>
			<cfcase value="3">
				<cfset orderField = 'Interval_tm' />
			</cfcase>
			<cfcase value="4">
				<cfset orderField = 'StartDate_dt' />
			</cfcase>
			<cfcase value="5">
				<cfset orderField = 'EndDate_dt' />
			</cfcase>
			<cfcase value="6">
				<cfset orderField = 'StartTime_tm' />
			</cfcase>
			<cfcase value="7">
				<cfset orderField = 'EndTime_tm' />
			</cfcase>
			<cfcase value="8">
				<cfset orderField = 'Paused_ti' />
			</cfcase>
			<cfcase value="9">
				<cfset orderField = 'Port_int' />
			</cfcase>
			<cfcase value="10">
				<cfset orderField = 'TimeOut_int' />
			</cfcase>
			<cfcase value="11">
				<cfset orderField = 'Created_dt' />
			</cfcase>
		</cfswitch>
	
		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="NOCList">
				SELECT SQL_CALC_FOUND_ROWS
					PKId_int,
					Name_vch,
					URL_vch,
					IntervalType_vch,
					StartDate_dt,
					StartTime_tm,
					EndDate_dt,
					EndTime_tm,
					Interval_tm,
					Paused_ti,
					Port_int,
					TimeOut_int,
					LaunchedTime_bi,
					Created_dt,
					QueryString_vch
				FROM
					simpleobjects.sire_noc_settings
				WHERE
					Active_ti = 1
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='cf_sql_varchar' value='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Created_dt DESC
				</cfif>
				LIMIT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#"> OFFSET <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
			</cfquery>

			<cfif NOCList.RecordCount GT 0>
				<cfloop query="NOCList">
					<cftry>
						<cfif NOCList.QueryString_vch neq "">
							<cfset listaddition = deserializeJSON(NOCList.QueryString_vch)>
							<cfset description_noc = listaddition['Description']/>
						<cfelse>
							<cfset description_noc =""/>
						</cfif>
					<cfcatch type="any">
						<cfset description_noc =""/>
						<cfset dataout.MESSAGE = cfcatch.MESSAGE />
						<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
						<cfset dataout.TYPE = cfcatch.TYPE />
					</cfcatch>
					</cftry>
					<cfset item = {
						"PKId_int": NOCList.PKId_int,
						"Name_vch": NOCList.Name_vch,
						"URL_vch": NOCList.URL_vch,
						"IntervalType_vch": NOCList.IntervalType_vch,
						"StartDate_dt": DateFormat(NOCList.StartDate_dt, 'dd/mm/yyyy'),
						"StartTime_tm": TimeFormat(NOCList.StartTime_tm, 'HH:MM:SS'),
						"EndDate_dt": DateFormat(NOCList.EndDate_dt, 'dd/mm/yyyy'),
						"EndTime_tm": TimeFormat(NOCList.EndTime_tm, 'HH:MM:SS'),
						"Interval_tm": TimeFormat(NOCList.Interval_tm, 'HH:MM:SS'),
						"Paused_ti": int(NOCList.Paused_ti) EQ 1 ? "Yes" : "No",
						"Port_int": NOCList.Port_int,
						"TimeOut_int": NOCList.TimeOut_int,
						"LaunchedTime_bi": NOCList.LaunchedTime_bi,
						"Created_dt": DateFormat(NOCList.Created_dt, 'dd/mm/yyyy'),
						"Description_vch": description_noc
					} />
					<cfset arrayAppend(dataout["DATALIST"],item) />
				</cfloop>

				<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="CreateNewNOC" access="remote" output="false" hint="Create new NOC">
		<cfargument name="inpName" required="true" hint="NOC Name"/>
		<cfargument name="inpURL" required="true" hint="NOC URL"/>
		<cfargument name="inpIntervalType" required="true" hint="NOC interval type"/>
		<cfargument name="inpStartDate" required="true" hint="NOC start date"/>
		<cfargument name="inpStartTime" required="true" hint="NOC start time"/>
		<cfargument name="inpEndDate" required="false" hint="NOC end date"/>
		<cfargument name="inpEndTime" required="false" hint="NOC end time"/>
		<cfargument name="inpPaused" required="true" hint="NOC pause status"/>
		<cfargument name="inpInterval" required="false" hint="NOC interval"/>
		<cfargument name="inpPort" required="true" hint="NOC port"/>
		<cfargument name="inpTimeOut" required="true" hint="NOC time out"/>
		<cfargument name="inptotalfail" required="true" hint=" Total fail"/>
		<cfargument name="inpavgconnecttime" required="true" hint="Avg connect time"/>
		<cfargument name="inpdescription" required="true" hint="Noc description"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var ReCreateNewNOC = '' />

		<cfset var inpaddition = {
				"TotalFail": inptotalfail,
				"Avgconnecttime": inpavgconnecttime,
				"Description": inpdescription
		}/>
		<cfset var inpadditionJSON = SerializeJSON(inpaddition) />
	
		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="ReCreateNewNOC">
				INSERT INTO
					simpleobjects.sire_noc_settings
				(
					Name_vch,
					URL_vch,
					IntervalType_vch,
					StartDate_dt,
					StartTime_tm,
					Interval_tm,
					EndDate_dt,
					EndTime_tm,
					Paused_ti,
					Port_int,
					TimeOut_int,
					UserId_int,
					QueryString_vch
				)
				VALUES
				(
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpName#"/>,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpURL#"/>,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpIntervalType#"/>,
					<cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpStartDate#"/>,
					<cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpStartTime#"/>,
					<cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpInterval#"/>,
					<cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpEndDate#"/>,
					<cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpEndTime#"/>,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPaused#"/>,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPort#"/>,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTimeOut#"/>,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#inpadditionJSON#"/>
				)
			</cfquery>

			<cfif ReCreateNewNOC.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Create new NOC successfully"/>
			</cfif>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetOneNOCById" access="remote" output="false" hint="Get all info of one ONC">
		<cfargument name="inpNOCId" required="true" hint="NOC ID"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.NOC = {}/>

		<cfset var GetNOC = {} />
		<cfset var listaddition = ""/>
	
		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="GetNOC">
				SELECT
					PKId_int,
					Name_vch,
					URL_vch,
					IntervalType_vch,
					StartDate_dt,
					StartTime_tm,
					EndDate_dt,
					EndTime_tm,
					Interval_tm,
					Paused_ti,
					Port_int,
					TimeOut_int,
					LaunchedTime_bi,
					Created_dt,
					QueryString_vch
				FROM
					simpleobjects.sire_noc_settings
				WHERE
					PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpNOCId#"/>
				LIMIT
					1
			</cfquery>

			<cfif GetNOC.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get NOC successfully"/>
				<cfif GetNOC.QueryString_vch neq "">
					<cfset listaddition = deserializeJSON(GetNOC.QueryString_vch)>
					<cfset dataout.NOC.TOTALFAIL = listaddition['TotalFail']/>
					<cfset dataout.NOC.AVGCONNECTTIME = listaddition['Avgconnecttime']/>
					<cftry>
						<cfset dataout.NOC.DESCRIPTION = listaddition['Description']/>
					<cfcatch type="any">
						<cfset dataout.NOC.DESCRIPTION = ""/>
					</cfcatch>
					</cftry>
				<cfelse>
					<cfset dataout.NOC.TOTALFAIL = ""/>
					<cfset dataout.NOC.AVGCONNECTTIME = ""/>
					<cfset dataout.NOC.DESCRIPTION =""/>
				</cfif>
				<cfset dataout.NOC.PKID = GetNOC.PKId_int/>
				<cfset dataout.NOC.NAME = GetNOC.Name_vch/>
				<cfset dataout.NOC.URL = GetNOC.URL_vch/>
				<cfset dataout.NOC.INTERVALTYPE = arrayFindNoCase(_NOC_INTERVAL_TYPE, GetNOC.IntervalType_vch)/>
				<cfset dataout.NOC.STARTDATE = DateFormat(GetNOC.StartDate_dt, "dd-mm-yyyy")/>
				<cfset dataout.NOC.STARTTIME = TimeFormat(GetNOC.StartTime_tm, "HH:MM:SS")/>
				<cfset dataout.NOC.ENDDATE = GetNOC.EndDate_dt EQ "" ? "" : DateFormat(GetNOC.EndDate_dt, "dd-mm-yyyy")/>
				<cfset dataout.NOC.ENDTIME = GetNOC.EndTime_tm EQ "" ? "" : TimeFormat(GetNOC.EndTime_tm, "HH:MM:SS")/>
				<cfset dataout.NOC.INTERVAL = GetNOC.Interval_tm EQ "" ? "" : TimeFormat(GetNOC.Interval_tm, "HH:MM:SS")/>
				<cfset dataout.NOC.PAUSED = GetNOC.Paused_ti/>
				<cfset dataout.NOC.PORT = GetNOC.Port_int/>
				<cfset dataout.NOC.TIMEOUT = GetNOC.TimeOut_int/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetOneNOCMBLox" access="remote" hint="Get info of NOC MBLox">

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.NOC = {}/>

		<cfset var GetNOCMBLox = {} />
		<cfset var listaddition = ""/>
	
		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="GetNOCMBLox">
				SELECT
					PKId_int,
					Name_vch,
					URL_vch,
					IntervalType_vch,
					StartDate_dt,
					StartTime_tm,
					EndDate_dt,
					EndTime_tm,
					Interval_tm,
					Paused_ti,
					Port_int,
					TimeOut_int,
					LaunchedTime_bi,
					Created_dt,
					QueryString_vch
				FROM
					simpleobjects.sire_noc_settings
				WHERE
					Name_vch = 'NOC MBLox Status'
				LIMIT
					1
			</cfquery>

			<cfif GetNOC.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get NOC successfully"/>
				<cfif GetNOC.QueryString_vch neq "">
					<cfset listaddition = deserializeJSON(GetNOC.QueryString_vch)>
					<cfset dataout.NOC.TOTALFAIL = listaddition['TotalFail']/>
					<cfset dataout.NOC.AVGCONNECTTIME = listaddition['Avgconnecttime']/>
				<cfelse>
					<cfset dataout.NOC.TOTALFAIL = ""/>
					<cfset dataout.NOC.AVGCONNECTTIME = ""/>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="EditNOCById" access="remote" output="false" hint="Edit NOC">
		<cfargument name="inpNOCId" required="true" hint="NOC ID"/>
		<cfargument name="inpName" required="true" hint="NOC Name"/>
		<cfargument name="inpURL" required="true" hint="NOC URL"/>
		<cfargument name="inpIntervalType" required="true" hint="NOC interval type"/>
		<cfargument name="inpStartDate" required="true" hint="NOC start date"/>
		<cfargument name="inpStartTime" required="true" hint="NOC start time"/>
		<cfargument name="inpEndDate" required="false" hint="NOC end date"/>
		<cfargument name="inpEndTime" required="false" hint="NOC end time"/>
		<cfargument name="inpPaused" required="true" hint="NOC pause status"/>
		<cfargument name="inpInterval" required="false" hint="NOC interval"/>
		<cfargument name="inpPort" required="true" hint="NOC port"/>
		<cfargument name="inpTimeOut" required="true" hint="NOC time out"/>
		<cfargument name="inptotalfail" required="true" hint=" Total fail"/>
		<cfargument name="inpavgconnecttime" required="true" hint="Avg connect time"/>
		<cfargument name="inpdescription" required="true" hint="NOC description"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var ReEditNOC = '' />

		<cfset var inpaddition = {
				"TotalFail": inptotalfail,
				"Avgconnecttime":inpavgconnecttime,
				"Description": inpdescription
		}/>
		<cfset var inpadditionJSON = SerializeJSON(inpaddition) />
	
		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="ReEditNOC">
				UPDATE
					simpleobjects.sire_noc_settings
				SET
					Name_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpName#"/>,
					URL_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpURL#"/>,
					IntervalType_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpIntervalType#"/>,
					StartDate_dt = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpStartDate#"/>,
					StartTime_tm = <cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpStartTime#"/>,
					Interval_tm = <cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpInterval#"/>,
					EndDate_dt = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpEndDate#"/>,
					EndTime_tm = <cfqueryparam cfsqltype="cf_sql_time" value="#arguments.inpEndTime#"/>,
					Paused_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPaused#"/>,
					Port_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPort#"/>,
					TimeOut_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpTimeOut#"/>,
					QueryString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#inpadditionJSON#"/>
				WHERE
					PKId_int = <cfqueryparam cfsqltype="integer" value="#arguments.inpNOCId#"/>
				LIMIT
					1
			</cfquery>

			<cfif ReEditNOC.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Edit NOC successfully"/>
			</cfif>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="DeleteNOCById" access="remote" output="false" hint="Delete a NOC">
		<cfargument name="inpNOCId" required="true" hint="NOC Id"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var DeleteNOC = '' />
	
		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="DeleteNOC">
				UPDATE
					simpleobjects.sire_noc_settings
				SET
					Active_ti = 0
				WHERE
					PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpNOCId#"/>
				LIMIT
					1
			</cfquery>

			<cfif DeleteNOC.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Delete NOC successfully."/>
			</cfif>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="BulkNOCProcess" access="remote" output="false" hint="Process multiple NOC">
		<cfargument name="inpNOCIds" required="true" hint="List of noc ids"/>
		<cfargument name="inpAction" required="true" hint="What to do with NOC list"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var NOCIds = "" />
		<cfset var RetVarExecuteNOCs = '' />
		<cfset var ExecuteAction = '' />
	
		<cftry>
			<cfset NOCIds = REReplace(arguments.inpNOCIds,"[^0-9\,]","","ALL")/>

			<cfswitch expression="#arguments.inpAction#">
				<cfcase value="delete">
					<cfquery datasource="#session.DBSourceEBM#" result="ExecuteAction">
						UPDATE
							simpleobjects.sire_noc_settings
						SET
							Active_ti = 0
						WHERE
							PKId_int IN (#NOCIds#)
					</cfquery>

					<cfif ExecuteAction.RecordCount GT 0>
						<cfset dataout.RXRESULTCODE = 1/>
						<cfset dataout.MESSAGE = "Delete NOCs successfully!"/>
					</cfif>
				</cfcase>

				<cfcase value="pause">
					<cfquery datasource="#session.DBSourceEBM#" result="ExecuteAction">
						UPDATE
							simpleobjects.sire_noc_settings
						SET
							Paused_ti = 1
						WHERE
							PKId_int IN (#NOCIds#)
					</cfquery>

					<cfif ExecuteAction.RecordCount GT 0>
						<cfset dataout.RXRESULTCODE = 1/>
						<cfset dataout.MESSAGE = "Pause NOCs successfully!"/>
					</cfif>
				</cfcase>

				<cfcase value="resume">
					<cfquery datasource="#session.DBSourceEBM#" result="ExecuteAction">
						UPDATE
							simpleobjects.sire_noc_settings
						SET
							Paused_ti = 0
						WHERE
							PKId_int IN (#NOCIds#)
					</cfquery>

					<cfif ExecuteAction.RecordCount GT 0>
						<cfset dataout.RXRESULTCODE = 1/>
						<cfset dataout.MESSAGE = "Resume NOCs successfully!"/>
					</cfif>
				</cfcase>

				<cfcase value="execute">
					<cfinvoke method="ExecuteNOCs" returnvariable="RetVarExecuteNOCs">
						<cfinvokeargument name="inpNOCIds" value="#NOCIds#"/>
					</cfinvoke>

					<cfset dataout.RXRESULTCODE = RetVarExecuteNOCs.RXRESULTCODE/>
					<cfset dataout.MESSAGE = RetVarExecuteNOCs.MESSAGE/>
				</cfcase>
			</cfswitch>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="ExecuteNOCs" access="private" output="false" hint="Execute one or more NOCs">
		<cfargument name="inpNOCIds" required="true" hint="NOC id list"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var GetNOCs = '' />
		<cfset var NOCIds = '' />
		<cfset var index = 1/>
		<cfset var executeNoc = '' />
		<cfset var threadList = ''>

		<cftry>
			<cfset NOCIds = REReplace(arguments.inpNOCIds,"[^0-9\,]","","ALL")/>

			<cfquery datasource="#session.DBSourceREAD#" name="GetNOCs">
				SELECT
					PKId_int,
					URL_vch,
					Port_int,
					TimeOut_int
				FROM
					simpleobjects.sire_noc_settings
				WHERE
					PKId_int IN (#NOCIds#)
			</cfquery>

			<cfif GetNOCs.RecordCount GT 0>
				<cfloop query="GetNOCs">
					<cfthread action="run" NOCURL="#GetNOCs.URL_vch#" NOCPort="#GetNOCs.Port_int#" NOCTimeout="#GetNOCs.TimeOut_int#" name="NOCThreadExecute#index#">
						<cfhttp url="#NOCURL#" port="#NOCPort#" timeout="#NOCTimeout#" method="get"></cfhttp>
					</cfthread>

					<cfset listAppend(threadList,"NOCThreadExecute#index#")>
					<cfset index++/>
				</cfloop>

				<cfthread action="join" timeout="86400000" name="#threadList#"></cfthread>

				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "NOCs executed!"/>
			</cfif>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetNOCReportsList" access="remote" output="false" hint="Get all noc reports">
		<cfargument name="inpNOCId" type="numeric" required="no" default="0"/>
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfargument name="customFilter" default="">
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset dataout['DATALIST'] = []/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>

		<cfset var NOCList = '' />
		<cfset var filterItem = '' />
		<cfset var item = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
			<cfcase value="0">
				<cfset orderField = 'PKId_int' />
			</cfcase>
			<cfcase value="1">
				<cfset orderField = 'NOCName_vch' />
			</cfcase>
			<cfcase value="2">
				<cfset orderField = 'Result_int' />
			</cfcase>
			<cfcase value="3">
				<cfset orderField = 'RunTimeMS_int' />
			</cfcase>
			<cfcase value="4">
				<cfset orderField = 'Details_vch' />
			</cfcase>
		</cfswitch>
	
		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="NOCList">
				SELECT SQL_CALC_FOUND_ROWS
					PKId_int,
					TRUNCATE(RunTimeMS_int/1000, 2) AS RunTimeS,
					Result_int,
					Details_vch,
					NOCName_vch,
					NOCId_int,
					Created_dt
				FROM
					simplequeue.sire_noc_checks_log
				WHERE
					1
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.NAME EQ " RunTimeMS_int ">
						AND (RunTimeMS_int/1000) #filterItem.OPERATOR# <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='cf_sql_varchar' value='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				<cfif arguments.inpNOCId GT 0>
					AND
						NOCId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpNOCId#"/>
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Created_dt DESC
				</cfif>
				LIMIT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#"> OFFSET <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
			</cfquery>

			<cfif NOCList.RecordCount GT 0>
				<cfloop query="NOCList">
					<cfset item = {
						"PKId_int": NOCList.PKId_int,
						"RunTimeS": NOCList.RunTimeS,
						"Result_int": NOCList.Result_int,
						"Details_vch": NOCList.Details_vch,
						"NOCName_vch": NOCList.NOCName_vch,
						"Created_dt": "#DateFormat(NOCList.Created_dt, "dd/mm/yyyy")# #TimeFormat(NOCList.Created_dt, "HH:MM:SS")#",
						"NOCId_int": NOCList.NOCId_int
					} />
					<cfset arrayAppend(dataout["DATALIST"],item) />
				</cfloop>

				<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="ExtraReport" access="remote" output="false" hint="Get extra report for noc">
		<cfargument name="inpStartDate" type="string" required="true" default=""/>
		<cfargument name="inpEndDate" type="string" required="true" default=""/>
		<cfargument name="inpType" required="false" hint="For further future reports"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset dataout["ChartList"] = [] />
		<cfset dataout['DATALIST'] = []/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>

		<cfset var strGroupType = "day" />

		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


		<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
		<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
		<cfset var dateOffset  = DateDiff("d",startDate, endDate) />

		<cfset var method = "CountChatSessionByDate"/>
	
		<cftry>
			<cfinvoke method="GetGroupString" returnvariable="strGroupType">
				<cfinvokeargument name="inpOffsetDate" value="#dateOffset#">
			</cfinvoke>

			<cfswitch expression="#strGroupType#">
				<cfcase value="day"> 
					
					<cfset var currDateOut = startDate >
					<cfset var day = '' />
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(1,0,0,0)#">
						<cfset var valueItem = {
							"Time" 	= "#DateFormat(currDateOut, "yyyy-mm-dd")#",
							"Value" = 0,
							"format" = "#strGroupType#"
						}/>
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#currDateOut#"/>
						</cfinvoke>

						<cfset dataout["ChartList"].append(valueItem) />
						<cfset currDateOut = currDateOut.add('d', 1) >
					</cfloop>

				</cfcase>
				<cfcase value="week"> 

					<!--- Create key list for data --->
					<cfset var dateList = [] />
					<cfset var currDate = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(7,0,0,0)#">
						<cfif DateDiff("d",currDate, endDate) GT 0 AND currDate LT endDate>
							<cfset  dateList.append(currDate) />
							<cfset currDate = currDate.add('d', 7) >
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />
					<cfset var eachDay = '' />
					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
						</cfinvoke>
						<cfset dataout["ChartList"].append(valueItem) />
					</cfloop>
				</cfcase> 
				<cfcase value="month"> 
					<cfset var dateList = [] />
					<cfset var currDate = startDate />
					<cfset dateList.append(currDate) />
					
					<cfloop condition="DateCompare(currDate, endDate) LT 0)">
						<cfset currDate = currDate.add('m', 1) /> 
						<cfif DateCompare(currDate, endDate) LT 0>
							<cfset dateList.append(CreateDate(currDate.year(), currDate.month(), 1)) />
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />

					
					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="#method#" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
						</cfinvoke>
						<cfset dataout["ChartList"].append(valueItem) />
					</cfloop>
				</cfcase>
			</cfswitch>

			<cfset dataout.RXRESULTCODE = 1/>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="CountChatSessionByDate" access="private" hint="Count Chat Session By Date">
		<cfargument name="inpDate" type="string" required="true"/>

		<cfset var value = 0 />
		<cfset var getReportData =  '' />

		<cftry>
			<cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					CountResult_int AS Total
				FROM
					simplequeue.sire_noc_checks_log
				WHERE
					NOCId_int = #_COUNT_CHAT_SESSION_NOC_ID#
				AND
					DATE(Created_dt) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(arguments.inpDate, "yyyy-mm-dd")#"/>
				LIMIT 1
			</cfquery>

			<cfif getReportData.RECORDCOUNT GT 0>
				<cfset value = getReportData.Total/>
			</cfif>
			<cfcatch>
				<cfreturn value/>
			</cfcatch>
		</cftry>

		<cfreturn value />
	</cffunction>

	<cffunction name="GetGroupString" access="private" hint="Get Group String">
		<cfargument name="inpOffsetDate" type="numeric" required="true">

		<cfset var type = "day">

		<cfif arguments.inpOffsetDate GTE 14>
			<cfset type = "week">
		</cfif>
			
		<cfif arguments.inpOffsetDate GTE 89>
			<cfset type = "month">
		</cfif>

		<cfreturn type />
	</cffunction>

	<cffunction name="GetCountSessionNOCReport" access="remote" output="false" hint="Get all noc reports">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfargument name="customFilter" default=""/>

		<cfargument name="inpStartDate" type="string" required="true" default=""/>
		<cfargument name="inpEndDate" type="string" required="true" default=""/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset dataout['DATALIST'] = []/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout["TOTALSESSION"] = 0/>

		<cfset var NOCList = '' />
		<cfset var filterItem = '' />
		<cfset var item = '' />
		<cfset var rsCount = '' />
		<cfset var totalSession = '' />

		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
		<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


		<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
		<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
			<cfcase value="0">
				<cfset orderField = 'Created_dt' />
			</cfcase>
			<cfcase value="1">
				<cfset orderField = 'RunTimeMS_int' />
			</cfcase>
			<cfcase value="2">
				<cfset orderField = 'CountResult_int' />
			</cfcase>
			<cfcase value="3">
				<cfset orderField = 'RunTimeMS_int' />
			</cfcase>
			<cfcase value="4">
				<cfset orderField = 'Details_vch' />
			</cfcase>
		</cfswitch>

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="NOCList">
				SELECT SQL_CALC_FOUND_ROWS
					Created_dt,
					TRUNCATE(RunTimeMS_int/1000, 2) AS RunTimeS,
					CountResult_int
				FROM
					simplequeue.sire_noc_checks_log
				WHERE
					NOCId_int = #_COUNT_CHAT_SESSION_NOC_ID#
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.NAME EQ " RunTimeMS_int ">
						AND (RunTimeMS_int/1000) #filterItem.OPERATOR# <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='#filterItem.TYPE#' value='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <cfqueryparam cfsqltype='cf_sql_varchar' value='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				AND
					DATE(Created_dt) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(startDate, 'yyyy-mm-dd')#"/>
				AND
					DATE(Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(endDate, 'yyyy-mm-dd')#"/>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						Created_dt DESC
				</cfif>
				LIMIT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#"> OFFSET <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
			</cfquery>

			<cfquery name="totalSession" datasource="#session.DBSourceREAD#">
				SELECT
					SUM(CountResult_int) AS TotalSession
				FROM
					simplequeue.sire_noc_checks_log
				WHERE
					NOCId_int = #_COUNT_CHAT_SESSION_NOC_ID#
				AND
					DATE(Created_dt) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(startDate, 'yyyy-mm-dd')#"/>
				AND
					DATE(Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(endDate, 'yyyy-mm-dd')#"/>
			</cfquery>

			<cfif NOCList.RecordCount GT 0>
				<cfloop query="NOCList">
					<cfset item = {
						"RunTimeS": NOCList.RunTimeS,
						"Created_dt": "#DateFormat(NOCList.Created_dt, "dd/mm/yyyy")# #TimeFormat(NOCList.Created_dt, "HH:MM:SS")#",
						"CountResult_int": NOCList.CountResult_int
					} />
					<cfset arrayAppend(dataout["DATALIST"],item) />
				</cfloop>

				<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>

				<cfset dataout["TOTALSESSION"] = totalSession.TotalSession/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>