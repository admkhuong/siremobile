<cfcomponent>
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfset LOCAL_LOCALE = "English (US)">
    
    <cffunction access="remote" name="checkContactStringOptout" output="true" hint="check if contactString optout or not. return 1 = optout, 2 = optin">
    	<cfargument name="inpContactString" require="true"/>
    	<cfargument name="inpShortCode" required="true"/>

    	<cfset var dataout = {} />    
    	<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />           
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
		<cfset var ContactString	= '' />
		<cfset var getOptinData	= '' />
		<cfset ContactString = REReplaceNoCase(arguments.inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

        <cfif len(arguments.inpContactString) GT 0>
        	<cfquery name="getOptinData" datasource="#Session.DBSourceEBM#" >
        		SELECT 
        			optIn_dt,optOut_dt 
        		FROM 
        			simplelists.optinout 
        		WHERE 
        			ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ContactString#"/>
        		AND 
        			ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
        		ORDER BY 
        			OptId_int desc
        		LIMIT 1
        	</cfquery>

        	<cfif getOptinData.RECORDCOUNT GT 0>
        		<cfif isDate(getOptinData.optOut_dt)>
        			<cfset dataout.RXRESULTCODE = "1" /> 
        		<Cfelse>
        			<cfset dataout.RXRESULTCODE = "2" /> 	
        		</cfif>
        	<Cfelse>
        		<cfset dataout.RXRESULTCODE = "0" /> 	
        	</cfif>
        </cfif>

        <cfreturn dataout />

    </cffunction>

</cfcomponent>