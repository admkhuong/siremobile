<cfcomponent>
	<cfinclude template="/session/administration/constants/userConstants.cfm">
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/public/paths.cfm" >
    
    <cfinclude template="/session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="/session/cfc/csc/inc_ext_csc.cfm">


	<cfset ACCESSDENIEDMESSAGE ="You don't have permission to access!">
	<!---check permission--->
	<cfset permissionObject = CreateObject("component", "#Session.SessionCFCPath#.administrator.permission")>
	<cfset smsAddSCPermission = permissionObject.havePermission(Short_Code_Add_Title)>
	<cfset smsRemoveSCPermission = permissionObject.havePermission(Short_Code_Remove_Title)>
	<cfset smsActionKeywordPermission = permissionObject.havePermission(Keyword_Action_Title)>

	<cffunction name="GetKeywordFromBatchId" access="remote" output="true" hint="Get Keyword From BatchId">
		<cfargument name="inpBatchId" TYPE="numeric"/>
		
		<cfset var GetKeyword = '' />
        <cfset var GetShortCode = '' />
		
		<cftry>	
			<cfset var LOCALOUTPUT = {} />
			<!--- Get short code data by id --->
			<cfquery name="GetKeyword" datasource="#Session.DBSourceREAD#">                                                                                                   
	             SELECT                        	
	                KeywordId_int,
					Keyword_vch,
					Response_vch,
					ToolTip_vch,
                    BatchId_bi,
                    Survey_int,
                    ShortCodeId_int

	             FROM
	                sms.keyword
				 WHERE
				 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	        </cfquery>  
	        <cfif GetKeyword.ShortCodeId_int EQ "">
				<cfset GetKeyword.ShortCodeId_int = 0>				
			</cfif>
            <cfif GetKeyword.RecordCount GT 0>	            
	            <cfquery name="GetShortCode" datasource="#Session.DBSourceREAD#">                                                                                                   
		             SELECT                        	
		                ShortCodeId_int,
	                    ShortCode_vch
		             FROM
		                sms.shortcode
					 WHERE
					 	ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetKeyword.ShortCodeId_int#">
		        </cfquery>              
	            
				<cfif GetKeyword.recordCount GT 0>
					<cfset LOCALOUTPUT.RXRESULTCODE = 1 /> 
					<cfset LOCALOUTPUT.KEYWORDID = GetKeyword.KeywordId_int/>				
					<cfset LOCALOUTPUT.KEYWORD = GetKeyword.Keyword_vch/>
					<cfset LOCALOUTPUT.RESPONSE = GetKeyword.Response_vch/>
	                <cfset LOCALOUTPUT.SHORTCODE = GetShortCode.ShortCode_vch/>
					<cfset LOCALOUTPUT.TOOLTIP = GetKeyword.ToolTip_vch/>
	                <cfset LOCALOUTPUT.BATCHID = GetKeyword.BatchId_bi/>
	                <cfset LOCALOUTPUT.ISSURVEY = GetKeyword.Survey_int/>
	                
	                
					<cfset LOCALOUTPUT.MESSAGE = ""/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
				<cfelse>
	                <cfset LOCALOUTPUT.KEYWORDID = ""/>				
					<cfset LOCALOUTPUT.KEYWORD = ""/>
					<cfset LOCALOUTPUT.RESPONSE = ""/>
	                <cfset LOCALOUTPUT.SHORTCODE = ""/>
					<cfset LOCALOUTPUT.TOOLTIP = ""/>
	                <cfset LOCALOUTPUT.BATCHID = 0/>
	                <cfset LOCALOUTPUT.ISSURVEY = 0/>
					<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
					<cfset LOCALOUTPUT.MESSAGE = "Keyword not found"/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
				</cfif>
			<cfelse>
	                <cfset LOCALOUTPUT.KEYWORDID = ""/>				
					<cfset LOCALOUTPUT.KEYWORD = ""/>
					<cfset LOCALOUTPUT.RESPONSE = ""/>
	                <cfset LOCALOUTPUT.SHORTCODE = ""/>
					<cfset LOCALOUTPUT.TOOLTIP = ""/>
	                <cfset LOCALOUTPUT.BATCHID = 0/>
	                <cfset LOCALOUTPUT.ISSURVEY = 0/>
					<cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
					<cfset LOCALOUTPUT.MESSAGE = "Keyword not found"/>
					<cfset LOCALOUTPUT.ERRMESSAGE = ""/>
					<cfset LOCALOUTPUT.OPTINMESSAGE = ""/>
	                <cfset LOCALOUTPUT.SECONDTIMEOPTINMESSAGE = ""/>
	                <cfset LOCALOUTPUT.UNSUBSCRIBEDATAPOLICY = ""/>
			</cfif>
	    <cfcatch TYPE="any">
		    <cfset LOCALOUTPUT.RXRESULTCODE = -1 /> 
            <cfset LOCALOUTPUT.KEYWORDID = ""/>				
			<cfset LOCALOUTPUT.KEYWORD = ""/>
            <cfset LOCALOUTPUT.RESPONSE = ""/>
            <cfset LOCALOUTPUT.SHORTCODE = ""/>
            <cfset LOCALOUTPUT.TOOLTIP = ""/>
            <cfset LOCALOUTPUT.BATCHID = 0/>
            <cfset LOCALOUTPUT.ISSURVEY = 0/>
			<cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
			<cfset LOCALOUTPUT.OPTINMESSAGE = ""/>
	        <cfset LOCALOUTPUT.SECONDTIMEOPTINMESSAGE = ""/>
	        <cfset LOCALOUTPUT.UNSUBSCRIBEDATAPOLICY = ""/>
			
			<cfreturn LOCALOUTPUT>
		</cfcatch>   
		</cftry>
		<cfreturn LOCALOUTPUT />
	</cffunction>
	
	<cffunction name="UpdateKeyword" access="remote" output="true" hint="Update Keyword">
		<cfargument name="INPBATCHDESC" required="yes" default="">
		<cfargument name="inpBatchId" TYPE="numeric">
		<cfargument name="KeywordId" TYPE="numeric">
		<cfargument name="ShortCode" TYPE="string" required="no" default="">
		<cfargument name="Keyword" required="no" default="">
		
        <cfargument name="IsSurvey" required="false" default="0">
        <cfargument name="CustomHELP" required="no" default="">
        <cfargument name="CustomSTOP" required="no" default="">
        
        <!---CP1--->
        <cfargument name="OptInMessage" TYPE="string" required="yes" default="">
		<cfargument name="OptInMessageId" TYPE="numeric" required="yes" default="0">
		<!---CP5--->
		<cfargument name="ConfirmMessage" TYPE="string" required="yes" default="">
		<cfargument name="ConfirmMessageId" TYPE="numeric" required="yes" default="">
		<cfargument name="GroupId" TYPE="numeric" required="yes" default="">
		
		<cfargument name="RequestMore1" TYPE="string" default="">
		<cfargument name="SCDF1" TYPE="string" default="">
		<cfargument name="RequestMore2" TYPE="string" default="">
		<cfargument name="SCDF2" TYPE="string" default="">
		<cfargument name="RequestMore3" TYPE="string" default="">
		<cfargument name="SCDF3" TYPE="string" default="">
		<cfargument name="RequestMore4" TYPE="string" default="">
		<cfargument name="SCDF4" TYPE="string" default="">
		
		<cfset var dataout = '' />
		<cfset var CountKeyword = '' />
		<cfset var UpdateKeyword = '' />
        <cfset var GetCustomHELP = '' />
        <cfset var UpdateCustomHELP = '' />
        <cfset var DeleteCustomHELP = '' />
        <cfset var AddCustomHELP = '' />
        <cfset var GetCustomSTOP = '' />
        <cfset var UpdateCustomSTOP = '' />
        <cfset var DeleteCustomSTOP = '' />
        <cfset var AddCustomSTOP = '' />
        <cfset var GetBatchIdFromKeyword = '' />
        
        <cfset var XMLControlString	= '' />
		<cfset var TRAILER	= '' />
		<cfset var RQ_TRAILER	= '' />
		<cfset var TEXT_TRAILER	= '' />
		<cfset var WriteBatchOptions	= '' />
		<cfset var UpdateBatchResult	= '' />
		<cfset var SetCustomHelpMessage	= '' />
		<cfset var SetCustomSTOPMessage	= '' />

		<cfset dataout = {}>
		
		<cftry>
				<cfinvoke method="UpdateBatchDesc" component="session/cfc/distribution" returnvariable="UpdateBatchResult">
                <cfinvokeargument name="INPBATCHID" value="#inpBatchId#"/>
                <cfinvokeargument name="Desc_vch" value="#INPBATCHDESC#"/>
        		</cfinvoke>
        		
        		<cfinvoke method="SetCustomHelpMessage" component="session/cfc/csc/csc" returnvariable="SetCustomHelpMessage">
                <cfinvokeargument name="INPBATCHID" value="#inpBatchId#"/>
                <cfinvokeargument name="INPDESC" value="#CustomHELP#"/>
        		</cfinvoke>
        		
        		<cfinvoke method="SetCustomSTOPMessage" component="session/cfc/csc/csc" returnvariable="SetCustomSTOPMessage">
                <cfinvokeargument name="INPBATCHID" value="#inpBatchId#"/>
                <cfinvokeargument name="INPDESC" value="#CustomSTOP#"/>
        		</cfinvoke>
				
				<cfset XMLControlString = "<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='OPTIN' GN='1' X='200' Y='200'>">
				
				<!--- opt-in --->
				<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='2' IENQID='0' IHOUR='0' IMIN='0' IMRNR='0' INOON='0' INRMO='END' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='1' TEXT='"&OptInMessage&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='' BOFNQ='4' GID='1' ID='8' IENQID='' IHOUR='' IMIN='' IMRNR='' INOON='' INRMO='' ITYPE='' IVALUE='' OIG='0' REQANS='undefined' RQ='2' TEXT='Branch' TYPE='BRANCH' errMsgTxt='undefined'><COND BOAV='y,yes' BOC='=' BOCDV='undefined' BOQ='2' BOTNQ='6' BOV='' CID='1' TYPE='RESPONSE'/></Q>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='4' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='3' TEXT='Oops! Invalid Response.' TYPE='STATEMENT' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='' BOFNQ='2' GID='1' ID='9' IENQID='' IHOUR='' IMIN='' IMRNR='' INOON='' INRMO='' ITYPE='' IVALUE='' OIG='0' REQANS='undefined' RQ='4' TEXT='Branch' TYPE='BRANCH' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='"&GroupId&"' REQANS='undefined' RQ='5' TEXT='"&ConfirmMessage&"' TYPE='OPTIN' errMsgTxt='undefined'/>">
        		<!--- opt-in info --->
        		<cfset TRAILER = "<Q AF='NOFORMAT' GID='1' ID='19' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined'">
        		<cfset RQ_TRAILER = 6>
        		<cfset TEXT_TRAILER = "">
        		<cfif RequestMore1 NOT EQUAL ''>
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='11' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='6' TEXT='"&RequestMore1&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='12' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='7' SCDF='"&SCDF1&"' TEXT='"&SCDF1&"' TYPE='CDF' errMsgTxt='undefined'/>">
        			<!---<cfset TRAILER = TRAILER & "<Q AF='NOFORMAT' GID='1' ID='19' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='8' TEXT='Your First Name: {%"&SCDF1&"%}">--->
        			<cfset RQ_TRAILER+=2>
        			<cfset TEXT_TRAILER = " TEXT='Your "&SCDF1&": {%"&SCDF1&"%}">
        		</cfif>
        		
        		<cfif RequestMore2 NOT EQUAL ''>
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='13' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='8' TEXT='"&RequestMore2&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='14' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='9' SCDF='"&SCDF2&"' TEXT='"&SCDF2&"' TYPE='CDF' errMsgTxt='undefined'/>">
        			<!---<cfset TRAILER = TRAILER & "&amp;lt;br /&amp;gt;Your Email Address: {%"&SCDF2&"%}">--->
        			<cfset RQ_TRAILER+=2>
        			<cfset TEXT_TRAILER = TEXT_TRAILER & "&amp;lt;br /&amp;gt;Your "&SCDF2&": {%"&SCDF2&"%}">
        		</cfif>
        		
        		<cfif RequestMore3 NOT EQUAL ''>
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='15' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='10' TEXT='"&RequestMore3&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='16' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='11' SCDF='"&SCDF3&"' TEXT='"&SCDF3&"' TYPE='CDF' errMsgTxt='undefined'/>">
        			<!---<cfset TRAILER = TRAILER & "&amp;lt;br /&amp;gt;Your Date of Birth: {%"&SCDF3&"%}">--->
        			<cfset RQ_TRAILER+=2>
        			<cfset TEXT_TRAILER = TEXT_TRAILER & "&amp;lt;br /&amp;gt;Your "&SCDF3&": {%"&SCDF3&"%}">
        		</cfif>
        		
        		<cfif RequestMore4 NOT EQUAL ''>
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='17' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='12' TEXT='"&RequestMore4&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        			<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='18' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='13' SCDF='"&SCDF4&"' TEXT='"&SCDF4&"' TYPE='CDF' errMsgTxt='undefined'/>">
        			<!---<cfset TRAILER = TRAILER & "&amp;lt;br /&amp;gt;Your Zip code: {%"&SCDF4&"%}">--->
        			<cfset RQ_TRAILER+=2>
        			<cfset TEXT_TRAILER = TEXT_TRAILER & "&amp;lt;br /&amp;gt;Your "&SCDF4&": {%"&SCDF4&"%}">
        		</cfif>
        		
        		
        		<cfif TEXT_TRAILER NOT EQUAL ''>
        			<cfset TRAILER = TRAILER & " RQ='"&RQ_TRAILER&"'" & TEXT_TRAILER & "' TYPE='TRAILER' errMsgTxt='undefined'/>">
        			<cfset XMLControlString = XMLControlString & TRAILER>
        		</cfif>
        		
        		<cfset XMLControlString = XMLControlString & "</RXSSCSC><CONFIG BA='1' CT='SMS' FT='' GN='1' LOGO='' RT='' RU='' SN='' THK='' VOICE='0' WC='1' WT=''>0</CONFIG><EXP DATE=''>0</EXP><DM BS='0' DSUID='"&Session.USERID&"' Desc='Description Not Specified' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM><RXSS/><RULES/><CCD CID=' ' ESI='-14' SSVMD='20'>0</CCD><STAGE h='950' s='1' w='950'>0</STAGE>">	
        		
        		<!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#XMLControlString#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">
					</cfquery>
                 
                                                  
                 
                 
			    <cfset dataout.RXRESULTCODE = 1 />				
     
					
   		    <cfset dataout.KeywordId = "#KeywordId#" />		    
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "Update success" />
		<cfcatch>
			<cfset dataout = {}>
		    <cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.KeywordId = "#KeywordId#" />		    
   		    <cfset dataout.Keyword = "#Keyword#" />
   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>   
	
	<cffunction name="AddOptIn" access="remote" output="true" hint="Add Opt-In">
		<cfargument name="INPBATCHDESC" required="yes" default="">
		<cfargument name="inpBatchId" TYPE="numeric" required="true">
		<cfargument name="ShortCodeRequestId" TYPE="numeric">
		<cfargument name="ShortCode" required="no" default="">
		<cfargument name="Keyword" required="yes" default="">
		<cfargument name="Response" required="no" default="">
		<cfargument name="CustomHELP" required="no" default="">
		<cfargument name="CustomSTOP" required="no" default="">
		<cfargument name="IsSurvey" required="no" default="0">
		<!---CP1--->
        <cfargument name="OptInMessage" TYPE="string" required="yes" default="">
		<!---<cfargument name="OptInMessageId" TYPE="numeric" required="yes" default="">--->
		<!---CP5--->
		<cfargument name="ConfirmMessage" TYPE="string" required="yes" default="">
		<!---<cfargument name="ConfirmMessageId" TYPE="numeric" required="yes" default="">--->
		
		<cfset var dataout = '' />
		<cfset dataout = {}>
		<cfset var _has_Org	= '' />
		<cfset var orgInfoData	= '' />
		<cfset var CustomHELP	= '' />
		<cfset var CustomSTOP	= '' />
		<cfset var inpBatchId	= '' />
		<cfset var XMLControlString	= '' />
		<cfset var WriteBatchOptions	= '' />
		<cfset var addGroupResult	= '' />
		<cfset var userOrgInfo	= '' />
		<cfset var RetVarAddNewBatch	= '' />
		<cfset var response	= '' />
		
		<cftry>
			<cfinvoke method="addgroup" component="#Session.SessionCFCPath#.MultiLists2" returnvariable="addGroupResult">
                <cfinvokeargument name="inpGroupDesc" value="Group-#ShortCode#-#Keyword#"/>
        	</cfinvoke>
        	
        	<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
        	
        	<cfset _has_Org = false>	
			<cfif userOrgInfo.RXRESULTCODE EQ 1>
				<cfset _has_Org = true>
				<cfset orgInfoData = userOrgInfo.ORGINFO>
			</cfif>
        	<cfset CustomHELP = orgInfoData.CustomHelpMessage_vch>
        	<cfset CustomSTOP = orgInfoData.CustomStopMessage_vch>
			
			<cfinvoke method="AddNewBatch" component="#Session.SessionCFCPath#.distribution" returnvariable="RetVarAddNewBatch">
	                        <cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#">
                            <cfinvokeargument name="operator" value="#Keyword_Action_Title#">  
                            <cfinvokeargument name="INPALLOWDUPLICATES" value="1">                             
             </cfinvoke>
	                    
	          <cfif RetVarAddNewBatch.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetVarAddNewBatch.MESSAGE#" TYPE="Any" detail="#RetVarAddNewBatch.ERRMESSAGE#" errorcode="-2">                   
	                    <cfelse>
                        	<cfset dataout.BATCHID = "#RetVarAddNewBatch.NEXTBATCHID#" />
               </cfif> 
        	<cfset inpBatchId = RetVarAddNewBatch.NEXTBATCHID>
        	
			<cfinvoke method="AddKeyword" returnvariable="response">
							<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                            <cfinvokeargument name="ShortCodeRequestId" value="#ShortCodeRequestId#">
                            <cfinvokeargument name="ShortCode" value="#ShortCode#">
                            <cfinvokeargument name="Keyword" value="#Keyword#"> 
                            <cfinvokeargument name="Response" value="#Response#">
                            <cfinvokeargument name="CustomHELP" value="#CustomHELP#">
                            <cfinvokeargument name="CustomSTOP" value="#CustomSTOP#">
                            <cfinvokeargument name="IsSurvey" value="#IsSurvey#"> 
             </cfinvoke>
            
       			<cfset XMLControlString = "<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='OPTIN' GN='1' X='200' Y='200'>">
				<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='2' IENQID='0' IHOUR='0' IMIN='0' IMRNR='0' INOON='0' INRMO='END' ITYPE='SECONDS' IVALUE='0' OIG='0' REQANS='undefined' RQ='1' TEXT='"&OptInMessage&"' TYPE='SHORTANSWER' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='' BOFNQ='4' GID='1' ID='8' IENQID='' IHOUR='' IMIN='' IMRNR='' INOON='' INRMO='' ITYPE='' IVALUE='' OIG='0' REQANS='undefined' RQ='2' TEXT='Branch' TYPE='BRANCH' errMsgTxt='undefined'><COND BOAV='y,yes' BOC='=' BOCDV='undefined' BOQ='2' BOTNQ='6' BOV='' CID='1' TYPE='RESPONSE'/></Q>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='4' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' OIG='0' REQANS='undefined' RQ='3' TEXT='Oops! Invalid Response.' TYPE='STATEMENT' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='' BOFNQ='2' GID='1' ID='9' IENQID='' IHOUR='' IMIN='' IMRNR='' INOON='' INRMO='' ITYPE='' IVALUE='' OIG='0' REQANS='undefined' RQ='4' TEXT='Branch' TYPE='BRANCH' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "<Q AF='NOFORMAT' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' OIG='"&addGroupResult.INPGROUPID&"' REQANS='undefined' RQ='5' TEXT='"&ConfirmMessage&"' TYPE='OPTIN' errMsgTxt='undefined'/>">
        		<cfset XMLControlString = XMLControlString & "</RXSSCSC><CONFIG BA='1' CT='SMS' FT='' GN='1' LOGO='' RT='' RU='' SN='' THK='' VOICE='0' WC='1' WT=''>0</CONFIG><EXP DATE=''>0</EXP><DM BS='0' DSUID='"&Session.USERID&"' Desc='Description Not Specified' LIB='0' MT='1' PT='12'><ELE ID='0'>0</ELE></DM><RXSS/><RULES/><CCD CID=' ' ESI='-14' SSVMD='20'>0</CCD><STAGE h='950' s='1' w='950'>0</STAGE>">	
        		      
             
             <!--- Save Local Query to DB --->
					<cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">
						UPDATE 
							simpleobjects.batch 
						SET 
							XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#XMLControlString#">
						WHERE 
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">
					</cfquery>
             
             
			<cfset dataout.MESSAGE = "Update success" />
		<cfcatch>
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>  
	
	<cffunction name="AddKeyword" access="remote" output="true" hint="Add Keyword">
		<cfargument name="inpBatchId" TYPE="numeric">
		<cfargument name="ShortCodeRequestId" TYPE="numeric">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Keyword">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="0">
        <cfargument name="CustomHELP" required="no" default="">
        <cfargument name="CustomSTOP" required="no" default="">
        
		<cfset var dataout = '' />
        <cfset var keyword_upper = '' />
		<cfset var CountDefaultKeyword = '' />
		<cfset var CountKeyword = '' />
		<cfset var AddKeyword = '' />
		<cfset var RetVarAddNewBatch = '' />
		<cfset var RetVarAddNewSurvey = '' />
        <cfset var AddCustomHELP = '' />
		<cfset var AddCustomSTOP = '' />

		<cfset dataout = {}>
		<cfset dataout.BATCHID = "0" />
		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>
			
                 <cfset keyword_upper = UCase(Keyword)>
            	<!--- Count keyword exist in db --->
				<cfquery name="CountDefaultKeyword" datasource="#Session.DBSourceEBM#" >
					SELECT 
						COUNT(k.Keyword_vch) AS records
			       	FROM 
			       		sms.keyword AS k
			       	JOIN 
			       		sms.shortcode AS sc
			       	ON 
			       		sc.ShortCodeId_int = k.ShortCodeId_int
			       	WHERE 
			       		UPPER(k.Keyword_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword_upper#">
				    AND 
						sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
					AND
						k.IsDefault_bit = 1
                    AND
                    	k.EMSFlag_int = 0    
				</cfquery>
				
				<cfif CountDefaultKeyword.records EQ 0>
					<cfquery name="CountKeyword" datasource="#Session.DBSourceEBM#" >
				        SELECT 
							COUNT(k.Keyword_vch) AS records
				       	FROM 
				       		sms.keyword AS k
				       	JOIN
				       		sms.shortcoderequest AS scr
				       	ON
				       		k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
				       	JOIN 
				       		sms.shortcode AS sc
				       	ON 
				       		sc.ShortCodeId_int = scr.ShortCodeId_int
				       	WHERE 
				       		UPPER(k.Keyword_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#keyword_upper#">
					    AND 
							sc.ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">
                        AND
                        	k.EMSFlag_int = 0    
						AND
							k.Active_int = 1
						AND
							k.IsDefault_bit = 0
			    	</cfquery> 
				
					<cfif #CountKeyword.records# EQ 0>
						
						<!--- Each keyowrd short code gets it's own Batch Id---> 
	                                           
	                    
	                    
						<!--- Add keyword into db --->
	                    <cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
	                        INSERT INTO sms.keyword
	                            (
	                                `Keyword_vch`, 
	                                `ShortCodeRequestId_int`,  
	                                `Response_vch`, 
	                                `Created_dt`,
	                                `IsDefault_bit`,
	                                 BatchId_bi,
	                                 Survey_int,
                                     EMSFlag_int
	                             )
	                        VALUES
	                        	(	
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">, 
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
	                                NOW(),
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">,
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">,
                                    0                               
	                            )
	                    </cfquery>  
                        
	    				<cfset dataout.KeywordId = "#AddKeyword.GENERATED_KEY#" />

                        
                        <!--- Add Custom HELP/STOP--->
                        
                        <cfif TRIM(CustomHELP) NEQ "">
                    	
							<!--- Add it --->
                            <cfquery name="AddCustomHELP" datasource="#Session.DBSourceEBM#">                                                                                                   
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch                             
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                    'HELP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHELP), 640)#">
                                 )
                            </cfquery>
                        
                        </cfif>	
                    
                    
                        <cfif TRIM(CustomSTOP) NEQ "">
                    	
							<!--- Add it --->
                            <cfquery name="AddCustomSTOP" datasource="#Session.DBSourceEBM#">                                                                                                   
                                 INSERT INTO
                                    sms.smsshare
                                 (
                                    ShortCode_vch,
                                    Keyword_vch,
                                    BatchId_bi,
                                    Content_vch                             
                                 )
                                 VALUES
                                 (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                                    'STOP',
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomSTOP), 640)#">
                                 )
                            </cfquery>
                        
                        </cfif>
                        
	                    
						<cfset dataout.RXRESULTCODE = 1 />
					<cfelse>
						<cfset dataout.RXRESULTCODE = -1 />
						<cfset dataout.ERRMESSAGE = "The keyword you have input already exists!" />	
					</cfif>	
				<cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.ERRMESSAGE = "The keyword you have input already exists!" />	
				</cfif>			
				
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.BATCHID = "0" />
                <cfset dataout.KeywordId = 0 />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
		</cfif>	
		<cfreturn dataout />
	</cffunction> 
	
	<cffunction name="getShortLink" access="remote" output="true" hint="get Short Link">
		<cfargument name="longUrl" TYPE="string" required="true">
		<cfset var longLink	= '' />
		<cfset var restResult	= '' />
		<cfset longLink ="https://api-ssl.bitly.com/v3/shorten?access_token=3de9bea0c7de7e3b86811c67a789176c6fd26993&longUrl="&longUrl>
		<cfhttp url=#longLink# result="restResult" method="GET"/>
		<cfreturn restResult />
	</cffunction>

	<cffunction name="AddKeywordEMS" access="remote" output="true" hint="EMS Keywords added here do not count against total keyword counts.">
		<cfargument name="ShortCode" TYPE="string">
		<cfargument name="Keyword">
        <cfargument name="INPBATCHID">
		<cfargument name="Response">
		<cfargument name="IsDefault" required="false" default="0">
        <cfargument name="IsSurvey" required="false" default="0">
        <cfargument name="CustomHELP" required="no" default="">
        <cfargument name="CustomSTOP" required="no" default="">
        
		<cfset var dataout = '' />
		<cfset var keyword_upper = '' />
		<cfset var CountDefaultKeyword = '' />
		<cfset var CountKeyword = '' />
		<cfset var AddKeyword = '' />
		<cfset var RetVarAddNewBatch = '' />
		<cfset var RetVarAddNewSurvey = '' />
        <cfset var GetSCRId = '' />
        <cfset var AddCustomHELP = ''/>
        <cfset var AddCustomSTOP = ''/>

		<cfset dataout = {}>
		<!---Check Permission --->
		<cfif smsActionKeywordPermission.havePermission>
			<cftry>
			
                 <cfset keyword_upper = UCase(Keyword)>
                 
                    
				<!--- get requester Id and requester Type --->
                <cfset var requestObj = SireRequesterIdAndType()>					
                                    
                <!--- get number of Short code request Pending --->
                <cfquery name="GetSCRId" datasource="#Session.DBSourceEBM#">
                    SELECT                        	
                        ShortCodeRequestId_int,
                        SC.ShortCodeId_int
                    FROM
                        sms.shortcoderequest as SCR
                    LEFT JOIN
                        sms.shortcode as SC
                    ON 
                        SC.ShortCodeId_int = SCR.ShortCodeId_int
                    WHERE
                        SCR.status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SHORT_CODE_APPROVED#">
                    AND
                        SCR.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Id#">
                    AND
                        SCR.RequesterType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestObj.Type#">
                    AND
                        SC.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">    
                    
                </cfquery>
                
                
                <cfif GetSCRId.RecordCount GT 0>
                    <cfset arguments.ShortCodeRequestId = GetSCRId.ShortCodeRequestId_int />
                    
                <cfelse>
                    <cfthrow MESSAGE="Short code is not approved for this account." TYPE="Any" detail="UID=#requestObj.Id#  ShortCode=#ShortCode# Type=#requestObj.Type#" errorcode="-2">
                </cfif>
                    
	                      <!--- 
	                    <!--- Add default SMS survey logic to Batch Id in case user chooses survey later ---> 
	                    <cfinvoke method="AddNewSurvey" component="#Session.SessionCFCPath#.ire.marketingSurveyTools" returnvariable="RetVarAddNewSurvey">
	                        <cfinvokeargument name="INPBATCHID" value="#RetVarAddNewBatch.NEXTBATCHID#">
	                        <cfinvokeargument name="INPBATCHDESC" value="Short Code {#ShortCode#} Keyword {#Keyword#}">
	                        <cfinvokeargument name="inpXML" value="<HEADER/><BODY/><FOOTER/><RXSSCSC DESC='#XMLFORMAT(Response)#' GN='1' X='200' Y='200'></RXSSCSC><CONFIG WC='1' BA='1' THK='' CT='SMS' GN='1' WT='' RT='' RU='' SN='' FT='' LOGO='' VOICE='0'>0</CONFIG><EXP DATE=''>0</EXP>">                                             
	                    </cfinvoke>
	                    
	                    <cfif RetVarAddNewSurvey.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetVarAddNewSurvey.MESSAGE#" TYPE="Any" detail="#RetVarAddNewSurvey.ERRMESSAGE#" errorcode="-2">                   
	                    </cfif>       --->
	        
	                    
				<!--- Add keyword into db --->
                <cfquery name="AddKeyword" datasource="#Session.DBSourceEBM#" result="AddKeyword">
                    INSERT INTO sms.keyword
                        (
                            `Keyword_vch`, 
                            `ShortCodeRequestId_int`,  
                            `Response_vch`, 
                            `Created_dt`,
                            `IsDefault_bit`,
                             BatchId_bi,
                             Survey_int,
                             EMSFlag_int,
                             ShortCodeId_int
                             
                         )
                    VALUES
                        (	
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Keyword#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ShortCodeRequestId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Response#">,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsDefault#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IsSurvey#">,
                            1,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSCRId.ShortCodeId_int#">                              
                        )
                </cfquery>

                <!--- Add Custom HELP/STOP--->
                        
                <cfif TRIM(CustomHELP) NEQ "">
            	
					<!--- Add it --->
                    <cfquery name="AddCustomHELP" datasource="#Session.DBSourceEBM#">                                                                                                   
                         INSERT INTO
                            sms.smsshare
                         (
                            ShortCode_vch,
                            Keyword_vch,
                            BatchId_bi,
                            Content_vch                             
                         )
                         VALUES
                         (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                            'HELP',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomHELP), 640)#">
                         )
                    </cfquery>
                
                </cfif>	
            
            
                <cfif TRIM(CustomSTOP) NEQ "">
            	
					<!--- Add it --->
                    <cfquery name="AddCustomSTOP" datasource="#Session.DBSourceEBM#">                                                                                                   
                         INSERT INTO
                            sms.smsshare
                         (
                            ShortCode_vch,
                            Keyword_vch,
                            BatchId_bi,
                            Content_vch                             
                         )
                         VALUES
                         (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortCode#">,
                            'STOP',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CustomSTOP), 640)#">
                         )
                    </cfquery>
                
                </cfif>  
                
                <cfset dataout.RXRESULTCODE = 1 />
				
				<cfset dataout.KeywordId = "#AddKeyword.GENERATED_KEY#" />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "" />
	   		    <cfset dataout.MESSAGE = "" />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.KeywordId = 0 />
	   		    <cfset dataout.ShortCodeRequestId = "#ShortCodeRequestId#" />
	   		    <cfset dataout.Keyword = "#Keyword#" />
	   		    <cfset dataout.Response = "#Response#" />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
			</cftry>
		<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "You do not have Action Keyword permission for this account."/>
		</cfif>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="SireRequesterIdAndType" hint="Sire Requester Id And Type">
		
		<cfset var requestObj = '' />
		
		<cfset requestObj = {}>
		<!--- 
			if current user is being in a company requesterId EQ company ID and requesterType EQ 1 (OWNER_TYPE_COMPANY)  
			else requesterId EQ useri ID and requesterType EQ 2 (OWNER_TYPE_USER )
		--->
		<cfif session.companyId GT 0>
			<cfset requestObj.Id = session.companyId>
			<cfset requestObj.Type= OWNER_TYPE_COMPANY>
		<cfelse>
			<cfset requestObj.Id = session.userId>
			<cfset requestObj.Type= OWNER_TYPE_USER>
		</cfif>
		
		<cfreturn requestObj>
		
	</cffunction>

</cfcomponent>                        