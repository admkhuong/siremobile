<cfcomponent>
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 

    <cffunction name="GetOrderPlan" access="remote" output="true" hint="Get list Order Plan">
        <cfargument name="plan" TYPE="numeric" required="true"/>
        <cfset var dataout  = {} />
        <cfset var getPlan  = '' />
        <cfset var limitNumber = '' />
        <cfset var shortUrl = '' />
        <!--- USE READ FROM MATER FOR TRANSACTION WHEN USER SIGNUP. MINHHTN --->
        <cfquery name="getPlan" datasource="#Session.DBSourceEBM#">
            SELECT 
                PlanId_int, 
                PlanName_vch,
                Amount_dec,
                YearlyAmount_dec,
                UserAccountNumber_int,
                KeywordsLimitNumber_int,
                FirstSMSIncluded_int,
                PriceMsgAfter_dec,
                PriceKeywordAfter_dec,
                MlpsLimitNumber_int,
                ShortUrlsLimitNumber_int,
                MlpsImageCapacityLimit_bi
            FROM 
                simplebilling.plans
            WHERE
                PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.plan#"> 
            AND
                Status_int in (1,8,-8,4,2)
        </cfquery>
        
            <cfset dataout =  QueryNew("RXRESULTCODE, PLANID, PLANNAME, AMOUNT, YEARLYAMOUNT, USERACCOUNTNUMBER, KEYWORDSLIMITNUMBER, FIRSTSMSINCLUDED, MLPSLIMITNUMBER, SHORTURLSLIMITNUMBER, IMAGECAPACITYLIMIT, PRICEMSGAFTER,PRICEKEYWORDAFTER,MESSAGE,ERRMESSAGE")> 
        <cfif getPlan.RecordCount GT 0>

            <!--- MLPSLIMITNUMBER --->
            <cfif getPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getPlan.MlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getPlan.ShortUrlsLimitNumber_int/>
            </cfif>

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "PLANID", "#getPlan.PlanId_int#") /> 
            <cfset QuerySetCell(dataout, "PLANNAME", "#getPlan.PlanName_vch#") />  
            <cfset QuerySetCell(dataout, "AMOUNT", "#getPlan.Amount_dec#") />  
			<cfset QuerySetCell(dataout, "YEARLYAMOUNT", "#getPlan.YearlyAmount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getPlan.UserAccountNumber_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#getPlan.PriceMsgAfter_dec#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#getPlan.PriceKeywordAfter_dec#") /> 
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
            <cfset QuerySetCell(dataout, "IMAGECAPACITYLIMIT", "#getPlan.MlpsImageCapacityLimit_bi#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "Ok")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
        </cfif>
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="GetUserPlan" access="remote" output="true" hint="get current user plan">
        <cfargument name="InpUserId" TYPE="numeric" required="false"/>

        <cfset var userId = arguments.InpUserId>

        <cfif structKeyExists(arguments, "InpUserId")>
            <cfset userId = arguments.InpUserId>
        <cfelse>
            <cfset userId = SESSION.USERID>
        </cfif>

        <cfset var totalKeyword = '' />
        <cfset var KeywordUse   = '' />
        <cfset var KeywordAvailable = '' />
        <cfset var dataout  = {} />
        <cfset var getUserPlan  = '' />
        <cfset var getUserBilling  = '' />
        <cfset var getKeywordUse    = '' />
        <cfset var checkUserPlan    = '' />
        <cfset var PriceKeywordAfter    = '' />
        <cfset var PriceMsgAfter    = '' />
        <cfset var getOrderPlan = '' />

        <cfset var limitNumber = '' />
        <cfset var shortUrl = '' />
        <cfset var RXGetOrderPlan = {}/>
        <cfset var downgradePlanName = '' />
        
        <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                u.UserPlanId_bi, 
                u.Status_int,
                u.PlanId_int,
                u.StartDate_dt,
                u.EndDate_dt,
                p.PlanName_vch,
                p.Amount_dec,
                u.BillingType_ti,
                p.UserAccountNumber_int,
                p.Order_int,
                u.KeywordsLimitNumber_int,
                p.KeywordsLimitNumber_int as PlanKeywordsLimitNumber_int,
                u.KeywordMinCharLimit_int,
                p.KeywordMinCharLimit_int as PlanKeywordMinCharLimit_int,
                p.FirstSMSIncluded_int,
                u.FirstSMSIncluded_int as UserPlanFirstSMSIncluded_int,
                u.PriceMsgAfter_dec,
                u.PriceKeywordAfter_dec,
                u.BuyKeywordNumber_int,
                u.BuyKeywordNumberExpired_int,
                u.DowngradeDate_dt,
                u.MlpsLimitNumber_int,
                u.ShortUrlsLimitNumber_int,
                u.MlpsImageCapacityLimit_bi,
                u.UserDowngradeDate_dt,
                u.UserDowngradePlan_int,
                u.UserDowngradeKeyword_txt,
                u.UserDowngradeMLP_txt,
                u.UserDowngradeShortUrl_txt,
                u.PromotionId_int,
                u.PromotionLastVersionId_int,
                u.PromotionKeywordsLimitNumber_int,
                u.PromotionMlpsLimitNumber_int,
                u.PromotionShortUrlsLimitNumber_int,
                u.NumOfRecurringKeyword_int,
                u.NumOfRecurringPlan_int,
                u.MonthlyAddBenefitDate_dt,
                p.Status_int as PlanStatus,
                u.TotalKeywordHaveToPaid_int
            FROM 
                simplebilling.userplans u 
            INNER JOIN 
                simplebilling.plans p 
            ON 
                u.PlanId_int = p.PlanId_int
            WHERE
                u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
            AND 
                u.Status_int = 1
            ORDER BY 
                UserPlanId_bi DESC
            LIMIT 
                1
        </cfquery>

        <cfquery name="getUserBilling" datasource="#Session.DBSourceEBM#">
            SELECT 
                PromotionCreditBalance_int
            FROM 
                simplebilling.billing 
            WHERE
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
            ORDER BY 
                UserId_int DESC
            LIMIT 1
        </cfquery>
        
        <!--- CHECK IF USER PLAN IS EXPIRED --->        
        <cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
            <cfinvokeargument name="userId" value="#userId#">
            </cfinvoke>
            
            <!--- GET FREE PLAN INFO --->
            <cfinvoke component="session.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="getOrderPlan">
                <cfinvokeargument name="plan" value="1">
                </cfinvoke>        

                <cfquery name="getKeywordUse" datasource="#Session.DBSourceREAD#">
                    SELECT COUNT(k.KeywordId_int) AS keywordUse 
                    FROM sms.keyword k 
                    INNER JOIN simpleobjects.batch b ON k.BatchId_bi = b.BatchId_bi 
                    WHERE 
                    k.Active_int = 1
                    AND k.IsDefault_bit = 0
                    AND EMSFlag_int = 0
                    AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
                </cfquery>
                
                <cfset totalKeyword = 0>
                <cfset KeywordUse = 0>
                <cfset KeywordAvailable = 0>

                <cfif getUserPlan.RecordCount GT 0>
                    <cfset totalKeyword = (totalKeyword + getUserPlan.KeywordsLimitNumber_int + getUserPlan.BuyKeywordNumber_int + getUserPlan.PromotionKeywordsLimitNumber_int)>
                </cfif>  
                
                <cfif getKeywordUse.RecordCount GT 0>
                    <cfset KeywordUse = getKeywordUse.keywordUse>
                </cfif>
                
        <!--- IF USER PLAN HAS EXPRIED within 30 days : keyword avaiable = (free plan keyword + keyword buy after expired) - keyword in used 
            PriceKeywordAfter = Free Plan Price
            PriceMsgAfter = Free Plan Price
        --->    
        <cfif checkUserPlan.RXRESULTCODE GT 0> 
            <cfif checkUserPlan.RXRESULTCODE EQ 1>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER + getUserPlan.BuyKeywordNumberExpired_int - KeywordUse)>        
            </cfif>

            <cfif checkUserPlan.RXRESULTCODE EQ 2>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER - KeywordUse)>    
            </cfif>
            
            <cfset PriceKeywordAfter =  getOrderPlan.PRICEKEYWORDAFTER >
            <cfset PriceMsgAfter =  getOrderPlan.PRICEMSGAFTER >
        <cfelse>
            <cfset KeywordAvailable = (totalKeyword - KeywordUse)>    
            <cfset PriceKeywordAfter =  getUserPlan.PriceKeywordAfter_dec >
            <cfset PriceMsgAfter =  getUserPlan.PriceMsgAfter_dec >
        </cfif>
        
        <cfif KeywordAvailable LT 0>
            <cfset KeywordAvailable = 0 >
        </cfif>
        
        <cfset dataout = QueryNew("RXRESULTCODE, USERPLANID,STATUS,PLANID,BILLINGTYPE,PLANORDER, STARTDATE,ENDDATE,PLANNAME,PLANSTATUS,AMOUNT,USERACCOUNTNUMBER,KEYWORDMINCHARLIMIT,KEYWORDSLIMITNUMBER,FIRSTSMSINCLUDED,
            PROMOTIONCREDIT,USERPLANFIRSTSMSINCLUDED,PRICEMSGAFTER,PRICEKEYWORDAFTER,KEYWORDPURCHASED, TOTALKEYWORD,KEYWORDUSE,KEYWORDAVAILABLE,KEYWORDBUYAFTEREXPRIED,PLANEXPIRED,
            PLANKEYWORDSLIMITNUMBER,MLPSLIMITNUMBER,SHORTURLSLIMITNUMBER,IMAGECAPACITYLIMIT,PROMOTIONID,PROMOTIONLASTVERSIONID,PROMOTIONKEYWORDSLIMITNUMBER,PROMOTIONMLPSLIMITNUMBER,
            PROMOTIONSHORTURLSLIMITNUMBER,FREEPLANKEYWORDSLIMITNUMBER,DOWNGRADEDATE,USERDOWNGRADEPLAN, NUMOFRECURRINGKEYWORD, NUMOFRECURRINGPLAN, MONTHLYADDBENEFITDATE, USERDOWNGRADEDATE,
            USERDOWNGRADEKEYWORD,USERDOWNGRADEMLP,USERDOWNGRADESHORTURL,USERDOWNGRADEPLANNAME,TOTALKEYWORDHAVETOPAID,MESSAGE,ERRMESSAGE")> 
        <cfif getUserPlan.RecordCount GT 0>   

            <!--- MLPSLIMITNUMBER --->
            <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getUserPlan.MlpsLimitNumber_int  + getUserPlan.PromotionMlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int  + getUserPlan.PromotionShortUrlsLimitNumber_int/>
            </cfif>     

            <cfif getUserPlan.UserDowngradePlan_int GT 0>
                <cfinvoke component="session.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="RXGetOrderPlan">
                    <cfinvokeargument name="plan" value="#getUserPlan.UserDowngradePlan_int#"/>
                </cfinvoke>
                <cfif RXGetOrderPlan.RXRESULTCODE EQ 1>
                    <cfset downgradePlanName = RXGetOrderPlan.PLANNAME>
                </cfif>
            </cfif> 

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "USERPLANID", "#getUserPlan.UserPlanId_bi#") /> 
            <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
            <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />
            <cfset QuerySetCell(dataout, "BILLINGTYPE", "#getUserPlan.BillingType_ti#") />
            <cfset QuerySetCell(dataout, "PLANORDER", "#getUserPlan.Order_int#") /> 
            <cfset QuerySetCell(dataout, "STARTDATE", "#getUserPlan.StartDate_dt#") />
            <cfset QuerySetCell(dataout, "ENDDATE", "#getUserPlan.EndDate_dt#") />            
            <cfset QuerySetCell(dataout, "PLANNAME", "#(getUserPlan.PlanStatus EQ 1 OR getUserPlan.PlanStatus EQ 0) ? "":""##getUserPlan.PlanName_vch#") />                          
            <cfset QuerySetCell(dataout, "PLANSTATUS", "#getUserPlan.PlanStatus#") />
            <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />               
            <cfset QuerySetCell(dataout, "KEYWORDMINCHARLIMIT", "#getUserPlan.KeywordMinCharLimit_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PROMOTIONCREDIT", "#getUserBilling.PromotionCreditBalance_int#") />  
            <cfset QuerySetCell(dataout, "USERPLANFIRSTSMSINCLUDED", "#getUserPlan.UserPlanFirstSMSIncluded_int#") />
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#PriceMsgAfter#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#PriceKeywordAfter#") />
            <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "#getUserPlan.BuyKeywordNumber_int#") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
            <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
            <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
            <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "#getUserPlan.BuyKeywordNumberExpired_int#") />  
            <cfset QuerySetCell(dataout, "PLANEXPIRED", "#checkUserPlan.RXRESULTCODE#") />  
            <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "#getUserPlan.PlanKeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FREEPLANKEYWORDSLIMITNUMBER", "#getOrderPlan.KEYWORDSLIMITNUMBER#") />  
            <cfset QuerySetCell(dataout, "DOWNGRADEDATE", "#getUserPlan.DowngradeDate_dt#") />  
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
            <cfset QuerySetCell(dataout, "IMAGECAPACITYLIMIT", "#getUserPlan.MlpsImageCapacityLimit_bi#") />
            
            <cfset QuerySetCell(dataout, "PROMOTIONID", "#getUserPlan.PromotionId_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONLASTVERSIONID", "#getUserPlan.PromotionLastVersionId_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONKEYWORDSLIMITNUMBER", "#getUserPlan.PromotionKeywordsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONMLPSLIMITNUMBER", "#getUserPlan.PromotionMlpsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONSHORTURLSLIMITNUMBER", "#getUserPlan.PromotionShortUrlsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEPLAN", "#getUserPlan.UserDowngradePlan_int#") />
            <cfset QuerySetCell(dataout, "NUMOFRECURRINGKEYWORD", "#getUserPlan.NumOfRecurringKeyword_int#") />
            <cfset QuerySetCell(dataout, "NUMOFRECURRINGPLAN", "#getUserPlan.NumOfRecurringPlan_int#") />
            <cfset QuerySetCell(dataout, "MONTHLYADDBENEFITDATE", "#getUserPlan.MonthlyAddBenefitDate_dt#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEDATE", "#getUserPlan.UserDowngradeDate_dt#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEKEYWORD", "#getUserPlan.UserDowngradeKeyword_txt#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEMLP", "#getUserPlan.UserDowngradeMLP_txt#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADESHORTURL", "#getUserPlan.UserDowngradeShortUrl_txt#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEPLANNAME", "#downgradePlanName#") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORDHAVETOPAID", "#getUserPlan.TotalKeywordHaveToPaid_int#") />
            

            <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
        <cfelse>
            
           <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                p.PlanId_int,
                p.PlanName_vch,
                p.Status_int,
                p.Amount_dec,
                p.UserAccountNumber_int,
                p.KeywordMinCharLimit_int,
                p.KeywordsLimitNumber_int,
                p.FirstSMSIncluded_int,
                p.CreditsAddAmount_int,
                p.PriceMsgAfter_dec,
                p.PriceKeywordAfter_dec,
                p.Order_int,
                p.MlpsLimitNumber_int,
                p.ShortUrlsLimitNumber_int,
                p.MlpsImageCapacityLimit_bi                
            FROM 
                simplebilling.plans p
            WHERE  
                PlanId_int = 1 
        </cfquery>
        
        <!--- MLPSLIMITNUMBER --->
        <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
            <cfset limitNumber = "Unlimited"/>
        <cfelse>
            <cfset limitNumber = getUserPlan.MlpsLimitNumber_int  + getUserPlan.PromotionMlpsLimitNumber_int/>
        </cfif>                

        <!--- SHORTURLSLIMITNUMBER --->
        <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
            <cfset shortUrl = "Unlimited"/>
        <cfelse>
            <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int  + getUserPlan.PromotionShortUrlsLimitNumber_int/>
        </cfif>

        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 0) />
        <cfset QuerySetCell(dataout, "USERPLANID", "0") /> 
        <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
        <cfset QuerySetCell(dataout, "PLANSTATUS", "#getUserPlan.Status_int#") /> 
        <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />
        <cfset QuerySetCell(dataout, "PLANORDER", "#getUserPlan.Order_int#") /> 
        <cfset QuerySetCell(dataout, "BILLINGTYPE", "#getUserPlan.BillingType_ti#") />
        <cfset QuerySetCell(dataout, "STARTDATE", "") />
        <cfset QuerySetCell(dataout, "ENDDATE", "") />
        <cfset QuerySetCell(dataout, "PLANNAME", "#getUserPlan.PlanName_vch#") />    
        
        <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
        <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />  
        <cfset QuerySetCell(dataout, "KEYWORDMINCHARLIMIT", "#getUserPlan.KeywordMinCharLimit_int#") />  
        <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
        <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
        <cfset QuerySetCell(dataout, "PROMOTIONCREDIT", "#getUserBilling.PromotionCreditBalance_int#") />
        <cfset QuerySetCell(dataout, "USERPLANFIRSTSMSINCLUDED", "0") />  
        <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#getUserPlan.PriceMsgAfter_dec#") /> 
        <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#getUserPlan.PriceKeywordAfter_dec#") />
        <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "0") />
        <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
        <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
        <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
        <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "0") /> 
        <cfset QuerySetCell(dataout, "PLANEXPIRED", "0") />
        <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "0") /> 
        <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
        <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") /> 
        <cfset QuerySetCell(dataout, "IMAGECAPACITYLIMIT", "#getUserPlan.MlpsImageCapacityLimit_bi#") /> 

        <cfset QuerySetCell(dataout, "PROMOTIONID", 0) />
        <cfset QuerySetCell(dataout, "PROMOTIONLASTVERSIONID", 0) />
        <cfset QuerySetCell(dataout, "PROMOTIONKEYWORDSLIMITNUMBER", 0) />
        <cfset QuerySetCell(dataout, "PROMOTIONMLPSLIMITNUMBER", 0) />
        <cfset QuerySetCell(dataout, "PROMOTIONSHORTURLSLIMITNUMBER", 0) />
        <cfset QuerySetCell(dataout, "USERDOWNGRADEPLAN", "0") />
        <cfset QuerySetCell(dataout, "NUMOFRECURRINGKEYWORD", "#getUserPlan.NumOfRecurringKeyword_int#") />
        <cfset QuerySetCell(dataout, "NUMOFRECURRINGPLAN", "#getUserPlan.NumOfRecurringPlan_int#") />
        <cfset QuerySetCell(dataout, "MONTHLYADDBENEFITDATE", "#getUserPlan.MonthlyAddBenefitDate_dt#") />
        <cfset QuerySetCell(dataout, "USERDOWNGRADEDATE", "") />
        <cfset QuerySetCell(dataout, "USERDOWNGRADEKEYWORD", "[]") />
        <cfset QuerySetCell(dataout, "USERDOWNGRADEMLP", "[]") />
        <cfset QuerySetCell(dataout, "USERDOWNGRADESHORTURL", "[]") />
        <cfset QuerySetCell(dataout, "USERDOWNGRADEPLANNAME", "") />
        <cfset QuerySetCell(dataout, "TOTALKEYWORDHAVETOPAID", 0) />

        <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
    </cfif>
    <cfreturn dataout />
</cffunction>

<cffunction name='insertUsersPlan' access="remote" output="true" hint="insert User Plan">
    <cfargument name="inpPlanId" type="string" required="yes" default="1">
    <cfargument name="inpBillingType" type="numeric" required="no" default="1">
    <cfargument name="inpUserId" type="string" required="yes" default="0">
    <cfargument name="inpStarDate" type="date" required="yes">
    <cfargument name="inpEndDate" type="date" required="yes">
    <cfargument name="inpMonthlyAddBenefitDate" type="date" required="no">

    <cfset var dataout  = {} />
    <cfset var Amount_dec   = '' />
    <cfset var UserAccountNumber_int    = '' />
    <cfset var KeywordsLimitNumber_int  = '' />
    <cfset var FirstSMSIncluded_int = '' />
    <cfset var PriceMsgAfter_dec    = '' />
    <cfset var PriceKeywordAfter_dec    = '' />
    <cfset var insertUsersPlan  = '' />
    <cfset var planInfo = '' />
    <cfset var planadd  = '' />
    <cfset var MlpsLimitNumber_int = 2 />
    <cfset var ShortUrlsLimitNumber_int = 5 />
    <cfset var MlpsImageCapacityLimit_bi = 50*1024 />
    <cfset dataout = {}>
    <cfset dataout.RXRESULTCODE = -1 />
    <cfset dataout.USERID = "#arguments.inpUserId#" />
    <cfset dataout.PLANID = "#arguments.inpPlanId#" />
    <cfset dataout.MESSAGE = "" />
    <cfset dataout.ERRMESSAGE = "" />

    
    <cfinvoke method="GetOrderPlan" returnvariable="planInfo">
        <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
    </cfinvoke>    

        <cfif planInfo.RXRESULTCODE GT 0 && inpUserId GT 0>
            <cfif arguments.inpBillingType EQ 1>
                <cfset Amount_dec = planInfo.AMOUNT>
            <cfelse>
                <cfset Amount_dec = planInfo.YEARLYAMOUNT*12>
            </cfif>
            <cfset UserAccountNumber_int = planInfo.USERACCOUNTNUMBER>
            <cfset KeywordsLimitNumber_int = planInfo.KEYWORDSLIMITNUMBER>
            <cfset FirstSMSIncluded_int = planInfo.FIRSTSMSINCLUDED>
            <cfset PriceMsgAfter_dec = planInfo.PRICEMSGAFTER>
            <cfset PriceKeywordAfter_dec = planInfo.PRICEKEYWORDAFTER>
            <cfset MlpsImageCapacityLimit_bi = planInfo.IMAGECAPACITYLIMIT>
            <cfif isNumeric(planInfo.MLPSLIMITNUMBER)>
                <cfset MlpsLimitNumber_int = planInfo.MLPSLIMITNUMBER>
            <cfelse>
                <cfset MlpsLimitNumber_int = 0>
            </cfif>            

            <cfif isNumeric(planInfo.SHORTURLSLIMITNUMBER)>
                <cfset ShortUrlsLimitNumber_int = planInfo.SHORTURLSLIMITNUMBER>
            <cfelse>
                <cfset ShortUrlsLimitNumber_int = 0>
            </cfif>
            
            <cftry>               
                <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
                    INSERT INTO simplebilling.userplans
                    (
                        Status_int,
                        UserId_int,
                        PlanId_int,
                        Amount_dec,
                        UserAccountNumber_int,
                        KeywordsLimitNumber_int,
                        FirstSMSIncluded_int,
                        PriceMsgAfter_dec,
                        PriceKeywordAfter_dec,
                        MlpsLimitNumber_int,
                        ShortUrlsLimitNumber_int,
                        MlpsImageCapacityLimit_bi,
                    <cfif arguments.inpMonthlyAddBenefitDate NEQ ''>
                        MonthlyAddBenefitDate_dt,
                    </cfif>
                        StartDate_dt,
                    EndDate_dt,
                    BillingType_ti
                    
                        )
                    VALUES 
                    (
                        1,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPlanId#">,
                        <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#Amount_dec#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#UserAccountNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#KeywordsLimitNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#FirstSMSIncluded_int#">,
                        <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#PriceMsgAfter_dec#">,
                        <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#PriceKeywordAfter_dec#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#MlpsLimitNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#ShortUrlsLimitNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_bigint" value="#MlpsImageCapacityLimit_bi#">,
                    <cfif arguments.inpMonthlyAddBenefitDate NEQ ''>
                        <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.inpMonthlyAddBenefitDate#">,
                    </cfif>
                        <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.inpStarDate#">,
                    <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.inpEndDate#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBillingType#">
                    
                        )
                </cfquery>

                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = "Add plan success" />
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                    <cfreturn dataout /> 
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "Plan #inpPlanId# Or User #inpUserId# not found" />
            <cfset dataout.ERRMESSAGE = "Plan #inpPlanId# Or User #inpUserId# not found"/>
            <cfreturn dataout />   
        </cfif>  

        <cfreturn dataout />    
    </cffunction>
    
    <cffunction name="updatePaymentWorlDay" output="true" hint="update Payment World Day">
        <cfargument name="planId" type="string" default="0">
        <cfargument name="numberSMS" type="string" default="0">
        <cfargument name="numberKeyword" type="string" default="0">
        <cfargument name="moduleName" type="string" required="no" default="">
        <cfargument name="paymentRespose" type="string" required="yes" default="">
        <cfargument name="inppaymentmethod" type="number" required="no" default="1">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
        <cfargument name="PaymentByUserId" required="no" default="#arguments.inpUserId#">
        <cfargument name="inpAdminUserId" required="no" default="">
        
        
        <cfset var data = '' />
        <cfset var dataout = {}/>
        <cfset var updatePayment = ''/>
        <cfset dataout = {}>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />


        <cfset var planId = "" />
        <cfset var RetUserPlan = "" />
        <cfset var rxUpdatePayment = {} />
        <cftry>

            <cfset data = deserializeJSON(paymentRespose)>
            <cfif arguments.inppaymentmethod eq 1>
                <cfquery name="updatePayment" datasource="#Session.DBSourceEBM#" result="updatePayment">
                    INSERT INTO `simplebilling`.`payment_worldpay` 
                    (   
                        `userId_int`,
                        `planId`, 
                        `moduleName`,
                        `bynumberKeyword`,
                        `bynumberSMS`,
                        `transactionType`, 
                        `customerId`, 
                        `orderId`, 
                        `transactionId`, 
                        `authorizationCode`, 
                        `authorizedAmount`, 
                        `paymentTypeCode`, 
                        `paymentTypeResult`, 
                        `transactionData_date`, 
                        `transactionData_amount`, 
                        `creditCardType`, 
                        `cardNumber`, 
                        `avsCode`, 
                        `cardHolder_FirstName`, 
                        `cardHolder_LastName`, 
                        `billAddress_line1`, 
                        `billAddress_city`, 
                        `billAddress_state`, 
                        `billAddress_zip`, 
                        `billAddress_company`, 
                        `billAddress_phone`, 
                        `email`, 
                        `method`, 
                        `responseText`,
                        `created`,
                        'adminUserid_int',
                        `PaymentByUserId_int`
                        )VALUES 
                    (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.planId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberKeyword#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberSMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionType#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.customerId#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.orderId#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionId#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.authorizationCode#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.authorizedAmount#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.paymentTypeCode#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.paymentTypeResult#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionData.date#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionData.amount#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.creditCardType#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardNumber#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.avsCode#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardHolder_FirstName#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardHolder_LastName#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.line1#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.city#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.state#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.zip#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.company#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.phone#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.email#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.method#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.responseText#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAdminUserId#" null="#NOT len(arguments.inpAdminUserId)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PaymentByUserId#">
                    )
                </cfquery>
            
            <cfelse>    

                    <cfif !structKeyExists(arguments, "planId") || arguments.planId EQ 0>
                        <!--- GET USER PLAN DETAIL --->
                        <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan">
                            <cfinvokeargument name="InpUserId" value="#arguments.inpUserId#"/>
                        </cfinvoke>

                        <cfif RetUserPlan.RXRESULTCODE EQ 1>
                            <cfset planId = RetUserPlan.PLANID/>
                        </cfif>
                    </cfif>

                    <cfquery name="updatePayment" datasource="#Session.DBSourceEBM#" result="rxUpdatePayment">
                    INSERT INTO `simplebilling`.`payment_worldpay` 
                    (   
                        `userId_int`,
                        `planId`, 
                        `moduleName`,
                        `bynumberKeyword`,
                        `bynumberSMS`,
                        `transactionType`,
                        `orderId`, 
                        `transactionId`, 
                        `authorizedAmount`, 
                        `paymentTypeCode`, 
                        `paymentTypeResult`, 
                        `transactionData_amount`, 
                        `cardNumber`, 
                        `avsCode`, 
                        `cardHolder_FirstName`, 
                        `cardHolder_LastName`, 
                        `billAddress_line1`, 
                        `billAddress_city`, 
                        `billAddress_state`, 
                        `billAddress_zip`, 
                        `billAddress_company`, 
                        `billAddress_phone`, 
                        `email`, 
                        `method`, 
                        `responseText`,
                        `paymentGateway_ti`,
                        `created`,
                        `adminUserid_int`,
                        `PaymentByUserId_int`
                        
                        )VALUES 
                    (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#planId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberKeyword#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberSMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONTYPE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.ORDERID#">,   
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPNUMBER#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCVV2#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPFIRSTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLASTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLINE1#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCITY#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPSTATE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPPOSTALCODE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.PHONE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.EMAIL#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.METHOD#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.RESPONSE#">,                     
                    <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inppaymentmethod#">,
                    NOW(),
                    <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAdminUserId#" null="#NOT len(arguments.inpAdminUserId)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PaymentByUserId#">
                    )
                </cfquery>
            </cfif>
           
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    
    <cffunction name="updateLogPaymentWorlPay" hint="update Log PaymentWorldPay">
        <cfargument name="moduleName" type="string" required="no" default="">
        <cfargument name="status_code" type="string" required="no" default="">
        <cfargument name="status_text" type="string" required="no" default="">
        <cfargument name="errordetail" type="string" required="no" default="">
        <cfargument name="filecontent" type="string" required="no" default="">
        <cfargument name="paymentdata" required="no" default="">
        <cfargument name="paymentmethod" type="numeric" required="no" default="1">
        <cfargument name="inpUserId" required="no" default="#Session.USERID#">
        <cfargument name="inpPaymentByUserId" required="no" default="#arguments.inpUserId#">
        
        <cfset var updateLogPayment = '' />
        <cfquery name="updateLogPayment" datasource="#Session.DBSourceEBM#">
            INSERT INTO `simplebilling`.`log_payment_worldpay` 
            (
                `userId_int`,
                `moduleName`,
                `status_code`,
                `status_text`,
                `errordetail`,
                `filecontent`,
                `paymentdata`,
                `paymentGateway_ti`,
                `created`,
                `PaymentByUserId_int`
            ) 
            VALUES 
            (
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.status_code#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.status_text#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.errordetail#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.filecontent#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJSON(arguments.paymentdata)#">,
                <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.paymentmethod#">,
                NOW(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentByUserId#">
            );

        </cfquery>

    </cffunction>
    
    <cffunction name="UnsubscribeKeyword" access="remote" output="true" hint="remove avaiable keyword">
        <cfargument name="numberKeywordUnsubscribe" type="string" required="yes" default="">
        
        <cfset var dataout  = {} />
        <cfset var UnsubscribeNumberMax = '' />
        <cfset var updateUsers  = '' />
        <cfset var getUserPlan  = '' />

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        
        <cftry>
            <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
            </cfinvoke>
            <cfset UnsubscribeNumberMax = 0>

            <cfif getUserPlan.PLANEXPIRED GT 0>
                <cfif getUserPlan.KEYWORDAVAILABLE GT getUserPlan.KEYWORDBUYAFTEREXPRIED>
                    <cfset UnsubscribeNumberMax = getUserPlan.KEYWORDBUYAFTEREXPRIED>
                <cfelse>
                    <cfset UnsubscribeNumberMax = getUserPlan.KEYWORDAVAILABLE>
                </cfif>
            <cfelse>
                <cfif getUserPlan.KEYWORDAVAILABLE GT getUserPlan.KEYWORDPURCHASED>
                    <cfset UnsubscribeNumberMax = getUserPlan.KEYWORDPURCHASED>
                <cfelse>
                    <cfset UnsubscribeNumberMax = getUserPlan.KEYWORDAVAILABLE>
                </cfif>
            </cfif>
            
            <cfif numberKeywordUnsubscribe GT UnsubscribeNumberMax>
             <cfset dataout.RXRESULTCODE = -1 />
             <cfset dataout.MESSAGE = "Cannot Unsubscribe Keyword" />
             <cfreturn dataout>
         </cfif>
         
         <cfif numberKeywordUnsubscribe EQ 0>
             <cfset dataout.RXRESULTCODE = -1 />
             <cfset dataout.MESSAGE = "Cannot Unsubscribe Keyword" />
             <cfreturn dataout>
         </cfif>
         
         <cfif getUserPlan.USERPLANID GT 0>

            <cfif getUserPlan.PLANEXPIRED GT 0>
                <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                    UPDATE  
                    simplebilling.userplans
                    SET     
                    BuyKeywordNumberExpired_int = BuyKeywordNumberExpired_int - <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.numberKeywordUnsubscribe#">
                    WHERE   
                    UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getUserPlan.USERPLANID#">
                </cfquery>

            <cfelse>
                <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                    UPDATE  
                    simplebilling.userplans
                    SET     
                    BuyKeywordNumber_int = BuyKeywordNumber_int - <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.numberKeywordUnsubscribe#">
                    WHERE   
                    UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getUserPlan.USERPLANID#">
                </cfquery>
            </cfif>   
            

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Unsubscribe Success" />
            <cfreturn dataout>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = "Cannot Unscriber Keyword" />
            <cfreturn dataout>
        </cfif>
        
        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -3 />
            <cfset dataout.MESSAGE = "Cannot Unsubscribe Keyword" />
            <cfreturn dataout>
        </cfcatch>
    </cftry>
    
    
</cffunction>

<cffunction name="checkUserPlanExpried" output="true" hint="check if your plan is expried, expried within 30d or over 30 days">
    <cfargument name="userId" type="numeric" required="yes" default="">
        <!---
        RXRESULTCODE :
        -2 : errors
        -1 : user not exit anymore
        0 : user plan is not exprired
        1 : user plan is expired within 30d
        2 : user plan is expired over 30d to run downgrade
        3 : user plan is already downgrade
    --->

    <cfset var dataout = {}>
    <cfset dataout.RXRESULTCODE = 0 />
    <cfset dataout.MESSAGE = "" />
    <cfset dataout.ERRMESSAGE = "" />
    <cfset dataout.DIFFDATE = 0 />
    <cfset dataout.ENDDATE = "" />

    <cfset var userPlansQuery = ''/>

    <cfif arguments.userId GT 0>
        
        <cftry>

            <cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
                SELECT UserPlanId_bi,EndDate_dt,DATEDIFF(EndDate_dt,CURDATE()) AS DiffDate, BuyKeywordNumber_int, BuyKeywordNumberExpired_int, KeywordsLimitNumber_int, DowngradeDate_dt 
                FROM simplebilling.userplans
                WHERE Status_int = 1
                <!--- AND DATE(EndDate_dt) <= CURDATE() --->
                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.userId#">
                ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
            </cfquery>

            <cfif userPlansQuery.RecordCount GT 0>

                <cfset dataout.DIFFDATE = userPlansQuery.DiffDate />
                <cfset dataout.ENDDATE = userPlansQuery.EndDate_dt />

                <cfif userPlansQuery.DiffDate GT 0 AND userPlansQuery.BuyKeywordNumberExpired_int EQ 0>
                    <cfset dataout.RXRESULTCODE = 0 />
                <cfelseif userPlansQuery.DiffDate LT 0 AND userPlansQuery.DiffDate GT -30>                           
                    <cfset dataout.RXRESULTCODE = 1 />
                <cfelseif userPlansQuery.DiffDate LTE -30 >                               
                    <cfset dataout.RXRESULTCODE = 2 />
                <cfelseif userPlansQuery.DowngradeDate_dt NEQ '' >
                    <cfset dataout.RXRESULTCODE = 3 />
                <cfelseif userPlansQuery.DiffDate EQ 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                </cfif>     
            <cfelse> 
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "UserPlan not exits" />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "There is an errors when check user plan expired" />
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                <cfreturn dataout>
            </cfcatch>    
        </cftry>

    <cfelse>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "UserId not exits" />
    </cfif>    

    <cfreturn dataout>
</cffunction>

<cffunction name="GetNextPlanUpgrade" output="false" access="remote" hint="Get user plan upgrade">
    <cfargument name="inpPlanOrder" type="numeric" required="true">

    <cfset var dataout = {} />
    <cfset var nextPlanUpgrade = structNew() />

    <!--- Prepare return data --->
    <cfset dataout.RXResultCode = -1 />
    <cfset dataout.PlanId_int = 0 />

    <cfset dataout.message = "" />
    <cfset dataout.type = "" />
    <cfset dataout.errMessage = "" />

    <cftry>
        <cfquery name="nextPlanUpgrade" datasource="#Session.DBSourceREAD#">
            SELECT
            PlanId_int
            FROM
            simplebilling.plans
            WHERE
            Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
            AND
            Order_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanOrder#">
            ORDER BY
            Order_int
            LIMIT
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
        </cfquery>

        <cfset dataout.RXResultCode = 1 />

        <cfif nextPlanUpgrade.RECORDCOUNT GT 0>
            <cfset dataout.PlanId_int = nextPlanUpgrade.PlanId_int />
        </cfif>

        <cfcatch>
            <cfset dataout.type = "#cfcatch.Type#" />
            <cfset dataout.message = "#cfcatch.Message#" />
            <cfset dataout.errMessage = "#cfcatch.detail#" />
        </cfcatch>
    </cftry>

    <cfreturn dataout />

</cffunction>

<cffunction name="GetUsedMLPsByUserId" output="true" hint="Get number of user's used MLPs">
    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />

    <cfset var getUsedMLPS = '' />

    <cftry>
        <cfquery name="getUsedMLPS" datasource="#Session.DBSourceREAD#">
            SELECT
            ccpxDataId_int
            FROM
            simplelists.cppx_data
            WHERE
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
                status_int <> -1
            AND
                MlpType_ti = 1
        </cfquery>
        <cfset dataout.usedMLP = getUsedMLPS.RecordCount>
        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.Message#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
    </cftry>

    <cfreturn dataout />
</cffunction>

<cffunction name="GetUsedShortURLsByUserId" output="true" hint="Get number of user's used ShortURLs">
    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />

    <cfset var getUsedShortURL = ''/>

    <cftry>
        <cfquery name="getUsedShortURL" datasource="#Session.DBSourceREAD#">
            SELECT
            PKId_bi
            FROM
            simplelists.url_shortner
            WHERE
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
            AND
            Active_int = 1
            AND   
            Type_ti = 1  
        </cfquery>
        <cfset dataout.usedURL = getUsedShortURL.RecordCount>
        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.Message#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
    </cftry>
    
    <cfreturn dataout />
</cffunction>

<cffunction name="getPlanUpgradeInfo" access="remote" hint="get Plan Upgrade Info" output="true">
    <cfargument name="inpPlanId" type="numeric" required="true">
    <cfargument name="inpPlanType" type="numeric" required="false" default="0">

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset dataout.TOTALAMOUNT = 0 />
    <cfset dataout.PLANSTATUS = 1 />
    <cfset var BuyKeywordNumber = 0>
    <cfset var totalKeyword = 0>
    <cfset var totalAMOUNT = 0>
    <cfset var getUserPromotion = ''>
    <cfset var getPromotion = ''>
    <cfset var getUserPlan = ''>
    <cfset var price = ''>
    <cfset var discount_plan_price_percent = 0>
    <cfset var discount_plan_price_flat_rate = 0>
    <cfset var TotalSentReceived = ''>
    <cfset var _numberCreditUsing = ''>
    <cfset var _priceMesAfter = ''>
    
    

    <cftry>
        <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan"></cfinvoke>
        <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
            <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
        </cfinvoke>        
            <cfset dataout.PLANSTATUS = price.STATUS />    
            <!--- When upgrate plan get plan keyword limit number https://setaintl2008.atlassian.net/browse/SIRE-965 --->
            <cfset totalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED > 

        <cfif totalKeyword GT price.KEYWORDSLIMITNUMBER>
              <cfset BuyKeywordNumber = (totalKeyword - price.KEYWORDSLIMITNUMBER)>
        </cfif>

          <cfquery name="getUserPromotion" datasource="#Session.DBSourceREAD#">
              SELECT 
              up.PromotionId_int,
              up.PromotionLastVersionId_int,
              up.UsedDate_dt,
              up.RecurringTime_int
              FROM 
              simplebilling.userpromotions up 
              WHERE
              up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
              AND up.UsedTimeByRecurring_int < up.RecurringTime_int
              AND up.PromotionStatus_int = 1
              LIMIT 1
          </cfquery>

          <cfset totalAMOUNT = (price.AMOUNT + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>

          <!--- Check User Have Promotion --->
          <cfif getUserPromotion.RecordCount GT 0>

            <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
                SELECT
                pc.promotion_id,
                pc.origin_id,
                pc.discount_type_group,
                pc.discount_plan_price_percent,
                pc.discount_plan_price_flat_rate,
                pc.promotion_keyword,
                pc.promotion_MLP,
                pc.promotion_short_URL,
                pc.promotion_credit
                FROM 
                simplebilling.promotion_codes pc 
                WHERE
                pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPromotion.PromotionId_int#">
                AND 
                pc.promotion_status  = 1
                LIMIT 1
            </cfquery>

            <!--- Have a Promotion --->
            <cfif getUserPromotion.RecordCount GT 0>
                <cfif getPromotion.discount_type_group EQ 1>
                    <cfset totalAMOUNT = (price.AMOUNT - (price.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
                    <cfset discount_plan_price_percent = getPromotion.discount_plan_price_percent/>
                    <cfset discount_plan_price_flat_rate = getPromotion.discount_plan_price_flat_rate/>
                </cfif>

            </cfif>
        </cfif>
        <!--- from postpaid to monthly--->        
        <cfif Listfind("4,2",getUserPlan.PLANSTATUS) GT 0 AND Listfind("4,2",price.STATUS) EQ 0>
            <cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
                SELECT                        
                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                FROM
                    simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                LEFT JOIN
                    simpleobjects.useraccount as ua
                ON
                    ire.UserId_int = ua.UserId_int					
                WHERE
                    LENGTH(ire.ContactString_vch) < 14
                AND 
                    ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#" >	
                AND
                    ua.IsTestAccount_ti = 0                    
                AND
                    ire.Created_dt
                BETWEEN 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getUserPlan.STARTDATE#">
                AND 
                    
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getUserPlan.ENDDATE#">, INTERVAL 86399 SECOND)
                GROUP BY
                    ire.UserId_int     
            </cfquery>                        
            <cfset _numberCreditUsing=0>					
            <cfset _priceMesAfter=LSParseNumber(getUserPlan.PRICEMSGAFTER)>
            <cfif TotalSentReceived.RecordCount GT 0>											
                <cfset _numberCreditUsing= LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received)>					
                <cfset totalAMOUNT =totalAMOUNT + (_numberCreditUsing * _priceMesAfter / 100)>	
            <cfelse>
                
            </cfif>	
        </cfif>
        <cfset dataout.TOTALAMOUNT = totalAMOUNT />
        <cfset dataout.RXRESULTCODE = 1 />

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.Message#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
    </cftry>
    
    <cfreturn dataout />        

</cffunction> 


<cffunction name="getPurchaseAmout" access="remote" hint="get Purchase Amout by planId" output="true">
    <cfargument name="inpPlanId" type="numeric" required="true">
    <cfargument name="inpPromotionCode" type="string" required="no">
    <cfargument name="inpPlanType" type="numeric" required="no" default="0">    
    <cfargument name="inpLastPlanId" type="numeric" required="no" default="0" hint="check if last plain is postpaid then need add amount at the time change plan">
    <cfargument name="inpUserId" type="string" required="no" default="#Session.UserId#">
    <!--- inpPlanType=1 for monthly plan, 2 for yearly plan --->

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset dataout.TOTALAMOUNT = 0 />
    <cfset dataout.INCLUDELASTPOSTPAIDPLANAMOUNT = 0 />
    <cfset var BuyKeywordNumber = 0>
    <cfset var totalKeyword = 0>
    <cfset var totalAMOUNT = 0>
    <cfset var getUserPromotion = ''>
    <cfset var getPromotion = ''>
    <cfset var getUserPlan = ''>
    <cfset var price = ''>
    <cfset var discount_plan_price_percent = 0>
    <cfset var discount_plan_price_flat_rate = 0>
    <cfset var INP_PERCENT = 0>
    <cfset var INP_FLATRATE = 0>
    <cfset var PERCENT_CC = 0>
    <cfset var FLATRATE_CC = 0>
    <cfset var inpPC = ''/>
    <cfset var checkpromocode = {}/>
    <cfset var promocode = {}/>
    <cfset var GetLastPlanInfo = ''>
    <cfset var TotalSentReceived = ''>
    <cfset var priceBuyKeywordNumber = 0/>
    <cfset var userAffiliateCode=''>
    <cfset var rtGetAffiliateCode=''>
    <cfset var GetUserInfo=''>
    <cfset var rtGetCouponFromAffiliateCode	= '' />
    

    <cftry>
        <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan"></cfinvoke>
        <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
            <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
        </cfinvoke>    
            
        <!--- When upgrate plan get plan keyword limit number https://setaintl2008.atlassian.net/browse/SIRE-965 --->
        <cfset totalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED + getUserPlan.TOTALKEYWORDHAVETOPAID > 
        
        <cfif totalKeyword GT price.KEYWORDSLIMITNUMBER>
            <cfset BuyKeywordNumber = (totalKeyword - price.KEYWORDSLIMITNUMBER)>
        </cfif>

        <cfif BuyKeywordNumber GT 0>
            <cfset priceBuyKeywordNumber = BuyKeywordNumber * price.PRICEKEYWORDAFTER />
        </cfif>

        <!--- <cfif getUserPlan.BILLINGTYPE EQ 1>
            <cfset totalAmount = price.AMOUNT >    
        <cfelse>
            <cfset totalAmount = price.YEARLYAMOUNT*12 >    
        </cfif> --->
          
        <cfif arguments.inpPlanType EQ 1>
            <cfset totalAmount = price.AMOUNT + priceBuyKeywordNumber>
        <cfelse>
            <cfset totalAmount = (price.YEARLYAMOUNT*12) + priceBuyKeywordNumber >
        </cfif>
        

          <!---   <cfquery name="getUserPromotion" datasource="#Session.DBSourceREAD#">
              SELECT 
                up.PromotionId_int,
                up.PromotionLastVersionId_int,
                up.UsedDate_dt,
                up.RecurringTime_int
              FROM 
                simplebilling.userpromotions up 
              WHERE
                  up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
                  AND up.UsedTimeByRecurring_int < up.RecurringTime_int
                  AND up.PromotionStatus_int = 1
              LIMIT 1
          </cfquery> --->

          <!--- Check User Have Promotion --->
            <!---             
            <cfif getUserPromotion.RecordCount GT 0>
                <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
                    SELECT
                      pc.promotion_id,
                      pc.origin_id,
                      pc.discount_type_group,
                      pc.discount_plan_price_percent,
                      pc.discount_plan_price_flat_rate,
                      pc.promotion_keyword,
                      pc.promotion_MLP,
                      pc.promotion_short_URL,
                      pc.promotion_credit
                    FROM 
                        simplebilling.promotion_codes pc 
                    WHERE
                        pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPromotion.PromotionId_int#">
                    AND 
                        pc.promotion_status  = 1
                    LIMIT 1
                </cfquery>

                <!--- Have a Promotion --->
                <cfif getUserPromotion.RecordCount GT 0>
                    <cfif getPromotion.discount_type_group EQ 1>
                        <cfset totalAMOUNT = (price.AMOUNT - (price.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
                        <cfset discount_plan_price_percent = getPromotion.discount_plan_price_percent/>
                        <cfset discount_plan_price_flat_rate = getPromotion.discount_plan_price_flat_rate/>
                    </cfif>

                </cfif>
            </cfif> --->

                <cfset inpPC = "#arguments.inpPromotionCode#">
                <!--- check coupon linkage affiliate--->
                <cfif arguments.inpPromotionCode EQ ''>
                    
                    <cfquery name="GetUserInfo" datasource="#Session.DBSourceREAD#">
                        SELECT
                            u.amc_vch
                        FROM                            
                            simpleobjects.useraccount u
                        WHERE
                            u.UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 					
                    </cfquery>
                    
                    <cfset userAffiliateCode=GetUserInfo.amc_vch>
                    <cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
                        <cfinvokeargument name="inpAffiliateCode" value="#userAffiliateCode#"/>
                    </cfinvoke>
                    
                    <cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>                           
                        <cfset arguments.inpPromotionCode =rtGetCouponFromAffiliateCode.COUPONCODE >
                    </cfif>
                </cfif>
                <cfif arguments.inpPromotionCode NEQ ''>

                <cfinvoke component="session.sire.models.cfc.promotion" method="CheckCouponForUpgradePlan" returnvariable="checkpromocode">
                  <cfinvokeargument name="inpCouponCode" value="#arguments.inpPromotionCode#">
                  </cfinvoke>
                    
                    
                  <cfif checkpromocode.RXRESULTCODE EQ 1>

                    <!--- coupon input --->
                    <cfinvoke component="session.sire.models.cfc.promotion" method="GetCouponDetails" returnvariable="promocode">
                        <cfinvokeargument name="inpCouponId" value="#checkpromocode.promotion_id#">
                        </cfinvoke>

                        <cfset INP_PERCENT = promocode.COUPONDISCOUNTPRICEPERCENT>
                        <cfset INP_FLATRATE = promocode.COUPONDISCOUNTPRICEFLATRATE >
                    </cfif>

                    <cfset PERCENT_CC = INP_PERCENT + discount_plan_price_percent>
                    <cfset FLATRATE_CC = INP_FLATRATE + discount_plan_price_flat_rate>
                    <cfset totalAmount = (totalAmount - (totalAmount * PERCENT_CC / 100) - FLATRATE_CC)>

                </cfif>

                <cfif totalAmount LT 0>
                    <cfset totalAmount = 0/>
                </cfif>
                <!--- include amount last postpaid plan--->
                <cfif arguments.inpLastPlanId GT 0>
                    
                    <cfquery name="GetLastPlanInfo" datasource="#Session.DBSourceREAD#">
                        SELECT
                            p.Status_int as PlanStatus,
                            up.PriceMsgAfter_dec,
                            up.StartDate_dt,
                            up.EndDate_dt
                        FROM 
                            simplebilling.userplans up
                        INNER JOIN
                            simplebilling.plans p
                            ON      up.PlanId_int=p.PlanId_int
                            AND     up.Status_int=1
                        WHERE
                            up.PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpLastPlanId#">   
                        AND
                            up.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >	                     
                    </cfquery>
                    <cfloop query="GetLastPlanInfo">
                        <cfif PlanStatus EQ 4>
                            <cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
                                SELECT                        
                                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                                FROM
                                    simplexresults.ireresults as ire 
                                LEFT JOIN
                                    simpleobjects.useraccount as ua
                                ON
                                    ire.UserId_int = ua.UserId_int					
                                WHERE
                                    LENGTH(ire.ContactString_vch) < 14
                                AND 
                                    ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >	
                                AND
                                    ua.IsTestAccount_ti = 0                    
                                AND
                                    ire.Created_dt
                                BETWEEN 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#StartDate_dt#">
                                AND 
                                    
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#EndDate_dt#">, INTERVAL 86399 SECOND)
                                GROUP BY
                                    ire.UserId_int     
                            </cfquery>
                            <cfif TotalSentReceived.RecordCount GT 0>
                                <cfset dataout.INCLUDELASTPOSTPAIDPLANAMOUNT =  (LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received) )*PriceMsgAfter_dec/100 />											                                
                            </cfif>	                            
                        </cfif>
                    </cfloop>
                </cfif>
                <cfset totalAmount=totalAmount+ dataout.INCLUDELASTPOSTPAIDPLANAMOUNT/>
                <cfset dataout.TOTALAMOUNT = totalAmount />
                <cfset dataout.RXRESULTCODE = 1 />
                                
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />        

        </cffunction>

        <cffunction name="getPurchaseAmount1" access="remote" hint="get Purchase Amount" output="true">
            <cfargument name="inpPlanId" type="numeric" required="true">
            <cfargument name="inpPromotionCode" type="string" required="no">
            <cfargument name="inpPlanType" type="numeric" required="no" default="0">
            <cfargument name="inpUserId" type="numeric" required="no" default="0">
            <!--- inpPlanType=1 for monthly plan, 2 for yearly plan --->

            <cfset var dataout = {} />
            <cfset dataout.RXRESULTCODE = '' />
            <cfset dataout.MESSAGE = '' />
            <cfset dataout.ERRMESSAGE = '' />
            <cfset dataout.TOTALAMOUNT = 0 />
            <cfset var BuyKeywordNumber = 0>
            <cfset var totalKeyword = 0>
            <cfset var totalAMOUNT = 0>
            <cfset var getUserPromotion = ''>
            <cfset var getPromotion = ''>
            <cfset var getUserPlan = ''>
            <cfset var price = ''>
            <cfset var discount_plan_price_percent = 0>
            <cfset var discount_plan_price_flat_rate = 0>
            <cfset var INP_PERCENT = 0>
            <cfset var INP_FLATRATE = 0>
            <cfset var PERCENT_CC = 0>
            <cfset var FLATRATE_CC = 0>
            <cfset var inpPC = ''/>
            <cfset var checkpromocode = {}/>
            <cfset var promocode = {}/>
            <cfset var priceBuyKeywordNumber = 0 />
            <cfset var userAffiliateCode=''>
            <cfset var rtGetAffiliateCode=''>
            <cfset var GetUserInfo=''>
            <cfset var rtGetCouponFromAffiliateCode	= '' />

            <cftry>
                <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
                    <cfinvokeargument name="InpUserId" value="#arguments.inpUserId#"/>
                </cfinvoke>
                <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
                    <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                </cfinvoke>
                    
                <!--- When upgrate plan get plan keyword limit number https://setaintl2008.atlassian.net/browse/SIRE-965 --->
                <cfset totalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED + getUserPlan.TOTALKEYWORDHAVETOPAID > 

                <cfif totalKeyword GT price.KEYWORDSLIMITNUMBER>
                    <cfset BuyKeywordNumber = (totalKeyword - price.KEYWORDSLIMITNUMBER)>
                </cfif>

                <cfif BuyKeywordNumber GT 0>
                    <cfset priceBuyKeywordNumber = BuyKeywordNumber * price.PRICEKEYWORDAFTER />
                </cfif>
                  
                <cfif arguments.inpPlanType EQ 1>
                    <cfset totalAmount = price.AMOUNT + priceBuyKeywordNumber >
                <cfelse>
                    <cfset totalAmount = (price.YEARLYAMOUNT*12) + priceBuyKeywordNumber >
                </cfif>

                  
                <!--- check coupon linkage affiliate--->
                <cfif arguments.inpPromotionCode EQ ''>
                    
                    <cfquery name="GetUserInfo" datasource="#Session.DBSourceREAD#">
                        SELECT
                            u.amc_vch
                        FROM                            
                            simpleobjects.useraccount u
                        WHERE
                            u.UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> 					
                    </cfquery>
                    
                    <cfset userAffiliateCode=GetUserInfo.amc_vch>
                    <cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
                        <cfinvokeargument name="inpAffiliateCode" value="#userAffiliateCode#"/>
                    </cfinvoke>
                    
                    <cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>                           
                        <cfset arguments.inpPromotionCode =rtGetCouponFromAffiliateCode.COUPONCODE >
                    </cfif>
                </cfif>
                <cfset inpPC = "#arguments.inpPromotionCode#">
                <cfif arguments.inpPromotionCode NEQ ''>             

                <cfinvoke component="session.sire.models.cfc.promotion" method="CheckCouponForUpgradePlan" returnvariable="checkpromocode">
                  <cfinvokeargument name="inpCouponCode" value="#arguments.inpPromotionCode#">
                  </cfinvoke>

                  <cfif checkpromocode.RXRESULTCODE EQ 1>

                    <!--- coupon input --->
                    <cfinvoke component="session.sire.models.cfc.promotion" method="GetCouponDetails" returnvariable="promocode">
                        <cfinvokeargument name="inpCouponId" value="#checkpromocode.promotion_id#">
                        </cfinvoke>

                        <cfset INP_PERCENT = promocode.COUPONDISCOUNTPRICEPERCENT>
                        <cfset INP_FLATRATE = promocode.COUPONDISCOUNTPRICEFLATRATE >
                    </cfif>

                    <cfset PERCENT_CC = INP_PERCENT + discount_plan_price_percent>
                    <cfset FLATRATE_CC = INP_FLATRATE + discount_plan_price_flat_rate>
                    <cfset totalAmount = (totalAmount - (totalAmount * PERCENT_CC / 100) - FLATRATE_CC)>

                </cfif>

                <cfif totalAmount LT 0>
                    <cfset totalAmount = 0/>
                </cfif>

                <cfset dataout.TOTALAMOUNT = totalAmount />
                <cfset dataout.RXRESULTCODE = 1 />

                <!--- <cfdump var="#dataout#" abort="true"> --->
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />        

        </cffunction>
        

        <cffunction name="getUserDowngradeInfo" access="remote" hint="get User Downgrade Info by planid" output="true">
            <cfargument name="inpPlanId" type="numeric" required="true">

            <cfset var dataout = {} />
            <cfset dataout.RXRESULTCODE = '' />
            <cfset dataout.MESSAGE = '' />
            <cfset dataout.ERRMESSAGE = '' />
            <cfset dataout.KEYWORDAVAILABLE = 0 />
            <cfset dataout.SHORTURLAVAIABLE = 0 />
            <cfset dataout.MLPAVAIABLE = 0 />
            <cfset dataout.NOTE = '' />

            <cfset var BuyKeywordNumber = 0>
            <cfset var totalKeyword = 0>
            <cfset var totalAMOUNT = 0>
            <cfset var getUserPromotion = ''>
            <cfset var getPromotion = ''>
            <cfset var getUserPlan = ''>
            <cfset var downgradePlan = ''>
            <cfset var RetUserPlan = ''/>
            <cfset var RetUsedMLP = ''/>
            <cfset var RetUsedShortURL = ''/>

            <cftry>
                
                <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="downgradePlan">
                    <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
                    </cfinvoke>
                    
                    <cfif downgradePlan.RXRESULTCODE EQ 1>
                        <!--- GET USER PLAN DETAIL --->
                        <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>
                        
                        <cfif RetUserPlan.RXRESULTCODE EQ 1>
                            <!--- GET NUMBER OF USED MLPS BY USER --->
                            <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>
                            
                            <!--- GET NUMBER OF USED SHORT URLS BY USER --->
                            <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>
                            
                            <cfif downgradePlan.SHORTURLSLIMITNUMBER NEQ 'Unlimited'> <!--- NOT IN CASE UNLIMITED --->
                                <cfset dataout.SHORTURLAVAIABLE = downgradePlan.SHORTURLSLIMITNUMBER - RetUsedShortURL.USEDURL  />    
                            </cfif>
                            
                            <cfif downgradePlan.MLPSLIMITNUMBER NEQ 'Unlimited'> <!--- NOT IN CASE UNLIMITED --->
                                <cfset dataout.MLPAVAIABLE = downgradePlan.MLPSLIMITNUMBER - RetUsedMLP.USEDMLP  />    
                            </cfif>

                            <cfset dataout.KEYWORDAVAILABLE = downgradePlan.KEYWORDSLIMITNUMBER + RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED - RetUserPlan.KEYWORDUSE  />

                            <cfif dataout.KEYWORDAVAILABLE LT 0 >
                              <cfset dataout.KEYWORDNOTE = '
                              <p>The downgraded plan includes '&#downgradePlan.KEYWORDSLIMITNUMBER + RetUserPlan.KEYWORDPURCHASED#&' Keywords.  Select the Keywords you would like to remove.  If you choose to keep more than '&downgradePlan.KEYWORDSLIMITNUMBER + RetUserPlan.KEYWORDPURCHASED&', you will be billed $'&RetUserPlan.PRICEKEYWORDAFTER&'/month for each additional Keyword.</p>
                              <p>Oh, by the way, any Keyword you choose to remove will remain on hold in your account for 30 days in case you change your mind and want to upgrade again. </p>'
                              />    
                          </cfif>    

                          <cfif dataout.SHORTURLAVAIABLE LT 0 >
                              <cfset dataout.SHORTURLNOTE = '
                              <p>The downgraded plan includes '&#downgradePlan.SHORTURLSLIMITNUMBER#&' ShortUrl.  Select the ShortUrl you would like to remove.</p>
                              <p>Oh, by the way, any ShortUrl you choose to remove will remain on hold in your account for 30 days in case you change your mind and want to upgrade again. </p>'
                              />    
                          </cfif>    

                          <cfif dataout.MLPAVAIABLE LT 0 >
                              <cfset dataout.MLPNOTE = '
                              <p>The downgraded plan includes '&#downgradePlan.MLPSLIMITNUMBER#&' MLP.  Select the MLP you would like to remove.</p>
                              <p>Oh, by the way, any MLP you choose to remove will remain on hold in your account for 30 days in case you change your mind and want to upgrade again. </p>'
                              />    
                          </cfif>    

                          <cfset dataout.RXRESULTCODE = 1 />

                      <cfelse>
                        <cfset dataout.MESSAGE = "User Plan not found." />    
                    </cfif>
                <cfelse>
                    <cfset dataout.MESSAGE = "Plan not found." />
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />        

        </cffunction>  

<cffunction name="doDowngrade" access="remote" hint="do Downgrade" output="true">
    <cfargument name="inpPlanId" type="numeric" required="true">
    <cfargument name="inpListKeyword" default="">
    <cfargument name="inpListShortUrl" default="">
    <cfargument name="inpListMLP" default="">
    <cfargument name="inpUserId" default="" required="false">

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset var resultClearKeywordById = ''/>
    <cfset var updateShortUrl = ''/>
    <cfset var updateCPP = ''/>
    <cfset var result = ''/>
    <cfset var getUserPlan = ''/>
    <cfset var updateUserPlan = ''/>
    <cfset var CouponForUser = ''/>
    <cfset var userId = ''/>

    <cfif !isArray(arguments.inpListKeyword) >
        <cfset arguments.inpListKeyword = [] />
    </cfif>    
    
    <cfif !isArray(arguments.inpListShortUrl)>
        <cfset arguments.inpListShortUrl = [] />
    </cfif>    

    <cfif !isArray(arguments.inpListMLP)>
        <cfset arguments.inpListMLP = [] />
    </cfif>

    <cfset userId = arguments.inpUserId EQ '' ? Session.USERID : arguments.inpUserId/>

    <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan"></cfinvoke>

    <cftransaction>
        <cftry>
            <cftransaction action="begin">
                <!--- Disable Keyword --->    
                <cfif arrayLen(arguments.inpListKeyword) GT 0>
                    <cfinvoke component="session.sire.models.cfc.control-point" method="ClearKeywordById" returnvariable="resultClearKeywordById">
                        <cfinvokeargument name="inpKeywordId" value="#arguments.inpListKeyword#">
                        <cfinvokeargument name="inpShortCode" value="#SESSION.SHORTCODE#">
                        <cfinvokeargument name="inpDisable" value="0">
                        <cfinvokeargument name="inpEndDate" value="#getUserPlan.ENDDATE#">
                    </cfinvoke>

                    <cfif resultClearKeywordById.RXRESULTCODE NEQ 1>
                    <cftransaction action="rollback">
                        <cfset dataout.ERRMESSAGE = "Can not remove keyword" />
                        <cfreturn dataout />  
                    </cfif>
                </cfif>
                                

                <!--- Disable MLP --->    
                <cfif arrayLen(arguments.inpListMLP) GT 0>
                    <cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
                        UPDATE 
                            simplelists.cppx_data
                        SET 
                            <!--- status_int = 2, --->
                            UserDowngradeDate_dt = <cfqueryparam cfsqltype="cf_sql_date" value="#getUserPlan.ENDDATE#"/> 
                        WHERE  
                            ccpxDataId_int IN (#arrayToList(arguments.inpListMLP)#)
                        AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">     
                    </cfquery>
                </cfif>

                <!--- Disable SHORTURL --->    
                <cfif arrayLen(arguments.inpListShortUrl) GT 0>
                   <cfquery name="updateShortUrl" datasource="#Session.DBSourceEBM#" >
                        UPDATE
                        simplelists.url_shortner
                        SET
                            <!--- Active_int = 2, --->
                            UserDowngradeDate_dt = <cfqueryparam cfsqltype="cf_sql_date" value="#getUserPlan.ENDDATE#"/>       
                        WHERE
                            PKId_bi IN (#arrayToList(arguments.inpListShortUrl)#)
                        AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
                    </cfquery>    
                </cfif>

                <!--- update downgrade time in userplan --->
                <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplebilling.userplans
                    SET
                        UserDowngradeDate_dt = now(),
                        UserDowngradePlan_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#">,
                        UserDowngradeKeyword_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(arguments.inpListKeyword)#">,
                        UserDowngradeMLP_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(arguments.inpListMLP)#">,
                        UserDowngradeShortUrl_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(arguments.inpListShortUrl)#">,
                        UserIdDowngrade_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
                    AND 
                        Status_int = 1    
                </cfquery>

                <!--- Remove Coupon on this account --->
                <cfinvoke component="public.sire.models.cfc.promotion" method="DeactiveUserCoupon" returnvariable="CouponForUser">
                    <cfinvokeargument name="inpUserId" value="#userId#">
                </cfinvoke>

                <cfset arguments.AdminId = SESSION.UserId/>
                <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#userId#">
                    <cfinvokeargument name="moduleName" value="Downgrade Process">
                    <cfinvokeargument name="operator" value="#serializeJSON(arguments)#">
                </cfinvoke>    

                <cftransaction action="commit">    
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'This account will be downgraded on its next recurring date!'/>
                <cfcatch type="any">
                    <cftransaction action="rollback">
                        <cfset dataout.RXRESULTCODE = -100 />
                        <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                    </cfcatch>
                </cftry>

            </cftransaction>    
                                                
    <cfreturn dataout />        

</cffunction>  


<cffunction name="getUserUpgradeInfo" access="remote" hint="get User Upgrade Info by planid" output="true">
    <cfargument name="inpPlanId" type="numeric" required="true">

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset dataout.KEYWORDAVAILABLE = 0 />
    <cfset dataout.SHORTURLAVAIABLE = 0 />
    <cfset dataout.MLPAVAIABLE = 0 />
    <cfset dataout.NOTE = '' />
    <cfset dataout.SHORTURLNOTE = ''/>
    <cfset dataout.MLPNOTE = ''/>
    <cfset dataout.TOTALMLPENABLE = 0/>
    <cfset dataout.TOTALSHORTURLENABLE = 0/>
    <cfset dataout.TOTALKEYWORDENABLE = 0/>

    <cfset dataout.listKeywordEnable = []/>
    <cfset dataout.listShortUrlEnable = []/>
    <cfset dataout.listMLPEnable = []/>

    <cfset var BuyKeywordNumber = 0>
    <cfset var totalKeyword = 0>
    <cfset var totalAMOUNT = 0>
    <cfset var getUserPromotion = ''>
    <cfset var getPromotion = ''>
    <cfset var getUserPlan = ''>
    <cfset var upgradePlan = ''>
    <cfset var RetUserPlan = ''>
    <cfset var RetUsedMLP = ''>
    <cfset var RetUsedShortURL = ''>
    <cfset var getKeyword = ''>
    <cfset var getShortUrl = ''>
    <cfset var getMLP = ''>

    <cftry>
        
        <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="upgradePlan">
            <cfinvokeargument name="plan" value="#arguments.inpPlanId#">
            </cfinvoke>
            
            <cfif upgradePlan.RXRESULTCODE EQ 1>
                <!--- GET USER PLAN DETAIL --->
                <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>
                
                <cfif RetUserPlan.RXRESULTCODE EQ 1>
                    <!--- GET NUMBER OF USED MLPS BY USER --->
                    <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>
                    
                    <!--- GET NUMBER OF USED SHORT URLS BY USER --->
                    <cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>
                    
                    <cfif upgradePlan.SHORTURLSLIMITNUMBER NEQ 'Unlimited'> <!--- NOT IN CASE UNLIMITED --->
                        <cfset dataout.SHORTURLAVAIABLE = upgradePlan.SHORTURLSLIMITNUMBER - RetUsedShortURL.USEDURL  />    
                    </cfif>
                    
                    <cfif upgradePlan.MLPSLIMITNUMBER NEQ 'Unlimited'> <!--- NOT IN CASE UNLIMITED --->
                        <cfset dataout.MLPAVAIABLE = upgradePlan.MLPSLIMITNUMBER - RetUsedMLP.USEDMLP  />    
                    </cfif>

                    <cfset dataout.KEYWORDAVAILABLE = upgradePlan.KEYWORDSLIMITNUMBER + RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED - RetUserPlan.KEYWORDUSE  />

                    <cfif dataout.KEYWORDAVAILABLE GT 0 >
                        <cfquery name="getKeyword" datasource="#Session.DBSourceEBM#">
                            SELECT
                            kw.Keyword_vch,
                            kw.Created_dt,
                            kw.KeywordId_int,
                            kw.BatchId_bi
                            FROM sms.keyword AS kw
                            INNER JOIN simpleobjects.batch AS ba 
                            ON kw.BatchId_bi = ba.BatchId_bi 
                            WHERE kw.Active_int = 2
                            AND ba.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
                            <!--- AND kw.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#SESSION.SHORTCODE#"> --->
                            GROUP BY 
                            kw.Keyword_vch
                            ORDER BY
                            kw.Created_dt
                            DESC
                        </cfquery>
                        <cfif getKeyword.RecordCount GT 0>
                            <cfloop query="#getKeyword#">
                                <cfset arrayAppend(dataout.listKeywordEnable, KeywordId_int)>     
                            </cfloop>
                        </cfif>

                        <cfif arrayLen(dataout.listKeywordEnable) GT 0 AND dataout.KEYWORDAVAILABLE GT 0>
                            <cfset dataout.TOTALKEYWORDENABLE = arrayLen(dataout.listKeywordEnable)/>
                        </cfif>

                        <cfset dataout.KEYWORDNOTE = 'You can enable '&dataout.KEYWORDAVAILABLE&' keywords'/> 
                    </cfif>    

                    <cfif dataout.SHORTURLAVAIABLE GT 0 OR upgradePlan.SHORTURLSLIMITNUMBER EQ 'Unlimited' >

                        <cfquery name="getShortUrl" datasource="#Session.DBSourceEBM#">
                            SELECT 
                            PKId_bi 
                            FROM 
                            simplelists.url_shortner
                            WHERE 
                            Active_int = 2
                            AND 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
                            ORDER BY 
                            PKId_bi DESC    
                        </cfquery>

                        <cfif getShortUrl.RecordCount GT 0>
                            <cfloop query="#getShortUrl#">
                                <cfset arrayAppend(dataout.listShortUrlEnable, PKId_bi)>      
                            </cfloop>
                        </cfif>

                        <cfif arrayLen(dataout.listShortUrlEnable) GT 0 AND (dataout.SHORTURLAVAIABLE GT 0 OR upgradePlan.SHORTURLSLIMITNUMBER EQ 'Unlimited')>
                            <cfset dataout.TOTALSHORTURLENABLE = arrayLen(dataout.listShortUrlEnable)/>    
                        </cfif>

                        <cfif upgradePlan.SHORTURLSLIMITNUMBER EQ 'Unlimited'>
                            <cfset dataout.SHORTURLAVAIABLE = 'Unlimited'>
                        </cfif>
                        
                        <cfset dataout.SHORTURLNOTE = 'You can enable '&dataout.SHORTURLAVAIABLE&' short urls'/> 
                        
                    </cfif>    

                    <cfif dataout.MLPAVAIABLE GT 0 OR upgradePlan.MLPSLIMITNUMBER EQ 'Unlimited' >

                        <cfquery name="getMLP" datasource="#Session.DBSourceEBM#">
                            SELECT 
                            ccpxDataId_int 
                            FROM 
                            simplelists.cppx_data
                            WHERE 
                            status_int = 2
                            AND 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
                            AND 
                            MlpType_ti = 1
                            ORDER BY 
                            ccpxDataId_int DESC     
                        </cfquery>
                        
                        <cfif getMLP.RecordCount GT 0>
                            <cfloop query="#getMLP#">
                                <cfset arrayAppend(dataout.listMLPEnable, ccpxDataId_int)>        
                            </cfloop>
                        </cfif>

                        <cfif arrayLen(dataout.listMLPEnable) GT 0 AND ( dataout.MLPAVAIABLE GT 0 OR upgradePlan.MLPSLIMITNUMBER EQ 'Unlimited')>
                            <cfset dataout.TOTALMLPENABLE = arrayLen(dataout.listMLPEnable)/>    
                        </cfif>

                        <cfif upgradePlan.MLPSLIMITNUMBER EQ 'Unlimited' >
                            <cfset dataout.MLPAVAIABLE = 'Unlimited'/>    
                        </cfif>

                        <cfset dataout.MLPNOTE = 'You can enable'&dataout.MLPAVAIABLE&' MLP'/> 
                    </cfif>    

                    <cfset dataout.RXRESULTCODE = 1 />

                <cfelse>
                    <cfset dataout.MESSAGE = "User Plan not found." />    
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = "Plan not found." />
            </cfif>

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.Message#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
    </cftry>
    
    <cfreturn dataout />        

</cffunction> 
        
<cffunction name="activeUserData" access="public" hint="active user keywords, mlp, shortUrl" output="true">
    <cfargument name="inpListKeyword" default="">
    <cfargument name="inpListShortUrl" default="">
    <cfargument name="inpListMLP" default="">

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset var resultClearKeywordById = ''/>
    <cfset var updateShortUrl = ''/>
    <cfset var updateCPP = ''/>
    <cfset var result = ''/>

    <cfif !isArray(arguments.inpListKeyword) >
        <cfset arguments.inpListKeyword = [] />
    </cfif>    
    
    <cfif !isArray(arguments.inpListShortUrl)>
        <cfset arguments.inpListShortUrl = [] />
    </cfif>    

    <cfif !isArray(arguments.inpListMLP)>
        <cfset arguments.inpListMLP = [] />
    </cfif>    

    
    <cftry>
      
        <!--- Active Keyword --->    
        <cfif arrayLen(arguments.inpListKeyword) GT 0>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ActiveKeywordById" returnvariable="resultClearKeywordById">
                <cfinvokeargument name="inpKeywordId" value="#arguments.inpListKeyword#">
                <cfinvokeargument name="inpShortCode" value="#SESSION.SHORTCODE#">
            </cfinvoke>
        </cfif>

        <cfif resultClearKeywordById.RXRESULTCODE NEQ 1>
            <cfset dataout.ERRMESSAGE = "Can not active keyword" />
            <cfreturn dataout />  
        </cfif>

        <!--- Disable MLP --->    
        <cfif arrayLen(arguments.inpListMLP) GT 0>
            <cfquery name="updateCPP" datasource="#Session.DBSourceEBM#" result="result" >
                UPDATE 
                    simplelists.cppx_data
                SET 
                    status_int = 1
                WHERE  
                    ccpxDataId_int IN (#arrayToList(arguments.inpListMLP)#)
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">     
                AND 
                    status_int = 2    
            </cfquery>
        </cfif>

        <!--- Disable SHORTURL --->    
        <cfif arrayLen(arguments.inpListShortUrl) GT 0>
           <cfquery name="updateShortUrl" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplelists.url_shortner
                SET
                    Active_int = 1
                WHERE
                    PKId_bi IN (#arrayToList(arguments.inpListShortUrl)#)
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
                AND 
                    Active_int = 2    
             </cfquery>    
        </cfif>

        <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
            <cfinvokeargument name="userId" value="#SESSION.userid#">
            <cfinvokeargument name="moduleName" value="active user data">
            <cfinvokeargument name="operator" value="#serializeJSON(arguments)#">
        </cfinvoke>    

        <cfset dataout.RXRESULTCODE = 1 />

    <cfcatch type="any">
        <cfset dataout.RXRESULTCODE = -100 />
        <cfset dataout.MESSAGE = "#cfcatch.Message#" />
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
    </cfcatch>
    </cftry>
                    
    <cfreturn dataout />        

</cffunction>  

<cffunction name="upgradeUserPlan" access="public" hint="insert new user plan and add benefit from plan, coupon">
    <cfargument name="inpUserId" required="true" type="numeric" default="0">
    <cfargument name="inpPlanData" required="true" type="query"/>
    <cfargument name="inpTotalAmount" required="true" type="numeric"/>
    <cfargument name="inpBuyKeywordNumber" required="true"/>
    <cfargument name="inpKEYWORD_CC" required="true"/>
    <cfargument name="inpMLP_CC" required="true"/>
    <cfargument name="inpURL_CC" required="true"/>
    <cfargument name="inpCREDIT_CC" required="true"/>
    <cfargument name="inpMonthYearSwitch" required="true" type="numeric"/>

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />

    <cfset var getPlan = arguments.inpPlanData/>
    <cfset var buyKeywordNumber = arguments.inpBuyKeywordNumber/>
    <cfset var totalAmount = arguments.inpTotalAmount/>
    <cfset var startDate =''/>
    <cfset var endDate = ''/>
    <cfset var keywordCC = arguments.inpKEYWORD_CC/>
    <cfset var shortUrlCC = arguments.inpURL_CC/>
    <cfset var mlpCC = arguments.inpMLP_CC/>
    <cfset var creditCC = arguments.inpCREDIT_CC/>
    <cfset var mlpLimitNumber = 0/>
    <cfset var shortUrl = 0/>
    <cfset var updatePlanCredits = {}/>
    <cfset var updateUserPlanStatus = {}/>
    <cfset var insertUsersPlan = {}/>
    <cfset var updatePromoCreditUsers = {}/>
    <cfset var monthlyAddBenefitDate = ''/>
    <cfset var GetOrderPlan=''>

    <!--- MLPSLIMITNUMBER --->
    <cfif getPlan.MLPSLIMITNUMBER EQ "Unlimited">
        <cfset mlpLimitNumber = 0/>
    <cfelse>
        <cfset mlpLimitNumber = getPlan.MLPSLIMITNUMBER/>
    </cfif>                

    <!--- SHORTURLSLIMITNUMBER --->
    <cfif getPlan.SHORTURLSLIMITNUMBER EQ "Unlimited">
        <cfset shortUrl = 0/>
    <cfelse>
        <cfset shortUrl = getPlan.SHORTURLSLIMITNUMBER/>
    </cfif>

    <cfset var todayDate = Now()>
    <cfset var startDay = Day(todayDate)>

    <cfif startDay GT 28>
        <cfset startDay = 28>
    </cfif>

    <cfset var startMonth = Month(todayDate)>
    <cfset var startYear = Year(todayDate)>
  
    <cfset startDate = CreateDate(startYear, startMonth, startDay)> 

    <cfif arguments.inpMonthYearSwitch EQ 1>
      <cfset endDate = DateAdd("m",1,startDate)> 
      <cfset monthlyAddBenefitDate = endDate> 
    <cfelse>
      <cfset endDate = DateAdd("yyyy",1,startDate)> 
      <cfset monthlyAddBenefitDate = DateAdd("m",1,startDate)> 
    </cfif>  

    <!--- UPDATE ALL OTHER PLAN STATUS = 0 --->
    <cfquery name="updateUserPlanStatus" datasource="#Session.DBSourceEBM#">
      UPDATE simplebilling.userplans
      SET 
        Status_int = 0
      WHERE 
        userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
      AND 
        Status_int = 1
    </cfquery>

    <!--- Insert new plan --->
    <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#">
      INSERT INTO simplebilling.userplans
        (
          Status_int,
          UserId_int,
          PlanId_int,
          Amount_dec,
          UserAccountNumber_int,
          KeywordsLimitNumber_int,
          FirstSMSIncluded_int,
          PriceMsgAfter_dec,
          PriceKeywordAfter_dec,
          BuyKeywordNumber_int,
          MlpsLimitNumber_int,
          ShortUrlsLimitNumber_int,
          MlpsImageCapacityLimit_bi,
          StartDate_dt,
          EndDate_dt,
          PromotionId_int,
          PromotionLastVersionId_int,
          PromotionKeywordsLimitNumber_int,
          PromotionMlpsLimitNumber_int,
          MonthlyAddBenefitDate_dt,
          PromotionShortUrlsLimitNumber_int,
          BillingType_ti
        )
        VALUES 
        (
          1,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.PLANID#">,
          <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.AMOUNT#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.USERACCOUNTNUMBER#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.KEYWORDSLIMITNUMBER#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.FIRSTSMSINCLUDED#">,
          <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEMSGAFTER#">,
          <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEKEYWORDAFTER#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#buyKeywordNumber#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#mlpLimitNumber#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#shortUrl#">,
          <cfqueryparam cfsqltype="cf_sql_bigint" value="#getPlan.IMAGECAPACITYLIMIT#">,
          <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startDate#">,
          <cfqueryparam cfsqltype="CF_SQL_DATE" value="#endDate#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#keywordCC#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#mlpCC#">,
          <cfqueryparam cfsqltype="CF_SQL_DATE" value="#monthlyAddBenefitDate#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#shortUrlCC#">,
          <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpMonthYearSwitch#">
        )
    </cfquery>

    <!--- Update plan credit --->
    <cfquery name="updatePlanCredits" datasource="#Session.DBSourceEBM#">
        UPDATE  
            simplebilling.billing
        SET   
            Balance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.FIRSTSMSINCLUDED#"> 
        WHERE 
            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
    </cfquery>

    <!--- Update promotion billing --->
    <cfquery name="updatePromoCreditUsers" datasource="#Session.DBSourceEBM#">
        UPDATE  
          simplebilling.billing
        SET   
          PromotionCreditBalance_int = PromotionCreditBalance_int+<cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#creditCC#">
        WHERE 
          userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
    </cfquery>

    <cfreturn dataout />

</cffunction>


<cffunction name="cancelDowngrade" access="remote" hint="cancel Downgrade Request" output="true">
    <cfargument name="inpUserId" default="#SESSION.Userid#" required="false">
    <cfargument name="inpAdminCancel" default="0" required="false">

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '-1' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset var userId = arguments.inpUserId/>
    <cfset var updateUserPlan = ''/>

    <cfset var inpListKeyword = [] />
    <cfset var inpListShortUrl = [] />
    <cfset var inpListMLP = [] />

    <cfif userId GT 0>
        <cftry>
            <!--- update downgrade time in userplan --->
            <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#" >
                UPDATE
                    simplebilling.userplans
                SET
                    UserDowngradeDate_dt = NULL,
                    UserDowngradePlan_int = 0,
                    UserDowngradeKeyword_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(inpListKeyword)#">,
                    UserDowngradeMLP_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(inpListMLP)#">,
                    UserDowngradeShortUrl_txt = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#serializeJSON(inpListShortUrl)#">
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
                AND 
                    Status_int = 1
                AND  
                    UserDowngradePlan_int > 0      
            </cfquery>

            <cfif arguments.inpAdminCancel EQ 1>
                <cfinvoke method="createSireUserLog" component="session.sire.models.cfc.logs">
                    <cfinvokeargument name="userId" value="#userId#">
                    <cfinvokeargument name="moduleName" value="Cancel Process">
                    <cfinvokeargument name="operator" value="Admin #SESSION.Userid# cancel downgrade">
                </cfinvoke>    
            <cfelse>    
                <cfinvoke method="createSireUserLog" component="session.sire.models.cfc.logs">
                    <cfinvokeargument name="userId" value="#userId#">
                    <cfinvokeargument name="moduleName" value="Cancel Downgrade Process">
                    <cfinvokeargument name="operator" value="Manual Cancel Downgrade">
                </cfinvoke>    
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
    <cfelse>
      <cfset dataout.RXRESULTCODE = '-2' />   
      <cfset dataout.MESSAGE = 'UserId #userId# not found.' />   
    </cfif>     
                                                
    <cfreturn dataout />        

</cffunction>  
 <cffunction name="UpdateUserWhiteGlove" access="remote" output="true" hint="remove avaiable keyword">
        <cfargument name="inpGloveActive" type="string" required="no" default="0">
        <cfargument name="inpUserId" type="string" required="no" default="#Session.UserId#">
        
        <cfset var dataout  = {} />    
        <cfset var updateWhiteGlove = ''>
        <cfset var INPMAINEMAIL = 'sirevn1@gmail.com'>        
        <cfif  (FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0  OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 )>
            <cfset var INPMAINEMAIL = 'support@siremobile.com'>                
        </cfif>
        <cfset var userInfo = {}>

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        
        <cftry>           
            <cfquery name="updateWhiteGlove" datasource="#Session.DBSourceEBM#">
                UPDATE  
                    simpleobjects.useraccount
                SET     
                    WhiteGloveStatus_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGloveActive#">
                WHERE   
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">                
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Update success" />         
            <!--- Send email notification --->
            <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                <cfinvokeargument name="to" value="#INPMAINEMAIL#">
                <cfinvokeargument name="type" value="html">
                <cfinvokeargument name="subject" value="Active White Glove!">
                <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_white_glove_active.cfm">
                <cfinvokeargument name="data" value="#TRIM(serializeJSON(userInfo))#">
            </cfinvoke>   
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -3 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                                
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
            </cfcatch>            
        </cftry>      
        <cfreturn dataout>
</cffunction>

<cffunction name="doUpgradePlan" access="public" hint="full process to upgrade plan">
    <cfargument name="inpPlanId" required="true"/>
    <cfargument name="inpUserId" required="false" default="#Session.USERID#"/>
    <cfargument name="inpPromotionCode" required="false"/>
    <cfargument name="inpMonthYearSwitch" required="false"/>
    <cfargument name="inpListKeyword" required="false"/>
    <cfargument name="inpListMLP" required="false"/>
    <cfargument name="inpListShortUrl" required="false"/>
    <cfargument name="inpPaymentGateWay" required="false" default="1"/>
    <cfargument name="inpSendEmailToUser" required="false" default="1"/>
    <cfargument name="inpAdminDo" required="true" default="0"/>
    <cfargument name="inpEmailAddress" required="false" default=""/>
    <cfargument name="inpOrderAmount" required="false" default="0"/>
    
    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '0' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset dataout.PLANNAME = '' />

    <cfset var buyKeywordNumber = 0>
    <cfset var totalKeyword = 0 >
    <cfset var totalAmount = 0 >
    <cfset var checkpromocode = {}/>
    <cfset var checkExistPlan = {}/>
    <cfset var getPlan = {}/>
    <cfset var getUserPlan = {}/>
    <cfset var promocode = {}/>

    <cfset var promotionId = 0>
    <cfset var originId = 0>
    <cfset var recurringTime = 0>

    <cfset var promotionPercent = 0>
    <cfset var promotionFlatRate = 0 >
    <cfset var promotionCredit = 0>
    <cfset var promotionKeyword = 0>
    <cfset var promotionMLP = 0>
    <cfset var promotionURL = 0>

    <cfset var percent_dis = 0>
    <cfset var flatrate_dis = 0>
    <cfset var promotion_id = 0>
    <cfset var promotion_origin_id = 0>
    <cfset var promotion_credit = 0>
    <cfset var promotion_mlp = 0>
    <cfset var promotion_keyword = 0>
    <cfset var promotion_shorturl = 0>

    <cfset var promocodeListToIncreaseUsedTimeTemp = []/>
    <cfset var userListToIncreasePromotionUsedTime = []>
    <cfset var promocodeListToIncreaseUsedTime = []>

    <cfset var percent_cc = 0>
    <cfset var flatRate_cc = 0>
    <cfset var credit_cc = 0>
    <cfset var keyword_cc = 0>
    <cfset var mlp_cc = 0>
    <cfset var url_cc = 0>
    <cfset var txtUpgradePlan = ''/>
    <cfset var logModuleName = ''/>
    <cfset var COUPONDISCOUNTTYPE = ''/>
    <cfset var index = ''/>
    <cfset var CouponForUser = ''/>
    <cfset var newUserPromoCode	 = ''/>
    <cfset var RetAddReferralCredits = ''/>
    <cfset var RetVarmailChimpAPIs = ''/>
    <cfset var resultActiveUserData = ''/>

    <cfset var purchaseAmount = 0>
    <cfset var priceBuyKeywordNumber = 0 />
    <cfset var getAffiliateFromCouponCode = ''/>
    <cfset var rtGetAffiliateFromCouponCode	= '' />
    <cfset var rtApplyAffiliateCode	= '' />


    <cfinvoke method="GetOrderPlan" component="session.sire.models.cfc.order_plan" returnvariable="getPlan">
        <cfinvokeargument name="plan" value="#arguments.inpPlanId#"/>
    </cfinvoke>

    <!--- CHECK IF UPGRADE PLAN IS EXITS --->
    <cfif arguments.inpAdminDo EQ 0>
        <cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                PlanId_int,Order_int 
            FROM 
                simplebilling.plans 
            WHERE 
                Status_int = 1 
            AND 
                PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#"> 
        </cfquery>

        <cfif checkExistPlan.RecordCount EQ 0>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = 'Upgrade Plan not found or inactived.' />
            <cfreturn dataout/>
        </cfif>
    </cfif>


    <!--- check if upgrade plan is greater than old plan --->
    <cfinvoke method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getUserPlan">
        <cfinvokeargument name="InpUserId" value="#arguments.inpUserId#"/>
    </cfinvoke>
    <cfif arguments.inpAdminDo EQ 0>
        <cfif getUserPlan.PLANORDER GTE checkExistPlan.Order_int >
            <cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = 'Upgrade Plan not greater than old plan.' />
            <cfreturn dataout/>
        </cfif>
    </cfif>    

    <cfset txtUpgradePlan =  "Ugraded Plan to: "&getPlan.PLANNAME>

    <cfset totalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED + getUserPlan.TOTALKEYWORDHAVETOPAID  > 

    <cfif totalKeyword GT getPlan.KEYWORDSLIMITNUMBER>
      <cfset buyKeywordNumber = (totalKeyword - getPlan.KEYWORDSLIMITNUMBER)>
    </cfif>

    <cfif buyKeywordNumber GT 0>
        <cfset priceBuyKeywordNumber = buyKeywordNumber * getPlan.PRICEKEYWORDAFTER />
    </cfif>

    <cfif arguments.inpMonthYearSwitch EQ 1>
      <cfset totalAmount = getPlan.AMOUNT + priceBuyKeywordNumber>
    <cfelse>
      <cfset totalAmount = (getPlan.YEARLYAMOUNT * 12) + priceBuyKeywordNumber>
    </cfif>
    <cfif arguments.inpOrderAmount GT 0>
        <cfset totalAmount=arguments.inpOrderAmount>
    </cfif>
    
    <cfif arguments.inpPromotionCode NEQ ''>
      <cfinvoke component="session.sire.models.cfc.promotion" method="CheckCouponForUpgradePlan" returnvariable="checkpromocode">
          <cfinvokeargument name="inpCouponCode" value="#arguments.inpPromotionCode#">
      </cfinvoke>

      <cfif checkpromocode.RXRESULTCODE EQ 1>
        <!--- coupon input --->
        <cfinvoke component="session.sire.models.cfc.promotion" method="GetCouponDetails" returnvariable="promocode">
            <cfinvokeargument name="inpCouponId" value="#checkpromocode.promotion_id#">
        </cfinvoke>
        
        <cfset promotionId = promocode.PROMOTIONID>
        <cfset originId = promocode.ORIGINID>
        <cfset recurringTime = promocode.COUPONRECURRING>

        <cfset promotionPercent = promocode.COUPONDISCOUNTPRICEPERCENT>
        <cfset promotionFlatRate = promocode.COUPONDISCOUNTPRICEFLATRATE >
        <cfset promotionCredit = promocode.COUPONCREDIT>
        <cfset promotionKeyword = promocode.COUPONKEYWORD>
        <cfset promotionMLP = promocode.COUPONMLP>
        <cfset promotionURL = promocode.COUPONSHORTURL>
        <!--- check afffiliate linkage--->
        <cfinvoke component="public.sire.models.cfc.affiliate" method="GetAffiliateFromCouponCode" returnvariable="rtGetAffiliateFromCouponCode">
            <cfinvokeargument name="inpCouponCode" value="#arguments.inpPromotionCode#">
        </cfinvoke>
        <cfif rtGetAffiliateFromCouponCode.RXRESULTCODE EQ 1>            
            <cfinvoke component="public.sire.models.cfc.affiliate" method="ApplyAffiliateCode" returnvariable="rtApplyAffiliateCode">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                <cfinvokeargument name="inpAffiliateCode" value="#rtGetAffiliateFromCouponCode.AFFILIATECODE#">
            </cfinvoke>
        </cfif>                                        
      </cfif>
    </cfif>

    <cfinvoke component="session.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
    </cfinvoke>

      <!--- Check CouponForUser --->
    <cfif CouponForUser.rxresultcode EQ 1>
        <cfloop array="#CouponForUser['DATALIST']#" index="index">
            <cfif index.COUPONDISCOUNTTYPE EQ 1>
                <cfset COUPONDISCOUNTTYPE = 1>
                <cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
                <cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
                <cfset promotion_id = index.PROMOTIONID>
                <cfset promotion_origin_id = index.ORIGINID>
            <cfelseif index.COUPONDISCOUNTTYPE EQ 2>
                <cfset COUPONDISCOUNTTYPE = 2>
                <cfset promotion_id = index.PROMOTIONID>
                <cfset promotion_origin_id = index.ORIGINID>
                <cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
                <cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
                <cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
                <cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
            <cfelse>
                <cfset promotion_id = index.PROMOTIONID>
                <cfset promotion_origin_id = index.ORIGINID>
            </cfif>

            <cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, arguments.inpUserId])/>
        </cfloop>
    <cfelse>
        <cfset promotion_id = 0>
        <cfset promotion_origin_id = 0>
    </cfif>

    <cfset percent_cc = promotionPercent + percent_dis>
    <cfset flatRate_cc = promotionFlatRate + flatrate_dis>
    <cfset credit_cc = promotionCredit>
    <cfset keyword_cc = promotionKeyword + promotion_keyword>
    <cfset mlp_cc = promotionMLP + promotion_mlp>
    <cfset url_cc = promotionURL + promotion_shorturl>
    <cfset purchaseAmount = (totalAmount - (totalAmount * promotionPercent / 100) - flatRate_cc)>
    
    <!--- check to reactive keyword --->
    <cfif arguments.inpListKeyword NEQ ''> 
      <cfset arguments.inpListKeyword = listToArray(arguments.inpListKeyword,',')> 
    <cfelse>
       <cfset arguments.inpListKeyword = []/>
    </cfif>

    <cfif arguments.inpListShortUrl NEQ ''> 
      <cfset arguments.inpListShortUrl = listToArray(arguments.inpListShortUrl,',')> 
    <cfelse>
       <cfset arguments.inpListShortUrl = []/>
    </cfif>

    <cfif arguments.inpListMLP NEQ ''> 
      <cfset arguments.inpListMLP = listToArray(arguments.inpListMLP,',')> 
    <cfelse>
       <cfset arguments.inpListMLP = []/>
    </cfif>
    
    <cftransaction>
        <cftry>
            <cftransaction action="begin">
                  <!--- Insert new plan & add benefit from plan & coupon --->
                  <cfinvoke method="upgradeUserPlan" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpPlanData" value="#getPlan#">
                    <cfinvokeargument name="inpTotalAmount" value="#totalAmount#">
                    <cfinvokeargument name="inpBuyKeywordNumber" value="#buyKeywordNumber#">
                    <cfinvokeargument name="inpKEYWORD_CC" value="#keyword_cc#">
                    <cfinvokeargument name="inpMLP_CC" value="#mlp_cc#">
                    <cfinvokeargument name="inpURL_CC" value="#url_cc#">
                    <cfinvokeargument name="inpCREDIT_CC" value="#credit_cc#">
                    <cfinvokeargument name="inpMonthYearSwitch" value="#arguments.inpMonthYearSwitch#">
                  </cfinvoke>
                  

                <cfif promotionId GT 0>
                    <!--- New Promocode User --->
                        <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                          <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                          <cfinvokeargument name="inpPromoId" value="#promotionId#">
                          <cfinvokeargument name="inpPromoOriginId" value="#originId#">
                          <cfinvokeargument name="inpUsedDate" value="#Now()#">
                          <cfinvokeargument name="inpRecurringTime" value="#recurringTime#">
                          <cfinvokeargument name="inpPromotionStatus" value="1">
                        </cfinvoke>
                    <!---END New Promocode User --->    

                    <!--- update used time coupon --->
                    <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                      <cfinvokeargument name="inpUsedFor" value="3">
                      <cfinvokeargument name="inpPromotionId" value="#promotionId#">
                      <cfinvokeargument name="inpLimit" value="1">
                    </cfinvoke>
                    <!--- end update used time --->

                    <!--- Insert coupon code used log --->
                    <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpCouponId" value="#promotionId#"/>
                      <cfinvokeargument name="inpOriginCouponId" value="#originId#"/>
                      <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
                      <cfinvokeargument name="inpUsedFor" value="Upgrade Plan"/>
                   </cfinvoke>   

                    <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpCouponId" value="#originId#"/>
                      <cfinvokeargument name="inpUsedFor" value="2"/>
                    </cfinvoke>

                </cfif>  

                <!--- Add Userlog --->
                <cfif arguments.inpAdminDo EQ 1>
                    <cfset logModuleName ='Admin Payment Processing'/>
                <cfelse>
                    <cfset logModuleName ='Payment Processing'/>
                </cfif>

                <cfinvoke method="createSireUserLog" component="session.sire.models.cfc.logs">
                  <cfinvokeargument name="userId" value="#arguments.inpUserId#">
                  <cfinvokeargument name="moduleName" value="#logModuleName#">
                  <cfinvokeargument name="operator" value="Payment Executed OK! Upgrade Plan for $#purchaseAmount# - plan #getPlan.PLANNAME# - planID #getPlan.PLANID# - plan type #arguments.inpMonthYearSwitch# - user did:#Session.USERID#">
                </cfinvoke>

                <!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
                <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
                  <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                </cfinvoke>
                
                <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                    <cfinvokeargument name="InpEmailString" value="#arguments.inpEmailAddress#">
                    <cfinvokeargument name="InpAccountType" value="#getPlan.PLANNAME#">         
                </cfinvoke>
                

                  <!--- active user keywords, mlp, short url --->
                <cfif arrayLen(arguments.inpListKeyword) GT 0 || arrayLen(arguments.inpListShortUrl) GT 0 || arrayLen(arguments.inpListMLP) GT 0 >
                    <cfinvoke component="session.sire.models.cfc.order_plan" method="activeUserData" returnvariable="resultActiveUserData">
                        <cfinvokeargument name="inpListKeyword" value="#arguments.inpListKeyword#">
                        <cfinvokeargument name="inpListShortUrl" value="#arguments.inpListShortUrl#">
                        <cfinvokeargument name="inpListMLP" value="#arguments.inpListMLP#">
                    </cfinvoke>
                </cfif>

                <!--- Sendmail payment --->
              <!---   <cfif arguments.inpPaymentGateWay EQ 1 AND arguments.inpSendEmailToUser EQ 1>
                    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                      <cfinvokeargument name="to" value="#arguments.inpEmailAddress#">
                      <cfinvokeargument name="type" value="html">
                      <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                      <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
                      <cfinvokeargument name="data" value="#serializeJSON(paymentResposeContent)#">
                    </cfinvoke>  
                </cfif>

                <cfif arguments.inpPaymentGateWay EQ 2 AND arguments.inpSendEmailToUser EQ 1>
                    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                      <cfinvokeargument name="to" value="#arguments.inpEmailAddress#">
                      <cfinvokeargument name="type" value="html">
                      <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                      <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
                      <cfinvokeargument name="data" value="#serializeJSON(paymentResposeContent)#">
                    </cfinvoke>  
                </cfif> --->

                <cftransaction action="commit">
                <cfset dataout.RXRESULTCODE = 1 />    
                <cfset dataout.PLANNAME = getplan.PLANNAME/>
                 
        <cfcatch type="any">
          <cftransaction action="rollback">
            <cfset dataout.RXRESULTCODE = -3 />
            <cfset dataout.MSG = "Transaction Failed!">
            <cfset dataout.ERRMSG =  cfcatch.message & " " & cfcatch.detail>
        </cfcatch>
        </cftry>
    </cftransaction>
    <cfreturn dataout>
</cffunction> 
<cffunction name="RenewUserPlan" access="remote" hint="Admin Renew User Plan" output="true">
    <cfargument name="inpUserId" default="">
    <cfargument name="inpFirstSMSIncluded" default="">
    <cfargument name="inpKeyWordLimit" default="">
    <cfargument name="inpEndDate" default="">   

    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = '' />
    <cfset dataout.MESSAGE = '' />
    <cfset dataout.ERRMESSAGE = '' />
    <cfset var updateUserPlan = ''/>    
    <cfset var updateBilling = ''/>    
    <cfset var RetVarGetAdminPermission	= '' />

    <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
        <cfinvokeargument name="inpSkipRedirect" value="1"/>
    </cfinvoke>
    <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
        <cfthrow message="You do not have permission!" />
    </cfif>   
    <cftry>     
        <!--- Update simplebilling.userplans --->    
        <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#">
            UPDATE  
                `simplebilling`.`userplans` 
            SET    
                FirstSMSIncluded_int        = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpFirstSMSIncluded#">,
                KeywordsLimitNumber_int     = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpKeyWordLimit#">,
                EndDate_dt                  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEndDate#">
            WHERE
                userId_int                  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                Status_int                  = 1  -- status = 1 is active
        </cfquery>

        <!--- Update simplebilling.billing --->    
        <cfquery name="updateBilling" datasource="#Session.DBSourceEBM#">
            UPDATE  
                `simplebilling`.`billing` 
            SET    
                Balance_int                 = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpFirstSMSIncluded#">             
            WHERE
                userId_int                  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">              
        </cfquery>

        <cfset dataout.RXRESULTCODE = 1 />

    <cfcatch type="any">
        <cfset dataout.RXRESULTCODE = -100 />
        <cfset dataout.MESSAGE = "#cfcatch.Message#" />
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
    </cfcatch>
    </cftry>
                    
    <cfreturn dataout />        

</cffunction>     

</cfcomponent>
