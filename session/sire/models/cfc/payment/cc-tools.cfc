<cfcomponent>
    <cffunction name="checkCcType" access="remote" output="true" hint="Detect card type (visa,master,amex,discover) ">
	 	<cfargument name="inpcardNumber" required="true" hint="card numbers">
                
        <cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 /> 
		<cfset dataout.RESULT = ''/>                    
		<cfset dataout.MESSAGE = 'Can not found user cc data.'/>
		<cfset var inpType="visa">

        <cftry>
            <cfif (left(arguments.inpcardNumber, 2) EQ 34 OR left(arguments.inpcardNumber, 2) EQ 37) AND len(arguments.inpcardNumber) EQ 15>			
				<cfset inpType="amex">
			<cfelseif (left(arguments.inpcardNumber, 4) EQ 6011 OR left(arguments.inpcardNumber, 2) EQ 65) AND len(arguments.inpcardNumber) EQ 16
					OR left(arguments.inpcardNumber,3) EQ 300 
					OR left(arguments.inpcardNumber,3) EQ 302 
					OR left(arguments.inpcardNumber,3) EQ 303 
					OR left(arguments.inpcardNumber,3) EQ 303 
					OR left(arguments.inpcardNumber,3) EQ 304 
					OR left(arguments.inpcardNumber,3) EQ 305
					OR left(arguments.inpcardNumber,3) EQ 309
					OR left(arguments.inpcardNumber,2) EQ 36
					OR left(arguments.inpcardNumber,2) EQ 37
					OR left(arguments.inpcardNumber,2) EQ 38
					OR left(arguments.inpcardNumber,2) EQ 39
			>							
				<cfset inpType="discover">
			<cfelseif (left(arguments.inpcardNumber, 2) GTE 51 AND left(arguments.inpcardNumber, 2) LTE 55) AND len(arguments.inpcardNumber) EQ 16>		
				<cfset inpType="mastercard">
			<cfelseif left(arguments.inpcardNumber, 1) EQ 4 AND (len(arguments.inpcardNumber) EQ 13 OR len(arguments.inpcardNumber) EQ 16)>
				<cfset inpType="visa">
			</cfif>
			<cfset dataout.CCTYPE = inpType/>
            <cfset dataout.RXRESULTCODE = 1 /> 
			<cfset dataout.RESULT = 'Success'>                    
			<cfset dataout.MESSAGE = "Success">
	        
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 /> 
			<cfset dataout.RESULT = 'ERROR'>                    
			<cfset dataout.MESSAGE = "Error while get info">
		</cfcatch> 
        </cftry>

		<cfreturn dataout />
    </cffunction>	
</cfcomponent>