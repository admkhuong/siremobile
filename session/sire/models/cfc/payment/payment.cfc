<cfcomponent>

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">

	<cffunction name="SireFraudCheck" access="remote" output="true" hint="Sire Frault check payment">
    	<cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">						
        <cfargument name="inpCardObject" TYPE="string" required="false" default="">    
		<cfargument name="inpPaymentGateway" TYPE="string" required="true" default=""> 
		<cfargument name="inpPaymentType" TYPE="string" required="true" default="1 is current card, 2 is new card">
		<!--- 
		inpCardObject template:
		<cfset arguments.inpCardObject={
			inpNumber:'4444333322221111',
			inpFirstName: 'hung',
			inpLastName: 'nguyen',
			inpLine1: 'ha noi',
			inpCity: 'ha noi',
			inpPostalCode: '234',
			inpState: 'HN',
			inpEmail: 'hung@gmail.com',
			inpPhone:'3344334455',
			inpCountryCode: 'US',
			inpIpAddress:''
			
		}>
		--->

        <cfset var GetProfileInfo = ''/>
		<cfset var cardObject= {}>
		<cfset var profileData= {}>
		<cfset var checkVaultStatus= {}>
		<cfset var rtTokenRegisterCardNumber= {}>
		<cfset var VaultID=''>
		<cfset var RetVarGetOperatorId={}>
		<cfset var RetVarGetOperatorIdFromService={}>
		<cfset var rtgetDetailCreditCardInVault={}>
		<cfset var inpCarrier=0>
		<cfset var CardDataApprovalForever={}>
		<cfset var rtstoreCreditCardInVault={}>
		<cfset var avsResult=''>
		
		
		
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">  
		<cfset dataout.RTCARDOBJECT = {}>  		

		
		<!--- <cfset dataout.RXRESULTCODE = 1>
		<cfreturn dataout> --->
		<cfset dataout.DATACHECK={
				FirstNameMatch_ti: 0,
				LastNameMatch_ti: 0,
				AddressMatch_ti: 0,
				CityMatch_ti: 0,
				StateMatch_ti: 0,
				CountryMatch_ti: 0,
				PostCodeMatch_ti: 0,
				EmailMatch_ti : 0,
				PhoneMatch_ti: 0,
				Phone_vch:'',
				IpMatch_ti: 0,
				IpToIp_Mile_dec:0,
				LastProfileUpdateDay_vch:'',
				AVS_Result_vch:''
		} >
		<cfset var checkCC = 1>
		<cfset var stringReturn = "">
		<cfset var userCurrentIp="">
		<cfset var fraudlabspro={}>
		<cfset var distanceInMile=0>
		<cfset var tempObject={}>
		<cfset var inpType="visa">
        <cftry>				
			<cfif arguments.inpPaymentType EQ 1>
				<cfif arguments.inpPaymentGateway EQ _WORLDPAY_GATEWAY>
				
				<cfelseif arguments.inpPaymentGateway EQ _PAYPAL_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-paypal" method="getDetailCreditCardInVault" returnvariable="rtgetDetailCreditCardInVault">											
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					</cfinvoke>	

				<cfelseif arguments.inpPaymentGateway EQ _MOJO_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-mojo" method="getDetailCreditCardInVault" returnvariable="rtgetDetailCreditCardInVault">											
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					</cfinvoke>															
				<cfelseif arguments.inpPaymentGateway EQ _TOTALAPPS_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="getDetailCreditCardInVault" returnvariable="rtgetDetailCreditCardInVault">											
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					</cfinvoke>											
				</cfif>				
				
				<cfif rtgetDetailCreditCardInVault.RXRESULTCODE EQ 1>
					<cfset VaultID = rtgetDetailCreditCardInVault.VAULTID/>
					<cfset arguments.inpCardObject =deserializeJson(arguments.inpCardObject)>
					<cfset arguments.inpCardObject.inpNumber = rtgetDetailCreditCardInVault.CUSTOMERINFO.maskedNumber>
					<cfset arguments.inpCardObject.inpFirstName = rtgetDetailCreditCardInVault.CUSTOMERINFO.firstName>
					<cfset arguments.inpCardObject.inpLastName = rtgetDetailCreditCardInVault.CUSTOMERINFO.lastName>
					<cfset arguments.inpCardObject.inpLine1 = rtgetDetailCreditCardInVault.CUSTOMERINFO.line1>
					<cfset arguments.inpCardObject.inpCity = rtgetDetailCreditCardInVault.CUSTOMERINFO.city>
					<cfset arguments.inpCardObject.inpPostalCode = rtgetDetailCreditCardInVault.CUSTOMERINFO.zip>
					<cfset arguments.inpCardObject.inpState = rtgetDetailCreditCardInVault.CUSTOMERINFO.state>
					<cfset arguments.inpCardObject.inpEmail = rtgetDetailCreditCardInVault.CUSTOMERINFO.emailAddress>
					<cfset arguments.inpCardObject.inpPhone = rtgetDetailCreditCardInVault.CUSTOMERINFO.phone>
					<cfset arguments.inpCardObject.inpCountryCode = rtgetDetailCreditCardInVault.CUSTOMERINFO.country>														
					<cfset arguments.inpCardObject.inpExpireMonth = Left(rtgetDetailCreditCardInVault.CUSTOMERINFO.expirationDate,2)>														
					<cfset arguments.inpCardObject.inpExpreYear = Right(rtgetDetailCreditCardInVault.CUSTOMERINFO.expirationDate,4)>																				
					<cfset arguments.inpCardObject.inpType = rtgetDetailCreditCardInVault.CUSTOMERINFO.creditCardType>	
					<cfset arguments.inpCardObject.inpCvv2 = rtgetDetailCreditCardInVault.CUSTOMERINFO.cvv>																			
					
					
					<cfset arguments.inpCardObject =serializeJson(arguments.inpCardObject)>
					<cfset avsResult= 	rtgetDetailCreditCardInVault.CUSTOMERINFO.AVSCODE/>
					<cfset inpType = rtgetDetailCreditCardInVault.CUSTOMERINFO.creditCardType>																					
				</cfif>	
								

			<cfelseif arguments.inpPaymentType EQ 2>
				<!--- <cfif arguments.inpPaymentGateway EQ _WORLDPAY_GATEWAY>
				
				<cfelseif arguments.inpPaymentGateway EQ _PAYPAL_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-paypal" method="storeCreditCardInVault" returnvariable="rtstoreCreditCardInVault">					
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="0"/>
					</cfinvoke>

				<cfelseif arguments.inpPaymentGateway EQ _MOJO_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-mojo" method="storeCreditCardInVault" returnvariable="rtstoreCreditCardInVault">					
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="0"/>
					</cfinvoke>		
				<cfelseif arguments.inpPaymentGateway EQ _TOTALAPPS_GATEWAY>
					<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="storeCreditCardInVault" returnvariable="rtstoreCreditCardInVault">					
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="0"/>
					</cfinvoke>														
				</cfif> --->
				<cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="rtstoreCreditCardInVault">					
					<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
					<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					<cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#"/>					
				</cfinvoke>	
				
				<cfif rtstoreCreditCardInVault.RXRESULTCODE EQ 1>
					<cfset VaultID = rtstoreCreditCardInVault.USERCARDID/>	
					<cfset avsResult= 	rtstoreCreditCardInVault.AVSCODE/>	
					<cfset inpType= 	rtstoreCreditCardInVault.CCTYPE/>	
					
				<cfelseif rtstoreCreditCardInVault.RXRESULTCODE EQ -2>
					<cfset dataout.RXRESULTCODE = -2>
					<cfset dataout.RESULT = 'Your card was Declined by payment gateway or duplicate transaction, please wait for 20 minutes and try again'>
					<cfset dataout.MESSAGE = 'Your card was Declined by payment gateway or duplicate transaction, please wait for 20 minutes and try again'>  
					<cfreturn dataout> 
				<cfelse>
					<cfset avsResult= 	rtstoreCreditCardInVault.AVSCODE/>
					<cfset dataout.RXRESULTCODE = -2>
					<cfset dataout.RESULT = rtstoreCreditCardInVault.MESSAGE>
					<cfset dataout.MESSAGE = rtstoreCreditCardInVault.MESSAGE>  
					<cfreturn dataout> 			
				</cfif>		
			</cfif>			
			<cfset tempObject=deserializeJSON(arguments.inpCardObject)>
			<cfset tempObject.inpVaultId=VaultID>
			<cfset tempObject.inpType=inpType>
			
			<cfset dataout.RTCARDOBJECT = serializeJSON(tempObject)>  

			<!--- collect data for compare--->		
			<cfquery name="GetProfileInfo" datasource="#Session.DBSourceREAD#">
				SELECT                        
					EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,City_vch,State_vch,
					LastProfileUpdate_dt
				FROM					
					simpleobjects.useraccount									
				WHERE					 
					UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >					   
			</cfquery>							
                    
			<cfset profileData.USERID = '#arguments.inpUserId#'>
			<cfset profileData.EMAIL = '#GetProfileInfo.EmailAddress_vch#'>                    
			<cfset profileData.MFAPHONE = '#GetProfileInfo.MFAContactString_vch#'>                    
			<cfset profileData.FIRSTNAME = '#GetProfileInfo.FirstName_vch#'>                    
			<cfset profileData.LASTNAME = '#GetProfileInfo.LastName_vch#'>
			<cfset profileData.ADDRESS = "#GetProfileInfo.Address1_vch#">
			<cfset profileData.POSTALCODE = "#GetProfileInfo.PostalCode_vch#">                    
			<cfset profileData.CITY = "#GetProfileInfo.City_vch#">
			<cfset profileData.STATE = "#GetProfileInfo.State_vch#">
			<cfset profileData.PROFILELASTUPDATE = '#GetProfileInfo.LastProfileUpdate_dt#'>
			
			<!--- check Sire fraud pass or reject by admin--->
			<cfquery name="checkVaultStatus" datasource="#Session.DBSourceREAD#">
				SELECT
					Status_ti,
					CardDataApprovalForever_txt
				FROM
					simplebilling.list_cc_review
				WHERE					
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >
				AND
					VaultId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VaultID#" >
				ORDER BY
					AdmActionDate_dt DESC
				LIMIT 1
			</cfquery>	
			
			<cfif isJson(arguments.inpCardObject)>
				<cfset cardObject = deserializeJson(arguments.inpCardObject)>
	        </cfif>
			
			<cfif checkVaultStatus.RECORDCOUNT GT 0>
				<!---0 NOT EXIST, 1 APPROVAL ONE TIME, 2 APPROVAL FOREVER, 3 REJECT ONT TIME, 4 REJECT FOREVER --->
				<cfif checkVaultStatus.Status_ti EQ 4>				
					<cfset dataout.RXRESULTCODE = -2>
					<cfset dataout.RESULT = 'It looks like there was a problem processing this credit card for your account.<br>Some common issues that may have caused this include:<br>
					<ul><li>Account address and credit card address do not match</li><li>Invalid credit card numbers, expiration or CVV</li><li>Name on the card does not match the name on the account</li></ul><br>Please double check your info or use a different credit card and try again.  Feel <br>free to reach out with any questions at <a href="mailto:support@siremobile.com"> support@siremobile.com </a>'>
					<cfset dataout.MESSAGE = 'It looks like there was a problem processing this credit card for your account.<br>Some common issues that may have caused this include:<br>
					<ul><li>Account address and credit card address do not match</li><li>Invalid credit card numbers, expiration or CVV</li><li>Name on the card does not match the name on the account</li></ul><br>Please double check your info or use a different credit card and try again.  Feel <br>free to reach out with any questions at <a href="mailto:support@siremobile.com"> support@siremobile.com </a>'>  
					<cfreturn dataout> 
				<cfelseif checkVaultStatus.Status_ti EQ 2>
					<!--- double check card data input same as card data approval 4ever b4--->
					
					<cfset CardDataApprovalForever= DeserializeJSON(checkVaultStatus.CardDataApprovalForever_txt)>
					
					<cfif reReplace(cardObject.inpFirstName, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpFirstName, "\s+", "","all"))								
					AND reReplace(cardObject.inpLastName, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpLastName, "\s+", "","all"))
					
					AND reReplace(cardObject.inpLine1, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpLine1, "\s+", "","all"))
					AND reReplace(cardObject.inpCity, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpCity, "\s+", "","all"))		
					AND reReplace(cardObject.inpPostalCode, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpPostalCode, "\s+", "","all"))
					AND reReplace(cardObject.inpState, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpState, "\s+", "","all"))>			
					
						<cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.RESULT = 'SUCCESS'>
						<cfset dataout.MESSAGE = 'SUCCESS'>   
						<cfreturn dataout> 
					</cfif>					
				</cfif>
			</cfif>
			
			 

			

			<!--- check phone operator--->			
			<cfif profileData.MFAPHONE NEQ "">
				<cfset profileData.MFAPHONE = replace(profileData.MFAPHONE, "(", "")/>
				<cfset profileData.MFAPHONE = replace(profileData.MFAPHONE, ")", "")/>
				<cfset profileData.MFAPHONE = replace(profileData.MFAPHONE, "-", "")/>

				<!--- Search list(s) for last carrier info --->
				<cfinvoke component="session.cfc.csc.csc" method="GetOperatorId" returnvariable="RetVarGetOperatorId" >
					<cfinvokeargument name="inpContactString" value="#TRIM(profileData.MFAPHONE)#">                                           
				</cfinvoke>
				<cfif RetVarGetOperatorId.OPERATORID GT 0>
					<cfset inpCarrier = RetVarGetOperatorId.OPERATORID />      
				<cfelse>
				
					<!--- Next search carrier --->
					<cfinvoke component="session.cfc.csc.csc" method="GetOperatorIdFromService" returnvariable="RetVarGetOperatorIdFromService" >
						<cfinvokeargument name="inpContactString" value="#TRIM(profileData.MFAPHONE)#">                                           
					</cfinvoke>
			
					<cfif RetVarGetOperatorIdFromService.OPERATORID GT 0>
						<cfset inpCarrier = RetVarGetOperatorIdFromService.OPERATORID />
						
						<!--- Add to generic list for future faster/cheaper search  --->            
						<cfinvoke component="session.cfc.csc.csc" method="SaveOperatorId">
							<cfinvokeargument name="inpContactString" value="#TRIM(profileData.MFAPHONE)#">       
							<cfinvokeargument name="inpOperatorId" value="#TRIM(inpCarrier)#">                                           
						</cfinvoke>
						
					</cfif> 
						
				</cfif>
				
				<cfset dataout.DATACHECK.Phone_vch = profileData.MFAPHONE>
				<cfif inpCarrier LTE 0 OR inpCarrier EQ "10488">					
					<cfset checkCC = -1>
					<cfset dataout.DATACHECK.PhoneMatch_ti = -1>
					<cfset stringReturn = listAppend(stringReturn, "Phone carrier invalid")>	
				<cfelse>
					<cfset dataout.DATACHECK.PhoneMatch_ti = 1>
				</cfif>

			</cfif>
			<!--- end--->

			<!--- compare--->

			<!--- check profile last update 30 days--->
			
			<cfif Len(profileData.PROFILELASTUPDATE) GT 0>
				<cfset dataout.DATACHECK.LastProfileUpdateDay_vch = DateDiff("d", profileData.PROFILELASTUPDATE, NOW()) >
				<cfif DateDiff("d", profileData.PROFILELASTUPDATE, NOW()) LTE 30>				
					<cfset checkCC = -1>				
					<cfset stringReturn = listAppend(stringReturn, "Profile update in"& dataout.DATACHECK.LastProfileUpdateDay_vch & " days" )>	
				</cfif>
			</cfif>

			<cfif ! reReplace(profileData.FIRSTNAME, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpFirstName, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.FirstNameMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "First Name not match")>	
			<cfelse>
				<cfset dataout.DATACHECK.FirstNameMatch_ti = 1>			
			</cfif> 
			<cfif ! reReplace(profileData.LASTNAME, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpLastName, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.LastNameMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "Last Name not match")>	
			<cfelse>
				<cfset dataout.DATACHECK.LastNameMatch_ti = 1>			
			</cfif> 
			<!--- hide check addr
			<cfif ! reReplace(profileData.ADDRESS, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpLine1, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.AddressMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "Address not match")>		
			<cfelse>
				<cfset dataout.DATACHECK.AddressMatch_ti = 1>	
			</cfif> 
			<cfif ! reReplace(profileData.CITY, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpCity, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.CityMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "City not match")>	
			<cfelse>
				<cfset dataout.DATACHECK.CityMatch_ti = 1>		
			</cfif> 
			<cfif ! reReplace(profileData.POSTALCODE, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpPostalCode, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.PostCodeMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "Post code not match")>	
			<cfelse>
				<cfset dataout.DATACHECK.PostCodeMatch_ti = 1>			
			</cfif> 
			<cfif ! reReplace(profileData.STATE, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpState, "\s+", "","all"))>			
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.StateMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "State not match")>	
			<cfelse>
				<cfset dataout.DATACHECK.StateMatch_ti = 1>			
			</cfif> 
			end hide check addr--->
			<cfif TRIM(cardObject.inpEmail) NEQ "">
				<cfif ! reReplace(profileData.EMAIL, "\s+", "","all").EqualsIgnoreCase(reReplace(cardObject.inpEmail, "\s+", "","all"))>			
					<cfset checkCC = -1>
					<cfset dataout.DATACHECK.EmailMatch_ti = -1>
					<cfset stringReturn = listAppend(stringReturn, "Email not match")>		
				<cfelse>
					<cfset dataout.DATACHECK.EmailMatch_ti = 1>	
				</cfif> 
			</cfif> 

			
			<!---fraud 100 miles--->
			<cfset userCurrentIp=cgi.remote_addr>
			<!--- <cfset userCurrentIp="107.136.2.18"> --->

			<cfset dataout.IP=userCurrentIp>					
			<cftry>			
			<cfhttp url="#screen_order_url#" method="get" result="fraudlabspro" timeout="30">
				<cfhttpparam type="URL" name="format" value="json">
				<cfhttpparam type="URL" name="first_name" value="#cardObject.inpFirstName#"/>
				<cfhttpparam type="URL" name="last_name" value="#cardObject.inpLastName#"/>
				<cfhttpparam type="URL" name="bill_addr" value="#cardObject.inpLine1#"/>
				<cfhttpparam type="URL" name="bill_city" value="#cardObject.inpCity#"/>
				<cfhttpparam type="URL" name="bill_state" value="#cardObject.inpState#"/>
				<cfhttpparam type="URL" name="bill_zip_code" value="#cardObject.inpPostalCode#"/>
				<cfhttpparam type="URL" name="bill_country" value="#cardObject.inpCountryCode#"/>
				
				<cfhttpparam type="URL" name="email" value="#cardObject.inpEmail#"/>
				<cfhttpparam type="URL" name="bin_no" value="#LEFT(cardObject.inpNumber,6)#"/>   
				<cfhttpparam type="URL" name="currency" value="USD"/>        
				<!--- <cfhttpparam type="URL" name="avs_result" value="M"/>   --->
				
				<cfhttpparam type="URL" name="ip" value="#userCurrentIp#">
				<cfhttpparam type="URL" name="key" value="#fraudlabpro_apikey#">
				<cfhttpparam type="URL" name="card_hash" value="#cardObject.inpPSHA1#">
				<cfhttpparam type="URL" name="amount" value="10">
			</cfhttp>			
			
			<cfif structKeyExists(fraudlabspro, "status_code") AND fraudlabspro.status_code EQ 200>    
				<cfset distanceInMile=DeserializeJSON(fraudlabspro.filecontent).distance_in_mile>
				<!--- <cfset avsResult=DeserializeJSON(fraudlabspro.filecontent).fraudlabspro_status> --->
			</cfif>	
			<cfcatch></cfcatch>	
			</cftry>		
			
			<cfif ! IsValid("float", distanceInMile) >
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.IpMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "IP invalid "& distanceInMile)>
			<cfelseif distanceInMile GT 100>
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.IpMatch_ti = -1>
				<cfset stringReturn = listAppend(stringReturn, "IP distance over 100 miles "& distanceInMile)>
			<cfelse>
				<cfset dataout.DATACHECK.IpMatch_ti = 1>
			</cfif>
			<cfif IsValid("float", distanceInMile)>
				<cfset dataout.DATACHECK.IpToIp_Mile_dec = numberFormat(distanceInMile,'.999')>			
			</cfif>

			<!---fraud AVS--->			
			<cfif listfind("00,01,02,10,11,14,30,D,M,Y,X,L,P,Z,W,G,I,S",avsResult) EQ 0 >
				<cfset checkCC = -1>
				<cfset dataout.DATACHECK.AVS_Result_vch = ucase(avsResult)>
				<cfset stringReturn = listAppend(stringReturn, "AVS not pass "& avsResult)>
			<cfelse>
				<cfset dataout.DATACHECK.AVS_Result_vch = avsResult>
			</cfif>
			
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.RESULT = 'SUCCESS'>
			<cfset dataout.MESSAGE = 'SUCCESS'>   
			<cfif checkCC NEQ 1>				
				<cfset dataout.RXRESULTCODE = 0 />			
				<cfset dataout.RESULT = "FAIL">            
				<cfset dataout.MESSAGE = "#stringReturn#" />
				<cfset dataout.ERRMESSAGE = "#stringReturn#" />			
			</cfif>			
			
			
	    <cfcatch>		
			
			
			<cfset dataout.RXRESULTCODE = -1 />			
    	    <cfset dataout.RESULT = "FAIL">            
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>
    </cffunction>	
	<cffunction name="LogPaymentNeedReview" access="remote" output="true" hint="Log Payment Need Review">
    	<cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">		
		<cfargument name="inpPaymentGateway" TYPE="string" required="true" default=""> 
        <cfargument name="inpCardObject" TYPE="string" required="false" default=""> 
		<cfargument name="inpModuleName" TYPE="string" required="true" default="">
		<cfargument name="inpAmount" TYPE="string" required="true" default="">   
		<cfargument name="inpNumberKeyword" TYPE="string" required="false" default="0">   
		<cfargument name="inpNumberCredit" TYPE="string" required="false" default="0">   
		<cfargument name="inpBuyPlanId" TYPE="string" required="false" default="0">   
		<cfargument name="inpCheckData" TYPE="string" required="true">   		
		<cfargument name="inpPaymentType" TYPE="string" required="true" default="1 is current card, 2 is new card">

		<cfset var data ={}>
		<cfset var GetUserInfo={}>
        <cfset var rtLogListPaymentNeedReview = ''/>
		<cfset var LogListCCReview = ''/>	
		<cfset var rtTokenRegisterCardNumber={}	>
			
		<cfset var checkData = {}/>		
		<cfset var checkVaultExist = {}/>		
		<cfset var getDetailCreditCardInVault={}>
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">       
		<cfset var CardObjectSaveLogs=  deserializeJSON(arguments.inpCardObject)>
		<cfset CardObjectSaveLogs.inpNumber=  right(CardObjectSaveLogs.inpNumber,4)>
		<cfset var VaultID = CardObjectSaveLogs.inpVaultId/>	
		<cfset CardObjectSaveLogs=  serializeJSON(CardObjectSaveLogs)>
		<cfset var LogListPaymentNeedReview = {}/>
		<cfset var rtgetDetailCreditCardInVault = {}/>
        <cftry>	
			
			
			<cfset checkData = deserializeJson(arguments.inpCheckData)>
			<!--- collect data for compare--->		
			<cfquery name="LogListPaymentNeedReview" datasource="#Session.DBSourceEBM#" result="rtLogListPaymentNeedReview">
				INSERT INTO `simplebilling`.`list_payment_review`
					(
						`UserId_int`,
						`ModuleName_vch`,
						`Amount_dec`,
						`BuyPlanId_int`,
						`BuyNumberKeywords`,
						`BuyNumberCredits`,
						`TransactionDate_dt`,
						`CardData_txt`,
						`VaultId_vch`,

						`FirstNameMatch_ti`,
						`LastNameMatch_ti`,
						`AddressMatch_ti`,
						`CityMatch_ti`,
						`StateMatch_ti`,
						`CountryMatch_ti`,
						`PostCodeMatch_ti`,
						`EmailMatch_ti`,
						`PhoneMatch_ti`,
						`Phone_vch`,
						`IpMatch_ti`,
						`IpToIp_Mile_dec`,
						`LastProfileUpdateDay_vch`,
						`AVS_Result_vch`,
						`PaymentGateway_ti`
						

					)
					VALUES
					(						
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpModuleName#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpAmount#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBuyPlanId#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNumberKeyword#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNumberCredit#" >,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CardObjectSaveLogs#" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VaultID#" >,

						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'FirstNameMatch_ti') ? '#checkData.FirstNameMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'LastNameMatch_ti') ? '#checkData.LastNameMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'AddressMatch_ti') ? '#checkData.AddressMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'CityMatch_ti') ? '#checkData.CityMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'StateMatch_ti') ? '#checkData.StateMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'CountryMatch_ti') ? '#checkData.CountryMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'PostCodeMatch_ti') ? '#checkData.PostCodeMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'EmailMatch_ti') ? '#checkData.EmailMatch_ti#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'PhoneMatch_ti') ? '#checkData.PhoneMatch_ti#' : '0' #" >,
						
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#structKeyExists(checkData,'Phone_vch') ? '#checkData.Phone_vch#' : '' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#structKeyExists(checkData,'IpMatch_ti') ? '#checkData.IpMatch_ti#' : '0' #" >,						

						<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#structKeyExists(checkData,'IpToIp_Mile_dec') ? '#checkData.IpToIp_Mile_dec#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#structKeyExists(checkData,'LastProfileUpdateDay_vch') ? '#checkData.LastProfileUpdateDay_vch#' : '0' #" >,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#structKeyExists(checkData,'AVS_Result_vch') ? '#checkData.AVS_Result_vch#' : '' #" >,

						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentGateway#" >
					)					 					
			</cfquery>	
			<cfquery name="checkVaultExist" datasource="#Session.DBSourceREAD#">
				SELECT
					UserId_int
				FROM
					simplebilling.list_cc_review
				WHERE					
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >
				AND
					VaultId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VaultID#" >
			</cfquery>	
			<cfif checkVaultExist.RECORDCOUNT EQ 0>
				<cfquery name="LogListCCReview" datasource="#Session.DBSourceEBM#">
					INSERT INTO `simplebilling`.`list_cc_review`
						(
							`UserId_int`,
							`VaultId_vch`						
							
						)
						VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VaultID#" >					
							
						)				 					
				</cfquery>				
			</cfif>
			<!--- Send Email to Admin --->
			<cfquery name="GetUserInfo" datasource="#Session.DBSourceREAD#">
				SELECT
					EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch 
				FROM
					simpleobjects.useraccount
				WHERE                
					userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">				
			</cfquery>
			<cftry>
				<cfinclude template="/public/sire/configs/paths.cfm"/>
				<cfset data.USERID = arguments.inpUserId/>
				<cfset data.AMOUNT = arguments.inpAmount/>	
				<cfset data.PHONENUMBER = GetUserInfo.MFAContactString_vch />			
				<cfset data.USERNAME = GetUserInfo.FirstName_vch & " " & GetUserInfo.LastName_vch/>
				<cfset data.TRANSACTION=arguments.inpModuleName>
				
					
				<!--- Send email alert #SupportEMailUserName#--->
				
				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
					<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
					<cfinvokeargument name="type" value="html"/>
					<cfinvokeargument name="subject" value="Customer Suspicious Transaction"/>
					<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_payment_suspicious.cfm"/>
					<cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
				</cfinvoke>
				<cfcatch></cfcatch>

			</cftry>						
                    			

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.RESULT = 'SUCCESS'>
			<cfset dataout.MESSAGE = 'SUCCESS'>   
	    <cfcatch>
    	    <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>
    </cffunction>
	<cffunction name="GetListTransactionReview" access="remote" output="true" hint="Get list of transaction need review"><!--- returnformat="JSON"--->        
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        
        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetList = ''>        
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
		<cfset var GateWay   = '' />
        <cfset var sSortDir_0= arguments.sSortDir_0>

        <cfset var fullName   = '' />
		<cfset var notes=''>
		<cfset var status=''>
        <cfset var rsCount   = {} />
		<cfset var gwText=''>
        
        
        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListData"] = ArrayNew(1)>
        
            <!--- Order by --->
           <cfif iSortCol_0 GTE 0>
                <cfswitch expression="#Trim(iSortCol_0)#">
                    <cfcase value="0"> 
                    	<cfset order = "lpr.UserId_int" />
                    </cfcase> 
                    <cfcase value="1"> 
                    	<cfset order = "ua.FirstName_vch" />
                    </cfcase>
					<cfcase value="2"> 
                    	<cfset order = "ua.EmailAddress_vch" />
                    </cfcase> 
					<cfcase value="3"> 
                    	<cfset order = "ua.MFAContactString_vch" />
                    </cfcase> 
					<cfcase value="4"> 
                    	<cfset order = "lpr.ModuleName_vch" />
                    </cfcase> 
					<cfcase value="5"> 
                    	<cfset order = "lpr.TransactionDate_dt" />
                    </cfcase> 
					<cfcase value="6"> 
                    	<cfset order = "lpr.Amount_dec" />
                    </cfcase> 
					
                    <cfdefaultcase> 
                        <cfset order = "lpr.TransactionDate_dt"/>                            
                    </cfdefaultcase>    
                </cfswitch>
            <cfelse>
                <cfset order = "lpr.TransactionDate_dt"/>
                <cfset sSortDir_0 = 'DESC'/>
            </cfif>

			

            <cfquery name="GetList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS 										
					lpr.Id_bi,
					lpr.UserId_int,
					lpr.ModuleName_vch,
					lpr.Amount_dec,
					lpr.BuyPlanId_int,
					lpr.BuyNumberKeywords,
					lpr.BuyNumberCredits,
					lpr.TransactionDate_dt,
					lpr.CardData_txt,
					lpr.VaultId_vch,						
					lpr.AdmActionBy_int,
					lpr.AdmActionDate_dt,
					lpr.PaymentGateway_ti,

					lpr.Status_ti as TransactionStatus,					

                    ua.EmailAddress_vch, 
					ua.FirstName_vch ,
					ua.LastName_vch, 
					ua.MFAContactString_vch,
					p.PlanName_vch
                FROM 
                    simplebilling.list_payment_review lpr	
				LEFT JOIN 
					simplebilling.plans p
				ON
					lpr.BuyPlanId_int = p.PlanId_int								
                LEFT JOIN
                    simpleobjects.useraccount ua
                ON    
                    lpr.UserId_int = ua.UserId_int        
				WHERE
				ua.IsTestAccount_ti=0	
				
				<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelseif filterItem.NAME EQ ' u.MFAContactString_vch '>
								AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>             
                ORDER BY 
                    #order# #sSortDir_0#
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetList">                
                
                <cfset fullName = FirstName_vch&" "&LastName_vch/>
				<cfif TransactionStatus EQ 0>
					<cfset status="Waiting for review">
				<cfelseif TransactionStatus EQ 1> 
					<cfset status="Approval by " & AdmActionBy_int & " on "& DateFormat(AdmActionDate_dt,'mm/dd/yyyy')>
				<cfelseif TransactionStatus EQ -1> 
					<cfset status="Reject by " & AdmActionBy_int & " on "& DateFormat(AdmActionDate_dt,'mm/dd/yyyy')>
				</cfif>	
				<cfif ModuleName_vch EQ "Upgrade Plan">
					<cfset notes= PlanName_vch>
				<cfelseif ModuleName_vch EQ "Buy Credits and Keywords">
					<cfset notes= BuyNumberCredits & " Credits, "& BuyNumberKeywords& " Keywords">					
				</cfif>		
				
				<cfif PaymentGateway_ti EQ 1>
					<cfset gwText='Worldpay'>
				<cfelseif  PaymentGateway_ti EQ 2>
					<cfset gwText='Paypal'>
				<cfelseif  PaymentGateway_ti EQ 3>
					<cfset gwText='Mojo'>
				<cfelseif  PaymentGateway_ti EQ 4>
					<cfset gwText='Total Apps'>
				</cfif>
                <cfset tempItem = {		
					ID= Id_bi,
					VAULTID= VaultId_vch,			
					USERID = '#UserId_int#',
					NAME = "#fullName#",
					EMAIL = "#EmailAddress_vch#",
					PHONE = "#MFAContactString_vch#",
					BUYPLAN =	"#BuyPlanId_int#",
					BUYCREDITS =	"#BuyNumberCredits#",
					BUYKEYWORDS =	"#BuyNumberKeywords#",
					TRANSACTION = "#ModuleName_vch#",
					TRANSACTION_DATE = "#DateFormat(TransactionDate_dt,'mm/dd/yyyy')#",					
					TRANSACTION_AMOUNT = "$#NumberFormat(Amount_dec,'0.00')#",		                    					
					TRANSACTIONSTATUS= status,
					TRANSACTIONSTATUS_CODE= TransactionStatus,
					GATEWAY= gwText,
					NOTES = notes					
				}>	 
                <cfset ArrayAppend(dataout["ListData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	
	<cffunction name="GetCCCheckByID" access="remote" output="true" hint="Get CC Checked By ID">

    	<cfargument name="inpId" TYPE="string" required="true" >				

        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.CCSATUS = "">
		<cfset dataout.DATACHECK={
				FirstNameMatch_ti: 0,
				LastNameMatch_ti: 0,
				AddressMatch_ti: 0,
				CityMatch_ti: 0,
				StateMatch_ti: 0,
				CountryMatch_ti: 0,
				PostCodeMatch_ti: 0,
				EmailMatch_ti : 0,
				PhoneMatch_ti: 0,
				Phone_vch:'',
				IpMatch_ti: 0,
				IpToIp_Mile_dec:0,
				LastProfileUpdateDay_vch:'',
				AVS_Result_vch:''
		} >
		<cfset dataout.PROFILE={} >
		<cfset dataout.CARDDATA= {}>
        <cfset var GetCCCheckByID = ''/>
		<cfset var UserId=0>	
		<cfset var GetProfileInfo={}>
					

        <cftry>
			<cfquery name="GetCCCheckByID" datasource="#Session.DBSourceEBM#">
				SELECT 					
					lpr.UserId_int,
					lpr.VaultId_vch,					
					lpr.FirstNameMatch_ti,
					lpr.LastNameMatch_ti,
					lpr.AddressMatch_ti,
					lpr.CityMatch_ti,
					lpr.StateMatch_ti,
					lpr.CountryMatch_ti,
					lpr.PostCodeMatch_ti,
					lpr.EmailMatch_ti,
					lpr.PhoneMatch_ti,
					lpr.Phone_vch,
					lpr.IpMatch_ti,		
					lpr.IpToIp_Mile_dec,
					lpr.LastProfileUpdateDay_vch,
					lpr.AVS_Result_vch,

					lpr.AdmActionBy_int,
					lpr.AdmActionDate_dt,
					lpr.CardData_txt,
					lpr.Status_ti as TransactionStatus,
					lpr.CC_Status_ti,

					lcr.Status_ti as CCStatus
				FROM 
					simplebilling.list_payment_review lpr		
				INNER JOIN
					simplebilling.list_cc_review lcr
				ON
					lpr.UserId_int =lcr.UserId_int
				AND
					lpr.VaultId_vch =lcr.VaultId_vch
				WHERE
					lpr.Id_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#" >				
			</cfquery>
			<cfif GetCCCheckByID.RECORDCOUNT GT 0>
				<cfset UserId= GetCCCheckByID.UserId_int>
				<cfset dataout.CCSATUS = GetCCCheckByID.CC_Status_ti>
				<cfset dataout.DATACHECK={
						FirstNameMatch_ti: GetCCCheckByID.FirstNameMatch_ti,
						LastNameMatch_ti: GetCCCheckByID.LastNameMatch_ti,
						AddressMatch_ti: GetCCCheckByID.AddressMatch_ti,
						CityMatch_ti: GetCCCheckByID.CityMatch_ti,
						StateMatch_ti: GetCCCheckByID.StateMatch_ti,
						CountryMatch_ti: GetCCCheckByID.CountryMatch_ti,
						PostCodeMatch_ti: GetCCCheckByID.PostCodeMatch_ti,
						EmailMatch_ti : GetCCCheckByID.EmailMatch_ti,
						PhoneMatch_ti: GetCCCheckByID.PhoneMatch_ti,
						Phone_vch: GetCCCheckByID.Phone_vch,
						IpMatch_ti: GetCCCheckByID.IpMatch_ti,
						IpToIp_Mile_dec: GetCCCheckByID.IpToIp_Mile_dec ,
						LastProfileUpdateDay_vch: GetCCCheckByID.LastProfileUpdateDay_vch,
						AVS_Result_vch:GetCCCheckByID.AVS_Result_vch,
						CCStatus=  GetCCCheckByID.CCStatus,
						TransactionStatus=  GetCCCheckByID.TransactionStatus
				} >
				<cfset dataout.CARDDATA= DeserializeJSON(GetCCCheckByID.CardData_txt)>
			</cfif>
			

			<!--- collect data for compare--->		
			<cfquery name="GetProfileInfo" datasource="#Session.DBSourceREAD#">
				SELECT                        
					EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,City_vch,State_vch,
					Country_vch,
					LastProfileUpdate_dt
				FROM					
					simpleobjects.useraccount									
				WHERE					 
					UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#" >					   
			</cfquery>							
                    
			<cfset dataout.PROFILE.USERID = '#UserId#'>
			<cfset dataout.PROFILE.EMAIL = '#GetProfileInfo.EmailAddress_vch#'>                    
			<cfset dataout.PROFILE.MFAPHONE = '#GetProfileInfo.MFAContactString_vch#'>                    
			<cfset dataout.PROFILE.FIRSTNAME = '#GetProfileInfo.FirstName_vch#'>                    
			<cfset dataout.PROFILE.LASTNAME = '#GetProfileInfo.LastName_vch#'>
			<cfset dataout.PROFILE.ADDRESS = "#GetProfileInfo.Address1_vch#">
			<cfset dataout.PROFILE.POSTALCODE = "#GetProfileInfo.PostalCode_vch#">                    
			<cfset dataout.PROFILE.CITY = "#GetProfileInfo.City_vch#">
			<cfset dataout.PROFILE.STATE = "#GetProfileInfo.State_vch#">
			<cfset dataout.PROFILE.COUNTRY = "#GetProfileInfo.Country_vch#">
			<cfset dataout.PROFILE.PROFILELASTUPDATE = '#GetProfileInfo.LastProfileUpdate_dt#'>

	        <cfset dataout.RXRESULTCODE = 1 />
	        
	    <cfcatch>
    	    <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>
    </cffunction>	
	<cffunction name="AdminActionSuspiciousTransaction" access="remote" output="true" hint="Admin Action Suspicious Transaction">
    	<cfargument name="inpId" TYPE="string" required="true" >				
		<cfargument name="inpAction" TYPE="string" required="true" >				
		<cfargument name="inpCardDataApprovalForever" TYPE="string" required="false" default="" >
        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		
        <cfset var GetCCInfo = ''/>		
		<cfset var userID=''>		
		<cfset var vaultID=''>

		<cfset var updateCCStatus = ''/>
		<cfset var updateTransactionStatus = ''/>

		<cfset var transactionStatus = '0'/>
		<cfset var rtAdminActionSuspiciousTransactionBuyCreditsAndKeywords={}>
		<cfset var rtAdminActionSuspiciousTransactionBuyPlan={}>
		
		<cfset var paymentGateway="">
		<cfset var amount=0>
		<cfset var paymentData={}>
		<cfset var moduleName="">

        <cfset var email = ''/>
        <cfset var paymentdataObject = ''/>
		<cfset var saveCC=0>
		

        <cftry>
			<cfif arguments.inpAction EQ "1" OR arguments.inpAction EQ "2">
				<cfset transactionStatus = 1/>
			<cfelseif arguments.inpAction EQ "3" OR arguments.inpAction EQ "4">
				<cfset transactionStatus = -1/>
			</cfif>
			<cfquery name="GetCCInfo" datasource="#Session.DBSourceREAD#">
				SELECT 				
					lpr.ModuleName_vch,	
					lpr.UserId_int,
					lpr.VaultId_vch,
					lpr.PaymentGateway_ti,
					lpr.Amount_dec,
					lpr.CardData_txt						
				FROM 
					simplebilling.list_payment_review lpr						
				WHERE
					lpr.Id_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#" >				
			</cfquery>		
			<cfset userID=GetCCInfo.UserId_int>		
			<cfset vaultID=GetCCInfo.VaultId_vch>
			<cfset paymentGateway=GetCCInfo.PaymentGateway_ti>	
			<cfset amount= GetCCInfo.Amount_dec>	
			<cfset paymentData= GetCCInfo.CardData_txt>	
			<cfset moduleName=GetCCInfo.ModuleName_vch>	
			<cfif structKeyExists(deserializeJSON(paymentData), "saveccinfo")>
				<cfset saveCC=deserializeJSON(paymentData).saveccinfo>
			</cfif>
			

			<cfif transactionStatus EQ 1>
				<cfif moduleName EQ "Buy Credits and Keywords">
					<cfinvoke method="AdminActionSuspiciousTransactionBuyCreditsAndKeywords" returnvariable="rtAdminActionSuspiciousTransactionBuyCreditsAndKeywords">
						<cfinvokeargument name="inpUserId" value="#userID#"/>
						<cfinvokeargument name="inpOrderAmount" value="#amount#"/>
						<cfinvokeargument name="inpPaymentdata" value="#paymentData#"/>
						<cfinvokeargument name="inpPaymentGateway" value="#paymentGateway#"/>					
						<cfinvokeargument name="inpSaveCCInfo" value="#saveCC#"/>		
						<cfinvokeargument name="inpVaultId" value="#vaultID#"/>		
						<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpId#"/>		
						<cfinvokeargument name="inpModuleName" value="#moduleName#"/>		
						
					</cfinvoke>					
					<cfif rtAdminActionSuspiciousTransactionBuyCreditsAndKeywords.RXRESULTCODE NEQ 1>
						<cfthrow type="any" message="#rtAdminActionSuspiciousTransactionBuyCreditsAndKeywords.MESSAGE#" detail="#rtAdminActionSuspiciousTransactionBuyCreditsAndKeywords.MESSAGE#">
					</cfif>
				<cfelseif moduleName EQ "Upgrade Plan">
					
					<cfinvoke method="AdminActionSuspiciousTransactionBuyPlan" returnvariable="rtAdminActionSuspiciousTransactionBuyPlan">
						<cfinvokeargument name="inpUserId" value="#userID#"/>
						<cfinvokeargument name="inpOrderAmount" value="#amount#"/>
						<cfinvokeargument name="inpPaymentdata" value="#paymentData#"/>
						<cfinvokeargument name="inpPaymentGateway" value="#paymentGateway#"/>					
						<cfinvokeargument name="inpSaveCCInfo" value="#saveCC#"/>		
						<cfinvokeargument name="inpVaultId" value="#vaultID#"/>		
						<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpId#"/>		
						<cfinvokeargument name="inpModuleName" value="#moduleName#"/>		
						
					</cfinvoke>					
					<cfif rtAdminActionSuspiciousTransactionBuyPlan.RXRESULTCODE NEQ 1>
						<cfthrow type="any" message="#rtAdminActionSuspiciousTransactionBuyPlan.MESSAGE#" detail="#rtAdminActionSuspiciousTransactionBuyPlan.MESSAGE#">
					</cfif>
				</cfif>	
			</cfif>
			
			
			<cfquery name="updateCCStatus" datasource="#Session.DBSourceEBM#">
				UPDATE 					
					simplebilling.list_cc_review					
				SET
					Status_ti= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAction#" >,				
					AdmActionBy_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >,
					AdmActionDate_dt= NOW()
					<cfif arguments.inpAction EQ 2>
					,CardDataApprovalForever_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCardDataApprovalForever#" >
					</cfif>
				WHERE
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userID#" >
				AND
					VaultId_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#vaultID#" >

			</cfquery>
			<cfquery name="updateTransactionStatus" datasource="#Session.DBSourceEBM#">
				UPDATE 					
					simplebilling.list_payment_review					
				SET
					Status_ti= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#transactionStatus#" >,				
					CC_Status_ti= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAction#" >,				
					AdmActionBy_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >,
					AdmActionDate_dt= NOW()
				WHERE
					Id_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#" >				
			</cfquery>


			<cfif arguments.inpAction EQ "3" OR arguments.inpAction EQ "4">
				<cfset paymentdataObject =  DeserializeJSON(paymentData)>
				<cfif paymentdataObject.inpEmail EQ ''>
					<cfset email = paymentdataObject.h_email/>
				<cfelse>
					<cfset email = paymentdataObject.inpEmail/>
				</cfif>
				<cftry>
                    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                        <cfinvokeargument name="to" value="#email#">
                        <cfinvokeargument name="type" value="html">
                        <cfinvokeargument name="subject" value="Transaction Declined">
                        <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_decline.cfm">
                        <cfinvokeargument name="data" value="#arguments.inpCardDataApprovalForever#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>
			</cfif>

	        <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update transaction successfully" />
	        
	    <cfcatch>
    	    <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>
    </cffunction>
	<cffunction name="AdminActionSuspiciousTransactionBuyCreditsAndKeywords" access="remote" output="true" hint="core buy keyword,credits">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">	    
        <cfargument name="inpSaveCCInfo" TYPE="string" required="false" default="0" hint="Save cc for user">
		<cfargument name="inpVaultId" TYPE="string" required="true">
        <cfargument name="inpPaymentReviewId" TYPE="string" required="true">
		<cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">        

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
        <cfset var RxMakePayment = {} />
        <cfset var DoAddPurchasedCreditKeywordResult = {} />
        <cfset var RevalupdatePaymentWorlPay = {}/>
        <cfset var RxStoreCreditCardInVault = {}/>

        <cfset var paymentdata = ''/>
        <cfset var email = ''/>

        <cftry>
            <cfif arguments.inpOrderAmount GT 0>
             
                <cfswitch expression="#arguments.inpPaymentGateway#">
					<cfcase value="1">
						
					</cfcase>

					<cfcase value="2">
						<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
					<cfcase value="3">
						<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
					<cfcase value="4">
						<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
				</cfswitch>

                <cfif RxMakePayment.RXRESULTCODE EQ 1> <!--- IF MAKE PAYMENT SUCCESS THEN ADD CC,KW TO USER --->
                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>

                    <cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
                        <cfinvokeargument name="inpNumberSMS" value="#paymentdata.numberSMSToSend#">
                        <cfinvokeargument name="inpNumberKeyword" value="#paymentdata.numberKeywordToSend#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1>
                        <cfset dataout.RESULT = "Success"/>
                        <cfset dataout.MESSAGE = "You have successfully purchased!"/>

                      

                        <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePayment, "REPORT")>
                            <cfset logData =  RxMakePayment['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                            <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#">
                            <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>

                        <!--- <cfif inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
								<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#">
								
                            </cfinvoke> 
                        </cfif> --->
						

                        <!--- SEND MAIL TO CUSTOMER --->
                        <!--- Sendmail payment --->
                        <cfif paymentdata.inpEmail EQ ''>
							<cfset email = paymentdata.h_email/>
						<cfelse>
							<cfset email = paymentdata.inpEmail/>
						</cfif>
                        <cfset RxMakePayment.REPORT.EMAIL = email/>
                        <cfset RxMakePayment.REPORT.AMT = arguments.inpOrderAmount/>
                        <cfset RxMakePayment.REPORT.numberSMS = paymentdata.numberSMSToSend />
                        <cfset RxMakePayment.REPORT.numberKeyword = paymentdata.numberKeywordToSend/>
                        <cftry>
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#email#">
                                <cfinvokeargument name="type" value="html">
                                <cfinvokeargument name="subject" value="Transaction Approved">
                                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                <cfinvokeargument name="data" value="#SerializeJSON(RxMakePayment)#">
                            </cfinvoke>
                            <cfcatch type="any">
                            </cfcatch>
                        </cftry>

                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = "#DoAddPurchasedCreditKeywordResult.MESSAGE#" />
                        <cfset dataout.RESULT = "Payment failed"/>        
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = "#RxMakePayment.MESSAGE#" />
                    <cfset dataout.RESULT = "Payment failed"/>    
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
	<cffunction name="AdminActionSuspiciousTransactionBuyPlan" access="remote" output="true" hint="core buy plan">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">		
        <cfargument name="inpSaveCCInfo" TYPE="string" required="false" default="0" hint="Save cc for user (1)">

		<cfargument name="inpVaultId" TYPE="string" required="true">
        <cfargument name="inpPaymentReviewId" TYPE="string" required="true">
		<cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords"> 

        <cfset var dataout  = {} />
        <cfset var rsgetsettdata    = {} />
        
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.RESULT = ""/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var logData = {} />
		<cfset var RxMakePayment = ""/>
		<cfset var DoUpgradePlanResult = ""/>
		<cfset var RevalupdatePaymentWorlPay = ""/>
		<cfset var doUpgradePlanResult = ""/>

        <cfset var paymentdata = ''/>
        <cfset var emailData = ''/>
        <cfset var RxStoreCreditCardInVault = ''/>
		<cfset var email = ''/>

        <cftry>

            <cfif arguments.inpOrderAmount GT 0>
				<cfswitch expression="#arguments.inpPaymentGateway#">
					<cfcase value="#_WORLDPAY_GATEWAY#">
						
					</cfcase>

					<cfcase value="#_PAYPAL_GATEWAY#">
						<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
					<cfcase value="#_MOJO_GATEWAY#">
						<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
					<cfcase value="#_TOTALAPPS_GATEWAY#">
						<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithSuspiciousVault" returnvariable="RxMakePayment">
							<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
							<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#"/>
							<cfinvokeargument name="inpPaymentReviewId" value="#arguments.inpPaymentReviewId#"/>
							
						</cfinvoke>
					</cfcase>
				</cfswitch>




                <cfif RxMakePayment.RXRESULTCODE EQ 1> <!--- MAKE PAYMENT SUCCESS THEN BUY PLAN TO USER --->
                    <cfset paymentdata =  DeserializeJSON(inpPaymentdata)>

					<cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="doUpgradePlanResult">
						<cfinvokeargument name="inpPlanId" value="#paymentdata.planId#">
						<cfinvokeargument name="inpAdminDo" value="#Session.UserId#">
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">						
						<cfinvokeargument name="inpPromotionCode" value="#paymentdata.inpPromotionCode#">
						<cfinvokeargument name="inpMonthYearSwitch" value="#paymentdata.monthYearSwitch#">
						<cfinvokeargument name="inpListKeyword" value="">
						<cfinvokeargument name="inpListMLP" value="">
						<cfinvokeargument name="inpListShortUrl" value="">
						<cfinvokeargument name="inpPaymentGateWay" value="#arguments.inpPaymentGateway#">
					</cfinvoke>
					<cfif doUpgradePlanResult.RXRESULTCODE EQ 1>
						<cfif paymentdata.inpEmail EQ ''>
							<cfset email = paymentdata.h_email/>
						<cfelse>
							<cfset email = paymentdata.inpEmail/>
						</cfif>
						<cfif email NEQ ''>
							<cfset emailData ={}/>
							<cfset emailData['FullName'] = paymentdata.inpFirstName/>
							<cfset emailData['numberKeywords'] = 0/>
							<cfset emailData['numberSMS'] = 0/>
							<cfset emailData['authorizedAmount'] = arguments.inpOrderAmount/>
							<cfset emailData['transactionId'] = RxMakePayment.REPORT.TRANSACTIONID/>
							<cfset emailData['planName'] = doUpgradePlanResult['PLANNAME']/>
							<!--- Sendmail payment --->

							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							<cfinvokeargument name="to" value="#email#">
							<cfinvokeargument name="type" value="html">
							<cfinvokeargument name="subject" value="Transaction Approved">
							<cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
							<cfinvokeargument name="data" value="#serializeJSON(emailData)#">
							</cfinvoke>
						</cfif>

						 <!--- LOG PAYMENT SUCCESS --->
                        <cfif structKeyExists(RxMakePayment, "REPORT")>
                            <cfset logData =  RxMakePayment['REPORT']>
                        </cfif>
                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan" returnvariable="RevalupdatePaymentWorlPay">
                           <!---  <cfinvokeargument name="numberKeyword" value="#paymentdata.numberKeywordToSend#">
                            <cfinvokeargument name="numberSMS" value="#paymentdata.numberSMSToSend#"> --->
                            <cfinvokeargument name="moduleName" value="Buy Plan">
                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentGateway#">
                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        </cfinvoke>

                        <!--- <cfif inpSaveCCInfo EQ 1>
                            <cfinvoke component="session.sire.models.cfc.payment.vault" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
                                <cfinvokeargument name="inpCardObject" value="#inpPaymentdata#">
                                <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
								<cfinvokeargument name="inpVaultId" value="#arguments.inpVaultId#">
                            </cfinvoke> 
                        </cfif> --->

						<cfset dataout.RXRESULTCODE = 1/>	
						<cfset dataout.MESSAGE = 'Payment transaction successfully'/>
					<cfelse>
						<cfset dataout.RXRESULTCODE = -3/>	
						<cfset dataout.MESSAGE = 'Payment successfully but can not add credits/keywords.'/>
					</cfif>
				<cfelse>
					<cfset dataout.MESSAGE =  RxMakePayment.MESSAGE/>
					<cfset dataout.ERRMESSAGE = RxMakePayment.MESSAGE />
				</cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <cffunction name="MakePaymentWithUserCardId" access="public" output="true" hint="Make Payment With User CardId">

    	<cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inpOrderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="840">
        <cfargument name="inpPaymentdata" TYPE="string" required="false" default="">
        <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
        <cfargument name="inpModuleName" TYPE="string" required="false" default="">

        <cfset var RxSaleWithTokenzine = ''/>
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.DATA = {}>
		
		<cfset var cardObject={
			inpFirstName: '',
			inpLastName: '',
			inpLine1: '',
			inpCity: '',
			inpPostalCode: '',
			inpState: '',
			inpEmail: '',
			inpPhone:'',
			inpCountryCode: '',
			inpIpAddress:''			
		}>

        <cftry>
			
	        <cfswitch expression="#arguments.inpPaymentGateway#">
	        	<cfcase value="#_WORLDPAY_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-worldpay" method="storeCreditCardInVault" returnvariable="RxSaleWithTokenzine">
	            	</cfinvoke>
	            </cfcase>

	            <cfcase value="#_PAYPAL_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="RxSaleWithTokenzine">
						<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
	            		<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
	            		<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
	            	</cfinvoke>
	            </cfcase>
	            <cfcase value="#_MOJO_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="RxSaleWithTokenzine">
	            		<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
	            		<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
	            		<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
	            	</cfinvoke>
	            </cfcase>
				<cfcase value="#_TOTALAPPS_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="RxSaleWithTokenzine">
	            		<cfinvokeargument name="inpOrderAmount" value="#arguments.inpOrderAmount#"/>
	            		<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
	            		<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
	            	</cfinvoke>
	            </cfcase>
	        </cfswitch>

	        <cfif RxSaleWithTokenzine.RXRESULTCODE EQ 1>
	        	<cfset dataout.RXRESULTCODE = 1>
	        	<cfset dataout.RESULT = "Success">
	        	<cfif structKeyExists(RxSaleWithTokenzine, "REPORT")>
	        		<cfset dataout.REPORT = RxSaleWithTokenzine.REPORT>
	        	</cfif>
	        <cfelse>
	        	<cfset dataout.RXRESULTCODE = RxSaleWithTokenzine.RXRESULTCODE>
	        	<cfset dataout.MESSAGE = RxSaleWithTokenzine.MESSAGE>
	        </cfif>
	    <cfcatch>
    	    <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>
    </cffunction>	

   <cffunction name="MakePaymentWithCC" access="remote" output="true" hint="Make Payment With CC">
   	    <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
   	    <cfargument name="inpPaymentGateway" TYPE="string" required="true" default="1">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpPaymentdata" TYPE="string" required="true">
        <cfargument name="inpModuleName" TYPE="string" required="false" default="">
      

        <cfset var RxSaleWithCC = ''/>
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.DATA = {}>
        <cfset var paymentData = {}/>
        <cfset var RxSaleWithTokenzine = {}/>				
        <cftry>			
			
	        <cfswitch expression="#arguments.inpPaymentGateway#">
	        	<cfcase value="#_WORLDPAY_GATEWAY#">
	            	
	            </cfcase>

	            <cfcase value="#_PAYPAL_GATEWAY#">
	            	<cfif isJson(inpPaymentdata)>
						<cfset paymentData = deserializeJson(inpPaymentdata)>
						<cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,4)>
						<cfinvoke component="session.sire.models.cfc.vault-paypal" method="Sale" returnvariable="RxSaleWithTokenzine">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inporderAmount" value="#arguments.inpOrderAmount#">
							<cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">		
							<cfinvokeargument name="inpcurrencyCode" value="USD">
							<cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
							<cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
							<cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
							<cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
							<cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
							<cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
							<cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
							<cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
							<cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
							<cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
							<cfinvokeargument name="inpbillingPhone" value="#paymentData.phone#">
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
						</cfinvoke>	  						
					<cfelse>
						<cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>				
					</cfif>  
	            </cfcase>

	            <cfcase value="#_MOJO_GATEWAY#">	         
					<cfif isJson(inpPaymentdata)>
						<cfset paymentData = deserializeJson(inpPaymentdata)>
						<cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,2)>
						<cfinvoke component="session.sire.models.cfc.vault-mojo" method="Sale" returnvariable="RxSaleWithTokenzine">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inporderAmount" value="#arguments.inpOrderAmount#">
							<cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">		
							<cfinvokeargument name="inpcurrencyCode" value="840">
							<cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
							<cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
							<cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
							<cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
							<cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
							<cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
							<cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
							<cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
							<cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
							<cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
							<cfinvokeargument name="inpbillingPhone" value="#paymentData.phone#">
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
						</cfinvoke>	  						
					<cfelse>
						<cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>				
					</cfif>  						          	
	            	
	            </cfcase>
				<cfcase value="#_TOTALAPPS_GATEWAY#">	         
					<cfif isJson(inpPaymentdata)>
						<cfset paymentData = deserializeJson(inpPaymentdata)>
						<cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,2)>
						<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="Sale" returnvariable="RxSaleWithTokenzine">
							<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
							<cfinvokeargument name="inporderAmount" value="#arguments.inpOrderAmount#">
							<cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">		
							
							<cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
							<cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
							<cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
							<cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
							<cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
							<cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
							<cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
							<cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
							<cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
							<cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
							<cfinvokeargument name="inpbillingPhone" value="#paymentData.phone#">
							<cfinvokeargument name="inpModuleName" value="#arguments.inpModuleName#"/>
						</cfinvoke>	  						
					<cfelse>
						<cfset dataout.RXRESULTCODE = -2>
						<cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>				
					</cfif>  						          	
	            	
	            </cfcase>

	        </cfswitch>

	        <cfif RxSaleWithTokenzine.RXRESULTCODE EQ 1>
	        	<cfset dataout.RXRESULTCODE = 1>
	        	<cfset dataout.RESULT = "Success">
	        	<cfif structKeyExists(RxSaleWithTokenzine, "REPORT")>
	        		<cfset dataout.REPORT = RxSaleWithTokenzine.REPORT>
	        	</cfif>
	        <cfelse>
	        	<cfset dataout.RXRESULTCODE = RxSaleWithTokenzine.RXRESULTCODE>
	        	<cfset dataout.MESSAGE = RxSaleWithTokenzine.MESSAGE>
	        </cfif>
	    <cfcatch>
    	    <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
	    </cfcatch>    

        </cftry>

        <cfreturn dataout/>

   </cffunction>	


</cfcomponent>    