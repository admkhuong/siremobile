<cfcomponent>

	<!--- this cfc used to save and get cc information to/from Sire vault --->
	<!--- if you need implement another gateway, please refers to old gateway code --->

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">

    <cffunction name="storeCreditCardInVault" access="remote" output="true" hint="Store credit card to sire vault">
    	<cfargument name="inpCardObject" required="true" hint="credi card information in json object">
        <cfargument name="inpPaymentGateway" required="true" default="2" hint="gateway id">
        <cfargument name="inpUserId" required="false" default="#SESSION.USERID#" hint="user id">		
		

        <cfset var userCardId = ''/>
        <cfset var paymentType = ''/>
		<cfset var cardinfo = ''/>
		<cfset var extranote = ''/>

		<cfset var checkVaultStatus={}>
		<cfset var RxStoreCreditCardInVault = ''/>
		<cfset var RxTokenRegisterCardNumber = {}/>
		<cfset var RetUserAuthenInfo = ''/>
		<cfset var isResetFraud=0>
		<cfset var CardDataApprovalForever={}>
		<cfset var dataout = {}/>

		<cfset dataout.RXRESULTCODE = 0 /> 
		<cfset dataout.RESULT = ''/>                    
		<cfset dataout.MESSAGE = ''/>

		<cfset dataout.USERCARDID = ''/>
		<cfset dataout.AVSCODE = ''/>
		<cfset dataout.CCTYPE = ''/>

		<cfset var cardinfo = DESERIALIZEJSON(inpCardObject)/>
		<cfset var ResetFraund = {}/>
		<cfset var insertUserLog = {}/>
		<cfset var RetCaptureUserCCData = {} />

        <cftry>
        	<!--- call to make an Authorization request  --->
	        <cfswitch expression="#arguments.inpPaymentGateway#">
	        	<cfcase value="#_WORLDPAY_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-worldpay" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
	            	</cfinvoke>
	            </cfcase>

	            <cfcase value="#_PAYPAL_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-paypal" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
	            	</cfinvoke>
	            	<cfset paymentType = 'PP'/>
	            </cfcase>

	            <cfcase value="#_MOJO_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-mojo" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					</cfinvoke>

					<cfset paymentType = 'mj'/>					
	            </cfcase>
				<cfcase value="#_TOTALAPPS_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="storeCreditCardInVault" returnvariable="RxStoreCreditCardInVault">
						<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
					</cfinvoke>

					<cfset paymentType = 'TotalApps'/>					
	            </cfcase>
	        </cfswitch>

	        <!--- there are some case include both inpExpireMonth and inpExpreYear to expirationDate. We need to check and separate before save to db --->
			<cfif structKeyExists(cardinfo, "expirationDate")>
				<cfif !structKeyExists(cardinfo, "inpExpireMonth")>
					<cfset cardinfo.inpExpireMonth= Left(cardinfo.expirationDate,2)>
				</cfif>
				<cfif !structKeyExists(cardinfo, "inpExpreYear")>
					<cfset cardinfo.inpExpreYear= "20"&right(cardinfo.expirationDate,2)>
				</cfif>
			</cfif>
			<cfif !structKeyExists(cardinfo, "inpCvv2")>
				<cfset cardinfo.inpCvv2= "">
			</cfif>
			<cfset extranote = {
				inpNumber	= right(cardinfo.inpNumber, 4),
				inpType		= '',
				inpExpireMonth  = cardinfo.inpExpireMonth,
				inpExpreYear	= cardinfo.inpExpreYear,
				inpCvv2			= cardinfo.inpCvv2,
				inpFirstName	= cardinfo.inpFirstName,
				inpLastName		= cardinfo.inpLastName,
				inpLine1		= cardinfo.inpLine1,
				inpCity         = cardinfo.inpCity,
				inpCountryCode 	= cardinfo.inpCountryCode,
				inpPostalCode   = cardinfo.inpPostalCode,
				inpState		= cardinfo.inpState,
				inpEmail		= cardinfo.inpEmail,
				inpAVS			= RxStoreCreditCardInVault.AVSCODE
			}>			
						
			
	        <cfif RxStoreCreditCardInVault.RXRESULTCODE EQ 1>				
				<cfset var userCardId = RxStoreCreditCardInVault.USERCARDID />
				<cfset extranote['inpType'] = RxStoreCreditCardInVault.CCTYPE/>

				<cfset dataout.USERCARDID = RxStoreCreditCardInVault.USERCARDID />
				<cfset dataout.AVSCODE = RxStoreCreditCardInVault.AVSCODE />
				<cfset dataout.CCTYPE = RxStoreCreditCardInVault.CCTYPE />
	        	
	        	<!--- UPDATE TO DB VAULT --->
	        	<cfinvoke component="session.sire.models.cfc.billing" method="updateUserAuthen" returnvariable="RetUserAuthenInfo">
					<cfinvokeargument name="PaymentMethodID" value="#userCardId#"/>
					<cfinvokeargument name="PaymentType" value="#paymentType#"/>
					<cfinvokeargument name="userId" value="#arguments.inpUserId#">
					<cfinvokeargument name="intPaymentGateway" value="#arguments.inpPaymentGateway#">
					<cfinvokeargument name="intExtraNote" value="#TRIM( serializeJSON(extranote) )#">
				</cfinvoke>

				<!--- After user update their cc info, we need to reset Sire Fraud --->
				<cfquery name="checkVaultStatus" datasource="#Session.DBSourceEBM#">
					SELECT
						Status_ti,
						CardDataApprovalForever_txt
					FROM
						simplebilling.list_cc_review
					WHERE					
						UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" >
					AND
						VaultId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userCardId#" >
					ORDER BY
						AdmActionDate_dt DESC
					LIMIT 1
				</cfquery>	
				<cfif checkVaultStatus.RECORDCOUNT GT 0>
					
					<cfif Listfind("1,3,4",checkVaultStatus.Status_ti ) GT 0>
						<cfset isResetFraud=1>
					<cfelseif checkVaultStatus.Status_ti EQ 2> 
						<cfset CardDataApprovalForever= DeserializeJSON(checkVaultStatus.CardDataApprovalForever_txt)>
						<cfif reReplace(extranote.inpFirstName, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpFirstName, "\s+", "","all"))								
						AND reReplace(extranote.inpLastName, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpLastName, "\s+", "","all"))
						
						AND reReplace(extranote.inpLine1, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpLine1, "\s+", "","all"))		
						AND reReplace(extranote.inpCity, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpCity, "\s+", "","all"))		
						AND reReplace(extranote.inpPostalCode, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpPostalCode, "\s+", "","all"))
						AND reReplace(extranote.inpState, "\s+", "","all").EqualsIgnoreCase(reReplace(CardDataApprovalForever.inpState, "\s+", "","all"))>			
							<cfset isResetFraud=0> <!--- we do not reset if vault have status approve forever and old card information map with new card infomation  --->
						<cfelse>
							<cfset isResetFraud=1>
						</cfif>
					</cfif>

					<!--- reset fraud and save to log--->	
					<cfif isResetFraud EQ 1>
						<cfquery name="ResetFraund" datasource="#Session.DBSourceEBM#">
							UPDATE simplebilling.list_cc_review
							SET 
								Status_ti=0
							WHERE 
								UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
							AND
								VaultId_vch=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userCardId#">
						</cfquery>
						<cfinvoke method="createSireUserLog" component="session.sire.models.cfc.logs">
							<cfinvokeargument name="userId" value="#arguments.inpUserId#">
							<cfinvokeargument name="moduleName" value="Fraud check reset">							
							<cfinvokeargument name="operator" value="Reset Vault status when user update profile-[#userCardId#]">
						</cfinvoke> 
						
					</cfif>

				</cfif>

				<!--- Capture full user CC --->
				<cfinvoke component="session.sire.models.cfc.users" method="captureUserCCData" returnvariable="RetCaptureUserCCData">
					<cfinvokeargument name="inpUserID" value="#arguments.inpUserId#"/>
					<cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
					<cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentGateway#"/>
				</cfinvoke>
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 /> 
				<cfset dataout.RESULT = 'ERROR'>                    
				<cfset dataout.MESSAGE = RxStoreCreditCardInVault.MESSAGE>
				<cfreturn dataout />
	        </cfif>
			<cfset dataout.RXRESULTCODE = 1 /> 
			<cfset dataout.RESULT = 'SUCCESS'>                    
            <cfset dataout.MESSAGE = "New card was created">
			
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 /> 
				<cfset dataout.RESULT = 'ERROR'>                    
				<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			</cfcatch> 
			

        </cftry>
		<cfreturn dataout />
    </cffunction>

	<cffunction name="getDetailCreditCardInVault" access="remote" output="true" hint="get detail card infor in sire vault">
	 	<cfargument name="inpPaymentGateway" required="true" default="2" hint="gateway id">
        <cfargument name="inpUserId" required="false" default="#SESSION.USERID#">

        <cfset var userCardId = ''/>
        <cfset var paymentType = ''/>
        <cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 /> 
		<cfset dataout.RESULT = ''/>                    
		<cfset dataout.MESSAGE = 'Can not found user cc data.'/>
		<cfset var RxgetDetailCreditCardInVault = {}/>
		<cfset dataout.CUSTOMERINFO = {}/>

		<!--- return dataout.CUSTOMERINFO with credit card information --->
        <cftry>
	        <cfswitch expression="#arguments.inpPaymentGateway#">
	        	<cfcase value="#_WORLDPAY_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-worldpay" method="getDetailCreditCardInVault" returnvariable="RxgetDetailCreditCardInVault">
	            	</cfinvoke>
	            </cfcase>

	            <cfcase value="#_PAYPAL_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-paypal" method="getDetailCreditCardInVault" returnvariable="RxgetDetailCreditCardInVault">
						<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
	            	</cfinvoke>
	            	<cfset paymentType = 'PP'/>
	            </cfcase>

	            <cfcase value="#_MOJO_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-mojo" method="getDetailCreditCardInVault" returnvariable="RxgetDetailCreditCardInVault">
	            		<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
	            	</cfinvoke>
					<cfset paymentType = 'Mj'>
	            </cfcase>
				<cfcase value="#_TOTALAPPS_GATEWAY#">
	            	<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="getDetailCreditCardInVault" returnvariable="RxgetDetailCreditCardInVault">
	            		<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
	            	</cfinvoke>
					<cfset paymentType = 'TotalApps'>
	            </cfcase>
	        </cfswitch>

	        <cfif RxgetDetailCreditCardInVault.RXRESULTCODE EQ 1>
				<cfset dataout.CUSTOMERINFO = RxgetDetailCreditCardInVault.CUSTOMERINFO>
	        	<cfset dataout.RXRESULTCODE = 1 /> 
				<cfset dataout.RESULT = 'SUCCESS'>                    
	            <cfset dataout.MESSAGE = "Get info successfully.">
	        </cfif>
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1 /> 
			<cfset dataout.RESULT = 'ERROR'>                    
			<cfset dataout.MESSAGE = "Error while get info">
		</cfcatch> 
        </cftry>

		<cfreturn dataout />
    </cffunction>	

</cfcomponent>    