<cfcomponent>

    <cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">
   
    <cffunction name="GetAccessTokenKey" access="remote" output="true" hint="Get acces token key">                
                
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TOKENKEY = '' />
        <cfset dataout.EXPIRES = 0 />
        <cfset var getAccessToken=''> 
        <cfset var fileContent=''> 
        <cfset var RetVarGetAdminPermission	= '' />
        
        <cftry>
            <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>    
            <cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
                <cfset dataout.RESULT = "No Admin Permission.">
                <cfset dataout.MESSAGE = "No Admin Permission."/>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfreturn dataout>
            </cfif>
            <cfhttp url="#paypal_token#" method="post" result="getAccessToken">
                <cfhttpparam type="header" name="Authorization" value="Basic #ToBase64("#paypal_ClienID#:#paypal_Secret#")#" />
                <cfhttpparam type="header" name="Content-Type" value="application/x-www-form-urlencoded" >
                <cfhttpparam type="header" name="Accept" value=" application/json" >                    
                <cfhttpparam type="header" name="Accept-Language" value="en_US" >                  
                <cfhttpparam type="formField" name="grant_type" value="client_credentials" >
                
            </cfhttp>
            <cfset dataout.RESPONSE =getAccessToken />                 
            <cfif structKeyExists(getAccessToken, "status_code") AND getAccessToken.status_code EQ 200>
                <cfset dataout.RXRESULTCODE = 1 /> 
                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = getAccessToken.statuscode>    
                <cfset fileContent= getAccessToken.filecontent>
                
                <cfset dataout.TOKENKEY = DeserializeJSON(fileContent).access_token/>
                <cfset dataout.EXPIRES = DeserializeJSON(fileContent).expires_in />
            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = getAccessToken.errordetail>
                <cfset dataout.MESSAGE = getAccessToken.statuscode>                        
            </cfif>
                                        
            <cfcatch>                
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = "Error">                                                
            </cfcatch>
        </cftry>
        
        <cfreturn dataout />
    </cffunction> 
    <cffunction name="PaypalPayout" access="remote" output="true" hint="Paypal payout">                
        <cfargument name="inpToken" type="string" required="yes" default="">
        <cfargument name="inpSenderBatchId" type="string" required="yes" default="">
        <cfargument name="inpSubject" type="string" required="yes" default="">
        <cfargument name="inpReciptType" type="string" required="no" default="EMAIL">
        <cfargument name="inpAmount" type="string" required="yes" default="">
        <cfargument name="inpNotes" type="string" required="yes" default="">
        <cfargument name="inpReceiver" type="string" required="yes" default="">
        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.RXRESULTCODE = -1 />    
        <cfset dataout.MESSAGE = "Unable to payment">                                                    
        <cfset var payOut=''> 
        <cfset var rtUpdatePaymentCommission=''> 
        <cfset var rtCreatePaypalPaymentLogs=''> 
        <cfset var RetVarGetAdminPermission	= '' />
        
        <cfset var fileContent=''> 
        <cfset var JSONDATA="">
        <cfset var inplogPaymentPaypal={}>
        <cfset var tempFileContent=''> 
        <cfset var GetCommissionDetail=''>         
        
        <cftry>
            <cfquery name="GetCommissionDetail" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserID_int
                FROM
                    simplebilling.affiliate_commission_by_periods 
                WHERE 
                    Id_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSenderBatchId#">                
            </cfquery>
            <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>    
            <cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
                <cfset dataout.RESULT = "No Admin Permission.">
                <cfset dataout.MESSAGE = "No Admin Permission."/>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfreturn dataout>
            </cfif>            
            <cfset JSONDATA='{
                "sender_batch_header": {
                "sender_batch_id": "#arguments.inpSenderBatchId#",
                "email_subject": "#arguments.inpSubject#"
                },
                "items": [
                {
                    "recipient_type": "#arguments.inpReciptType#",
                    "amount": {
                    "value": "#arguments.inpAmount#",
                    "currency": "USD"
                    },
                    "note": "#arguments.inpNotes#",
                    "sender_item_id": "#arguments.inpSenderBatchId#",
                    "receiver": "#arguments.inpReceiver#"
                }
                ]
            }'>  
            
                        
            <cfhttp url="#paypal_payout#" method="post" result="payOut">
                <cfhttpparam type="header" name="Authorization" value="Bearer #arguments.inpToken#" />
                <cfhttpparam type="header" name="Content-Type" value="application/json" >
                <cfhttpparam type="body" value="#JSONDATA#">   
                
            </cfhttp>
            <cfset dataout.RESPONSE =payOut />                 
            <cfif structKeyExists(payOut, "status_code") AND payOut.status_code EQ 201>
                
                <cfinvoke method="UpdatePaymentCommission" component="session.sire.models.cfc.paypal-payout" returnvariable="rtUpdatePaymentCommission">
                    <cfinvokeargument name="inpId" value="#arguments.inpSenderBatchId#">
                    <cfinvokeargument name="inpAmount" value="#arguments.inpAmount#">
                </cfinvoke>    
                <cfif rtUpdatePaymentCommission.RXRESULTCODE NEQ 1>                                        
                    <!--- add rollback payment--->                    
                    <cfset dataout.RXRESULTCODE = -1 />                     
                    <cfset dataout.MESSAGE = rtUpdatePaymentCommission.MESSAGE>  
                </cfif>
                <!--- --->
                <cfset fileContent= payOut.filecontent>
                
                <cfset inplogPaymentPaypal= {
                    UserId_int: GetCommissionDetail.UserID_int,							
                    AdminAction_int : Session.UserID ,
                    SourceId_int : arguments.inpSenderBatchId,
                    PayoutBatchId_vch: DeserializeJSON(fileContent).batch_header.payout_batch_id,
                    ModuleName_vch: 'Affiliate Commission',
                    Status_code_vch: payOut.status_code,
                    Status_text_vch: payOut.status_text,
                    Errordetail_vch: payOut.errordetail,
                    Filecontent_txt: fileContent,
                    Paymentdata_txt:JSONDATA 
                }>

                <cfinvoke method="CreatePaypalPaymentLogs" component="session.sire.models.cfc.paypal-payout" returnvariable="rtCreatePaypalPaymentLogs">
                    <cfinvokeargument name="inplogPaymentPaypal" value="#inplogPaymentPaypal#">                    
                </cfinvoke>    
                <cfif rtCreatePaypalPaymentLogs.RXRESULTCODE NEQ 1>                    
                    <!--- add rollback payment--->                    
                    <cfset dataout.RXRESULTCODE = -1 />                     
                    <cfset dataout.MESSAGE = rtCreatePaypalPaymentLogs.MESSAGE>  
                </cfif>

                <cfset dataout.RXRESULTCODE = 1 /> 
                <cfset dataout.RESULT = 'Payment create successfully.'>
                <cfset dataout.MESSAGE = "Payment create successfully.">    	
                                           
            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = payOut.errordetail>
                <cfset dataout.MESSAGE = payOut.statuscode>                        
            </cfif>
                                        
            <cfcatch>                
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.detail>                                                
            </cfcatch>
        </cftry>
        
        <cfreturn dataout />
    </cffunction> 
    <cffunction name="GetPaypalPaymentItemDetail" access="remote" output="true" hint="get detail of paypal payment item">
        <cfargument name="inpToken" type="string" required="yes" default="">                
        <cfargument name="inpPayoutBatchId" TYPE="string" required="yes" default="0" />	
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.RXRESULTCODE = -1 />   
        <cfset dataout.DATA=''>               
        <cfset dataout.PAYMENT_AMOUNT=''>     
        <cfset dataout.PAYMENT_STATUS=''>     
        
        <cfset var getDetail=''> 
        <cfset var fileContent=''> 
        <cfset var RetVarGetAdminPermission	= '' />
        
        <cftry>
            <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>    
            <cfif RetVarGetAdminPermission.RXRESULTCODE NEQ 1>
                <cfset dataout.RESULT = "No Admin Permission.">
                <cfset dataout.MESSAGE = "No Admin Permission."/>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfreturn dataout>
            </cfif>
            <cfhttp url="#paypal_get_payment_item_detail##arguments.inpPayoutBatchId#?fields=batch_header" method="get" result="getDetail">
                <cfhttpparam type="header" name="Authorization" value="Bearer #arguments.inpToken#" />
                <cfhttpparam type="header" name="Content-Type" value="application/json" >                                            
                
            </cfhttp>
            <cfset dataout.RESPONSE =getDetail />                 
            <cfif structKeyExists(getDetail, "status_code") AND getDetail.status_code EQ 200>
                <cfset dataout.RXRESULTCODE = 1 /> 
                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = getDetail.statuscode>    
                <cfset fileContent= getDetail.filecontent>
                
                <cfset dataout.DATA = DeserializeJSON(fileContent)/>                    
                <cfset dataout.PAYMENT_AMOUNT=dataout.DATA.batch_header.amount.value>     
                <cfset dataout.PAYMENT_STATUS=dataout.DATA.batch_header.batch_status>     
            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = getDetail.errordetail>
                <cfset dataout.MESSAGE = getDetail.statuscode>                        
            </cfif>
                                        
            <cfcatch>                
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = "Error">                                                
            </cfcatch>
        </cftry>
        
        <cfreturn dataout />
    </cffunction> 
    <cffunction name="UpdatePaymentCommission" access="remote" output="false" hint="Update payment commission">
		<cfargument name="inpId" TYPE="string" required="yes" default="0" />	
        <cfargument name="inpAmount" type="string" required="yes" default="">				
		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		
		
        <cftry>			
			
			<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
                UPDATE
                    simplebilling.affiliate_commission_by_periods
                SET						
                    PaymentStatus_ti=1,
                    PaymentDate_dt= NOW(),
                    PaymentConfirmation_dec= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#arguments.inpAmount#"/>	
                WHERE
                    Id_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>											                   
            </cfquery>
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="CreatePaypalPaymentLogs" access="remote" output="false" hint="Create log paypal payment">		
        <cfargument name="inplogPaymentPaypal" required="yes" default="">				
		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />	        
        <cfset var UpdateData=''/>
		
		
        <cftry>			
			
			<cfquery datasource="#Session.DBSourceEBM#" name="UpdateData">
                INSERT INTO
                    simplebilling.logs_payment_paypal
                    (																								
                        UserId_int,							
                        AdminAction_int,
                        SourceId_int,
                        PayoutBatchId_vch,
                        ModuleName_vch,
                        Status_code_vch,
                        Status_text_vch,
                        Errordetail_vch,
                        Filecontent_txt,
                        Paymentdata_txt,

                        Created_dt						
                    )
                VALUES
                    (							
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ISDEFINED('inplogPaymentPaypal.UserId_int') ? inplogPaymentPaypal.UserId_int : '0'#"/>,													
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ISDEFINED('inplogPaymentPaypal.AdminAction_int') ? inplogPaymentPaypal.AdminAction_int: '0'#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ISDEFINED('inplogPaymentPaypal.SourceId_int')? inplogPaymentPaypal.SourceId_int :'0'#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.PayoutBatchId_vch')? inplogPaymentPaypal.PayoutBatchId_vch :'0'#"/>,	
                        
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.ModuleName_vch')? inplogPaymentPaypal.ModuleName_vch : ''#"/>,																		
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.Status_code_vch')? inplogPaymentPaypal.Status_code_vch: ''#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.Status_text_vch')? inplogPaymentPaypal.Status_text_vch:''#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.Errordetail_vch') ? inplogPaymentPaypal.Errordetail_vch : ''#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.Filecontent_txt') ? inplogPaymentPaypal.Filecontent_txt :''#"/>,	
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ISDEFINED('inplogPaymentPaypal.Paymentdata_txt')? inplogPaymentPaypal.Paymentdata_txt :'' #"/>,	
                                                
                        NOW()
                    )							                   
            </cfquery>
			
			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
</cfcomponent>
