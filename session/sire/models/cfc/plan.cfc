<cfcomponent>
	<cfinclude template="/public/paths.cfm"/>

	<cffunction name="GetPlanList" access="remote" output="false" hint="get plan list">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="20" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="ASC">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_3" default="-1">
		<cfargument name="sSortDir_3" default="">
		<cfargument name="iSortCol_4" default="-1">
		<cfargument name="sSortDir_4" default="">
		<cfargument name="customFilter" default="">

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout["PlanList"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<cfset var filterData	= '' />
		<cfset var order	= '' />
		<cfset var sSortDir_0	= arguments.sSortDir_0 />
		<cfset var item	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetPlanList	= '' />
		<cfset var GetPlanCount	= '' />
		<cfset var RetVarGetAdminPermission	= '' />

		<cftry>
			<cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfset filterData = DeserializeJSON(arguments.customFilter) />

			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="1"> 
						<cfset order = "PlanName_vch" />
					</cfcase> 
					<cfcase value="2"> 
						<cfset order = "Amount_dec" />
					</cfcase> 
					<cfcase value="3"> 
						<cfset order = "Status_int"/>
					</cfcase> 
					<cfdefaultcase>
						<cfset order = "Order_int"/>   	
					</cfdefaultcase>
				</cfswitch>
			<cfelse>
				<cfset order = "PlanId_int"/>
			</cfif>

			<cfset sSortDir_0 = (uCase(sSortDir_0) EQ 'DESC' ? 'DESC' : 'ASC')/>

			<cfquery datasource="#Session.DBSourceREAD#" name="GetPlanList">
				SELECT 
				SQL_CALC_FOUND_ROWS

				PlanId_int,
				PlanName_vch,
				Status_int,
				Amount_dec,
				YearlyAmount_dec,
				UserAccountNumber_int,
				KeywordsLimitNumber_int,
				KeywordMinCharLimit_int,
				FirstSMSIncluded_int,
				CreditsAddAmount_int,
				PriceMsgAfter_dec,
				PriceKeywordAfter_dec,
				Order_int,
				MlpsLimitNumber_int,
				ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi,				
				DATE_FORMAT(LastUpdateDate_dt, '%m-%d-%Y %r') AS LastUpdateDate_dt
				FROM
				simplebilling.plans
				WHERE
				1
				<cfif customFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR# 
						<cfif filterItem.TYPE EQ 'CF_SQL_VARCHAR'>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='%#filterItem.VALUE#%'/>
						<cfelse>
							<CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'/>
						</cfif>
					</cfloop>
				</cfif>
				<cfif arguments.iSortCol_0 EQ 3>
					<cfif sSortDir_0 EQ "asc">
						ORDER BY FIELD(Status_int, 1, 8, -8,4,2, 0)
					<cfelse>
						ORDER BY FIELD(Status_int, 0,2,4, -8, 8, 1)
					</cfif>
				<cfelse>
					ORDER BY #order# #sSortDir_0#	
				</cfif>
				
			</cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="GetPlanCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="GetPlanList">
				<cfset item =  {
				PlanId_int = PlanId_int,
				PlanName_vch = PlanName_vch,
				Status_int = Status_int,
				Amount_dec = Amount_dec,
				YearlyAmount_dec = YearlyAmount_dec,
				UserAccountNumber_int = UserAccountNumber_int,
				KeywordsLimitNumber_int = KeywordsLimitNumber_int,
				KeywordMinCharLimit_int = KeywordMinCharLimit_int,
				FirstSMSIncluded_int = FirstSMSIncluded_int,
				CreditsAddAmount_int = CreditsAddAmount_int,
				PriceMsgAfter_dec = PriceMsgAfter_dec,
				PriceKeywordAfter_dec = PriceKeywordAfter_dec,
				Order_int = Order_int,
				MlpsLimitNumber_int = MlpsLimitNumber_int,
				ShortUrlsLimitNumber_int = ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi = MlpsImageCapacityLimit_bi,
				LastUpdateDate_dt=LastUpdateDate_dt
			}/>
			<cfset ArrayAppend(dataout["PlanList"], item) />
			</cfloop>
			<cfloop query="GetPlanCount">
				<cfset dataout["iTotalRecords"] = iTotalRecords/>
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords/>
			</cfloop>

			<cfset dataout.RXRESULTCODE = "1" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="ToggleStatus" access="remote" output="false" hint="toggle plan status">
		<cfargument name="inpPlanId" TYPE="numeric" required="true" default="0" />

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var UpdatePlan	= '' />
		<cfset var RetVarGetAdminPermission	= '' />

		<cftry>
			<cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfquery datasource="#Session.DBSourceEBM#" name="UpdatePlan">
				UPDATE
					simplebilling.plans
				SET
					Status_int = IF(Status_int = 0, 1, 0-Status_int)
				WHERE
					PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#"/>
				AND
					ABS(Status_int) = 8
			</cfquery>

			<cfset dataout.RXRESULTCODE = "1" />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetPlanById" access="public" output="false" hint="get plan by id">
		<cfargument name="inpPlanId" TYPE="numeric" required="true" default="0" />

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.PLAN = {
			RECORDCOUNT = 0,
			PlanId_int = 0,
			PlanName_vch = '',
			Status_int = 1,
			Amount_dec = 0,
			YearlyAmount_dec = 0,
			UserAccountNumber_int = 1,
			Controls_int = '',
			KeywordsLimitNumber_int = 3,
			KeywordMinCharLimit_int = 8,
			FirstSMSIncluded_int = 25,
			CreditsAddAmount_int = 0,
			PriceMsgAfter_dec = 2.7,
			PriceKeywordAfter_dec = 5,
			ByMonthNumber_int = 0,
			ExtraConfigs = '',
			Order_int = 100,
			MlpsLimitNumber_int = 2,
			ShortUrlsLimitNumber_int = 2,
			MlpsImageCapacityLimit_bi = 52428800			
		} />

		<cfset var GetPlan	= '' />
		<cfset var RetVarGetAdminPermission	= '' />

		<cftry>
			<cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>

			<cfquery datasource="#Session.DBSourceREAD#" name="GetPlan">
				SELECT
					PlanId_int,
					PlanName_vch,
					Status_int,
					Amount_dec,
					YearlyAmount_dec,
					UserAccountNumber_int,
					Controls_int,
					KeywordsLimitNumber_int,
					KeywordMinCharLimit_int,
					FirstSMSIncluded_int,
					CreditsAddAmount_int,
					PriceMsgAfter_dec,
					PriceKeywordAfter_dec,
					ByMonthNumber_int,
					ExtraConfigs,
					Order_int,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int,
					MlpsImageCapacityLimit_bi					
				FROM
					simplebilling.plans
				WHERE
					PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#"/>				
			</cfquery>

			<cfset dataout.PLAN = GetPlan />

			<cfset dataout.RXRESULTCODE = "1" />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetAllIntegrateCompany" access="remote" output="false" hint="Get all integrate company">
		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var GetAllIntegrateCompany=''/>
		<cftry>
			<cfquery datasource="#Session.DBSourceREAD#" name="GetAllIntegrateCompany">
				SELECT
					ID_int,
					CompanyName_vch

				FROM
					simpleobjects.integrate_company
				WHERE
					Status_ti = 1
			</cfquery>
			<cfset dataout.INTEGRATECPNLIST = GetAllIntegrateCompany />
			<cfset dataout.RXRESULTCODE = "1" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="SavePlan" access="remote" output="false" hint="get plan by id">
		<cfargument name="PlanId_int" type="numeric" default="0" />
		<cfargument name="PlanName_vch" type="string" default="" />
		<cfargument name="Status_int" type="numeric" default="8" />
		<cfargument name="Amount_dec" type="numeric" default="0" />
		<cfargument name="YearlyAmount_dec" type="numeric" default="0" />
		<cfargument name="UserAccountNumber_int" type="numeric" default="1" />
		<cfargument name="Controls_int" type="numeric" default="0" />
		<cfargument name="KeywordsLimitNumber_int" type="numeric" default="3" />
		<cfargument name="KeywordMinCharLimit_int" type="numeric" default="8" />
		<cfargument name="FirstSMSIncluded_int" type="numeric" default="25" />
		<cfargument name="CreditsAddAmount_int" type="numeric" default="0" />
		<cfargument name="PriceMsgAfter_dec" type="numeric" default="2.7" />
		<cfargument name="PriceKeywordAfter_dec" type="numeric" default="5" />
		<cfargument name="ByMonthNumber_int" type="numeric" default="0" />
		<cfargument name="ExtraConfigs" type="string" default="" />
		<cfargument name="Order_int" type="numeric" default="100" />
		<cfargument name="MlpsLimitNumber_int" type="numeric" default="2" />
		<cfargument name="ShortUrlsLimitNumber_int" type="numeric" default="2" />
		<cfargument name="MlpsImageCapacityLimit_bi" type="numeric" default="52428800" />		
		

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var UpdatePlan	= '' />
		<cfset var InsertPlan	= '' />
		<cfset var RetVarGetAdminPermission	= '' />
		<cfset var CheckPlanIsUsing	= '' />
		<cfset var NewStatusText=''/>
		<cfset var GetIntegrateCompany=''/>

		<cftry>
			<cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
				<cfinvokeargument name="inpSkipRedirect" value="1"/>
			</cfinvoke>
			<cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
				<cfthrow message="You do not have permission!" />
			</cfif>
			
			<!--- check plan is using wwith any user--->				
			<cfquery datasource="#Session.DBSourceREAD#" name="CheckPlanIsUsing">
				SELECT 	UserId_int
				FROM	simplebilling.userplans
				WHERE	PlanId_int=	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PlanId_int#"/>					
			</cfquery>
			<cfif CheckPlanIsUsing.RECORDCOUNT GT 0>
				<cfthrow type="Application" message="This plan is already in-use by other accounts, it cannot be edit." detail="This plan is already in-use by other accounts, it cannot be edit."/>
			</cfif>
			
			
			<cfif isNumeric(arguments.PlanId_int) AND arguments.PlanId_int GT 0>
				<cfquery datasource="#Session.DBSourceEBM#" name="UpdatePlan">
					UPDATE
						simplebilling.plans
					SET
						PlanName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PlanName_vch#"/>,
						Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Status_int#"/>,
						Amount_dec = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#arguments.Amount_dec#"/>,
						YearlyAmount_dec = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#arguments.YearlyAmount_dec#"/>,
						UserAccountNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.UserAccountNumber_int#"/>,
						Controls_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Controls_int#"/>,
						KeywordsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.KeywordsLimitNumber_int#"/>,
						KeywordMinCharLimit_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.KeywordMinCharLimit_int#"/>,
						FirstSMSIncluded_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.FirstSMSIncluded_int#"/>,
						CreditsAddAmount_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.CreditsAddAmount_int#"/>,
						PriceMsgAfter_dec = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#arguments.PriceMsgAfter_dec#"/>,
						PriceKeywordAfter_dec = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#arguments.PriceKeywordAfter_dec#"/>,
						ByMonthNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ByMonthNumber_int#"/>,
						ExtraConfigs = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.ExtraConfigs#"/>,
						Order_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Order_int#"/>,
						MlpsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MlpsLimitNumber_int#"/>,
						ShortUrlsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ShortUrlsLimitNumber_int#"/>,
						MlpsImageCapacityLimit_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#arguments.MlpsImageCapacityLimit_bi#"/>,
						LastUpdateBy_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.USERID#"/>,
						LastUpdateDate_dt=NOW()						
					WHERE
						PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PlanId_int#"/>					
				</cfquery>
			<cfelse>
				<cfquery datasource="#Session.DBSourceEBM#" name="InsertPlan">
					INSERT INTO
						simplebilling.plans
						(
							PlanName_vch,
							Status_int,
							Amount_dec,
							YearlyAmount_dec,
							UserAccountNumber_int,
							Controls_int,
							KeywordsLimitNumber_int,
							KeywordMinCharLimit_int,
							FirstSMSIncluded_int,
							CreditsAddAmount_int,
							PriceMsgAfter_dec,
							PriceKeywordAfter_dec,
							ByMonthNumber_int,
							ExtraConfigs,
							Order_int,
							MlpsLimitNumber_int,
							ShortUrlsLimitNumber_int,
							MlpsImageCapacityLimit_bi,
							
							CreateBy_int,
							LastUpdateBy_int,
							LastUpdateDate_dt							
						)
					VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.PlanName_vch#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Status_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Amount_dec#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.YearlyAmount_dec#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.UserAccountNumber_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Controls_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.KeywordsLimitNumber_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.KeywordMinCharLimit_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.FirstSMSIncluded_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.CreditsAddAmount_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PriceMsgAfter_dec#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.PriceKeywordAfter_dec#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ByMonthNumber_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.ExtraConfigs#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.Order_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MlpsLimitNumber_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.ShortUrlsLimitNumber_int#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MlpsImageCapacityLimit_bi#"/>,

							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"/>,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"/>,
							NOW()							
						)
				</cfquery>
			</cfif>

			<cfset dataout.RXRESULTCODE = "1" />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	<cffunction name="GetAllMasterCouponPlan" access="remote" output="false" hint="Get all master coupon plan">						

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset var GetAllMasterCouponPlan=''/>
		<cftry>
			<cfquery datasource="#Session.DBSourceREAD#" name="GetAllMasterCouponPlan">
				SELECT
					PlanId_int,
					PlanName_vch,
					Status_int,
					Amount_dec,
					YearlyAmount_dec,
					UserAccountNumber_int,
					Controls_int,
					KeywordsLimitNumber_int,
					KeywordMinCharLimit_int,
					FirstSMSIncluded_int,
					CreditsAddAmount_int,
					PriceMsgAfter_dec,
					PriceKeywordAfter_dec,
					ByMonthNumber_int,
					ExtraConfigs,
					Order_int,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int,
					MlpsImageCapacityLimit_bi,
					
					CreateBy_int,
					LastUpdateBy_int,
					LastUpdateDate_dt	

				FROM
					simplebilling.plans
				WHERE
					Status_int = 4
			</cfquery>
			<cfset dataout.MASTERCOUPONPLANLIST = GetAllMasterCouponPlan/>
			<cfset dataout.RXRESULTCODE = "1" />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />        
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	
</cfcomponent>	