<cfcomponent>
	<cffunction name='getActivePromoCode' access="remote" output="true" hint="get active promotion code via input code">
		<cfargument name="inpCode" type="string" required="yes">
		<cfargument name="inpIsYearlyPlan" type="string" required="no" default="0">
		<cfargument name="inpEmail" type="string" required="no">
		
		<cfset var getActivePromoCode = {} />
		<cfset var dataout	= {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout.promotion_id = 0 />
        <cfset dataout.recurring_time = 0 />
        <cfset dataout.promotion_discount = '' />
		
        <cfset var discountValue = '' />
        <cfset var percentageValue = '' />
        <cfset var flatrateValue = '' />
        <cfset var creditValue = '' />
        <cfset var keywordValue = '' />
        <cfset var mlpValue = '' />
        <cfset var shortUrlValue = '' />
        <cfset var findEmailInInvitationList = '' />

		<cftry>
			<cfset var now = Now()>

			<!--- <cfif arguments.inpIsYearlyPlan EQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
        		<cfset dataout.MESSAGE = "This coupon is not available for this plan. We are sorry for this inconvenience." />
				<cfreturn dataout />
			</cfif> --->
			
			<cfquery name="getActivePromoCode" datasource="#Session.DBSourceREAD#">
				SELECT
					promotion_id,
					promotion_desc,
					origin_id,
					recurring_time,
					expiration_date,
					DATEDIFF(expiration_date,CURDATE()) AS DaysUntilExpires,
					discount_plan_price_percent,
					discount_plan_price_flat_rate,
					promotion_credit,
					promotion_keyword,
					promotion_MLP,
					promotion_short_URL,
					discount_type_group,
					invitation_only_ti,
					invitation_email_list
				FROM
					`simplebilling`.`promotion_codes`
				WHERE
					latest_version = 1 
					AND promotion_code LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCode#"> 
					AND promotion_status = 1
					AND date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">
					AND IFNULL(expiration_date, date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">)
					AND (( redemption_count_int > 0 AND max_redemption_int > 0) OR max_redemption_int = 0)
					AND promotion_id > 0
					AND origin_id > 0
				ORDER BY
					promotion_id DESC
				LIMIT 1
			</cfquery>
			
				 
			<cfif getActivePromoCode.RecordCount GT 0>
				<cfloop query="getActivePromoCode">

					<cfif getActivePromoCode.invitation_only_ti EQ 1>
						<cfset findEmailInInvitationList = Find('#arguments.inpEmail#',getActivePromoCode.invitation_email_list)/>
						<cfif findEmailInInvitationList LTE 0>
							<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again." />
							<cfset dataout.RXRESULTCODE = -2 />
							<cfreturn dataout />
						</cfif>
					</cfif>
					<cfif getActivePromoCode.expiration_date EQ '' OR DaysUntilExpires GTE 0>
				        <cfset dataout.promotion_id = getActivePromoCode.promotion_id />
				        <cfset dataout.recurring_time = getActivePromoCode.recurring_time />
				        <cfset dataout.origin_id = getActivePromoCode.origin_id />

						<cfif getActivePromoCode.discount_plan_price_percent GT 0>
							<cfset percentageValue = '#getActivePromoCode.discount_plan_price_percent#' & '% off plan discount' & '<br/>'/>
						</cfif>
						<cfif getActivePromoCode.discount_plan_price_flat_rate GT 0>
							<cfset flatrateValue = '$'&'#NumberFormat(getActivePromoCode.discount_plan_price_flat_rate, '9.99')#' & ' plan price discount' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_credit GT 0>
							<cfset creditValue = '#getActivePromoCode.promotion_credit#' & ' addtional credits' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_keyword GT 0>
							<cfset keywordValue = '#getActivePromoCode.promotion_keyword#' & ' addtional keywords' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_MLP GT 0>
							<cfset mlpValue = '#getActivePromoCode.promotion_MLP#' & ' addtional MLPs' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_short_URL GT 0>
							<cfset shortUrlValue = '#getActivePromoCode.promotion_short_URL#' & ' addtional Short URLs' & '<br/>'/>
						</cfif>

						<cfif getActivePromoCode.discount_type_group EQ 0 >
							<!--- <cfset dataout.promotion_discount = 'No Discount' & '<br/>' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						<cfelseif getActivePromoCode.discount_type_group EQ 1 >
							<!--- <cfset dataout.promotion_discount = '#percentageValue#' & '#flatrateValue#' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						<cfelse>
							<!--- <cfset dataout.promotion_discount = '#creditValue#' & '#keywordValue#' & '#mlpValue#' & '#shortUrlValue#' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						</cfif>

						<cfset dataout.RXRESULTCODE = 1/>
				   	<cfelse>
				    	<cfset dataout.RXRESULTCODE = -3 />
				    	<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again." />
				    </cfif>
				</cfloop>
			<cfelse>
				<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again."/>
			</cfif>
			
            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		
		<cfreturn dataout /> 
	</cffunction>
	
	<cffunction name='insertUserPromoCode' access="public" output="false" hint="insert active promotion code for user input code">
		<cfargument name="inpUserId" type="numeric" required="yes">
		<cfargument name="inpPromoId" type="numeric" required="yes">
		<cfargument name="inpPromoOriginId" type="numeric" required="yes">
		<cfargument name="inpUsedDate" type="date" required="yes">
		<cfargument name="inpRecurringTime" type="numeric" required="yes">
		<cfargument name="inpPromotionStatus" type="numeric" required="yes">
		
		<cfset var insertUserPromoCode = {} />
		<cfset var getNewUserPromoCode = {} />
		<cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.UserPromotionId = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
		<cfquery name="insertUserPromoCode" datasource="#Session.DBSourceEBM#" result="getNewUserPromoCode">
			INSERT INTO 
				simplebilling.userpromotions
				(
					UserId_int,
					PromotionId_int,
					PromotionLastVersionId_int,
					UsedDate_dt,
					RecurringTime_int,
					UsedTimeByRecurring_int,
					UsedTimeByUpgrade_int,
					PromotionStatus_int
				)
			VALUES
				(
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPromoOriginId#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPromoId#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpUsedDate#">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpRecurringTime#">,
					0,
					0,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPromotionStatus#">
				)
		</cfquery>

		<cfset dataout.UserPromotionId = getNewUserPromoCode.GENERATED_KEY />
		<cfset dataout.RXRESULTCODE = 1 />

		<cfreturn dataout /> 
		
	</cffunction>

	<!--- CheckAvailableCouponForUser	 --->
	<cffunction name="CheckAvailableCouponForUser" access="remote" hint="Check if coupon is available for user">
		<cfargument name="inpUserId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout['DATALIST'] = [] />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = 0 />

		<cfset var checkAvailableCoupon = '' />
		<cfset var getUserCoupon = '' />
		<cfset var getCouponResult = '' />
		<cfset var disableCoupon = '' />
		<cfset var temp = {} />

		<cftry>
			<cfquery name="getUserCoupon" datasource="#Session.DBSourceEBM#">
				SELECT
					PromotionLastVersionId_int,
					UsedTimeByRecurring_int,
					RecurringTime_int
				FROM
					simplebilling.userpromotions
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				AND
					PromotionStatus_int = 1
			</cfquery>	

			<cfif getUserCoupon.RECORDCOUNT GT 0>
				<cfloop query="getUserCoupon">
					<cfif getUserCoupon.RecurringTime_int EQ -1>
						<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
							<cfinvokeargument name="inpCouponId" value="#getUserCoupon.PromotionLastVersionId_int#"/>
							<cfinvokeargument name="isIndepentOnVersion" value="1"/>
						</cfinvoke>

						<cfif getCouponResult.RXRESULTCODE GT 0>
							<cfset temp = getCouponResult />
							<cfset arrayAppend(dataout['DATALIST'], temp)/>
						</cfif>
					<cfelseif (getUserCoupon.UsedTimeByRecurring_int GT getUserCoupon.RecurringTime_int AND getUserCoupon.RecurringTime_int NEQ -1) OR (getUserCoupon.RecurringTime_int EQ 0)>
						<cfquery name="disableCoupon" datasource="#Session.DBSourceEBM#">
							UPDATE
								simplebilling.userpromotions
							SET
								PromotionStatus_int = 0
							WHERE
								UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
							AND
								PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUserCoupon.PromotionLastVersionId_int#">
						</cfquery>
						<cfset dataout.RXRESULTCODE = -1/>
						<cfset dataout.MESSAGE = 'Coupon not available' />
					<cfelse>
						<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
							<cfinvokeargument name="inpCouponId" value="#getUserCoupon.PromotionLastVersionId_int#"/>
							<cfinvokeargument name="isIndepentOnVersion" value="1"/>
						</cfinvoke>
						<cfif getCouponResult.RXRESULTCODE GT 0>
							<cfset temp = getCouponResult />
							<cfset arrayAppend(dataout['DATALIST'], temp)/>
						</cfif>
					</cfif>
				</cfloop>

				<cfif arrayLen(dataout['DATALIST']) GT 0>
					<cfset dataout.MESSAGE = 'Get available coupon list successfully' />
					<cfset dataout.RXRESULTCODE = 1/>
				<cfelse>
					<cfset dataout.MESSAGE = 'Empty list' />
					<cfset dataout.RXRESULTCODE = 0/>
				</cfif>
				
			<cfelse>
				<cfset dataout.RXRESULTCODE = -3/>
				<cfset dataout.MESSAGE = 'No coupon found' />
				<cfreturn dataout />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = "#cfcatch.Message#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetPromoCodeList" access="remote" output="true" hint="Get Promotion code list">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<!--- <cfargument name="sEcho"> --->
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->

		<cfargument name="inpCouponCode" required="false">
		<cfargument name="inpStartDate" required="false">
		<cfargument name="inpEndDate" required="false">
		<!--- <cfargument name="isSearchFeature" required="false"> --->

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout["LISTPROMOTION"] = ArrayNew(1) />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var tempItem = {} />

		<cfset var getPromotion = '' />
		<cfset var discountType = '' />
		<cfset var recurringTime = '' />
		<cfset var getTotalPromotion = ''/>
		<cfset var promotionStatus = ''/>
		<cfset var discountValue = ''/>
		<cfset var percentageValue = ''/>
		<cfset var flatrateValue = ''/>
		<cfset var creditValue = ''/>
		<cfset var keywordValue = ''/>
		<cfset var mlpValue = ''/>
		<cfset var shortURLValue = ''/>
		<cfset var getCouponCreatedTime = ''/>

		<cftry>
			<cfif arguments.iDisplayStart < 0>
				<cfset arguments.iDisplayStart = 0>
			</cfif>

			<!--- <cfif arguments.isSearchFeature NEQ '' AND arguments.inpCouponCode EQ '' AND arguments.inpStartDate EQ '' AND arguments.inpEndDate EQ ''>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = "Please input data for filtering, at least one field is required!">
				<cfreturn dataout />
			</cfif> --->

			<cfquery name="getTotalPromotion" datasource="#Session.DBSourceREAD#">
				SELECT
					promotion_id
				FROM
					simplebilling.promotion_codes
				WHERE
					latest_version = 1
				AND promotion_status >= 0
				<cfif arguments.inpCouponCode NEQ ''>
				AND
					promotion_code LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpCouponCode#%">
				</cfif>

				<cfif arguments.inpStartDate NEQ ''>
				AND
					DATE(start_date) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpStartDate#">
				</cfif>

				<cfif arguments.inpEndDate NEQ ''>
				AND
					DATE(expiration_date) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEndDate#">
				</cfif>
				
			</cfquery>

			<cfset dataout["iTotalRecords"] = (getTotalPromotion.RECORDCOUNT GTE 1 ? getTotalPromotion.RECORDCOUNT : 0) >
			<cfset dataout["iTotalDisplayRecords"] = (getTotalPromotion.RECORDCOUNT GTE 1 ? getTotalPromotion.RECORDCOUNT : 0) >

			<cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
				SELECT
					promotion_id,
					origin_id,
					promotion_code,
					promotion_name,
					promotion_status,
					start_date,
					expiration_date,
					discount_type_group,
					discount_plan_price_percent,
					discount_plan_price_flat_rate,
					promotion_keyword,
					promotion_credit,
					promotion_MLP,
					promotion_short_URL,
					last_updated_account,
					last_updated_time,
					recurring_time,
					used_time_by_recurring_int
				FROM
					simplebilling.promotion_codes
				WHERE
					latest_version = 1
    			AND promotion_status >= 0
				<cfif arguments.inpCouponCode NEQ ''>
				AND
					promotion_code LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpCouponCode#%">
				</cfif>

				<cfif arguments.inpStartDate NEQ ''>
				AND
					DATE(start_date) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpStartDate#">
				</cfif>

				<cfif arguments.inpEndDate NEQ ''>
				AND
					DATE(expiration_date) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEndDate#">
				</cfif>
				<!--- ORDER BY
					origin_id --->
				ORDER BY last_updated_time DESC
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
			</cfquery>

			<cfif getPromotion.RECORDCOUNT GT 0>
				<cfloop query="getPromotion">
					<cfif getPromotion.discount_plan_price_percent GT 0>
						<cfset percentageValue = 'Percentage: ' & '#getPromotion.discount_plan_price_percent#' & '<br>'/>
					</cfif>
					<cfif getPromotion.discount_plan_price_flat_rate GT 0>
						<cfset flatrateValue = 'Flat rate: ' & '#getPromotion.discount_plan_price_flat_rate#' />
					</cfif>
					<cfif getPromotion.promotion_credit GT 0>
						<cfset creditValue = 'Credit: ' & '#getPromotion.promotion_credit#' & '<br>' />
					</cfif>
					<cfif getPromotion.promotion_keyword GT 0>
						<cfset keywordValue = 'Keyword: ' & '#getPromotion.promotion_keyword#' & '<br>' />
					</cfif>
					<cfif getPromotion.promotion_MLP GT 0>
						<cfset mlpValue = 'MLP: ' & '#getPromotion.promotion_MLP#' & '<br>' />
					</cfif>
					<cfif getPromotion.promotion_short_URL GT 0>
						<cfset shortUrlValue = 'Short URL: ' & '#getPromotion.promotion_short_URL#' />
					</cfif>

					<cfif getPromotion.discount_type_group EQ 0 >
						<cfset discountType = "No Discount">
						<cfset discountValue = 'None' />
					<cfelseif getPromotion.discount_type_group EQ 1 >
						<cfset discountType = "Plan Price Discount">
						<cfset discountValue = '#percentageValue#' & '#flatrateValue#'/>
					<cfelse>
						<cfset discountType = "Other Discount">
						<cfset discountValue = '#creditValue#' & '#keywordValue#' & '#mlpValue#' & '#shortUrlValue#' />
					</cfif>

					<cfif getPromotion.recurring_time EQ 0>
						<cfset recurringTime = "Apply once">
					<cfelseif getPromotion.recurring_time EQ -1>
						<cfset recurringTime = "Apply on every renewals">
					<cfelse>
						<cfset recurringTime = "Fixed number of renewals:" & "<br>" & "#getPromotion.recurring_time#">
					</cfif>

					<cfif getPromotion.promotion_status EQ 1>
						<cfset promotionStatus = 'Active' />
					<cfelse>
						<cfset promotionStatus = 'Inactive' />
					</cfif>

					<cfquery name="getCouponCreatedTime" datasource="#Session.DBSourceREAD#">
						SELECT
							last_updated_time,
							used_time_by_recurring_int,
							used_time_by_upgrade_int
						FROM
							simplebilling.promotion_codes
						WHERE
							origin_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getPromotion.origin_id#">
					</cfquery>

					<cfset tempItem = 
						{
							ID = '#getPromotion.promotion_id#',
							ORIGINID = '#origin_id#',
							CODE = '#getPromotion.promotion_code#',
							NAME = '#getPromotion.promotion_name#', 
							STARTDATE = '#DateFormat(getPromotion.start_date, 'mm-dd-yyyy')#',
							ENDDATE = '#DateFormat(getPromotion.expiration_date, 'mm-dd-yyyy')#',
							DISCOUNTTYPE = '#discountType#',
							VALUETODISCOUNT = '#discountValue#',
							LASTUPDATEDBY = '#getPromotion.last_updated_account#' & '<br>' & '#DateFormat(getPromotion.last_updated_time, 'mm-dd-yyyy')#',
							RECURRING = '#recurringTime#',
							STATUS = '#promotionStatus#',
							REDEEMED = '#getCouponCreatedTime.used_time_by_recurring_int+getCouponCreatedTime.used_time_by_upgrade_int#',
							CREATED = '#DateFormat(getCouponCreatedTime.last_updated_time, 'mm-dd-yyyy')#'
						}
					>
					<cfset ArrayAppend(dataout["LISTPROMOTION"],tempItem)>
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Get Coupon list successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No coupon found' />
			</cfif>
			<cfcatch type="any">
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetNextPromotionId" access="private" output="true" hint="Get Next Promotion Id">
		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var getNextCouponId = '' />

		<cftry>
			<cfquery name="getNextCouponId" datasource="#Session.DBSourceREAD#">
				SELECT 
					promotion_id
				FROM
					simplebilling.promotion_codes
				ORDER BY
					promotion_id
				DESC
				LIMIT
					1
			</cfquery>
			<cfif getNextCouponId.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.NEXTCOUPONID = getNextCouponId.promotion_id+1/>
				<cfset dataout.MESSAGE = "Get next coupon id successfully!"/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.NEXTCOUPONID = 1/>
				<cfset dataout.MESSAGE = "Get next coupon id successfully!"/>
			</cfif>
			<cfcatch>
                <cfset dataout.TYPE = "#cfcatch.Type#" />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="AddNewPromoCode" access="remote" output="true" hint="Add new coupon">
		<cfargument name="inpCouponName" type="string" required="true">
		<cfargument name="inpCouponCode" type="string" required="true">
		<cfargument name="inpCouponDescription" type="string" required="true">
		<cfargument name="inpCouponStatus" type="numeric" required="false" default="0">
		<cfargument name="inpCouponDiscountType" type="numeric" required="false" default="0">
		<cfargument name="inpDiscountPricePercent" required="false">
		<cfargument name="inpDiscountPriceFlatRate" required="false">
		<cfargument name="inpCouponCredit" required="false">
		<cfargument name="inpCouponKeyword" required="false">
		<cfargument name="inpCouponMLP" required="false">
		<cfargument name="inpCouponShortUrl" required="false">
		<cfargument name="inpCouponStartDate" required="false" default="">
		<cfargument name="inpCouponEndDate" required="false" default="">
		<cfargument name="inpRecurringTime" type="numeric" required="false" default="0">
		<cfargument name="inpLatestVersion" type="numeric" required="false" default="1">
		<cfargument name="inpCouponOriginId" required="false" default="">
		<cfargument name="inpMaxredemption" required="false" default="0">
		<cfargument name="inpRedemtionCount" required="false">
		<cfargument name="inpLimitCb1" required="false" default="0">
		<cfargument name="inpLimitCb2" required="false" default="0">
		<cfargument name="inpInvitationOnly" required="false" default="0">
		<cfargument name="inpInvitationList" required="false">
		<cfargument name="isEditCoupon" required="false" default="0" hint="If this function is used to edit coupon, don't need to check duplicate coupon code">


		<cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var addNewCoupon = '' />
        <cfset var addNewCouponResult = '' />
        <cfset var getNextCouponIdResult = '' />
        <cfset var updateLatestVersion = '' />
        <cfset var updateLatestVersionResult = '' />
        <cfset var updatedBy = '' />
        <cfset var getUserResult = '' />
        <cfset var checkDuplicatedCoupon = '' />

        <cftry>

        	<cfif arguments.inpCouponDiscountType EQ 1 AND arguments.inpDiscountPricePercent EQ '' AND arguments.inpDiscountPriceFlatRate EQ ''>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please select Plan Price Discount details: Percentage or Flat rate">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpCouponDiscountType EQ 2 AND arguments.inpCouponCredit EQ '' AND arguments.inpCouponKeyword EQ '' AND arguments.inpCouponMLP EQ '' AND arguments.inpCouponShortUrl EQ ''>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please select Other Discount details: Credit, Keyword, MLP and Short URL">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpDiscountPricePercent NEQ '' AND arguments.inpDiscountPricePercent LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for Percentage!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpDiscountPriceFlatRate NEQ '' AND arguments.inpDiscountPriceFlatRate LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for Flat Rate!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpCouponCredit NEQ '' AND arguments.inpCouponCredit LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for Credit!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpCouponKeyword NEQ '' AND arguments.inpCouponKeyword LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for Keyword!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpCouponMLP NEQ '' AND arguments.inpCouponMLP LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for MLP!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpCouponShortUrl NEQ '' AND arguments.inpCouponShortUrl LTE 0>
        		<cfset dataout.RXRESULTCODE = 0>
        		<cfset dataout.MESSAGE = "Please input Positive value for Short URL!">
        		<cfreturn dataout />
        	</cfif>

        	<cfif arguments.inpDiscountPricePercent EQ ''>
        		<cfset arguments.inpDiscountPricePercent = 0/>
        	</cfif>

        	<cfif arguments.inpDiscountPriceFlatRate EQ ''>
        		<cfset arguments.inpDiscountPriceFlatRate = 0/>
        	</cfif>

        	<cfif arguments.inpCouponCredit EQ ''>
        		<cfset arguments.inpCouponCredit = 0/>
        	</cfif>

        	<cfif arguments.inpCouponKeyword EQ ''>
        		<cfset arguments.inpCouponKeyword = 0/>
        	</cfif>

        	<cfif arguments.inpCouponMLP EQ ''>
        		<cfset arguments.inpCouponMLP = 0/>
        	</cfif>

        	<cfif arguments.inpCouponShortUrl EQ ''>
        		<cfset arguments.inpCouponShortUrl = 0/>
        	</cfif>

        	<cfinvoke method="GetNextPromotionId" returnvariable="getNextCouponIdResult"/>

        	<cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="getUserResult">
        		<cfinvokeargument name="inpUserId" value="#Session.USERID#">
        	</cfinvoke>

        	<cfset updatedBy = "#getUserResult.USERNAME#" & " #left(getUserResult.LASTNAME,1)#.">


        	<cfif arguments.isEditCoupon EQ 0>
	        	<cfquery name="checkDuplicatedCoupon" datasource="#Session.DBSourceREAD#">
	        		SELECT
	        			promotion_code
	        		FROM
	        			simplebilling.promotion_codes
	        		WHERE
	        			promotion_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCouponCode#">
	        		AND
	        			promotion_status >= 0
	        		AND
	        			latest_version = 1
	        	</cfquery>

	        	<cfif checkDuplicatedCoupon.RECORDCOUNT GT 0>
	        		<cfset dataout.RXRESULTCODE = -1 />
	        		<cfset dataout.MESSAGE = "Coupon code duplicated! Please input another code.">
	        		<cfreturn dataout />
	        	</cfif>
	        </cfif>

        	<cftransaction action="begin">
        		<cftry>
		        	<cfif arguments.inpCouponOriginId NEQ ''>
		        		<cfquery name="updateLatestVersion" datasource="#Session.DBSourceEBM#" result="updateLatestVersionResult">
		        			UPDATE
		        				simplebilling.promotion_codes
		        			SET 
		        				latest_version = 0
		        			WHERE
		        				origin_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponOriginId#">
		        			AND
		        				latest_version = 1
		        		</cfquery>

		        		<cfif updateLatestVersionResult.RECORDCOUNT LT 1>
		        			<cftransaction action="rollback" />
		        			<cfset dataout.RXRESULTCODE = 0 />
		        			<cfset dataout.MESSAGE = 'Update origin id failed!'/>
		        			<cfreturn dataout />
		        		</cfif>

		        	</cfif>

		        	<cfquery name="addNewCoupon" datasource="#Session.DBSourceEBM#" result="addNewCouponResult">
		        		INSERT INTO
		        			simplebilling.promotion_codes
		        			(
		        				origin_id,
		        				promotion_code,
		        				promotion_name,
		        				promotion_desc,
		        				promotion_status,
		        				discount_type_group,
		        				discount_plan_price_percent,
		        				discount_plan_price_flat_rate,
		        				promotion_credit,
		        				promotion_keyword,
		        				promotion_MLP,
		        				promotion_short_URL,
		        				start_date,
		        				expiration_date,
		        				recurring_time,
		        				created_by_id,
		        				created_by_account,
		        				last_updated_by_id,
		        				last_updated_account,
		        				latest_version,
		        				limit_cb1_ti,
		        				limit_cb2_ti,
		        				invitation_only_ti,
		        				invitation_email_list,
		        				max_redemption_int,
		        				redemption_count_int
		        			)
		        		VALUES
		        			(	
		        				<cfif arguments.inpCouponOriginId EQ ''>
		        					<cfqueryparam cfsqltype="cf_sql_integer" value="#getNextCouponIdResult.NEXTCOUPONID#">,
		        				<cfelse>
		        					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponOriginId#">,
		        				</cfif>
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCouponCode#">,
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCouponName#">,
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCouponDescription#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponStatus#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponDiscountType#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpDiscountPricePercent#">,
		        				<cfqueryparam cfsqltype="cf_sql_decimal" value="#arguments.inpDiscountPriceFlatRate#">,
		        				<cfqueryparam cfsqltype="cf_sql_decimal" value="#arguments.inpCouponCredit#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponKeyword#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponMLP#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponShortUrl#">,

		        				<cfif arguments.inpCouponStartDate NEQ ''>
		        					<cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpCouponStartDate#">,
		        				<cfelse>
		        					NOW(),
		        				</cfif>
		        				<cfqueryparam cfsqltype="cf_sql_date" value="#arguments.inpCouponEndDate#" null="#NOT len(trim(arguments.inpCouponEndDate))#">,	
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpRecurringTime#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#updatedBy#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#updatedBy#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpLatestVersion#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpLimitCb1#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpLimitCb2#">,
		        				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpInvitationOnly#">,
		        				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpInvitationList#">,
		        				<cfif arguments.inpMaxredemption NEQ ''>
		        					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpMaxredemption#">,
		        				<cfelse>
		        					0,
		        				</cfif>
		        				<cfif arguments.inpMaxredemption NEQ ''>
		        					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpMaxredemption#">
		        				<cfelse>
		        					0
		        				</cfif>
		        			)
		        	</cfquery>

		        	<cfif addNewCouponResult.RECORDCOUNT GT 0>
			        	<cfset dataout.RXRESULTCODE = 1>
			        	<cfif arguments.inpCouponOriginId EQ '' >
			        		<cfset dataout.MESSAGE = "Create new coupon successfully!">
			        	<cfelse>
			        		<cfset dataout.MESSAGE = "Edit coupon successfully!">
			        	</cfif>
			        <cfelse>
			        	<cftransaction action="rollback" />
			        	<cfset dataout.RXRESULTCODE = 0>
			        	<cfset dataout.MESSAGE = "Create new coupon failed!">
			        </cfif>

			        <cftransaction action="commit" />
                    <cfcatch>
                        <cftransaction action="rollback" />
                        <cfset dataout.RXRESULTCODE = -3>
                        <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>                    
                        <cfreturn dataout>
                    </cfcatch>
			    </cftry>
		    </cftransaction>    
        	<cfcatch>
                <cfset dataout.TYPE = "#cfcatch.Type#" />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
	</cffunction>

	<cffunction name="GetCouponDetails" access="remote" output="true" hint="Get coupon detail by promotion id">
		<cfargument name="inpCouponId" required="true" type="numeric">
		<cfargument name="isIndepentOnVersion" required="no" default="0">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />

		<cfset var getCouponDetails = '' />

		<cftry>
			<cfquery name="getCouponDetails" datasource="#Session.DBSourceREAD#">
				SELECT
					origin_id,
					promotion_id,
    				promotion_code,
    				promotion_name,
    				promotion_desc,
    				promotion_status,
    				discount_type_group,
    				discount_plan_price_percent,
    				discount_plan_price_flat_rate,
    				promotion_credit,
    				promotion_keyword,
    				promotion_MLP,
    				promotion_short_URL,
    				start_date,
    				expiration_date,
    				recurring_time,
    				limit_cb1_ti,
    				limit_cb2_ti,
    				invitation_only_ti,
    				invitation_email_list,
    				max_redemption_int,
    				used_time_by_recurring_int,
    				used_time_by_upgrade_int
    			FROM
    				simplebilling.promotion_codes
    			WHERE
    				promotion_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponId#">
    			<cfif arguments.isIndepentOnVersion EQ 0>
	    			AND 
	    				latest_version = 1
	    		</cfif>
			</cfquery>

			<cfif getCouponDetails.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get coupon detail successfully!"/>
				<cfset dataout.ORIGINID = getCouponDetails.origin_id/>
				<cfset dataout.PROMOTIONID = getCouponDetails.promotion_id/>
				<cfset dataout.COUPONCODE = getCouponDetails.promotion_code/>
				<cfset dataout.COUPONNAME = getCouponDetails.promotion_name />
				<cfset dataout.COUPONDESC = getCouponDetails.promotion_desc />
				<cfset dataout.COUPONSTATUS = getCouponDetails.promotion_status />
				<cfset dataout.COUPONDISCOUNTTYPE = getCouponDetails.discount_type_group />
				<cfset dataout.COUPONDISCOUNTPRICEPERCENT = getCouponDetails.discount_plan_price_percent />
				<cfset dataout.COUPONDISCOUNTPRICEFLATRATE = getCouponDetails.discount_plan_price_flat_rate />
				<cfset dataout.COUPONCREDIT = getCouponDetails.promotion_credit />
				<cfset dataout.COUPONKEYWORD = getCouponDetails.promotion_keyword />
				<cfset dataout.COUPONMLP = getCouponDetails.promotion_MLP />
				<cfset dataout.COUPONSHORTURL = getCouponDetails.promotion_short_URL />
				<cfset dataout.COUPONSTARTDATE = DateFormat(getCouponDetails.start_date, 'yyyy-mm-dd') />
				<cfset dataout.COUPONENDDATE = DateFormat(getCouponDetails.expiration_date, 'yyyy-mm-dd') />
				<cfset dataout.COUPONRECURRING = getCouponDetails.recurring_time />
				<cfset dataout.LIMITCB1 = getCouponDetails.limit_cb1_ti />
				<cfset dataout.LIMITCB2 = getCouponDetails.limit_cb2_ti />
				<cfset dataout.INVITATIONONLY = getCouponDetails.invitation_only_ti />
				<cfset dataout.INVITATIONLIST = getCouponDetails.invitation_email_list />
				<cfset dataout.COUPONREDEMPTION = getCouponDetails.max_redemption_int />
				<cfset dataout.USEDTIME = getCouponDetails.used_time_by_recurring_int + getCouponDetails.used_time_by_upgrade_int />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Invalid coupon!'/>
			</cfif>

			<cfcatch>
                <cfset dataout.TYPE = "#cfcatch.Type#" />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateUserPromoCodeUsedTime" access="remote" output="false" hint="Update used time">
		<cfargument name="inpUserId" type="numeric" required="yes">
		<cfargument name="inpUsedFor" required="true" hint="1 for recurring or sign up, 2 for upgrade">	
		<cfargument name="inpPromotionId" required="false" default="0" hint="">	
		<cfargument name="inpLimit" required="false" default="0" hint="">

		<cfset var updateUserPromoCode = {} />
		<cfset var updateUserPromoCodeResult = {} />
		<cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfif inpUserId GT 0>
        	<cftry>

				<cfquery name="updateUserPromoCode" datasource="#Session.DBSourceEBM#" result="updateUserPromoCodeResult">
					UPDATE simplebilling.userpromotions
					<cfif arguments.inpUsedFor EQ 1>
						SET 
							UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
					<cfelseif arguments.inpUsedFor EQ 2>
						SET 
							UsedTimeByUpgrade_int = UsedTimeByUpgrade_int + 1
					<cfelseif arguments.inpUsedFor EQ 3>
						SET 
							UsedTimeByUpgrade_int = UsedTimeByUpgrade_int + 1,
							UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
					</cfif>
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
					AND
						PromotionStatus_int = 1
					<cfif arguments.inpPromotionId GT 0>
						AND PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPromotionId#">
					</cfif>
					<cfif arguments.inpLimit GT 0>
						ORDER BY UserPromotionId_int DESC
						LIMIT 1
					</cfif>
				</cfquery>

				<cfif updateUserPromoCodeResult.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = 1 />	
				</cfif>
			<cfcatch>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>	
			</cftry>
		</cfif>
		
		<cfreturn dataout /> 
		
	</cffunction>

	<cffunction name="DeleteCoupon" access="remote" hint="Delete coupon code">
		<cfargument name="inpCouponId" required="true"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var deleteCouponCode = '' />
        <cftry>
        	<cfquery result="deleteCouponCode" datasource="#Session.DBSourceEBM#">
        		UPDATE
        			simplebilling.promotion_codes
        		SET
        			promotion_status = -1
        		WHERE
        			promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">
        	</cfquery>

        	<cfif deleteCouponCode.RECORDCOUNT EQ 1>
        		<cfset dataout.RXRESULTCODE = 1 />
        		<cfset dataout.MESSAGE = 'Delete coupon successfully!'/>
        	<cfelse>
        		<cfset dataout.RXRESULTCODE = 0 />
        		<cfset dataout.MESSAGE = 'Delete coupon failed!'/>
        	</cfif>
        	<cfcatch>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
        </cftry>

        <cfreturn dataout />
	</cffunction>

	<cffunction name="IncreaseCouponUsedTime" access="remote" hint="Increase Coupon Used Time">
		<cfargument name="inpCouponId" required="true"/>
		<cfargument name="inpUsedFor" required="true" hint="1 for recurring, 2 for upgrade plan"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var updateCouponUsedTime = "" />

        <cftry>
        	<cfquery result="updateCouponUsedTime" datasource="#Session.DBSourceEBM#">
        		UPDATE
        			simplebilling.promotion_codes
        		<cfif arguments.inpUsedFor EQ 1>
	        		SET
	        			used_time_by_recurring_int = used_time_by_recurring_int + 1
	        	<cfelseif arguments.inpUsedFor EQ 2>
	        		SET
	        			used_time_by_upgrade_int = used_time_by_upgrade_int + 1
	        	</cfif>
        		WHERE
        			origin_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">
        		AND
        			promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">
        	</cfquery>

        	<cfif updateCouponUsedTime.RECORDCOUNT GT 0>
        		<cfset dataout.RXRESULTCODE = 1 />
        		<cfset dataout.MESSAGE = 'Increase coupon used time successfully!'/>
        	<cfelse>
        		<cfset dataout.RXRESULTCODE = 0 />
        		<cfset dataout.MESSAGE = 'Increase coupon used time failed!'/>
        	</cfif>
        	<cfcatch>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
        </cftry>

        <cfreturn dataout />
	</cffunction>

	<!--- Insert Promotion CodeUsed Log --->
	<cffunction name="InsertPromotionCodeUsedLog" access="remote" output="false" hint="Insert Promotion Code Used Log">
		
		<cfargument name="inpCouponId" type="numeric" required="yes">
		<cfargument name="inpOriginCouponId" type="numeric" required="yes">
		<cfargument name="inpUserId" type="numeric" required="yes">
		<cfargument name="inpUsedFor" type="string" required="yes">

		<cfset var insertUsedLogPromoCode = {} />
		<cfset var insertUsedLogPromoCodeResult = {} />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfquery name="insertUsedLogPromoCode" datasource="#Session.DBSourceEBM#" result="insertUsedLogPromoCodeResult">
		        INSERT INTO
		            simplebilling.log_used_promotion_codes
		            (
		            promotion_id_int,
		            promotion_origin_id_int,
		            UserId_int,
		            UsedFor_vch
		            )
		        VALUES 
		            (
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.promotion_origin_id_int#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUsedFor#">
		            )                                        
		    </cfquery> 

			<cfif insertUsedLogPromoCodeResult.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Insert promotion code used log successfully" />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Insert promotion code used log failed" />
			</cfif>
			<cfcatch>
			      <cfset dataout.RXRESULTCODE = -2 />
			      <cfset dataout.MESSAGE = "#cfcatch.Message#" />
			      <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch> 
		</cftry>
	  
	  <cfreturn dataout />
	</cffunction>

	<cffunction name="CheckCouponForUpgradePlan" access="remote" hint="Check if a coupon is available for upgrading plan">
		<cfargument name="inpCouponCode" required="true"/>
		<cfargument name="inpUserId" required="false" default="0"/>

		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var getUserCoupon = ''/>
		<cfset var getCouponResult = ''/>
		<cfset var checkAvailableCoupon = ''/>
		<cfset var allAvailableCouponCode = [] />
		<cfset var index = ''/>
		<cfset var couponId = '' />
		<cfset var getActivePromoCodeResult = ''/> 
		<cfset var userId = ''/> 


		<cftry>

			<cfset userId = arguments.inpUserId EQ 0 ? SESSION.USERID : arguments.inpUserId/>

			<cfinvoke method="getActivePromoCode" returnvariable="getActivePromoCodeResult">
				<cfinvokeargument name="inpCode" value="#arguments.inpCouponCode#"/>
				<cfinvokeargument name="inpEmail" value="#SESSION.EMAILADDRESS#"/>
			</cfinvoke>
			
			<cfif getActivePromoCodeResult.RXRESULTCODE EQ 1>
				<cfset couponId = getActivePromoCodeResult.promotion_id/>
				<cfset dataout = getActivePromoCodeResult/>
			<cfelse>
				<cfset dataout = getActivePromoCodeResult/>
				<cfreturn dataout />
			</cfif>

			<cfquery name="getUserCoupon" datasource="#Session.DBSourceEBM#">
				SELECT
					PromotionLastVersionId_int,
					RecurringTime_int,
					UsedTimeByRecurring_int
				FROM
					simplebilling.userpromotions
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userId#">
				AND
					PromotionStatus_int = 1
				AND
					PromotionLastVersionId_int > 0
				AND
					PromotionId_int > 0
			</cfquery>

			<cfif getUserCoupon.RECORDCOUNT GT 0>

				<!--- <cfinvoke method="CheckAvailableCouponForUser1" returnvariable="checkAvailableCoupon">
					<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
				</cfinvoke> --->

				<cfloop query="getUserCoupon">

					<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
						<cfinvokeargument name="inpCouponId" value="#couponId#"/>
						<cfinvokeargument name="isIndepentOnVersion" value="1"/>
					</cfinvoke>

					<cfif getCouponResult.LIMITCB1 EQ 1 AND getUserCoupon.PromotionLastVersionId_int NEQ couponId AND (getUserCoupon.UsedTimeByRecurring_int LTE getUserCoupon.RecurringTime_int OR getUserCoupon.RecurringTime_int EQ -1)>
						<cfset dataout.RXRESULTCODE = -4/>
						<cfset dataout.MESSAGE = 'Promo Code: #arguments.inpCouponCode#. This coupon can not be combined with other offers!'/>
						<cfreturn dataout/>
					</cfif>

					<cfif getUserCoupon.PromotionLastVersionId_int EQ couponId AND (getUserCoupon.UsedTimeByRecurring_int LTE getUserCoupon.RecurringTime_int OR getUserCoupon.RecurringTime_int EQ -1)>
						
						<cfif getCouponResult.LIMITCB2 EQ 1>
							<cfset dataout.RXRESULTCODE = -3/>
							<cfset dataout.MESSAGE = 'Promo Code: #arguments.inpCouponCode#. This coupon can not be used by the same individual more than once, you used it before!'/>
							<cfreturn dataout/>
						</cfif>

						<!--- <cfif checkAvailableCoupon.RXRESULTCODE GT 0>
							<cfloop array="#checkAvailableCoupon['DATALIST']#" index="index">
								<cfset arrayAppend(allAvailableCouponCode, index.PROMOTIONID)/>
							</cfloop>

							<cfif !arrayContains(allAvailableCouponCode, couponId)>
								<cfset dataout.RXRESULTCODE = -5/>
								<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCouponCode#. We're sorry, the promotional code you entered is expired or inactive, please try again."/>
								<cfreturn dataout/>
							</cfif>
						<cfelse>
							<cfset dataout.RXRESULTCODE = -6/>
							<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCouponCode#. We're sorry, the promotional code you entered is expired or inactive, please try again."/>
							<cfreturn dataout/>
						</cfif> --->
					<cfelse>
						<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
							<cfinvokeargument name="inpCouponId" value="#getUserCoupon.PromotionLastVersionId_int#"/>
							<cfinvokeargument name="isIndepentOnVersion" value="1"/>
						</cfinvoke>

						<cfif getCouponResult.LIMITCB1 EQ 1 AND getUserCoupon.UsedTimeByRecurring_int LTE getUserCoupon.RecurringTime_int>
							<cfset dataout.RXRESULTCODE = -7/>
							<cfset dataout.MESSAGE = 'You already have had a coupon that can not be combined with other offers!'/>
							<cfreturn dataout/>
						</cfif>
					</cfif>
				</cfloop>
			</cfif>

			<cfcatch>
			      <cfset dataout.RXRESULTCODE = -2 />
			      <cfset dataout.MESSAGE = "#cfcatch.Message#" />
			      <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetCouponReportById" access="remote" hint="Get coupon report by id">
		<cfargument name="inpCouponId" required="true"/>
		<cfargument name="inpStartDate" required="false"/>
		<cfargument name="inpEndDate" required="false"/>

		<cfargument name="inpSkipLimit" required="false" default="0"/>

		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.SIGNUPCOUNT = 0 />
		<cfset dataout.UPGRADECOUNT = 0 />
		<cfset dataout.RECURRINGCOUNT = 0 />
		<cfset dataout["DATALIST"] = ArrayNew(1) />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var tempItem = {} />

		<cfset var getTotalLogById = '' />
		<cfset var getReport = '' />

		<cfset var orderField = '' />
		<cfset var getCouponResult = '' />

		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
		        <cfset orderField = 'ua.FirstName_vch' />
		    </cfcase>
			<cfcase value="4">
		        <cfset orderField = 'lp.UsedFor_vch' />
		    </cfcase>
		</cfswitch>

		<cftry>
			<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
				<cfinvokeargument name="inpCouponId" value="#arguments.inpCouponId#">
			</cfinvoke>

			<cfif getCouponResult.RXRESULTCODE LTE 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No record found' />
				<cfreturn dataout />
			</cfif>

			<cfquery name="getTotalLogById" datasource="#Session.DBSourceEBM#">
				SELECT
					id_int,
					UsedFor_vch
				FROM
					simplebilling.log_used_promotion_codes
				WHERE
					UserId_int > 0
				AND
					promotion_origin_id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCouponResult.ORIGINID#">
				<cfif arguments.inpStartDate NEQ ''>
				AND
					DATE(Created_dt) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpStartDate#">
				</cfif>

				<cfif arguments.inpEndDate NEQ ''>
				AND
					DATE(Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEndDate#">
				</cfif>
			</cfquery>

			<cfloop query="getTotalLogById">
				<cfif getTotalLogById.UsedFor_vch EQ 'Sign up and use coupon'>
					<cfset dataout.SIGNUPCOUNT = dataout.SIGNUPCOUNT + 1/>
				<cfelseif getTotalLogById.UsedFor_vch EQ 'Recurring'>
					<cfset dataout.RECURRINGCOUNT = dataout.RECURRINGCOUNT + 1/>
				<cfelse>
					<cfset dataout.UPGRADECOUNT = dataout.UPGRADECOUNT + 1/>
				</cfif>
			</cfloop>

			<cfset dataout["iTotalRecords"] = (getTotalLogById.RECORDCOUNT GTE 1 ? getTotalLogById.RECORDCOUNT : 0) >
			<cfset dataout["iTotalDisplayRecords"] = (getTotalLogById.RECORDCOUNT GTE 1 ? getTotalLogById.RECORDCOUNT : 0) >

			<cfquery name="getReport" datasource="#Session.DBSourceEBM#">
				SELECT 
					ua.FirstName_vch,
					ua.LastName_vch,
					ua.MFAContactString_vch,
					ua.EmailAddress_vch,
					lp.Created_dt,
					lp.UsedFor_vch,
					lp.promotion_id_int
				FROM
					simplebilling.log_used_promotion_codes as lp
				INNER JOIN
					simpleobjects.useraccount as ua
				ON 
					ua.UserId_int = lp.UserId_int
				AND
					lp.promotion_origin_id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCouponResult.ORIGINID#">
				AND
					lp.UserId_int > 0
				<cfif arguments.inpStartDate NEQ ''>
				AND
					DATE(lp.Created_dt) >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpStartDate#">
				</cfif>

				<cfif arguments.inpEndDate NEQ ''>
				AND
					DATE(lp.Created_dt) <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEndDate#">
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						lp.Created_dt DESC
				</cfif>

				<cfif arguments.inpSkipLimit EQ 0>
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfif>
			</cfquery>

			<cfif getReport.RECORDCOUNT GT 0>

				<cfloop query="getReport">
					<cfif getReport.UsedFor_vch EQ 'Sign up and use coupon'>
						<cfset getReport.UsedFor_vch = 'Sign up' />
					<cfelseif getReport.UsedFor_vch EQ 'Upgrade plan'>
						<cfset getReport.UsedFor_vch = 'Upgrade'>
					</cfif>

					<cfset tempItem = {
							ID = '#getReport.promotion_id_int#',
							NAME = '#getReport.FirstName_vch# ' & '#getReport.LastName_vch#',
							PHONE = '#getReport.MFAContactString_vch#',
							EMAIL = '#getReport.EmailAddress_vch#',
							REDEEMED = '#DateFormat(getReport.Created_dt, 'mmm dd, yyyy')# ' & '#TimeFormat(getReport.Created_dt, "hh:mm")#',
							TYPE = '#getReport.UsedFor_vch#'
						} 
					/>

					<cfset ArrayAppend(dataout["DATALIST"],tempItem)>
				</cfloop>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Get report successfully!' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No record found' />
			</cfif>

			<cfcatch type="any">
			      <cfset dataout.RXRESULTCODE = -2 />
			      <cfset dataout.MESSAGE = "#cfcatch.Message#" />
			      <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

</cfcomponent>