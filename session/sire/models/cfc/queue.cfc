<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/session/cfc/csc/constants.cfm">
	
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
    <cfset LOCAL_LOCALE = "English (US)">

    <cffunction name="saveQueue" access="remote" output="true" hint="save to queue">
    	<cfargument name="campaignId" type="number" required="yes">
    	<cfargument name="subcriberListId" type="number" required="yes">
    	<cfargument name="numberOfSubcriber" type="number" required="yes">

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CAMPAIGN = "0" />
		<cfset dataout.SUBCRIBERLISTID = "0" />
		<cfset dataout.NUMBEROFSUBCRIBER = "0" />
		<cfset dataout.QUEUEID = "" />
		<cfset dataout.QUEUEUUID = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset var CreateUUID = '' />
	   	<cfset var insertQueueQuery = '' />
	   	<cfset var insertResult = '' />

    	<cfif campaignId GT 0 AND SubcriberListId GT 0 AND NumberOfSubcriber GT 0>
    		<cfset CreateUUID = CreateUUID()/>
	    	<cftry>
    			<cfquery name="insertQueueQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
					INSERT INTO simplequeue.batch_load_queue (
						ProcessKeyId_int,
                        UserId_int,
                        CampaignId_int, 
                        SubcriberListId_int, 
                        NumberOfSubcriber_int, 
						Created_dt
					) VALUE (
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CreateUUID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.campaignId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.subcriberListId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.numberOfSubcriber#">,
						NOW()
					)
				</cfquery>
				<cfif insertResult.GENERATED_KEY GT 0>
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.QUEUEID = insertResult.GENERATED_KEY  />
						<cfset dataout.QUEUEUUID = CreateUUID  />
						<cfset dataout.MESSAGE = "Save to queue success." />
					<cfelse>
						<cfset dataout.ERRMESSAGE = "Save to queue fail." />
	   					<cfset dataout.MESSAGE = "Save to queue fail." />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "Save to queue fail." />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>

    </cffunction>	

    <!--- <cffunction name="updateQueueStatus" access="remote" output="true" hint="update queue status">
    	<cfargument name="inpUUID" type="string" required="yes">
        <cfargument name="inpQueueID" type="number" required="yes">
        <cfargument name="inpStatus" type="number" required="yes" default="0">
        <cfargument name="inpNote" TYPE="string" required="true" />

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CAMPAIGN = "0" />
		<cfset dataout.SUBCRIBERLISTID = "0" />
		<cfset dataout.NUMBEROFSUBCRIBER = "0" />
		<cfset dataout.QUEUEID = "" />
		<cfset dataout.QUEUEUUID = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset var updateQueueQuery = '' />
	   	<cfset var result = '' />

    	<cfif inpUUID NEQ '' OR inpQueueID GT 0 >
    		
	    	<cftry>
    			<cfquery name="updateQueueQuery" datasource="#Session.DBSourceEBM#" result="result">
					UPDATE simplequeue.batch_load_queue 
					SET 
						Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
						Update_dt = NOW(),
						Note_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNote#">
					WHERE	
						<!---UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND	--->
					 <cfif inpUUID NEQ ''>
                        ProcessKeyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUUID#">
                    <cfelseif inpCPPID GT 0>
                        QueueId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQueueID#">
                    </cfif>
				</cfquery>
				<cfif result.RecordCount GT 0>
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.QUEUEID = inpQueueID/>
						<cfset dataout.QUEUEUUID = inpUUID/>
						<cfset dataout.MESSAGE = "Update queue success." />
					<cfelse>
						<cfset dataout.ERRMESSAGE = "Update queue fail." />
	   					<cfset dataout.MESSAGE = "Update queue fail." />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "Update queue fail." />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>

    </cffunction> --->

	<cffunction name="updateQueueStatus" access="remote" output="true" hint="update queue status">
    	<cfargument name="inpPKID" type="string" required="yes">
        <cfargument name="inpStatus" type="number" required="yes" default="0">
        <cfargument name="inpNote" TYPE="string" required="true" />

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CAMPAIGN = "0" />
		<cfset dataout.SUBCRIBERLISTID = "0" />
		<cfset dataout.NUMBEROFSUBCRIBER = "0" />
		<cfset dataout.QUEUEID = "" />
		<cfset dataout.QUEUEUUID = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset var updateQueueQuery = '' />
	   	<cfset var result = '' />

    	<cftry>
			<cfquery name="updateQueueQuery" datasource="#Session.DBSourceEBM#" result="result">
				UPDATE simpleobjects.batch_blast_log 
				SET 
					Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
					Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNote#">
				WHERE	
                    PKId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPKID#">
                AND
                	Status_int = 2 -- Processing
			</cfquery>
			<cfif result.RecordCount GT 0>
				<cfquery name="updateQueueQuery" datasource="#Session.DBSourceEBM#" result="result">
					UPDATE
						simplequeue.batch_load_queue
					SET
						Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						Note_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="User stopped">
					WHERE
	                    BlastLogId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPKID#">
	                AND
	                	Status_ti = 1
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.QUEUEUUID = inpPKID/>
				<cfset dataout.MESSAGE = "Update queue success." />
			<cfelse>
				<cfset dataout.ERRMESSAGE = "Update queue fail." />
				<cfset dataout.MESSAGE = "This process has errors. Please try to check your billing information and resend blast." />
			</cfif>	
		<cfcatch>
			<cfset dataout.ERRMESSAGE = "Update queue fail." />
   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		</cfcatch>
    	</cftry>

    	<cfreturn dataout>

    </cffunction>

<!---     <cffunction name="checkCampaignQueueStatus" access="remote" output="true" hint="update queue status">
    	<cfargument name="inpCampaignId" type="string" required="yes">
       
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CAMPAIGN = "0" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset var selectQueueQuery = '' />
	   	<cfset var result = '' />

    	<cfif inpCampaignId GT 0 >
    		
	    	<cftry>
    			<cfquery name="selectQueueQuery" datasource="#Session.DBSourceREAD#" result="result">
					SELECT Id_int
					FROM simplequeue.batch_load_queue
					WHERE	
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND	
                        CampaignId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#">
                    AND
                    	Status_ti = 1    
				</cfquery>
				<cfif result.RecordCount GT 0 >
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = "Have queue process." />
					<cfelse>
						<cfset dataout.ERRMESSAGE = "Do not have queue process." />
	   					<cfset dataout.MESSAGE = "Do not have queue process." />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "Do not have queue success." />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>

    </cffunction>	 --->

    <cffunction name="checkCampaignQueueStatus" access="public" output="true" hint="check queue status">
    	<cfargument name="inpCampaignId" type="string" required="yes">
    	<cfargument name="inpQueueStatus" type="number" required="yes">
       
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CAMPAIGN = "0" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset var selectQueueQuery = "" />
	   	<cfset var result = "" />

    	<cfif inpCampaignId GT 0 >
    		
	    	<cftry>
    			<cfquery name="selectQueueQuery" datasource="#Session.DBSourceREAD#" result="result">
					SELECT Id_int
					FROM simplequeue.batch_load_queue
					WHERE	
					<!---
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND	
					--->
                        CampaignId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignId#">
                    AND
                    	Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQueueStatus#">    
				</cfquery>
				<cfif result.RecordCount GT 0 >
						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = "Have queue process." />
				<cfelse>
						<cfset dataout.RXRESULTCODE = 2 />
						<cfset dataout.ERRMESSAGE = "Do not have queue process." />
	   					<cfset dataout.MESSAGE = "Do not have queue process." />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "Do not have queue success." />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>
    </cffunction>

    <!--- <cffunction name="GetQueueProcessInfo" access="remote" output="true" hint="stop queue status">
    	<cfargument name="inpUUID" type="string" required="yes">

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.CAMPAIGN = "0" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.PERCENT = "0" />
	   	<cfset dataout.REMAINTIMES = "1" />
	   	<cfset dataout.RUNTIMES = "0" />

	   	<cfset var diff = ''>
	   	<cfset var selectQueueQuery = ''>
	   	<cfset var result = ''>

    	<cfif arguments.inpUUID NEQ '' >
    		
	    	<cftry>
    			<cfquery name="selectQueueQuery" datasource="#Session.DBSourceREAD#" result="result">
					SELECT Status_ti,Created_dt,NumberOfSubcriber_int,NumberOfSubcriberInQueue_int,ActualNumberOfSubcriber_int
					FROM simplequeue.batch_load_queue
					WHERE	
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND	
                        ProcessKeyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUUID#">
                    /*AND
                    	Status_ti IN (1,2)*/
                    LIMIT 1	
				</cfquery>
			
				<cfif result.RecordCount GT 0 >
					<cfloop query="selectQueueQuery">
						<cfif Status_ti EQ 1 OR Status_ti EQ 2>
							<cfset dataout.PERCENT = round(100*NumberOfSubcriberInQueue_int/(iIf(ActualNumberOfSubcriber_int GT 0, DE(ActualNumberOfSubcriber_int), DE(NumberOfSubcriber_int))))>
							<cfset diff = DateDiff("n",ParseDateTime(Created_dt),now())>
							<cfset var percent = 1>
							<cfif dataout.PERCENT GT 0>
								<cfset var percent = dataout.PERCENT>
							</cfif>
							<cfset dataout.REMAINTIMES = round(100/percent * diff) - diff />	
							<cfset dataout.RUNTIMES = diff />

							<cfset dataout.RXRESULTCODE = 1 />
							<cfset dataout.MESSAGE = "" />
						<cfelse>
							<cfset dataout.RXRESULTCODE = 0 />
							<cfset dataout.ERRMESSAGE = "This process can not complete have errors" />
			   				<cfset dataout.MESSAGE = "This process can not complete have errors" />			
						</cfif>
					</cfloop>
				<cfelse>
					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.ERRMESSAGE = "This process does not exits or have errors" />
	   				<cfset dataout.MESSAGE = "This process does not exits or have errors" />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "There is an errors" />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>

    </cffunction> --->

    <cffunction name="GetQueueProcessInfo" access="remote" output="true" hint="stop queue status">
    	<cfargument name="inpPKID" type="string" required="yes">

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.CAMPAIGN = "0" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.PERCENT = "0" />
	   	<cfset dataout.REMAINTIMES = "1" />
	   	<cfset dataout.RUNTIMES = "0" />

	   	<cfset var diff = ''>
	   	<cfset var selectQueueQuery = ''>
	   	<cfset var result = ''>

    	<cfif arguments.inpPKID NEQ '' >
    		
	    	<cftry>
    			<cfquery name="selectQueueQuery" datasource="#Session.DBSourceREAD#" result="result">
					SELECT AmountLoaded_int, ActualLoaded_int, Created_dt
					FROM simpleobjects.batch_blast_log
					WHERE	
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND	
                        PKId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPKID#">
                    AND
                    	Status_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_LOADING#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PROCESSING#">)
                    LIMIT 1	
				</cfquery>

				<cfif result.RecordCount GT 0 >
					<cfloop query="selectQueueQuery">
						<cfset dataout.PERCENT = round(100*ActualLoaded_int/(AmountLoaded_int))>
						<cfset diff = DateDiff("s",ParseDateTime(Created_dt),now())>
						<cfset var percent = 1>
						<cfif dataout.PERCENT GT 0>
							<cfset var percent = dataout.PERCENT>
						</cfif>
						<cfset dataout.REMAINTIMES = round(100/percent * diff) - diff />	
						<cfset dataout.RUNTIMES = diff />

						<cfset dataout.RXRESULTCODE = 1 />
						<cfset dataout.MESSAGE = "" />
					</cfloop>
				<cfelse>
					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.ERRMESSAGE = "This process has errors" />
	   				<cfset dataout.MESSAGE = "Process has completed!" />	
				</cfif>	
			<cfcatch>
				<cfset dataout.ERRMESSAGE = "There is an errors" />
	   			<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
    		</cfcatch>
	    	</cftry>
	    <cfelse>
	    	<cfset dataout.ERRMESSAGE = "Invalid input value" />
	   		<cfset dataout.MESSAGE = "Invalid input value" />
    	</cfif>

    	<cfreturn dataout>

    </cffunction>

</cfcomponent>    