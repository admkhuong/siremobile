<cfcomponent>

    <cfinclude template="/public/paths.cfm" >
    <cfinclude template="/public/sire/configs/paths.cfm" >
    <cfinclude template="/session/sire/configs/paths.cfm" >

    <cffunction name="GetUserReferInfo" access="public" output="false" hint="Get user refer info: number of friend and promo credit">
        <cfargument name="inpUserId" required="false" default="#Session.UserId#">

        <cfset var dataout = {} />
        <cfset var GetUserReferInfo = "" />

        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.Type = "" />
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.FriendsReferred = 0 />
        <cfset dataout.PromoCreditsReceived = 0 />

        <cftry>
            <cfquery name="GetUserReferInfo" datasource="#Session.DBSourceEBM#">
                SELECT
                    Total_referral_int,
                    Total_promotion_credits_received_last_month_int
                FROM
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                LIMIT
                    1
            </cfquery>

            <cfif GetUserReferInfo.RECORDCOUNT GT 0>
                <cfset dataout.FriendsReferred = GetUserReferInfo.Total_referral_int />
                <cfset dataout.PromoCreditsReceived = GetUserReferInfo.Total_promotion_credits_received_last_month_int />
            </cfif>

            <cfset dataout.RXResultCode = 1 />

            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GenReferralCcde" access="public" output="true" hint="Gennerate an random string for Referral Code">
        <cfset var result = ''/>
        <cfset var i = '' />
        <!--- Create string --->
        <cfloop index="i" from="1" to="9">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(65, 90))>
        </cfloop>
        <!--- Encrtypt --->
        <cfset result = toBase64(result&'_'&Session.UserId)>
        
            
        <cfreturn result />
    </cffunction>

    <cffunction name="DecryptReferralCode" access="public" output="true" hint="Decrypt Referral Code to string">
        <cfargument name="inpReferralCode" required="true" Type="string">

        <cfset var dataout = {}/>
        <cfset var arrData = ''/>
        <cfset var checkRfCode = ''/>
        <cfset var checkRfCodeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.ReferralCode = "0" />
        <cfset dataout.UserId = "0" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cfif len(arguments.inpReferralCode) NEQ 0>
            <cftry>
                <cfset dataout.ReferralCode = ToString( ToBinary( arguments.inpReferralCode ) ) />
                <cfset arrData = ListToArray (dataout.ReferralCode,'_')>
                <cfif arrayLen(arrData) EQ 2 AND arrData[2] GT 0>
                    <!--- CHECK IN DB --->
                    <cfquery name="checkRfCode" datasource="#Session.DBSourceEBM#" result="checkRfCodeResult">
                        SELECT
                            ID_int
                        FROM 
                            simpleobjects.sire_user_referral
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arrData[2]#">
                        AND     
                            Referral_code_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpReferralCode#">
                        AND 
                            Status_int = 1    
                    </cfquery>

                    <cfif checkRfCodeResult.recordCount GT 0>
                        <cfset dataout.UserId = arrData[2] />
                        <cfset dataout.RXResultCode = "1" />
                    <cfelse>
                        <cfset dataout.Message = "Invalid Referral Code" />        
                    </cfif>
                <cfelse>
                    <cfset dataout.Message = "Invalid Referral Code" />    
                </cfif>
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry>
        <cfelse>
            <cfset dataout.Message = "Invalid Referral Code" />
        </cfif>  
        <cfreturn dataout />
    </cffunction>

    <cffunction name="InsertMonthFree" access="public" output="false" hint="Insert month free to sign up and invite user">
        <cfargument name="inpReferralCode" required="true" Type="string">
        <cfargument name="inpUserId" required="true" Type="numeric">

        <cfset var dataout = {}/>
        <cfset var referralCode = ''/>
        <cfset var insertUsersReferral =''/>
        <cfset var insertUsersReferralResult =''/>

        <cfset var updateUsersReferral =''/>
        <cfset var updateUsersReferralResult =''/>
        <cfset var resultReferralCode = structNew() />

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.SenderUserId = 0 />

        <cfif len(arguments.inpReferralCode) NEQ 0>
            <cftry>
                <!--- Decode RF CODE --->
                <cfinvoke method="DecryptReferralCode" returnvariable='resultReferralCode'>
                    <cfinvokeargument  name="inpReferralCode" VALUE="#arguments.inpReferralCode#">
                </cfinvoke>

                <!--- CHECK IF ReferralCode exits in DB --->
                <cfif resultReferralCode.RXResultCode EQ 1>
                    <cfset referralCode = resultReferralCode.ReferralCode/>

                    <!--- ADD MORE Referral TO USER --->
                    <cfquery name="updateUsersReferral" datasource="#Session.DBSourceEBM#" result="insertUsersReferralResult">
                        UPDATE 
                            simpleobjects.sire_user_referral
                        SET 
                            Total_referral_int = Total_referral_int+1,
                            Update_dt = NOW()
                        WHERE 
                            Referral_code_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpReferralCode#">
                        AND 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#resultReferralCode.UserId#">
                    </cfquery>    

                    <!---    
                    <!--- ADD Referral TO SIGN UP USER --->
                    <cfquery name="insertUsersReferral" datasource="#Session.DBSourceEBM#" result="insertUsersReferralResult">
                        INSERT INTO
                            simpleobjects.sire_user_referral
                            (
                                UserId_int,
                                Created_dt,
                                Is_refErral_int
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                            )    
                    </cfquery>
                    --->

                    <cfset dataout.SenderUserId = '#resultReferralCode.UserId#' />

                    <cfset dataout.RXResultCode = "1" />
                <cfelse>
                    <cfset dataout.ErrMessage = "Invalid Referral Code" />
                </cfif>

            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
             </cfcatch>
            </cftry>

        </cfif>  
            
        <cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateInviterPromotionCredits" access="public" output="false" hint="Update Promotion Credits for inviter after friend's payment success">
        <cfargument name="inpUserId" required="true" Type="numeric">
        <cfset var GetInviter = '' />
        <cfset var GetInviterInfo = '' />
        <cfset var UpdatePromotionCredits = '' />
        <cfset var getCurrentUserPlanResult = ''/>
        <cfset var UpdateUserPlanId = ''/>

        <cfset var dataout = {}/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.AddedCredits = 0 />
        <cfset dataout.SenderUserId = '' />

        <cfif arguments.inpUserId GT 0>
            <cftry>
                <cfquery name="GetInviter" datasource="#Session.DBSourceREAD#">
                    SELECT
                        UserReferral_int,
                        UserReferralCredit_int,
                        UserReferralPlanId_int
                    FROM
                        simpleobjects.sire_referral_log
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    LIMIT 
                        1
                </cfquery>

                <cfquery name="GetInviterInfo" datasource="#session.DBSourceREAD#">
                    SELECT
                        UserId_int
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetInviter.UserReferral_int#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfif GetInviterInfo.RECORDCOUNT LT 1>
                    <cfthrow type="Application" message="Account suspended!">
                </cfif>

                <!--- GET CURRENT USER PLAN ID --->
                <cfinvoke  method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getCurrentUserPlanResult">
                    <cfinvokeargument name="InpUserId" value="#arguments.inpUserId#">
                </cfinvoke>

                <cfif GetInviter.RECORDCOUNT GT 0 && GetInviter.UserReferralPlanId_int GT 1>
                    <cfquery name="UpdatePromotionCredits" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.sire_user_referral
                        SET
                            Total_promotion_credits_int = Total_promotion_credits_int + #_PromotionCredits#,
                            Update_dt = NOW()
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetInviter.UserReferral_int#">
                    </cfquery>

                    <cfquery name="UpdateUserPlanId" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.sire_referral_log
                        SET
                            UserPlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUserPlanResult.PLANID#">,
                            Update_dt = NOW()
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    </cfquery>

                    <!--- <cfset dataout.AddedCredits = "#GetInviter.UserReferralCredit_int#"> --->

                    <cfset dataout.RXResultCode = "1" />
                    <cfset dataout.SenderUserId = "#GetInviter.UserReferral_int#" />
                    
                <cfelseif GetInviter.RECORDCOUNT GT 0 && GetInviter.UserReferralPlanId_int EQ 1>
                    <cfquery name="UpdateUserPlanId" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.sire_referral_log
                        SET
                            UserPlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCurrentUserPlanResult.PLANID#">,
                            Update_dt = NOW()
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    </cfquery>
                    
                    <cfset dataout.RXResultCode = "1" />
                    <cfset dataout.SenderUserId = "#GetInviter.UserReferral_int#" />
                <cfelse>
                    <cfset dataout.ErrMessage = "Update Credits Failed!" />
                </cfif>
                <cfcatch>
                        <cfset dataout.Type = "#cfcatch.Type#" />
                        <cfset dataout.Message = "#cfcatch.Message#" />
                        <cfset dataout.ErrMessage = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        </cfif>
            
        <cfreturn dataout />

    </cffunction>
 
    <cffunction name="GetUserReferralCode" access="public" output="false" hint="Return current User referral Code">
        <cfset var dataout = {}/>
        <cfset var referralCode = ''/>
        <cfset var checkRfCode =''/>
        <cfset var insertUsersReferralResult =''/>
        <cfset var insertUsersReferral = ''/>
        <cfset var updateUsersReferral = ''/>
        <cfset var updateUsersReferralResult = ''/>
        <cfset var checkRfCodeResult = '' />

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.RfCode = "" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.RfLink = "" />
        

        <cftry>
            <cfquery name="checkRfCode" datasource="#Session.DBSourceEBM#" result="checkRfCodeResult">
                SELECT
                    Referral_code_vch
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">
            </cfquery>

            <cfif checkRfCode.recordCount GT 0>
                <cfset referralCode = checkRfCode.Referral_code_vch[1]>
               
                <cfif trim(referralCode EQ '')>  <!--- IF RF CODE is empty, create new one then update --->
                    <cfinvoke method="GenReferralCcde" returnvariable='referralCode'></cfinvoke>
                    <!--- UPDATE RF CODE TO USER --->
                    <cfquery name="updateUsersReferral" datasource="#Session.DBSourceEBM#" result="updateUsersReferralResult">
                        UPDATE 
                            simpleobjects.sire_user_referral
                        SET 
                            Referral_code_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#referralCode#">
                        WHERE 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">
                    </cfquery>    
                </cfif>

                <cfset dataout.RfCode = referralCode />
                <cfset dataout.RXResultCode = "1" />
                <cfset dataout.RfLink = _ReferralPlanPricingPath&""&referralCode/>

            <cfelse><!--- IF USER DO NOT HAVE RF CODE , CREATE NEW THEN INSER TO DB--->
                <cfinvoke method="GenReferralCcde" returnvariable='referralCode'></cfinvoke>
                <!--- ADD REFErrAL TO SIGN UP USER --->
                <cfquery name="insertUsersReferral" datasource="#Session.DBSourceEBM#" result="insertUsersReferralResult">
                    INSERT INTO simpleobjects.sire_user_referral
                    (    
                        UserId_int,
                        Created_dt,
                        Referral_code_vch
                    )
                    VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#referralCode#">
                    )    
                </cfquery>   
                <cfset dataout.RfCode = referralCode />
                <cfset dataout.RXResultCode = "1" />
                <cfset dataout.RfLink = _ReferralPlanPricingPath&referralCode/>
            </cfif>

        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
           <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="CheckListContactStatus" access="remote" output="false" hint="Check if contact is already signed up or has been sent an invite, process for a list of contact, return two array: registedUserEmail - emails have already signed up, sentUserEmail - emails have been sent invitation">
        <cfargument name="inpContacts" type="string" required="true">

        <cfset var dataout = {} />
        <cfset var checkContacts = DeserializeJSON(arguments.inpContacts) />
        <cfset var retValGetRegistedUserEmail = '' />
        <cfset var retValGetSentInviteUserEmail = '' />
        <cfset var getRegistedUserEmail = '' />
        <cfset var getSentInviteUserEmail = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.registedUserEmail = arrayNew(1) />
        <cfset dataout.sentUserEmail = arrayNew(1) />
        <cfset dataout.message = "" />
        <cfset dataout.type = "" />
        <cfset dataout.errMessage = "" />

        <!--- Check if contact is already signup --->

        <cftry>
            <cfquery name="getRegistedUserEmail" datasource="#Session.DBSourceEBM#" result="retValGetRegistedUserEmail">
                SELECT
                    EmailAddress_vch
                FROM 
                    simpleobjects.useraccount
            </cfquery>

            <cfset dataout.RXResultCode = 1 />

            <cfif getRegistedUserEmail.RECORDCOUNT GT 0>
                <cfloop query="#getRegistedUserEmail#">
                    <cfif ArrayContains(checkContacts, EmailAddress_vch)>
                        <cfset ArrayAppend(dataout.registedUserEmail, EmailAddress_vch) />
                    </cfif>
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cftry>
            <cfquery name="getSentInviteUserEmail" datasource="#Session.DBSourceEBM#" result="retValGetSentInviteUserEmail">
                SELECT
                    ContactString_vch
                FROM 
                    simplequeue.sire_referral_msg_queue
                WHERE
                    Status_ti != 0
                    -- 0 is status of sent failed
                AND
                    Type_ti = 2
                    -- 2 is type of email
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    -- get this current user contact queue only
            </cfquery>

            <cfset dataout.RXResultCode = 1 />

            <cfif getSentInviteUserEmail.RECORDCOUNT GT 0>
                <cfloop query="#getSentInviteUserEmail#">
                    <cfif ArrayContains(checkContacts, ContactString_vch) && !ArrayContains(dataout.registedUserEmail, ContactString_vch)>
                        <cfset ArrayAppend(dataout.sentUserEmail, ContactString_vch) />
                    </cfif>
                </cfloop>
            </cfif>
            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="CheckContactStatus" access="remote" output="false" hint="Check if contact is already signed up or has been sent an invite, return contact status">
        <cfargument name="inpContact" type="string" required="true">
        <cfargument name="inpType" type="numeric" required="true">
        <!--- type: 1-sms, 2-email --->

        <cfset var dataout = {} />
        <cfset var checkContact = arguments.inpContact />
        <cfset var getRegistedUserContact = '' />
        <cfset var retValGetRegistedUserContact = '' />
        <cfset var getSentInviteUserContact = '' />
        <cfset var retValGetSentInviteUserContact = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.contactStatus = 0 />
        <cfset dataout.dateDiff = 0 />
        <!--- contactStatus: 0 - not exist, 1 - registed, 2 - invitation sent , 3 - not valid US number, 4 - user blocked --->

        <cfset dataout.message = "" />
        <cfset dataout.type = "" />
        <cfset dataout.errMessage = "" />

        <!--- Check if contact is already signup --->

        <cftry>
            <cfif arguments.inpType EQ 1>
                <!---Find and replace all non numerics except P X * #--->
                <cfset checkContact = REReplaceNoCase(checkContact, "[^\d^\*^P^X^##]", "", "ALL")>

                <!--- Clean up where start character is a 1 --->
                <cfif LEFT(checkContact, 1) EQ "1">
                    <cfset checkContact = RemoveChars(checkContact, 1, 1)>
                </cfif>
            </cfif>
            <cfquery name="getRegistedUserContact" datasource="#Session.DBSourceEBM#" result="retValGetRegistedUserContact">
                SELECT
                    <cfif #arguments.inpType# EQ 1>
                        MFAContactString_vch
                    <cfelse>
                        EmailAddress_vch
                    </cfif>
                FROM
                    simpleobjects.useraccount
                WHERE
                    <cfif #arguments.inpType# EQ 1>
                        MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContact#">
                    <cfelse>
                        EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkContact#">
                    </cfif>
                LIMIT 1
            </cfquery>

            <cfset dataout.RXResultCode = 1 />

            <cfif getRegistedUserContact.RECORDCOUNT GT 0>
                <cfset dataout.contactStatus = 1 />
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfif dataout.contactStatus EQ 0>
            <cftry>
                <cfif #arguments.inpType# EQ 2>
                    <cfquery name="getSentInviteUserContact" datasource="#Session.DBSourceEBM#" result="retValGetSentInviteUserContact">
                        SELECT
                            ContactString_vch
                        FROM
                            simplequeue.sire_referral_msg_queue
                        WHERE
                            Status_ti != 0
                            -- 0 is status of sent failed
                        AND
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkContact#">
                        AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            -- get this current user contact queue only
                        LIMIT 1
                    </cfquery>
                <cfelse>
                    <cfquery name="getSentInviteUserContact" datasource="#Session.DBSourceEBM#" result="retValGetSentInviteUserContact">
                        SELECT
                            ContactString_vch,
                            DATEDIFF(CURDATE(), DATE(Scheduled_dt)) as dateDiff,
                            UserId_int
                        FROM
                            simplequeue.contactqueue
                        WHERE
                            DTSStatusType_ti != 6
                            -- 0 is status of sent failed
                        AND
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkContact#">
                        AND
                            ContactType_int = 10 -- SMS Invite type
                        ORDER BY
                            DTSId_int DESC
                        LIMIT 1
                    </cfquery>
                </cfif>
                <cfset dataout.RXResultCode = 1 />

                <cfif getSentInviteUserContact.RECORDCOUNT GT 0>
                    <cfset dataout.contactStatus = 2 />
                    <cfif StructKeyExists(getSentInviteUserContact, 'dateDiff')>
                        <cfif getSentInviteUserContact.dateDiff GT _SameNumberInviteDuration>
                            <cfset dataout.contactStatus = 0 />
                        <cfelseif getSentInviteUserContact.UserId_int EQ #Session.USERID#>
                            <cfset dataout.dateDiff = getSentInviteUserContact.dateDiff />
                        <cfelse>
                            <cfset dataout.dateDiff = -1>
                        </cfif>
                    </cfif>
                </cfif>
                <cfcatch>
                    <cfset dataout.type = "#cfcatch.Type#" />
                    <cfset dataout.message = "#cfcatch.Message#" />
                    <cfset dataout.errMessage = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        </cfif>

        <cfif dataout.contactStatus EQ 0 AND arguments.inpType EQ 1>
            <cftry>
                <cfif NOT isvalid('telephone', arguments.inpContact)>
                    <cfset dataout.RXResultCode = 1 />
                    <cfset dataout.contactStatus = 3 />
                </cfif>
                <cfcatch>
                    <cfset dataout.type = "#cfcatch.Type#" />
                    <cfset dataout.message = "#cfcatch.Message#" />
                    <cfset dataout.errMessage = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        </cfif>

        <cfif dataout.contactStatus EQ 0 AND arguments.inpType EQ 1>
            <cftry>
                <cfset var CheckUserDNCBlock = CheckUserDNCBlock(checkContact) />
                <cfif CheckUserDncBlock.RXResultCode GT 0 AND CheckUserDncBlock.UserLocalDNCBlocked GT 0>
                    <cfset dataout.RXResultCode = 1 />
                    <cfset dataout.contactStatus = 4 />
                </cfif>
                <cfcatch>
                    <cfset dataout.type = "#cfcatch.Type#" />
                    <cfset dataout.message = "#cfcatch.Message#" />
                    <cfset dataout.errMessage = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        </cfif>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="CheckUserDNCBlock" access="private" output="false" hint="Check contact string if user had blocked service">
        <cfargument name="inpContactString" type="string" required="true">

        <cfset var dataout = {} />
        <cfset var GetUserDNCFroServiceRequest = '' />
        <cfset var GetShortCodeData = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.UserLocalDNCBlocked = 0 />

        <cfset dataout.message = "" />
        <cfset dataout.type = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
            <cfquery name="GetShortCodeData" datasource="#Session.DBSourceEBM#">
                SELECT
                    k.Response_vch,
                    k.Survey_int,
                    scr.RequesterId_int,
                    k.Keyword_vch,
                    sc.ShortCode_vch
                FROM
                    SMS.Keyword AS k
                LEFT OUTER JOIN
                    SMS.shortcoderequest AS scr
                ON
                    k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                JOIN
                    SMS.ShortCode AS sc
                ON
                    sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
                WHERE
                    k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#_SMSInviteBatchId#">
                AND
                    k.Active_int IN (1,-3)
                ORDER BY
                    k.BatchId_bi
                LIMIT 1
            </cfquery>

            <cfif GetShortCodeData.RecordCount GT 0>
                <cfquery name="GetUserDNCFroServiceRequest" datasource="#Session.DBSourceEBM#" >
                    SELECT
                        COUNT(oi.ContactString_vch) AS TotalCount
                    FROM
                        simplelists.optinout AS oi
                    WHERE
                        OptOut_dt IS NOT NULL
                    AND
                        OptIn_dt IS NULL
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetShortCodeData.ShortCode_vch#">
                    AND
                        oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                </cfquery>

                <cfif GetUserDNCFroServiceRequest.TotalCount GT 0>
                    <cfset dataout.RXResultCode = 1 />
                    <cfset dataout.UserLocalDNCBlocked = 1 />
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetSMSQuota" access="remote" output="false" hint="Get current user sms invite quota">

        <cfset var dataout = {} />
        <cfset var countSentSMS = '' />
        <cfset var retValCountSentSMS = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.smsQuota = _MaxSMSInvite />

        <cfset dataout.message = "" />
        <cfset dataout.type = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
            <cfquery name="countSentSMS" datasource="#Session.DBSourceEBM#" result="retValCountSentSMS">
                SELECT
                    ContactString_vch
                FROM
                    simplequeue.contactqueue
                WHERE
                    DTSStatusType_ti IN ('1', '2', '5')
                    -- get contact has no failed status
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    -- get this current user contact queue only
                AND
                    DATE(Scheduled_dt) = CURDATE()
                AND
                    ContactType_int = 10 -- SMS Invite type
            </cfquery>

            <cfset dataout.RXResultCode = 1 />

            <cfif countSentSMS.RECORDCOUNT GT 0>
                <cfset dataout.smsQuota = dataout.smsQuota - countSentSMS.RECORDCOUNT />
            </cfif>


            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetYahooAuthUrl" access="remote" output="false" hint="Get Yahoo api authentication url">
        <cfset var dataout = {} />
        <cfset var state = "" />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.AUTHURL = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cftry>
            <cfset state = randRange(100000, 999999) />

            <cfset Session.yhState = state />

            <cfset dataout.AUTHURL = "https://api.login.yahoo.com/oauth2/request_auth?"&
                        "client_id="& "#_YHClientId#" &
                        "&response_type=token"&
                        "&redirect_uri="& "#_RedirectURL#" &
                        "&scope=sdct-r"&
                        "&state="& "#state#" />
            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserYahooContacts" access="remote" output="false" hint="Get user's yahoo contact by Yahoo Contact API, using authorized access token">
        <cfargument name="inpAccessToken" type="string" required="true">
        <cfargument name="inpState" type="string" required="true">

        <cfset var dataout = {}>
        <cfset var retValAPIRequest = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.contacts = structNew() />
        <cfset dataout.message = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
            <!--- check yhState for api request attack --->
            <cfif arguments.inpState EQ Session.yhState>
                <!--- Call yahoo contact api to get user's contact, email only, limit by 1000 --->
                <cfhttp
                    url="https://social.yahooapis.com/v1/user/me/contacts;email.present=1?format=json"
                    method="get"
                    result="retValAPIRequest"
                    timeout="10">
                    <!--- Setting access token for oauth --->
                    <cfhttpparam type="header" name="Authorization" VALUE="Bearer #inpAccessToken#" />
                </cfhttp>

                <cfif retValAPIRequest.status_code EQ 200>
                    <!--- Return result --->
                    <cfset dataout.RXResultCode = 1>
                    <cfset dataout.message = "Success" />
                    <cfset dataout.contacts = #retValAPIRequest.filecontent#>
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetOutlookAuthUrl" access="remote" output="false" hint="Get outlook api authentication url">
        <cfset var dataout = {} />
        <cfset var nonce = "" />
        <cfset var state = "" />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.AUTHURL = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cftry>
            <cfset nonce = randRange(100000, 999999) />
            <cfset state = randRange(100000, 999999) />

            <cfset Session.wdNonce = nonce />
            <cfset Session.wdState = state />

            <cfset dataout.AUTHURL = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?"&
                        "client_id="& "#_WDClientId#" &
                        "&response_type=token"&
                        "&redirect_uri="& "#_RedirectURL#" &
                        "&scope=openid+"& "#_WDScope#" &
                        "&state="& "#state#" &
                        "&nonce=" & "#nonce#" />
            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserOutlookContacts" access="remote" output="false" hint="Get user's outlook contact by Outlook Contact API, using authorized access token">
        <cfargument name="inpAccessToken" type="string" required="true">
        <cfargument name="inpState" type="string" required="true">

        <cfset var dataout = {}>
        <cfset var retValAPIRequest = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.contacts = structNew() />
        <cfset dataout.message = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
            <!--- Check wdState for api request attack --->
            <cfif arguments.inpState EQ Session.wdState>
                <!--- Call outlook contact api to get user's contact, email only, limit by 1000 --->
                <cfhttp
                    url="https://outlook.office.com/api/v2.0/me/contacts?$select=EmailAddresses&$top=1000"
                    method="get"
                    result="retValAPIRequest"
                    timeout="20">
                    <!--- Setting access token for oauth --->
                    <cfhttpparam type="header" name="Authorization" VALUE="Bearer #inpAccessToken#" />
                </cfhttp>

                <cfif structKeyExists(retValAPIRequest, "statuscode")>
                    <cfif retValAPIRequest.statuscode EQ 200>
                        <!--- Return result --->
                        <cfset dataout.RXResultCode = 1>
                        <cfset dataout.message = "Success" />
                        <cfset dataout.contacts = #retValAPIRequest.filecontent#>
                    </cfif>
                <cfelseif structKeyExists(retValAPIRequest, "status_code")>
                    <cfif retValAPIRequest.status_code EQ 200>
                        <!--- Return result --->
                        <cfset dataout.RXResultCode = 1>
                        <cfset dataout.message = "Success" />
                        <cfset dataout.contacts = #retValAPIRequest.filecontent#>
                    </cfif>
                </cfif>
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="GetGmailConfig" access="remote" output="false" hint="Get google api config">
        <cfset var dataout = {} />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.CONFIG = {} />
        <cfset dataout.CONFIG.CLIENT_ID = "" />
        <cfset dataout.CONFIG.SCOPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.TYPE = "" />

        <cftry>
            <cfset dataout.CONFIG.CLIENT_ID = _GGClientId />
            <cfset dataout.CONFIG.SCOPE = _GGScope />
            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserGmailContacts" access="remote" output="false" hint="Get user's gmail contact by Google Contact API, using authorized access token">
        <cfargument name="inpAccessToken" type="string" required="true">

        <cfset var dataout = {}>
        <cfset var retValAPIRequest = '' />

        <!--- Prepare return data --->
        <cfset dataout.RXResultCode = -1 />
        <cfset dataout.contacts = structNew() />
        <cfset dataout.message = "" />
        <cfset dataout.errMessage = "" />

        <cftry>
            <!--- Call google contact api to get user's contact, limit by 1000 --->
            <cfhttp 
                url="https://www.google.com/m8/feeds/contacts/default/thin?access_token=#inpAccessToken#&alt=json&max-results=1000"
                method="get"
                result="retValAPIRequest"
                timeout="10">
            </cfhttp>

            <cfif retValAPIRequest.status_code EQ 200>
                <!--- Return result --->
                <cfset dataout.RXResultCode = 1>
                <cfset dataout.message = "Success" />
                <cfset dataout.contacts = #retValAPIRequest.filecontent#>
            </cfif>

            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>

    <cffunction name="InsertReferralLog" access="public" output="true" hint="Insert log when user signup success">
        <cfargument name="inpReferralType" required="no" type="numeric" default="">
        <cfargument name="inpReferralCode" required="true" type="string">
        <cfargument name="inpUserId" required="true" type="string">

        <cfset var dataout = {} />
        <cfset var resultReferralCode = '' />
        <cfset var insertUsersReferral = '' />
        <cfset var insertUsersReferralResult = '' />
        <cfset var PlanCredits = '' />
        <cfset var getUserPlanResult = '' />
        <cfset var getCurrentUserPlanResult = '' />

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
     
        <cftry>
            <cfinvoke method="DecryptReferralCode" returnvariable='resultReferralCode'>
                <cfinvokeargument  name="inpReferralCode" VALUE="#arguments.inpReferralCode#">
            </cfinvoke>

            <!--- GET INVITER PLAN ID --->
            <cfinvoke  method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlanResult">
                <cfinvokeargument name="InpUserId" value="#resultReferralCode.UserId#">
            </cfinvoke>

            <cfquery name="PlanCredits" datasource="#Session.DBSourceREAD#">
                SELECT
                    FirstSMSIncluded_int
                FROM
                    simplebilling.plans
                WHERE
                    PlanId_int = #getUserPlanResult.PLANID#
            </cfquery>

            <!--- ADD Referral TO SIGN UP USER --->
            <cfquery name="insertUsersReferral" datasource="#Session.DBSourceEBM#" result="insertUsersReferralResult">
                INSERT INTO simpleobjects.sire_referral_log
                (    
                    UserId_int,
                    UserReferral_int,
                    ReferralType_int,
                    UserReferralPlanId_int,
                    UserReferralCredit_int,
                    Created_dt
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#resultReferralCode.UserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpReferralType#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPlanResult.PLANID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#PlanCredits.FirstSMSIncluded_int#">,
                    NOW()
                )    
            </cfquery> 
            <cfset dataout.RXResultCode = "1" />
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
               <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />

    </cffunction>


    <cffunction access="remote" returnformat="JSON" name="SaveUserInvitation" hint="Receive User Invitation Request And Save To DB">
        <cfargument name="inpType" required="true" type="numeric">
        <cfargument name="inpContacts" required="true" Type="string">

        <!--- Define local vars --->
        <cfset var dataout = {} />
        <cfset var rsInsert = "" />
        <cfset var sqlInsert = "" />
        <cfset var _comma = "" /> 
        <cfset var msgContent = {} />
        <cfset var userRFCode = {} />
        <cfset var longUrl = "" />
        <cfset var rsSUrl = "" />
        <cfset var retValAddInvitesToQueue = '' />
        <cfset var userSMSQuota = '' />
        <cfset var contact = '' />

        <!--- Message content here  --->
        <cfset var inpData = {
            FULLNAME = SESSION.FULLNAME,
            FIRSTNAME = SESSION.FIRSTNAME
        } />
        
        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cftry>

            <cfif arguments.inpType EQ 1 >

                <cfinvoke method="GetSMSQuota" returnvariable="userSMSQuota"></cfinvoke>

                <cfif userSMSQuota.RXResultCode LT 1 OR userSMSQuota.smsQuota LT ArrayLen(DeserializeJson(arguments.inpContacts))>
                    <cfthrow MESSAGE="User run out of sms quota" TYPE="Any" detail="" errorcode="-1">
                </cfif>

                <cfinvoke  method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>
                <cfset longUrl = "https://"&#CGI.http_host#&#userRFCode.RFLink#&"&type=1" />
                <cfinvoke method="getShortUrl" returnvariable="rsSUrl">
                    <cfinvokeargument name="longUrl" value="#longUrl#" />
                </cfinvoke>
                <cfset inpData.shortLink = rsSUrl.shortUrl />

                <cfinvoke method="GetInvitationSMSContent" returnvariable="msgContent">
                    <cfinvokeargument name="inpData" value="#inpData#">
                </cfinvoke>

            </cfif>
            <cfif arguments.inpType EQ 2 >
                <cfinvoke method="GetInvitationEmailContent" returnvariable="msgContent">
                    <cfinvokeargument name="inpType" value="1"> 
                    <cfinvokeargument name="inpData" value="#inpData#">
                </cfinvoke>
            </cfif>

            <!--- Detemate if contact data existed then insert to database  --->
            <cfif ArrayLen(DeserializeJson(arguments.inpContacts)) GT 0>
                <cfif arguments.inpType EQ 2>
                    <cfquery name="sqlInsert" datasource="#Session.DBSourceEBM#" result="rsInsert">
                        INSERT INTO
                            simplequeue.sire_referral_msg_queue
                            (
                                UUID_vch,
                                Type_ti,
                                UserId_int,
                                ContactString_vch,
                                Created_dt,
                                Content_txt
                            )
                        VALUES
                            <cfloop array="#DeserializeJson(arguments.inpContacts)#" index="contact">
                            #_comma#(
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CreateUUID()#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpType#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contact.email#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#msgContent.content#">
                            )
                            <cfset _comma = ',' />
                        </cfloop>
                    </cfquery>
                    <cfif rsInsert.recordCount GT 0>
                        <cfset dataout.RXResultCode = "1" />
                    </cfif>
                <cfelse>
                    <cfinvoke
                        component="session.sire.models.cfc.invitesmsdistribution"
                        method="AddInvitesToQueue"
                        returnvariable="retValAddInvitesToQueue">

                        <cfinvokeargument name="inpContactStrings" value="#arguments.inpContacts#">
                        <cfinvokeargument name="inpBatchId" value="#_SMSInviteBatchId#"> <!-- This batch id need to change went come to productions -->
                        <cfinvokeargument name="inpFriendName" value="#inpData.FULLNAME#">
                        <cfinvokeargument name="inpSignupURL" value="#inpData.shortLink#">
                    </cfinvoke>
                    <cfif retValAddInvitesToQueue.RXResultCode GT 0>
                        <cfset dataout.RXResultCode = "1" />
                    </cfif>
                </cfif>
            </cfif>

            <cfcatch>
            <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfcontent type="application/json" reset="true">
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="GetUserMonthFree" access="public" hint="get user free month" >
        <cfargument name="inpUserId" required="true" type="numeric">

        <cfset var dataout = {} />
        <cfset var getUserMonthFree = ''/>
        <cfset var getUserMonthFreeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.TotalMonthFeeHave = 0/>
        <cfset dataout.TotalMonthFeeUsed = 0/>
        <cfset dataout.TotalMonthFee = 0/>
        <cfset dataout.TotalReferral = 0/>
        <cftry>
            <cfquery name="getUserMonthFree" datasource="#Session.DBSourceEBM#" result="getUserMonthFreeResult">
                SELECT
                    Total_referral_int, Is_referral_int, Month_free_used_referral_used_int
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
            </cfquery>

            <cfif getUserMonthFree.RecordCount GT 0>
                <cfset dataout.TotalMonthFeeHave = getUserMonthFree.Total_referral_int[1] + getUserMonthFree.Is_referral_int[1] />
                <cfset dataout.TotalMonthFeeUsed = getUserMonthFree.Month_free_used_referral_used_int[1] />
                <cfset dataout.TotalMonthFee = dataout.TotalMonthFeeHave - dataout.TotalMonthFeeUsed />
                <cfset dataout.TotalReferral = getUserMonthFree.Total_referral_int[1] />
                 <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Message = "#cfcatch.Message#" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    

        </cftry>    
        <cfreturn dataout/>
    </cffunction>



    <cffunction name="GetInvitationEmailContent" access="public" hint="Get content for Email of Invitation message">
        <cfargument name="inpType" required="true" type="numeric">
        <cfargument name="inpData" required="true" type="struct">
        <cfset var dataout = {} />
        <cfset var template = '' />
        <cfset var dataEmail = {} />
        <cfset var contentString = '' />
        <cfset var userRFCode = '' />


        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.content = "" />

        <cftry>

            <cfif arguments.inpType EQ 1>
                <cfset dataEmail.senderName = arguments.inpData.FULLNAME />
                <cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>
                <cfset template = '/session/sire/views/emailtemplates/mail_template_referral_invitation.cfm' />
            </cfif>
            <cfif arguments.inpType EQ 2>
                <cfset dataEmail.senderFistName = arguments.inpData.FIRSTNAME />
                <cfset dataEmail.senderName = arguments.inpData.FULLNAME/>
                <cfset template = '/session/sire/views/emailtemplates/mail_template_referral_sender.cfm' />
            </cfif>

            <cfsavecontent variable="contentString">
                <cfinclude template="#template#">
            </cfsavecontent>

            <cfset dataout.content = contentString />
            <cfset dataout.RXResultCode = "1" />
            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>


    <cffunction name="GetInvitationSMSContent" access="private" hint="Get content for SMS of Invitation message">
        <cfargument name="inpData" required="true" type="struct">
        <cfset var dataout = {} />
        <cfset var contentString = structNew() />


        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.content = "" />

        <cfset contentString.fullName = arguments.inpData.fullName />
        <cfset contentString.shortLink = arguments.inpData.shortLink />
        <cfset contentString = serializeJSON(contentString) />

        <cftry>
            <cfset dataout.content = contentString />
            <cfset dataout.RXResultCode = "1" />
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>



    <cffunction name="SendMailNotificationToSender" access="public" hint="Send email to sender invitation.">
        <cfargument name="inpData" required="true" type="struct">

        <cfset var dataout = {} />
        <cfset var rsContent = {} />
        <cfset var dataEmail = {} />


        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.sendResult = "0" />

        <cftry>
            <cfset dataEmail.fullName = arguments.inpData.fullName />
            <cfset dataEmail.firstName = arguments.inpData.firstName />
            
            <cfinvoke method="GetInvitationEmailContent" returnvariable="rsContent">
                <cfinvokeargument name="inpType" value="2">
                <cfinvokeargument name="inpData" value="#dataEmail#">
            </cfinvoke>

            <cfinvoke component="public.sire.models.cfc.sendmail" method="SendMailByStringContent">
                <cfinvokeargument name="inpTo" VALUE="#arguments.inpData.emailAddress#">
                <cfinvokeargument name="inpSubject" VALUE="#arguments.inpData.subject#">
                <cfinvokeargument name="inpContent" VALUE="#rsContent.content#">
            </cfinvoke>

            <cfset dataout.RXResultCode = "1" />
            <cfset dataout.sendResult = "1" />
            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="getShortUrl" access="private" hint="Convert long url to short url by Google shorten url">
        <cfargument name="longUrl" type="string">
        <cfset var dataout = {} />
        <cfset var apiUrl = "https://www.googleapis.com/urlshortener/v1/url?key="&#ServerAPIKeyShortUrl# />
        <cfset var httpResp = {} />
        <cfset var dataResp = {} />
        <cfset var dataPost = "{longUrl: '"&#arguments.longUrl#&"'}" />
       
        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.shortUrl = "" />

        <cftry>

            <cfhttp url="#apiUrl#" charset="utf-8" method="POST" result="httpResp" timeout="120">
                <cfhttpparam type="header" name="Content-Type" value="application/json" />
                <cfhttpparam type="body" value="#dataPost#">
            </cfhttp>

            <cfset dataResp = DeserializeJSON(httpResp.filecontent) />
            <cfset dataout.shortUrl = dataResp.id />
            <cfset dataout.RXResultCode = "1" />
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserByReferralCode" access="public" hint="return user by referral code">
        <cfargument name="inpReferralCode" required="true" type="string" default="">

        <cfset var dataout = {} />
        <cfset var getInfo = '' />
        <cfset var UserId = '' />
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.sendResult = "0" />
        <cftry>
            <cfquery name="getInfo" datasource="#Session.DBSourceEBM#" result="UserId">
                SELECT
                    UserId_int
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    Referral_code_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpReferralCode#">
                AND 
                    Status_int = 1
            </cfquery>
            <cfset dataout.RXResultCode = "1" />
            <cfset dataout.sendResult = "1" />
            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>    
    </cffunction>

    <cffunction name="GetTwitterFollowers" access="remote" output="false" hint="Get user's twitter follower">
        <cfargument name="inpOAuthVerifier" required="true" default="">

        <cfset var dataout = {} />
        <cfset var oAuthToken = '' />
        <cfset var oAuthTokenSecret = '' />
        <cfset var returnData = structNew() />

        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.FOLLOWERS = [] />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.sendResult = "0" />

        <cfset oAuthToken = SESSION.twOAuthToken />
        <cfset oAuthTokenSecret = SESSION.twOAuthTokenSecret />

        <cftry>
            <cfscript>
                returnData    = application.objMonkehTweet.getAccessToken(
                    requestToken    =     oAuthToken,
                    requestSecret    =     oAuthTokenSecret,
                    verifier    =    arguments.inpOAuthVerifier
                );
            </cfscript>
            <cfif returnData.success GT 0>

                <cfscript>
                    session['twAccessToken']  = returnData.token;
                    session['twAccessSecret'] = returnData.token_secret;
                    session['twScreenName']  = returnData.screen_name;
                    session['twUserId'] = returnData.user_id;

                    application.objMonkehTweet.setFinalAccessDetails(
                            oauthToken        =     returnData.token,
                            oauthTokenSecret    =    returnData.token_secret,
                            userAccountName        =    returnData.screen_name
                    );
                </cfscript>

                <cfset dataout.FOLLOWERS = application.objMonkehTweet.getFollowersList('','','', 200) />

                <cfset dataout.RXResultCode = 1 />

            </cfif>
            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="SendTwitterDirectMessages" access="remote" output="false" hint="Get user's twitter follower">
        <cfargument name="inpContacts" required="true" type="string">

        <cfset var dataout = {} />
        <cfset var rsContent = '' />
        <cfset var userDetails = '' />
        <cfset var contact = '' />

        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.sendFailed = [] />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.sendResult = "0" />

        <cfset arguments.inpContacts = deserializeJSON(arguments.inpContacts) />

        <cftry>
            <cfif arrayLen(arguments.inpContacts) GT _ReferralLimit>
                <cfthrow MESSAGE="User run out of referral limit" TYPE="Any" detail="" errorcode="-1">
            </cfif>
            <cfif structKeyExists(session, 'twAccessToken') AND
                    structKeyExists(session, 'twAccessSecret') AND
                    structKeyExists(session, 'twScreenName')>

                <cfscript>
                    application.objMonkehTweet.setFinalAccessDetails(
                            oauthToken        =     session.twAccessToken,
                            oauthTokenSecret    =    session.twAccessSecret,
                            userAccountName        =    session.twScreenName
                    );

                    userDetails = deserializeJSON(application.objMonkehTweet.getUserDetails(session['twUserId']));
                </cfscript>

                <cfloop index="contact" array="#arguments.inpContacts#">
                    <cfinvoke method="GetTwitterMessageContent" returnvariable="rsContent">
                        <cfinvokeargument name="inpUserName" VALUE="#userDetails.name#">
                    </cfinvoke>
                    <cfif rsContent.RXResultCode LT 0>
                        <cfset var failedAttemp = {} />
                        <cfset failedAttemp.userId = "#contact.userId#" />
                        <cfset failedAttemp.userName = "#contact.userName#" />
                        <cfset failedAttemp.message = "Can't get user message" />
                        <cfset arrayAppend(dataout.sendFailed, failedAttemp) />
                    </cfif>

                    <cfset var returnData = structNew() />

                    <cfset returnData = deserializeJSON(application.objMonkehTweet.createDM('', contact.userId, rsContent.content)) />

                    <cfif structKeyExists(returnData, 'errors')>
                        <cfset var failedAttemp = {} />
                        <cfset failedAttemp.userId = "#contact.userId#" />
                        <cfset failedAttemp.userName = "#contact.userName#" />
                        <cfset failedAttemp.message = "#returnData.errors[1].message#" />
                        <cfset failedAttemp.code = "#returnData.errors[1].code#" />
                        <cfset arrayAppend(dataout.sendFailed, failedAttemp) />
                    </cfif>
                </cfloop>

                <cfset dataout.RXResultCode = 1 />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTwitterMessageContent" access="private" hint="Get content for twitter message">
        <cfargument name="inpUserName" required="true" type="string">
        <cfset var dataout = {} />
        <cfset var contentString = '' />
        <cfset var longUrl = '' />
        <cfset var userRFCode = '' />

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.content = "" />

        <cfinvoke  method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>
        <cfset longUrl = "https://"&#CGI.http_host#&#userRFCode.RFLink#&"&type=3" />

        <cfset contentString = arguments.inpUsername & ' has invited you to try Sire Mobile! Sign up to Sire and reach millions of customers in less than 5 minutes! ' & longUrl />

        <cftry>
            <cfset dataout.content = contentString />
            <cfset dataout.RXResultCode = "1" />
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetTwitterAuthUrl" access="remote" output="false" hint="Get twitter authen url">
        <cfset var dataout = {} />
        <cfset var twAuthStruct = structNew() />


        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.AUTHURL = '' />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.content = "" />

        <cftry>
            <cfset twAuthStruct
                = APPLICATION.objMonkehTweet.getAuthorisation(
                    callbackURL='https://'&'#CGI.SERVER_NAME#'&'/session/sire/pages/invite-friend-callback'
            )/>

            <cfif twAuthStruct.success>
                <cfset SESSION['twOAuthToken']
                    = twAuthStruct.token />
                <cfset SESSION['twOAuthTokenSecret']
                    = twAuthStruct.token_secret />

                <cfset dataout.AUTHURL = "#twAuthStruct.authURL#" />

                <cfset dataout.RXResultCode = "1" />
            </cfif>
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <!--- NEW REFERRAL PROMOTION CREDITS --->
     <cffunction name="GetUserReferralCredits" access="public" hint="get current rewards credits which not added to billing balance" output="false" >
        <cfargument name="inpUserId" required="true" type="numeric">

        <cfset var dataout = {} />
        <cfset var getUserMonthFree = ''/>
        <cfset var getUserMonthFreeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.referralCredits = 0/>

        <cftry>
            <cfquery name="getUserMonthFree" datasource="#Session.DBSourceEBM#" result="getUserMonthFreeResult">
                SELECT
                    Total_promotion_credits_int
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
            </cfquery>

            <cfif getUserMonthFree.RecordCount GT 0>
                <cfset dataout.referralCredits = getUserMonthFree.Total_promotion_credits_int[1]/>
                 <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Message = "#cfcatch.Message#" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    

        </cftry>    
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="ResetUserReferral" access="public" hint="reset current rewards credits to 0" output="false" >
        <cfargument name="inpUserId" required="true" type="numeric">

        <cfset var dataout = {} />
        <cfset var resetUserMonthFree = ''/>
        <cfset var resetUserMonthFreeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cftry>

            <cfquery name="resetUserMonthFree" datasource="#Session.DBSourceEBM#" result="resetUserMonthFreeResult">
                UPDATE
                    simpleobjects.sire_user_referral
                SET 
                    Total_promotion_credits_received_last_month_int = Total_promotion_credits_int,
                    Total_promotion_credits_received_int = Total_promotion_credits_received_int + Total_promotion_credits_int,
                    Total_promotion_credits_int = 0,
                    Total_referral_int = 0    
                WHERE
                    UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
            </cfquery>

            <cfif resetUserMonthFree.RecordCount GT 0>
                <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Message = "#cfcatch.Message#" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    

        </cftry>    
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="AddReferralCredits" access="public" hint="add referral credits to user balance" output="false" >
        <cfargument name="inpUserId" required="true" type="numeric" default="0">
        <cfargument name="inpReferralCredits" required="false" type="numeric" default="0">

        <cfset var dataout = {} />
        <cfset var updateUserPromotionCredits = ''/>
        <cfset var updateUserPromotionCreditsResult = ''/>
        <cfset var GetUserReferralCreditsResult = ''/>
        <cfset var referralCredits = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cfif arguments.inpUserId GT 0>
            <cftry>

                <cfif !structKeyExists(arguments, "inpReferralCredits") OR arguments.inpReferralCredits EQ 0 >
                    <cfinvoke method="GetUserReferralCredits" returnvariable="GetUserReferralCreditsResult">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>
                     <cfset referralCredits = GetUserReferralCreditsResult.referralCredits>
                <cfelse>
                      <cfset referralCredits = arguments.inpReferralCredits>
                </cfif> 

                <!--- UPDATE NEW REFERRAL CREDITS = REFERRAL CREDIT --->
                <cfquery name="updateUserPromotionCredits" datasource="#Session.DBSourceEBM#" result="updateUserPromotionCreditsResult">
                    UPDATE
                        simplebilling.billing
                    SET 
                        PromotionCreditBalance_int = PromotionCreditBalance_int + <cfqueryparam cfsqlType="CF_SQL_DECIMAL" value="#referralCredits#">
                    WHERE
                        UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>

                <cfif updateUserPromotionCreditsResult.RecordCount GT 0>

                    <!--- RESET USER CREDITS --->
                    <cfinvoke method="ResetUserReferral">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfset dataout.RXResultCode = "1" />
                <cfelse>
                    <cfset dataout.RXResultCode = "-1" />
                    <cfset dataout.Message = "#cfcatch.Message#" />
                </cfif>
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    

            </cftry>    
        <cfelse>
            <cfset dataout.ErrMessage = "UserId invalid." />
        </cfif>

        <cfreturn dataout/>
    </cffunction>

</cfcomponent>