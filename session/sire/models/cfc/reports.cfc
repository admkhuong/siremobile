<cfcomponent output="no">
        
<cfsetting showdebugoutput="no" />
   
    <!--- SMS Constants--->
<!---     <cfinclude template="../../../cfc/csc/constants.cfm"> --->

	<cfinclude template="/public/paths.cfm" >
	
   	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/session/sire/configs/paths.cfm">
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>

    
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
     
       
 	<cffunction name="GetChartByName" access="remote" output="false" hint="Get chart by name">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
		<cfargument name="inpChartName" required="yes" type="string">
        <cfargument name="inpReportType" required="no" type="string" hint="Type of report - CHART or TABLE or FORM">
        <cfargument name="inpChartPostion" required="yes" type="string">
        <cfargument name="ShowLegend" type="string" default="false" required="no"/>
        <cfargument name="CustomPie" type="string" default="false" required="no"/>
        <cfargument name="inpcustomdata1" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata2" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata3" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata4" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata5" required="no" type="string" default="" hint="Allow for custom report parameters">
                
        <cfset var TargetCFC = "sms" />
        <cfset var TargetReportName = "" />
        <cfset var DEBUG	= '' />
		<cfset var reportArr	= '' />
		<cfset var report	= '' />
		<cfset var reportItem	= '' />
		<cfset var dataout	= '' />
		<cfset var updateNullDial6	= '' />
		<cfset var Dial6Result	= '' />
        <cfset DEBUG = "OK">
                        
        <cftry>
	        <cfset arguments.inpStart = "#dateformat(arguments.inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset arguments.inpEnd = "#dateformat(arguments.inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			
			<!---<!--- Pre populate north american phone number NPANPXX from lookup data - makes other queries run faster.--->
	        <!--- Old copy and paste code - I fixed Coding stadards on query structure - JLP --->
			<cfquery name="updateNullDial6" datasource="#Session.DBSourceEBM#" result="Dial6Result">
	        	UPDATE
	            	simplexresults.contactresults
				SET 
	            	dial6_vch = left(ContactString_vch,6)
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       
	            AND
	            	dial6_vch is null
	        </cfquery>--->
	    	
	    	
	    	<cfif ListLen(inpChartName, '.') EQ 2>
		    	
		    	<cfset TargetCFC = ListGetAt(arguments.inpChartName, 1, '.') />
		    	<cfset TargetReportName = ListGetAt(arguments.inpChartName, 2, '.') />
		    	
	    	</cfif>	
	    	
			<cfset reportArr = ArrayNew(1) />
			
          	<cfset report = {}>
            <cfset report.DISPLAYDATA = "">
            
            <cfset reportItem = StructNew() />
            <cfset reportItem.REPORTTYPE = inpReportType/>    
            <cfset reportItem.DISPLAYDATA = ""/>
            <cfset reportItem.TABLEREPORTNAME = ReplaceNoCase(inpChartName, "Display_", "", "ALL")/>
            
            <cfset reportItem.INPCUSTOMDATA1 = "#inpcustomdata1#" />
            <cfset reportItem.INPCUSTOMDATA2 = "#inpcustomdata2#" />
            <cfset reportItem.INPCUSTOMDATA3 = "#inpcustomdata3#" />
            <cfset reportItem.INPCUSTOMDATA4 = "#inpcustomdata4#" />
            <cfset reportItem.INPCUSTOMDATA5 = "#inpcustomdata5#" />
			
            <cfif inpChartName NEQ "" >
            
                <cfinvoke component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" returnvariable="report">
                    <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                    <cfinvokeargument name="inpStart" value="#inpStart#"/>
                    <cfinvokeargument name="inpEnd" value="#inpEnd#"/>
                    <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>
                    <cfinvokeargument name="ShowLegend" value="#ShowLegend#"/>
                    <cfinvokeargument name="CustomPie" value="#CustomPie#"/>
                    <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
                    <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
                    <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
                    <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
                    <cfinvokeargument name="inpcustomdata5" value="#inpcustomdata5#"/>
                </cfinvoke>
        
                <!---<cfset reportItem.DISPLAYDATA = "Yo!">--->
                <cfset reportItem.DISPLAYDATA = report/>
                <cfset reportItem.REPORTTYPE = "#inpReportType#"/> 
                   
            </cfif>
         
            <cfset ArrayAppend(reportArr,reportItem)>	
				              
	        
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.ID = inpBatchIdList>
			<cfset dataout.REPORT_ARRAY = reportArr>
			
			<cfset dataout.User = Session.userID>	        	        
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   
   
   <cffunction name="GetSummaryInfo" access="remote" output="false" hint="Get summary information for report">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
       
       
		<cfset var DEBUG	= '' />
		<cfset var dataout	= '' />
		<cfset var updateNullDial6	= '' />
		<cfset var GetCountComplete	= '' />
		<cfset var GetCountQueue	= '' />
		<cfset var Dial6Result	= '' />
        <cfset DEBUG = "OK">
        
        <cftry>
	        <cfset arguments.inpStart = "#dateformat(arguments.inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset arguments.inpEnd = "#dateformat(arguments.inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			
            <!---<!--- Pre populate north american phone number NPANPXX from lookup data - makes other queries run faster.--->
	        <!--- Old copy and paste code - I fixed Coding stadards on query structure - JLP --->
			<cfquery name="updateNullDial6" datasource="#Session.DBSourceEBM#" result="Dial6Result">
	        	UPDATE 	
	            	simplexresults.contactresults
				SET 
	            	dial6_vch = left(ContactString_vch,6)
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       
	            AND
	            	dial6_vch is null
	        </cfquery>--->
            
			<cfquery name="GetCountComplete" datasource="#Session.DBSourceEBM#">
	        	SELECT
                	COUNT(*) AS TotalCount
                FROM 	
	            	simplexresults.contactresults
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	            AND    
	                rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                       	            
	        </cfquery>
            
            <cfquery name="GetCountQueue" datasource="#Session.DBSourceEBM#">
	        	SELECT
                	COUNT(*) AS TotalCount
                FROM 	
	            	simplequeue.contactqueue
				WHERE	
	            	batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
	           	AND
                	DTSStatusType_ti = 1                     	            
	        </cfquery>
	        
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.COUNTCOMPLETE = GetCountComplete.TotalCount>
            <cfset dataout.COUNTQUEUED = GetCountQueue.TotalCount>
				
			<cfset dataout.User = Session.userID>	        	        
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.COUNTCOMPLETE = 0>
            <cfset dataout.COUNTQUEUED = 0>
			<cfset dataout.DEBUG = DEBUG>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   

	<cffunction name="GetChartFromPreferences" access="remote" output="false" hint="Get summary information for report">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpStart" required="yes" type="string">
        <cfargument name="inpEnd" required="yes" type="string">
        <cfargument name="inpChartPostion" required="yes" type="string" hint="Which position in display to read chart for - One based 1,2,3....">
	    
	    <cfset var TargetCFC = "" />
	    <cfset var TargetReportName = "" />
	    <cfset var reportArr	= '' />
		<cfset var report	= '' />
		<cfset var reportItem	= '' />
		<cfset var dataout	= '' />
		<cfset var RetVarReadReportPreference	= '' />
      
        <cftry>
	        <cfset arguments.inpStart = "#dateformat(arguments.inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	        <cfset arguments.inpEnd = "#dateformat(arguments.inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
				    	
			<cfset reportArr = ArrayNew(1) />
            
            <!--- Read keyword from prefenerences --->
            <cfinvoke method="ReadReportPreference" returnvariable="RetVarReadReportPreference">
                <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>              
            </cfinvoke>
                   
            <cfset report = {}>
            <cfset report.DISPLAYDATA = "">
            
            <cfset reportItem = StructNew() />
            <cfset reportItem.REPORTTYPE = RetVarReadReportPreference.REPORTTYPE/>    
            <cfset reportItem.DISPLAYDATA = report/>
            <cfset reportItem.TABLEREPORTNAME = ""/>
            <cfset reportItem.TABLEREPORTNAME = ReplaceNoCase(RetVarReadReportPreference.REPORTNAME, "Display_", "", "ALL")/>                        
            <cfset reportItem.INPCUSTOMDATA1 = "#RetVarReadReportPreference.INPCUSTOMDATA1#" />
            <cfset reportItem.INPCUSTOMDATA2 = "#RetVarReadReportPreference.INPCUSTOMDATA2#" />
            <cfset reportItem.INPCUSTOMDATA3 = "#RetVarReadReportPreference.INPCUSTOMDATA3#" />
            <cfset reportItem.INPCUSTOMDATA4 = "#RetVarReadReportPreference.INPCUSTOMDATA4#" />
            <cfset reportItem.INPCUSTOMDATA5 = "#RetVarReadReportPreference.INPCUSTOMDATA5#" />
            <cfset reportItem.CLASSINFO = "#RetVarReadReportPreference.CLASSINFO#" />
                  
            <cfif RetVarReadReportPreference.REPORTNAME NEQ "" >            
            
	            <cfif ListLen(RetVarReadReportPreference.REPORTNAME, '.') EQ 2>
			    	
			    	<cfset TargetCFC = ListGetAt(RetVarReadReportPreference.REPORTNAME, 1, '.') />
			    	<cfset TargetReportName = ListGetAt(RetVarReadReportPreference.REPORTNAME, 2, '.') />
			    	
		    	</cfif>
	    	           
                <cfinvoke component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" returnvariable="report">
                    <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#"/>
                    <cfinvokeargument name="inpStart" value="#inpStart#"/>
                    <cfinvokeargument name="inpEnd" value="#inpEnd#"/>
                    <cfinvokeargument name="inpChartPostion" value="#inpChartPostion#"/>
                    <cfinvokeargument name="inpcustomdata1" value="#RetVarReadReportPreference.INPCUSTOMDATA1#"/>
                    <cfinvokeargument name="inpcustomdata2" value="#RetVarReadReportPreference.INPCUSTOMDATA2#"/>
                    <cfinvokeargument name="inpcustomdata3" value="#RetVarReadReportPreference.INPCUSTOMDATA3#"/> 
                    <cfinvokeargument name="inpcustomdata4" value="#RetVarReadReportPreference.INPCUSTOMDATA4#"/> 
                    <cfinvokeargument name="inpcustomdata5" value="#RetVarReadReportPreference.INPCUSTOMDATA5#"/>                            
                </cfinvoke>
                
                <cfset reportItem.REPORTTYPE = RetVarReadReportPreference.REPORTTYPE/> 
                <cfset reportItem.DISPLAYDATA = report/>
                
            </cfif>
                      
	        <cfset dataout = StructNew()>
	        <cfset dataout.RXRESULTCODE = 1>			
			<cfset dataout.inpStart = inpStart>
			<cfset dataout.inpEnd = inpEnd>
			<cfset dataout.ID = inpBatchIdList>
			<cfset dataout.REPORT_ARRAY = reportItem>
			
        <cfcatch type="Any" >
        	<cfset dataout = StructNew()>
        	<cfset dataout.RXRESULTCODE = -1>
			<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			<cfset dataout.ERR_MESSAGE = cfcatch.Detail>	
        </cfcatch>
        </cftry>
       
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="GetDashBoardCount" access="remote" output="false" hint="Count all charts currently on this dashboard">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
	    
	    <cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var GetCountReportingPreference = '' />
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetCountReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					COUNT(UserReportingPref_id) AS TotalCount
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">                             
			 </cfquery>
		 
         	<cfif GetCountReportingPreference.RecordCount GT 0>
            	<cfset dataout.TOTALCOUNT = GetCountReportingPreference.TotalCount />     
                <cfset dataout.ERR_MESSAGE = "RC=#GetCountReportingPreference.RecordCount#"/>                   
            <cfelse>            
           		<cfset dataout.TOTALCOUNT = "0" />              
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TOTALCOUNT = "0" /> 
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="UpdateReportPreference" access="remote" output="false" hint="Update report preference">
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
		<cfargument name="inpReportName" required="yes" type="string" hint="Name of the report to run for this report position">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
        <cfargument name="inpReportType" required="no" type="string" hint="Type of report - CHART or TABLE or FORM">
        <cfargument name="inpcustomdata1" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata2" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata3" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata4" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpcustomdata5" required="no" type="string" default="" hint="Allow for custom report parameters">
        <cfargument name="inpClassInfo" required="no" type="string" default="" hint="Allow object display parameters to be changed by user">
        <cfargument name="inpStart" required="no" type="string" default=""  hint="Store Master Report Date Range">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="Store Master Report Date Range">
 		
 		<cfset var dataout	= '' />
		<cfset var GetPreviousReportingPreference	= '' />
		<cfset var UpdatePreference	= '' />
		<cfset var InsertPreference	= '' />

        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					Count(*) AS TotalCount
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                
			 </cfquery>
		 
         	 <cfif GetPreviousReportingPreference.TotalCount GT 0>
         	
				 <!---update to database ---> 
                 <cfquery name="UpdatePreference" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.userreportingpref
                    SET
                        ReportName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportName#">,
                        ReportType_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportType#">,
                        DateUpdated_dt = NOW(),
                        customdata1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata1#">,
                        customdata2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata2#">,
                        customdata3_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata3#">,
                        customdata4_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">,
                        customdata5_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata5#">,
                        ClassInfo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">,
                        Start_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#" null="#IIF(TRIM(inpStart) EQ "", true, false)#">,
                        End_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#" null="#IIF(TRIM(inpEnd) EQ "", true, false)#">
                    WHERE	
                        userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                    AND 
                        BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                    AND
               	 		QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">
                 </cfquery>
			  
              
              <cfelse>
              
              	 <!--- Insert to database ---> 
                 <cfquery name="InsertPreference" datasource="#Session.DBSourceEBM#">
                    INSERT INTO 
                        simpleobjects.userreportingpref
                        (
                            UserId_int,
                            BatchListId_vch,
                            QuadarantId_int,
                            ReportName_vch,
                            ReportType_vch,
                            customdata1_vch,
                            customdata2_vch,
                            customdata3_vch,
                            customdata4_vch,
                            customdata5_vch,
                            ClassInfo_vch,
                            DateUpdated_dt,
                            Start_dt,
                            End_dt
                        )
                    VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportName#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpReportType#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata1#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata2#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata3#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata5#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#" null="#IIF(TRIM(inpStart) EQ "", true, false)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#" null="#IIF(TRIM(inpEnd) EQ "", true, false)#">                    
                        )
                 </cfquery>

              
              </cfif>
              
              
			 <cfset dataout.RXRESULTCODE = 1/>
			  <cfset dataout.MESSAGE = "Update report preference successfully"/>
        <cfcatch type="Any" >
			 <cfset dataout.RXRESULTCODE = -1/>
			 <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
			 <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   </cffunction>
   
   <cffunction name="ReadReportPreference" access="remote" output="false" hint="Read report preference">
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
	    <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
 		
 		<cfset var dataout	= '' />
 		<cfset var GetPreviousReportingPreference	= '' />

        <cfset dataout = StructNew()/>
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	SELECT
					ReportName_vch,
                    ReportType_vch,
                    customdata1_vch,
                    customdata2_vch,
                    customdata3_vch,
                    customdata4_vch,
                    customdata5_vch,
                    ClassInfo_vch,
                    Start_dt,
                    End_dt 
            	FROM
            		simpleobjects.userreportingpref
                WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#"> 
                ORDER BY
	                QuadarantId_int ASC                 
			 </cfquery>
		 
         	<cfif GetPreviousReportingPreference.RecordCount GT 0>
            	<cfset dataout.REPORTNAME = GetPreviousReportingPreference.ReportName_vch />
                <cfset dataout.REPORTTYPE = GetPreviousReportingPreference.ReportType_vch />
                <cfset dataout.INPCUSTOMDATA1 = GetPreviousReportingPreference.customdata1_vch />
                <cfset dataout.INPCUSTOMDATA2 = GetPreviousReportingPreference.customdata2_vch />
                <cfset dataout.INPCUSTOMDATA3 = GetPreviousReportingPreference.customdata3_vch />   
                <cfset dataout.INPCUSTOMDATA4 = GetPreviousReportingPreference.customdata4_vch />   
                <cfset dataout.INPCUSTOMDATA5 = GetPreviousReportingPreference.customdata5_vch />   
                <cfset dataout.CLASSINFO = GetPreviousReportingPreference.ClassInfo_vch />
                <cfset dataout.START = GetPreviousReportingPreference.Start_dt />
                <cfset dataout.END = GetPreviousReportingPreference.End_dt />    
                
                
                <cfif GetPreviousReportingPreference.Start_dt EQ "">
                	<cfset dataout.START = DATEADD("d", -7, NOW())/>
                </cfif>
                
                 <cfif GetPreviousReportingPreference.End_dt EQ "">
                	<cfset dataout.END = NOW()/>
                </cfif>
                            
            <cfelse>            
           		<cfset dataout.REPORTNAME = "" />
                <cfset dataout.REPORTTYPE = "" />
                <cfset dataout.INPCUSTOMDATA1 = "" />
                <cfset dataout.INPCUSTOMDATA2 = "" />
                <cfset dataout.INPCUSTOMDATA3 = "" />
                <cfset dataout.INPCUSTOMDATA4 = "" />
                <cfset dataout.INPCUSTOMDATA5 = "" />
                <cfset dataout.CLASSINFO = "" />
                <cfset dataout.START = "" />
                <cfset dataout.END = "" />
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Get report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   </cffunction>
   
    <cffunction name="ReadDateBoundaries" access="remote" output="false" hint="Get the date range of data for specified Batches. Output date as strings in Java locale-specific time encoding standards for date picker to use." >
   	    <cfargument name="inpBatchIdList" required="yes" type="string"  hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="LimitDays" required="no" type="numeric"  hint="Pass in 1 or more days to limit max range too. 0 is default full range" default="0">
		
        <cfset var dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = {} />
         <cfset var GetPreviousReportingPreference1 = {} />
        <cfset var inpStart = ''/>
   		<cfset var inpEnd = ''/>
   		
   		<cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.REPORTNAME = "" />
   		<cfset dataout.MAXDATE = "" />
        <cfset dataout.MINDATE = "" />
        
        <cftry>
			
			<cfset GetPreviousReportingPreference.RecordCount = 0/>
			
			<!--- First look if there is already a set of stored values for any object --->
			 <cfquery name="GetPreviousReportingPreference1" datasource="#Session.DBSourceEBM#">
                SELECT 
                       MAX(End_dt) AS MAXDATE,
                       MIN(Start_dt) AS MINDATE
                FROM 	
                    simpleobjects.userreportingpref
                 WHERE	
                	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
             </cfquery>  
			
			<cfif GetPreviousReportingPreference1.RecordCount GT 0>
            
                <cfif GetPreviousReportingPreference1.MINDATE NEQ "">
                	<cfset dataout.MINDATE = "#LSDateFormat(GetPreviousReportingPreference1.MINDATE,'full')#" />
                </cfif>
                
                <cfif GetPreviousReportingPreference1.MAXDATE NEQ "">
                	<cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference1.MAXDATE,'full')#"  />
                </cfif>
                
			<cfelse>            
           		<cfset dataout.MAXDATE = "" />
                <cfset dataout.MINDATE = "" />
            </cfif>
            
            
            <cfif dataout.MAXDATE NEQ "" AND dataout.MAXDATE NEQ "">
	            
	            <cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = ""/>
				<cfreturn dataout/>
            
            </cfif>    
            
            
            <cfif TRIM(inpBatchIdList) EQ "">
            	<cfset GetPreviousReportingPreference.RecordCount = 1 />
            	<cfset GetPreviousReportingPreference.MAXDATE = NOW() />
                <cfset GetPreviousReportingPreference.MINDATE = DATEADD("d", -7, NOW()) />
            
            <cfelse>
            	
                <cfif LimitDays	GT 0>
                
					<cfset GetPreviousReportingPreference.RecordCount = 1 />
                    <cfset GetPreviousReportingPreference.MAXDATE = NOW() />
                    <cfset GetPreviousReportingPreference.MINDATE = DATEADD("d", -LimitDays, NOW()) />
                
                <cfelse>
                
                     <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                        SELECT 
                               MAX(rxcdlstarttime_dt) AS MAXDATE,
                               MIN(rxcdlstarttime_dt) AS MINDATE
                        FROM 	
                            simplexresults.contactresults cd 
                        WHERE
                            batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
                     </cfquery>  
    			
                
                </cfif>
                
		 	</cfif>
            
            
         	<cfif GetPreviousReportingPreference.RecordCount GT 0>
            
                <cfif GetPreviousReportingPreference.MINDATE EQ "">
                	<cfset GetPreviousReportingPreference.MINDATE = "#dateformat(NOW(),'yyyy-mm-dd')#" & " " & "00:00:00"/>
                </cfif>
                
                 <cfif GetPreviousReportingPreference.MAXDATE EQ "">
                	<cfset GetPreviousReportingPreference.MAXDATE = "#dateformat(NOW(),'yyyy-mm-dd')#" & " " & "23:59:59"/>
                </cfif>
            
            	<!--- LimitDays will keep long running campaigns from causing reporting to be unresponsive or crash on too large of data --->
            	<cfif LimitDays	GT 0>
                	
                    <cfif DateDiff("d", GetPreviousReportingPreference.MINDATE, GetPreviousReportingPreference.MAXDATE) GT LimitDays >
                    
                    	<cfif LimitDays EQ 1>
                    
                    		<cfset inpStart = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "00:00:00">
   							<cfset inpEnd = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "23:59:59">

							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(inpStart,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(inpEnd,'full')#" />
                    
                    	<cfelse>
                        
                        	<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(DateAdd("d", -LimitDays, GetPreviousReportingPreference.MAXDATE),'full')#" />
                    
                        </cfif>
                        
                    <cfelse>
                    
                    	<cfif LimitDays EQ 1>
                    
                    		<cfset inpStart = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "00:00:00">
   							<cfset inpEnd = "#dateformat(GetPreviousReportingPreference.MAXDATE,'yyyy-mm-dd')#" & " " & "23:59:59">

							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(inpStart,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(inpEnd,'full')#" />
                    
                    	<cfelse>
                        
							<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                            <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                            <cfset dataout.MINDATE = "#LSDateFormat(GetPreviousReportingPreference.MINDATE,'full')#" />
                        
                        </cfif>
                        
                    </cfif>
                
                <cfelse>
            
					<!--- Output in Java locale-specific time encoding standards for date picker to use --->
                    <cfset dataout.MAXDATE =  "#LSDateFormat(GetPreviousReportingPreference.MAXDATE,'full')#"  />
                    <cfset dataout.MINDATE = "#LSDateFormat(GetPreviousReportingPreference.MINDATE,'full')#" />
                
                </cfif>
                
                
            <cfelse>            
           		<cfset dataout.MAXDATE = "" />
                <cfset dataout.MINDATE = "" />
            </cfif>
              
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = ""/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.MAXDATE = "" />
            <cfset dataout.MINDATE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>


			<cfset dataout.FULL = SerializeJSON(cfcatch)/>

        </cfcatch>
        </cftry>

		<cfreturn dataout/>
   	</cffunction>

	
 <cffunction name="DeleteDashboardObj" access="remote" output="false" hint="Remove a single Dashboard Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
	    
	    <cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var RemovePreviousReportingPreference = '' />
        <cfset var UpdateReportingPreference = '' />
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="RemovePreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	DELETE FROM simpleobjects.userreportingpref
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
             <!--- Reorder --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int - 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Remove report preference success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="UpdateDashboardObjectClassInfo" access="remote" output="false" hint="Update an Objects class info for a single Dashboard Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpChartPostion" required="yes" type="numeric" hint="Position in array of reports this is being set">
        <cfargument name="inpClassInfo" required="yes" type="string" hint="Update Class list">
	    
		<cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var UpdateReportingPreference = '' />
        <cftry>
			             
             <!--- Reorder --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	ClassInfo_vch =<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpClassInfo#">
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpChartPostion#">                                      
			 </cfquery>
             
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Report preference update success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>

   <cffunction name="UpdateDashboardObjectDateRange" access="remote" output="false" hint="Update the Date Range for the Report">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
	   	<cfargument name="inpStart" required="no" type="string" default=""  hint="Store Master Report Date Range">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="Store Master Report Date Range">
        
		<cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var UpdateReportingPreference = '' />
        <cftry>
			             
             <!--- Reorder --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	 Start_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#" null="#IIF(TRIM(inpStart) EQ "", true, false)#">,
                     End_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#" null="#IIF(TRIM(inpEnd) EQ "", true, false)#">
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
            </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Report preference update success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
      
   <cffunction name="UpdateDashboardObjectPosition" access="remote" output="false" hint="Reorder Charts for Display">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpOriginalPos" required="yes" type="numeric" hint="1 Based Position - Original">
        <cfargument name="inpNewPos" required="yes" type="numeric" hint="1 Based Position - New">
	    
	    <cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var UpdateReportingPreference = '' />
        <cfset var ReorderAboveOld = '' />
        <cfset var ReorderAboveNew = '' />
        <cfset var ReplaceStash = '' />
        <cftry>
			             
             <!--- Stash in 100000 (unreachable) position  --->
             <cfquery name="UpdateReportingPreference" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = 100000
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpOriginalPos#">                                      
			 </cfquery>
             
             <!--- Reorder everthing above old position --->
             <cfquery name="ReorderAboveOld" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int - 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpOriginalPos#">                                      
			 </cfquery>             
             
             <!--- Reorder everthing above new position --->
             <cfquery name="ReorderAboveNew" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = QuadarantId_int + 1 
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNewPos#">                                      
			 </cfquery>             
             
             <!--- ReplaceStash--->
             <cfquery name="ReplaceStash" datasource="#Session.DBSourceEBM#">
			 	UPDATE
            		simpleobjects.userreportingpref
                SET
                	QuadarantId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNewPos#">
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">  
                AND
                	QuadarantId_int = 100000                                      
			 </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Report preference update success"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   
   <cffunction name="SaveCurrentAsTemplate" access="remote" output="false" hint="Save current Dashboard as Template Object">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        <cfargument name="inpDesc" required="yes" type="string" hint="Give the template a descriptive name">
        
        <cfset var dataout	= '' />
		<cfset var DashTemplate	= '' />
		<cfset var Position	= '' />
		<cfset var TemplateJSON	= '' />       
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
        <cfset var InsertTemplate = '' />
        <cfset DashTemplate = ArrayNew(1)>
        <cfset var tempItem = {} />
        
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT
                    ReportName_vch,
                    ReportType_vch,
                    customdata1_vch,
                    customdata2_vch,
                    customdata3_vch,
                    customdata4_vch,
                    customdata5_vch,
                    ClassInfo_vch,
                    QuadarantId_int
                FROM
                    simpleobjects.userreportingpref
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                    BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">
                ORDER BY
                    QuadarantId_int ASC                 
            </cfquery>
            
            <cfset Position = 1/>
            <cfloop query="GetPreviousReportingPreference">
            
				<cfset tempItem = {} />
                                
                <cfset tempItem.POS = Position />
                <cfset tempItem.NAME = GetPreviousReportingPreference.ReportName_vch />
                <cfset tempItem.TYPE = GetPreviousReportingPreference.ReportType_vch />
                <cfset tempItem.CLASSINFO = GetPreviousReportingPreference.ClassInfo_vch />
                <cfset tempItem.CUSTOMDATA1 = GetPreviousReportingPreference.customdata1_vch />
                <cfset tempItem.CUSTOMDATA2 = GetPreviousReportingPreference.customdata2_vch />
                <cfset tempItem.CUSTOMDATA3 = GetPreviousReportingPreference.customdata3_vch />
                <cfset tempItem.CUSTOMDATA4 = GetPreviousReportingPreference.customdata4_vch />
                <cfset tempItem.CUSTOMDATA5 = GetPreviousReportingPreference.customdata5_vch />
                                
                <cfset ArrayAppend(DashTemplate,tempItem)>
                
                <cfset Position = Position + 1/>
            </cfloop>
             
          
  			<cfset TemplateJSON = serializeJSON(DashTemplate) />	
             
            <!--- Insert to database ---> 
            <cfquery name="InsertTemplate" datasource="#Session.DBSourceEBM#">
                INSERT INTO 
                    simpleobjects.dashboard_templates
                    (
                        UserId_int,
                        Desc_vch,
                        Template_vch,
                        Created_dt,
                        LastUpdated_dt,
                        Active_int
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TemplateJSON#">,
                        NOW(),
                        NOW(),
                        1                                            
                    )
            </cfquery>
             
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template saved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <cffunction name="ReadTemplate" access="remote" output="false" hint="Remove a single Dashboard Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns or Generic Template Id">
        
        <cfset var dataout	= '' />              
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
                
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT
                    Template_vch,
                    Desc_vch                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    (
                    	userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                    OR
                    	userid_int=0
                     )
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">       
                AND 
                    Active_int = 1                       
            </cfquery>
            
            <cfset dataout.TEMPLATE = "#GetPreviousReportingPreference.Template_vch#"/>
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.TEMPLATE = "[]"/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
    <cffunction name="ReadTemplateList" access="remote" output="false" hint="Read List of Dashboard Templates">
	   	            
		<cfset var dataout	= '' />
		<cfset var tempItem	= '' />
		<cfset var GetPreviousReportingPreferenceGlobal	= '' />		
        <cfset dataout = StructNew()/>
        <cfset var GetPreviousReportingPreference = '' />
        <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
                
        <cftry>
			
            <!---Get previous preferences - good for all users --->        	        
            <cfquery name="GetPreviousReportingPreferenceGlobal" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    userid_int = 0
                AND 
                    Active_int = 1 
                ORDER BY 
                	pkId_int ASC                            
            </cfquery>
            
            <cfloop query="GetPreviousReportingPreferenceGlobal">	
                                    
                <cfset tempItem = [				
                    '#GetPreviousReportingPreferenceGlobal.pkId_int#',                    
                    '#GetPreviousReportingPreferenceGlobal.Desc_vch#',
					'#GetPreviousReportingPreferenceGlobal.userid_int#'
					
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
                                     
			<!---Get previous preferences--->        	        
            <cfquery name="GetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                SELECT  
	                pkId_int,                  
                    Desc_vch,
                    userid_int                    
                FROM
                    simpleobjects.dashboard_templates
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                    Active_int = 1  
                ORDER BY
                	Desc_vch ASC, pkId_int ASC                               
            </cfquery>
                        
            <cfloop query="GetPreviousReportingPreference">	
                                    
                <cfset tempItem = [				
                    '#GetPreviousReportingPreference.pkId_int#',                    
                    '#GetPreviousReportingPreference.Desc_vch#',
					'#GetPreviousReportingPreference.userid_int#'
                ]>		
                <cfset ArrayAppend(dataout["TEMPLATELIST"],tempItem)>
            </cfloop>
            
                         
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template retrieved!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout["TEMPLATELIST"] = ArrayNew(1)>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   
   <cffunction name="ClearDashboard" access="remote" output="false" hint="Remove a all Dashboard Objects">
	   	<cfargument name="inpBatchIdList" required="yes" type="string" hint="Pass in one or more comma seperated Batch Ids">
        
        <cfset var dataout	= '' />      
        <cfset dataout = StructNew()/>
        <cfset var RemovePreviousReportingPreference = '' />
    
        <cftry>
			 <!---Get previous preference--->        	        
			 <cfquery name="RemovePreviousReportingPreference" datasource="#Session.DBSourceEBM#">
			 	DELETE FROM simpleobjects.userreportingpref
                WHERE	
                	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
                	BatchListId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchIdList#">                                                      
			 </cfquery>
             
                        
		     
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Clear dashboard success!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
      <cffunction name="RemoveTemplate" access="remote" output="false" hint="Remove a single Dashboard Template Object">
	   	<cfargument name="inpId" required="yes" type="numeric" hint="Template Id that this user owns">
        
        <cfset var dataout	= '' />              
        <cfset dataout = StructNew()/>
        <cfset var SetPreviousReportingPreference = '' />
                
        <cftry>
			             
			<!---Get previous preferences--->        	        
            <cfquery name="SetPreviousReportingPreference" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.dashboard_templates
                SET
                 	Active_int = 0    
                WHERE	
                    userid_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
                AND 
	                pkId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpId#">                                           
            </cfquery>
                                  
			<cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = "Dashboard template removed!"/>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1/>
            <cfset dataout.REPORTNAME = "" />
            <cfset dataout.REPORTTYPE = "" />
            <cfset dataout.MESSAGE = cfcatch.MESSAGE/>
            <cfset dataout.ERR_MESSAGE = cfcatch.Detail/>
        </cfcatch>
        </cftry>
      
		<cfreturn dataout>
   </cffunction>
   
   <!---  http://www.bennadel.com/blog/1239-Updated-Converting-A-ColdFusion-Query-To-CSV-Using-QueryToCSV-.htm  --->
    <cffunction
        name="QueryToCSV"
        access="public"
        returntype="string"
        output="false"
        hint="I take a query and convert it to a comma separated value string.">
     
        <!--- Define arguments. --->
        <cfargument
            name="Query"
            type="query"
            required="true"
            hint="I am the query being converted to CSV."
            />
                 
        <cfargument
            name="Fields"
            required="false"
            default="#arguments.query.columnList#"
            hint="I am the list of query fields to be used when creating the CSV value. Print everything by default"
            />

        <cfargument
            name="CreateHeaderRow"
            type="boolean"
            required="false"
            default="true"
            hint="I flag whether or not to create a row of header values."
            />
     
        <cfargument
            name="Delimiter"
            type="string"
            required="false"
            default=","
            hint="I am the field delimiter in the CSV value."
            />
     
        <!--- Define the local scope. --->
        <cfset var LOCAL = {} />
     
        <!---
            First, we want to set up a column index so that we can
            iterate over the column names faster than if we used a
            standard list loop on the passed-in list.
        --->
        <cfset LOCAL.ColumnNames = [] />
     
        <!---
            Loop over column names and index them numerically. We
            are working with an array since I believe its loop times
            are faster than that of a list.
        --->
        <cfloop
            index="LOCAL.ColumnName"
            list="#ARGUMENTS.Fields#"
            delimiters=",">
     
            <!--- Store the current column name. --->
            <cfset ArrayAppend(
                LOCAL.ColumnNames,
                Trim( LOCAL.ColumnName )
                ) />
     
        </cfloop>
     
        <!--- Store the column count. --->
        <cfset LOCAL.ColumnCount = ArrayLen( LOCAL.ColumnNames ) />
     
     
        <!---
            Now that we have our index in place, let's create
            a string buffer to help us build the CSV value more
            effiently.
        --->
        <cfset LOCAL.Buffer = CreateObject( "java", "java.lang.StringBuffer" ).Init() />
     
        <!--- Create a short hand for the new line characters. --->
        <cfset LOCAL.NewLine = (Chr( 13 ) & Chr( 10 )) />
     
     
        <!--- Check to see if we need to add a header row. --->
        <cfif ARGUMENTS.CreateHeaderRow>
     
            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />
     
            <!--- Loop over the column names. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">
     
                <!--- Add the field name to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#LOCAL.ColumnNames[ LOCAL.ColumnIndex ]#""" />
     
            </cfloop>
     
            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />
     
        </cfif>
     
     
        <!---
            Now that we have dealt with any header value, let's
            convert the query body to CSV. When doing this, we are
            going to qualify each field value. This is done be
            default since it will be much faster than actually
            checking to see if a field needs to be qualified.
        --->
     
        <!--- Loop over the query. --->
        <cfloop query="ARGUMENTS.Query">
     
            <!--- Create array to hold row data. --->
            <cfset LOCAL.RowData = [] />
     
            <!--- Loop over the columns. --->
            <cfloop
                index="LOCAL.ColumnIndex"
                from="1"
                to="#LOCAL.ColumnCount#"
                step="1">
     
                <!--- Add the field to the row data. --->
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = """#Replace( ARGUMENTS.Query[ LOCAL.ColumnNames[ LOCAL.ColumnIndex ] ][ ARGUMENTS.Query.CurrentRow ], """", """""", "all" )#""" />
     
     			<!--- JLP Data with newlines will screw up export so remove them - covers both linux and dos newlines--->
     			<cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 13 ), "", "ALL") />
                <cfset LOCAL.RowData[ LOCAL.ColumnIndex ] = REREPLACE(LOCAL.RowData[ LOCAL.ColumnIndex ], Chr( 10 ), "", "ALL") />
            </cfloop>
     
     
            <!--- Append the row data to the string buffer. --->
            <cfset LOCAL.Buffer.Append(
                JavaCast(
                    "string",
                    (
                        ArrayToList(
                            LOCAL.RowData,
                            ARGUMENTS.Delimiter
                            ) &
                        LOCAL.NewLine
                    ))
                ) />
     
        </cfloop>
     
     
        <!--- Return the CSV value. --->
        <cfreturn LOCAL.Buffer.ToString() />
    </cffunction>

    <cffunction name="GetCampaignReport" access="remote" hint="Get campaign report">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="CampaignId">
    	<cfargument name="inpStartDate" type="string" required="no" default="">
	    <cfargument name="inpEndDate" type="string" required="no" default="">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />


        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
	   	<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.QUESTION = ''>
		<cfset var getCPResult = ''/>
		<cfset var cpid = 0 />
		<cfset var qid = 0 />
		<cfset var lastIndex = 0 />
		<cfset var GetCampaignType = '' />
		<cfset var RetGetCampaignReport = '' />
		<cfset var questionType = '' />

	   	<cfif arguments.inpUserId EQ "">
	   		<cfset arguments.inpUserId = SESSION.USERID>
	   	</cfif>

        <!--- <cftry> --->
            <cfquery name="GetCampaignType" datasource="#Session.DBSourceREAD#">
				SELECT
					TemplateId_int,TemplateType_ti
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				AND	
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                LIMIT 1    
            </cfquery>

            <cfif GetCampaignType.RECORDCOUNT GT 0>

            	<cfif GetCampaignType.TemplateId_int EQ 9 OR GetCampaignType.TemplateId_int EQ 6 OR GetCampaignType.TemplateId_int EQ 5 
            	OR GetCampaignType.TemplateId_int EQ 1 OR GetCampaignType.TemplateId_int EQ 2 OR GetCampaignType.TemplateId_int EQ 3 >
            		<cfset questionType = 'SHORTANSWER'/>
            	<cfelseif GetCampaignType.TemplateId_int EQ 4>	
            		<cfset questionType = 'ONESELECTION'/>
            	</cfif>	

            	<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    				<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    				<cfinvokeargument name="inpCPType" value="#questionType#"/>
    			</cfinvoke>    			
    			<cfif getCPResult.RXRESULTCODE EQ 1>
    				<cfset lastIndex = arrayLen(getCPResult['DATALIST'])/>
    				<cfset qid = getCPResult['DATALIST'][lastIndex].ID/>
    				<cfset cpid = getCPResult['DATALIST'][lastIndex].RQ/>
    				<cfset dataout.QUESTION = getCPResult['DATALIST'][lastIndex].TEXT>

    				<cfif GetCampaignType.TemplateId_int EQ 1 OR GetCampaignType.TemplateId_int EQ 2 OR GetCampaignType.TemplateId_int EQ 3 OR GetCampaignType.TemplateId_int EQ 8 OR GetCampaignType.TemplateId_int EQ 10>
    					<cfinvoke method="GetCampaignBuildYourFirstReport" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		    	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		    	        	<cfinvokeargument name="iDisplayStart" value="#arguments.iDisplayStart#"/>
		    	        	<cfinvokeargument name="iDisplayLength" value="#arguments.iDisplayLength#"/>
		    	        	<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
			    			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
		            	</cfinvoke>
		            	
	    			<cfelseif GetCampaignType.TemplateId_int EQ 9>
	    				<cfset dataout.QUESTION = getCPResult['DATALIST'][1].TEXT>
    					<cfset qid = getCPResult['DATALIST'][1].ID/>
    					<cfset cpid = getCPResult['DATALIST'][1].RQ/>
						<!--- set 0 for select all--->
						<cfset qid = 0/>
    					<cfset cpid = 0/>
						<!--- end set 0 for select all--->
		            	<cfinvoke method="GetCampaignCollectFeedbackReport" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		    	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		    	        	<cfinvokeargument name="iDisplayStart" value="#arguments.iDisplayStart#"/>
		    	        	<cfinvokeargument name="iDisplayLength" value="#arguments.iDisplayLength#"/>
		    	        	<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
			    			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
			    			<cfinvokeargument name="inpCPID" value="#cpid#"/>
			    			<cfinvokeargument name="inpQID" value="#qid#"/>
		            	</cfinvoke>
	            	<cfelseif GetCampaignType.TemplateId_int EQ 6>
	            		<cfinvoke method="GetCampaignOpenEndedReport" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		    	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		    	        	<cfinvokeargument name="iDisplayStart" value="#arguments.iDisplayStart#"/>
		    	        	<cfinvokeargument name="iDisplayLength" value="#arguments.iDisplayLength#"/>
		    	        	<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
			    			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
							<cfinvokeargument name="inpCPID" value="#cpid#"/>
			    			<cfinvokeargument name="inpQID" value="#qid#"/>
		            	</cfinvoke>
		            <cfelseif GetCampaignType.TemplateId_int EQ 5>	
		            	<cfinvoke method="GetCampaignSingleYesNoReport" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		    	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		    	        	<cfinvokeargument name="iDisplayStart" value="#arguments.iDisplayStart#"/>
		    	        	<cfinvokeargument name="iDisplayLength" value="#arguments.iDisplayLength#"/>
		    	        	<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
			    			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
							<cfinvokeargument name="inpCPID" value="#cpid#"/>
			    			<cfinvokeargument name="inpQID" value="#qid#"/>
		            	</cfinvoke>
	            	<cfelseif GetCampaignType.TemplateId_int EQ 4>	
		            	<cfinvoke method="GetMultipleChoiceByBatch" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
		    	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		    	        	<cfinvokeargument name="iDisplayStart" value="#arguments.iDisplayStart#"/>
		    	        	<cfinvokeargument name="iDisplayLength" value="#arguments.iDisplayLength#"/>
		    	        	<cfinvokeargument name="inpStartDate" value="#arguments.inpStartDate#"/>
			    			<cfinvokeargument name="inpEndDate" value="#arguments.inpEndDate#"/>
							<cfinvokeargument name="inpCPID" value="#cpid#"/>
			    			<cfinvokeargument name="inpQID" value="#qid#"/>
		            	</cfinvoke>
	            	</cfif>	

	            	<cfif RetGetCampaignReport.RXRESULTCODE GT 0>
	            		<cfset dataout["DATALIST"] = RetGetCampaignReport["DATALIST"]>
	            		<cfset dataout["iTotalRecords"] = RetGetCampaignReport['iTotalRecords']>
						<cfset dataout["iTotalDisplayRecords"] = RetGetCampaignReport['iTotalDisplayRecords']>
	            	</cfif>

    			</cfif>

            	<cfset dataout.RXRESULTCODE = 1 />   
            </cfif>
            

        <!---     <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry> --->

        <cfreturn dataout />              

    </cffunction>

    <cffunction name="GetCampaignBuildYourFirstReport" hint="get report by campaign type = build your first campaign" access="remote">
	    <cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="1" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="1" />

        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getReportData = ''/>
	   	<cfset var rsCount = '' />
	   	<cfset var dataItem = {} />

        <!--- <cftry> --->
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ire.ContactString_vch,
					ire.Response_vch,
					ire.Created_dt,
					ire.BatchId_bi,
					ss.LastCP_int,
					ss.SessionId_bi,
					ss.SessionState_int,
					ire.T64_ti
				FROM
					simplexresults.ireresults ire
				JOIN
					simplequeue.sessionire ss
				ON
					ire.IRESessionId_bi = ss.SessionId_bi 
				WHERE
					ire.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				AND	
                    ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                	ire.IREType_int = 2 
                AND	
                	LENGTH(ire.ContactString_vch) < 14
                AND 
                	ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
                ORDER BY ire.Created_dt DESC	
            	<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GTE 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>	
            </cfquery>
            
            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getReportData">
					<cfset dataItem = {} />
					<cfset dataItem.CONTACTSTRING = #ContactString_vch#/>
					<cfif T64_ti EQ 1>
						<cfset dataItem.RESPONSE = ToString( ToBinary(Response_vch ), "UTF-8" )/>
					<cfelse>
						<cfset dataItem.RESPONSE = #Response_vch#/>
					</cfif>
					<cfset dataItem.CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt')/>
					<cfset dataItem.LASTCP = getReportData.LastCP_int/>
					<cfset dataItem.SESSIONID = getReportData.SessionId_bi/>
					<cfset dataItem.SESSIONSTATE = getReportData.SessionState_int/>
					<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>
        <!---     <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry> --->

        <cfreturn dataout />        	
    </cffunction>

    <cffunction name="GetCampaignCollectFeedbackReport" hint="get report by campaign type = collect feedback" access="remote">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="1" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="1" />

        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getReportData = ''/>
	   	<cfset var rsCount = '' />
	   	<cfset var dataItem = {} />

        <!--- <cftry> --->
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ire.ContactString_vch,
					ire.Response_vch,
					ire.Created_dt,
					ire.BatchId_bi,
					ss.LastCP_int,
					ss.SessionId_bi,
					ss.SessionState_int,
					ire.T64_ti
				FROM
					simplexresults.ireresults ire
				JOIN
					simplequeue.sessionire ss
				ON
					ire.IRESessionId_bi = ss.SessionId_bi 
				WHERE
					ire.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				AND	
                    ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                	ire.IREType_int = 2 
                AND	
                	LENGTH(ire.ContactString_vch) < 14
                AND 
                	ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
				<cfif arguments.inpQID GT 0>
                AND 
                	ire.QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQID#">
				</cfif>
				<cfif arguments.inpCPID GT 0>
                AND 				
                	ire.CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPID#">
				</cfif>
                ORDER BY ire.Created_dt DESC	
            	<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GTE 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>	
            </cfquery>
            
            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getReportData">
					<cfset dataItem = {} />
					<cfset dataItem.CONTACTSTRING = #ContactString_vch#/>
					<cfif T64_ti EQ 1>
						<cfset dataItem.RESPONSE = ToString( ToBinary(Response_vch ), "UTF-8" )/>
					<cfelse>
						<cfset dataItem.RESPONSE = #Response_vch#/>
					</cfif>
					<cfset dataItem.CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt')/>
					<cfset dataItem.LASTCP = getReportData.LastCP_int/>
					<cfset dataItem.SESSIONID = getReportData.SessionId_bi/>
					<cfset dataItem.SESSIONSTATE = getReportData.SessionState_int/>
					<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>
        <!---     <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry> --->

        <cfreturn dataout />              

    </cffunction>

    <cffunction name="GetCampaignOpenEndedReport" hint="get report by campaign type = OpenEnded" access="remote">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="4" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="2" />

        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getReportData = ''/>
	   	<cfset var rsCount = '' />
	   	<cfset var dataItem = {} />

        <!--- <cftry> --->
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ContactString_vch,
					Response_vch,
					Created_dt,
					BatchId_bi,
					T64_ti
				FROM
					simplexresults.ireresults
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				AND	
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                	IREType_int = 2 
                AND	
                	LENGTH(ContactString_vch) < 14
                AND 
                	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
                AND 
                	QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQID#">
                AND 
                	CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPID#">

                ORDER BY Created_dt DESC	
            	<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GTE 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>	
            </cfquery>

            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getReportData">
					<cfset dataItem = {} />
					<cfset dataItem.CONTACTSTRING = #ContactString_vch#/>
					<cfif T64_ti EQ 1>
						<cfset dataItem.RESPONSE = ToString( ToBinary(Response_vch ), "UTF-8" )/>
					<cfelse>
						<cfset dataItem.RESPONSE = #Response_vch#/>
					</cfif>
					<cfset dataItem.CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt')/>
					<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>
        <!---     <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry> --->
        <cfreturn dataout />              

    </cffunction>

    <cffunction name="GetCampaignSingleYesNoReport" hint="get report by campaign type = SingleYesNo" access="remote">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="4" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="2" />

        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getReportData = ''/>
	   	<cfset var rsCount = '' />
	   	<cfset var dataItem = {} />

        <!--- <cftry> --->
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					Response_vch,
					T64_ti,
					COUNT(*) AS Total
				FROM
					simplexresults.ireresults
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
				AND	
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                	IREType_int = 2 
                AND	
                	LENGTH(ContactString_vch) < 14
                AND 
                	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
                AND 
                	QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQID#">
                AND 
                	CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPID#">
                GROUP BY
                	Response_vch,T64_ti
                ORDER BY Created_dt DESC
            	<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GTE 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>	
            </cfquery>

            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getReportData">
					<cfset dataItem = {} />
					<cfif T64_ti EQ 1>
						<cfset dataItem.RESPONSE = ToString( ToBinary(Response_vch ), "UTF-8" )/>
					<cfelse>
						<cfset dataItem.RESPONSE = #Response_vch#/>
					</cfif>
					<cfset dataItem.COUNT = Total/>
					<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>
        <!---     <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry> --->
        <cfreturn dataout />              

    </cffunction>

    <cffunction name="GetSumSelectionByBatchAndSelection" access="public" hint="Get total user choose a selection of a single multiple choice question" >
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpAVAL" required="true"/>
    	<cfargument name="inpCPID" required="false" default=""/>
    	<cfargument name="inpQID" required="false" default=""/>

    	<cfset var dataout = {} />
    	<cfset var answerList = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.SUM = 0 />

    	<cfset var getSum = '' />
    	<cfset var RetVarReadCPs = '' />
    	<cfset var getCPResult = '' />
    	<cfset var parseVal = '' />
    	<cfset var readQuestionResult = '' />
    	<cfset var index = '' />
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>

    	<cftry>

    		<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
    		</cfinvoke>

    		<!--- Get ALL SVAL --->
    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestionAnswers" returnvariable="readQuestionResult">
			    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
			    <cfinvokeargument name="inpQID" value="#arguments.inpCPID#"/>
			</cfinvoke>

    		<cfif getCPResult['DATALIST'][1].AF EQ 'ALPHAPAR' OR getCPResult['DATALIST'][1].AF EQ 'ALPHA'>
				<cfloop from="1" to="#arrayLen(readQuestionResult.CPOBJ.OPTIONS)#" index="index" >
					<cfset answerList[#readQuestionResult.CPOBJ.OPTIONS[index].AVAL#] = alphaAnswerChoice[index]>
				</cfloop>
			<cfelse>
				<cfloop from="1" to="#arrayLen(readQuestionResult.CPOBJ.OPTIONS)#" index="index" >
					<cfset answerList[#readQuestionResult.CPOBJ.OPTIONS[index].AVAL#] = numberAnswerChoice[index]>
				</cfloop>
			</cfif>

			<cfset parseVal = arguments.inpAVAL/>


    		<!--- <cfif getCPResult.RXRESULTCODE GT 0> --->
    			<!--- <cfset arguments.inpCPID = getCPResult['DATALIST'][1].RQ/>
    			<cfset arguments.inpQID = getCPResult['DATALIST'][1].ID/> --->
    			<cfquery name="getSum" datasource="#Session.DBSourceREAD#">
	    			SELECT
	    				IREREsultsId_bi
	    			FROM
	    				simplexresults.ireresults
	    			WHERE
	    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
	    			AND
	    				IREType_int = 2
	    			AND
	    				Response_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#answerList[#parseVal#]#">
	    			AND
	    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCPID#">
	    			AND
	    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpQID#">
	    			AND
	    			 	LENGTH(ContactString_vch) < 14
	    		</cfquery>
	    		<cfset dataout.RXRESULTCODE = 1/>
	    		<cfset dataout.SUM = getSum.RECORDCOUNT />
	    		<cfset dataout.MESSAGE = 'Get sum successfully' />
	    <!--- 	<cfelse>
	    		<cfset dataout.RXRESULTCODE = 0/>
	    		<cfset dataout.SUM = getSum.RECORDCOUNT />
	    		<cfset dataout.MESSAGE = 'Get sum failed' />
    		</cfif> --->
    		
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetMultipleChoiceByBatch" access="remote" hint="get multiple choice by batch id">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="0" />

    	<cfset var dataout = {} />
    	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var temp = {} />

    	<cfset var readQuestionResult = '' />
    	<cfset var getSumResult = '' />
    	<cfset var index = '' />
    	<cfset var getCPResult = '' />

    	<!--- <cftry> --->

<!---     		<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
    		</cfinvoke>

    		<cfset arguments.inpQId = getCPResult['DATALIST'][1].RQ/> --->

    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestionAnswers" returnvariable="readQuestionResult">
			    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
			    <cfinvokeargument name="inpQID" value="#arguments.inpCPID#"/>
			</cfinvoke>
			
			<cfif readQuestionResult.RXRESULTCODE EQ 1>
				<cfloop array="#readQuestionResult.CPOBJ.OPTIONS#" index="index">
					<cfinvoke method="GetSumSelectionByBatchAndSelection" returnvariable="getSumResult">
						<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
						<cfinvokeargument name="inpAVAL" value="#index.AVAL#"/>
						<cfinvokeargument name="inpCPID" value="#inpCPID#"/>
						<cfinvokeargument name="inpQID" value="#inpQID#"/>
					</cfinvoke>

					<cfset temp = {
						ID = #arguments.inpBatchId#,
						TEXT = #index.TEXT#,
						VAL = #index.AVAL#,
						SUM = #getSumResult.SUM#
					}/>

					<cfset ArrayAppend(dataout["DATALIST"],temp)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Get multiple choice failed' />
			</cfif>

    	<!--- 	<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry> --->

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetCustomerByResponseAndBatch" access="remote" hint="get customer by response and batch id of one selection question">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpAVAL" required="true"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpCPID" required="false" default=""/>
    	<cfargument name="inpQID" required="false" default=""/>
    	<cfargument name="inpCPType" required="false" default=""/>

		<cfset var dataout = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getList = ''/>
    	<cfset var rsCount = ''/>
    	<cfset var readQuestionResult = ''/>
    	<cfset var parseVal = ''/>
    	<cfset var index = ''/>
    	<cfset var getCPResult = ''/>
    	<cfset var temp = {} />
    	<cfset var answerList = {} />
    	<cfset var getCPResult = ''/>
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>

		<cftry>
			<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="#arguments.inpCPType#"/>
    		</cfinvoke>

    		<!--- Get ALL SVAL --->
    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestionAnswers" returnvariable="readQuestionResult">
			    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
			    <cfinvokeargument name="inpQID" value="#getCPResult['DATALIST'][1].RQ#"/>
			</cfinvoke>

    		<cfif getCPResult['DATALIST'][1].AF EQ 'ALPHAPAR' OR getCPResult['DATALIST'][1].AF EQ 'ALPHA'>
				<cfloop from="1" to="#arrayLen(readQuestionResult.CPOBJ.OPTIONS)#" index="index" >
					<cfset answerList[#readQuestionResult.CPOBJ.OPTIONS[index].AVAL#] = alphaAnswerChoice[index]>
				</cfloop>
			<cfelse>
				<cfloop from="1" to="#arrayLen(readQuestionResult.CPOBJ.OPTIONS)#" index="index" >
					<cfset answerList[#readQuestionResult.CPOBJ.OPTIONS[index].AVAL#] = numberAnswerChoice[index]>
				</cfloop>
			</cfif>

			<cfset parseVal = arguments.inpAVAL/>

			<cfquery name="getList" datasource="#Session.DBSourceREAD#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				Created_dt,
    				ContactString_vch,
    				Response_vch
    			FROM
    				simplexresults.ireresults
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
    			AND
    				IREType_int = 2
    			<cfif arguments.inpAVAL NEQ ''>
    			AND
    				Response_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#answerList[#parseVal#]#">
    			</cfif>	
    			AND
    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].RQ#">
    			AND
    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].ID#">
    			AND
    				LENGTH(ContactString_vch) < 14
    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>
    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceREAD#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getList">
					<cfset temp = {
						CONTACTSTRING = #ContactString_vch#,
						CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt'),
						RESPONSE = #Response_vch#
					} />
					<cfset ArrayAppend(dataout["DATALIST"],temp)>
				</cfloop>
            </cfif>
            <cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
    </cffunction>

    <cffunction name="GetCustomerByResponseAndBatchSingleYesNo" access="remote" hint="get customer by response and batch id of one selection question">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpAVAL" required="true"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpCPID" required="false" default=""/>
    	<cfargument name="inpQID" required="false" default=""/>
    	<cfargument name="inpCPType" required="false" default=""/>

		<cfset var dataout = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getList = ''/>
    	<cfset var rsCount = ''/>
    	<cfset var temp = {} />
    	<cfset var getCPResult = '' />
    	<cfset var length = 1 />

		<cftry>
			<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="#arguments.inpCPType#"/>
    		</cfinvoke>

    		<cfif getCPResult.RXRESULTCODE GT 0>
    			<cfset length = len(getCPResult['DATALIST'])/>
    		</cfif>

			<cfquery name="getList" datasource="#Session.DBSourceREAD#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				Created_dt,
    				ContactString_vch,
    				Response_vch
    			FROM
    				simplexresults.ireresults
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
    			AND
    				IREType_int = 2
    			<cfif arguments.inpAVAL NEQ ''>
    			AND
    				Response_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpAVAL#">
    			</cfif>	
    			AND
    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][length].RQ#">
    			AND
    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][length].ID#">
    			AND
    				LENGTH(ContactString_vch) < 14
    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>
    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceREAD#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

				<cfloop query="getList">
					<cfset temp = {
						CONTACTSTRING = #ContactString_vch#,
						CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt'),
						RESPONSE = #Response_vch#
					} />
					<cfset ArrayAppend(dataout["DATALIST"],temp)>
				</cfloop>
            </cfif>
            <cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
    </cffunction>

    <cffunction name="GetCPByBatchIdAndType" access="remote" hint="Get CP by BatchId and Type">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpCPType" required="true"/>

    	<cfset var dataout = {} />
    	<cfset dataout["DATALIST"] = [] />
    	<cfset dataout.MESSAGE = 'Get CP failed' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var RetVarReadCPs = '' />
    	<cfset var CPOBJ = '' />
    	<cfset var RetVarReadCPDataById = '' />

    	<cftry>
    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
                <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
            </cfinvoke>

            <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">  

                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
                    <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                </cfinvoke>

                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ "#arguments.inpCPType#">
                    <cfset ArrayAppend(dataout["DATALIST"],RetVarReadCPDataById.CPOBJ)>
                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.MESSAGE = 'Get CP successfully!'/>
                </cfif>

            </cfloop>
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetMultipleChoiceBySurveyBatch" access="remote" hint="get multiple choice question of a Multiple Choice Survey question">
    	<cfargument name="inpBatchId" required="yes"/>
    	<!--- <cfargument name="inpQId" required="no" default=""/> --->
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />

		<cfset var dataout = {} />
		<cfset var temp = {} />
		<cfset var temp1 = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var questionList = [] />
		<cfset var answerList = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getCPResult = ''/>
    	<cfset var questionOrder = 1/>
    	<cfset var parseVal = ''/>
    	<cfset var index = ''/>
    	<cfset var index1 = ''/>
    	<cfset var getSum = ''/>
    	<cfset var ind = ''/>
    	<cfset var userOrgInfo = ''/>
    	<cfset var TOTALALLCHOOSE = 0/><!--- Numbers of customer answered a question ---> 
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>

    	<cftry>
    		<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
    		</cfinvoke>

    		<cfif getCPResult.RXRESULTCODE GT 0>
    			<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
    			</cfinvoke>

	    		<!--- Loop all One selection question in a Multiple Choice Batch --->
	    		<cfloop array="#getCPResult['DATALIST']#" index="index"> 
	    			<cfset TOTALALLCHOOSE = 0/>

	    			<cfif index.AF EQ 'ALPHAPAR' OR index.AF EQ 'ALPHA'>
						<cfloop from="1" to="#arrayLen(index.OPTIONS)#" index="ind" >
							<cfset answerList[#index.OPTIONS[ind].AVAL#] = alphaAnswerChoice[ind]>
						</cfloop>
					<cfelse>
						<cfloop from="1" to="#arrayLen(index.OPTIONS)#" index="ind" >
							<cfset answerList[#index.OPTIONS[ind].AVAL#] = numberAnswerChoice[ind]>
						</cfloop>
					</cfif>

	    			<cfset var valList = [] />

	    			<!--- Loop all Selection in one question --->
	    			<cfloop array="#index.OPTIONS#" index="index1">

	    				<cfset parseVal = index1.AVAL/>

	    				<cfquery name="getSum" datasource="#Session.DBSourceREAD#">
			    			SELECT
			    				IREREsultsId_bi
			    			FROM
			    				simplexresults.ireresults
			    			WHERE
			    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
			    			AND
			    				IREType_int = 2
			    			AND
			    				Response_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#answerList[#parseVal#]#">
			    			AND
			    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.RQ#">
			    			AND
			    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.ID#">
			    			AND
			    				LENGTH(ContactString_vch) < 14
			    		</cfquery>

	    				<cfset temp1 = {
	    					TEXT = index1.TEXT,
	    					VAL = index1.AVAL,
	    					TOTALCHOOSE = getSum.RecordCount,
	    					FORMAT = answerList[#parseVal#]
	    				}/>

	    				<cfset TOTALALLCHOOSE = TOTALALLCHOOSE + temp1.TOTALCHOOSE />
	    				<cfset ArrayAppend(valList,temp1)>
	    			</cfloop>

	    			<cfset temp = {
	    				NAME = "Question " & #questionOrder#,
	    				ALLVAL = valList,
	    				ALLCHOOSE = TOTALALLCHOOSE,
	    				TEXT = Replace(index.TEXT, "#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#:","")
		    		}/>
		    		<cfset questionOrder = questionOrder + 1/>
		    		<cfset ArrayAppend(dataout["DATALIST"],temp)>
	    		</cfloop>
	    		
	    		<cfset dataout["iTotalRecords"] = arrayLen(getCPResult['DATALIST'])>
				<cfset dataout["iTotalDisplayRecords"] = arrayLen(getCPResult['DATALIST'])>


	    		<cfset dataout.RXRESULTCODE = 1/>
	    		<cfset dataout.MESSAGE = 'Get Details successfully!' />
	    	<cfelse>
	    		<cfset dataout.RXRESULTCODE = 0/>
	    		<cfset dataout.MESSAGE = 'Get Details failed!' />
	    	</cfif>
    		
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>
    	
    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetReportDetailsForSurveyBatch" access="remote" hint="get customer list answered Survey">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />

    	<cfset var dataout = {} />
    	<cfset var dataout = {} />
		<cfset var temp = {} />
		<cfset var temp1 = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var answerList = []/>
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getCustomer = '' />
    	<cfset var getAnswerDetails = '' />
    	<cfset var index = '' />
    	<cfset var rsCount = '' />
    	<cfset var getCPResult = '' />
    	<cfset var userOrgInfo = '' />
    	<cfset var answerList = [] />
    	<cfset var questionTextList = [] />
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var alphaAnswer = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[A-Za-z]" ) ) />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>

    	<cftry>
    		<cfinvoke method="GetCPByBatchIdAndType" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
    		</cfinvoke>

    		<cfif getCPResult.RXRESULTCODE GT 0>

    			<!--- Get All Customer Answered the Survey --->
    			<cfquery name="getCustomer" datasource="#Session.DBSourceREAD#">
    				SELECT
    					SQL_CALC_FOUND_ROWS
	    				Created_dt,
	    				ContactString_vch,
	    				IRESessionId_bi
	    			FROM
	    				simplexresults.ireresults
	    			WHERE
	    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
	    			AND
	    				IREType_int = 2
	    			AND

	    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].RQ#">
	    			AND
	    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].ID#">

	    			AND
	    				LENGTH(ContactString_vch) < 14
	    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
						LIMIT 
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
						OFFSET 
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					</cfif>
    			</cfquery>

    			<cfif getCustomer.RECORDCOUNT GT 0>
	    			<cfquery datasource="#Session.DBSourceREAD#" name="rsCount">
						SELECT FOUND_ROWS() AS iTotalRecords
					</cfquery>
					<cfloop query="rsCount">
						<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
						<cfset dataout["iTotalRecords"] = iTotalRecords>
					</cfloop>
				</cfif>

				<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
				</cfinvoke>

    			<cfloop query="getCustomer">

    				<cfset answerList = [] />

    				<cfloop array="#getCPResult['DATALIST']#" index="index">
		    			<cfquery name="getAnswerDetails" datasource="#Session.DBSourceREAD#">
			    			SELECT
			    				Response_vch
			    			FROM
			    				simplexresults.ireresults
			    			WHERE
			    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
			    			AND
			    				IREType_int = 2
			    			AND
			    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.RQ#">
			    			AND
			    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.ID#">
			    			AND 
			    				ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getCustomer.ContactString_vch#">
			    			AND
			    				IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCustomer.IRESessionId_bi#">
			    			AND
			    				LENGTH(ContactString_vch) < 14
			    		</cfquery>

			    		<cfif ((index.AF EQ 'ALPHA' OR index.AF EQ 'ALPHAPAR') AND alphaAnswer.Matcher(JavaCast( "string", getAnswerDetails.Response_vch ) ).Find()) OR ((index.AF EQ 'NUMERIC' OR index.AF EQ 'NOFORMAT' OR index.AF EQ 'NUMERICPAR') AND ArrayContains(numberAnswerChoice, getAnswerDetails.Response_vch))>

				    		<cfset temp1 = {
				    			ANSWER = getAnswerDetails.Response_vch
				    		}/>

				    		<cfset ArrayAppend(answerList,temp1)>
				    	<cfelse>
				    		<cfset temp1 = {
				    			ANSWER = 'Not match Answer'
				    		}/>

				    		<cfset ArrayAppend(answerList,temp1)>
				    	</cfif>
		    		</cfloop>

		    		<cfset temp = {
	    				CREATED = LSDateFormat(getCustomer.Created_dt , 'mm/dd/yyyy') & ' ' & LSTimeFormat(getCustomer.Created_dt , 'hh:mm:ss tt'),
	    				PHONE = getCustomer.ContactString_vch,
	    				ANSWERLIST = answerList
		    		}/>

		    		<cfset ArrayAppend(dataout['DATALIST'],temp)>

    			</cfloop>
	    		
	    		<cfset dataout.RXRESULTCODE = 1/>
	    		<cfset dataout.MESSAGE = "Get list successfully!"/>
	    	</cfif>
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>
    	
    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetSingleYesNoResponseForReport" access="public" output="false" hint="Get single yes no campaign report">
    	<cfargument name="inpBatchId" required="true" hint="Batch id to get report"/>
		<cfargument name="inpCPID" required="false" default="4"/>
    	<cfargument name="inpQID" required="false" default="14"/>
    
    	<cfset var dataout = {}/>
    	<cfset dataout.RXRESULTCODE = -1/>
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.TYPE = "" />
    	<cfset dataout['DATALIST'] = []/>
    	<cfset var item = structNew()/>
    	<cfset var getListResponse = '' />
    
    	<cftry>
			<cfquery name="getListResponse" datasource="#Session.DBSourceREAD#">
    			SELECT
    				Response_vch,
    				Created_dt,
    				ContactString_vch
    			FROM
    				simplexresults.ireresults
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
    			AND
    				IREType_int = 2
    			AND
    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCPID#">
    			AND
    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpQID#">
    			AND
    				LENGTH(ContactString_vch) < 14
    			AND
    				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
    		</cfquery>

    		<cfloop query="getListResponse">
    			<cfset item = {
    				"RESPONSE": Response_vch,
    				"CREATED": LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt'),
    				"CONTACTSTRING": ContactString_vch
    			}/>
    			<cfset arrayAppend(dataout["DATALIST"], item)/>
    		</cfloop>
    		<cfset dataout.RXRESULTCODE = 1/>
    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
    			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
    			<cfset dataout.TYPE = cfcatch.TYPE />
    		</cfcatch>
    	</cftry>
    
    	<cfreturn dataout/>
    </cffunction>

    <cffunction name="GetPaymentTransactionList" access="remote" hint="Get payment transaction list">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default="">
		
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="DESC">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_3" default="-1">
		<cfargument name="sSortDir_3" default="">
		<cfargument name="iSortCol_4" default="-1">
		<cfargument name="sSortDir_4" default="">


        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout["DATALIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />
		<cfset var adddatarow = 1/>
		<cfset var order = ""/>

        <cfset var getTotal = '' />
		<cfset var order = '' />
        <cfset var getList = '' />
        <cfset var getAmount = '' />
        <cfset var getTransId = '' />
        <cfset var getCCNumber = '' />
        <cfset var filecontentParseResult = {} />
        <cfset var filterItem = {}/>
        <cfset var getCCResult = ''/>
        <cfset var sSortDir_0 = 'DESC'/>

        <cfset var myXMLDocument = ''/>
        <cfset var transactionId = ''/>
        <cfset var processorResponse = ''/>
        <cfset var paymentdataXML = ''/>
        <cfset var orderAmount = ''/>
		<cfset var getGW = ''/>
		<cfset var cardNumber=''>
		<cfset var rsCount=''>

		<!--- Order by --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "lpw.userId_int"/>
			    </cfcase> 
			    <cfcase value="4"> 
			       <cfset order = "lpw.created" />
			    </cfcase> 
			    <cfdefaultcase> 
			 		<cfset order = "lpw.created"/> 
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "lpw.created"/>
		</cfif>

        <cftry>
           <!---  <cfquery name="getTotal" datasource="#Session.DBSourceEBM#">
                SELECT
                   	COUNT(lpw.id) 
				AS 
					TOTALCOUNT
                FROM
                    simplebilling.log_payment_worldpay as lpw
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    lpw.userId_int = ua.UserId_int
                WHERE
                    lpw.UserId_int > 0                
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.NAME EQ 'amount'>

						<cfelseif filterItem.NAME EQ 'transid'>
								AND
									lpw.filecontent LIKE ('%"transactionId":#filterItem.VALUE#%')
						<cfelseif filterItem.NAME EQ 'cc'>
								AND
									lpw.filecontent LIKE ('%"cardNumber":"XXXXXXXXXXXX #filterItem.VALUE#"%')
						<cfelseif filterItem.NAME EQ 'status'>
								<cfif filterItem.VALUE NEQ 'ALL'>
									<cfif filterItem.OPERATOR EQ '<>'>
										-- AND	lpw.filecontent NOT LIKE ('%"result":"#filterItem.VALUE#"%')
										AND	lpw.status_text <> '#filterItem.VALUE#'
									<cfelse>
										-- AND	lpw.filecontent LIKE ('%"result":"#filterItem.VALUE#"%')
										AND	lpw.status_text = '#filterItem.VALUE#'
									</cfif>
								</cfif>
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
                    </cfloop>
                </cfif>                
                AND lpw.status_text <> '' -- Only get payment has status
            </cfquery>
			<cfif getTotal.RECORDCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = getTotal.TOTALCOUNT >
				<cfset dataout["iTotalDisplayRecords"] = getTotal.TOTALCOUNT >
			</cfif> --->
            <cfquery name="getList" datasource="#Session.DBSourceEBM#">
                SELECT
					SQL_CALC_FOUND_ROWS
                    lpw.id,
                    lpw.userId_int,
                    ua.EmailAddress_vch,
					ua.FirstName_vch,
					ua.LastName_vch,
                    lpw.moduleName,
                    lpw.filecontent,
                    lpw.created,
					lpw.paymentGateway_ti,
					lpw.paymentdata,
					lpw.status_text
                FROM
                    simplebilling.log_payment_worldpay as lpw
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    lpw.userId_int = ua.UserId_int
                WHERE
                    lpw.UserId_int > 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'amount'>

						<cfelseif filterItem.NAME EQ 'transid'>
							AND
								lpw.filecontent LIKE ('%"transactionId":#filterItem.VALUE#%')
                        <cfelseif filterItem.NAME EQ 'cc'>
							AND
								lpw.filecontent LIKE ('%"cardNumber":"XXXXXXXXXXXX #filterItem.VALUE#"%')
						<cfelseif filterItem.NAME EQ 'status'>
							<cfif filterItem.VALUE NEQ 'ALL'>
								<cfif filterItem.OPERATOR EQ '<>'>
									-- AND	lpw.filecontent NOT LIKE ('%"result":"#filterItem.VALUE#"%')
									AND	lpw.status_text <> '#filterItem.VALUE#'
								<cfelse>
									-- AND	lpw.filecontent LIKE ('%"result":"#filterItem.VALUE#"%')
									AND	lpw.status_text = '#filterItem.VALUE#'
								</cfif>
							</cfif>
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                
                AND lpw.status_text <> '' -- Only get payment has status

                ORDER BY
                   #order# #sSortDir_0#
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">

            </cfquery>
			<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			
            <cfif getList.RECORDCOUNT GT 0>	
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>
                <cfloop query="getList">
					<cfset adddatarow = 1/>
                    
					<cfif paymentGateway_ti EQ 2>
						<cfset getGW = "Paypal">
						<cfset getAmount = '0'/>
						<cfset getTransId = ''/>
						<cfset getCCNumber = ''/>
						<cfset getCCResult = "Payment fail"/>
						<cftry>	
							<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />	
							<cfif structKeyExists(filecontentParseResult, "state") AND filecontentParseResult.state EQ 'ok'>
								<cfset getAmount=0>
								<cfset getTransId = filecontentParseResult.id/>
								<cfset getCCResult = "ok"/>
							<cfelseif structKeyExists(filecontentParseResult, "AMT")>
								<cfset getAmount=filecontentParseResult.AMT>
								<cfset getTransId = filecontentParseResult.PNREF/>
								<cfset getCCResult = filecontentParseResult.RESPMSG />					
							<cfelse>								
								<cfset getAmount=0>	
								<cfset getTransId = filecontentParseResult.PNREF/>
								<cfset getCCResult = filecontentParseResult.RESPMSG />												
							</cfif>																	
							
							<cfset getCCNumber = ""/>
							<cfif structKeyExists(deserializeJSON(paymentdata), "NUMBER")>
								<cfset getCCNumber = deserializeJSON(paymentdata).NUMBER/>
							</cfif>
						<cfcatch></cfcatch>
						</cftry>
					<cfelseif paymentGateway_ti EQ 4>
						<cfset getGW = "TotalApps">
						<cfset getAmount = '0'/>
						<cfset getTransId = ''/>
						<cfset getCCNumber = ''/>
						<cfset getCCResult = "Payment fail"/>	
						<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="filecontentParseResult">
							<cfinvokeargument name="QueryString" value="#filecontent#">
						</cfinvoke>	
						<cfif structKeyExists(deserializeJSON(paymentdata), "INPAMOUNT")>
							<cfset getAmount=  deserializeJSON(paymentdata).INPAMOUNT >	
						</cfif>
						<cfset getTransId = filecontentParseResult.transactionid/>
						<cfset getCCResult = filecontentParseResult.responsetext />	
						<cfif structKeyExists(deserializeJSON(paymentdata), "INPNUMBER")>
							<cfset getCCNumber = deserializeJSON(paymentdata).INPNUMBER/>																																								
						</cfif>
						
					<cfelseif paymentGateway_ti EQ 3>
							<cfset getGW = "Mojo">						
							<cfset getAmount = '0'/>
							<cfset getTransId = ''/>
							<cfset getCCNumber = ''/>
							<cfset getCCResult = "Payment fail"/>							

							<cfset myXMLDocument = XmlParse(filecontent)> 
							<cfset transactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>							
							<cfif ArrayLen(transactionId) GT 0>			
								<cfset getTransId = transactionId[1].XmlText/>    							
							</cfif>	

							<cfset processorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>							
							<cfif ArrayLen(processorResponse) GT 0>			
								<cfset getCCResult = processorResponse[1].XmlText/>    														
							</cfif>		
							<cfset paymentdataXML = XmlParse(deserializeJSON(paymentdata))> 
							<cfset orderAmount = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/orderAmount")>
							<cfif ArrayLen(orderAmount) GT 0>			
								<cfset getAmount = orderAmount[1].XmlText/>      
							</cfif>	

							<cfset cardNumber = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/card/cardNumber")>
							<cfif ArrayLen(cardNumber) GT 0>			
								<cfset getCCNumber = cardNumber[1].XmlText/>      
							</cfif>

							
					<cfelse>
						<cfset getGW = "Worldpay">
						<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />
						<cfset getAmount = filecontentParseResult.transaction.authorizedAmount/>
							<cfif customFilter NEQ "">
								<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
									<cfif filterItem.NAME EQ "amount">
										<cfif filterItem.OPERATOR EQ "=">
											<cfif LSParseNumber(getAmount)  NEQ LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelseif filterItem.OPERATOR EQ "<>">
											<cfif LSParseNumber(getAmount)  EQ LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelseif filterItem.OPERATOR EQ ">">
											<cfif LSParseNumber(getAmount)  LT LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelse>
											<cfif LSParseNumber(getAmount)  GT LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										</cfif>
									</cfif>
								</cfloop>
							</cfif>
							<cfset getTransId = filecontentParseResult.transaction.transactionId/>
							<cfif NOT isNull(filecontentParseResult.transaction.cardNumber)>
								<cfset getCCNumber = Right(filecontentParseResult.transaction.cardNumber, 4)/>
							<cfelse>
								<cfset getCCNumber = ''/>
							</cfif>
							<cfset getCCResult = filecontentParseResult.result />
					</cfif>
					<cfif adddatarow EQ 1>
						
						<cfset getAmount= "$" & numberFormat(replace(getAmount, "$", "","all"),'.99')>
						<cfset tempItem = {
								ID = '#getList.id#',
								USERID = '#getList.userId_int#',
								FIRSTNAME = '#getList.FirstName_vch#',
								LASTNAME = '#getList.LastName_vch#',
								EMAIL = '#getList.EmailAddress_vch#',
								CREATED = '#DateFormat(getList.created, 'mm-dd-yyyy')#',
								MODULE = '#getList.moduleName#',
								// STATUS = '#getCCResult#',
								STATUS = '#getList.status_text#',
								AMOUNT = '#getAmount#',
								TRANSID = '#getTransId#',
								CCNUMBER = '#getCCNumber#',
								GATEWAY = '#getGW#',
								JSONCONTENT = '#getList.filecontent#'
							} 
						/>
                    	<cfset ArrayAppend(dataout["DATALIST"],tempItem)>
					</cfif>
                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get report successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch> 
        </cftry>

        <cfreturn dataout />
    </cffunction>    
	 <cffunction name="GetPaymentTransactionListByAdminAction" access="remote" hint="Get list payment transaction by list admin's action">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="inpUserId" TYPE="numeric" required="yes" />
		
        <cfargument name="customFilter" default="">
		
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="DESC">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_3" default="-1">
		<cfargument name="sSortDir_3" default="">
		<cfargument name="iSortCol_4" default="-1">
		<cfargument name="sSortDir_4" default="">


        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout["DATALIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />
		<cfset var adddatarow = 1/>
		<cfset var order = ""/>

        <cfset var getTotal = '' />
		<cfset var order = '' />
        <cfset var getList = '' />
        <cfset var getAmount = '' />
        <cfset var getTransId = '' />
        <cfset var getCCNumber = '' />
        <cfset var filecontentParseResult = {} />
        <cfset var filterItem = {}/>
        <cfset var getCCResult = ''/>
        <cfset var sSortDir_0 = arguments.sSortDir_0/>

        <cfset var myXMLDocument = ''/>
        <cfset var transactionId = ''/>
        <cfset var processorResponse = ''/>
        <cfset var paymentdataXML = ''/>
        <cfset var orderAmount = ''/>
		<cfset var vaultId = ''/>
		<cfset var rsCount = ''/>

		<!--- Order by --->
		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "lpw.userId_int"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "lpw.created" />
			    </cfcase> 

				<cfcase value="6"> 
			       <cfset order = "lpw.paymentGateway_ti" />
			    </cfcase>
			    <cfdefaultcase> 
			 		<cfset order = "lpw.created"/> 
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "lpw.created"/>
		</cfif>

        <cftry>
           
            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
					SQL_CALC_FOUND_ROWS
                    lpw.id,
                    lpw.userId_int,
                    ua.EmailAddress_vch,
					ua.FirstName_vch,
					ua.LastName_vch,
                    lpw.moduleName,
                    lpw.filecontent,
                    lpw.created,
					lpw.paymentGateway_ti,
					lpw.paymentdata
                FROM
                    simplebilling.log_payment_worldpay as lpw
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    lpw.userId_int = ua.UserId_int
                WHERE
                    lpw.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'amount'>

						<cfelseif filterItem.NAME EQ 'transid'>
							AND
								lpw.filecontent LIKE ('%"transactionId":#filterItem.VALUE#%')
                        <cfelseif filterItem.NAME EQ 'cc'>
							AND
								lpw.filecontent LIKE ('%"cardNumber":"XXXXXXXXXXXX #filterItem.VALUE#"%')
						<cfelseif filterItem.NAME EQ 'status'>
							<cfif filterItem.VALUE NEQ 'ALL'>
								<cfif filterItem.OPERATOR EQ '<>'>
									AND	lpw.filecontent NOT LIKE ('%"result":"#filterItem.VALUE#"%')
								<cfelse>
									AND	lpw.filecontent LIKE ('%"result":"#filterItem.VALUE#"%')
								</cfif>
							</cfif>
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
				
                ORDER BY
                   #order# #sSortDir_0#
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">

            </cfquery>
			<cfquery datasource="#Session.DBSourceREAD#" name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>
            <cfif getList.RECORDCOUNT GT 0>	

                <cfloop query="getList">
					<cfset adddatarow = 1/>
                    
					<cfif paymentGateway_ti EQ 2>
						<cfset filecontentParseResult = deserializeJSON(filecontent) />	
						
						<cfif structKeyExists(filecontentParseResult, "state") AND filecontentParseResult.state EQ 'ok'>
							<cfset getAmount=0>
							<cfset getTransId = filecontentParseResult.id/>
							<cfset getCCResult = "ok"/>

						<cfelseif structKeyExists(filecontentParseResult, "AMT")>
							<cfset getAmount=filecontentParseResult.AMT>														
													
						<cfelse>								
							<cfset getAmount=0>															
						</cfif>	
						<cfif structKeyExists(filecontentParseResult, "PNREF")>
							<cfset getTransId = filecontentParseResult.PNREF/>
						</cfif>		
						<cfif structKeyExists(filecontentParseResult, "RESPMSG")>							
							<cfset getCCResult = filecontentParseResult.RESPMSG />
						</cfif>																
						
						<cfset getCCNumber = ""/>
						
						<cfif structKeyExists(filecontentParseResult, "PNREF")>
							<cfset vaultId = filecontentParseResult.PNREF/>
						</cfif>	
						
					<cfelseif paymentGateway_ti EQ 3>						
							<cfset getAmount = '0'/>
							<cfset getTransId = ''/>
							<cfset getCCNumber = ''/>
							<cfset getCCResult = "Payment fail"/>							

							<cfset myXMLDocument = XmlParse(filecontent)> 
							<cfset transactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>							
							<cfif ArrayLen(transactionId) GT 0>			
								<cfset getTransId = transactionId[1].XmlText/>    							
							</cfif>	

							<cfset processorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>							
							<cfif ArrayLen(processorResponse) GT 0>			
								<cfset getCCResult = processorResponse[1].XmlText/>    														
							</cfif>		
							<cfset paymentdataXML = XmlParse(deserializeJSON(paymentdata))> 
							<cfset orderAmount = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/orderAmount")>
							<cfif ArrayLen(orderAmount) GT 0>			
								<cfset getAmount = orderAmount[1].XmlText/>      
							</cfif>	

							<cfset vaultId = XmlSearch(myXMLDocument, "/UAPIResponse/token")>							
							<cfif ArrayLen(vaultId) GT 0>			
								<cfset vaultId = vaultId[1].XmlText/>    							
							<cfelse>
								
								<cfset vaultId = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/cardToken")>
								<cfif ArrayLen(vaultId) GT 0>			
									<cfset vaultId = vaultId[1].XmlText/>      
								</cfif>	
							</cfif>	
					<cfelseif paymentGateway_ti EQ 4>
						<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="filecontentParseResult">
							<cfinvokeargument name="QueryString" value="#filecontent#">
						</cfinvoke>
						
						
						<cfset getAmount= 0 >	
						<cfif structKeyExists( deserializeJSON(paymentdata), "INPAMOUNT")>
							<cfset getAmount=  deserializeJSON(paymentdata).INPAMOUNT >	
						</cfif>	
						
						<cfset getTransId = filecontentParseResult.transactionid/>
						<cfset getCCResult = filecontentParseResult.responsetext />																	
						<cfset vaultId =  "">
						<cfif structKeyExists( deserializeJSON(paymentdata), "INPCUSTOMERVAULTID")>
							<cfset vaultId =  deserializeJSON(paymentdata).INPCUSTOMERVAULTID>
						<cfelseif structKeyExists(filecontentParseResult, "customer_vault_id")>							
							<cfset vaultId =  filecontentParseResult.customer_vault_id>
						</cfif>	
						<cfset getCCNumber = ""/>		
					<cfelse>
						<cfif structKeyExists(filecontentParseResult, "transaction")>
							<cfset getAmount = filecontentParseResult.transaction.authorizedAmount/>
							<cfif customFilter NEQ "">
								<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
									<cfif filterItem.NAME EQ "amount">
										<cfif filterItem.OPERATOR EQ "=">
											<cfif LSParseNumber(getAmount)  NEQ LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelseif filterItem.OPERATOR EQ "<>">
											<cfif LSParseNumber(getAmount)  EQ LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelseif filterItem.OPERATOR EQ ">">
											<cfif LSParseNumber(getAmount)  LT LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										<cfelse>
											<cfif LSParseNumber(getAmount)  GT LSParseNumber(filterItem.VALUE)>
												<cfset adddatarow = 0/>
											</cfif>
										</cfif>
									</cfif>
								</cfloop>
							</cfif>
							<cfset getTransId = filecontentParseResult.transaction.transactionId/>
							<cfif NOT isNull(filecontentParseResult.transaction.cardNumber)>
								<cfset getCCNumber = Right(filecontentParseResult.transaction.cardNumber, 4)/>
							<cfelse>
								<cfset getCCNumber = ''/>
							</cfif>
							<cfset getCCResult = filecontentParseResult.result />
						<cfelse>
							<cfset getAmount = ''/>
							<cfset getTransId = ''/>
							<cfset getCCNumber = ''/>
							<cfset getCCResult = '' />
						</cfif>
					</cfif>
					<cfif adddatarow EQ 1>
						
						<cfset getAmount= "$" & numberFormat(replace(getAmount, "$", "","all"),'.99')>
						<cfset tempItem = {
								ID = '#getList.id#',
								USERID = '#getList.userId_int#',
								FIRSTNAME = '#getList.FirstName_vch#',
								LASTNAME = '#getList.LastName_vch#',
								EMAIL = '#getList.EmailAddress_vch#',
								CREATED = '#DateFormat(getList.created, 'mm-dd-yyyy')#',
								MODULE = '#getList.moduleName#',
								STATUS = '#getCCResult#',
								VAUTID = '#vaultId#',
								AMOUNT = '#getAmount#',
								TRANSID = '#getTransId#',
								CCNUMBER = '#getCCNumber#',
								JSONCONTENT = '#getList.filecontent#',
								GATEWAY= '#getList.paymentGateway_ti#'
							} 
						/>
                    	<cfset ArrayAppend(dataout["DATALIST"],tempItem)>
					</cfif>
                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get report successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch> 
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="GetListStatusPayment" access="remote" hint="Get list status of payment transaction">    
        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout["DATALIST"] = ArrayNew(1) />   
        <cfset var getList = ''/>         
        <cfset var tempItem = {} />
        <cftry>
            
            <cfquery name="getList" datasource="#Session.DBSourceEBM#">
                SELECT
                    distinct(status_text)                    
                FROM
                    simplebilling.log_payment_worldpay            
                WHERE status_text <> ''
                ORDER BY
                    id
                DESC
            </cfquery>

            <cfloop query="getList">
            	
                <cfset tempItem = {
                        VALUE = '#getList.status_text#',
                        TEXT = '#getList.status_text#'                            
                    }
                />
                <cfset ArrayAppend(dataout["DATALIST"],tempItem)>
            </cfloop>

            <cfif getList.RECORDCOUNT GT 0>              
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get report successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch> 
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetPaymentTransactionById" access="remote" hint="Get payment transaction by transId">
        <cfargument name="inpTransId" default="0" required="true">

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout["DATALIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />

        <cfset var getTotal = '' />
        <cfset var getList = '' />
        <cfset var getAmount = '' />
        <cfset var getTransId = '' />
        <cfset var getCCNumber = '' />
        
        <cfset var getCCFirstName = ''/>
        <cfset var getCCLastName = ''/>
        <cfset var getCCEmail = ''/>
        <cfset var getCCAddress = ''/>
        <cfset var getCCMessage = ''/>
        <cfset var getCCResult = ''/>
        <cfset var getCCAVSCode = ''/>
        <cfset var getCCAVSResult = ''/>
		<cfset var getStatus = ''/>

        <cfset var filecontentParseResult = {} />
        <cfset var userOrgInfo = {}/>
		<cfset var RevalGetUserInfoById = ''/>

		<cfset var myXMLDocument = ''/>
		<cfset var transactionId = ''/>
		<cfset var processorResponse = ''/>
		<cfset var avsResponse = ''/>
		<cfset var paymentdataXML = ''/>
		<cfset var orderAmount = ''/>
		<cfset var queryStruct	= '' />

        <cftry>
            
            <cfquery name="getList" datasource="#Session.DBSourceEBM#">
                SELECT
                    lpw.id,
                    lpw.userId_int,
                    ua.EmailAddress_vch,
                    lpw.moduleName,
                    lpw.filecontent,
                    lpw.created,
					lpw.paymentGateway_ti,
					lpw.paymentdata,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Address1_vch,
                    ua.PostalCode_vch,
                    ua.Address2_vch,
                    so.OrganizationName_vch,
                    so.OrganizationWebsite_vch
                FROM
                    simplebilling.log_payment_worldpay as lpw
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    lpw.userId_int = ua.UserId_int
                LEFT JOIN
                	simpleobjects.Organization so
                ON
                	lpw.userId_int = so.userId_int	    
                WHERE
                    lpw.UserId_int > 0
                AND
                    (lpw.filecontent LIKE ('%"transactionId":#inpTransId#%') 
						OR lpw.filecontent LIKE ('%"PAYMENTINFO_0_TRANSACTIONID":"#inpTransId#"%') 
						OR lpw.filecontent LIKE ('%"TRANSACTIONID":"#inpTransId#"%') 
						OR lpw.filecontent LIKE ('%"PNREF":"#inpTransId#"%')
						OR lpw.filecontent LIKE ('%transactionid=#inpTransId#%')
						OR lpw.filecontent LIKE ('%<transactionId>#inpTransId#</transactionId>%') )
                ORDER BY
                    lpw.created
                DESC
            </cfquery>
            <cfif getList.RECORDCOUNT GT 0>

                <cfloop query="getList">

                	<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
					<cfif paymentGateway_ti EQ 2>
						<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />
						<cfset getAmount = 0/>
						<cfset getTransId = ''/>
						<cfset getCCNumber = ''/>
						<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="RevalGetUserInfoById">
							<cfinvokeargument name="inpUserId" value="#getList.userId_int#">
						</cfinvoke>
						<cfset getCCFirstName = RevalGetUserInfoById.USERNAME/>
						<cfset getCCLastName = RevalGetUserInfoById.LASTNAME/>
						<cfset getCCEmail = RevalGetUserInfoById.FULLEMAIL/>

						<cfset getCCAddress =''/>
						<cfset getCCResult ="Payment fail"/>
						<cfset getCCMessage ="Payment fail"/>
						<cfset getCCAVSCode =''/>
						<cfset getCCAVSResult = ""/>
						<cfset getStatus = ""/>						

						<cfset getTransId = filecontentParseResult.PNREF/>    							
						<cfset getCCResult = filecontentParseResult.RESPMSG/> 
						<cfset getCCMessage =  filecontentParseResult.RESPMSG/>   
						<cfif structKeyExists(filecontentParseResult, "AVSADDR")>
							<cfset getCCAVSCode = filecontentParseResult.AVSADDR/> 
						</cfif>
						<cfif structKeyExists(filecontentParseResult, "PROCAVS")>
							<cfset getCCAVSCode = filecontentParseResult.PROCAVS/> 
						</cfif>
						<cfif structKeyExists(deserializeJSON(paymentdata),'FIRST_NAME')>
							<cfset getCCFirstName = deserializeJSON(paymentdata).FIRST_NAME/>						
						</cfif>  
						<cfif structKeyExists(deserializeJSON(paymentdata),'LAST_NAME')>
							<cfset getCCLastName = deserializeJSON(paymentdata).LAST_NAME/>						
						</cfif>  
						
						

						<cfif structKeyExists(deserializeJSON(paymentdata),'MONEY')>
							<cfset getAmount = deserializeJSON(paymentdata).MONEY/>
						<cfelse>
							<cfset getAmount = ''/>
						</cfif>  
						<cftry>
							<cfif structKeyExists(deserializeJSON(paymentdata), "BILLING_ADDRESS")>
								<cfset getCCAddress = deserializeJSON(paymentdata).BILLING_ADDRESS.LINE1 &",</br> "& deserializeJSON(paymentdata).BILLING_ADDRESS.CITY &",</br> "& deserializeJSON(paymentdata).BILLING_ADDRESS.STATE &" "& deserializeJSON(paymentdata).BILLING_ADDRESS.POSTAL_CODE &"</br> "& deserializeJSON(paymentdata).BILLING_ADDRESS.COUNTRY_CODE/> 
							</cfif>										      
							<cfcatch>
							</cfcatch>
						</cftry>
						
						<cfif structKeyExists(deserializeJSON(paymentdata), "NUMBER")>
							<cfset getCCNumber = deserializeJSON(paymentdata).NUMBER> 
						</cfif>	
						
					<cfelseif paymentGateway_ti EQ 3>	
						<cfset getAmount = 0/>
						<cfset getTransId = ''/>
						<cfset getCCNumber = ''/>
						<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="RevalGetUserInfoById">
							<cfinvokeargument name="inpUserId" value="#getList.userId_int#">
						</cfinvoke>
						<cfset getCCFirstName = RevalGetUserInfoById.USERNAME/>
						<cfset getCCLastName = RevalGetUserInfoById.LASTNAME/>
						<cfset getCCEmail = RevalGetUserInfoById.FULLEMAIL/>
						<cfset getCCAddress =''/>
						<cfset getCCResult ="Payment fail"/>
						<cfset getCCMessage ="Payment fail"/>
						<cfset getCCAVSCode =''/>
						<cfset getCCAVSResult = ""/>
						<cfset getStatus = ""/>						

						<cfset myXMLDocument = XmlParse(filecontent)> 
						<cfset transactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>							
						<cfif ArrayLen(transactionId) GT 0>			
							<cfset getTransId = transactionId[1].XmlText/>    							
						</cfif>	

						<cfset processorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>							
						<cfif ArrayLen(processorResponse) GT 0>			
							<cfset getCCResult = processorResponse[1].XmlText/>    	
							<cfset getCCMessage =processorResponse[1].XmlText/>													
						</cfif>		
						
						<cfset avsResponse = XmlSearch(myXMLDocument, "/UAPIResponse/avsResponse")>							
						<cfif ArrayLen(avsResponse) GT 0>			
							<cfset getCCAVSCode = avsResponse[1].XmlText/>    														
						</cfif>	

						<cfset paymentdataXML = XmlParse(deserializeJSON(paymentdata))> 
						<cfset orderAmount = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/orderAmount")>
						<cfif ArrayLen(orderAmount) GT 0>			
							<cfset getAmount = orderAmount[1].XmlText/>      
						</cfif>	
					<cfelseif paymentGateway_ti EQ 4>	
						
						<cfset getAmount = 0/>
						<cfset getTransId = ''/>
						<cfset getCCNumber = ''/>
						<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="RevalGetUserInfoById">
							<cfinvokeargument name="inpUserId" value="#getList.userId_int#">
						</cfinvoke>
						<cfset getCCFirstName = RevalGetUserInfoById.USERNAME/>
						<cfset getCCLastName = RevalGetUserInfoById.LASTNAME/>
						<cfset getCCEmail = RevalGetUserInfoById.FULLEMAIL/>
						<cfset getCCAddress =''/>
						<cfset getCCResult ="Payment fail"/>
						<cfset getCCMessage ="Payment fail"/>
						<cfset getCCAVSCode =''/>
						<cfset getCCAVSResult = ""/>
						<cfset getStatus = ""/>						

						<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
							<cfinvokeargument name="QueryString" value="#filecontent#">
						</cfinvoke>
						<cfset getTransId = queryStruct.transactionid/>    							
						<cfset getCCResult = queryStruct.responsetext/> 
						<cfset getCCMessage =  queryStruct.responsetext/>   														
						<cfset getCCAVSCode = queryStruct.avsresponse/> 
						<cfset getAmount = deserializeJSON(paymentdata).INPAMOUNT/>
						<cfif structKeyExists(deserializeJSON(paymentdata),'INPFIRSTNAME')>
							<cfset getCCFirstName = deserializeJSON(paymentdata).INPFIRSTNAME/>						
						</cfif>  
						<cfif structKeyExists(deserializeJSON(paymentdata),'INPLASTNAME')>
							<cfset getCCLastName = deserializeJSON(paymentdata).INPLASTNAME/>						
						</cfif>  
						
						<cftry>
							<cfif structKeyExists(deserializeJSON(paymentdata), "inpLine1")>
								<cfset getCCAddress = deserializeJSON(paymentdata).inpLine1 &",</br> "& deserializeJSON(paymentdata).inpCity &",</br> "& deserializeJSON(paymentdata).inpState &" "& deserializeJSON(paymentdata).inpPostalCode &"</br> "& deserializeJSON(paymentdata).inpCountryCode/> 
							</cfif>										      
							<cfcatch>
							</cfcatch>
						</cftry>
						<cfif structKeyExists(deserializeJSON(paymentdata), "INPNUMBER")>
							<cfset getCCNumber = deserializeJSON(paymentdata).INPNUMBER> 
						</cfif>	
						
						 		
					<cfelse>
						<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />
						<cfif filecontentParseResult.success EQ true>
							<cfset getAmount = filecontentParseResult.transaction.authorizedAmount/>
							<cfset getTransId = filecontentParseResult.transaction.transactionId/>

							<cfif NOT isNull(paymentResposeFilecontent.transaction.cardNumber)>
								<cfset getCCNumber = Right(filecontentParseResult.transaction.cardNumber, 4)/>
							<cfelse>
								<cfset getCCNumber = ''/>
							</cfif>
						


							<cfset getCCFirstName = filecontentParseResult.transaction.cardHolder_FirstName/>
							<cfset getCCLastName = filecontentParseResult.transaction.cardHolder_LastName/>
							<cfset getCCEmail = filecontentParseResult.transaction.email/>

							<cfif structKeyExists(filecontentParseResult.transaction, "billAddress")>
								<cfset getCCAddress = filecontentParseResult.transaction.billAddress.line1/>	
								<cfset getCCAddress &= ',' &filecontentParseResult.transaction.billAddress.city/>	
								<cfset getCCAddress &= ',' &filecontentParseResult.transaction.billAddress.state/>	
								<!--- <cfset getCCAddress &= ',' &filecontentParseResult.transaction.billAddress.state/> --->
							</cfif>

							<cfset getCCResult = filecontentParseResult.result/>
							<cfset getCCMessage = filecontentParseResult.message/>
							<cfset getCCAVSCode = filecontentParseResult.transaction.avsCode/>
							<cfset getCCAVSResult = filecontentParseResult.transaction.avsResult/>
							<cfset getStatus = filecontentParseResult.result/>
						</cfif>
					</cfif>
                    <cfset tempItem = {
                            ID = '#getList.id#',
                            USERID = '#getList.userId_int#',
                            EMAIL = '#getList.EmailAddress_vch#',
                            CREATED = '#DateFormat(getList.created, 'mm-dd-yyyy')#',
                            MODULE = '#getList.moduleName#',
                            STATUS = '#getstatus#',
                            AMOUNT = '#getAmount#',
                            TRANSID = '#getTransId#',
                            CCNUMBER = '#getCCNumber#',
                            JSONCONTENT = '#getList.filecontent#',
                            PHONENUMBER = "#getList.MFAContactString_vch#",
                            FIRSTNAME = "#getList.FirstName_vch#",
                            LASTNAME = "#getList.LastName_vch#",
                            ADDRESS = "#getList.Address1_vch#",
                            ZIPCODE = "#getList.PostalCode_vch#",
                            ORGNAME = "#getList.OrganizationName_vch#",
                            WEBSITE = "#getList.OrganizationWebsite_vch#",
                            CCFIRSTNAME = "#getCCFirstName#",
                            CCLASTNAME = "#getCCLastName#",
                            CCEMAIL = "#getCCEmail#",
                            CCADDRESS = "#getCCAddress#",
                            CCRESULT = "#getCCResult#",
                            CCMESSAGE = "#getCCMessage#",
                            CCAVSCODE = "#getCCAVSCode#",
                            CCAVSRESULT = "#getCCAVSResult#"
                        } 
                    />

                    <cfset ArrayAppend(dataout["DATALIST"],tempItem)>
                </cfloop>

                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get report successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch> 
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetCampaignOpenEndedReport1" hint="get report by campaign type = OpenEnded" access="remote">
    	<cfargument name="inpBatchId" required="yes" type="numeric" hint="CampaignId">
    	<cfargument name="inpUserId" required="no" type="numeric" hint="userId">
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpCPID" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpQID" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpIsForReport" TYPE="numeric" required="no" default="0" />

        <cfset var dataout	= {} />
       	<cfset dataout.RXRESULTCODE = -1 />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MESSAGE = "" />
	   	<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getReportData = ''/>
	   	<cfset var rsCount = '' />
	   	<cfset var countJoinedBatch = '' />
	   	<cfset var countAnsweredQuestion = '' />
	   	<cfset var dataItem = {} />
	   	<cfset var order = 0/>

        <cftry>
            <cfquery name="getReportData" datasource="#Session.DBSourceEBM#">
				SELECT
					SQL_CALC_FOUND_ROWS
					ContactString_vch,
					Response_vch,
					Created_dt,
					BatchId_bi,
					T64_ti
				FROM
					simplexresults.ireresults
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpBatchId#">
                AND
                	IREType_int = 2 
                AND	
                	LENGTH(ContactString_vch) < 14
                AND 
                	ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">
                AND 
                	QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQID#">
                AND 
                	CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPID#">

                ORDER BY Created_dt ASC
                <cfif arguments.inpIsForReport EQ 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
				</cfif>
            </cfquery>

            <cfif getReportData.RECORDCOUNT GT 0>
            	<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>
				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>
				<cfset order = 1/>
				<cfloop query="getReportData">
					<cfset dataItem = {} />
					<cfset dataItem.CONTACTSTRING = #ContactString_vch#/>
					<cfif T64_ti EQ 1>
						<cfset dataItem.RESPONSE = HTMLEditFormat( ToString( ToBinary( Response_vch), "UTF-8" ) )/>
					<cfelse>
						<cfset dataItem.RESPONSE = HTMLEditFormat(Response_vch)/>
					</cfif>
					<cfset dataItem.CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt')/>
					<cfset dataItem.ORDER = order/>
					<cfset order = order + 1/>
					<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
				</cfloop>

				<cfif arguments.inpIsForReport EQ 1>
					<cfquery name="countJoinedBatch" datasource="#Session.DBSourceEBM#">
						SELECT
							ContactString_vch
						FROM
		    				simplexresults.ireresults
		    			WHERE
		    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
		    			AND
					    	LENGTH(ContactString_vch) < 14
					    GROUP BY
					    	ContactString_vch
					</cfquery>

		            <cfquery name="countAnsweredQuestion" datasource="#Session.DBSourceEBM#">
						SELECT
							ContactString_vch
						FROM
							simplexresults.ireresults
						WHERE
							BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
						AND 
		                	QID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQID#">
		                AND 
		                	CPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPID#">
		    			AND
		    				LENGTH(ContactString_vch) < 14
		    			GROUP BY
		    				ContactString_vch
					</cfquery>
					<cfset ArrayAppend(dataout["DATALIST"],{ORDER="Answered", RESPONSE=getReportData.RECORDCOUNT, CONTACTSTRING="",CREATED=""})>

					<cfset ArrayAppend(dataout["DATALIST"],{ORDER="Skipped", RESPONSE=countJoinedBatch.RECORDCOUNT - countAnsweredQuestion.RECORDCOUNT, CONTACTSTRING="",
						CREATED=""})>
				</cfif>

				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>
            <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />              

    </cffunction>

    <cffunction name="GetCPByNewSurveyBatch" access="remote" hint="Get Control Point of the new 'Survey' batch by BatchId and Type - One Selection And Short Answer">
    	<cfargument name="inpBatchId" required="true"/>

    	<cfset var dataout = {} />
    	<cfset dataout["DATALIST"] = [] />
    	<cfset dataout.MESSAGE = 'Get CP failed' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var RetVarReadCPs = '' />
    	<cfset var CPOBJ = '' />
    	<cfset var RetVarReadCPDataById = '' />

    	<cftry>
    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
                <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
            </cfinvoke>

            <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">  

                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
                    <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                </cfinvoke>

                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ "ONESELECTION" OR RetVarReadCPDataById.CPOBJ.TYPE EQ "SHORTANSWER">
                    <cfset ArrayAppend(dataout["DATALIST"],RetVarReadCPDataById.CPOBJ)>
                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.MESSAGE = 'Get CP successfully!'/>
                </cfif>

            </cfloop>
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetDataForSurveyReport" access="remote" hint="Get data for Survey Report">
    	<cfargument name="inpBatchId" required="yes"/>
    	<!--- <cfargument name="inpQId" required="no" default=""/> --->
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="inpStartDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpEndDate" TYPE="string" required="no" default="0" />
		<cfargument name="inpIsForReport" TYPE="string" required="no" default="0" />

		<cfset var dataout = {} />
		<cfset var temp = {} />
		<cfset var temp1 = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var questionList = [] />
		<cfset var answerList = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getCPResult = ''/>
    	<cfset var questionOrder = 1/>
    	<cfset var parseVal = ''/>
    	<cfset var index = ''/>
    	<cfset var index1 = ''/>
    	<cfset var index2 = ''/>
    	<cfset var index3 = ''/>
    	<cfset var getSum = ''/>
    	<cfset var ind = ''/>
    	<cfset var RetGetCampaignReport = ''/>
    	<cfset var countJoinedBatch = ''/>
    	<cfset var countAnsweredQuestion = ''/>
    	<cfset var userOrgInfo = ''/>
    	<cfset var TOTALALLCHOOSE = 0/><!--- Numbers of customer answered a question ---> 
    	<cfset var TOTALALLCHOOSE1 = 0/><!--- Numbers of customer answered a question ---> 
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>
		<cfset var AVALREG="">
		<cfset var TotalChoice=0>
		
    	<cftry>
    		<cfinvoke method="GetCPByNewSurveyBatch" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    		</cfinvoke>

    		<cfif getCPResult.RXRESULTCODE GT 0>
    			<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
    			</cfinvoke>

    			<cfquery name="countJoinedBatch" datasource="#Session.DBSourceEBM#">
    				SELECT
    					ContactString_vch
					FROM
	    				simplexresults.ireresults
	    			WHERE
	    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
	    			AND
				    	LENGTH(ContactString_vch) < 14					
				    GROUP BY
				    	ContactString_vch					
    			</cfquery>
				
	    		<!--- Loop all One selection question in a Multiple Choice Batch --->
				
	    		<cfloop array="#getCPResult['DATALIST']#" index="index"> 
	    			<cfset TOTALALLCHOOSE = 0/>

	    			<cfif index.AF EQ 'ALPHAPAR' OR index.AF EQ 'ALPHA'>
						<cfloop from="1" to="#arrayLen(index.OPTIONS)#" index="ind" >
							<cfset answerList[#index.OPTIONS[ind].ID#] = alphaAnswerChoice[ind]>
						</cfloop>
					<cfelse>
						<cfloop from="1" to="#arrayLen(index.OPTIONS)#" index="ind" >
							<cfset answerList[#index.OPTIONS[ind].ID#] = numberAnswerChoice[ind]>
						</cfloop>
					</cfif>
					
					
	    			<cfset var valList = [] />
	    			<cfif index.TYPE EQ "ONESELECTION">
	    				<!--- Loop all Selection in one question --->						
		    			<cfloop array="#index.OPTIONS#" index="index1">
							<cfset TotalChoice =0>
		    				<cfset parseVal = index1.ID/>
							<cfset AVALREG=index1.AVALREG>
							
		    				<cfquery name="getSum" datasource="#Session.DBSourceREAD#">
				    			SELECT
				    				Response_vch
				    			FROM
				    				simplexresults.ireresults
				    			WHERE
				    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
				    			AND
				    				IREType_int = 2
				    			
				    			AND
				    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.RQ#">
				    			AND
				    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.ID#">
				    			AND
				    				LENGTH(ContactString_vch) < 14									
				    		</cfquery>
							 
							<cfloop query="getSum">								
								<cfif (REFind(AVALREG,Response_vch) GT 0 AND AVALREG NEQ "") OR Response_vch EQ answerList[#parseVal#]>
									<cfset TotalChoice ++>
								</cfif>																
							</cfloop>	
							
		    				<cfset temp1 = {
		    					TEXT = index1.TEXT,
		    					VAL = index1.ID,
		    					TOTALCHOOSE = TotalChoice,
		    					FORMAT = answerList[#parseVal#],
		    					CPID = index.RQ,
		    					QID = index.ID,
		    					REGEX = AVALREG
		    				}/>							
		    				<cfset TOTALALLCHOOSE = TOTALALLCHOOSE + temp1.TOTALCHOOSE />
		    				<cfset ArrayAppend(valList,temp1)>
		    			</cfloop>
		    			<cfif TOTALALLCHOOSE EQ 0>
		    				<cfset TOTALALLCHOOSE1 = 1>
		    			<cfelse>
		    				<cfset TOTALALLCHOOSE1 = TOTALALLCHOOSE>
		    			</cfif>
		    			<cfloop array="#valList#" index="index2">
		    				<cfset StructInsert(index2, "PERCENT", numberFormat((index2.TOTALCHOOSE/TOTALALLCHOOSE1)*100, '9.99') )/>
		    			</cfloop>

		    			<cfquery name="countAnsweredQuestion" datasource="#Session.DBSourceEBM#">
		    				SELECT
		    					ContactString_vch
		    				FROM
		    					simplexresults.ireresults
		    				WHERE
		    					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
		    				AND
			    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.RQ#">
			    			AND
			    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.ID#">
			    			AND
			    				LENGTH(ContactString_vch) < 14
			    			GROUP BY
			    				ContactString_vch
		    			</cfquery>

		    			<cfset arguments.inpIsForReport EQ 1 ? ArrayAppend(valList,{TEXT="Answered", VAL="", TOTALCHOOSE=TOTALALLCHOOSE, FORMAT="", PERCENT=""}) : ArrayAppend(valList,{TEXT="Total", VAL="", TOTALCHOOSE=TOTALALLCHOOSE, FORMAT="", PERCENT=""})>
		    			<cfif arguments.inpIsForReport EQ 1>
		    				<cfset ArrayAppend(valList,{TEXT="Skipped", VAL="", TOTALCHOOSE=countJoinedBatch.RECORDCOUNT - countAnsweredQuestion.RECORDCOUNT, FORMAT="", PERCENT=""})>
		    			</cfif>
		    			<cfset temp = {
		    				NAME = "",
		    				ALLVAL = valList,
		    				ALLCHOOSE = TOTALALLCHOOSE,
		    				SKIPPED = TOTALALLCHOOSE - countAnsweredQuestion.RECORDCOUNT,
		    				TEXT = Replace(index.TEXT, "#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#:",""),
		    				TYPE = "ONESELECTION"
			    		}/>
			    		<cfset ArrayAppend(dataout["DATALIST"],temp)>
			    	<cfelseif index.TYPE EQ "SHORTANSWER">

			    		<cfinvoke method="GetCampaignOpenEndedReport1" returnvariable="RetGetCampaignReport">
		    	        	<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
							<cfinvokeargument name="inpCPID" value="#index.RQ#"/>
			    			<cfinvokeargument name="inpQID" value="#index.ID#"/>
			    			<cfinvokeargument name="inpIsForReport" value="#arguments.inpIsForReport#"/>
		            	</cfinvoke>

		            	<cfset StructInsert(RetGetCampaignReport, "TYPE", "SHORTANSWER") />
		            	<cfset StructInsert(RetGetCampaignReport, "NAME", "") />
		            	<cfset StructInsert(RetGetCampaignReport, "TEXT", Replace(index.TEXT, "#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#:","")) />

		            	<!--- <cfif RetGetCampaignReport.RXRESULTCODE GT 0> --->
		            		<cfset ArrayAppend(dataout["DATALIST"],RetGetCampaignReport)/>
		            	<!--- </cfif> --->
	    			</cfif>
	    			
	    		</cfloop>
	    		
	    		<cfset dataout["iTotalRecords"] = arrayLen(getCPResult['DATALIST'])>
				<cfset dataout["iTotalDisplayRecords"] = arrayLen(getCPResult['DATALIST'])>

				<cfloop array="#dataout["DATALIST"]#" index="index3">
					<cfset index3.NAME = "Q" & "#questionOrder#: " & "#index3.TEXT#"/>
					<cfset questionOrder = questionOrder + 1/>
				</cfloop>

	    		<cfset dataout.RXRESULTCODE = 1/>
	    		<cfset dataout.MESSAGE = 'Get Details successfully!' />
	    	<cfelse>
	    		<cfset dataout.RXRESULTCODE = 0/>
	    		<cfset dataout.MESSAGE = 'Get Details failed!' />
	    	</cfif>
    		
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>
    	
    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetAllCustomersReponsedABatch" access="remote" hint="Get all customers responsed a batch by batch id">
    	<cfargument name="inpBatchId" required="true"/>

    	<cfset var dataout = {} />
    	<cfset var temp = {} />
    	<cfset dataout["DATALIST"] = [] />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getList = '' />
    	<cfset var orderNumb = 0 />

    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				IREREsultsId_bi,
    				IRESessionId_bi,
    				ContactString_vch,
    				Created_dt
    			FROM
    				simplexresults.ireresults
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
    			AND
    				IREType_int = 2
    			AND
    				LENGTH(ContactString_vch) < 14
    			GROUP BY
    				IRESessionId_bi
    			ORDER BY
    				Created_dt
    			ASC
    		</cfquery>

    		<cfif getList.RECORDCOUNT GT 0>
    			<cfset orderNumb = 1>
    			<cfloop query="#getList#">
    				<cfset temp = {
    					ORDER = orderNumb,
    					IRERESULTID = "#getList.IREREsultsId_bi#",
    					IRESESSIONID = "#getList.IRESessionId_bi#",
    					PHONE = "#getList.ContactString_vch#",
    					CREATED = "#getList.Created_dt#"
    				}/>
    				<cfset orderNumb = orderNumb + 1 />
    				<cfset ArrayAppend(dataout['DATALIST'],temp)>
    			</cfloop>
    		</cfif>

    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetReportDetailsForNewSurveyBatch" access="remote" hint="Get a customer answered new-Survey">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpOrder" required="true"/>

    	<cfset var dataout = {} />
		<cfset var temp = {} />
		<cfset var temp1 = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset var answerList = []/>
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.PHONE = ''/>
    	<cfset dataout.CREATED = ''/>
    	<cfset dataout.STATUS = 'INCOMPLETE'/>

    	<cfset var getCustomer = '' />
    	<cfset var getAnswerDetails = '' />
    	<cfset var index = '' />
    	<cfset var index1 = '' />
    	<cfset var index2 = '' />
    	<cfset var rsCount = '' />
    	<cfset var getCPResult = '' />
    	<cfset var userOrgInfo = '' />
    	<cfset var answerList = [] />
    	<cfset var questionTextList = [] />
    	<cfset var alphaAnswerChoice = ['A', 'B', 'C', 'D', 'E', 'F', 'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' ] />
    	<cfset var numberAnswerChoice = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26' ]/>
    	<cfset var getPhone = ''/>
    	<cfset var getIRESessionId = ''/>
    	<cfset var getCreated = ''/>
    	<cfset var avalToSearch = ''/>
    	<cfset var getAnswer = ''/>
    	<cfset var qOrder = 0/>
    	<cfset var status = ''/>
    	<cfset var getAllCustomerResult = ''/>
		<cfset var AVALREG="">
		
    	<cftry>
    		
    		<cfinvoke method="GetAllCustomersReponsedABatch" returnvariable="getAllCustomerResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    		</cfinvoke>

    		<cfloop array="#getAllCustomerResult['DATALIST']#" index="index">
    			<cfif index.ORDER EQ arguments.inpOrder>
    				<cfset getPhone = index.PHONE>
    				<cfset getIRESessionId = index.IRESESSIONID/>
    				<cfset getCreated = index.CREATED />
    				<cfbreak/>
    			</cfif>
    		</cfloop>

    		<cfset dataout.CREATED = LSDateFormat(getCreated , 'mm/dd/yyyy') & ' ' & LSTimeFormat(getCreated, 'hh:mm:ss tt')/>
    		<cfset dataout.PHONE = getPhone/>

    		<cfinvoke method="GetCPByNewSurveyBatch" returnvariable="getCPResult">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    		</cfinvoke>

    		<cfif getCPResult.RXRESULTCODE GT 0>

    			<!--- Get All Customer Answered the Survey --->
    			<cfquery name="getCustomer" datasource="#Session.DBSourceREAD#">
    				SELECT
    					SQL_CALC_FOUND_ROWS
	    				Created_dt,
	    				ContactString_vch,
	    				IRESessionId_bi,
	    				CPId_int,
	    				QID_int
	    			FROM
	    				simplexresults.ireresults
	    			WHERE
	    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
	    			AND
	    				IREType_int = 2
	    			AND
	    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].RQ#">
	    			AND
	    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCPResult['DATALIST'][1].ID#">
	    			AND
	    				ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getPhone#">
	    			AND
	    				IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getIRESessionId#">
	    			AND
	    				LENGTH(ContactString_vch) < 14
	    			LIMIT 1
    			</cfquery>

				<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
				</cfinvoke>

				<cfif getCustomer.RECORDCOUNT GT 0>
	    			<cfloop query="getCustomer">

	    				<cfset answerList = [] />
	    				<cfset qOrder = 1/>
	    				<cfloop array="#getCPResult['DATALIST']#" index="index">

			    			<cfquery name="getAnswerDetails" datasource="#Session.DBSourceREAD#">
				    			SELECT
				    				Response_vch,T64_ti
				    			FROM
				    				simplexresults.ireresults
				    			WHERE
				    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
				    			AND
				    				IREType_int = 2
				    			AND
				    				CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.RQ#">
				    			AND
				    				QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index.ID#">
				    			AND 
				    				ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#getCustomer.ContactString_vch#">
				    			AND
				    				IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCustomer.IRESessionId_bi#">
				    			AND
				    				LENGTH(ContactString_vch) < 14
				    		</cfquery>

				    		<cfif getAnswerDetails.Response_vch NEQ ''>
				    			<cfif index.TYPE EQ "SHORTANSWER">
									
									<cfif getAnswerDetails.T64_ti EQ 1>
										<cfset temp1 = {
											ANSWER = ToString( ToBinary( getAnswerDetails.Response_vch ), "UTF-8" ) ,
											QUESTION = "Q#qOrder#: " & "#index.TEXT#"
										}/>
									<cfelse>
										<cfset temp1 = {
											ANSWER = getAnswerDetails.Response_vch ,
											QUESTION = "Q#qOrder#: " & "#index.TEXT#"
										}/>
									</cfif>

						    		<cfset ArrayAppend(answerList,temp1)>
						    	<cfelseif index.TYPE EQ "ONESELECTION">
						    		<cfif index.AF EQ 'ALPHAPAR' OR index.AF EQ 'ALPHA'>										
						    			<cfset avalToSearch = ArrayFind(alphaAnswerChoice, "#UCase(getAnswerDetails.Response_vch)#")/>
						    			<cfset getAnswer = "Not match answer"/>
						    			<cfloop array="#index.OPTIONS#" index="index1">											
											<cfset AVALREG= index1.AVALREG>
						    				<cfif index1.ID EQ avalToSearch>
						    					<cfset getAnswer = index1.TEXT/>																								
						    					<cfbreak/>
											<cfelseif REFind(AVALREG,getAnswerDetails.Response_vch) GT 0 AND AVALREG NEQ "">
												<cfset getAnswer = getAnswerDetails.Response_vch />
						    				</cfif>
						    			</cfloop>
										
						    			<cfset temp1 = {
							    			ANSWER = getAnswer,
							    			QUESTION = "Q#qOrder#: " & "#index.TEXT#"
							    		}/>
							    		<cfset ArrayAppend(answerList,temp1)>
							    	<cfelse>

							    		<cfset getAnswer = "Not match answer"/>
							    		<cfloop array="#index.OPTIONS#" index="index2">
						    				<cfif index2.ID EQ getAnswerDetails.Response_vch>
						    					<cfset getAnswer = index2.TEXT/>
						    					<cfbreak/>
						    				</cfif>
						    			</cfloop>

						    			<cfset temp1 = {
							    			ANSWER = getAnswer,
							    			QUESTION = "Q#qOrder#: " & "#index.TEXT#"
							    		}/>
							    		<cfset ArrayAppend(answerList,temp1)>
						    		</cfif>
						    	</cfif>
						    	<cfset qOrder = qOrder + 1/>
				    		</cfif>
			    		</cfloop>

			    		<cfset status = "COMPLETE"/>

			    		<cfif ArrayLen(answerList) LT ArrayLen(getCPResult['DATALIST'])>
			    			<cfset status = "INCOMPLETE"/>
			    		</cfif>

			    		<cfset temp = {
		    				CREATED = LSDateFormat(getCustomer.Created_dt , 'mm/dd/yyyy') & ' ' & LSTimeFormat(getCustomer.Created_dt , 'hh:mm:ss tt'),
		    				PHONE = getCustomer.ContactString_vch,
		    				ANSWERLIST = answerList,
		    				STATUS = status
			    		}/>

			    		<cfset ArrayAppend(dataout['DATALIST'],temp)>

	    			</cfloop>
    			</cfif>
	    		
	    		<cfset dataout.RXRESULTCODE = 1/>
	    		<cfset dataout.MESSAGE = "Get list successfully!"/>
	    	</cfif>
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "#cfcatch.type#" />
                <cfset dataout.MESSAGE = "#cfcatch.message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>
    	<!--- <cfdump var="#dataout#" abort="true"/> --->
    	
    	<cfreturn dataout />

    </cffunction>

    <cffunction name="GetReportByQuestionInCustomerSurvey" access="remote" hint="Get report by question in customer survey campaign">
    	<cfargument name="inpQuestionOrder" type="numeric" required="true"/>
    	<cfargument name="inpBatchId" type="numeric" required="true"/>

    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var getData = '' />

    	<cftry>
    		<cfinvoke method="GetDataForSurveyReport" returnvariable="getData">
    			<cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#"/>
    			<cfinvokeargument name="inpIsForReport" value="1"/>
    		</cfinvoke>

    		<cfset dataout = getData.DATALIST[arguments.inpQuestionOrder+1] />

    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>
    	
    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetOneSelectionResponseDetailList" access="remote" hint="Get one selection response question by batch id">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpFormat" required="true"/>
    	<cfargument name="inpCPID" required="true"/>
    	<cfargument name="inpQID" required="true"/>
    	<cfargument name="inpSkipPaging" default="0"/>
    	<cfargument name="customFilter" default=""/>
    	<cfargument name="inpRegex" default="" required="false"/>

    	<cfset var dataout = {} />
		<cfset dataout["DATALIST"] = [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var getList = '' />
		<cfset var rsCount = '' />
		<cfset var filterItem = '' />
		<cfset var dataItem = '' />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cftry>
	    	<cfquery name="getList" datasource="#Session.DBSourceREAD#">
				SELECT
					ContactString_vch,
					Created_dt,
					Response_vch
				FROM
					simplexresults.ireresults
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
				AND
					IREType_int = 2
				AND
					CPId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCPID#">
				AND
					QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpQID#">
				AND
					LENGTH(ContactString_vch) < 14
				<cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                    </cfloop>
                </cfif>
				ORDER BY Created_dt DESC

            	<cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
			</cfquery>

			<cfif getList.RECORDCOUNT GT 0>

				<cfloop query="getList">
					<cfif (REFind(arguments.inpRegex,Response_vch) GT 0 AND arguments.inpRegex NEQ "") OR Response_vch EQ arguments.inpFormat>
						<cfset dataItem = {} />
						<cfset dataItem.PHONE = #ContactString_vch#/>
						<cfset dataItem.CREATED = LSDateFormat(Created_dt, 'mm/dd/yyyy') & ' ' & LSTimeFormat(Created_dt, 'hh:mm:ss tt')/>
						<cfset ArrayAppend(dataout["DATALIST"],dataItem)>
					</cfif>
					
				</cfloop>

				<cfset dataout["iTotalDisplayRecords"] = len(dataout["DATALIST"])>
				<cfset dataout["iTotalRecords"] = len(dataout["DATALIST"])>
				<cfset dataout.RXRESULTCODE = 1 />
            </cfif>

			<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="TestFailPayment" access="remote" hint="Test fail payment">
    	<cfargument name="inpF" required="true"/>
    	<cfargument name="inpFU" required="true"/>

    	<cfset var dataout = {}/>
    	<cfset dataout['DATALIST'] = [] />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />

		<cfset var aamount = ''/>
		<cfset var paymentStatus = ''/>
		<cfset var Reason = ''/>
		<cfset var paymentResposeFilecontent = ''/>
		<cfset var AccountID = ''/>
		<cfset var Email = ''/>
		<cfset var Plan = ''/>
		<cfset var PlanID = ''/>
		<cfset var paymentDataSaved = ''/>
		<cfset var CreditCardNumber = ''/>
		<cfset var DueDate = ''/>
		<cfset var PaymentDate = ''/>
		<cfset var Amount = ''/>
		<cfset var tempItem = ''/>
		<cfset var GetEMS = ''/>

    	<cftry>
	    	<!--- Get ems data --->     
		    <cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
		        SELECT 
		            l.id,
		            l.created,
		            l.userId_int,
		            p.PlanName_vch,
		            up.UserPlanId_bi,
		            p.PlanId_int,
		            u.EmailAddress_vch,
		            l.filecontent,
		            l.status_text,
		            l.paymentdata
		        FROM 
		            simplebilling.log_payment_worldpay l 
		        INNER JOIN 
		            simpleobjects.useraccount u
		        ON
		            u.UserId_int = l.userId_int
		        INNER JOIN  
		            simplebilling.userplans up 
		        ON 
		            up.UserId_int = u.UserId_int
		        AND 
		            up.PlanId_int > 0
		        AND
		            up.Status_int = 1
		        INNER JOIN 
		            simplebilling.plans p 
		        ON 
		            up.PlanId_int = p.PlanId_int
		        WHERE 
		            l.created >= DATE_ADD(NOW(), INTERVAL - #arguments.inpF# #arguments.inpFU#)
		            AND l.created < NOW()
		        ORDER BY 
		            l.id 
		        DESC
		    </cfquery>
		    <!--- <cfdump var="#GetEMS#" abort="true"/> --->
		    <cfloop query="GetEMS"> 
		        <cfif isJson(GetEMS.filecontent)>
		            <!--- <cfif isJson(GetEMS.filecontent) > --->
		                
		            <!--- <cfelse>
		                <cfset aamount = 0/>
		                <cfset paymentStatus = GetEMS.PaymentData_txt/>
		                <cfset Reason = GetEMS.PaymentResponse_txt/>
		            </cfif> --->
		            <cfset paymentResposeFilecontent = deserializeJSON(GetEMS.filecontent)/>
		            <cfif paymentResposeFilecontent.result NEQ 'APPROVED'>
		                <cfset AccountID = GetEMS.UserId_int/>
		                <cfset Email = GetEMS.EmailAddress_vch/>

		                <cfset Plan = GetEMS.PlanName_vch/>
		                <cfset PlanID = GetEMS.PlanId_int/>

		                
		                <cfif !paymentResposeFilecontent.success>
		                    <cfset paymentStatus = paymentResposeFilecontent.result/>
		                    <cfset Reason = paymentResposeFilecontent.message/>
		                </cfif>

		                <cfif GetEMS.status_text NEQ 'OK'>
		                    <cfset aamount = 0/>
		                <cfelseif paymentResposeFilecontent.success EQ 'true'>
		                    <cfset aamount = paymentResposeFilecontent.transaction.authorizedAmount/>
		                <cfelse>
		                    <cfset aamount = 0/>
		                </cfif>
		                
		                <cfif isJson(GetEMS.paymentdata)>
		                    <cfset paymentDataSaved = deserializeJSON(GetEMS.paymentdata)>
		                    <cfset aamount = paymentDataSaved.AMOUNT/>
		                </cfif>

		                <cfif NOT isNull(paymentResposeFilecontent.transaction.cardNumber)>
		                    <cfset CreditCardNumber = right(paymentResposeFilecontent.transaction.cardNumber, 4)/>
		                <cfelse>
		                    <cfif isJson(GetEMS.paymentdata)>
		                        <cfset paymentDataSaved = deserializeJSON(GetEMS.paymentdata)>
		                        <cfset CreditCardNumber = paymentDataSaved.CARD.NUMBER/>
		                    <cfelse>
		                        <cfset CreditCardNumber = ''/>
		                    </cfif>
		                </cfif>
		                
		                <cfset DueDate = GetEMS.created/>
		                <cfset PaymentDate =  DateTimeFormat(GetEMS.created,'yyyy-mm-dd h:m:s') />
		                
		                <cfset Amount = '$' & aamount/>
		                <cfset tempItem = {
		                    ID = '#GetEMS.id#',
		                    ACCOUNT_ID = "#AccountID#",
		                    EMAIL = "#Email#",
		                    PLAN = "#Plan#",
		                    PLAN_ID = "#PlanID#",
		                    CREDIT_CARD_NUMBER = "#CreditCardNumber#",
		                    DUE_DATE = "#DueDate#",
		                    PAYMENT_DATE = "#PaymentDate#",
		                    AMOUNT = "#Amount#",
		                    PAYMENTSTATUS = "#paymentStatus#",
		                    REASON = "#Reason#"
		                }>      
		                <cfset ArrayAppend(dataout["DATALIST"],tempItem)>
		            </cfif>
		            
		            
		        <cfelse>

		        </cfif>
		    </cfloop>
		    <cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfdump var="#dataout#" abort="true"/>
		<cfreturn dataout/>
    </cffunction>
</cfcomponent>
