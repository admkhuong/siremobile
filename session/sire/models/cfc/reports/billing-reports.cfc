<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="Form_BillingInvoice" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="1" hint="The User Id" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="Not Used" />
		<cfargument name="inpcustomdata3" TYPE="any" required="no" default="0" hint="Not Used" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="Not Used" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Not Used" />
	
	    <cfset var SCMBbyUser	= '' />
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var LimitItem	= '' />
	    <cfset var getShortCodeRequestByUser = '' />
	    <cfset var outPutToDisplay	= '' />
	    <cfset var DefaultshortCode	= '' />
	    <cfset var getUser = '' />
	    <cfset var RetVarGetAdminPermission = '' />
	    
	    
	    <cfif LEN(trim(arguments.inpcustomdata4)) EQ 0 >
	    
			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="DefaultshortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="DefaultshortCode"></cfinvoke>
	  
			<cfset arguments.inpcustomdata4 = DefaultshortCode.SHORTCODE/>
	  	</cfif>
	  
		<cfsavecontent variable="outPutToDisplay">
	       		<style >
	       			.right-input > input{
	       				width:96%;
	       				margin-bottom:0;
	       				height:100%;
	       			}
	       		</style>
	        	<!--- Any scripts you place here MUST be quadrant specific --->
	        	<script type="text/javascript">
					$(function(){	
			            			            
			           						
						$("#inpUserIds<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );
						$("#inpCostCalculator<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );
						
					});
					 
					
				</script>
			
	        <cfoutput>
		        
		        
		       <!--- Verify admin user --->  
			   <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>	
    
			   <!----- used when login using username/email and password ---->
                <CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
                    
                    SELECT 
                        UserId_int,
                        PrimaryPhoneStr_vch,
                        EmailAddress_vch,
                        FirstName_vch,
                        LastName_vch
                    FROM 
                        simpleobjects.useraccount
                    WHERE
                        Active_int = 1
                        					                    					                        
                </CFQUERY>
	        	                         
				<div class="FormContainerX" id="FormContainerX_#inpChartPostion#">                	
	                	
					<!--- Only display list of short codes assigned to current user--->
	                <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
	                <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
	                  
	                   
	                <div class="head-EMS"><div class="DashObjHeaderText">User Billing Report</div></div>
	           
	                <input type="hidden" id="keywordText#inpChartPostion#" name="keywordText#inpChartPostion#" value="#inpcustomdata2#">
	                    
	                <div class="row">
	                                
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                           
	                       <label for="inpUserIds#inpChartPostion#">User Id</label> 
	                       <select name="inpUserIds#inpChartPostion#" id="inpUserIds#inpChartPostion#" size="5" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
						
								<cfif getUser.RecordCount GT 0>
							    
							    	<cfloop query="getUser">
							         	<cfoutput><option value="#getUser.UserId_int#" style="" <cfif getUser.UserId_int EQ inpcustomdata1>selected</cfif> <cfif getUser.UserId_int EQ Session.UserId>selected</cfif>   >#getUser.UserId_int# - #getUser.EmailAddress_vch#</option></cfoutput>
									</cfloop>
								
								<cfelse>
								
									<option value="0" style="">0 - No Users Found!</option>
								
								</cfif>	 
				    
						    </select>
	                    
	                    </div>
	                   
	                   
	                    <div class="col-md-12 col-sm-12 col-xs-12">
	                           
	                       <label for="inpCostCalculator#inpChartPostion#">Show Costs - <b style="color:##FF1100;">WARNING! Do NOT send to anyone!!!! EVER!</b></label> 
	                       <select name="inpCostCalculator#inpChartPostion#" id="inpCostCalculator#inpChartPostion#" size="2" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
						
						   		<option value="0" style="" <cfif 0 EQ inpcustomdata2 OR '' EQ inpcustomdata2>selected</cfif> >No</option>
						   		<option value="1" style="" <cfif 1 EQ inpcustomdata2>selected</cfif> >Yes</option>
						
						    </select>
	                    
	                    </div>
	                     
	                </div>
	                              
	                <div class="row">
	                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
	                    <div class="inputbox-container" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'billing-reports.display_BillingInvoice', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpUserIds#inpChartPostion#').val(), inpcustomdata2: $('##inpCostCalculator#inpChartPostion#').val(), inpcustomdata3: '', inpcustomdata4: '', inpcustomdata5: '' }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'billing-reports.display_BillingInvoice', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpUserIds#inpChartPostion#').val(), inpcustomdata2: $('##inpCostCalculator#inpChartPostion#').val(), inpcustomdata3: '', inpcustomdata4: '', inpcustomdata5: ''}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Details!" style="line-height:18px;"><img style="border:0; float:left;" src="/public/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Billing Report</span></a>                       
	                    </div>
	               	</div>
	               	
	               	                                       
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	   
	    <cfreturn outPutToDisplay />
	</cffunction>
	
	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="display_BillingInvoice" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="0" hint="The User Id" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="0" hint="Show Costs Flag" />
		<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="Not Used" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="Not Used" />
	 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Not Used" />
	   
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var outPutToDisplay	= '' />
	    <cfset var getCPforbatch = '' />
	    <cfset var hasData = false />
	    <cfset var node	= '' />
		<cfset var qFilteredraw	= '' />
		<cfset var qFilteredCount	= '' />
		<cfset var GetUserPlan = ''/>
		<cfset var GetNumbersCount = 0 />
		<cfset var getUser = ''/>
		<cfset var EnterpriseBillingVolume = 0/>
		<cfset var GetPlanData = '' />
		<cfset var GetPlanDataCost = '' />
		<cfset var GetNumbersCountTotal = '' />
		
		<cfset var TotalMO1 = 0/>
		<cfset var TotalMO2 = 0/>
		<cfset var TotalMO3 = 0/>
		<cfset var TotalMO4 = 0/>
		
		<cfset var TotalMT1 = 0/>
		<cfset var TotalMT2 = 0/>
		<cfset var TotalMT3 = 0/>
		<cfset var TotalMT4 = 0/>
	    
	    
	    <cfset var TotalMO1Billable = 0/>
		<cfset var TotalMO2Billable = 0/>
		<cfset var TotalMO3Billable = 0/>
		<cfset var TotalMO4Billable = 0/>
		
		<cfset var TotalMT1Billable = 0/>
		<cfset var TotalMT2Billable = 0/>
		<cfset var TotalMT3Billable = 0/>
		<cfset var TotalMT4Billable = 0/>
		
		<cfset var TotalMO1Cost = 0/>
		<cfset var TotalMO2Cost = 0/>
		<cfset var TotalMO3Cost = 0/>
		<cfset var TotalMO4Cost = 0/>
		
		<cfset var TotalMT1Cost = 0/>
		<cfset var TotalMT2Cost = 0/>
		<cfset var TotalMT3Cost = 0/>
		<cfset var TotalMT4Cost = 0/>
		
		
		<cfset var TotalAmountDue = 0 />
		
		
		<cfset arguments.inpStart = "#dateformat(arguments.inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		<cfset arguments.inpEnd = "#dateformat(arguments.inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
		    
		<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
		<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
	    
	    <!--- Get user Information  --->
        <cfquery name="getUser" datasource="#Session.DBSourceREAD#">            
            SELECT 
                UserId_int,
                PrimaryPhoneStr_vch,
                EmailAddress_vch,
                FirstName_vch,
                LastName_vch
            FROM 
                simpleobjects.useraccount
            WHERE
                userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">                					                    					                        
        </cfquery>
	    		
		<!--- Get user enterprise plan --->
		<!--- Currently 6 plans for Enterprise
			Plan1
			Plan2
			Plan3
			Plan4
			Plan5
			Plan6	
		--->
				
        <cfquery name="GetUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
				PlanId_int,
				CostPlanId_int,
				OIDCharge_dec,
				ImportListCharge_dec,
				BillEverythingSentFlag_ti,
				BillTermsDueDays_int
			FROM 
	            simplebilling.enterprise_user_billing_plan
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
	    </cfquery>
		
		
		<cfif GetUserPlan.RecordCount EQ 0>
			<!--- Default to plan 0 if none speciifed - plan 0 will not continue  --->
			<cfset GetUserPlan.PlanId_int = 0 />		        
		</cfif>
		
		<!--- Get total volume counts to see which price break the user gets  --->
		<!--- Currently 5 Price Breaks for Enterprise Plan
			0-250,000
			250,000-1,000,000
			1,000,000 to 5,000,000
			5,000,000 to 10,000,000
			10,000,000+			
		--->
		
		<!---Get total count for volume discount and sanity check --->
        <cfquery name="GetNumbersCountTotal" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
	        AND
	        	(IREType_int= 1 OR IREType_int= 2)	
	        AND
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
        </cfquery>

		<cfif GetNumbersCountTotal.TOTALCOUNT GTE 0>
			<cfset EnterpriseBillingVolume = '0'/>	
		</cfif>			

		<cfif GetNumbersCountTotal.TOTALCOUNT GT 250000>
			<cfset EnterpriseBillingVolume = '250000'/>	
		</cfif>	
		
		<cfif GetNumbersCountTotal.TOTALCOUNT GT 1000000>
			<cfset EnterpriseBillingVolume = '1000000'/>	
		</cfif>	
		
		<cfif GetNumbersCountTotal.TOTALCOUNT GT 5000000>
			<cfset EnterpriseBillingVolume = '5000000'/>	
		</cfif>	
		
		<cfif GetNumbersCountTotal.TOTALCOUNT GT 10000000>
			<cfset EnterpriseBillingVolume = '10000000'/>	
		</cfif>		
		
		<!--- Get Plan Ammounts --->
        <cfquery name="GetPlanData" datasource="#Session.DBSourceREAD#">            
			SELECT 
				PlanId_int,
			    Volume_int,
			    MO1_dec,
			    MO2_dec,
			    MO3_dec,
			    MO4_dec,
			    MT1_dec,
			    MT2_dec,
			    MT3_dec,
			    MT4_dec,
			    CommissionPercentage_int
			FROM 
				simplebilling.enterprise_billing_plan
			WHERE
				PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserPlan.PlanId_int#">
			AND
				Volume_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EnterpriseBillingVolume#">
        </cfquery>
		
		
		<!--- Get Plan Ammounts --->
        <cfquery name="GetPlanDataCost" datasource="#Session.DBSourceREAD#">            
			SELECT 
				PlanId_int,
			    Volume_int,
			    MO1_dec,
			    MO2_dec,
			    MO3_dec,
			    MO4_dec,
			    MT1_dec,
			    MT2_dec,
			    MT3_dec,
			    MT4_dec,
			    CommissionPercentage_int
			FROM 
				simplebilling.enterprise_billing_plan
			WHERE
				PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserPlan.CostPlanId_int#">
			AND
				Volume_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EnterpriseBillingVolume#">
        </cfquery>
        

<!---  Future - multi short code accounts
	        AND
	        	shortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">
--->
				
		<!--- See SELECT * FROM sms.carrierlist; for list of CarrierIds (OperatorId_int) --->
		
		
		<!--- **** Everyone Else  Verizon, AT&T & most others US --->

		<!--- Count MTs from   Verizon, AT&T & most others US  --->
		<!--- Limit to MTs --->	
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults AS IR	
	            
	            <!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
	            <cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0>            
	            	INNER JOIN simplexresults.contactresults AS CR ON (cr.MasterRXCallDetailId_int = IR.MasterRXCallDetailId_bi)
	            </cfif>
	        WHERE
	        	IR.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IR.IREType_int= 1   		       		
	        AND
	        	IR.Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            IR.Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				((IR.Carrier_vch IS NULL) OR (IR.Carrier_vch <> '31005' AND IR.Carrier_vch <> '31010' AND IR.Carrier_vch <> '31004' AND IR.Carrier_vch <> '31012'))		
			
			<!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
			<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0>  
				AND 
					CR.SMSMTPostResultCode_vch = '0'	    
			</cfif>	 
        </cfquery>
        
		<cfset TotalMT1 = GetNumbersCount.TOTALCOUNT/> 
		<cfset TotalMT1Billable = NumberFormat(TotalMT1 * GetPlanData.MT1_dec, "0.0000") /> 
		<cfset TotalMT1Cost = NumberFormat(TotalMT1 * GetPlanDataCost.MT1_dec, "0.0000") /> 
     	
		<!--- Count MOs from  Verizon, AT&T & most others US --->
		<!--- Limit to MOs --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IREType_int= 2   			       		
	        AND
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				((Carrier_vch IS NULL) OR (Carrier_vch <> '31005' AND Carrier_vch <> '31010' AND Carrier_vch <> '31004' AND Carrier_vch <> '31012')) 		    
        </cfquery>

 		<cfset TotalMO1 = GetNumbersCount.TOTALCOUNT/> 
 		<cfset TotalMO1Billable = NumberFormat(TotalMO1 * GetPlanData.MO1_dec, "0.0000") />
 		<cfset TotalMO1Cost = NumberFormat(TotalMO1 * GetPlanDataCost.MO1_dec, "0.0000") />		

		<!--- **** Sprint/Boost/Virgin Mobile--->

		<!--- Count MTs from  Sprint/Boost/Virgin Mobile  --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults AS IR	     
	            <!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
				<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0>        
	            	INNER JOIN simplexresults.contactresults AS CR ON (cr.MasterRXCallDetailId_int = IR.MasterRXCallDetailId_bi)
				</cfif>		            
	        WHERE
	        	IR.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IR.IREType_int= 1   <!--- Limit to MTs --->			       		
	        AND
	        	IR.Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            IR.Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				(IR.Carrier_vch = '31005' OR IR.Carrier_vch = '31010')		     
			<!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
			<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0> 
				AND 
					CR.SMSMTPostResultCode_vch = '0'	
			</cfif>	
        </cfquery>
        
		<cfset TotalMT2 = GetNumbersCount.TOTALCOUNT/>
		<cfset TotalMT2Billable = NumberFormat(TotalMT2 * GetPlanData.MT2_dec, "0.0000") /> 
		<cfset TotalMT2Cost = NumberFormat(TotalMT2 * GetPlanDataCost.MT2_dec, "0.0000") /> 
      	
		<!--- Count MOs from Sprint/Boost/Virgin Mobile  --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IREType_int= 2   <!--- Limit to MOs --->			       		
	        AND
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				(Carrier_vch = '31005' OR Carrier_vch = '31010')		    
        </cfquery>

 		<cfset TotalMO2 = GetNumbersCount.TOTALCOUNT/> 
 		<cfset TotalMO2Billable = NumberFormat(TotalMO2 * GetPlanData.MO2_dec, "0.0000") />
 		<cfset TotalMO2Cost = NumberFormat(TotalMO2 * GetPlanDataCost.MO2_dec, "0.0000") />
				
		<!--- **** T-Mobile/MetroPCS GSM --->

		<!--- Count MTs from  T-Mobile/MetroPCS GSM  --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults AS IR	            
	            <!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
				<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0>        
	            	INNER JOIN simplexresults.contactresults AS CR ON (cr.MasterRXCallDetailId_int = IR.MasterRXCallDetailId_bi)
				</cfif>	
	        WHERE
	        	IR.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IR.IREType_int= 1   <!--- Limit to MTs --->			       		
	        AND
	        	IR.Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            IR.Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				(IR.Carrier_vch = '31004')		
			<!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
			<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0> 
				AND 
					CR.SMSMTPostResultCode_vch = '0'	
			</cfif>	
	     
        </cfquery>
        
		<cfset TotalMT3 = GetNumbersCount.TOTALCOUNT/> 
		<cfset TotalMT3Billable = NumberFormat(TotalMT3 * GetPlanData.MT3_dec, "0.0000") />
		<cfset TotalMT3Cost = NumberFormat(TotalMT3 * GetPlanDataCost.MT3_dec, "0.0000") />
      	
		<!--- Count MOs from T-Mobile/MetroPCS GSM  --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IREType_int= 2   <!--- Limit to MOs --->			       		
	        AND
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				(Carrier_vch = '31004')		    
        </cfquery>

 		<cfset TotalMO3 = GetNumbersCount.TOTALCOUNT/> 
 		<cfset TotalMO3Billable = NumberFormat(TotalMO3 * GetPlanData.MO3_dec, "0.0000") />
 		<cfset TotalMO3Cost = NumberFormat(TotalMO3 * GetPlanDataCost.MO3_dec, "0.0000") />
		
		<!--- **** U.S. Cellular --->

		<!--- Count MTs from  U.S. Cellular MT 4 Carrier_vch = 31012  --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults AS IR	            
	            <!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
				<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0>        
	            	INNER JOIN simplexresults.contactresults AS CR ON (cr.MasterRXCallDetailId_int = IR.MasterRXCallDetailId_bi)
				</cfif>	
	        WHERE
	        	IR.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IR.IREType_int= 1   <!--- Limit to MTs --->			       		
	        AND
	        	IR.Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            IR.Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				IR.Carrier_vch = '31012'	
			<!--- If specified - only bill successful initisal send SMS results - accepted into carrier networks --->
			<cfif GetUserPlan.BillEverythingSentFlag_ti EQ 0> 
				AND 
					CR.SMSMTPostResultCode_vch = '0'	
			</cfif>	
		     
        </cfquery>
        
		<cfset TotalMT4 = GetNumbersCount.TOTALCOUNT /> 
		<cfset TotalMT4Billable = NumberFormat(TotalMT4 * GetPlanData.MT4_dec, "0.0000") />
		<cfset TotalMT4Cost = NumberFormat(TotalMT4 * GetPlanDataCost.MT4_dec, "0.0000") />
      	
		<!--- Count MOs from  U.S. Cellular MO 4 Carrier_vch = 31012 --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
            SELECT 
				 COUNT(*) AS TOTALCOUNT
			FROM 
	            simplexresults.ireresults
	        WHERE
	        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
			AND
				IREType_int= 2   <!--- Limit to MOs --->			       		
	        AND
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
			AND
				Carrier_vch = '31012'		    
        </cfquery>

 		<cfset TotalMO4 = GetNumbersCount.TOTALCOUNT/> 
 		<cfset TotalMO4Billable = NumberFormat(TotalMO4 * GetPlanData.MO4_dec, "0.0000") /> 
 		<cfset TotalMO4Cost = NumberFormat(TotalMO4 * GetPlanDataCost.MO4_dec, "0.0000") /> 

		<!--- Get total volume count --->
		
		<!--- Add logic to CDRs for total cost of each CDR --->   
	   
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>	
				<!--- Build javascript objects here - include any custom data parameters at the end --->
	        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'billing-reports.Form_BillingInvoice', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'billing-reports.Form_BillingInvoice', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	           
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for download links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS container PDFPrint">
	                <div class="head-EMS row NoPrint"><div class="DashObjHeaderText">Enterprise Billing Report for #inpcustomdata1# </div></div>
	                
	                <div class="row" style="padding: 1em; margin: 1em; border: double black ">	                   
	                   
	                   
	                   <cfif inpcustomdata2 EQ 1>
						   	
						   	<div class="col-sm-12" style="margin-top: 2em;">
							   	<h2 style="color: ##FF1100;">BREAKDOWN ONLY NOT A BILL - DO NOT SEND</h2>
						   	</div>   
	                   
	                   </cfif>
	                      	
	                   	<div class="col-sm-4">
	                   		<img src="/public/sire/images/logo_top.png" style="max-width:100%; height:auto"/>	
	                   		<BR>
	                   		4533 MacArthud Blvd Ste 1090<BR>
	                   		Newport Beach, CA 92660</BR>
	                   		Telephone: (888) 747-4411                   		
	                   	</div>             
	                   	                  
					   	<div class="col-sm-8">		
		
					   		<table style="float:right; margin-right: 1em;">
		                   		
		                   		<tr><td colspan='2'><b>Billing Cycle</b></td></tr>
		                   		
		                   		<tr>
			                   		<td style="text-align: left; width: 4em;"><b>From:</b></td>
			                   		<td style="text-align: left; padding-left: 1em;">#LSDateFormat(inpStart, 'yyyy-mm-dd')#</td>
							   		<!--- 			                   		<td style="text-align: left; padding-left: 1em;">#inpStart#</td> --->
		                   		</tr>	
		                   		
		                   		<tr>
			                   		<td style="text-align: left; width: 4em;"><b>Through:</b></td>
			                   		<td style="text-align: left; padding-left: 1em;">#LSDateFormat(inpEnd, 'yyyy-mm-dd')#</td>
							   		<!--- 			                   		<td style="text-align: left; padding-left: 1em;">#inpEnd#</td> --->
		                   		</tr>
	                   		</table>	

                   		</div>		
					   
					   	<div class="col-sm-12" style="margin-top: 3em;">
						   	
						   	
							<b>User Id:</b> #inpcustomdata1#
							<br/>	
							<b>Account Id:</b> #getUser.EmailAddress_vch#
							<br/> 
							<b>Account Phone:</b> #getUser.PrimaryPhoneStr_vch#
							<br/>  
							<b>Account Name:</b> #getUser.FirstName_vch# #getUser.LastName_vch#
							<br/>						  	
						   				
						   				
<!---
							<!--- Sanity Check --->	
							<cfif (TotalMO1 + TotalMO2 + TotalMO3 + TotalMO4) + (TotalMT1 + TotalMT2 + TotalMT3 + TotalMT4) EQ GetNumbersCountTotal.TOTALCOUNT>
								Sanity Check Passed!  (#TotalMO1 + TotalMO2 + TotalMO3 + TotalMO4#) + (#TotalMT1 + TotalMT2 + TotalMT3 + TotalMT4#) EQ #GetNumbersCountTotal.TOTALCOUNT#								
							<cfelse>
								Sanity Check Failed!  (#TotalMO1 + TotalMO2 + TotalMO3 + TotalMO4#) + (#TotalMT1 + TotalMT2 + TotalMT3 + TotalMT4#) NEQ #GetNumbersCountTotal.TOTALCOUNT#							
							</cfif>  
--->
								                 
<!---
		                  <BR>
		                  GetUserPlan.PlanId_int = #GetUserPlan.PlanId_int#
		                  </BR>      
--->           	                  
		               	
		               	</div>					   	
					   					   	
					   	<div class="col-sm-12" style="margin-top: 2em; margin-bottom: 2em;">
						   	
							<table style="width: 100%;">
			                   
			                   <tr>
				                   <th style="text-align: left;">Carrier Group</th>
				                   <th style="text-align: right;">MO Count</th>
				                   <th style="text-align: right;">MO Rate</th>				                   
				                   <th style="text-align: right;">MO Billed</th>
  			                       <th style="text-align: right;">MT Count</th>
  			                       <th style="text-align: right;">MT Rate</th>				                   
				                   <th style="text-align: right;">MT Billed</th>				                   
			                   </tr>
			                  		                   
			                   <tr>
				                   <td style="text-align: left;">Verizon, AT&T & most others US</td>  
				                   <td style="text-align: right;">#NumberFormat(TotalMO1, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MO1_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMO1Billable, ',0.0000')#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT1, ',')#</td>	
				                   <td style="text-align: right;">#GetPlanData.MT1_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT1Billable, ',0.0000')#</td>			                 			                   
			                   </tr>    
			                  		                  
			                   <tr>
				                   <td style="text-align: left;">Sprint/Boost/Virgin Mobile</td>  
				                   <td style="text-align: right;">#NumberFormat(TotalMO2, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MO2_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMO2Billable, ',0.0000')#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT2, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MT2_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT2Billable, ',0.0000')#</td>			                 			                   
			                   </tr> 
			                   
			                   <tr>
				                   <td style="text-align: left;">T-Mobile/MetroPCS GSM</td>  
				                   <td style="text-align: right;">#NumberFormat(TotalMO3, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MO3_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMO3Billable, ',0.0000')#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT3, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MT3_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT3Billable, ',0.0000')#</td>			                 			                   
			                   </tr> 
			                   
			                   <tr>
				                   <td style="text-align: left;">U.S. Cellular</td>  
				                   <td style="text-align: right;">#NumberFormat(TotalMO4, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MO4_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMO4Billable, ',0.0000')#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT4, ',')#</td>
				                   <td style="text-align: right;">#GetPlanData.MT4_dec#</td>
				                   <td style="text-align: right;">#NumberFormat(TotalMT4Billable, ',0.0000')#</td>		                 			                   
			                   </tr> 
			                   
			                   <tr>
				                   <td style="text-align: left;"><i>Summary Totals</i></td>
				                   <td style="text-align: right;">#NumberFormat(TotalMO1 + TotalMO2 + TotalMO3 + TotalMO4, ',')#</td>
								   <td style="text-align: left;">&nbsp;</td>
								   <td style="text-align: right;">$#NumberFormat(TotalMO1Billable + TotalMO2Billable + TotalMO3Billable + TotalMO4Billable, ",0.0000")#</td>
								   <td style="text-align: right;">#NumberFormat(TotalMT1 + TotalMT2 + TotalMT3 + TotalMT4, ',')#</td>
								   <td style="text-align: left;">&nbsp;</td>
								   <td style="text-align: right;">$#NumberFormat(TotalMT1Billable + TotalMT2Billable + TotalMT3Billable + TotalMT4Billable, ",0.0000")#</td>
			                   </tr>	
		                    
			                   
							</table>    
							
						
						 	<cfset TotalAmountDue = NumberFormat((TotalMO1Billable + TotalMO2Billable + TotalMO3Billable + TotalMO4Billable + TotalMT1Billable + TotalMT2Billable + TotalMT3Billable + TotalMT4Billable), ",0.00") />

					   	
					   	</div>
							
						<cfif GetUserPlan.OIDCharge_dec GT 0 >
							
							<!--- Get count imported fields --->
							<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
					            SELECT 
									 COUNT(*) AS TOTALCOUNT
								FROM 
						            simplexresults.ireresults AS IR	
						        WHERE
						        	IR.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
								AND
									(IR.IREType_int= 4 OR IR.IREType_int= 8)	       		
						        AND
						        	IR.Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						        AND
						            IR.Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">										     
					        </cfquery>
							
							
							<div class="col-sm-12" style="">
												   	
								<b>OID Fees:</b> #GetNumbersCount.TOTALCOUNT# * $#NUMBERFORMAT(GetUserPlan.OIDCharge_dec, "0.0000")# = $#NUMBERFORMAT((GetNumbersCount.TOTALCOUNT*GetUserPlan.OIDCharge_dec), "0.00")#
								<br/>	
			               	</div>	
						
						
						   	<!--- Watch for adding numbers after they have been prviously formated as strings - remove the non-numerics first using java--->												
 							<cfset TotalAmountDue = NumberFormat(VAL(TotalAmountDue.replaceAll('[^0-9\.]+','')) + (GetNumbersCount.TOTALCOUNT*GetUserPlan.OIDCharge_dec), ",0.00") /> 
 							
						</cfif>	
						
						
												
						<cfif GetUserPlan.ImportListCharge_dec GT 0>
									
									
							<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
					            SELECT 
									 COUNT(*) AS TOTALCOUNT
								FROM 
						            simplexuploadstage.uploads
						        WHERE
						        	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
								AND
									Unique_int > 0	       		
						        AND
						        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						        AND
						            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">										     
					        </cfquery>
							
							
							<div class="col-sm-12" style="">
												   	
								<b>Upload Fees:</b> #GetNumbersCount.TOTALCOUNT# * $#NUMBERFORMAT(GetUserPlan.ImportListCharge_dec, "0.00")# = $#NUMBERFORMAT((GetNumbersCount.TOTALCOUNT*GetUserPlan.ImportListCharge_dec), "0.00")#
								<br/>	
			               	</div>	
									
							<!--- Watch for adding numbers after they have been prviously formated as strings - remove the non-numerics first using java--->												
 							<cfset TotalAmountDue = NumberFormat(VAL(TotalAmountDue.replaceAll('[^0-9\.]+','')) + (GetNumbersCount.TOTALCOUNT*GetUserPlan.ImportListCharge_dec), ",0.00") /> 
																
						</cfif>	
																		   	
					   	<div class="col-sm-12" style="text-align: right; margin-top: 2em;">
						   							   						   	
						   	<b>Total Amount Due:</b> <b>$US #TotalAmountDue#</b>
					   	</div>					   	
					   	
					   	<div class="col-sm-12" style="text-align: right;">
						   	Payment Terms: #GetUserPlan.BillTermsDueDays_int# days
						   	<BR>
						   	Due #LSDateFormat(DateAdd("d", GetUserPlan.BillTermsDueDays_int, inpEnd), 'yyyy-mm-dd')#
					   	</div>   	
					   	
					   	<div class="col-sm-12" style="text-align: center;">
					   		<i>For assistance with this invoice Email: support@siremobile.com</i>
					   	</div>

					   	
					   	<cfif inpcustomdata2 EQ 1>
						   	
						   	<div class="col-sm-12" style="margin-top: 2em;">
							   	<h2 style="color: ##FF1100;">BREAKDOWN ONLY NOT A BILL - DO NOT SEND</h2>
						   	</div>   	
						   	
							<div class="col-sm-12" style="margin-top: 2em;">
						   	
								<table style="width: 100%;">
				                   
				                   <tr>
					                   <th style="text-align: left;">Carrier Group</th>
					                   <th style="text-align: right;">MO Count</th>
					                   <th style="text-align: right;">MO Rate</th>				                   
					                   <th style="text-align: right;">MO Billed</th>
	  			                       <th style="text-align: right;">MT Count</th>
	  			                       <th style="text-align: right;">MT Rate</th>				                   
					                   <th style="text-align: right;">MT Billed</th>				                   
				                   </tr>
				                  		                   
				                   <tr>
					                   <td style="text-align: left;">Verizon, AT&T & most others US</td>  
					                   <td style="text-align: right;">#TotalMO1#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MO1_dec#</td>
					                   <td style="text-align: right;">#TotalMO1Billable#</td>
					                   <td style="text-align: right;">#TotalMT1#</td>	
					                   <td style="text-align: right;">#GetPlanDataCost.MT1_dec#</td>
					                   <td style="text-align: right;">#TotalMT1Billable#</td>			                 			                   
				                   </tr>    
				                  		                  
				                   <tr>
					                   <td style="text-align: left;">Sprint/Boost/Virgin Mobile</td>  
					                   <td style="text-align: right;">#TotalMO2#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MO2_dec#</td>
					                   <td style="text-align: right;">#TotalMO2Billable#</td>
					                   <td style="text-align: right;">#TotalMT2#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MT2_dec#</td>
					                   <td style="text-align: right;">#TotalMT2Billable#</td>			                 			                   
				                   </tr> 
				                   
				                   <tr>
					                   <td style="text-align: left;">T-Mobile/MetroPCS GSM</td>  
					                   <td style="text-align: right;">#TotalMO3#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MO3_dec#</td>
					                   <td style="text-align: right;">#TotalMO3Billable#</td>
					                   <td style="text-align: right;">#TotalMT3#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MT3_dec#</td>
					                   <td style="text-align: right;">#TotalMT3Billable#</td>			                 			                   
				                   </tr> 
				                   
				                   <tr>
					                   <td style="text-align: left;">U.S. Cellular</td>  
					                   <td style="text-align: right;">#TotalMO4#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MO4_dec#</td>
					                   <td style="text-align: right;">#TotalMO4Billable#</td>
					                   <td style="text-align: right;">#TotalMT4#</td>
					                   <td style="text-align: right;">#GetPlanDataCost.MT4_dec#</td>
					                   <td style="text-align: right;">#TotalMT4Billable#</td>		                 			                   
				                   </tr> 
				                   
				                   <tr>
					                   <td style="text-align: left;"><i>Summary Totals</i></td>
					                   <td style="text-align: right;">#NumberFormat(TotalMO1 + TotalMO2 + TotalMO3 + TotalMO4, ',')#</td>
									   <td style="text-align: left;">&nbsp;</td>
									   <td style="text-align: right;">$#NumberFormat(TotalMO1Cost + TotalMO2Cost + TotalMO3Cost + TotalMO4Cost, ",0.0000")#</td>
									   <td style="text-align: right;">#NumberFormat(TotalMT1 + TotalMT2 + TotalMT3 + TotalMT4, ',')#</td>
									   <td style="text-align: left;">&nbsp;</td>
									   <td style="text-align: right;">$#NumberFormat(TotalMT1Cost + TotalMT2Cost + TotalMT3Cost + TotalMT4Cost, ",0.0000")#</td>
				                   </tr>	
			                    
				                   
								</table>    
								
								<cfset TotalAmountDue = NumberFormat((TotalMO1Cost + TotalMO2Cost + TotalMO3Cost + TotalMO4Cost + TotalMT1Cost + TotalMT2Cost + TotalMT3Cost + TotalMT4Cost), ",0.00") />
													   	
						   	</div>
	
						   
						   	
						   	<div class="col-sm-12" style="text-align: right; margin-top: 2em;">
							   	<b>Total Amount Due:</b> <b>$US #TotalAmountDue#</b>
						   	</div>
					   	
					   						   	
					   	<cfelse>
					   	
					   		<div class="col-sm-12" style="margin-top: 3em;">
							   			
								<table style="width: 100%;">
									
									<tr>
										<td style="text-align: left;"><b>Check Payable To:</b></td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Sire Investments LLC.</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>

									<tr>
										<td style="text-align: left;">533 MacArthur Blvd Ste 1090</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>

									<tr>
										<td style="text-align: left;">Newport Beach, CA 92660</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">&nbsp;</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">&nbsp;</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>
									
									<tr>
										<td colspan="2" style="text-align: left;"><b>Wire Transfer:</b></td>	
									</tr>
																			
									<tr>
										<td style="text-align: left;">Direct to Wire Routing Transit Number:</td>	
										<td style="text-align: left;">(RTN/ABA) 121000248</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">&nbsp;</td>	
										<td style="text-align: left;">&nbsp;</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">For International SWIFT/BIC code for Transfer only Wells Fargo is:</td>	
										<td style="text-align: left;">(USD payments)      WFBIUS6S</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">&nbsp;</td>	
										<td style="text-align: left;">(foreign currency payments) WFBIUS6WFFX</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Bank name:</td>	
										<td style="text-align: left;">Wells Fargo Bank, N.A.</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Bank address:</td>	
										<td style="text-align: left;">420 Montgomery, San Francisco, CA 94104</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Account Number:</td>	
										<td style="text-align: left;">3191611858</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Account Name:</td>	
										<td style="text-align: left;">SIRE INVESTMENTS LLC</td>										
									</tr>
									
									<tr>
										<td style="text-align: left;">Account Address:</td>	
										<td style="text-align: left;">1720 SAN ALVARADO CIR CORONA CA 92882</td>										
									</tr>										
																		
								</table>																	
																   	
						   	</div>
					   	
					   	</cfif>	

	                </div>					
				
                	<div style="padding:1em; font-size:12px; text-align:left; clear:both; width:100%; " class="no-print">
	                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
	                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
	                   <div class="ReportLink no-print" style="text-align: right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change User Id</div>
	                </div>
	                
	                <div class="DownloadLink pdfIcon no-print" onclick="OutputObjAsPDF($(this).parent(), false)"></div>
	                           
	        	</div>
	            
	        </cfoutput>
	                    
	 	</cfsavecontent>
	
	    <cfreturn outPutToDisplay />
	</cffunction>
	
</cfcomponent>