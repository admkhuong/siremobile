<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<cffunction name="GetOptOutListByBatch" access="remote" hint="Get list opt out by batch">
    	<cfargument name="inpBatchIdList" required="true" type="string"/>    
    	<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
		<cfargument name="customFilter" default="">
		<cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">

    	<cfset var dataout = {} />
    	<cfset var getList = '' />
    	<cfset var rsCount = '' />
    	<cfset var tmp = {} />
    	<cfset dataout['DATALIST'] = [] />
    	<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = "" />
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset var filterItem = ''/>

    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceEBM#">
    			SELECT
    				SQL_CALC_FOUND_ROWS
    				oi.ContactString_vch as Phone,    				
					DATE_FORMAT(oi.OptOut_dt,'%b %d %Y %T') AS Date,
                    b.UserId_int AS UserId,
                    b.BatchId_bi AS BatchId,
                    b.Desc_vch AS CamaignName
				FROM 
					simplelists.optinout as oi
                INNER JOIN 
                    simpleobjects.batch as b 
                ON  
                    b.BatchId_bi = oi.BatchId_bi
				WHERE 
					oi.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchIdList#"> 
				AND
					oi.OptOut_dt IS NOT NULL
				AND
					oi.OptIn_dt IS NULL							    			
    			AND
    				LENGTH(ContactString_vch) < 14
    			<cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
               	
                ORDER BY
					oi.OptOut_dt DESC
    			<cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
					LIMIT 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
					OFFSET 
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
				</cfif>
				
    		</cfquery>
			<cfif arguments.OutQueryResults EQ "1"> 
				<cfset DataOut.QUERYRES = getList />
			</cfif>
    		<cfif getList.RECORDCOUNT GT 0>
    			<cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
					SELECT FOUND_ROWS() AS iTotalRecords
				</cfquery>

				<cfloop query="rsCount">
					<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
					<cfset dataout["iTotalRecords"] = iTotalRecords>
				</cfloop>

	    		<cfloop query="#getList#">
	    			<cfset tmp = {
	    				// ID = Id_int,
	    				PHONE = Phone,
	    				DATE = Date,
                        USERID = UserId,
                        BATCHID = BatchId,
                        CAMPAIGNNAME = CamaignName          
	    			}/>

	    			<cfset arrayAppend(dataout['DATALIST'], tmp)/>
	    		</cfloop>
	    		
    		</cfif>
			<cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>
    <cffunction name="GetOptOutListByDateRange" access="remote" hint="Get a list opt out by date range">        
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
        <cfargument name="customFilter" default="">
        <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {} />
        <cfset var getList = '' />
        <cfset var rsCount = '' />
        <cfset var tmp = {} />
        <cfset dataout['DATALIST'] = [] />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var filterItem = ''/>

        <cftry>
            <cfquery name="getList" datasource="#Session.DBSourceEBM#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    oi.ContactString_vch as Phone,                 
                    DATE_FORMAT(oi.OptOut_dt,'%b %d %Y %T') AS Date,
                    b.UserId_int AS UserId,
                    b.BatchId_bi AS BatchId,
                    b.Desc_vch AS CamaignName
                FROM 
                    simplelists.optinout  AS oi 
                INNER JOIN 
                    simpleobjects.batch as b 
                ON  
                    b.BatchId_bi = oi.BatchId_bi
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = b.UserId_int

                WHERE                 
                    oi.OptOut_dt IS NOT NULL
                AND
                    oi.OptIn_dt IS NULL                                            
                AND
                    LENGTH(oi.ContactString_vch) < 14
                AND
                    oi.OptOut_dt 
                    BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)            

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                GROUP BY ContactString_vch, OptOut_dt
                ORDER BY
                    oi.OptOut_dt DESC

                <cfif arguments.inpSkipPaging EQ 0>
                    <cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
                        LIMIT 
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                        OFFSET 
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
                    </cfif>
                </cfif>
               
                
            </cfquery>
            <cfif arguments.OutQueryResults EQ "1"> 
                <cfset DataOut.QUERYRES = getList />
            </cfif>
            <cfif getList.RECORDCOUNT GT 0>
                <cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>

                <cfloop query="rsCount">
                    <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                    <cfset dataout["iTotalRecords"] = iTotalRecords>
                </cfloop>

                <cfloop query="#getList#">
                    <cfset tmp = {
                        // ID = Id_int,
                        PHONE = Phone,
                        DATE = Date,
                        USERID = UserId,
                        BATCHID = BatchId,
                        CAMPAIGNNAME = CamaignName                   
                    }/>

                    <cfset arrayAppend(dataout['DATALIST'], tmp)/>
                </cfloop>
                
            </cfif>
            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
	
	

</cfcomponent>