BATCHID_BI<cfcomponent output="no">

	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">

    <cfparam name="Session.DBSourceEBM" default="bishop"/>
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="Form_cpresponses" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

	    <cfset var SCMBbyUser	= '' />
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var LimitItem	= '' />
	    <cfset var getShortCodeRequestByUser	= '' />
	    <cfset var DataOut	= '' />
	    <cfset var DefaultshortCode	= '' />

		<cfif LEN(trim(arguments.inpcustomdata4)) EQ 0 >


			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="DefaultshortCode"></cfinvoke>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="DefaultshortCode"></cfinvoke>

			<cfset arguments.inpcustomdata4 = DefaultshortCode.SHORTCODE/>
	  	</cfif>

		<cfsavecontent variable="DataOut">

	        	<!--- Any scripts you place here MUST be quadrant specific --->
	        	<script type="text/javascript">
					$(function(){



			        	<!--- Load Batch Ids to start --->
			        	ReloadBatchIds<cfoutput>#inpChartPostion#</cfoutput>(null);


						<cfif inpcustomdata3 GT 0>
							ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput>();
						</cfif>

						<cfif inpcustomdata5 neq 0>
							GetControlPointsText<cfoutput>#inpChartPostion#</cfoutput>();
						</cfif>

						$("#inpListBatchIds<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );
						$("#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );
						$("#inpLimit<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );

					});

					function escapeHtml(text) {

						  // Preserve newlines
						  text = text.replace(/<br>/gi, 'nexlinex');

						  var map = {
						    '&': '&amp;',
						    '<': '&lt;',
						    '>': '&gt;',
						    '"': '&quot;',
						    "'": '&#039;'
						  };

						  text = text.replace(/[&<>"']/g, function(m) { return map[m]; });
						  text = text.replace(/nexlinex/gi, '<br>');

						  return text;
						}

					<!--- This function needs to be unique to the page so use inpChartPostion as part of function name --->
					function ReloadBatchIds<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
					{

						$.ajax({

							type: "POST",
							url: '/session/sire/models/cfc/control-point.cfc?method=GetBatchList&returnformat=json&queryformat=column',
							dataType: 'json',
							async:false,
							data:
							{
								inpActive: 1
							},
							success:function(res)
							{
								if(parseInt(res.RXRESULTCODE) == 1)
								{

									$('#inpListBatchIds<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();

									var CurrKeyWordList = $('#inpListBatchIds<cfoutput>#inpChartPostion#</cfoutput>');

									<!--- Convert ajax request result array to jquery object and loop over it --->
									$.each($(res.BATCHARRAY.DATA.BATCHID_BI), function(i, item) {

										 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + res.BATCHARRAY.DATA.BATCHID_BI[i] || '<cfoutput>#inpBatchIdList#</cfoutput>' == '' + res.BATCHARRAY.DATA.BATCHID_BI[i])
										 {
										 	CurrKeyWordList.append('<option value="' + res.BATCHARRAY.DATA.BATCHID_BI[i] + '" selected>' + res.BATCHARRAY.DATA.DESC_VCH[i] + '</option>')
										 	ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput>();
										 }
										 else
										 	CurrKeyWordList.append('<option value="' + res.BATCHARRAY.DATA.BATCHID_BI[i] + '">' + res.BATCHARRAY.DATA.DESC_VCH[i] + '</option>')

									});

								}
							}

						});

					 }

					 function ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput>()
					 {
							$.ajax({

								type: "POST",
								url: '/session/sire/models/cfc/control-point.cfc?method=GetControlPointsForBatch&returnformat=json&queryformat=column',
								dataType: 'json',
								async:false,
								data:
								{
									INPBATCHID: $('#inpListBatchIds<cfoutput>#inpChartPostion#</cfoutput>').val()
								},
								success:function(res)
								{
									if(parseInt(res.RXRESULTCODE) == 1)
									{

										$('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();

										var CurrControlPointsList = $('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput>');

										<!--- Convert ajax request result array to jquery object and loop over it --->
										$.each($(res.CPDATA), function(i, item) {

												if('<cfoutput>#inpcustomdata1#</cfoutput>' == '' + item.RQ)
											 		CurrControlPointsList.append('<option value="' + item.RQ + '" selected><b>CP'+ item.RQ+ '</b> ' + escapeHtml(item.TEXT) + '</option>')
												else
													CurrControlPointsList.append('<option value="' + item.RQ + '"><b>CP'+ item.RQ+ '</b> ' + escapeHtml(item.TEXT) + '</option>')

										});


									}
								}

						});


					 }

					 function GetControlPointsText<cfoutput>#inpChartPostion#</cfoutput>(){
					 	var CPText = $('#inpListControlPoints<cfoutput>#inpChartPostion#</cfoutput> option:selected').html();

					 	$('#CPText<cfoutput>#inpChartPostion#</cfoutput>').val(CPText);
					 }
				</script>

	        <cfoutput>

				<div class="FormContainerX" id="FormContainerX_#inpChartPostion#">

				   <!--- Only display list of short codes assigned to current user--->
	               <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
	               <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />

	               <div class="head-EMS"><div class="DashObjHeaderText">Control Point Selection</div></div>

	                <input type="hidden" id="CPText#inpChartPostion#" name="CPText#inpChartPostion#" value="NO CP">

	                <div class="row">


	                   <div class="col-md-12 col-sm-12 col-xs-12">
	                       <label for="inpListBatchIds#inpChartPostion#">Campaign Name</label>
	                       <select id="inpListBatchIds#inpChartPostion#" onchange="ReloadControlPoints#inpChartPostion#();" class="ebmReport">
	                               <option value="0">Select a Campaign</option>
	                        </select>
	                   </div>

	                   <div class="col-md-12 col-sm-12 col-xs-12">

	                        <label for="inpListControlPoints#inpChartPostion#">Control Point</label>
	                        <select id="inpListControlPoints#inpChartPostion#" onchange="GetControlPointsText#inpChartPostion#()"class="ebmReport">


	                               <option value="0">Select a Control Point</option>


	                        </select>

	                   </div>

	                </div>

	                <div class="row">
	                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.chart_cpresponses', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.chart_cpresponses', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;">
	                        <!---<img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/baricon_32x18web.png" alt="" width="32" height="18">--->
	                        <span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Chart</span></a>
	                    </div>

<!--- Bad math here - hide for now
	                   <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.display_cpresponsescount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.display_cpresponsescount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Count!" style="line-height:18px;">
	                       	<!--- <img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/numbericon.png" alt="" width="32" height="32">--->
	                       	<span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Counter</span></a>
	                   </div>
--->

	                    <div class="col-md-4 col-sm-4 col-xs-4" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: '25', inpcustomdata3: $('##inpListBatchIds#inpChartPostion#').val(), inpcustomdata4: '0', inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;">
	                       	<!--- <img style="border:0; float:left;" src="#rootUrl#/#publicPath#/images/icons/tableicon_32x18web.png" alt="" width="32" height="18">--->
	                        <span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Table</span></a>
	                   </div>
	               	</div>

	            </div>

			</cfoutput>
	 	</cfsavecontent>

	    <cfreturn DataOut />
	</cffunction>

	<!--- Nameing convention for "FORM" reports data lookup method is NNNNN where NNNNN is the name of the Report --->
	<cffunction name="cpresponses" access="remote" output="false" returntype="any" hint="list all responses to a given control point for a given short code">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	    <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">

	    <!--- Paging --->
	    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
	    <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
	    <cfargument name="iDisplayStart" required="no"  default="0">
	    <cfargument name="iDisplayLength" required="no"  default="10">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
	    <!--- Default sort this descending on the 5th (0 based) column--->
	    <!---<cfargument name="iSortCol_0" required="no"  default="4">
	    <cfargument name="sSortDir_0" required="no"  default="DESC">--->

	        <cfset var sTableName	= '' />
	        <cfset var listColumns	= '' />
	        <cfset var thisColumn	= '' />
	        <cfset var thisS	= '' />
	        <cfset var qFiltered	= '' />
	        <cfset var DataOut	= {} />
	        <cfset var LOCALOUTPUT = {} />
	        <cfset var qFilteredraw = '' />
	        <cfset var qFilteredSum = '' />
	        <cfset var FormatBuff = '' />

	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

	       	<cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
				<cfset arguments.inpcustomdata1 = 0 />
	        </cfif>

	        <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
	            <cfset arguments.inpcustomdata2 = 0 />
	        </cfif>

	        <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
	            <cfset arguments.inpcustomdata3 = 0 />
	        </cfif>

	        <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
	            <cfset arguments.inpcustomdata4 = 0 />
	        </cfif>

	        <!---
	            Easy set variables
	         --->

	        <!--- ***JLP Todo: Validate user has control of batch(es) --->

	        <!--- table name --->
	        <cfset sTableName = "simplequeue.moinboundqueue" />

	        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
	        <cfset listColumns = "Response_vch,chart,TotalCount" />


	        <!--- Get data to display --->
	        <!--- Data set after filtering --->
	        <cfquery name="qFilteredraw" datasource="#Session.DBSourceREAD#">
	            SELECT
	                COUNT(IREResultsId_bi) AS TotalCount,
	                '' as chart,
	                REPLACE(TRIM(Response_vch), '\n', '') AS Response_vch
	            FROM
	                simplexresults.ireresults
	            WHERE
	                <cfif inpcustomdata3 GT 0>
			             BATCHID_BI IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			        <cfelse>
			             BATCHID_BI IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			        </cfif>
	            AND
	                Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		        AND
		            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	            AND
	                CPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
	            AND
	            	IREType_int= #IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#
				 <!--- Dont let users URL hack data that is not theirs --->
	            <cfif session.userrole NEQ 'SuperUser'>
	                AND
	                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
	            </cfif>

	            GROUP BY
	                REPLACE(TRIM(Response_vch), '\n', '')
	           	<!---<!--- Temp fix until using data from new simplexresults.iresesults table--->
	                AND QID_int <> 0--->

	            <cfif len(trim(sSearch))>
	     <!---           <cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)> OR </cfif>#thisColumn# LIKE <cfif thisColumn is "version"><!--- special case ---><cfqueryparam cfsqltype="CF_SQL_FLOAT" value="#val(url.sSearch)#" /><cfelse><cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#trim(url.sSearch)#%" /></cfif></cfloop>
	     --->
	     		</cfif>
	            <cfif iSortingCols gt 0>
	                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
	            </cfif>
	        </cfquery>

	       <cfquery name="qFilteredSum" dbtype="query">
	       		SELECT
	       			SUM(TotalCount) AS total
	       		FROM
	       			qFilteredraw
	       </cfquery>

	       <cfif qFilteredraw.recordCount eq 0>
	       		<cfset qFiltered = qFilteredraw />
	       <cfelse>

		       	<cfquery name="qFiltered" dbtype="query">
		       		SELECT

		                <cfif OutQueryResults NEQ '1'>
							cast(TotalCount as varchar) + ' (' + cast((TotalCount/#qFilteredSum.total#)*100 as varchar) + '%)' as TotalCount,
							'<div style="width:'+cast((TotalCount/#qFilteredSum.total#)*100 as varchar)+'%;height:100%;background-color:##118ACF;"></div>' as Chart,
							<!---
							0 AS TotalCount,
							'Sample' as Chart,
--->
		                <cfelse>
		                	TotalCount,
		                	cast((TotalCount/#qFilteredSum.total#)*100 as varchar) as Percentage,
		                </cfif>
		                Response_vch
		            FROM
		            	qFilteredraw
		       </cfquery>
	       </cfif>

	       <cfif OutQueryResults EQ "1">

	            <cfset DataOut.RXRESULTCODE = 1 />
				<cfset DataOut.QUERYRES = qFiltered />
				<cfset DataOut.TYPE = "" />
		        <cfset DataOut.MESSAGE = "" />
		        <cfset DataOut.ERRMESSAGE = "" />

                <cfreturn DataOut />
            </cfif>

	        <!---<cfdump var="#qFiltered#" ><cfabort>--->
	        <!---
	            Output
	         --->

	       <cfif url.iDisplayLength eq -1>
		     	<cfset url.iDisplayLength = qFiltered.recordCount />
		     </cfif>

	       <cfsavecontent variable="DataOut">


	   <!--- What was "//'" all about?  --->
	        {"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>,
	        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
	        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
	        "aaData": [
	            <cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#">
	                <cfif currentRow gt (url.iDisplayStart+1)>,</cfif>
	                [<cfloop list="#listColumns#" index="thisColumn">
	                	<cfif thisColumn neq listFirst(listColumns)>,</cfif>
	                	<cfif thisColumn is "version">
	                		<cfif version eq 0>"-"<cfelse>"#version#"</cfif>

						<cfelseif thisColumn is "Chart">
	                		<cfset FormatBuff = jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]) />
	                		"#FormatBuff#"
	                	<cfelseif thisColumn is "Response_vch">

	                		<!--- Preserve possible newlines in display but format all other tags --->
							<cfset FormatBuff = ReplaceNoCase(qFiltered[thisColumn][qFiltered.currentRow], "<br>", "newlinex", "ALL")>
							<cfset FormatBuff = ReplaceNoCase(FormatBuff, "\n", "newlinex", "ALL")>
							<cfset FormatBuff = Replace(FormatBuff, chr(10), "newlinex", "ALL")>

							<cfset FormatBuff = HTMLEditFormat(FormatBuff) />

							<!--- Allow newline in text messages - reformat for display --->
							<cfset FormatBuff = ReplaceNoCase(FormatBuff, "newlinex", "<br>", "ALL")>
							<cfset FormatBuff = Replace(FormatBuff, "\n", "<br>", "ALL")>
							<!--- Display quote but store as &apos; in the XML --->
							<cfset FormatBuff = Replace(FormatBuff, "&apos;", "'", "ALL")>

	                		"#FormatBuff#"
	                	<cfelse>
		                	<cfset FormatBuff = REPLACE(qFiltered[thisColumn][qFiltered.currentRow], CHR(39), '&apos;', 'ALL') />
	                		"#FormatBuff#"
	                	</cfif>
	                </cfloop>]
	            </cfoutput> ] }
	       </cfsavecontent>

	 <!---	<cfsavecontent variable="DataOut">{"sEcho": <cfoutput>#val(url.sEcho)#</cfoutput>, "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>, "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>, "aaData": [<cfoutput query="qFiltered" startrow="#val(url.iDisplayStart+1)#" maxrows="#val(url.iDisplayLength)#"><cfif currentRow gt (url.iDisplayStart+1)>,</cfif>[<cfloop list="#listColumns#" index="thisColumn"><cfif thisColumn neq listFirst(listColumns)>,</cfif><cfif thisColumn is "version"><cfif version eq 0>"-"<cfelse>"#jsStringFormat(version)#"</cfif><cfelse>"#HTMLCODEFORMAT(qFiltered[thisColumn][qFiltered.currentRow])#"</cfif></cfloop>]</cfoutput>] }</cfsavecontent>
	 --->
	    <cfreturn DataOut />

	</cffunction>


	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="display_cpresponses" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="numeric" required="yes" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="25" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="numeric" required="yes" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	 	<cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var DataOut	= '' />
	    <cfset var qFilteredraw	= '' />

		<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
		<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />

	    <cfquery name="qFilteredraw" datasource="#Session.DBSourceREAD#">
	        SELECT
	            COUNT(IREResultsId_bi) AS TotalCount
	        FROM
	            simplexresults.ireresults
	        WHERE
	        <cfif inpcustomdata3 GT 0>
	             BATCHID_BI IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        <cfelse>
	             BATCHID_BI IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        </cfif>
	        AND
	            Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	            CPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
	       	AND
	       		IREType_int= #IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#
		 <!--- Dont let users URL hack data that is not theirs --->
	        <cfif session.userrole NEQ 'SuperUser'>
	            AND
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
	        </cfif>
	    </cfquery>

		<cfsavecontent variable="DataOut">
	       	<cfoutput>

				<!--- Build javascript objects here - include any custom data parameters at the end --->
	        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: '#inpChartPostion#', inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />

				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
	                <div class="head-EMS"><div class="DashObjHeaderText">Reply for : #inpcustomdata5# (#qFilteredraw.TotalCount#)</div></div>

	        		<!--- Pass in extra data to set ddefault sorting you would like --->
	        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >

	                <!--- Keep table auto-scroll in managed area--->
	                <div class="EBMDataTableWrapper">

						<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->
	                    <table id="ReportDataTable" class="table table-striped table-bordered table-hover" style="clear:both; width:100%;" border="0">
	                        <thead>
	                            <tr>
	                                <th width="40%" style="width:40%;">Response</th>
	                                <th width="40%" style="width:40%;">Chart</th>
	                                <th width="20%" style="width:20%;">Count</th>
	                            </tr>
	                        </thead>

	                        <tbody>
	                            <tr>
	                                <td colspan="5" class="datatables_empty">Loading data from server</td>
	                            </tr>
	                        </tbody>
	                    </table>

	                </div>

	                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
	                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
	                   <div class="DownloadLink excelIcon" rel1="cpresponsesregex.cpresponses" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#"></div>
	                   <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	                </div>
	            </div>

			</cfoutput>
	 	</cfsavecontent>

	    <cfreturn DataOut />
	</cffunction>



	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="chart_cpresponses" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

	    <cfset var barChartObj	= '' />
	    <cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var category	= '' />
	    <cfset var data	= '' />
	    <cfset var index	= '' />
	    <cfset var AggregateQuery	= '' />
	    <cfset var AggregateQuerySorted	= '' />
	    <cfset var objResponseSorter	= '' />
		<cfset var AggregateQuerySortedindex	= '' />

	    <cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
	       	<cfset arguments.inpcustomdata1 = 0 />
	    </cfif>

	    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
	       	<cfset arguments.inpcustomdata2 = 0 />
	    </cfif>

	    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
	       	<cfset arguments.inpcustomdata3 = 0 />
	    </cfif>

	    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
	       	<cfset arguments.inpcustomdata4 = 0 />
	    </cfif>


	 	<!---- to find the total active by keyword ---->
	    <cfquery name="AggregateQuery" datasource="#Session.DBSourceREAD#">
	        SELECT
	            COUNT(IREResultsId_bi) AS TotalCount,
	            REPLACE(TRIM(Response_vch), '\n', '') AS Response_vch
	        FROM
	            simplexresults.ireresults
	        WHERE
	 		<cfif inpcustomdata3 GT 0>
	             BATCHID_BI IN (<cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        <cfelse>
	             BATCHID_BI IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        </cfif>
	        AND
	            Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	            CPId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
	        AND
	        	IREType_int = #IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#
	        GROUP BY
	            REPLACE(TRIM(Response_vch), '\n', '')
	        ORDER  BY
	            COUNT(IREResultsId_bi) DESC
	        LIMIT
	        	#inpcustomdata2#
	    </cfquery>

	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <cfset barChartObj.setTitle("") />
	    <cfset barChartObj.setYAxisTitle("Count Response") />
	    <cfset barChartObj.setXAxisTitle("Response") />
	    <cfset barChartObj.setAverageAvailable(false) />


	   <!--- Build javascript objects here - include any custom data parameters at the end --->
		<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: '#inpChartPostion#', inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: '#inpChartPostion#', inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />

		<!--- store link data in body variable for later retrival  --->
		<cfset barChartObj.setResetLink( '$("body").data("ResetLink#inpChartPostion#", "RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)");') />
	    <cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Response Counts For Control Point CP#inpcustomdata1#");') />

	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>

	    <cfset index=0>

	    <cfif AggregateQuery.RecordCount EQ 0>

		    <cfset barChartObj.setXAxisTitle("No Response Data in the Selected Date Range") />
		    <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>

	    <cfelse>
	   		<!--- Sort numbers and alpha in answer order for easier analysis --->
	    	<!---<cfset AggregateQuery.sort(JavaCast("int", AggregateQuery.findColumn("Response_vch")),TRUE)>--->

	       <!--- <cfquery name="AggregateQuerySorted" dbtype="query">
	            SELECT
	                TotalCount,
	                Response_vch
	            FROM
	                AggregateQuery
	            ORDER BY
	            	Response_vch DESC
	        </cfquery>--->

	        <!--- http://www.bennadel.com/blog/387-Sorting-ColdFusion-Arrays-With-Sortable-Interfaces.htm --->
	        <cfset objResponseSorter = CreateObject("component", "sortableresponses").Init() />

	        <cfset var arrAggregateQuerySorted = objResponseSorter.QueryToArray(AggregateQuery) />


	        <!--- Sort the array. --->
	        <cfset arrAggregateQuerySorted = objResponseSorter.SortArray(
	            arrAggregateQuerySorted,
	            "LT"
	            ) />


	    	<cfloop array="#arrAggregateQuerySorted#" index="AggregateQuerySortedindex">
	            <cfset index= index+1>

	            <!--- Friggin mySQL TRIM does not remove newlines. Newlines will break chart --->
	            <cfset ArrayAppend(category, Left(Replace( TRIM( REReplace(AggregateQuerySortedindex.Response_vch,"#chr(13)#|#chr(9)#|\n|\r","","ALL")  ), "'", "`", "ALL"), 10))/>
	            <cfset ArrayAppend(data, AggregateQuerySortedindex.TotalCount)/>
	        </cfloop>

	    <!---
				<cfloop query="AggregateQuery">
					<cfset index= index+1>
					<!---<cfset ArrayAppend(category, Replace(AggregateQuery.Response_vch, "'", "''", "ALL"))/>--->
					<cfset ArrayAppend(category, TRIM(AggregateQuery.Response_vch))/>
					<cfset ArrayAppend(data, AggregateQuery.TotalCount)/>
				</cfloop>
		--->
		</cfif>

	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>

	    <cfreturn barChartObj.drawChart()>

	</cffunction>

	<cffunction name="display_cpresponsescount" access="remote" output="false" returntype="any" hint="Number and Percentage of users on Control Point">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />

	    <cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var CPUserCountDisplay	= '' />
	    <cfset var GetCPCounts = '' />
	    <cfset var GetCPCountsTotal = '' />
	    <cfset var getBatchIdsofshortcode = '' />
	    <cfset var node = '' />
	    <cfset var keyword = '' />
	    <cfset var SCMBbyUser	= '' />
		<cfset var getShortCodeRequestByUser	= '' />
		<cfset var GetBatches	= '' />

	    <!--- Build javascript objects here - include any custom data parameters at the end --->
		<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: '#inpChartPostion#', inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#' }" />
	    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'cpresponsesregex.Form_cpresponses', inpReportType: 'FORM', inpChartPostion: '#inpChartPostion#', inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#', inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />

	   	<cfinvoke component="session.cfc.csc.csc" method="GetKeywordsForShortCode" returnvariable="getBatchIdsofshortcode">
			<cfinvokeargument name="inpShortCode" value="#ARGUMENTS.inpcustomdata4#">
		</cfinvoke>

		<cfloop array="#getBatchIdsofshortcode.KeywordData#" index="node">
			<cfif node.batchId eq inpcustomdata3>
				<cfset keyword = node.keyword />
			</cfif>
		</cfloop>

		<cfquery name="GetBatches" datasource="#Session.DBSourceEBM#">
                SELECT
                	BATCHID_BI,
                	DESC_VCH
                FROM
					simpleobjects.batch
	            WHERE
	            	<cfif inpcustomdata3 GT 0>
			             BATCHID_BI = <cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" />
			        <cfelse>
			             BATCHID_BI IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			        </cfif>
		        AND
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">


        </cfquery>

	    <cfquery name="GetCPCounts" datasource="#Session.DBSourceREAD#">
	    	SELECT
	            sum(case when CPId_int = #inpcustomdata1# then 1 else 0 end) AS TotalCPCount,
	            COUNT(ContactString_vch) AS TotalCount
	        FROM
	            simplexresults.ireresults
	        WHERE
	        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
		        <cfif inpcustomdata3 GT 0>
		             BATCHID_BI = <cfqueryparam value="#inpcustomdata3#" cfsqltype="CF_SQL_BIGINT" />
		        <cfelse>
		             BATCHID_BI IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		        </cfif>
	       AND
	       		IREType_int= #IREMESSAGETYPE_IRE_ASSIGNED_ANSWER_VALUE#
	       AND
				CPID_int <> 0
	    </cfquery>

	    <cfif not isnumeric(GetCPCounts.TotalCPCount)>
	    	<cfset GetCPCounts.TotalCPCount = 0 />
	    </cfif>

	     <cfif not isnumeric(GetCPCounts.TotalCount)>
	    	<cfset GetCPCounts.TotalCount = 0 />
	    </cfif>

		<cfsavecontent variable="CPUserCountDisplay" >
			<cfoutput>
				<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;background-color:##8099B1" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
					<cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
				    <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />

	                <div class="head-EMS"><div class="DashObjHeaderText">Number of users on Control Point</div></div>
	                <div style="width:100%;font-size:2em;font-weight:bold;margin:0;">

	                	<div class="col-xs-12">
	                	<h3 style="color:##fff">#GetBatches.DESC_VCH#</h3>
	                	</div>
	                </div>

	                <div style="clear:both;margin:0;"></div><BR />

	                <div style="font-size:1em;font-weight:bold;margin:0 auto;color:##fff;text-align:center;padding:4px;">
	                	#ARGUMENTS.inpcustomdata5#
	                </div>

	                <div style="clear:both;margin:0;"></div><BR />

	                <div style="width:100%;font-size:2em;font-weight:bold;">
	                	<div style="width:33%;float:left;color:##fff;text-align: center;margin:0;">
	                		<div style="text-align: center;margin:0;">#GetCPCounts.TotalCPCount#</div>
	                		<span style="font-size:0.5em;color:##ccc;">Replies for CP</span>
	                	</div>
	                	<div style="width:33%;float:left;color:##fff;text-align: center;margin:0;">
	                		<div style="text-align: center;margin:0;">#GetCPCounts.TotalCount#</div>
	                		<span style="font-size:0.5em;color:##ccc;">Total Replies</span>
	                	</div>
	                	<div style="width:33%;float:left;color:##fff;text-align: center;margin:0;">
	                		<div style="text-align: center;margin:0;">
	                			<cfif GetCPCounts.TotalCount eq 0>
		                			0%
		                		<cfelse>
									#NumberFormat((GetCPCounts.TotalCPCount/GetCPCounts.TotalCount)*100,"0.00")#%
		                		</cfif>
	                		</div>
	                		<span style="font-size:0.5em;color:##ccc;">Percentage</span>
	                	</div>
	                </div>
					<div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
		            <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
		            </div>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn CPUserCountDisplay />
	</cffunction>

</cfcomponent>
