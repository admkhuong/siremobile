<cfcomponent>
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cffunction name="GetRecentActiveFeed" access="remote" hint="Get recent active feed">
		<cfargument name="inpLimit" type="numeric" required="no" default="100">
		<cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">

		<cfset var dataout = {} />
		<cfset var GetActiveFeedData = {} />
		<cfset var curUserID = arguments.inpUserID />
		<cfset var campaignBatchIds = "" />
		<cfset var batchIds = '' />
		<cfset var getCampaignName = '' />
		<cfset var ScrubbedSMS = '' />

		<cfset dataout["DATA"] = arrayNew(1) />
		<cftry>
			<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
				SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">; 
			</cfquery>
			<cfif campaignBatchIds.RecordCount GT 0>
				<cfset  batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
			<cfelse>
				<cfset  batchIds = '-1'>
			</cfif>


			<cfquery name="GetActiveFeedData" datasource="#Session.DBSourceREAD#">
				SELECT Id_bi,BatchId_bi, ContactString_vch, DATE_FORMAT(dt, '%Y/%m/%d  at  %h:%i:%s %p') AS  ActionAt, sms_type, sms, log_type   FROM (
					(
					SELECT
						OT.OptId_int AS Id_bi,
				    	OT.BatchId_bi, 
				        OT.ContactString_vch, 
						IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
						0 AS sms_type,
						'' AS sms,
						IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type 
					FROM 
				    	simplelists.optinout OT INNER JOIN simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
						INNER JOIN simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				    	
					WHERE (OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
						AND LENGTH(OT.ContactString_vch) < 14
					ORDER BY 
				    	dt DESC, Id_bi DESC
				    LIMIT 100
					)
					
					UNION
					
					(
					SELECT
						IREResultsId_bi AS Id_bi,
				    	BatchId_bi, 
				        ContactString_vch, 
						Created_dt AS dt,
						IREType_int AS sms_type,
						Response_vch AS sms,
						0 AS log_type 
					FROM 
				    	simplexresults.ireresults

					WHERE BatchId_bi IN (#batchIds#)		
						AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND IREType_int IN (1,2)
						AND LENGTH(ContactString_vch) < 14
						ORDER BY 
					    	dt DESC, Id_bi DESC
					    LIMIT 100
					)
				) AS ActiveFeed
				ORDER BY 
			    	dt DESC, Id_bi DESC
			    LIMIT 100
			</cfquery>
			<cfif GetActiveFeedData.RecordCount GT 0>

				<cfloop query="GetActiveFeedData">
					<cfquery name="getCampaignName" datasource="#Session.DBSourceREAD#">
						SELECT
							Desc_vch
						FROM
							simpleobjects.batch
						WHERE
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetActiveFeedData.BatchId_bi#">
						AND
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						LIMIT
							1
					</cfquery>


					<!--- Preserve possible newlines in display but format all other tags --->
					<cfset ScrubbedSMS = ReplaceNoCase(GetActiveFeedData.sms, "<br>", "newlinex", "ALL")>
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "\n", "newlinex", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, chr(10), "newlinex", "ALL")>
					
					<cfset ScrubbedSMS = HTMLEditFormat(ScrubbedSMS) />
					
					<!--- Allow newline in text messages - reformat for display --->
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "newlinex", "<br>", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "\n", "<br>", "ALL")>
					<!--- Display ' but store as &apos; in the XML --->
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "&apos;", "'", "ALL")>

					<cfset var item = {
						phone = "#GetActiveFeedData.ContactString_vch#",
						date = "#GetActiveFeedData.ActionAt#",
						batchid = "#GetActiveFeedData.BatchId_bi#",
						logtype = "#GetActiveFeedData.log_type#",
						smstype = "#GetActiveFeedData.sms_type#",
						sms = "#ScrubbedSMS#",
						id = "#GetActiveFeedData.Id_bi#",
						campaignName = "#getCampaignName.Desc_vch#"
					} />

					<cfset arrayAppend(dataout["DATA"], item)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get activity feed successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			
			<cfcatch>
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
                <cfset dataout.RXRESULTCODE= -1 />    
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetRecentActiveFeedTable" access="remote" hint="Get recent active feed for datatable">
		<cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />
		<cfargument name="customFilter" default="" />

		<cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var dataout = {} />
		<cfset var GetActiveFeedDataTotal = '' />
		<cfset var GetActiveFeedData = {} />
		<cfset var curUserID = arguments.inpUserID />
		<cfset var campaignBatchIds = "" />
		<cfset var batchIds = '' />
		<cfset var rsCount = '' />
		<cfset var getCampaignName = '' />
		<cfset var ScrubbedSMS = '' />
		<cfset var count = arguments.iDisplayStart/>
		<cfset var filterItem = '' />
		<cfset var page = (arguments.iDisplayStart/arguments.iDisplayLength) + 1/>
		<cfset var fieldName = '' />

		<cfset dataout["ActivityList"] = arrayNew(1) />
		<cftry>
			<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
				SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">; 
			</cfquery>
			<cfif campaignBatchIds.RecordCount GT 0>
				<cfset  batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
			<cfelse>
				<cfset  batchIds = '-1'>
			</cfif>

			<cfquery name="GetActiveFeedDataTotal" datasource="#Session.DBSourceREAD#">
				SELECT
					SUM(TOTAL) AS TOTAL
				FROM (
					(
						SELECT
							COUNT(OT.OptId_int) AS TOTAL
						FROM 
							simplelists.optinout OT
						INNER JOIN
							simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
						INNER JOIN
							simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
						INNER JOIN
							simpleobjects.batch B ON B.BatchId_bi = OT.BatchId_bi
						WHERE 
							(OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
						AND
							LENGTH(OT.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " B.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " OT.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " OT.OptId_int " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
					)
					
					UNION
					
					(
						SELECT
							COUNT(i.IREResultsId_bi) AS TOTAL
						FROM 
							simplexresults.ireresults i
						INNER JOIN
							simpleobjects.batch b
						ON
							i.BatchId_bi = b.BatchId_bi
						WHERE
							i.BatchId_bi IN (#batchIds#)		
						AND 
							i.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND 
							i.IREType_int IN (1,2)
						AND 
							LENGTH(i.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " i.Created_dt "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " b.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " i.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " i.Response_vch " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
					)
					
				) AS ActiveFeed
			</cfquery>

			<cfquery name="GetActiveFeedData" datasource="#Session.DBSourceREAD#">
				SELECT
					Id_bi,
					BatchId_bi,
					ContactString_vch,
					dt,
					sms_type,
					sms,
					log_type,
					Desc_vch
				FROM (
					(
						SELECT
							OT.OptId_int AS Id_bi,
							OT.BatchId_bi AS BatchId_bi,
						    OT.ContactString_vch, 
							IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
							0 AS sms_type,
							'' AS sms,
							IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type,
							B.Desc_vch as Desc_vch
						FROM 
							simplelists.optinout OT
						INNER JOIN
							simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
						INNER JOIN
							simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
						INNER JOIN
							simpleobjects.batch B ON B.BatchId_bi = OT.BatchId_bi
						WHERE 
							(OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
						AND
							LENGTH(OT.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " B.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " OT.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " OT.OptId_int " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
						ORDER BY
							<cfswitch expression="#arguments.iSortCol_0#">
								<cfcase value="6">
									dt #arguments.sSortDir_0#
								</cfcase>
								<cfcase value="1">
									BatchId_bi #arguments.sSortDir_0#
								</cfcase>
								<cfdefaultcase>
									dt #arguments.sSortDir_0#
								</cfdefaultcase>
							</cfswitch>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />
					)
					
					UNION
					
					(
						SELECT
							i.IREResultsId_bi AS Id_bi,
							i.BatchId_bi AS BatchId_bi , 
						    i.ContactString_vch AS ContactString_vch, 
							i.Created_dt AS dt,
							i.IREType_int AS sms_type,
							i.Response_vch AS sms,
							0 AS log_type,
							b.Desc_vch AS Desc_vch
						FROM 
							simplexresults.ireresults i
						INNER JOIN
							simpleobjects.batch b
						ON
							i.BatchId_bi = b.BatchId_bi
						WHERE
							i.BatchId_bi IN (#batchIds#)		
						AND 
							i.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND 
							i.IREType_int IN (1,2)
						AND 
							LENGTH(i.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " i.Created_dt "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " b.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " i.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " i.Response_vch " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>

						ORDER BY
							<cfswitch expression="#arguments.iSortCol_0#">
								<cfcase value="6">
									dt #arguments.sSortDir_0#
								</cfcase>
								<cfcase value="1">
									BatchId_bi #arguments.sSortDir_0#
								</cfcase>
								<cfdefaultcase>
									dt #arguments.sSortDir_0#
								</cfdefaultcase>
							</cfswitch>
						LIMIT
							<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />
					)
					
				) AS ActiveFeed
				WHERE
					1
					<cfif arguments.customFilter NEQ "">
						<cfloop array="#filterData#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
				ORDER BY
					<cfswitch expression="#arguments.iSortCol_0#">
						<cfcase value="6">
							dt #arguments.sSortDir_0#
						</cfcase>
						<cfcase value="1">
							BatchId_bi #arguments.sSortDir_0#
						</cfcase>
						<cfdefaultcase>
							dt #arguments.sSortDir_0#
						</cfdefaultcase>
					</cfswitch>
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
			</cfquery>

			<cfset dataout["iTotalDisplayRecords"] = GetActiveFeedDataTotal.TOTAL />
			<cfset dataout["iTotalRecords"] = GetActiveFeedDataTotal.TOTAL />

			<cfif GetActiveFeedData.RecordCount GT 0>

				<cfloop query="GetActiveFeedData">
					<cfset ScrubbedSMS = ReplaceNoCase(GetActiveFeedData.sms, "<br>", "newlinex", "ALL")>
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "\n", "newlinex", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, chr(10), "newlinex", "ALL")>
					
					<cfset ScrubbedSMS = HTMLEditFormat(ScrubbedSMS) />
					
					<!--- Allow newline in text messages - reformat for display --->
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "newlinex", "<br>", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "\n", "<br>", "ALL")>
					<!--- Display ' but store as &apos; in the XML --->
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "&apos;", "'", "ALL")>

					<cfset var activity = '' />

					<cfif GetActiveFeedData.log_type EQ 1>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# joined #Desc_vch#"/>
					<cfelseif GetActiveFeedData.log_type EQ 2>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# leave your campaign"/>
					<cfelseif GetActiveFeedData.log_type EQ 3>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# joined #ScrubbedSMS#CPP"/>
					<cfelse>
						<cfset activity = ScrubbedSMS/>
					</cfif>

					<cfset var item = [
						id = "#++count#",
						batchid = "#GetActiveFeedData.BatchId_bi#",
						campaignName = "#Desc_vch#",
						phone = "#GetActiveFeedData.ContactString_vch#",
						smstype = "#iIf(GetActiveFeedData.sms_type EQ 1, DE("Received"), DE("Sent"))#",
						sms = "#activity#",
						date = "#dateFormat(GetActiveFeedData.dt, 'yyyy-mm-dd')# #timeFormat(GetActiveFeedData.dt, 'HH:mm:ss')#",
						logtype = "#GetActiveFeedData.log_type#"
					] />

					<cfset arrayAppend(dataout["ActivityList"], item)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get activity feed successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			
			<cfcatch>
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
                <cfset dataout.RXRESULTCODE= -1 />    
			</cfcatch>
		</cftry>

		<cfreturn SerializeJSON(dataout)  />
	</cffunction>

	<cffunction name="getUserHistory" access="remote" hint="Get Activity of User">
		<cfargument name="inpUserID" type="numeric" default="#Session.USERID#">
		<cfset var dataout = {} />
		<cfset var getUserHistory = "" />

		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />

        <cfset dataout['dataList'] = [] />

		<cftry>
			<cfquery name="getUserHistory" datasource="#Session.DBSourceREAD#">
				SELECT 
					ModuleName_vch,
					Operator_vch,
					DATE_FORMAT(Timestamp_dt, '%Y/%m/%d  at  %h:%i:%s %p') AS  ActionAt
				FROM 
					simpleobjects.userlogs
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				ORDER BY 
					Timestamp_dt DESC
				LIMIT 100	
			</cfquery>

			<cfloop query="getUserHistory" >
				<cfset var item = {moduleName = "#ModuleName_vch#", operator = "#Operator_vch#", ActionAt = "#ActionAt#"} />
				<cfset ArrayAppend(dataout["dataList"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
         	<cfset dataout.MESSAGE = "Get user history success!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>



	<cffunction name="GetRecentCampaigns" access="remote" hint="Get Top 5 of campaigns">
		<cfargument name="inpUserID" type="numeric" default="#Session.USERID#">
		<cfset var dataout = {} />
		<cfset var getCampaignList = "" />

		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />

        <cfset dataout['dataList'] = [] />

		<cftry>
			<cfquery name="getCampaignList" datasource="#Session.DBSourceREAD#">
				SELECT
					b.BatchId_bi,
	                b.DESC_VCH,
                    b.EMS_Flag_int,
                    bl.AmountLoaded_int,
                    bl.Created_dt AS BlastCreated_dt
				FROM
					simpleobjects.batch AS b
					LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1) 
					LEFT JOIN simpleobjects.batch_blast_log bl ON bl.PKId_int = (
					
																				    SELECT
																				      	MAX(fa.PKId_int) 
																				    FROM 
																				    	simpleobjects.batch_blast_log fa 
																				    WHERE 
																				    	fa.BatchId_bi = b.BatchId_bi
																				  ) 						
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
					b.Active_int > 0
					AND
					b.EMS_Flag_int IN (0,1,2)
				ORDER BY 
					b.Created_dt DESC
				LIMIT 
					5
			</cfquery>

			<cfloop query="getCampaignList" >
				<cfset var item = {id = "#BatchId_bi#", name = "#DESC_VCH#", blastCreated = "#dateformat(BlastCreated_dt,'yyyy-mm-dd')#", AmountLoaded = "#AmountLoaded_int#"} />
				<cfset ArrayAppend(dataout["dataList"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
         	<cfset dataout.MESSAGE = "Get campaign list success!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="GetRecentWailist" access="remote" hint="Get Top 5 Waitlist">
		<cfargument name="inpUserID" type="numeric" default="#Session.USERID#">
		<cfset var dataout = {} />
		<cfset var getWaitList = "" />

		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />

        <cfset dataout['dataList'] = [] />

		<cftry>
			<cfquery name="getWaitList" datasource="#Session.DBSourceREAD#">
				SELECT 
					ListId_bi,
					ListName_vch,
					Created_dt
				FROM 
					simpleobjects.waitlist
				WHERE 
					Active_ti = 1
					AND 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				ORDER BY
					Created_dt DESC
				LIMIT 5
			</cfquery>

			<cfloop query="getWaitList" >
				<cfset var item = {id = "#ListId_bi#", name = "#ListName_vch#" }/>
				<cfset ArrayAppend(dataout["dataList"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
         	<cfset dataout.MESSAGE = "Get waitlist data success!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
    



	<cffunction name="GetCampaignStatistic" access="remote" >
		<cfargument name="inpUserID" type="numeric" default="#Session.USERID#">
		<cfset var dataout = {} />
		<cfset var countCampaign = "" />

		<cfset var CountKeyword = "" />

		<cfset var countSMSLast30Day = "" />




		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />


        <cftry>
			<cfquery name="countCampaign" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS Total
				FROM
					simpleobjects.batch AS b
					LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1)
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				AND
					b.Active_int > 0
				AND
					b.EMS_Flag_int IN (0,1,2)
			</cfquery>

			<cfquery name="CountKeyword" datasource="#Session.DBSourceREAD#">
				SELECT 
	        		COUNT(k.KeywordId_int) AS Total 
	        	FROM 
	        		sms.keyword k 
	        		INNER JOIN simpleobjects.batch b ON k.BatchId_bi = b.BatchId_bi 
	        	WHERE 
	        		k.Active_int = 1
	        	AND 
	        		k.IsDefault_bit = 0
	        	AND 
	        		EMSFlag_int = 0
	        	AND 
	        		b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#"> 
			</cfquery>

			<cfquery name="countSMSLast30Day" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(1) AS Total
				FROM 
					simplexresults.ireresults as ire
				WHERE
					ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
					AND 
					ire.IREType_int IN (1,2)
					AND 
					ire.Created_dt > DATE_ADD(NOW(), INTERVAL -30 DAY)
			</cfquery>


			<cfset var dataout['DATA'] = { TotalCampaign = "#countCampaign.TOTAL#", TotalKeyword = "#CountKeyword.TOTAL#",  TotalSMS = "#countSMSLast30Day.TOTAL#"} />

			<cfset dataout.RXRESULTCODE = 1 />
         	<cfset dataout.MESSAGE = "Get data success!" />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>
		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetRecentActiveDashboard" access="remote" hint="Get recent active feed for dashboard">
		<cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">
		<cfargument name="isFirstLoad" required="no" default="">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />
		<cfargument name="customFilter" default="" />

		<cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var dataout = {} />
		<cfset var GetActiveFeedDataTotal = '' />
		<cfset var GetActiveFeedData = {} />
		<cfset var curUserID = arguments.inpUserID />
		<cfset var campaignBatchIds = "" />
		<cfset var batchIds = '' />
		<cfset var rsCount = '' />
		<cfset var getCampaignName = '' />
		<cfset var ScrubbedSMS = '' />
		<cfset var count = arguments.iDisplayStart/>
		<cfset var filterItem = '' />
		<cfset var page = (arguments.iDisplayStart/arguments.iDisplayLength) + 1/>
		<cfset var fieldName = '' />

		<cfset dataout["ActivityList"] = arrayNew(1) />
		<cftry>
			<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
				SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">; 
			</cfquery>
			<cfif campaignBatchIds.RecordCount GT 0>
				<cfset  batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
			<cfelse>
				<cfset  batchIds = '-1'>
			</cfif>

			<cfquery name="GetActiveFeedDataTotal" datasource="#Session.DBSourceREAD#">
				SELECT
					SUM(TOTAL) AS TOTAL
				FROM (
					(
						SELECT
							COUNT(OT.OptId_int) AS TOTAL
						FROM 
							simplelists.optinout OT
						INNER JOIN
							simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
						INNER JOIN
							simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
						INNER JOIN
							simpleobjects.batch B ON B.BatchId_bi = OT.BatchId_bi
						INNER JOIN
							simpleobjects.templatecampaign TC ON B.TemplateId_int = TC.TID_int
						INNER JOIN
							simplelists.subscriberbatch SB ON SB.BatchId_bi = B.BatchId_bi
						INNER JOIN
							simplelists.grouplist GL ON GL.GroupId_bi = SB.GroupId_bi
						WHERE 
							(OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
						AND
							LENGTH(OT.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " B.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " OT.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " OT.OptId_int " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
						<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						</cfif>
					)
					
					UNION
					
					(
						SELECT
							COUNT(i.IREResultsId_bi) AS TOTAL
						FROM 
							simplexresults.ireresults i
						INNER JOIN
							simpleobjects.batch b
						ON
							i.BatchId_bi = b.BatchId_bi
						INNER JOIN
							simpleobjects.templatecampaign tc ON b.TemplateId_int = tc.TID_int
						INNER JOIN
							simplelists.subscriberbatch sb ON sb.BatchId_bi = b.BatchId_bi
						INNER JOIN
							simplelists.grouplist gl ON gl.GroupId_bi = sb.GroupId_bi
						WHERE
							i.BatchId_bi IN (#batchIds#)		
						AND 
							i.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND 
							i.IREType_int IN (1,2)
						AND 
							LENGTH(i.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " i.Created_dt "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " b.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " i.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " i.Response_vch " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
						<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						</cfif>
					)
					
				) AS ActiveFeed
			</cfquery>

			<cfquery name="GetActiveFeedData" datasource="#Session.DBSourceREAD#">
				SELECT
					Id_bi,
					BatchId_bi,
					ContactString_vch,
					dt,
					sms_type,
					sms,
					log_type,
					Desc_vch,
					userid,
					BatchCreatedDt,
					BatchTemplate,
					SubscriberList
				FROM (
					(
						SELECT
							OT.OptId_int AS Id_bi,
							OT.BatchId_bi AS BatchId_bi,
						    OT.ContactString_vch, 
							IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
							0 AS sms_type,
							'' AS sms,
							IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type,
							B.Desc_vch as Desc_vch,
							B.UserId_int as userid,
							B.Created_dt as BatchCreatedDt,
							TC.Name_vch as BatchTemplate,
							GL.GroupName_vch as SubscriberList

						FROM 
							simplelists.optinout OT
						INNER JOIN
							simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
						INNER JOIN
							simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
						INNER JOIN
							simpleobjects.batch B ON B.BatchId_bi = OT.BatchId_bi
						INNER JOIN
							simpleobjects.templatecampaign TC ON B.TemplateId_int = TC.TID_int
						INNER JOIN
							simplelists.subscriberbatch SB ON SB.BatchId_bi = B.BatchId_bi
						INNER JOIN
							simplelists.grouplist GL ON GL.GroupId_bi = SB.GroupId_bi
						WHERE 
							(OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
						AND
							LENGTH(OT.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " B.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " OT.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " OT.OptId_int " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>
						ORDER BY
							<cfswitch expression="#arguments.iSortCol_0#">
								<cfcase value="6">
									dt #arguments.sSortDir_0#
								</cfcase>
								<cfcase value="1">
									BatchId_bi #arguments.sSortDir_0#
								</cfcase>
								<cfdefaultcase>
									dt DESC
								</cfdefaultcase>
							</cfswitch>
						<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						<cfelse>
							LIMIT
								<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />
						</cfif>
					)
					
					UNION
					
					(
						SELECT
							i.IREResultsId_bi AS Id_bi,
							i.BatchId_bi AS BatchId_bi , 
						    i.ContactString_vch AS ContactString_vch, 
							i.Created_dt AS dt,
							i.IREType_int AS sms_type,
							i.Response_vch AS sms,
							0 AS log_type,
							b.Desc_vch AS Desc_vch,
							b.UserId_int as userid,
							b.Created_dt as BatchCreatedDt,
							tc.Name_vch as BatchTemplate,
							gl.GroupName_vch as SubscriberList
						FROM 
							simplexresults.ireresults i
						INNER JOIN
							simpleobjects.batch b
						ON
							i.BatchId_bi = b.BatchId_bi
						INNER JOIN
							simpleobjects.templatecampaign tc ON b.TemplateId_int = tc.TID_int
						INNER JOIN
							simplelists.subscriberbatch sb ON sb.BatchId_bi = b.BatchId_bi
						INNER JOIN
							simplelists.grouplist gl ON gl.GroupId_bi = sb.GroupId_bi
						WHERE
							i.BatchId_bi IN (#batchIds#)		
						AND 
							i.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
						AND 
							i.IREType_int IN (1,2)
						AND 
							LENGTH(i.ContactString_vch) < 14
						<cfif arguments.customFilter NEQ "">
							<cfloop array="#filterData#" index="filterItem">
								<cfif filterItem.NAME EQ " dt ">
									<cfset fieldName = " i.Created_dt "/>
								<cfelseif filterItem.NAME EQ " Desc_vch ">
									<cfset fieldName = " b.Desc_vch "/>
								<cfelseif filterItem.NAME EQ "" >
									<cfset fieldName = " i.ContactString_vch "/>
								<cfelse>
									<cfset fieldName = " i.Response_vch " />
								</cfif>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
								AND 
									#PreserveSingleQuotes(fieldName)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
							</cfloop>
						</cfif>

						ORDER BY
							<cfswitch expression="#arguments.iSortCol_0#">
								<cfcase value="6">
									dt #arguments.sSortDir_0#
								</cfcase>
								<cfcase value="1">
									BatchId_bi #arguments.sSortDir_0#
								</cfcase>
								<cfdefaultcase>
									dt DESC
								</cfdefaultcase>
							</cfswitch>
						<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						<cfelse>
							LIMIT
								<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />
						</cfif>
					)
					
				) AS ActiveFeed
				WHERE
					1
					<cfif arguments.customFilter NEQ "">
						<cfloop array="#filterData#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
				ORDER BY
					<cfswitch expression="#arguments.iSortCol_0#">
						<cfcase value="6">
							dt #arguments.sSortDir_0#
						</cfcase>
						<cfcase value="1">
							BatchId_bi #arguments.sSortDir_0#
						</cfcase>
						<cfdefaultcase>
							dt DESC
						</cfdefaultcase>
					</cfswitch>
				<cfif arguments.isFirstLoad EQ 1>
					LIMIT 3
				<cfelse>
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
				</cfif>

				OFFSET
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
			</cfquery>
			<!--- <cfdump var="#GetActiveFeedData#" abort="true" /> --->
			<cfset dataout["iTotalDisplayRecords"] = GetActiveFeedDataTotal.TOTAL />
			<cfset dataout["iTotalRecords"] = GetActiveFeedDataTotal.TOTAL />
			
			<cfif GetActiveFeedData.RecordCount GT 0>

				<cfloop query="GetActiveFeedData">
					<cfset ScrubbedSMS = ReplaceNoCase(GetActiveFeedData.sms, "<br>", "newlinex", "ALL")>
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "\n", "newlinex", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, chr(10), "newlinex", "ALL")>
					
					<cfset ScrubbedSMS = HTMLEditFormat(ScrubbedSMS) />
					
					<!--- Allow newline in text messages - reformat for display --->
					<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "newlinex", "<br>", "ALL")>
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "\n", "<br>", "ALL")>
					<!--- Display ' but store as &apos; in the XML --->
					<cfset ScrubbedSMS = Replace(ScrubbedSMS, "&apos;", "'", "ALL")>

					<cfset var activity = '' />

					<cfif GetActiveFeedData.log_type EQ 1>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# joined #Desc_vch#"/>
					<cfelseif GetActiveFeedData.log_type EQ 2>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# leave your campaign"/>
					<cfelseif GetActiveFeedData.log_type EQ 3>
						<cfset activity = "#GetActiveFeedData.ContactString_vch# joined #ScrubbedSMS#CPP"/>
					<cfelse>
						<cfset activity = ScrubbedSMS/>
					</cfif>

					<cfset var item = [
						id = "#++count#",
						campaignName = "#Desc_vch#",
						list = "#GetActiveFeedData.SubscriberList#",
						template = "#GetActiveFeedData.BatchTemplate#",
						// userid = "#GetActiveFeedData.userid#",
						sms = "#activity#",
						date = "#dateFormat(GetActiveFeedData.dt, 'yyyy-mm-dd')# #timeFormat(GetActiveFeedData.dt, 'HH:mm:ss')#"
					] />

					<cfset arrayAppend(dataout["ActivityList"], item)>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get activity feed successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			
			<cfcatch>
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
                <cfset dataout.RXRESULTCODE= -1 />    
			</cfcatch>
		</cftry>
		
		<cfreturn SerializeJSON(dataout)  />
	</cffunction>

	<cffunction name="GetCampaignListForDashboard" access="remote" output="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">
		<cfargument name="inpUserId" required="false" default="#Session.UserId#">
		<cfargument name="isForReport" required="false" default="0">

		<cfset var emsLists	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetEMSStatus	= '' />
		<cfset var getKeywords	= '' />

		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>
		<cfset var GetEMS = 0>
		<cfset var total_pages = 1>
		<cfset var ownerType = 0 >
		<cfset var ownerId = 0 > 
		<cfset var RecipientCount = 0>
		<cfset var tempItem  = ArrayNew(1) >
		<cfset var GetNumbersCount  = 0 >
		<cfset var iFlashCount  = 0 >
		<cfset var PlayMyMP3_BD  = 0 >
		<cfset var PlayVoiceFlashHTML5  = 0 >
		<cfset var stopHtml  = 0 >
		<cfset var editHtml  = 0 >
		<cfset var cloneHtml  = 0 >
		<cfset var deleteHtml  = "" >
		<cfset var permissionStr  = 0 >
		<cfset var getUserByUserId  = 0 >
		<cfset var stopPermission  = 0 >
		<cfset var launchPermission  = 0 >
		<cfset var deletePermission  = 0 >
		<cfset var clonePermission  = 0 >
		<cfset var editPermission  = 0 >
		<cfset var order 	= "">
		<cfset var PausedCount 		= "">
		<cfset var PendingCount 		= "">
		<cfset var InProcessCount 		= "">
		<cfset var CompleteCount 		= "">
		<cfset var ContactResultCount 		= "">
		<cfset var IsScheduleInFuture 		= "">
		<cfset var IsScheduleInPast 		= "">
		<cfset var IsScheduleInCurrent 		= "">
		<cfset var LastUpdatedFormatedDateTime 		= "">
		<cfset var startDate 	= "">
		<cfset var endDate 	= "">
		<cfset var ContactTypes 		= "">
		<cfset var statusHtml 	= "">
		<cfset var ImageStatus 	= "">
		<cfset var StatusName 		= "">
		<cfset var errorMessage 		= "">
		<cfset var reportHtml 		= "">
		<cfset var GetCampaignSchedule 	= "">
		<cfset var GetPausedCount 		= "">
		<cfset var GetPendingCount 	= "">
		<cfset var GetInProcessCount 		= "">
		<cfset var GetCompleteCount 		= "">
		<cfset var GetContactResult 		= "">
		<cfset var GetDelivered 	= "">
		<cfset var GetListElligableGroupCount_ByType 		= "">
		<!---for checking and disabling none method EMS--->
		<cfset var disableEmailImage  = "" >
		<cfset var disableVoiceImage  = "" >
		<cfset var disableSmsImage  = "" >
        <cfset var DisplayDeliveredCount = "0" />
        <cfset var DisplayQueuedCount = "0" />
        <cfset var reviewHtml = "" />
        <cfset var LoadHtml = ""/>
        <cfset var BatchDescLink = '' />
        <cfset var batchListHasKeyword = ''>
        <cfset var KeywordBuff = '' />
        <cfset var DescBuff = '' />
        <cfset var GetQueueStatus = '' />
        <cfset var defaultShortCode = '' />
        
		
		<!--- remove old method: 
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode">
			<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
		</cfinvoke>
		
		<cftry>
			
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
		
			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS TOTALCOUNT
				FROM
					simpleobjects.batch AS b
					LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1)
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					b.Active_int > 0
				AND
					b.EMS_Flag_int IN (0,1,2)
				AND
					b.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
			   <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
			</cfquery>
			
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
		
		
		<!--- LEFT JOIN simpleobjects.batch_blast_log bl ON (bl.BatchId_bi = b.BatchId_bi) --->
		
		        <!--- Get ems data --->		
				<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
					SELECT
						b.BatchId_bi,
		             	b.RXDSLibrary_int,
		             	b.RXDSElement_int,
		             	b.RXDSScript_int,
		                b.DESC_VCH,
		                b.Created_dt,
		                b.ContactGroupId_int,
					 	b.ContactTypes_vch,
					 	b.ContactNote_vch, 
					 	b.ContactIsApplyFilter,
					 	b.ContactFilter_vch,
	                    b.EMS_Flag_int,
	                    b.XMLControlString_vch,
	                    k.KeywordId_int,
	                    k.Keyword_vch,
	                    bl.AmountLoaded_int,
	                    bl.Created_dt AS BlastCreated_dt,
	                    tc.Name_vch

					FROM
						simpleobjects.batch AS b
						LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1) 
						LEFT JOIN simpleobjects.batch_blast_log bl ON bl.PKId_int = (
						
																					    SELECT
																					      	MAX(fa.PKId_int) 
																					    FROM 
																					    	simpleobjects.batch_blast_log fa 
																					    WHERE 
																					    	fa.BatchId_bi = b.BatchId_bi
																					  )
						LEFT JOIN simpleobjects.templatecampaign tc ON tc.TID_int = b.TemplateId_int															  						
					WHERE        
						b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
					AND
						b.Active_int > 0
					AND
						b.EMS_Flag_int IN (0,1,2)
					AND
						b.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
				   <cfif customFilter NEQ "">
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
							AND 
								#PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
					<!---#order#
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">--->
					ORDER BY b.Created_dt DESC 
					<cfif arguments.isForReport EQ 0>
						LIMIT 5
					<cfelse>
						LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
					</cfif>
		        </cfquery>
	        
	        	<cfset var emsIds = []>
	        	<cfloop query="GetEMS">
	        		<cfset arrayAppend(emsIds,BatchId_bi)>
	        	</cfloop>

	        	<cfset emsLists = {}>

	        	<cfif arrayLen(emsIds) GT 0>

					<cfquery name="GetQueueStatus" datasource="#Session.DBSourceREAD#">
						SELECT BatchId_bi,count(PKId_int) as total,PKId_int
						FROM simpleobjects.batch_blast_log
						WHERE 
							Status_int = 2 -- Processing
						AND
							ActualLoaded_int < AmountLoaded_int
						GROUP BY BatchId_bi 
						HAVING BatchId_bi IN (#arrayToList(emsIds, ',')#)
					</cfquery>

		        	<cfif GetQueueStatus.RECORDCOUNT GT 0>
						<cfloop query="GetQueueStatus">
			        		<cfif total GT 0>
			        			<cfset emsLists['BS' & BatchId_bi] = 'In Process'>
			        			<cfset emsLists['PKID' & BatchId_bi] = PKId_int>
			        		</cfif>	
			        	</cfloop>
		        	</cfif>

				</cfif>

	        	<cfloop query="GetEMS">
		      		<cfif arguments.isForReport EQ 0>
						<cfif GetEMS.AmountLoaded_int GTE 0 >
			        		<cfset DescBuff = '<a class="" href="campaign-reports?campaignid=' & BatchId_bi & '">'& DESC_VCH &'</a>' & '<br/><span class="BlastText">Last Blast Sent ' & dateformat(GetEMS.BlastCreated_dt,'yyyy-mm-dd') & '</span>' />
			        	<cfelse>
			        		<cfset DescBuff = '<a class="" href="campaign-reports?campaignid=' & BatchId_bi & '">'& DESC_VCH &'</a>' />
			        	</cfif>
			        <cfelse>
			        	<cfset DescBuff = DESC_VCH />
			        </cfif>
		        	
		        	<cfif TRIM(GetEMS.Keyword_vch) NEQ "" >
		        		<cfset KeywordBuff = TRIM(GetEMS.Keyword_vch)/>
		        	<cfelse>
		        		<cfset KeywordBuff = '' />
		        	</cfif>
		        	
					<cfset tempItem = [
						'#DescBuff#',
						'#Name_vch#',
						'#KeywordBuff#'
					]>
					<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
				</cfloop>
	        </cfif>
      	<cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataOut)>
	</cffunction>

	<cffunction name="GetSentMessageForDashboard" access="remote" output="true">

		<cfargument name="inpUserId" required="false" default="#Session.USERID#"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.TOTALMESSAGE = 0 />
		<cfset var countSentMessage = '' />
		<cfset var defaultShortCode = '' />

		<cftry>
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>

			<cfquery name="countSentMessage" datasource="#Session.DBSourceREAD#">
				SELECT
					count(IREResultsId_bi) as totalSendMsg
				FROM 
					simplexresults.ireresults as ire
				WHERE
					ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND 
					ire.IREType_int = 1
				AND
					ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultShortCode.SHORTCODE#">
				AND 	
					LENGTH(ContactString_vch) < 14
			</cfquery>

			<cfset dataout.TOTALMESSAGE =  countSentMessage.totalSendMsg />
			
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = 'Get Sent Message successfully!' />
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>
				
		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetMLPShortURL" access="remote" output="true">
		<cfargument name="inpUserId" required="false" default="#Session.USERID#"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var getMLPViews = '' />
		<cfset var getMLPActive = '' />
		<cfset var getTotalShortUrl = '' />
		<cfset var getClickTimeShortUrl = '' />

		<cftry>
			<cfquery name="getMLPViews" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(mt.PKId_int) AS MLPVIEWS
				FROM 
					simplexresults.mlptracking mt
				JOIN
					simplelists.cppx_data cd
				ON
					cd.ccpxDataId_int = mt.ccpxDataId_int
				AND
					cd.status_int = 1
				WHERE				
					cd.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					cd.MlpType_ti = 1
			</cfquery>

			<cfquery name="getMLPActive" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(ccpxDataId_int) AS MLPACTIVE
				FROM 
					simplelists.cppx_data 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">  
				AND
					Status_int = 1
				AND
					MlpType_ti = 1	
			</cfquery>

			<cfquery name="getTotalShortUrl" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(PKId_bi) AS TOTALSHORTURL
				FROM 
					simplelists.url_shortner
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">  
				AND
					Active_int = 1
			</cfquery>

			<cfquery name="getClickTimeShortUrl" datasource="#Session.DBSourceREAD#">
				SELECT 
					SUM(ClickCount_int) AS TOTALCLICKSHORTURL
				FROM 
					simplelists.url_shortner
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">  
				AND
					Active_int = 1
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = 'Get MLP, SHORTURL information successfully!'/>
			<cfset dataout.MLPVIEWS = getMLPViews.MLPVIEWS />
			<cfset dataout.MLPACTIVE = getMLPActive.MLPACTIVE />
			<cfset dataout.TOTALSHORTURL = getTotalShortUrl.TOTALSHORTURL />
			<cfif getClickTimeShortUrl.TOTALCLICKSHORTURL EQ ''>
				<cfset getClickTimeShortUrl.TOTALCLICKSHORTURL = 0 />
			</cfif>
			<cfset dataout.TOTALCLICKSHORTURL = getClickTimeShortUrl.TOTALCLICKSHORTURL />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   	
			</cfcatch>
		</cftry>

		
		<cfreturn dataout />
 	</cffunction>

 	<cffunction name="GetSubscriberDetail" access="remote" output="true" hint="Get Subscriber Detail for Dashboard">
 		<cfset var dataout = {} />
 		<cfset dataout.RXRESULTCODE = ''/>
 		<cfset dataout.MESSAGE = '' />
 		<cfset dataout.ERRMESSAGE = '' />
 		<cfset dataout['LISTDETAIL'] = [] />
 		<cfset var getSubscriberDetail = '' />
 		<cfset var getTotalSubscriber = '' />
 		<cfset var defaultShortCode = '' />

 		<cftry>
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
			
 			<cfquery name="getSubscriberDetail" datasource="#Session.DBSourceREAD#">
 				SELECT
 					gl.GroupName_vch as Name,
 					COUNT(gcl.ContactAddressId_bi) as TotalSubscriber
 				FROM
 					simplelists.grouplist as gl
 				LEFT JOIN simplelists.groupcontactlist as gcl ON gl.GroupId_bi = gcl.GroupId_bi
 				WHERE gl.UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#Session.USERID#'>
 				AND gl.Active_int = 1
 				AND gl.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
 				GROUP BY
 				gl.GroupId_bi
 				ORDER BY
 				gl.Created_dt DESC
 				LIMIT 4
 			</cfquery>

 			<cfquery name="getTotalSubscriber" datasource="#Session.DBSourceREAD#">
	 			SELECT
					COUNT(DISTINCT gcl.ContactAddressId_bi) as AllSubscriber
				FROM
					simplelists.grouplist as gl
				INNER JOIN simplelists.groupcontactlist as gcl ON gl.GroupId_bi = gcl.GroupId_bi
				WHERE gl.UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#Session.USERID#'>
				AND gl.Active_int = 1
				AND gl.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
 			</cfquery>

 			<cfif getSubscriberDetail.RECORDCOUNT GT 0>
				<cfloop query="getSubscriberDetail">
					<cfset var tempItem = {}/>
					<cfset tempItem = 
						{
							NAME = '#getSubscriberDetail.Name#',
							TOTALSUBSCRIBER = '#getSubscriberDetail.TotalSubscriber#'
						}
					>
					<cfset ArrayAppend(dataout["LISTDETAIL"],tempItem)>
				</cfloop>
				
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Get Subscriber Detail successfully!' />
				<cfset dataout.ALLSUB = getTotalSubscriber.AllSubscriber />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No Detail found' />
				<cfset dataout.ALLSUB = getTotalSubscriber.AllSubscriber />
			</cfif>

 			<cfcatch>
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
                <cfset dataout.RXRESULTCODE= -1 />    
			</cfcatch>
 		</cftry>

 		<cfreturn dataout />
 	</cffunction>

 	<cffunction name="GetCampaignReport" access="remote" hint="Get campaign report with blast and keyword">

	    <cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">
		<cfargument name="isFirstLoad" required="no" default="">
		<cfargument name="customFilter" default="">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />

	    <cfset var dataout = {}/>
	    <cfset dataout.MESSAGE = ""/>
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.RXRESULTCODE = -1 />

	    <cfset var GetReport = '' />
	    <cfset var batchArray = '' />
	    <cfset var batchMessages = {} />

	    <cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var dataout = {} />
		<cfset var GetActiveFeedDataTotal = '' />
		<cfset var GetActiveFeedData = {} />
		<cfset var curUserID = arguments.inpUserID />
		<cfset var campaignBatchIds = "" />
		<cfset var batchIds = '' />
		<cfset var rsCount = '' />
		<cfset var getCampaignName = '' />
		<cfset var ScrubbedSMS = '' />
		<cfset var count = arguments.iDisplayStart/>
		<cfset var filterItem = '' />
		<cfset var page = (arguments.iDisplayStart/arguments.iDisplayLength) + 1/>
		<cfset var fieldName = '' />
		<cfset var batchId = ''/>
		<cfset var RQ = '' />
		<cfset var RetVarReadCP = '' />
		<cfset var defaultShortCode = '' />

		<cfset dataout["ActivityList"] = [] />

	    <cftry>
		    <!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
	        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
	            SELECT SQL_CALC_FOUND_ROWS
	            BatchId_bi,
	            Desc_vch,
	            GroupName_vch,
	            Created_dt,
	            Type_vch,
	            st.Name_vch as TemplateName_vch
	            FROM
	            (
	                (
	                    SELECT
	                    sb.BatchId_bi,
	                    sb.Desc_vch,
	                    sg.GroupName_vch,
	                    sbbl.Created_dt,
	                    sb.TemplateId_int,
	                    "Blast" AS Type_vch
	                    FROM
	                    simpleobjects.batch sb
	                    INNER JOIN
	                    simpleobjects.batch_blast_log sbbl
	                    ON
	                    sb.BatchId_bi = sbbl.BatchId_bi
	                    INNER JOIN
	                    simplelists.grouplist sg
	                    ON
	                    sbbl.GroupIds_vch = sg.GroupId_bi
	                    WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                    AND
	                    sb.Active_int = 1
	                    AND
	                    sbbl.Status_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PAUSED#">
						AND
                        sg.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
	                    ORDER BY
	                    	sbbl.Created_dt DESC
	                	<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						<cfelse>
							<!---LIMIT
								<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />--->
						</cfif>
	                    )
	                    
	                UNION
	                (
	                    SELECT
	                    sb.BatchId_bi,
	                    sb.Desc_vch,
	                    sg.GroupName_vch,
	                    ss.Created_dt,
	                    sb.TemplateId_int,
	                    "Optin" AS Type_vch
	                    FROM
	                    simpleobjects.batch sb
	                    INNER JOIN
	                    simplelists.subscriberbatch ss
	                    ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                    LEFT JOIN
	                    simplelists.grouplist sg
	                    ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                    WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                    AND
	                    sb.Active_int = 1
	                    AND
	                    ss.GroupId_bi > 0
						AND
                        sg.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
	                    ORDER BY
	                    	ss.Created_dt DESC
	                	<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						<cfelse>
							<!---LIMIT
								<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />--->
						</cfif>
	                    )
	                UNION
	               	(
	               		SELECT
	                    sb.BatchId_bi,
	                    sb.Desc_vch,
	                    sg.GroupName_vch,
	                    ss.Created_dt,
	                    sb.TemplateId_int,
	                    "NewType" AS Type_vch
	                    FROM
	                    simpleobjects.batch sb
	                    INNER JOIN
	                    simplelists.subscriberbatch ss
	                    ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                    LEFT JOIN
	                    simplelists.grouplist sg
	                    ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                    WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                    AND
	                    sb.Active_int = 1
	                    AND
	                    ss.GroupId_bi = 0
	                    ORDER BY
	                    	ss.Created_dt DESC
	                	<cfif arguments.isFirstLoad EQ 1>
							LIMIT 3
						<cfelse>
							<!---LIMIT
								<cfqueryparam cfsqltype="cf_sql_integer" value="#page*arguments.iDisplayLength#" />--->
						</cfif>
	               		)
	                    
	                ) AS Report
	            LEFT JOIN
	            simpleobjects.templatecampaign st
	            ON
	            Report.TemplateId_int = st.TID_int
	            GROUP BY
	            BatchId_bi
	            ORDER BY
	            	Created_dt DESC
	            <cfif arguments.isFirstLoad EQ 1>
					LIMIT 3
				<cfelse>
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
				</cfif>
	        </cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>

			<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
			<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

	        <cfif GetReport.RECORDCOUNT GT 0>
	            <cfset batchArray = listToArray(listRemoveDuplicates(valueList(GetReport.BatchId_bi)))/>

	            <cfloop array="#batchArray#" index="batchId">
	                <cfset batchMessages["#batchId#"] = ""/>
	                <cfloop from="1" to="10" index="RQ">
	                    <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCP">
	                        <cfinvokeargument name="inpBatchId" value="#batchId#"/>
	                        <cfinvokeargument name="inpQID" value="#RQ#"/>
	                    </cfinvoke>
	                    <cfif structKeyExists(RetVarReadCP.CPOBJ, "TEXT") AND RetVarReadCP.CPOBJ.TEXT NEQ "">
	                        <cfset batchMessages["#batchId#"] = RetVarReadCP.CPOBJ.TEXT/>
	                        <cfbreak/>
	                    </cfif>
	                </cfloop>
	            </cfloop>

	            <cfloop query="GetReport">
	                <cfset var item = [
	                	 "#GetReport.BatchId_bi#",
		                 "#GetReport.Desc_vch#",
		                 "#GetReport.GroupName_vch#",
		                 "#GetReport.TemplateName_vch#",
		                 "#batchMessages[GetReport.BatchId_bi]#",
		                 "#dateFormat(GetReport.Created_dt, 'mm/dd/yy')#"
	                ]/>

	                <!--- <cfset item.BatchId_bi = GetReport.BatchId_bi/>
	                <cfset item.Desc_vch = GetReport.Desc_vch/>
	                <cfset item.GroupName_vch = GetReport.GroupName_vch/>
	                <cfset item.TemplateName_vch = GetReport.TemplateName_vch/>
	                <cfset item.Message_vch = batchMessages[GetReport.BatchId_bi]/>
	                <cfset item.Created_dt = "#dateFormat(GetReport.Created_dt, 'yyyy-mm-dd')# #timeFormat(GetReport.Created_dt, 'HH:mm:ss')#"/> --->
	                
	                <cfset arrayAppend(dataout["ActivityList"], item)/>
	            </cfloop>
	        </cfif>

	        <cfset dataout.RXRESULTCODE = 1/>
	        <cfset dataout.MESSAGE = "Get report success"/>

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
	    </cftry>

	    <cfreturn SerializeJSON(dataout)/>
	</cffunction>

	<cffunction name="CountActivityFeed" access="remote" hint="Get campaign report with blast and keyword">

	    <cfset var dataout = {}/>
	    <cfset dataout.MESSAGE = ""/>
	    <cfset dataout.ERRMESSAGE = "" />
	    <cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.REPORT = [] />

	    <cfset var GetReport = '' />
	    <cfset var batchArray = '' />
	    <cfset var batchMessages = {} />

	    <cftry>

	        <cfquery name="GetReport" datasource="#session.DBSourceREAD#">
	            SELECT
	            BatchId_bi,
	            Desc_vch,
	            GroupName_vch,
	            Created_dt,
	            Type_vch,
	            st.Name_vch as TemplateName_vch
	            FROM
	            (
	                (
	                    SELECT
	                    sb.BatchId_bi,
	                    sb.Desc_vch,
	                    sg.GroupName_vch,
	                    sbbl.Created_dt,
	                    sb.TemplateId_int,
	                    "Blast" AS Type_vch
	                    FROM
	                    simpleobjects.batch sb
	                    INNER JOIN
	                    simpleobjects.batch_blast_log sbbl
	                    ON
	                    sb.BatchId_bi = sbbl.BatchId_bi
	                    INNER JOIN
	                    simplelists.grouplist sg
	                    ON
	                    sbbl.GroupIds_vch = sg.GroupId_bi
	                    WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                    AND
	                    sb.Active_int = 1
	                    AND
	                    sbbl.Status_int > 0
	                    )
	                UNION
	                (
	                    SELECT
	                    sb.BatchId_bi,
	                    sb.Desc_vch,
	                    sg.GroupName_vch,
	                    ss.Created_dt,
	                    sb.TemplateId_int,
	                    "Optin" AS Type_vch
	                    FROM
	                    simpleobjects.batch sb
	                    INNER JOIN
	                    simplelists.subscriberbatch ss
	                    ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                    LEFT JOIN
	                    simplelists.grouplist sg
	                    ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                    WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                    AND
	                    sb.Active_int = 1
	                    AND
	                    ss.GroupId_bi > 0
	                    )
	                ) AS Report
	            LEFT JOIN
	            simpleobjects.templatecampaign st
	            ON
	            Report.TemplateId_int = st.TID_int
	        </cfquery>
	        
	        <cfset dataout.COUNT = GetReport.RECORDCOUNT/>

	        <cfset dataout.RXRESULTCODE = 1/>
	        <cfset dataout.MESSAGE = "Get report success"/>

	        <cfcatch TYPE="any">
	            <cfif cfcatch.errorcode EQ "">
	                <cfset cfcatch.errorcode = -1 />
	            </cfif>

	            <cfset dataout.RXRESULTCODE = cfcatch.errorcode/>
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
	    </cftry>

	    <cfreturn SerializeJSON(dataout)/>

	</cffunction>

	<cffunction name="ShortUrlClickList" access="remote" hint="Short URL click detail list">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default=""/>
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />
		
		<cfset var temp = {} />
		<cfset var filterItem = {}/>
        <cfset var getAllRecord = '' />
		<cfset var temp = '' />
		<cfset var filterItem = '' />
		
        <cfset var rsCount = '' />
		<cfset var ShortURLSite = "http://mlp-x.com" />

		<cftry>
			<cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                	SQL_CALC_FOUND_ROWS
                    ShortURL_vch,
                    Desc_vch,
                    TargetURL_vch,
                    DynamicData_vch,
                    Created_dt,
                    ClickCount_int,
                    LastClicked_dt
                FROM
                    simplelists.url_shortner
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                	Active_int = 1
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
                
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    URL = ShortURLSite&"/#ShortURL_vch#",
                    DESC = '#Desc_vch#',
                    TARGET = '#TargetURL_vch#',
                    DYNAMICDATA = '#DynamicData_vch#',
                    COUNT = '#ClickCount_int#',
                    CREATED = '#DateFormat(Created_dt, "yyyy-mm-dd")# #TimeFormat(Created_dt, "HH:mm:ss")#',
                    LASTCLICK = '#LastClicked_dt#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

			<cfcatch TYPE="any">
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="MLPViewList" access="remote" hint="MLP View detail list">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default=""/>
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RESULT = 0 />
        <cfset dataout.RXRESULTCODE = -1 />

		<cfset var temp = {} />
		<cfset var filterItem = {}/>
        <cfset var getAllRecord = '' />
		<cfset var temp = '' />
		<cfset var filterItem = '' />
		

		<cfset var order = '' />
        <cfset var rsCount = '' />
		<cfset var ShortURLSite = "https://mlp-x.com/lz" />

		<cftry>
			<cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllRecord" datasource="#Session.DBSourceREAD#">
                SELECT
                	SQL_CALC_FOUND_ROWS
                    COUNT(mp.PKId_int) as MLPVIEWCOUNT,
                    cd.cppxName_vch,
					cd.cppxURL_vch,
                    cd.created_dt
                FROM
                	simplexresults.mlptracking as mp
                JOIN
                	simplelists.cppx_data as cd
                ON
                	cd.ccpxDataId_int = mp.ccpxDataId_int
                AND
                	cd.status_int = 1
                WHERE
                    mp.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
                    </cfloop>
                </cfif>
                GROUP BY
                	mp.ccpxDataId_int
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
                
            </cfquery>
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="#getAllRecord#">

                <cfset temp = {
                    NAME = '#cppxName_vch#',
                    URL = ShortURLSite&"/#cppxURL_vch#",
                    COUNT = '#MLPVIEWCOUNT#',
                    CREATED = '#DateFormat(Created_dt, "yyyy-mm-dd")# #TimeFormat(Created_dt, "HH:mm:ss")#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

			<cfcatch TYPE="any">
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
	<cffunction name="GetAllSubAccount" access="remote" output="false" hint="">
		<cfargument name="inpIncludeSubAccLv" type="string" required="true" default="0">
		<cfargument name="inpUserId" type="string" required="false" default="#Session.USERID#">
		<cfargument name="inpForceFilterByMasterUser" type="string" required="false" default="0" hint="for filter by master account">
		<cfset var dataout = {} />            	

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
		<cfset dataout.LISTSUBACCINTEGRATE = '' />
		<cfset dataout.LISTSUBACCSIRE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />                       
        
		
		<cfset var ListAllSubAccSire = '' /> 		
		<cfset var GetListSubAccSire= '' /> 
		<cfset var ListAccountFilterByLevel= '' /> 
		<cfset var GetOwnerInfo = '' /> 				
		<cfset var UserLevel = '' /> 		
		<cfset var i = '' /> 	
		<cfset var RetVarGetAdminPermission={}>
		<cftry>
			<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">    
				<cfinvokeargument  name="inpSkipRedirect"  value="1">
			</cfinvoke>
			
			<cfif RetVarGetAdminPermission.ISADMINOK EQ 1 AND inpForceFilterByMasterUser EQ 0>
				<cfquery name="GetOwnerInfo" datasource="#Session.DBSourceREAD#">
					SELECT					
						UserLevel_int,
						UserId_int					
					FROM
						simpleobjects.useraccount
					WHERE
						UserType_int=  2 
					AND
						UserLevel_int=1
				</cfquery> 
				<cfset UserLevel= 1>	
				<cfloop query="GetOwnerInfo">
					<cfset ListAllSubAccSire= ListAppend(ListAllSubAccSire, UserId_int)/>
				</cfloop>
			<cfelse>
				<cfquery name="GetOwnerInfo" datasource="#Session.DBSourceREAD#">
					SELECT					
						UserLevel_int					
					FROM
						simpleobjects.useraccount
					WHERE
						UserId_int=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 
				</cfquery>      			          
				<cfset UserLevel= GetOwnerInfo.UserLevel_int>			
				<cfset ListAllSubAccSire= ListAppend(ListAllSubAccSire, arguments.inpUserId)/>
			</cfif>
			
			
			<cfloop from="1" to="3" index="i">
				<cfif i GTE UserLevel>
					<!--- get all sub acc--->
					<cfquery name="GetListSubAccSire" datasource="#Session.DBSourceREAD#">
						SELECT
							UserId_int							
						FROM
							simpleobjects.useraccount
						WHERE
							HigherUserID_int IN( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )
						AND
							UserLevel_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#i#">						
					</cfquery>					
					<cfloop query="GetListSubAccSire">
						<cfset ListAllSubAccSire= ListAppend(ListAllSubAccSire, UserId_int)/>
					</cfloop>
				</cfif>
			</cfloop>	
			<cfif arguments.inpIncludeSubAccLv GT 0>
				<cfquery name="ListAccountFilterByLevel" datasource="#Session.DBSourceREAD#">
					SELECT
						UserId_int							
					FROM
						simpleobjects.useraccount
					WHERE
						UserId_int IN( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )
					AND
						UserLevel_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIncludeSubAccLv#">						
				</cfquery>	
				<!--- reset list--->		
				<cfset ListAllSubAccSire="">
				<cfloop query="ListAccountFilterByLevel">
					<cfset ListAllSubAccSire= ListAppend(ListAllSubAccSire, UserId_int)/>
				</cfloop>
			</cfif>
			
								
			<cfset dataout.LISTSUBACCSIRE = ListAllSubAccSire/>
			<cfset dataout.RXRESULTCODE = 1/>
			<cfcatch TYPE="any">
	            <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

	        </cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
	<cffunction name="GetAllReportCount" access="remote" output="false" hint="Get user plan report on entire system">
        <cfargument name="inpStartDate" type="string" required="true" default="">
        <cfargument name="inpEndDate" type="string" required="true" default="">
		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">

        <cfset var dataout = {} />        
    	<cfset var countSentMessage = '' /> 
		<cfset var TotalOPTIn = '' /> 
		<cfset var TotalOPTOut = '' /> 
		<cfset var VarGetAllSubAccount = '' /> 
		       
      
        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = '' />
        
        
        <cfset dataout.MESSAGESENT = 0/>
        <cfset dataout.MESSAGERECEIVED = 0/>
        <cfset dataout.TOTALOPTIN = 0/>
        <cfset dataout.TOTALOPTOUT = 0/>        
		<cfset dataout.TOTALUSER = 0/>        
        		
		<cfset var ListAllSubAccSire = '' /> 
		
					
		
        <cftry>   
			<cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>                
            </cfinvoke>						
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>
			<cfset dataout.TOTALUSER = listlen(ListAllSubAccSire)/>        					
			
		    <cfquery name="TotalOPTIn" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalOptIn
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					b.batchid_bi = oi.BatchId_bi 
				LEFT JOIN
					simpleobjects.useraccount as ua
				ON
					ua.UserId_int = b.UserId_int
				WHERE 
					oi.OptIn_dt IS NOT NULL 
				AND 
					oi.OptOut_dt IS NULL
				AND
                    ua.IsTestAccount_ti = 0		
				AND 
					ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
				AND
                    LENGTH(oi.ContactString_vch) < 14
				<cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
					AND
	                    oi.OptIn_dt
	                BETWEEN 
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
	                AND 
	                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
	            </cfif>
			</cfquery>
                
            <cfset dataout.TOTALOPTIN = TotalOPTIn.TotalOptIn />            
            
            <cfquery name="TotalOPTOut" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalOptOut
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					b.batchid_bi = oi.BatchId_bi 
				LEFT JOIN
					simpleobjects.useraccount as ua
				ON
					ua.UserId_int = b.UserId_int
				WHERE 
					OptOut_dt IS NOT NULL 
				AND 
					OptIn_dt IS NULL
				AND
                    ua.IsTestAccount_ti = 0
				AND 
					ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	

				<cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
					AND
	                    oi.OptOut_dt
	                BETWEEN 
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
	                AND 
	                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
	            </cfif>
				 AND
                    LENGTH(oi.ContactString_vch) < 14
		    </cfquery>

            <cfset dataout.TOTALOPTOUT = TotalOPTOut.TotalOptOut />

            <cfquery name="countSentMessage" datasource="#Session.DBSourceREAD#">
                SELECT
                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                FROM 
                    simplexresults.ireresults as ire 
                
                WHERE 
                    ire.IREType_int IN (1,2)				
                AND
                    LENGTH(ire.ContactString_vch) < 14                
				AND 
					ire.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                AND
                    ire.Created_dt

                BETWEEN 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND 
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
            </cfquery>

            <cfset dataout.MESSAGESENT = (countSentMessage.Sent EQ '') ? 0 : countSentMessage.Sent/>
            <cfset dataout.MESSAGERECEIVED =  (countSentMessage.Received EQ '') ? 0 : countSentMessage.Received/>                                   

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset  dataout.TYPE = "#cfcatch.TYPE#" />
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>
	<cffunction name="GetTotalSentReceivedMessageList" access="remote" hint="Get total sent-received message list">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <!---
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        --->
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getAllUserIdInIreResults = '' />
        <cfset var getUserInfo = '' />
        <cfset var getTotalSentReceivedMessageAllUser = '' />
        <cfset var getSentReceivedMessageByUser = '' />
        <cfset var rsCount = '' />
        <cfset var filterItem = '' />
        <cfset var temp = {} />
        <cfset var totalSent = 0/>
        <cfset var totalReceived = 0/>

        <cfset var CheckSummaryDataByJob = ''/>
        <cfset var MaxSummaryDate = ''/>
		<cfset var MinSummaryDate = ''/>
		<cfset var OneDayAfterMaxSummaryDate = ''/>
		<cfset var QsCountTotalRows = ''/>
		<cfset var VarGetAllSubAccount = ''/>

		
		<cfset var ListAllSubAccSire = '' /> 
		
        

        <cftry>
			<cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>                
            </cfinvoke>			
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>

            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>
            <cfquery name="CheckSummaryDataByJob" datasource="#Session.DBSourceREAD#">
                SELECT  MAX(SummaryDate_dt) AS MaxSummaryDate, MIN(SummaryDate_dt) as MinSummaryDate
                FROM    simplexresults.summary_total_send_received_by_date                
            </cfquery>
            <cfset MaxSummaryDate=CheckSummaryDataByJob.MaxSummaryDate>
			<cfset MinSummaryDate=CheckSummaryDataByJob.MinSummaryDate>
            <cfif MaxSummaryDate EQ "" OR DateDiff( "d", MaxSummaryDate, startDate ) GT 0 OR DateDiff( "d", startDate, MinSummaryDate ) GT 0 >                                  
                <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        ire.UserId_int,
                        ua.EmailAddress_vch,
                        ua.MFAContactString_vch,
                        ua.FirstName_vch,
                        ua.LastName_vch,
                        SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                        SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                    FROM
                        simplexresults.ireresults as ire 
                    LEFT JOIN
                        simpleobjects.useraccount as ua
                    ON
                        ire.UserId_int = ua.UserId_int					
                    WHERE
                        LENGTH(ire.ContactString_vch) < 14
					AND 
						ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                    AND
                        ua.IsTestAccount_ti = 0
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.NAME EQ 'ire.UserId_int'>
                            AND 
                                #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                            <cfelse>
								<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
								<cfelse>
									AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
								</cfif>
                            </cfif>
                        </cfloop>
                    </cfif>
                    AND
                        ire.Created_dt
                    BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                    AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                    GROUP BY
                        ire.UserId_int
                    HAVING
                        (
                            Sent > 0
                            OR
                            Received > 0
                        )
                    <cfif arguments.inpSkipPaging EQ 0>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                    </cfif>
                </cfquery>   
				
                 <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>

                <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
                <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>             
            <cfelse>  
                <cfif DateDiff( "d", MaxSummaryDate, endDate ) GT 0>		
			        <cfset OneDayAfterMaxSummaryDate= DateAdd("d",1,MaxSummaryDate)>	 <!--- 1 days after max day--->
                     <cfquery name="QsCountTotalRows" datasource="#Session.DBSourceREAD#">
                        SELECT                            
                            UserId_int,
                            SUM(Sent) AS Sent,
                            SUM(Received) as Received
                        FROM
                        (
                            SELECT
                                
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(ire.TotalSent_int) AS Sent,
                                SUM(ire.TotalReceived_int) AS Received
                            FROM
                                simplexresults.summary_total_send_received_by_date as ire
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE                            
                                ua.IsTestAccount_ti = 0
							AND 
								ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND 
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
										<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										<cfelse>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.SummaryDate_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#MaxSummaryDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                            UNION ALL
                            SELECT                                
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                                SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                            FROM
                                simplexresults.ireresults as ire 
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                LENGTH(ire.ContactString_vch) < 14
                            AND
                                ua.IsTestAccount_ti = 0
							AND 
								ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND 
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
										<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										<cfelse>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.Created_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#OneDayAfterMaxSummaryDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                        ) as alldata
                        GROUP BY
                                UserId_int                        
                     </cfquery>
                    
                    <cfset dataout["iTotalDisplayRecords"] = QsCountTotalRows.RECORDCOUNT/>
                    <cfset dataout["iTotalRecords"] = QsCountTotalRows.RECORDCOUNT/>
                    <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                        SELECT                            
                            UserId_int,
                            EmailAddress_vch,
                            MFAContactString_vch,
                            FirstName_vch,
                            LastName_vch,
                            SUM(Sent) AS Sent,
                            SUM(Received) as Received
                        FROM
                        (
                            SELECT
                                
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(ire.TotalSent_int) AS Sent,
                                SUM(ire.TotalReceived_int) AS Received
                            FROM
                                simplexresults.summary_total_send_received_by_date as ire
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE                            
                                ua.IsTestAccount_ti = 0
							AND 
								ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND 
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
										<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										<cfelse>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.SummaryDate_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#MaxSummaryDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                            UNION ALL
                            SELECT                                
                                ire.UserId_int as UserId_int,
                                ua.EmailAddress_vch as EmailAddress_vch,
                                ua.MFAContactString_vch as MFAContactString_vch,
                                ua.FirstName_vch as FirstName_vch,
                                ua.LastName_vch as LastName_vch,
                                SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                                SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                            FROM
                                simplexresults.ireresults as ire 
                            LEFT JOIN
                                simpleobjects.useraccount as ua
                            ON
                                ire.UserId_int = ua.UserId_int
                            WHERE
                                LENGTH(ire.ContactString_vch) < 14
                            AND
                                ua.IsTestAccount_ti = 0
							AND 
								ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                            <cfif customFilter NEQ "">
                                <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                    <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                    AND 
                                        #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                    <cfelse>
										<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										<cfelse>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
                                    </cfif>
                                </cfloop>
                            </cfif>
                            AND
                                ire.Created_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#OneDayAfterMaxSummaryDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                            GROUP BY
                                UserId_int
                            HAVING
                                (
                                    Sent > 0
                                    OR
                                    Received > 0
                                )
                        ) as alldata
                        GROUP BY
                                UserId_int
                        <cfif arguments.inpSkipPaging EQ 0>
                            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                        </cfif>
                    </cfquery>
                    
                <cfelse>
                    <cfquery name="getAllUserIdInIreResults" datasource="#Session.DBSourceREAD#">
                        SELECT
                            SQL_CALC_FOUND_ROWS
                            ire.UserId_int,
                            ua.EmailAddress_vch,
                            ua.MFAContactString_vch,
                            ua.FirstName_vch,
                            ua.LastName_vch,
                            SUM(ire.TotalSent_int) AS Sent,
                            SUM(ire.TotalReceived_int) AS Received
                        FROM
                            simplexresults.summary_total_send_received_by_date as ire
                        LEFT JOIN
                            simpleobjects.useraccount as ua
                        ON
                            ire.UserId_int = ua.UserId_int
                        WHERE                            
                            ua.IsTestAccount_ti = 0
						AND 
							ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.NAME EQ 'ire.UserId_int'>
                                AND 
                                    #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                                <cfelse>
									<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
										AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
									<cfelse>
										AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
									</cfif>
                                </cfif>
                            </cfloop>
                        </cfif>
                        AND
                            ire.SummaryDate_dt
                        BETWEEN 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                        AND 
                            DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        GROUP BY
                            ire.UserId_int
                        HAVING
                            (
                                Sent > 0
                                OR
                                Received > 0
                            )
                        <cfif arguments.inpSkipPaging EQ 0>
                            LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                        </cfif>
                    </cfquery>
                     <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                        SELECT FOUND_ROWS() AS iTotalRecords
                    </cfquery>

                    <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
                    <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
                </cfif>
            </cfif>                       

            <cfloop query="getAllUserIdInIreResults">
                <cfset temp = {
                    USERID = '#getAllUserIdInIreResults.UserId_int#',
                    PHONE = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.MFAContactString_vch#',
                    NAME = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.FirstName_vch#' & ' #getAllUserIdInIreResults.LastName_vch#',
                    EMAIL = '#getAllUserIdInIreResults.UserId_int#' EQ '' ? 'Undefined' : '#getAllUserIdInIreResults.EmailAddress_vch#',
                    SENT = '#Sent#',
                    RECEIVED = '#Received#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            
            
            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
	<cffunction name="GetSummarySentReceivedMessage" access="remote" hint="Get campaign and total sent-received messages">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="numeric" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var temp = {} />
        <cfset var getAllBatch = '' />
        <cfset var getSentMessageByBatch = '' />
        <cfset var getReceivedMessageByBatch = '' />
        <cfset var rowName = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var getSMSChatKeyword = '' />
        <cfset var totalSent = 0/>
        <cfset var totalReceived = 0/>
        <cfset var ireList = []/>
		
        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>

            <cfquery name="getAllBatch" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ire.BatchId_bi as BatchId,
                    ba.Desc_vch as Desc_vch,
                    ire.IREResultsId_bi,
                    ire.UserId_int as UserId,
                    SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                    SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                FROM
                    simplexresults.ireresults as ire 
                LEFT JOIN
                    simpleobjects.batch as ba
                ON
                    ba.BatchId_bi = ire.BatchId_bi
                WHERE
                    ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                AND
                    LENGTH(ire.ContactString_vch) < 14
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'ire.BatchId_bi'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
							<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
                        </cfif>
                    </cfloop>
                </cfif>
                AND
                    ire.Created_dt
                BETWEEN 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                AND 
                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)

                GROUP BY
                    ire.BatchId_bi
                HAVING
                    (
                        Sent > 0
                        OR
                        Received > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getAllBatch">

                <cfset rowName = '#getAllBatch.Desc_vch#'/>

                <cfif getAllBatch.Desc_vch EQ '' AND getAllBatch.BatchId NEQ 0>
                <!--- SMS chat campaign --->
                    <cfquery name="getSMSChatKeyword" datasource="#Session.DBSourceREAD#">
                        SELECT
                            Keyword_vch
                        FROM
                            sms.keyword
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#getAllBatch.BatchId#'>
                        LIMIT
                            1
                    </cfquery>

                    <cfif getSMSChatKeyword.RECORDCOUNT GT 0 AND getSMSChatKeyword.Keyword_vch NEQ ''>
                        <cfset rowName = 'SMS Chat - Keyword: #getSMSChatKeyword.Keyword_vch#'/>
                    <cfelse>
                        <cfset rowName = 'SMS Chat - ID: #getAllBatch.BatchId#'/>
                    </cfif>
                <cfelseif getAllBatch.Desc_vch EQ '' AND getAllBatch.BatchId EQ 0>
                <!--- System message campaign --->
                    <cfset rowName = 'System message(s)'/>
                </cfif>

                <cfset temp = {
                    NAME = '#rowName#',
                    SENT = '#getAllBatch.Sent#',
                    RECEIVED = '#getAllBatch.Received#',
                    USERID = '#getAllBatch.UserId#',
                    BATCHID = '#getAllBatch.BatchId#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch> 
        </cftry>

        <cfreturn dataout />
    </cffunction>
	<cffunction name="GetBillingHistoryList" access="remote" output="true" hint="Get Billing history list">
		<cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">
        <!--- <cfargument name="sEcho"> --->
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = '' />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout["BILLINGHISTORYLIST"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />

        <cfset var getList = '' />
        <cfset var ListAllSubAccSire = '' />
        <cfset var amount = '' />
        <cfset var status = '' />
        <cfset var filecontentParseResult = {} />
		<cfset var rsCount = '' />
		<cfset var VarGetAllSubAccount = '' />
		<Cfset var myXMLDocument = ''/>
        <Cfset var processorResponseCode = ''/>
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var paymentdataXML = ''/>
        <Cfset var orderAmount = ''/>
		
		<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cftry>
            <cfif arguments.iDisplayStart < 0>
                <cfset arguments.iDisplayStart = 0>
            </cfif>
           <cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>                
            </cfinvoke>			
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>

            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
					SQL_CALC_FOUND_ROWS
                    id,
                    created,
                    filecontent,
                    moduleName,
                    status_text,
					UserId_int,
					paymentdata,
					PaymentGateway_ti
                FROM
                    simplebilling.log_payment_worldpay
                WHERE                    
					UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )		
				AND
                    created
					BETWEEN 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
					AND 
						DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)			
                AND
                    moduleName = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Recurring">
                ORDER BY created DESC
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
            </cfquery>
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfif getList.RECORDCOUNT GT 0>
                <cfloop query="getList">
						<cfif PaymentGateway_ti EQ _WORLDPAY_GATEWAY>
							<cfset filecontentParseResult = deserializeJSON(getList.filecontent) />						
                            <cfif getList.status_text NEQ 'OK'>
                                <cfset amount = 0/>
                                <cfset status = getList.status_text/>
                            <cfelse>
                                <cfset amount = filecontentParseResult.transaction.authorizedAmount/>
                                <cfset status = filecontentParseResult.result/>
                            </cfif>
                        <cfelseif PaymentGateway_ti EQ _PAYPAL_GATEWAY><!--- Paypal --->
							<cfset status = getList.status_text/>
							<cfif structKeyExists(deserializeJSON(getList.paymentdata), "MONEY")>								
								<cfset amount= deserializeJSON(getList.paymentdata).MONEY>						
							<cfelse>
								<cfset amount = 0/>
							</cfif>
						<cfelseif PaymentGateway_ti EQ _MOJO_GATEWAY>										
							<cfset myXMLDocument = XmlParse(getList.filecontent)> 
							<cfset processorResponseCode = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
							<cfset status = getList.status_text/>
							<cfset amount = 0/>
							<cfif ArrayLen(processorResponseCode) GT 0>			
								<cfset processorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
								<cfif ArrayLen(processorResponse) GT 0>			
									<cfset status = processorResponse[1].XmlText/>      
								</cfif>	
							<cfelse>								
								<cfset statusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
								<cfif ArrayLen(statusMessage) GT 0>			
									<cfset status = statusMessage[1].XmlText/>      
								</cfif>
							</cfif>	
							<cfset paymentdataXML = XmlParse(deserializeJSON(getList.paymentdata))> 
							<cfset orderAmount = XmlSearch(paymentdataXML, "/UAPIRequest/requestData/orderAmount")>
							<cfif ArrayLen(orderAmount) GT 0>			
								<cfset amount = orderAmount[1].XmlText/>      
							</cfif>
						<cfelseif PaymentGateway_ti EQ _TOTALAPPS_GATEWAY>					
							<cfset status = getList.status_text/>
							<cfif structKeyExists(deserializeJSON(getList.paymentdata), "INPAMOUNT")>								
								<cfset amount= deserializeJSON(getList.paymentdata).INPAMOUNT>						
							<cfelse>
								<cfset amount = 0/>
							</cfif>
                        </cfif>


                        <cfset tempItem = 
                            {
                                ID = '#getList.id#',
                                DATE = '#DateFormat(getList.created, 'mm/dd/yyyy')#',
                                AMOUNT = '#amount#',
                                PRODUCT = '#getList.moduleName#',
                                STATUS = '#status#',
								SUBACCID=UserId_int
                            }
                        >
                        <cfset ArrayAppend(dataout["BILLINGHISTORYLIST"],tempItem)>
                </cfloop>
                
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get billing history list successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No record found' />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
	<cffunction 
        name="getUsers"
        access="remote"
        returntype="struct" 
        output="false"
        hint="Return new users list."
        >
        <cfargument name="inpUserId" type="string" required="false" default="#Session.USERID#">
		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">
		<cfargument name="inpForceFilterByMasterUser" type="string" required="false" default="0">
		
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var getNewUsers = ''/>
        <cfset var filterItem = '' />
        <cfset var countReport = '' />
		<cfset var rsCount=''/>		
		<cfset var ListAllSubAccSire=''/>
		<cfset var VarGetAllSubAccount=''/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        

        <cftry>
			
            <cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>     
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>  
				<cfinvokeargument name="inpForceFilterByMasterUser" value="#arguments.inpForceFilterByMasterUser#"/>  
				   
            </cfinvoke>			
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>

            <cfquery name="getNewUsers" datasource="#Session.DBSourceREAD#">
                SELECT 
					SQL_CALC_FOUND_ROWS
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch
                FROM 
                    `simpleobjects`.`useraccount` ua
                WHERE 
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0                
                AND
					ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )	
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND 
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND 
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC                
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">                
            </cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
			
        
            <cfif getNewUsers.RECORDCOUNT GT 0>
                <cfloop query="getNewUsers">
                    <cfset var tempItem = {
                        USERID = '#getNewUsers.UserId_int#',
                        USEREMAIL =  '#getNewUsers.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getNewUsers.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getNewUsers.FirstName_vch# #getNewUsers.LastName_vch#",
                        FIRSTNAME = "#getNewUsers.FirstName_vch#",
                        LASTNAME = "#getNewUsers.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getNewUsers.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getNewUsers.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getNewUsers.Active_int#",
                        CONTACTSTRING = "#getNewUsers.MFAContactString_vch#"
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        
        <cfreturn dataout/>

    </cffunction>	
	<cffunction name="GetOptOutListByDateRange" access="remote" hint="Get a list opt out by date range">        
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="0" />
        <cfargument name="customFilter" default="">
		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">
        <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>

        <cfset var dataout = {} />
        <cfset var getList = '' />
        <cfset var rsCount = '' />
        <cfset var tmp = {} />
        <cfset dataout['DATALIST'] = [] />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var filterItem = ''/>
		<cfset var ListAllSubAccSire=''>
		<cfset var VarGetAllSubAccount	= '' />

        <cftry>
			<cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>                
            </cfinvoke>			
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>
            <cfquery name="getList" datasource="#Session.DBSourceEBM#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    oi.ContactString_vch as Phone,                 
                    DATE_FORMAT(oi.OptOut_dt,'%b %d %Y %T') AS Date,
                    b.UserId_int AS UserId,
                    b.BatchId_bi AS BatchId,
                    b.Desc_vch AS CamaignName
                FROM 
                    simplelists.optinout  AS oi 
                INNER JOIN 
                    simpleobjects.batch as b 
                ON  
                    b.BatchId_bi = oi.BatchId_bi
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = b.UserId_int

                WHERE   
					ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )		              
				AND
                    oi.OptOut_dt IS NOT NULL
                AND
                    oi.OptIn_dt IS NULL                                            
                AND
                    LENGTH(oi.ContactString_vch) < 14
                AND
                    oi.OptOut_dt 
                    BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                    AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)            

                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                        </cfif>
                    </cfloop>
                </cfif>
                GROUP BY ContactString_vch, OptOut_dt
                ORDER BY
                    oi.OptOut_dt DESC

                <cfif arguments.inpSkipPaging EQ 0>
                    <cfif arguments.iDisplayStart GTE 0 AND arguments.iDisplayLength GT 0>
                        LIMIT 
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                        OFFSET 
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
                    </cfif>
                </cfif>
               
                
            </cfquery>
            <cfif arguments.OutQueryResults EQ "1"> 
                <cfset DataOut.QUERYRES = getList />
            </cfif>
            <cfif getList.RECORDCOUNT GT 0>
                <cfquery datasource="#Session.DBSourceEBM#" name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>

                <cfloop query="rsCount">
                    <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                    <cfset dataout["iTotalRecords"] = iTotalRecords>
                </cfloop>

                <cfloop query="#getList#">
                    <cfset tmp = {
                        // ID = Id_int,
                        PHONE = Phone,
                        DATE = Date,
                        USERID = UserId,
                        BATCHID = BatchId,
                        CAMPAIGNNAME = CamaignName                   
                    }/>

                    <cfset arrayAppend(dataout['DATALIST'], tmp)/>
                </cfloop>
                
            </cfif>
            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
	<cffunction name="GetOptInOutList" access="remote" hint="Get opt in-out list for admin">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>
		<cfargument name="inpIncludeSubAccLv" type="string" required="false" default="0">
		

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {}/>
		<cfset var ListAllSubAccSire=''>
		<cfset var VarGetAllSubAccount	= '' />

        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>
			<cfinvoke method="GetAllSubAccount" returnvariable="VarGetAllSubAccount">
                <cfinvokeargument name="inpIncludeSubAccLv" value="#arguments.inpIncludeSubAccLv#"/>                
            </cfinvoke>			
			<cfset ListAllSubAccSire= VarGetAllSubAccount.LISTSUBACCSIRE/>
            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    b.UserId_int,
                    ua.EmailAddress_vch,
                    ua.MFAContactString_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    SUM(IF(oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL, 1, 0)) AS OptIn,
                    SUM(IF(oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL, 1, 0)) AS OptOut
                FROM 
                    simplelists.optinout AS oi 
                INNER JOIN 
                    simpleobjects.batch as b ON 
                    (b.BatchId_bi = oi.BatchId_bi)
                INNER JOIN
                    simpleobjects.useraccount as ua
                ON
                    ua.UserId_int = b.UserId_int
                WHERE
					ua.UserId_int IN ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListAllSubAccSire#"  list="yes" > )		
				AND
                    ua.UserId_int > 0                
                AND
                    LENGTH(oi.ContactString_vch) < 14
                AND
                    ua.IsTestAccount_ti = 0
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'b.UserId_int'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
                    AND
                        CASE WHEN
                            (oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL)
                        THEN
                            oi.OptIn_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                    AND
                        CASE WHEN
                            (oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL)
                        THEN
                            oi.OptOut_dt
                                BETWEEN 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                                AND 
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                </cfif>
                GROUP BY
                    b.UserId_int
                HAVING
                    (
                        OptIn > 0
                        OR
                        OptOut > 0
                    )                    
                <cfif arguments.inpSkipPaging EQ 0>
               
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getList">

                <cfset temp = {
                    USERID = '#getList.UserId_int#',
                    PHONE = '#getList.MFAContactString_vch#',
                    NAME = '#getList.FirstName_vch#' & ' #getList.LastName_vch#',
                    EMAIL = '#getList.EmailAddress_vch#',
                    OPTIN = '#getList.OptIn#',
                    OPTOUT = '#getList.OptOut#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            
            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
	<cffunction name="GetOptInOutSummaryList" access="remote" hint="Get opt in-out list for admin">
        <cfargument name="inpStartDate" type="string" required="true" hint=""/>
        <cfargument name="inpEndDate" type="string" required="true" hint=""/>
        <cfargument name="inpUserId" type="string" required="true" hint=""/>

        <cfargument name="customFilter" default=""/>		

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
        <cfset var dataout = {}/>
        <cfset dataout["DATALIST"] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />

        <cfset var getList = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />
        <cfset var temp = {}/>
		
        <cftry>
            <cfif arguments.iDisplayStart LT 0>
                <cfset arguments.iDisplayStart = 0 />
            </cfif>
			
            <cfquery name="getList" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    b.UserId_int,
                    b.BatchId_bi,
                    b.Desc_vch,
                    SUM(IF(oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL, 1, 0)) AS OptIn,
                    SUM(IF(oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL, 1, 0)) AS OptOut
                FROM 
                    simplelists.optinout AS oi 
                INNER JOIN 
                    simpleobjects.batch as b ON 
                    (b.BatchId_bi = oi.BatchId_bi)
                WHERE
                    b.UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpUserId#'>
                    AND
                     LENGTH(oi.ContactString_vch) < 14
                <cfif customFilter NEQ "">
                    <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        <cfif filterItem.NAME EQ 'b.BatchId_bi'>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#filterItem.VALUE#'>
                        <cfelse>
                        AND 
                            #PreserveSingleQuotes(filterItem.NAME)# =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
                    AND
                        CASE WHEN
                            (oi.OptIn_dt IS NOT NULL AND oi.OptOut_dt IS NULL)
                        THEN
                            oi.OptIn_dt
                            BETWEEN 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                            AND 
                                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                    AND
                        CASE WHEN
                            (oi.OptOut_dt IS NOT NULL AND oi.OptIn_dt IS NULL)
                        THEN
                            oi.OptOut_dt
                                BETWEEN 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
                                AND 
                                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
                        ELSE
                            1
                        END
                </cfif>
                GROUP BY
                    b.BatchId_bi
                HAVING
                    (
                        OptIn > 0
                        OR
                        OptOut > 0
                    )
                <cfif arguments.inpSkipPaging EQ 0>
                LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">
                </cfif>
            </cfquery>

            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>

            <cfloop query="getList">

                <cfset temp = {
                    NAME = '#getList.Desc_vch#',
                    OPTIN = '#getList.OptIn#',
                    OPTOUT = '#getList.OptOut#'
                }/>

                <cfset ArrayAppend(dataout["DATALIST"],temp)>
            </cfloop>
            
            <cfset dataout.RXRESULTCODE = 1/>
            <cfset dataout.MESSAGE = 'Get report successfully!'/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
</cfcomponent>