﻿<cfcomponent name="GroupBarchart" hint="draw stack bar chart with return data in am  chart" output="false">
	
	<cffunction name="init" hint="default contructor function">
		<cfset variables.title = "">
        <cfset variables.yAxisTitle = "">
        <cfset variables.xAxisTitle = "">
		<cfset variables.chartId= "">
		<cfset variables.chartType = 1>
		<cfset variables.data = ArrayNew(1)>
		<cfset variables.category = ArrayNew(1)>
		<cfset variables.chartWidth="700">
		<cfset variables.chartHeight="300">
		<cfset variables.groupType= "group">
        
       
		<!--- MORE COLORS http://colorschemedesigner.com/ MONO
        BASE COLORS F18922 8DC050 785A3A 1A8FC9 681D2A FFFA00
		
		rEGULAR
		
		 "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73",
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300" 
			   
		iNVERTED 
 			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73" 
			   
        --->
        <cfset variables.colorArr= [
			  
			   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40",
			   "##F8BD83", "##BFE098", "##BCA286", "##77C0E4", "##B36C79", "##FFFC73",
			   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
			   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
			   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300" 
			]>
            
		<cfreturn this>
	</cffunction>
			
	<cffunction name="drawAmChart" returntype="string" access="public" output="true" hint="Draw option for amChart">
		
		<cfset var amChart	= '' />
		<cfset var amchartData	= '' />
		<cfset var currRowIndex	= '' />
		<cfset var dataItem	= '' />
		<cfset var cateIndex	= '' />
		<cfset var dataIndex	= '' />
		<cfset var i	= '' />
		<cfset var amChartContent	= '' />

		<cfset amChart="">
		<cfset amchartData = ArrayNew(1)/>
		<cfset currRowIndex = 1>
		<cfloop array="#getCategory()#" index="cateIndex">
			<cfset dataItem = {}>
			<cfset dataItem["category"] = cateIndex>
			<cfloop array="#getData()#" index="dataIndex">
				<cfset dataItem["#dataIndex.name#"] = dataIndex.data[currRowIndex]>
			</cfloop>
			<cfset arrayAppend(amchartData,dataItem)>
			<cfset currRowIndex++>
		</cfloop>
		<cfsavecontent variable="amChartContent">
			var chart;
			var chartData = #serializeJSON(amchartData)#;
			
		    chart = new AmCharts.AmSerialChart();
		    chart.dataProvider = chartData;
		    chart.categoryField = "category";
		    chart.startDuration = 1;
			chart.depth3D = 20;
            chart.angle = 30;
            chart.colors = [ <cfoutput>"#variables.colorArr[1]#"<cfloop index="i" from="2" to="#Arraylen(variables.colorArr)#">, "#variables.colorArr[i]#"</cfloop></cfoutput> ];
         
         	<cfif TRIM(variables.yAxisTitle) NEQ "">
                chart.addLabel(3,"100%","#variables.yAxisTitle#","center",11,"##000000",270);
            </cfif>
                                          
            <cfif TRIM(variables.xAxisTitle) NEQ "">
                chart.addLabel("3","!15","#variables.xAxisTitle#","center",11,"##000000",0);
            </cfif>
            
            <cfif TRIM(variables.title) NEQ "">
                <!---chart.addLabel(50, 40, "Beer Consumption by country", "left", 15, "##000000", 0, 1, true);--->                
                chart.addLabel(5,3,"#variables.title#","center",12,"##000000",0,1,true);
            </cfif>
            
		    var categoryAxis = chart.categoryAxis;
		    categoryAxis.labelRotation = 90;
		    categoryAxis.gridPosition = "start";
		    
		    
                
		    var valueAxis = new AmCharts.ValueAxis();
		    valueAxis.stackType = '<cfoutput><cfif #getGroupType()# EQ "group">none<cfelse>regular</cfif></cfoutput>';
		    
		
			valueAxis.gridAlpha = 1;
			valueAxis.axisAlpha = 0;
			valueAxis.labelsEnabled = true;
			chart.addValueAxis(valueAxis); 
			
			
			<cfloop array="#getData()#" index="dataIndex">
				var graph = new AmCharts.AmGraph();
				graph.title = "<cfoutput>#dataIndex.name#</cfoutput>";
				graph.valueField = "<cfoutput>#dataIndex.name#</cfoutput>";
				graph.balloonText = "<cfoutput>#dataIndex.name#</cfoutput>: [[value]]";
				graph.type = "column";
			    graph.lineAlpha = 0;
			    graph.fillAlphas = 0.8;
           	    chart.addGraph(graph);
			</cfloop>
			var legend = new AmCharts.AmLegend();
            legend.borderAlpha = 0.3;
            legend.horizontalGap = 10;
            legend.switchType = "v";
            chart.addLegend(legend);
          
		</cfsavecontent>
		<cfset amChart = amChart & amChartContent>
		<cfreturn amChart>
		<!---<cfoutput>
				<cfif #getGroupType()# EQ "group">
					regular
				<cfelse>
					none
				</cfif>
			</cfoutput>
		    ';--->
	</cffunction>
			
	<cffunction name="drawChart">
				
		<cfreturn drawAmChart()/>
	
	</cffunction>

	<cffunction name="setWidth" returntype="void" output="false" access="public" hint="Set width for chart">
		<cfargument name="_chartWidth" type="string" />
		<cfset variables.chartWidth = arguments._chartWidth >
	</cffunction>
	
	<cffunction name="getWidth" returntype="string" access="public" hint="Get width of chart">
		<cfreturn variables.chartWidth>
	</cffunction>
	
	<cffunction name="setHeight" returntype="void" output="false" access="public" hint="Set Height for chart">
		<cfargument name="_chartHeight" type="string" />
		<cfset variables.chartHeight = arguments._chartHeight >
	</cffunction>
	
	<cffunction name="getHeight" returntype="string" access="public" hint="Get Height of chart">
		<cfreturn variables.chartHeight>
	</cffunction>

	<cffunction name="setChartId" returntype="void" output="false" access="public" hint="Set Id for chart">
		<cfargument name="_chartId" type="string" />
		<cfset variables.chartId = arguments._chartId >
	</cffunction>
	
	<cffunction name="getChartId" returntype="string" access="public" hint="Get id of chart">
		<cfreturn variables.chartId>
	</cffunction>

	<cffunction name="setChartType" returntype="void" output="false" access="public" hint="Set type for chart">
		<cfargument name="_chartType" type="string" />
		<cfset variables.chartType = arguments._chartType >
	</cffunction>
	
	<cffunction name="getChartType" returntype="string" access="public" hint="Get type of chart">
		<cfreturn variables.chartType>
	</cffunction>
	
	<cffunction name="setTitle" returntype="void" output="false" access="public" hint="Set title for chart">
		<cfargument name="_Title" type="string" />
		<cfset variables.title = arguments._Title >
	</cffunction>
    
     <cffunction name="setXAxisTitle" returntype="void" output="false" access="public" hint="Set xAxisTitle for chart">
		<cfargument name="_xAxisTitle" type="string" />
		<cfset variables.xAxisTitle = arguments._xAxisTitle >
	</cffunction>
	
	<cffunction name="getXAxisTitle" returntype="string" access="public" hint="Get xAxisTitle of chart">
		<cfreturn variables.xAxisTitle>
	</cffunction>
    
    <cffunction name="setYAxisTitle" returntype="void" output="false" access="public" hint="Set yAxisTitle for chart">
		<cfargument name="_yAxisTitle" type="string" />
		<cfset variables.yAxisTitle = arguments._yAxisTitle >
	</cffunction>
	
	<cffunction name="getYAxisTitle" returntype="string" access="public" hint="Get yAxisTitle of chart">
		<cfreturn variables.yAxisTitle>
	</cffunction>
	
	<cffunction name="getTitle" returntype="string" access="public" hint="Get title for chart">
		<cfreturn variables.title>
	</cffunction>
	
	<cffunction name="setData" returntype="void" output="false" access="public" hint="Set data for chart, number of items of data must equal category's">
		<cfargument name="_data" type="Array" />
		<cfset variables.data = arguments._data >
	</cffunction>
	
	<cffunction name="getData" returntype="array" output="false" access="public" hint="get data for chart">
		<cfreturn variables.data>
	</cffunction>
	
	<cffunction name="setCategory" returntype="void" output="false" access="public" hint="Set category for chart, number of items of category must equal data's ">
		<cfargument name="_category" type="Array" />
		<cfset variables.category = arguments._category >
	</cffunction>
	
	<cffunction name="getCategory" returntype="array" output="false" access="public" hint="get category for chart">
		<cfreturn variables.category>
	</cffunction>
	
	<cffunction name="setGroupType" returntype="void" output="false" access="public" hint="Set group type">
		<cfargument name="_groupType" type="String" />
		<cfset variables.groupType = arguments._groupType >
	</cffunction>
	
	<cffunction name="getGroupType" returntype="string" output="false" access="public" hint="get group type: 'group' or 'stack' chart">
		<cfreturn variables.groupType>
	</cffunction>
	
</cfcomponent>