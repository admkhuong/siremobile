<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
	
	<cffunction name="ConsoleSummaryMLPCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt IN Counts - Combo ">
		<cfargument name="inpLimit" TYPE="any" hint="Limit how many to get" required="false" default="3" />
		
	   	<cfset var dataout = {} />    
		<cfset var GetSMSResults = '' />
		<cfset var RetVarConsoleSummaryMLPViewCounts = '' />
		<cfset var RetVarConsoleActiveMLPCounts = '' />
				
		<!--- Set default return values --->  
		<cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.MLPLOADCOUNT = 0 />
    	<cfset dataout.MLPACTIVECOUNT = 0 />
    					
		<cftry>
				
			<!--- Validate user keywords counts in use from DB --->
			<cfinvoke method="ConsoleSummaryMLPViewCounts" returnvariable="RetVarConsoleSummaryMLPViewCounts"></cfinvoke>
		
			<cfset dataout.MLPLOADCOUNT = RetVarConsoleSummaryMLPViewCounts.MLPLOADCOUNT /> 
				   
			<!--- Validate user keyword length limits from DB --->
			<cfinvoke method="ConsoleActiveMLPCounts" returnvariable="RetVarConsoleActiveMLPCounts"></cfinvoke>
		   		
			<cfset dataout.MLPACTIVECOUNT = RetVarConsoleActiveMLPCounts.MLPACTIVECOUNT /> 	        
	        
	       	        	        
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>

	
	<cffunction name="ConsoleSummaryMLPViewCounts" access="remote" output="false" returntype="any" hint="Get MLP load Counts for current user for last 30 days">
		
	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
			
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.MLPLOADCOUNT = 0 />
				
		<cftry>
		
		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplexresults.mlptracking 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Created_dt > DATE_ADD(NOW(), INTERVAL -30 DAY)
		    </cfquery>
	        
			<cfset dataout.MLPLOADCOUNT = GetResults.TotalCount /> 
		      			   			           
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
	
	
	<cffunction name="ConsoleActiveMLPCounts" access="remote" output="false" returntype="any" hint="Get Active MLP Counts for current user for last 30 days">
		
	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
			
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.MLPACTIVECOUNT = 0 />
				
		<cftry>
		
		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplelists.cppx_data 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Status_int = 1 
				AND 
					MlpType_ti = 1	
		    </cfquery>
	        
			<cfset dataout.MLPACTIVECOUNT = GetResults.TotalCount /> 
		      			   			           
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
		
	<cffunction name="DashboardMLPSummary" access="remote" output="false" hint="Get MLP Views, MLP Active, MLP Deactive for Dashoboard">
		<cfset var dataout = {} />
		<cfset var getMLPViews = '' />
		<cfset var getMLPActive = '' />
		<cfset var getMLPDeactive = '' />

		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.MLPVIEWS = 0 />
    	<cfset dataout.MLPACTIVE = 0 />
    	<cfset dataout.MLPDEACTIVE = 0 />

		<cftry>
			<cfquery name="getMLPViews" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS MLPVIEWS
				FROM 
					simplexresults.mlptracking 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Created_dt > DATE_ADD(NOW(), INTERVAL -30 DAY)
			</cfquery>

			<cfquery name="getMLPActive" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS MLPACTIVE
				FROM 
					simplelists.cppx_data 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Status_int = 1
				AND
					MlpType_ti = 1	
			</cfquery>

			<cfquery name="getMLPDeactive" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS MLPDEACTIVE
				FROM 
					simplelists.cppx_data 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Status_int = 0
				AND 
					MlpType_ti = 1	
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MLPVIEWS = getMLPViews.MLPVIEWS />
			<cfset dataout.MLPACTIVE = getMLPActive.MLPACTIVE />
			<cfset dataout.MLPDEACTIVE = getMLPDeactive.MLPDEACTIVE />
			<cfset dataout.MESSAGE = 'Get MLP summary data successfully' />
			<cfcatch TYPE="any">
	            <cfset dataout.RXRESULTCODE = "-2" />
	            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
	        </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="DashBoardActiveMLPList" access="remote" output="false" hint="Get active MLP list for Dashoboard">
		<cfset var dataout = {} />
		<cfset var dataout['MLPList'] = arrayNew(1)>
		<cfset var MLPItem = {} />
		<cfset var getActiveMLPList = '' />

		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery name="getActiveMLPList" datasource="#Session.DBSourceREAD#">
				SELECT
					ccpxDataId_int,
					cppxName_vch
				FROM 
					simplelists.cppx_data 
				WHERE				
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Status_int = 1 
				AND
					MlpType_ti = 1	
			</cfquery>

			<cfif getActiveMLPList.Recordcount GT 0>
				<cfloop query="getActiveMLPList">
					<cfset MLPItem = {
						id = ccpxDataId_int,
						name = cppxName_vch
					}>
					<cfset arrayAppend(dataout['MLPList'], MLPItem)>
				</cfloop>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = "Get Active MLP List successfully">
 			<cfcatch TYPE="any">
	            <cfset dataout.RXRESULTCODE = "-2" />
	            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
	        </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
</cfcomponent>