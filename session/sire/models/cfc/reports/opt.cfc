<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
	
	
	<cffunction name="ConsoleSummaryOptCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt IN Counts - Combo ">
		<cfargument name="inpLimit" TYPE="any" hint="Limit how many to get" required="false" default="3" />
		<cfargument name="inpUserId" required="false" default="#Session.UserId#"/>
		
	   	<cfset var dataout = {} />    
		<cfset var GetSMSResults = '' />
		<cfset var RetVarConsoleSummaryNewOptInCounts = '' />
		<cfset var RetVarConsoleSummaryTotalOptInCounts = '' />
		<cfset var RetVarConsoleSummaryNewOptOutCounts = '' />
		<cfset var RetVarConsoleSummaryTotalOptOutCounts = '' />
		
		<!--- Set default return values --->  
		<cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.OPTINCOUNT = 0 />
    	<cfset dataout.KEYWORDLIMIT = 0 />
    	<cfset dataout.TOPACTIVE = ArrayNew(1) />
				
		<cftry>
				
			<!--- Validate user keywords counts in use from DB --->
			<cfinvoke method="ConsoleSummaryNewOptInCounts" returnvariable="RetVarConsoleSummaryNewOptInCounts">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>
		
			<cfset dataout.OPTINCOUNT = RetVarConsoleSummaryNewOptInCounts.OPTINCOUNT /> 				   
			<!--- Validate user keyword length limits from DB --->
			<cfinvoke method="ConsoleSummaryTotalOptInCounts" returnvariable="RetVarConsoleSummaryTotalOptInCounts">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>
		   		
			<cfset dataout.OPTINTOTALCOUNT = RetVarConsoleSummaryTotalOptInCounts.OPTINTOTALCOUNT /> 	        
	        
	        <cfinvoke method="ConsoleSummaryNewOptOutCounts" returnvariable="RetVarConsoleSummaryNewOptOutCounts">
	        	<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
	        </cfinvoke>
		   		
			<cfset dataout.OPTOUTCOUNT = RetVarConsoleSummaryNewOptOutCounts.OPTOUTCOUNT />

			<cfinvoke method="ConsoleSummaryTotalOptOutCounts" returnvariable="RetVarConsoleSummaryTotalOptOutCounts">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>
		   		
			<cfset dataout.OPTOUTTOTALCOUNT = RetVarConsoleSummaryTotalOptOutCounts.OPTOUTCOUNT />
	        	        
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
	
		
	<cffunction name="ConsoleSummaryNewOptInCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt IN Counts for current user for last 30 days">
		<cfargument name="inpUserId" required="false" default="#Session.UserId#"/>

	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
			
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.OPTINCOUNT = 0 />
				
		<cftry>
		
		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					(b.batchid_bi = oi.BatchId_bi AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">)
				WHERE 
					OptIn_dt IS NOT NULL 
				AND 
					OptOut_dt IS NULL 				
				AND
					Created_dt > DATE_ADD(NOW(), INTERVAL -90 DAY)
		    </cfquery>
	        
			<cfset dataout.OPTINCOUNT = GetResults.TotalCount /> 
		      			   			           
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
	
	<cffunction name="ConsoleSummaryTotalOptInCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt IN Counts for current user - Total">
		<cfargument name="isByUser" required="false" default="1">
		<cfargument name="inpStartDate" type="string" required="false" hint=""/>
        <cfargument name="inpEndDate" type="string" required="false" hint=""/>
        <cfargument name="inpUserId" required="false" default="#Session.UserId#"/>

        <cfargument name="customFilter" default=""/>

        <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
	        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
	        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
	        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
	    </cfif>

	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
		<cfset var defaultShortCode = {}/>	
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.OPTINTOTALCOUNT = 0 />
				
		<cftry>
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>

		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					(b.batchid_bi = oi.BatchId_bi <cfif arguments.isByUser EQ 1> AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"></cfif>)
				LEFT JOIN
					simpleobjects.useraccount as ua
				ON
					ua.UserId_int = b.UserId_int
				WHERE 
					oi.OptIn_dt IS NOT NULL 
				AND 
					oi.OptOut_dt IS NULL
				AND
                    ua.IsTestAccount_ti = 0
				<cfif arguments.isByUser EQ 1>
					AND 
						b.ShortCodeId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
				</cfif>					
				<cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
					AND
	                    oi.OptIn_dt
	                BETWEEN 
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
	                AND 
	                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
	            </cfif>
			</cfquery>
	        
			<cfset dataout.OPTINTOTALCOUNT = GetResults.TotalCount /> 
		      			   			           
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>


	<cffunction name="ConsoleSummaryNewOptOutCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt OUT Counts for current user for last 30 days">
		<cfargument name="inpUserId" required="false" default="#Session.UserId#"/>

	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
		
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.OPTOUTCOUNT = 0 />
    	<cfset var defaultShortCode = {}/>
				
		<cftry>
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					(b.batchid_bi = oi.BatchId_bi AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">)
				WHERE 
					OptOut_dt IS NOT NULL 
				AND 
					OptIn_dt IS NULL 
				AND
					LENGTH(oi.ContactString_vch) < 14
				AND 
					b.ShortCodeId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultShortCode.SHORTCODEID#">
				AND
					b.Created_dt > DATE_ADD(NOW(), INTERVAL - 90 DAY)
		    </cfquery>
	        
			<cfset dataout.OPTOUTCOUNT = GetResults.TotalCount /> 
		               
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
	
	<cffunction name="ConsoleSummaryTotalOptOutCounts" access="remote" output="false" returntype="any" hint="Get SMS Opt OUT Counts for current user for last 30 days">
		<cfargument name="isByUser" required="false" default="1">
		<cfargument name="inpStartDate" type="string" required="false" hint=""/>
        <cfargument name="inpEndDate" type="string" required="false" hint=""/>
        <cfargument name="inpUserId" required="false" default="#Session.UserId#"/>

        <cfargument name="customFilter" default=""/>

        <cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
	        <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
	        <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	        <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	        <cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
	        <cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
	    </cfif>

	   	<cfset var dataout = {} />    
		<cfset var GetResults = '' />
		
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.OPTOUTCOUNT = 0 />
				
		<cftry>
		
		    <cfquery name="GetResults" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(*) AS TotalCount
				FROM 
					simplelists.optinout AS oi 
				INNER JOIN 
					simpleobjects.batch as b ON 
					(b.batchid_bi = oi.BatchId_bi <cfif arguments.isByUser EQ 1> AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"></cfif>)
				LEFT JOIN
					simpleobjects.useraccount as ua
				ON
					ua.UserId_int = b.UserId_int
				WHERE 
					OptOut_dt IS NOT NULL 
				AND 
					OptIn_dt IS NULL
				AND
                    ua.IsTestAccount_ti = 0
				<cfif arguments.inpStartDate NEQ '' AND arguments.inpEndDate NEQ ''>
					AND
	                    oi.OptOut_dt
	                BETWEEN 
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#">
	                AND 
	                    DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
	            </cfif>
				 AND
                    LENGTH(oi.ContactString_vch) < 14
		    </cfquery>
	        
			<cfset dataout.OPTOUTCOUNT = GetResults.TotalCount /> 			
		               
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
</cfcomponent>
	
