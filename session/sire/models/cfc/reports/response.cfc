<cfcomponent output="no">

	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">

    <cfparam name="Session.DBSourceEBM" default="bishop"/>
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<cffunction name="ResponseTable" access="remote" output="false" returntype="any" hint="list all opt outs for a given short code">
          <cfargument name="inpBatchId" required="yes" type="any">
          <cfargument name="inpStart" required="yes" type="string">
          <cfargument name="inpEnd" required="yes" type="string">
          <cfargument name="inpQId" TYPE="any" required="yes" default="1" hint="The Control Point - Physical Id"/>
          <cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">

          <!--- Paging --->
          <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
          <!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
          <cfargument name="iDisplayStart" required="no"  default="0">
          <cfargument name="iDisplayLength" required="no"  default="10">
          <cfargument name="sEcho" required="no"  default="">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
	    <!--- Default sort this descending on the 5th (0 based) column--->
	    <!---<cfargument name="iSortCol_0" required="no"  default="4">
	    <cfargument name="sSortDir_0" required="no"  default="DESC">--->

	        <cfset var sTableName	= '' />
	        <cfset var listColumns	= '' />
	        <cfset var thisColumn	= '' />
	        <cfset var thisS	= '' />
	        <cfset var qFiltered	= '' />
	        <cfset var DataOut	= {} />
	        <cfset var LOCALOUTPUT = {} />
	        <cfset var qFilteredraw = '' />
	        <cfset var qFilteredSum = '' />
	        <cfset var FormatBuff = '' />
	        <cfset var RetVarReadCPDataById = '' />
	        <cfset var ind	= '' />
	        <cfset var AnswerOptions = '' />
	        <cfset var tempItem = '' />

	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

	        <cfset DataOut["aaData"] = ArrayNew(1)>

	        <cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
	        <cfset DataOut.MESSAGE = "" />
	        <cfset DataOut.ERRMESSAGE = "" />

	        <cfset DataOut["iTotalRecords"] = 0>
	        <cfset DataOut["iTotalDisplayRecords"] = 0>


	        <cftry>

		        <!---
		            Easy set variables
		         --->

		        <!--- table name --->
		        <cfset sTableName = "simplequeue.ireresults" />

		        <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
		        <cfset listColumns = "Response_vch,chart,TotalCount" />

		        <!--- Make sure there is a result for each possible option --->
				<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
	                <cfinvokeargument name="inpQId" value="#inpQId#">
	                <cfinvokeargument name="inpIDKey" value="ID">
	            </cfinvoke>

				<!--- Make sure there is a still a result for this QId in the control string --->
				<cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

					<!--- Build a UNION query of all possible answers --->
					<cfloop from="1" to="#arrayLen(RetVarReadCPDataById.CPOBJ.OPTIONS)#" index="ind" >

						<cfif TRIM(RetVarReadCPDataById.CPOBJ.OPTIONS[ind].AVAL) EQ "">
							<cfset RetVarReadCPDataById.CPOBJ.OPTIONS[ind].AVAL = RetVarReadCPDataById.CPOBJ.OPTIONS[ind].TEXT />
						</cfif>

						<cfif ind EQ 1>
							<cfset AnswerOptions = 'SELECT "#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].AVAL#" AS Response_vch, "#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].TEXT#" AS Text_vch' />
						<cfelse>
							<cfset AnswerOptions = AnswerOptions & ' UNION SELECT "#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].AVAL#" AS Response_vch, "#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].TEXT#" AS Text_vch' />
						</cfif>

					</cfloop>

					<!--- Make sure to include the "NO MATCH" option --->
					<cfset AnswerOptions = AnswerOptions & ' UNION SELECT "NO MATCH" AS Response_vch, "NO MATCH" AS Text_vch' />
				</cfif>



		        <!--- Get data to display --->
		        <!--- Data set after filtering --->
		        <cfquery name="qFilteredraw" datasource="#Session.DBSourceREAD#">
		            SELECT
		                COUNT(IRE.PKId_bi) AS TotalCount,
		                '' as chart,
		                ANS.Text_vch AS Response_vch
		            FROM
		            (#AnswerOptions#) AS ANS
		            LEFT JOIN
		                simplexresults.irebucket AS IRE
		            ON
		               ( BINARY ANS.Response_vch = BINARY IRE.Response_vch OR BINARY ANS.Text_vch = BINARY IRE.Response_vch ) <!--- BINARY - Force handle collation issues for the manually generated text above --->
		            AND
				        batchid_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
		            AND
		                Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
			        AND
			            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		            AND
		                QId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQID#">

				  <!--- Dont let users URL hack data that is not theirs --->
		            <cfif session.userrole NEQ 'SuperUser'>
		                AND
		                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
		            </cfif>

		            GROUP BY
		                ANS.Response_vch
		        </cfquery>

				<!--- Handle case where there is no data --->
		       	<cfif qFilteredraw.recordCount eq 0>
					<cfset qFiltered = qFilteredraw />

					<cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
					<cfset DataOut.TYPE = "" />
					<cfset DataOut.MESSAGE = "" />
					<cfset DataOut.ERRMESSAGE = "" />

					<cfreturn DataOut />

				<cfelse>

					<cfquery name="qFilteredSum" dbtype="query">
						SELECT
							SUM(TotalCount) AS total
						FROM
							qFilteredraw
					</cfquery>

					<cfquery name="qFiltered" dbtype="query">
						SELECT

					    <cfif OutQueryResults NEQ '1'>
							cast(TotalCount as varchar) + ' (' + cast((TotalCount/#qFilteredSum.total#)*100 as varchar) + '%)' as TotalCount,
							<cfif qFilteredSum.total EQ 0>
								'<div style="width:'+cast(0 as varchar)+'%;height:100%;background-color:##118ACF;"></div>' as Chart,
							<cfelse>
								'<div style="width:'+cast((TotalCount/#qFilteredSum.total#)*100 as varchar)+'%;height:100%;background-color:##118ACF;"></div>' as Chart,
							</cfif>
							<!---
									0 AS TotalCount,
									'Sample' as Chart,
							--->
					    <cfelse>
					    	TotalCount,
					    	cast((TotalCount/#qFilteredSum.total#)*100 as varchar) as Percentage,
					    </cfif>
					    Response_vch
					FROM
						qFilteredraw
					</cfquery>

		       </cfif>

		       <cfif OutQueryResults EQ "1">

		            <cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
                         <cfset DataOut.TYPE = "" />
			        <cfset DataOut.MESSAGE = "" />
			        <cfset DataOut.ERRMESSAGE = "" />

	                <cfreturn DataOut />
	            </cfif>

		        <!---<cfdump var="#qFiltered#" ><cfabort>--->
		        <!---
		            Output
		         --->

				<cfif arguments.iDisplayLength eq -1>
					<cfset arguments.iDisplayLength = qFiltered.recordCount />
				</cfif>

		        <cfif qFilteredraw.TOTALCOUNT GT 0>
	                <cfset DataOut["iTotalRecords"] = qFilteredraw.TOTALCOUNT>
	                <cfset DataOut["iTotalDisplayRecords"] = qFilteredraw.TOTALCOUNT>
	            </cfif>

		        <cfloop query="qFilteredraw">

		            <cfif qFilteredSum.total EQ 0>

		                <cfset tempItem = [
		                    '#qFilteredraw.Response_vch#',
		                    '#qFilteredraw.TotalCount#',
		                    '#LSNumberFormat( 0, "9.99")#'
		                ]>

			        <cfelse>

		                <cfset tempItem = [
		                    '#qFilteredraw.Response_vch#',
		                    '#qFilteredraw.TotalCount#',
		                    '#LSNumberFormat( (qFilteredraw.TotalCount/qFilteredSum.total)*100, "9.99")#'
		                ]>

		            </cfif>

	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>

		        <!--- Append a summary row of totals --->
				<cfset tempItem = [
	                'Total',
	                '#qFilteredSum.total#',
	                ''
	            ]>
	            <cfset ArrayAppend(DataOut["aaData"],tempItem)>

                    <cfset DataOut.RXRESULTCODE = 1 />
                    <cfset DataOut.TYPE = "" />
                    <cfset DataOut.MESSAGE = "" />
                    <cfset DataOut.ERRMESSAGE = "" />

	<!---
		       <cfsavecontent variable="DataOut">
		        {"sEcho": <cfoutput>#val(arguments.sEcho)#</cfoutput>,
		        "iTotalRecords": <cfoutput>#qFiltered.RecordCount#</cfoutput>,
		        "iTotalDisplayRecords": <cfoutput>#qFiltered.recordCount#</cfoutput>,
		        "aaData": [
		            <cfoutput query="qFiltered" startrow="#val(arguments.iDisplayStart+1)#" maxrows="#val(arguments.iDisplayLength)#">
		                <cfif currentRow gt (arguments.iDisplayStart+1)>,</cfif>
		                [<cfloop list="#listColumns#" index="thisColumn">
		                	<cfif thisColumn neq listFirst(listColumns)>,</cfif>
		                	<cfif thisColumn is "version">
		                		<cfif version eq 0>"-"<cfelse>"#version#"</cfif>

							<cfelseif thisColumn is "Chart">
		                		<cfset FormatBuff = jsStringFormat(qFiltered[thisColumn][qFiltered.currentRow]) />
		                		"#FormatBuff#"
		                	<cfelseif thisColumn is "Response_vch">

		                		<!--- Preserve possible newlines in display but format all other tags --->
								<cfset FormatBuff = ReplaceNoCase(qFiltered[thisColumn][qFiltered.currentRow], "<br>", "newlinex", "ALL")>
								<cfset FormatBuff = ReplaceNoCase(FormatBuff, "\n", "newlinex", "ALL")>
								<cfset FormatBuff = Replace(FormatBuff, chr(10), "newlinex", "ALL")>

								<cfset FormatBuff = HTMLEditFormat(FormatBuff) />

								<!--- Allow newline in text messages - reformat for display --->
								<cfset FormatBuff = ReplaceNoCase(FormatBuff, "newlinex", "<br>", "ALL")>
								<cfset FormatBuff = Replace(FormatBuff, "\n", "<br>", "ALL")>
								<!--- Display quote but store as &apos; in the XML --->
								<cfset FormatBuff = Replace(FormatBuff, "&apos;", "'", "ALL")>

		                		"#FormatBuff#"
		                	<cfelse>
			                	<cfset FormatBuff = REPLACE(qFiltered[thisColumn][qFiltered.currentRow], CHR(39), '&apos;', 'ALL') />
		                		"#FormatBuff#"
		                	</cfif>
		                </cfloop>]
		            </cfoutput> ] }
		       </cfsavecontent>
	--->


		<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.AnswerOptions = AnswerOptions />
			    <cfset dataout.Type = "#cfcatch.type#" />
			    <cfset dataout.Message = "#cfcatch.message#" />
			    <cfset dataout.ErrMessage = "#cfcatch.detail#" />

			    <cfreturn dataout />

			</cfcatch>
    	</cftry>


	    <cfreturn DataOut />

	</cffunction>

	<cffunction name="ResponseOptionListTable" access="remote" output="false" returntype="any" hint="list all opt outs for a given short code">
		<cfargument name="inpBatchId" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">
		<cfargument name="inpQId" TYPE="any" required="yes" default="1" hint="The Control Point - Physical Id"/>
		<cfargument name="OutQueryResults" required="no" type="any" default="0" hint="This allows query results to be output for QueryToCSV method.">

		<!--- Paging --->
		<!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
		<!--- ColdFusion Specific Note: I am handling paging in the cfoutput statement instead of limit.  --->
		<cfargument name="iDisplayStart" required="no"  default="0">
		<cfargument name="iDisplayLength" required="no"  default="10">
		<cfargument name="sEcho" required="no"  default="">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
	    <!--- Default sort this descending on the 5th (0 based) column--->
	    <!---<cfargument name="iSortCol_0" required="no"  default="4">
	    <cfargument name="sSortDir_0" required="no"  default="DESC">--->

			<cfset var sTableName	= '' />
			<cfset var listColumns	= '' />
			<cfset var thisColumn	= '' />
			<cfset var thisS	= '' />
			<cfset var qFiltered	= '' />
			<cfset var DataOut	= {} />
			<cfset var LOCALOUTPUT = {} />
			<cfset var qFilteredraw = '' />
			<cfset var qFilteredSum = '' />
			<cfset var FormatBuff = '' />
			<cfset var RetVarReadCPDataById = '' />
			<cfset var ind	= '' />
			<cfset var AnswerOptions = '' />
			<cfset var tempItem = '' />

			<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
			<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

			<cfset DataOut["aaData"] = ArrayNew(1)>

			<cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
			<cfset DataOut.MESSAGE = "" />
			<cfset DataOut.ERRMESSAGE = "" />

			<cfset DataOut["iTotalRecords"] = 0>
			<cfset DataOut["iTotalDisplayRecords"] = 0>


		   	<cftry>

			   <!---
				  Easy set variables
			    --->

			   <!--- table name --->
			   <cfset sTableName = "simplequeue.ireresults" />

			   <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
			   <cfset listColumns = "Response_vch,chart,TotalCount" />

			   <!--- Make sure there is a result for each possible option --->
				<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
				 <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				 <cfinvokeargument name="inpQId" value="#inpQId#">
				 <cfinvokeargument name="inpIDKey" value="ID">
			  </cfinvoke>

				<!--- Make sure there is a still a result for this QId in the control string --->
				<cfif RetVarReadCPDataById.RXRESULTCODE EQ 1>

					<!--- Build a UNION query of all possible answers --->
					<cfloop from="1" to="#arrayLen(RetVarReadCPDataById.CPOBJ.OPTIONS)#" index="ind" >

						<cfset tempItem = [
							'#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].AVAL#',
							'#RetVarReadCPDataById.CPOBJ.OPTIONS[ind].TEXT#'
 					    	]>

						<cfset ArrayAppend(DataOut["aaData"],tempItem)>

					</cfloop>

					<cfset DataOut["iTotalRecords"] = arrayLen(RetVarReadCPDataById.CPOBJ.OPTIONS)>
				     <cfset DataOut["iTotalDisplayRecords"] = arrayLen(RetVarReadCPDataById.CPOBJ.OPTIONS)>

				</cfif>

				<cfset DataOut.RXRESULTCODE = 1 />
				<cfset DataOut.TYPE = "" />
				<cfset DataOut.MESSAGE = "" />
				<cfset DataOut.ERRMESSAGE = "" />

			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.Type = "#cfcatch.type#" />
				<cfset dataout.Message = "#cfcatch.message#" />
				<cfset dataout.ErrMessage = "#cfcatch.detail#" />

			    <cfreturn dataout />

			</cfcatch>
		</cftry>


	    <cfreturn DataOut />

	</cffunction>

	<!--- Support Methods for Response reports --->

	<cffunction name="GetCPList" access="remote" hint="Get Control Point data for any flow - optional Type filter - inpTypeFilter">
    	<cfargument name="inpBatchId" required="true"/>
    	<cfargument name="inpTypeFilter" TYPE="string" required="false" hint="Optional TYPE of question filter" default="" />

    	<cfset var dataout = {} />
    	<cfset dataout["DATALIST"] = [] />
    	<cfset dataout.MESSAGE = 'Get CP failed' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset var RetVarReadXMLQuestions = '' />
    	<cfset var CPOBJ = '' />
    	<cfset var RetVarReadCPDataById = '' />

    	<cftry>
    		<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestions" returnvariable="RetVarReadXMLQuestions">
                <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
                <cfinvokeargument name="inpTypeFilter" value="#arguments.inpTypeFilter#">
            </cfinvoke>

            <cfloop array="#RetVarReadXMLQuestions.CPOBJ#" index="CPOBJ">

                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpBatchId" value="#arguments.inpBatchId#">
                    <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                </cfinvoke>

                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ "ONESELECTION" OR RetVarReadCPDataById.CPOBJ.TYPE EQ "SHORTANSWER">
                    <cfset ArrayAppend(dataout["DATALIST"],RetVarReadCPDataById.CPOBJ)>
                    <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.MESSAGE = 'Get CP successfully!'/>
                </cfif>

            </cfloop>
    		<cfcatch type="any">
            	<cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.Type = "#cfcatch.type#" />
                <cfset dataout.Message = "#cfcatch.message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetDistinctResponseCount" access="remote" output="false" returntype="any" hint="Get the count of distinc responses">
		<cfargument name="inpBatchId" required="no" type="any">
		<cfargument name="inpStart" required="no" type="string" default="" >
		<cfargument name="inpEnd" required="no" type="string" default="">
		<cfargument name="inpIREType" required="no" type="string" default="2" hint="Optional IRE Type filter 1 or CSV list of 1,2,3,4 etc -  2 = only get count of inbound responses">
	    <cfargument name="inpQID" TYPE="string" required="no" default="0" hint="The question ID to look up - inpIDKey controls which key to use" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>

        <cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = "0" />
		<cfset dataout.TOTALCOUNT = "0" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

		<cfset var GetDistinctCount = '' />

        <cftry>

			<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
				<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
				<cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
			</cfif>

		    <!--- Check if opt in on ShortCode AND Batch --->
            <cfquery name="GetDistinctCount" datasource="#Session.DBSourceEBM#" >
                SELECT
                    COUNT(DISTINCT ContactString_vch) as TOTALCOUNT
                FROM
                	simplexresults.ireresults
                WHERE
                 	<!--- Dont let users URL hack data that is not theirs --->
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

			    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
					AND
			        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
			    </cfif>

				<cfif TRIM(inpIREType) NEQ "">
					<!--- 2 = only get count of respondents --->
					AND
						IREType_int IN (<cfqueryparam value="#inpIREType#" cfsqltype="cf_sql_integer" list="yes" />)
				</cfif>

                <!--- optional Q ID filter some questions might get answered early but others never completed - this count will show how many answered this Question--->
                <cfif inpQId GT 0>
            	    AND
			    		QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQId#">
                </cfif>

                <cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
                    AND
						Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
					AND
						Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
                </cfif>

    		</cfquery>

            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.TOTALCOUNT = "#GetDistinctCount.TOTALCOUNT#" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.IS2XOPTIN = "0" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>

        <cfreturn dataout />

	</cffunction>


	<cffunction name="ResponseOpenEndedTable" access="remote" output="false" returntype="any" hint="list all responses to a given control point for a given short code">
		<cfargument name="inpBatchId" required="no" type="any" hint="Batch Id of the campaign">
		<cfargument name="inpStart" required="no" type="string" default="" hint="Start date to run report from">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="End date to run report to">
		<cfargument name="inpIREType" required="no" type="string" default="2" hint="Optional IRE Type filter 1 or CSV list of 1,2,3,4 etc -  2 = only get count of inbound responses">
	    <cfargument name="inpQID" TYPE="string" required="no" default="0" hint="The question ID to look up - inpIDKey controls which key to use" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>

	    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">

	    <!--- Paging --->
	    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
	    <cfargument name="iDisplayStart" required="no"  default="0">
	    <cfargument name="iDisplayLength" required="no"  default="5">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">

	        <cfset var GetNumbersCount = '' />
	        <cfset var qFiltered = '' />

	        <cfset var listColumns	= '' />
			<cfset var tempItem	= '' />
			<cfset var thisS	= '' />
			<cfset var LoopIndex	= '' />

	        <cfset var DataOut = {}>

	        <!---ListEMSData is key of DataTable control--->
			<cfset DataOut["aaData"] = ArrayNew(1)>

	        <cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
	        <cfset DataOut.MESSAGE = "" />
	        <cfset DataOut.ERRMESSAGE = "" />

	        <cfset DataOut["iTotalRecords"] = 0>
	        <cfset DataOut["iTotalDisplayRecords"] = 0>

	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">


	        <cftry>

	            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
	            <cfset listColumns = "Response_vch,TOTALCOUNT" />


	            <!---Get total  EMS for paginate --->
	            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
	                SELECT
						 COUNT(DISTINCT Response_vch) AS TOTALCOUNT
					FROM
			            simplexresults.ireresults
			        WHERE

			        <!--- Dont let users URL hack data that is not theirs --->
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

			        <!--- optional Q ID filter some questions might get answered early but others never completed - this count will show how many answered this Question--->
	                <cfif inpQId GT 0>
	            	    AND
				    		QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQId#">
	                </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

					<cfif TRIM(inpIREType) NEQ "">
						<!--- 2 = only get count of respondents --->
						AND
							IREType_int IN (<cfqueryparam value="#inpIREType#" cfsqltype="cf_sql_integer" list="yes" />)
					</cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>
		        </cfquery>

	            <cfif GetNumbersCount.TOTALCOUNT GT 0>
	                <cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
	                <cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	            </cfif>

	            <!--- Get data to display --->
	            <!--- Data set after filtering --->
	            <cfquery name="qFiltered" datasource="#Session.DBSourceREAD#">

		            SELECT
		            	COUNT(*) AS TOTALCOUNT,
						Response_vch
			        FROM
			            simplexresults.ireresults
			        WHERE
						<!--- Dont let users URL hack data that is not theirs --->
		                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

			        <!--- optional Q ID filter some questions might get answered early but others never completed - this count will show how many answered this Question--->
	                <cfif inpQId GT 0>
	            	    AND
				    		QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQId#">
	                </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

                	<cfif TRIM(inpIREType) NEQ "">
						<!--- 2 = only get count of respondents --->
						AND
							IREType_int IN (<cfqueryparam value="#inpIREType#" cfsqltype="cf_sql_integer" list="yes" />)
					</cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>

			 		GROUP BY
						Response_vch

					<cfif iSortingCols gt 0>
		                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
		            </cfif>

		            <!---
			            By limiting results to paging here instead of once query returns, we can:
			            limit how much data is transfered from DB
			            the amount of memory this query results takes up
			        	Greatly speed up processing
			        --->
			     		<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength AND iDisplayLength GT 0>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    <cfelseif iDisplayLength LT 0>
	                    <!--- No limit records--->
	                    <cfelse>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    </cfif>

	            </cfquery>

	            <cfif OutQueryResults EQ "1">

		            <cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
					<cfset DataOut.TYPE = "" />
			        <cfset DataOut.MESSAGE = "" />
			        <cfset DataOut.ERRMESSAGE = "" />

	                <cfreturn DataOut />
	            </cfif>

	            <!---
	                Output
	             --->


	            <cfloop query="qFiltered">

	                <cfset tempItem = [
	                    '#qFiltered.Response_vch#',
	                    '#qFiltered.TOTALCOUNT#'
	                ]>
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>



	             <!--- Append min to 5 - add blank rows --->
	            <cfloop from="#qFiltered.RecordCount#" to="#iDisplayLength#" step="1" index="LoopIndex">

	                <cfset tempItem = [
	                    ' ',
	                    ' '
	                ]>
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>


	            <cfset DataOut.RXRESULTCODE = "1" />
				<cfset DataOut.TYPE = "" />
		        <cfset DataOut.MESSAGE = "" />
		        <cfset DataOut.ERRMESSAGE = "" />

	  		<cfcatch type="Any" >
				<cfset DataOut.RXRESULTCODE = -1 />
			    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
			    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset DataOut["iTotalRecords"] = 0>
				<cfset DataOut["iTotalDisplayRecords"] = 0>
				<cfset DataOut["aaData"] = ArrayNew(1)>
	        </cfcatch>
	        </cftry>

	        <cfreturn DataOut>

	</cffunction>


	<cffunction name="GetSessionsTableData" access="remote" output="false" returntype="any" hint="List all responses to all control points for a session - allow looping over session by date range">
		<cfargument name="inpBatchId" required="no" type="any" hint="Batch Id of the campaign">
		<cfargument name="inpStart" required="no" type="string" default="" hint="Start date to run report from">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="End date to run report to">
		<cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">

	    <!--- Paging --->
	    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
	    <cfargument name="iDisplayStart" required="no"  default="0">
	    <cfargument name="iDisplayLength" required="no"  default="5">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">

			<cfset var GetNumbersCount = '' />
			<cfset var qFiltered = '' />

			<cfset var listColumns	= '' />
			<cfset var tempItem	= '' />
			<cfset var thisS	= '' />
			<cfset var LoopIndex	= '' />

			<cfset var UTCConvertedDateTime = '' />
			<cfset var ISOFormatedDateTimeString =  '' />
			<cfset var RespondentString = '' />
			<cfset var USFormatPhoneNumber = '' />


			<cfset var DataOut = {}>

	        <!---ListEMSData is key of DataTable control--->
			<cfset DataOut["aaData"] = ArrayNew(1)>

	        <cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
	        <cfset DataOut.MESSAGE = "" />
	        <cfset DataOut.ERRMESSAGE = "" />

	        <cfset DataOut["iTotalRecords"] = 0>
	        <cfset DataOut["iTotalDisplayRecords"] = 0>

	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">


	        <cftry>

	            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->

	            <!--- SessionId_bi is in here twice as it is used in a hidden field for tr attr - sort wont work if columns are not all accounted for--->
	            <cfset listColumns = "SessionId_bi,Created_dt,SessionId_bi,ContactString_vch" />

	            <!---Get total  EMS for paginate --->
	            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
	                SELECT
						 COUNT(*) AS TOTALCOUNT
					FROM
			            simplequeue.sessionire
			        WHERE

			        <!--- Dont let users URL hack data that is not theirs --->
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>
		        </cfquery>

	            <cfif GetNumbersCount.TOTALCOUNT GT 0>
	                <cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
	                <cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	            </cfif>

	            <!--- Get data to display --->
	            <!--- Data set after filtering --->
	            <cfquery name="qFiltered" datasource="#Session.DBSourceREAD#">

		            SELECT
		            	SessionId_bi,
		            	Created_dt,
		            	ContactString_vch
			        FROM
			            simplequeue.sessionire
			        WHERE
						<!--- Dont let users URL hack data that is not theirs --->
		                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>

					<cfif iSortingCols gt 0>
		                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
		            </cfif>

		            <!---
			            By limiting results to paging here instead of once query returns, we can:
			            limit how much data is transfered from DB
			            the amount of memory this query results takes up
			        	Greatly speed up processing
			        --->
			     		<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength AND iDisplayLength GT 0>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    <cfelseif iDisplayLength LT 0>
	                    <!--- No limit records--->
	                    <cfelse>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    </cfif>

	            </cfquery>

	            <cfif OutQueryResults EQ "1">

		            <cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
					<cfset DataOut.TYPE = "" />
			        <cfset DataOut.MESSAGE = "" />
			        <cfset DataOut.ERRMESSAGE = "" />

	                <cfreturn DataOut />
	            </cfif>

	            <!---
	                Output
	             --->


	            <cfloop query="qFiltered">

	                <!--- Conver to UTC and then format as ISO string with UTC time zone for use by browser side javascript to convert to local timezone in reports--->
	                <cfset UTCConvertedDateTime = DateConvert( "Local2UTC", qFiltered.Created_dt ) />
	                <cfset ISOFormatedDateTimeString =  dateFormat( UTCConvertedDateTime, "yyyy-mm-dd" ) & "T" & timeFormat( UTCConvertedDateTime, "HH:mm:ss" ) & "Z" />

					<!--- If sorting by session id or date ASC - reverse the respondent numbers --->
					<cfif (url["iSortCol_0"] EQ 0 OR url["iSortCol_0"] EQ 1) AND  url["sSortDir_0"] EQ "asc">
						<cfset RespondentString = 'Respondent #DataOut["iTotalRecords"] - qFiltered.currentRow + 1 - iDisplayStart#' />
					<cfelse>
						<cfset RespondentString = 'Respondent #qFiltered.currentRow + iDisplayStart#' />
					</cfif>


					<cfif LEN(qFiltered.ContactString_vch) EQ 10>
						<cfset USFormatPhoneNumber = '(' & Left(qFiltered.ContactString_vch,3)& ') '& MID(qFiltered.ContactString_vch,4,3)& '-' &RIGHT(qFiltered.ContactString_vch,4)/>
					<cfelse>
						<cfset USFormatPhoneNumber = qFiltered.ContactString_vch/>
					</cfif>

	                <cfset tempItem = [
	                    '#RespondentString#',
	                    '#ISOFormatedDateTimeString#',
	                    '#qFiltered.SessionId_bi#',
	                    '#USFormatPhoneNumber#'
	                ]>
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>

	            <!--- only adjust blank rows for 5 or less requested records or else UI is ugly for larger amount of empty rows--->
				<cfif iDisplayLength LTE 5 >

	            	<!--- Append min to iDisplayLength - add blank rows --->
		            <cfloop from="#qFiltered.RecordCount#" to="#iDisplayLength#" step="1" index="LoopIndex">

		                <cfset tempItem = [
		                    ' ',
		                    ' ',
		                    ' ',
		                    ' '
		                ]>
		                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
		            </cfloop>

				</cfif>


	            <cfset DataOut.RXRESULTCODE = "1" />
				<cfset DataOut.TYPE = "" />
		        <cfset DataOut.MESSAGE = "" />
		        <cfset DataOut.ERRMESSAGE = "" />

	  		<cfcatch type="Any" >
				<cfset DataOut.RXRESULTCODE = -1 />
			    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
			    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset DataOut["iTotalRecords"] = 0>
				<cfset DataOut["iTotalDisplayRecords"] = 0>
				<cfset DataOut["aaData"] = ArrayNew(1)>
	        </cfcatch>
	        </cftry>

	        <cfreturn DataOut>

	</cffunction>

	<cffunction name="GetSessionDetails" access="remote" output="false" returntype="any" hint="Get the answers returned for a given session for a given batch - outputs HTML">
		<cfargument name="inpBatchId" required="no" type="any">
		<cfargument name="inpSessionId" required="no" type="string" default="0" hint="Session Id in which to get all the possible responses">
	    <cfargument name="inpContactString" required="no" type="any" default="Phone number or other contact string for this session">
	    <cfargument name="inpDateString" required="no" type="any" default="" hint="JS Moment formated string local to browser">

        <cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = "0" />
		<cfset dataout.TOTALCOUNT = "0" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset dataout.DETAILS_HTML = "" />

		<cfset var RetVarReadXMLQuestions = '' />
		<cfset var RetVarGetQuestionAnswer = '' />
		<cfset var USFormatPhoneNumber = inpContactString />
		<cfset var CPOBJ	= '' />
		<cfset var DETAILS_HTML	= '' />

        <cftry>


            <cfif LEN(arguments.inpContactString) EQ 10>
				<cfset USFormatPhoneNumber = '(' & Left(arguments.inpContactString,3)& ') '& MID(arguments.inpContactString,4,3)& '-' &RIGHT(arguments.inpContactString,4)/>
			<cfelse>
				<cfset USFormatPhoneNumber = inpContactString/>
			</cfif>

			<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestions" returnvariable="RetVarReadXMLQuestions">
                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
                <cfinvokeargument name="inpDefaultOption" value="">
                <cfinvokeargument name="inpTypeFilter" value="ONESELECTION,SHORTANSWER">
            </cfinvoke>

		    <cfsavecontent variable="DETAILS_HTML">
				<cfoutput>

					<div class="uk-width-1-1">
                        <ul class="uk-list stt-head-individual" style="">
                            <li>
                                <h4 id="customer-respondent-status"></h4>
                            </li>

                            <li>
                                <b>Session:</b> <span id="current-session-id">#inpSessionId#</span>
                            </li>
                            <li>
                                <b>Date:</b> <span id="customer-respondent-date">#inpDateString#</span>
                            </li>
                            <li>
                                <b>Phone Number:</b> <span id="customer-respondent-phone">#USFormatPhoneNumber#</span><a id="make-a-chat" title="Create new chat session to this customer" class="btn-chat-xs"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>

                    <div class="uk-width-1-1">
                        <hr class="line-space">
                    </div>


					<cfloop array="#RetVarReadXMLQuestions.QUESTIONS#" index="CPOBJ">


					    <cfinvoke method="GetQuestionAnswer" returnvariable="RetVarGetQuestionAnswer">
			                <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
			                <cfinvokeargument name="inpSessionId" value="#inpSessionId#">
			                <cfinvokeargument name="inpQID" value="#CPOBJ.ID#">
			                <cfinvokeargument name="inpQuestionType" value="#CPOBJ.TYPE#">
			            </cfinvoke>

			            <cfoutput>

							 <div class="portlet light">
			                    <div class="ques-item-details">
			                        <div><h4 class="title"><b>Q#CPOBJ.RQ#: #CPOBJ.TEXT#</b></h4></div>

			                        <cfif CPOBJ.TYPE EQ "ONESELECTION">

										<!--- Write out answer and bucket --->
							            <div class="answer-container">
								            <cfif RetVarGetQuestionAnswer.RECORDCOUNT GT 0 >
								                #RetVarGetQuestionAnswer.ANSWER#
											<cfelse>
												Question Not Answered
								            </cfif>
								        </div>

								        <div class="answer-container">
								            <cfif RetVarGetQuestionAnswer.RECORDCOUNT GT 0 >
								                Bucket: #RetVarGetQuestionAnswer.BUCKET#
											<cfelse>
												<!--- Intentionally Leave Blank for now --->
								            </cfif>
								        </div>

							        <cfelse>

							        	<!--- Write out answer only - there is no bucket --->
							            <div class="answer-container">
								            <cfif RetVarGetQuestionAnswer.RECORDCOUNT GT 0 >
								                #RetVarGetQuestionAnswer.ANSWER#
											<cfelse>
												Question Not Answered
								            </cfif>
								        </div>
			                        </cfif>


			                    </div>
							 </div>

			            </cfoutput>

					</cfloop>

				</cfoutput>

		    </cfsavecontent>



            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.DETAILS_HTML = "#DETAILS_HTML#" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.DETAILS_HTML = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>
        </cftry>

        <cfreturn dataout />

	</cffunction>


	<cffunction name="GetQuestionAnswer" access="public" output="false" hint="Get the answer for a given question for a given session">
    	<cfargument name="inpBatchId" required="true" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpSessionId" required="true" type="string" default="0" hint="Session Id in which to get all the possible responses">
	    <cfargument name="inpQID" TYPE="string" required="no" default="0" hint="The question's physical ID to look up - Physical Id here in case it moves" />
	    <cfargument name="inpQuestionType" TYPE="string" required="no" default="0" hint="The question's Type ONESELECTION OR SHORTANSWER " />

        <cfset var dataout = {} />
        <cfset var GetQuestionAnswerValue= '' />
        <cfset var GetQuestionAnswerValueBucket= '' />

        <!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.RECORDCOUNT = 0/>
		<cfset dataout.BATCHID = "#inpBatchId#"/>
		<cfset dataout.ANSWER = ""/>
		<cfset dataout.BUCKET = ""/>
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>

            <cfquery name="GetQuestionAnswerValue" datasource="#Session.DBSourceREAD#">
    			SELECT
    				IREResultsId_bi,
    				Response_vch,
    				T64_ti
    			FROM
    				simplexresults.ireresults
    			WHERE
    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
    			AND
    				IREType_int = 2
    			AND
    				QId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQID#">
    			AND
    				IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpSessionId#">
    			AND
    				LENGTH(ContactString_vch) < 14
    			ORDER BY
    				IREResultsId_bi DESC
    			LIMIT
    				100
    		</cfquery>

			<!--- Cheap loop to final record - assumes not very many repeat answers per session - no more than 100 or this wont work --->
			<cfloop query="GetQuestionAnswerValue">

				<cfif GetQuestionAnswerValue.T64_ti EQ 1>

					<cfset dataout.ANSWER = ToString( ToBinary( GetQuestionAnswerValue.Response_vch ), "UTF-8" ) />

				<cfelse>

					<cfset dataout.ANSWER = "#GetQuestionAnswerValue.Response_vch#"/>

				</cfif>

			</cfloop>

			<!--- check for a bucket assignment --->
			<cfif inpQuestionType EQ "ONESELECTION">

				<cfquery name="GetQuestionAnswerValueBucket" datasource="#Session.DBSourceREAD#">
	    			SELECT
	    				PKId_bi,
	    				Response_vch,
	    				T64_ti,
	    				ResponseRaw_vch,
	    				T64Raw_ti
	    			FROM
	    				simplexresults.irebucket
	    			WHERE
	    				BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#">
	    			AND
	    				IREType_int = 10
	    			AND
	    				QId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQID#">
	    			AND
	    				IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpSessionId#">
	    			AND
	    				LENGTH(ContactString_vch) < 14
	    			ORDER BY
	    				PKId_bi DESC
	    			LIMIT
	    				100
	    		</cfquery>

				<!--- Cheap loop to final record - assumes not very many repeat answers per session - no more than 100 or this wont work --->
				<cfloop query="GetQuestionAnswerValueBucket">

					<cfif GetQuestionAnswerValueBucket.T64_ti EQ 1>

						<cfset dataout.BUCKET = ToString( ToBinary( GetQuestionAnswerValueBucket.Response_vch ), "UTF-8" ) />

					<cfelse>

						<cfset dataout.BUCKET = "#GetQuestionAnswerValueBucket.Response_vch#"/>

					</cfif>

				</cfloop>


			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.RECORDCOUNT = "#GetQuestionAnswerValue.RecordCount#" />
			<cfset dataout.BATCHID = "#inpBatchId#"/>

		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>

     <cffunction name="LoadNoMatchesTable" access="remote" output="false" returntype="any" hint="list all unique No Match Responses with Count">
		<cfargument name="inpBatchId" required="no" type="any" hint="Batch Id of the campaign">
		<cfargument name="inpStart" required="no" type="string" default="" hint="Start date to run report from">
		<cfargument name="inpEnd" required="no" type="string" default="" hint="End date to run report to">
	    <cfargument name="inpQID" TYPE="string" required="no" default="0" hint="The question ID to look up - inpIDKey controls which key to use" />
        <cfargument name="inpIDKey" TYPE="string" required="no" default="RQ" hint="RQ will look up Question in order - ID will look up by physical question ID"/>

	    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">

	    <!--- Paging --->
	    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
	    <cfargument name="iDisplayStart" required="no"  default="0">
	    <cfargument name="iDisplayLength" required="no"  default="5">

		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection --->
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">

	        <cfset var GetNumbersCount = '' />
	        <cfset var qFiltered = '' />

	        <cfset var listColumns	= '' />
			<cfset var tempItem	= '' />
			<cfset var SortLoopItem	= '' />
			<cfset var LoopIndex	= '' />

	        <cfset var DataOut = {}>

	        <!---ListEMSData is key of DataTable control--->
			<cfset DataOut["aaData"] = ArrayNew(1)>

	        <cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
	        <cfset DataOut.MESSAGE = "" />
	        <cfset DataOut.ERRMESSAGE = "" />

	        <cfset DataOut["iTotalRecords"] = 0>
	        <cfset DataOut["iTotalDisplayRecords"] = 0>

	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">


	        <cftry>

	            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
	            <cfset listColumns = "Action,ResponseRaw_vch,TOTALCOUNT" />


	            <!---Get total  EMS for paginate --->
	            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
	                SELECT
						 COUNT(DISTINCT ResponseRaw_vch) AS TOTALCOUNT
					FROM
			            simplexresults.irebucket
			        WHERE

			        <!--- Dont let users URL hack data that is not theirs --->
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

			        <!--- optional Q ID filter some questions might get answered early but others never completed - this count will show how many answered this Question--->
	                <cfif inpQId GT 0>
	            	    AND
				    		QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQId#">
	                </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>

                         AND  Response_vch = "NO MATCH"
		        </cfquery>

	            <cfif GetNumbersCount.TOTALCOUNT GT 0>
	                <cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
	                <cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	            </cfif>

	            <!--- Get data to display --->
	            <!--- Data set after filtering --->
	            <cfquery name="qFiltered" datasource="#Session.DBSourceREAD#">

		            SELECT
		            	COUNT(*) AS TOTALCOUNT,
						ResponseRaw_vch
			        FROM
			            simplexresults.irebucket
			        WHERE
						<!--- Dont let users URL hack data that is not theirs --->
		                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">

				    <cfif TRIM(inpBatchId) NEQ "" AND TRIM(inpBatchId) NEQ "0000" AND TRIM(inpBatchId) NEQ "EXPORTALL">
						AND
				        	BatchId_bi IN (<cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
				    </cfif>

			        <!--- optional Q ID filter some questions might get answered early but others never completed - this count will show how many answered this Question--->
	                <cfif inpQId GT 0>
	            	    AND
				    		QID_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpQId#">
	                </cfif>

					<cfif TRIM(inpStart) NEQ "" AND TRIM(inpEnd) NEQ "" >
	                    AND
							Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
						AND
							Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                </cfif>

			        <cfif len(trim(sSearch))>
			    		AND
							ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
		     		</cfif>

                         AND  Response_vch = "NO MATCH"

			 		GROUP BY
						ResponseRaw_vch

					<cfif iSortingCols gt 0>
     		                ORDER BY
						<cfloop from="0" to="#iSortingCols-1#" index="SortLoopItem">
							  <cfif SortLoopItem is not 0>,</cfif>
							  #listGetAt(listColumns,(url["iSortCol_"&SortLoopItem]+1))#
							  <cfif listFindNoCase("asc,desc",url["sSortDir_"&SortLoopItem]) gt 0>#url["sSortDir_"&SortLoopItem]#</cfif>
						</cfloop>
     		          </cfif>

		            <!---
			            By limiting results to paging here instead of once query returns, we can:
			            limit how much data is transfered from DB
			            the amount of memory this query results takes up
			        	Greatly speed up processing
			        --->
			     		<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength AND iDisplayLength GT 0>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    <cfelseif iDisplayLength LT 0>
	                    <!--- No limit records--->
	                    <cfelse>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    </cfif>

	            </cfquery>

	            <cfif OutQueryResults EQ "1">

		            <cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
					<cfset DataOut.TYPE = "" />
			        <cfset DataOut.MESSAGE = "" />
			        <cfset DataOut.ERRMESSAGE = "" />

	                <cfreturn DataOut />
	            </cfif>

	            <!---
	                Output
	             --->


	            <cfloop query="qFiltered">

	                <cfset tempItem = [
	                    '#qFiltered.ResponseRaw_vch#',
	                    '#qFiltered.TOTALCOUNT#'
	                ]>
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>


<!---
	             <!--- Append min to 5 - add blank rows --->
	            <cfloop from="#qFiltered.RecordCount#" to="#iDisplayLength#" step="1" index="LoopIndex">

	                <cfset tempItem = [
	                    ' ',
	                    ' '
	                ]>
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>
--->

               <cfset DataOut.RXRESULTCODE = "1" />
               <cfset DataOut.TYPE = "" />
               <cfset DataOut.MESSAGE = "" />
               <cfset DataOut.ERRMESSAGE = "" />

	  		<cfcatch type="Any" >
				<cfset DataOut.RXRESULTCODE = -1 />
			    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
			    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset DataOut["iTotalRecords"] = 0>
				<cfset DataOut["iTotalDisplayRecords"] = 0>
				<cfset DataOut["aaData"] = ArrayNew(1)>
	        </cfcatch>
	        </cftry>

	        <cfreturn DataOut>

	</cffunction>

	<cffunction name="UpdateResponseBucket" access="remote" output="false" hint="Update all matching Responses to a particular bucket.">
		<cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />
		<cfargument name="inpResponse" TYPE="string" hint="The Bucket Response" />
		<cfargument name="inpResponseRaw" TYPE="string" hint="The Raw response to upadate where current bucket is NO MATCH" />
		<cfargument name="inpQId" TYPE="any" required="yes" default="1" hint="The Control Point - Physical Id"/>

		<cfset var dataout = {}>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var UpdateKeywordQuery = '' />

		<!--- Set defaults --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>

			<!--- Secure by session User Id - dont let other users web tools hack data they dont own --->
			<!--- Update keyword in db --->
			<cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplexresults.irebucket
				SET
					Response_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpResponse#">
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
				AND
				    	BatchId_bi = <cfqueryparam value="#inpBatchId#" cfsqltype="CF_SQL_BIGINT" />
			     AND
				    	QId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpQID#">
				AND
					Response_vch = 'NO MATCH'
				AND
					ResponseRaw_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpResponseRaw#">
			</cfquery>

			<!--- Log the Update --->
			<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
				<cfinvokeargument name="moduleName" value="Response Bucket Updated via Reporting Tool"/>
				<cfinvokeargument name="operator" value="inpBatchId = #inpBatchId# inpQID = #inpQID# inpResponse=#inpResponse#"/>
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch TYPE="any">

			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

	   </cftry>

	   <cfreturn dataout />

	</cffunction>

</cfcomponent>
