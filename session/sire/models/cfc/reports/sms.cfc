<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
	
	<cffunction name="Display_c_smsmt" access="remote" output="false" returntype="any" hint="Display Data for SMS MT Counts">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	
		<cfset var GetSMSResults = StructNew()/>
	    <cfset var outPutToDisplay = ''/>
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />	
	
		<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	        
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	        SELECT
	            COUNT(IREResultsId_bi) AS TOTALCOUNT
	        FROM
	            simplexresults.ireresults             
	        WHERE  
	           	created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND         
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>                
	        AND
	        	IREType_int = 1     
	            
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 AND TRIM(inpBatchIdList) NEQ "">
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                	
	                	UNION
	                
	                    SELECT
	                        COUNT(IREResultsId_bi) AS TOTALCOUNT
	                    FROM
	                        simplexresults.ireresults             
	                    WHERE  
	                       	created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND        
	          		    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	        				IREType_int = 1 
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>    
	                       
	    </cfquery>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TOTALCOUNT) AS TOTALCOUNT
	            FROM	
	                GetSMSResults            
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset UnionSumQuery.TOTALCOUNT = 0 />
	        
	    </cfif>
	    
	        
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
	              
	               <div class="head-EMS"><div class="DashObjHeaderText">SMS MT Counter</div></div>
	          
	               <!--- Keep table auto-scroll in managed area--->
	               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
	                    <div class="Absolute-Center">            
	                        <h1>#LSNUMBERFORMAT(UnionSumQuery.TOTALCOUNT, ",")#</h1>
	                        <h2>SMS MT Sent</h2>
	                    </div>             
	                <!---</div>--->
	              
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction> 
	
	<cffunction name="Display_c_smsmo" access="remote" output="false" returntype="any" hint="Display Data for SMS MO Counts">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	
		<cfset var GetSMSResults = StructNew()/>
	    <cfset var outPutToDisplay = ''/>
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />	
	
		<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	        
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	        SELECT
	            COUNT(IREResultsId_bi) AS TOTALCOUNT
	        FROM
	            simplexresults.ireresults             
	        WHERE  
	           	created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND        
	            <!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>    
	        AND
	        	IREType_int = 2     
	            
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 AND TRIM(inpBatchIdList) NEQ "">
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                	
	                	UNION
	                
	                    SELECT
	                        COUNT(IREResultsId_bi) AS TOTALCOUNT
	                    FROM
	                        simplexresults.ireresults             
	                    WHERE  
	                       	created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND        
	          		    	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	        				IREType_int = 2  
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>    
	                       
	    </cfquery>
	            
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TOTALCOUNT) AS TOTALCOUNT
	            FROM	
	                GetSMSResults            
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset UnionSumQuery.TOTALCOUNT = 0 />
	        
	    </cfif>
	    
	        
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
	              
	               <div class="head-EMS"><div class="DashObjHeaderText">SMS MO Counter</div></div>
	          
	               <!--- Keep table auto-scroll in managed area--->
	               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
	                    <div class="Absolute-Center">            
	                        <h1>#LSNUMBERFORMAT(UnionSumQuery.TOTALCOUNT, ",")#</h1>
	                        <h2>SMS MO Recieved</h2>
	                    </div>             
	                <!---</div>--->
	              
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction> 
	
	<cffunction name="Display_c_smsmo_noq" access="remote" output="false" returntype="any" hint="Display Data for SMS MO Counts">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	
		<cfset var GetSMSResults = StructNew()/>
	    <cfset var outPutToDisplay = ''/>
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />	
	
		<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	
	
		<cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.TOTALCOUNT = -1 />
	        <cfset GetSMSResults.RECORDCOUNT = 0 />
	        
	    <cfelse>    
	         
	        <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	            SELECT
	                COUNT(surveyresultsid_bi) AS TOTALCOUNT
	            FROM
	                simplexresults.surveyresults             
	            WHERE  
	               created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	            AND
	               created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	            AND        
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
	            AND
	                Response_vch NOT LIKE 'EBM -%'
	            AND
	                CPId_int > 0  
	                
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                    	
	                    	UNION
	                    
	                        SELECT
	                            COUNT(surveyresultsid_bi) AS TOTALCOUNT
	                        FROM
	                            simplexresults.surveyresults             
	                        WHERE  
	                           created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                        AND
	                           created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                        AND        
	              		     	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                        AND
	                            Response_vch NOT LIKE 'EBM -%'
	                        AND
	                            CPId_int > 0
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>    
	                           
	        </cfquery>
	        
	    </cfif>
	    
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TOTALCOUNT) AS TOTALCOUNT
	            FROM	
	                GetSMSResults            
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset UnionSumQuery.TOTALCOUNT = 0 />
	        
	    </cfif>
	    
	        
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
	              
	               <div class="head-EMS"><div class="DashObjHeaderText">SMS MO Counter</div></div>
	          
	               <!--- Keep table auto-scroll in managed area--->
	               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
	                    <div class="Absolute-Center">            
	                        <h1>#LSNUMBERFORMAT(UnionSumQuery.TOTALCOUNT, ",")#</h1>
	                        <h2>SMS MO Recieved</h2>
	                    </div>             
	                <!---</div>--->
	              
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction> 
	
	
	<cffunction name="Display_c_smsmo_sys" access="remote" output="false" returntype="any" hint="Display Data for SMS MO Counts">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	
		<cfset var GetSMSResults = StructNew/>
	    <cfset var outPutToDisplay = ''/>
	
		<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	
	
		<cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.TOTALCOUNT = -1 />
	    	<cfset GetSMSResults.RECORDCOUNT = 0 />
	        
	    <cfelse>    
	         
	        <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	            SELECT
	                COUNT(surveyresultsid_bi) AS TOTALCOUNT
	            FROM
	                simplexresults.surveyresults             
	            WHERE  
	               created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	            AND
	               created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">                      
	        </cfquery>
	        
	    </cfif>
	        
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
	              
	               <div class="head-EMS"><div class="DashObjHeaderText">SMS MO Counter</div></div>
	          
	                <!--- Keep table auto-scroll in managed area--->
	               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
	                    <div class="Absolute-Center">            
	                        <h1>#LSNUMBERFORMAT(GetSMSResults.TOTALCOUNT, ",")#</h1>
	                        <h2>SMS MO Recieved</h2>
	                    </div>             
	                <!---</div>--->
	              
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction> 
	
	
	<!---- Total Calls by State bar chart---->
	<cffunction name="MTCountByState" access="remote" output="false" returntype="any" hint="Total Calls by State">
		<cfargument name="inpBatchIdList" required="yes" type="any" hint="from 1 to up to 100 comma seperated list of Batch IDs">
	    <cfargument name="inpChartPostion" required="yes" type="any" hint="Used to specify a Title for the current chart report object.">
	  	<cfargument name="inpStart" required="yes" type="string" hint="Starting date range  - will always be converted to Midnoght start time" >
		<cfargument name="inpEnd" required="yes" type="string" hint="End date range - will always be converted to Midnoght start time">
	    
	    <!--- Declare thread safe variables here - always use var scope in cfc's --->
	    <cfset var TotalCallsByState = StructNew()  />    
	    <cfset var GetSMSResults =  StructNew() />
	    <cfset var barChartObj = '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var category	= '' />
		<cfset var data	= '' />
	    
	    <!--- Make sure query is searching entire date range --->    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	     
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	        SELECT 
			   	count(*) as total,
					l.state as state				
	        FROM 	
	            simplexresults.ireresults cd 
	            inner join melissadata.FONE l ON
	            cd.NPA_vch = l.NPA
	                    AND
	                       	cd.NXX_vch = l.NXX               
	        WHERE
	           	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>   
	        AND
	            IREType_int = 1
	        GROUP BY
	            l.state
	        
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->
	        <!--- Max 100 in a list --->		
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                    UNION
	                    
	                    SELECT 
	                        count(*) as total,
	                        l.state as state				
	                    FROM 	
	                        simplexresults.ireresults cd 
	                        inner join melissadata.FONE l ON
	                        <!---cd.dial6_vch = concat(NPA,NXX)--->
	                        <!--- Left and MID seems to work better on mySQL for this than CONCAT --->    
	                        LEFT(cd.contactstring_vch, 3) = l.NPA
	                    AND
	                        MID(cd.contactstring_vch, 4,3) = l.NXX               
	                    WHERE
	                       	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	            			IREType_int = 1
	                    GROUP BY
	                        l.state
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>          
	                                     
	    </cfquery>
	    
	 	
		
	    <!--- Sum up all options --->       
		<cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
	            SELECT 	
	                state,
	                SUM(total) AS TOTAL
	            FROM	
	                GetSMSResults
	            GROUP BY
	                state
	            ORDER BY 
	                state ASC
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset TotalCallsByState.RecordCount = 0 />
	        
	    </cfif>
	    
	    <!--- Create a standardized bar chart object  --->
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <cfset barChartObj.setYAxisTitle("Total MT Count") />
	    <!---<cfset barChartObj.setChartType(chartReportingType) /> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	    
	    
	    <!--- Show no data--->
	    <cfif TotalCallsByState.RecordCount EQ 0>
	    	 
	        <!--- Cleanly handle no data found ---> 
	        <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	        
	        <cfloop query="TotalCallsByState">
	        		
	             <!--- gracefully handle unknown states --->   
	             <cfif TotalCallsByState.state EQ "">
	             	<cfset TotalCallsByState.state = "UNK" />
	             </cfif>   
	        
	             <cfset ArrayAppend(category, TotalCallsByState.state)/>
	             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
	        </cfloop>   
	
		</cfif>
	
		<!--- Set the title here --->
		<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by State");') />
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <!--- Return raw chart data --->
	    <cfreturn barChartObj.drawChart()>
	    
	</cffunction>
	
	<!---- Total SMS MT by Zip Code bar chart---->
	<cffunction name="MTCountByZip" access="remote" output="false" returntype="any" hint="Total SMS MT by Zip Code">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpChartPostion" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">    
	    
	    <cfset var TotalCallsByState = StructNew()  />    
	    <cfset var GetSMSResults =  StructNew() />
	    <cfset var barChartObj = '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var category	= '' />
		<cfset var data	= '' />
	    
	    <!--- Make sure query is searching entire date range --->    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	             
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	        SELECT 
			   	count(*) as total,
					l.zip1 as state				
	        FROM 	
	            simplexresults.ireresults cd 
	            inner join melissadata.FONE l ON
	            cd.NPA_vch = l.NPA
            AND
               	cd.NXX_vch = l.NXX               
	        WHERE
	           	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>   
	        AND
	            IREType_int = 1
	        GROUP BY
	            l.zip1
	        
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                    UNION
	                    
	                    SELECT 
	                        count(*) as total,
	                        l.zip1 as state				
	                    FROM 	
	                        simplexresults.ireresults cd 
	                        inner join melissadata.FONE l ON
	                        cd.NPA_vch = l.NPA
	                    AND
	                       	cd.NXX_vch = l.NXX    
	                    WHERE
	                       	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	            			IREType_int = 1
	                    GROUP BY
	                        l.zip1
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>          
	                                     
	    </cfquery>
	    
	
		       
		<cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
	            SELECT 	
	                state,
	                SUM(total) AS TOTAL
	            FROM	
	                GetSMSResults
	            GROUP BY
	                state
	            ORDER BY 
	                state ASC
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset TotalCallsByState.RecordCount = 0 />
	        
	    </cfif>
	    
	    
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <!---<cfset barChartObj.setTitle("Total SMS MT by State") />--->
	    <cfset barChartObj.setYAxisTitle("Total MT Count") />
<!--- 	    <!---<!---<cfset barChartObj.setChartType(chartReportingType) /> ---> ---> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	    
	    
	    <!--- Show no data--->
	    <cfif TotalCallsByState.RecordCount EQ 0>
	    	 
	        <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	        
	        <cfloop query="TotalCallsByState">
	        		
	             <cfif TotalCallsByState.state EQ "">
	             	<cfset TotalCallsByState.state = "UNK" />
	             </cfif>   
	        
	             <cfset ArrayAppend(category, TotalCallsByState.state)/>
	             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
	        </cfloop>   
	
		</cfif>
	
	
		<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by Zip Code");') />
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	    
	</cffunction>
	
	
	
	
	<!---- Total SMS MT by FIPS bar chart---->
	<cffunction name="MTCountByFIPS" access="remote" output="false" returntype="any" hint="Total SMS MT by FIPS">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpChartPostion" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">    
	    
	    <cfset var TotalCallsByState = StructNew()  />    
	    <cfset var GetSMSResults =  StructNew() />
	    <cfset var barChartObj = '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var category	= '' />
		<cfset var data	= '' />
	    
	    <!--- Make sure query is searching entire date range --->    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	            
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	        SELECT 
			   	count(*) as total,
					CASE
				WHEN l.fips IS NULL OR trim(l.fips) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
				ELSE l.fips 
				END
				as state			
	        FROM 	
	            simplexresults.ireresults cd 
	            inner join melissadata.FONE l ON
	            cd.NPA_vch = l.NPA
            AND
               	cd.NXX_vch = l.NXX               
	        WHERE
	           	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>  
	        AND
	            IREType_int = 1
	        GROUP BY
	            l.fips
	        
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                    UNION
	                    
	                    SELECT 
	                        count(*) as total,
	                        CASE
	                        WHEN l.fips IS NULL OR trim(l.fips) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
	                        ELSE l.fips 
	                        END
	                        as state		
	                    FROM 	
	                        simplexresults.ireresults cd 
	                        inner join melissadata.FONE l ON
	                        cd.NPA_vch = l.NPA
	                    AND
	                       	cd.NXX_vch = l.NXX             
	                    WHERE
	                       	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	            			IREType_int = 1
	                    GROUP BY
	                        l.fips
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>          
	                                     
	    </cfquery>
	           
		<cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
	            SELECT 	
	                state,
	                SUM(total) AS TOTAL
	            FROM	
	                GetSMSResults
	            GROUP BY
	                state
	            ORDER BY 
	                state ASC
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset TotalCallsByState.RecordCount = 0 />
	        
	    </cfif>
	    
	    
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <!---<cfset barChartObj.setTitle("Total SMS MT by State") />--->
	    <cfset barChartObj.setYAxisTitle("Total MT Count") />
	    <!---<!---<cfset barChartObj.setChartType(chartReportingType) /> ---> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	    
	    
	    <!--- Show no data--->
	    <cfif TotalCallsByState.RecordCount EQ 0>
	    	 
	        <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	        
	        <cfloop query="TotalCallsByState">
	        		
	             <cfif TotalCallsByState.state EQ "">
	             	<cfset TotalCallsByState.state = "UNK" />
	             </cfif>   
	        
	             <cfset ArrayAppend(category, TotalCallsByState.state)/>
	             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
	        </cfloop>   
	
		</cfif>
	
	
		<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by Zip Code");') />
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	    
	</cffunction>
	
	
	<!---- Total SMS MT by MSA bar chart---->
	<cffunction name="MTCountByMSA" access="remote" output="false" returntype="any" hint="Total SMS MT by MSA">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpChartPostion" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">    
	    
	    <cfset var TotalCallsByState = StructNew()  />    
	    <cfset var GetSMSResults =  StructNew() />
	    <cfset var barChartObj = '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var category	= '' />
		<cfset var data	= '' />
	    
	    <!--- Make sure query is searching entire date range --->    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	        SELECT 
			   	count(*) as total,
					CASE
				WHEN l.msa IS NULL OR trim(l.msa) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
				ELSE l.msa 
				END
				as state			
	        FROM 	
	            simplexresults.ireresults cd 
	            inner join melissadata.FONE l ON
	            cd.NPA_vch = l.NPA
            AND
               	cd.NXX_vch = l.NXX               
	        WHERE
	           	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>   
	        AND
	            IREType_int = 1
	        GROUP BY
	            l.msa
	        
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                    UNION
	                    
	                    SELECT 
	                        count(*) as total,
	                        CASE
	                        WHEN l.msa IS NULL OR trim(l.msa) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
	                        ELSE l.msa 
	                        END
	                        as state		
	                    FROM 	
	                        simplexresults.ireresults cd 
	                        inner join melissadata.FONE l ON
	                        cd.NPA_vch = l.NPA
	                    AND
	                       	cd.NXX_vch = l.NXX              
	                    WHERE
	                       	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	            			IREType_int = 1
	                    GROUP BY
	                        l.msa
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>          
	                                     
	    </cfquery>
	           
		<cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
	            SELECT 	
	                state,
	                SUM(total) AS TOTAL
	            FROM	
	                GetSMSResults
	            GROUP BY
	                state
	            ORDER BY 
	                state ASC
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset TotalCallsByState.RecordCount = 0 />
	        
	    </cfif>
	    
	    
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <!---<cfset barChartObj.setTitle("Total SMS MT by State") />--->
	    <cfset barChartObj.setYAxisTitle("Total MT Count") />
	    <!---<!---<cfset barChartObj.setChartType(chartReportingType) /> ---> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	    
	    
	    <!--- Show no data--->
	    <cfif TotalCallsByState.RecordCount EQ 0>
	    	 
	        <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	        
	        <cfloop query="TotalCallsByState">
	        		
	             <cfif TotalCallsByState.state EQ "">
	             	<cfset TotalCallsByState.state = "UNK" />
	             </cfif>   
	        
	             <cfset ArrayAppend(category, TotalCallsByState.state)/>
	             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
	        </cfloop>   
	
		</cfif>
	
	
		<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by Zip Code");') />
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	    
	</cffunction>
	
	
	<!---- Total SMS MT by PMSA bar chart---->
	<cffunction name="MTCountByPMSA" access="remote" output="false" returntype="any" hint="Total SMS MT by PMSA">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpChartPostion" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">    
	    
	    <cfset var TotalCallsByState = StructNew()  />    
	    <cfset var GetSMSResults =  StructNew() />
	    <cfset var barChartObj = '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var category	= '' />
		<cfset var data	= '' />
	    
	    <!--- Make sure query is searching entire date range --->    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	         
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	        SELECT 
			   	count(*) as total,
					CASE
				WHEN l.pmsa IS NULL OR trim(l.pmsa) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
				ELSE l.pmsa 
				END
				as state			
	        FROM 	
	            simplexresults.ireresults cd 
	            inner join melissadata.FONE l ON
	            cd.NPA_vch = l.NPA
            AND
               	cd.NXX_vch = l.NXX               
	        WHERE
	           	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	<!--- Allow lookup of batch list OR All of User account --->       
	        	<cfif TRIM(inpBatchIdList) NEQ "">            
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                  
	            <cfelse>                	
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
	            </cfif>  
	        AND
	            IREType_int = 1
	        GROUP BY
	            l.pmsa
	        
	        <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
				<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	        
	            <cfset ListIndexCounter = 1 />	
	            
	            <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	        
	        		<!--- Single case already ran above --->
	                <cfif ListIndexCounter GT 1>
	                    UNION
	                    
	                    SELECT 
	                        count(*) as total,
	                        CASE
	                        WHEN l.pmsa IS NULL OR trim(l.pmsa) = ""  THEN concat('(NPANXX-',NPA,NXX, ')') 				
	                        ELSE l.pmsa 
	                        END
	                        as state		
	                    FROM 	
	                        simplexresults.ireresults cd 
	                        inner join melissadata.FONE l ON
	                        cd.NPA_vch = l.NPA
	                    AND
	                       	cd.NXX_vch = l.NXX             
	                    WHERE
	                       	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                    AND
	                       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                    AND
	                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#"> 
	                    AND
	            			IREType_int = 1
	                    GROUP BY
	                        l.pmsa
	                </cfif>        
	        
	                <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                
	            </cfloop>
	        
	        </cfif>          
	                                     
	    </cfquery>
	      
		<cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="TotalCallsByState" dbtype="query" maxrows="125">
	            SELECT 	
	                state,
	                SUM(total) AS TOTAL
	            FROM	
	                GetSMSResults
	            GROUP BY
	                state
	            ORDER BY 
	                state ASC
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset TotalCallsByState.RecordCount = 0 />
	        
	    </cfif>
	    
	    
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <!---<cfset barChartObj.setTitle("Total SMS MT by State") />--->
	    <cfset barChartObj.setYAxisTitle("Total MT Count") />
	    <!---<!---<cfset barChartObj.setChartType(chartReportingType) /> ---> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	    
	    
	    <!--- Show no data--->
	    <cfif TotalCallsByState.RecordCount EQ 0>
	    	 
	        <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	        
	        <cfloop query="TotalCallsByState">
	        		
	             <cfif TotalCallsByState.state EQ "">
	             	<cfset TotalCallsByState.state = "UNK" />
	             </cfif>   
	        
	             <cfset ArrayAppend(category, TotalCallsByState.state)/>
	             <cfset ArrayAppend(data, TotalCallsByState.total)/>   
	        </cfloop>   
	
		</cfif>
	
	
		<cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Total SMS MT by Zip Code");') />
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	    
	</cffunction>
	
	<cffunction name="Display_c_smsSurvey" access="remote" output="false" returntype="any" hint="Display Data for SMS MT Counts">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	
		<cfset var GetSMSResults = StructNew()/>
	    <cfset var outPutToDisplay = ''/>
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />	
	
		<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.RECORDCOUNT = -1 />
	        
	    <cfelse>    
	         
	        <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	            SELECT
	                COUNT(SessionId_bi) AS TOTALCOUNT
	            FROM 	
	                simplequeue.sessionire
	            WHERE
	               	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	            AND
	               	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	            AND
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">                
	             
	                
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                        UNION
	                        
	                        SELECT
	                            COUNT(SessionId_bi) AS TOTALCOUNT
	                        FROM 	
	                            simplequeue.sessionire
	                        WHERE
	                           Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                        AND
	                           Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                        AND
	                           BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#">                
	                                               
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>                      
	                   
	        </cfquery>
	        
	    </cfif>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TOTALCOUNT) AS TOTALCOUNT
	            FROM	
	                GetSMSResults            
	        </cfquery>
	    
	    <cfelse>
	    
		    <cfset UnionSumQuery.TOTALCOUNT = 0 />
	        
	    </cfif>    
	    
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
	              
	               <div class="head-EMS"><div class="DashObjHeaderText">SMS Survey Counter</div></div>
	          
	                <!--- Keep table auto-scroll in managed area--->
	               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
	                    <div class="Absolute-Center">            
	                        <h1>#LSNUMBERFORMAT(UnionSumQuery.TOTALCOUNT, ",")#</h1>
	                        <h2>SMS Surveys Started</h2>
	                    </div>             
	                <!---</div>--->
	              
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction> 
	
	
	<!---- total message sent results pie chart---->
	<cffunction name="SMSSurveyComboResults" access="remote" output="true" returntype="any">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="ShowLegend" type="string" default="false" required="no"/>
	    <cfargument name="CustomPie" type="string" default="false" required="no"/>
	    
	    <cfset var GetSMSResults = StructNew()>
	    <cfset var piechartObj = ''>
	    <cfset var dataItem = ''>
	    <cfset var pieData	= '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />
	    <cfset var TotalCount	= '' />
	    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <!--- See SMS cfc/csc/contstants.cfm file for value meanings --->
	      
		<cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.RecordCount = -1 />
	        <cfset GetSMSResults.NOTASURVEY = -1 />
	        <cfset GetSMSResults.RUNNING = -1 />
	        <cfset GetSMSResults.INTERVALHOLD = -1 />
	        <cfset GetSMSResults.INTERVALRESPONSE = -1 />
	        <cfset GetSMSResults.COMPLETE = -1 />
	        <cfset GetSMSResults.CANCELLED = -1 />
	        <cfset GetSMSResults.EXPIRED = -1 />
	        <cfset GetSMSResults.TERMINATED = -1 />
	        <cfset GetSMSResults.STOPPED = -1 />
	        
	    <cfelse>    
	         
	       <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	            SELECT            
	                    sum(case when SessionState_int = 0 then 1 else 0 end) as 'NOTASURVEY',
	                    sum(case when SessionState_int = 1 then 1 else 0 end) as 'RUNNING',
	                    sum(case when SessionState_int = 2 then 1 else 0 end) as 'INTERVALHOLD',
	                    sum(case when SessionState_int = 3 then 1 else 0 end) as 'INTERVALRESPONSE',
	                    sum(case when SessionState_int = 4 then 1 else 0 end) as 'COMPLETE',
	                    sum(case when SessionState_int = 5 then 1 else 0 end) as 'CANCELLED',
	                    sum(case when SessionState_int = 6 then 1 else 0 end) as 'EXPIRED',
	                    sum(case when SessionState_int = 7 then 1 else 0 end) as 'TERMINATED',
	                    sum(case when SessionState_int = 8 then 1 else 0 end) as 'STOPPED'     
	            FROM 	
	                simplequeue.sessionire
	            WHERE
	               Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	            AND
	               Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	            AND        
	               BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#">       
	                           
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                        UNION
	                        
	                        SELECT        
	                                sum(case when SessionState_int = 0 then 1 else 0 end) as 'NOTASURVEY',
	                                sum(case when SessionState_int = 1 then 1 else 0 end) as 'RUNNING',
	                                sum(case when SessionState_int = 2 then 1 else 0 end) as 'INTERVALHOLD',
	                                sum(case when SessionState_int = 3 then 1 else 0 end) as 'INTERVALRESPONSE',
	                                sum(case when SessionState_int = 4 then 1 else 0 end) as 'COMPLETE',
	                                sum(case when SessionState_int = 5 then 1 else 0 end) as 'CANCELLED',
	                                sum(case when SessionState_int = 6 then 1 else 0 end) as 'EXPIRED',
	                                sum(case when SessionState_int = 7 then 1 else 0 end) as 'TERMINATED',
	                                sum(case when SessionState_int = 8 then 1 else 0 end) as 'STOPPED'     
	                        FROM 	
	                            simplequeue.sessionire 
	                        WHERE
	                           Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	                        AND
	                           Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	                        AND        
	                           BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#">                
	                                              
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>          
	            
	        </cfquery>
	        
	    </cfif>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(NOTASURVEY) AS NOTASURVEY,
	                SUM(RUNNING) AS RUNNING,
	                SUM(INTERVALHOLD) AS INTERVALHOLD,
	                SUM(INTERVALRESPONSE) AS INTERVALRESPONSE,
	                SUM(COMPLETE) AS COMPLETE,
	                SUM(CANCELLED) AS CANCELLED,
	                SUM(EXPIRED) AS EXPIRED,
	                SUM(TERMINATED) AS TERMINATED,
	                SUM(STOPPED) AS STOPPED                
	            FROM	
	                GetSMSResults            
	        </cfquery>
	    
	    <cfelse>
	    	   
	        <cfset UnionSumQuery.RecordCount = 0 />
	        <cfset UnionSumQuery.NOTASURVEY = 0 />
	        <cfset UnionSumQuery.RUNNING = 0 />
	        <cfset UnionSumQuery.INTERVALHOLD = 0 />
	        <cfset UnionSumQuery.INTERVALRESPONSE = 0 />
	        <cfset UnionSumQuery.COMPLETE = 0 />
	        <cfset UnionSumQuery.CANCELLED = 0 />
	        <cfset UnionSumQuery.EXPIRED = 0 />
	        <cfset UnionSumQuery.TERMINATED = 0 />
	        <cfset UnionSumQuery.STOPPED = 0 />        
	        
	    </cfif>   
	    
	    
		<!--- Call Result Data --->
		
		<cfset piechartObj = createObject("component", "session.sire.models.cfc.reports.lib.Piechart").init() />
		<cfset piechartObj.setTitle("Call Results") />
		<cfset piechartObj.setChartType(chartReportingType) />
		 
	     
	     <!---prepare data---> 
	     <cfset pieData = ArrayNew(1)>
	        
	     <cfif GetSMSResults.RecordCount EQ 0>
	    	           
	        <cfset dataItem =[ 'No Data', '1' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	       
	    <cfelse>
	    
	    	<!--- Pie charts need at least one nont null value to display or else will be blank--->
	    	<cfset TotalCount = val(UnionSumQuery.NOTASURVEY) + val(UnionSumQuery.RUNNING) + val(UnionSumQuery.INTERVALHOLD) + val(UnionSumQuery.INTERVALRESPONSE) + val(UnionSumQuery.COMPLETE) + val(UnionSumQuery.CANCELLED) + val(UnionSumQuery.EXPIRED) + val(UnionSumQuery.TERMINATED)  + val(UnionSumQuery.STOPPED) >
	        
	        <cfif TotalCount EQ 0>
	        	<cfset dataItem =[ 'No Data', '1' ]>
	        	<cfset ArrayAppend(pieData, dataItem)>	        
	        </cfif>
	    
	        
	        <cfset dataItem =[ 'NOT A SURVEY', '#UnionSumQuery.NOTASURVEY#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'RUNNING', '#UnionSumQuery.RUNNING#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL HOLD', '#UnionSumQuery.INTERVALHOLD#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL RESPONSE', '#UnionSumQuery.INTERVALRESPONSE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'COMPLETE', '#UnionSumQuery.COMPLETE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'CANCELLED', '#UnionSumQuery.CANCELLED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'EXPIRED', '#UnionSumQuery.EXPIRED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'TERMINATED', '#UnionSumQuery.TERMINATED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	    
		    <cfset dataItem =[ 'STOPPED', '#UnionSumQuery.STOPPED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	    </cfif>
	    
		
		<cfset piechartObj.setData(pieData) />
	    
	    <cfset piechartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "SMS Survey Status");') />
	 	
	    <cfif CustomPie EQ "false">
			<cfreturn piechartObj.drawChart(ShowLegend) />
	    <cfelse>
	        <cfreturn piechartObj.drawAmChartCustomPie(ShowLegend) />
	    </cfif>
	    
	</cffunction>
	    
	<!---- total message sent results pie chart---->
	<cffunction name="SMSSurveyPartialResults" access="remote" output="true" returntype="any">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="ShowLegend" type="string" default="false" required="no"/>
	    <cfargument name="CustomPie" type="string" default="false" required="no"/>
	    
	    <cfset var GetSMSResults = ''>
	    <cfset var piechartObj = ''>
	    <cfset var dataItem = ''>
	    <cfset var pieData	= '' />
	    <cfset var TotalCount	= '' />
		<cfset var GetSMSResultsTotals	= '' />
	    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <!--- See SMS cfc/csc/contstants.cfm file for value meanings --->
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	    
	    OR MAYBEY 
	    
	    SELECT 
	            COUNT(DISTINCT sr.contactstring_vch) AS TOTALCOUNT
	        FROM 	 	
				simplexresults.contactresults cd <!---FORCE INDEX (IDX_CDLDT_BATCH_CALLRESULT) --->
			INNER JOIN
		        simplexresults.surveyresults sr FORCE INDEX (IDX_Contact_Created)
	        ON
	            cd.contactstring_vch = sr.contactstring_vch
	        LEFT OUTER JOIN
	            simpleobjects.batch b
	        ON 
	            b.BatchId_bi = sr.BatchId_bi 
	        WHERE
				rxcdlstarttime_dt >= '2014-05-14 00:00:00'
	        AND
				rxcdlstarttime_dt <= '2014-05-15 00:00:00'
			AND
			   	sr.created_dt >= '2014-05-14 00:00:00'
	        AND
	           	sr.created_dt <= '2014-05-15 00:00:00'
	      	AND 
				Response_vch NOT LIKE '%INTERVAL%' 		
	        AND       
	        	b.userId_int = 316
			AND
				SMSResult_int = 100	
	            
	            .
	            .
	            .
	            .
	            
	            
	    
	        SELECT 
	            COUNT(DISTINCT sr.contactstring_vch) AS TOTALCOUNT
	        FROM 
	            simplequeue.moinboundqueue mib  FORCE INDEX (IDX_ContactString)
	        LEFT OUTER JOIN
	            simplexresults.surveyresults sr FORCE INDEX (IDX_Contact_Created)
	        ON
	            mib.contactstring_vch = sr.contactstring_vch
	        LEFT OUTER JOIN
	            simpleobjects.batch b
	        ON 
	            b.BatchId_bi = sr.BatchId_bi 
	        WHERE
			   	sr.created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	sr.created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND    
	        	mib.created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           	mib.created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">   
	       	AND
				Keyword_vch NOT LIKE '%INTERVAL%'    
	 		AND 
				Response_vch NOT LIKE '%INTERVAL%' 		
	        AND
	        <cfif trim(inpBatchIdList) EQ "">
	        	b.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	        <cfelse>
	            b.BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        </cfif>     
	    </cfquery>
	    
	    
	    <cfquery name="GetSMSResultsTotals" datasource="#Session.DBSourceREAD#">
	        SELECT
	            COUNT(MasterRXCallDetailid_int) AS TOTALCOUNT
	        FROM 	
				simplexresults.contactresults cd 
	        INNER JOIN 
	        	simpleobjects.batch 
	        ON 
	        	simpleobjects.batch.BatchId_bi = cd.BatchId_bi 
	        AND
	        <cfif trim(inpBatchIdList) EQ "">
	        	simpleobjects.batch.userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	        <cfelse>
	            simpleobjects.batch.BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
	        </cfif> 
	        WHERE
			   rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	SMSResult_int = 100		
	    </cfquery>
	    
		
		<!--- Call Result Data --->
		
		<cfset piechartObj = createObject("component", "session.sire.models.cfc.reports.lib.Piechart").init() />
		<cfset piechartObj.setTitle("Call Results") />
		<cfset piechartObj.setChartType(chartReportingType) />
		 
	     
	     <!---prepare data---> 
	     <cfset pieData = ArrayNew(1)>
	        
	     <cfif GetSMSResults.RecordCount EQ 0>
	    	           
	        <cfset dataItem =[ 'No Data', '1' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	       
	    <cfelse>
	    
	    	<!--- Pie charts need at least one nont null value to display or else will be blank--->
	    	<cfset TotalCount = val(GetSMSResults.NOTASURVEY) + val(GetSMSResults.RUNNING) + val(GetSMSResults.INTERVALHOLD) + val(GetSMSResults.INTERVALRESPONSE) + val(GetSMSResults.COMPLETE) + val(GetSMSResults.CANCELLED) + val(GetSMSResults.EXPIRED) + val(GetSMSResults.TERMINATED)  + val(GetSMSResults.STOPPED) >
	        
	        <cfif TotalCount EQ 0>
	        	<cfset dataItem =[ 'No Data', '1' ]>
	        	<cfset ArrayAppend(pieData, dataItem)>	        
	        </cfif>
	    
	        
	        <cfset dataItem =[ 'NOT A SURVEY', '#GetSMSResults.NOTASURVEY#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'RUNNING', '#GetSMSResults.RUNNING#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL HOLD', '#GetSMSResults.INTERVALHOLD#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL RESPONSE', '#GetSMSResults.INTERVALRESPONSE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'COMPLETE', '#GetSMSResults.COMPLETE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'CANCELLED', '#GetSMSResults.CANCELLED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'EXPIRED', '#GetSMSResults.EXPIRED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'TERMINATED', '#GetSMSResults.TERMINATED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	    
		    <cfset dataItem =[ 'STOPPED', '#GetSMSResults.STOPPED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	    </cfif>
	    
		
		<cfset piechartObj.setData(pieData) />
	    
	    <cfset piechartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "SMS Survey Status");') />
	 	
	    <cfif CustomPie EQ "false">
			<cfreturn piechartObj.drawChart(ShowLegend) />
	    <cfelse>
	        <cfreturn piechartObj.drawAmChartCustomPie(ShowLegend) />
	    </cfif>
	    
	</cffunction>
	    
	
	
	
	<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="Form_filteredresponses" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	
	    <cfset var SCMBbyUser	= '' />
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var LimitItem	= '' />
	    <cfset var getShortCodeRequestByUser	= '' />
	    <cfset var outPutToDisplay	= '' />
	  
		<cfsavecontent variable="outPutToDisplay">
	       	
	        	<!--- Any scripts you place here MUST be quadrant specific --->
	        	<script type="text/javascript">
					$(function(){	
			            
											
						
					});
					
					$('.showToolTip').each(function() {
						 $(this).qtip({
							 content: {
								 text: $(this).next('.tooltiptext')
							 }
						 });
					 });					 
					
				</script>
			
	        <cfoutput>
	        	                         
				<div style="overflow:auto; width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
	                
	                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
	                
	                	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
	                                   
	                      <!--- <div class="inputbox-container" style="margin-top:15px;">
	                       		<label for="inpContactString">Choose a Short Code</label>
	                       </div>--->
	                       <!--- Only display list of short codes assigned to current user--->
	                       <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
					       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
	                       
	                       <h1 class="head-EMS"><div class="DashObjHeaderText">Control Point Selection</div></h1>
	                       
	                       <div style="clear:both"></div>
	                       <BR />
	                       
	                       	<div class="message-block">
					        	<div class="left-input">
	                                <span class="em-lbl">Campaign Filter</span>
	                                <div class="hide-info showToolTip">&nbsp;</div>
	                                <div class="tooltiptext">
	                                    Your must choose a text to filter on against Campaign Names
	                                </div>		
	                                <div class="info-block" style="display:none">content info block</div>
	                            </div>                            
	                            <div class="right-input"><input type="text" value="#inpcustomdata1#" id="inpDesc#inpChartPostion#" name="inpDesc#inpChartPostion#" style="width: 367px; height: 28px;"></div>
	                       </div>
	                       
	                       <div style="clear:both"></div>
	                       <BR />
	                       
	                                         
	                       
	                       
	                        
	                </div>
	                
	                <div style="clear:both"></div>
	                <BR />
	                
	                <div class="message-block" style="width:100%; text-align:center;">
	                    
	                    <div class="inputbox-container" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'chart_filteredresponses', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpDesc#inpChartPostion#').val(), inpcustomdata2: '', inpcustomdata3: '', inpcustomdata4: '' }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'chart_filteredresponses', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpDesc#inpChartPostion#').val(), inpcustomdata2: '', inpcustomdata3: '', inpcustomdata4: ''}" />
	                        <a class="ReportLink no-print" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;"><img style="border:0; float:left;" src="/public/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Chart</span></a>                       
	                    </div>
	                   
	               	</div>
	                                                                                   
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	   
	    <cfreturn outPutToDisplay />
	</cffunction> 
	          
	
	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="chart_filteredresponses" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="ShowLegend" type="string" default="false" required="no"/>
	    <cfargument name="CustomPie" type="string" default="false" required="no"/>
	    <cfargument name="inpcustomdata1" TYPE="any" required="yes" default="1" hint="The Batch Desc to Filter for" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
	
	    <cfset var barChartObj	= '' />
	    <cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var category	= '' />
	    <cfset var data	= '' />
	    <cfset var index	= '' />
	    <cfset var AggregateQuery	= '' />
	    <cfset var AggregateQuerySorted	= '' />      
	    <cfset var TotalCount	= '' />
	    
	    <cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
	       	<cfset arguments.inpcustomdata1 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
	       	<cfset arguments.inpcustomdata2 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
	       	<cfset arguments.inpcustomdata3 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
	       	<cfset arguments.inpcustomdata4 = 0 />
	    </cfif>   
	        
	   	    
	 	<cfset var GetSMSResults = ''>
	    <cfset var piechartObj = ''>
	    <cfset var dataItem = ''>
	    <cfset var pieData	= '' />
	    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <!--- See SMS cfc/csc/contstants.cfm file for value meanings --->
	    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	        SELECT
	        
	        		sum(case when SMSSurveyState_int = 0 then 1 else 0 end) as 'NOTASURVEY',
					sum(case when SMSSurveyState_int = 1 then 1 else 0 end) as 'RUNNING',
					sum(case when SMSSurveyState_int = 2 then 1 else 0 end) as 'INTERVALHOLD',
					sum(case when SMSSurveyState_int = 3 then 1 else 0 end) as 'INTERVALRESPONSE',
					sum(case when SMSSurveyState_int = 4 then 1 else 0 end) as 'COMPLETE',
	                sum(case when SMSSurveyState_int = 5 then 1 else 0 end) as 'CANCELLED',
	                sum(case when SMSSurveyState_int = 6 then 1 else 0 end) as 'EXPIRED',
	                sum(case when SMSSurveyState_int = 7 then 1 else 0 end) as 'TERMINATED',
	                sum(case when SMSSurveyState_int = 8 then 1 else 0 end) as 'STOPPED',
	                0 AS 'NODATAPLACEHOLDER'     
	        FROM 	
				simplexresults.contactresults cd <!---FORCE INDEX (IDX_CDLDT_BATCH_CALLRESULT)---> 
	       
	        WHERE
			   rxcdlstarttime_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
	        AND
	           rxcdlstarttime_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
	        AND
	        	SMSResult_int = 100
	        AND
	    	    BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Active_int = 1 AND Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#inpcustomdata1#%">)
	        
		</cfquery>
	    	
		<!--- Call Result Data --->
		
		<cfset piechartObj = createObject("component", "session.sire.models.cfc.reports.lib.Piechart").init() />
		<cfset piechartObj.setTitle("Call Results") />
		<cfset piechartObj.setChartType(chartReportingType) />
		 
	     
	     <!---prepare data---> 
	     <cfset pieData = ArrayNew(1)>
	        
	     <cfif GetSMSResults.RecordCount EQ 0>
	    	           
	        <cfset dataItem =[ 'No Data', '1' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	       
	    <cfelse>
	    
	    	<!--- Pie charts need at least one nont null value to display or else will be blank--->
	    	<cfset TotalCount = val(GetSMSResults.NODATAPLACEHOLDER) + val(GetSMSResults.NOTASURVEY) + val(GetSMSResults.RUNNING) + val(GetSMSResults.INTERVALHOLD) + val(GetSMSResults.INTERVALRESPONSE) + val(GetSMSResults.COMPLETE) + val(GetSMSResults.CANCELLED) + val(GetSMSResults.EXPIRED) + val(GetSMSResults.TERMINATED)  + val(GetSMSResults.STOPPED) >
	        
	        <cfif TotalCount EQ 0 OR TotalCount EQ "">
	        	<cfset dataItem =[ 'No Data', '1' ]>
	        	<cfset ArrayAppend(pieData, dataItem)>	        
	        </cfif>
	    
	        
	        <cfset dataItem =[ 'NOT A SURVEY', '#GetSMSResults.NOTASURVEY#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'RUNNING', '#GetSMSResults.RUNNING#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL HOLD', '#GetSMSResults.INTERVALHOLD#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'INTERVAL RESPONSE', '#GetSMSResults.INTERVALRESPONSE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'COMPLETE', '#GetSMSResults.COMPLETE#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'CANCELLED', '#GetSMSResults.CANCELLED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'EXPIRED', '#GetSMSResults.EXPIRED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'TERMINATED', '#GetSMSResults.TERMINATED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	    
		    <cfset dataItem =[ 'STOPPED', '#GetSMSResults.STOPPED#' ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	    
	    	<!---<cfset dataItem = [] />
	        <cfset dataItem.STOPPED2 = '#GetSMSResults.STOPPED#' />
	        <cfset dataItem.pulledField = true />
			<cfset ArrayAppend(pieData, dataItem)>	--->
	        
	      <!---  <cfset dataItem =[ 'STOPPED3':'#GetSMSResults.STOPPED#',  'pulledField': true ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
	        
	        <cfset dataItem =[ 'pulledField', true ]>
	        <cfset ArrayAppend(pieData, dataItem)>	
			
			--->
	        
	    </cfif>
	    
		
		<cfset piechartObj.setData(pieData) />
	    
	    <!--- Build javascript objects here - include any custom data parameters at the end --->
		<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_filteredresponses', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#'}" />
	    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_filteredresponses', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#', inpcustomdata2: '#inpcustomdata2#', inpcustomdata3: '#inpcustomdata3#', inpcustomdata4: '#inpcustomdata4#'}" />
	   
	    <cfset piechartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "SMS Survey Status - Filtered on #inpcustomdata1#");') />
	 	<cfset piechartObj.setResetLink( '$("body").data("ResetLink#inpChartPostion#", "RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)");') />
	    
	    <cfif CustomPie EQ "false">
			<cfreturn piechartObj.drawChart(ShowLegend) />
	    <cfelse>
	        <cfreturn piechartObj.drawAmChartCustomPie(ShowLegend) />
	    </cfif>
	         
	</cffunction> 
	
	
	<cffunction name="SMSSessionStates" access="remote" output="false" hint="SMS Summary by Keyword">
	    <cfargument name="inpBatchIdList" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
	    <cfargument name="inpEnd" required="yes" type="string">
	       
	    <cfset var GetSMSResults = StructNew()>
	    <cfset var piechartObj = ''>
	    <cfset var dataItem = ''>
	    <cfset var pieData	= '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />
	    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.RecordCount = -1 />
	        <cfset GetSMSResults.NOTASURVEY = -1 />
	        <cfset GetSMSResults.RUNNING = -1 />
	        <cfset GetSMSResults.INTERVALHOLD = -1 />
	        <cfset GetSMSResults.INTERVALRESPONSE = -1 />
	        <cfset GetSMSResults.COMPLETE = -1 />
	        <cfset GetSMSResults.CANCELLED = -1 />
	        <cfset GetSMSResults.EXPIRED = -1 />
	        <cfset GetSMSResults.TERMINATED = -1 />
	        <cfset GetSMSResults.STOPPED = -1 />
	        
	    <cfelse>    
	         
	       <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	            SELECT 
	               COUNT(*) as TotalCount,
	               sr.DescSmall_vch                               
	            FROM 	
	                simplequeue.sessionire cd 
	            JOIN
	                simplexresults.smssurveystates sr
	                ON sr.SMSSurveyState_int = cd.SessionState_int
	             WHERE
	                Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	            AND
	                Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
	            AND 
	                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
	            AND
	                cd.SessionState_int <> 0    
	            GROUP BY
	                sr.DescSmall_vch    
	                
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                        UNION
	                       
	                       	SELECT 
	                           COUNT(*) as TotalCount,
	                           sr.DescSmall_vch                               
	                        FROM 	
	                            simplequeue.sessionire cd 
	                        JOIN
	                            simplexresults.smssurveystates sr
	                            ON sr.SMSSurveyState_int = cd.SessionState_int
	                         WHERE
	                            Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">  
	                        AND
	                            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">    
	                        AND 
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#">  
	                        AND
	                            cd.SessionState_int <> 0    
	                        GROUP BY
	                            sr.DescSmall_vch                                                  
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>     
	             
	        </cfquery>
	        
	    </cfif>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TotalCount) AS TotalCount,
	                DescSmall_vch    
	            FROM	
	                GetSMSResults
	            GROUP BY
	            	DescSmall_vch          
	        </cfquery>
	    
	    <cfelse>
	    	   
	        <cfset UnionSumQuery.RecordCount = 0 />
	        <cfset UnionSumQuery.NOTASURVEY = 0 />
	        <cfset UnionSumQuery.RUNNING = 0 />
	        <cfset UnionSumQuery.INTERVALHOLD = 0 />
	        <cfset UnionSumQuery.INTERVALRESPONSE = 0 />
	        <cfset UnionSumQuery.COMPLETE = 0 />
	        <cfset UnionSumQuery.CANCELLED = 0 />
	        <cfset UnionSumQuery.EXPIRED = 0 />
	        <cfset UnionSumQuery.TERMINATED = 0 />
	        <cfset UnionSumQuery.STOPPED = 0 />        
	        
	    </cfif>   
	    
	          
	    <cfset piechartObj = createObject("component", "session.sire.models.cfc.reports.lib.Piechart").init() />
	    <cfset piechartObj.setTitle("Call Results") />
	    <cfset piechartObj.setChartType(chartReportingType) />
	     
	    <!---prepare data---> 
	    <cfset pieData = ArrayNew(1)>
	    <cfloop query="UnionSumQuery">
	        <cfset dataItem =[
	            '#DescSmall_vch#', '#TotalCount#'
	        ]>
	        <cfset ArrayAppend(pieData, dataItem)>
	    </cfloop>	
	    <cfset piechartObj.setData(pieData) />
	      
	    <cfreturn piechartObj.drawChart("true") />
	</cffunction>    
	
	
	<!--- This one is not functional yet.... dont use until you fix it ... --->    
	<!--- Average call time for each result type  --->
	<cffunction name="ICByKeyword" access="package" output="true" returntype="any" hint="Active Interactive Campaign counts by keyword">
	    <cfargument name="inpBatchIdList" required="yes" type="any">
	    <cfargument name="inpStart" required="yes" type="string">
	    <cfargument name="inpEnd" required="yes" type="string">
	    
	    <cfset var GetSMSResults = StructNew()>
	    <cfset var barChartObj = ''>
	    <cfset var dataItem = ''>
	    <cfset var pieData	= '' />
	    <cfset var ListIndexCounter = 1 />
	    <cfset var CurrListIndex = "" />
	    <cfset var UnionSumQuery = StructNew() />
	    <cfset var category	= '' />
		<cfset var data	= '' />
		<cfset var index	= '' />
		<cfset var AggregateQuery	= '' />
	    
	    <cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	    
	    <!---
		
		   <cfquery name="AggregateQuery" datasource="#Session.DBSourceEBM#">
	        SELECT 
	            keyword_vch,
	            Count(simplexresults.contactresults.BatchId_bi) AS TotalCount
	        FROM
	            simplexresults.contactresults JOIN
	            sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
	        WHERE
	            SMSSurveyState_int IN (1,2,3)
	        AND
	            SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
	        AND 
	        	simplexresults.contactresults.BatchId_bi IN (SELECT Batchid_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> )
	        GROUP BY
	            keyword_vch
	    </cfquery>   
		
		--->
	    
	     <cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.RecordCount = -1 />
	        
	    <cfelse>    
	         
	    	<cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	                
	            SELECT 
	                keyword_vch,
	                Count(simplexresults.contactresults.BatchId_bi) AS TotalCount
	            FROM
	                simplexresults.contactresults JOIN
	                sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
	            WHERE
	                SMSSurveyState_int IN (1,2,3)
	            AND
	                SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
	            AND 
	                simplexresults.contactresults.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
	            GROUP BY
	                keyword_vch
	                
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                        UNION
	                       
	                        SELECT 
	                            keyword_vch,
	                            Count(simplexresults.contactresults.BatchId_bi) AS TotalCount
	                        FROM
	                            simplexresults.contactresults JOIN
	                            sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
	                        WHERE
	                            SMSSurveyState_int IN (1,2,3)
	                        AND
	                            SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
	                        AND 
	                            simplexresults.contactresults.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#">
	                        GROUP BY
	                            keyword_vch
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>     
	             
	        </cfquery>
	        
	    </cfif>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="UnionSumQuery" dbtype="query">
	            SELECT 	
	                SUM(TotalCount) AS TotalCount,
	                keyword_vch    
	            FROM	
	                GetSMSResults
	            GROUP BY
	            	DescSmall_vch          
	        </cfquery>
	    
	    <cfelse>
	    	   
	        <cfset UnionSumQuery.TotalCount = 0 />           
	        
	    </cfif>   
	          
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <cfset barChartObj.setTitle("Total Active") />
	    <cfset barChartObj.setYAxisTitle("Count Active") />
	    <cfset barChartObj.setXAxisTitle("Keyword") />
	    <!---<!---<cfset barChartObj.setChartType(chartReportingType) /> ---> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	        
	    <cfset index=0>	
	    <cfloop query="UnionSumQuery">
	        <cfset index= index+1>
	         <cfset ArrayAppend(category, keyword_vch)/>
	         <cfset ArrayAppend(data, TotalCount)/>   
	    </cfloop>   
	
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	</cffunction>
	
	<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="Form_chartMO" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
	
	    <cfset var SCMBbyUser	= '' />
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var LimitItem	= '' />
	    <cfset var getShortCodeRequestByUser	= '' />
	    <cfset var outPutToDisplay	= '' />
	  
		<cfsavecontent variable="outPutToDisplay">
	       	
	        	<!--- Any scripts you place here MUST be quadrant specific --->
	        	<script type="text/javascript">
					$(function(){	
			            
						$('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
							style:'popup',
							width: 370 //, format: addressFormatting
						});
						
						$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
							style:'popup',
							width: 370 //, format: addressFormatting
						});
						
						$('#graphtype<cfoutput>#inpChartPostion#</cfoutput>').selectmenu({
							style:'popup',
							width: 370 //, format: addressFormatting
						});
						
						
						<cfif inpcustomdata4 GT 0>
							ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(null);
						</cfif>
						
						<cfif inpcustomdata3 GT 0>
							ReloadControlPoints<cfoutput>#inpChartPostion#</cfoutput>(null);
						</cfif>
						
					});
									
					 
					 $('.showToolTip').each(function() {
						 $(this).qtip({
							 content: {
								 text: $(this).next('.tooltiptext')
							 },
							  style: {
								classes: 'qtip-bootstrap'
							}
						 });
					 });	
			 
					 
					 function ReloadKeyWords<cfoutput>#inpChartPostion#</cfoutput>(inpObject)
					 {
							$.ajax({
		
								type: "POST", 
								url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordsForShortCode&returnformat=json&queryformat=column',   
								dataType: 'json',
								async:false,
								data:  
								{ 
									inpShortCode: $('#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>').val(),
									IsDefault: 0,
									IsSurvey: 1
								},					  
								success:function(res)
								{
									if(parseInt(res.RXRESULTCODE) == 1)
									{
										
										$('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput> option[value!="0"]').remove();
										
										var CurrKeyWordList = $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>');
										
										<!--- Convert ajax request result array to jquery object and loop over it --->
										$.each($(res.KeywordData), function(i, item) {
											 <!---// console.log(item.KEYWORDID);--->
											 
											 if('<cfoutput>#inpcustomdata3#</cfoutput>' == '' + item.BATCHID)
											 	CurrKeyWordList.append('<option value="' + item.BATCHID + '" selected>' + item.KEYWORD + '</option>')	
											 else
											 	CurrKeyWordList.append('<option value="' + item.BATCHID + '">' + item.KEYWORD + '</option>')			
											 
										});
										
										CurrKeyWordList.selectmenu({
											style:'popup',
											width: 370 //, format: addressFormatting
										});			
									
											<!--- Disable and reenable select box with new data --->
											<!---   $('#inpListKeywords<cfoutput>#inpChartPostion#</cfoutput>'). --->
									}
								}
									
						}); 
					 }
				</script>
			
	        <cfoutput>
	        	                         
				<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
	                
	                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
	                	
	                	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
	                                   
	                      <!--- <div class="inputbox-container" style="margin-top:15px;">
	                       		<label for="inpContactString">Choose a Short Code</label>
	                       </div>--->
	                       <!--- Only display list of short codes assigned to current user--->
	                       <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
					       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
	                       
	                       <h1 class="head-EMS"><div class="DashObjHeaderText">Control Point Selection</div></h1>
	                       
	                       <div style="clear:both"></div>
	                       <BR />
	                        <input type="hidden" id="CPText#inpChartPostion#" name="CPText#inpChartPostion#" value="NO CP">
	                        	
	                       	<div class="message-block">
					        	<div class="left-input">
	                                <span class="em-lbl">Short Code</span>
	                                <div class="hide-info showToolTip">&nbsp;</div>
	                                <div class="tooltiptext">
	                                    Your must choose which short code to use from this accounts multiple short codes.
	                                </div>		
	                                <div class="info-block" style="display:none">content info block</div>
	                            </div>
	                        
	                       		<div class="right-input">
	                      
	                               <select id="inpListShortCode#inpChartPostion#" onchange="ReloadKeyWords#inpChartPostion#(this);">
	                               
	                               		<option value="0">Select a Short Code</option>
	                                   <cfloop query="SCMBbyUser">
	                                                                              
	                                       <cfif ShortCode_vch EQ inpcustomdata4 >
	    	                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
	                                       <cfelse>
		                                       <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
	                                       </cfif>
	                                       
	                                       
	                                   </cfloop>
	                           
	                             	</select>
	                       
	                       		</div>
	                       
	                       </div>
	                       
	                       <div style="clear:both"></div>
	                       <BR />
	                       
	                       <div class="message-block">
					        	<div class="left-input">
	                                <span class="em-lbl">Keyword</span>
	                                <div class="hide-info showToolTip">&nbsp;</div>
	                                <div class="tooltiptext">
	                                    Your must choose which keyword under this short code to look at.
	                                </div>		
	                                <div class="info-block" style="display:none">content info block</div>
	                            </div>
	                        
	                       		<div class="right-input">
	                      
	                               <select id="inpListKeywords#inpChartPostion#">
	                               
	                                  
	                                       <option value="0">Select a Keyword</option>
	                                   
	                           
	                             	</select>
	                       
	                       		</div>
	                       
	                       </div>
	                       
	                       <div style="clear:both"></div>
	                       <BR />
	                       
	                       <div class="message-block">
					        	<div class="left-input">
	                                <span class="em-lbl">Time</span>
	                                <div class="hide-info showToolTip">&nbsp;</div>
	                                <div class="tooltiptext">
	                                    Your must choose which Control Point under the selected Short Code and Keyword to look at.
	                                </div>		
	                                <div class="info-block" style="display:none">content info block</div>
	                            </div>
	                        
	                       		<div class="right-input">
	                      
	                               <select id="graphtype#inpChartPostion#" >
	                               
	                                  
	                                       <option value="1">By Hour</option>
	                                       <option value="2">By Day</option>
	                                       <option value="3">By Week</option>
	                                       <option value="4">By Month</option>
	                                   
	                           
	                             	</select>
	                       
	                       		</div>
	                       
	                       </div>
	                        
	                </div>
	                
	                <div style="clear:both"></div>
	                <BR />
	                
	                <div class="message-block" style="width:100%; text-align:center;">
	                    <div class="inputbox-container" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'chart_MOChart', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##graphtype#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'chart_MOChart', inpReportType: 'CHART', inpChartPostion: #inpChartPostion#, inpcustomdata2: $('##graphtype#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Chart!" style="line-height:18px;"><img style="border:0; float:left;" src="/public/images/icons/baricon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Chart</span></a>                       
	                    </div>
	                    
	                  <!--- <div class="inputbox-container" style="margin-bottom:5px; margin-left:200px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_cpresponsescount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_cpresponsescount', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Count!" style="line-height:18px;"><img style="border:0; float:left;" src="/public/images/icons/numbericon.png" alt="" width="32" height="32"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Count</span></a>                       
	                   </div>
	                                                           
	                    <div class="inputbox-container" style="margin-bottom:5px; margin-left:400px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val() }" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'display_cpresponses', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata1: $('##inpListControlPoints#inpChartPostion#').val(), inpcustomdata2: $('##inpLimit#inpChartPostion#').val(), inpcustomdata3: $('##inpListKeywords#inpChartPostion#').val(), inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val(), inpcustomdata5: $('##CPText#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><img style="border:0; float:left;" src="/public/images/icons/tableicon_32x18web.png" alt="" width="32" height="18"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
	                   </div>--->
	               	</div>                                      
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	 
	    <cfreturn outPutToDisplay />
	</cffunction>
	
	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="chart_MOChart" access="remote" output="false" returntype="any" hint="Chart of MO Count">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="yes" type="string">
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="any" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
	
		<cfset var barChartObj	= '' />
	    <cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var category	= '' />
	    <cfset var data	= '' />
	    <cfset var index	= '' />
	    <cfset var AggregateQuery	= '' />
	    <cfset var AggregateQuerySorted	= '' />     
	   	<cfset var GetSMSResults	= '' />
		<cfset var ListIndexCounter	= '' />
		<cfset var UnionSumQuery	= '' />
		<cfset var objResponseSorter	= '' />
		<cfset var CurrListIndex	= '' />
		<cfset var AggregateQuerySortedindex	= '' /> 
	    
	    <cfif arguments.inpcustomdata1 EQ "undefined" OR arguments.inpcustomdata1 EQ "">
	       	<cfset arguments.inpcustomdata1 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata2 EQ "undefined" OR arguments.inpcustomdata2 EQ "">
	       	<cfset arguments.inpcustomdata2 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata3 EQ "undefined" OR arguments.inpcustomdata3 EQ "">
	       	<cfset arguments.inpcustomdata3 = 0 />
	    </cfif>
	    
	    <cfif arguments.inpcustomdata4 EQ "undefined" OR arguments.inpcustomdata4 EQ "">
	       	<cfset arguments.inpcustomdata4 = 0 />
	    </cfif>   
	        
	   	 
	 	<cfif ListLen(inpBatchIdList) EQ 0>
	    
	    	<cfset GetSMSResults.RecordCount = -1 />
	        
	    <cfelse>    
	              
	    	<cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
	                
	            SELECT 
	                keyword_vch,
	                Count(simplexresults.contactresults.BatchId_bi) AS TotalCount,
	                date_format(simplexresults.contactresults.Created_dt, "m-d-Y") AS date
	            FROM
	                simplexresults.contactresults JOIN
	                sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
	            WHERE
	                SMSSurveyState_int IN (1,2,3)
	            AND
	                SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
	            AND 
	                simplexresults.contactresults.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ListGetAt(inpBatchIdList,1)#"> 
	            GROUP BY
	                keyword_vch,date
	                
	            <!--- Unfortunate feature of mySQL - this is faster than IN clause for some reason - check future updates for better index untilization --->	
	 			<cfif ListLen(inpBatchIdList) GT 1 AND ListLen(inpBatchIdList) LT 100 >
	            
	                <cfset ListIndexCounter = 1 />	
	                
	                <cfloop list="#inpBatchIdList#" index="CurrListIndex" >
	            
	            		<!--- Single case already ran above --->
	                    <cfif ListIndexCounter GT 1>
	                        UNION
	                       
	                        SELECT 
	                            keyword_vch,
	                            Count(simplexresults.contactresults.BatchId_bi) AS TotalCount,
	                            date_format(simplexresults.contactresults.Created_dt, "m-d-Y") AS date
	                        FROM
	                            simplexresults.contactresults JOIN
	                            sms.keyword on simplexresults.contactresults.Batchid_bi = sms.keyword.batchid_bi
	                        WHERE
	                            SMSSurveyState_int IN (1,2,3)
	                        AND
	                            SMSCSC_vch IN  (SELECT DISTINCT ShortCode_vch FROM sms.shortcoderequest JOIN sms.shortcode ON sms.shortcoderequest.ShortCodeId_int = sms.shortcoderequest.ShortCodeId_int WHERE OwnerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> AND Status_int = 1 ) 
	                        AND 
	                            simplexresults.contactresults.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrListIndex#">
	                        GROUP BY
	                            keyword_vch,date
	                    </cfif>        
	            
	                    <cfset ListIndexCounter = ListIndexCounter + 1 />	                    
	                    
	                </cfloop>
	            
	            </cfif>     
	             
	        </cfquery>
	        
	    </cfif>
	    
	    <cfif GetSMSResults.RECORDCOUNT GT 0>
	    
	        <cfquery name="GetSMSResults" dbtype="query">
	            SELECT 	
	                SUM(TotalCount) AS TotalCount,
	                keyword_vch,
	                date   
	            FROM	
	                GetSMSResults
	            GROUP BY
	            	DescSmall_vch          
	        </cfquery>
	    
	    <cfelse>
	    	   
	        <cfset UnionSumQuery.TotalCount = 0 />           
	        
	    </cfif>        
	      
	    <cfset barChartObj = createObject("component", "session.sire.models.cfc.reports.lib.Barchart").init() />
	    <cfset barChartObj.setTitle("") />
	    <cfset barChartObj.setYAxisTitle("Count Response") />
	    <cfset barChartObj.setXAxisTitle("Response") />
	    <!---<cfset barChartObj.setChartType(chartReportingType) /> --->
	    <cfset barChartObj.setAverageAvailable(false) />
	    
	   
	   <!--- Build javascript objects here - include any custom data parameters at the end --->
		<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_chartMO', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: #inpcustomdata2#, inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	    <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_chartMO', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: #inpcustomdata1#, inpcustomdata2: #inpcustomdata2#, inpcustomdata3: #inpcustomdata3#, inpcustomdata4: #inpcustomdata4#, inpcustomdata5: '#urlEncodedFormat(inpcustomdata5)#'}" />
	   
		<!--- store link data in body variable for later retrival  ---> 
		<cfset barChartObj.setResetLink( '$("body").data("ResetLink#inpChartPostion#", "RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)");') />
	    <cfset barChartObj.setDashboardHeader( '$("body").data("DashBoardHeaer#inpChartPostion#", "Response Counts For Control Point CP#inpcustomdata1#");') />
	 	
	    <cfset category = ArrayNew(1)>
	    <cfset data = ArrayNew(1)>
	        
	    <cfset index=0>	
	   
	    <cfif GetSMSResults.RecordCount EQ 0>
	    
		    <cfset barChartObj.setXAxisTitle("No Response Data in the Selected Date Range") />
		    <cfset ArrayAppend(category, "No Data")/>
	        <cfset ArrayAppend(data, 0)/>  
	       
	    <cfelse>
	
	        <!--- http://www.bennadel.com/blog/387-Sorting-ColdFusion-Arrays-With-Sortable-Interfaces.htm --->
	        <cfset objResponseSorter = CreateObject("component", "sortableresponses").Init() />
	         
	        <cfset var arrAggregateQuerySorted = objResponseSorter.QueryToArray(GetSMSResults) /> 
	        
	        <!--- Sort the array. --->
	        <cfset arrAggregateQuerySorted = objResponseSorter.SortArray(
	            arrAggregateQuerySorted,
	            "LT"
	            ) />
	            
	         
	                   
	    	<cfloop array="#arrAggregateQuerySorted#" index="AggregateQuerySortedindex">
	            <cfset index= index+1>
	                        
	            <!--- Friggin mySQL TRIM does not remove newlines. Newlines will break chart --->
	            <cfset ArrayAppend(category, Left(Replace( TRIM( REReplace(AggregateQuerySortedindex.Response_vch,"#chr(13)#|#chr(9)#|\n|\r","","ALL")  ), "'", "`", "ALL"), 10))/>
	            <cfset ArrayAppend(data, AggregateQuerySortedindex.TotalCount)/>   
	        </cfloop>  
	     
		</cfif>
		
	    <cfset barChartObj.setCategory(category)>
	    <cfset barChartObj.setData(data)>
	    
	    <cfreturn barChartObj.drawChart()>
	</cffunction>
	
	<cffunction name="ConsoleSummaryIRECounts" access="remote" output="false" returntype="any" hint="Get SMS Activity Counts for current user for last month">
		
	   	<cfset var dataout = {} />    
		<cfset var GetSMSResults = '' />
		
		<!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.MTCOUNT = 0 />
		<cfset dataout.MOCOUNT = 0 />
		<cfset dataout.APICOUNT = 0 />
		
		<cftry>
		
		    <cfquery name="GetSMSResults" datasource="#Session.DBSourceREAD#">
		       SELECT
					COUNT(*) AS TotalCount,
					IreType_int
				FROM 
					simplexresults.ireresults
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">  
				AND
					Created_dt > DATE_ADD(NOW(), INTERVAL -30 DAY)
				GROUP BY
					IreType_int
		    </cfquery>
	        
	        <cfloop query="GetSMSResults"> 
		        
		        <cfif GetSMSResults.IreType_int EQ IREMESSAGETYPE_MT>
			        <cfset dataout.MTCOUNT = GetSMSResults.TotalCount /> 
		        </cfif>
		        
		        <cfif GetSMSResults.IreType_int EQ IREMESSAGETYPE_MO>
			        <cfset dataout.MOCOUNT = GetSMSResults.TotalCount /> 
		        </cfif>
		        
		        <cfif GetSMSResults.IreType_int EQ IREMESSAGETYPE_API_TRIGGERED>
			        <cfset dataout.APICOUNT = GetSMSResults.TotalCount /> 
		        </cfif>
		        
	        </cfloop> 
	        
	        <!--- Everything processed OK --->
			<cfset dataout.RXRESULTCODE = "1" />
	
		<cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
	    
	</cffunction>
	
			
	
</cfcomponent>
	
