<cfcomponent output="no">
        
	<cfsetting showdebugoutput="no" />

	<!--- SMS Constants--->
    <cfinclude template="../../../../cfc/csc/constants.cfm">
    
    <cfparam name="Session.DBSourceEBM" default="bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<!--- Nameing convention for "FORM" reports Form_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="Form_summary" access="remote" output="false" returntype="any" hint="List Short codes for wich to retrieve list of MO keywords sent ">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="no" type="string" default="1">
	    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	    <cfargument name="inpcustomdata5" TYPE="any" required="no" default="0" hint="Control Point Text" />
	
	    <cfset var SCMBbyUser	= '' />
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var LimitItem	= '' />
	    <cfset var getShortCodeRequestByUser	= '' />
	    <cfset var outPutToDisplay	= '' />
	    <cfset var DefaultshortCode	= '' />
	  
	    <cfif LEN(trim(arguments.inpcustomdata4)) EQ 0 >
	    
			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="DefaultshortCode"></cfinvoke>						
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="DefaultshortCode"></cfinvoke>
	  
			<cfset arguments.inpcustomdata4 = DefaultshortCode.SHORTCODE/>
	  	</cfif>
	  	
		<cfsavecontent variable="outPutToDisplay">
	       	
	        	<!--- Any scripts you place here MUST be quadrant specific --->
	        	<script type="text/javascript">
					$(function(){	
								            	        			
						$("#inpListShortCode<cfoutput>#inpChartPostion#</cfoutput>").select2( { theme: "bootstrap"} );
										
					});
						
					 
					
				</script>
			
	        <cfoutput>
	        	                         
				<div style="width:100%; height:100%; position:relative; vertical-align:central; text-align:center;" class="FormContainerX" id="FormContainerX_#inpChartPostion#">
	                
	                <div class="content-EMS" style="border:none; width:100%; height:200px; vertical-align:central; text-align:center;">
	                	
	                	<!---<div style="width:168px; margin: 0 auto; text-align:left; overflow:auto;">--->
	                                   
	                      <!--- <div class="inputbox-container" style="margin-top:15px;">
	                       		<label for="inpContactString">Choose a Short Code</label>
	                       </div>--->
	                       <!--- Only display list of short codes assigned to current user--->
	                       <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
					       <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />
	                       
	                       <div class="head-EMS"><div class="DashObjHeaderText">Summary Report</div></div>
	                       
	                       <div class="row">
	                                
	                            <div class="col-md-12 col-sm-12 col-xs-12">
	                                   
	                               <label for="inpListShortCode#inpChartPostion#">Short Code</label> 
	                               <select id="inpListShortCode#inpChartPostion#" class="ebmReport">
	                               
	                                    <option value="0">Select a Short Code</option>
	                                    <cfloop query="SCMBbyUser">
	                                                                              
	                                       <cfif ShortCode_vch EQ inpcustomdata4 >
	                                           <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
	                                       <cfelse>
	                                           <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
	                                       </cfif>
	                                       
	                                       
	                                   </cfloop>
	                           
	                                </select>
	                           
	                           	</div>
					       
	                       </div>
	                                               
	                </div>
	                
	                <div style="clear:both"></div>
	                <BR />
	                
	                <div class="message-block" style="width:100%; text-align:center;">                                                           
	                   <div class="inputbox-container" style="margin-bottom:5px;">
	                       <!--- Build javascript objects here - include any custom data parameters at the end --->
	                        <cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'stop.display_stoptable', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
	                        <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'stop.display_stoptable', inpReportType: 'TABLE', inpChartPostion: #inpChartPostion#, inpcustomdata4: $('##inpListShortCode#inpChartPostion#').val()}" />
	                        <a class="ReportLink" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)" title="Show Table!" style="line-height:18px;"><span style=" margin:2px 0 0 3px; float:left;" class="ReportLink">Show Table</span></a>                       
	                   </div>
	               	</div>                                      
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	   
	    <cfreturn outPutToDisplay />
	</cffunction>
	          
	
	<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
	<cffunction name="display_summary" access="remote" output="false" returntype="any" hint="Chart of Control Point response counts for given batch id and control point in inpcustomdata1">
		<cfargument name="inpBatchIdList" required="yes" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpChartPostion" required="no" type="string" default="1">
	    <cfargument name="inpcustomdata1" TYPE="string" required="no" default="1" hint="The Control Point" />
	    <cfargument name="inpcustomdata2" TYPE="string" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="string" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	 	<cfargument name="inpcustomdata5" TYPE="string" required="no" default="0" hint="Control Point Text" />
	   
		<cfset var inpDataURP	= '' />
	    <cfset var inpDataGC	= '' />
	    <cfset var outPutToDisplay	= '' />
	    
		<!--- Escape douple quotes so can be passed as javascript string in custom variable --->
		<cfset arguments.inpcustomdata1 = Replace(arguments.inpcustomdata1,'"', "&quot;", "ALL") />
	    	
		<cfsavecontent variable="outPutToDisplay">
	       	<cfoutput>
	
				<!--- Build javascript objects here - include any custom data parameters at the end --->
	        	<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'stop.Form_stop', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata4: #inpcustomdata4#}" />
	            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'stop.Form_stop', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata4: #inpcustomdata4#}" />
	                       
				<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
				<div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
	                <div class="head-EMS"><div class="DashObjHeaderText" rel1="#SESSION.userid#">Summary #inpcustomdata4#</div></div>
	        
	        		<!--- Pass in extra data to set ddefault sorting you would like --->
	        	   	<input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 1, "desc" ]]' >
	            
	                <!--- Keep table auto-scroll in managed area--->
	                <div class="EBMDataTableWrapper">
	                
						<!--- ID must be ReportDataTable for all reports - this is used by CSS and jQuery to locate stuu--->     
	                    <table id="ReportDataTable" class="table table-striped table-bordered table-hover" style="clear:both; width:100%;" border="0">
	                        <thead>
	                            <tr>
	                                <th width="20%" style="width:20%;">Campaign</th>
	                                <th width="30%" style="width:30%;">Direction</th>
	                                <th width="30%" style="width:30%;">Total</th>                           
	                            </tr>
	                        </thead>
	                        
	                        <tbody>
	                            <tr>
	                                <td colspan="5" class="datatables_empty">Loading data from server</td> 
	                            </tr>        
	                        </tbody>                              
	                    </table>    
	                                
	                </div>
	                
	                <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
	                	<!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
	                    <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
	                   <div class="DownloadLink excelIcon" rel1="summary.summarytable" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#" inpcustomdata5="#inpcustomdata5#"></div>
<!---
	                   <div class="DownloadLink wordIcon" rel1="summary.summarytable" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#" inpcustomdata5="#inpcustomdata5#"></div>
	                   <div class="DownloadLink pdfIcon" rel1="summary.summarytable" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#" inpcustomdata2="#inpcustomdata2#" inpcustomdata3="#inpcustomdata3#" inpcustomdata4="#inpcustomdata4#" inpcustomdata5="#inpcustomdata5#"></div>
	          
--->         <div class="ReportLink" style="float:right; margin-top:10px; margin-right:20px; text-align:bottom;" onclick="RedrawChartWithNewData(#inpDataURP#, #inpDataGC#, #inpChartPostion#)">Change</div>
	                </div>
	            </div>
	                       
			</cfoutput>                 
	 	</cfsavecontent>
	
	    <cfreturn outPutToDisplay />
	</cffunction>
	
	
	<cffunction name="summarytable" access="remote" output="false" returntype="any" hint="list all opt outs for a given short code">
		<cfargument name="inpBatchIdList" required="no" type="any">
		<cfargument name="inpStart" required="yes" type="string" >
		<cfargument name="inpEnd" required="yes" type="string">
	    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="0" hint="The User Id" />
	    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
		<cfargument name="inpcustomdata3" TYPE="any" required="no" default="0" hint="The Batch Id" />
	    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
	    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
	    
	    <!--- Paging --->
	    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
	    <cfargument name="iDisplayStart" required="no"  default="0">
	    <cfargument name="iDisplayLength" required="no"  default="5">
	    
		<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
			  word by word on any field. It's possible to do here, but concerned about efficiency
			  on very large tables, and MySQL's regex functionality is very limited
	    --->
	    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
	    <cfargument name="sSearch" required="no"  default="">
	    <cfargument name="iSortingCols" required="no"  default="0">
	  
	        <cfset var GetNumbersCount = '' />
	        <cfset var qFiltered = '' />
	        
	        <cfset var listColumns	= '' />
			<cfset var tempItem	= '' />
			<cfset var thisS	= '' />
			<cfset var LoopIndex	= '' />
	        
	        <cfset var DataOut = {}>
			<cfset DataOut.DATA = ArrayNew(1)>
	        
	        <!---ListEMSData is key of DataTable control--->
			<cfset DataOut["aaData"] = ArrayNew(1)>
	        
	        <cfset DataOut.RXRESULTCODE = 1 />
			<cfset DataOut.TYPE = "" />
	        <cfset DataOut.MESSAGE = "" />
	        <cfset DataOut.ERRMESSAGE = "" />
	        
	        <cfset DataOut["iTotalRecords"] = 0>
	        <cfset DataOut["iTotalDisplayRecords"] = 0>
	                 
	     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
		    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
	        
	        
	        <cftry>
	             
	            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
	            <cfset listColumns = "BatchId_bi,IREType_int,TOTALCOUNT" />
	                  
	            <!---Get total  EMS for paginate --->
	            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
	             	SELECT 
						 COUNT(*) AS TOTALCOUNT
					FROM 
						simplexresults.ireresults
					WHERE
						<cfif inpcustomdata1 GT 0>
					        userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
					    <cfelse>
					    	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.userid#">
					    </cfif>  
					AND
						(IREType_int= 1 OR IREType_int= 2)
					
					<cfif TRIM(inpBatchIdList) NEQ "" AND TRIM(inpBatchIdList) NEQ "0000" AND TRIM(inpBatchIdList) NEQ "EXPORTALL">
					AND	
			        	BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
			        </cfif>	
			        
					AND
						Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
					AND
						Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
					GROUP BY
					    BatchId_bi, 
					    IREType_int
					ORDER BY 
						BatchId_bi ASC, IREType_int ASC
				</cfquery>
	            
	            <cfif GetNumbersCount.TOTALCOUNT GT 0>
	                <cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
	                <cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	            </cfif>
	                                    
	            <!--- Get data to display --->         
	            <!--- Data set after filtering --->
	            <cfquery name="qFiltered" datasource="#Session.DBSourceREAD#"> 	            
		            
		            SELECT 
						 COUNT(*) AS TOTALCOUNT, 
					     BatchId_bi, 
					     IREType_int
					FROM 
						simplexresults.ireresults
					WHERE
						<cfif inpcustomdata1 GT 0>
					        userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpcustomdata1#">
					    <cfelse>
					    	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.userid#">
					    </cfif>  
					AND
						(IREType_int= 1 OR IREType_int= 2)
					
					<cfif TRIM(inpBatchIdList) NEQ "" AND TRIM(inpBatchIdList) NEQ "0000" AND TRIM(inpBatchIdList) NEQ "EXPORTALL">
					AND	
			        	b.BatchId_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />) 
			        </cfif>	
			        
					AND
						Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
					AND
						Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
					GROUP BY
					    BatchId_bi, 
					    IREType_int
					ORDER BY 
						BatchId_bi ASC, IREType_int ASC		            
		            <!--- 
			            By limiting results to paging here instead of once query returns, we can:
			            limit how much data is transfered from DB
			            the amount of memory this query results takes up
			        	Greatly speed up processing     
			        --->
			     		<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength>	
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	                    <cfelse>
	                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
	                    </cfif>
	                                   
	            </cfquery>
	           
	            <cfif OutQueryResults EQ "1">
		            
		            <cfset DataOut.RXRESULTCODE = 1 />
					<cfset DataOut.QUERYRES = qFiltered />
					<cfset DataOut.TYPE = "" />
			        <cfset DataOut.MESSAGE = "" />
			        <cfset DataOut.ERRMESSAGE = "" />
		            
	                <cfreturn DataOut />  
	            </cfif>
	                   
	            <!---
	                Output
	             --->
	             
	            <cfloop query="qFiltered">	
	                                    
	                <cfset tempItem = [				
	                    '#qFiltered.BatchId_bi#',
	                    '#qFiltered.IREType_int#',
	                    '#qFiltered.TOTALCOUNT#'
	                ]>		
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>
	            
	             <!--- Append min to 5 - add blank rows --->
	            <cfloop from="#qFiltered.RecordCount#" to="4" step="1" index="LoopIndex">	
	                                
	                <cfset tempItem = [			
	                    ' ',
	                    ' ',
	                    ' '		
	                ]>		
	                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
	            </cfloop>
	                    
	  		<cfcatch type="Any" >
				<cfset DataOut.RXRESULTCODE = -1 />
			    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
			    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset DataOut["iTotalRecords"] = 0>
				<cfset DataOut["iTotalDisplayRecords"] = 0>
				<cfset DataOut["aaData"] = ArrayNew(1)>
	        </cfcatch>
	        </cftry>
	        <cfreturn serializeJSON(DataOut)>
	        
	</cffunction>

</cfcomponent>