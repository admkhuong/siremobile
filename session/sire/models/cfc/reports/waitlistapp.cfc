<cfcomponent>
	<cffunction name="GetAnalyticData" access="remote" hint="Get data for statistics">
		<cfargument name="inpDateStart" type="string" required="yes">
		<cfargument name="inpDateEnd" type="string" required="yes">
		<cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">

		<cfset var dataout = {} />
		<cfset var GetAnalyticData = {} />
		<cfset var curUserID = arguments.inpUserID />
		<cfset var defineItem = "" />
		<cfset var sqlDefinition = [
			{"TYPE" =  "TIMELINE", "VALUE" = "FROM_UNIXTIME(UNIX_TIMESTAMP(waitlistapplogs.CreatedAt_dt), ""%Y-%m-%d"")"},
			{"TYPE" =  "WEEK", "VALUE" = "SUBSTR(DAYNAME(waitlistapplogs.CreatedAt_dt), 1, 3)"},
			{"TYPE" =  "HOUR", "VALUE" = "FROM_UNIXTIME(UNIX_TIMESTAMP(waitlistapplogs.CreatedAt_dt), ""%h:00"")"},
		] />

		<cfset dataout["DATA"] = {} />

		<cfloop array="#sqlDefinition#" index="defineItem">
			<cfset dataout["DATA"]["#defineItem.TYPE#"] = ArrayNew() />
			<cfquery name="GetAnalyticData" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(IF(waitlistapplogs.Status_int = 1, 1, NULL)) AS WAITLISTED,
					COUNT(IF(waitlistapplogs.Status_int = 2, 1, NULL)) AS SERVED,
					COUNT(IF(waitlistapplogs.Status_int = 3, 1, NULL)) AS NOSHOW,
					#REReplace(defineItem.VALUE,"''","'","ALL")# AS CreatedAt
				FROM 
					simpleobjects.waitlistapp INNER JOIN simpleobjects.waitlistapplogs ON waitlistapp.WaitListId_bi = waitlistapplogs.WaitListId_bi
				WHERE 
					waitlistapp.UserId_int = #inpUserID#
					AND 
					waitlistapplogs.CreatedAt_dt 
						BETWEEN 
							STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateStart#">,'%Y-%m-%d') 
						AND 
							STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateEnd#">, '%Y-%m-%d')
				GROUP BY
					CreatedAt
			</cfquery>
			
			<cfloop query="GetAnalyticData">
				<cfset var item = {"period": "#CreatedAt#", "waitlisted": #WAITLISTED#, "served": #SERVED#, "noshow": #NOSHOW#} />
				<cfset ArrayAppend(dataout["DATA"]["#defineItem.TYPE#"], item) />
			</cfloop>
		</cfloop>	
		
		<cfreturn SerializeJSON(dataout)  />
	</cffunction>



</cfcomponent>