<cfcomponent>
	<!---get list recorded response to select in emergency --->
	<cffunction name="GetCredentialsListForDatatable" access="remote" output="true" hint="Get list of credential for datatable">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="customFilter" default="">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Access Key ID, 2 is Secret Access Key--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Access Key ID, 2 is Secret Access Key--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->

		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
	    <cfset dataout.allowCreate = true />
        <cfset var no = arguments.iDisplayStart />
        <cfset var countCredentials = 0 />
        <cfset var order_1 = '' />
        <cfset var order_0 = '' />
        <cfset var GetTotalCredentials = "">
        <cfset var GetCredentials = "">
        <cfset var data = {}/>

		<cftry>
       		<!---todo: check permission here --->
		   	<cfif 1 NEQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
			   	<cfset dataout["aaData"] = ArrayNew(1)>
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
			    <cfset dataout.MESSAGE = "Access denied"/>
				<cfset dataout.ERRMESSAGE = "You do not have permision to view Credentials"/>
			    <cfset dataout.TYPE = '' />
			   	<cfreturn serializeJSON(dataOut)>
		   	</cfif>
		   	<!---set order param for query--->
			<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
				<cfset order_1 = sSortDir_1>
			<cfelse>
				<cfset order_1 = "">
			</cfif>
			<cfif iSortCol_0 EQ 0>
				<cfset order_0 = "desc">
			<cfelse>
				<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
					<cfset order_0 = sSortDir_0>
				<cfelse>
					<cfset order_0 = "">
				</cfif>
			</cfif>
			<!---get data here --->

			<cfquery name="GetTotalCredentials" datasource="#Session.DBSourceEBM#">
	            SELECT
						count(Userid_int) AS totalCredentials
				    FROM
				    	simpleobjects.securitycredentials
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
			</cfquery>

			<cfquery name="GetCredentials" datasource="#Session.DBSourceEBM#">
	            SELECT
		   	    		securityCredentialsId_int AS securityCredentialsId,
						Userid_int AS Userid,
						Created_dt AS Created,
						AccessKey_vch AS AccessKey,
						SecretKey_vch AS SecretKey,
						Active_ti AS Active,
                        SecretName_vch AS SecretName
				    FROM
				    	simpleobjects.securitycredentials
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>

			<cfinvoke method="countCredentials" component="session.cfc.administrator.credential" returnvariable="countCredentials">

			</cfinvoke>
			<cfif countCredentials.count GTE 5 >
				<cfset dataout.allowCreate = false />
			</cfif>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalCredentials.totalCredentials>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalCredentials.totalCredentials>

 			<cfloop query="GetCredentials">
				<cfset var Created = LSDateFormat(GetCredentials.Created, 'mm/dd/yyyy') & ' ' & TimeFormat(GetCredentials.Created, 'hh:mm:ss tt')>
				<cfset no++ />
				<cfset data = {
                    No = #no#,
					Created = #Created#,
					AccessKey = #GetCredentials.AccessKey#,
					SecretKey = #GetCredentials.SecretKey#,
					Status = #GetCredentials.Active#
				}>
				<cfset arrayappend(dataout["aaData"],data)>
            </cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn serializeJSON(dataOut)>
	</cffunction>

	<cffunction name="GetDefaultActiveAPICredentials" access="public" output="false" hint="Get the default Active API Credentials">

	   <cfset var dataout = {} />
	   <cfset var GetCredentials = '' />

	   <!--- Set defaults --->
	   <cfset dataout.RXRESULTCODE = 0 />
	   <cfset dataout.RECORDCOUNT = 0/>

		<cfset dataout.ACCESSKEY = ""/>
		<cfset dataout.SECRETKEY = ""/>
		<cfset dataout.MESSAGE = ""/>
	   <cfset dataout.ERRMESSAGE = ""/>

	   <cftry>

		   	<cfquery name="GetCredentials" datasource="#Session.DBSourceEBM#">
		     	SELECT
		   			securityCredentialsId_int AS securityCredentialsId,
		   			Userid_int AS Userid,
		   			Created_dt AS Created,
		   			AccessKey_vch AS AccessKey,
		   			SecretKey_vch AS SecretKey,
		   			Active_ti AS Active,
		   	    		SecretName_vch AS SecretName
		   	    FROM
		   			simpleobjects.securitycredentials
		   		WHERE
		   			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
				AND
					Active_ti = 1
		   		LIMIT 1
		   	</cfquery>

		   	<cfset dataout.RXRESULTCODE = 1 />

			<cfif GetCredentials.RECORDCOUNT GT 0>
				<cfset dataout.RECORDCOUNT = "#GetCredentials.RecordCount#" />
				<cfset dataout.ACCESSKEY = "#GetCredentials.AccessKey#"/>
				<cfset dataout.SECRETKEY = "#GetCredentials.SecretKey#"""/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />


		<cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

	   </cftry>

		<cfreturn dataout />

	</cffunction>

</cfcomponent>
