<cfcomponent>
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cffunction name="getByName" output="true"  access="public" hint="Get a record of the settings table by primary key">
	    	<cfargument name="inpName" type="string" required="yes">
			<cfset var returnValue = {} />
			<cfset var rsQuery = {} />
			
			<cftry>
				<cfquery name="rsQuery" datasource="#Session.DBSourceREAD#">
					SELECT 
						VariableName_txt, Value_txt
					FROM 
						simpleobjects.sire_setting
					WHERE 
						VariableName_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpName#">
					LIMIT 1
				</cfquery>
				<cfloop query="rsQuery">
					<cfset returnValue[VariableName_txt] = Value_txt>
				</cfloop>
				<cfcatch>
					
				</cfcatch>
			</cftry>
			<cfreturn returnValue >
    </cffunction>


    <cffunction name="setByName" output="true" access="public" hint="set the value for specify key">
	    <cfargument name="inpName" type="string" required="yes">
	    <cfargument name="inpValue" type="string" required="yes">

	    <cfset var rsUpdate = 0>

		<cftry>
	    	<cfquery datasource="#Session.DBSourceEBM#" result="rsUpdate">
		    	UPDATE 
		    		simpleobjects.sire_setting
		    	SET 
		    		Value_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpValue#">
		    	WHERE 
		    		VariableName_txt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpName#">
		    	LIMIT 1
	    	</cfquery>
	    	<cfset rsUpdate = rsUpdate.recordCount >
	    	<cfcatch>
		    	<cfset rsUpdate = false>
		    </cfcatch>
	    </cftry>
	    <cfreturn rsUpdate>
    </cffunction>
    
</cfcomponent>