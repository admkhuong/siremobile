<cfcomponent>
	<cfinclude template="../../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/cfc/csc/constants.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/scheduleConstants.cfm">
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />

	<!---this function should be used to create new emergency --->
	<cffunction name="AddNewEmergency" access="remote" hint="Add a new batch with default XML and schedule options" output="false">
       	<cfargument name="INPBATCHDESC" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
        <cfargument name="INPXMLCONTROLSTRING" required="no" default="">
        <cfargument name="INPrxdsLIBRARY" required="no" default="0">
        <cfargument name="INPrxdsELEMENT" required="no" default="0">
        <cfargument name="INPrxdsSCRIPT" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpAltSysId" required="no" default="">
        <cfargument name="SCHEDULETYPE_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="DAYID_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="TIMEZONE" TYPE="string" default="0" required="no"/>
        <cfargument name="operator" TYPE="string" default="#Create_Campaign_Title#" required="no"/>
		<cfargument name="DefaultCIDVch" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0" hint="Keyweord and Interactive SMS will require allowing of duplicate messages">
		<cfargument name="CONTACTTYPES" required="no" default="1,2,3">
		<cfargument name="CONTACTTYPES_BITFORMAT" required="no" default="000">
		<cfargument name="EMAILCONTENT" required="no" default="">
		<cfargument name="SMSCONTENT" required="no" default="">
		<cfargument name="VOICECONTENT" required="no" default="">
  		<cfargument name="INPENABLEIMMEDIATEDELIVERY" required="no" default="">		  
  		<cfargument name="SAVEONLY" required="no" default="false">		  
  		<cfargument name="CONTACTFILTER" required="no" default="">	
        <cfargument name="ELIGIBLECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="UNITCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DNCCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DUPLICATECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="INVALIDTIMEZONECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="COSTCURRENTQUEUE" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="CURRENTBALANCE" required="no" default="" hint="Used for notifying admins launch values">	
  		<cfargument name="IVRBatchId" required="no" default="">
       	<cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>		
		<cfargument name="SCHEDULETIMEDATA" required="no"/>

<!---  		<cfset var args=StructNew()>
		<cfset args.RightName="STARTNEWCAMPAIGN">
		<cfset var res=false>
		<cfinvoke argumentcollection="#args#" method="checkRight" component="#LocalServerDirPath#.management.cfc.permission" returnvariable="res">
		</cfinvoke> --->
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
	      	<cfset var RetValAddKeywordEMS = '' />
            <cfset var GetEMSAdmins = '' />
       		<!---  <cfset var INPrxdsSCRIPT 	 = '' />
			<cfset var INPrxdsLIBRARY 	 = '' />
			<cfset var INPrxdsELEMENT 	 = '' />--->
			<cfset var GetGroupsData 	 = '' />
            <cfset var RetValAddFilter = 0>
            
            <cfset var DebugStr = '' />
            <cftry>
				<!--- Set default to error in case later processing goes bad --->
				<cfset var dataout = {}>
				<cfset var NEXTBATCHID = -1>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "##" />                
                <cfset dataout.ERRMESSAGE= "" />
				
				<!---check permission --->
				<cfset var permissionStr = 0>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#EMS_Add_Title#">
				</cfinvoke>
                
				<!--- Check permission against acutal logged in user not "Shared" user--->
				<cfset var getUserByUserId = 0>
				
				<cfif Session.CompanyUserId GT 0>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
				    </cfinvoke>
				<cfelse>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#session.userId#">
				    </cfinvoke>
				</cfif>
				
				<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
					OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
					<cfset dataout.RXRESULTCODE = -2 />
	                <cfset dataout.MESSAGE = "#permissionStr.message#" /> 
					<cfreturn dataout>               
				</cfif>
				
	        	<cfset var dataoutBatchId = '0' />
				
            	<!--- Validate session still in play - handle gracefully if not --->
	        	<cfset var retValHavePermission = 0/>
            	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="retValHavePermission">
					<cfinvokeargument name="operator" value="#operator#">
				</cfinvoke>
                                             
            	<cfif NOT retValHavePermission.havePermission>
					<!--- have not permission --->
                    
                    <cfset dataout.RXRESULTCODE = -2 />
					<cfset dataout.SAVEONLY = SAVEONLY />
                    <cfset dataout.NEXTBATCHID = "" />     
					<cfset dataout.INPBATCHDESC = "" />  
                    <cfset dataout.INPXMLCONTROLSTRING = "" />  
                    <cfset dataout.INPrxdsLIBRARY = "" />  
                    <cfset dataout.INPrxdsELEMENT = "" />  
                    <cfset dataout.INPrxdsSCRIPT = "" />  
                    <cfset dataout.INPGROUPID = "" />  
                    <cfset dataout.TYPE = -2 />
                    <cfset dataout.MESSAGE = retValHavePermission.message />                
                    <cfset dataout.ERRMESSAGE= "" /> 
				<cfelse>
                
                	<!--- Validations do not apply to filtered data yet - Do we even need these here??? JLP Removed for now--->
					<!---<cfquery name="GetGroupsData" datasource="#Session.DBSourceREAD#">
			            SELECT simplelists.groupcontactlist.groupid_bi,
							sum( case when ContactType_int = 1 then 1 else 0 end) as phoneNumber,
							sum( case when ContactType_int = 2 then 1 else 0 end) as mailNumber,
							sum( case when ContactType_int = 3 then 1 else 0 end) as smsNumber				
						FROM simplelists.groupcontactlist 
			            	INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
				            INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi 
						WHERE simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#INPGROUPID#">
            		</cfquery>
					<cfif GetGroupsData.recordCount NEQ 1 >
						<cfthrow MESSAGE='Group contact is invalid! <br/>' />
					</cfif>
                    
                    <!--- Do not need to check this --->
            		<!--- <cfif findnocase(1,CONTACTTYPES) >
					 	 <cfif GetGroupsData.phoneNumber EQ 0>
							<cfthrow MESSAGE='Number of phone contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>
	        		 <cfif findnocase(2,CONTACTTYPES) >
					 	 <cfif GetGroupsData.mailNumber EQ 0>
							<cfthrow MESSAGE='Number of email contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>
	        		 <cfif findnocase(3,CONTACTTYPES) >
					 	 <cfif GetGroupsData.smsNumber EQ 0>
							<cfthrow MESSAGE='Number of sms contact must be greater than zero. Please update group or select another group! <br/>' />
					 	 </cfif>
					 </cfif>--->
					 
					 --->
                     
                	<!---validate caller id for voice --->
                    <cfif findnocase(1,CONTACTTYPES) >
						<cfset var haveCidPermission = 0>
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
							<cfinvokeargument name="operator" value="#Caller_ID_Title#">
						</cfinvoke>
						<cfif (NOT haveCidPermission.havePermission) OR (NOT isvalid('telephone',DefaultCIDVch)) OR (DefaultCIDVch EQ '') >
							<cfthrow MESSAGE='Caller Id #DefaultCIDVch# is not a telephone or you do not have permission! <br/>' />
						</cfif>
                    </cfif>
					
                    <cfset var emailData= #deserializeJSON(EMAILCONTENT)# >
                    <cfset var smsData= #deserializeJSON(SMSCONTENT)# >
                    <cfset var voiceData= #deserializeJSON(VOICECONTENT)# >
					
                    <!--- Build XMLControlString 
					<cfset var XMLCONTROLSTRING = buildXmlControlString(
						callerId = "#DefaultCIDVch#",
						inpContactType = "#CONTACTTYPES_BITFORMAT#",<!---contact can be voice(010), sms(100), email(001),etc --->
						inpGroupId = "#INPGROUPID#",
						inpLib = "#voiceData.LIBID#",
				  		inpEle = "#voiceData.ELEID#",
					  	inpScriptId = "#voiceData.SCRID#",
						emailContent = #emailData#,
						smscontent = #smsData#,
						IVRBatchId = "#IVRBatchId#"
					) >
					--->

					
					<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="buildXmlControlString" returnvariable="XMLCONTROLSTRING">
                    	<cfinvokeargument name="callerId" value="#DefaultCIDVch#">  
                    	<cfinvokeargument name="inpContactType" value="#CONTACTTYPES_BITFORMAT#">                    
                    	<cfinvokeargument name="inpGroupId" value="#INPGROUPID#">
                    	<cfinvokeargument name="inpLib" value="#voiceData.LIBID#">
                    	<cfinvokeargument name="inpEle" value="#voiceData.ELEID#">
                    	<cfinvokeargument name="inpScriptId" value="#voiceData.SCRID#">
                    	<cfinvokeargument name="emailContent" value="#emailData#">
                    	<cfinvokeargument name="smscontent" value="#smsData#">
                    	<cfinvokeargument name="IVRBatchId" value="#IVRBatchId#">
                	</cfinvoke>  

					<cfif XMLCONTROLSTRING.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Error building XML control String" TYPE="Any" detail="#XMLCONTROLSTRING.MESSAGE# - #XMLCONTROLSTRING.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
                    
					<!---use return value from build xml function to create new campaign --->
					<cfset  INPrxdsSCRIPT = XMLCONTROLSTRING.INPrxdsSCRIPT>
					<cfset  INPrxdsLIBRARY = XMLCONTROLSTRING.INPrxdsLIBRARY>
					<cfset  INPrxdsELEMENT = XMLCONTROLSTRING.INPrxdsELEMENT>
					
					<!--- Verify all numbers are actual numbers ---> 
					 <cfif !isnumeric(INPrxdsLIBRARY) OR !isnumeric(INPrxdsLIBRARY) >
                    	<cfthrow MESSAGE="Invalid rxds Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsELEMENT) OR !isnumeric(INPrxdsELEMENT) >
                    	<cfthrow MESSAGE="Invalid rxds Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsSCRIPT) OR !isnumeric(INPrxdsSCRIPT) >
                    	<cfthrow MESSAGE="Invalid rxds Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
				  	<!--- Set default to -1 --->
				   	<cfset NEXTBATCHID = -1>                                  
                    
                    <!---check balance --->
					<!---get balance --->
					<cfset var RetValBillingData = 0>
                    <cfinvoke 
						 component="#LocalSessionDotPath#.cfc.billing"
						 method="GetBalance"
						 returnvariable="RetValBillingData">                     
					</cfinvoke>

					
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
					</cfif>

					

					<!---get list recipients --->
					<!---contact type : 1 is voice, 2 is mail, 3 is sms --->
					<cfset var RecipientList = 0>
					<cfinvoke 
						 component="#LocalSessionDotPath#.cfc.distribution"
						 method="GetRecipientList"
						 returnvariable="RecipientList">
				  		<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
				        <cfinvokeargument name="INPBATCHID" value="0">
				        <cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
					</cfinvoke>

					<cfif RecipientList.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Get Recipient List Error" TYPE="Any" detail="#RecipientList.MESSAGE# - #RecipientList.ERRMESSAGE#" errorcode="-5">
					</cfif>
					<!---compare balance and list GetRecipientList --->
					<!---todo: messagePrice -- as in listcampaigns is 10--->
					<cfset var messagePrice = 1>						
					<!---if balance is not enough, return --->
					
					<cfif RetValBillingData.BALANCE LT (arraylen(RecipientList.data)/messagePrice) >

					    <cfthrow MESSAGE="Balance Error" TYPE="Any" detail="You do not have enough funds to run this campaign." errorcode="-4">
					</cfif>
					
					<cfset var AddBatchQuery = 0>
					<cfset var AddBatch = 0>
                  	<cfquery name="AddBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                        INSERT INTO simpleobjects.batch
                            (
                            	UserId_int,
                                rxdsLibrary_int,
                                rxdsElement_int, 
                                rxdsScript_int, 
                                UserBatchNumber_int, 
                                Created_dt, 
                                DESC_VCH, 
                                LASTUPDATED_DT, 
                                GroupId_int, 
                                AltSysId_vch ,
                                XMLCONTROLSTRING_VCH,
                                AllowDuplicates_ti,
                                Active_int,
                                EMS_Flag_int
                                <cfif CONTACTFILTER NEQ "">,ContactFilter_vch</cfif>
                            )
                        VALUES
                        	(
                              	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
                                0,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHDESC#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAltSysId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING.XMLCONTROLSTRING_VCH#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">,
                                1,
                                1<!---this field should be set as 1 since we create new ems --->
                                <cfif CONTACTFILTER NEQ "">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTFILTER#"></cfif>
                            )
                    </cfquery>  
					
					<!---get batchid of recent inserted record ---> 
                    <cfset NEXTBATCHID = #AddBatch.generated_key#>
                    
                    <!--- Lee Note: Now that we have a batch id we can add an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->                    
                    <cfif bitAnd(CONTACTTYPES_BITFORMAT, 4) EQ 4>		
                    
	                  <!---  <cfset var mainMessage = XmlFormat(SMSCONTENT.MESSAGE)>
						<cfset var shortcode = SMSCONTENT.SHORTCODE>
						<!--- or !IsNumeric(shortcode) --->
                        <cfif MainMessage EQ "" or shortcode EQ "">
                            <cfset dataOut.RXRESULTCODE = -1>
                            <cfset dataOut.XMLCONTROLSTRING_VCH = "">
                            <cfset dataOut.MESSAGE = "Short code is invalid!">
                            <cfset dataOut.ERRMESSAGE = "Validate short code data fail!">
                            <cfreturn dataOut>
                        </cfif>
                        <cfset var HelpResponseMessage = "">
                        <cfset var StopMOMessage = "">
                        <cfset var Keyword = "">--->
						
                        <cfinvoke 
                         component="#LocalSessionDotPath#.cfc.csc.csc"
                         method="AddKeywordEMS"
                         returnvariable="RetValAddKeywordEMS">
                            <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                            <cfinvokeargument name="ShortCode" value="#smsData.SHORTCODE#"/>
                            <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTBATCHID#"/>
                            <cfinvokeargument name="Response" value="#smsData.MESSAGE#"/>
                            <cfinvokeargument name="IsDefault" value="0"/>
                            <cfinvokeargument name="IsSurvey" value="0"/>         
                        </cfinvoke>

                        <cfif RetValAddKeywordEMS.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetValAddKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValAddKeywordEMS.ERRMESSAGE#" errorcode="-2">                   
	                    </cfif>    
                    </cfif>
                  	<!---2 is use advance schedule--->
					<cfif INPENABLEIMMEDIATEDELIVERY NEQ 2>						
						<!--- Auto create schedule with defaults or specified values--->											
						<!---Not enable immediate delivery: 1 is delivery now--->
						<cfset var ScheduleTimeArray = ArrayNew(1)>	
						<cfset var scheduleTime = {}>							
						<!---Enable immediate delivery: 0 is immediate delivery--->					
						<cfscript>
							scheduleTime.scheduleType = SCHEDULETYPE_INT_ENABLE;
							scheduleTime.startDate = START_DT_ENABLE;
							scheduleTime.stopDate = STOP_DT_ENABLE;
							scheduleTime.startHour = STARTHOUR_TI_ENABLE;
							scheduleTime.startMinute = STARTMINUTE_TI_ENABLE;
							scheduleTime.endHour = ENDHOUR_TI_ENABLE;
							scheduleTime.endMinute = ENDMINUTE_TI_ENABLE;	
							scheduleTime.enabledBlackout = ENABLEDBLACKOUT_BT_ENABLE;
							scheduleTime.blackoutStartHour = BLACKOUTSTARTHOUR_TI_ENABLE;
							scheduleTime.blackoutStartMinute = BLACKOUTSTARTMINUTE_TI_ENABLE;
							scheduleTime.blackoutEndHour = BLACKOUTENDHOUR_TI_ENABLE;
							scheduleTime.blackoutEndMinute = BLACKOUTENDMINUTE_TI_ENABLE;
							scheduleTime.dayId = DAYID_INT_ENABLE;
							scheduleTime.timeZone = TIMEZONE_ENABLE;
						</cfscript>
						<cfset ArrayAppend(ScheduleTimeArray, scheduleTime)>
						<cfset var RetValSchedule = 0>
	                    <cfinvoke 
	                     component="#LocalSessionDotPath#.sire.models.cfc.schedule"
	                     method="UpdateSchedule"
	                     returnvariable="RetValSchedule">
	                        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
	                        <cfinvokeargument name="SCHEDULETIME" value="#SerializeJSON(ScheduleTimeArray)#"/>
	                        <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
	                    </cfinvoke>
	                   
	                <cfelse>
						<cfset var RetValSchedule = 0>
						<!---use advance schedule --->							
						 <cfinvoke 
                         component="schedule"
                         method="UpdateSchedule"
                         returnvariable="RetValSchedule">
                            <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#"/>
                            <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                            <cfinvokeargument name="SCHEDULETIME" value="#SCHEDULETIMEDATA#"/>                          
                        </cfinvoke>
					</cfif>
                 	<!---todo:in version 1.0, we user only one group 	                	                                        
	             		but in later one, input contacts can be from multiple group or individual contact
	             		therefore we need to create new group before insert new emergency 
					--->
					<!--- update recipient --->
					<cfset var IsApplyFilter = 1>
					<cfset var Note = "">
					<cfset var UpdateCampaignData = 0>
					
					<cfquery name="UpdateCampaignData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.batch
                        SET   
                            ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
							ContactTypes_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactTypes#">,
							ContactNote_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Note#">,
							ContactIsApplyFilter = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#IsApplyFilter#">
                        WHERE   
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NEXTBATCHID#">
                    </cfquery>
					
					<!---launch campaign --->
					<!---If SAVEONLY false, EMS lauch now--->
					<cfif SAVEONLY EQ false>
					                    	
                    	<!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                            	simpleobjects.systemalertscontacts
                            WHERE
                            	ContactType_int = 2
                            AND
                            	EMSNotice_int = 1    
                        </cfquery>
                       
                       	<cfif GetEMSAdmins.RecordCount GT 0>
                        
                        	<cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                            
                            <cfmail to="#EBMAdminEMSList#" subject="EMS Campaign Scheduled" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                
                                
                                   <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EMS Launch Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserName#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.EmailAddress#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.CompanyUserId#">
                                            </div>				
                                        </div>
                                         <div class="clear"></div>                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.companyId#">
                                            </div>				
                                        </div>
                                       
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Eligible Contact Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#ELIGIBLECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Credit Unit Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#UNITCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Do not contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DNCCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---duplicate --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Duplicate</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DUPLICATECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---invalid timezone --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Invalid Timezone</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INVALIDTIMEZONECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current in queue cost--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current in queue cost</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#COSTCURRENTQUEUE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current balance --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current balance</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#CURRENTBALANCE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        
                                    </div>
                                </cfoutput>
                            </cfmail>
                        
                        </cfif>
                    
						<cfinvoke 
							 component="#Session.SessionCFCPath#.distribution"
							 method="AddFiltersToQueue"
							 returnvariable="RetValAddFilter">
					  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
					        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
					        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
					        <cfinvokeargument name="INPBATCHID" value="#NEXTBATCHID#">
					        <cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#">
							<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
							<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
							<cfinvokeargument name="CONTACTFILTER" value="#CONTACTFILTER#">
							<cfinvokeargument name="Note" value="">
							<cfinvokeargument name="IsApplyFilter" value="1">
						</cfinvoke>	
						
						<cfif RetValAddFilter.RXRESULTCODE LT 1>
							<cfthrow MESSAGE="Launch EMS error" TYPE="Any" detail="#RetValAddFilter.MESSAGE# - #RetValAddFilter.ERRMESSAGE#" errorcode="-6">
						</cfif>
                        
                        <cfset DebugStr = DebugStr & " #RetValAddFilter.MESSAGE#" />
					</cfif>
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Campaign">
						<cfinvokeargument name="operator" value="Create Campaign">
					</cfinvoke>
					
				 	<cfset dataout.RXRESULTCODE = 1 />
				 	<cfset dataout.SAVEONLY = SAVEONLY />
	                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
					<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & "&nbsp;" />  
	                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
	                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
	                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
	                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
	                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
	                <cfset dataout.TYPE = "" />
	                <cfset dataout.MESSAGE = "#DebugStr#" />                
	                <cfset dataout.ERRMESSAGE= "" />    
                </cfif>          
            <cfcatch TYPE="any">
               	<cfset var dataout = {} />
               	<cfset dataout.RXRESULTCODE = -1 />
			 	<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.NEXTBATCHID = "#NEXTBATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & "&nbsp;" />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />            
            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>

    <!---this function should be used to update emergency --->
	<cffunction name="UpdateEmergency" output="false" access="remote" hint="Add a new batch with default XML and schedule options">
       	<cfargument name="BATCHID" required="yes" default="0">
       	<cfargument name="INPBATCHDESC" required="no" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#">
        <cfargument name="INPXMLCONTROLSTRING" required="no" default="">
        <cfargument name="INPrxdsLIBRARY" required="no" default="0">
        <cfargument name="INPrxdsELEMENT" required="no" default="0">
        <cfargument name="INPrxdsSCRIPT" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="inpAltSysId" required="no" default="">
        <cfargument name="SCHEDULETYPE_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="SUNDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="MONDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="TUESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="WEDNESDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="THURSDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="FRIDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="SATURDAY_TI" TYPE="string" default="1" required="no"/>
        <cfargument name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#" required="no"/>
        <cfargument name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd("d", 30, START_DT), "yyyy-mm-dd")#" required="no"/>
        <cfargument name="STARTHOUR_TI" TYPE="string" default="9" required="no"/>
        <cfargument name="ENDHOUR_TI" TYPE="string" default="20" required="no"/>
        <cfargument name="STARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="ENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTSTARTMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDHOUR_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="BLACKOUTENDMINUTE_TI" TYPE="string" default="0" required="no"/>
        <cfargument name="DAYID_INT" TYPE="string" default="1" required="no"/>
        <cfargument name="TIMEZONE" TYPE="string" default="0" required="no"/>
        <cfargument name="operator" TYPE="string" default="#Create_Campaign_Title#" required="no"/>
		<cfargument name="DefaultCIDVch" default="">
        <cfargument name="INPALLOWDUPLICATES" required="no" default="0" hint="Keyweord and Interactive SMS will require allowing of duplicate messages">
		<cfargument name="CONTACTTYPES" required="no" default="1,2,3">
		<cfargument name="CONTACTTYPES_BITFORMAT" required="no" default="000">
		<cfargument name="EMAILCONTENT" required="no" default="">
		<cfargument name="SMSCONTENT" required="no" default="">
		<cfargument name="VOICECONTENT" required="no" default="">
  		<cfargument name="INPENABLEIMMEDIATEDELIVERY" required="no" default="">		  
  		<cfargument name="SAVEONLY" required="no" default="false">
       	<cfargument name="LOOPLIMIT_INT" TYPE="string" default="100" required="no"/>		
		<cfargument name="SCHEDULETIMEDATA" required="no"/>
		<cfargument name="CONTACTFILTER" required="no" default = ""/>
        <cfargument name="ELIGIBLECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="UNITCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DNCCOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="DUPLICATECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="INVALIDTIMEZONECOUNT" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="COSTCURRENTQUEUE" required="no" default="" hint="Used for notifying admins launch values">	
        <cfargument name="CURRENTBALANCE" required="no" default="" hint="Used for notifying admins launch values">	

<!---  		<cfset var args=StructNew()>
		<cfset args.RightName="STARTNEWCAMPAIGN">
		<cfset var res=false>
		<cfinvoke argumentcollection="#args#" method="checkRight" component="#LocalServerDirPath#.management.cfc.permission" returnvariable="res">
		</cfinvoke> --->
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
	      	<cfset var RetValAddKeywordEMS = '' />
       	 <!--- 	<cfset var INPrxdsSCRIPT= '' />
			<cfset var INPrxdsLIBRARY= '' />
			<cfset var INPrxdsELEMENT= '' /> --->
			<cfset var NEXTBATCHID= '' />			
			<cfset var GetEMSAdmins= '' />
			<cfset var RetValUpdateKeywordEMS= '' />
			<cfset var UpdateBatchQuery= '' />
            <cftry>
				<!--- Set default to error in case later processing goes bad --->
				<cfset var dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.SAVEONLY = SAVEONLY />
                <cfset dataout.BATCHID = "#BATCHID#" />     
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;' />  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "##" />                
                <cfset dataout.ERRMESSAGE= "" />
				
				<!---check permission --->
				<cfset var permissionStr = 0>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
					<cfinvokeargument name="operator" value="#EMS_Add_Title#">
				</cfinvoke>
                
				<!--- Check permission against acutal logged in user not "Shared" user--->
				<cfset var getUserByUserId = 0>
				
				<cfif Session.CompanyUserId GT 0>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
				    </cfinvoke>
				<cfelse>
				    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
				        <cfinvokeargument name="userId" value="#session.userId#">
				    </cfinvoke>
				</cfif>
				
				<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
					OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
					<cfset dataout.RXRESULTCODE = -2 />
	                <cfset dataout.MESSAGE = "#permissionStr.message#" /> 
					<cfreturn dataout>               
				</cfif>
				
	        	<cfset var dataoutBatchId = '0' />
				
            	<!--- Validate session still in play - handle gracefully if not --->
	        	<cfset var retValHavePermission = 0/>
            	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="retValHavePermission">
					<cfinvokeargument name="operator" value="#operator#">
				</cfinvoke>
                                             
            	<cfif NOT retValHavePermission.havePermission>
					<!--- have not permission --->
                    
                    <cfset dataout.RXRESULTCODE = -2 />                         
					<cfset dataout.INPBATCHDESC = "" />  
					<cfset dataout.SAVEONLY = SAVEONLY />
                    <cfset dataout.INPXMLCONTROLSTRING = "" />  
                    <cfset dataout.INPrxdsLIBRARY = "" />  
                    <cfset dataout.INPrxdsELEMENT = "" />  
                    <cfset dataout.INPrxdsSCRIPT = "" />  
                    <cfset dataout.INPGROUPID = "" />  
                    <cfset dataout.TYPE = -2 />
                    <cfset dataout.MESSAGE = retValHavePermission.message />                
                    <cfset dataout.ERRMESSAGE= "" /> 
				<cfelse>
            	
                	<!---validate caller id for voice --->
                    <cfif findnocase(CONTACTTYPES,"1") >
						<cfset var haveCidPermission = 0>
						<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
							<cfinvokeargument name="operator" value="#Caller_ID_Title#">
						</cfinvoke>
						<cfif (NOT haveCidPermission.havePermission) OR (NOT isvalid('telephone',DefaultCIDVch)) OR (DefaultCIDVch EQ '') >
							<cfthrow MESSAGE='Caller Id #DefaultCIDVch# is not a telephone or you do not have permission! <br/>' />
						</cfif>
                    </cfif>
					
                    <cfset var emailData= #deserializeJSON(EMAILCONTENT)# >
                    <cfset var smsData= #deserializeJSON(SMSCONTENT)# >
                    <cfset var voiceData= #deserializeJSON(VOICECONTENT)# >
					
                    <!--- Build XMLControlString 
					<cfset var XMLCONTROLSTRING = buildXmlControlString(
						callerId = "#DefaultCIDVch#",
						inpContactType = "#CONTACTTYPES_BITFORMAT#",<!---contact can be voice(010), sms(100), email(001),etc --->
						inpGroupId = "#INPGROUPID#",
						inpLib = "#voiceData.LIBID#",
				  		inpEle = "#voiceData.ELEID#",
					  	inpScriptId = "#voiceData.SCRID#",
						emailContent = #emailData#,
						smscontent = #smsData#
					) >
					--->

					<cfinvoke component="#LocalSessionDotPath#.cfc.emergencymessages" method="buildXmlControlString" returnvariable="XMLCONTROLSTRING">
                    	<cfinvokeargument name="callerId" value="#DefaultCIDVch#">  
                    	<cfinvokeargument name="inpContactType" value="#CONTACTTYPES_BITFORMAT#">                    
                    	<cfinvokeargument name="inpGroupId" value="#INPGROUPID#">
                    	<cfinvokeargument name="inpLib" value="#voiceData.LIBID#">
                    	<cfinvokeargument name="inpEle" value="#voiceData.ELEID#">
                    	<cfinvokeargument name="inpScriptId" value="#voiceData.SCRID#">
                    	<cfinvokeargument name="emailContent" value="#emailData#">
                    	<cfinvokeargument name="smscontent" value="#smsData#">
                	</cfinvoke>  
					
					<cfif XMLCONTROLSTRING.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Error building XML control String" TYPE="Any" detail="#XMLCONTROLSTRING.MESSAGE# - #XMLCONTROLSTRING.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
                    
					<!---use return value from build xml function to create new campaign --->
					<cfset  INPrxdsSCRIPT = XMLCONTROLSTRING.INPrxdsSCRIPT>
					<cfset  INPrxdsLIBRARY = XMLCONTROLSTRING.INPrxdsLIBRARY>
					<cfset  INPrxdsELEMENT = XMLCONTROLSTRING.INPrxdsELEMENT>
					
					<!--- Verify all numbers are actual numbers ---> 
					 <cfif !isnumeric(INPrxdsLIBRARY) OR !isnumeric(INPrxdsLIBRARY) >
                    	<cfthrow MESSAGE="Invalid rxds Library Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsELEMENT) OR !isnumeric(INPrxdsELEMENT) >
                    	<cfthrow MESSAGE="Invalid rxds Element Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPrxdsSCRIPT) OR !isnumeric(INPrxdsSCRIPT) >
                    	<cfthrow MESSAGE="Invalid rxds Script Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                    	<cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
				  	<!--- Set default to -1 --->
				   	<cfset NEXTBATCHID = -1>                                  
                    
                    <!---check balance --->
					<!---get balance --->
					<cfset var RetValBillingData = 0>
                    <cfinvoke 
						 component="#LocalSessionDotPath#.cfc.billing"
						 method="GetBalance"
						 returnvariable="RetValBillingData">                     
					</cfinvoke>
                    <cfif RetValBillingData.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
					</cfif>
					<!---get list recipients --->
					<!---contact type : 1 is voice, 2 is mail, 3 is sms --->
					<cfset var RecipientList = 0>
					<cfinvoke 
						 component="#Session.SessionCFCPath#.distribution"
						 method="GetRecipientList"
						 returnvariable="RecipientList">
				  		<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
				        <cfinvokeargument name="INPBATCHID" value="0">
				        <cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
					</cfinvoke>
					<cfif RecipientList.RXRESULTCODE LT 1>
					    <cfthrow MESSAGE="Get Recipient List Error" TYPE="Any" detail="#RecipientList.MESSAGE# - #RecipientList.ERRMESSAGE#" errorcode="-5">
					</cfif>
					<!---compare balance and list GetRecipientList --->
					<!---todo: messagePrice -- as in listcampaigns is 10--->
					<cfset var messagePrice = 1>						
					<!---if balance is not enough, return --->
					<cfif RetValBillingData.BALANCE LT (arraylen(RecipientList.data)/messagePrice) >
					    <cfthrow MESSAGE="Balance Error" TYPE="Any" detail="You do not have enough funds to run this campaign." errorcode="-4">
					</cfif>
					
					<cfset var AddBatchQuery = 0>
					<cfset var AddBatch = 0>
                  	<cfquery name="UpdateBatchQuery" datasource="#Session.DBSourceEBM#" result="AddBatch">
                        UPDATE simpleobjects.batch
						SET
							rxdsLibrary_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsLIBRARY#">,
							rxdsElement_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsELEMENT#">,
                            rxdsScript_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPrxdsSCRIPT#">,
                            UserBatchNumber_int = 0,
                            DESC_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHDESC#">,
                            LASTUPDATED_DT =  NOW(),
                            GroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
                            AltSysId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAltSysId#">,
                            XMLCONTROLSTRING_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLCONTROLSTRING.XMLCONTROLSTRING_VCH#">,
							ContactFilter_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CONTACTFILTER#">,
                            AllowDuplicates_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPALLOWDUPLICATES#">
						WHERE
							Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#BATCHID#">
                    </cfquery>  
					
                    <!--- Lee Note: Now that we have a batch id we can update an SMS keyword with content if one is required. Check CONTACTTYPES_BITFORMAT --->                    
                    <cfif bitAnd(CONTACTTYPES_BITFORMAT, 4) EQ 4>
            			
                        <cfinvoke 
                         component="#LocalSessionDotPath#.cfc.csc.csc"
                         method="UpdateKeywordEMS"
                         returnvariable="RetValUpdateKeywordEMS">
                            <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
                            <cfinvokeargument name="ShortCode" value="#smsData.SHORTCODE#"/>
                            <cfinvokeargument name="Keyword" value="EMS_#Session.USERID#_#NEXTBATCHID#"/>
                            <cfinvokeargument name="Response" value="#smsData.MESSAGE#"/>
                            <cfinvokeargument name="IsDefault" value="0"/>
                            <cfinvokeargument name="IsSurvey" value="0"/>                           
                        </cfinvoke>
                        
                        <cfif RetValUpdateKeywordEMS.RXRESULTCODE LT 0>                    
	                        <cfthrow MESSAGE="#RetValUpdateKeywordEMS.MESSAGE#" TYPE="Any" detail="#RetValUpdateKeywordEMS.ERRMESSAGE#" errorcode="-2">                   
	                    </cfif>
                    </cfif>
                  	<!---Update schedule --->
					<cfif SCHEDULETIMEDATA NEQ "">
						<cfset var RetValSchedule = 0>
						<!---use advance schedule --->							
						 <cfinvoke 
                         component="#LocalSessionDotPath#.sire.models.cfc.schedule"
                         method="UpdateSchedule"
                         returnvariable="RetValSchedule">
                            <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
                            <cfinvokeargument name="LOOPLIMIT_INT" value="#LOOPLIMIT_INT#"/>
                            <cfinvokeargument name="SCHEDULETIME" value="#SCHEDULETIMEDATA#"/>                          
                        </cfinvoke>
					</cfif> 
					 
                 	<!---todo:in version 1.0, we user only one group 	                	                                        
	             		but in later one, input contacts can be from multiple group or individual contact
	             		therefore we need to create new group before insert new emergency 
					--->
					<!--- update recipient --->
					<cfset var IsApplyFilter = 1>
					<cfset var Note = "">
					<cfset var UpdateCampaignData = 0>
					
					<cfquery name="UpdateCampaignData" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simpleobjects.batch
                        SET   
                            ContactGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">,
							ContactTypes_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactTypes#">,
							ContactNote_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Note#">,
							ContactIsApplyFilter = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#IsApplyFilter#">
                        WHERE   
							BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BATCHID#">
                    </cfquery>
					
					<!---launch campaign --->
					<!---If SAVEONLY false, EMS lauch now--->
					<cfif SAVEONLY EQ false>
                    
                    <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
                            SELECT
                                ContactAddress_vch                              
                            FROM 
                            	simpleobjects.systemalertscontacts
                            WHERE
                            	ContactType_int = 2
                            AND
                            	EMSNotice_int = 1    
                        </cfquery>
                       
                       	<cfif GetEMSAdmins.RecordCount GT 0>
                        
                        	<cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
                            
                           <cfmail to="#EBMAdminEMSList#" subject="EMS Campaign Scheduled" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                
                                
                                  <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                
                                	
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EMS Launch Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserName#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.EmailAddress#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.CompanyUserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>                                      
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.companyId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Eligible Contact Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#ELIGIBLECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Credit Unit Count</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#UNITCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Do not contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DNCCOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---duplicate --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Duplicate</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#DUPLICATECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---invalid timezone --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Invalid Timezone</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#INVALIDTIMEZONECOUNT#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current in queue cost--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current in queue cost</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#COSTCURRENTQUEUE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current balance --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current balance</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#CURRENTBALANCE#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        
                                    </div>
                                
                                </cfoutput>
                            </cfmail>
                        
                        </cfif>
                    
						<cfset var RetValAddFilter = 0>
						<cfinvoke 
							 component="#Session.SessionCFCPath#.distribution"
							 method="AddFiltersToQueue"
							 returnvariable="RetValAddFilter">
					  		<cfinvokeargument name="INPSCRIPTID" value="#INPrxdsSCRIPT#">
					        <cfinvokeargument name="inpLibId" value="#INPrxdsLIBRARY#">
					        <cfinvokeargument name="inpEleId" value="#INPrxdsELEMENT#">
					        <cfinvokeargument name="INPBATCHID" value="#BATCHID#">
					        <cfinvokeargument name="INPBATCHDESC" value="#INPBATCHDESC#">
							<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#">
							<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
							<cfinvokeargument name="CONTACTFILTER" value="#CONTACTFILTER#">
							<cfinvokeargument name="Note" value="">
							<cfinvokeargument name="IsApplyFilter" value="1">
						</cfinvoke>	
						
						<cfif RetValAddFilter.RXRESULTCODE LT 1>
							<cfthrow MESSAGE="Launch EMS error" TYPE="Any" detail="#RetValAddFilter.MESSAGE# - #RetValAddFilter.ERRMESSAGE#" errorcode="-6">
						</cfif>
					</cfif>
					<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
						<cfinvokeargument name="userId" value="#session.userid#">
						<cfinvokeargument name="moduleName" value="Campaign">
						<cfinvokeargument name="operator" value="Create Campaign">
					</cfinvoke>
					
				 	<cfset dataout.RXRESULTCODE = 1 />
				 	<cfset dataout.SAVEONLY = SAVEONLY />     
					<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;'/>  
	                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
	                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
	                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
	                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
	                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
	                <cfset dataout.TYPE = "" />
	                <cfset dataout.MESSAGE = "" />                
	                <cfset dataout.ERRMESSAGE= "" />    
                </cfif>          
            <cfcatch TYPE="any">
               	<cfset var dataout = {} />
               	<cfset dataout.RXRESULTCODE = -1 />
			 	<cfset dataout.SAVEONLY = SAVEONLY />
				<cfset dataout.INPBATCHDESC = "#INPBATCHDESC#" & '&nbsp;'/>  
                <cfset dataout.INPXMLCONTROLSTRING = "#INPXMLCONTROLSTRING#" />  
                <cfset dataout.INPrxdsLIBRARY = "#INPrxdsLIBRARY#" />  
                <cfset dataout.INPrxdsELEMENT = "#INPrxdsELEMENT#" />  
                <cfset dataout.INPrxdsSCRIPT = "#INPrxdsSCRIPT#" />  
                <cfset dataout.INPGROUPID = "#INPGROUPID#" />  
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />            
            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>
</cfcomponent>    