<cfcomponent hint="SMS Chat Feature" output="false">

	<cfinclude template="/public/sire/configs/paths.cfm"/>

	<cffunction name="getListKeyword" access="remote" output="false" hint="Get list key word with info">
		<cfargument name="inpUserId" TYPE="string" required="yes" default="#session.UserId#" />
		<cfargument name="inpKeyword" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var listKeyWord = "" />
		<cfset var countKeyWord = "" />
		<cfset var item = {} />
		<cfset var shortcode = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
		        <cfset orderField = 'K.Keyword_vch' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'K.Created_dt' />
		    </cfcase>
		    <cfcase value="2">
		        <cfset orderField = 'KeywordStatusActive' />
		    </cfcase>
		    <cfcase value="3">
		        <cfset orderField = 'KeywordStatusClosed' />
		    </cfcase>
			<cfcase value="4">
		        <cfset orderField = 'KeywordStatusTotal' />
		    </cfcase>
		</cfswitch>

		<cftry>
			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<cfquery name="countKeyWord" datasource="#Session.DBSourceREAD#">
				SELECT 
					COUNT(K.KeywordId_int) AS total
					
				FROM 
					`simpleobjects`.`batch` AS B
					 
				INNER JOIN 
					`sms`.keyword AS K 
					ON 
						B.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/> AND 
						B.EMS_Flag_int = 20 AND 
						B.BatchId_bi = K.BatchId_bi AND
						K.Active_int = 1 AND
				        K.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#"/>
						<cfif arguments.inpKeyword NEQ ''>
						AND
							K.Keyword_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%"/>
						</cfif>
			</cfquery>

			<cfif countKeyWord.total GT 0>
				<cfquery name="listKeyWord" datasource="#Session.DBSourceREAD#">
					SELECT 
						B.UserId_int, 
						B.BatchId_bi, 
						K.KeywordId_int, 
						K.Keyword_vch, 
						K.Created_dt,
						COUNT(IF(S.SessionState_int < 4, 1, NULL)) AS KeywordStatusActive,
						COUNT(IF(S.SessionState_int >= 4, 1, NULL)) AS KeywordStatusClosed,
						COUNT(S.SessionId_bi) AS KeywordStatusTotal
					FROM 
						`simpleobjects`.`batch` AS B
						 
					INNER JOIN 
						`sms`.keyword AS K 
						ON 
							B.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> AND 
							B.EMS_Flag_int = 20 AND 
							B.BatchId_bi = K.BatchId_bi AND
							K.Active_int = 1 AND
							K.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#"/>
							<cfif arguments.inpKeyword NEQ ''>
							AND
								K.Keyword_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
							</cfif>
							
					LEFT JOIN
						`simplequeue`.`sessionire` AS S
						ON
							B.BatchId_bi = S.BatchId_bi AND
							LENGTH(S.ContactString_vch) < 14
						
					GROUP BY 
						UserId_int, 
						BatchId_bi, 
						KeywordId_int, 
						Keyword_vch, 
						Created_dt

					<cfif orderField NEQ "">
						ORDER BY
							#orderField# #arguments.sSortDir_0#
					<cfelse>
						ORDER BY 
							Created_dt DESC
					</cfif>

					LIMIT 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
				</cfquery>
				
				<cfif listKeyWord.RecordCount GT 0>
					<cfloop query="listKeyWord">
						<cfset item = {
							"UserId_int": listKeyWord.UserId_int, 
							"BatchId_bi": listKeyWord.BatchId_bi, 
							"KeywordId_int": listKeyWord.KeywordId_int, 
							"Keyword_vch": HTMLEditFormat(listKeyWord.Keyword_vch), 
							"Created_dt": DateFormat(listKeyWord.Created_dt, 'mm/dd/yyyy'),
							"KeywordStatusActive": listKeyWord.KeywordStatusActive, 
							"KeywordStatusClosed": listKeyWord.KeywordStatusClosed,
							"KeywordTotal": listKeyWord.KeywordStatusTotal
						} />
						<cfset arrayAppend(dataout["aaData"],item) />
					</cfloop>
					
					<cfset dataout["iTotalRecords"] = countKeyWord.total/>
					<cfset dataout["iTotalDisplayRecords"] = countKeyWord.total/>
				</cfif>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="getListSession" access="remote" output="false" hint="Get list sessions, key word detail info">
		<cfargument name="inpBatchId" TYPE="numeric" required="yes" default="0"  />
		<cfargument name="inpNPA" TYPE="string" required="yes" default="0"  />
		<cfargument name="inpNXX" TYPE="string" required="yes" default="0"  />
		<cfargument name="inpContactString" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />
		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var listSession = "" />
		<cfset var countSession = "" />
		<cfset var item = {} />

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfquery name="countSession" datasource="#Session.DBSourceREAD#" >
				SELECT 
					COUNT(*) AS total
				FROM 
					(
					SELECT 
						BatchId_bi, 
						SessionId_bi, 
						SessionState_int, 
						ContactString_vch, 
						MAX(Created_dt) AS LastMSGTime_dt, 
						MAX(IF(IREType_int = 1, Created_dt, 0)) AS LastResponseTime_dt, 
						GROUP_CONCAT(IF(IREType_int = 2, Response_vch, '') SEPARATOR '') AS Receive_vch, 
						GROUP_CONCAT(IF(IREType_int = 1, Response_vch, '') SEPARATOR '') AS Response_vch

					FROM
						(
						SELECT 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch, 
							SUBSTRING_INDEX(GROUP_CONCAT(Response_vch ORDER BY Created_dt DESC, IREResultsId_bi DESC SEPARATOR '{% %}'), '{% %}', 1) AS Response_vch, 
							IREType_int, 
							MAX(Created_dt) AS Created_dt
							
						FROM
							(
							SELECT 
								S.BatchId_bi, 
								S.SessionId_bi, 
								S.SessionState_int, 
								S.ContactString_vch, 
								R.Response_vch, 
								IF(R.IREType_int IS NULL, 1, R.IREType_int) AS IREType_int, 
								IF(R.Created_dt IS NULL, S.LastUpdated_dt, R.Created_dt) AS Created_dt,
								R.IREResultsId_bi
							
							FROM 
								(
								SELECT 
									S.BatchId_bi, 
									S.SessionId_bi, 
									S.SessionState_int, 
									S.ContactString_vch,
									S.LastUpdated_dt
								FROM
									`simplequeue`.`sessionire` AS S
								WHERE
									S.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> AND 
									S.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"> AND
									S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNPA##arguments.inpNXX#%">
									<cfif arguments.inpContactString NEQ "">
										AND S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactString#%">
									</cfif>
								) AS S
							
							LEFT JOIN 
								`simplexresults`.`ireresults` R 
								ON 
									R.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> AND
									R.IREType_int IN (1,2) AND 
									S.SessionId_bi = R.IRESessionId_bi
							
							ORDER BY 
								Created_dt DESC, IREResultsId_bi DESC
							) AS list1
							
						GROUP BY 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch, 
							IREType_int
						
						ORDER BY 
							Created_dt DESC
						) AS list2

					GROUP BY 
						BatchId_bi, 
						SessionId_bi, 
						SessionState_int, 
						ContactString_vch
					) AS list3
			</cfquery>

			<cfif countSession.total GT 0>

				<cfquery name="listSession" datasource="#Session.DBSourceREAD#" >
					SELECT 
						BatchId_bi, 
						SessionId_bi, 
						SessionState_int, 
						ContactString_vch, 
						MAX(Created_dt) AS LastMSGTime_dt, 
						MAX(IF(IREType_int = 1, Created_dt, 0)) AS LastResponseTime_dt, 
						GROUP_CONCAT(IF(IREType_int = 2, Response_vch, '') SEPARATOR '') AS Receive_vch, 
						GROUP_CONCAT(IF(IREType_int = 1, Response_vch, '') SEPARATOR '') AS Response_vch

					FROM
						(
						SELECT 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch, 
							SUBSTRING_INDEX(GROUP_CONCAT(Response_vch ORDER BY Created_dt DESC, IREResultsId_bi DESC SEPARATOR '{% %}'), '{% %}', 1) AS Response_vch, 
							IREType_int, 
							MAX(Created_dt) AS Created_dt
							
						FROM
							(
							SELECT 
								S.BatchId_bi, 
								S.SessionId_bi, 
								S.SessionState_int, 
								S.ContactString_vch, 
								R.Response_vch, 
								IF(R.IREType_int IS NULL, 1, R.IREType_int) AS IREType_int, 
								IF(R.Created_dt IS NULL, S.LastUpdated_dt, R.Created_dt) AS Created_dt,
								R.IREResultsId_bi
							
							FROM 
								(
								SELECT 
									S.BatchId_bi, 
									S.SessionId_bi, 
									S.SessionState_int, 
									S.ContactString_vch,
									S.LastUpdated_dt
								FROM
									`simplequeue`.`sessionire` AS S
								WHERE
									S.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> AND 
									S.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"> AND
									S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNPA##arguments.inpNXX#%">
									<cfif arguments.inpContactString NEQ "">
										AND S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactString#%">
									</cfif>
								) AS S
							
							LEFT JOIN 
								`simplexresults`.`ireresults` R 
								ON 
									R.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> AND
									R.IREType_int IN (1,2) AND 
									S.SessionId_bi = R.IRESessionId_bi
							
							ORDER BY 
								Created_dt DESC, IREResultsId_bi DESC
							) AS list1
							
						GROUP BY 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch, 
							IREType_int
						
						ORDER BY Created_dt DESC
						) AS list2

					GROUP BY 
						BatchId_bi, 
						SessionId_bi, 
						SessionState_int, 
						ContactString_vch
					
					ORDER BY
						LastMSGTime_dt DESC
					
					LIMIT 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
				</cfquery>

				<cfif listSession.RecordCount GT 0>
					<cfloop query="listSession">
						<cfset var areaCode = Left(listSession.ContactString_vch, 3)>
						<cfset var firstThree = Mid(listSession.ContactString_vch, 4,3)>
						<cfset var lastFour = Right(listSession.ContactString_vch, 4)>
						<cfset var formatedNumber = "(#areaCode#) #firstThree#-#lastFour#"/>
						<cfset item = {
							"BatchId_bi": listSession.BatchId_bi, 
							"SessionId_bi": listSession.SessionId_bi, 
							"SessionState_int": listSession.SessionState_int, 
							"ContactString_vch": formatedNumber, 
							"LastMSGTime_dt": "#dateFormat(listSession.LastMSGTime_dt, "yyyy-mm-dd")# #timeFormat(listSession.LastMSGTime_dt, "HH:mm:ss")#", 
							"LastResponseTime_dt": "#dateFormat(listSession.LastResponseTime_dt, "yyyy-mm-dd")# #timeFormat(listSession.LastResponseTime_dt, "HH:mm:ss")#", 
							"Receive_vch": HTMLEditFormat(listSession.Receive_vch), 
							"Response_vch": HTMLEditFormat(listSession.Response_vch)
						} />
						<cfset arrayAppend(dataout["aaData"],item) />
					</cfloop>

					<cfset dataout["iTotalRecords"] = countSession.total>
					<cfset dataout["iTotalDisplayRecords"] = countSession.total>
				</cfif>

			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="createKeyword" access="remote" output="false" hint="Create a new key word for sms chat">
		<cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
		<cfargument name="inpSendEmailStart" TYPE="boolean" hint="Send email for user's email list when start a conversation." />
		<cfargument name="inpSendEmailReceived" TYPE="boolean" hint="Send email for user's email list when received a new message." />
		<cfargument name="inpEmailList" TYPE="string" hint="Email list will receive a notification." />
		<cfargument name="inpSendSMSStart" TYPE="boolean" hint="Send SMS for user's phone numer list when start a conversation." />
		<cfargument name="inpSendSMSReceived" TYPE="boolean" hint="Send SMS for user's phone numer list when received a new message." />
		<cfargument name="inpSMSList" TYPE="string" hint="Phone number list will receive a notification." />
		<cfargument name="inpWelcomeMsg" TYPE="string" hint="Welcome message customer receive when start a new chat"/>
		<cfargument name="inpEndMsg" TYPE="string" hint="Message customer receive when end chat"/>

		<!--- Default Return Structure --->		
		<cfset var DataOut	= {} />
		<cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.BATCHID = 0 />
		<cfset DataOut.KEYWORDID = 0 />
		<cfset DataOut.MESSAGE = "" />
		<cfset DataOut.ERRMESSAGE = "" />

		<cfset var RetVarGetUserOrganization = ""/>
		<cfset var shortCode = ""/>

		<cfset var XMLControlString_vch = ""/>
		<cfset var UpdateAPIControlPoint = {} />
		<cfset var RetVarDoDynamicTransformations = '' />
		<cfset var inpFormData = '' />
		<cfset var InsertBatch = '' />
		<cfset var RetVarValidateKeyword = '' />
		<cfset var RetVarSetCustomStandardReply = '' />
		<cfset var RetVarSaveKeyword = '' />
		<cfset var InsertBatchResult = '' />
		
		<!--- remove old method: 
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>				
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
		
		<!--- ZZZ check keyword... --->
		<cfinvoke component="session.sire.models.cfc.control-point" method="ValidateKeywordWithoutBatchId" returnvariable="RetVarValidateKeyword">
			<cfinvokeargument name="inpKeyword" value="#inpKeyword#">
			<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
		</cfinvoke>
		
		<cfif RetVarValidateKeyword.RXRESULTCODE EQ 1>
			<!--- Get default XML Control String for Chat Campaign --->
			<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 OR LCase(CGI.SERVER_NAME) EQ 'dev.siremobile.com'>
				<cffile action="read" variable="XMLControlString_vch" file="#getDirectoryFromPath(getCurrentTemplatePath())#/smschatxmlstring.txt" />
			<cfelseif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'staging.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'apiqa.siremobile.com') GT 0>
				<cffile action="read" variable="XMLControlString_vch" file="#getDirectoryFromPath(getCurrentTemplatePath())#/smschatxmlstring.qa.txt" />
			<cfelse>
				<cffile action="read" variable="XMLControlString_vch" file="#getDirectoryFromPath(getCurrentTemplatePath())#/smschatxmlstring.lc.txt" />
			</cfif>

			<!--- Update API settings for XML String --->
			<cfinvoke method="UpdateAPISettings" returnvariable="UpdateAPIControlPoint">
				<cfinvokeargument name="inpXMLControlString_vch" value="#XMLControlString_vch#" />
				<cfinvokeargument name="inpSettings_obj" value="#{
					"inpSendEmailStart": arguments.inpSendEmailStart,
					"inpSendEmailReceived": arguments.inpSendEmailReceived,
					"inpEmailList": arguments.inpEmailList,
					"inpSendSMSStart": arguments.inpSendSMSStart,
					"inpSendSMSReceived": arguments.inpSendSMSReceived,
					"inpSMSList": arguments.inpSMSList
				}#" />
				<cfinvokeargument name="inpWelcomeMsg" value="#arguments.inpWelcomeMsg#"/>
				<cfinvokeargument name="inpEndMsg" value="#arguments.inpEndMsg#"/>
			</cfinvoke>

			<cfif UpdateAPIControlPoint.RXRESULTCODE EQ 1>
				<cfset XMLControlString_vch = UpdateAPIControlPoint.XMLControlString />
			</cfif>
		
			<!--- Read Profile data and put into form --->
	        <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>
	          
	        <!---Create a data set to run against dynamic data --->
	        <cfset inpFormData = StructNew() />
	        
	        <cfset inpFormData.inpBrand = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
	        <cfset inpFormData.inpBrandFull = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
	        <cfset inpFormData.inpBrandWebURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
	        <cfset inpFormData.inpBrandWebTinyURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
	        <cfset inpFormData.inpBrandPhone = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
	        <cfset inpFormData.inpBrandeMail = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />
	        
	        <!---<cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"/></cfif>
	        <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"/></cfif>
	        <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"/></cfif>
	        <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"/></cfif>
	        <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"/></cfif>
	        <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"/></cfif>--->
	                    
	        <cfset inpFormData.CompanyNameShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
	        <cfset inpFormData.CompanyName = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
	        <cfset inpFormData.CompanyWebSite = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
	        <cfset inpFormData.CompanyWebSiteShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
	        <cfset inpFormData.CompanyPhoneNumber = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
	        <cfset inpFormData.CompanyEMailAddress = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />
	
	        <!--- Do the transforms here --->  
	        <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
	            <cfinvokeargument name="inpResponse_vch" value="#XMLControlString_vch#"> 
	            <cfinvokeargument name="inpContactString" value="">
	            <cfinvokeargument name="inpRequesterId_int" value=""> 
	            <cfinvokeargument name="inpFormData" value="#inpFormData#"> 
	            <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
	            <cfinvokeargument name="inpShortCode" value="">
	            <cfinvokeargument name="inpBatchId" value="">
	            <cfinvokeargument name="inpBrandingOnly" value="1">
	        </cfinvoke>   
	        
	        <!--- Update the current template data --->           
	        <cfset XMLControlString_vch = RetVarDoDynamicTransformations.RESPONSE />   

			<cftry>
	                               
			 	<!--- Write basic batch to DB --->    
			    <cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="InsertBatchResult">
					INSERT INTO simpleobjects.batch 
					(
						UserId_int,
		                rxdsLibrary_int,
		                rxdsElement_int, 
		                rxdsScript_int, 
		                UserBatchNumber_int, 
						Created_dt,
						<!--- Desc_vch, --->
						LastUpdated_dt,
						GroupId_int,
						XMLControlString_vch,
						Active_int,
						ContactGroupId_int,
						ContactTypes_vch,
						TemplateType_ti,
						EMS_Flag_int,
						RealTimeFlag_int,
						TemplateId_int
					) 
					VALUES 
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						0,
						NOW(),
						<!--- <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">, --->
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="20">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
					)
				</cfquery>
			
			    <cfcatch TYPE="any">
			    
			    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
				    <cfset DataOut.RXRESULTCODE = -1 /> 
		    		<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>
					
					<cfreturn DataOut>
				</cfcatch>
				
			</cftry>   
						
			<cfset DataOut.BATCHID = #InsertBatchResult.generated_key#>
						
			<cfset DataOut.CustomHelpMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch>
			<cfset DataOut.CustomStopMessage_vch = RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch>
			<cfset DataOut.XMLControlString = XMLControlString_vch>
			
			<!--- Write generic keyword HELP STOP   --->
			<cfinvoke component="session.sire.models.cfc.control-point" method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#DataOut.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="HELP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomHelpMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
			</cfinvoke>
	
			<cfinvoke component="session.sire.models.cfc.control-point" method="SetCustomStandardReply" returnVariable="RetVarSetCustomStandardReply">
				<cfinvokeargument name="inpBatchId" value="#DataOut.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="STOP">
				<cfinvokeargument name="inpResponse" value="#RetVarGetUserOrganization.ORGINFO.CustomStopMessage_vch#">
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
			</cfinvoke>
			
	    	<!--- If Keyword is specified then set/update it --->
			<cfinvoke component="session.sire.models.cfc.control-point" method="SaveKeyword" returnvariable="RetVarSaveKeyword">
				<cfinvokeargument name="inpBatchId" value="#DataOut.BATCHID#">
				<cfinvokeargument name="inpKeyword" value="#inpKeyword#">
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">	
			</cfinvoke>
			
			<cfset DataOut.KEYWORDID = RetVarSaveKeyword.KEYWORDID />
		
			<cfif RetVarSaveKeyword.RXRESULTCODE NEQ 1>	        	
	        	<cfthrow detail="#RetVarSaveKeyword.ERRMESSAGE#" message="#RetVarSaveKeyword.MESSAGE#" />	        	
	    	</cfif>
	    	
	    	<cfset DataOut.RXRESULTCODE = 1 />
	    	<cfset DataOut.MESSAGE = "Create keyword successfully!"/>
	    	
		<cfelse>
		
			<cfset DataOut.MESSAGE = "This Keyword is already in use on this Short Code. Please try another." />
    	
    	</cfif>

		<cfreturn DataOut />

	</cffunction>

	<cffunction name="completeSession" access="remote" output="false" hint="Close sms chat session by session id">
		<cfargument name="inpSessionId" TYPE="numeric" required="yes" default="0" />
		
		<cfset var DataOut	= {} />
		<cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.MESSAGE = "" />
		<cfset DataOut.ERRMESSAGE = "" />

		<cfset var updateCompleteSession = "" />
		<cfset var RetVarCompleteSession = "" />
		
		<cftry>
			<cfquery name="updateCompleteSession" datasource="#Session.DBSourceEBM#" result="RetVarCompleteSession">
				UPDATE
	                `simplequeue`.`sessionire`
	            SET
	                SessionState_int = 4,
	                LastUpdated_dt = NOW()                                 
	            WHERE
	                SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSessionId#"> AND
	                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#"> 
			</cfquery>
			
			<cfset DataOut.RXRESULTCODE = 1 />

		    <cfcatch TYPE="any">
		    
			    <cfset DataOut.RXRESULTCODE = -1 /> 
	    		<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset DataOut.ERRMESSAGE = "#cfcatch.detail#"/>
				
				<cfreturn DataOut>
			</cfcatch>

		</cftry>
		
		<cfreturn serializeJSON(DataOut)/>
		
	</cffunction>
	
	<cffunction name="getGroupResponse" access="remote" output="false" hint="Get group for response by list session id">
		<cfargument name="inpSessionIds" required="true" type="string" default="" />
		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />
		<cfset dataout.PHONEERROR = []/>

		<cfset var sessionIds = [] />
		<cfset var sessionId = 0 />
		<cfset var getListSession = "" />
		<cfset var validateContactString = "" />
		<cfset var item = {} />
		
		<cftry>
			
			<cfset sessionIds = listToArray(inpSessionIds, ',') />
			
			<cfloop list="arguments.inpSessionIds" index="sessionId">
				<cfif IsNumeric(sessionId)>
					<cfset arrayAppend(sessionIds,sessionId) />
				</cfif>
			</cfloop>
			
			<cfif arrayLen(sessionIds) GT 0 >
				
				<cfquery name="getListSession" datasource="#session.DBSourceREAD#">
					SELECT 
						SessionId_bi,
						SessionState_int,
						ContactString_vch,
						BatchId_bi
					
					FROM 
						`simplequeue`.`sessionire`
						
					WHERE 
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/> AND
						SessionId_bi IN (#arrayToList(sessionIds, ',')#) AND 
						SessionState_int = 1
				</cfquery>
				
				<cfif getListSession.RecordCount GT 0>
					
					<cfloop query="getListSession">
						<cfset validateContactString = ValidateContactStringSession(getListSession.ContactString_vch, getListSession.BatchId_bi)/>

						<cfif validateContactString.ISVALIDATE NEQ 1>
							<cfset arrayAppend(dataout.PHONEERROR, getListSession.ContactString_vch)/>
							<cfinvoke method="completeSession">
								<cfinvokeargument name="inpSessionId" value="#getListSession.SessionId_bi#"/>
							</cfinvoke>
							<cfcontinue/>
						</cfif>

						<cfset item = {
							"SessionId_bi": getListSession.SessionId_bi,
							"SessionState_int": getListSession.SessionState_int,
							"ContactString_vch": getListSession.ContactString_vch,
							"BatchId_bi": getListSession.BatchId_bi
						} />
						<cfset arrayAppend(dataout["aaData"],item) />
					</cfloop>
					
					<cfset dataout["iTotalRecords"] = getListSession.RecordCount>
					<cfset dataout["iTotalDisplayRecords"] = getListSession.RecordCount>
					
				</cfif>
				
			</cfif>
			
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout>

	</cffunction>
	
	<cffunction name="groupResponse" access="remote" output="false" returnformat="JSON" returntype="struct" hint="Send sms from sire user to group multi device">
		<cfargument name="inpSessionIds" required="true" type="string" default="" />
		<cfargument name="inpTextToSend" required="true" type="string" hint="Text to send"/>
		
		<cfset var DataOut	= {} />
		<cfset DataOut.RXRESULTCODE = 0 />
		<cfset DataOut.MESSAGE = "" />
		<cfset DataOut.ERRMESSAGE = "" />
		<cfset dataout.TYPE = '' />
		<cfset dataout.SENDERRORS = [] />
		<cfset dataout.SENDSUCCESS = [] />

		<cfset var sessionIds = [] />
		<cfset var sessionId = 0 />
		<cfset var getListSession = "" />
		<cfset var item = {} />
		<cfset var sessionResponseList = [] />
		<cfset var SendSingleMT = '' />
		<cfset var pushWS = '' />
		
		<cftry>
			
			<cfloop list="#arguments.inpSessionIds#" index="sessionId" delimiters=",">
				<cfif IsNumeric(sessionId)>
					<cfset arrayAppend(sessionIds,sessionId) />
				</cfif>
			</cfloop>
			
			<cfif arrayLen(sessionIds) GT 0 >
				
				<cfquery name="getListSession" datasource="#session.DBSourceREAD#">
					SELECT 
						SessionId_bi,
						SessionState_int,
						ContactString_vch,
						BatchId_bi
					
					FROM 
						`simplequeue`.`sessionire`
						
					WHERE 
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" /> AND 
						SessionId_bi IN (#arrayToList(sessionIds, ',')#) AND 
						SessionState_int = 1
				</cfquery>
				
				<cfif getListSession.RecordCount GT 0>

					<cfloop query="getListSession">

						<cfinvoke method="SendSMS" returnvariable="SendSingleMT">
							<cfinvokeargument name="inpSessionId" value="#getListSession.SessionId_bi#"/>
							<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
							<cfinvokeargument name="inpSkipPushWS" value="1"/>
						</cfinvoke>

						<cfif deserializeJSON(SendSingleMT).RXRESULTCODE NEQ 1>
							<cfset arrayAppend(dataout.SENDERRORS, getListSession.ContactString_vch)/>
						<cfelse>
							<cfset arrayAppend(dataout.SENDSUCCESS, getListSession.ContactString_vch)/>
						</cfif>

					</cfloop>

					<cfif len(dataout.SENDSUCCESS) GT 0>
						<cfinvoke component="session.sire.models.cfc.smschat" method="PushWS" returnvariable="pushWS">
							<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
							<cfinvokeargument name="inpContactString" value="#arrayToList(dataout.SENDSUCCESS)#"/>
						</cfinvoke>
					</cfif>

					<cfset dataout.RXRESULTCODE = 1/>
					<cfset dataout.MESSAGE = "Send messages successfully!"/>
				<cfelse>
					<cfset dataout.MESSAGE = "Converstions were closed!"/>
				</cfif>
				
			</cfif>
			
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout>

	</cffunction>
	
	<cffunction name="formatContactString" access="public" returnformat="plain" returntype="string" output="false" hint="Return format for contact number">
		<cfargument name="inpContactString" required="true" type="string" hint="Contact String"/>
		<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
		<cfset var phone = '' />
		<cfset var lead = '' />
		<cfif Len(arguments.inpContactString) NEQ 10 AND Len(arguments.inpContactString) NEQ 11>
			<cfreturn arguments.inpContactString/>
		<cfelseif Len(arguments.inpContactString) EQ 11>
			<cfset phone = Right(arguments.inpContactString, 10)/>
			<cfset lead = Left(arguments.inpContactString, 1)/>
		<cfelse>
			<cfset phone = arguments.inpContactString/>
		</cfif>
		<cfset var areaCode = Left(phone, 3)>
		<cfset var firstThree = Mid(phone, 4,3)>
		<cfset var lastFour = Right(phone, 4)>
		<cfset var formatedNumber = "#lead#(#areaCode#) #firstThree#-#lastFour#"/>

		<cfreturn formatedNumber>
	</cffunction>

	<cffunction name="callCPAPI" access="public" returnformat="plain" returntype="string" output="false" hint="Call from cp api">
		<cffile action="write" output="#SerializeJSON(url)#\n#SerializeJSON(form)#" file="#getDirectoryFromPath(getCurrentTemplatePath())#/zzz.txt" />
		<cfreturn 1>
	</cffunction>
	
	<cffunction name="SendSMS" access="remote" output="false" hint="Send sms from sire user to user's device">
		<cfargument name="inpSessionId" required="true" type="numeric" hint="Chat session id"/>
		<cfargument name="inpTextToSend" required="true" type="string" hint="Text to send"/>
		<cfargument name="inpSkipPushWS" required="false" default="0"/>
		<cfargument name="inpShortCodeid" required="false" default="0"/>

		<cfset var dataout = {} />
		<cfset var RetVarSendSingleMT = '' />
		<cfset var checkSession = ''/>
		<cfset var shortCode = '' />
		<cfset var getSessionInfo = '' />
		<cfset var validateContactString = ''/>
		<cfset var reg = '' />
		<cfset var pushWS = '' />
		<cfset var RetVarSplit = '' />
		<cfset var text = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<!--- <cfset arguments.inpTextToSend = reReplace(arguments.inpTextToSend, '\n', '<br />', 'ALL')/> --->

			<!--- Check session owner --->
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.UserId_int
				FROM
					simplequeue.sessionire ss
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					ss.SessionState_int = 1
			</cfquery>

			<cfif checkSession.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Session closed! The customer maybe left this conversation!"/>
			</cfif>

			<cfinvoke component="public.sire.models.cfc.shortcode" method="GetShortCodeById" returnvariable="shortCode">
				<cfinvokeargument name="inpShortCodeid" value="#arguments.inpShortCodeid#"/>
			</cfinvoke>

			<cfset getSessionInfo = GetSessionDetails(arguments.inpSessionId)/>

			<cfif getSessionInfo.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Session ID not valid!"/>
			</cfif>

			<!--- Validate contact string, to make sure it not on another conversation with different user --->
			<cfset validateContactString = ValidateContactStringSession(getSessionInfo.INFO.CONTACTSTRING, getSessionInfo.INFO.BATCHID)/>

			<!--- Close session if not valid --->
			<cfif validateContactString.ISVALIDATE NEQ 1>
				<cfinvoke method="completeSession">
					<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
				</cfinvoke>
				<cfthrow type="Application" message="Customer is no longer attend this conversation!"/>
			</cfif>

			<!--- Send one single MT --->
			<cfinvoke component="session.cfc.csc.csc" method="SendSingleMT" returnvariable="RetVarSendSingleMT">
				<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#"/>
				<cfinvokeargument name="inpIRESessionId" value="#arguments.inpSessionId#"/>
				<cfinvokeargument name="inpBatchId" value="#getSessionInfo.INFO.BATCHID#"/>
				<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
				<cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/>
			</cfinvoke>

			<cfif RetVarSendSingleMT.PRM NEQ "">
				<cfthrow message="#RetVarSendSingleMT.PRM#"/>
			</cfif>

			<cfif RetVarSendSingleMT.BLOCKEDBYDNC GT 0>
				<cfthrow type="Blocked" message="Message Blocked by Previous DNC request. Until they opt back in you can not message them."/>
			</cfif>

			<cfset dataout.RXRESULTCODE = RetVarSendSingleMT.RXRESULTCODE/>
			<cfset dataout.MESSAGE = RetVarSendSingleMT.MESSAGE/>
			<cfset dataout.ERRMESSAGE = RetVarSendSingleMT.ERRMESSAGE/>

			<cfif dataout.RXRESULTCODE GT 0 AND arguments.inpSkipPushWS EQ 0>
				<cfif len(arguments.inpTextToSend) GT 160>
					<cfinvoke method="SplitMessage" component="session.sire.models.cfc.smschat" returnvariable="RetVarSplit">
					    <cfinvokeargument name="inpText" value="#arguments.inpTextToSend#"/>
					</cfinvoke>
					<cfif RetVarSplit.RXRESULTCODE GT 0>
						<cfloop array="#RetVarSplit.TEXTARRAY#" index="text">
							<cfinvoke component="session.sire.models.cfc.smschat" method="PushWS" returnvariable="pushWS">
								<cfinvokeargument name="inpTextToSend" value="#text#"/>
								<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
								<cfinvokeargument name="inpType" value="1"/>
								<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
							</cfinvoke>
						</cfloop>
					</cfif>
				<cfelse>
					<cfinvoke component="session.sire.models.cfc.smschat" method="PushWS" returnvariable="pushWS">
						<cfinvokeargument name="inpTextToSend" value="#arguments.inpTextToSend#"/>
						<cfinvokeargument name="inpContactString" value="#getSessionInfo.INFO.CONTACTSTRING#"/>
						<cfinvokeargument name="inpType" value="1"/>
						<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
					</cfinvoke>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfif findNoCase("billing" ,dataout.MESSAGE)>
					<cfset dataout.MESSAGE = "You do not have enough credits to send message."/>
				</cfif>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="SplitMessage" access="public" hint="Split text message to x characters each">
		<cfargument name="inpText" required="true" type="string" default=""/>
		<cfargument name="inpLength" type="numeric" required="no" default="153"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.TEXTARRAY = [] />

		<cfset var text = trim(arguments.inpText)/>
		<cfset var length = arguments.inpLength/>

		<cfset var partNumber = 0/>
		<cfset var i = '' />
		<cfset var lastPart = 0/>

		<cftry>
			<cfif len(text) LTE length>
				<cfset dataout.MESSAGE = "Input text is valid already!"/>
				<cfset arrayAppend(dataout.TEXTARRAY, text)/>
				<cfset dataout.RXRESULTCODE = 1/>
			</cfif>
			<cfset partNumber = len(text)/length/>
			<cfset lastPart  = len(text)%length/>
			<cfloop from="1" to="#partNumber#" index="i">
				<cfset arrayAppend(dataout.TEXTARRAY, mid(text, (i-1)*length+1, length))/>
			</cfloop>
			<cfset arrayAppend(dataout.TEXTARRAY, right(text, lastPart))/>
			<cfset dataout.RXRESULTCODE = 1/>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="PushWS" output="false" access="public" hint="Push to web socket">
		<cfargument name="inpTextToSend" required="true" default="" />
		<cfargument name="inpContactString" required="true" default=""/>
		<cfargument name="inpType" required="true" default="1"/>
		<cfargument name="inpSessionId" required="true" default="0"/>
		<cfargument name="inpUserId" required="false" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var reg = '' />
		<cfset var channels = []/>
		<cfset var env = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_SESSION_" & "#arguments.inpSessionId#"), 6))/>
			<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_" & "#arguments.inpUserId#"), 6))/>
			<cfhttp url="https://ws.siremobile.com:8443/send" result="reg" method="post" timeout="10">
			    <cfhttpparam name="msg" type="formfield" value="#arguments.inpTextToSend#"/>
			    <cfhttpparam name="time" type="formfield" value="#timeFormat(now(), "HH:mm tt")# #dateFormat(now(), "yyyy/mm/dd")#"/>
			    <cfhttpparam name="phone" type="formfield" value="#arguments.inpContactString#"/>
			    <cfhttpparam name="channel" type="formfield" value="#serializeJSON(channels)#"/>
			    <cfhttpparam name="type" type="formfield" value="#arguments.inpType#"/>
			    <cfhttpparam name="userid" type="formfield" value="#arguments.inpUserId#"/>
			    <cfhttpparam name="sessionid" type="formfield" value="#arguments.inpSessionId#"/>
			    <cfhttpparam name="host" type="formfield" value="#CGI.SERVER_NAME#"/>
			</cfhttp>
			<cfif reg.status_code EQ "200">
				<cfset dataout.RXRESULTCODE = 1/>
			<cfelse>
				<cfset dataout.ERRMESSAGE = req.errordetail/>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetResponseForIRESession" access="remote" output="false" hint="Get response for ire sessionid">
		<cfargument name="inpSessionId" type="numeric" required="true" hint="session id"/>

		<cfset var dataout = {} />
		<cfset var getResponse = '' />
		<cfset var checkSession = ''/>
		<cfset var sessionReadResult = ''/>
		<cfset var GenChn = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RESPONSE = [] />
		<cfset dataout.SESSIONSTATE = 0/>
		<cfset dataout.SSCHN = '' />

		<cftry>
			<!--- Check session owner --->
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.UserId_int,
					ss.SessionState_int,
					b.EMS_Flag_int,
					ss.ContactString_vch
				FROM
					simplequeue.sessionire ss
				JOIN
					simpleobjects.batch b
				ON
					ss.BatchId_bi = b.BatchId_bi
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							ss.LastCP_int != 3
						)
					)
			</cfquery>

			<cfif checkSession.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Session ID not valid!"/>
			</cfif>

			<cfset dataout.SESSIONSTATE = checkSession.SessionState_int/>
			<cfset dataout.EMSFLAG = checkSession.EMS_Flag_int/>
			<cfset dataout.PREVIEW = len(checkSession.ContactString_vch) GTE 14/>

			<!--- Get response for session --->
			<cfquery name="getResponse" datasource="#session.DBSourceREAD#">
				SELECT
					ire.IREType_int,
					ire.Response_vch,
					ire.Created_dt,
					ire.T64_ti
				FROM
					simplexresults.ireresults ire
				WHERE
					ire.IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					ire.IREType_int IN (1,2)
				AND
					ire.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ire.Response_vch NOT LIKE "%SIRE - %"
				ORDER BY
					ire.IREResultsId_bi ASC
			</cfquery>
			<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->						
			<cfif getResponse.RECORDCOUNT GT 0>
				<cfloop query="getResponse">
					<cfset var item = {}/>
					<cfset item.TYPE = getResponse.IREType_int/>					 
										
					<cfif T64_ti EQ 1 >
						<cfset item.MSG =   ToString( ToBinary( getResponse.Response_vch ) ) />
					<cfelse>					
						<cfset item.MSG = getResponse.Response_vch />						
					</cfif>
					<cfset item.TIME = "#timeFormat(getResponse.Created_dt, "HH:mm tt")# #dateFormat(getResponse.Created_dt, "yyyy/mm/dd")#"/>
					<cfset arrayAppend(dataout.RESPONSE, item)/>
				</cfloop>
			</cfif>

			<cfinvoke method="setSessionRead" returnvariable="sessionReadResult">
				<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
			</cfinvoke>	

			<cfinvoke method="GenSessionChannel" returnvariable="GenChn">
				<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
			</cfinvoke>

			<cfset dataout.SSCHN = GenChn/>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetResponseForIRESessionCalendar" access="remote" output="false" hint="Get response for ire sessionid">
		<cfargument name="inpSessionId" type="numeric" required="true" hint="session id"/>

		<cfset var dataout = {} />
		<cfset var getResponse = '' />
		<cfset var checkSession = ''/>
		<cfset var sessionReadResult = ''/>
		<cfset var GenChn = '' />
		<cfset var ReminderSession = ''/>
		<cfset var GetDTSIdReminder = ''/>
		<cfset var GetSessionReminder =''/>

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RESPONSE = [] />
		<cfset dataout.SESSIONSTATE = 0/>
		<cfset dataout.SSCHN = '' />

		<cftry>
			<!--- Check session owner --->
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.UserId_int,
					ss.SessionState_int,
					b.EMS_Flag_int,
					ss.ContactString_vch
				FROM
					simplequeue.sessionire ss
				JOIN
					simpleobjects.batch b
				ON
					ss.BatchId_bi = b.BatchId_bi
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							ss.LastCP_int != 3
						)
					)
			</cfquery>

			<cfif checkSession.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Session ID not valid!"/>
			</cfif>

			<cfset dataout.SESSIONSTATE = checkSession.SessionState_int/>
			<cfset dataout.EMSFLAG = checkSession.EMS_Flag_int/>
			<cfset dataout.PREVIEW = len(checkSession.ContactString_vch) GTE 14/>
			
			<!--- Get reminder DTSId --->
			<cfquery name="GetDTSIdReminder" datasource="#Session.DBSourceEBM#">
				SELECT 
					ev.ConfirmationOneDTSId_int
				FROM 
					calendar.events ev
				INNER JOIN
					simplexresults.contactresults rs ON ev.ConfirmationOneDTSId_int = rs.DTSID_int
				WHERE 
					ev.ChatSessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND 
					ev.ConfirmationOneDTSId_int <> 0
				AND 
					ev.Active_int = 1
			</cfquery>
			<!--- Get reminder session --->
			<cfif GetDTSIdReminder.RecordCount GT 0>
				<cfquery name="GetSessionReminder" datasource="#Session.DBSourceEBM#">
					SELECT 
						SessionId_bi
					FROM 
						simplequeue.sessionire
					WHERE 
						DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetDTSIdReminder["ConfirmationOneDTSId_int"]#">
				</cfquery>
				<cfif GetSessionReminder.RecordCount GT 0>
					<cfset ReminderSession = GetSessionReminder["SessionId_bi"]>
				</cfif>
			</cfif>
			<!--- Get response for session --->
			<cfquery name="getResponse" datasource="#session.DBSourceREAD#">
				SELECT
					ire.IREType_int,
					ire.Response_vch,
					ire.Created_dt,
					ire.T64_ti
				FROM
					simplexresults.ireresults ire
				WHERE
					ire.IREType_int IN (1,2)
				AND
					(
						ire.IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
						<cfif ReminderSession NEQ "">
						OR 
							ire.IRESessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#ReminderSession#"/>
						</cfif>
					)
				AND
					ire.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ire.Response_vch NOT LIKE "%SIRE - %"
				ORDER BY
					ire.IREResultsId_bi ASC
			</cfquery>
			<!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->						
			<cfif getResponse.RECORDCOUNT GT 0>
				<cfloop query="getResponse">
					<cfset var item = {}/>
					<cfset item.TYPE = getResponse.IREType_int/>					 
										
					<cfif T64_ti EQ 1 >
						<cfset item.MSG =   ToString( ToBinary( getResponse.Response_vch ) ) />
					<cfelse>					
						<cfset item.MSG = getResponse.Response_vch />						
					</cfif>
					<cfset item.TIME = "#timeFormat(getResponse.Created_dt, "HH:mm tt")# #dateFormat(getResponse.Created_dt, "yyyy/mm/dd")#"/>
					<cfset arrayAppend(dataout.RESPONSE, item)/>
				</cfloop>
			</cfif>

			<cfinvoke method="setSessionRead" returnvariable="sessionReadResult">
				<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
			</cfinvoke>	

			<cfinvoke method="GenSessionChannel" returnvariable="GenChn">
				<cfinvokeargument name="inpSessionId" value="#arguments.inpSessionId#"/>
			</cfinvoke>

			<cfset dataout.SSCHN = GenChn/>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GenSessionChannel" access="public" hint="Generate session web socket channel" output="false">
		<cfargument name="inpSessionId" required="true"/>

		<cfreturn left(hash("#EnvPrefix#"&"SIRE_CHAT_SESSION_" & "#arguments.inpSessionId#"), 6)/>
	</cffunction>

	<cffunction name="GetActiveSessionForUser" access="public" output="false" hint="Get current active chat session of user">
		<cfargument name="inpUserId" required="true" type="numeric" hint="User id" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var getSession = {} />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.SESSIONIDS = []/>

		<cftry>
			<cfquery name="getSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.SessionId_bi
				FROM
					simplequeue.sessionire ss
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
				AND
					ss.SessionState_int = 1
			</cfquery>

			<cfif getSession.RECORDCOUNT GT 0>
				<cfloop query="getSession">
					<cfset arrayAppend(dataout.SESSIONIDS, getSession.SessionId_bi)/>
				</cfloop>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get session successfully"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetActiveSessionForContactString" access="remote" output="false" hint="Get current active chat session of user">
		<cfargument name="inpContactString" required="true" type="string" hint="Contact string to get"/>

		<cfset var dataout = {} />
		<cfset var getSession = {} />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.SESSIONID = 0/>

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>

			<cfquery name="getSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.SessionId_bi
				FROM
					simplequeue.sessionire ss
				INNER JOIN
					simpleobjects.batch b
				ON
					ss.BatchId_bi = b.BatchId_bi
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.SessionState_int = 1
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					b.EMS_Flag_int = 20
				ORDER BY
					ss.SessionId_bi DESC
				LIMIT
					1
			</cfquery>

			<cfif getSession.RECORDCOUNT EQ 1>
				<cfset dataout.SESSIONID = getSession.SessionId_bi/>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get session successfully"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetSessionDetails" access="public" output="false" hint="Get contact string, keyword for chat session">
		<cfargument name="inpSessionId" required="true" type="numeric" hint="Session id" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var getSessionInfo = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cfset dataout.INFO = {} />
		<cfset dataout.INFO.KEYWORD = '' />
		<cfset dataout.INFO.CONTACTSTRING = '' />
		<cfset dataout.INFO.BATCHID = '' />
		<cfset dataout.INFO.SESSIONSTATE = 0/>

		<cftry>
			<cfquery name="getSessionInfo" datasource="#session.DBSourceREAD#">
				SELECT
					k.Keyword_vch,
					s.ContactString_vch,
					s.BatchId_bi,
					s.SessionState_int
				FROM
					simplequeue.sessionire s
				INNER JOIN
					sms.keyword k
				ON
					k.BatchId_bi = s.BatchId_bi
				INNER JOIN
					simpleobjects.batch b
				ON
					b.BatchId_bi = s.BatchId_bi
				WHERE
					s.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							s.LastCP_int != 3
						)
					)
			</cfquery>

			<cfif getSessionInfo.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.INFO.KEYWORD = getSessionInfo.Keyword_vch/>
				<cfset dataout.INFO.CONTACTSTRING = getSessionInfo.ContactString_vch/>
				<cfset dataout.INFO.BATCHID = getSessionInfo.BatchId_bi/>
				<cfset dataout.INFO.SESSIONSTATE = getSessionInfo.SessionState_int/>
				<cfset dataout.INFO.EMSFLAG = getSessionInfo.EMS_Flag_int/>
				<cfset dataout.INFO.TEMPLATEID = getSessionInfo.TemplateId_int/>
				<cfset dataout.MESSAGE = "Get info success"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="ValidateContactStringSession" access="public" hint="Check contact string for overlap with other user">
		<cfargument name="inpContactString" required="true" type="string" hint="contact string to check"/>
		<cfargument name="inpBatchId" required="true" type="string" hint="batch id to check"/>

		<cfset var dataout = {} />
		<cfset var getSession = '' />
		<cfset var getUserShortCode = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.ISVALIDATE = 0/>

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="getUserShortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getUserShortCode"></cfinvoke>

			<cfquery name="getSession" datasource="#session.DBSourceREAD#">
				SELECT
					s.SessionId_bi,
					s.UserId_int,
					b.BatchId_bi
				FROM
					simplequeue.sessionire s
				INNER JOIN
					simpleobjects.batch b
				ON
					s.BatchId_bi = b.BatchId_bi
				INNER JOIN
					sms.keyword k
				ON
					k.BatchId_bi = s.BatchId_bi
				WHERE
					s.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>				
				AND
					k.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUserShortCode.SHORTCODEID#"/>
				AND
					s.SessionState_int = 1
				ORDER BY
					s.SessionId_bi DESC
				LIMIT
					1
			</cfquery>

			<cfif getSession.RECORDCOUNT GT 0>
				<cfif getSession.BatchId_bi EQ arguments.inpBatchId>
					<cfset dataout.ISVALIDATE = 1/>
				</cfif>
			<cfelse>
				<cfset dataout.ISVALIDATE = 1/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1/>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE/>
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
	<cffunction name="CheckExistChatSession" access="remote" hint="CheckAvailableChatSession with customer">
		<cfargument name="inpContactString" required="true" type="string" hint="Customer phone number"/>
		
		<cfset var dataout = {} />		
		<cfset var CheckExistChatSession = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.AVAILABLESESSION = 0 />
		<cfset dataout.CHATSESSION = 0 />
		<cfset dataout.KEYWORD = '' />
		<cfset dataout.ALERTRETURN = '' />
		
		

		<cftry>
			<cfquery name="CheckExistChatSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.SessionId_bi,
					k.Keyword_vch					
				FROM
					simplequeue.sessionire ss
				LEFT JOIN
					simpleobjects.batch b
					ON
					ss.BatchId_bi = b.BatchId_bi
				INNER JOIN 
					sms.keyword AS k ON k.BatchId_bi = b.BatchId_bi 	
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					ss.SessionState_int = 1
				AND 
					k.Active_int = 1
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							ss.LastCP_int != 3
						)
					)
				LIMIT 1
			</cfquery>
			<cfif CheckExistChatSession.RECORDCOUNT GT 0>
				<cfset dataout.AVAILABLESESSION = 1 />
				<cfset dataout.CHATSESSION = CheckExistChatSession.SessionId_bi />
				<cfset dataout.KEYWORD = CheckExistChatSession.Keyword_vch />
				<cfset dataout.ALERTRETURN = "You can only have one <a href='/session/sire/pages/sms-response?keyword=#CheckExistChatSession.Keyword_vch#&ssid=#CheckExistChatSession.SessionId_bi#'>active chat session</a> per subscriber, if you need to create a new session please end the current one." />
			</cfif>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>

	</cffunction>
	<cffunction name="MakeNewChatSessionForContactString" access="remote" hint="Make new chat session with customer">
		<cfargument name="inpContactString" required="true" type="string" hint="Customer phone number"/>
		<cfargument name="inpUserId" required="true" type="numeric" default="#session.UserId#" hint="Sire user id"/>
		<cfargument name="inpKeyword" required="true" type="string" hint="Chat keyword"/>
		<cfargument name="inpSkipCheckOPT" required="false" default="0" type="string" hint="check phone number opt in system"/>

		<cfset var dataout = {} />
		<cfset var makeNewChatSession = ''/>
		<cfset var checkSession = '' />
		<cfset var getBatchIdOfKeyword = '' />
		<cfset var getUserShortCode = '' />
		<cfset var RetVarValidateContactString = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.NEWSESSIONID = 0/>
		<cfset dataout.AVALABLESESSIONID = 0/>

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
            </cfloop>
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.SessionId_bi,
					k.Keyword_vch					
				FROM
					simplequeue.sessionire ss
				LEFT JOIN
					simpleobjects.batch b
					ON
					ss.BatchId_bi = b.BatchId_bi
				INNER JOIN 
					sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1) 	
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					ss.SessionState_int = 1
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							ss.LastCP_int != 3
						)
					)
				LIMIT 1
			</cfquery>

			<cfif checkSession.RECORDCOUNT GT 0>
				<cfset dataout.AVALABLESESSIONID = checkSession.SessionId_bi/>
				<cfthrow type="Application" message="You can only have one <a href='/session/sire/pages/sms-response?keyword=#checkSession.Keyword_vch#&ssid=#checkSession.SessionId_bi#'>active chat session</a> per subscriber, if you need to create a new session please end the current one."/>
			</cfif>
			<cfif arguments.inpSkipCheckOPT EQ "0">
				<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
					SELECT
						ss.SessionId_bi
					FROM
						simplequeue.sessionire ss
					INNER JOIN
						simpleobjects.batch b
					ON
						ss.BatchId_bi = b.BatchId_bi 
					WHERE
						ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
					AND
						ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
					AND
						(
							b.EMS_Flag_int = 20
						OR
							(
								b.TemplateId_int = 9
							AND
								ss.LastCP_int != 3
							)
						)
				</cfquery>

				<cfif checkSession.RECORDCOUNT LT 1>
					<cfthrow type="Application" message="You haven't had a conversation with this customer"/>
				</cfif>
			</cfif>
			<cfquery name="getBatchIdOfKeyword" datasource="#session.DBSourceREAD#">
				SELECT
					k.BatchId_bi
				FROM
					sms.keyword k
				WHERE
					k.Keyword_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpKeyword#"/>
				AND
					k.Active_int = 1
			</cfquery>

			<cfif getBatchIdOfKeyword.RECORDCOUNT NEQ 1>
				<cfthrow type="Application" message="Keyword invalid!"/>
			</cfif>

			<cfinvoke method="ValidateContactStringSession" returnvariable="RetVarValidateContactString">
				<cfinvokeargument name="inpContactString" value="#arguments.inpContactString#"/>
				<cfinvokeargument name="inpBatchId" value="#getBatchIdOfKeyword.BatchId_bi#"/>
			</cfinvoke>

			<cfif RetVarValidateContactString.ISVALIDATE LT 1>
				<cfthrow type="Application" message="The customer is not available right now! Please try again later."/>
			</cfif>

			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="getUserShortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getUserShortCode">
				<cfinvokeargument name="inpBatchId" value="#getBatchIdOfKeyword.BatchId_bi#">
			</cfinvoke>

			<cfquery result="makeNewChatSession" datasource="#session.DBSourceEBM#">
				INSERT INTO simplequeue.sessionire
                    (
                        SessionState_int,
                        CSC_vch,
                        ContactString_vch,
                        APIRequestJSON_vch,
                        LastCP_int,
                        XMLResultString_vch,
                        BatchId_bi,
                        UserId_int,
                        Created_dt,
                        LastUpdated_dt,
                        DeliveryReceipt_ti
                    )
                VALUES
                    (
                        1,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#getUserShortCode.SHORTCODE#"/>,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>,
                        '""',
                        4,
                        '',
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#getBatchIdOfKeyword.BatchId_bi#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>,
                        NOW(),
                        NOW(),
                        0                        
                    )
			</cfquery>

			<cfif makeNewChatSession.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Make new session successfully!"/>
				<cfset dataout.NEWSESSIONID = makeNewChatSession.GENERATEDKEY/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="MakeNewChatSessionForContactStringCalendar" access="remote" hint="Make new chat session with customer">
		<cfargument name="inpContactString" required="true" type="string" hint="Customer phone number"/>
		<cfargument name="inpUserId" required="true" type="numeric" default="#session.UserId#" hint="Sire user id"/>
		<cfargument name="inpKeyword" required="true" type="string" hint="Chat keyword"/>
		<cfargument name="inpSkipCheckOPT" required="false" default="0" type="string" hint="check phone number opt in system"/>

		<cfset var dataout = {} />
		<cfset var makeNewChatSession = ''/>
		<cfset var checkSession = '' />
		<cfset var getBatchIdOfKeyword = '' />
		<cfset var getUserShortCode = '' />
		<cfset var RetVarValidateContactString = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.NEWSESSIONID = 0/>
		<cfset dataout.AVALABLESESSIONID = 0/>

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfloop condition="LEFT(arguments.inpContactString,1) EQ '1' AND LEN(arguments.inpContactString) GT 1">
            	<cfset arguments.inpContactString = RIGHT(arguments.inpContactString, LEN(arguments.inpContactString) - 1) />
            </cfloop>
			<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
				SELECT
					ss.SessionId_bi,
					k.Keyword_vch					
				FROM
					simplequeue.sessionire ss
				LEFT JOIN
					simpleobjects.batch b
					ON
					ss.BatchId_bi = b.BatchId_bi
				INNER JOIN 
					sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1) 	
				WHERE
					ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					ss.SessionState_int = 1
				AND
					(
						b.EMS_Flag_int = 20
					OR
						(
							b.TemplateId_int = 9
						AND
							ss.LastCP_int != 3
						)
					)
				LIMIT 1
			</cfquery>

			<cfif checkSession.RECORDCOUNT GT 0>
				<cfset dataout.AVALABLESESSIONID = checkSession.SessionId_bi/>
				<cfthrow type="Application" message="You can only have one active chat session per subscriber, if you need to create a new session please end the current one."/>
			</cfif>
			<cfif arguments.inpSkipCheckOPT EQ "0">
				<cfquery name="checkSession" datasource="#session.DBSourceREAD#">
					SELECT
						ss.SessionId_bi
					FROM
						simplequeue.sessionire ss
					INNER JOIN
						simpleobjects.batch b
					ON
						ss.BatchId_bi = b.BatchId_bi 
					WHERE
						ss.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
					AND
						ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
					AND
						(
							b.EMS_Flag_int = 20
						OR
							(
								b.TemplateId_int = 9
							AND
								ss.LastCP_int != 3
							)
						)
				</cfquery>

				<cfif checkSession.RECORDCOUNT LT 1>
					<cfthrow type="Application" message="You haven't had a conversation with this customer"/>
				</cfif>
			</cfif>
			<cfquery name="getBatchIdOfKeyword" datasource="#session.DBSourceREAD#">
				SELECT
					k.BatchId_bi
				FROM
					sms.keyword k
				WHERE
					k.Keyword_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpKeyword#"/>
				AND
					k.Active_int = 1
			</cfquery>

			<cfif getBatchIdOfKeyword.RECORDCOUNT NEQ 1>
				<cfthrow type="Application" message="Keyword invalid!"/>
			</cfif>

			<cfinvoke method="ValidateContactStringSession" returnvariable="RetVarValidateContactString">
				<cfinvokeargument name="inpContactString" value="#arguments.inpContactString#"/>
				<cfinvokeargument name="inpBatchId" value="#getBatchIdOfKeyword.BatchId_bi#"/>
			</cfinvoke>

			<cfif RetVarValidateContactString.ISVALIDATE LT 1>
				<cfthrow type="Application" message="The customer is not available right now! Please try again later."/>
			</cfif>

			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="getUserShortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getUserShortCode">
				<cfinvokeargument name="inpBatchId" value="#getBatchIdOfKeyword.BatchId_bi#">
			</cfinvoke>

			<cfquery result="makeNewChatSession" datasource="#session.DBSourceEBM#">
				INSERT INTO simplequeue.sessionire
                    (
                        SessionState_int,
                        CSC_vch,
                        ContactString_vch,
                        APIRequestJSON_vch,
                        LastCP_int,
                        XMLResultString_vch,
                        BatchId_bi,
                        UserId_int,
                        Created_dt,
                        LastUpdated_dt,
                        DeliveryReceipt_ti
                    )
                VALUES
                    (
                        1,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#getUserShortCode.SHORTCODE#"/>,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>,
                        '""',
                        4,
                        '',
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#getBatchIdOfKeyword.BatchId_bi#"/>,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>,
                        NOW(),
                        NOW(),
                        0                        
                    )
			</cfquery>

			<cfif makeNewChatSession.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Make new session successfully!"/>
				<cfset dataout.NEWSESSIONID = makeNewChatSession.GENERATEDKEY/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>
	<cffunction name="CheckExistChatCampaign" access="remote" hint="Check exist chat campaign" output="false">
		<cfset var dataout = {} />		

		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.KEYWORD = '' />
		
		<cfset var shortcode = '' />
		<cfset var CheckExistChatCampaign = '' />
		
		<cftry>
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode"></cfinvoke>
			<cfquery name="CheckExistChatCampaign" datasource="#session.DBSourceREAD#">
				SELECT
					b.BatchId_bi,
					k.Keyword_vch
				FROM
					simpleobjects.batch b
				INNER JOIN 
					sms.keyword k
					ON b.BatchId_bi=k.BatchId_bi 	
				WHERE
					k.Keyword_vch IS NOT NULL
				AND
					b.ShortCodeId_int= <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>	
				AND
					b.UserId_int=<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>			
				AND
					b.TemplateId_int=9
				AND
					k.Active_int = 1
				ORDER BY 
					b.Created_dt DESC
				LIMIT 1
			</cfquery>
			<cfif CheckExistChatCampaign.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.KEYWORD = CheckExistChatCampaign.Keyword_vch />
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>		
		
	</cffunction>
	<cffunction name="GetKeywordDetails" access="public" hint="Get keyword information" output="false">
		<cfargument name="inpKeyword" required="true" type="string"/>

		<cfset var dataout = {} />
		<cfset var getKeyword = '' />

		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RXRESULTCODE = -1/>

		<cfset dataout.INFO = {} />
		<cfset dataout.INFO.BATCHID = 0/>
		<cfset dataout.INFO.SHORTCODEID = 0/>
		<cfset dataout.INFO.SHORTCODE = ''/>

		<cfset var shortcode = '' />

		<cftry>
			
			<!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortcode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode"></cfinvoke>
			<cfquery name="getKeyword" datasource="#session.DBSourceREAD#">
				SELECT
					k.BatchId_bi,
					b.EMS_Flag_int,
					b.TemplateId_int,
					k.ShortCodeId_int,
					sc.SHortCode_vch
				FROM
					sms.keyword k
				INNER JOIN
					simpleobjects.batch b
				ON
					k.BatchId_bi = b.BatchId_bi
				INNER JOIN
					sms.shortcode sc
				ON
					k.ShortCodeId_int = sc.ShortCodeId_int
				WHERE
					b.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					k.Keyword_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpKeyword#"/>
				AND
					k.Active_int = 1
				AND
					sc.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
				AND
					(
						b.EMS_Flag_int = 20
					OR
						b.TemplateId_int = 9
					)
				LIMIT
					1
			</cfquery>

			<cfif getKeyword.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.INFO.BATCHID = getKeyword.BatchId_bi/>
				<cfset dataout.INFO.EMSFLAG = getKeyword.EMS_Flag_int/>
				<cfset dataout.INFO.TEMPLATEID = getKeyword.TemplateId_int/>
				<cfset dataout.INFO.SHORTCODEID = getKeyword.ShortCodeId_int/>
				<cfset dataout.INFO.SHORTCODE = getKeyword.SHortCode_vch/>
				<cfset dataout.MESSAGE = "Get keyword information successfully!"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetCityByPhoneOrAreaCode" access="public" output="false" hint="Get city by phone prefix, NPA, NXX">
		<!--- This function take input either a contact string or NPA-NXX combination, do not pass in both of these type --->
		<cfargument name="inpContactString" required="false" default=""/>
		<cfargument name="inpNPA" required="false" default=""/>
		<cfargument name="inpNXX" required="false" default=""/>

		<cfset var dataout = {} />
		<cfset var NPA = '' />
		<cfset var NXX = '' />
		<cfset var contactString = '' />
		<cfset var prefix = ''/>

		<cfset var getCity = '' />

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.CITY = '' />

		<cftry>
			<cfif arguments.inpContactString EQ "" AND (arguments.inpNXX EQ "" OR arguments.inpNPA EQ "")>
				<cfthrow type="Application" message="Invalid input!"/>
			</cfif>
			<cfif arguments.inpContactString NEQ "">
				<!--- Only take 10 or 11 string length contact --->
				<cfif len(arguments.inpContactString) NEQ 10 OR len(arguments.inpContactString) NEQ 11>
					<cfthrow type="Application" message="Invalid contact string"/>
				</cfif>
				<!--- Bypass first number if contact is 11 string length --->
				<cfif len(arguments.inpContactString) EQ 11>
					<cfset contactString = right(arguments.inpContactString, 10)/>
				<cfelse>
					<cfset contactString = arguments.inpContactString/>
				</cfif>
				<cfset NPA = left(contactString,3)/>
				<cfset NXX = mid(contactString, 4, 3)/>
			<cfelse>
				<cfset NPA = arguments.inpNPA/>
				<cfset NXX = arguments.inpNXX/>
			</cfif>
			<cfif len(NPA) NEQ 3 OR len(NXX) NEQ 3>
				<cfthrow type="Application" message="Invalid area code!"/>
			</cfif>
			<cfquery name="getCity" datasource="#session.DBSourceREAD#">
				SELECT
					fx.CITY
				FROM
					melissadata.fone fx
				WHERE
					fx.NPA = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NPA#"/>
				AND
					fx.NXX = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NXX#"/>
				LIMIT
					1
			</cfquery>
			<cfif getCity.RECORDCOUNT GT 0>
				<cfset dataout.MESSAGE = "Get city successfully!"/>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.CITY = getCity.CITY/>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
	

	<!--- Phase 1 Feedback 1 --->
	<cffunction name="getListSessionForView2" access="remote" output="false" hint="Get list sessions, key word detail info">
		<cfargument name="inpBatchId" TYPE="numeric" required="yes" default="0" />
		<cfargument name="inpContactString" TYPE="string" required="no" default="" />
		<cfargument name="inpSessionState" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1000" />
		<cfargument name="inpShowPreview" TYPE="numeric" required="no" default="0"/>
		
		<cfset var dataout = StructNew() />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1) />
		<cfset dataout["iTotalRecords"] = 0 />
		<cfset dataout["iTotalDisplayRecords"] = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var listSession = "" />
		<cfset var countSession = "" />
		<cfset var item = {} />

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
			<cfquery name="countSession" datasource="#Session.DBSourceREAD#">
				SELECT 
					<cfswitch expression="#arguments.inpSessionState#">
					<cfcase value="1">
					COUNT(IF(SSM.MessageUnread_int IS NOT NULL AND SSM.MessageUnread_int > 0, 1, NULL)) AS total
					</cfcase>
					<cfcase value="2">
					COUNT(IF(SSM.MessageUnread_int IS NULL OR SSM.MessageUnread_int = 0, 1, NULL)) AS total
					</cfcase>
					<cfcase value="101">
					COUNT(IF(S.SessionState_int < 4, 1, NULL)) AS total
					</cfcase>
					<cfcase value="102">
					COUNT(IF(S.SessionState_int >= 4, 1, NULL)) AS total
					</cfcase>
					<cfdefaultcase>
					COUNT(S.SessionId_bi) AS total
					</cfdefaultcase>
					</cfswitch>
				
				FROM
					`simplequeue`.`sessionire` AS S
				
				LEFT JOIN 
					`simplequeue`.`smschat_sessionire_messageunread` AS SSM
					ON
						S.SessionId_bi = SSM.SessionId_bi

				WHERE
					S.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#" /> AND 
					S.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#" />
					<cfif arguments.inpContactString NEQ "">
						AND S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactString#%" />
					</cfif>
					<cfif arguments.inpShowPreview EQ 0>
						AND
							LENGTH(S.ContactString_vch) < 14
					</cfif>
			</cfquery>

			<cfif countSession.total GT 0>

				<cfquery name="listSession" datasource="#Session.DBSourceREAD#">
					SELECT 
						list2.BatchId_bi, 
						list2.SessionId_bi, 
						list2.SessionState_int, 
						list2.ContactString_vch, 
						list2.NewMessage, 
						list2.LastUpdated_dt,
						IF(SSM.MessageUnread_int IS NOT NULL, SSM.MessageUnread_int, 0) AS MessageUnread_int
						
					FROM
						(
						SELECT 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch, 
							-- IF(2 = SUBSTRING_INDEX(GROUP_CONCAT(IREType_int ORDER BY LastUpdated_dt DESC, IREResultsId_bi DESC SEPARATOR ','), ',', 1), 1, 0) AS NewMessage, 
							IF(('2,1' = SUBSTRING_INDEX(GROUP_CONCAT(IREType_int ORDER BY LastUpdated_dt ASC, IREResultsId_bi ASC SEPARATOR ','), ',', 2)) AND (SUM(CASE WHEN IREType_int = 1 THEN 1 ELSE 0 END) = 1), 1, 0) AS NewMessage,
							MAX(LastUpdated_dt) AS LastUpdated_dt
							
						FROM
							(
							SELECT 
								S.BatchId_bi, 
								S.SessionId_bi, 
								S.SessionState_int, 
								S.ContactString_vch, 
								S.LastUpdated_dt, 
								IF(R.IREType_int IS NULL, 1, R.IREType_int) AS IREType_int, 
								R.IREResultsId_bi
							
							FROM 
								(
								SELECT 
									S.BatchId_bi, 
									S.SessionId_bi, 
									S.SessionState_int, 
									S.ContactString_vch,
									S.LastUpdated_dt
								
								FROM
									`simplequeue`.`sessionire` AS S
								
								WHERE
									S.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#"> AND 
									S.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
									<cfswitch expression="#arguments.inpSessionState#">
									<cfcase value="101">
										AND S.SessionState_int < 4
									</cfcase>
									<cfcase value="102">
										AND S.SessionState_int >= 4
									</cfcase>
									</cfswitch>
									<cfif arguments.inpContactString NEQ "">
										AND S.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpContactString#%" />
									</cfif>
									<cfif arguments.inpShowPreview EQ 0>
										AND
											LENGTH(S.ContactString_vch) < 14
									</cfif>
								) AS S
							
							LEFT JOIN 
								`simplexresults`.`ireresults` R 
								ON 
									R.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#" /> AND
									R.IREType_int IN (1,2) AND 
									S.SessionId_bi = R.IRESessionId_bi
							
							ORDER BY 
								LastUpdated_dt DESC, 
								IREResultsId_bi DESC
							) AS list1
							
						GROUP BY 
							BatchId_bi, 
							SessionId_bi, 
							SessionState_int, 
							ContactString_vch 
						) AS list2

					LEFT JOIN 
						`simplequeue`.`smschat_sessionire_messageunread` AS SSM
						ON
							list2.SessionId_bi = SSM.SessionId_bi

					<cfswitch expression="#arguments.inpSessionState#">
					<cfcase value="1">
					WHERE
						MessageUnread_int > 0 AND list2.SessionState_int <>4
					</cfcase>
					<cfcase value="2">
					WHERE
						MessageUnread_int = 0
					</cfcase>
					</cfswitch>

					ORDER BY 
						NewMessage DESC, 
						LastUpdated_dt DESC

					LIMIT 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#" />, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#" />
				</cfquery>

				<cfif listSession.RecordCount GT 0>
					<cfloop query="listSession">
						<cfset item = {
							"BatchId_bi": listSession.BatchId_bi, 
							"SessionId_bi": listSession.SessionId_bi, 
							"SessionState_int": listSession.SessionState_int, 
							"ContactString_vch": formatContactString(listSession.ContactString_vch), 
							"NewMessage": listSession.NewMessage, 
							"LastUpdated_dt": "#dateFormat(listSession.LastUpdated_dt, "yyyy-mm-dd")# #timeFormat(listSession.LastUpdated_dt, "HH:mm:ss")#", 
							"MessageUnread_int": listSession.MessageUnread_int,
							"Preview": (len(listSession.ContactString_vch) GTE 14 ? 1 : 0)
						} />
						<cfset arrayAppend(dataout["aaData"],item) />
					</cfloop>

					<cfset dataout["iTotalRecords"] = countSession.total />
					<cfset dataout["iTotalDisplayRecords"] = countSession.total />
				</cfif>

			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>


	<cffunction name="setSessionRead" access="remote" output="false" hint="Set session was read">
		<cfargument name="inpSessionId" TYPE="numeric" required="yes" default="0" />

		<cfset var dataout = StructNew() />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var updateSessionRead = '' />

		<cftry>

			<cfquery result="updateSessionRead" datasource="#session.DBSourceEBM#">
				UPDATE
					`simplequeue`.`smschat_sessionire_messageunread`
				SET 
					MessageUnread_int = 0
				WHERE
					SessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSessionId#">
			</cfquery>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />

	</cffunction>

	<cffunction name="GetChatSetting" access="public" output="false" hint="Get user smschat setting, make new setting if not exist">
		<cfset var dataout = {} />
		<cfset dataout.DefaultView = 1 />
		<cfset dataout.EnterToSend = 0/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1/>

		<cfset var GetSetting = '' />
		<cfset var MakeNewSetting = '' />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="GetSetting">
				SELECT
					DefaultView_ti AS DefaultView,
					EnterToSend_ti AS EnterToSend
				FROM
					simpleobjects.smschat_user_settings
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				LIMIT
					1
			</cfquery>
			<cfif GetSetting.RECORDCOUNT GT 0>
				<cfset dataout.DefaultView = GetSetting.DefaultView/>
				<cfset dataout.EnterToSend = GetSetting.EnterToSend/>
				<cfset dataout.RXRESULTCODE = 1/>
			<cfelse>
				<cfquery datasource="#Session.DBSourceEBM#" result="MakeNewSetting">
					INSERT INTO
						simpleobjects.smschat_user_settings
					(
						UserId_int,
						DefaultView_ti,
						EnterToSend_ti
					)
					VALUES
					(
						<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
						<cfqueryparam cfsqltype="cf_sql_integer" value="1"/>,
						<cfqueryparam cfsqltype="cf_sql_integer" value="0"/>
					)
				</cfquery>
				<cfset dataout.DefaultView = 1/>
				<cfset dataout.EnterToSend = 0/>
				<cfset dataout.RXRESULTCODE = 1/>
			</cfif>
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="UpdateUserSetting" access="remote" output="false" hint="Update user smschat settings">
		<cfargument name="inpSettingType" required="true" type="numeric"/>
		<cfargument name="inpValue" required="true" type="numeric"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cfset var updateSetting = '' />
		<cfset var fieldName = '' />

		<cftry>
			<cfswitch expression="#arguments.inpSettingType#">
				<cfcase value="1">
					<cfset fieldName = "DefaultView_ti"/>
				</cfcase>
				<cfcase value="2">
					<cfset fieldName = "EnterToSend_ti"/>
				</cfcase>
				<cfdefaultcase>
					<cfthrow message="Not valid setting type"/>
				</cfdefaultcase>
			</cfswitch>
			<cfquery datasource="#session.DBSourceEBM#" result="updateSetting">
				UPDATE
					simpleobjects.smschat_user_settings
				SET
					#fieldName# = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpValue#"/>
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
			</cfquery>
			<cfif updateSetting.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Update setting successfully!"/>
			</cfif>
			<cfcatch>
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="UpdateAPISettings" access="public" output="false" hint="Update API settings for XML String">
		<cfargument name="inpXMLControlString_vch" required="true" type="string"/>
		<cfargument name="inpSettings_obj" required="true" type="struct"/>
		<cfargument name="inpWelcomeMsg" required="false" type="string" default=""/>
		<cfargument name="inpEndMsg" required="false" type="string" default=""/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.XMLControlString = ""/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var XMLControlString_vch = arguments.inpXMLControlString_vch />
		<cfset var xmlControl = {} />
		<cfset var controlPointArray = [] />
		<cfset var RXSSCSCINDEX = 1>
		<cfset var controlPointIndex = 0 />
		<cfset var APIDate = {} />
		<cfset var settingObj = '' />
		<cfset var APIRQ = 1 />
		<cfset var children = '' />

		<cftry>

			<cfset xmlControl = XmlParse("<XMLControlStringDoc>" & XMLControlString_vch & "</XMLControlStringDoc>") />
			<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
				<cfif children.xmlName EQ 'RXSSCSC'>
					<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
						<cfset controlPointArray = children.XmlChildren>
					</cfif>
					<cfbreak>
				</cfif>
			</cfloop>

			<cfif !arrayIsEmpty(controlPointArray)>

				<cfloop from="1" to="#arrayLen(controlPointArray)#" index="controlPointIndex">
					<cfif controlPointArray[controlPointIndex].XmlAttributes.type EQ 'API'>
						<cfset APIDate = {} />
						<cfif IsJSON(controlPointArray[controlPointIndex].XmlAttributes.API_DATA)>
							<cfset APIDate = DeserializeJSON(controlPointArray[controlPointIndex].XmlAttributes.API_DATA) />
						</cfif>
						<cfloop collection="#arguments.inpSettings_obj#" item="settingObj">
							<cfset StructInsert(APIDate, settingObj, arguments.inpSettings_obj[settingObj], true) />
						</cfloop>
						<cfset StructInsert(APIDate, 'CPID', controlPointArray[controlPointIndex].XmlAttributes.ID, true) />
						<cfset StructInsert(APIDate, 'CPRQ', controlPointArray[controlPointIndex].XmlAttributes.RQ, true) />
						<cfset StructInsert(APIDate, 'APIRQ', APIRQ, true) />
						<cfset controlPointArray[controlPointIndex].XmlAttributes.API_DATA = SerializeJSON(APIDate) />
						<cfset APIRQ++ />
					<cfelseif controlPointArray[controlPointIndex].XmlAttributes.type EQ 'STATEMENT' AND arguments.inpWelcomeMsg NEQ "">
						<cfset controlPointArray[controlPointIndex].XmlAttributes.TEXT = arguments.inpWelcomeMsg/>
					<cfelseif controlPointArray[controlPointIndex].XmlAttributes.type EQ 'TRAILER' AND arguments.inpEndMsg NEQ "">
						<cfset controlPointArray[controlPointIndex].XmlAttributes.TEXT = arguments.inpEndMsg/>
					</cfif>
				</cfloop>

				<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
					<cfif children.xmlName EQ 'RXSSCSC'>
						<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
							<cfset xmlControl.XmlChildren[1].XmlChildren[RXSSCSCINDEX].XmlChildren = controlPointArray />
						</cfif>
						<cfbreak />
					</cfif>
					<cfset RXSSCSCINDEX++ />
				</cfloop>

				<cfset XMLControlString_vch = REReplaceNoCase(toString(xmlControl), '<\?[^>]+\?>', '') />
				<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, '<\/?XMLControlStringDoc>', '', "all") />
				<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, "'", '&apos;', "all") />
				<cfset XMLControlString_vch = REReplaceNoCase(XMLControlString_vch, '"', "'", "all") />

			</cfif>	

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.XMLControlString = XMLControlString_vch />

			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="getAPISettingsByBatchId" access="remote" output="false" hint="get api settings by batch id">
		<cfargument name="inpBatchId" required="true" type="numeric"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.SETTINGS = {}/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />
		<cfset dataout.WELCOMEMSG = '' />
		<cfset dataout.ENDMSG = '' />

		<cfset var getBatch = '' />
		<cfset var XMLControlString_vch = '' />
		<cfset var xmlControl = {} />
		<cfset var controlPointArray = [] />
		<cfset var controlPointIndex = 0 />
		<cfset var APIDate = {} />
		<cfset var children = '' />
		<cfset var xmlRXSSCSCElement = '' />
		<cfset var xmlStatement = '' />
		<cfset var xmlTrailer = '' />

		<cftry>

			<cfquery name="getBatch" datasource="#Session.DBSourceREAD#">
				SELECT 
					XMLControlString_vch

				FROM
					`simpleobjects`.`batch`

				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">

				LIMIT 1
			</cfquery>

			<cfif getBatch.RecordCount GT 0>
				<cfset XMLControlString_vch = getBatch.XMLControlString_vch />
				<cfset xmlControl = XmlParse("<XMLControlStringDoc>" & XMLControlString_vch & "</XMLControlStringDoc>") />
				<cfloop array="#xmlControl.XmlChildren[1].XmlChildren#" index="children">
					<cfif children.xmlName EQ 'RXSSCSC'>
						<cfif structKeyExists(children,'XmlChildren') AND isArray(children.XmlChildren) AND !arrayIsEmpty(children.XmlChildren)>
							<cfset controlPointArray = children.XmlChildren>
						</cfif>
						<cfbreak>
					</cfif>
				</cfloop>
				<cfif !arrayIsEmpty(controlPointArray)>
					<cfloop from="1" to="#arrayLen(controlPointArray)#" index="controlPointIndex">
						<cfif controlPointArray[controlPointIndex].XmlAttributes.type EQ 'API'>
							<cfset APIDate = {} />
							<cfif IsJSON(controlPointArray[controlPointIndex].XmlAttributes.API_DATA)>
								<cfset APIDate = DeserializeJSON(controlPointArray[controlPointIndex].XmlAttributes.API_DATA) />
							</cfif>
							<cfset dataout.SETTINGS = APIDate />
							<cfbreak />
						</cfif>
					</cfloop>
				</cfif>

				<cfif !structKeyExists(dataout.SETTINGS, "inpSMSList")>
					<cfset dataout.SETTINGS.inpSMSList = ""/>
				</cfif>
				<cfif !structKeyExists(dataout.SETTINGS, "inpEmailList")>
					<cfset dataout.SETTINGS.inpEmailList = ""/>
				</cfif>
				<cfif !structKeyExists(dataout.SETTINGS, "inpSendEmailStart")>
					<cfset dataout.SETTINGS.inpSendEmailStart = "false"/>
				</cfif>
				<cfif !structKeyExists(dataout.SETTINGS, "inpSendEmailReceived")>
					<cfset dataout.SETTINGS.inpSendEmailReceived = "false"/>
				</cfif>
				<cfif !structKeyExists(dataout.SETTINGS, "inpSendSMSStart")>
					<cfset dataout.SETTINGS.inpSendSMSStart = "false"/>
				</cfif>
				<cfif !structKeyExists(dataout.SETTINGS, "inpSendSMSReceived")>
					<cfset dataout.SETTINGS.inpSendSMSReceived = "false"/>
				</cfif>

				<cfset xmlRXSSCSCElement =  XmlSearch(xmlControl,"//RXSSCSC") />
				<cfset xmlStatement = XmlSearch(xmlRXSSCSCElement[1], "//Q[ @TYPE = 'STATEMENT' ]") />
				<cfset dataout.WELCOMEMSG = xmlStatement[1].XmlAttributes.text/>
				<cfset xmlTrailer = XmlSearch(xmlRXSSCSCElement[1], "//Q[ @TYPE = 'TRAILER' ]") />
				<cfset dataout.ENDMSG = xmlTrailer[1].XmlAttributes.text/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="saveAPISettings" access="remote" output="false" hint="save api settings by batch id">
		<cfargument name="inpBatchId" required="true" type="numeric"/>
		<cfargument name="inpSendEmailStart" TYPE="boolean" hint="Send email for user's email list when start a conversation." />
		<cfargument name="inpSendEmailReceived" TYPE="boolean" hint="Send email for user's email list when received a new message." />
		<cfargument name="inpEmailList" TYPE="string" hint="Email list will receive a notification." />
		<cfargument name="inpSendSMSStart" TYPE="boolean" hint="Send SMS for user's phone numer list when start a conversation." />
		<cfargument name="inpSendSMSReceived" TYPE="boolean" hint="Send SMS for user's phone numer list when received a new message." />
		<cfargument name="inpSMSList" TYPE="string" hint="Phone number list will receive a notification." />
		<cfargument name="inpWelcomeMsg" TYPE="string" hint="Welcome message customer receive when start a new chat"/>
		<cfargument name="inpEndMsg" TYPE="string" hint="Message customer receive when end chat"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.SETTINGS = {}/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var getBatch = '' />
		<cfset var XMLControlString_vch = '' />
		<cfset var UpdateBatch = '' />
		<cfset var UpdateBatchResult = '' />
		<cfset var UpdateAPIControlPoint = '' />

		<cftry>

			<cfquery name="getBatch" datasource="#Session.DBSourceREAD#">
				SELECT 
					XMLControlString_vch

				FROM
					`simpleobjects`.`batch`

				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">

				LIMIT 1
			</cfquery>

			<cfif getBatch.RecordCount GT 0>

				<cfset XMLControlString_vch = getBatch.XMLControlString_vch />

				<!--- Update API settings for XML String --->
				<cfinvoke method="UpdateAPISettings" returnvariable="UpdateAPIControlPoint">
					<cfinvokeargument name="inpXMLControlString_vch" value="#XMLControlString_vch#" />
					<cfinvokeargument name="inpSettings_obj" value="#{
						"inpSendEmailStart": arguments.inpSendEmailStart,
						"inpSendEmailReceived": arguments.inpSendEmailReceived,
						"inpEmailList": arguments.inpEmailList,
						"inpSendSMSStart": arguments.inpSendSMSStart,
						"inpSendSMSReceived": arguments.inpSendSMSReceived,
						"inpSMSList": arguments.inpSMSList
					}#" />
					<cfinvokeargument name="inpWelcomeMsg" value="#arguments.inpWelcomeMsg#"/>
					<cfinvokeargument name="inpEndMsg" value="#arguments.inpEndMsg#"/>
				</cfinvoke>

				<cfif UpdateAPIControlPoint.RXRESULTCODE EQ 1>
					<cfset XMLControlString_vch = UpdateAPIControlPoint.XMLControlString />
				</cfif>

				<cfquery name="UpdateBatch" datasource="#Session.DBSourceEBM#" result="UpdateBatchResult">
					UPDATE 
						`simpleobjects`.`batch`

					SET
						LastUpdated_dt = NOW(),
						XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">

					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Save settings successfully!" />

			<cfelse>
				<cfset dataout.MESSAGE = "Could not find the keyword" />
			</cfif>	

			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>

	<cffunction name="MakeNewChatForTellMgr" access="remote" output="false" hint="Make new chat conversation for tell the manager">
		<cfargument name="inpBatchId" required="true" hint="Batch id of tell manager template"/>
		<cfargument name="inpContactString" required="true" hint="Target contact string"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.SESSIONID = 0/>

		<cfset var ReactiveIRESession = '' />
		<cfset var GetSessionID = '' />

		<cftry>
			<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>

			<cfquery datasource="#session.DBSourceREAD#" name="GetSessionID">
				SELECT
					SessionId_bi
				FROM
					simplequeue.sessionire
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					SessionState_int = 4
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					LastCP_int = 3
				ORDER BY
					SessionId_bi
				LIMIT 1
			</cfquery>

			<cfif GetSessionID.RECORDCOUNT LT 1>
				<cfthrow message="Can't make new chat session"/>
			</cfif>

			<cfquery datasource="#session.DBSourceEBM#" result="ReactiveIRESession">
				UPDATE
					simplequeue.sessionire
				SET
					SessionState_int = 1,
					LastCP_int = 5
				WHERE
					SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetSessionID.SessionId_bi#"/>
			</cfquery>

			<cfif ReactiveIRESession.RECORDCOUNT GT 0>
				<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
					<cfinvokeargument name="userId" value="#session.userid#"/>
					<cfinvokeargument name="moduleName" value="Make chat for customer feedback"/>
					<cfinvokeargument name="operator" value="BatchID = #arguments.inpBatchId#. ContactString = #arguments.inpContactString#."/>
				</cfinvoke>

				<cfset dataout.SESSIONID = GetSessionID.SessionId_bi/>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Make new chat successfully!"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

</cfcomponent>